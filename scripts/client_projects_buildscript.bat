SET rootDir=%~dp0
echo "Root Dir:" %rootDir%
cd %rootDir%/../client

echo '----DOWNLOADING NODE DEPENDENCIES----"
call yarn install
call yarn install --production --prefer-offline
::echo '----REMOVING DEVELOPMENT DEPENDENCIES----"
::call npm prune --production

echo '----COPYING CLIENT PROJECT BUILD ARTIFACTS----"
call echo F|xcopy "%rootDir%/../installer/Resources/Portal/winser.cmd" "%rootDir%/../../buildartifacts/Portal/winser.cmd" /Y/V/F/I
call Xcopy "%rootDir%/../client/build" "%rootDir%/../../buildartifacts/Portal" /Y/V/F/s/q
call echo D|Xcopy "%rootDir%/../installer/Resources/Portal/keys" "%rootDir%/../../buildartifacts/Portal/keys" /Y/V/F/s/q
call echo D|Xcopy "%rootDir%/../client/node_modules" "%rootDir%/../../buildartifacts/Portal/node_modules" /Y/V/F/s/q
cd %rootDir%
exit /b 0

:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%