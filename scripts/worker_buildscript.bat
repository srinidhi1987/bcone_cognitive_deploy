SET rootDir=%~dp0
SET msbuild="C:/Program Files (x86)/MSBuild/14.0/Bin/MSBuild"
SET ZIP="C:/Program Files/7-Zip/7z.exe"
SET isveracodescan=%1
SET buildConfigurationTypeCPP=Release
SET buildConfigurationTypeNET=Release

if "%isveracodescan%" == "1" (
 echo inside if
 SET buildConfigurationTypeCPP=Debug
 SET buildConfigurationTypeNET=Debug
)

echo "Root Dir:" %rootDir%

ECHO "BUILDING Automation.iqbot.logger"
call %msbuild% "%rootDir%/../services\classifier\Automation.IQBot.Logger\Automation.IQBot.Logger.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.VisionBotEngine.Model"
call %msbuild% "%rootDir%/../services/classifier/VisionBotEngine.Model/Automation.Cognitive.VisionBotEngine.Model.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automatin.Cognitive.Validator.Models"
call %msbuild% "%rootDir%/../services/classifier/Validator.Common/Automatin.Cognitive.Validator.Models.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.Services.Client"
call %msbuild% "%rootDir%/../services/classifier/Automation.VisionBot.Services/Automation.Cognitive.Services.Client.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.VisionBotEngine.Validation"
call %msbuild% "%rootDir%/../services/classifier/Automation.VisionBotEngine.Validation/Automation.Cognitive.VisionBotEngine.Validation.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.Designer"
call %msbuild% "%rootDir%/../services/classifier/Automation.Designer/Automation.Cognitive.Designer.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.VisionBotEngine"
call %msbuild% "%rootDir%/../services/classifier/Automation.VisionBotEngine/Automation.Cognitive.VisionBotEngine.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.Worker.Visionbot"
call %msbuild% "%rootDir%/../services/classifier/Automation.Worker.Visionbot/Automation.Cognitive.Worker.Visionbot.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.Worker.Visionbot.Executor"
call %msbuild% "%rootDir%/../services/classifier/Automation.Worker.Visionbot.Executor/Automation.Cognitive.Worker.Visionbot.Executor.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.ValidatorServices"
call %msbuild% "%rootDir%/../services/classifier/ValidatorServices/Automation.Cognitive.ValidatorServices.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.Validator"
call %msbuild% "%rootDir%/../services/classifier/Automation.ValidatorLite/Automation.Cognitive.Validator.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.Configuration.Util"
call %msbuild% "%rootDir%/../services/classifier/Automation.Configuration.Util/Automation.Cognitive.Configuration.Util.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.DocumentClassifier"
call %msbuild% "%rootDir%/../services/classifier/Automation.DocumentClassifier/Automation.Cognitive.DocumentClassifier.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Cognitive.DocumentClassifier.Executor"
call %msbuild% "%rootDir%/../services/classifier/Automation.DocumentClassifier.Executor/Automation.Cognitive.DocumentClassifier.Executor.csproj" /t:rebuild /p:Configuration=%buildConfigurationTypeNET%;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "--- CPP PROJECTS---"

ECHO "BUILDING Automation.Cognitive.CppLogger"
call %msbuild% "%rootDir%/../services/classifier/Automation.Cognitive.CppLogger/Automation.Cognitive.CppLogger.vcxproj" /t:rebuild /p:Configuration=%buildConfigurationTypeCPP%;Platform=Win32;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING ClassifierDBManagerCpp"
call %msbuild% "%rootDir%/../services/classifier/Automation.ClassifierDBManagerCpp/ClassifierDBManagerCpp.vcxproj" /t:rebuild /p:Configuration=%buildConfigurationTypeCPP%;Platform=Win32
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.LayoutClassifier"
call %msbuild% "%rootDir%/../services/classifier/Automation.LayoutClassifier/Automation.LayoutClassifier.vcxproj" /t:rebuild /p:Configuration=%buildConfigurationTypeCPP%;Platform=Win32
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.PatternRecognition"
call %msbuild% "%rootDir%/../services/classifier/Automation.PatternRecognition/Automation.PatternRecognition.vcxproj" /t:rebuild /p:Configuration=%buildConfigurationTypeCPP%;Platform=Win32
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Tesseract"
call %msbuild% "%rootDir%/../services/classifier/Automation.Tesseract/Automation.Tesseract.vcxproj" /t:rebuild /p:Configuration=%buildConfigurationTypeCPP%;Platform=Win32
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

ECHO "BUILDING Automation.Tesseract4.0"
call %msbuild% "%rootDir%/../services/classifier/Automation.Tesseract4.0/Automation.Tesseract4.0.vcxproj" /t:rebuild /p:Configuration=%buildConfigurationTypeCPP%;Platform=Win32
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING VISIONBOT WORKER ARTIFACTS----"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../services/classifier/Automation.Worker.Visionbot/bin/x86/%buildConfigurationTypeNET%" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../services/classifier/Automation.Worker.Visionbot.Executor/bin/x86/%buildConfigurationTypeNET%" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.VisionBotEngine/bin/x86/%buildConfigurationTypeNET%/Automation.Cognitive.VisionBotEngine.dll" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.Tesseract/%buildConfigurationTypeCPP%/Automation.Tesseract.dll" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.Tesseract4.0/%buildConfigurationTypeCPP%/Automation.Tesseract4.0.dll" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/%buildConfigurationTypeCPP%/Automation.Cognitive.CppLogger.dll" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/%buildConfigurationTypeCPP%/Automation.PatternRecognition.dll" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.VisionBotEngine/Tesseract4Settings.json" "%rootDir%/../../buildartifacts/workers/visionbotengine"
call echo D|xcopy /Y/V/F/q "%rootDir%/../installer/Resources/workers/visionbotengine" "%rootDir%/../../buildartifacts/workers/visionbotengine"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING CALSSIFIER WORKER ARTIFACTS----"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../services/classifier/Automation.DocumentClassifier/bin/x86/%buildConfigurationTypeNET%" "%rootDir%/../../buildartifacts/workers/classifier"
call echo D|xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.DocumentClassifier.Executor/bin/x86/%buildConfigurationTypeNET%" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.Tesseract/%buildConfigurationTypeCPP%/Automation.Tesseract.dll" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.Tesseract4.0/%buildConfigurationTypeCPP%/Automation.Tesseract4.0.dll" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/%buildConfigurationTypeCPP%/Automation.Cognitive.CppLogger.dll" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.ClassifierDBManagerCpp/%buildConfigurationTypeCPP%/Automation.ClassifierDBManagerCpp.dll" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.LayoutClassifier/%buildConfigurationTypeCPP%/Automation.LayoutClassifier.exe" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/%buildConfigurationTypeCPP%/Automation.PatternRecognition.dll" "%rootDir%/../../buildartifacts/workers/classifier"
call xcopy /Y/V/F/q "%rootDir%/../services/classifier/Automation.VisionBotEngine/Tesseract4Settings.json" "%rootDir%/../../buildartifacts/workers/classifier"
call echo D|xcopy /Y/V/F/q "%rootDir%/../installer/Resources/workers/classifier" "%rootDir%/../../buildartifacts/workers/classifier"


call echo A|%ZIP% x "%rootDir%/../installer/Resources/workers/tessdata.zip" -o"%rootDir%/../../buildartifacts/workers/OCRTrainingData"
call echo A|%ZIP% x "%rootDir%/../installer/Resources/workers/tessdata_4.zip" -o"%rootDir%/../../buildartifacts/workers/OCRTrainingData"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING CLIENT COMPONENTS----"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../services/classifier/Automation.Designer/bin/x86/%buildConfigurationTypeNET%" "%rootDir%/../../buildartifacts/Client Components/designer"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../installer/Resources/client/designer" "%rootDir%/../../buildartifacts/Client Components/designer"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../services/vision/validator/ui/Automation.ValidatorLite/bin/x86/%buildConfigurationTypeNET%" "%rootDir%/../../buildartifacts/Client Components/validator"
call echo D|xcopy /Y/V/F/s/q "%rootDir%/../installer/Resources/client/validator" "%rootDir%/../../buildartifacts/Client Components/validator"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
cd %rootDir%
exit /b 0

:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%

