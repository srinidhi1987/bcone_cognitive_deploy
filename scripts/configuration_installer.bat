@echo off
SET rootDir=%~dp0
SET msbuild="C:/Program Files (x86)/MSBuild/14.0/Bin/MSBuild"
echo "Root Dir:" %rootDir%

ECHO "BUILDING Automation.iqbot.logger"
call %msbuild% "%rootDir%/../installer/Utility/Custom/Automation.Cognitive.Installer.Custom/Automation.Cognitive.Installer.Custom.csproj" /t:rebuild /p:Configuration=Release;Platform=x86;RunCodeAnalysis=false;CodeAnalysisResultsAsErrors=false
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

call echo F|xcopy /Y/V/F/q "%rootDir%/../installer/Utility/Custom/Automation.Cognitive.Installer.Custom/bin/x86/Release/Automation.Cognitive.Installer.Custom.dll" "%rootDir%/../installer/Resources/setup-support-files/controlroom/Automation.Cognitive.Installer.Custom.dll"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING PRODUCT AND LICENSE FILES----"
call xcopy "%rootDir%/../installer\Resources\*.*" "%rootDir%/../../buildartifacts/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING JRE----"
call xcopy "%rootDir%/../installer\Resources\JRE\*.*" "%rootDir%/../../buildartifacts/JRE/" /Y/V/F/s/q
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING JDBC Driver----"
call xcopy "%rootDir%/../installer\Resources\JDBC\*.*" "%rootDir%/../../buildartifacts/JDBC/" /Y/V/F/s/q
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING CONFIGURATION FILES----"
call xcopy "%rootDir%/../installer\Resources\Configurations\*.*" "%rootDir%/../../buildartifacts/configurations/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING CLIENT COMPONENTS PRODUCT FILES----"
call xcopy "%rootDir%/../installer\Resources\client\*.*" "%rootDir%/../../buildartifacts/Client Components/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING SetupSupportFiles FILES----"

Set setupsupportsource="%rootDir%/../installer\Resources\setup-support-files"
Set setupsupportdestination="%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/"

pushd %setupsupportsource%
  for /r %%a in (*.*) do (
    echo D | call xcopy "%%a" %setupsupportdestination% /Y/V/F/I
  )
popd

call xcopy "%rootDir%/../services\alias\db\alias_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\alias\db\InsertMasterAliasData1.sql" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\alias\db\InsertLanguageMasterAlias.sql" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\vision\designer\db\visionbot_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\vision\designer\db\InsertMasterFileData.sql" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\vision\validator\db\validator_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\vision\validator\db\InsertMasterValidatorData.sql" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\vision\validator\db\VisionBotDocuments2AdditionalColumn.sql" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\classifier\db\classifier_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\project\db\project_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\filemanager\db\filemanager_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services\ML\db\ml_changelog.xml" "%rootDir%/../installer\Setup\InstallShield2015/setup-support-files/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

call echo F|xcopy /Y/V/F/q "%rootDir%/../installer\Resources\ProductReleaseInfo.xml" "%rootDir%/../../buildartifacts/ProductReleaseInfo.xml"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----Updating Product Release Information----"

set buildArtPath=%1
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "datestamp=%YYYY%%MM%%DD%"
echo datetime: %datestamp%

set buildArtPath=%rootDir%/../../buildartifacts
set platformProdReleaseInfoFile=%buildArtPath%\ProductReleaseInfo.xml
echo PlatformProductReleaseInfoFilePath: %platformProdReleaseInfoFile%

set "search=XXXXXXXXXX"
set "replace=%datestamp%%"

for /f "delims=" %%i in ('type "%platformProdReleaseInfoFile%" ^& break ^> "%platformProdReleaseInfoFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%platformProdReleaseInfoFile%" echo(!line:%search%=%replace%!
    endlocal
)

set clientProdReleaseInfoFile=%buildArtPath%\Client Components\ProductReleaseInfo.xml
echo ClientProductReleaseInfoFilePath: %clientProdReleaseInfoFile%

for /f "delims=" %%i in ('type "%clientProdReleaseInfoFile%" ^& break ^> "%clientProdReleaseInfoFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%clientProdReleaseInfoFile%" echo(!line:%search%=%replace%!
    endlocal
)

call xcopy  "%rootDir%/../../buildartifacts/*.*" "%rootDir%/../installer/Setup/Resources/" /Y/V/F/I /s
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

call xcopy  "%rootDir%/../../buildartifacts/Client Components/*.*" "%rootDir%/../installer/ClientSetup/Resources/" /Y/V/F/I /s
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

cd %rootDir%
exit /b 0

:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%