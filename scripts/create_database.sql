USE master;  
GO  
DECLARE @databases TABLE (dbname VARCHAR(50) PRIMARY KEY NOT NULL)
DECLARE @createdatabase nvarchar(200) 
DECLARE @dropdatabase nvarchar(200)
DECLARE @value VARCHAR(50)
INSERT @databases(dbname) VALUES ("AliasData"),("ClassifierData"),("Configurations"),("FileManager"),("MLData")
DECLARE db_cursor CURSOR FOR SELECT dbname FROM @databases
OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @value
WHILE @@FETCH_STATUS = 0   
BEGIN   
  IF EXISTS (SELECT 1 FROM sys.databases WHERE name=@value )
  BEGIN
	 SELECT @dropdatabase = 'DROP DATABASE ' + @value 
	 PRINT @dropdatabase
	 EXEC sp_executesql @dropdatabase
  END 	
  SELECT @createdatabase = 'CREATE DATABASE ' +  @value 
  PRINT @createdatabase
  EXEC sp_executesql @createdatabase
  FETCH NEXT FROM db_cursor INTO @value   
END   
CLOSE db_cursor   
DEALLOCATE db_cursor
GO