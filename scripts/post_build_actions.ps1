echo "-------Running Post Build actions script------"
$ErrorActionPreference = "Stop"
$authtoken=$args[0]
$reposlug=$args[1]
$pullrequestid=$args[2]
$headers = @{}
$headers.Add("Authorization", "Basic " +  $authtoken)
$mergeurl="https://bitbucket.org/api/2.0/repositories/" + $reposlug + "/pullrequests/" + $pullrequestid + "/merge"
echo $mergeurl
Invoke-RestMethod $mergeurl -Method Post -Headers $headers
exit $LASTEXITCODE
