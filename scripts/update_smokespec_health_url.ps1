echo "------------Updating the heartbeat and healthcheck URL in Smoke Spec -----------"
$executingDir = $(get-location).Path
echo $executingDir
$dest = (new-object system.io.directoryinfo $executingDir).parent.fullname
$dest = $dest+"\testautomation\specs\SmokeSpec_Staging.spec"
$healthurltoken="http://localhost:8100/health"
$testintegrationserverhealthurl="http://"+ $args[0] + ":8100/health"
(Get-Content $dest).replace($healthurltoken, $testintegrationserverhealthurl) | Set-Content $dest
$filemgrhealthurltoken="http://localhost:9996/heartbeat"
$testintegrationfilemgrhealthurl="http://" + $args[0] + ":9996/heartbeat"
(Get-Content $dest).replace($filemgrhealthurltoken, $testintegrationfilemgrhealthurl) | Set-Content $dest
$aliashealthurltoken="http://localhost:9997/heartbeat"
$testintegrationaliashealthurl="http://" + $args[0] + ":9997/heartbeat"
(Get-Content $dest).replace($aliashealthurltoken, $testintegrationaliashealthurl) | Set-Content $dest
$vbotmgrhealthurltoken="http://localhost:9998/heartbeat"
$testintegrationvbotmgrhealthurl="http://" + $args[0] + ":9998/heartbeat"
(Get-Content $dest).replace($vbotmgrhealthurltoken, $testintegrationvbotmgrhealthurl) | Set-Content $dest
$projecthealthurltoken="http://localhost:9999/heartbeat"
$testintegrationprojecthealthurl="http://" + $args[0] + ":9999/heartbeat"
(Get-Content $dest).replace($projecthealthurltoken, $testintegrationprojecthealthurl) | Set-Content $dest


