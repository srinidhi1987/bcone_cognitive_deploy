echo "-----COPYING BUILD ARTIFACTS TO REMOTE HOST-------"
SET rootDir=%~dp0
echo %rootDir%
set pathtobuildartifacts=%rootDir%/../../buildartifacts
echo %pathtobuildartifacts%
set servername=%1%
echo ServerName %servername%
set serverusername=%2%
echo Username %serverusername%
set serverpassword=%3%
set installdir=%4%
echo installdir  %installdir%
set pathtopscputility="C:\Program Files\PSCP"
cd %pathtopscputility%
pscp -r -pw %serverpassword% %pathtobuildartifacts% %serverusername%@%servername%:%installdir%