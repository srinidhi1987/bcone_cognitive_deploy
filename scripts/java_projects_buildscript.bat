:: navingating out of scripts folder, moving back to repo root. 
SET rootDir=%~dp0
echo "Root Dir:" %rootDir%

::echo "---BUIDLING COMMON PROJECT---"
::cd %rootDir%/../services/common
::call gradlew.bat clean build --rerun-tasks
::if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING FILE MANAGER SERVICE---"
cd %rootDir%/../services/filemanager/service
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/filemanager/service/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/*.jar" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/filemanager/service/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_filemanager.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING ALIAS SERVICE---"
cd %rootDir%/../services/alias/service
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/alias/service/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/alias/service/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_alias.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING PROJECT SERVICE---"
cd %rootDir%/../services/project/service
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/project/service/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/project/service/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_project.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING VALIDATOR SERVICE---"
cd %rootDir%/../services/vision/validator/service
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/vision/validator/service/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/vision/validator/service/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_validator.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING DESIGNER SERVICE---"
cd %rootDir%/../services/vision/designer/service
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/vision/designer/service/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/vision/designer/service/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_visionbot.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING AUTHENTICATION SERVICE---"
cd %rootDir%/../services/authentication
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/authentication/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/authentication/action_roles.json" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/authentication/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_authentication.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING GATEWAY SERVICE---"
cd %rootDir%/../services/gateway
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/gateway/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/gateway/*.json" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/gateway/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_gateway.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING GATEWAY-2 SERVICE---"
cd %rootDir%/../services/gateway-2
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/gateway-2/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/gateway-2/application.yaml" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/gateway-2/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_gateway-2.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING APPLICATION SERVICE---"
cd %rootDir%/../services/application
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/application/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/application/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_application.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "---BUILDING REPORT SERVICE---"
cd %rootDir%/../services/reports/service
call gradlew.bat clean build
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call xcopy "%rootDir%/../services/reports/service/build/libs/*.jar" "%rootDir%/../../buildartifacts/services/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy "%rootDir%/../services/reports/service/src/main/resources/log4j2.xml" "%rootDir%/../../buildartifacts/services/log4j2_reports.xml" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
cd %rootDir%
exit /b 0

:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%
