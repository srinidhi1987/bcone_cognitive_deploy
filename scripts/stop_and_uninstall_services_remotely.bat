SET rootDir=%~dp0
set servername=%1%
set serverusername=%2%
set serverpassword=%3%
set pathtopscputility="C:\Program Files\PSCP\pscp.exe"
set pathtopstools="C:\IQBotCICD\PSTools\PsExec.exe"
set installdir=%4%
echo "---------Copying the uninstall servicesbatch files to remote server %servername%---------------"
%pathtopscputility% -r -pw %serverpassword% "%rootDir%../installer/Resources/Configurations/stopanduninstallallservices.bat" %serverusername%@%servername%:%installdir%
%pathtopscputility% -r -pw %serverpassword% "%rootDir%../installer/Resources/Configurations/microservices_stop.bat" %serverusername%@%servername%:%installdir%
%pathtopscputility% -r -pw %serverpassword% "%rootDir%../installer/Resources/Configurations/workers_stop.bat" %serverusername%@%servername%:%installdir%
%pathtopscputility% -r -pw %serverpassword% "%rootDir%../installer/Resources/Configurations/npmstop.bat" %serverusername%@%servername%:%installdir%
echo "-----Copying the remove older build artifacts batch files to remote server %servername%---------------"
%pathtopscputility% -r -pw %serverpassword% "%rootDir%remove_older_build_artifacts.bat" %serverusername%@%servername%:%installdir%
echo "---------Copying nssm.exe to remote server %servername%---------------"
%pathtopscputility% -r -pw %serverpassword% "%rootDir%../installer/Resources/Configurations/nssm.exe" %serverusername%@%servername%:%installdir%
echo "---------uninstalling services on remote server %servername%---------------"
%pathtopstools% "\\%servername%" -u %serverusername% -p %serverpassword%  "%installdir%/stopanduninstallallservices.bat"