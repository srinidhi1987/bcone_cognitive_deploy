##get the files modified in the commit hash
$branchname="origin/" + $args[0]
$hash=git rev-parse $branchname
$listoffilesinhash=git show --pretty="" --name-only $hash 
$countofcommonfileschanged=($listoffilesinhash | select-string -pattern "^services/common/").count
##if no files are changed then return false
$countofnewlines = $listoffilesinhash | Measure-Object -Line
if($countofcommonfileschanged -eq 0){
	$iscommon = "no";
}
elseif($countofcommonfileschanged -eq $countofnewlines.Lines){
	$iscommon = "yes";
}
else{
	$iscommon = "no";
}
$finalout="iscommon=" + $iscommon
return $finalout