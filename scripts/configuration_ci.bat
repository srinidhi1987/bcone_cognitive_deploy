@echo off
SET rootDir=%~dp0
set SQLServerAddress=%1
set SQLUserName=%2
set SQLPort=%3
set installDir=%4
echo "installDir: in config script: %installDir%"

echo "----COPYING JRE----"
call xcopy "%rootDir%/../installer\Resources\JRE\*.*" "%rootDir%/../../buildartifacts/JRE/" /Y/V/F/s/q
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING JDBC Driver----"
call xcopy "%rootDir%/../installer\Resources\JDBC\*.*" "%rootDir%/../../buildartifacts/JDBC/" /Y/V/F/s/q
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING CONFIGURATION FILES----"
call xcopy "%rootDir%/../installer\Resources\Configurations\*.*" "%rootDir%/../../buildartifacts/configurations/" /Y/V/F/I
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----COPYING OSS DISCLOSURE, NOTICE and PRODUCT RELEASE INFORMATION FILES----"
call echo F|xcopy /Y/V/F/q "%rootDir%/../installer\Resources\*.*" "%rootDir%/../../buildartifacts/*.*"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

echo "----UPDATING OCR ENGINE PATHS----"

for %%a in ("%rootDir%") do set "p_dir=%%~dpa"
echo %p_dir%

for %%a in (%p_dir:~0,-1%) do set "p2_dir=%%~dpa"
echo %p2_dir%

for %%a in (%p2_dir:~0,-1%) do set "home_dir=%%~dpa"
echo Installation path home directory: %home_dir%

echo "---RENAMING TEMPLATE FILE----"
DEL "%home_dir%buildartifacts\configurations\OCREngineSettings.json"
REN "%home_dir%buildartifacts\configurations\OCREngineSettings_template.json" "OCREngineSettings.json"

set buildArtPath=%home_dir%buildartifacts\configurations
echo Configuration filepath: %buildArtPath%
set tessrecPath=%installDir%buildartifacts\workers\classifier\tessdata\
echo Tessdata filepath: %tessrecPath%
set tessrec4Path=%installDir%buildartifacts\workers\classifier\tessdata_4\
echo Tessdata4 filepath: %tessrec4Path%

setlocal enableextensions disabledelayedexpansion

set textFile=%buildArtPath%\OCREngineSettings.json
echo OCREngineFile: %textFile%

echo "----UPDATING TESSDATA PATH----"
set "search=tessdata_path_template"
set "replace=%tessrecPath%"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

set "search=tessdata4_path_template"
set "replace=%tessrec4Path%"
echo "----UPDATING TESSDATA4 PATH----"
for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

set "search=\"
set "replace=\\"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

echo "---RENAMING CLASSIFIER CONFIGURATION TEMPLATE FILE----"
DEL "%home_dir%buildartifacts\workers\classifier\CognitiveServiceConfiguration.json"
REN "%home_dir%buildartifacts\workers\classifier\CognitiveServiceConfiguration_template.json" "CognitiveServiceConfiguration.json"

echo "----UPDATING DATABASE CONNECTION INFORMATION IN CLASSIFIER WORKER CONFIGURATION FILE----"

set textFile=%home_dir%buildartifacts\workers\classifier\CognitiveServiceConfiguration.json
set "search=SQLSERVERHOSTNAME_TEMPLATE"
set "replace=%SQLServerAddress%"

echo Classifier configuration filepath: %textFile%

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

set "search=SQLSERVERPORT_TEMPLATE"
set "replace=%SQLPort%"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

set "search=SQLSERVERUSERNAME_TEMPLATE"
set "replace=%SQLUserName%"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

echo "---RENAMING VISIONBOT CONFIGURATION TEMPLATE FILE----"
DEL "%home_dir%buildartifacts\workers\visionbotengine\CognitiveServiceConfiguration.json"
REN "%home_dir%buildartifacts\workers\visionbotengine\CognitiveServiceConfiguration_template.json" "CognitiveServiceConfiguration.json"

echo "----UPDATING DATABASE CONNECTION INFORMATION IN VISIONBOT WORKER CONFIGURATION FILE----"

set textFile=%home_dir%buildartifacts\workers\visionbotengine\CognitiveServiceConfiguration.json
set "search=SQLSERVERHOSTNAME_TEMPLATE"
set "replace=%SQLServerAddress%"

echo Visionbot configuration filepath: %textFile%

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

set "search=SQLSERVERPORT_TEMPLATE"
set "replace=%SQLPort%"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

set "search=SQLSERVERUSERNAME_TEMPLATE"
set "replace=%SQLUserName%"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)

cd %rootDir%
exit /b 0

:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%