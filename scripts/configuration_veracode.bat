SET rootDir=%~dp0
echo "Root Dir:" %rootDir%

echo "--- CREATING ZIP ---"
del /q "%rootDir%..\cognitive-codeanalysis.zip"
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\Automation.Designer\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\Automation.VisionBot.Services\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\Automation.VisionBotEngine\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\Automation.VisionBotEngine.Validation\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\Automation.Worker.Visionbot\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\Validator.Common\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\services\services\classifier\ValidatorServices\bin\x86\Debug\Automation.*.pdb"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts\workers\classifier\Automation.*"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts\workers\visionbotengine\Automation.*"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts/Client Components/designer/Automation.*"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts/Client Components/validator/Automation.*"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts\ML"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts\Portal\server"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts\Portal\www"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
"C:\Program Files\7-Zip\7z.exe" a -tzip "%rootDir%..\cognitive-codeanalysis.zip" "%rootDir%..\..\buildartifacts\services\*.jar"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

if %ERRORLEVEL% == 0 ( goto :COMPLETED )
:ErrorOccurred
echo "Errors encountered during installing services. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog "
exit /b %errorlevel%
:COMPLETED
exit /b 0