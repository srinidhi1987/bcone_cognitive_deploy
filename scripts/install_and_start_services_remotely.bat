SET rootDir=%~dp0
set servername=%1%
set serverusername=%2%
set serverpassword=%3%
set installdir=%4%
set pathtopstools="C:\IQBotCICD\PSTools"
echo "-------------------Installing services remotely on %servername%--------------------------"
cd %pathtopstools%
psexec "\\%servername%" -u %serverusername% -p %serverpassword% "%installdir%/buildartifacts/configurations/installandstartallservices.bat" -h
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
exit /b 0
:ErrorOccurred
exit /b %errorlevel%