echo "------------Updating output folder name of test automation-----------"
$executingDir = $(get-location).Path
echo $executingDir
$dest = (new-object system.io.directoryinfo $executingDir).parent.fullname
$dest = $dest+"\testautomation\src\test\resources\IQBot_TestData\Output_Folder_Name.txt"
$outputpathtoken="Output_Folder=C:\Output"
$testintegrationserveroutputpath="Output_Folder="+ $args[0]
(Get-Content $dest).replace($outputpathtoken, $testintegrationserveroutputpath) | Set-Content $dest