::Run test automation 
cd "%rootDir%../testautomation"
call mvn clean install
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call mvn gauge:execute -DspecsDir=specs\SmokeSpec_Staging.spec
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
exit /b 0
:ErrorOccurred
echo "Errors encountered during testautomation. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%