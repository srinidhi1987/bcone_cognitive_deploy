SET rootDir=%~dp0
SET pyinstaller="C:/ProgramData/Miniconda2/Scripts/pyinstaller.exe"
echo "----BUILDING MACHINELEARNING MODULE----"
call %pyinstaller% "%rootDir%/../services/ml/TranslationModel/directl-p/webservice/translationsvc.py" --clean --hidden-import=webservice -y --distpath "%rootDir%/../../buildartifacts/ML/"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call %pyinstaller% "%rootDir%/../services/ml/TranslationModel/directl-p/webservice/webservice.py" --clean --hidden-import=webservice -y --distpath "%rootDir%/../../buildartifacts/ML/"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call echo F|xcopy /Y/V/F/q "%rootDir%/../services/ML/TranslationModel/directl-p/webservice/directlp.exe" "%rootDir%/../../buildartifacts/ML/webservice/"
call echo F|xcopy /Y/V/F/q "%rootDir%/../services/ML/TranslationModel/directl-p/webservice/m2m-aligner.exe" "%rootDir%/../../buildartifacts/ML/webservice/"
call echo F|xcopy /Y/V/F/q "%rootDir%/../installer/Resources/ml/translationsvc/CognitiveServiceConfiguration.json" "%rootDir%/../../buildartifacts/ML/translationsvc/"
call echo F|xcopy /Y/V/F/q "%rootDir%/../installer/Resources/ml/webservice/CognitiveServiceConfiguration.json" "%rootDir%/../../buildartifacts/ML/webservice/"
echo "MACHINE LEARNING ARTIFACTS COPIED TO " %rootDir%/../../buildartifacts/ML/
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
cd %rootDir%
exit /b 0

:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%