@setlocal enableextensions
@cd /d "%~dp0"
@echo off
SET rootDir=%~dp0
echo "Root Dir:" %rootDir%
set SQLServerAddress=%1
set UserName=%2
set Password=%3
set SQLPort=%4
set RabbitMQPassword=%5
set ControlRoomHost=%6
set ControlRoomPort=%7
set OutputDir=%8
set GatewayHostName=%9
shift
shift
set installDir=%8
set dbpasswordcleartext=%9
set AliasChangeLog=""

if NOT DEFINED JAVA_HOME goto error
set RUN_JAVA=%JAVA_HOME%\bin\java
set LIQUIBASE_JARFILEPATH= %rootDir%\..\installer\Resources\setup-support-files\liquibase\liquibase.jar
set SQLJDBC_JARFILEPATH="%rootDir%/../installer/Resources/setup-support-files/liquibase/sqljdbc4-2.0.jar"
set sqlcmd="C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE"
set mypath=%cd%
pushd %mypath%

@rem set RUN_JAVA=C:\Program Files (x86)\Java\jre1.8.0_91\bin\java
echo "----Applying database scripts----"
@Rem sqlcmd -U %UserName% -P %Password% -S %SQLServerAddress% -i "DatabaseCreate.sql"
%sqlcmd% -U %UserName% -P %dbpasswordcleartext% -S %SQLServerAddress% -i "create_database.sql"
:: Copy liquibase change log xmls that have dependencies alias, vision bot designer , validator
:: Copy liquibase and dependent files for alias database
copy  "%rootDir%\..\services\alias\db\alias_changelog.xml" .
copy  "%rootDir%\..\services\alias\db\InsertMasterAliasData1.sql" .
copy  "%rootDir%\..\services\alias\db\InsertLanguageMasterAlias.sql" .
:: Copy liquibase and dependent files for vision bot designer
copy "%rootDir%\..\services\vision\designer\db\visionbot_changelog.xml" .
copy "%rootDir%\..\services\vision\designer\db\InsertMasterFileData.sql" .
:: Copy liquibase and dependent files for vision bot validator
copy "%rootDir%\..\services\vision\validator\db\validator_changelog.xml" .
copy "%rootDir%\..\services\vision\validator\db\InsertMasterValidatorData.sql" .
copy "%rootDir%\..\services\vision\validator\db\VisionBotDocuments2AdditionalColumn.sql" .
echo "----Applying Liquibase scripts----"
::DONE
"%RUN_JAVA%" -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="%rootDir%\..\installer\Resources\setup-support-files\liquibase\configuration_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=Configurations --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="%rootDir%\..\services\classifier\db\classifier_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=ClassifierData --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="%rootDir%\..\services\project\db\project_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="%rootDir%\..\services\filemanager\db\filemanager_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="visionbot_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="validator_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="alias_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=AliasData --username=%UserName% --password=%dbpasswordcleartext% update
java -jar %LIQUIBASE_JARFILEPATH% --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="..\installer\Resources\setup-support-files\liquibase\sqljdbc4-2.0.jar" --changeLogFile="%rootDir%\..\services\ML\db\ml_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=MLData --username=%UserName% --password=%dbpasswordcleartext%  update
:: Delete the files that are no longer needed
DEL /f InsertMasterAliasData1.sql
DEL /f InsertLanguageMasterAlias.sql
DEL /f alias_changelog.xml
DEL /f InsertMasterFileData.sql
DEL /f visionbot_changelog.xml
DEL /f InsertMasterValidatorData.sql
DEL /f VisionBotDocuments2AdditionalColumn.sql
DEL /f validator_changelog.xml
::DONE
:: Write to a file database password and rabbit mq password
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
@echo off
echo "Updating Cognitive Properties"
( echo database.password = %Password%) > "%rootDir%\..\..\buildartifacts\configurations\Cognitive.properties"
( echo rabbitmq.password = %RabbitMQPassword%) >> "%rootDir%\..\..\buildartifacts\configurations\Cognitive.properties"
:: Write to settings.txt
echo "Updating Settings"
( echo SQLServerAddress=%SQLServerAddress%) > "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo UserName=%UserName%) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo SQLPort=1433) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo ControlRoomLink=%ControlRoomHost%) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo ControlRoomPort=%ControlRoomPort%) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo OutputPath=%OutputDir%) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo GatewayHost=%GatewayHostName%) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo OCREngine=Tesseract4) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo InstallDIR=%installDir%buildartifacts\) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo SecureConnectionToCR=false) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo SecureConnectionToLB=false) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo GatewayPort=8100) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo LoadBalancerHost=%GatewayHostName%) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo LoadBalancerPort=3000) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo secureConnectionToPortal=false) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo ConfidenceThreshold=0) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo SQLWindowsAuthentication=false) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo InstallationType=CUSTOM) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo CopyProductionFiles=true) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo VisionBotLockedTimeout=15) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo DocumentLockedTimeout=900) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"
( echo RabbitMQPort=5673) >> "%rootDir%\..\..\buildartifacts\configurations\Settings.txt"

if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
@rem liquibase.bat 172.16.16.35 sa Automation@123 1433
exit /b 0
:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog and checkinlog file.."
exit /b %errorlevel%