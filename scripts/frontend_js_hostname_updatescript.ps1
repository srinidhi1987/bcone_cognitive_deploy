echo "------------Updating Hostname for Frontend components-----------"

$executingDir = $(get-location).Path
$PortalFolderPath = Join-Path (Get-Item $executingDir).Parent.Parent.FullName "buildartifacts\Portal\www\js\"
$GatewayHostName = $args[0] + ":3000"

$files = ls -Path $PortalFolderPath Main*.js -recurse
foreach ($file in $files){
    echo $file.FullName
    (Get-Content $file.FullName) | ForEach-Object {$_ -replace "localhost:3000", $GatewayHostName } | Set-Content $file.FullName
    echo "Hostname updated to file: " + $file.FullName
}

echo $error[0].errordetails.message
exit $LASTEXITCODE