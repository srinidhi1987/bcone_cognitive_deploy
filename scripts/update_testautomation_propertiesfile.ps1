echo "------------Updating Test Automation user.properties file-----------"
$executingDir = $(get-location).Path
echo $executingDir
$dest = (new-object system.io.directoryinfo $executingDir).parent.fullname
$dest = $dest+"\testautomation\env\default\user.properties"
$appurltoken="http://ta1.unicornmangorage.com:3000"
$testintegrationserverurl="http://" + $args[0]+ ":3000"
(Get-Content $dest).replace($appurltoken, $testintegrationserverurl) | Set-Content $dest
$dbhosttoken="hostName = localhost"
$testintegrationserverdbhostname="hostName = " + $args[0]
(Get-Content $dest).replace($dbhosttoken, $testintegrationserverdbhostname) | Set-Content $dest
$dbusername="dbUserName = saadmin"
$testintegrationserverdbuser="dbUserName = " + $args[1]
(Get-Content $dest).replace($dbusername, $testintegrationserverdbuser) | Set-Content $dest
$dbpasswd="dbpassword = Automation@123"
$testintegrationserverdbpassword="dbpassword = " + $args[2]
(Get-Content $dest).replace($dbpasswd, $testintegrationserverdbpassword) | Set-Content $dest
$outputpath="output_Path=C:\\Output"
$testintegrationserveroutputpath="output_Path=" + $args[3]
(Get-Content $dest).replace($outputpath, $testintegrationserveroutputpath) | Set-Content $dest