::Publishing common jar
set nexus_nuget_publisher_password=%1
set nexus_publisher_password=%2
set rootdir=%~dp0
cd %rootDir%/../services/common
.\gradlew.bat -P nexusUrl=https://nexus.automationanywhere.net -P nexusUsername=aa-nexus-nuget-publisher -P nexusPassword=%nexus_nuget_publisher_password% -P nexusPublisherUsername=aa-nexus-publisher -P nexusPublisherPassword=%nexus_publisher_password% publish
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
:ErrorOccurred
echo "Errors encountered during installing services. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog "
exit /b %errorlevel%
:COMPLETED
exit /b 0