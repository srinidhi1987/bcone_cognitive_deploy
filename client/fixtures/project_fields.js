/**
 * Created by iamcutler on 11/29/16.
 */

/* eslint-disable */

export const ProjectFields1 = [
    {
        filetype_id: 1001,
        description: '',
        language: 'en-us',
        display_name: 'Invoice Number',
        filetypefields: [
            {
                filetypefield_id: '',
                description: '',
                display_name: '',
                aliases: [
                    {
                        filetypefieldalias_id: '',
                        description: '',
                        value: '',
                    },
                ],
                fieldtype: [
                    {
                        fieldtype_id: '',
                        description: '',
                        value: 'FormField',
                    },
                ],
            },
        ],
    }, {
        filetype_id: 1002,
        description: '',
        language: 'en-us',
        display_name: 'Quantity',
        filetypefields: [
            {
                filetypefield_id: '',
                description: '',
                display_name: '',
                aliases: [
                    {
                        filetypefieldalias_id: '',
                        description: '',
                        value: '',
                    },
                ],
                fieldtype: [
                    {
                        fieldtype_id: '',
                        description: '',
                        value: 'TableField',
                    },
                ],
            },
        ],
    }, {
        filetype_id: 1003,
        description: '',
        language: 'en-us',
        display_name: 'Name',
        filetypefields: [
            {
                filetypefield_id: '',
                description: '',
                display_name: '',
                aliases: [
                    {
                        filetypefieldalias_id: '',
                        description: '',
                        value: '',
                    },
                ],
                fieldtype: [
                    {
                        fieldtype_id: '',
                        description: '',
                        value: 'TableField',
                    }, {
                        fieldtype_id: '',
                        description: '',
                        value: 'FormField',
                    }
                ],
            },
        ],
    }
];

export const ProjectFields2 = [{
    id: '1',
    name: 'Invoice',
    description: null,
    language: 'English',
    fields: [
        {
            id: '1',
            name: 'Invoice Number',
            description: null,
            fieldType: 'FormField',
            aliases: [
                'Invoice',
                'Invoice No.',
                'Invoice Number',
            ],
            isDefaultSelected: true,
        },
        {
            id: '2',
            name: 'Invoice Date',
            description: null,
            fieldType: 'FormField',
            aliases: [
                'Invoice Date',
                'Invoice Dt',
            ],
            isDefaultSelected: true,
        },
        {
            id: '3',
            name: 'Item Description',
            description: null,
            fieldType: 'TableField',
            aliases: [
                'Description',
                'Desc',
            ],
            isDefaultSelected: true,
        },
        {
            id: '4',
            name: 'Line Quantiry',
            description: null,
            fieldType: 'FormField',
            aliases: [
                'Qty',
                'Quantity',
                'Count',
            ],
            isDefaultSelected: true,
        },
        {
            id: '5',
            name: 'Line Total',
            description: null,
            fieldType: 'FormField',
            aliases: [
                'Value',
                'Total',
                'Amount',
            ],
            isDefaultSelected: true,
        },
        {
            id: '6',
            name: 'Invoice Total',
            description: null,
            fieldType: 'FormField',
            aliases: [
                'Invoice Total',
            ],
            isDefaultSelected: true,
        },
    ],
}];

export const domainLanguages = [{
    "name": "Invoice",
    "languages": [
        "English",
        "Italian",
        "French",
        "German",
        "Spanish"
    ]
},
{
    "name": "my test invoice",
    "languages": [
        "English",
        "French"
    ]
}];