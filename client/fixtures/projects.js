/**
 * Created by iamcutler on 11/29/16.
 */

/* eslint-disable */

export const Project1 = {
    "id": "38a5c1bc-2d45-4d1e-99a0-f456faca26bf",
    "name": "Isabel Reynolds",
    "description": "Qui voluptas et molestias.",
    "organizationId": 1,
    "projectType": "invoice",
    "fileType": "invoice",
    "numberOfFiles": 19394,
    "numberOfCategories": 89834,
    "unprocessedFileCount": 99050,
    "primaryLanguage": "english",
    "accuracyPercentage": 52,
    "visionBotCount": 4,
    "currentTrainedPercentage": 30,
    "categories": [],
    "fields": {
        "standard": [1, 2, 4],
        "custom": [
            {"name": "Invoice Date", "type": "FormField"}
        ],
    },
    "projectState": "training",
    "environment": "staging",
    "updatedAt": "2016-11-29T10:25:54.293Z",
    "createdAt": "2016-08-24T18:49:57.063Z",
    "versionId":111,
};

export const Project2 = {
    "id": "ca9c9d75-c690-4222-b1ae-225afd194af1",
    "name": "Testing priority selector",
    "description": null,
    "organizationId": "1",
    "projectType": "invoice",
    "numberOfFiles": 1,
    "numberOfCategories": 1,
    "unprocessedFileCount": 0,
    "primaryLanguage": "English",
    "accuracyPercentage": 0,
    "visionBotCount": 0,
    "currentTrainedPercentage": 0,
    "fields": {
        "standard": ["1", "2", "6", "3", "4", "5"],
        "custom": [{
            "type": "FormField",
            "name": "selector",
            "id": "7a354946-522d-43f1-afb4-7f99feca9aa7"
        }, {"type": "TableField", "name": "selector 2", "id": "fdf731a5-4c1a-4ef2-9e2f-e3bf93b99fd6"}]
    },
    "projectState": "training",
    "environment": "staging",
    "updatedAt": "2017-03-10T01:47:44.446+0000",
    "createdAt": "2017-03-10T01:47:44.446+0000",
    "categories": [],
    "versionId":111,
};

export const Project3 = {
    "id": "38a5c1bc-2d45-4d1e-99a0-f456faca26bf",
    "name": "Isabel Reynolds",
    "description": "Qui voluptas et molestias.",
    "organizationId": 1,
    "projectType": "invoice",
    "fileType": "invoice",
    "numberOfFiles": 19394,
    "numberOfCategories": 89834,
    "unprocessedFileCount": 99050,
    "primaryLanguage": "1",
    "accuracyPercentage": 52,
    "visionBotCount": 4,
    "currentTrainedPercentage": 30,
    "categories": [],
    "fields": {
        "standard": [1, 2, 4],
        "custom": [
            {"name": "Invoice Date", "type": "FormField"}
        ],
    },
    "projectState": "training",
    "environment": "staging",
    "updatedAt": "2016-11-29T10:25:54.293Z",
    "createdAt": "2016-08-24T18:49:57.063Z",
    "versionId":111,
};

export const currentProjectData = {
    ccuracyPercentage:0,
    categories: null,
    confidenceThreshold:0,
    createdAt: 1520008873254,
    currentTrainedPercentage:0,
    description:'desc',
    environment:'staging',
    fields:{standard: ['1', '3', '6', '11', '13'],
        custom: [{
            id:'f672e9b3-8d47-426f-b1c6-1dd5ecf35967',
            name:'custom one',
            type:'FormField',
        }],
    },
    id:'5d06b78f-5a86-4294-8dbf-f57c06becaa1',
    name:'2373_test',
    numberOfCategories:2,
    numberOfFiles:3,
    ocrEngineDetails:[{
        engineType:'Tesseract4',
    }],
    organizationId:'1',
    primaryLanguage:'1',
    projectState:'training',
    projectType:'Invoices',
    projectTypeId:'1',
    unprocessedFileCount:3,
    updatedAt:1520008873254,
    visionBotCount:0,
    versionId: 111,
};

export const project1MetaData = {
    "id":"eaa2c03a-818d-4534-a762-bd3fdff0c306",
    "name":"M5",
    "description":null,
    "organizationId":"1",
    "projectType":"Invoices",
    "projectTypeId":"1",
    "confidenceThreshold":0,
    "primaryLanguage":"1",
    "fields":{"standard":["1","3","6","10","11","13","8","9","12","37","36","35","34"],"custom":[]},
    "projectState":"training",
    "environment":"production",
    "ocrEngineDetails":[{"id":"1","engineType":"Tesseract4"}]
}