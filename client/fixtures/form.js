import {currentProjectData} from './projects';

export const initialValuesForEditProject = {
    description: currentProjectData.description,
    files: [],
    projectType: currentProjectData.projectType,
    projectTypeId: currentProjectData.projectTypeId,
    fields: {standard:[], custom:[]},
};

export const operations = {
    description: 'Testing Description',
    fields: {
        standard:['1', '3', '5', '8', '2'],
        custom: [
            {
                id:'f672e9b3-8d47-426f-b1c6-1dd5ecf35967',
                name:'custom one',
                type:'FormField',
            },
            {
                id:'f672e9b3-8d47-426f-b1c6-1dd5ecf35967',
                name:'Custom Two',
                type:'TableField',
            },
        ],
    },
};

/**
 * this object is formed based on above operations object.
*/
export const expectedPatchBody = [
    {
        op:'replace',
        path:'/description',
        value: 'Testing Description',
    },
    {
        op:'add',
        path:'/fields/standard',
        value: ['1', '3', '5', '8', '2'],
    },
    {
        op:'add',
        path:'/fields/custom',
        value: [
            {
                id:'f672e9b3-8d47-426f-b1c6-1dd5ecf35967',
                name:'custom one',
                type:'FormField',
            },
            {
                id:'f672e9b3-8d47-426f-b1c6-1dd5ecf35967',
                name:'Custom Two',
                type:'TableField',
            },
        ],
    },
];
