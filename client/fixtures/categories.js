/**
 * Created by brantbarger on 11/29/16.
 */

/* eslint-disable */

export const Category1 = {
    "id": 97896,
    "name": "Merritt Volkman",
    "visionBot": [{
        "id": "175c608a-2f16-4803-95af-ef5999b6dfe3",
        "name": "Lawson Bartell",
        "currentState": "design_mode"
    }],
    "numberOfFiles": 77840,
    "unprocessedFileCount": 36486,
    "files": [{
        "id": "b388373c-bcf8-469f-84a7-0f9dcf1c370f",
        "name": "Mr. Porter Flatley",
        "location": "http://lorempixel.com/640/480/people",
        "size": 64226,
        "format": "jpg",
        "processed": true
    }, {
        "id": "b388373c-bcf8-469f-84a7-436546565",
        "name": "Mr. Porter Flatley 24",
        "location": "http://lorempixel.com/640/481/people",
        "size": 64226,
        "format": "jpg",
        "processed": true
    }]
};

export const Category2 = {
    "id": "-1",
    "name": "Folder_-1",
    "categoryDetail": {
        "id": "-1",
        "noOfDocuments": 1,
        "priority": 0,
        "index": 0,
        "allFieldDetail": [
            {"fieldId": "1", "foundInTotalDocs": 0},
            {"fieldId": "2", "foundInTotalDocs": 0},
            {"fieldId": "3", "foundInTotalDocs": 0},
            {"fieldId": "4", "foundInTotalDocs": 0},
            {"fieldId": "5", "foundInTotalDocs": 0},
            {"fieldId": "6", "foundInTotalDocs": 0},
            {"fieldId": "7a354946-522d-43f1-afb4-7f99feca9aa7", "foundInTotalDocs": 0},
            {"fieldId": "fdf731a5-4c1a-4ef2-9e2f-e3bf93b99fd6", "foundInTotalDocs": 0},
        ]
    },
    "visionBot": null,
    "fileCount": 1,
    "files": [{
        "id": "4ec26605-6fee-48c9-b4f3-aa7a624bc03b",
        "name": "4466041.jpg",
        "location": "",
        "format": "jpg",
        "processed": false
    }]
};

export const Category3 = {
    'id': '629',
    'name': 'Group_629',
    'categoryDetail': {
        'id': '629',
        'noOfDocuments': 7,
        'priority': 15,
        'index': 1.4583332538604736,
        'allFieldDetail': [
            {
                'fieldId': '1',
                'foundInTotalDocs': 7,
            },
            {
                'fieldId': '10',
                'foundInTotalDocs': 7,
            },
            {
                'fieldId': '11',
                'foundInTotalDocs': 7,
            },
            {
                'fieldId': '13',
                'foundInTotalDocs': 0,
            },
            {
                'fieldId': '3',
                'foundInTotalDocs': 7,
            },
            {
                'fieldId': '6',
                'foundInTotalDocs': 7,
            },
        ],
    },
    'visionBot': null,
    'fileCount': 7,
    'files': null,
    'productionFileDetails': {
        'totalCount': 0,
        'unprocessedCount': 0,
        'totalSTPCount': 0,
    },
    'stagingFileDetails': {
        'totalCount': 7,
        'unprocessedCount': 7,
        'totalSTPCount': 0,
    },
};

export const Category4 = {
    'id': '492',
    'name': 'Group_492',
    'categoryDetail': {
        'id': '492',
        'noOfDocuments': 1,
        'priority': 2,
        'index': 0.1666666716337204,
        'allFieldDetail': [
            {
                'fieldId': '1',
                'foundInTotalDocs': 1,
            },
            {
                'fieldId': '10',
                'foundInTotalDocs': 1,
            },
            {
                'fieldId': '11',
                'foundInTotalDocs': 1,
            },
            {
                'fieldId': '13',
                'foundInTotalDocs': 0,
            },
            {
                'fieldId': '3',
                'foundInTotalDocs': 0,
            },
            {
                'fieldId': '6',
                'foundInTotalDocs': 1,
            },
        ],
    },
    'visionBot': null,
    'fileCount': 1,
    'files': null,
    'productionFileDetails': {
        'totalCount': 0,
        'unprocessedCount': 0,
    },
    'stagingFileDetails': {
        'totalCount': 1,
        'unprocessedCount': 1,
    },
};
