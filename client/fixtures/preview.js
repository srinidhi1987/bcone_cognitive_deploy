/* eslint-disable */


export const previewDocument1 = {
  format: "tif",
  processed: false,
  fileId: "98b9124c-128f-478d-897c-75092d357633",
  projectId: "e869162d-1298-4a6f-96ea-cbf2f474819f",
  fileName: "Group1_SinglePage.tif",
  fileLocation: "",
  fileSize: 30359,
  fileHeight: 2200,
  fileWidth: 1700,
  classificationId: "2",
  uploadrequestId: "cjgdynlro00003rpvqwaozkx0",
  layoutId: "2",
  isProduction: false
}


export const previewDocs = [previewDocument1];

export const previewData = {
   "$type":"Automation.VisionBotEngine.Model.VBotData, Automation.Cognitive.VisionBotEngine.Model",
   "FieldDataRecord":{
      "$type":"Automation.VisionBotEngine.Model.DataRecord, Automation.Cognitive.VisionBotEngine.Model",
      "Fields":{
         "$type":"System.Collections.Generic.List`1[[Automation.VisionBotEngine.Model.FieldData, Automation.Cognitive.VisionBotEngine.Model]], mscorlib",
         "$values":[
            {
               "$type":"Automation.VisionBotEngine.Model.FieldData, Automation.Cognitive.VisionBotEngine.Model",
               "Id":"78b693d4-cff4-4af8-8176-cd9cf704c65d",
               "Field":{
                  "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                  "Id":"1",
                  "Name":"Invoice_Number",
                  "ValueType":0,
                  "Formula":"",
                  "ValidationID":"",
                  "IsRequired":true,
                  "DefaultValue":"",
                  "StartsWith":"",
                  "EndsWith":"",
                  "FormatExpression":"",
                  "IsDeleted":false
               },
               "Value":{
                  "$type":"Automation.VisionBotEngine.Model.TextValue, Automation.Cognitive.VisionBotEngine.Model",
                  "Value":null,
                  "Field":{
                     "$type":"Automation.VisionBotEngine.Model.Field, Automation.Cognitive.VisionBotEngine.Model",
                     "Id":"",
                     "ParentId":null,
                     "Text":"_ Invoice Number 917979580 05/04/2017 S1 Generated",
                     "Confidence":62,
                     "SegmentationType":0,
                     "Type":0,
                     "Bounds":"848, 83, 751, 67",
                     "Angle":0
                  }
               },
               "ValidationIssue":null
            }
         ]
      }
   },
   "TableDataRecord":{
      "$type":"System.Collections.Generic.List`1[[Automation.VisionBotEngine.Model.TableValue, Automation.Cognitive.VisionBotEngine.Model]], mscorlib",
      "$values":[
         {
            "$type":"Automation.VisionBotEngine.Model.TableValue, Automation.Cognitive.VisionBotEngine.Model",
            "TableDef":{
               "$type":"Automation.VisionBotEngine.Model.TableDef, Automation.Cognitive.VisionBotEngine.Model",
               "Id":"4c4e712d-e485-4ceb-a687-bf74a26571f5",
               "Name":"Table_1",
               "Columns":{
                  "$type":"System.Collections.Generic.List`1[[Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model]], mscorlib",
                  "$values":[
                     {
                        "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                        "Id":"10",
                        "Name":"Item_Description",
                        "ValueType":0,
                        "Formula":"",
                        "ValidationID":"",
                        "IsRequired":true,
                        "DefaultValue":"",
                        "StartsWith":"",
                        "EndsWith":"",
                        "FormatExpression":"",
                        "IsDeleted":false
                     },
                     {
                        "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                        "Id":"11",
                        "Name":"Quantity",
                        "ValueType":3,
                        "Formula":"",
                        "ValidationID":"",
                        "IsRequired":true,
                        "DefaultValue":"",
                        "StartsWith":"",
                        "EndsWith":"",
                        "FormatExpression":"",
                        "IsDeleted":false
                     },
                     {
                        "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                        "Id":"13",
                        "Name":"Item_Total",
                        "ValueType":3,
                        "Formula":"",
                        "ValidationID":"",
                        "IsRequired":true,
                        "DefaultValue":"",
                        "StartsWith":"",
                        "EndsWith":"",
                        "FormatExpression":"",
                        "IsDeleted":false
                     }
                  ]
               },
               "IsDeleted":false
            },
            "Headers":{
               "$type":"System.Collections.Generic.List`1[[Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model]], mscorlib",
               "$values":[
                  {
                     "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                     "Id":"10",
                     "Name":"Item_Description",
                     "ValueType":0,
                     "Formula":"",
                     "ValidationID":"",
                     "IsRequired":true,
                     "DefaultValue":"",
                     "StartsWith":"",
                     "EndsWith":"",
                     "FormatExpression":"",
                     "IsDeleted":false
                  },
                  {
                     "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                     "Id":"11",
                     "Name":"Quantity",
                     "ValueType":3,
                     "Formula":"",
                     "ValidationID":"",
                     "IsRequired":true,
                     "DefaultValue":"",
                     "StartsWith":"",
                     "EndsWith":"",
                     "FormatExpression":"",
                     "IsDeleted":false
                  },
                  {
                     "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                     "Id":"13",
                     "Name":"Item_Total",
                     "ValueType":3,
                     "Formula":"",
                     "ValidationID":"",
                     "IsRequired":true,
                     "DefaultValue":"",
                     "StartsWith":"",
                     "EndsWith":"",
                     "FormatExpression":"",
                     "IsDeleted":false
                  }
               ]
            },
            "Rows":{
               "$type":"System.Collections.Generic.List`1[[Automation.VisionBotEngine.Model.DataRecord, Automation.Cognitive.VisionBotEngine.Model]], mscorlib",
               "$values":[
                  {
                     "$type":"Automation.VisionBotEngine.Model.DataRecord, Automation.Cognitive.VisionBotEngine.Model",
                     "Fields":{
                        "$type":"System.Collections.Generic.List`1[[Automation.VisionBotEngine.Model.FieldData, Automation.Cognitive.VisionBotEngine.Model]], mscorlib",
                        "$values":[
                           {
                              "$type":"Automation.VisionBotEngine.Model.FieldData, Automation.Cognitive.VisionBotEngine.Model",
                              "Id":"d134ce76-09db-499d-813f-6b0bcec97226",
                              "Field":{
                                 "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                                 "Id":"10",
                                 "Name":"Item_Description",
                                 "ValueType":0,
                                 "Formula":"",
                                 "ValidationID":"",
                                 "IsRequired":true,
                                 "DefaultValue":"",
                                 "StartsWith":"",
                                 "EndsWith":"",
                                 "FormatExpression":"",
                                 "IsDeleted":false
                              },
                              "Value":{
                                 "$type":"Automation.VisionBotEngine.Model.TextValue, Automation.Cognitive.VisionBotEngine.Model",
                                 "Value":null,
                                 "Field":{
                                    "$type":"Automation.VisionBotEngine.Model.Field, Automation.Cognitive.VisionBotEngine.Model",
                                    "Id":"",
                                    "ParentId":null,
                                    "Text":"BX",
                                    "Confidence":96,
                                    "SegmentationType":0,
                                    "Type":0,
                                    "Bounds":"364, 1013, 30, 18",
                                    "Angle":0
                                 }
                              },
                              "ValidationIssue":null
                           },
                           {
                              "$type":"Automation.VisionBotEngine.Model.FieldData, Automation.Cognitive.VisionBotEngine.Model",
                              "Id":"81f7be2b-b28b-46fa-b308-5536c57e63ef",
                              "Field":{
                                 "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                                 "Id":"11",
                                 "Name":"Quantity",
                                 "ValueType":3,
                                 "Formula":"",
                                 "ValidationID":"",
                                 "IsRequired":true,
                                 "DefaultValue":"",
                                 "StartsWith":"",
                                 "EndsWith":"",
                                 "FormatExpression":"",
                                 "IsDeleted":false
                              },
                              "Value":{
                                 "$type":"Automation.VisionBotEngine.Model.TextValue, Automation.Cognitive.VisionBotEngine.Model",
                                 "Value":null,
                                 "Field":{
                                    "$type":"Automation.VisionBotEngine.Model.Field, Automation.Cognitive.VisionBotEngine.Model",
                                    "Id":"",
                                    "ParentId":null,
                                    "Text":"5",
                                    "Confidence":96,
                                    "SegmentationType":0,
                                    "Type":0,
                                    "Bounds":"269, 1013, 12, 18",
                                    "Angle":0
                                 }
                              },
                              "ValidationIssue":null
                           },
                           {
                              "$type":"Automation.VisionBotEngine.Model.FieldData, Automation.Cognitive.VisionBotEngine.Model",
                              "Id":"8dfa31c0-34b8-4187-ab0c-a9398299deb9",
                              "Field":{
                                 "$type":"Automation.VisionBotEngine.Model.FieldDef, Automation.Cognitive.VisionBotEngine.Model",
                                 "Id":"13",
                                 "Name":"Item_Total",
                                 "ValueType":3,
                                 "Formula":"",
                                 "ValidationID":"",
                                 "IsRequired":true,
                                 "DefaultValue":"",
                                 "StartsWith":"",
                                 "EndsWith":"",
                                 "FormatExpression":"",
                                 "IsDeleted":false
                              },
                              "Value":{
                                 "$type":"Automation.VisionBotEngine.Model.TextValue, Automation.Cognitive.VisionBotEngine.Model",
                                 "Value":null,
                                 "Field":{
                                    "$type":"Automation.VisionBotEngine.Model.Field, Automation.Cognitive.VisionBotEngine.Model",
                                    "Id":"",
                                    "ParentId":null,
                                    "Text":"208.40",
                                    "Confidence":95,
                                    "SegmentationType":0,
                                    "Type":0,
                                    "Bounds":"1537, 1012, 88, 22",
                                    "Angle":0
                                 }
                              },
                              "ValidationIssue":null
                           }
                        ]
                     }
                  }
               ]
            }
         }
      ]
   }
};

export const previewLayoutData = {
    SirFields: {
        Fields: {
            $values: [{Bounds: '6, 8, 7, 8', Text: 'value text'}],
        },
    },
    DocumentProperties: {
        Height: 200,
        Width: 300,
        Id: '12',
        PageProperties: [
            {
                Height: 200,
                Width: 300,
            },
        ],
    },
};
