/* eslint-disable */

export const languages1 = [
    {
        "id": 1,
        "name": "English",
        "code": "eng"
    }, {
        "id": 2,
        "name": "German",
        "code": "gem"
    }, {
        "id": 3,
        "name": "French",
        "code": "fra"
    }, {
        "id": 4,
        "name": "Spanish",
        "code": "spa"
    }, {
        "id": 5,
        "name": "Italian",
        "code": "ita"
    }
];
