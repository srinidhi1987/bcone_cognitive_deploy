const url = '/bots/image.png';
const visionBotId = '21313123132123';
const layouts = [
    {
        Id: '123',
        DocProperties: {
            Id: '12',
            Width: 300,
            Height: 200,
            PageProperties: [{Height: 200, Width: 300}],
        },
        SirFields: {
            Fields: [{Bounds: '6, 8, 7, 8', Text: 'value text'}, {Bounds: '1, 2, 3, 4', Text: 'label text'}, {Bounds: '11, 23, 40, 50', Text: 'TABLE'}],
        },
        Fields: [{Bounds: '1, 2, 3, 4', FieldId: '5', ValueBounds: '5, 6, 7, 8'}],
        Tables: [{Columns: [{Bounds: '11, 23, 40, 50', FieldId: '3'}]}],
    },
];
const language = '1';

export const fields1 = [
    {
        Id: '5',
    },
];
export const tables1 = [
    {
        Id: '12312524',
        Columns: [
            {
                Id: '3',
            },
        ],
    },
];

export const tables2 = [
    {
        Id: 't1',
        Columns: [
            {
                Id: '10',
            },
            {
                Id: '11',
            },
        ],
    },
    {
        Id: 't2',
        Columns: [
            {
                Id: '12',
            },
            {
                Id: '14',
            },
        ],
    },
];

export const visionBotData1 = {
    url,
    visionBotData: {
        Id: visionBotId,
        Layouts: layouts,
        DataModel: {
            Fields: fields1,
            Tables: tables1,
        },
        Language: language,
    },
    validationData,
};

export const validationData = [
    {
        'ID':'1',
        'ValidatorData':{
            'FilePath':null,
            'StaticListItems':[
                '729081',
                '729082',
                '729083',
            ],
        },
        'TypeOfData':'Automation.VisionBotEngine.Validation.StaticListProvider, Automation.Cognitive.VisionBotEngine.Validation, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null',
        'SerializedData':'{"StaticListItems":["729081","729082","729083"]}',
    },
    {
        'ID':'8',
        'ValidatorData':{
            'FilePath':null,
            'StaticListItems':[
                'CEH24L,CLPE6',
            ],
        },
        'TypeOfData':'Automation.VisionBotEngine.Validation.StaticListProvider, Automation.Cognitive.VisionBotEngine.Validation, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null',
        'SerializedData':'{"StaticListItems":["CEH24L,CLPE6"]}',
    },
];
