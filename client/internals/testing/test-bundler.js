// Mock the browser local storage fot testing
jest.resetModules();
window.localStorage = {
    // storage map
    storage: {},
    /**
     * get item from sotrage
     *
     * @param {string} key
     * @returns {*}
     */
    getItem(key) {
        return this.storage[key];
    },
    /**
     * Set item in storage
     *
     * @param {string} key
     * @param {*} value
     * returns void
     */
    setItem(key, value) {
        this.storage[key] = value;
    },
    /**
     * Remove item from storage
     *
     * @param {string} key
     * returns void
     */
    removeItem(key) {
        delete this.storage[key];
    },
};
