/**
 * Created by iamcutler on 5/19/17.
 */

/**
 * Mock local storage
 */
export const mockLocalStorage = {
    // storage map
    storage: {},
    /**
     * get item from sotrage
     *
     * @param {string} key
     * @returns {*}
     */
    getItem(key) {
        return this.storage[key];
    },
    /**
     * Set item in storage
     *
     * @param {string} key
     * @param {*} value
     * returns void
     */
    setItem(key, value) {
        this.storage[key] = value;
    },
    /**
     * Remove item from storage
     *
     * @param {string} key
     * returns void
     */
    removeItem(key) {
        delete this.storage[key];
    },
    /**
     * Removes all keys from storage
     */
    clear() {
        Object.keys(this.storage).forEach(this.removeItem.bind(this));
    },
};
