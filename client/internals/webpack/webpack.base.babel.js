/**
 * COMMON WEBPACK CONFIGURATION
 */

const path = require('path');
const webpack = require('webpack');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');

const DEV = process.env.NODE_ENV !== 'production';
const FONT_FILENAME = 'asset/[fontname]-[hash:6][ext]'; // omit the '.', [ext] includes it.
const FONT_PARAMS = DEV ? 'embed=true&types=svg' : `fileName=${FONT_FILENAME}`;
const ASSET_FILENAME = 'asset/[name]-[hash:6].[ext]';

const URL_PARAMS = DEV
    ? 'limit=4294967296'
    : `limit=1024&name=${ASSET_FILENAME}`;

// Common frontend dependencies
const commonDepsRegex = /^node_modules\/(?!(common-frontend-components|common-frontend-utils))/;
module.exports = (options) => ({
    entry: options.entry,
    output: Object.assign({ // Compile into js/build.js
        path: path.resolve(process.cwd(), 'build/www'),
        publicPath: '',
    }, options.output), // Merge with env dependent settings
    module: {
        loaders: [{
            test: /\.jsx?$/, // Transform all .js files required somewhere with Babel
            loader: 'babel',
            exclude: commonDepsRegex,
            query: options.babelQuery,
        }, {
            // Fix for add-module-exports plugin error with react-fontawesome
            test: /\.js$/,
            loader: 'babel',
            include: /node_modules\/react-fontawesome/,
            query: Object.assign({},
                options.babelQuery,
                {
                    plugins: ['babel-plugin-add-module-exports'].map(require.resolve),
                }
            ),
        }, {
            test: /\.scss$/,
            exclude: commonDepsRegex,
            loaders: ['style-loader', 'css-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader'],
        }, {
            // Do not transform vendor's CSS with CSS-modules
            // The point is that they remain in global scope.
            // Since we require these CSS files in our JS or CSS files,
            // they will be a part of our compilation either way.
            // So, no need for ExtractTextPlugin here.
            test: /\.css$/,
            include: /node_modules/,
            loaders: ['style-loader', 'css-loader'],
        }, {
            test: /\.font\.js$/,
            exclude: commonDepsRegex,
            loaders: [
                'style-loader', 'css-loader', `webfonts-loader?${FONT_PARAMS}`,
            ],
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            //loader: `url?${URL_PARAMS}&mimetype=application/vnd.ms-fontobject`,
            loader: 'file-loader?useRelativePath=true',
        }, {
            test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
            exclude: commonDepsRegex,
            //loader: `url?${URL_PARAMS}&mimetype=application/font-woff`,
            loader: 'file-loader?useRelativePath=true',
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            exclude: commonDepsRegex,
            //loader: `url?${URL_PARAMS}&mimetype=application/octet-stream`,
            loader: 'file-loader?useRelativePath=true',
        }, {
            test: /\.ico(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader?name=[name].[ext]&useRelativePath=true',
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            exclude: commonDepsRegex,
            loader: `url-loader?${URL_PARAMS}&useRelativePath=true`,
        }, {
            test: /\.(jpg|png|gif)$/,
            exclude: commonDepsRegex,
            loader: `url-loader?${URL_PARAMS}&context=./src&useRelativePath=true`,
        }, {
            test: /\.html$/,
            loader: 'html-loader',
        }, {
            test: /\.json$/,
            loader: 'json-loader',
        }, {
            test: /\.(mp4|webm)$/,
            loader: 'url-loader?limit=10000',
        }],
    },
    plugins: options.plugins.concat([
        new webpack.ProvidePlugin({
            // make fetch available
            fetch: 'exports?self.fetch!whatwg-fetch',
        }),

        // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
        // inside your code for any environment checks; UglifyJS will automatically
        // drop any unreachable code.
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
        new webpack.NamedModulesPlugin(),
    ]),
    resolve: {
        alias: {
            // Use this projects react dependency
            'fs': path.join(process.cwd(), 'node_modules', 'fs-extra'),
            'react': path.join(process.cwd(), 'node_modules', 'react'),
            'react-dom': path.join(process.cwd(), 'node_modules', 'react-dom'),
            'react-router': path.join(process.cwd(), 'node_modules', 'react-router'),
            'react-addons-test-utils': path.join(process.cwd(), 'node_modules', 'common-frontend-components', 'node_modules', 'react-addons-test-utils'),
        },
        modules: ['app', 'node_modules'],
        extensions: [
            '.js',
            '.jsx',
            '.react.js',
        ],
        mainFields: [
            'browser',
            // Necessary hack because of a bug in redux-form
            // https://github.com/erikras/redux-form/issues/1637
            'main',
            'jsnext:main',
        ],
    },
    devtool: options.devtool,
    target: 'web', // Make web variables accessible to webpack, e.g. window
    node: {
        net: 'empty',
        tls: 'empty',
    },
    externals: {
        iqBotConfig: 'iqBotConfig',
    },
});
