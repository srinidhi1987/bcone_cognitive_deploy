/**
 * Created by iamcutler on 4/20/17.
 */

const fs = require('fs');
const Promise = require('bluebird');

/**
 * Build package.js file for production
 *
 * @param {object} options
 * @param {string} options.packagePath - Resolve path to package.js file
 * @constructor
 */
function BuildProductionPackageFilePlugin(options = {}) {
    this.packagePath = options.packagePath || `${process.cwd()}/package.json`;
    this.destinationPath = options.destinationPath || `${process.cwd()}/build`;
}

BuildProductionPackageFilePlugin.prototype.apply = function(compiler) {
    // Fire after webpack compilation has run
    compiler.plugin('after-emit', (compilation, callback) => {
        try {
            fs.readFile(this.packagePath, 'utf8', (err, content) => {
                if (err) throw err;

                const pkg = JSON.parse(content);

                Promise.props({
                    branch: null,
                    commitHash: null,
                })
                .then((results) => {
                    delete pkg.babel; // remove transpiler in production
                    delete pkg.contributors; // remove development contributors
                    delete pkg.devDependencies; // remove development dependencies
                    delete pkg.dllPlugin; // Don't need DLL list in production
                    delete pkg.repository; // remove codebase information
                    delete pkg.publishConfig; // remove nexus internal repo
                    delete pkg['lint-staged'];
                    delete pkg['pre-commit'];
                    pkg['buildTime'] = new Date(); // set build time
                    pkg['branchName'] = results.branch;
                    pkg['commitHash'] = results.commitHash;
                    // Replace start from dev to prod
                    pkg.scripts.start = pkg.scripts['start:prod'];

                    // remove unused production scripts
                    [
                        /analyze/,
                        /^build/,
                        /^clean/,
                        /install/,
                        /^lint/,
                        /^start:/,
                        /^test/,
                        'coveralls',
                        'extract-intl',
                        'generate',
                        'npmcheckversion',
                        'pagespeed',
                        'postsetup',
                        'prepublish',
                        'prebuild',
                        'presetup',
                        'pretest',
                        'setup',
                    ].forEach((key) => {
                        if (typeof key === 'string' && key in pkg.scripts) {
                            delete pkg.scripts[key];
                        }
                        else {
                            // run a regex
                            for (let i = 0, scripts = Object.keys(pkg.scripts); i < scripts.length; i++) {
                                if (new RegExp(key).test(scripts[i])) delete pkg.scripts[scripts[i]];
                            }
                        }
                    });

                    // write new package file
                    fs.writeFileSync(`${this.destinationPath}/package.json`, JSON.stringify(pkg, null, 2));

                    callback();
                })
                .catch(callback);
            });
        }
        catch (e) {
            console.error(e); // eslint-disable-line no-console
            callback(e);
        }
    });
};

module.exports = BuildProductionPackageFilePlugin;
