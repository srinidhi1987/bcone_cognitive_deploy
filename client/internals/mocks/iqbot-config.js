module.exports = {
    IQ_BOT_BASE_PATH: '/iqbot/',
    CONTROL_ROOM_URL: 'http://1.2.3.4',
    APP_STORAGE: {
        token: 'authToken',
        user: 'user',
        lastUserInteractionTimestamp: 'lastUserInteractionTimestamp',
    },
    AUTH_TOKEN_HEADER: 'x-authorization',
    SESSION_EXPIRE_TIME_OUT: 20,
};
