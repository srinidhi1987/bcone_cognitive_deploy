/* eslint-disable */
const path = require('path');

module.exports = {
    "collectCoverage": true,
    "collectCoverageFrom": [
        "**/*.js",
        "!index.js",
        "!jest.config.js",
        "!api.js",
        "!config.js"
    ],
    "browser": false,
    "testEnvironment": "node",
    "coverageThreshold": {
        "global": {
        "statements": 0,
        "branches": 0,
        "functions": 0,
        "lines": 0,
        }
    },
    "globals": {
        "appRoot": path.resolve(__dirname, '../server'),
        "appControllers": `${__dirname}/controllers`,
        "appServices": `${__dirname}/services`,
    },
    "modulePathIgnorePatterns": [
        "<rootDir>/build/",
    ],
    "testRegex": ".*\\.test\\.js$"
};
