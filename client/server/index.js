/* eslint consistent-return:0 */

const cluster = require('cluster');
const isProduction = process.env.NODE_ENV === 'production';

// -----------------------------------------------------------
// Development
// -----------------------------------------------------------
if (!isProduction) return launchExpressApp();

// -----------------------------------------------------------
// Production
// -----------------------------------------------------------
if (cluster.isMaster) {
    const logger = require('./utils/logger');
    // Count the machine's CPUs
    const cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (let i = 0; i < cpuCount; i++) {
        cluster.fork();
    }

    // Listen for dying workers
    cluster.on('exit', (worker) => {
        logger.error('Worker %d died :(', worker.id);
        cluster.fork();
    });
}
else {
    // Launch application in a worker process
    launchExpressApp();
}

// -----------------------------------------------------------
// Express Application Wrapper
// -----------------------------------------------------------
function launchExpressApp() {
    // include node variables for development
    // read from .env file
    if (!isProduction) {
        require('dotenv').config();
    }

    const express = require('express');
    const app = express();
    const argv = require('minimist')(process.argv.slice(2));
    const busboy = require('express-busboy');
    const config = require('./config');
    const cors = require('cors');
    const ngrok = (!isProduction && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
    const resolve = require('path').resolve;
    const serverUtils = require('./utils/server');
    const setup = require('./middlewares/frontendMiddleware');
    const isSecureServer = (process.env.RUN_WITH_SSL === 'true');
    const proxy = require('express-http-proxy');
    const iqBotProxy = require('./middlewares/iqBotLiteProxy');
    const logger = require('./utils/logger');

    // ========================================================================
    /**
     * IMPORTANT NOTE
     * /gateway, /v1/registration, and /organizations route middleware must be defined BEFORE busboy.extend is called.
     * This is in order for busboy not to interfere with body parsing in the proxy middleware used by these routes.
     */

    /**
     * Gateway proxy API - passes requests directly to the gateway api
     * Used by thick client apps (ex. Designer and Validator)
     */
    app.use(
        '/gateway',
        cors(),
        proxy(config.gateway.endpoint, {
            proxyReqPathResolver: (req) => {
                return `${req.originalUrl.replace('/gateway', '')}`;
            },
            preserveHostHdr: true,
            limit: '50mb',
        })
    );

    /**
     * IQBot Registration API - passes requests directly to the gateway api
     * Used by CR to register application
     */
    app.use(
        '/v1/registration',
        cors(),
        proxy(config.gateway.endpoint, {
            proxyReqPathResolver: (req) => req.originalUrl,
            preserveHostHdr: true,
            limit: '50mb',
        })
    );

    /**
     * IQBot Command API - passes request directly to gateway api
     * and specifically handle multipart request for production documents
     */
    app.use(
        '/organizations',
        cors(),
        iqBotProxy.iqBotLiteProxy()
    );

    /**
     * IQBot configurations API - passes request directly to configurations api
     */
    app.use(
        '/configurations',
        cors(),
        proxy(config.gateway.endpoint, {
            proxyReqPathResolver: (req) => req.originalUrl,
            preserveHostHdr: true,
        })
    );

    // ========================================================================


    // Custom api response
    app.use((req, res, next) => {
        res.apiResponse = (data = null, errors = [], success = true, status = 200) => {
            return res.status(status).json({data, errors, success, statusCode: status});
        };
        next();
    });

    busboy.extend(app, {
        upload: true,
        path: './tmp',
        allowedPath: /./,
    });
    // Map global values
    global.appRoot = __dirname;
    global.appControllers = `${process.cwd()}/server/controllers`;
    global.appServices = `${process.cwd()}/server/services`;
    global.appRoutes = `${process.cwd()}/server/routes`;
    // Set res locals from current session
    app.use((req, res, next) => {
        res.locals.currentUser = {
            organizationId: config.ORGANIZATION_ID,
            authToken: req.headers[config.AUTH_TOKEN_HEADER]
                || req.query.token
                || req.headers[config.BEARER_TOKEN_HEADER]
                || null,
        };
        next();
    });

    // middleware to avoid caching in IE-Edge browser
    app.use((req, res, next) => {
        res.setHeader('Expires', 0);
        res.setHeader('Cache-control', 'no-store');
        next();
    });

    // Health check
    const {healthCheck} = require(`${global.appControllers}/app/v1`);
    app.get('/healthcheck', healthCheck);

    // Backend API
    app.use('/api', cors(), require('./api'));

    // In production we need to pass these values in instead of relying on webpack
    setup(app, {
        outputPath: resolve(process.cwd(), 'www'),
        publicPath: '/',
    });

    // Error handling
    app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
        logger.error(err);

        const getErrorsArray = (err) => {
            if (!err) return null; // return null to api response if no error is present
            if (err instanceof Error || err instanceof TypeError) {
                return [err.message];
            }
            else if (err.hasOwnProperty('errors')) {
                return err.errors;
            }

            return (Array.isArray(err)) ? err : [err];
        };

        res.apiResponse(
            null,
            getErrorsArray(err),
            false,
            (err.statusCode || 500)
        );
    });


    // get the intended port number, use port 3000 if not provided
    const port = argv.port || process.env.PORT || 3000;
    // Start server
    const server = serverUtils.createServer(app, {
        secure: isSecureServer,
    }).listen(port, (err) => {
        serverUtils.logAppStarted(ngrok, {
            port,
            secure: isSecureServer || false,
        }, err);
    });

    server.timeout = 300000;
}
