/**
 * Created by iamcutler on 5/9/17.
 */

const apiUtils = require('../../../utils/requestHandlers');

describe('utils: requestHandler', () => {
    describe('method: isStatusCodeOkayWithNoBody', () => {
        it('should return true if status code is 201', () => {
            // given
            // when
            // then
            expect(apiUtils.isStatusCodeOkayWithNoBody(201)).toBe(true);
        });

        it('should return true if status code is 204', () => {
            // given
            // when
            // then
            expect(apiUtils.isStatusCodeOkayWithNoBody(204)).toBe(true);
        });

        it('should return true if status code is 205', () => {
            // given
            // when
            // then
            expect(apiUtils.isStatusCodeOkayWithNoBody(205)).toBe(true);
        });

        it('should return false if status code is 200', () => {
            // given
            // when
            // then
            expect(apiUtils.isStatusCodeOkayWithNoBody(200)).toBe(false);
        });

        it('should return false if status code is 500', () => {
            // given
            // when
            // then
            expect(apiUtils.isStatusCodeOkayWithNoBody(500)).toBe(false);
        });
    });
});
