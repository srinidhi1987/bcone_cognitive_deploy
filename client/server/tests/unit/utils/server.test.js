jest.mock('child_process', () => ({
    exec: jest.fn(),
}));

const fs = require('fs');
const child_process = require('child_process'); // eslint-disable-line camelcase
const shelljs = require('shelljs');
const serverUtil = require('../../../utils/server');

describe('Util: server', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('method: getPackage', () => {
        it('should return the package.json content', (done) => {
            // given
            // when
            serverUtil.getPackage()
                .then((result) => {
                    // then
                    expect(Object.keys(result)).toEqual(expect.arrayContaining([
                        'name',
                        'version',
                        'author',
                        'version',
                        'dependencies',
                        'devDependencies',
                    ]));
                    done();
                });
        });

        it('should return an error if reading the package file fails', (done) => {
            // given
            const error = new Error('reading package.json file failed');
            jest.spyOn(fs, 'readFile').mockImplementation((file, cb) => cb(error));
            // when
            serverUtil.getPackage()
                .catch((err) => {
                    // then
                    expect(err.message).toEqual(error.message);
                    done();
                });
        });
    });

    describe('method: getCurrentSCMBranchName', () => {
        // given
        const branchNames =
            'RC-1.0-1\n' +
            '* develop\n' +
            'RC-2.0-1\n';

        beforeEach(() => {
            jest.spyOn(shelljs, 'exec');
        });

        it('should call the expected command to fetch the current branch name', (done) => {
            // given
            shelljs.exec.mockImplementation((command, options, cb) => cb(0, branchNames, null));
            // when
            serverUtil.getCurrentSCMBranchName()
                .then(() => {
                    // then
                    expect(shelljs.exec).toHaveBeenCalledWith('git branch', {silent: true}, expect.any(Function));
                    done();
                });
        });

        it('should return the current branch name', (done) => {
            // given
            shelljs.exec.mockImplementation((command, options, cb) => cb(0, branchNames, null));
            // when
            serverUtil.getCurrentSCMBranchName()
                .then((result) => {
                    // then
                    expect(result).toBe('develop');
                    done();
                });
        });

        describe('failure path', () => {
            it('should return an error if present', (done) => {
                // given
                const error = new Error('something went wrong');
                shelljs.exec.mockImplementation((command, options, cb) => cb(0, '', error));
                // when
                serverUtil.getCurrentSCMBranchName()
                    .catch((err) => {
                        // then
                        expect(err.message).toBe(error.message);
                        done();
                    });
            });

            it('should return an error if code is not 0', (done) => {
                // given
                const error = new Error('something went wrong');
                shelljs.exec.mockImplementation((command, options, cb) => cb(1, '', error));
                // when
                serverUtil.getCurrentSCMBranchName()
                    .catch((err) => {
                        // then
                        expect(err.message).toBe(error.message);
                        done();
                    });
            });
        });
    });

    describe('method: getLastCommitHash', () => {
        // given
        const commitHash = '908y90ygh309h2490h234g';

        it('should call the expected command to fetch the late comment hash', (done) => {
            // given
            child_process.exec.mockImplementation((command, cb) => cb(null, commitHash, null)); // eslint-disable-line camelcase
            // when
            serverUtil.getLastCommitHash()
                .then(() => {
                    // then
                    expect(child_process.exec).toHaveBeenCalledWith('git log --pretty=format:\'%H\' -n 1', expect.any(Function)); // eslint-disable-line camelcase
                    done();
                });
        });

        it('should return the current branch name', (done) => {
            // given
            child_process.exec.mockImplementation((command, cb) => cb(null, commitHash, null)); // eslint-disable-line camelcase
            // when
            serverUtil.getLastCommitHash()
                .then((result) => {
                    // then
                    expect(result).toBe(commitHash);
                    done();
                });
        });

        describe('failure path', () => {
            it('should return an error if present', (done) => {
                // given
                const error = new Error('something went wrong');
                child_process.exec.mockImplementation((command, cb) => cb(error)); // eslint-disable-line camelcase
                // when
                serverUtil.getLastCommitHash()
                    .catch((err) => {
                        // then
                        expect(err.message).toBe(error.message);
                        done();
                    });
            });

            it('should return an the stderr if present', (done) => {
                // given
                const error = new Error('something went wrong');
                child_process.exec.mockImplementation((command, cb) => cb(null, commitHash, error)); // eslint-disable-line camelcase
                // when
                serverUtil.getLastCommitHash()
                    .catch((err) => {
                        // then
                        expect(err.message).toBe(error.message);
                        done();
                    });
            });
        });
    });
});
