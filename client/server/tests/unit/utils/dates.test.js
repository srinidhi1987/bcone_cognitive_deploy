const dateUtil = require('../../../utils/dates');

describe('Util: Dates', () => {
    describe('method: toHHMMSS', () => {
        it('should display expected format from seconds', () => {
            // given
            const seconds = 45676;
            // when
            const result = dateUtil.toHHMMSS(seconds);
            // then
            expect(result).toBe('12h 41m 16s');
        });
    });

    describe('method: leftPad', () => {
        it('should return the same number if greater than 10', () => {
            // given
            const num = 14;
            // when
            const result = dateUtil.leftPad(num);
            // then
            expect(result).toBe(14);
        });

        it('should return the number padded with a 0 if less than 10', () => {
            // given
            const num = 4;
            // when
            const result = dateUtil.leftPad(num);
            // then
            expect(result).toBe(Number('04'));
        });
    });
});
