const {isServerConnectionError} = require('../../../utils/domains');

describe('domain utilities', () => {
    describe('isServerConnectionError', () => {
        it('should give error if sql db is down', () => {
            const result = {errors: 'Internal Server Error.'};

            expect(isServerConnectionError(result)).toEqual(true);
        });

        it('should give error if gateway service is down', () => {
            const result = {code: 'ECONNREFUSED'};

            expect(isServerConnectionError(result)).toEqual(true);
        });

        it('should give error if alias service is down', () => {
            const result = {exception: 'java.net.ConnectException'};

            expect(isServerConnectionError(result)).toEqual(true);
        });
    });
});
