const Promise = require('bluebird');
const clone = require('clone');
const {req, res} = require('../../fixtures/app');
const validatorController = require('../../../../controllers/validator/v1.js');
const validatorService = require('../../../../services/validator');

describe('Controller: Validator v1', () => {
    beforeEach(() => {
        jest.spyOn(res, 'apiResponse');
        jest.spyOn(validatorService, 'markAsInvalid');
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('method: markAsInvalid', () => {
        it('should mark file as invalid without any reasons given', () => {
            const request = clone(req);
            // given
            request.body = [];
            request.params = {projectId: '2', fileId: '3'};

            const apiResponse = {
                success: true,
            };

            validatorService.markAsInvalid.mockImplementation(() => {
                return Promise.resolve(apiResponse);
            });

            const next = jest.fn();

            // when
            return validatorController.markAsInvalid(request, res, next)
                .finally(() => {
                    // then
                    expect(res.apiResponse).toHaveBeenCalledWith(apiResponse);
                    expect(next).not.toHaveBeenCalled();
                });
        });

        it('should mark file as invalid with reasons given', () => {
            const request = clone(req);
            // given
            request.body = [{reasonId: '1', reasonText: 'Tables missing', fileId: '3'}];
            request.params = {projectId: '2', fileId: '3'};

            const apiResponse = {
                success: true,
            };

            const next = jest.fn();
            validatorService.markAsInvalid.mockImplementation(() => {
                return Promise.resolve(apiResponse);
            });

            // when
            return validatorController.markAsInvalid(request, res, next)
                .finally(() => {
                    // then
                    expect(res.apiResponse).toHaveBeenCalledWith(apiResponse);
                    expect(next).not.toHaveBeenCalled();
                });
        });
    });

    it('should throw an error if the service call was not succesful', () => {
        const request = clone(req);
        // given
        request.body = [{reasonId: '1', reasonText: 'Tables missing', fileId: '3'}];
        request.params = {projectId: '2', fileId: '3'};

        const next = jest.fn();
        validatorService.markAsInvalid.mockImplementation(() => {
            return Promise.reject(['could not invalidate file']);
        });

        // when
        return validatorController.markAsInvalid(request, res, next)
            .finally(() => {
                // then
                expect(res.apiResponse).not.toHaveBeenCalled();
                expect(next).toHaveBeenCalledWith(['could not invalidate file']);
            });
    });
});
