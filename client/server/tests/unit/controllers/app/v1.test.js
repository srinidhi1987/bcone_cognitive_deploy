const Promise = require('bluebird');
const app = require('../../fixtures/app');
const serverUtils = require('../../../../utils/server');
const dateUtil = require('../../../../utils/dates');
const appController = require('../../../../controllers/app/v1');

describe('Controller: App', () => {
    beforeEach(() => {
        jest.spyOn(app.res, 'send').mockImplementation((input) => input);
        jest.spyOn(app.res, 'sendStatus').mockImplementation((input) => input);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('method: healthCheck', () => {
        it('should call service to get package.json content', (done) => {
            // given
            const packageJSON = {
                name: 'cognitive-frontend',
                version: '1.0.0',
                description: 'Automation Anywhere IQ Bot',
                buildTime: new Date(),
                branchName: 'testing',
                commitHash: '3453545676758676578',
            };
            jest.spyOn(serverUtils, 'getPackage').mockImplementation(() => Promise.resolve(packageJSON));
            // when
            appController.healthCheck(app.req, app.res)
                .finally(() => {
                    // then
                    expect(app.res.send).toHaveBeenCalledWith(
                        'Application: Client Gateway\n' +
                        'Status: OK\n' +
                        `Version: ${packageJSON.version}\n` +
                        `Branch: ${packageJSON.branchName}\n` +
                        `GIT #: ${packageJSON.commitHash}\n` +
                        `Build Time: ${packageJSON.buildTime}\n` +
                        `Application uptime: ${dateUtil.toHHMMSS(Math.floor(process.uptime()))}`
                    );
                    done();
                });
        });

        it('should return a 500 if something goes wrong', (done) => {
            // given
            jest.spyOn(serverUtils, 'getPackage').mockImplementation(() => Promise.reject('Something went wrong'));
            // when
            appController.healthCheck(app.req, app.res)
                .finally(() => {
                    // then
                    expect(app.res.sendStatus).toHaveBeenCalledWith(500);
                    done();
                });
        });
    });
});
