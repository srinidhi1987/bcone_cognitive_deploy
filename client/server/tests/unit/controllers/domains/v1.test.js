jest.mock('fs');

const clone = require('clone');
const Promise = require('bluebird');
const fs = require('fs');
const {req, res} = require('../../fixtures/app');
const domainController = require(`${global.appControllers}/domains/v1`);
const domainService = require(`${global.appServices}/domain`);

describe('Controller: Domains v1', () => {
    beforeEach(() => {
        jest.spyOn(res, 'apiResponse');
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('method: importUnencryptedDomain', () => {
        beforeEach(() => {
            jest.spyOn(domainService, 'sendUnencryptedDomain');
        });

        const fileResponse = {
            toString() {
                return `
                    { "name": "testing" }
                `;
            },
        };

        it('should read the file from the temp location', () => {
            // given
            const request = clone(req);
            request.files = {
                domain: {
                    mimetype: 'application/json',
                    file: 'temp/testing-is-awesomeness', // temp location
                },
            };
            const apiResponse = {
                success: true,
            };
            fs.readFile.mockImplementation((filePath, cb) => cb(null, fileResponse));
            domainService.sendUnencryptedDomain.mockImplementation(() => Promise.resolve(apiResponse));
            const next = jest.fn();
            // when
            return domainController.importUnencryptedDomain(request, res, next)
                .finally(() => {
                    // then
                    expect(res.apiResponse).toHaveBeenCalledWith(apiResponse);
                    expect(next).not.toHaveBeenCalled();
                });
        });

        describe('failure path', () => {
            it('should throw an exception if a file is not provided', () => {
                // given
                const next = jest.fn();
                // when
                domainController.importUnencryptedDomain(req, res, next);
                // then
                expect(next).toHaveBeenCalledWith(new Error('No domain file was provided'));
            });

            it('should throw an exception if the file type is not application/json', () => {
                // given
                const request = clone(req);
                request.files = {
                    domain: {
                        mimetype: 'text',
                    },
                };
                const next = jest.fn();
                // when
                domainController.importUnencryptedDomain(request, res, next);
                // then
                expect(next).toHaveBeenCalledWith(new Error('Incorrect type file. Must provide a json domain file'));
            });

            it('should throw an exception if an error occurred while reading the upload file', () => {
                // given
                const error = new Error('Error reading the file');
                const request = clone(req);
                request.files = {
                    domain: {
                        mimetype: 'application/json',
                        file: 'temp/testing-is-awesomeness', // temp location
                    },
                };
                fs.readFile.mockImplementation((filePath, cb) => cb(error));
                const next = jest.fn();
                jest.spyOn(res, 'apiResponse');
                // when
                return domainController.importUnencryptedDomain(request, res, next)
                    .finally(() => {
                        // then
                        expect(next).toHaveBeenCalledWith(error);
                    });
            });

            it('should throw an exception if not able to parse the JSON from the file', () => {
                // given
                const expectedError = new Error('Error parsing domain file');
                const request = clone(req);
                request.files = {
                    domain: {
                        mimetype: 'application/json',
                        file: 'temp/testing-is-awesomeness', // temp location
                    },
                };
                fs.readFile.mockImplementation((filePath, cb) => cb(null, 'this is not valid json'));
                const next = jest.fn();
                jest.spyOn(res, 'apiResponse');
                // when
                return domainController.importUnencryptedDomain(request, res, next)
                    .finally(() => {
                        // then
                        expect(next).toHaveBeenCalledWith(expectedError);
                    });
            });
        });
    });
});
