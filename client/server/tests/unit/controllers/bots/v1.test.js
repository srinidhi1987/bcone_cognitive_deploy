const Promise = require('bluebird');
const clone = require('clone');
const {req, res} = require('../../fixtures/app');
const botController = require('../../../../controllers/bots/v1.js');
const botService = require('../../../../services/bot');

describe('Controller: Validator v1', () => {
    beforeEach(() => {
        jest.spyOn(res, 'apiResponse');
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('method: keepAlive', () => {
        beforeEach(() => {
            jest.spyOn(botService, 'keepAlive');
        });

        it('should hit keep designer Alive service', () => {
            const request = clone(req);
            // given
            request.body = [];
            request.params = {projectId: '2', categoryId: '3', visionBotId: '4'};

            const apiResponse = {
                success: true,
            };

            const next = jest.fn();
            botService.keepAlive.mockImplementation(() => {
                return Promise.resolve(apiResponse);
            });

            // when
            return botController.keepDesignerAlive(request, res, next)
                .finally(() => {
                    // then
                    expect(res.apiResponse).toHaveBeenCalledWith(apiResponse);
                    expect(next).not.toHaveBeenCalled();
                });
        });

        it('should throw an error if the service call was not succesful', () => {
            const request = clone(req);
            // given
            request.body = [];
            request.params = {projectId: '2', categoryId: '3', visionBotId: '4'};

            const next = jest.fn();

            botService.keepAlive.mockImplementation(() => {
                return Promise.reject(['could not keep alive']);
            });

            // when
            return botController.keepDesignerAlive(request, res, next)
                .finally(() => {
                    // then
                    expect(res.apiResponse).not.toHaveBeenCalled();
                    expect(next).toHaveBeenCalledWith(['could not keep alive']);
                });
        });
    });
});
