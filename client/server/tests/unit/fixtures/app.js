
module.exports = {
    req: {
        files: {},
        params: {
            fileId: '1',
            projectId: '22',
        },
        headers: {
            username: 'andrew',
        },
    },
    res: {
        locals: {
            currentUser: {
                organizationId: '5',
            },
        },
        writable: true,
        send() {},
        json() {},
        sendStatus() {},
        apiResponse() {},
    },
};
