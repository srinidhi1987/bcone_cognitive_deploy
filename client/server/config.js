/**
 * Created by superman on 11/30/16.
 */

module.exports = Object.freeze({
    // available backend services
    gateway: {
        endpoint: `http://${process.env.API_GATEWAY_URL}:${process.env.API_GATEWAY_PORT || 80}`,
        username: process.env.API_GATEWAY_USERNAME,
    },
    basePath: `/${process.env.IQBOT_BASE_PATH}/`.replace(/(^\/+|\/+$)/g, '/'), // Ensure slash at start and end of base path
    controlRoom: {
        url: '',
        routingName: '',
        auth: {
            endpoint: '',
            path: 'v1/authentication/',
        },
        userManagement: {
            endpoint: '',
            path: 'v1/usermanagement/',
        },
    },
    AUTH_TOKEN_HEADER: 'x-authorization',
    BEARER_TOKEN_HEADER: 'authorization',
    ORGANIZATION_ID: 1,
    CLIENT_COMPONENT_PUB_KEY: process.env.CLIENT_COMPONENT_PUB_KEY,
});
