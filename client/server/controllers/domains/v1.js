const Promise = require('bluebird');
const domainsService = require(`${global.appServices}/domain`);
const fs = require('fs');
const {isServerConnectionError} = require('../../utils/domains');

module.exports = {
    /**
     * Import raw unecrypted domain
     * @description This will be deprecated after 5.3
     * @param req
     * @param res
     * @param next
     */
    importUnencryptedDomain(req, res, next) {
        const file = req.files.domain;

        if (!file) return next(new Error('No domain file was provided'));
        // check for file type
        if (file.mimetype !== 'application/json') return next(new Error('Incorrect type file. Must provide a json domain file'));
        // read domain file
        const readFile = Promise.promisify(fs.readFile);

        return Promise.resolve()
            .then(() => readFile(file.file))
            .then((content) => {
                try {
                    const domain = JSON.parse(content);
                    return domainsService.sendUnencryptedDomain(res, domain);
                }
                catch (e) {
                    return Promise.reject(new Error('Error parsing domain file'));
                }
            })
            .then((result) => {
                return res.apiResponse(result);
            })
            .catch((err) => {
                next(err || new Error('Failed to import domain'));
            });
    },
    /**
     * Get all project resources
     */
    getDomains: (req, res, next) => {
        domainsService.getDomains(res, (err, domains) => {
            if (err) return next(err);
            if (!domains.success) return next(domains.errors);

            res.json(domains);
        });
    },
    importDomains(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        domainsService.importDomain(res, orgId, req.files.files)
            .then(({domainData: domain}) => {
                if (!domain.success) {
                    return next({errors: [domain.errors]});
                }
                try {
                    const parsedDomain = JSON.parse(domain.domainData);

                    res.json(parsedDomain);
                }
                catch (e) {
                    next(e);
                }
            })
            .catch((err) => {
                const errors = isServerConnectionError(err) ? ['server connection error'] : err;
                res.status(508);

                return next(errors);
            });
    },
    exportDomains(req, res, next) {
        domainsService.exportDomain(res, req.params.domainName.toLowerCase(), (err, domain) => {
            if (err) {
                const errors = isServerConnectionError(err) ? ['server connection error'] : err;

                res.status(508);
                return next(errors);
            }

            domainsService.writeDomainFiles(domain)
                .then((filepath) => {
                    const domainNameParsed = domain.name.replace(' ', '_').toLowerCase();

                    res.download(filepath, `${domainNameParsed}.dom`, (downloadErr) => {
                        if (downloadErr) return next(downloadErr);

                        fs.unlink(filepath, (unlinkErr) => {
                            if (unlinkErr) return next(unlinkErr);
                        });
                    });
                })
                .catch((err) => {
                    const errors = isServerConnectionError(err) ? ['server connection error'] : err;

                    res.status(508);
                    return next(errors);
                });
        });
    },
};
