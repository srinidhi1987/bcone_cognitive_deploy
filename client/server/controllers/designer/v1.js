/**
 * Copyright (c) 2017 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

const fs = require('fs');
const async = require('async');
const designerService = require(`${global.appServices}/designer`);

/**
 * create operation element for patch body
 * @param {string} op
 * @param {string} path
 * @param {string} value
 */
function patchOperation(op, path, value) {
    return {op, path, value};
}

module.exports = {
    /**
     * Save annotation file from body data
     *
     * @param {stream} req
     * @param {stream} res
     * @param {function} next
     */
    saveAnnotationFile(req, res, next) {
        const fields = JSON.parse(req.query.fields);
        const fileName = `${fields[0].filename.split('.')[0]}.json`;
        const fileLocation = `./${fileName}`;

        // create new file from body data
        fs.writeFile(fileLocation, JSON.stringify(fields, null, 2), 'utf8', (err) => {
            if (err) return next(err);

            // send file to client
            res.download(fileLocation, fileName, (err) => {
                if (err) return next(err);

                // remove tmp file after sending to client
                fs.unlink(fileLocation);
            });
        });
    },
    /**
     * suggest value for given label
     *
     * @param {stream} req
     * @param {stream} res
     * @param {function} next
     */
    suggestValueForLabel(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {
            categoryId,
            documentId,
            fieldDefId,
            fieldLayout,
            layoutId,
            projectId,
            visionBotId,
        } = req.body;

        designerService.suggestValueForLabel(res, orgId, projectId, categoryId, layoutId, visionBotId, documentId, fieldDefId, fieldLayout, (err, field, response) => {
            if (err) return next(err);
            if (!response.data || response.data.Text === '') return next(response);
            res.json(Object.assign({}, response, {success: true}));
        });
    },

    /**
     * update vision bot data (partial update)
     *
     * @param {stream} req
     * @param {stream} res
     * @param {function} next
     */
    updateVisionBotData(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {
            projectId,
            categoryId,
            layoutId,
            visionBotId,
            tables,
            fields,
            dataModelTables,
            dataModelFields,
            layoutIndex,
            validatorData,
        } = req.body;

        try {
            const patchBody = [
                patchOperation('replace', `/Layouts/${layoutIndex}/Fields`, fields),
                patchOperation('replace', `/Layouts/${layoutIndex}/Tables`, tables),
                patchOperation('replace', '/DataModel/Fields', dataModelFields),
                patchOperation('replace', '/DataModel/Tables', dataModelTables),
            ];
            const validationBody = {
                ValidatorData:validatorData,
            };
            async.parallel(
                {
                    visionBotSaveResult: (cb) => {
                        designerService.updateVisionBotData(res, orgId, projectId, categoryId, visionBotId, layoutId, patchBody, (err, result) => {
                            if (err) return cb(err);
                            if (!result.success) return cb(result.errors);

                            cb(null, result);
                        });
                    },
                    validationSaveResult: (cb) => {
                        designerService.saveValidationData(res, orgId, projectId, categoryId, visionBotId, validationBody, (err, result) => {
                            if (err) return cb(err);
                            if (!result.success) return cb(result.errors);

                            cb(null, result);
                        });
                    },
                },
                (err) => {
                    if (err) return next(err);

                    const result = {
                        success: true,
                        errors:[],
                        data: null,
                    };
                    res.json(result);
                }
            );
        }
        catch (e) {
            next(e);
        }
    },
    /**
     * get csv data for export
     *
     * @param {stream} req
     * @param {stream} res
     * @param {function} next
     */
    exportPreviewDocument(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        if (!req.body) res.end();
        const {
            categoryId,
            projectId,
            data,
        } = req.body;
        const body = {
            FieldDataRecord: {
                Fields: data.fields,
            },
            TableDataRecord: data.tables,
        };
        designerService.exportToCSV(res, orgId, projectId, categoryId, body, (err, csvData) => {
            if (err) return next(err);
            res.json(csvData);
        });
    },
    //download csv file for given data
    downloadCSV(req, res, next) {
        const {csvdata} = req.query;
        if (!csvdata) res.end();
        try {
            res.setHeader('Content-disposition', 'attachment; filename=preview.csv');
            res.setHeader('Content-type', 'text/csv;charset=utf-8');
            res.send(JSON.parse(csvdata));
        }
        catch (e) {
            next(e);
        }
    },
};
