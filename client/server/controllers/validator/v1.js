const async = require('async');
const validatorService = require(`${global.appServices}/validator`);
const fileService = require(`${global.appServices}/file`);
const distanceAPI = require('../../utils/levenshteinDistance');
const getArrayOfTruthyIds = require('../../utils/validator').getArrayOfTruthyIds;
const config = require('../../config');
const Promise = require('bluebird');

module.exports = {
    getDocumentToValidate(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {params} = req;
        const username = req.headers.username;
        async.waterfall([
            function getDocument(cb) {
                validatorService.getDocumentToValidate(res, orgId, params.projectId, username, (err, result) => {
                    if (err) return cb(err);
                    if (!result.success) return cb(result.errors);

                    const vBotDocument = JSON.parse(result.data.vBotDocument);
                    result.data.vBotDocument = vBotDocument;
                    cb(null, result);
                });
            },
            function getDocumentMetadata(previousResult, cb) {
                fileService.getFileMetaData(res, orgId, params.projectId, previousResult.data.fileId, username, (err, result) => {
                    if (err) return cb(err);
                    if (!result.success) return cb(result.errors);
                    const {classificationId, fileName} = result.data[0];
                    const finalResult = {
                        success: previousResult.success,
                        data: Object.assign({},
                            previousResult.data,
                            {
                                url:`${config.basePath}api/projects/${params.projectId}/categories/${classificationId}/documents/${previousResult.data.fileId}/pages/0/images?token=${res.locals.currentUser.authToken}`,
                                classificationId,
                                fileName,
                            }),
                        errors: previousResult.errors,
                    };
                    cb(null, finalResult);
                });
            },
            function getValidationData(previousResult, cb) {
                validatorService.getVisionBotValidationData(res, orgId, params.projectId, previousResult.data.classificationId, previousResult.data.visionBotId, username, (err, result) => {
                    if (err) return cb(err);
                    if (!result.success) return cb(result.errors);
                    const validationParseData = JSON.parse(result.data);
                    const finalResult = {
                        success: true,
                        data: Object.assign({},
                            previousResult.data,
                            {
                                validationData: validationParseData.ValidatorData,
                            }),
                        errors: [],
                    };
                    cb(null, finalResult);
                });
            },
            function getValidatorDocumentCount(previousResult, cb) {
                validatorService.getValidatorRemainingDocumentCount(res, orgId, params.projectId, username, (err, result) => {
                    if (err) return cb(err);
                    if (!result.success) return cb(result.errors);

                    const finalResult = {
                        success: true,
                        data: Object.assign({},
                            previousResult.data,
                            {
                                remainingDocumentReviewCount: result.data,
                            }),
                        errors: [],
                    };
                    cb(null, finalResult);
                });
            },
        ], (err, result) => {
            if (err) return next(err);

            res.json(result);
        });
    },
    saveValidatedDocument(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {params:{projectId, fileId}} = req;
        const username = req.headers.username;
        const {
            fields,
            tables,
            fieldAccuracyList: fieldsAccuracy,
        } = req.body;
        //determin levenshtein distance for all fields based on values given
        const fieldAccuracyList = fieldsAccuracy.map((field) => {
            return Object.assign({}, field, {
                passAccuracyValue: distanceAPI.getLevenshteinDistance(field.oldValue, field.newValue),
            });
        });
        const patchBody = [
            {
                op: 'replace',
                path: '/vBotDocument/Data/FieldDataRecord/Fields',
                value:  fields,
            },
            {
                op: 'replace',
                path: '/vBotDocument/Data/TableDataRecord',
                value: tables,
            },
            {
                op: 'replace',
                path: '/fieldAccuracyList',
                value: fieldAccuracyList,
            },
        ];
        validatorService.saveValidatedDocument(res, orgId, projectId, fileId, patchBody, username, (err, result) => {
            if (err) return next(err);

            res.json(result);
        });
    },
    markAsInvalid(req, res, next) {
        return Promise.resolve()
            .then(() => {
                const orgId = res.locals.currentUser.organizationId;
                const {fileId, projectId} = req.params;
                const {username} = req.headers;

                const arrayOfReasons = getArrayOfTruthyIds(req.body);

                return validatorService.markAsInvalid(res, orgId, projectId, fileId, arrayOfReasons, username);
            })
            .then((result) => {
                return res.apiResponse(result);
            })
            .catch((err) => {
                next(err || new Error('Failure to mark as invalid'));
            });
    },
};
