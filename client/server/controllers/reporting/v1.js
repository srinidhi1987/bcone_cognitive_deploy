/**
 * Created by iamcutler on 11/21/16.
 */

const reportingService = require(`${global.appServices}/reporting`);

module.exports = {
    /**
     * Get project reporting totals
     */
    getProjectTotalsById(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        reportingService.getPerformanceReportByProjectId(res, orgId, req.params.projectId, (err, report) => {
            if (err) return next(err);

            res.json(report);
        });
    },
    /**
     * Get all project resources
     */
    getProjectTotals(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        reportingService.getDashboardTotals(res, orgId, (err, totals) => {
            if (err) return next(err);

            res.json(totals);
        });
    },

    /**
     * Get projects
     */
    getProjects(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        reportingService.getProjects(res, orgId, (err, response) => {
            if (err) return next(err);

            res.json(response);
        });
    },
};
