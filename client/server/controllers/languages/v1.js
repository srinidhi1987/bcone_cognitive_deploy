const languageService = require(`${global.appServices}/language`);

module.exports = {
    /**
     * Get all language resources
     */
    get(req, res, next) {
        languageService.get(res, (err, languages) => {
            if (err) return next(err);
            if (languages.errors) return next(languages.errors);

            res.json(languages);
        });
    },
    getDomainLanguages(req, res, next) {
        languageService.getDomainLanguages(res, (err, result) => {
            if (err) return next(err);
            if (result.errors) return next(result.errors);

            res.json(result);
        });
    },
};
