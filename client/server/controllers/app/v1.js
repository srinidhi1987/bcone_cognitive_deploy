/**
 * Application v1 Controller
 */
const config = require('../../config');
const NodeRSA = require('node-rsa');
const fs = require('fs');
const dateUtil = require('../../utils/dates');
const serverUtils = require('../../utils/server');

module.exports = {
    /**
     * Pass encrypted data to client
     *
     * @api {get} /app-config Get app configuration
     * @apiVersion 1.0.0
     * @apiGroup Configuration
     */
    appConfiguration(req, res) {
        const username = req.headers.username;

        const loadbalancer = {
            protocol: process.env.RUN_LB_WITH_SSL === 'true' ? 'https://' : 'http://',
            lbhost: process.env.API_LOADBALANCER_URL,
            lbport: process.env.API_LOADBALANCER_PORT,
        };

        // check if username header was passed
        if (!username) return res.apiResponse(null, ['No username provided'], false, 400);

        // Node private key and client component public key
        const key = new NodeRSA(fs.readFileSync(config.CLIENT_COMPONENT_PUB_KEY));
        key.setOptions({encryptionScheme: 'pkcs1'});
        const msg = `${loadbalancer.protocol}${loadbalancer.lbhost}:${loadbalancer.lbport}\\${config.ORGANIZATION_ID}\\${username}`;
        const configuration = {
            encrypted: key.encrypt(msg, 'base64'), // auth info for thick client
            designer: process.env.USE_WEB_DESIGNER === 'true',
            validator: process.env.USE_WEB_VALIDATOR === 'true',
        };

        res.apiResponse(configuration);
    },

    /**
     * Platform health check
     * @param {Object} req
     * @param {Object} res
     */
    healthCheck(req, res) {
        return serverUtils.getPackage()
            .then((pkg) => {
                res.send(
                    'Application: Client Gateway\n' +
                    'Status: OK\n' +
                    `Version: ${pkg.version}\n` +
                    `Branch: ${pkg.branchName ? pkg.branchName : 'N/A'}\n` +
                    `GIT #: ${pkg.commitHash ? pkg.commitHash : 'N/A'}\n` +
                    `Build Time: ${pkg.buildTime ? pkg.buildTime : 'N/A'}\n` +
                    `Application uptime: ${dateUtil.toHHMMSS(Math.floor(process.uptime()))}`
                );
            })
            .catch(() => {
                res.sendStatus(500);
            });
    },
};
