/**
 * Created by iamcutler on 11/21/16.
 */
const apiClient = require('../../utils/apiClient');
const config = require('../../config');

module.exports = {
    /**
     * Get all project resources
     */
    getProcessingStatusByProject(req, res) {
        const qs = {};

        if (req.query.requestid) {
            qs.requestid = req.query.requestid;
        }

        apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.params.projectId}/filestatus`,
            qs,
            json: true,
        }, (err, response, body) => {
            res.json(body);
        });
    },
};
