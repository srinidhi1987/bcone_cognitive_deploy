/**
 * Created by iamcutler on 11/21/16.
 */
const apiClient = require('../../utils/apiClient');
const apiUtils = require('../../utils/requestHandlers');
const botService = require(`${global.appServices}/bot`);
const config = require('../../config');
const Promise = require('bluebird');

// const async = require('async');
// const fs = require('fs');

module.exports = {
    /**
     * Get all project bots resources
     */
    get(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        botService.getOrganizationVisionBots(res, orgId, (err, bots) => {
            if (err) return next(err);
            if (!bots.success) return next(bots.errors);

            res.apiResponse(bots.data);
        });
    },
    editVisionBot(req, res, next) {
        const {headers: {username}, params} = req;
        const orgId = res.locals.currentUser.organizationId;

        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${params.projectId}/categories/${params.categoryId}/visionbots/Edit`,
            headers: {username},
        }, (err, response, bot) => {
            if (err) return next(err);
            try {
                const parsedResponse = JSON.parse(bot);
                const visionBotData = JSON.parse(parsedResponse.data.visionBotData);

                res.json({visionBotData, url: `${config.basePath}api/projects/${params.projectId}/categories/${params.categoryId}/documents/${visionBotData.Layouts[0].DocProperties.Id}/pages/0/images?token=${res.locals.currentUser.authToken}`});
            }
            catch (e) {
                next(e);
            }
        });
    },
    /**
     * Get bot environment state (staging or production)
     * @param req
     * @param {string} req.body.state
     */
    setEnvironmentState(req, res, next) {
        const {params} = req;
        const orgId = res.locals.currentUser.organizationId;

        apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${params.projectId}/categories/${params.categoryId}/visionbots/${params.botId}/state`,
            json: req.body,
        }, (err, response, bot) => {
            if (err) return next(err);
            const hasErrors = apiUtils.handleApiException(response.statusCode, bot.errors);
            if (hasErrors) return next(hasErrors);
            res.json(bot);
        });
    },
    /**
     * Run vision bot once
     * @param req
     * @param res
     * @param next
     */
    runVisionBot(req, res, next) {
        botService.runVisionBot(
            res,
            res.locals.currentUser.organizationId,
            req.params.projectId,
            req.params.categoryId,
            req.params.botId,
            req.query.environment,
            (err, result) => {
                if (err) return next(err);

                res.json(result);
            }
        );
    },
    previewVisionBot(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {botId, categoryId, projectId, layoutId, pageIndex} = req.params;

        botService.getBotPreview(res, orgId, projectId, categoryId, botId, layoutId, pageIndex, (err, bot) => {
            if (err) return next(err);
            try {
                const data = JSON.parse(bot.data);
                res.json({data});
            }
            catch (e) {
                next(e);
            }
        });
    },
    getVisionBotPreviewDocuments(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {categoryId, projectId} = req.params;

        botService.getBotPreviewDocuments(res, orgId, projectId, categoryId, (err, docData) => {
            if (err) return next(err);

            res.json(docData);
        });
    },
    previewVisionBotDocument(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const {botId, categoryId, projectId, documentId} = req.params;

        botService.getBotPreviewDocument(res, orgId, projectId, categoryId, botId, documentId, (err, bot) => {
            if (err) return next(err);
            try {
                res.json({data: JSON.parse(bot.data)});
            }
            catch (e) {
                next(e);
            }
        });
    },
    keepDesignerAlive(req, res, next) {
        return Promise.resolve()
            .then(() => {
                const orgId = res.locals.currentUser.organizationId;
                const {categoryId, projectId, visionBotId} = req.params;

                return botService.keepAlive(res, orgId, projectId, categoryId, visionBotId);
            })
            .then((result) => {
                return res.apiResponse(result);
            })
            .catch((err) => {
                next(err || new Error('Failure to keep alive'));
            });
    },
};
