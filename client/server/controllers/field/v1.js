/**
 * Created by iamcutler on 11/29/16.
 */

/* eslint-disable */

const config = require('../../config');
const fieldService = require(`${global.appServices}/field`);

module.exports = {
    /**
     * Get field metadata
     */
     getField(req, res, next) {
         fieldService.getField(res, req.params.fieldId, req.query, (err, field) => {
             if (err) return next(err);
             if (!field.success) return next(field.errors);
             res.json(field);
         });
     },
};
