/**
 * Authentication v1 Controller
 */
const config = require('../../config');
const apiClient = require('../../utils/apiClient');
const apiUtils = require('../../utils/requestHandlers');

module.exports = {
    /**
     * Refresh auth token and fetch updated user details
     */
    currentUser(req, res, next) {
        apiClient.get(res, {
            url: `${config.controlRoom.userManagement.endpoint}users/self`,
        }, (err, response, userData) => {
            if (err) return next(err);
            const hasErrors = apiUtils.handleApiException(response.statusCode, userData.errors);
            if (hasErrors) return next(hasErrors);
            res.json(JSON.parse(userData));
        });
    },
    refreshToken(req, res, next) {
        apiClient.post(res, {
            url: `${config.controlRoom.auth.endpoint}token`,
            json: {
                token: res.locals.currentUser.authToken,
            },
        }, (err, response, tokenData) => {
            if (err) return next(err);
            const hasErrors = apiUtils.handleApiException(response.statusCode, tokenData.errors || tokenData.resultCode);
            if (hasErrors) return next(hasErrors);
            if (tokenData && tokenData.token && tokenData.user) {
                tokenData.success = true;
            }
            res.json(tokenData);
        });

    },
    /**
     * logout controller
     * @param {object} req
     * @param {object} res
     * @param {Function} next
     */
    logout(req, res, next) {
        apiClient.post(res, {
            url: `${config.controlRoom.auth.endpoint}/logout`,
        }, (err) => {
            if (err) return next(err);
            res.json({success: true});
        });
    },
};
