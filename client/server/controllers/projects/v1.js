/**
 * Created by iamcutler on 11/21/16.
 */
const apiClient = require('../../utils/apiClient');
const apiUtils = require('../../utils/requestHandlers');
const async = require('async');
const config = require('../../config');
const botService = require(`${global.appServices}/bot`);
const projectService = require(`${global.appServices}/project`);
const MIME_TYPES = require('../../enums/mimeTypes');
const resultFilter = require('../../utils/resultFilter');

module.exports = {
    /**
     * Get all project resources
     */
    get(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        projectService.get(res, orgId, req.query, (err, projects) => {
            if (err) return next(err);
            if (!projects.success) return next(projects.errors);
            res.json(projects);
        });
    },

    /**
     * Get project by id
     */
    getById(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const projectId = req.params.id;

        projectService.getById(res, orgId, projectId, req.query, (err, project) => {
            if (err) return next(err);
            if (!project.success) return next(project.errors);

            res.json(project);
        });
    },

    /**
     * Get project metadata by id
     */
    getMetadataById(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const projectId = req.params.id;

        projectService.getMetadataById(res, orgId, projectId, req.query, (err, project) => {
            if (err) return next(err);
            if (!project.success) return next(project.errors);

            res.json(project);
        });
    },

    getClassificationReport(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const projectId = req.params.id;

        projectService.getClassificationReport(res, orgId, projectId, (err, report) => {
            if (err) return next(err);

            res.json(report);
        });
    },

    /**
     * Create new project
     */
    create(req, res, next) {
        const projectData = req.body;
        const orgId = res.locals.currentUser.organizationId;

        projectData.organizationId = orgId;
        projectData.environment = 'staging';
        projectData.projectState = 'training';

        apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects`,
            json: projectData,
        }, (err, response, project) => {
            if (err) return next(err);

            const hasErrors = apiUtils.handleApiException(response.statusCode, project.errors ? project.errors : ['Error Creating Learning instance']);
            if (hasErrors) return next(hasErrors);

            res.json(project);
        });
    },

    /**
     * Update project
     */
    update(req, res, next) {
        apiClient.put(res, {
            url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.body.id}`,
            json: req.body,
        }, (err, response, project) => {
            if (err) return next(err);
            if (!project.success) return next(new Error('Error updating project'));

            res.json(project);
        });
    },

    /**
     * Partial project update
     */
    patch(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        apiClient.patch(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${req.params.id}`,
            json: req.body,
            headers: {
                updatedBy: req.headers.updatedby,
            },
        }, (err, response, project) => {
            if (err) return next(err);
            if (!project) return new Error('Error updating project');

            const errors = apiUtils.handleApiException(response.statusCode, project.errors);
            if (errors) {
                return res.apiResponse(project.data, project.errors, project.success, response.statusCode);
            }
            res.json(project);
        });
    },

    /**
     * Remove project by id
     */
    remove(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        projectService.remove(res, orgId, req.params.id, {
            updatedBy: req.headers.updatedby,
        }, (err, result) => {
            if (err) return next(err);

            res.json(result);
        });
    },

    /**
     * get categories related to a specific project
     */
    getProjectCategories(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        const projectId = req.params.id;
        const query = req.query || {};

        projectService.getProjectCategories(res, orgId, projectId, query, (err, result) => {
            if (err) return next(err);

            if (!result || !result.data) return next(new Error('No category data received'));

            // THIS IS MY MOCK DATA CREATOR!!!
            result.data.categories = result.data.categories.map((category) => {
                const {lockedUserId} = category.visionBot || {};

                return Object.assign({}, category, {
                    visionBot: Object.assign({}, category.visionBot, {
                        lockedUserId: lockedUserId === '' || !lockedUserId ? null : lockedUserId,
                    }),
                });
            });

            res.json(result);
        });
    },

    /**
     * Get projects to validate
     */
    getValidations(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        async.parallel({
            validations(cb) {
                projectService.getValidations(res, orgId, {username:config.gateway.username}, (err, validations) => {
                    if (err) return cb(err);

                    if (validations.success) {
                        return cb(null, validations.data);
                    }

                    cb(validations.errors);
                });
            },
            projects(cb) {
                projectService.get(res, orgId, {}, (err, projects) => {
                    if (err) return cb(err);
                    if (!projects.success) return cb(projects.errors);

                    cb(null, projects.data);
                });
            },
            bots(cb) {
                botService.getOrganizationVisionBots(res, orgId, (err, bots) => {
                    if (err) return cb(err);
                    if (!bots.success) return cb(bots.errors);

                    cb(null, bots.data);
                });
            },
        }, (err, results) => {
            if (err) return next(err);
            const validations = results.validations || [];
            const projects = results.projects || [];
            const bots = results.bots || [];

            // transform projects array to object. Projects can be found by key (id)
            const projectsMapppedToIdKeyObject = projects.reduce((acc, project) => {
                acc[project.id] = project;
                return acc;
            }, {});

            // transform bots array to object. Array of associated bots can be found by key (projectId)
            const botsArraysMappedToProjectIdKeyObject = bots.reduce((acc, bot) => {
                if (acc[bot.projectId]) {
                    acc[bot.projectId].push(bot);
                }
                else {
                    acc[bot.projectId] = [bot];
                }
                return acc;
            }, {});

            //iterate through and extend validations with name and status properties
            const finalValidations = validations.map((validation) => {
                const project = projectsMapppedToIdKeyObject[validation.projectId];
                const projectBots = botsArraysMappedToProjectIdKeyObject[project.id];

                return (Object.assign({}, validation, {
                    name: project.name,
                    status: projectService.getValidationStatus(project, validation, projectBots),
                }));
            });
            res.apiResponse(finalValidations);

        });
    },

    /**
     * @description fetch project summary data from different endpoints and modal it for UI purpose.
     */
    getProjectDetailSummary(req, res, next) {
        async.parallel({
            classificationSummary(cb) {
                apiClient.get(res, {
                    url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.params.projectId}/classificationsummary`,
                    json: true,
                }, (err, response, body) => {
                    if (err) return cb(err);
                    cb(null, body);
                });
            },
            summary(cb) {
                apiClient.get(res, {
                    url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.params.projectId}/summary`,
                    json: true,
                }, (err, response, body) => {
                    if (err) return cb(err);
                    cb(null, body);
                });
            },
            productionSummary(cb) {
                apiClient.get(res, {
                    url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.params.projectId}/productionsummary`,
                    json: true,
                }, (err, response, body) => {
                    if (err) return cb(err);
                    cb(null, body);
                });
            },
        }, (err, {classificationSummary, summary, productionSummary}) => {
            if (err) return next(err);
            try {
                const classificationSummaryStagingData = classificationSummary.data.stagingFileSummary;
                const classificationSummaryProductionData = classificationSummary.data.productionFileSummary;
                const summaryBotDetailsData = summary.data.botDetails;
                const summaryStagingData = summary.data.stagingSummary;
                const productionSummaryData = productionSummary.data;
                const stagingSTP = classificationSummaryStagingData.totalFiles !== 0 ?
                    Math.floor(summaryStagingData.totalPassedFileCount * 100 / classificationSummaryStagingData.totalFiles) :
                    0;
                const productionSTP = classificationSummaryProductionData.totalFiles !== 0 ?
                    Math.floor(productionSummaryData.successFiles * 100 / classificationSummaryProductionData.totalFiles) :
                    0;
                const stagingTestedFilesPercentage = classificationSummaryStagingData.totalFiles !== 0 ?
                    Math.floor(summaryStagingData.totalTestedFileCount * 100 / classificationSummaryStagingData.totalFiles) :
                    0;
                const productionProcessedFilesPercentage = classificationSummaryProductionData.totalFiles !== 0 ?
                    Math.floor(productionSummaryData.processedFiles * 100 / classificationSummaryProductionData.totalFiles) :
                    0;
                const data = {
                    'stagingTotalGroups' : classificationSummaryStagingData.totalGroups,
                    'stagingTotalBots' : summaryBotDetailsData.numberOfBotsCreated,
                    'stagingDocumentsUnclassified' : classificationSummaryStagingData.documentsUnclassified,
                    'stagingSuccessFiles' : summaryStagingData.totalPassedFileCount,
                    'stagingFailedFiles' : summaryStagingData.totalFailedFileCount,
                    'stagingAccuracy' : summaryStagingData.accuracy,
                    'stagingTestedFiles' : summaryStagingData.totalTestedFileCount,
                    'stagingTotalFiles' : classificationSummaryStagingData.totalFiles,
                    stagingSTP,
                    'productionTotalGroups' : classificationSummaryProductionData.totalGroups,
                    'productionTotalBots' : summaryBotDetailsData.numberOfProductionBots,
                    'productionDocumentsUnclassified' : classificationSummaryProductionData.documentsUnclassified,
                    'productionSuccessFiles' : productionSummaryData.successFiles,
                    'productionReviewFiles' : productionSummaryData.reviewFiles,
                    'productionInvalidFiles' : productionSummaryData.invalidFiles,
                    'productionAccuracy' : productionSummaryData.accuracy,
                    'productionProcessedFiles' : productionSummaryData.processedFiles,
                    'productionTotalFiles' : classificationSummaryProductionData.totalFiles,
                    productionSTP,
                    'pendingForReview' : productionSummaryData.pendingForReview,
                    stagingTestedFilesPercentage,
                    productionProcessedFilesPercentage,
                };
                const summaryResult = {
                    success : true,
                    data,
                    errors: [],
                };
                res.json(summaryResult);
            }
            catch (e) {
                const summaryResult = {
                    success : false,
                    data: null,
                    errors: ['Error in fetching project summary data.'],
                };
                res.json(summaryResult);
            }
        });
    },
    getImageData(req, res, next) {
        const {projectId, documentId, categoryId, pageId} = req.params;
        const orgId = res.locals.currentUser.organizationId;

        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/documents/${documentId}/pages/${pageId}/image`,
        }, (err, response, fileBlobResponse) => {
            if (err) return next(err);

            try {
                // parse the response
                const parsedResponse = JSON.parse(fileBlobResponse);
                const parsedData = JSON.parse(parsedResponse.data);

                const buffer = new Buffer(parsedData.$value, 'base64');

                writeImageDataToResponse(err, buffer);

            }
            catch (e) {
                next(e);
            }
        });

        function writeImageDataToResponse(err, data) {
            try {
                if (err) return next(err);
                res.writeHead(200, {'Content-Type': MIME_TYPES.png});
                res.end(data, 'binary');

            }
            catch (e) {
                return next(e);
            }
        }
    },
    // initiate exporting of selected projects.
    exportProjects(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/export/start`,
            json: req.body,
        }, (err, response, result) => {
            if (err) return next(err);
            if (!result.success) {
                res.json(resultFilter.applyExportImportErrorFilter(response.statusCode, result, 'Error exporting projects'));
            }
            else res.json(result);
        });
    },
    // fetch migration status
    getMigrationStatus(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/tasks/status`,
            json: true,
        }, (err, response, result) => {
            if (err) return next(err);
            if (!result.success) return next(new Error('Error getting migration status'));
            const filteredResult = {
                success: result.success,
                data: result.data.find((task) => task.taskType === 'exports' || task.taskType === 'imports') || {},
                errors: result.errors,
            };
            res.json(filteredResult);
        });
    },

    // initiate exporting of selected projects.
    importProjects(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/import/start`,
            json: req.body,
        }, (err, response, result) => {
            if (err) return next(err);
            if (!result.success) {
                res.json(resultFilter.applyExportImportErrorFilter(response.statusCode, result, 'Error importing projects'));
            }
            else res.json(result);
        });
    },

    // fetch archive file list
    getArchiveDetails(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/import/archivedetails`,
            json: true,
        }, (err, response, result) => {
            if (err) return next(err);
            if (!result.success) return next(new Error('Error getting archive details'));
            res.json(result);
        });
    },

    getLastMigrationInfo(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;
        apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/tasks?sort=endTime&order=desc`,
            json: true,
        }, (err, response, result) => {
            if (err)
                return next(err);
            if (!result.success || !Array.isArray(result.data))
                return next(new Error('Error getting latest import details'));

            res.apiResponse(result.data[0]);
        });
    },
};
