/**
 * Created by iamcutler on 11/29/16.
 */

/* eslint-disable */

const apiClient = require('../../utils/apiClient');
const config = require('../../config');

module.exports = {
    /**
     * Get project fields
     */
    get(req, res, next) {
        apiClient.get(res, {
            url: `${config.gateway.endpoint}/projecttypes`,
            json: true,
        }, (err, response, fields) => {
            if (err) return next(err);

            res.json(fields);
        });
    },
};
