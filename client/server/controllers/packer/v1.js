/**
 * Created by iamcutler on 1/12/17.
 */
const apiClient = require('../../utils/apiClient');
const config = require('../../config');

/**
 * Packer controller
 */
module.exports = {
    /**
     * Download validator
     * @param req
     * @param res
     */
    launchValidator(req, res) {
        apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.params.projectId}/validator`,
        }, () => {}).pipe(res);
    },

    /**
     * Download designer
     * @param req
     * @param res
     */
    launchDesigner(req, res) {
        apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${res.locals.currentUser.organizationId}/projects/${req.params.projectId}/categories/${req.params.categoryId}/designer`,
        }, () => {}).pipe(res);
    },
};

