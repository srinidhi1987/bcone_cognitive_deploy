/**
 * Created by iamcutler on 12/5/16.
 */
const fileService = require(`${global.appServices}/file`);

/**
 * Project files controller
 */

module.exports = {
    /**
     * Get all project files
     * @param req
     * @param res
     */
    get(req, res) {
        const orgId = res.locals.currentUser.organizationId;

        fileService.getByProject(res, orgId, req.params.id, (err, response) => {
            if (err) return res.sendStatus(500);

            return res.json(response);
        });
    },
    /**
     * Upload files
     * @param req
     * @param res
     * @param next
     */
    upload(req, res, next) {
        if (!req.files) return res.apiResponse();
        const orgId = res.locals.currentUser.organizationId;
        fileService.upload(res, orgId, req.params.id, req.files.files, (err, response) => {
            if (err) return next(err);

            res.apiResponse(response);
        });
    },
    /**
     * Check file upload status by transaction id
     * @param req
     * @param res
     * @param next
     */
    checkUploadStatus(req, res, next) {
        const orgId = res.locals.currentUser.organizationId;

        fileService.checkUploadStatus(res, orgId, req.params.id, req.params.transactionId, (err, response) => {
            if (err) return next(err);

            res.json(response);
        });
    },
};
