/**
 * Created by iamcutler on 12/5/16.
 */

const apiClient = require('../utils/apiClient');
const config = require('../config');
const cuid = require('cuid');
const fs = require('fs');
const path = require('path');

module.exports = {
    /**
     * Get files by project id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {generalCallback} cb
     */
    getByProject(res, orgId, projectId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/files`,
            json: true,
        }, (err, response, project) => {
            if (err) return cb(err);

            cb(null, project);
        });
    },

    /**
     * Get files by project id and category id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} categoryId
     * @param {generalCallback} cb
     */
    getByProjectAndCategory(res, orgId, projectId, categoryId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/files`,
            json: true,
        }, (err, response, project) => {
            if (err) return cb(err);

            cb(null, project);
        });
    },

    /**
     * Upload file to server
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} transactionId - used to track transaction of file uploads
     * @param {object} file
     * @param {object} file.file - location of temp file
     * @param {object} file.filename - name of the file
     * @param {generalCallback} cb
     */
    uploadFile(res, orgId, projectId, transactionId = null, file, cb) {
        return apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/files/upload/0`,
            qs: {
                requestid: transactionId,
            },
            formData: {
                data: {
                    value: fs.createReadStream(file.file),
                    options: {
                        filename: file.filename,
                    },
                },
            },
        }, () => {
            if (cb && typeof cb === 'function') cb(file);
        });
    },

    /**
     * Get files by project id and category id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {array} files
     * @param {generalCallback} cb
     */
    upload(res, orgId, projectId, files, cb) {
        try {
            const fileTransactionId = cuid(); // set transaction id to track status

            if (Array.isArray(files)) {
                files.map((file) => this.uploadFile(res, orgId, projectId, fileTransactionId, file, this.deleteTempFileAfterUpload));
            }
            else {
                this.uploadFile(res, orgId, projectId, fileTransactionId, files, this.deleteTempFileAfterUpload);
            }

            // return callback
            cb(null, {
                fileTransactionId,
            });
        }
        catch (err) {
            cb(err);
        }
    },

    /**
     * Check the status of a running file upload transaction
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} transactionId - uuid of transaction
     * @param {generalCallback} cb
     */
    checkUploadStatus(res, orgId, projectId, transactionId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/filestatus`,
            qs: {
                requestid: transactionId,
            },
            json: true,
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },

    /**
     * deleteTempFileAfterUpload to delete file after upload
     * @param {object} file
     * @param {object} file.file - location of temp file
     * @param {object} file.filename - name of the file
     */
    deleteTempFileAfterUpload(file) {
        if (!file) return;
        const splitResult = file.file.split(path.sep);
        fs.unlink(file.file, (err) => {
            if (err) return;
            fs.rmdir(`./${splitResult[0]}/${splitResult[1]}/${splitResult[2]}`, (err) => {
                if (err) return;
                fs.rmdir(`./${splitResult[0]}/${splitResult[1]}`, (err) => {
                    if (err) return;
                });
            });
        });
    },
    /**
     *
     * @param {number} orgId
     * @param {string} projectId
     * @param {string} fileId
     * @param {string} username
     * @param {generalCallback} cb
     */
    getFileMetaData(res, orgId, projectId, fileId, username, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/files/${fileId}`,
            json: true,
            headers: {
                username,
            },
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },
};

/**
 * General callback
 * @callback generalCallback
 * @param {Error} error
 * @param {*} results
 */
