const config = require('../config');
const apiClient = require('../utils/apiClient');
const requestHeader = require('../enums/requestHeaders');

module.exports = {
    /**
     * Check the status of a running file upload transaction
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} transactionId - uuid of transaction
     * @param {generalCallback} cb
     */
    suggestValueForLabel(res, orgId, projectId, categoryId, layoutId, visionBotId, documentId, fieldDefId, fieldLayout, cb) {
        return apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${visionBotId}/layout/${layoutId}/fielddef/${fieldDefId}/documents/${documentId}/valuefield`,
            json: Object.assign({}, fieldLayout),
        }, (err, response, result) => {
            if (err) return cb(err);
            cb(null, response, result);
        });
    },
    /**
     * update the vision bot data (partial update)
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} categoryId
     * @param {string} visionBotId
     * @param {string} layoutId
     * @param {object} patchBody
     * @param {generalCallback} cb
     */
    updateVisionBotData(res, orgId, projectId, categoryId, visionBotId, layoutId, patchBody, cb) {
        return apiClient.patch(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${visionBotId}`,
            json: patchBody,
            headers: {
                'content-type': requestHeader.APPLICATION_JSON_PATCH_JSON,
                'accept': requestHeader.APPLICATION_JSON_PATCH_JSON,
            },
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },
    /**
     * save list validation type of data
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} categoryId
     * @param {string} visionBotId
     * @param {object} validatorData
     * @param {generalCallback} cb
     */
    saveValidationData(res, orgId, projectId, categoryId, visionBotId, validatorData, cb) {
        return apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${visionBotId}/validationdata`,
            json: validatorData,
            headers: {
                'content-type': requestHeader.APPLICATION_JSON,
                'accept': requestHeader.APPLICATION_JSON,
            },
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },
    /**
     * save list validation type of data
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} categoryId
     * @param {string} visionBotId
     * @param {object} validatorData
     * @param {Function} cb
     */
    exportToCSV(res, orgId, projectId, categoryId, data, cb) {
        return apiClient.post(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/vbotdata`,
            json: data,
            headers: {
                'content-type': requestHeader.APPLICATION_JSON,
                'accept': requestHeader.APPLICATION_JSON,
            },
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },
};
