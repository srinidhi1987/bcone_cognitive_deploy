/**
 * Created by iamcutler on 3/20/17.
 */

const apiClient = require('../utils/apiClient');
const config = require('../config');

module.exports = {
    /**
     * Get all supported languages
     * @param {generalCallback} cb
     */
    get(res, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/languages`,
            json: true,
        }, (err, response, langs) => {
            if (err) return cb(err);

            cb(null, langs, response);
        });
    },
    getDomainLanguages(res, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/domains/languages`,
            json: true,
        }, (err, response, langs) => {
            if (err) return cb(err);

            cb(null, langs, response);
        });
    },
};

/**
 * General API callback
 * @callback {generalCallback}
 * @param {Error} err
 * @param {Object} result
 */
