const config = require('../config');
const fs = require('fs-extra');
const fstream = require('fstream');
const archiver = require('archiver');
const cuid = require('cuid');
const unzip = require('unzip');
const path = require('path');
const apiClient = require('../utils/apiClient');

const TEMP_DIR = 'tmp';

module.exports = {
    /**
     * Send unencrypted sharable domain
     * @param res
     * @param {Object} domain
     */
    sendUnencryptedDomain(res, domain) {
        return new Promise((resolve, reject) => {
            try {
                return apiClient.post(res, {
                    url: `${config.gateway.endpoint}/domains`,
                    json: domain,
                }, (err, response, result) => {
                    if (err) return reject(err);
                    if (result && !result.success) return reject(result.errors);

                    resolve(result);
                });
            }
            catch (err) {
                reject(err);
            }
        });
    },
    /**
     * Get domains
     *
     * @param {generalCallback} cb
     */
    getDomains(res, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/projecttypes`,
            json: true,
        }, (err, response, results) => {
            if (err) return cb(err);

            cb(null, results, response);
        });
    },
    exportDomain(res, domainName, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/domains/${domainName}/export`,
            json: true,
        }, (err, response, results) => {
            if (err) return cb(err);
            cb(null, results, response);
        });
    },
    sendDomainToGateway(res, domain) {
        return new Promise((resolve, reject) => {
            try {
                return apiClient.post(res, {
                    url: `${config.gateway.endpoint}/domains/import`,
                    json: JSON.parse(domain.toString()),
                }, (err, response, result) => {
                    if (err || (result && !result.success)) {
                        return reject(err || result);
                    }

                    if (!result) {
                        resolve({success: true, domainData: domain});
                    }
                });
            }
            catch (err) {
                reject(err);
            }
        });
    },
    importDomain(res, orgId, files) {
        return new Promise((resolve, reject) => {
            try {
                const fileTransactionId = cuid(); // set transaction id to track status

                if (Array.isArray(files)) {
                    Promise.all(files.map((file) => this.uploadFile(res, orgId, fileTransactionId, file, this.deleteTempFileAfterUpload)))
                        .then((domains) => {
                            return Promise.all(domains.map((domain) => this.sendDomainToGateway(res, domain)));
                        })
                        .then((domainData) => {
                            return resolve({
                                domainData,
                                fileTransactionId,
                            });
                        })
                        .catch((err) => reject(err));
                }
                else {
                    this.uploadFile(res, orgId, fileTransactionId, files, this.deleteTempFileAfterUpload)
                        .then((domain) => {
                            return this.sendDomainToGateway(res, domain);
                        })
                        .then((domainData) => {
                            resolve({
                                domainData,
                                fileTransactionId,
                            });
                        })
                        .catch((err) => {
                            reject(err);
                        });
                }
            }
            catch (err) {
                reject(err);
            }
        });
    },
    writeDomainFiles(domain) {
        return new Promise((resolve, reject) => {
            // decode base64 encoded domain meta
            if (!domain || !domain.domain) return reject(domain || {error: 'doesnt exist'});

            // parse so we can get name
            const domainFileName = domain.name.toLowerCase().replace(' ', '_');

            //where we will hold our temp files until we download them
            const tempDirName = `tmp/${domainFileName}`;

            if (!fs.existsSync(TEMP_DIR)) {
                fs.mkdirSync(TEMP_DIR);
            }

            // make temp directory if it doesnt exist already
            if (!fs.existsSync(tempDirName)) {
                fs.mkdirSync(tempDirName);
            }
            //zip folder we send to client
            const zipFilePath = `tmp/${domainFileName}.zip`;

            // write temp domain file
            const domainFilePath = `${tempDirName}/domain.json`;
            fs.writeFileSync(domainFilePath, JSON.stringify(domain, 0, 2));

            // write temp manifest file with name and version
            const manifestFilePath = `${tempDirName}/manifest.json`;
            const {name, version} = domain;
            const stringifiedManifest = JSON.stringify({name, version}, 0, 2);

            fs.writeFileSync(manifestFilePath, stringifiedManifest);

            const output = fs.createWriteStream(zipFilePath);
            const archive = archiver('zip', {
                gzip: true,
                zlib: {level: 9}, // Sets the compression level.
            });


            archive.on('error', (err) => {
                throw err;
            });

            // once the archive is finalized and zip is complete resolve the file path
            output.on('finish', () => {
                // clean up files
                fs.remove(tempDirName);
                fs.remove(TEMP_DIR);

                resolve(zipFilePath);
            });

            // pipe archive data to the output file
            archive.pipe(output);

            // append files
            archive.file(manifestFilePath, {name: 'manifest.json'});
            archive.file(domainFilePath, {name: 'domain.json'});

            archive.finalize();
        });
    },
    /**
     * deleteTempFileAfterUpload to delete file after upload
     * @param {object} file
     * @param {object} file.file - location of temp file
     * @param {object} file.filename - name of the file
     */
    deleteTempFileAfterUpload(file) {
        if (!file) return;
        const splitResult = file.file.split(path.sep);
        fs.unlink(file.file, (err) => {
            if (err) return;
            fs.rmdir(`./${splitResult[0]}/${splitResult[1]}/${splitResult[2]}`, (err) => {
                if (err) return;
                fs.rmdir(`./${splitResult[0]}/${splitResult[1]}`, (err) => {
                    if (err) return;
                });
            });
        });
    },
    /**
     * Upload file to server
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} transactionId - used to track transaction of file uploads
     * @param {object} file
     * @param {object} file.file - location of temp file
     * @param {object} file.filename - name of the file
     * @param {generalCallback} cb
     */
    uploadFile(res, orgId, transactionId = null, file, unlinkFileAndFolder) {
        return new Promise((resolve, reject) => {
            const unzippedDirName = file.filename.replace('.dom', '');
            fs.renameSync(file.file, file.file.replace('.dom', '.zip'));
            if (!fs.existsSync(`${TEMP_DIR}/${unzippedDirName}`)) {
                fs.mkdirSync(`${TEMP_DIR}/${unzippedDirName}`);
            }
            const writeStream = fstream.Writer(`${TEMP_DIR}/${unzippedDirName}`); //eslint-disable-line

            fs.createReadStream(file.file.replace('.dom', '.zip'))
            .pipe(unzip.Parse()) //eslint-disable-line
            .pipe(writeStream)
            .on('close', () => {
                try {
                    // file names
                    const domainFile = `${TEMP_DIR}/${unzippedDirName}/domain.json`;
                    const manifestFile = `${TEMP_DIR}/${unzippedDirName}/manifest.json`;
                    // read files
                    const domainData = fs.readFileSync(domainFile, 'utf8');

                    unlinkFileAndFolder({file: file.file.replace('.dom', '.zip')});
                    fs.unlink(domainFile, (err) => {
                        if (err) return;
                        unlinkFileAndFolder({file: manifestFile});

                        if (fs.existsSync(`${TEMP_DIR}/${unzippedDirName}`)) {
                            fs.rmdir(`${TEMP_DIR}/${unzippedDirName}`, (rmdirErr) => {
                                if (rmdirErr) return;
                            });
                        }

                        resolve(domainData);
                    });
                }
                catch (err) {
                    reject(err);
                }
            });
        });
    },
};
