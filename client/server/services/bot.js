const config = require('../config');
const apiClient = require('../utils/apiClient');

module.exports = {
    /**
     * Get organization level vision bots
     *
     * @param {string} orgId
     * @param {generalCallback} cb
     */
    getOrganizationVisionBots(res, orgId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/visionbotdata`,
            json: true,
        }, (err, response, results) => {
            if (err) return cb(err);

            cb(null, results, response);
        });
    },
    /**
     * Get vision bot meta data
     *
     * @param {string} orgId
     * @param {generalCallback} cb
     */
    // TODO: This doesn't appear to be used anywhere
    getVisionBotMetaData(res, orgId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/visionbotmetadata`,
            json: true,
        }, (err, response, metadata) => {
            if (err) return cb(err);

            cb(null, metadata, response);
        });
    },

    /**
     * Run vision bot once
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {string} categoryId
     * @param {string} botId
     * @param {string} environment
     * @param {generalCallback} cb
     */
    runVisionBot(res, orgId, projectId, categoryId, botId, environment = 'staging', cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${botId}/mode/${environment}/run`,
            json: true,
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },

    getBotPreview(res, orgId, projectId, categoryId, botId, layoutId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${botId}/layout/${layoutId}/vbotdocument`,
            json: true,
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },

    getBotPreviewDocument(res, orgId, projectId, categoryId, botId, documentId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${botId}/documents/${documentId}/previewvbotdocument`,
            json: true,
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },

    getBotPreviewDocuments(res, orgId, projectId, categoryId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/files/staging`,
            json: true,
        }, (err, response, result) => {
            if (err) return cb(err);

            cb(null, result, response);
        });
    },
    keepAlive(res, orgId, projectId, categoryId, visionBotId) {
        return new Promise((resolve, reject) => {
            try {
                return apiClient.post(res, {
                    url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${visionBotId}/keepalive`,
                }, (err, response, result) => {
                    try {
                        const parsedResult = JSON.parse(result);

                        if (err || (parsedResult && !parsedResult.success)) {
                            return reject(err || parsedResult);
                        }

                        return resolve(parsedResult);
                    }
                    catch (e) {
                        reject(e);
                    }

                });
            }
            catch (err) {
                reject(err);
            }
        });
    },
};

/**
 * General callback
 * @callback generalCallback
 * @param {Error} error
 * @param {*} results
 */
