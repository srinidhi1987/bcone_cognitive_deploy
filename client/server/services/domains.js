const config = require('../config');
const Promise = require('bluebird');
const apiClient = require('../utils/apiClient');

module.exports = {
    /**
     * Send unencrypted sharable domain
     * @param res
     * @param {Object} domain
     */
    sendUnencryptedDomain(res, domain) {
        return new Promise((resolve, reject) => {
            try {
                return apiClient.post(res, {
                    url: `${config.gateway.endpoint}/domains`,
                    json: domain,
                }, (err, response, result) => {
                    if (err) return reject(err);
                    if (result && !result.success) return reject(result.errors);

                    resolve(result);
                });
            }
            catch (err) {
                reject(err);
            }
        });
    },
};
