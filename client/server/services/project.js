/**
 * Created by iamcutler on 12/7/16.
 */

const config = require('../config');
const apiUtils = require('../utils/requestHandlers');
const apiClient = require('../utils/apiClient');

module.exports = {
    /**
     * Get projects
     *
     * @param args
     * @param {string} args.orgId
     * @param {object} args.queryParams
     * @param {generalCb} args.cb
     */
    get(res, orgId, queryParams = {}, cb) {
        const qs = _hardCodeCategoryQueryParam(queryParams);

        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects`,
            json: true,
            qs,
        }, (err, response, projects) => {
            if (err) return cb(err);

            cb(null, projects, response);
        });
    },

    /**
     * Get project by id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {generalCb} cb
     */
    getById(res, orgId, projectId, queryParams = {}, cb) {
        const qs = _hardCodeCategoryQueryParam(queryParams);

        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}`,
            json: true,
            qs,
        }, (err, response, project) => {
            if (err) return cb(err);

            cb(apiUtils.handleApiException(response.statusCode, project.errors), project, response);
        });
    },

    /**
     * Get project by id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {generalCb} cb
     */
    getMetadataById(res, orgId, projectId, queryParams = {}, cb) {
        const qs = _hardCodeCategoryQueryParam(queryParams);

        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/metadata`,
            json: true,
            qs,
        }, (err, response, project) => {
            if (err) return cb(err);

            cb(apiUtils.handleApiException(response.statusCode, project.errors), project, response);
        });
    },

    /**
     * Remove project by id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {generalCb} cb
     */
    remove(res, orgId, projectId, headers = {}, cb) {
        return apiClient.del(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}`,
            headers,
            json: true,
        }, (err, response, project) => {
            if (err) return cb(err);

            cb(null, project, response);
        });
    },
    /**
     * Get Categories associated with a single project
     * @param {string} orgId
     * @param {string} projectId
     * @param {object} qs
     * @param {function} cb
     * @returns {promise}
     */
    getProjectCategories(res, orgId, projectId, qs, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories`,
            json: true,
            qs,
        }, (err, response, categories) => {
            if (err) return cb(err);

            cb(null, categories, response);
        });
    },

    /**
     * Get projects to be validated
     *
     * @param {string} orgId
     * @param {generalCb} cb
     */
    getValidations(res, orgId, headers = {}, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/validator/reviewedfilecount`,
            headers,
            json: true,
        }, (err, response, projects) => {
            if (err) return cb(err);
            if (typeof projects === 'string') return cb(new Error(projects));

            cb(null, projects);
        });
    },

    /**
     * Get project classification report
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {generalCb} cb
     */
    getClassificationReport(res, orgId, projectId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/report`,
            json: true,
        }, (err, response, report) => {
            if (err) return cb(err);

            cb(null, report, response);
        });
    },
    getValidationStatus(project, validation, bots = []) {

        const {markedForReviewCount, processedCount} = bots.reduce((acc, bot) => {
            if (bot.productionBotRunDetails && bot.productionBotRunDetails.validatorReviewDetails) {
                acc.processedCount = acc.processedCount + Number(bot.productionBotRunDetails.validatorReviewDetails.processedCount);
                acc.markedForReviewCount = acc.markedForReviewCount + Number(bot.productionBotRunDetails.validatorReviewDetails.failedFileCount);
            }
            return acc;
        }, {
            markedForReviewCount: 0,
            processedCount: 0,
        });

        if (markedForReviewCount === (validation.reviewFileCount + validation.invalidFileCount)) {
            return 'reviewed';
        }
        else if (markedForReviewCount > (validation.reviewFileCount + validation.invalidFileCount)) {
            return 'validating';
        }
        else if (processedCount < project.numberOfFiles) {
            return 'processing';
        }

        return 'validating';
    },
};

/**
 * _hardCodeCategoryQueryParam
 * Note: this is used due to not wanting to make breaking changes to Java Project API
 * Instead of changing API, new param excludeFields was added to remove categories from project call
 * It is essential that categories are removed until changes are made to the API
 * There could be thousands of categories and this will cause problems with frontend app
 * @param {object} queryParams
 * @returns {object}
 * @private
 */
function _hardCodeCategoryQueryParam(queryParams) {
    return Object.assign({}, queryParams, {excludeFields: 'categories'});
}
