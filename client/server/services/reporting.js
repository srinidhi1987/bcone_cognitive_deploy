/**
 * Created by iamcutler on 3/22/17.
 */

const apiClient = require('../utils/apiClient');
const config = require('../config');

module.exports = {
    /**
     * Get dashboard aggregated totals
     *
     * @param {string} orgId
     * @param {generalCallback} cb
     */
    getDashboardTotals(res, orgId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/reporting/projects/totals`,
            json: true,
        }, (err, response, totals) => {
            if (err) return cb(err);

            cb(null, totals, response);
        });
    },
    /**
     * Get performance report by project id
     *
     * @param {string} orgId
     * @param {string} projectId
     * @param {generalCallback} cb
     */
    getPerformanceReportByProjectId(res, orgId, projectId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/reporting/projects/${projectId}/totals`,
            json: true,
        }, (err, response, report) => {
            if (err) return cb(err);

            cb(null, report, response);
        });
    },
    /**
     * Get All project's statistic
     *
     * @param {string} orgId
     * @param {generalCallback} cb
     */
    getProjects(res, orgId, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/reporting/projects`,
            json: true,
        }, (err, response, data) => {
            if (err) return cb(err);

            cb(null, data, response);
        });
    },
};

/**
 * General node signature callback
 * @callback generalCallback
 * @param {Error} err
 * @param {object} result
 * @param {object} http response
 */
