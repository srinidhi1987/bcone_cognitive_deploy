const config = require('../config');
const requestHeader = require('../enums/requestHeaders');
const apiClient = require('../utils/apiClient');
module.exports = {
    /**
     *
     * @param {number} orgId
     * @param {string} projectId
     * @param {string} username
     * @param {generalCallback} cb
     */
    getDocumentToValidate(res, orgId, projectId, username, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/visionbot/`,
            json: true,
            headers: {
                username,
            },
        }, (err, response, results) => {
            if (err) return cb(err);

            cb(null, results, response);
        });
    },
    /**
     * get validation data for given visionbot
     * @param {number} orgId
     * @param {string} projectId
     * @param {number} categoryId
     * @param {string} visionBotId
     * @param {string} username
     * @param {generalCallback} cb
     */
    getVisionBotValidationData(res, orgId, projectId, categoryId, visionBotId, username, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/categories/${categoryId}/visionbots/${visionBotId}/validationdata`,
            json: true,
            headers: {
                username,
            },
        }, (err, response, results) => {
            if (err) return cb(err);

            cb(null, results, response);
        });
    },
    /**
     * get remaining documents count for given project id
     * @param {number} orgId
     * @param {string} projectId
     * @param {string} username
     * @param {generalCallback} cb
     */
    getValidatorRemainingDocumentCount(res, orgId, projectId, username, cb) {
        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/validator/projects/${projectId}`,
            json: true,
            headers: {
                username,
            },
        }, (err, response, results) => {
            if (err) return cb(err);

            cb(null, results, response);
        });
    },
    saveValidatedDocument(res, orgId, projectId, fileId, patchBody, username, cb) {
        return apiClient.patch(res, {
            url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projectId}/visionbot/${fileId}`,
            json: patchBody,
            headers: {
                username,
                'content-type': requestHeader.APPLICATION_JSON_PATCH_JSON,
                'accept': requestHeader.APPLICATION_JSON_PATCH_JSON,
            },
        }, (err, response, results) => {
            if (err) return cb(err);

            cb(null, results, response);
        });
    },
    markAsInvalid(res, orgId, projId, fileId, reasons, username) {
        return new Promise((resolve, reject) => {
            try {
                return apiClient.post(res, {
                    url: `${config.gateway.endpoint}/organizations/${orgId}/projects/${projId}/files/${fileId}/invalidate`,
                    json: reasons,
                    headers: {username},
                }, (err, response, result) => {
                    if (err) return reject(err);
                    if (result && !result.success) return reject(result.errors);

                    resolve(result);
                });
            }
            catch (err) {
                reject(err);
            }
        });
    },
};
