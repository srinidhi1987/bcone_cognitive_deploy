const apiClient = require('../utils/apiClient');
const config = require('../config');

module.exports = {
    /**
    * @param args
    * @param {string} args.fieldId
    * @param {object} args.queryParams
    * @param {generalCb} args.cb
    */
    getField(res, fieldId, queryParams = {}, cb) {
        const qs = _hardCodeCategoryQueryParam(queryParams);

        return apiClient.get(res, {
            url: `${config.gateway.endpoint}/fields/${fieldId}`,
            json: true,
            qs,
        }, (err, response, field) => {
            if (err) return cb(err);

            cb(null, field, response);
        });
    },
};

/**
 * _hardCodeCategoryQueryParam
 * Note: this is used due to not wanting to make breaking changes to Java Project API
 * Instead of changing API, new param excludeFields was added to remove categories from project call
 * It is essential that categories are removed until changes are made to the API
 * There could be thousands of categories and this will cause problems with frontend app
 * @param {object} queryParams
 * @returns {object}
 * @private
 */
function _hardCodeCategoryQueryParam(queryParams) {
    return Object.assign({}, queryParams, {excludeFields: 'categories'});
}
