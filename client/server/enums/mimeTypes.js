const IMAGE_FORMATS = require('./imageFormats');

module.exports = {
    [IMAGE_FORMATS.jpeg]: 'image/jpeg',
    [IMAGE_FORMATS.jpg]: 'image/jpeg',
    [IMAGE_FORMATS.png]: 'image/png',
    [IMAGE_FORMATS.tiff]: 'image/tiff',
    [IMAGE_FORMATS.tif]: 'image/tiff',
};
