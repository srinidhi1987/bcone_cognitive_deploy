module.exports = {
    jpeg: 'jpeg',
    jpg: 'jpg',
    png: 'png',
    tiff: 'tiff',
    tif: 'tif',
};
