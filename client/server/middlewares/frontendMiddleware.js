/* eslint-disable global-require */
const express = require('express');
const path = require('path');
const compression = require('compression');
const pkg = require(path.resolve(process.cwd(), 'package.json'));
const config = require('../config');
const logger = require('../logger');
const request = require('../utils/request');

// Dev middleware
const addDevMiddlewares = (app, webpackConfig) => {
    const webpack = require('webpack');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const compiler = webpack(webpackConfig);
    const middleware = webpackDevMiddleware(compiler, {
        noInfo: true,
        publicPath: webpackConfig.output.publicPath,
        silent: true,
        stats: 'errors-only',
    });

    // Since webpackDevMiddleware uses memory-fs internally to store build
    // artifacts, we use it instead
    const fs = middleware.fileSystem;

    const homeRoute = homeRouteHandler.bind(null, fs, compiler.outputPath, config);
    app.get('/', homeRoute);
    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));

    if (pkg.dllPlugin) {
        app.get(/\.dll\.js$/, (req, res) => {
            const filename = req.path.replace(/^\//, '');
            res.sendFile(path.join(process.cwd(), pkg.dllPlugin.path, filename));
        });
    }

    app.get('*', homeRoute);
};

// Production middlewares
const addProdMiddlewares = (app, options) => {
    const fs = require('fs');
    const publicPath = options.publicPath || '/';
    const outputPath = options.outputPath || path.resolve(process.cwd(), 'www');

    // compression middleware compresses your server responses which makes them
    // smaller (applies also to assets). You can read more about that technique
    // and other good practices on official Express.js docs http://mxs.is/googmy
    app.use(compression());

    const homeRoute = homeRouteHandler.bind(null, fs, outputPath, config);
    app.get('/', homeRoute);
    app.use(publicPath, express.static(outputPath));
    app.use(config.basePath, express.static(outputPath));

    app.get('*', homeRoute);
};

function homeRouteHandler(fs, outputPath, config, req, res) {
    getControlRoomConfiguration(req, res, () => {
        fs.readFile(path.resolve(outputPath, 'index.html'), (err, file) => {
            if (err) {
                res.sendStatus(404);
            }
            else {
                res.send(file.toString()
                    .replace(/\{\{IQ_BOT_BASE_PATH\}\}/g, config.basePath)
                    .replace(/\{\{CONTROL_ROOM_URL\}\}/g, config.controlRoom.url)
                );
            }
        });
    });
}

/**
 * Get Control Room registration configuration if we don't have it already
 */
function getControlRoomConfiguration(req, res, next) {
    if (config.controlRoom.url) {
        return next();
    }

    // configuration is missing, so we update before continuing
    request.get({
        url: `${config.gateway.endpoint}/configuration`,
    }, (err, response, crConfigJSON) => {
        try {
            if (err) {
                throw err;
            }
            const crConfig = JSON.parse(crConfigJSON);
            config.controlRoom.url = (process.env.CONTROL_ROOM_URL || crConfig.crUrl || '').replace(/\/$/, ''); // Strip any slash at end of the url;
            config.controlRoom.routingName = crConfig.routingName || '';
            config.controlRoom.auth.endpoint = `${config.controlRoom.url}/${config.controlRoom.auth.path}`;
            config.controlRoom.userManagement.endpoint = `${config.controlRoom.url}/${config.controlRoom.userManagement.path}`;
        }
        catch (err) {
            logger.error(err);
        }
        next();
    });
}

/**
 * Front-end middleware
 */
module.exports = (app, options) => {
    const isProd = process.env.NODE_ENV === 'production';

    if (isProd) {
        addProdMiddlewares(app, options);
    }
    else {
        const webpackConfig = require('../../internals/webpack/webpack.dev.babel');
        addDevMiddlewares(app, webpackConfig);
    }

    return app;
};
