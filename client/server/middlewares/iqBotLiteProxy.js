const config = require('../config');
const proxy = require('express-http-proxy');

const isMultipartRequest = (req) => {
    const contentTypeHeader = req.headers['content-type'];
    return contentTypeHeader && contentTypeHeader.indexOf('multipart') > -1;
};
/**
 * IQ Bot lite command proxy API - passes requests directly to the gateway api
 * Used by IQ Bot Lite Command (Called from AAE Client)
 */
exports.iqBotLiteProxy = () => {
    return (req, res, next) => {
        const parseReqBody = !isMultipartRequest(req);
        return proxy(config.gateway.endpoint, {
            proxyReqOptDecorator: (proxyReqOpts) => {
                proxyReqOpts.headers.connection = 'keep-alive';
                return proxyReqOpts;
            },
            proxyReqPathResolver: (req) => {
                return `${req.originalUrl}`;
            },
            preserveHostHdr: true,
            parseReqBody,
            timeout: 20000,
        })(req, res, next);
    };
};
