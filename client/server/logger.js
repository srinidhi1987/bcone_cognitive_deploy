/* eslint-disable no-console */

const chalk = require('chalk');
const ip = require('ip');
const logger = require('./utils/logger');

const divider = chalk.gray('\n-----------------------------------');

/**
 * Logger middleware, you can customize it to make messages more personal
 */
const serverLogger = {

    // Called whenever there's an error on the server we want to print
    error: (err) => {
        logger.error(chalk.red(err));
    },

    // Called when express.js app starts on given port w/o errors
    appStarted: (port, tunnelStarted, protocol = 'http') => {
        logger.info(`Server (${protocol}) started ${chalk.green('✓')}`);

        // If the tunnel started, log that and the URL it's available at
        if (tunnelStarted) {
            console.log(`Tunnel initialised ${chalk.green('✓')}`);
        }

        logger.info(`
${chalk.bold('Access URLs:')}${divider}
Localhost: ${chalk.magenta(`${protocol}://localhost:${port}`)}
      LAN: ${chalk.magenta(`${protocol}://${ip.address()}:${port}`) +
        (tunnelStarted ? `\n    Proxy: ${chalk.magenta(tunnelStarted)}` : '')}${divider}
${chalk.blue(`Press ${chalk.italic('CTRL-C')} to stop`)}
    `);
    },
};

module.exports = serverLogger;
