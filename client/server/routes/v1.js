/**
 * API Version 1 Routes
 */
const router = require('express').Router(); // eslint-disable-line new-cap
const apiVersion = 'v1'; // routing api version

/**
 * Import controllers / logic
 */
const appController = require(`${global.appControllers}/app/${apiVersion}`);
const authController = require(`${global.appControllers}/authentication/${apiVersion}`);
const botsController = require(`${global.appControllers}/bots/${apiVersion}`);
const designerController = require(`${global.appControllers}/designer/${apiVersion}`);
const fieldController = require(`${global.appControllers}/field/${apiVersion}`);
const filesController = require(`${global.appControllers}/files/${apiVersion}`);
const projectsController = require(`${global.appControllers}/projects/${apiVersion}`);
const projectFieldsController = require(`${global.appControllers}/project-fields/${apiVersion}`);
const classificationController = require(`${global.appControllers}/classification/${apiVersion}`);
const packerContainer = require(`${global.appControllers}/packer/${apiVersion}`);
const reportingController = require(`${global.appControllers}/reporting/${apiVersion}`);
const languagesController = require(`${global.appControllers}/languages/${apiVersion}`);
const validatorController = require(`${global.appControllers}/validator/${apiVersion}`);
const domainsController = require(`${global.appControllers}/domains/${apiVersion}`);

/**
 * Routes go below
 */

router.get('/app-config', appController.appConfiguration);

// Web Designer
router.get('/designer/save', designerController.saveAnnotationFile);
router.post('/designer/suggest-value-for-label', designerController.suggestValueForLabel);
router.patch('/designer/update-vision-bot', designerController.updateVisionBotData);
// Authentication
router.post('/authentication/current-user', authController.currentUser);
router.post('/authentication/refresh-token', authController.refreshToken);
router.post('/logout', authController.logout);
// Project bots
router.route('/bots')
    .get(botsController.get);

router.get('/projects/:projectId/categories/:categoryId/bots/:botId/run', botsController.runVisionBot);
router.get('/projects/:projectId/categories/:categoryId/bots/Edit', botsController.editVisionBot);
router.get('/projects/:projectId/categories/:categoryId/files/staging', botsController.getVisionBotPreviewDocuments);
router.get('/projects/:projectId/categories/:categoryId/bots/:botId/layout/:layoutId/pages/:pageIndex/preview', botsController.previewVisionBot);
router.get('/projects/:projectId/categories/:categoryId/bots/:botId/documents/:documentId/preview', botsController.previewVisionBotDocument);
router.get('/projects/:projectId/categories/:categoryId/documents/:documentId/pages/:pageId/images', projectsController.getImageData);
router.post('/projects/:projectId/categories/:categoryId/visionBot/:visionBotId/keepalive', botsController.keepDesignerAlive);

router.route('/projects/:projectId/categories/:categoryId/bots/:botId/state')
    .post(botsController.setEnvironmentState);

// File classification
router.route('/projects/:projectId/files/status')
    .get(classificationController.getProcessingStatusByProject);

// Projects
router.route('/projects')
    .get(projectsController.get)
    .post(projectsController.create);

//data migration
router.post('/projects/export/start', projectsController.exportProjects);
router.get('/projects/migrationstatus', projectsController.getMigrationStatus);
router.post('/projects/import/start', projectsController.importProjects);
router.get('/projects/archivedetails', projectsController.getArchiveDetails);
router.get('/projects/last-migration-info', projectsController.getLastMigrationInfo);
// Project validations

router.get('/projects/validations', projectsController.getValidations);

router.route('/projects/:id')
    .get(projectsController.getById)
    .put(projectsController.update)
    .patch(projectsController.patch)
    .delete(projectsController.remove);
router.get('/projects/:id/metadata', projectsController.getMetadataById);

router.get('/projects/:id/report', projectsController.getClassificationReport);

router.get('/projects/:id/categories', projectsController.getProjectCategories);

// Project fields
router.route('/project-fields/:id?')
    .get(projectFieldsController.get);

// Project files
router.route('/projects/:id/files')
    .get(filesController.get)
    .post(filesController.upload);
// Get file upload status
router.get('/projects/:id/files/status/:transactionId', filesController.checkUploadStatus);

// Service launcher
router.get('/projects/:projectId/validator', packerContainer.launchValidator);
router.get('/projects/:projectId/categories/:categoryId/designer', packerContainer.launchDesigner);

// Reporting
router.get('/reporting/project-totals', reportingController.getProjectTotals);
router.get('/reporting/projects/:projectId/totals', reportingController.getProjectTotalsById);
router.get('/reporting/projects', reportingController.getProjects);

// Languages
router.get('/languages/', languagesController.get);
router.get('/domain-languages', languagesController.getDomainLanguages);

//aggregator
router.get('/projects/:projectId/detail-summary', projectsController.getProjectDetailSummary);

// fields
router.get('/fields/:fieldId', fieldController.getField);

//validator
router.get('/validator/:projectId', validatorController.getDocumentToValidate);
router.post('/validator/:projectId/file/:fileId/save', validatorController.saveValidatedDocument);
router.post('/validator/:projectId/file/:fileId/markAsInvalid', validatorController.markAsInvalid);

// domains
router.post('/domains/import-raw', domainsController.importUnencryptedDomain);
router.get('/domains', domainsController.getDomains);
router.post('/domains/import', domainsController.importDomains);
router.get('/domains/export/:domainName', domainsController.exportDomains);

router.post('/export-csv', designerController.exportPreviewDocument);
router.get('/download-csv', designerController.downloadCSV);

module.exports = router;
