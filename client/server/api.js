/**
 * Server side API entry
 * This file manages the entry points to the API and versions available
 */

const express = require('express');
const app = express();
const routesV1 = require(`${global.appRoutes}/v1`);

// Version 1
app.use('/v1', routesV1);
// Defaults to latest version if not passed in the URI
app.use('/', routesV1);

module.exports = app;
