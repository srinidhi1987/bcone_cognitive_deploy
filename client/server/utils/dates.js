module.exports = {
    /**
     * Format seconds to HH MM SS
     * @param secs
     * @returns {string}
     */
    toHHMMSS(secs) {
        const secNum = parseInt(secs, 10); // don't forget the second param
        const hours = this.leftPad(Math.floor(secNum / 3600));
        const minutes = this.leftPad(Math.floor((secNum - (hours * 3600)) / 60));
        const seconds = secNum - (hours * 3600) - (minutes * 60);

        return `${hours}h ${minutes}m ${seconds}s`;
    },

    /**
     * pad the left side of a number
     * @param {number} int
     * @returns {number}
     */
    leftPad(int) {
        if (int < 10) return Number(`0${int}`);

        return int;
    },
};
