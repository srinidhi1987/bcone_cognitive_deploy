module.exports = {
    applyExportImportErrorFilter(statusCode, result, defaultErrorMessage) {
        if (result.success) return;
        const filteredResult = {
            success: result.success,
            data: result.data,
            errors: result.errors && [(statusCode === 412 || statusCode === 423) ?
            result.errors[0] : defaultErrorMessage],
        };
        return filteredResult;
    },
};
