/**
 * Created by iamcutler on 12/1/16.
 */

module.exports = {
    handleJSONRequestResponse(res) {
        return (err, response, body) => {
            if (err) {
                console.log(JSON.stringify(err)); // eslint-disable-line no-console
                return res.sendStatus(500);
            }

            res.json(body);
        };
    },

    /**
     * Convert JS object to query string
     * @param {object} json
     * @returns {*}
     */
    jsonToQueryString(json) {
        return Object.keys(json).map((key) => {
            return `${encodeURIComponent(key)}=${encodeURIComponent(json[key])}`;
        }).join('&');
    },

    /**
     * Handle any API exceptions
     *
     * @param {string|number} statusCode
     * @param {string|error} errors
     * @returns {object|null}
     */
    handleApiException(statusCode, errors = []) {
        if (statusCode && (statusCode < 200 || statusCode > 399)) {
            return {
                statusCode,
                errors,
            };
        }

        return null;
    },

    /**
     * Check if status code with body
     *
     * @description Allow no response body based on HTTP status code
     * @param {number} statusCode
     * @returns {boolean}
     */
    isStatusCodeOkayWithNoBody(statusCode) {
        return ([
            201,
            204,
            205,
        ].indexOf(statusCode) !== -1);
    },
};
