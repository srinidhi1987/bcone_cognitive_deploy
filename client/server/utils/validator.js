const InvalidReasons = {
    1: 'Fields missing',
    2: 'Tables missing',
    3: 'Wrong values',
};

module.exports = {
    getArrayOfTruthyIds(ids) {
        return Object.keys(ids).reduce((accumulatedIds, id) => {
            const currentId = ids[id];

            return currentId ? accumulatedIds.concat([{reasonId: id, reasonText: InvalidReasons[id]}]) : accumulatedIds;
        }, []);
    },
};
