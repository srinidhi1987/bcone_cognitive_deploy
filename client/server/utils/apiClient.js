const apiUtils = require('./requestHandlers');
const logger = require('./logger');
const request = require('./request');
const config = require('../config');


/**
 * Api client which supplies a wrapper to services to include bearer authentication token
 *
 * @param {object} res
 * @param {function} service
 * @param {array} args
 */
module.exports = {
    get: apiClient.bind(request, request.get),
    post: apiClient.bind(request, request.post),
    put: apiClient.bind(request, request.put),
    patch: apiClient.bind(request, request.patch),
    delete: apiClient.bind(request, request.delete),
    del: apiClient.bind(request, request.del),
};

/**
 * Api client which supplies a wrapper to request to include bearer authentication token and headers for logging
 *
 * @param {function} requestMethod request method
 * @param {object} res ServerResponse object
 * @param {function} cb request callback
 */
function apiClient(requestMethod, res, requestOptions, cb) {
    try {
        const options = Object.assign(
            requestOptions,
            {headers: Object.assign(
                {},
                requestOptions.headers || {},
                {
                    [config.AUTH_TOKEN_HEADER]: res.locals.currentUser.authToken,
                }
            )}
        );
        const callback = (...cbArgs) => {
            const responseBody = cbArgs[1];
            const apiResponse = cbArgs[2];

            // check for condition
            if (apiResponse && !apiUtils.isStatusCodeOkayWithNoBody(apiResponse.statusCode)) {
                if (!responseBody) return cb(new Error('No response body provided from the server'));
                // check for valid json
                if (typeof responseBody !== 'object') {
                    cbArgs[0] = new TypeError(responseBody || 'Server response was not valid json');
                }
            }

            return cb && cb(...cbArgs);
        };

        return requestMethod(options, callback);
    }
    catch (err) {
        console.error(err); // eslint-disable-line no-console
        logger.error(err);
    }
}
