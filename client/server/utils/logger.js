const {config, Logger, transports} = require('winston');

const appLogger = new (Logger)({levels: config.syslog.levels});

appLogger.add(transports.Console, {name: 'debug-console', level: 'debug', colorize: false});
appLogger.add(transports.Console, {name: 'errors-console', level: 'error', colorize: false});

if (process.env.IQ_BOT_LOGS_PATH) {
    appLogger.add(transports.File, {name: 'warnings-file', level: 'warning', filename: `${process.env.IQ_BOT_LOGS_PATH}/ClientGateway_Warnings.txt`, handleExceptions: true});
    appLogger.add(transports.File, {name: 'errors-file', level: 'error', filename: `${process.env.IQ_BOT_LOGS_PATH}/ClientGateway_Errors.txt`, handleExceptions: true});
}
else {
    appLogger.warning('The logs output path is not current set');
}

module.exports = appLogger;
