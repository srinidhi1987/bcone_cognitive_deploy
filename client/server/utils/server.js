const http = require('http');
const https = require('https');
const fs = require('fs');
const Promise = require('bluebird');
const {exec} = require('child_process');
const logger = require('../logger');
const path = require('path');
const shelljs = require('shelljs');

module.exports = {
    /**
     * Create server
     *
     * @param {object} app
     * @param {object} options
     * @param {string} options.secure
     * @returns {Server}
     */
    createServer(app, options = {secure: false}) {
        // Start server with HTTPS
        if (options.secure) {
            return https.createServer({
                key: fs.readFileSync(process.env.SSL_CERT_KEY).toString(),
                cert: fs.readFileSync(process.env.SSL_CERT_FILE).toString(),
            }, app);
        }

        // Start server with HTTP
        return http.createServer(app);
    },

    /**
     * Log that app started
     *
     * @param {object} ngrok
     * @param {object} options
     * @param {string} options.secure
     * @param {string|number} options.port
     * @param {Error} err
     * @returns {*}
     */
    logAppStarted(ngrok, options = {}, err) {
        if (err) return logger.error(err.message);

        // default options
        options.secure = options.secure || false;

        const protocol = options.secure ? 'https' : 'http';

        // Connect to ngrok in dev mode
        if (ngrok) {
            ngrok.connect(options.port, (innerErr, url) => {
                if (innerErr) return logger.error(innerErr);

                logger.appStarted(options.port, url, protocol);
            });
        }
        else {
            logger.appStarted(options.port, null, protocol);
        }
    },

    /**
     * Fetch package.json content
     */
    getPackage() {
        return new Promise((resolve, reject) => {
            try {
                fs.readFile(path.resolve(`${global.appRoot}`, '../package.json'), (err, result) => {
                    if (err) return reject(err);
                    resolve(JSON.parse(result.toString()));
                });
            }
            catch (err) {
                reject(err);
            }
        });
    },

    /**
     * Get the current branch name
     */
    getCurrentSCMBranchName() {
        return new Promise((resolve, reject) => {
            shelljs.exec('git branch', {silent: true}, (code, output, error) => {
                if (error) return reject(error);
                if (code !== 0) return reject(new Error('Failed to get the SCM branch name'));
                const branch = output.match(/^\*(.*)/gm);

                resolve(branch && branch.length ? branch[0].replace('* ', '') : branch);
            });
        });
    },

    /**
     * Get the last commit in the branch
     */
    getLastCommitHash() {
        return new Promise((resolve, reject) => {
            exec('git log --pretty=format:\'%H\' -n 1', (err, stdout, stderr) => {
                if (err || stderr) return reject(err || stderr);
                resolve(stdout);
            });
        });
    },
};
