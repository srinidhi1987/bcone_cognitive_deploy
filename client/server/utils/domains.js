module.exports = {
    isServerConnectionError({code, exception, errors}) {
        return code === 'ECONNREFUSED' || exception === 'java.net.ConnectException' || errors === 'Internal Server Error.';
    },
};
