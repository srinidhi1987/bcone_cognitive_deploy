// The module has been changes to incorporate the intermediate security changes for 5.2 release.
// Node Server has been configured to run securely however backend API and inter-service communication will not be secured.
// This module can be eliminate as it just proxying the requests but we are keeping this as it is because we will be
// secureing backend communication with self-signed certificate soon after next release and this module will be having
// all configuration to disable strict SSL check for self-signed certificate.

const requestOriginal = require('request');
const isSecureServer = process.env.RUN_WITH_SSL === 'true';

const handler = {
    apply: (target, thisArg, argumentsList) => {
        const option = Object.assign({}, argumentsList[0]);
        const argList = [
            option,
            argumentsList[1],
        ];
        return target.apply(thisArg, argList);
    },
};

const request = isSecureServer ?
                    Object.assign({}, requestOriginal, {
                        get: new Proxy(requestOriginal.get, handler),
                        post: new Proxy(requestOriginal.post, handler),
                        put: new Proxy(requestOriginal.put, handler),
                        patch: new Proxy(requestOriginal.patch, handler),
                        delete: new Proxy(requestOriginal.delete, handler),
                        del: new Proxy(requestOriginal.del, handler),
                    }) : requestOriginal;
module.exports = request;
