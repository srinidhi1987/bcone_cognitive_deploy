
module.exports = {
    // TODO: This is not a levenstein distance...
    getLevenshteinDistance(oldValue, newValue) {
        if (!oldValue || !newValue) return 0;
        if (oldValue === newValue) return 1;
        return 1;
    },
};

