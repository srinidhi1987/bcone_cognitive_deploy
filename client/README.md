# Automation Anywhere IQ Bot

## Tech Stack
- ReactJS
- Redux
- Redux middlewares: redux-sagas
- Redux Reselect
- ImmutableJS
- NodeJS (Express)
- Yarn
- Webpack2
- Babel
- SASS, PostCSS
- Jest Test Runner (jasmine2, enzyme)

Please see the [cognitive-frontend-docs wiki](https://bitbucket.org/automationanywhere/cognitive-frontend/wiki/Home)
for details on how to set up your editor and environment.

****

## Development Server

For our package manager we use yarn as an alternative to npm. Install yarn globally using npm.
```npm
npm install -g yarn
```

Install dependencies.

```npm
yarn install
brew update
brew install traefik
```

> On Windows? [Download the Windows binary from the traefik releases page](https://github.com/containous/traefik/releases)

Edit traefik reverse proxy configuration `internals/traefik/config/rules.toml`
```bash
[frontends]
  [frontends.http]
    entryPoints = ["http"]
    backend = "dummy"
  [frontends.http.routes.all]
    rule = "PathPrefix:/"

[backends.dummy]
  [backends.dummy.servers.server1]
    # Control Room
    url = "http://ec2-13-57-235-206.us-west-1.compute.amazonaws.com:8080" <---- EDIT THIS LINE to your Control Room URL
```

Run a local reverse proxy from a command prompt
```bash
yarn run start:devproxy
``` 

*Run a local development server.*

```npm
yarn start
```

And then, in your browser, navigate to
```
http://localhost:8080/IQBot/
```

Note: If your install result in errors you may need to install some additional dependencies globally due to dependency `sharp`. On Windows, the easiest way to do this is to use npm (yes npm in this case) to install the global dependecies needed to build native modules.
```npm
npm install -g windows-build-tools
```

After the windows-build-tool are successfully installed run `yarn install` again. See [node-gyp](https://github.com/nodejs/node-gyp) for more details.


****

## Command Line Interface (CLI)

#### Generate Container (Smart component)
```npm
yarn generate container
```

#### Generate Component
```npm
yarn generate component
```

#### Generate Route
```npm
yarn generate route
```

## Linting

Lint with ESLint and SASSLint.

```npm
yarn lint
```

****

## Testing

Test with Jest.

```npm
yarn test
```

When making many test modifications, it is helpful to bring up the headless browser and keep it up while autorunning changes:

```npm
yarn test:watch
```

****

## Building

```npm
yarn build
```
