/**
 * Created by lordoftherings on 11/29/16.
 * This file was modified with the minimal modifications necessary for IE11 support
 */

window.iqBotConfig = (function() {
    const IQ_BOT_BASE_PATH = document.getElementsByTagName('base')[0].getAttribute('href');
    const CONTROL_ROOM_URL = document.getElementsByTagName('meta').CONTROL_ROOM_URL.getAttribute('content');
    const IQ_BOT_FULL_URL = CONTROL_ROOM_URL.concat(IQ_BOT_BASE_PATH);

    // Redirect to CR host to manage SSO and proxy, no direct access to IQBot
    if (CONTROL_ROOM_URL && window.location.origin.toLowerCase() !== CONTROL_ROOM_URL.toLowerCase()) {
        window.location.replace(IQ_BOT_FULL_URL);
    }

    const APP_STORAGE = Object.freeze({
        token: 'authToken',
        user: 'user',
        lastUserInteractionTimestamp: 'lastUserInteractionTimestamp',
    });
    const AUTH_TOKEN_HEADER = 'x-authorization';
    const SESSION_EXPIRE_TIME_OUT = 20;

    return Object.freeze({
        'IQ_BOT_BASE_PATH' : IQ_BOT_BASE_PATH, //eslint-disable-line object-shorthand
        'CONTROL_ROOM_URL' : CONTROL_ROOM_URL, //eslint-disable-line object-shorthand
        'APP_STORAGE' : APP_STORAGE, //eslint-disable-line object-shorthand
        'AUTH_TOKEN_HEADER' : AUTH_TOKEN_HEADER, //eslint-disable-line object-shorthand
        'SESSION_EXPIRE_TIME_OUT' : SESSION_EXPIRE_TIME_OUT, //eslint-disable-line object-shorthand
    });
}());
