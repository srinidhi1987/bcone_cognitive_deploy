// @flow

/**
 * Created by iamcutler on 5/10/17.
 */

/**
 * Constructs the designer protocol url (used to launch native windows app)
 */
export function constructDesignerProtocolHandler(
    appConfig: string = '',
    authToken: string = '',
    categoryId: string,
    projectId: string
) : string {
    return `AACP.Designer:${appConfig}\\Bearer ${authToken}\\${projectId}\\${categoryId}`;
}

/**
 * Construct the validator protocol URL (used to launch native windows app)
 */
export function constructValidatorProtocolHandler(
    appConfig: string = '',
    authToken: string = '',
    projectId: string) : string {
    return `AACP.Validator:${appConfig}\\Bearer ${authToken}\\${projectId}`;
}
