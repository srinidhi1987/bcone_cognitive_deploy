/**
 * Created by iamcutler on 11/30/16.
 */

/**
 * Create form data from JSON
 * @param {object} data
 * @param {object} data.files - list of uploaded files if present
 * @returns {*}
 */
export function getFormDataFromJSON(data) {
    const formData = new FormData();
    const files = data.files;

    Object.keys(data).forEach((key) => {
        const type = typeof data[key];

        if (type === 'string') {
            formData.append(key, data[key]);
        }
    });

    // append files
    if (files) {
        Object.keys(files).forEach((key) => {
            formData.append('files', files[key]);
        });
    }

    return formData;
}

/**
 * get checkbox state checked or false
 * @param {boolean} isChecked
 * @returns {Boolean || String}
 */
export function getCheckboxState(isChecked) {
    return isChecked ? 'checked' : false;
}
