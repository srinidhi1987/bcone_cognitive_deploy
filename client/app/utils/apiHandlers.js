/**
 * Created by iamcutler on 11/23/16.
 */

export function handleApiResponse(response) {
    return new Promise((resolve, reject) => {
        if (response) {
            return resolve(response);
        }

        reject(response);
    });
}

/**
 * handleApiError
 *
 * @param {*} originalResponseError
 * @returns {*}
 */
export function handleApiError(originalResponseError) {
    try {
        const parsedError = JSON.parse(originalResponseError);
        return parsedError;
    }
    catch (err) {
        return originalResponseError;
    }
}
