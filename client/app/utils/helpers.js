import {navBarWidths} from '../containers/HomePage/constants';

/**
 * Created by iamcutler on 3/18/17.
 */
/**
 * Format bytes to a readable string
 *
 * @param {number} bytes
 * @param {number} decimals
 * @returns {string}
 */
export function formatBytes(bytes, decimals = 0) {
    if (bytes === 0) {
        return '0 Byte';
    }

    const k = 1024; //Or 1 kilo = 1000
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(decimals))} ${sizes[i]}`;
}

/**
 * returns navbar width based on url path regex
 *
 * @param {string} location
 * @returns {string}
 */
export function decideNavBarWidth(location) {
    const regexForSkinnyNavBar = new RegExp(/\/bots\/.*\/edit|\/bots\/.*\/preview|\/learning-instances\/.*\/validator/i);
    return regexForSkinnyNavBar.test(location) ? navBarWidths.SKINNY_WIDTH : navBarWidths.REGULAR_WIDTH;
}

export function isJSON(str) {
    try {
        JSON.parse(str);
    }
    catch (e) {
        return false;
    }

    return true;
}
