import {IQBOT_USER_ROLES} from '../constants/enums';

export function isAllowedAccess(allowedRoles, rolesUserHas) {
    if (!Array.isArray(allowedRoles)) return false;
    if (!Array.isArray(rolesUserHas)) return false;
    const accessLevel = getAccessLevel(rolesUserHas);
    return allowedRoles.some((role) => (role === accessLevel) || (role === IQBOT_USER_ROLES.LOGGED_IN));
}

export function getAccessLevel(roles) {
    if (!Array.isArray(roles)) return IQBOT_USER_ROLES.UNKNOWN;
    if (roles.some((role) => role.name === IQBOT_USER_ROLES.SERVICES)) return IQBOT_USER_ROLES.SERVICES;
    if (roles.some((role) => role.name === IQBOT_USER_ROLES.VALIDATOR)) return IQBOT_USER_ROLES.VALIDATOR;
    return IQBOT_USER_ROLES.UNKNOWN;
}

export function isLicensed(allowedLicenses, licensesUserHas) {
    if (!Array.isArray(allowedLicenses)) return false;
    if (!Array.isArray(licensesUserHas)) return false;
    return allowedLicenses.some((license) => licensesUserHas.some((userLicense) => license === userLicense));
}
