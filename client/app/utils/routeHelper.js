import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {IQ_BOT_BASE_PATH} = iqBotConfig;
const base = IQ_BOT_BASE_PATH.replace(/\/$/, ''); // Strip any slash at end of the base path

export default function routeHelper(path = '') {
    const start = Number(path[0] === '/'); // Strip any slash at start of path
    return `${base}/${path.slice(start)}`;
}

export {
    routeHelper,
};
