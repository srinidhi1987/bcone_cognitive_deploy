const logger = {
    log(...args) {
        return console.log(...args); // eslint-disable-line no-console
    },
    error(...args) {
        return console.error(...args); // eslint-disable-line no-console
    },
};

export default logger;
