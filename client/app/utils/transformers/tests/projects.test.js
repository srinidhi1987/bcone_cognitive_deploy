/**
 * Created by iamcutler on 11/30/16.
 */

import {
    transformProjectUpdateMessageForServer,
    getProjectPatchBody} from '../projects';
import {Project1} from '../../../../fixtures/projects';
import {operations, expectedPatchBody} from '../../../../fixtures/form';
describe('Project transformers:', () => {
    it('should transform project for correct messaging with backend', () => {
        // given
        // when
        const result = transformProjectUpdateMessageForServer(Project1);
        // then
        expect(result).toEqual({
            id: Project1.id,
            name: Project1.name,
            description: Project1.description,
            organizationId: Project1.organizationId,
            projectType: Project1.projectType,
            primaryLanguage: Project1.primaryLanguage,
            fields: Project1.fields,
            projectState: Project1.projectState,
            environment: Project1.environment,
        });
    });

    it('should transform operations to require format by patch request', () => {
        const result = getProjectPatchBody(operations);
        expect(result).toEqual(expectedPatchBody);
    });
});
