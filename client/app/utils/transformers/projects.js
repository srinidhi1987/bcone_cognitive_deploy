/**
 * Created by iamcutler on 11/30/16.
 */

export function transformProjectUpdateMessageForServer(project) {
    const desiredValues = [
        'id', 'name', 'description', 'organizationId', 'projectType', 'primaryLanguage',
        'fields', 'projectState', 'environment', 'files',
    ];

    return fetchKeysFromModel(project, desiredValues);
}

export function fetchKeysFromModel(model, keys = []) {
    return Object.keys(model).reduce((acc, key) => {
        if (keys.indexOf(key) !== -1) {
            acc[key] = model[key];
        }

        return acc;
    }, {});
}

/**
 * getPatchBody method returns array of operation that need to be passed to BE Patch endpoint.
 * @param {ojbect} operations with updated values
 * @returns {array} return object form will always be [{op:'replace/add', path:'fieldName',value:object}]
 */
export function getProjectPatchBody(operations, versionId) {
    const body = Object.keys(operations).reduce((acc, op) => {
        if (op === 'fields') {

            operations[op].standard.length > 0 &&
            acc.push({op: 'add', path:`/${op}/standard`, value: operations[op].standard, versionId});

            operations[op].custom.length > 0 &&
            acc.push({op: 'add', path:`/${op}/custom`, value: operations[op].custom, versionId});
        }
        else {
            acc.push({
                op: 'replace',
                path: `/${op}`,
                value: operations[op] ? operations[op] : '',
                versionId,
            });
        }
        return acc;
    }, []);
    return body;
}
