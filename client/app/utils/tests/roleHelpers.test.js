/**
 * Created by brantbarger on 5/8/17
 */

import {isAllowedAccess, getAccessLevel} from '../roleHelpers';
import {IQBOT_USER_ROLES} from '../../constants/enums';

describe('utility roleHelpers', () => {

    describe('helper method isAllowedAccess', () => {
        it('should return true if valid IQBot role and ControlRoom role combination is passed', () => {
            expect(isAllowedAccess([IQBOT_USER_ROLES.SERVICES], [{name: IQBOT_USER_ROLES.SERVICES}])).toEqual(true);
            expect(isAllowedAccess([IQBOT_USER_ROLES.VALIDATOR], [{name: IQBOT_USER_ROLES.VALIDATOR}])).toEqual(true);
        });

        it('should return false if invalid IQBot role and ControlRoom role combination is passed', () => {
            expect(isAllowedAccess([IQBOT_USER_ROLES.VALIDATOR], [{name: IQBOT_USER_ROLES.SERVICES}])).toEqual(false);
            expect(isAllowedAccess([IQBOT_USER_ROLES.SERVICES], [{name: IQBOT_USER_ROLES.VALIDATOR}])).toEqual(false);
            expect(isAllowedAccess([], [{name: IQBOT_USER_ROLES.SERVICES}])).toEqual(false);
            expect(isAllowedAccess([IQBOT_USER_ROLES.UNKNOWN], [{name: IQBOT_USER_ROLES.SERVICES}])).toEqual(false);
        });
        it('should return false in case if passed roles  is not type array', () => {
            expect(isAllowedAccess({}, {})).toEqual(false);
            expect(isAllowedAccess({}, [])).toEqual(false);
            expect(isAllowedAccess([], {})).toEqual(false);
        });
    });
    describe('helper method getAccessLevel', () => {
        it('should return IQBot Service role', () => {
            expect(getAccessLevel([{name: IQBOT_USER_ROLES.SERVICES}])).toEqual(IQBOT_USER_ROLES.SERVICES);
            expect(getAccessLevel([{name: IQBOT_USER_ROLES.SERVICES}, {name: IQBOT_USER_ROLES.VALIDATOR}])).toEqual(IQBOT_USER_ROLES.SERVICES);
        });
        it('should return IQBot Validator role', () => {
            expect(getAccessLevel([{name: IQBOT_USER_ROLES.VALIDATOR}])).toEqual(IQBOT_USER_ROLES.VALIDATOR);
            expect(getAccessLevel([])).toEqual(IQBOT_USER_ROLES.UNKNOWN);
        });
        it('should return Unknown in case if passed roles is not type array', () => {
            expect(getAccessLevel({})).toEqual(IQBOT_USER_ROLES.UNKNOWN);
            expect(getAccessLevel('Services')).toEqual(IQBOT_USER_ROLES.UNKNOWN);
        });
    });
});
