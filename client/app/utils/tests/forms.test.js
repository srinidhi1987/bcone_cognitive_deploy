import {getCheckboxState} from '../forms';

// given
describe('getCheckboxState: function', () => {
    it('should return checked when target.checked is true', () => {
        //when
        const isChecked = getCheckboxState(true);
        // then
        expect(isChecked).toEqual('checked');
    });

    it('should return checked when target.checked is true', () => {
        //when
        const isChecked = getCheckboxState(false);
        // then
        expect(isChecked).toEqual(false);
    });
});
