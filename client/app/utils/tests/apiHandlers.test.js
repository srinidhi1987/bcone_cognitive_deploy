/**
 * Created by iamcutler on 11/23/16.
 */

import {handleApiResponse, handleApiError} from '../apiHandlers';

const responseSig = {
    success: true,
    errors: [],
    data: [],
};

describe('handleApiResponse', () => {
    it('should return response if success is true', () => {
        // given
        // when
        return handleApiResponse(responseSig).then((response) => {
            // then
            expect(response).toEqual(responseSig);
        });
    });

    it('should return error if success is false', () => {
        // given
        const response = {
            ...responseSig,
            success: false,
        };
        // when
        return handleApiResponse(response).catch((err) => {
            // then
            expect(err).toEqual(response);
        });
    });
});

describe('handleApiError', () => {
    // Will be flushed out
    it('should return parsed JSON if valid JSON', () => {
        // given
        const example = JSON.stringify({
            data: null,
            success: false,
            errors: ['Error 1', 'Error 2'],

        });

        // when
        const returnValue = handleApiError(example);

        // then
        expect(JSON.stringify(returnValue)).toEqual(example); // checking string equality

    });

    it('should return original value given to method if not valid JSON (will fail parse)', () => {
        // given
        const errorInstance = new Error('exception message');
        // when
        const returnValueOne = handleApiError(errorInstance);
        // then
        expect(returnValueOne).toEqual(errorInstance);

        // given
        const errorString = 'This is a sample error';
        // when
        const returnValueTwo = handleApiError(errorString);
        // then
        expect(returnValueTwo).toEqual(errorString);
    });
});
