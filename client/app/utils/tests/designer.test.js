import * as designerUtils from '../designer';

import {
    fieldDefTables,
    originalFieldDefTables,
    mergedColumns,
    mergeTableSirFields,
    // mergedTableResultNoAutoMapped,
} from '../../../fixtures/designer';

import {FIELD_TYPES_ENUM} from '../../constants/enums';

describe('designer utils', () => {
    describe('filterFieldTypesForDropdown', () => {
        it('should return array of field types without tables', () => {
            const result = designerUtils.filterFieldTypesForDropdown(FIELD_TYPES_ENUM);

            expect(result).toEqual(['0', '1', '3']);
        });
    });

    describe('mergeTableData', () => {

        it('should merge table fields if no tables are automapped', () => {
            const result = designerUtils.mergeTableData(fieldDefTables, [], originalFieldDefTables, mergedColumns, mergeTableSirFields);

            expect(result[0].PrimaryColumn).toBeDefined();
        });
    });

    describe('getDrawnArea', () => {
        it('should create drawnArea object so that all preview fields can be created at once', () => {
            const result = designerUtils.getDrawnArea({Id: '5', Value: {Bounds: '10, 10, 25, 40'}});

            expect(result).toEqual({
                currentZoomScale: 1,
                x: 10,
                y: 10,
                width: 25,
                height: 40,
                overlappedStaticAnnotations: [],
                designerData: {
                    isAuto: true,
                    isPreview: true,
                    isFromClick: false,
                    activeFieldId: '5',
                    activeFieldOption: 'Preview',
                },
            });
        });
    });
});
