/**
 * Created by iamcutler on 5/10/17
 */

import {constructDesignerProtocolHandler, constructValidatorProtocolHandler} from '../thickClientApps';

describe('utility thickClientApps', () => {
    // given
    const appConfig = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9';
    const token = '6344563tijvt9righger0ht43g30rgiregr';
    const categoryId = 3;
    const projectId = '456567756534342432';

    describe('helper method constructDesignerProtocolHandler', () => {
        it('should return URL with designer protocol handler', () => {
            // given
            // when
            const result = constructDesignerProtocolHandler(appConfig, token, categoryId, projectId);
            // then
            expect(result).toEqual(`AACP.Designer:${appConfig}\\Bearer ${token}\\${projectId}\\${categoryId}`);
        });
    });

    describe('helper method constructValidatorProtocolHandler', () => {
        it('should return URL with validator protocol handler', () => {
            // given
            // when
            const result = constructValidatorProtocolHandler(appConfig, token, projectId);
            // then
            expect(result).toEqual(`AACP.Validator:${appConfig}\\Bearer ${token}\\${projectId}`);
        });
    });
});
