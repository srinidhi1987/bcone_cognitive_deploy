/**
 * Created by iamcutler on 3/18/17.
 */
import {formatBytes} from '../helpers';

describe('util helpers', () => {
    describe('formatBytes', () => {
        it('should return 900 Bytes', () => {
            // given
            // when
            const result = formatBytes(900);
            // then
            expect(result).toEqual('900 Bytes');
        });

        it('should return 5 MB', () => {
            // given
            // when
            const result = formatBytes(5242880);
            // then
            expect(result).toEqual('5 MB');
        });

        it('should return 2 GB', () => {
            // given
            // when
            const result = formatBytes(2097152000);
            // then
            expect(result).toEqual('2 GB');
        });

        it('should return 2 TB', () => {
            // given
            // when
            const result = formatBytes(2097152000000);
            // then
            expect(result).toEqual('2 TB');
        });
    });
});
