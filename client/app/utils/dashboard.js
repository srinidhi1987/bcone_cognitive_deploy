export const sortByEnvironmentAndName = (a, b) => {
    const env = a.environment.localeCompare(b.environment);

    if (env !== 0) {
        return a.environment.localeCompare(b.environment);
    }

    return b.name.localeCompare(a.name);
};
