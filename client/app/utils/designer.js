import Rect from '../components/Rect';
import React from 'react';
import {
    DESIGNER_AREA_TYPES,
    DESIGNER_FIELD_THEMES,
    FIELD_MENU_ITEM_ICONS,
    FIELD_OPTIONS_MAP,
} from '../constants/enums';
import {ANNOTATION_MODES} from '../components/AnnotationEngine/constants/enums';
import {EMPTY_BOUNDS, DEFAULT_SIMILARITY_FACTOR, DEFAULT_EMPTY_FIELD} from '../containers/DesignerContainer/constants';
import {EMPTY_STRING} from '../constants/common';
import {Icon} from 'common-frontend-components';
import routeHelper from './routeHelper';
const {FIELD, COLUMN, TABLE} = DESIGNER_AREA_TYPES;

export const getDefaultEmptyField = () => ({...DEFAULT_EMPTY_FIELD, Id: guid()});

/**
 * @description get field type
 * @param {string} areaType
  *@returns {string} fieldType in reducer
 */
export const getFieldType = (areaType) => `${areaType}s`;

/**
 * @description is matching id
 * @param {string} id1
 * @param {string} id2
  *@returns {boolean} is matching id
 */
export const isMatchingId = (id1, id2) => id1 === id2;

/**
 * @description get matching aliases for a given field
 * @param {Object[]} fields
 * @param {string} id
  *@returns {Object[]} matching aliases
 */
export const getMatchingAliases = (fields, id) => {
    const matchingField = fields.find((actionField) => isMatchingId(actionField.Id, id));

    return matchingField ? matchingField.aliases : [];
};

export const isSelectedDrawTool = (selectedField, fieldBeingTested, mode) => {
    return selectedField === fieldBeingTested && mode === ANNOTATION_MODES.DRAW;
};

export const getLabelSuggestionData = (props) => {
    const {layouts, url, visionBotId} = props;
    // for now we are only handling one layout, so we just take the first index
    // of the layouts array
    const [layout] = layouts;
    const urlArray = url.split('/');
    const projectId = urlArray[4];
    const categoryId = urlArray[6];
    const layoutId = layout.Id;
    const documentId = layout.DocProperties.Id;

    return {
        categoryId,
        documentId,
        layoutId,
        projectId,
        visionBotId,
    };
};

const getProtectedBoundsVal = (val) => {
    return val || 0;
};

export const getTransformedSirField = (...args) => {
    const [
        annotatorState,
        docMeta,
        selectedObjects,
        activeField,
        currentDocPage,
        pageData,
        boundsString,
        textContent,
        Id,
        index,
    ] = args;
    // Bounds: "246, 73, 205, 76"
    const bounds = boundsString.split(', ');
    // the plus + in front of each value is because this is a highly performant way of converting strings to numbers
    const x = +bounds[0]; //eslint-disable-line
    const y = +bounds[1]; //eslint-disable-line
    const width = +bounds[2]; //eslint-disable-line
    const height = +bounds[3]; //eslint-disable-line
    const activeFieldOption = annotatorState.get('activeFieldOption');
    const currentZoomScale = annotatorState.get('currentZoomScale');
    const layoutRatio = 1 / annotatorState.get('aspectRatio');
    const selectedObject = selectedObjects.find((area) => {
        return area.index === index + pageData[currentDocPage - 1].startIndex;
    });
    const mappedValue = FIELD_OPTIONS_MAP[activeFieldOption];
    const isSameFieldOption = selectedObject && selectedObject.index === activeField[mappedValue];
    const fill = !selectedObject ? 'none' : !isSameFieldOption ? 'rgba(255,188,3,0.15)' : Rect.meta.initial.fill;
    const stroke = !selectedObject ? '#7ba8c7' : 'rgba(255,188,3,1)';
    const opacity = !selectedObject ? 1 : !isSameFieldOption ? 0.5 : 1;

    return {
        index: index + pageData[currentDocPage - 1].startIndex,
        x,
        y,
        x2: x + width,
        y2: y + height,
        width,
        height,
        style: {fill, stroke, opacity},
        stroke: '#02AFD1',
        strokeDasharray: 'none',
        textContent,
        isStatic: true,
        Id,
        type: Rect,
        refHeight:  docMeta.Metadata.height,
        refWidth:  docMeta.Metadata.width,
        windowXOffset:0,
        windowYOffset:0,
        windowWidthOffset: 0,
        windowHeightOffset: 0,
        layoutRatio,
        currentZoomScale,
    }; // eslint-disable-line no-implicit-coercion
};

export const getTransformedSirPreviewField = (...args) => {
    const [
        boundsString,
        textContent,
        Id,
        index,
        annotatorState,
        docMeta,
        selectedObjects,
        currentDocPage,
        pageData,
    ] = args;
    // Bounds: "246, 73, 205, 76"
    const bounds = boundsString.split(', ');
    // the plus + in front of each value is because this is a highly performant way of converting strings to numbers
    const x = +bounds[0]; //eslint-disable-line
    const y = +bounds[1]; //eslint-disable-line
    const width = +bounds[2]; //eslint-disable-line
    const height = +bounds[3]; //eslint-disable-line

    const currentZoomScale = annotatorState.get('currentZoomScale');
    const layoutRatio = 1 / annotatorState.get('aspectRatio');
    const selectedObject = selectedObjects.find((area) => {
        return area.index === index + pageData[currentDocPage - 1].startIndex;
    });
    const fill = !selectedObject ? 'none' : Rect.meta.initial.fill;
    const stroke = !selectedObject ? '#7ba8c7' : 'rgba(255,188,3,1)';
    const opacity = 1;

    return {
        index: index + pageData[currentDocPage - 1].startIndex,
        x,
        y,
        x2: x + width,
        y2: y + height,
        width,
        height,
        style: {fill, stroke, opacity},
        stroke: '#02AFD1',
        strokeDasharray: 'none',
        textContent,
        isStatic: true,
        Id,
        type: Rect,
        refHeight:  docMeta.Metadata.height,
        refWidth:  docMeta.Metadata.width,
        windowXOffset:0,
        windowYOffset:0,
        windowWidthOffset: 0,
        windowHeightOffset: 0,
        layoutRatio,
        currentZoomScale,
    }; // eslint-disable-line no-implicit-coercion
};

// this function should always have a context binded to it, so it can
// compare variables in the context it is called from
export function isMatchingElement(element) {
    let item;

    try {
        item = this.accessor(element); // eslint-disable-line
    }
    catch (e) {
        item = Math.random() * Math.random();
    }

    return item === this.elementToCompare; // eslint-disable-line
}

export const isTruthy = (data) => data;

/**
 *
 * @param {Object} BotResult
 * @param {Object[]} sirFields
 * @returns {area: Object, activeArea: Object}
 */
export const findMappedLabels = (botResult, sirFields) => {
    // these are the automap suggestions for mapping columns OR previously created mappings
    const suggestedColumns = botResult.visionBotData.Layouts[0].Tables.reduce((accumulator, table) => {
        return accumulator.concat(table.Columns.map((column) => ({...column, isColumn: true})));
    }, []);

    // these are the column fields that the user has created as a part of the learning instance
    // that are instance wide...
    const dataModelColumns = botResult.visionBotData.DataModel.Tables.reduce((accumulator, table) => {
        return accumulator.concat(table.Columns);
    }, []);

    // these are the fields that the user has created as a part of the learning instance
    // that are instance wide...
    const dataModelFields = botResult.visionBotData.DataModel.Fields;

    return botResult.visionBotData.Layouts[0].Fields.concat(suggestedColumns).map((field) => {
        // find the sir field with matching bounds to the suggested label bounds given by the backend
        const sirFieldIndex = sirFields.findIndex(isMatchingElement, {elementToCompare: field.Bounds, accessor: ({Bounds}) => Bounds});
        const areaType = field.isColumn ? COLUMN : FIELD;
        // if theres not a match, create a new area to automap.. the isDrawn flag lets the automapping saga functionality
        // know that it needs to "draw" an area instead of "click" on an area (or trigger the reusable sir click handler)
        if (sirFieldIndex === -1) {
            const area = {textContent: field.Label, Bounds: field.Bounds};
            const activeArea = {fieldOption: 'Label', Bounds: field.Bounds, Label: field.Label, Id: field.FieldId, areaType};

            return {isDrawn: true, area, activeArea};
        }
        // this creates an object with meta data that tells the reducer which SIR field to 'click' on or in other words,
        // map to the desired label in this case
        const area = {index: sirFieldIndex, textContent: field.Label};

        const fieldArray = field.isColumn ? dataModelColumns : dataModelFields;

          // active area lets us know where to find that field we want to map this label to
        const activeArea = fieldArray.find(isMatchingElement, {elementToCompare: field.FieldId, accessor: ({Id}) => Id});

        return {area, activeArea: {...activeArea, fieldOption: 'Label', areaType}};
    }).filter(isTruthy);
};

/**
 *
 * @param {Object} BotResult
 * @param {Object[]} sirFields
 * @returns {area: Object, activeArea: Object}
 */
export const findMappedValues = (botResult, sirFields) => {
    return botResult.visionBotData.Layouts[0].Fields.map((field) => {
      // lets take the x and y of the label sir
        const [x1, y1] = field.Bounds.split(', ');
        const [newX, newY, width, height] = field.ValueBounds.split(', ');
        // we want to subtract the x and y delta values from the ValueBounds, but keep the height and width as is
        const elementToCompare = `${getProtectedBoundsVal(Number(x1) + Number(newX))}, ${getProtectedBoundsVal(Number(y1) + Number(newY))}, ${getProtectedBoundsVal(width)}, ${getProtectedBoundsVal(height)}`;

        const sirFieldIndex = sirFields.findIndex(isMatchingElement, {elementToCompare, accessor: ({Bounds}) => Bounds});

        // active area lets us know where to find that field we want tp map this value to
        const activeArea = botResult.visionBotData.DataModel.Fields.find(isMatchingElement, {elementToCompare: field.FieldId, accessor: ({Id}) => Id});

        if (sirFieldIndex === -1) {
          // if theres not a match, create a new area to automap.. the isDrawn flag lets the automapping saga functionality
          // know that it needs to "draw" an area instead of "click" on an area (or trigger the reusable sir click handler)
          // same as in label, but with DisplayValue, and areaType is always field since tables dont have display values
            const area = {textContent: field.DisplayValue, Bounds: elementToCompare};
            const activeArea = {
                areaType: FIELD,
                fieldOption: 'DisplayValue',
                Bounds: elementToCompare,
                DisplayValue: field.DisplayValue,
                Id: field.FieldId,
                isAuto: Boolean(field.IsValueBoundAuto),
            };

            return {isDrawn: true, area, activeArea};
        }
        // this creates an object with meta data that tells the reducer which SIR field to 'click' on or in other words,
        // map to the desired Value in this case
        const area = {index: sirFieldIndex, textContent: field.DisplayValue};

        return {area, activeArea: {...activeArea, fieldOption: 'DisplayValue', areaType: FIELD, isAuto: Boolean(field.IsValueBoundAuto)}};
    }).filter(isTruthy);
};

export const findMappedTables = (botResult, sirFields) => {
    // lets map through auto or previously mapped tables
    return botResult.visionBotData.Layouts[0].Tables.map((table) => {
        const {Footer, PrimaryColumn} = table;

        let footer, refColumn;

        // active area lets us know where to find the table that we have mapped the relevant info to
        const activeArea = botResult.visionBotData.DataModel.Tables.find(isMatchingElement, {elementToCompare: table.TableId, accessor: ({Id}) => Id});

        if (Footer && Footer.Bounds) {
            const sirFieldIndex = sirFields.findIndex(isMatchingElement, {elementToCompare: Footer.Bounds, accessor: ({Bounds}) => Bounds});

            if (sirFieldIndex === -1) {
                // the footer always maps to the "Label" property since there is only one value, not a key and a value (backend does it this way)
                // hoever it is important to note that the bounds are always 0,0,0,0 by teh thick client (Since we dont use label bounds), amd sometimes ahve
                // a real value (probably from automapping), or our web designer that is NOT 0,0,0,0..
                // so it wont match up to anything...but we still need to map it so we can use the Footer.Label
                const area = {textContent: Footer.Label, Bounds: Footer.Bounds};
                const activeArea = {areaType: TABLE, fieldOption: 'Footer', Bounds: Footer.Bounds, Footer: Footer.Label, Id: table.TableId};

                footer = {isDrawn: true, area, activeArea};
            }
            else {
                const area = {index: sirFieldIndex, textContent: Footer.Label};

                footer = {area, activeArea: {...activeArea, fieldOption: 'Footer', areaType: TABLE}};
            }
        }
          // VERY IMPORTANT
        // this also needs to be changed and is probably causing our table settings persisting defect. please see above footer mapping logic
        if (PrimaryColumn) {
            const matchingColumn = activeArea.Columns.find((col) => col.Id === PrimaryColumn.FieldId) || activeArea.Columns[0];

            refColumn = {refColumn: matchingColumn.Name, Id: table.TableId};
        }

        return {footer, refColumn};

    }).reduce((tableFields, table) => tableFields.concat(table), []);
};

/**
 * renders menu icon to collapse or expand, with animation
 *
 * @param shouldHideMenuItems
 * @returns {XML}
 */
export const renderDoubleChevrons = (shouldHideMenuItems) => ( // eslint-disable-line react/display-name
    <Icon fa="angle-double-left" className={`double-chevron-${shouldHideMenuItems ? 'down' : 'up'}`}/>
);

export const getFieldMenuItemIcon = (fieldType) => FIELD_MENU_ITEM_ICONS[fieldType];

export const getFieldIconElement = (fieldIcon) => { // eslint-disable-line react/display-name
    return fieldIcon === 'hashtag' ?
    <img className="label-icon label-icon--number" src={routeHelper('/images/field-type-number.svg')} /> : <Icon className="label-icon" fa={fieldIcon}/>;
};

export const findDrawnField = (drawnAreas, field, option, pageData) => {
    const area = drawnAreas.find((area) => area.designerData.activeFieldId === field.Id && area.designerData.activeFieldOption === option);

    if (!area) {
        return {bounds: null};
    }

    const pageDrawnOn = pageData.find((page) => area.y >= page.startPx && area.y < page.endPx);

    const {currentZoomScale} = area || {};
    const protectedCurrentZoomScale = currentZoomScale || 1;
    const x = Math.round(area.x / protectedCurrentZoomScale);
    const y = Math.round(((area.y - pageDrawnOn.startPx) / protectedCurrentZoomScale) + pageDrawnOn.startPx);
    const width = Math.round(area.width / protectedCurrentZoomScale);
    const height = Math.round(area.height / protectedCurrentZoomScale);

    return {multiplyByLayout: !area.designerData.isAuto && area.designerData.isAuto !== false, bounds: `${getProtectedBoundsVal(x)}, ${getProtectedBoundsVal(y)}, ${getProtectedBoundsVal(width)}, ${getProtectedBoundsVal(height)}`};
};


// a lot of these properties are jsut default values that were given to me by india team....(fieldDirection, mergeRatio, formatExpression, or anything with hard coded values)
// also for Id, when we dont have an Id previously created (because the field wasnt previously mapped),
// we create our own
export const getMappedFieldDef = ({
    labelBounds,
    DisplayValue,
    EndsWith,
    Label,
    StartsWith,
    Id,
    matchingField,
    relativeBounds,
    IsValueBoundAuto,
    FormatExpression,
}) => ({
    Bounds: labelBounds,
    DisplayValue,
    EndsWith,
    Label,
    StartsWith,
    FieldDirection: 2,
    FieldId: Id,
    FormatExpression,
    MergeRatio: 1,
    IsMultiline: false,
    IsValueBoundAuto: Boolean(IsValueBoundAuto),
    SimilarityFactor: matchingField.SimilarityFactor || DEFAULT_SIMILARITY_FACTOR,
    Type: 0,
    ValueType: 0,
    Id: matchingField.Id || guid(),
    XDistant: 0,
    ValueBounds: relativeBounds,
});

const roundBounds = (bounds) => {
    const [x, y, w, h] = bounds.split(', ');

    return `${getProtectedBoundsVal(Math.round(x))}, ${getProtectedBoundsVal(Math.round(y))}, ${getProtectedBoundsVal(Math.round(w))}, ${getProtectedBoundsVal(Math.round(h))}`;
};

const adjustLabelBounds = (bounds, layoutRatio, pageData) => {
    const [x, y, w, h] = bounds.split(', ');
    const numberY = Number(y);

    const pageOfBound = pageData.find((page) => numberY >= page.startPx && numberY < page.endPx);
    const adjustedBounds = `${getProtectedBoundsVal(Number(x) * layoutRatio)}, ${getProtectedBoundsVal(((Number(y) - pageOfBound.startPx) * layoutRatio) + pageOfBound.startPx)}, ${getProtectedBoundsVal(Number(w) * layoutRatio)}, ${getProtectedBoundsVal(Number(h) * layoutRatio)}`;

    return roundBounds(adjustedBounds);
};


// to determine displayValue bounds since they are just relative calculations compared to label
const relativizeBounds = (bounds, displayBounds) => {
    const [x, y] = bounds.split(', ');
    const [x2, y2, w2, h2] = displayBounds.split(', ');

    return `${getProtectedBoundsVal(Number(x2) - Number(x))}, ${getProtectedBoundsVal(Number(y2) - Number(y))}, ${getProtectedBoundsVal(w2)}, ${getProtectedBoundsVal(h2)}`;
};

export const mergeFieldData = (layoutFields, fields, sirFields, drawnFields, layoutRatio, pageData) => {
    const relevantFields = fields.filter(({mappedSirDisplayValue, mappedSirLabel, drawnLabel, drawnDisplayValue}) => mappedSirLabel || drawnLabel || mappedSirDisplayValue || drawnDisplayValue);
    return relevantFields.map(({StartsWith, EndsWith, FormatExpression, mappedSirLabel, mappedSirDisplayValue, Label, DisplayValue, Id, drawnLabel, drawnDisplayValue, IsValueBoundAuto}) => {
        const matchingField = layoutFields.find((field) => field.FieldId === Id) || {};

        // try to find the mapped sir field, if mnot we keep the value as the default for later use: EMPTY_BOUNDS
        const labelSirField = mappedSirLabel !== null ? sirFields[mappedSirLabel].Bounds : EMPTY_BOUNDS;
        const displayValueSirField = mappedSirDisplayValue !== null ? sirFields[mappedSirDisplayValue].Bounds : EMPTY_BOUNDS;
        // if we found labelSirField use that as the bounds, if not try to find a drawn field...if we cant find the drawn field, keep it as emptu bounds
        const drawnLabelField = findDrawnField(drawnFields, {Id}, 'Label', pageData);
        const drawnValueField = findDrawnField(drawnFields, {Id}, 'DisplayValue', pageData);
        const labelBounds = labelSirField !== EMPTY_BOUNDS ? labelSirField : (drawnLabelField.bounds || EMPTY_BOUNDS);
        const displayValueBounds = displayValueSirField !== EMPTY_BOUNDS ? displayValueSirField : (drawnValueField.bounds || EMPTY_BOUNDS);
        // if we drew the field, we need to apply the layout ratio coeefficient to save properly
        const labelLayoutRatio = drawnLabelField && drawnLabelField.multiplyByLayout ? layoutRatio : 1;
        const valueLayoutRatio = drawnValueField && drawnValueField.multiplyByLayout ? layoutRatio : 1;

        const labelBoundsAdjusted = drawnLabel && labelBounds !== matchingField.Bounds ? adjustLabelBounds(labelBounds, labelLayoutRatio, pageData) : labelBounds;

        // after layout ratio is applied, only then can we create proper relative bounds
        const [labelX, labelY] = labelBoundsAdjusted.split(', ');
        // adjust the display value bounds if needed
        const adjustedDisplayValueBounds = drawnDisplayValue && relativizeBounds(labelBoundsAdjusted, displayValueBounds) !== matchingField.ValueBounds ? adjustLabelBounds(displayValueBounds, valueLayoutRatio, pageData) : displayValueBounds;
        const [x, y, width, height] = adjustedDisplayValueBounds.split(', ');

        const relativeBounds = `${getProtectedBoundsVal(Number(x) - Number(labelX))}, ${getProtectedBoundsVal(Number(y) - Number(labelY))}, ${getProtectedBoundsVal(width)}, ${getProtectedBoundsVal(height)}`;

        const roundedLabel = roundBounds(labelBoundsAdjusted);
        const roundedRelative = roundBounds(relativeBounds);

        // produces object in the shape we want for Layouts.Fields
        return getMappedFieldDef({
            labelBounds: roundedLabel,
            DisplayValue,
            EndsWith,
            FormatExpression,
            Label,
            StartsWith,
            Id,
            IsValueBoundAuto,
            matchingField,
            relativeBounds: roundedRelative,
        });
    });
};

// same as above but always works and way simpler simnce columns dont have displayValues and therefore dont have to worry about relative bounds
export const mergeColumnData = (layoutTables, columns, sirFields, drawnFields, layoutRatio, pageData) => {
    const flatColumnArray = layoutTables.reduce((cols, table) => {
        return cols.concat(table.Columns);
    }, []);

    const mappedColumns = columns.filter(({mappedSirLabel, drawnLabel}) => mappedSirLabel || drawnLabel);

    return mappedColumns.map(({StartsWith, EndsWith, FormatExpression, mappedSirLabel, mappedSirDisplayValue, drawnLabel, Label, DisplayValue, Id, tableId}) => {
        const matchingColumn = flatColumnArray.find((col) => col.FieldId === Id) || {};

        const labelSirField = mappedSirLabel ? sirFields[mappedSirLabel].Bounds : EMPTY_BOUNDS;
        const drawnLabelField = findDrawnField(drawnFields, {Id}, 'Label', pageData);

        const labelBounds = labelSirField !== EMPTY_BOUNDS ? labelSirField : (drawnLabelField.bounds || EMPTY_BOUNDS);
        const labelLayoutRatio = drawnLabelField && drawnLabelField.multiplyByLayout ? layoutRatio : 1;

        const labelBoundsAdjusted = drawnLabel && labelBounds !== matchingColumn.Bounds ? adjustLabelBounds(labelBounds, labelLayoutRatio, pageData) : labelBounds;

        return {
            ...getMappedFieldDef({
                labelBounds: labelBoundsAdjusted,
                DisplayValue,
                EndsWith,
                FormatExpression,
                Label,
                StartsWith,
                Id,
                mappedSirDisplayValue,
                matchingField: matchingColumn,
                relativeBounds: EMPTY_BOUNDS,
            }),
            // tableid for later use when we map the columns to the tables we want to save
            tableId,
        };
    });
};

export const mergeTableData = (fieldDefTables, mappedTables, originalTables, mergedColumns, sirFields) => {
    return fieldDefTables.filter((table) => !table.IsDeleted).map((table, tableIndex) => {
        const matchingTable = mappedTables.find((mappedTable) => mappedTable.TableId === table.Id) || {TableId: table.Id};
        const matchingOriginalTable = originalTables.find((originalTable) => originalTable.Id === table.Id);
        // removes tableId
        const Columns = mergedColumns.filter((column) => column.tableId === table.Id).map(({
            Bounds,
            EndsWith,
            Label,
            StartsWith,
            FieldDirection,
            FieldId,
            FormatExpression,
            MergeRatio,
            IsValueBoundAuto,
            SimilarityFactor,
            Type,
            ValueType,
            Id,
            XDistant,
            ValueBounds,
        }) => ({
            Bounds,
            DisplayValue: '',
            EndsWith,
            Label,
            StartsWith,
            FieldDirection,
            FieldId,
            FormatExpression,
            MergeRatio,
            IsValueBoundAuto,
            SimilarityFactor,
            Type,
            ValueType,
            Id,
            XDistant,
            ValueBounds,
        }));

        const previouslyMappedFooter = matchingTable.Footer;
        const designerMappedFooter = table.mappedSirFooter ? sirFields[table.mappedSirFooter].Bounds : null;
        const defaultFooter = previouslyMappedFooter || getDefaultEmptyField();
        // if we already have a mapped footer from before opening designer, use the meta data it had,
        // otherwise use the default field properties
        // also in layouts[x].Tables, the Footer will be in the shape of any other mapped field
        const Footer = {
            ...defaultFooter,
            Bounds: designerMappedFooter || defaultFooter.ValueBounds,
            Id: previouslyMappedFooter && previouslyMappedFooter.Id ? previouslyMappedFooter.Id : guid(),
            Label: table.Footer || '',
        };

        // same as footer, the PrimaryColumn (ref column) will look like any other mapped field
        const previouslyMappedPrimaryColumn = matchingTable.PrimaryColumn;
        // if nothing has been automapped for a certain table, use the first column as the default
        const designerMappedPrimaryColumn = table.refColumn || originalTables[tableIndex].Columns[0].Name;

        const matchingRefColumn = matchingOriginalTable.Columns.find((column) => column.Name === designerMappedPrimaryColumn);

        const mappedRefColumn = Columns.find((column) => column.FieldId === matchingRefColumn.Id);

        const PrimaryColumn = mappedRefColumn || {
            ...(previouslyMappedPrimaryColumn || {...getDefaultEmptyField(), Id: guid()}),
        };

        return {
            Bounds: EMPTY_BOUNDS,
            Id: EMPTY_STRING,
            TableId: matchingTable.TableId || guid(),
            IsValueMappingEnabled: matchingTable.IsValueMappingEnabled || false,
            Columns,
            Footer,
            PrimaryColumn,
        };
    });
};


// gets rid of any mappings we made to the instance wide field defs so its only the original fieldDef for saving purposes
export function simplifyFieldData(originalField) {
    const matchingMappedField = this.modifiedFields.find((modifiedField) => modifiedField.Id === originalField.Id);  // eslint-disable-line

    const {
        Id,
        Name,
        ValueType,
        Formula,
        ValidationID,
        IsRequired,
        DefaultValue,
        StartsWith,
        EndsWith,
        FormatExpression,
        IsDeleted,
    } = matchingMappedField;

    return {
        Id,
        Name,
        ValueType,
        Formula,
        ValidationID,
        IsRequired,
        DefaultValue,
        StartsWith,
        EndsWith,
        FormatExpression,
        IsDeleted,
    };
}

// same as above, gets rid of any extra properties that we extended or mapped to tables
export function simplifyTableData(originalTable) {
    const matchingMappedTable = this.tables.find((modifiedField) => modifiedField.Id === originalTable.Id);  // eslint-disable-line

    if (!matchingMappedTable) {
        return originalTable;
    }

    const {Id, Name, IsDeleted} = matchingMappedTable;
    const mappedColumns = this.columns.filter((column) => column.tableId === Id);  // eslint-disable-line

    const simplifiedColumns = originalTable.Columns.map(simplifyFieldData, {modifiedFields: mappedColumns});

    return {
        Id,
        Name,
        IsDeleted,
        Columns: simplifiedColumns,
    };
}

export const getBotSaveData = ({visionBotId, url, tables, fields, columns, layouts, allSirFields: sirFields, drawnAreas, originalTables, originalFields, layoutRatio: ratioToOriginalSize, pageData}) => {
    // these functions take the values we have mapped to visionBotData.Tables, visiomBotData.Fields (we mapped to these properties before we
    // ever knew about the layout mappings) and maps them to the correct layout mapping locations
    const mappedColumns = mergeColumnData(layouts[0].Tables, columns, sirFields, drawnAreas, ratioToOriginalSize, pageData);
    const mappedFields = mergeFieldData(layouts[0].Fields, fields, sirFields, drawnAreas, ratioToOriginalSize, pageData);
    const mappedTables = mergeTableData(tables, layouts[0].Tables, originalTables, mappedColumns, sirFields);

    // this gets rid of all the new "mapped" properties from visionBotData.Tables, visiomBotData.Fields that we extended
    // so we can save them properly
    const dataModelTables = originalTables.map(simplifyTableData, {tables, columns});
    const dataModelFields = originalFields.map(simplifyFieldData, {modifiedFields: fields});
    const validatorData = [...fields, ...columns].reduce(listValidationIssueFieldsFilter, []);
    // just grabs these properties from this function since it was pre existing
    const {categoryId, projectId, layoutId} = getLabelSuggestionData({url, layouts});


    return {
        tables: mappedTables,
        fields: mappedFields,
        dataModelFields,
        dataModelTables,
        categoryId,
        projectId,
        layoutId,
        visionBotId,
        layoutIndex: 0,
        validatorData,
    };
};

// grabbed this from stack overflow
export const guid = () => {
    function _p8(s) {
        const p = (`${Math.random().toString(16)}000000000`).substr(2, 8);

        return s ? `-${p.substr(0, 4)}-${p.substr(4, 4)}` : p;
    }

    return _p8() + _p8(true) + _p8(true) + _p8();
};

export const getFlattenedPreviewFields = ({fields, tables}) => {
    const rowData = tables.reduce((tableFields, table) => {
        const thisTablesFields = table.Rows.reduce((fields, row) => {
            return row.Fields.reduce((fields, field) => {
                return {...fields, [field.Id]: {...field, isRow: true}};
            }, fields);
        }, {});

        return {...tableFields, ...thisTablesFields};
    }, {});

    const fieldData = fields.reduce((fields, currentField) => {
        return {
            ...fields,
            [currentField.Id]: currentField,
        };
    }, {});

    return {...fieldData, ...rowData};
};

export const getActivePreviewId = ({fields, tables}) => {
    return fields.length ? fields[0].Id : tables[0].Rows[0].Fields[0].Id;
};

export const findActivePreviewField = (activeId, fields) => {
    const id = Object.keys(fields).find((fieldId) => fields[fieldId].Id === activeId);

    return fields[id] || {Field: {}, Value: {}};
};

export function mapActivePreviewField(field) {
    const {Id: id} = field;
    const isActive = id === this.activePreviewId;  // eslint-disable-line
    const modifiedField = this.flattenedFields[id].Field; // eslint-disable-line
    const modifiedValue = this.flattenedFields[id].Value; // eslint-disable-line

    return {
        ...field,
        Field: {...modifiedField, isActive},
        Value: {...modifiedValue},
    };
}

export function mapTablePreviewField(table) {
    return {
        ...table,
        Rows: table.Rows.map((row) => {
            return {
                ...row,
                Fields: row.Fields.map(mapActivePreviewField, this), // eslint-disable-line
            };
        }),
    };
}

export function syncDocsWithCurrentLayout(docs, currentLayout) {
    const sortedDocsWithCurrentTrainingFileFirst = docs.reduce((docs, doc) => {
        return currentLayout.DocProperties.Id === doc.fileId ? [doc, ...docs] : [...docs, doc];
    }, []);

    const [firstFile] = sortedDocsWithCurrentTrainingFileFirst;

    if (firstFile.fileId === currentLayout.DocProperties.Id) {
        return sortedDocsWithCurrentTrainingFileFirst;
    }

    return [{fileId: currentLayout.DocProperties.Id}, ...sortedDocsWithCurrentTrainingFileFirst];
}

/**
 * @description finds non standard sir preview boxes
 * @param {Object[]} sirFields
 * @param {Object} previewData
  *@returns {Object[]} drawnBoxes {isDrawn: boolean, area: Object, activeArea: Object, isPreview: boolean}
 */
export function findNonStandardPreviewBoxes(sirFields, previewData) {
    const flattenedFields = getFlattenedPreviewFields(previewData);

    const drawnBoxes = Object.keys(flattenedFields).map((field) => flattenedFields[field]);

    return drawnBoxes.reduce((nonStandardFields, field) => {
        if (!field.Value.Text) return nonStandardFields;

        return sirFields.find((sir) => sir.Bounds === field.Value.Bounds) ? nonStandardFields : [...nonStandardFields, getDrawnArea(field)];
    }, []);
}

/**
 * Extends column with specific view values needed for the Designer feature
 * @param {Object} field
 * @returns {...Object, currentZoomScale: Number, x: Number, y: Number, width: Number, height: Number, overlappedStaticAnnotations: Object[], designerData: Object}
 */
export function getDrawnArea(field) {
    const [x, y, width, height] = field.Value.Bounds.split(',');
    const numberX = Number(x);
    const numberY = Number(y);
    const numberWidth = Number(width);
    const numberHeight = Number(height);

    return {
        currentZoomScale: 1,
        x: numberX,
        y: numberY,
        width: numberWidth,
        height: numberHeight,
        overlappedStaticAnnotations: [],
        designerData: {
            isAuto: true,
            isPreview: true,
            isFromClick: false,
            activeFieldId: field.Id,
            activeFieldOption: 'Preview',
        },
    };
}

//This prepared data is used to store list validation
/**
 * This is map function to parepare validatorData object to save at BE.
 * This is special object which holds info of list validation for respective fields.
 * @param {array} acc
 * @param {object} field
 */
export function listValidationIssueFieldsFilter(acc, field) {
    if (field.List) {
        const separator = ',';
        //remove last separator if any
        const refineList = field.List.charAt(field.List.length) === separator ? field.List.substring(0, field.List.length - 1) : field.List;
        const valueList = refineList.split(separator);
        //convert it to array without null or emtpy value
        const StaticListItems = valueList.reduce((acc, value) => {
            if (value) return [...acc, value];
            return acc;
        }, []);
        const ValidatorData = {
            StaticListItems,
        };
        const validatorData = {
            ID: field.Id,
            ValidatorData,
            TypeOfData: 'Automation.VisionBotEngine.Validation.StaticListProvider, Automation.Cognitive.VisionBotEngine.Validation, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null',
            SerializedData: JSON.stringify(ValidatorData),
        };

        return [
            ...acc,
            validatorData,
        ];
    }
    return acc;
}

export const getUrlParamsForNavigatingPreview = (originalUrl) => {
    const [,,,, project,, category,,,, page, token] = originalUrl.split('/');

    return {project, category, page, token};
};

export const getNewAnnotatorImageUrl = (originalUrl, currentIndex, previewDocuments, currentDocPage) => {
    const {project, category, token} = getUrlParamsForNavigatingPreview(originalUrl);
    const newDoc = previewDocuments[currentIndex];
    const newDocId = newDoc && newDoc.fileId;

    if (!newDocId) {
        return originalUrl;
    }

    return routeHelper(`/api/projects/${project}/categories/${category}/documents/${newDocId}/pages/${currentDocPage - 1}/${token}`);
};

export const filterFieldTypesForDropdown = (fieldTypeEnum) => Object.keys(fieldTypeEnum).filter((type) => fieldTypeEnum[type] !== DESIGNER_FIELD_THEMES.TABLE);
