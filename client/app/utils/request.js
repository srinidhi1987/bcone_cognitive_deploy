/**
 * This is a request wrapper used to attach application data like tokens to requests
 * while using the common request utilities.
 * Created by iamcutler on 3/28/17.
 */

import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {
    APP_STORAGE,
    AUTH_TOKEN_HEADER,
} = iqBotConfig;
import {request} from 'common-frontend-utils';

/**
 * Append app token to the request header
 *
 * @param {object} options
 * @param {object} options.headers
 * @returns {object}
 */
function setAppTokenWithHeader(options = {}) {
    if (localStorage && localStorage.getItem(APP_STORAGE.token) !== null) {
        options.headers = {
            ...options.headers,
            [AUTH_TOKEN_HEADER]: JSON.parse(localStorage.getItem(APP_STORAGE.token)),
        };
    }
    return options;
}

/**
 * A `GET` XMLHttpRequest resolved as a Promise.
 *
 * @param {string} url - The http request's url.
 * @param {{body: Object, query: Object, headers: Object}} [options={}] - The http request's url.
 * @param {Function} [onError] - a callback for handling a bad response.
 * @return {Promise<Object, Error>} - A Promise that resolves or rejects the XMLHttpRequest.
 */
function requestGet(url, options, onError) {
    return request('GET', url, setAppTokenWithHeader(options), onError);
}

/**
 * A `PUT` XMLHttpRequest resolved as a Promise.
 *
 * @param {string} url - The http request's url.
 * @param {{body: Object, query: Object, headers: Object}} [options={}] - The http request's url.
 * @param {Function} [onError] - a callback for handling a bad response.
 * @return {Promise<Object, Error>} - A Promise that resolves or rejects the XMLHttpRequest.
 */
function requestPut(url, options, onError) {
    return request('PUT', url, setAppTokenWithHeader(options), onError);
}

/**
 * A `PATCH` XMLHttpRequest resolved as a Promise.
 *
 * @param {string} url - The http request's url.
 * @param {{body: Object, query: Object, headers: Object}} [options={}] - The http request's url.
 * @param {Function} [onError] - a callback for handling a bad response.
 * @return {Promise<Object, Error>} - A Promise that resolves or rejects the XMLHttpRequest.
 */
function requestPatch(url, options, onError) {
    return request('PATCH', url, setAppTokenWithHeader(options), onError);
}

/**
 * A `POST` XMLHttpRequest resolved as a Promise.
 *
 * @param {string} url - The http request's url.
 * @param {{body: Object, query: Object, headers: Object}} [options={}] - The http request's url.
 * @param {Function} [onError] - a callback for handling a bad response.
 * @return {Promise<Object, Error>} - A Promise that resolves or rejects the XMLHttpRequest.
 */
function requestPost(url, options, onError) {
    return request('POST', url, setAppTokenWithHeader(options), onError);
}

/**
 * A `DELETE` XMLHttpRequest resolved as a Promise.
 *
 * @param {string} url - The http request's url.
 * @param {{body: Object, query: Object, headers: Object}} [options={}] - The http request's url.
 * @param {Function} [onError] - a callback for handling a bad response.
 * @return {Promise<Object, Error>} - A Promise that resolves or rejects the XMLHttpRequest.
 */
function requestDelete(url, options, onError) {
    return request('DELETE', url, setAppTokenWithHeader(options), onError);
}

export {
    requestGet,
    requestPost,
    requestPut,
    requestPatch,
    requestDelete,
};
