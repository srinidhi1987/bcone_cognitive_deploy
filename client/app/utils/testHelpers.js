// @flow
import {kebabCase} from 'lodash';
import {Link} from 'react-router';
import type {ShallowWrapper} from 'enzyme';
import type {ComponentType} from 'react';

/**
 * a component that takes a react component constructor and returns a function that uses the components name to
 * generate class names that follow the convention '.my-react-component--section'
 *
 * @param {{displayName: string}} componentConstructor
 * kkj@returns {(sectionName: string) => string}
 */
export const createNamingFunction = (componentConstructor: ComponentType<any>) => (
    (sectionName: string) : string => `.${kebabCase(componentConstructor.displayName)}--${sectionName}`
);


type LinkTester = {
    url(): string,
    label(): string,
}

type BreadcrumbTester = {
    link(index: number): LinkTester,
}


/**
 * enzyme helper which finds info on child <Link> components of <Breadcrumbs>. <Link> 1 would be index 0.
 *
 * @example
 * // <Breadcrumbs><Link to="/learning-instances">Learning Instances</Link><Breadcrumbs>
 * breadcrumbTester(breadcrumbWrapper).link(0).url();
 * // returns '/learning-instances'
 * @example
 * // <Breadcrumbs><Link to="/bots">My bots</Link><Breadcrumbs>
 * breadcrumbTester(breadcrumbWrapper).link(0).label();
 * // returns 'My bots'
 */
export const breadcrumbTester = (breadcrumbComponentEnzymeWrapper: ShallowWrapper) : BreadcrumbTester => {
    return {
        // for method chaining. Pick the index of the Link you want to assert against
        link(index: number) : LinkTester {
            const linkWrapper = breadcrumbComponentEnzymeWrapper.find(Link).at(index);

            return {
                // returns the value of the 'to' prop on the Link component.
                url() : string {
                    return linkWrapper.props().to;
                },
                // returns the child text node/s of <Label> and concatenates the nodes for convenience.
                label() : string {
                    return linkWrapper
                        .children()
                        .map((textWrapper) => textWrapper.text())
                        .join('');
                },
            };
        },
    };
};
