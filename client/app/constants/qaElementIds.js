/**
 * THESE CONSTANTS ARE RESERVED FOR THE TEST AUTOMATION TEAM -
 * PLEASE DON'T CHANGE OR REMOVE THESE CONSTANTS WITHOUT FIRST CHECKING WITH THE TEST AUTOMATION TEAM
 * See: https://automationanywhere.atlassian.net/wiki/spaces/CD/pages/1349722/UI+Selectors+-+Finding+stable+selectors+in+the+browser
 */

// NEW PROJECT FORM
export const QA_NEW_PROJECT_DOMAIN_WRAPPER = 'qa-new-proj-domain-wrapper';
export const QA_NEW_PROJECT_LANGUAGE_WRAPPER = 'qa-new-proj-language-wrapper';
export const QA_NEW_PROJECT_CUSTOM_FIELDS_ADD = 'qa-new-proj-custom-fields-add';
export const QA_EDIT_PROJECT_CUSTOM_FIELDS_ADD = 'qa-edit-proj-custom-fields-add';
// DASHBOARD
export const QA_DASHBOARD_REPORT_ACCURACY = 'qa-dashboard-report-accuracy';

export const QA_LBL_LABEL = 'qa-lbl-label';
export const QA_TXT_LABEL = 'qa-text-label';
export const QA_LBL_VALUE = 'qa-lbl-value';
export const QA_TXT_VALUE = 'qa-txt-value';
export const QA_LST_TYPE = 'qa-lst-type';
export const QA_IMAGE_DESIGNER = 'qa-image-designer';
