import React from 'react';

export const TRAINING_PERCENTAGE = {
    label: 'Training %',
    description: (label) => ( // eslint-disable-line react/display-name
        <span>The <strong>{label}</strong> represents the number of files that are covered by the bots that were already trained. Note that for a bot to be considered as trained, it has to be in production.</span>
    ),
};

export const INSTANCE_NAME = {
    label: 'Instance Name',
    description: (label) => ( // eslint-disable-line react/display-name
        <p>
            The <strong>{label}</strong> is the name you'll use to find your Learning Instance later on. Each name must be unique.
        </p>
    ),
};

export const APP_NAME = {
    label: 'IQ Bot',
    description: () => ( // eslint-disable-line react/display-name
        <span></span>
    ),
};
