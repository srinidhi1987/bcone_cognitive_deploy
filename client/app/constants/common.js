export const EMPTY_STRING = '';
export const MIN_ZOOM_SCALE = 0.5;
export const MAX_ZOOM_SCALE = 2.0;
export const initialZoomMetadata = {
    zoomScale: 1,
    currentZoomScale: 1,
    scaleAmount: 0.25,
};

/**
 * @enum {string}
 * @readonly
 */
export const ZOOM_ACTION_ENUM = {
    zoomIn: '+',
    zoomOut: '-',
};

export const INITIAL_FIELD_COLUMN_TOP_OFFSET = 74;
