import {FIELD_PROPERTY_NAMES} from '../containers/DesignerContainer/constants';
const {MAPPED_SIR_LABEL, MAPPED_SIR_DISPLAY_VALUE, MAPPED_SIR_FOOTER} = FIELD_PROPERTY_NAMES;

/**
 * Created by iamcutler on 3/17/17.
 */

/**
 * @readonly
 * @enum {string}
 */
export const ValidatorStatus = {
    PROCESSING: 'processing',
    VALIDATING: 'validating',
    REVIEWED: 'reviewed',
};

/**
 * @readonly
 * @enum {string}
 */
export const IQBOT_USER_ROLES = {
    LOGGED_IN: 'LoggedIn', // Not an actual role. Used to indicate that any role is acceptable, just need a valid auth token
    // SERVICES: 'IQBotServices',
    SERVICES: 'AAE_IQ Bot Services',
    // VALIDATOR: 'IQBotValidator',
    VALIDATOR: 'AAE_IQ Bot Validator',
    UNKNOWN: 'Unknown',
};

/**
 * @readonly
 * @enum {string}
 */
export const IQBOT_LICENSES = {
    CREATOR: 'DEVELOPMENT', // Bot Creator
    RUNNER: 'IQBOTRUNTIME', // Bot Runner
};

/**
 * @readonly
 * @enum {string}
 */
export const APP_NOTIFICATIONS = {
    MESSAGE: 'info',
    SUCCESS: 'success',
    WARNING: 'warn',
    ERROR: 'error',
};

/**
 * @readonly
 * @enum {string}
 */
export const BOT_STATUSES = {
    READY: 'ready',
    TRAINING: 'training',
    ACTIVE: 'active',
};

/**
 * @readonly
 * @enum {string}
 */
export const DESIGNER_FIELD_THEMES = {
    MONEY: 'money',
    TEXT: 'text',
    DATE: 'date',
    NUMBER: 'number',
    TABLE: 'table',
};

/**
 * @readonly
 * @enum {string}
 */
export const FIELD_TYPES_ENUM = {
    0: DESIGNER_FIELD_THEMES.TEXT,
    1: DESIGNER_FIELD_THEMES.DATE,
    3: DESIGNER_FIELD_THEMES.NUMBER,
    4: DESIGNER_FIELD_THEMES.TABLE,
};

/**
 * @readonly
 * @enum {string}
 */
export const DESIGNER_AREA_TYPES = {
    FIELD: 'field',
    COLUMN: 'column',
    TABLE: 'table',
};

// just '...' because data filter already prepends 'Search'
/**
 * @readonly
 * @enum {string}
 */
export const DataTableCopyText = {
    SEARCH_ALL_PLACEHOLDER: '...',
    ALL_COLUMNS_DROPDOWN_VALUE: 'All Fields',
};

/**
 * @readonly
 * @enum {string}
 */
export const SortDirections = {
    ASCENDING: 'asc',
    DESCENDING: 'desc',
};

/**
 * @readonly
 * @enum {string}
 */
export const NavigationDirections = {
    NEXT: 'next',
    PREVIOUS: 'previous',
};

/**
 * @readonly
 * @enum {string}
 */
export const COMMON_VALIDATION_TYPES = {
    StartsWith: 'Starts With',
    EndsWith: 'Ends With',
    FormatExpression: 'Pattern',
};

export const VALIDATION_OPTIONS_BY_TYPES = {
    '0': { //Text
        ...COMMON_VALIDATION_TYPES,
        List: 'List',
    },
    '1': { //Date
        ...COMMON_VALIDATION_TYPES,
    },
    '3': { //Number
        ...COMMON_VALIDATION_TYPES,
        Formula: 'Formula',
    },
    '4': {

    },
};

/**
 * @readonly
 * @enum {string}
 */
export const FIELD_OPTIONS = {
    LABEL: 'Label',
    DISPLAY_VALUE: 'DisplayValue',
    IS_REQUIRED: 'IsRequired',
    IS_LABEL_OPTIONAL: 'IsLabelOptional',
    VALIDATE: 'ValidationID',
    FOOTER: 'Footer',
    REFERENCE_COLUMN: 'refColumn',
};

export const FIELD_MENU_ITEM_ICONS = {
    0: 'align-left', // text
    1: 'calendar',  // date
    3: 'hashtag', // number
    4: 'table',
};

export const FIELD_OPTIONS_MAP = {
    [FIELD_OPTIONS.LABEL] : MAPPED_SIR_LABEL,
    [FIELD_OPTIONS.FOOTER] : MAPPED_SIR_FOOTER,
    [FIELD_OPTIONS.DISPLAY_VALUE]: MAPPED_SIR_DISPLAY_VALUE,
};

export const VISION_BOT_MODES = {
    EDIT: 'edit',
    PREVIEW: 'preview',
};
