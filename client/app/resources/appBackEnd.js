import {HEADERS_JSON} from 'common-frontend-utils';
import {requestPost} from '../utils/request';
import routeHelper from '../utils/routeHelper';

export const appBackEnd = {
    /**
     * Voids the user session token
     * @returns {Promise}
     */
    logout() {
        return requestPost(
            routeHelper('api/logout'),
            {
                headers: {
                    ...HEADERS_JSON,
                },
            }
        );
    },
};
