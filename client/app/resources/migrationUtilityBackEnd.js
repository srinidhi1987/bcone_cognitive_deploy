import {HEADERS_JSON} from 'common-frontend-utils';
import {handleApiResponse, handleApiError} from '../utils/apiHandlers';
import {requestGet, requestPost} from '../utils/request';
import routeHelper from '../utils/routeHelper';

export const migrationUtilityBackEnd = {
    /**
     * Upload project files to API
     *
     * @param {string} projectId
     * @param {FileList} files
     * @returns {Promise}
     */
    exportProjects(selectedProjects, archiveName, userId) {

        return requestPost(
            routeHelper('api/projects/export/start'),
            {
                headers: HEADERS_JSON,
                body: {projectExportDetails: selectedProjects, archiveName, userId},
            }
        ).then(handleApiResponse)
        .catch(handleApiError);
    },
    /**
     * fetches migration status
     *  * @param {object} action action detail
     */
    fetchMigrationUtilityStatusCall() {
        return requestGet(
            routeHelper('api/projects/migrationstatus'),
            {
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse)
        .catch(handleApiError);
    },
    /**
     * fetchArchiveDetailsCall - get archive names
     */
    fetchArchiveDetailsCall() {
        return requestGet(
            routeHelper('api/projects/archivedetails'),
            {
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse)
        .catch(handleApiError);
    },
    /**
     * importProjects
     * @param {string} archiveName
     * @param {string} userId
     */
    importProjectsCall(archiveName, userId, option) {
        return requestPost(
            routeHelper('api/projects/import/start'),
            {
                headers: HEADERS_JSON,
                body: {
                    archive: {
                        archiveName,
                    },
                    userId,
                    option,
                },
            }
        ).then(handleApiResponse)
        .catch(handleApiError);
    },
    getLastMigrationInfo() {
        return requestGet(
            routeHelper('api/projects/last-migration-info'),
            {
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse)
        .catch(handleApiError);
    },
};
