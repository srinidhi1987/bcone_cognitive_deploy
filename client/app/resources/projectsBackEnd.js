import {HEADERS_JSON} from 'common-frontend-utils';
import {handleApiResponse, handleApiError} from '../utils/apiHandlers';
import {requestGet} from '../utils/request';
import routeHelper from '../utils/routeHelper';

export const projectsBackEnd = {
    /**
     * Fetch projects from the API
     *
     * @param {!string} projectId
     * @returns {Promise}
     */
    get(projectId) {
        return requestGet(
            routeHelper(`api/projects${projectId ? `/${projectId}` : ''}`),
            {
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse).catch(handleApiError);
    },

    /**
     * Fetch project metadata from the API
     * @param {string} projectId
     * @returns {Promise}
     */
    getMetadata(projectId) {
        return requestGet(
            routeHelper(`api/projects/${projectId}/metadata`),
            {
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse).catch(handleApiError);
    },
};
