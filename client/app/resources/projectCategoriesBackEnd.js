import {requestGet} from '../utils/request';
import {HEADERS_JSON} from 'common-frontend-utils';
import routeHelper from '../utils/routeHelper';

/**
 * fetchProjectCategoriesFromServer
 * @param {string} projectId
 * @param {object} query
 * @returns {Promise.<Object>}
 */

export function projectCategoriesBackEnd(projectId, query = {}) {
    return requestGet(
        routeHelper(`api/projects/${projectId}/categories`),
        {
            headers: HEADERS_JSON,
            query,
        },
    ).catch(() => (console.log('error'))); //eslint-disable-line
}
