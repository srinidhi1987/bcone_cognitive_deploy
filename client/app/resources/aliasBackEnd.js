import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet} from '../utils/request';
import routeHelper from '../utils/routeHelper';

export const aliasBackEnd = {
    /**
     * Fetch alias list for particular language
     *
     * @param {!string} languageId
     * @param {!string} fieldId
     * @returns {Promise}
     */
    get(languageId, fieldId) {
        return requestGet(
            routeHelper(`api/fields/${fieldId}?languageid=${languageId}`),
            {
                headers: HEADERS_JSON,
            }
        );
    },
};
