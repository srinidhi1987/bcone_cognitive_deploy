import {HEADERS_JSON} from 'common-frontend-utils';
import {requestPost, requestPatch} from '../utils/request';
import {handleApiResponse, handleApiError} from '../utils/apiHandlers';
import routeHelper from '../utils/routeHelper';

export const designerBackEnd = {
    /**
     * Fetch bot for specific category ID... may also create bot if not bot is avail?
     *
     * @param {!string} projectId
     * @returns {Promise}
     */
    suggestValueForLabel(projectId, categoryId, layoutId, visionBotId, documentId, fieldDefId, fieldLayout) {
        return requestPost(
            routeHelper('api/designer/suggest-value-for-label'),
            {
                body: {
                    projectId,
                    categoryId,
                    layoutId,
                    visionBotId,
                    documentId,
                    fieldDefId,
                    fieldLayout,
                    layoutIndex: 0,
                },
                headers: HEADERS_JSON,
            }
        );
    },
    saveBot(payload) {
        return requestPatch(
            routeHelper('api/designer/update-vision-bot'),
            {
                body: payload,
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse)
        .catch(handleApiError);
    },
    /**
     * get csv content to export
     * @param {string} projectId
     * @param {string} categoryId
     * @param {object} data
     * @returns {Promise}
     */
    exportToCSV(projectId, categoryId, data) {
        return requestPost(
            routeHelper('api/export-csv'),
            {
                body: {
                    projectId,
                    categoryId,
                    data,
                },
                headers: HEADERS_JSON,
            }
        ).then(handleApiResponse).catch(handleApiError);
    },
};
