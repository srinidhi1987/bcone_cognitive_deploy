import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet, requestPost} from '../utils/request';
import {handleApiResponse, handleApiError} from '../utils/apiHandlers';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import routeHelper from '../utils/routeHelper';

const domainsBackEnd = {
    /**
     * Fetch domains listing
     *
     * @returns {Promise}
     */
    get() {
        return requestGet(
            routeHelper('api/domains'),
            {
                headers: HEADERS_JSON,
            }
        );
    },
    importDomain(formData) {
        return requestPost(
            routeHelper('api/domains/import'),
            {
                body: formData,
                isRawBody: true,
            }
        ).then(handleApiResponse)
         .catch(handleApiError);
    },
    exportDomain(domainName) {
        const token = JSON.parse(localStorage.getItem(APP_STORAGE.token));

        return requestGet(
            routeHelper(`api/domains/export/${domainName}`),
            {
                body: {},
                query: {
                    token,
                },
            },
        ).then(handleApiResponse)
         .catch(handleApiError);
    },
};

export default domainsBackEnd;
