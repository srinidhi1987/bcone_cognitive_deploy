import {HEADERS_JSON} from 'common-frontend-utils';
import {
    //requestPost,
    //requestPatch,
    requestGet, requestPost} from '../utils/request';
import {handleApiResponse, handleApiError} from '../utils/apiHandlers';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import routeHelper from '../utils/routeHelper';

export const validatorAPI = {
    getdocumentToValidate(projectId) {
        const user = JSON.parse(localStorage.getItem(APP_STORAGE.user)); // TODO: Refactor to pass user as a param
        return requestGet(
            routeHelper(`api/validator/${projectId}`),
            {
                headers: {
                    ...HEADERS_JSON,
                    username: user.username,
                },
            },
        ).then(handleApiResponse)
         .catch(handleApiError);
    },
    saveValidatedDocument(projectId, fileId, fields, tables, fieldAccuracyList) {
        const user = JSON.parse(localStorage.getItem(APP_STORAGE.user)); // TODO: Refactor to pass user as a param
        return requestPost(
            routeHelper(`api/validator/${projectId}/file/${fileId}/save`),
            {
                headers: {
                    ...HEADERS_JSON,
                    username: user.username,
                },
                body: {
                    fields,
                    tables,
                    fieldAccuracyList,
                },
            },
        ).then(handleApiResponse)
         .catch(handleApiError);
    },
    markAsInvalid(projectId, fileId, reasons, username) {
        return requestPost(
            routeHelper(`api/validator/${projectId}/file/${fileId}/markAsInvalid`),
            {
                headers: {...HEADERS_JSON, username},
                body: reasons,
            },
        );
    },
};
