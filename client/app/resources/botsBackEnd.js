import {HEADERS_JSON, HEADER_ACCEPT_JSON} from 'common-frontend-utils';
import {requestGet, requestPost} from '../utils/request';
import routeHelper from '../utils/routeHelper';

export const botsBackEnd = {
    /**
     * Fetch bot for specific category ID... may also create bot if not bot is avail?
     *
     * @param {!string} projectId
     * @returns {Promise}
     */
    get(projectId, categoryId, username) {
        return requestGet(
            routeHelper(`api/projects/${projectId}/categories/${categoryId}/bots/Edit`),
            {
                headers: {...HEADERS_JSON, username},
            }
        );
    },

    fetchPreviewDocuments(prjid, catid) {
        return requestGet(
            routeHelper(`api/projects/${prjid}/categories/${catid}/files/staging`),
            {
                headers: HEADER_ACCEPT_JSON,
            },
        );
    },

    fetchPreviewData(prjid, catid, botid, documentId, pageIndex = 0) {
        return requestGet(
            routeHelper(`/api/projects/${prjid}/categories/${catid}/bots/${botid}/layout/${documentId}/pages/${pageIndex}/preview`),
            {
                headers: HEADER_ACCEPT_JSON,
            },
        );
    },
    fetchPreviewDataDocument(prjid, catid, botid, documentId) {
        return requestGet(
            routeHelper(`/api/projects/${prjid}/categories/${catid}/bots/${botid}/documents/${documentId}/preview`),
            {
                headers: HEADER_ACCEPT_JSON,
            },
        );
    },
    keepDesignerAlive(projectId, categoryId, visionBotId) {
        return requestPost(
            routeHelper(`api/projects/${projectId}/categories/${categoryId}/visionBot/${visionBotId}/keepalive`),
            {
                headers: HEADER_ACCEPT_JSON,
            },
        );
    },
};
