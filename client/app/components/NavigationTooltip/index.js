/**
*
* NavigationTooltip
*
*/

import React from 'react';

import './styles.scss';

const NavigationTooltip = ({children, label}) => (
    <div className="navigation-tooltip">
        {children}
        <div className="navigation-tooltip--message">
            <div className="arrow-outer"></div>
            <div className="arrow-inner"></div>
            <span>
                {label}
            </span>
        </div>
        <div className="navigation-tooltip--hover-overlay">

        </div>
    </div>
);

NavigationTooltip.displayName = 'NavigationTooltip';

export default NavigationTooltip;
