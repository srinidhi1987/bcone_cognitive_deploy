/**
*
* CircularProgressBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

const CircularProgressBar = ({progress, size, icon, barTheme}) => (
    <div className="aa-circular-progress-bar-container">
        <div className={classnames('aa-circular-progress-bar', barTheme, `p-${isNaN(progress) ? 0 : Math.floor(progress)}`, {
            'small-scale': size === 'small',
            'medium-scale': size === 'medium',
        })}></div>

        <div className="aa-circular-progress-bar-inner">
            {icon}
        </div>
    </div>
);

CircularProgressBar.displayName = 'CircularProgressBar';

CircularProgressBar.propTypes = {
    /**
     * Current progress to display in bar
     */
    progress: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    /**
     * Size of the progress bar
     */
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    /**
     * Render inner display in the progress bar
     */
    icon: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string,
    ]),
    /**
     * Set bar color based on theme
     * e.g. pink, teal, purple
     */
    barTheme: PropTypes.string,
};

CircularProgressBar.defaultProps = {
    progress: 0,
    size: 'large',
};

export default CircularProgressBar;
