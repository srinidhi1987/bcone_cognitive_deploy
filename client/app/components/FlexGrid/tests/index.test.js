import FlexGrid from '../index';

import {shallow} from 'enzyme';
import React from 'react';

describe('<FlexGrid />', () => {
    it('should render correct number of flex items', () => {
        // given
        // when
        const component = shallow(
            <FlexGrid>
                <FlexGrid.Item>
                    Testing
                </FlexGrid.Item>

                <FlexGrid.Item>
                    Testing
                </FlexGrid.Item>
            </FlexGrid>
        );
        // then
        expect(component.find('.aa-flex-grid-item').length).toEqual(2);
    });

    describe('props', () => {
        it('should add wrap secondary class if passed in', () => {
            // given
            // when
            const component = shallow(
                <FlexGrid wrap={true}>
                    <FlexGrid.Item>
                        Testing
                    </FlexGrid.Item>
                </FlexGrid>
            );
            // then
            expect(component.find('.aa-flex-grid.wrap').length).toEqual(1);
        });

        it('should NOT add wrap secondary class if NOT passed in', () => {
            // given
            // when
            const component = shallow(
                <FlexGrid>
                    <FlexGrid.Item>
                        Testing
                    </FlexGrid.Item>
                </FlexGrid>
            );
            // then
            expect(component.find('.aa-flex-grid.wrap').length).toEqual(0);
        });
    });

    describe('<FlexGrid.Item />', () => {
        let styles, component;

        beforeEach(() => {
            // given
            styles = {
                width: '50%',
                paddingLeft: '30px',
            };
            // when
            component = shallow(
                <FlexGrid>
                    <FlexGrid.Item itemStyle={styles}>
                        Testing
                    </FlexGrid.Item>

                    <FlexGrid.Item itemStyle={styles}>
                        Testing
                    </FlexGrid.Item>
                </FlexGrid>
            );
        });

        it('should pass in item override styles', () => {
            // then
            expect(component.find('.aa-flex-grid-item').first().props().style).toEqual(styles);
        });
    });
});
