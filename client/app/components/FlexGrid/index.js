/**
*
* FlexGrid
*
*/

import React, {Children} from 'react';
import PropTypes from 'prop-types';
import FlexGridItem from './item';
import classnames from 'classnames';

import './styles.scss';

/**
 * This component handles the grid system
 * built into flex box. You can create flex items
 * and pass sizing to each of the children.
 */
const FlexGrid = (props) => {
    let items = [];

    /**
     * Iterate through and find all flex item children
     */
    Children.forEach(props.children, (child) => {
        if (child && child.type.displayName === 'FlexGrid.Item') {
            const item = Object.keys(child.props).reduce((acc, key) => {
                if (key === 'children') {
                    acc.children = child.props.children;
                }
                else {
                    acc[key] = child.props[key];
                }

                acc['key'] = Math.random();

                return acc;
            }, {});

            items = [
                ...items,
                ...[
                    <div className="aa-flex-grid-item"
                         key={item.key}
                         style={item.itemStyle}
                    >
                        {item.children}
                    </div>,
                ],
            ];
        }
    });

    return (
        <div
            className={classnames('aa-flex-grid', props.className, {
                ...{wrap: props.wrap},
            })}
        >
            {items.map((item) => item)}
        </div>
    );
};

FlexGrid.propTypes = {
    /**
     * Array of FlexGrid.Item nodes are accepted
     */
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.element),
    ]).isRequired,
    /**
     * Set flex wrap settings
     */
    wrap: PropTypes.bool,
};

FlexGrid.displayName = 'FlexGrid';

FlexGrid.Item = FlexGridItem;

export default FlexGrid;
