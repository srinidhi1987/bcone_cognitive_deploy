/**
 * Created by iamcutler on 11/23/16.
 */

import React from 'react'; // eslint-disable-line no-unused-vars
import PropTypes from 'prop-types';

const FlexGridItem = () => null;

FlexGridItem.propTypes = {
    /**
     * Set override styles
     */
    itemStyle: PropTypes.object,
};

FlexGridItem.displayName = 'FlexGrid.Item';

export default FlexGridItem;
