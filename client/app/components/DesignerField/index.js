/**
*
* DesignerField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {DESIGNER_FIELD_THEMES} from '../../constants/enums';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const fieldThemeMap = {
    box: routeHelper('/images/box-icon.svg'),
    number: routeHelper('/images/number.svg'),
    text: routeHelper('/images/text.svg'),
    money: routeHelper('/images/money.svg'),
    calendar: routeHelper('/images/calendar.svg'),
    table: routeHelper('/images/table.svg'),
    success: routeHelper('/images/check.svg'),
    fail: routeHelper('/images/X.svg'),
};

const DesignerField = ({label, successful, theme, active, areaType, onClick}) => (
    <button
        onClick={onClick}
        className="right-view-body-fields-row"
        data-area-type={areaType} // temp for COG-2882
    >
        <div
            className={classnames('designer-field', 'aa-grid-row', {
                'designer-field--active': active,
            })}
        >
            <div className="field">
                <img
                    className={classnames('field--type', {
                        [theme]: true,
                    })}
                    src={fieldThemeMap[theme] || ''}
                />
                <div className="field--label aa-grid-row vertical-center horizontal-left">
                    {label}
                </div>
                <div
                    className={classnames('field--match', {
                        [successful ? 'success' : 'fail']: true,
                    })}
                ></div>
            </div>
        </div>
    </button>
);

DesignerField.displayName = 'DesignerField';

DesignerField.propTypes = {
    label: PropTypes.string.isRequired,
    successful: PropTypes.bool.isRequired,
    theme: PropTypes.oneOf([
        DESIGNER_FIELD_THEMES.MONEY,
        DESIGNER_FIELD_THEMES.TEXT,
        DESIGNER_FIELD_THEMES.DATE,
        DESIGNER_FIELD_THEMES.NUMBER,
        DESIGNER_FIELD_THEMES.TABLE,
    ]).isRequired,
    active: PropTypes.bool.isRequired,
};

export default DesignerField;
