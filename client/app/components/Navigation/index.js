/**
 *
 * Navigation
 *
 */

import React from 'react';
import {
    Navigation as CommonUINav,
    Icon,
} from 'common-frontend-components';
import routeHelper from '../../utils/routeHelper';

const Navigation = ({currentPath, isAdminUser, isValidatorUser}) => {
    if (isAdminUser) {
        return (
            <CommonUINav currentPath={currentPath || routeHelper()}>
                <CommonUINav.PrimaryOption
                    icon={(
                        <Icon aa="dashboard"/>
                    )}
                    title="Dashboards"
                    to={routeHelper('/dashboard')}
                    key="dashboard"
                />

                <CommonUINav.PrimaryOption
                    icon={(
                        <Icon fa="connectdevelop" />
                    )}
                    title="Learning Instances"
                    to={routeHelper('/learning-instances')}
                    key="projects"
                />

                <CommonUINav.PrimaryOption
                    icon={(
                        <Icon aa="bot" />
                    )}
                    title="Bots"
                    to={routeHelper('/bots')}
                    key="bots"
                />
            </CommonUINav>
        );
    }

    if (isValidatorUser) {
        return (
            <CommonUINav currentPath={currentPath || routeHelper()}>
                <CommonUINav.PrimaryOption
                    icon={(
                        <Icon fa="connectdevelop"/>
                    )}
                    title="Learning Instances"
                    to={routeHelper('/learning-instances/validations')}
                    key="Validations"
                />
            </CommonUINav>
        );
    }

    return null;
};

Navigation.displayName = 'Navigation';

export default Navigation;
