import Navigation from '../index';

import {shallow} from 'enzyme';
import React from 'react'; // eslint-disable-line no-unused-vars
import {Navigation as CommonUINav} from 'common-frontend-components';
import routeHelper from '../../../utils/routeHelper';

describe('<Navigation />', () => {
    describe('admin user', () => {
        // given
        // when
        const component = shallow(
            <Navigation
                isAdminUser="true"
            />
        );

        it('should render three navigation buttons', () => {
            // then
            expect(component.find(CommonUINav.PrimaryOption).length).toEqual(3);
        });

        describe('should pass correct props to', () => {
            describe('dashboards button', () => {
                const {title, to, icon} = component.find(CommonUINav.PrimaryOption).first().props();

                it('title', () => {
                    expect(title).toEqual('Dashboards');
                });

                it('to', () => {
                    expect(to).toEqual(routeHelper('/dashboard'));
                });

                it('icon set', () => {
                    expect(icon.props.aa).toEqual('dashboard');
                });
            });

            describe('projects button', () => {
                const {title, to, icon} = component.find(CommonUINav.PrimaryOption).at(1).props();

                it('title', () => {
                    expect(title).toEqual('Learning Instances');
                });

                it('to', () => {
                    expect(to).toEqual(routeHelper('/learning-instances'));
                });

                it('icon set', () => {
                    expect(icon.props.fa).toEqual('connectdevelop');
                });
            });

            describe('Bots button', () => {
                const {title, to, icon} = component.find(CommonUINav.PrimaryOption).at(2).props();

                it('title', () => {
                    expect(title).toEqual('Bots');
                });

                it('to', () => {
                    expect(to).toEqual(routeHelper('/bots'));
                });

                it('icon set', () => {
                    expect(icon.props.aa).toEqual('bot');
                });
            });
        });
    });

    describe('validator user', () => {
        // given
        // when
        const component = shallow(
            <Navigation
                isValidatorUser="true"
            />
        );

        it('should render one navigation button to validations', () => {
            // then
            expect(component.find(CommonUINav.PrimaryOption).length).toEqual(1);
        });

        describe('should pass correct props to', () => {
            describe('validations button', () => {
                const {title, to, icon} = component.find(CommonUINav.PrimaryOption).first().props();

                it('title', () => {
                    expect(title).toEqual('Learning Instances');
                });

                it('to', () => {
                    expect(to).toEqual(routeHelper('/learning-instances/validations'));
                });

                it('icon set', () => {
                    expect(icon.props.fa).toEqual('connectdevelop');
                });
            });
        });
    });
});
