export const InvalidReasons = {
    1: 'Fields missing',
    2: 'Tables missing',
    3: 'Wrong values',
};
