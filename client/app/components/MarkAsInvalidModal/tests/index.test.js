import React from 'react';
import {shallow} from 'enzyme';
import {
    CheckboxInput,
    Modal,
} from 'common-frontend-components';
import {themeType} from '../../ActionConfirmationModal';

import MarkAsInvalidModal from '../index';
import {InvalidReasons} from '../constants';
const props = {
    onClose: jest.fn(),
    onMarkAsInvalid: jest.fn(),
    show: true,
};

// given
describe(MarkAsInvalidModal.displayName, () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<MarkAsInvalidModal {...props} />);
    });

    it('should have a cancel button that calls onClose prop', () => {

        // when
        wrapper.instance().handleSelectInvalidReason(2, {target: {checked: true}});
        wrapper.instance().handleOnClose();
        // then
        expect(props.onClose).toHaveBeenCalled();
        expect(wrapper.state().invalidReasons).toEqual({1: false, 2: false, 3: false});
    });

    it('should have a function that sets a reason to be invalid', () => {
        expect(wrapper.state().invalidReasons[2]).toEqual(false);

        // when
        wrapper.instance().handleSelectInvalidReason(2, {target: {checked: true}});
        // then
        expect(wrapper.state().invalidReasons[2]).toEqual(true);
    });

    it('should have a mark as invalid button that calls onMarkAsInvalid prop fn with reasons marked invalid', () => {
        // when
        wrapper.instance().handleSelectInvalidReason(2, {target: {checked: true}});
        wrapper.instance().handleMarkAsInvalid();
        // then
        expect(props.onMarkAsInvalid).toHaveBeenCalledWith({1: false, 2: true, 3: false});
        expect(wrapper.state().invalidReasons).toEqual({1: false, 2: false, 3: false});
    });

    it('should set theme props to info for modal', () => {
        // when
        const modal = wrapper.find(Modal);
        // then
        expect(modal.props().theme).toEqual(themeType.info);
    });

    it('should show Modal when it recieves prop show=true', () => {
        // when
        const modal = wrapper.find(Modal);
        // then
        expect(modal.props().show).toEqual(true);
    });

    it('should not show Modal when it recieves prop show=false', () => {
        // when
        wrapper.setProps({show: false});

        const modal = wrapper.find(Modal);

        // then
        expect(modal.props().show).toEqual(false);
    });

    it('should have a checkbox for each reason to be invalid', () => {
        Object.keys(InvalidReasons).map((id) => {
            const expectedCheckbox = wrapper.findWhere((element) => element.is(CheckboxInput) && element.props().label === InvalidReasons[id]);

            expect(expectedCheckbox.length).not.toEqual(0);
        });
    });
});
