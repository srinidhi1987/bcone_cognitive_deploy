import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {autobind} from 'core-decorators';
import {
    CheckboxInput,
    CommandButton,
    Modal,
} from 'common-frontend-components';
import {themeType} from '../ActionConfirmationModal';
import {InvalidReasons} from './constants';
import {getCheckboxState} from '../../utils/forms';
import './styles.scss';

const invalidReasonsIds = Object.keys(InvalidReasons);

class MarkAsInvalidModal extends Component {
    static displayName = 'MarkAsInvalidModal';
    PropTypes = {
        /**
         * cancel click handler
         */
        onClose: PropTypes.func.isRequired,
        /**
         * confirm click handler
         */
        onMarkAsInvalid: PropTypes.func.isRequired,
        /**
         * Show deletion modal
         */
        show: PropTypes.bool.isRequired,
    };

    constructor() {
        super();
        this.state = {
            invalidReasons: {
                1: false,
                2: false,
                3: false,
            },
        };
    }

    clearSelectedInvalidReasons() {
        this.setState({invalidReasons: {1: false, 2: false, 3: false}});
    }

    @autobind handleOnClose() {
        const {onClose} = this.props;

        onClose();
        this.clearSelectedInvalidReasons();
    }


    @autobind handleMarkAsInvalid() {
        const {onMarkAsInvalid} = this.props;

        onMarkAsInvalid(this.state.invalidReasons);
        this.clearSelectedInvalidReasons();
    }

    /**
     * marks an invalid reaason as selected or not selected
     *
     * @param {string} id
     * @param {Object} targetProxy {target: {checked: boolean}}
     */
    @autobind handleSelectInvalidReason(id, {target: {checked}}) {
        const {invalidReasons} = this.state;

        this.setState({invalidReasons: {
            ...invalidReasons,
            [id]: checked,
        }});
    }

    render() {
        const {show} = this.props;

        return (
            <Modal
                show={show}
                theme={themeType.info}
                className="mark-as-invalid-modal"
            >
                <div className="mark-as-invalid-modal--body">
                    <strong><h3 className="mark-as-invalid-modal--body--title">Please choose the reason of invalidation.</h3></strong>
                    <div className="mark-as-invalid-modal--body--sub-title">The document is invalid because (optional):</div>
                    <div className="mark-as-invalid-modal--body--checkboxes">
                        {
                            invalidReasonsIds.map((id) => (
                                <CheckboxInput
                                    key={id}
                                    label={InvalidReasons[id]}
                                    checked={getCheckboxState(this.state.invalidReasons[id])}
                                    onChange={this.handleSelectInvalidReason.bind(null, id)}
                                />
                            ))
                        }
                    </div>
                    <div className="mark-as-invalid-modal--body--buttons">
                        <CommandButton
                            onClick={this.handleOnClose}
                            theme={themeType.info}
                        >
                            Cancel
                        </CommandButton>
                        <CommandButton
                            onClick={this.handleMarkAsInvalid}
                            theme={themeType.info}
                            recommended
                        >
                            Mark as Invalid
                        </CommandButton>
                    </div>
                </div>
            </Modal>
        );
    }
}

export default MarkAsInvalidModal;
