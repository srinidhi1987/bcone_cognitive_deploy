

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {PageTitle} from 'common-frontend-components';

import {
    FIELD_OPTIONS,
} from '../../constants/enums';
import classnames from 'classnames';
import routeHelper from '../../utils/routeHelper';

import '../DesignerPageWizardSelectedFieldDetails/styles.scss';

const {
    FOOTER,
} = FIELD_OPTIONS;

class DesignerPageWizardSelectedTableDetails extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DesignerPageWizardSelectedTableDetails';
    constructor(props) {
        super(props);
    }

    state = {
        optionsMenuOpened: false,
    };

    render() {
        const {
            activeField,
            activeFieldOption,
            isComplete,
            selectActiveFieldOptionHandler,
            setFieldValueHandler,
        } = this.props;

        const checkMarkUrl = routeHelper(`/images/status-${isComplete ? 'success' : 'todo'}.svg`);
        const formattedFieldName = activeField.Name.replace(/_/g, ' ');

        const mappedColumns = activeField.Columns.filter((field) => field.isMapped);

        return (
            <div className="designer-page-wizard-selected-field-details">
                <div className="designer-page-wizard-selected-field-details--body">

                    <div className="right-view-body-classes">
                        <div>
                            <div className="right-view-body-fields-row" style={{paddingTop: 0}}>
                                <div className="aa-grid-row vertical-center space-between">
                                    <PageTitle label={formattedFieldName} />
                                    <div className="designer-page-wizard-selected-field-details--icon-background">
                                    <img src={checkMarkUrl} />
                                    </div>
                                </div>
                            </div>
                            <div className="right-view-body-fields-row">
                                <label>
                                    Primary Column
                                    <div
                                        className={classnames(
                                        'designer-class',
                                        'aa-grid-row',
                                            {
                                                'designer-class--active': true,
                                            },
                                        )}
                                    >
                                        <select
                                            value={activeField.refColumn}
                                            onChange={(event) => setFieldValueHandler(activeField, 'refColumn', event.target.value)}
                                        >
                                            {
                                                mappedColumns.map((column, index) => {
                                                    return (
                                                        <option
                                                            key={index}
                                                            value={column.Name}
                                                        >
                                                            {column.Name}
                                                        </option>
                                                    );
                                                })
                                            }
                                        </select>
                                    </div>
                                </label>
                            </div>
                            <div className="right-view-body-fields-row">
                                <label>
                                   End of Table Indicator
                                    <div className="aa-grid-row">
                                        <div
                                            className={classnames('standard-input', 'field-option-input-margin', {
                                                'standard-input--active' : activeFieldOption === FOOTER,
                                            })}
                                        >
                                            <input
                                                className="footer-input"
                                                onClick={() => selectActiveFieldOptionHandler(FOOTER)}
                                                value={activeField.Footer}
                                            />
                                        </div>
                                    </div>
                                </label>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

DesignerPageWizardSelectedTableDetails.propTypes = {
    activeField: PropTypes.object.isRequired,
    activeFieldOption: PropTypes.string.isRequired,
    mode: PropTypes.number.isRequired,
    selectActiveFieldOptionHandler: PropTypes.func.isRequired,
    setFieldValueHandler: PropTypes.func.isRequired,
};

export default DesignerPageWizardSelectedTableDetails;
