import React from 'react';
import {shallow} from 'enzyme';

import DesignerPageWizardSelectedTableDetails from '../index';
import {ANNOTATION_MODES} from '../../AnnotationEngine/constants/enums';
import {column1, column2} from '../../../../fixtures/designer';
import routeHelper from '../../../utils/routeHelper';

describe(`Component: ${DesignerPageWizardSelectedTableDetails.displayName}`, () => {
    // state descriptions
    const {
        WHEN_NOT_DRAWING_FOOTER,
        WHEN_MAPPED,
        WHEN_NOT_MAPPED,
    } = {
        WHEN_DRAWING_FOOTER: 'when is drawing footer',
        WHEN_NOT_DRAWING_FOOTER: 'when is not drawing footer',
        FOOTER_MAPPED: 'when footer is mapped',
        FOOTER_NOT_MAPPED: 'when footer is not mapped',
        HAS_REFERENCE_COLUMN: 'has reference column',
    };

    // component section names
    const {
        DRAW_ICON_SELECTED_BORDER,
    } = {
        SELECT_REF_COLUMN: 'primary-column-select',
        DRAW_ICON_SELECTED_BORDER: '.select-draw-icon--selected',
    };

    // checkmark image url's
    const {UNMAPPED_CHECKMARK_TODO, MAPPED_CHECKMARK_SUCCESS} = {
        UNMAPPED_CHECKMARK_TODO: routeHelper('/images/status-todo.svg'),
        MAPPED_CHECKMARK_SUCCESS: routeHelper('/images/status-success.svg'),
    };

    // handlers
    const {
        fieldOptionDrawToolClickHandler,
        selectActiveFieldOptionHandler,
        setFieldValueHandler,
    } = {
        fieldOptionDrawToolClickHandler: jest.fn(),
        selectActiveFieldOptionHandler: jest.fn(),
        setFieldValueHandler: jest.fn(),
    };

    const Columns = [column1, column2];

    const props = {
        activeField: {
            Columns,
            Footer: '',
            Id: '0',
            IsDeleted: false,
            Name: 'Table_1',
            ValueType: 4,
            areaType: 'table',
            isActive: true,
            mappedSirFooter: null,
            refColumn: Columns[0].Name,
        },
        activeFieldOption: 'Footer',
        isComplete: false,
        mode: ANNOTATION_MODES.FREE,
        fieldOptionDrawToolClickHandler,
        selectActiveFieldOptionHandler,
        setFieldValueHandler,
    };

    const wrapper = shallow(<DesignerPageWizardSelectedTableDetails {...props} />);
    // given
    describe(WHEN_NOT_DRAWING_FOOTER, () => {
        // when
        describe(DRAW_ICON_SELECTED_BORDER, () => {
            // when
            const selectedBorder = wrapper.find(DRAW_ICON_SELECTED_BORDER);
            // then
            it('should not should show that draw is selected', () => {
                expect(selectedBorder.exists()).toEqual(false);
            });
        });
    });

    // given
    describe(WHEN_MAPPED, () => {
        // when
        describe(UNMAPPED_CHECKMARK_TODO, () => {

            // when
            const checkMarkTodo = wrapper.find(`img[src="${UNMAPPED_CHECKMARK_TODO}"]`);

            // then
            it('should have a todo checkmark', () => {
                expect(checkMarkTodo.exists()).toEqual(true);
            });
        });

        // when
        describe(MAPPED_CHECKMARK_SUCCESS, () => {
            // when
            const checkMarkSuccess = wrapper.find(`img[src="${MAPPED_CHECKMARK_SUCCESS}"]`);

            // then
            it('should not have a success checkmark', () => {
                expect(checkMarkSuccess.exists()).toEqual(false);
            });
        });
    });

    // given
    describe(WHEN_NOT_MAPPED, () => {
        // when
        wrapper.setProps({isComplete: true});

        // when
        describe(UNMAPPED_CHECKMARK_TODO, () => {
            // when
            const checkMarkTodo = wrapper.find(`img[src="${UNMAPPED_CHECKMARK_TODO}"]`);

            // then
            it('should have no todo checkmark', () => {
                expect(checkMarkTodo.exists()).toEqual(false);
            });
        });

        // when
        describe(MAPPED_CHECKMARK_SUCCESS, () => {
            // when
            const checkMarkSuccess = wrapper.find(`img[src="${MAPPED_CHECKMARK_SUCCESS}"]`);

            // then
            it('should not have a success checkmark', () => {
                expect(checkMarkSuccess.exists()).toEqual(true);
            });
        });
    });
});
