/**
*
* PercentRing
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const PercentRing = ({percentage}) => (
    <div className="aa-percent-ring-container">
        <div className="aa-percent-ring-container--outer"></div>
        <div className="aa-percent-ring-container--inner">
            <span className="aa-percent-ring-display">
                {percentage}%
            </span>
        </div>
    </div>
);

PercentRing.displayName = 'PercentRing';

PercentRing.propTypes = {
    percentage: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
};

PercentRing.defaultProps = {
    percentage: 0,
};

export default PercentRing;
