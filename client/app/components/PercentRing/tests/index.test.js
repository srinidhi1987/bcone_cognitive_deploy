import React from 'react';
import {shallow} from 'enzyme';

import PercentRing from '../index';

describe('<PercentRing />', () => {
    const percent = 40;
    const wrapper = shallow(
        <PercentRing
            percentage={percent}
        />
    );

    it('should have an outer ring', () => {
        // given
        // when
        const result = wrapper.find('.aa-percent-ring-container--outer');
        // then
        expect(result.length).toBe(1);
    });

    it('should have an inner ring', () => {
        // given
        // when
        const result = wrapper.find('.aa-percent-ring-container--inner');
        // then
        expect(result.length).toBe(1);
    });

    it('should display percentage from prop', () => {
        // given
        // when
        const result = wrapper.find('.aa-percent-ring-display');
        // then
        expect(result.text()).toEqual(`${percent}%`);
    });
});
