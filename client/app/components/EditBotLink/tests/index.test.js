import React from 'react';
import {shallow} from 'enzyme';

import EditBotLink from '../index';
import LinkToDesigner from '../../LinkToDesigner';

describe('<EditBotLink />', () => {
    const appConfig = {authToken: '23432423324', designer: true, encrypted: 'dfsfh3hkj343424'};
    const categoryId = '12345';
    const categoryName = 'category 1';
    const projectId = '12';
    const wrapper = shallow(
        <EditBotLink
            appConfig={appConfig}
            categoryId={categoryId}
            categoryName={categoryName}
            isNotInProduction={false}
            projectId={projectId}
        />
    );

    it('should not show linkToDesigner in production', () => {
        // given
        // when
        const result = wrapper.find('li').children().length;

        expect(result).toEqual(0);
    });

    describe('EditBotLink not in production', () => {
        const wrapper2 = shallow(
            <EditBotLink
                appConfig={appConfig}
                categoryId={categoryId}
                categoryName={categoryName}
                isNotInProduction={true}
                projectId={projectId}
            />
        );

        it('show LinkToDesigner if not in production', () => {
            // given
            // when
            const linkToDesigner = wrapper2.find(LinkToDesigner);
            // then
            expect(linkToDesigner.props().categoryId).toEqual(categoryId);
            expect(linkToDesigner.props().projectId).toEqual(projectId);
            expect(linkToDesigner.props().categoryName).toEqual(categoryName);
        });
    });
});
