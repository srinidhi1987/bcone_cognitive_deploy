/**
*
* ActionConfirmationModel
*
*/
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import LinkToDesigner from '../LinkToDesigner';
import {ActionBar} from 'common-frontend-components';

class EditBotLink extends PureComponent {
    static displayName = 'EditBotLink';

    static propTypes = {
        appConfig: PropTypes.object,
        categoryId: PropTypes.string,
        categoryName: PropTypes.string,
        isNotInProduction: PropTypes.bool,
        projectId: PropTypes.string,
    };

    render() {
        const {appConfig, categoryId, categoryName, isNotInProduction, projectId} = this.props;

        return (
            <li className="edit-bot-container">
                {
                    isNotInProduction ? (
                        <LinkToDesigner
                            projectId={projectId}
                            categoryId={categoryId}
                            categoryName={categoryName}
                            appConfig={appConfig}
                        >
                            <ActionBar.Action
                                key="edit-bot"
                                label="Edit Bot"
                                fa="pencil"
                                onClick={() => {}} />
                        </LinkToDesigner>
                    ) : null
                }
            </li>
        );
    }
}

export default EditBotLink;
