/**
*
* LinkToDesigner
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import {UNCLASSIFIED_REGEX} from '../../containers/ProjectsContainer/constants';
import {constructDesignerProtocolHandler} from '../../utils/thickClientApps';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const LinkToDesigner = ({projectId, categoryId, categoryName, appConfig, children, disabled}) => (
    categoryId && categoryName && !UNCLASSIFIED_REGEX.test(categoryName) ? (
        appConfig.designer === true ? (
            <Link
                to={disabled ? '' : routeHelper(`/learning-instances/${projectId}/bots/${categoryId}/edit`)}
                className="aa-project-category-bot-link aa-link"
            >
                {children}
            </Link>

        ) : (
            <a
                className="aa-project-category-bot-link aa-link"
                onClick={(e) => {
                    if (navigator.msLaunchUri) {
                        navigator.msLaunchUri(constructDesignerProtocolHandler(appConfig.encrypted, appConfig.authToken, categoryId, projectId));
                        e.preventDefault();
                    }
                }}
                href={constructDesignerProtocolHandler(appConfig.encrypted, appConfig.authToken, categoryId, projectId)}
            >
                {children}
            </a>
        )
    ) : null
);

LinkToDesigner.displayName = 'LinkToDesigner';

LinkToDesigner.propTypes = {
    projectId: PropTypes.string.isRequired,
    categoryName: PropTypes.string,
    categoryId: PropTypes.string,
    visionBot: PropTypes.object,
    appConfig: PropTypes.shape({
        designer: PropTypes.bool.isRequired,
        authToken: PropTypes.string.isRequired,
        encrypted: PropTypes.string.isRequired,
    }),
    shouldLinkToWebDesigner: PropTypes.bool,
    children: PropTypes.node,
};

export default LinkToDesigner;
