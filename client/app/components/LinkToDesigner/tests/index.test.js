import React from 'react';
import {shallow} from 'enzyme';
import {Category1} from '../../../../fixtures/categories';
import {Link} from 'react-router';

import LinkToDesigner from '../index';
import routeHelper from '../../../utils/routeHelper';

describe('Component: LinkToDesigner', () => {
    // given
    const projectId = '23ihj23hj23kjh23';
    const appConfig = {
        encrypted: 'dsfsdfds',
        authToken: 'a8sd89sd89asd8sa=',
        designer: true,
    };

    describe('when there is a category prop that is NOT unclassified or yet to be classified', () => {
        // given
        const wrapper = shallow(
            <LinkToDesigner
                projectId={projectId}
                categoryId={Category1.id}
                categoryName={Category1.name}
                appConfig={appConfig}
                visionBot={{}}
            />
        );

        // when
        const link = wrapper.find(Link);

        it('should create a Link internally to the bot designer url with project and category url params', () => {
            // then
            expect(link.prop('to')).toEqual(routeHelper(`/learning-instances/${projectId}/bots/${Category1.id}/edit`));
        });
    });

    describe('when there is a category prop that is unclassified or yet to be classified', () => {
        it('should return a component instance with html null', () => {
            // given
            const wrapper1 = shallow(
                <LinkToDesigner
                    projectId={projectId}
                    categoryId={Category1.id}
                    categoryName={'Yet to be classified'}
                    appConfig={appConfig}
                />
            );

            const wrapper2 = shallow(
                <LinkToDesigner
                    projectId={projectId}
                    categoryId={Category1.id}
                    categoryName={'unclassified'}
                    appConfig={appConfig}
                />
            );

            const wrapper3 = shallow(
                <LinkToDesigner
                    projectId={projectId}
                    categoryId={Category1.id}
                    categoryName={'_-1'}
                    appConfig={appConfig}
                />
            );

            // when
            // then
            expect(wrapper1.html()).toEqual(null);
            expect(wrapper2.html()).toEqual(null);
            expect(wrapper3.html()).toEqual(null);
        });
    });
});
