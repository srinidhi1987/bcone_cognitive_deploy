/**
*
* DisplayPageNotifications
*
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

export class DisplayPageNotifications extends Component {
    state = {
        forceClose: false,
    };

    static displayName = 'DisplayPageNotifications';

    static propTypes = {
        /**
         * notification to present
         */
        notification: PropTypes.string,
        /**
         * close notification handler
         */
        onClose: PropTypes.func.isRequired,
    };

    componentWillReceiveProps(newProps) {
        if (newProps.notification) {
            this.setState({forceClose: false});
        }
    }

    handleClose() {
        const {onClose} = this.props;

        // set timeout is used to show text till
        // CSS animation is complete to avoid
        // race condition with the store
        this.setState({forceClose: true});
        setTimeout(() => {
            onClose();
        }, 500);
    }

    render() {
        const {notification} = this.props;
        const {forceClose} = this.state;

        return (
            <div
                id="page-notification"
                className={classnames({
                    hide: !notification || forceClose,
                })}
            >
                <svg id="page-notification-branding" viewBox="0 7 137 142" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1="0.143973565%" y1="49.987382%" x2="100.084538%" y2="49.987382%" id="linearGradient-1">
                            <stop stopColor="#FFDE17" offset="0%"></stop>
                            <stop stopColor="#FED219" offset="4%"></stop>
                            <stop stopColor="#FAAD1E" offset="17%"></stop>
                            <stop stopColor="#F78F22" offset="31%"></stop>
                            <stop stopColor="#F47825" offset="45%"></stop>
                            <stop stopColor="#F26727" offset="61%"></stop>
                            <stop stopColor="#F15D29" offset="78%"></stop>
                            <stop stopColor="#F15A29" offset="99%"></stop>
                        </linearGradient>
                        <linearGradient x1="-0.0115548003%" y1="49.9463942%" x2="99.9896347%" y2="49.9463942%" id="linearGradient-2">
                            <stop stopColor="#FFFFFF" offset="33%"></stop>
                            <stop stopColor="#FBFBFB" offset="37%"></stop>
                            <stop stopColor="#EEEEEE" offset="42%"></stop>
                            <stop stopColor="#D9D9D9" offset="47%"></stop>
                            <stop stopColor="#BCBBBB" offset="52%"></stop>
                            <stop stopColor="#969595" offset="58%"></stop>
                            <stop stopColor="#686666" offset="63%"></stop>
                            <stop stopColor="#332F30" offset="68%"></stop>
                            <stop stopColor="#231F20" offset="69%"></stop>
                            <stop stopColor="#FFFFFF" offset="93%"></stop>
                        </linearGradient>
                    </defs>
                    <g id="Layer_1_1_" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(0.816041, 7.688525)">
                        <g id="Group" fillRule="nonzero">
                            <path d="M133.64473,119.611475 C133.20639,118.842623 132.76805,118.183607 132.220125,117.414754 C132.11054,116.97541 131.781785,116.755738 131.562615,116.536066 C123.343741,108.737705 113.590678,102.806557 103.07052,98.9622951 L102.41301,97.204918 L102.41301,97.204918 L70.4141959,9.44590164 C68.7704212,4.50327869 64.387022,0.98852459 59.126943,0.549180328 L58.3598481,0.549180328 L57.5927533,0.549180328 C52.4422592,1.09836066 48.05886,4.61311475 46.3055003,9.44590164 L0.279808596,136.196721 C-0.377701286,137.844262 0.389393576,139.931148 2.25233824,140.590164 C3.67694298,141.24918 5.32071769,140.7 6.30698251,139.381967 C6.41656749,139.272131 6.52615247,139.162295 6.52615247,138.942623 C25.1555991,107.529508 62.9624173,93.1409836 97.700856,104.234426 L107.563504,130.485246 L107.782674,131.034426 C110.522299,138.393443 118.631587,142.127869 125.973781,139.491803 C133.315975,136.745902 137.041864,128.618033 134.411824,121.259016 C134.302239,120.709836 134.083069,120.160656 133.64473,119.611475 L133.64473,119.611475 Z M73.9209153,93.8 C51.5655793,93.8 29.9773382,102.586885 14.0875161,118.513115 L52.8805991,11.752459 C53.647694,9.33606557 55.8393936,7.46885246 58.4694331,7.24918033 L58.5790181,7.24918033 C61.2090576,7.46885246 63.4007572,9.33606557 64.1678521,11.752459 L94.8516465,96.4360656 C87.9477928,94.6786885 81.043939,93.8 73.9209153,93.8 Z M121.261627,133.780328 C118.083662,133.780328 115.234453,131.693443 114.248188,128.727869 L114.138603,128.508197 L113.700263,127.84918 L106.248484,107.74918 C113.371508,110.934426 119.946607,115.218033 125.864196,120.6 L125.864196,120.6 C129.04216,123.236066 129.590085,127.84918 127.069631,131.034426 C125.645026,132.681967 123.453326,133.780328 121.261627,133.780328 L121.261627,133.780328 Z" id="Shape" fill="url(#linearGradient-1)"></path>
                            <path d="M74.0305003,93.8 C51.6751643,93.8 30.0869232,102.586885 14.1971011,118.513115 L6.52615247,139.491803 C6.63573745,139.381967 6.74532243,139.272131 6.74532243,139.052459 C25.3747691,107.639344 63.1815873,93.2508197 97.920026,104.344262 L94.9612315,96.4360656 C88.0573778,94.6786885 81.153524,93.8 74.0305003,93.8 Z M133.8639,119.611475 C133.42556,118.842623 132.98722,118.183607 132.439295,117.414754 C132.32971,116.97541 132.000955,116.755738 131.781785,116.536066 C123.562911,108.737705 113.809848,102.806557 103.28969,98.9622951 L106.467654,107.639344 C113.590678,110.82459 120.165777,115.327869 125.973781,120.490164 L125.973781,120.490164 C127.727141,121.918033 128.82299,124.114754 128.82299,126.311475 L135.507674,126.311475 C135.507674,123.895082 134.959749,121.698361 133.8639,119.611475 Z" id="Shape" fill="url(#linearGradient-2)" opacity="0.3"></path>
                        </g>
                    </g>
                </svg>

                <div id="page-notification--body">
                    <div dangerouslySetInnerHTML={{__html: notification}}></div>

                    <button
                        type="button"
                        onClick={this.handleClose.bind(this)}
                    >
                        Got it!
                    </button>
                </div>
            </div>
        );
    }
}

export default DisplayPageNotifications;
