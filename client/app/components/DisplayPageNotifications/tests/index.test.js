import React from 'react';
import {shallow} from 'enzyme';

import DisplayPageNotifications from '../index';

describe('<DisplayPageNotifications />', () => {
    const notification = '<b>Hello World</b>';
    let wrapper, closeSpy;

    beforeEach(() => {
        // given
        closeSpy = jest.fn();
        wrapper = shallow(
            <DisplayPageNotifications
                notification={notification}
                onClose={closeSpy}
            />
        );
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render notification from prop and allow HTML injection', () => {
        // given
        // when
        const body = wrapper.find('#page-notification--body > div');
        // then
        expect(body.props()).toEqual({
            dangerouslySetInnerHTML: {__html: notification},
        });
    });

    describe('Acknowledge Button', () => {
        it('should have a label of Got it!', () => {
            // given
            // when
            const button = wrapper.find('button');
            // then
            expect(button.props().children).toEqual('Got it!');
        });

        it('should call close callback when clicked', (done) => {
            // given
            const button = wrapper.find('button');
            // when
            button.simulate('click');
            // then
            setTimeout(() => {
                expect(closeSpy).toHaveBeenCalled();
                done();
            }, 500);
        });
    });
});
