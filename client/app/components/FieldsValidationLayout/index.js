import React, {Component} from 'react';
import {autobind} from 'core-decorators';

import {
    CollapsiblePane,
    DataTable,
    ActionBar,
} from 'common-frontend-components';
import {DataField, DataType} from 'common-frontend-utils';

import './styles.scss';
import ValidatorField from '../ValidatorField';
import routeHelper from '../../utils/routeHelper';

class FieldsValidationLayout extends Component {

    static displayName = 'FieldsValidationLayout';

    constructor(props) {
        super(props);
        this.state = {
            formCollapsibleState: {
                0: true,
            },
            tableCollapsibleState: {
                0: true,
            },
        };
    }

    getFormCollapsibleState(key) {
        return this.state.formCollapsibleState[key];
    }

    toggleFormCollapsibleState(key) {
        return () => {
            const active = {
                ...this.state.formCollapsibleState,
                [key]: !this.getFormCollapsibleState(key),
            };
            this.setState({formCollapsibleState:active});
        };
    }

    getTableCollapsibleState(key) {
        return this.state.tableCollapsibleState[key];
    }

    toggleTableCollapsibleState(key) {
        return () => {
            const active = {
                ...this.state.tableCollapsibleState,
                [key] : !this.getTableCollapsibleState(key),
            };
            this.setState({tableCollapsibleState:active});
        };
    }

    getTablesDataFields({tables, selectFieldHandler, setTableFieldValue}) {
        const tablesDataFields = tables.map((table) => {
            const columnArray = table.columns.map(({name, displayName}) => {
                return new DataField({id: name, label: displayName, type: DataType.STRING, render: (value, data) => {
                    return (
                        <ValidatorField
                            field={data[name]}
                            selectFieldHandler={selectFieldHandler}
                            setFieldValueHandler={setTableFieldValue}
                            isFormField={false}
                            tableIndex={data.tableIndex}
                            rowIndex={data.id}
                            fieldIndex={data[name].fieldIndex}
                        />
                    );
                }});
            });
            return columnArray;
        });
        return tablesDataFields;
    }

    @autobind renderTables() {
        const {
            tables,
            insertFirstRow,
            tableActionOptions,
            optionSelectHandler,
        } = this.props;
        const tablesDataFields = this.getTablesDataFields(this.props);

        return tables.map((table, tableIndex) => (
            table.showTable ?
            <CollapsiblePane
                key={tableIndex}
                label={`Table ${tableIndex + 1}`}
                active={this.getTableCollapsibleState(tableIndex)}
                onActive={this.toggleTableCollapsibleState(tableIndex)}
            >
                <CollapsiblePane.Content>
                    <div className="field-box-container field-box-container--table">
                        <DataTable
                            data={table.rows.map((row, rowIndex) => {
                                return row.reduce((acc, field, fieldIndex) => {
                                    acc[field.name] = {...field, fieldIndex};
                                    return acc;
                                }, {id: rowIndex, tableIndex});
                            })}
                            dataId="id"
                            fields={tablesDataFields[tableIndex]}
                            actionsGenerator={(
                                {tableIndex, id}
                            ) => (
                                <ActionBar>
                                    <ActionBar.Action fa="level-up" label={tableActionOptions[0].title} onClick={() => optionSelectHandler(tableActionOptions[0], {tableIndex, rowIndex: id})}/>
                                    <ActionBar.Action fa="level-down" label={tableActionOptions[1].title} onClick={() => optionSelectHandler(tableActionOptions[1], {tableIndex, rowIndex: id})}/>
                                    {!table.restrictDeleteRow && <ActionBar.Action aa="action-delete" label={tableActionOptions[2].title} onClick={() => optionSelectHandler(tableActionOptions[2], {tableIndex, rowIndex: id})}/>}
                                </ActionBar>
                            )}
                        />
                        {table.rows.length === 0 &&
                            <div className="field-box-container--message">
                                <img
                                    onClick={() => insertFirstRow(tableIndex)}
                                    src={routeHelper('images/plus-icon.svg')}
                                />
                                    Add row to Table {tableIndex + 1}
                            </div>
                        }
                    </div>
                </CollapsiblePane.Content>
            </CollapsiblePane>
            : null
        ));
    }

    render() {
        const {
            fields,
            selectFieldHandler,
            setFieldValueHandler,
        } = this.props;

        return (
            <div>
                <CollapsiblePane
                    label="Fields"
                    active={this.getFormCollapsibleState(0)}
                    onActive={this.toggleFormCollapsibleState(0)}
                >
                    <CollapsiblePane.Content>
                    <div className="field-box-container">
                    {
                        fields.map((field, index) => {
                            return (
                                <ValidatorField
                                    key={field.fieldGuid}
                                    field={field}
                                    selectFieldHandler={selectFieldHandler}
                                    setFieldValueHandler={setFieldValueHandler}
                                    isFormField={true}
                                    fieldIndex={index}
                                />
                            );
                        })
                    }
                    </div>
                    </CollapsiblePane.Content>
                </CollapsiblePane>
                {this.renderTables()}
            </div>
        );
    }
}

export default FieldsValidationLayout;
