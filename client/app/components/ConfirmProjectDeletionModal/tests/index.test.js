import React from 'react';
import {shallow} from 'enzyme';

import ConfirmProjectDeletionModal from '../index';

describe('Component: ConfirmProjectDeletionModal', () => {
    const currentProject = {
        id: '567756675765',
        name: 'Testing',
    };
    const deleteSpy = jest.fn();
    const hideSpy = jest.fn();
    const wrapper = shallow(
        <ConfirmProjectDeletionModal
            currentProject={currentProject}
            show={true}
            onHide={hideSpy}
            onDelete={deleteSpy}
        />
    );

    describe('component method: _confirmDeleteProject', () => {
        afterEach(() => {
            deleteSpy.mockReset();
        });

        describe('confirmed deletion', () => {
            beforeEach(() => {
                wrapper.instance()._confirmDeleteProject(currentProject.id, 'test', 'test');
            });

            it('should call dispatch event if name matches the confirm input', () => {
                // given
                // when
                // then
                expect(deleteSpy).toHaveBeenCalledWith(currentProject.id);
            });

            it('should set showDeletionError state to false', () => {
                // given
                // when
                // then
                expect(wrapper.state().showDeletionError).toEqual(false);
            });

            it('should set deleteConfirmName state to null', () => {
                // given
                // when
                // then
                expect(wrapper.state().deleteConfirmName).toEqual(null);
            });
        });

        describe('unconfirmed deletion', () => {
            beforeEach(() => {
                wrapper.instance()._confirmDeleteProject(currentProject.id, 'testing', 'test');
            });

            it('should NOT call dispatch event if name does NOT match the confirm input', () => {
                // given
                // when
                // then
                expect(deleteSpy).not.toHaveBeenCalled();
            });

            it('should set showDeletionError state to true', () => {
                // given
                // when
                // then
                expect(wrapper.state().showDeletionError).toEqual(true);
            });
        });
    });

    describe('component method: _handleOnHide', () => {
        beforeEach(() => {
            wrapper.instance()._handleOnHide();
        });

        afterEach(() => {
            hideSpy.mockReset();
        });

        it('should call onHide prop callback', () => {
            // given
            // when
            // then
            expect(hideSpy).toHaveBeenCalled();
        });

        it('should set showDeletionError state to false', () => {
            // given
            // when
            // then
            expect(wrapper.state().showDeletionError).toEqual(false);
        });
    });
});
