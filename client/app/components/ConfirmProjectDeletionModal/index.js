/**
*
* ConfirmProjectDeletionModal
*
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    Announcement,
    CommandButton,
    Modal,
    TextInput,
} from 'common-frontend-components';

class ConfirmProjectDeletionModal extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'ConfirmProjectDeletionModal';

    PropTypes = {
        currentProject: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        }).isRequired,
        /**
         * Callback for deletion action
         */
        onDelete: PropTypes.func.isRequired,
        /**
         * Callback to hide modal
         */
        onHide: PropTypes.func.isRequired,
        /**
         * Show deletion modal
         */
        show: PropTypes.bool.isRequired,
    };

    defaultProps = {
        show: false,
    };

    state = {
        deleteConfirmName: null,
        showDeletionError: false,
    };

    /**
     * Confirm project deletion
     * @param {string} projectId
     * @param {string} projectName
     * @param {string} confirmProjectName
     * @returns void
     */
    _confirmDeleteProject(projectId, projectName, confirmProjectName) {
        const {onDelete} = this.props;

        // If confirmed name matches project name
        if (confirmProjectName === projectName) {
            this.setState({showDeletionError: false, deleteConfirmName: null});
            return onDelete(projectId);
        }

        this.setState({showDeletionError: true});
    }

    /**
     * Handle hiding the modal
     * @returns void
     */
    _handleOnHide() {
        const {onHide} = this.props;

        this.setState({showDeletionError: false});
        onHide();
    }

    render() {
        const {currentProject, show} = this.props;
        const {deleteConfirmName, showDeletionError} = this.state;

        return (
            <Modal
                show={show}
                style={{padding: 20}}
            >
                <div>
                    <h2>Are you absolutely sure?</h2>

                    <p>
                        This action <strong>CANNOT</strong> be undone. This will permanently delete the learning instance <strong>{currentProject.name}</strong>, bots, and all other associated data.<br /><br />
                        Please type in the name of the learning instance to confirm:
                    </p>

                    {(() => {
                        if (showDeletionError) {
                            return (
                                <Announcement
                                    theme="error"
                                    title="Name must match the learning instance!"
                                />
                            );
                        }
                    })()}

                    <TextInput
                        name="delete-confirm-name"
                        value={deleteConfirmName}
                        onChange={(deleteConfirmName) => this.setState({deleteConfirmName})}
                    />

                    <div className="aa-grid-row space-between">
                        <div className="aa-grid-column">
                            <CommandButton
                                onClick={() => {
                                    this.setState({deleteConfirmName: null});
                                    this._handleOnHide();
                                }}
                            >
                                Cancel
                            </CommandButton>
                        </div>

                        <div className="aa-grid-column">
                            <CommandButton
                                theme="error"
                                onClick={() => this._confirmDeleteProject(currentProject.id, currentProject.name, deleteConfirmName)}
                                disabled={!deleteConfirmName}
                                recommended
                            >
                                I understand, please delete
                            </CommandButton>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}

export default ConfirmProjectDeletionModal;
