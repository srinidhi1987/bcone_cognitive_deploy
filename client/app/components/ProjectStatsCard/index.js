/**
*
* ProjectStatsCard
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {FormattedNumber} from 'react-intl';
import {Link} from 'react-router';
import {Icon} from 'common-frontend-components';

import {MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE} from '../../containers/ProjectsContainer/constants';
import EnvironmentBadge from '../EnvironmentBadge';
import PercentageTag from '../PercentageTag';
import CircularProgressBar from '../CircularProgressBar';
import FlexGrid from '../FlexGrid';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const _renderProgressIconByStatus = (status) => {
    switch (status) {
        case 'training':
            return routeHelper('/images/training-icon.svg');
        case 'classified':
            return routeHelper('/images/classify-icon.svg');
        case 'inDesign':
            return routeHelper('/images/perf-review-icon.svg');
        default:
            return routeHelper('/images/training-icon.svg');
    }
};

const ProjectStatsCard = ({project}) => (
    <div className="aa-project-stats-card">
        <PercentageTag
            percentage={project.currentTrainedPercentage}
        />

        {(() => {
            if (project.environment === 'staging') {
                return (
                    <FlexGrid>
                        <FlexGrid.Item>
                            <header className="aa-project-stats-card-header">
                                <div className="aa-project-stats-card-header--name">
                                    {project.name}
                                </div>

                                <EnvironmentBadge>
                                    {project.environment}
                                </EnvironmentBadge>
                            </header>

                            <p className="aa-project-stats-card-description">
                                {project.description}
                            </p>

                            <ul className="aa-project-stats-card-stats">
                                <li>
                                    <span className="aa-project-stats-card-stats--title">Domain:</span>
                                    &nbsp;
                                    {project.projectType}
                                </li>
                                <li>
                                    <span className="aa-project-stats-card-stats--title">Files Uploaded:</span>
                                    &nbsp;
                                    <FormattedNumber value={project.numberOfFiles} />
                                </li>
                            </ul>
                        </FlexGrid.Item>

                        <FlexGrid.Item>
                            <CircularProgressBar
                                progress={project.currentTrainedPercentage}
                                icon={(
                                    <div className="aa-circular-progress-bar-inner-content">
                                        <img src={_renderProgressIconByStatus(project.projectState)} />
                                        <span>{project.projectState}</span>
                                    </div>
                                )}
                            />

                            {(() => {
                                if (project.currentTrainedPercentage < MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE) {
                                    return (
                                        <div className="aa-project-stats-card-trainthreshold">
                                            <Icon
                                                fa="exclamation-triangle"
                                            /> You're below the training threshold
                                        </div>
                                    );
                                }
                            })()}
                        </FlexGrid.Item>
                    </FlexGrid>
                );
            }

            return (
                <Link to={routeHelper(`/learning-instances/${project.id}/report`)}>
                    <header className="aa-project-stats-card-header">
                        {project.name}
                        <EnvironmentBadge>
                            {project.environment}
                        </EnvironmentBadge>
                    </header>

                    <div className="aa-project-stats-card-progress-bars">
                        <CircularProgressBar
                            progress={Math.floor(project.totalFilesProcessed * 100 / project.numberOfFiles)}
                            barTheme="teal"
                            size="medium"
                            icon={(
                                <div className="aa-project-stats-card-progress-metrics">
                                    <span className="aa-project-stats-card-progress-metrics--metric">
                                        <FormattedNumber
                                            value={project.totalFilesProcessed}
                                        />
                                    </span>

                                    <span className="aa-project-stats-card-progress-metrics--title">
                                        Files processed
                                    </span>
                                </div>
                            )}
                        />

                        <CircularProgressBar
                            progress={Math.floor(project.totalSTP)}
                            barTheme="pink"
                            size="medium"
                            icon={(
                                <div className="aa-project-stats-card-progress-metrics">
                                    <span className="aa-project-stats-card-progress-metrics--metric">
                                        {Math.floor(project.totalSTP)}%
                                    </span>

                                    <span className="aa-project-stats-card-progress-metrics--title">
                                        Straight-through processing
                                    </span>
                                </div>
                            )}
                        />

                        <CircularProgressBar
                            progress={Math.floor(project.totalAccuracy)}
                            barTheme="purple"
                            size="medium"
                            icon={(
                                <div className="aa-project-stats-card-progress-metrics">
                                    <span className="aa-project-stats-card-progress-metrics--metric">
                                        {Math.floor(project.totalAccuracy)}%
                                    </span>

                                    <span className="aa-project-stats-card-progress-metrics--title">
                                        Field accuracy
                                    </span>
                                </div>
                            )}
                        />
                    </div>
                </Link>
            );
        })()}
    </div>
);

ProjectStatsCard.displayName = 'ProjectStatsCard';

ProjectStatsCard.propTypes = {
    /**
     * Project data model
     */
    project: PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
        description: PropTypes.string,
        environment: PropTypes.oneOf(['staging', 'production']),
    }).isRequired,
};

ProjectStatsCard.defaultProps = {
    environment: 'staging',
    progress: 0,
};

export default ProjectStatsCard;
export {
    ProjectStatsCard,
    _renderProgressIconByStatus,
};
