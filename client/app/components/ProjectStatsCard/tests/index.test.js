// import React from 'react';
import {_renderProgressIconByStatus} from '../index';
import routeHelper from '../../../utils/routeHelper';

describe('<ProjectStatsCard />', () => {
    describe('_renderProgressIconByStatus should return icon URL based on project status', () => {
        it('should return correct icon from training status', () => {
            // given
            const status = 'training';
            // when
            const result = _renderProgressIconByStatus(status);
            // then
            expect(result).toBe(routeHelper('/images/training-icon.svg'));
        });

        it('should return correct icon from classified status', () => {
            // given
            const status = 'classified';
            // when
            const result = _renderProgressIconByStatus(status);
            // then
            expect(result).toBe(routeHelper('/images/classify-icon.svg'));
        });

        it('should return correct icon from inDesign status', () => {
            // given
            const status = 'inDesign';
            // when
            const result = _renderProgressIconByStatus(status);
            // then
            expect(result).toBe(routeHelper('/images/perf-review-icon.svg'));
        });
    });
});
