/* eslint-disable */
/**
*
* DesignerPageWizardMenu
*
*/

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {autobind} from 'core-decorators';

import {Icon} from 'common-frontend-components';
import DesignerPageWizardMenuItem from '../DesignerPageWizardMenuItem';

import {renderDoubleChevrons, mapFieldMenuItems} from '../../utils/designer'
import {DESIGNER_AREA_TYPES} from '../../constants/enums';

const {FIELD, COLUMN, TABLE} = DESIGNER_AREA_TYPES;

import './styles.scss';

class DesignerPageWizardMenu extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DesignerPageWizardMenu';

    state = {
        shouldHideMenuItems: false
    }

    constructor(props) {
        super(props);
    }

    render() {
        const {items, label, selectFieldHandler, menuType, tableSettings} = this.props;
        const {shouldHideMenuItems} = this.state;

        return (
            <div className="designer-page-wizard-menu">
                <div
                    className="designer-page-wizard-menu--header aa-grid-row vertical-center space-between"
                    onClick={this.toggleMenu}
                >
                    <div>
                        <span className="designer-page-wizard-menu--label" style={{padding: '10px'}}>{label}</span>
                    </div>
                    <button style={{padding: '9px'}}>
                        {renderDoubleChevrons(shouldHideMenuItems)}
                    </button>
                </div>
                <div className="designer-page-wizard-menu--items">
                    {this.renderFieldMenuItems({shouldHideMenuItems, items, selectFieldHandler, menuType, tableSettings})}
                </div>
            </div>
        );
    }

    /**
     * show or hide menu items
     */
    @autobind
    toggleMenu() {
        const {shouldHideMenuItems} = this.state;

        this.setState({shouldHideMenuItems: !shouldHideMenuItems})
    }

    renderFieldMenuItems({shouldHideMenuItems, items, selectFieldHandler, menuType, tableSettings}) {
        if (shouldHideMenuItems) {
            return null;
        }

        const settingsFields = tableSettings ? [tableSettings] : [];

        return settingsFields.concat(items).map((item, index) => { // eslint-disable-line react/display-name
            const areaType = menuType === FIELD ? menuType : index ? COLUMN : TABLE;

            return (
                <DesignerPageWizardMenuItem
                    item={item}
                    isComplete={item.isMapped}
                    selectFieldHandler={selectFieldHandler}
                    areaType={areaType}
                    key={index}
                />
            );
        });
    }
}

DesignerPageWizardMenu.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    label: PropTypes.string.isRequired,
    selectFieldHandler: PropTypes.func.isRequired,
    areaType: PropTypes.string.isRequired,
};

export default DesignerPageWizardMenu;
