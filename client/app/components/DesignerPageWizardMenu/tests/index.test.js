import React from 'react';
import {shallow} from 'enzyme';
import DesignerPageWizardMenu from '../index';
import {Icon} from 'common-frontend-components';
import {createNamingFunction} from '../../../utils/testHelpers';


describe(DesignerPageWizardMenu.displayName, () => {
    const getName = createNamingFunction(DesignerPageWizardMenu);

    // state descriptions
    const {MENU_CLOSED, MENU_OPEN} = {
        MENU_OPEN: 'when menu is open (initial state)',
        MENU_CLOSED: 'when menu is closed',
    };

    // component section names
    const {HEADER, LABEL, ITEMS, ARROWS} = {
        HEADER: 'header',
        LABEL: 'label',
        ITEMS: 'items',
        ARROWS: 'arrows',
    };

    // event descriptions
    const {WHEN_CLICKED} = {
        WHEN_CLICKED: 'when clicked',
    };

    const props = {
        items: [{}],
        label: 'Title', selectFieldHandler: jest.fn(),
        areaType: 'string',
    };

    // given
    describe(MENU_OPEN, () => {
        // when
        const wrapper = shallow(<DesignerPageWizardMenu {...props} />);
        const setStateSpy = jest.spyOn(wrapper.instance(), 'setState').mockImplementation(() => {});

        describe(ARROWS, () => {
            // when
            const iconWrapper = wrapper.find(Icon);
            // then
            it('should face upward', () => {
                expect(iconWrapper.props().className).toEqual('double-chevron-up');
            });
        });

        describe(HEADER, () => {
            // when
            describe(WHEN_CLICKED, () => {
                // when
                const headerWrapper = wrapper.find(getName(HEADER));
                headerWrapper.simulate('click');
                // then
                it('should call setState and set shouldHideMenuItems to true', () => {
                    expect(setStateSpy).toHaveBeenCalledWith({shouldHideMenuItems: true});
                });
            });
        });

        describe(ITEMS, () => {
            // when
            const itemsWrapper = wrapper.find(getName(ITEMS));
            // then
            it('should contain the items give to the component originally', () => {
                expect(itemsWrapper.children(0).length).toEqual(props.items.length);
            });
        });

        describe(LABEL, () => {
            // when
            const labelWrapper = wrapper.find(getName(LABEL));
            // then
            it('should display text of the prop label', () => {
                expect(labelWrapper.text()).toEqual(props.label);
            });
        });
    });

    // given
    describe(MENU_CLOSED, () => {
        // when
        const wrapper = shallow(<DesignerPageWizardMenu {...props} />);
        wrapper.setState({shouldHideMenuItems: true});
        const setStateSpy = jest.spyOn(wrapper.instance(), 'setState').mockImplementation(() => {});

        describe(HEADER, () => {
            // when
            describe(WHEN_CLICKED, () => {
                // when
                const headerWrapper = wrapper.find(getName(HEADER));
                headerWrapper.simulate('click');
                //then
                it('should call setState and set shouldHideMenuItems to false', () => {
                    expect(setStateSpy).toHaveBeenCalledWith({shouldHideMenuItems: false});
                });
            });
        });

        describe(ITEMS, () => {
            // when
            const itemsWrapper = wrapper.find(getName(ITEMS));
            // then
            it('should not have any children items displayed', () => {
                expect(itemsWrapper.children(0).length).toEqual(0);
            });
        });

        describe(ARROWS, () => {
            // when
            const icon = wrapper.find(Icon);
            // then
            it('should face downward', () => {
                expect(icon.props().className).toEqual('double-chevron-down');
            });
        });
    });


});
