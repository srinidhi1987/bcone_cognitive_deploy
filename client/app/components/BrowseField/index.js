import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {CommandButton, Popup} from 'common-frontend-components';

import './styles.scss';

/**
  * A text field input with focus/validation+error states.
  *
  * <div class="api-stability-level api-stability-level--volatile"></div>
  */
class BrowseField extends Component {
    static displayName = 'BrowseField';

    static propTypes = {
        /**
          * The placeholder value for an empty value.
          */
        placeholder: PropTypes.string,
        /**
          * Whether it is disabled.
          */
        disabled: PropTypes.bool,
        /**
          * Whether the data is valid.
          */
        valid: PropTypes.bool,
        /**
          * A validation error if there is one
          */
        error: PropTypes.string,
        /**
          * Whether the element is currently active.
          */
        active: PropTypes.bool,
        /**
          * Whether it has been previously touched.
          */
        touched: PropTypes.bool,
        /**
         * Allow multiple files to upload
         */
        multiple: PropTypes.bool,
        /**
         * Types accepts from upload
         */
        acceptTypes: PropTypes.string,
        /**
         * Input handlers and the current value.
         */
        input: PropTypes.shape({
            name: PropTypes.string,
            onBlur: PropTypes.func,
            onDragStart: PropTypes.func,
            onDrop: PropTypes.func,
            onFocus: PropTypes.func,
            onChange: PropTypes.func,
            value: PropTypes.any,
        }),
        /**
         * The current state of the field.
         */
        meta: PropTypes.shape({
            active: PropTypes.bool,
            touched: PropTypes.bool,
            disabled: PropTypes.bool,
            valid: PropTypes.bool,
            error: PropTypes.any,
        }),
    };

    static defaultProps = {
        valid: true,
        error: null,
        active: false,
        touched: false,
        disabled: false,
        placeholder: '',
    };

    constructor(props) {
        super(props);

        this.input = undefined;
        this.inputFacade = undefined;
    }

    componentDidMount() {
        const {placeholder} = this.props;

        // set initial placeholder value of facade input
        if (placeholder) this.inputFacade.innerHTML = placeholder;
    }

    _handleBrowseSelection(e) {
        // convert files to an array
        if (e.target.files.length) {
            const files = [ ...e.target.files ];

            // set value of facade input
            this.inputFacade.innerHTML = `You've selected ${files.length} file(s) to upload`;

            // Pass selected files in callback prop
            this.props.input.onChange(files);
        }
    }

    render() {
        const {
            multiple, acceptTypes, meta = {},
        } = this.props;
        const {active, disabled, valid, error} = meta;
        // Avoid warning about controlled vs. uncontrolled inputs.
        // See: https://gist.github.com/markerikson/d71cfc81687f11609d2559e8daee10cc
        // See: https://github.com/facebook/react/pull/5864

        /**
         * the meta property 'active' is not working properly with this component.
         * In short this means that errors will result in Popup showing until error state cleared.
         */
        const isInvalid = !valid || error;

        return (
            <div className="aa-grid-row vertical-center">
                <div className={classnames('browsefield', {
                    'browsefield--focus': active,
                    'browsefield--invalid': isInvalid,
                    'browsefield--disabled': disabled,
                })}>
                    <div className="browsefield-wrapper">
                        <div
                            className={classnames('browsefield-input', {
                                'browsefield-input--focus': active,
                                'browsefield-input--error': error,
                            })}
                            ref={(input) => {
                                this.inputFacade = input;
                            }}
                            onClick={() => this.input.click()}
                        >
                        </div>

                        <input
                            ref={(input) => {
                                this.input = input;
                            }}
                            type="file"
                            style={{display: 'none'}}
                            {...(multiple) ? {multiple} : {}}
                            {...(disabled) ? {disabled: 'disabled'} : {}}
                            accept={acceptTypes}
                            onDrop={this._handleBrowseSelection.bind(this)}
                            onChange={this._handleBrowseSelection.bind(this)}
                        />
                    </div>

                    <Popup prefer={[Popup.TOP]} theme="error" hover={error}>
                        {error}
                    </Popup>
                </div>
                <CommandButton recommended onClick={() => this.input.click()}>
                    Browse...
                </CommandButton>
            </div>
        );
    }
}

export default BrowseField;
