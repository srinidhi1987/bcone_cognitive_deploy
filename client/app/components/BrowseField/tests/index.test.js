import React from 'react';
import {mount} from 'enzyme';

import BrowseField from '../index.js';

describe('components/BrowseField', () => {
    const handleChange = jest.fn().mockImplementation(() => {});
    const wrapper = mount(
        <BrowseField
            label="Upload files from"
            multiple={true}
            // input is passed via redux form components
            input={{onChange: handleChange}}
        />
    );

    it('Should handle onChange', () => {
        wrapper.find('input').simulate('change', {
            target: {
                files: {
                    0: {name:''},
                    1: {name:''},
                    length: 2,
                },
            },
        });

        expect(handleChange.mock.calls.length).toEqual(1);
    });
});
