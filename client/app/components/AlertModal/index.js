import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {autobind} from 'core-decorators';
import {
    CommandButton,
    Modal,
} from 'common-frontend-components';
import {themeType} from '../ActionConfirmationModal';
import './styles.scss';

class AlertModal extends Component {
    static displayName = 'SaveVisionBotConfirmationModal';
    PropTypes = {
        /**
         * cancel click handler
         */
        closeHandler: PropTypes.func.isRequired,
        /**
         * Show deletion modal
         */
        show: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,
        paragraphs: PropTypes.array,
    };

    @autobind _closeHandler() {
        const {closeHandler} = this.props;
        closeHandler();
    }

    render() {
        const {
            paragraphs,
            show,
            title,
        } = this.props;

        return (
            <Modal
                show={show}
                theme={themeType.info}
                className="alert-modal"
            >
                <div className="alert-modal--body">
                    <h3 className="title">{title}</h3>
                    {paragraphs && paragraphs.map((p) => <p className="paragraph">{p}</p>)}
                    <div className="aa-grid-row horizontal-center">
                        <CommandButton
                            onClick={this._closeHandler}
                            theme={themeType.info}
                            recommended
                        >
                            Close
                        </CommandButton>
                    </div>
                </div>
            </Modal>
        );
    }
}

export default AlertModal;
