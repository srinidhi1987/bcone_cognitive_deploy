import React from 'react';
import {shallow} from 'enzyme';
import AlertModal from '../index';
import {
    Modal,
} from 'common-frontend-components';
import {themeType} from '../../ActionConfirmationModal';
describe('Component: ActionConfirmationModal', () => {
    const closeSpy = jest.fn();
    const title = 'There are no files available for validation.';
    const wrapper = shallow(
        <AlertModal
            show={true}
            closeHandler={closeSpy}
            title={title}
        />
    );

    it('should receive all required properties', () => {
        //when
        const {title, show} = wrapper.props();
        //then
        expect(show).toEqual(true);
        expect(title).toEqual(title);
    });

    it('should set show props to true for Modal', () => {
        const modal = wrapper.find(Modal);
        expect(modal.props().show).toEqual(true);
    });

    it('should set theme props to info for modal', () => {
        const modal = wrapper.find(Modal);
        expect(modal.props().theme).toEqual(themeType.info);
    });
});
