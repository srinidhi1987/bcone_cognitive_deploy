/**
*
* Validate Project Button
*
*/
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import {ActionBar} from 'common-frontend-components';

class ValidateProjectButton extends PureComponent {
    static displayName = 'ValidateProjectButton';

    static PropTypes = {
        appConfig: PropTypes.object,
        authToken: PropTypes.string,
        environment: PropTypes.string,
        id: PropTypes.string,
        onClick: PropTypes.func,
    };

    render() {
        return (
            <li>
                <ActionBar.Action
                    label="View instance details"
                    fa="binoculars"
                    onClick={this.props.onClick} />
            </li>
        );
    }
}

export default ValidateProjectButton;
