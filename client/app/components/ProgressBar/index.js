/**
*
* ProgressBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

function ProgressBar({current}) {
    return (
        <div className="aa-progress-bar">
            <div className="aa-progress-bar-current">{Math.floor(current)}%</div>

            <div className="aa-progress-bar-fill" style={{width: `${Math.floor(current)}%`}}></div>
        </div>
    );
}

ProgressBar.displayName = 'ProgressBar';

ProgressBar.propTypes = {
    /**
     * Current progress amount
     */
    current: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
};

ProgressBar.defaultProps = {
    current: 0,
};

export default ProgressBar;
