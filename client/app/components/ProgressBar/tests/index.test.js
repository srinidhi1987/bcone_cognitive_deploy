import ProgressBar from '../index';

import {shallow} from 'enzyme';
import React from 'react';

describe('<ProgressBar />', () => {
    describe('props', () => {
        it('default current should be 0', () => {
            // given
            // when
            const component = shallow(<ProgressBar />);
            // then
            expect(component.find('.aa-progress-bar-current').text()).toEqual('0%');
        });
    });

    describe('rendered', () => {
        // given
        const current = 20;
        const component = shallow(<ProgressBar current={current} />);

        it('should show current progress percent count', () => {
            // given
            // when
            // then
            expect(component.find('.aa-progress-bar-current').text()).toEqual(`${current}%`);
        });

        it('should set width of fill bar based on current progress', () => {
            // given
            // when
            // then
            expect(component.find('.aa-progress-bar-fill').props().style.width).toEqual(`${current}%`);
        });
    });
});
