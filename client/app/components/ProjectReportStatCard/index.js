/**
*
* ProjectReportStatCard
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {FormattedNumber} from 'react-intl';
import classnames from 'classnames';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const ProjectReportStatCard = ({metric = 0, title, metricType, theme, loading}) => (
    <div className="aa-project-report-stat-card">
        <span className={classnames('aa-project-report-stat-card--metric', theme)}>
            {(() => {
                if (loading) {
                    return (
                        <img className="aa-project-report-stat-card-loading" src={routeHelper('images/ring-loader.gif')} />
                    );
                }

                if (metricType === 'percentage') {
                    return `${metric}%`;
                }

                return (
                    <FormattedNumber
                        value={metric}
                    />
                );
            })()}
        </span>

        <span className="aa-project-report-stat-card--title">
            {title}
        </span>
    </div>
);

ProjectReportStatCard.displayName = 'ProjectReportStatCard';

ProjectReportStatCard.propTypes = {
    /**
     * Metric shown for category
     */
    metric: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    /**
     * Display by metric type
     */
    metricType: PropTypes.oneOf(['number', 'percentage']),
    /**
     * Title description the metric
     */
    title: PropTypes.string.isRequired,
    /**
     * Show loading state
     */
    loading: PropTypes.bool,
    /**
     * Show metric color based on design theme
     */
    theme: PropTypes.oneOf(['purple']),
};

ProjectReportStatCard.defaultProps = {
    metric: 0,
    metricType: 'number',
    loading: false,
};

export default ProjectReportStatCard;
