import React from 'react';
import {FormattedNumber} from 'react-intl';
import {shallow} from 'enzyme';

import ProjectReportStatCard from '../index';

describe('<ProjectReportStatCard />', () => {
    // given
    const metric = '1000';
    const title = 'Uploaded files';
    const wrapper = shallow(
        <ProjectReportStatCard
            metric={metric}
            title={title}
        />
    );

    it('should format metric', () => {
        // given
        // when
        const result = wrapper.find(FormattedNumber);
        // then
        expect(result.props().value).toEqual(metric);
    });

    it('should display title of metric', () => {
        // given
        // when
        const result = wrapper.find('.aa-project-report-stat-card--title');
        // then
        expect(result.props().children).toEqual(title);
    });
});
