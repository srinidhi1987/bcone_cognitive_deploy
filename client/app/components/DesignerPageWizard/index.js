/* eslint-disable */
/**
*
* DesignerPageWizard
*
*/

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
import DesignerPageHeader from '../DesignerPageHeader'
import DesignerPageControlBar from '../DesignerPageControlBar';
import DesignerPageWizardFields from '../DesignerPageWizardFields';
import PageLoadingOverlay from '../PageLoadingOverlay/index';
import VisionBotControlBar from '../DesignerPageControlBar/index';
import DesignerPageWizardSelectedItemDetail from '../DesignerPageWizardSelectedItemDetails';
import {CommandButton, Breadcrumbs, SplitLayout} from 'common-frontend-components';
import {NavigationDirections} from '../../constants/enums';

export class DesignerPageWizard extends PureComponent {
    render () {
        const {
            activeField,
            activeFieldOption,
            botCanNavigate,
            fieldColumnTopOffset,
            fieldOptionDrawToolClickHandler,
            fields,
            isSavingEditData,
            mode,
            navigateNextBotsHandler,
            navigateDocumentsHandler,
            previewDocumentHandler,
            tables,
            selectActiveFieldOptionHandler,
            selectFieldHandler,
            selectedFieldType,
            setFieldValueHandler,
            toggleFieldCheckboxHandler,
            wizardElementIds,
        } = this.props;

        if (isSavingEditData) {
            return (
                <PageLoadingOverlay
                    label={'Saving designer data...'}
                    show={true}
                    fullscreen={false}
                />
            );
        }

        return (
            <div className="designer-page-wizard aa-grid-column">
                {/*<DesignerPageHeader/>*/}
                {
                    <div className="designer-page-wizard--body">
                        <SplitLayout
                            xs="right"
                            sm="horizontal"
                            gutter={0}
                            defaultSize={45}
                            minimumSize="20px"
                            maximumSize={90}
                            expand="right"
                        >
                            <SplitLayout.Left>
                                <DesignerPageWizardFields fields={fields} tables={tables} selectFieldHandler={selectFieldHandler}/>
                            </SplitLayout.Left>
                            <SplitLayout.Right>
                                <DesignerPageWizardSelectedItemDetail
                                    activeField={activeField}
                                    activeFieldOption={activeFieldOption}
                                    fieldColumnTopOffset={fieldColumnTopOffset}
                                    fieldOptionDrawToolClickHandler={fieldOptionDrawToolClickHandler}
                                    fields={fields}
                                    isComplete={activeField.isMapped}
                                    isTableSettings={Boolean(activeField.Columns)}
                                    mode={mode}
                                    selectActiveFieldOptionHandler={selectActiveFieldOptionHandler}
                                    selectFieldHandler={selectFieldHandler}
                                    selectedFieldType={selectedFieldType}
                                    setFieldValueHandler={setFieldValueHandler}
                                    tables={tables}
                                    toggleFieldCheckboxHandler={toggleFieldCheckboxHandler}
                                    wizardElementIds={wizardElementIds}
                                />
                            </SplitLayout.Right>
                        </SplitLayout>
                    </div>
                }
                <VisionBotControlBar>
                    <div className="training-control-bar aa-grid-row space-between">
                    <div className="training-control-bar-button-group aa-grid-row horizontal-right vertical-center">
                            <button onClick={previewDocumentHandler}>Preview</button>
                            <button
                               className="previous-next-button"
                               disabled={!botCanNavigate[NavigationDirections.NEXT]}
                               onClick={navigateNextBotsHandler}
                            >
                                Next group &#62;
                            </button>
                        </div>
                    </div>
                </VisionBotControlBar>
            </div>
        );
    }
}

DesignerPageWizard.displayName = 'DesignerPageWizard';
DesignerPageWizard.propTypes = {
    activeField: PropTypes.object.isRequired,
    activeFieldOption: PropTypes.string.isRequired,
    documentPreviewHandler: PropTypes.func.isRequired,
    fieldColumnTopOffset: PropTypes.number.isRequired,
    fieldOptionDrawToolClickHandler: PropTypes.func.isRequired,
    fields: PropTypes.arrayOf(PropTypes.object).isRequired,
    mode: PropTypes.number.isRequired,
    selectActiveFieldOptionHandler: PropTypes.func.isRequired,
    selectFieldHandler: PropTypes.func.isRequired,
    selectedFieldType: PropTypes.string.isRequired,
    setFieldValueHandler: PropTypes.func.isRequired,
    tables: PropTypes.arrayOf(PropTypes.object).isRequired,
    toggleFieldCheckboxHandler: PropTypes.func.isRequired,
};

export default DesignerPageWizard;
