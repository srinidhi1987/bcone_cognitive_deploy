/**
*
* ProjectReportTotal
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {FormattedNumber} from 'react-intl';
import ProjectReportTotalProcessBar from '../ProjectReportTotalProcessBar';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const getTrendingDiff = (current = 0, previous = 0) => {
    return Math.round(current >= previous ? current % previous : previous % current);
};

const getProgressPercentageByType = (type, metric, prevMetric, comparable = null) => {
    if (type === 'percentage') return metric;
    if (metric === 0 && (comparable === 0 || prevMetric === 0)) return 0; // return 0 if value and comparable are 0

    return Math.floor(metric / (comparable || prevMetric) * 100);
};

const ProjectReportTotal = ({metric, previousMetric, compareMetric, title, icon, metricType, loading}) => {
    const trending = getTrendingDiff(metric, compareMetric || previousMetric);

    if (loading) {
        return (
            <div className="aa-project-report-totals-comparison">
                <div className="aa-project-report-totals-comparison-loading">
                    <img src={routeHelper('images/ring-loader.gif')} />
                </div>
            </div>
        );
    }

    return (
        <div className="aa-project-report-totals-comparison">
            <div>
                <div className="aa-project-report-totals-comparison-icon">
                    {icon}
                </div>

                {(() => {
                    if (compareMetric && previousMetric) {
                        return (
                            <div className="aa-project-report-totals-comparison-trend">
                                <div className="aa-project-report-totals-comparison-trend--arrow">
                                    {(() => {
                                        if (metric > previousMetric) {
                                            return (
                                                <svg width="13px" height="13px" viewBox="-1 0 13 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                                    <path d="M7.45833333,7.5 L10.5878992,7.5 L5.5,0 L0.412100753,7.5 L3.54166667,7.5 L3.54166667,12 L7.45833333,12 L7.45833333,7.5 Z" id="Combined-Shape" stroke="none" fill="#72C7A0" fillRule="evenodd"></path>
                                                </svg>
                                            );
                                        }

                                        return (
                                            <svg width="13px" height="13px" viewBox="-1 0 13 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                                <path d="M7.45833333,7.5 L10.5878992,7.5 L5.5,0 L0.412100753,7.5 L3.54166667,7.5 L3.54166667,12 L7.45833333,12 L7.45833333,7.5 Z" id="Combined-Shape" stroke="none" fill="#FF0000" fillRule="evenodd" transform="translate(5.500000, 6.000000) scale(1, -1) translate(-5.500000, -6.000000) "></path>
                                            </svg>
                                        );
                                    })()}
                                </div>

                                <div>
                                    {metricType === 'percentage' ? `${trending}%` : trending}
                                </div>
                            </div>
                        );
                    }
                })()}
            </div>

            <div>
                <div className="aa-project-report-totals-comparison--metric">
                    {(() => {
                        if (metricType === 'count') {
                            return (
                                <FormattedNumber
                                    value={metric}
                                />
                            );
                        }

                        return `${metric}%`;
                    })()}
                </div>

                <div className="aa-project-report-totals-comparison--title">
                    {title}
                </div>

                <ProjectReportTotalProcessBar
                    label="NOW"
                    progress={getProgressPercentageByType(metricType, metric, previousMetric, compareMetric)}
                />
            </div>
        </div>
    );
};

ProjectReportTotal.displayName = 'ProjectReportTotal';

ProjectReportTotal.propTypes = {
    /**
     * Title shown for metric
     */
    title: PropTypes.string.isRequired,
    /**
     * Metric to display
     */
    metric: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    /**
     * Previous metric to display as a diff from current
     */
    previousMetric: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    /**
     * Used to compare count totals to display trends
     * e.g. total files uploaded vs the current count in the metric prop
     */
    compareMetric: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    /**
     * Icon to display in totals
     */
    icon: PropTypes.element,
    /**
     * Set the type of metric. e.g count or percentage
     */
    metricType: PropTypes.oneOf(['count', 'percentage']),
    /**
     * Show loading state
     */
    loading: PropTypes.bool,
};

ProjectReportTotal.defaultProps = {
    metric: 0,
    metricType: 'count',
    previousMetric: null,
    compareMetric: null,
    loading: false,
};

export default ProjectReportTotal;
