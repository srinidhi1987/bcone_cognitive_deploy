import React from 'react';
import {shallow} from 'enzyme';
import {FormattedNumber} from 'react-intl';

import ProjectReportTotal from '../index';

describe('<ProjectReportTotal />', () => {
    // given
    const metric = 20;

    it('should display percentage metric based on metricType', () => {
        // given
        const wrapper = shallow(
            <ProjectReportTotal
                title="Files Processed"
                metric={metric}
                metricType="percentage"
            />
        );
        // when
        const result = wrapper.find('.aa-project-report-totals-comparison--metric');
        // then
        expect(result.text()).toEqual(`${metric}%`);
    });

    it('should display count metric and formatted based on metricType by default', () => {
        // given
        const metric = 2000;
        const wrapper = shallow(
            <ProjectReportTotal
                title="Files Processed"
                metric={metric}
            />
        );
        // when
        const result = wrapper.find(FormattedNumber);
        // then
        expect(result.props().value).toEqual(metric);
    });
});
