import React from 'react';
import {mount} from 'enzyme';
import InfoTooltip from '../index';
import {Tooltip} from 'common-frontend-components';

const sampleTerm = {
    label: 'Unit Test',
    description: (label) => ( // eslint-disable-line react/display-name
        <span>a {label} is a test that isolates a specific piece of code</span>
    ),
};

describe('Component: InfoTooltip', () => {
    const wrapper = mount(<InfoTooltip term={sampleTerm} />);
    it('should take an object with label and description properties and pass jsx to the Tooltip child component', () => {
        const textArray = wrapper.find(Tooltip).props().children.props.children;
        expect(`${textArray[0]}${textArray[1]}${textArray[2]}`).toEqual('a Unit Test is a test that isolates a' +
            ' specific piece of code');
    });
});
