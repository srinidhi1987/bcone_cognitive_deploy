/**
*
* InfoTooltip
*
*/

import React from 'react';
import {Tooltip} from 'common-frontend-components';

import './styles.scss';

const InfoTooltip = ({term}) => (
    <Tooltip>
        {term.description(term.label)}
    </Tooltip>
);

InfoTooltip.displayName = 'InfoTooltip';

export default InfoTooltip;
