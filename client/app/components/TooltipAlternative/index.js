/**
*
* DesignerField
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {autobind} from 'core-decorators';
import {Popup} from 'common-frontend-components';
import './styles.scss';

export default class TooltipAlternative extends React.Component {
    static displayName = 'TooltipAlternative';
    static propTypes = {
        dataChildren: PropTypes.node.isRequired,
        popupChildren: PropTypes.node.isRequired,
        customPopupCls: PropTypes.string,
    };

    state = {
        hover: false,
    };

    @autobind handleToggleHover(hover) {
        this.setState({hover});
    }

    render() {
        const {
            customPopupCls,
            dataChildren,
            popupChildren,
        } = this.props;

        return (
            <span
                className="aa-data-with-popup"
                onMouseEnter={() => this.handleToggleHover(true)}
                onMouseLeave={() => this.handleToggleHover(false)}
            >
                {dataChildren}
                <Popup
                    className={customPopupCls}
                    disallow={[Popup.RIGHT, Popup.LEFT, Popup.BOTTOM]}
                    hover={this.state.hover}
                >
                    {popupChildren}
                </Popup>
            </span>
        );
    }
}
