/**
*
* ProjectDisplayHeader
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import FlexGrid from '../../components/FlexGrid';

import './styles.scss';

function ProjectDisplayHeader({project}) {
    return (
        <div className="aa-project-display-header">
            <FlexGrid>
                <FlexGrid.Item>
                    <FlexGrid className="aa-auth-project-name">
                        <FlexGrid.Item>
                            <div className="aa-project-status">
                                <div className="aa-project-status--circle"></div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item>
                            Upload files from:<br />
                            directoryofyourchoice<br /><br />

                            {project.primaryLanguage}
                        </FlexGrid.Item>
                    </FlexGrid>
                </FlexGrid.Item>

                <FlexGrid.Item>
                    {project.fileType}
                </FlexGrid.Item>

                <FlexGrid.Item>
                    Invoice Date<br />
                    Invoice #
                </FlexGrid.Item>
            </FlexGrid>
        </div>
    );
}

ProjectDisplayHeader.propTypes = {
    project: PropTypes.shape({
        name: PropTypes.string,
        description: PropTypes.string,
        primaryLanguage: PropTypes.string,
        fileType: PropTypes.string,
    }).isRequired,
};

ProjectDisplayHeader.displayName = 'ProjectDisplayHeader';

export default ProjectDisplayHeader;
