import React from 'react';
import {shallow} from 'enzyme';

import PercentageTag from '../index';

describe('<PercentageTag />', () => {
    it('should display default percentage prop if no prop is passed', () => {
        // given
        const wrapper = shallow(
            <PercentageTag />
        );
        // when
        const result = wrapper.find('.aa-percentage-tag--percent');
        // then
        expect(result.text()).toEqual('0%');
    });

    it('should display percentage prop', () => {
        // given
        const wrapper = shallow(
            <PercentageTag
                percentage={30}
            />
        );
        // when
        const result = wrapper.find('.aa-percentage-tag--percent');
        // then
        expect(result.text()).toEqual('30%');
    });
});
