/**
*
* PercentageTag
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE} from '../../containers/ProjectsContainer/constants';

import './styles.scss';

const PercentageTag = ({percentage}) => (
    <div className="aa-percentage-tag">
        <span className="aa-percentage-tag--percent">
            {Math.floor(percentage)}%
        </span>

        <svg width="76px" height="76px" viewBox="1144 0 76 76" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <polygon id="Rectangle-8" stroke="none" fill={Math.floor(percentage) >= MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE ? '#ADADAD' : '#E15A2F'} fillRule="evenodd" points="1144 0 1220 0 1220 76"></polygon>
        </svg>
    </div>
);

PercentageTag.displayName = 'PercentageTag';

PercentageTag.propTypes = {
    percentage: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
};

PercentageTag.defaultProps = {
    percentage: 0,
};

export default PercentageTag;
