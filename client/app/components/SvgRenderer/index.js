/**
*
* SvgRenderer
*
*/

import React, {PureComponent} from 'react';
import Rect from '../Rect';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {ANNOTATION_MODES} from '../../components/AnnotationEngine/constants/enums';

import './styles.scss';

class SvgRenderer extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'SvgRenderer';
    static propTypes = {
        axisGuide: PropTypes.shape({
            x: PropTypes.number,
            y: PropTypes.number,
        }),
        docHeight: PropTypes.number,
        docWidth: PropTypes.number,
        height: PropTypes.number,
        imageLoaded: PropTypes.bool,
        mode: PropTypes.number,
        objects: PropTypes.arrayOf(PropTypes.object).isRequired,
        onMouseDown: PropTypes.func,
        onRender: PropTypes.func,
        url: PropTypes.string,
        width: PropTypes.number,
        zoomScaledHeight: PropTypes.number,
        zoomScaledWidth: PropTypes.number,
    };

    constructor(props) {
        super(props);
        this.renderObject = this.renderObject.bind(this);

        //initial state
        this.state = {
            url: null,
        };
    }

    render() {
        // const {background, svgStyle} = this.props // for later
        const {
            axisGuide,
            docHeight,
            docWidth,
            mode,
            objects,
            onMouseDown,
            onRender,
            staticAnnotations,
            zoomScaledHeight,
            zoomScaledWidth,
        } = this.props;

        return (
            <svg
                ref={onRender} // note the ref here is passing reference through callback
                onMouseDown={onMouseDown}
                style={
                    {
                        height: parseInt(docHeight + zoomScaledHeight),
                        width: parseInt(docWidth + zoomScaledWidth),
                    }
                }
            >
                <image
                    height={parseInt(docHeight + zoomScaledHeight)}
                    ref={(ref) => (this.designerImage = ref)}
                    width={parseInt(docWidth + zoomScaledWidth)}
                    xlinkHref={this.state.url}
                >
                </image>
                {this.renderAnnotations(staticAnnotations, objects)}
                {
                    mode === ANNOTATION_MODES.DRAW ?
                    <g>
                        <line
                            x1="0"
                            x2={parseInt(docWidth + zoomScaledWidth)}
                            y1={axisGuide.y}
                            y2={axisGuide.y}
                            strokeWidth="1"
                            stroke="#49fb35"
                        />
                        <line
                            x1={axisGuide.x}
                            x2={axisGuide.x}
                            y1="0"
                            y2={parseInt(docHeight + zoomScaledHeight)}
                            strokeWidth="1"
                            stroke="#49fb35"
                        />
                    </g> : null
                }
            </svg>
        );
    }

    /**
     * @description this is a hack for IE 11. SVG images don't dispatch a load event.
     * @param {object} nextProps
     */
    componentWillReceiveProps(nextProps) {
        const hasDifferentImageUrl = nextProps.url !== this.props.url;
        if (!this.state.url || hasDifferentImageUrl) {
            const imageUrl = `${nextProps.url}&timestamp=${Date.now()}`;
            const imageProxy = new Image();

            this.setState({url: imageUrl});
            imageProxy.addEventListener('load', () => {
                this.props.onImageLoad(true);
            }, false);
            imageProxy.src = imageUrl;
        }
    }

    componentDidMount() {
        this.designerImage.addEventListener('load', () => {
            this.props.onImageLoad(true);
        });
    }

    componentWillUnmount() {
        this.designerImage.addEventListener('load', () => {
            this.props.onImageLoad(false);
        });
    }

    renderAnnotations(staticAnnotations, objects) {
        if (!this.props.imageLoaded) {
            return null;
        }

        return (
            <g>
                {staticAnnotations ? staticAnnotations.map(this.renderObject) : null}
                {objects.map(this.renderObject)}
            </g>
        );
    }

    /**
     * function to render individual Rect (rectangle) components for render method
     *
     * @param {Object} object
     * @param {Number} index
     * @returns {XML}
     */
    renderObject(object, index) {
        // const {onMouseOver} = this.props; // for later
        const {
            areaClickHandler,
            mode,
            objectRefs,
        } = this.props;

        return (
            <Rect
                className={classnames({
                    'hoverable-field': mode === ANNOTATION_MODES.FREE,
                })}
                onClick={areaClickHandler}
                object={object}
                key={index}
                index={index}
                onRender={(ref) => {
                    if (ref) objectRefs[object.index] = ref;
                }}
            />
        );
    }
}

export default SvgRenderer;
