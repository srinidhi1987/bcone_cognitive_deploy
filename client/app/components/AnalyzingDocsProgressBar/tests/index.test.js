import React from 'react';
import {FormattedNumber} from 'react-intl';
import {shallow} from 'enzyme';

import AnalyzingDocsProgressBar from '../index';

describe('<AnalyzingDocsProgressBar />', () => {
    const processedCount = 50;
    const totalCount = 100;
    const wrapper = shallow(
        <AnalyzingDocsProgressBar
            processedCount={processedCount}
            totalCount={totalCount}
        />
    );

    it('should set progress bar fill width based on processed and total counts', () => {
        // given
        // when
        const progress = wrapper.find('.aa-analyzing-project-progress-bar-fill');
        // then
        expect(progress.props().style).toEqual({width: '50%'});
    });

    it('should show current processing and total counts', () => {
        // given
        // when
        const label = wrapper.props().children;
        // then
        expect(label[4]).toEqual('Analyzing ');
        expect(label[5].type).toEqual(FormattedNumber);
        expect(label[5].props.value).toEqual(processedCount);
        expect(label[6]).toEqual(' of ');
        expect(label[7].type).toEqual(FormattedNumber);
        expect(label[7].props.value).toEqual(totalCount);
    });
});
