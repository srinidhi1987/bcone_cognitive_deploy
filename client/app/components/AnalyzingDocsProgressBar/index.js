/**
*
* AnalyzingDocsProgressBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {FormattedNumber} from 'react-intl';

import './styles.scss';

const calculateProcessWidth = (current, total) => {
    return (current / total) * 100;
};

const AnalyzingDocsProgressBar = ({processedCount, totalCount, timeRemaining}) => (
    <div className="aa-analyzing-project-progress-bar-container">
        Estimated time remaining = <FormattedNumber value={timeRemaining} /> minutes

        <div className="aa-analyzing-project-progress-bar">
            <div
                className="aa-analyzing-project-progress-bar-fill"
                style={{width: `${calculateProcessWidth(processedCount, totalCount)}%`}}
            ></div>
        </div>

        Analyzing <FormattedNumber value={processedCount} /> of <FormattedNumber value={totalCount} /> files
    </div>
);

AnalyzingDocsProgressBar.displayName = 'AnalyzingDocsProgressBar';

AnalyzingDocsProgressBar.propTypes = {
    /**
     * Current files processed
     */
    processedCount: PropTypes.number,
    /**
     * Total file count to be processed
     */
    totalCount: PropTypes.number,
    /**
     * Estimated remaining time for processing completion
     */
    timeRemaining: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
};

AnalyzingDocsProgressBar.defaultProps = {
    processedCount: 0,
    totalCount: 0,
};

export default AnalyzingDocsProgressBar;
