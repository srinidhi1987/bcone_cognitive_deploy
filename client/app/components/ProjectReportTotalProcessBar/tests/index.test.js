import React from 'react';
import {shallow} from 'enzyme';

import ProjectReportTotalProcessBar from '../index';

describe('<ProjectReportTotalProcessBar />', () => {
    // given
    const label = 'NOW';
    const wrapper = shallow(
        <ProjectReportTotalProcessBar
            label={label}
        />
    );
    it('Should display label props', () => {
        // given
        // when
        const result = wrapper.find('.aa-project-report-process-bar--title');
        // then
        expect(result.text()).toEqual(label);
    });
});
