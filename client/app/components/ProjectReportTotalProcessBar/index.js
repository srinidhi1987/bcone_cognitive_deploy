/**
*
* ProjectReportTotalProcessBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

const ProjectReportTotalProcessBar = ({label, progress}) => (
    <div className="aa-project-report-process-bar">
        <div className="aa-project-report-process-bar--title">
            {label}
        </div>

        <div className="aa-project-report-process-bar-container">
            <div className={classnames('aa-project-report-process-bar-fill', `p-${progress}`)}></div>
        </div>
    </div>
);

ProjectReportTotalProcessBar.displayName = 'ProjectReportTotalProcessBar';

ProjectReportTotalProcessBar.propTypes = {
    /**
     * Display label over progress bar
     */
    label: PropTypes.string.isRequired,
    /**
     * Percentage based on 100% width of the progress bar
     */
    progress: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
};

ProjectReportTotalProcessBar.defaultProps = {
    progress: 0,
};

export default ProjectReportTotalProcessBar;
