/**
*
* ProjectInfoDetails
*
*/

import React from 'react';
import {FormattedDate} from 'react-intl';
import './styles.scss';

const ProjectInfoDetails = ({project, projectSummary, language}) => (
    <div className="aa-project-info-container">
        <div className="aa-project-info-container-box-general">
            <div className="aa-project-info-container-header">
                GENERAL
            </div>

            <div className="aa-grid-column">
                {renderGeneralInfo(project, language)}
            </div>
        </div>
        {/*Div Line*/}
        <hr/>
        <div className="aa-project-info-container-box">
            <div className="aa-project-info-container-header">
                STAGING RESULTS: {projectSummary.stagingTotalFiles} FILES
            </div>
            <div className="aa-grid-row wrap">
                {renderStagingSummary(projectSummary)}
            </div>
        </div>
        {/*Div Line*/}
        <hr/>
        <div className="aa-project-info-container-box">
            <div className="aa-project-info-container-header">
                PRODUCTION RESULTS: {projectSummary.productionTotalFiles} FILES
            </div>
            <div className="aa-grid-row wrap">
                {renderProductionSummary(projectSummary)}
            </div>
        </div>
    </div>
);

/**
 * @description renders UI for general information about project
 * @param {object} project
 * @param {string} language
 */
function renderGeneralInfo({environment, projectType, updatedAt}, language) {
    return ([
        {label: 'Environment', value: environment === 'production' ? 'Production' : 'Staging'},
        {label: 'Domain', value: projectType},
        {label: 'Language', value: language},
        {label: 'Last modified', value: (
            <FormattedDate
                value={updatedAt}
                year="numeric"
                month="long"
                day="2-digit"
            />
        )},
    ].map((field) => (
        <div key={field.label}>
            <div className="aa-project-general-info-general aa-grid-row space-between">
                <span>{field.label}</span>
                <span className="aa-project-general-info--value horizontal-right">
                    {field.value}
                </span>
            </div>
        </div>
    )));
}

/**
 * @description renders staging summary for the current project
 * @param {object} projectSummary
 */
function renderStagingSummary(projectSummary) {
    return ([
        {label: 'Groups found', value: projectSummary.stagingTotalGroups},
        {label: 'Tested Files', value: `${projectSummary.stagingTestedFiles} (${projectSummary.stagingTestedFilesPercentage}%)`},
        {label: 'Bots created', value: projectSummary.stagingTotalBots},
        {label: 'Success', value: projectSummary.stagingSuccessFiles},
        {label: 'Documents Unclassified', value: projectSummary.stagingDocumentsUnclassified},
        {label: 'Failed', value: projectSummary.stagingFailedFiles},
        {label: 'Accuracy', value: `${Math.floor(projectSummary.stagingAccuracy)}%`},
        {label: 'STP', value: `${projectSummary.stagingSTP}% (${projectSummary.stagingSuccessFiles}/${projectSummary.stagingTotalFiles})`},
    ].map((field) => (
        <div key={field.label} className="aa-grid-row margin aa-width-50 summary-field">
            <div className="aa-project-general-info aa-grid-row space-between">
                <span>{field.label}:</span>
                <span className="aa-project-general-info--value">
                    {field.value}
                </span>
            </div>
        </div>
    )));
}

/**
 * @description render production summary for the current project
 * @param {object} projectSummary
 */
function renderProductionSummary(projectSummary) {
    return ([
        {label: 'Groups found', value: projectSummary.productionTotalGroups},
        {label: 'Processed Files', value: `${projectSummary.productionProcessedFiles} (${projectSummary.productionProcessedFilesPercentage}%)`},
        {label: 'Bots in Production', value:projectSummary.productionTotalBots},
        {label: 'Success', value: projectSummary.productionSuccessFiles},
        {label: 'Documents Unclassified', value: projectSummary.productionDocumentsUnclassified},
        {label: 'Reviewed', value: projectSummary.productionReviewFiles},
        {label: 'Invalid', value: projectSummary.productionInvalidFiles},
        {label: 'Pending review', value: projectSummary.pendingForReview},
        {label: 'Accuracy', value: `${Math.floor(projectSummary.productionAccuracy)}%`},
        {label: 'STP', value: `${projectSummary.productionSTP}% (${projectSummary.productionSuccessFiles}/${projectSummary.productionTotalFiles})`},
    ].map((field) => (
        <div key={field.label} className="aa-grid-row margin aa-width-50 summary-field">
            <div className="aa-project-general-info aa-grid-row space-between">
                <span>{field.label}:</span>
                <span className="aa-project-general-info--value">
                    {field.value}
                </span>
            </div>
        </div>
    )));
}

ProjectInfoDetails.displayName = 'ProjectInfoDetails';

export default ProjectInfoDetails;
