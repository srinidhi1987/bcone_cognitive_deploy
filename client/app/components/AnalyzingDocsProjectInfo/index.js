/**
*
* AnalyzingDocsProjectInfo
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const AnalyzingDocsProjectInfo = ({project, projectCustomFields, projectStandardFields}) => {
    if (project) {
        return (
            <div className="aa-analyzing-project-docs-info">
                <span className="aa-analyzing-project-docs-info--title">Learning Instance</span>

                <div className="aa-analyzing-project-docs-info-section">
                    <span className="aa-analyzing-project-docs-info-section--title">
                        Instance name
                    </span>

                    <span className="aa-analyzing-project-docs-info-section--value">
                        {project.name}
                    </span>
                </div>

                <div className="aa-analyzing-project-docs-info-section">
                    <span className="aa-analyzing-project-docs-info-section--title">
                        Domain
                    </span>

                    <span className="aa-analyzing-project-docs-info-section--value">
                        {project.projectType}
                    </span>
                </div>

                <div className="aa-analyzing-project-docs-info-section">
                    <span className="aa-analyzing-project-docs-info-section--title">
                        Standard fields
                    </span>

                    {projectStandardFields.map((field, i) => (
                        <span
                            key={`standard-field-${i}`}
                            className="aa-analyzing-project-docs-info-section--value"
                        >
                            {field.name}
                        </span>
                    ))}
                </div>

                <div className="aa-analyzing-project-docs-info-section">
                    <span className="aa-analyzing-project-docs-info-section--title">
                        Other fields
                    </span>

                    {projectCustomFields.map((field, i) => (
                        <span
                            key={`custom-field-${i}`}
                            className="aa-analyzing-project-docs-info-section--value"
                        >
                            {field.name}
                        </span>
                    ))}
                </div>
            </div>
        );
    }

    return null;
};

AnalyzingDocsProjectInfo.displayName = 'AnalyzingDocsProjectInfo';

AnalyzingDocsProjectInfo.propTypes = {
    project: PropTypes.shape({
        name: PropTypes.string,
    }),
    projectStandardFields: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
    })),
    projectCustomFields: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        type: PropTypes.string,
    })),
};

export default AnalyzingDocsProjectInfo;
