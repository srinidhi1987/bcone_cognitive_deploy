import React from 'react';
import {shallow} from 'enzyme';

import AnalyzingDocsProjectInfo from '../index';
import {Project1} from '../../../../fixtures/projects';

describe('<AnalyzingDocsProjectInfo />', () => {
    // given
    const wrapper = shallow(
        <AnalyzingDocsProjectInfo
            project={Project1}
            projectStandardFields={[{
                name: 'Standard Field',
            }]}
            projectCustomFields={Project1.fields.custom}
        />
    );

    describe('project info', () => {
        const projectInfo = wrapper.find('.aa-analyzing-project-docs-info');

        it('should display title of: Project', () => {
            // given
            // when
            const result = projectInfo.props().children;
            // then
            expect(result[0].props.children).toEqual('Learning Instance');
        });

        it('should display Project Name', () => {
            // given
            // when
            const result = projectInfo.props().children;
            // then
            expect(result[1].props.children[0].props.children).toEqual('Instance name');
            expect(result[1].props.children[1].props.children).toEqual(Project1.name);
        });

        it('should display type of files', () => {
            // given
            // when
            const result = projectInfo.props().children;
            // then
            expect(result[2].props.children[0].props.children).toEqual('Domain');
            expect(result[2].props.children[1].props.children).toEqual(Project1.projectType);
        });

        it('should display standard field names', () => {
            // given
            // when
            const result = projectInfo.props().children;
            // then
            expect(result[3].props.children[0].props.children).toEqual('Standard fields');
            expect(result[3].props.children[1][0].props.children).toEqual('Standard Field');
        });

        it('should display custom field names', () => {
            // given
            // when
            const result = projectInfo.props().children;
            // then
            expect(result[4].props.children[0].props.children).toEqual('Other fields');
            expect(result[4].props.children[1][0].props.children).toEqual(Project1.fields.custom[0].name);
        });
    });
});
