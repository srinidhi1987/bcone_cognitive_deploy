import React from 'react';
import {shallow} from 'enzyme';

import DisplayAppNotifications from '../index';

describe('<DisplayAppNotifications />', () => {
    it('should not display container element if no notifications are present', () => {
        // given
        const wrapper = shallow(
            <DisplayAppNotifications
                alerts={[]}
            />
        );
        // when
        const result = wrapper.find('#app-notifications');
        // then
        expect(result.length).toEqual(0);
    });

    it('should display container element if notifications are present', () => {
        // given
        const wrapper = shallow(
            <DisplayAppNotifications
                alerts={[
                    {message: 'Testing notifications', level: 'success'},
                ]}
            />
        );
        // when
        const result = wrapper.find('#app-notifications');
        // then
        expect(result.length).toEqual(1);
    });

    it('should render ul list for every notification', () => {
        // given
        const wrapper = shallow(
            <DisplayAppNotifications
                alerts={[
                    {message: 'Testing notifications', level: 'success'},
                ]}
            />
        );
        // when
        const result = wrapper.find('#app-notifications li');
        // then
        expect(result.length).toEqual(1);
    });
});
