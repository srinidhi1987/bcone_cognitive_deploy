/**
*
* DisplayAppNotifications
*
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Icon} from 'common-frontend-components';
import classnames from 'classnames';

import {ALERT_LEVEL_URGENT, ALERT_LEVEL_DANGER} from '../../containers/AlertsContainer/constants';

import './styles.scss';

class DisplayAppNotifications extends Component {
    static displayName = 'DisplayAppNotifications';

    static propTypes = {
        alerts: PropTypes.arrayOf(PropTypes.shape({
            message: PropTypes.string,
            level: PropTypes.string,
        })),
        /**
         * callback to remove alert
         */
        removeById: PropTypes.func.isRequired,
    };

    componentDidMount() {
        window.addEventListener('scroll', this._handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this._handleScroll);
    }

    _handleRemovingAlert(alert, index, expireTime = 6000) {
        const {removeById} = this.props;

        setTimeout(() => {
            const currentTimestamp = new Date();

            if (currentTimestamp - alert.createdAt >= expireTime) {
                // remove alert from DOM
                const elem = document.querySelector(`ul#app-notifications li:nth-child(${index + 1})`);
                if (elem) {
                    elem.classList.add('exit');
                }
                // remove alert from store
                setTimeout(() => removeById(alert.id), expireTime / 2);
            }
        }, expireTime);
    }

    _handleScroll() {
        const elem = document.getElementById('app-notifications');

        if (elem) {
            elem.style.top = (document.body.scrollTop >= 58) ? '10px' : '70px';
        }
    }

    render() {
        const {alerts} = this.props;

        if (alerts.length) {
            return (
                <ul id="app-notifications">
                    {alerts.map((alert, index) => (
                        <li
                            key={`alert-${alert.id}`}
                        >
                            <div
                                className={classnames('app-notification-container', {
                                    urgent: (alert.level === ALERT_LEVEL_URGENT),
                                    danger: (alert.level === ALERT_LEVEL_DANGER),
                                })}>
                                {this._handleRemovingAlert(alert, index)}

                                <Icon
                                    fa="check"
                                />

                                <div>
                                    {alert.message}
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            );
        }

        return null;
    }
}

export default DisplayAppNotifications;
