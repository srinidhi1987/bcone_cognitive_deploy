/**
*
* Rect
*
**/

import React, {Component} from 'react';

import './styles.scss';

class Rect extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'Rect';

    /**
     * @type {{initial: AnnotationVisualDefinition}}
     */
    static meta = {
        initial: {
            width: 0,
            height: 0,
            strokeWidth: 1,
            fill: 'rgba(255, 188, 3, 0.30)',
            strokeAlignment: 'outer',
            radius: 0,
            blendMode: 'normal',
            rotate: 0,
            strokeDasharray: '14,6',
            stroke: '#E36530',
            fillRule: 'evenodd',
        },
    };

    /**
     * @typedef {Object} AnnotationVisualDefinition
     * @property {number} height - height of the annotation
     * @property {number} width - width of the annotation
     * @property {number} strokeWidth - border width of the annotation
     * @property {string} fill - background color of the annotation
     * @property {number} radius - border radius of the annotation
     * @property {string} blendMode -
     * @property {number} rotate - rotate amount of the annotation
     * @property {string} strokeDasharry - length of the border-dash and space between border-dash
     * @property {string} stroke - color of the border
     * @property {string} fillRule - algorithm for creating dash pattern
     */

    render() {
        // const {index} = this.props; // for later
        const {
            className,
            index,
            object,
            onClick,
        } = this.props;

        return (
            <rect
                {...this.getObjectAttributes()}
                className={className}
                onClick={() => onClick(object, index)}
                rx={object.radius}
                x= {object.x + object.windowXOffset}
                y= {object.y + object.windowYOffset}
                width={object.width + object.windowWidthOffset}
                height={object.height + object.windowHeightOffset}
            />
        );
    }

    /**
     * return an object containing the attributes to be used on the
     * @returns {*}
     */
    getObjectAttributes() {
        const {object, onRender, ...rest} = this.props;

        return {
            ...object,
            ref: onRender,
            ...rest,
        };

    }
}

export default Rect;
