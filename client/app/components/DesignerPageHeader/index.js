/**
*
* DesignerPageHeader
*
*/

import React from 'react';

import './styles.scss';

const DesignerPageHeader = () => (
    <div className="designer-page-header">
    </div>
);

DesignerPageHeader.displayName = 'DesignerPageHeader';

export default DesignerPageHeader;
