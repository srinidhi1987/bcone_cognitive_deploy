/**
*
* DesignerPageControlBar
*
*/

import React from 'react';

import './styles.scss';

const DesignerPageControlBar = ({children}) => (
    <div className="designer-page-control-bar">
        {children}
    </div>
);

DesignerPageControlBar.displayName = 'DesignerPageControlBar';

export default DesignerPageControlBar;
