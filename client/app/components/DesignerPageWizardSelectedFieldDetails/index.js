/**
*
* DesignerPageWizardSelectedFieldDetails
*
*/

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {PageTitle} from 'common-frontend-components';
// import {PageTitle, TextInput, FieldLabel} from 'common-frontend-components';
import {
    FIELD_OPTIONS,
    FIELD_TYPES_ENUM,
    DESIGNER_AREA_TYPES,
    VALIDATION_OPTIONS_BY_TYPES,
} from '../../constants/enums';
import {ANNOTATION_MODES} from '../AnnotationEngine/constants/enums';
import {filterFieldTypesForDropdown, renderDoubleChevrons} from '../../utils/designer';
import classnames from 'classnames';
import {isSelectedDrawTool} from '../../utils/designer';
import {
    RadioInput,
} from 'common-frontend-components';

import './styles.scss';
import {autobind} from 'core-decorators';
import routeHelper from '../../utils/routeHelper';

const {
    LABEL,
    DISPLAY_VALUE,
    IS_REQUIRED,
} = FIELD_OPTIONS;

const AVAILABLE_FIELD_TYPES = filterFieldTypesForDropdown(FIELD_TYPES_ENUM);

class DesignerPageWizardSelectedFieldDetails extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DesignerPageWizardSelectedFieldDetails';
    constructor(props) {
        super(props);
    }

    state = {
        optionsMenuOpened: true,
    };

    @autobind handleLabelDrawHandler() {
        const {activeField, fieldOptionDrawToolClickHandler} = this.props;
        if (activeField.IsLabelOptional) return;

        fieldOptionDrawToolClickHandler(ANNOTATION_MODES.DRAW, LABEL);

    }
    render() {
        const {
            activeField,
            activeFieldOption,
            fieldOptionDrawToolClickHandler,
            isComplete,
            mode,
            selectActiveFieldOptionHandler,
            selectedFieldType,
            setFieldValueHandler,
            toggleFieldCheckboxHandler,
            wizardElementIds,
        } = this.props;

        const {
            optionsMenuOpened,
        } = this.state;

        const {QA_LBL_LABEL, QA_LBL_VALUE, QA_TXT_LABEL, QA_TXT_VALUE, QA_LST_TYPE} = wizardElementIds;
        const checkMarkUrl = routeHelper(`/images/status-${isComplete ? 'success' : 'todo'}.svg`);
        const selectIconUrl = routeHelper(`/images/select${ !activeField.IsLabelOptional && isSelectedDrawTool(activeFieldOption, LABEL, mode) ? '-active' : ''}.svg`);
        const formattedFieldName = activeField.Name && activeField.Name.replace(/_/g, ' ');
        const validationOptions = VALIDATION_OPTIONS_BY_TYPES[activeField.ValueType || 0];

        return (
            <div className="designer-page-wizard-selected-field-details">
                <div className="designer-page-wizard-selected-field-details--body">

                    <div className="right-view-body-classes">
                        <div>
                            <div className="right-view-body-fields-row" style={{paddingTop: 0}}>
                                <div className="aa-grid-row vertical-center space-between">
                                    <PageTitle label={formattedFieldName} />
                                    <div className="designer-page-wizard-selected-field-details--icon-background">
                                    <img src={checkMarkUrl} />
                                    </div>
                                </div>
                            </div>
                            <div className="right-view-body-fields-row">

                                <label id={QA_LBL_LABEL}>
                                   Label
                                    <div className="aa-grid-row">
                                        <div
                                            className={classnames('standard-input', 'field-option-input-margin', {
                                                'standard-input--active' : activeFieldOption === LABEL,
                                                'disabled' : activeField.IsLabelOptional,
                                            })}
                                        >
                                            <input
                                                id={QA_TXT_LABEL}
                                                onChange={(event) => setFieldValueHandler(activeField, LABEL, event.target.value)}
                                                onClick={() => selectActiveFieldOptionHandler(LABEL)}
                                                disabled={activeField.IsLabelOptional}
                                                value={activeField.Label}
                                            />
                                        </div>
                                        <div style={{paddingRight: '1px'}} className="aa-grid-column vertical-center horizontal-center">
                                            <div className={classnames('select-draw-icon-border', {
                                                'select-draw-icon--selected': isSelectedDrawTool(activeFieldOption, LABEL, mode),
                                            })}>
                                                <img
                                                    className="select-draw-icon"
                                                    style={{height: '19px', paddingTop: '1px'}}
                                                    onClick={this.handleLabelDrawHandler}
                                                    src={selectIconUrl} />
                                            </div>
                                             <span style={{lineHeight: '10px', fontSize: '9px'}}>Draw</span>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            {
                                selectedFieldType === DESIGNER_AREA_TYPES.FIELD ? (
                                    <div className="right-view-body-fields-row">
                                        <label id={QA_LBL_VALUE}>
                                            Value
                                            <div className="aa-grid-row">
                                                <div
                                                    className={classnames('standard-input', 'field-option-input-margin', {
                                                        'standard-input--active' : activeFieldOption === DISPLAY_VALUE,
                                                    })}
                                                >
                                                    <input
                                                        id={QA_TXT_VALUE}
                                                        onChange={(event) => setFieldValueHandler(activeField, DISPLAY_VALUE, event.target.value)}
                                                        onClick={() => selectActiveFieldOptionHandler(DISPLAY_VALUE)}
                                                        value={activeField.DisplayValue}
                                                    />
                                                </div>
                                                <div style={{paddingRight: '1px'}} className="aa-grid-column vertical-center horizontal-center">
                                                    <div className={classnames('select-draw-icon-border', {
                                                        'select-draw-icon--selected': isSelectedDrawTool(activeFieldOption, DISPLAY_VALUE, mode),
                                                    })}>
                                                        <img
                                                            className="select-draw-icon"
                                                            style={{height: '19px', paddingTop: '1px'}}
                                                            onClick={() => fieldOptionDrawToolClickHandler(ANNOTATION_MODES.DRAW, DISPLAY_VALUE)}
                                                            src={routeHelper(`/images/select${isSelectedDrawTool(activeFieldOption, DISPLAY_VALUE, mode) ? '-active' : ''}.svg`)} />
                                                    </div>
                                                    <span style={{lineHeight: '10px', fontSize: '9px'}}>Draw</span>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                ) : null
                            }
                            <div className="right-view-body-fields-row" style={{paddingTop: '15px'}}>
                                <div className="aa-grid-row vertical-center space-between" onClick={() => this.setState({optionsMenuOpened: !optionsMenuOpened})}>
                                    <span >
                                        Field Options
                                    </span>
                                    {renderDoubleChevrons(!optionsMenuOpened)}
                                </div>
                                <hr/>
                                {
                                    optionsMenuOpened ? (
                                        <div>
                                            <div className="right-view-body-fields-row">
                                                <label>
                                                    Type
                                                    <div
                                                        className={classnames(
                                                            'designer-class',
                                                            'aa-grid-row',
                                                            {
                                                                'designer-class--active': true,
                                                            },
                                                        )}
                                                    >
                                                        <select
                                                            id={QA_LST_TYPE}
                                                            value={activeField.ValueType}
                                                            onChange={(event) => setFieldValueHandler(activeField, 'ValueType', event.target.value)}
                                                        >
                                                            {
                                                                AVAILABLE_FIELD_TYPES.map((optionKey, index) => {
                                                                    return (
                                                                        <option
                                                                            key={index}
                                                                            value={optionKey}
                                                                        >
                                                                            {FIELD_TYPES_ENUM[optionKey]}
                                                                        </option>
                                                                    );
                                                                })
                                                            }
                                                        </select>
                                                    </div>
                                                </label>
                                            </div>
                                            <hr/>
                                            <div>
                                                <RadioInput
                                                    checked={activeField.IsRequired}
                                                    onChange={() => toggleFieldCheckboxHandler(activeField, IS_REQUIRED)}
                                                >
                                                    Required
                                                </RadioInput>
                                                <RadioInput
                                                    checked={!activeField.IsRequired}
                                                    onChange={() => toggleFieldCheckboxHandler(activeField, IS_REQUIRED)}
                                                >
                                                    Optional
                                                </RadioInput>
                                            </div>
                                            {
                                                !activeField.IsRequired ? (
                                                    <label className="not-required-default">
                                                        <span>
                                                            Default Value
                                                        </span>
                                                        <div className="aa-grid-row">
                                                            <div className="standard-input">
                                                                <input
                                                                    disabled={activeField.IsRequired}
                                                                    onChange={(event) => setFieldValueHandler(activeField, 'DefaultValue', event.target.value)}
                                                                    value={activeField.DefaultValue} />
                                                            </div>
                                                        </div>
                                                    </label>
                                                ) : (
                                                    null
                                                )
                                            }
                                            <hr/>
                                            <div>
                                                <span>Validate Pattern</span>
                                            </div>
                                        </div>
                                    ) : null
                                }
                            </div>
                            <div
                               className={classnames('right-view-body-fields-row', 'field-options')}
                            >
                                {
                                    optionsMenuOpened ? (
                                        <span>
                                            {
                                                Object.keys(validationOptions).map((propertyName) => {
                                                    return (
                                                        <label className="validation-options">
                                                            {validationOptions[propertyName]}
                                                            <div className="aa-grid-row">
                                                                <div className="standard-input">
                                                                    <input onChange={(event) => setFieldValueHandler(activeField, propertyName, event.target.value)} value={activeField[propertyName]} />
                                                                </div>
                                                            </div>
                                                        </label>
                                                    );
                                                })
                                            }
                                        </span>
                                    ) : (
                                        null
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

DesignerPageWizardSelectedFieldDetails.propTypes = {
    activeField: PropTypes.object.isRequired,
    activeFieldOption: PropTypes.string.isRequired,
    fieldColumnTopOffset: PropTypes.number.isRequired,
    fieldOptionDrawToolClickHandler: PropTypes.func.isRequired,
    fields: PropTypes.arrayOf(PropTypes.object).isRequired,
    isComplete: PropTypes.bool.isRequired,
    mode: PropTypes.number.isRequired,
    selectActiveFieldOptionHandler: PropTypes.func.isRequired,
    selectFieldHandler: PropTypes.func.isRequired,
    selectedFieldType: PropTypes.string.isRequired,
    setFieldValueHandler: PropTypes.func.isRequired,
    tables: PropTypes.arrayOf(PropTypes.object).isRequired,
    toggleFieldCheckboxHandler: PropTypes.func.isRequired,
};

export default DesignerPageWizardSelectedFieldDetails;
