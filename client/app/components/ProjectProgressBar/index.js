/**
*
* ProjectProgressBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

/**
 * Get current index of active step
 * @param {array} steps
 * @returns {number|*}
 */
const getCurrentIndex = (steps, currentStep) => {
    return steps.findIndex((step, i) => (i + 1) === currentStep);
};

/**
 * Check if current step bubble is highlighted
 * @param {array} steps - step props
 * @param {number} current - current index
 * @returns {boolean}
 */
const isStepBubbleLite = (steps = [], currentStep = 1, index) => {
    return index < getCurrentIndex(steps, currentStep);
};

/**
 * Is this the current step
 * @param {number} currentStep
 * @param {number} i - current iteration
 * @returns {boolean}
 */
const isCurrentStep = (currentStep = 1, i) => {
    return (currentStep - 1) === i;
};

const steps = [
    {
        label: {title: 'create', x: '0.215', y: '40'},
        icon: (
            <svg>
                <polygon id="Path-2" stroke="#FFFFFF" fill="#666666" transform="translate(1, 0.4)" points="9.07699248 14.776066 12.1381441 17.8372176 18.4828084 11.4925533 16.6004041 9.61014901 11.9079245 14.3026287 9.44518615 11.8398904 7.79280883 13.4922677"></polygon>
            </svg>
        ),
        position: {x: '1', y: '0'},
        active: false,
    },
    {
        label: {title: 'classify', x: '66.375', y: '40'},
        icon: (
            <svg>
                <g id="Pending-icon" transform="translate(8, 8.5)" fill="#FFFFFF">
                    <ellipse id="Oval-3" cx="9.94249549" cy="7.93814322" rx="1.94249549" ry="1.93814322"></ellipse>
                    <rect id="Rectangle" transform="translate(4.049542, 4.934469) rotate(36.000000) translate(-4.049542, -4.934469) " x="3.54954213" y="0.434469101" width="1" height="9"></rect>
                    <rect id="Rectangle-Copy" transform="translate(8.049542, 4.934469) rotate(-36.000000) translate(-8.049542, -4.934469) " x="7.54954213" y="0.434469101" width="1" height="9"></rect>
                    <ellipse id="Oval-3" cx="1.94249549" cy="7.93814322" rx="1.94249549" ry="1.93814322"></ellipse>
                    <ellipse id="Oval-3" cx="6.14249244" cy="1.93814322" rx="1.94249549" ry="1.93814322"></ellipse>
                </g>
            </svg>
        ),
        position: {x: '69', y: '0'},
        active: false,
    },
    {
        label: {title: 'train', x: '146.32', y: '40'},
        icon: (
            <svg>
                <g id="Train-icon" transform="translate(8, 11)" stroke="#FFFFFF" strokeWidth="2">
                    <use id="Rectangle-5" mask="url(#mask-2)" xlinkHref="#path-1"></use>
                    <use id="Rectangle-5" mask="url(#mask-4)" xlinkHref="#path-3"></use>
                    <use id="Rectangle-5" mask="url(#mask-6)" xlinkHref="#path-5"></use>
                </g>
            </svg>
        ),
        position: {x: '142', y: '0'},
        active: false,
    },
    {
        label: {title: 'production', x: '203', y: '40'},
        icon: (
            <svg>
                <g id="Tack-icon" transform="translate(7, 7)" fill="#FFFFFF">
                    <path d="M8.53225348,7.99177051 L7.59491965,3.02665775 L6.53956742,7.99177051 C6.53956742,7.99177051 6.42185318,8.90286127 7.53688379,8.90286127 C8.65191441,8.90286127 8.53225348,7.99177051 8.53225348,7.99177051 Z" id="Path-3" transform="translate(7.535990, 5.964760) rotate(36.000000) translate(-7.535990, -5.964760) "></path>
                    <path d="M7,13 C10.3137085,13 13,10.3137085 13,7 C13,3.6862915 10.3137085,1 7,1 C3.6862915,1 1,3.6862915 1,7 C1,10.3137085 3.6862915,13 7,13 Z M7,14 C3.13400675,14 0,10.8659932 0,7 C0,3.13400675 3.13400675,0 7,0 C10.8659932,0 14,3.13400675 14,7 C14,10.8659932 10.8659932,14 7,14 Z" id="Oval-4" fillRule="nonzero"></path>
                </g>
            </svg>
        ),
        position: {x: '211', y: '0'},
        active: true,
    },
];

const ProjectProgressBar = ({currentStep, complete}) => (
    <div className="aa-project-progress-bar aa-grid-row padded horizontal-center">
        <svg width="400px" height="60px" viewBox="25 76 241 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <defs>
                <rect id="path-1" x="0.904296875" y="3.5758948" width="1.39999998" height="2.30357921"></rect>
                <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-1" y="-1" width="3.39999998" height="4.30357921">
                    <rect x="-0.095703125" y="2.5758948" width="3.39999998" height="4.30357921" fill="white"></rect>
                    <use xlinkHref="#path-1" fill="black"></use>
                </mask>
                <rect id="path-3" x="5.31738281" y="1.8482104" width="1.39999998" height="4.03126361"></rect>
                <mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-1" y="-1" width="3.39999998" height="6.03126361">
                    <rect x="4.31738281" y="0.848210396" width="3.39999998" height="6.03126361" fill="white"></rect>
                    <use xlinkHref="#path-3" fill="black"></use>
                </mask>
                <rect id="path-5" x="9.73046875" y="0.12052599" width="1.39999998" height="5.75894802"></rect>
                <mask id="mask-6" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-1" y="-1" width="3.39999998" height="7.75894802">
                    <rect x="8.73046875" y="-0.87947401" width="3.39999998" height="7.75894802" fill="white"></rect>
                    <use xlinkHref="#path-5" fill="black"></use>
                </mask>
            </defs>
            <g id="Process-Bar" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(25.000000, 76.000000)">
                <g id="Base" transform="translate(13.000000, 12.000000)">
                    <rect id="Rectangle-4" fill="#DCDCDC" x="0" y="0" width="210" height="4"></rect>
                    <rect id="Filler" fill="#FFBC03" x="0" y="0" width={`calc(75px * ${complete ? 3 : getCurrentIndex(steps, currentStep)})`} height="4"></rect>
                </g>

                {steps.map((step, i) => (
                    <g
                        id={`${step.label.title}-bubble`}
                        key={`progress-icon-${i}`}
                        transform={`translate(${step.position.x}, ${step.position.y})`}
                    >
                        <circle id={`progress-icon-oval-outer-${i}`} fill={complete || isStepBubbleLite(steps, currentStep, i) ? '#FFBC03' : '#FFFFFF'} cx="14" cy="14" r="14"></circle>
                        <circle id={`progress-icon-oval-inner-${i}`} fill="#666666" cx="14" cy="14" r="10"></circle>
                        {step.icon}
                    </g>
                ))}

                {steps.map((step, i) => (
                    <text
                        id={step.label.title.toLowerCase().replace(' ', '_')}
                        key={`process-label-${i}`}
                        fontFamily="Century Gothic, CenturyGothic, AppleGothic, sans-serif"
                        fontSize="10"
                        fontWeight={complete || isCurrentStep(currentStep, i) ? '500' : '300'}
                        fill={complete || isCurrentStep(currentStep, i) ? '#FFBC03' : '#666666'}
                    >
                        <tspan x={step.label.x} y={step.label.y}>
                            {step.label.title.toLowerCase()}
                        </tspan>
                    </text>
                ))}
            </g>
        </svg>
    </div>
);

ProjectProgressBar.displayName = 'ProjectProgressBar';

ProjectProgressBar.propTypes = {
    /**
     * Current progress
     */
    currentStep: PropTypes.number,
    /**
     * Progress is complete
     */
    complete: PropTypes.bool,
};

ProjectProgressBar.defaultProps = {
    currentStep: 1,
    complete: false,
};

export default ProjectProgressBar;
