/**
*
* DesignerPageWizardFields
*
*/

import DesignerPageWizardMenu from '../DesignerPageWizardMenu';

import React, {PureComponent} from 'react';
import {
    DESIGNER_AREA_TYPES,
} from '../../constants/enums';

import './styles.scss';

class DesignerPageWizardFields extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DesignerPageWizardFields';

    render() {
        const {fields, tables, selectFieldHandler} = this.props;

        return (
            <div className="designer-page-wizard-fields">
                {
                    fields.length ?
                    <DesignerPageWizardMenu label="Fields" items={fields} selectFieldHandler={selectFieldHandler} menuType={DESIGNER_AREA_TYPES.FIELD} /> : null
                }
                {
                    tables.map((table, index) => {
                        const tableSettingsMenuItem = this.getTableSettingsObject(table);

                        return (
                            <DesignerPageWizardMenu
                                key={index}
                                label={`Table ${index + 1}`}
                                tableSettings={tableSettingsMenuItem}
                                items={table.Columns}
                                selectFieldHandler={selectFieldHandler}
                                menuType={DESIGNER_AREA_TYPES.TABLE}
                            />
                        );
                    })
                }
            </div>
        );
    }

    getTableSettingsObject(table) {
        return {...table, Name: 'Table Settings'};
    }
}

export default DesignerPageWizardFields;
