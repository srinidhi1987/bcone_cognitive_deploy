/**
*
* DocumentProcessingMarquee
*
*/

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

class DocumentProcessingMarquee extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DocumentProcessingMarquee';

    static propTypes = {
        messages: PropTypes.arrayOf(PropTypes.string).isRequired,
    };

    constructor(props) {
        super(props);

        this.counter = null;
        this.removeAnimation = null;
    }

    state = {
        currentCount: 0,
        displayMessages: [],
    };

    componentDidMount() {
        this.counter = setInterval(() => {
            const {messages} = this.props;
            const {currentCount} = this.state;
            const nextCount = currentCount + 1;
            const nextCurrentCount = (nextCount <= messages.length) ? nextCount : 0;
            const messageList = 'ul.aa-analyzing-project-marquee li';

            this.setState({currentCount: nextCurrentCount});

            const marqueeClass = document.querySelector(
                `${messageList}:nth-child(${nextCurrentCount}) svg`
            );

            if (marqueeClass && nextCurrentCount !== 0) {
                marqueeClass.classList.add('animate');
                return;
            }

            this.removeAnimation = setTimeout(() => {
                document.querySelectorAll(messageList).forEach((element) => {
                    const svgElem = element.querySelector('i svg');

                    if (svgElem) {
                        svgElem.classList.remove('animate');
                    }
                });
            }, 1000);
        }, 3000);
    }

    componentWillUnmount() {
        clearInterval(this.counter);
        clearTimeout(this.removeAnimation);
    }

    render() {
        const {messages} = this.props;
        const {currentCount} = this.state;

        return (
            <ul className="aa-analyzing-project-marquee">
                {messages.map((message, index) => (
                    <li key={`marquee-phrase-${index}`} className={classnames({
                        active: (currentCount > index),
                    })}>
                        <i>
                            <svg
                                viewBox="0 0 320 320"
                                width="40"
                                height="40"
                                xmlSpace="preserve"
                                className="progress-check-mark"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="160" cy="160" r="100"></circle>

                                <svg x="30" y="30">
                                    <polyline points="66 137.513666 104.585492 176.099157 191.684649 89"></polyline>
                                </svg>
                            </svg>
                        </i>
                        {message}
                    </li>
                ))}
            </ul>
        );
    }
}

export default DocumentProcessingMarquee;
