import React from 'react';
import {shallow} from 'enzyme';

import DocumentProcessingMarquee from '../index';

describe('<DocumentProcessingMarquee />', () => {
    // given
    const messages = [
        'Applying classifier...',
        'Performing semantic analysis...',
        'Learning from aliases...',
        'Recognizing patterns...',
        'Extracting data...',
    ];
    const wrapper = shallow(
        <DocumentProcessingMarquee
            messages={messages}
        />
    );

    it('should render list from prop messages', () => {
        // given
        // when
        const list = wrapper.find('ul.aa-analyzing-project-marquee li');
        // then
        expect(list.first().text()).toEqual(messages[0]);
        expect(list.at(1).text()).toEqual(messages[1]);
        expect(list.at(2).text()).toEqual(messages[2]);
        expect(list.at(3).text()).toEqual(messages[3]);
        expect(list.last().text()).toEqual(messages[4]);
    });
});
