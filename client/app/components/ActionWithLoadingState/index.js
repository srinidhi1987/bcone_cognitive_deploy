/**
*
* Run Bot Once
*
*/
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {ActionBar} from 'common-frontend-components';

class ActionWithLoadingState extends PureComponent {
    static displayName = 'RunBotOnce';

    static propTypes = {
        animate: PropTypes.string,
        aa: PropTypes.string,
        fa: PropTypes.string,
        isInProduction: PropTypes.bool,
        label: PropTypes.string,
        working: PropTypes.bool,
        onClick: PropTypes.func,
    };

    static defaultProps = {
        component: 'li',
    }


    render() {
        const {
            aa,
            animate,
            disabled,
            fa,
            label,
            working,
            onClick,
        } = this.props;

        const cls = classnames({
            disable: disabled,
        });

        return (
            <span className="action-with-loading-state">
                {/** Can't run once in prod **/}
                <a
                    className={cls}
                    onClick={onClick}
                >
                    <ActionBar.Action
                        animate={animate}
                        aa={aa}
                        fa={fa}
                        label={label}
                        working={working}
                        onClick={() => {}} />
                </a>
            </span>
        );
    }
}

export default ActionWithLoadingState;
