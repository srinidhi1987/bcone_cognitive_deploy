/**
*
* ProjectReportSection
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

const ProjectReportSection = ({children, icon, title, subtitle, theme, classes}) => (
    <section className={classnames('aa-project-performance-report-section', classes, {
        dark: theme === 'dark',
    })}>
        <header className="aa-project-performance-report-section-header">
            <div className="aa-project-performance-report-section-header-icon-container">
                <div className="aa-project-performance-report-section-header-icon">
                    {icon}
                </div>

                <div>
                    <span className="aa-project-performance-report-section-header--title">
                        {title}
                    </span>

                    {(() => {
                        if (subtitle) {
                            return (
                                <span className="aa-project-performance-report-section-header--subtitle">
                                    {subtitle}
                                </span>
                            );
                        }
                    })()}
                </div>
            </div>
        </header>

        {children}
    </section>
);

ProjectReportSection.displayName = 'ProjectReportSection';

ProjectReportSection.propTypes = {
    icon: PropTypes.element.isRequired,
    /**
     * Title of the section
     */
    title: PropTypes.string.isRequired,
    /**
     * Sub title of the section
     */
    subtitle: PropTypes.string,
    /**
     * Override default theme
     */
    theme: PropTypes.string,
    /**
     * Add additional secondary classes to section
     */
    classes: PropTypes.string,
};

export default ProjectReportSection;
