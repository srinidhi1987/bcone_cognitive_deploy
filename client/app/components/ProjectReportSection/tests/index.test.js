import React from 'react';
import {shallow} from 'enzyme';

import ProjectReportSection from '../index';

describe('<ProjectReportSection />', () => {
    it('should show subtitle if prop is passed in', () => {
        // given
        const wrapper = shallow(
            <ProjectReportSection
                icon={(<i></i>)}
                title="Testing"
                subtitle="this is a test"
            />
        );
        // when
        const result = wrapper.find('.aa-project-performance-report-section-header--subtitle');
        // then
        expect(result.length).toBe(1);
    });

    it('should hide subtitle if not present', () => {
        // given
        const wrapper = shallow(
            <ProjectReportSection
                icon={(<i></i>)}
                title="Testing"
            />
        );
        // when
        const result = wrapper.find('.aa-project-performance-report-section-header--subtitle');
        // then
        expect(result.length).toBe(0);
    });
});
