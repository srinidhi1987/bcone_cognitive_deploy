import React from 'react';
import {shallow} from 'enzyme';

import EnvironmentBadge from '../index';

describe('<EnvironmentBadge />', () => {
    it('should render child environment label', () => {
        // given
        const environment = 'staging';
        const wrapper = shallow(
            <EnvironmentBadge>
                {environment}
            </EnvironmentBadge>
        );
        // when
        const result = wrapper.find('.aa-project-environment-badge');
        // then
        expect(result.props().children).toEqual(environment);
    });
});
