/**
*
* EnvironmentBadge
*
*/

import React from 'react';
import classnames from 'classnames';

import './styles.scss';

const EnvironmentBadge = ({children}) => (
    <div className={classnames('aa-project-environment-badge', {
        production: children === 'production',
    })}>
        {children}
    </div>
);

EnvironmentBadge.displayName = 'EnvironmentBadge';

export default EnvironmentBadge;
