import React from 'react';
import {shallow} from 'enzyme';

import DesignerPageWizardSelectedItemDetails from '../index';
import DesignerPageWizardSelectedFieldDetails from '../../DesignerPageWizardSelectedFieldDetails';
import DesignerPageWizardSelectedTableDetails from '../../DesignerPageWizardSelectedTableDetails';

describe('Component: DesignerPageWizardSelectedItemDetails', () => {

    const defaultProps = {
        activeField: {Id: '5'},
        activeFieldOption: 'Label',
        fieldColumnTopOffset: 50,
        fieldOptionDrawToolClickHandler: jest.fn(),
        fields: [],
        isTableSettings: false,
        mode: 'free',
        selectActiveFieldOptionHandler: jest.fn(),
        selectFieldHandler: jest.fn(),
        selectedFieldType: 0,
        setFieldValueHandler: jest.fn(),
        tables: [],
        toggleFieldCheckboxHandler: jest.fn(),
    };

    it('Expect to render DesignerPageWizardSelectedFieldDetails if it is not a table', () => {
        const wrapper = shallow(<DesignerPageWizardSelectedItemDetails {...defaultProps} />);

        expect(wrapper.find(DesignerPageWizardSelectedFieldDetails).length).toEqual(1);
    });

    it('Expect to render DesignerPageWizardSelectedFieldDetails if it is not a table', () => {
        const wrapper = shallow(<DesignerPageWizardSelectedItemDetails {...defaultProps} isTableSettings={true} />);

        expect(wrapper.find(DesignerPageWizardSelectedFieldDetails).length).toEqual(0);
        expect(wrapper.find(DesignerPageWizardSelectedTableDetails).length).toEqual(1);
    });
});
