/**
*
* DesignerPageWizardMenuItemDetail
*
*/

import React from 'react';
import DesignerPageWizardSelectedFieldDetail from '../DesignerPageWizardSelectedFieldDetails';
import DesignerPageWizardSelectedTableDetail from '../DesignerPageWizardSelectedTableDetails';


const DesignerPageWizardMenuItemDetail = (props) => (
    props.isTableSettings ? (
        <DesignerPageWizardSelectedTableDetail {...props} />
    ) : (
        <DesignerPageWizardSelectedFieldDetail {...props} />
    )
);

DesignerPageWizardMenuItemDetail.displayName = 'DesignerPageWizardMenuItemDetail';

export default DesignerPageWizardMenuItemDetail;
