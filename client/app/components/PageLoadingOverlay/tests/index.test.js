import React from 'react';
import {shallow} from 'enzyme';

import PageLoadingOverlay from '../index';
import routeHelper from '../../../utils/routeHelper';

describe('<PageLoadingOverlay />', () => {
    describe('show loading state', () => {
        it('should show default label', () => {
            // given
            const wrapper = shallow(
                <PageLoadingOverlay
                    show={true}
                />
            );
            // when
            const result = wrapper.find('.aa-page-loading-overlay--label');
            // then
            expect(result.props().children).toEqual('Loading...');
        });

        it('should show passed in label', () => {
            // given
            const label = 'Creating instance...';
            const wrapper = shallow(
                <PageLoadingOverlay
                    show={true}
                    label={label}
                />
            );
            // when
            const result = wrapper.find('.aa-page-loading-overlay--label');
            // then
            expect(result.props().children).toEqual(label);
        });

        it('should show correct loader asset', () => {
            // given
            const wrapper = shallow(
                <PageLoadingOverlay
                    show={true}
                />
            );
            // when
            const result = wrapper.find('.aa-page-loading-overlay-loader img');
            // then
            expect(result.props()).toEqual({
                src: routeHelper('images/ring-loader.gif'),
                alt: 'loading',
            });
        });
    });

    describe('hide loading state', () => {
        it('should not render elements if show is false', () => {
            // given
            const wrapper = shallow(
                <PageLoadingOverlay
                    show={false}
                />
            );
            // when
            const result = wrapper.find('.aa-page-loading-overlay');
            // then
            expect(result.length).toBe(0);
        });
    });
});
