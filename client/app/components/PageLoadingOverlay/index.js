/**
*
* PageLoadingOverlay
*
*/

import React from 'react';
import classnames from 'classnames';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const PageLoadingOverlay = ({show, label, fullscreen}) => {
    if (show) {
        return (
            <div className={classnames('aa-page-loading-overlay', {
                full: fullscreen,
            })}>
                <div className="aa-page-loading-overlay-loader">
                    <img src={routeHelper('images/ring-loader.gif')} alt="loading" />

                    <div className="aa-page-loading-overlay--label">
                        {label}
                    </div>
                </div>
            </div>
        );
    }

    return null;
};

PageLoadingOverlay.displayName = 'PageLoadingOverlay';

PageLoadingOverlay.defaultProps = {
    label: 'Loading...',
    show: false,
    fullscreen: false,
};

export default PageLoadingOverlay;
