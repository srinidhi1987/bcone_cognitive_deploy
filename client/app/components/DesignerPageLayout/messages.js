/*
 * DesignerPageLayout Messages
 *
 * This contains all the text for the DesignerPageLayout component.
 */
import {defineMessages} from 'react-intl';

export default defineMessages({
    header: {
        id: 'app.components.DesignerPageLayout.header',
        defaultMessage: 'This is the DesignerPageLayout component !',
    },
});
