/**
*
* DesignerPageLayout
*
*/

import './styles.scss';

import React from 'react';
import PropTypes from 'prop-types';
import PageLoadingOverlay from '../PageLoadingOverlay';
import {
    SplitLayout,
    Breadcrumbs,
    PageTitle,
    CommandButton,
    Icon,
} from 'common-frontend-components';
import {Link} from 'react-router';
import DesignerPageViewer from '../DesignerPageViewer';
import PageDescription from '../PageDescription';

import SaveVisionBotConfirmationModal from '../SaveVisionBotConfirmationModal';

import {VISION_BOT_MODES} from '../../constants/enums';
import routeHelper from '../../utils/routeHelper';

// NOTE: we could probably make this a class component and instead of passing all our props
// into a HOC, we could just say <SplitLayout.Left>{this.renderVisionBotTools()} </SplitLayout.Left>
export class VisionBotLayout extends React.Component {
    componentWillMount() {
        this.isVisionBotEditMode = this.isEditMode(this.props.visionBotMode);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visionBotMode !== this.props.visionBotMode) {
            this.isVisionBotEditMode = this.isEditMode(nextProps.visionBotMode);
        }
    }

    render() {
        const {
            activeField,
            activeFieldId,
            activeFieldOption,
            allMapped,
            allPreviewFieldsExtractedConfidently,
            annotationClasses,
            appState,
            areaClickHandler,
            canNavigateBotPage,
            cancelNextConfirmationBoxHandler,
            changePage,
            currentDocPage,
            cancelSaveAndCloseConfirmationBoxHandler,
            currentPreviewDocument,
            currentZoomScale,
            docMeta,
            editLoading,
            imageLoaded,
            imageLoadHandler,
            isLoadingEditData,
            isSavingEditData,
            isWaitingForPreviewData,
            mode,
            nextConfirmationBoxVisibility,
            objects,
            pageInputChangeHandler,
            onUpdate,
            pageData,
            pageInputValue,
            params,
            previewDocuments,
            previewLoading,
            projectName,
            saveAndCloseConfirmationBoxVisibility,
            saveBotAndSendToProductionHandlerForNext,
            saveBotAndSendToProductionHandlerForSaveAndClose,
            saveHandlerForNext,
            saveHandlerForSaveAndClose,
            setLayoutRatio,
            staticAnnotations,
            startDragHandler,
            stopDragHandler,
            url,
            handleZoomIn,
            handleZoomOut,
            handleResetZoom,
            scaleAmount,
        } = this.props;

        if (!url) {
            return (
                <PageLoadingOverlay
                    label={'Loading Document...'}
                    show={!appState.isBootstrapping}
                    fullscreen={true}
                />
            );
        }
        const pageView = params.mode === 'edit' ? 'Train' : 'Preview';
        const currentDoc = previewDocuments && previewDocuments[currentPreviewDocument];
        const currentDocName = currentDoc ? currentDoc.fileName : '...';

        return (
              <div className="designer-page-layout aa-grid-column">
                  <Breadcrumbs>
                      <Link
                          to={routeHelper('/learning-instances')}
                      >
                          Learning instances
                      </Link>
                      <Link
                          to={routeHelper(`/learning-instances/${params.id}`)}
                      >
                          {projectName}
                      </Link>
                      <Link
                          to={routeHelper(`/learning-instances/${params.id}/bots/${params.botid}/edit`)}
                      >
                          Train bot group {params.botid}
                      </Link>
                  </Breadcrumbs>
                  <div className="designer-page-layout--main aa-grid-column flex-1">
                      <div className="aa-grid-row vertical-center space-between">
                          <div>
                              <PageTitle
                                  label={
                                      this.renderPageTitle(pageView, params, allPreviewFieldsExtractedConfidently)
                                  }
                              />
                              <PageDescription>
                                  <div className="designer-page-layout-description">
                                      <span title={currentDocName}>
                                          Document: {currentDocName}
                                      </span>
                                  </div>
                              </PageDescription>
                          </div>
                          {this.renderCommandButtons()}
                          <PageLoadingOverlay
                              show={(previewLoading || isWaitingForPreviewData) && !isSavingEditData}
                              label="Loading Preview Document..."
                              fullscreen={false}
                          />
                          <PageLoadingOverlay
                              show={editLoading || isLoadingEditData}
                              label={'Loading Edit Document...'}
                              fullscreen={false}
                          />
                      </div>
                      <div className="flex-1 aa-grid-column">
                          <SplitLayout
                              xs="left"
                              sm="horizontal"
                              defaultSize={45}
                              minimumSize="350px"
                              maximumSize={90}
                              expand="left"
                          >
                              <SplitLayout.Left>
                                  <section style={{height: 'calc(100% - 40px)'}}>
                                      {this.props.leftSideComponent}
                                  </section>
                              </SplitLayout.Left>
                              <SplitLayout.Right>
                                  <section style={{height: 'calc(100% - 40px)'}}>
                                      <DesignerPageViewer
                                          activeField={activeField}
                                          activeFieldId={activeFieldId}
                                          activeFieldOption={activeFieldOption}
                                          annotationClasses={annotationClasses}
                                          areaClickHandler={areaClickHandler}
                                          canNavigateBotPage={canNavigateBotPage}
                                          changePage={changePage}
                                          currentDocPage={currentDocPage}
                                          currentZoomScale={currentZoomScale}
                                          docMeta={docMeta}
                                          handleResetZoom={handleResetZoom}
                                          handleZoomIn={handleZoomIn}
                                          handleZoomOut={handleZoomOut}
                                          imageLoadHandler={imageLoadHandler}
                                          imageLoaded={imageLoaded}
                                          mode={mode}
                                          navigateNextBotDocPageHandler={this.props.navigateNextDocPageHandler}
                                          navigatePreviousBotDocPageHandler={this.props.navigatePreviousDocPageHandler}
                                          objects={objects}
                                          pageInputChangeHandler={pageInputChangeHandler}
                                          onUpdate={onUpdate}
                                          pageData={pageData}
                                          pageInputValue={pageInputValue}
                                          scaleAmount={scaleAmount}
                                          setLayoutRatio={setLayoutRatio}
                                          startDragHandler={startDragHandler}
                                          staticAnnotations={staticAnnotations}
                                          stopDragHandler={stopDragHandler}
                                          url={url}
                                      />
                                  </section>
                              </SplitLayout.Right>
                          </SplitLayout>
                      </div>
                  </div>

                  <SaveVisionBotConfirmationModal
                      cancelHandler={cancelSaveAndCloseConfirmationBoxHandler}
                      saveAndSendDisabled={!allMapped}
                      saveBotAndSendToProductionConfirmHandler={saveBotAndSendToProductionHandlerForSaveAndClose}
                      saveBotConfirmHandler={saveHandlerForSaveAndClose}
                      show={saveAndCloseConfirmationBoxVisibility}
                  />

                  <SaveVisionBotConfirmationModal
                      cancelHandler={cancelNextConfirmationBoxHandler}
                      saveAndSendDisabled={!allMapped}
                      saveBotAndSendToProductionConfirmHandler={saveBotAndSendToProductionHandlerForNext}
                      saveBotConfirmHandler={saveHandlerForNext}
                      show={nextConfirmationBoxVisibility}
                  />
              </div>
        );
    }

    renderVisionBotModeButton() {
        if (!this.isVisionBotEditMode) {
            return null;
        }

        const {previewDocumentHandler} = this.props;

        return <button onClick={previewDocumentHandler}>Preview</button>;
    }

    isEditMode(mode) {
        return mode === VISION_BOT_MODES.EDIT;
    }

    /**
     *  renders appropriate page title and icon
     * @param {string} pageView
     * @param {Object} params
     * @param {boolean} allPreviewFieldsExtractedConfidently
     */
    renderPageTitle(pageView, params, allPreviewFieldsExtractedConfidently) {
        return (
            <span>
                <div className="title-container">
                    <span>{pageView} group {params.botid}</span>
                    {this.renderPreviewIcon(pageView, allPreviewFieldsExtractedConfidently)}
                </div>
            </span>
        );
    }
    /**
     *  renders appropriate preview icon
     * @param {string} pageView
     * @param {boolean} allPreviewFieldsExtractedConfidently
     */
    renderPreviewIcon(pageView, allPreviewFieldsExtractedConfidently) {
        if (pageView !== 'Preview') {
            return null;
        }

        if (allPreviewFieldsExtractedConfidently) {
            return <img className="title-validation-icon" src={routeHelper('images/status-success.svg')} />;
        }

        return <img className="title-non-valid-icon" src={routeHelper('images/status-todo.svg')} />;
    }

    renderCommandButtons() {
        if (!this.isVisionBotEditMode) {
            const {editDocumentHandler, exportToCSV} = this.props;

            return (
                <div className="aa-grid-row space-between horizontal-center vertical-center">
                    <div className="aa-grid-row vertical-center designer-page-layout--main--action-bar" onClick={exportToCSV}>
                        <Icon className="action-icon" aa="action-export"/>
                        <span className="label">Export to CSV...</span>
                    </div>
                    <CommandButton onClick={editDocumentHandler}>
                        Back to Training
                    </CommandButton>
                </div>
            );
        }

        const {
            allMapped,
            saveAndCloseClickHandler,
        } = this.props;

        return (
            <div className="designer-page-layout--main-command-buttons">
                <CommandButton
                    onClick={saveAndCloseClickHandler}
                    disabled={!allMapped}
                    recommended
                >
                    Save and close
                </CommandButton>
            </div>
        );
    }
}

VisionBotLayout.displayName = 'VisionBotLayout';

VisionBotLayout.propTypes = {
    activeField: PropTypes.object,
    activeFieldOption: PropTypes.string,
    allMapped: PropTypes.bool,
    annotationClasses: PropTypes.arrayOf(PropTypes.string).isRequired,
    appState: PropTypes.object,
    botCanNavigate: PropTypes.object,
    cancelBotSaveHandler: PropTypes.func,
    currentZoomScale: PropTypes.number.isRequired,
    documentPreviewHandler: PropTypes.func,
    docMeta: PropTypes.object.isRequired,
    fieldColumnTopOffset: PropTypes.number.isRequired,
    fields: PropTypes.arrayOf(PropTypes.object).isRequired,
    fieldOptionDrawToolClickHandler: PropTypes.func,
    imageLoaded: PropTypes.bool,
    imageLoadHandler: PropTypes.func.isRequired,
    mode: PropTypes.number,
    navigateBotsHandler: PropTypes.func,
    navigateNextBotsHandler: PropTypes.func,
    objects: PropTypes.arrayOf(PropTypes.object).isRequired,
    onUpdate: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    previewDocumentHandler: PropTypes.func,
    projectName: PropTypes.string,
    saveBotAndSendToProductionConfirmHandler: PropTypes.func,
    saveBotConfirmHandler: PropTypes.func,
    saveConfirmationBoxVisibility: PropTypes.func,
    selectFieldHandler: PropTypes.func.isRequired,
    selectActiveFieldOptionHandler: PropTypes.func,
    selectedFieldType: PropTypes.func,
    setFieldValueHandler: PropTypes.func.isRequired,
    setLayoutRatio: PropTypes.func,
    staticAnnotations: PropTypes.arrayOf(PropTypes.object).isRequired,
    startDragHandler: PropTypes.func,
    stopDragHandler: PropTypes.func,
    tables: PropTypes.arrayOf(PropTypes.object).isRequired,
    url: PropTypes.string,
    handleZoomIn: PropTypes.func.isRequired,
    handleZoomOut: PropTypes.func.isRequired,
    handleResetZoom: PropTypes.func.isRequired,
    scaleAmount: PropTypes.number.isRequired,
};

export default VisionBotLayout;
