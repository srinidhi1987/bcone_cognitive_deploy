import React from 'react';
import {shallow} from 'enzyme';

import DesignerPageLayout from '../index';
import DesignerPageWizard from '../../DesignerPageWizard';
import DesignerPageViewer from '../../DesignerPageViewer';
import PageLoadingOverlay from '../../PageLoadingOverlay';
import {Breadcrumbs} from 'common-frontend-components';
import {breadcrumbTester} from '../../../utils/testHelpers';
import {field1, field2} from '../../../../fixtures/designer';
import routeHelper from '../../../utils/routeHelper';

describe(DesignerPageLayout.displayName, () => {
    // TEST SETUP START
    // states of the component
    const {NO_IMAGE_URL, IMAGE_URL_AVAILABLE} = {
        NO_IMAGE_URL: 'when there is no image url available',
        IMAGE_URL_AVAILABLE: 'when there is a image url available',
    };
    // sections of the component
    const {BREADCRUMBS, MAIN_COMMAND_BUTTONS, PAGE_LOADING_OVERLAY, WIZARD, VIEWER} = {
        BREADCRUMBS: Breadcrumbs.displayName,
        MAIN_COMMAND_BUTTONS: 'main-command-buttons',
        PAGE_LOADING_OVERLAY: PageLoadingOverlay.displayName,
        VIEWER: DesignerPageViewer.displayName,
        WIZARD: DesignerPageWizard.displayName,
    };
    // props
    const propsForThisComponent = {
        visionBotMode: 'edit',
        finishHandler: jest.fn(),
        params: {},
        projectName: 'Anything',
        leftSideComponent: <DesignerPageWizard />,
        previewDocuments: [{filename: 'hi.tiff'}],
        currentPreviewDocument: 0,
    };
    const propsSharedByWizardAndViewer = {
        activeField: {Id: '3', ValidationID: 3, IsRequired: false},
        activeFieldOption: 'string',
        mode: 1,
        objects: [],
        params: {
            id: '23423432432',
            botid: '32432532534',
        },
        projectName: 'Anything',
        selectedObjectIndex: 0,
        setFieldValueHandler: jest.fn(),
        staticAnnotations: [],
        tables: [],
        toggleFieldCheckboxHandler: jest.fn(),
        url: 'https://sample',
    };
    const propsForWizard = {
        ...propsSharedByWizardAndViewer,
        botCanNavigate: {},
        documentPreviewHandler: jest.fn(),
        fieldColumnTopOffset: 0,
        fieldOptionDrawToolClickHandler: jest.fn(),
        fields: [
            field1,
            field2,
        ],
        selectActiveFieldOptionHandler: jest.fn(),
        selectFieldHandler: jest.fn(),
        selectedFieldType: 'A type',
        tables: [],
        toggleFieldCheckboxHandler: jest.fn(),
    };
    const propsForViewer = {
        ...propsSharedByWizardAndViewer,
        annotationClasses: ['Example'],
        currentZoomScale: 1,
        docMeta: {Metadata: {width: 400, height: 800}},
        handleZoomIn: jest.fn(),
        handleZoomOut: jest.fn(),
        handleResetZoom: jest.fn(),
        objects: [],
        onUpdate: jest.fn(),
        setLayoutRatio: jest.fn(),
        scaleAmount: 1,
        startDragHandler: jest.fn(),
        staticAnnotations: [],
        stopDragHandler: jest.fn(),
    };
    const propsForPageLoader = {
        appState: {
            isBootstrapping: false,
        },
    };

    const props = {
        ...propsForThisComponent,
        ...propsForViewer,
        ...propsForWizard,
        ...propsForPageLoader,
    };

    // TEST SETUP END

    // given
    describe(NO_IMAGE_URL, () => {
        const noUrlProps = {
            ...props,
            url: null,
        };
        // when
        const wrapper = shallow(<DesignerPageLayout {...noUrlProps} />);

        describe(PAGE_LOADING_OVERLAY, () => {
            const pageOverlayWrapper = wrapper.find(PageLoadingOverlay);

            // then
            it('should be be displayed and with the appropriate props', () => {
                expect(pageOverlayWrapper.props().show).toEqual(!props.appState.isBootstrapping);
            });
        });
    });

    // given
    describe(IMAGE_URL_AVAILABLE, () => {
        // when
        const wrapper = shallow(<DesignerPageLayout {...props} />);
        const {projectName} = props;
        const {id, botid} = props.params;

        describe(BREADCRUMBS, () => {
            const breadCrumbWrapper = wrapper.find(Breadcrumbs);
            const breadcrumbs = breadcrumbTester(breadCrumbWrapper);

            describe('Link 1', () => {
                it('should have a have a link to the learning instances page', () => {
                    expect(breadcrumbs.link(0).url()).toEqual(routeHelper('/learning-instances'));
                    expect(breadcrumbs.link(0).label()).toEqual('Learning instances');
                });
            });
            describe('Link 2', () => {
                it('should have a have a link to the learning instances page', () => {
                    expect(breadcrumbs.link(1).url()).toEqual(routeHelper(`/learning-instances/${id}`));
                    expect(breadcrumbs.link(1).label()).toEqual(projectName);
                });
            });
            describe('Link 3', () => {
                it('should have a have a link to the learning instances page', () => {
                    expect(breadcrumbs.link(2).url()).toEqual(routeHelper(`/learning-instances/${id}/bots/${botid}/edit`));
                    expect(breadcrumbs.link(2).label()).toEqual(`Train bot group ${botid}`);
                });
            });
        });

        describe(VIEWER, () => {
            const viewerWrapper = wrapper.find(DesignerPageViewer);
            const viewerWrapperProps = viewerWrapper.props();
            const viewWrapperPropKeys = Object.keys(viewerWrapperProps);
            viewWrapperPropKeys.map((key) => {
                // when
                describe(`prop: ${key}`, () => {
                    // then
                    return it('should match that of its parent component', () => {
                        expect(viewerWrapperProps[key]).toEqual(propsForViewer[key]);
                    });
                });
            });
        });

        describe(WIZARD, () => {
            const wizardWrapper = wrapper.find(DesignerPageWizard);
            const wizardWrapperProps = wizardWrapper.props();
            const viewWrapperPropKeys = Object.keys(wizardWrapperProps);
            viewWrapperPropKeys.map((key) => {
                describe(`prop: ${key}`, () => {
                    return it('should match that of its parent component', () => {
                        expect(wizardWrapperProps[key]).toEqual(propsForWizard[key]);
                    });
                });
            });
        });

        describe(MAIN_COMMAND_BUTTONS, () => {
            describe('Save button', () => {
                // does nothing at the moment
            });
        });

    });
});
