/**
*
* HeaderBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {
    Headerbar,
    Message,
    Icon,
    CommandButton,
} from 'common-frontend-components';

import {APP_NOTIFICATION_LEVELS} from '../../containers/App/constants';

function HeaderBar({title, appNotifications, removeAppNotificationById, showToolBar, onLogout, user}) {

    const tray = [
        {
            icon: (
                <div className="cog-headerbar-icon-container"><Icon aa="tray-profile"/></div>
            ),
            component: () => ( //eslint-disable-line react/display-name
                <div className="user-info-container">
                   <div className="aa-grid-row padded">
                       <span> Signed in as <strong>{user.username}</strong>. </span>
                   </div>
                    <CommandButton
                        recommended
                        fill
                        onClick={onLogout}
                    >
                        Logout
                    </CommandButton>
                </div>
            ),
        },
        {
            icon: (
                <div className="cog-headerbar-icon-container">
                    { appNotifications.length ?
                        <div className="badge">
                            {appNotifications.length > 99 ? +99 : appNotifications.length}
                        </div> : null
                    }
                    <Icon aa="tray-notifications"/>
                </div>
            ),
            component: () => ( //eslint-disable-line react/display-name
                <div>
                    {appNotifications.map((notice, index) => (
                        <div
                            className="message-tray"
                            key={`notification-${index}`}
                            onClick={(e) => {
                                e.nativeEvent.stopImmediatePropagation();
                                removeAppNotificationById(notice.id);
                            }}
                        >
                            <Message theme={notice.level}>
                                {`${notice.message} - click to remove.`}
                            </Message>
                        </div>
                    ))}
                </div>
            ),
        },
    ];

    return (
        <Headerbar
            title={title}
            tray={showToolBar ? tray : []}
        />
    );
}

HeaderBar.displayName = 'HeaderBar';

HeaderBar.propTypes = {
    appNotifications: PropTypes.arrayOf(PropTypes.shape({
        message: PropTypes.string,
        level: PropTypes.oneOf(APP_NOTIFICATION_LEVELS),
    })),
    /**
     * Remove app notification by unique identifier
     */
    removeAppNotificationById: PropTypes.func,
    /**
     * Show tool bar on right side
     */
    showToolBar: PropTypes.bool,
    /**
     * cb function on logout
     */
    onLogout: PropTypes.func,
    /**
     * User profile data
     */
    user: PropTypes.object,
};

HeaderBar.defaultProps = {
    appNotifications: [],
    removeAppNotificationById: () => {},
    showToolBar: true,
    onLogout: () => {},
    user: {},
};

export default HeaderBar;
