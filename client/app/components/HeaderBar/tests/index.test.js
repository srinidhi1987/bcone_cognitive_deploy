import {shallow} from 'enzyme';
import React from 'react';

import HeaderBar from '../index';
import {Headerbar} from 'common-frontend-components';

describe('<HeaderBar />', () => {
    // given
    const logoutSpy = jest.fn();
    const title = 'Application Name';
    const component = shallow(
        <HeaderBar
            title={title}
            onLogout={logoutSpy}
        />
    );

    it('should pass title prop to Headerbar common component', () => {
        // then
        expect(component.find(Headerbar).prop('title')).toEqual(title);
    });
});
