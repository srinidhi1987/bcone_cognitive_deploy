/**
*
* ActionConfirmationModel
*
*/
import React, {Component} from 'react';
import {autobind} from 'core-decorators';
import PropTypes from 'prop-types';
import {
    Confirm,
    TextInput,
    Announcement,
} from 'common-frontend-components';
import {fieldValidations} from '../../containers/ProjectForm/validations';

export const themeType = {
    default: 'default',
    info: 'info',
    success: 'success',
    warn: 'warn',
    error: 'error',
};

class ActionConfirmationModal extends Component {
    static displayName = 'ActionConfirmationModal';

    PropTypes = {
        /**
         * parameter which will be passed back through onConfirm handler
         */
        param: PropTypes.object.isRequired,
        /**
         * title for confirmation box
         */
        title: PropTypes.object.isRequired,
        /**
         * paragraphs to be displayed in confirmation box body
         */
        paragraphs: PropTypes.array.isRequired,
        /**
         * Callback for deletion action
         */
        onConfirm: PropTypes.func.isRequired,
        /**
         * Callback to hide modal
         */
        onHide: PropTypes.func.isRequired,
        /**
         * Show deletion modal
         */
        show: PropTypes.bool.isRequired,
        /**
         * shows title on cancel button
         */
        cancelTitle: PropTypes.string.isRequired,
        /**
         * shows title on confim button.
         */
        confirmTitle: PropTypes.string.isRequired,
        /**
         * show modal box theme for different prupose.
         */
        theme: PropTypes.oneOf(['info', 'default', 'success', 'error', 'warn']),
        /**
         * input label
         */
        inputLabel: PropTypes.string,
        /**
         * default input value
         */
        defaultInputValue: PropTypes.string,
        /**
         * validations rule to be applied on input
         */
        validations: PropTypes.arrayOf(fieldValidations),
    };

    static defaultProps = {
        show: false,
    };

    constructor(props) {
        super(props);
        const {defaultInputValue} = props;
        this.state = {
            inputVal: this.getInputValue(defaultInputValue),
            errors: null,
        };
    }

    /**
     * returns value for inputVal state
     * @param {string} value
     */
    getInputValue(value) {
        return value ? value : '';
    }

    /**
     * Handle hiding the modal
     * @returns void
     */
     @autobind
    _handleOnCancel() {
        const {onCancel, defaultInputValue} = this.props;
        this.setState({
            inputVal: this.getInputValue(defaultInputValue),
            errors: null,
        });
        onCancel();
    }


    getError(value, validations) {
        return validations
        .map((validationFn) => validationFn(value))
        .filter((result) => result);
    }

    /**
     * Confirm project deletion
     * @returns void
     */
     @autobind
    _confirm() {
        const {onConfirm, param, inputLabel} = this.props;
        const {inputVal, errors} = this.state;
        if (errors && errors.length > 0) {
            return;
        }
        inputLabel ? onConfirm(param, inputVal)
            : onConfirm(param);
    }

    render() {
        const {show, title, paragraphs, cancelTitle, confirmTitle, theme, inputLabel, validations} = this.props;
        const {inputVal, errors} = this.state;
        return (
            <Confirm
                title={title}
                show={show}
                style={{padding: 15}}
                onHide={this._handleOnCancel}
                onSubmit={this._confirm}
                theme={theme}
                labelCancel={cancelTitle}
                labelAccept={confirmTitle}
            >
                <div style={{paddingTop: 15}}>
                    {paragraphs.map((paragraph, i) => (
                        <p key={i}>{paragraph}</p>
                    ))}
                </div>
                <br/>
                {(() => {
                    if (inputLabel) {
                        return (
                            <div>
                                <p>{inputLabel}</p>
                                {errors && errors.length > 0 && (
                                    <Announcement
                                        theme="error"
                                        title={errors[0]}
                                    />
                                )}
                                <TextInput
                                    key={'confirmation-input-name'}
                                    name={'confirmation-input-name'}
                                    value={inputVal}
                                    onChange={(inputVal) =>
                                        this.setState({inputVal,
                                            errors: this.getError(
                                                inputVal,
                                                validations
                                            ),
                                        })
                                    }
                                    autoFocus={true}
                                    autoSelect={true}
                                />
                            </div>
                        );
                    }
                })()}
            </Confirm>
        );
    }
}

export default ActionConfirmationModal;
