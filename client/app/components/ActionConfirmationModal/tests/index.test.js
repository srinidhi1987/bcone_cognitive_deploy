import React from 'react';
import {mount} from 'enzyme';
import {CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT,
} from '../../../containers/ProjectsContainer/constants';
import ActionConfirmationModal from '../index';
import {
    Confirm,
} from 'common-frontend-components';
import {themeType} from '../index';
import {fieldValidations} from '../../../containers/ProjectForm/validations';
describe('Component: ActionConfirmationModal', () => {
    //given
    const paramArgument = {
        id: '567756675765',
    };
    const confirmSpy = jest.fn();
    const cancelSpy = jest.fn();
    const wrapper = mount(
        <ActionConfirmationModal
            title={CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE}
            paragraphs={[CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1, CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2]}
            cancelTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT}
            confirmTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT}
            param={paramArgument}
            show={true}
            onCancel={cancelSpy}
            onConfirm={confirmSpy}
            theme={themeType.info}
        />
    );

    it('should receive all required properties', () => {
        //when
        const {title, paragraphs, param, show, onCancel, onConfirm, cancelTitle, confirmTitle, theme} = wrapper.props();
        //then
        expect(title).toBeDefined();
        expect(paragraphs).toBeDefined();
        expect(show).toEqual(true);
        expect(param).toBeDefined();
        expect(onCancel).toBeDefined();
        expect(onConfirm).toBeDefined();
        expect(cancelTitle).toBeDefined();
        expect(confirmTitle).toBeDefined();
        expect(theme).toBeDefined();
    });
    describe('check visibility', () => {
        it('should show modal when show props is set to true', () => {
            //when
            const modal = wrapper.find(Confirm);
            //then
            expect(modal.props().show).toEqual(true);
        });
        it('should hide modal when show props is set to false', () => {
            //when
            wrapper.setProps({show:false});
            //then
            const modal = wrapper.find(Confirm);
            expect(modal.props().show).toEqual(false);
        });
    });
    describe('method: _confirm', () => {
        it('should call onConfirm props whth param props as argument when user clicks confirm button', () => {
            //given
            const {onConfirm, param} = wrapper.props();
            //when
            wrapper.instance()._confirm();
            //then
            expect(onConfirm).toHaveBeenCalledWith(param);
        });
    });
    describe('method: _handleOnCancel', () => {
        it('should call onHide props when user clicks cancel button', () => {
            //given
            const {onCancel} = wrapper.props();
            //when
            wrapper.instance()._handleOnCancel();
            //then
            expect(onCancel).toHaveBeenCalled();
        });
    });
    describe('Component: ActionConfirmationModal with inputInfo', () => {
        //given
        const confirmSpy = jest.fn();
        const cancelSpy = jest.fn();
        const wrapperWithInput = mount(
            <ActionConfirmationModal
                title={CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE}
                paragraphs={[CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1, CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2]}
                cancelTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT}
                confirmTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT}
                param={paramArgument}
                show={true}
                onCancel={cancelSpy}
                onConfirm={confirmSpy}
                theme={themeType.info}
                inputLabel={'EXPORT_CONFIRMATION_BOX_INPUT_LABEL'}
                validations={[
                    fieldValidations.required,
                    fieldValidations.format,
                    fieldValidations.noOutsideWhitespace,
                    fieldValidations.noRepeatingWhitespace,
                ]}
            />
        );
        const {validations} = wrapperWithInput.props();

        describe('method: getError', () => {

            it('should return errors based on invalid input', () => {
                //when
                const result = wrapperWithInput.instance().getError(' filename', validations);
                //then
                expect(result.length).toEqual(1);
            });

            it('should return more errors', () => {
                //when
                const result = wrapperWithInput.instance().getError(' file  name#', validations);
                //then
                expect(result.length).toEqual(3);
            });

            it('should return no errors for valid input', () => {
                //when
                const result = wrapperWithInput.instance().getError('file name', validations);
                //then
                expect(result.length).toEqual(0);
            });
        });

        describe('method: _confirm', () => {

            it('should call onConfirm props whth param props as argument along with input value when user clicks confirm button', () => {
                //given
                const inputVal = 'inputVal';
                wrapperWithInput.setState({inputVal, errors: wrapperWithInput.instance().getError(inputVal, validations)});
                const {onConfirm, param} = wrapperWithInput.props();
                //when
                wrapperWithInput.instance()._confirm();
                //then
                expect(onConfirm).toHaveBeenCalledWith(param, inputVal);
            });
        });
    });
});
