import React from 'react';
import {mount} from 'enzyme';
import SaveVisionBotConfirmationModal from '../index';
import {
    Modal,
} from 'common-frontend-components';

describe('Component: SaveVisionBotConfirmationModal', () => {

    const confirmSpy = jest.fn();
    const cancelSpy = jest.fn();
    const confirmAndSendToProductionSpy = jest.fn();
    const wrapper = mount(
        <SaveVisionBotConfirmationModal
            cancelHandler={cancelSpy}
            saveAndSendDisabled={false}
            saveBotAndSendToProductionConfirmHandler={confirmAndSendToProductionSpy}
            saveBotConfirmHandler={confirmSpy}
            show={true}
        />
    );

    it('should receive all required properties', () => {
        //when
        const {
            cancelHandler,
            saveAndSendDisabled,
            saveBotAndSendToProductionConfirmHandler,
            saveBotConfirmHandler,
            show,
        } = wrapper.props();
        //then
        expect(cancelHandler).toBeDefined();
        expect(show).toEqual(true);
        expect(saveAndSendDisabled).toEqual(false);
        expect(saveBotAndSendToProductionConfirmHandler).toBeDefined();
        expect(saveBotConfirmHandler).toBeDefined();
    });

    describe('check visibility', () => {

        it('should show modal when show props is set to true', () => {
            //when
            const modal = wrapper.find(Modal);
            //then
            expect(modal.props().show).toEqual(true);
        });

        it('should hide modal when show props is set to false', () => {
            //when
            wrapper.setProps({show:false});
            //then
            const modal = wrapper.find(Modal);
            expect(modal.props().show).toEqual(false);
            wrapper.setProps({show:true});
        });
    });

    describe('method: _saveHandler', () => {
        it('should call saveBotConfirmHandler props with clickSource props as argument when user clicks save button', () => {
            //given
            const {saveBotConfirmHandler} = wrapper.props();
            //when
            wrapper.instance()._saveHandler();
            //then
            expect(saveBotConfirmHandler).toHaveBeenCalled();
        });
    });

    describe('method: _saveBotAndSendToProductionConfirmHandler', () => {
        it('should call saveBotAndSendToProductionConfirmHandler props when user clicks save and send to production button', () => {
            //given
            const {saveBotAndSendToProductionConfirmHandler} = wrapper.props();
            //when
            wrapper.instance()._saveBotAndSendToProductionConfirmHandler();
            //then
            expect(saveBotAndSendToProductionConfirmHandler).toHaveBeenCalled();
        });
    });

    describe('method: _cancelHandler', () => {
        it('should call cancelHandler props when user clicks cancel button', () => {
            //given
            const {cancelHandler} = wrapper.props();
            //when
            wrapper.instance()._cancelHandler();
            //then
            expect(cancelHandler).toHaveBeenCalled();
        });
    });
});
