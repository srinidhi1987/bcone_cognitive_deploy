import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {autobind} from 'core-decorators';
import {
    CommandButton,
    GridLayout,
    Modal,
} from 'common-frontend-components';
import {themeType} from '../ActionConfirmationModal';
import './styles.scss';

class SaveVisionBotConfirmationModal extends Component {
    static displayName = 'SaveVisionBotConfirmationModal';
    PropTypes = {
        /**
         * cancel click handler
         */
        cancelHandler: PropTypes.func.isRequired,
        /**
         * determin the enable propert of button save and send to production
         */
        saveAndSendDisabled: PropTypes.bool.isRequired,
        /**
         * save and send to production click handler
         */
        saveBotAndSendToProductionConfirmHandler: PropTypes.func.isRequired,
        /**
         * save only handler
         */
        saveBotConfirmHandler: PropTypes.func.isRequired,
        /**
         * Show deletion modal
         */
        show: PropTypes.bool.isRequired,
    };

    static defaultProps = {
        show: false,
    };

    constructor(props) {
        super(props);

    }

    @autobind _cancelHandler() {
        const {cancelHandler} = this.props;
        cancelHandler();
    }

    @autobind _saveHandler() {
        const {saveBotConfirmHandler} = this.props;
        saveBotConfirmHandler();
    }

    @autobind _saveBotAndSendToProductionConfirmHandler() {
        const {saveBotAndSendToProductionConfirmHandler} = this.props;
        saveBotAndSendToProductionConfirmHandler();
    }

    render() {
        const {
            show,
            saveAndSendDisabled,
        } = this.props;

        return (
            <Modal
                show={show}
                theme={themeType.info}
                style={{width: 500, padding: 10}}
            >
                <div className="confirmation">
                    <GridLayout>
                        <GridLayout.Row>
                            <GridLayout.Column>
                                <div className="confirmation-body">
                                    <h2 className="title">You are about to save current training.</h2>
                                    <p>
                                        Do you want to send this learning instance to production?
                                    </p>
                                    <p>
                                        You have following options:
                                    </p>
                                    <ul>
                                        <li>Cancel to stay on current training</li>
                                        <li>Save this training only but don't send to production</li>
                                        <li>Save this training and send to production</li>
                                    </ul>
                                </div>
                            </GridLayout.Column>
                        </GridLayout.Row>
                        <GridLayout.Row>
                            <GridLayout.Column xs={3}>
                                <CommandButton onClick={this._cancelHandler} theme={themeType.info}>
                                    Cancel
                                </CommandButton>
                            </GridLayout.Column>
                            <GridLayout.Column xs={3}>
                                <CommandButton onClick={this._saveHandler} theme={themeType.info}>
                                    Save
                                </CommandButton>
                            </GridLayout.Column>
                            <GridLayout.Column xs={3}>
                                <CommandButton
                                    onClick={this._saveBotAndSendToProductionConfirmHandler}
                                    recommended
                                    disabled={saveAndSendDisabled}
                                    theme={themeType.info}
                                >
                                    Save and send to production
                                </CommandButton>
                            </GridLayout.Column>
                        </GridLayout.Row>
                    </GridLayout>
                </div>
            </Modal>
        );
    }
}

export default SaveVisionBotConfirmationModal;
