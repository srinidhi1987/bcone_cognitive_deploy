import React, {Component} from 'react';
import {CommandButton} from 'common-frontend-components';

class FileUploadButton extends Component {
    static displayName = 'FileUploadButton';

    constructor(props) {
        super(props);
    }

    render() {
        const {acceptTypes, onFileSelected, type} = this.props;
        return (
            <div className="upload-button aa-grid-row">
                <CommandButton
                    recommended
                    fill
                    theme="success"
                    onClick={() => this.input.click()}
                >
                    Upload
                </CommandButton>
                <input
                    ref={(input) => {
                        this.input = input;
                    }}
                    type={type}
                    style={{display: 'none'}}
                    accept={acceptTypes}
                    onChange={(event) => {
                        onFileSelected(event.target.files);
                    }}
                />
            </div>
        );
    }
}

export default FileUploadButton;
