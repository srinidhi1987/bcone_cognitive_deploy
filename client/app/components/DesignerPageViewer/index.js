/**
*
* DesignerPageViewer
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import DesignerPageControlBar from '../DesignerPageControlBar';
import AnnotationEngine from '../AnnotationEngine';
import routeHelper from '../../utils/routeHelper';

import classnames from 'classnames';
import {IconButton} from 'common-frontend-components';
import './styles.scss';

const DesignerPageViewer = (
    {
        activeField,
        activeFieldId,
        activeFieldOption,
        annotationClasses,
        areaClickHandler,
        canNavigateBotPage,
        currentDocPage,
        currentZoomScale,
        docMeta,
        handleResetZoom,
        handleZoomIn,
        handleZoomOut,
        imageLoadHandler,
        imageLoaded,
        mode,
        navigateNextBotDocPageHandler,
        navigatePreviousBotDocPageHandler,
        objects,
        onUpdate,
        pageData,
        pageInputChangeHandler,
        pageInputValue,
        scaleAmount,
        setLayoutRatio,
        startDragHandler,
        staticAnnotations,
        stopDragHandler,
        changePage,
        url,
    }
) => (
    <div className={'designer-page-viewer aa-grid-column'}>
        <div className={'designer-page-viewer--body flex-1'}>
            <AnnotationEngine
                activeField={activeField}
                activeFieldId={activeFieldId}
                activeFieldOption={activeFieldOption}
                annotationClass={annotationClasses.length > 0 ? annotationClasses[0] : ''}
                areaClickHandler={areaClickHandler}
                currentZoomScale={currentZoomScale}
                docMeta={docMeta}
                handleResetZoom={handleResetZoom}
                handleZoomIn={handleZoomIn}
                handleZoomOut={handleZoomOut}
                imageLoadHandler={imageLoadHandler}
                imageLoaded={imageLoaded}
                mode={mode}
                objects={objects}
                onUpdate={onUpdate}
                scaleAmount={scaleAmount}
                setLayoutRatio={setLayoutRatio}
                startDragHandler={startDragHandler}
                staticAnnotations={staticAnnotations}
                stopDragHandler={stopDragHandler}
                url={url}
            />
        </div>
        <DesignerPageControlBar>
            <div className="designer-page-viewer--control-bar aa-grid-row vertical-center">
                <span className="page-navigation-buttons">
                    <span className={canNavigateBotPage.next ? '' : 'disabled'}>
                        <IconButton
                            aa="misc-search-filter-dropdown-arrow"
                            disabled={!canNavigateBotPage.next}
                            onClick={canNavigateBotPage.next ? navigateNextBotDocPageHandler : null}/>
                    </span>
                    <span className={canNavigateBotPage.previous ? '' : 'disabled'}>
                        <IconButton
                            aa="misc-search-filter-dropdown-arrow"
                            className={canNavigateBotPage.previous ? 'designer-page-viewer--updwards-arrow' : classnames('designer-page-viewer--updwards-arrow', 'disabled')}
                            disabled={!canNavigateBotPage.previous}
                            onClick={canNavigateBotPage.previous ? navigatePreviousBotDocPageHandler : null} />
                    </span>
                </span>
                <span className="designer-page-viewer--current-page-number">{currentDocPage} of {pageData.length} pages &nbsp;or</span>
                <form onSubmit={changePage}>
                    <input
                        className="page-input"
                        onChange={pageInputChangeHandler}
                        oninput="validity.valid||(value='')"
                        placeholder="Go to page"
                        type="number"
                        value={pageInputValue} />
                </form>

                <button
                    className="designer-page-viewer--zoom-in"
                    onClick={handleZoomIn}
                >
                    <img src={routeHelper('images/zoom-in.svg')}/>
                    <span>Zoom in</span>
                </button>
                <button
                    className="designer-page-viewer--zoom-out"
                    onClick={handleZoomOut}
                >
                    <img src={routeHelper('images/zoom-out.svg')}/>
                    <span>Zoom out</span>
                </button>
                <button
                    className="designer-page-viewer--zoom-out"
                    onClick={handleResetZoom}
                >
                    <img src={routeHelper('images/zoom-reset.svg')}/>
                    <span>Fit to screen</span>
                </button>
            </div>
        </DesignerPageControlBar>
    </div>
);

DesignerPageViewer.displayName = 'DesignerPageViewer';
DesignerPageViewer.propTypes = {
    activeField: PropTypes.object,
    activeFieldId: PropTypes.string,
    activeFieldOption: PropTypes.string,
    annotationClasses: PropTypes.arrayOf(PropTypes.string).isRequired,
    areaClickHandler: PropTypes.func,
    currentZoomScale: PropTypes.number.isRequired,
    docMeta: PropTypes.object.isRequired,
    handleResetZoom: PropTypes.func.isRequired,
    handleZoomIn: PropTypes.func.isRequired,
    handleZoomOut: PropTypes.func.isRequired,
    mode: PropTypes.number,
    objects: PropTypes.arrayOf(PropTypes.object).isRequired,
    onUpdate: PropTypes.func.isRequired,
    scaleAmount: PropTypes.number.isRequired,
    setLayoutRatio: PropTypes.func.isRequired,
    startDragHandler: PropTypes.func.isRequired,
    staticAnnotations: PropTypes.arrayOf(PropTypes.object),
    stopDragHandler: PropTypes.func.isRequired,
    url: PropTypes.string,
};

export default DesignerPageViewer;
