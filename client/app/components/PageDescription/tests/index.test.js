import PageDescription from '../index';

import {shallow} from 'enzyme';
import React from 'react';

describe('<PageDescription />', () => {
    it('should render children', () => {
        // given
        const description = 'Automation Anywhere Cognitive Console page description';
        // when
        const component = shallow(
            <PageDescription>
                {description}
            </PageDescription>
        );
        // then
        expect(component.contains(description)).toEqual(true);
    });
});
