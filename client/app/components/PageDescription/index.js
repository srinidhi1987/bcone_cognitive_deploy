/**
*
* PageDescription
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

function PageDescription({children}) {
    return (
        <div className="aa-page-description">
            {children}
        </div>
    );
}

PageDescription.displayName = 'PageDescription';

PageDescription.propTypes = {
    children: PropTypes.node,
};

export default PageDescription;
