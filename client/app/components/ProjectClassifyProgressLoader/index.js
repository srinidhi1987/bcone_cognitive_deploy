/**
*
* ProjectClassifyProgressLoader
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {Tooltip} from 'common-frontend-components';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const ProjectClassifyProgressLoader = ({progress}) => (
    <Tooltip icon={(
        <div className="aa-project-classify-progress">
            <div className="aa-project-classify-progress-loader">
                <img src={routeHelper('images/ring-loader.gif')} />
            </div>

            <div className="aa-project-classify-progress-icon">
                <img src={routeHelper('images/classify-icon.svg')} />
            </div>
        </div>
    )}>
        Classification in progress: {progress}%
    </Tooltip>
);

ProjectClassifyProgressLoader.displayName = 'ProjectClassifyProgressLoader';

ProjectClassifyProgressLoader.propTypes = {
    progress: PropTypes.number,
};

ProjectClassifyProgressLoader.defaultProps = {
    progress: 0,
};

export default ProjectClassifyProgressLoader;
