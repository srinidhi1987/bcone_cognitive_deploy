import React from 'react';
import {shallow} from 'enzyme';
import {Tooltip} from 'common-frontend-components';

import ProjectClassifyProgressLoader from '../index';

describe('<ProjectClassifyProgressLoader />', () => {
    // given
    const wrapper = shallow(
        <ProjectClassifyProgressLoader
            progress={24}
        />
    );
    // when
    const result = wrapper.find(Tooltip);

    it('should be wrapped in a tooltip to show classification percentage', () => {
        // given
        // when
        // then
        expect(result.length).toEqual(1);
    });

    it('should display string in tooltip of classification progress', () => {
        // given
        // when
        // then
        expect(result.props().children).toEqual(['Classification in progress: ', 24, '%' ]);
    });
});
