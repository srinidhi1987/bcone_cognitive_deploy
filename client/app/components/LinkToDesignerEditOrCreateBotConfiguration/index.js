/**
*
* LinkToDesignerEditOrCreateBotConfiguration
*
*/

import React from 'react';
import LinkToDesigner from '../LinkToDesigner';
import {Icon} from 'common-frontend-components';
import './styles.scss';

const LinkToDesignerEditOrCreateBotConfiguration = ({projectId, appConfig, categoryId, categoryName, visionBot, disabled}) => (
    <LinkToDesigner
        projectId={projectId}
        appConfig={appConfig}
        categoryId={categoryId}
        categoryName={categoryName}
        disabled={disabled}
    >
        <div className={disabled ? 'disabled-link-to-designer' : ''}>
            <Icon
                fa="adjust"
            />
            {(visionBot.id) ? 'Edit Bot' : 'Create Bot'}
        </div>
    </LinkToDesigner>
);

LinkToDesignerEditOrCreateBotConfiguration.displayName = 'LinkToDesignerEditOrCreateBotConfiguration';

export default LinkToDesignerEditOrCreateBotConfiguration;
