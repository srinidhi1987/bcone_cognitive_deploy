import React from 'react';
import {shallow} from 'enzyme';

import LinkToDesignerEditOrCreateBotConfiguration from '../index';
import {Icon} from 'common-frontend-components';
import LinkToDesigner from '../../LinkToDesigner';
import {Category1} from '../../../../fixtures/categories';

describe('Component: LinkToDesignerEditOrCreateBotConfiguration', () => {
    const standardProps = {
        categoryId: Category1.id,
        categoryName: Category1.name,
        projectId: 'j3h2j42j3h424h234',
        appConfig: {},
    };

    describe('when a bot has already been created', () => {
        const props = {
            ...standardProps,
            children: (
                <div className="">
                    <Icon
                        fa="adjust"
                    />
                    Edit Bot
                </div>
            ),
        };

        const wrapper = shallow(
            <LinkToDesignerEditOrCreateBotConfiguration {...props} visionBot={{id:'23232'}}/>
        );

        it('should pass its props to the LinkToDesigner component with message Edit Bot', () => {
            const linkToDesignerProps = wrapper.find(LinkToDesigner).props();
            expect(linkToDesignerProps).toEqual(props);
        });
    });

    describe('when a bot has not yet been created', () => {
        const props = {
            ...standardProps,
            children: (
                <div className="">
                    <Icon
                        fa="adjust"
                    />
                    Create Bot
                </div>
            ),
        };

        const wrapper = shallow(
            <LinkToDesignerEditOrCreateBotConfiguration {...props} visionBot={{}}/>
        );

        it('should pass its props to the LinkToDesigner component with message Create Bot', () => {
            const linkToDesignerProps = wrapper.find(LinkToDesigner).props();
            expect(linkToDesignerProps).toEqual(props);
        });
    });
});
