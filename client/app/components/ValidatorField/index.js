import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
    Icon,
} from 'common-frontend-components';
import {autobind} from 'core-decorators';
import './styles.scss';
import {getFieldIconElement} from '../../utils/designer';
class ValidatorField extends PureComponent {
    displayName = 'ValidatorField';

    PropTypes = {
        field: PropTypes.object.isRequired,
        fieldIndex: PropTypes.number.isRequired,
        isFormField: PropTypes.bool.isRequired,
        readonly: PropTypes.bool,
        rowIndex: PropTypes.number,
        selectFieldHandler: PropTypes.func.isRequired,
        setFieldValueHandler: PropTypes.func.isRequired,
        tableIndex: PropTypes.number,
    };

    constructor(props) {
        super(props);
    }

    renderFieldStatusIcon(isValid, isConfident) {
        if (!isValid) {
            return <Icon fa="exclamation-circle" className="field-validation-error-color"/>;
        }
        if (!isConfident) {
            return <Icon fa="exclamation-triangle" className="field-confidence-warning-color"/>;
        }

        return <i className={classnames('validation-icon', {
            'icon--normal' : !isConfident,
        })}></i>;
    }

    @autobind _onFocus(event) {
        const {selectFieldHandler, field} = this.props;
        selectFieldHandler(event, field);
    }

    @autobind _onChangeHandler(event) {
        const {setFieldValueHandler, isFormField, tableIndex, rowIndex, fieldIndex} = this.props;
        if (isFormField) return setFieldValueHandler('Text', event.target.value, fieldIndex);

        return setFieldValueHandler('Text', event.target.value, tableIndex, rowIndex, fieldIndex);
    }
    render() {
        const {
            field,
            isFormField,
            readOnly,
        } = this.props;
        const {
            isActive,
            displayName,
            text,
            isValid,
            isConfident,
            popupMessage,
            formFieldIcon: fieldIcon,
            validationMessageByValidationIssue,
            isValidByValidationIssue,
        } = field;
        //if field is readonly (preview field) then use validation done by validation issue (isValidByValidationIssue) or use validaiton done on field value at client end.(isValid)
        const isFieldValid = readOnly ? isValidByValidationIssue : isValid;
        //deciding which error message to display based on readonly
        const validationErrorMessage = readOnly ? validationMessageByValidationIssue : popupMessage;
        const icon = getFieldIconElement(fieldIcon);

        const selectRowClass = classnames('aa-grid-row', 'field-row', {
            'field-row--bottom-broder': isFormField,
        });
        const selectValueClass = classnames('aa-grid-row', 'vertical-center', {
            'value': isFormField,
            'table-value': !isFormField,
        });
        const highlightCSSClass = classnames('standard-input', {
            'standard-input--active' : isActive,
            'standard-input--error' : !isFieldValid,
            'standard-input--low-confidence': isFieldValid && !isConfident,
        });

        return (
            <div className="field-box">
                <div className={selectRowClass}>
                    {
                        isFormField &&
                        <div className="label aa-grid-row vertical-center">
                            {icon}
                            {displayName}
                        </div>
                    }
                    <div className={selectValueClass}>
                        <div
                            className={highlightCSSClass}
                            onFocus={this._onFocus}
                            onBlur={this._onBlur}
                            data-area-type={'field'}
                            onMouseEnter={this._onMouseEnter}
                            onMouseLeave={this._onMouseLeave}
                        >
                            <input
                                onChange={this._onChangeHandler}
                                readOnly={readOnly}
                                value={text}
                                title={validationErrorMessage}
                            />
                        </div>
                        {isFormField && this.renderFieldStatusIcon(isFieldValid, isConfident)}
                    </div>
                </div>
            </div>
        );
    }
}

export default ValidatorField;
