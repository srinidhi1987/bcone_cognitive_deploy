/**
*
* ProjectPreviewPane
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import {FormattedNumber} from 'react-intl';
import {
    ReadingPane,
    Icon,
    DataTable,
} from 'common-frontend-components';
import {DataField, DataType} from 'common-frontend-utils';

import FlexGrid from '../FlexGrid';
import ProgressBar from '../ProgressBar';
import LinkToDesignerEditOrCreateBotConfiguration from '../LinkToDesignerEditOrCreateBotConfiguration';
import {TRAINING_PERCENTAGE} from '../../constants/productTerms';
import routeHelper from '../../utils/routeHelper';


import './styles.scss';

function ProjectPreviewPane({title, data, supportedLanguages, appConfig}) {
    const itemStyle = {
        width: '50%',
        paddingLeft: 0,
        paddingRight: 0,
    };

    const dataForTable = Array.isArray(data.categories) ? data.categories.map((category) => {
        return {
            id: category.id,
            name: category.name,
            bot: {
                category,
                project: data,
            },
            priority: category.priority,
            fileCount: category.fileCount,
        };
    }) : null;

    /**
     * Table fields
     */
    const projectFields = [
        new DataField({id: 'name', label: 'Name'}),
        new DataField({id: 'bot', label: 'IQ bot', type: DataType.STRING, render: ({project, category}) => { //eslint-disable-line react/display-name
            const {id, name, visionBot} = category;

            return (
                <LinkToDesignerEditOrCreateBotConfiguration
                    projectId={project.id}
                    categoryId={id}
                    categoryName={name}
                    appConfig={appConfig}
                    shouldLinkToWebDesigner={true}
                    visionBot={visionBot}
                />
            );
        }}),
        new DataField({id: 'fileCount', label: '# of files', type: DataType.NUMBER, render: (count) => ( //eslint-disable-line react/display-name
            <FormattedNumber value={count} />
        )}),
    ];

    // Render if data prop is present
    if (data) {
        return (
            <ReadingPane
                title={title || data.name}
                icon={(
                    <Icon fa={data.environment === 'production' ? 'connectdevelop' : 'square-o'}/>
                )}
            >
                <div className="aa-project-preview">
                    <FlexGrid wrap={true}>
                        <FlexGrid.Item itemStyle={{width: '100%', paddingLeft: 0, paddingRight: 0}}>
                            <div className="aa-project-preview-info">
                                {TRAINING_PERCENTAGE.label}

                                <div className="aa-project-preview-info--value">
                                    <ProgressBar current={Math.floor(data.currentTrainedPercentage)} />
                                </div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item itemStyle={itemStyle}>
                            <div className="aa-project-preview-info">
                                # of vision bots

                                <div className="aa-project-preview-info--value">
                                    <FormattedNumber value={data.visionBotCount} />
                                </div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item itemStyle={itemStyle}>
                            <div className="aa-project-preview-info">
                                &#37; accuracy

                                <div className="aa-project-preview-info--value">
                                    {Math.floor(data.accuracyPercentage)}%
                                </div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item itemStyle={itemStyle}>
                            <div className="aa-project-preview-info">
                                Description

                                <div className="aa-project-preview-info--value">
                                    {data.description ? data.description : '-'}
                                </div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item itemStyle={itemStyle}>
                            <div className="aa-project-preview-info">
                                # of files

                                <div className="aa-project-preview-info--value">
                                    <FormattedNumber value={data.numberOfFiles} />
                                </div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item itemStyle={itemStyle}>
                            <div className="aa-project-preview-info">
                                Primary language of files

                                <div className="aa-project-preview-info--value">
                                    {supportedLanguages.reduce((acc, lang) => {
                                        if (lang.id.toString() === data.primaryLanguage) {
                                            return lang.name;
                                        }
                                        return acc;
                                    }, 'N/A')}
                                </div>
                            </div>
                        </FlexGrid.Item>

                        <FlexGrid.Item itemStyle={itemStyle}>
                            <div className="aa-project-preview-info">
                                Environment

                                <div className="aa-project-preview-info--value">
                                    {data.environment}
                                </div>
                            </div>
                        </FlexGrid.Item>
                    </FlexGrid>

                    {/** Categories table **/}
                    {dataForTable ? (
                        <div>
                            <div className="aa-project-preview-foldercount">
                                Groups (<FormattedNumber value={data.numberOfCategories} />)
                            </div>

                            <DataTable
                                data={dataForTable}
                                dataId="id"
                                fields={projectFields}
                            />
                        </div>
                    ) : <div className="aa-project-preview-loading"><img src={routeHelper('images/ring-loader.gif')}/></div>}
                    {/** End Categories table **/}
                </div>
            </ReadingPane>
        );
    }

    return null;
}

ProjectPreviewPane.displayName = 'ProjectPreviewPane';

ProjectPreviewPane.propTypes = {
    appConfig: PropTypes.object,
    /**
     * Project title
     */
    title: PropTypes.string,
    /**
     * Project data
     */
    data: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool,
    ]),
    supportedLanguages: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        name: PropTypes.string,
    })).isRequired,
};

ProjectPreviewPane.defaultProps = {
    data: false,
    supportedLanguages: [],
};

export default ProjectPreviewPane;
