import {shallow} from 'enzyme';
import React from 'react';
import {
    ReadingPane,
} from 'common-frontend-components';

import {Project1} from '../../../../fixtures/projects';
import ProjectPreviewPane from '../index';
import FlexGrid from '../../FlexGrid';

describe('<ProjectPreviewPane />', () => {
    let title, component;

    beforeEach(() => {
        // given
        title = 'Invoice Project';
    });

    describe('props', () => {
        it('should pass in title and populate ReadingPane title', () => {
            // given
            // when
            component = shallow(
                <ProjectPreviewPane
                    title={title}
                    data={Project1}
                />
            );
            // then
            expect(component.find(ReadingPane).first().props().title).toEqual(title);
        });

        describe('project icon', () => {
            it('should show staging based on environment', () => {
                // given
                // when
                component = shallow(
                    <ProjectPreviewPane
                        title={title}
                        data={Project1}
                    />
                );
                // then
                expect(component.find(ReadingPane).first().props().icon.props.fa).toEqual('square-o');
            });

            it('should show production based on environment', () => {
                // given
                // when
                component = shallow(
                    <ProjectPreviewPane
                        title={title}
                        data={{
                            ...Project1,
                            environment: 'production',
                        }}
                    />
                );
                // then
                expect(component.find(ReadingPane).first().props().icon.props.fa).toEqual('connectdevelop');
            });
        });
    });

    describe('', () => {
        beforeEach(() => {
            // when
            component = shallow(
                <ProjectPreviewPane
                    title={title}
                    data={Project1}
                />
            );
        });

        it('should render preview container', () => {
            // then
            expect(component.find('.aa-project-preview').length).toEqual(1);
        });

        it('should render the FlexGrid component with 7 flex items to hold project information', () => {
            // then
            expect(component.find(FlexGrid.Item).length).toEqual(7);
        });
    });
});
