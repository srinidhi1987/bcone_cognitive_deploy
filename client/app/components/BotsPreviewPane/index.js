/**
*
* BotsPreviewPane
*
*/

import React from 'react';
import {FormattedNumber} from 'react-intl';
import {
    ReadingPane,
    Icon,
} from 'common-frontend-components';

import {BOT_STATUSES} from '../../constants/enums';
import FlexGrid from '../FlexGrid';
import PercentRing from '../PercentRing';

import './styles.scss';

const getPercentage = (current, total) => {
    if (current === 0 || total === 0) return 0;

    const percentage = Number(Math.floor((current / total) * 100 + 'e+1') + 'e-1'); // eslint-disable-line prefer-template

    return isNaN(percentage) ? 0 : percentage;
};

const _isBotInProduction = (bot) => {
    return bot.environment === 'production';
};

const BotsPreviewPane = ({bot}) => {
    const {botRunDetails} = bot || {};
    const {validatorReviewDetails} = botRunDetails || {};

    if (bot) {
        return (
            <ReadingPane
                title={bot.categoryName}
                icon={(
                    <Icon fa={_isBotInProduction(bot) ? 'connectdevelop' : 'square-o'}/>
                )}
            >
                <div className="aa-project-bot-preview">
                    <div className="aa-project-bot-preview-percentage">
                        <PercentRing
                            percentage={getPercentage(botRunDetails.processedDocumentCount, botRunDetails.totalFiles)}
                        />
                    </div>

                    <FlexGrid>
                        <FlexGrid.Item>
                            <span className="aa-project-bot-preview--title">
                                Files Processed
                            </span>

                            <FormattedNumber value={botRunDetails.processedDocumentCount} /> / <FormattedNumber value={botRunDetails.totalFiles} />
                        </FlexGrid.Item>
                    </FlexGrid>

                    <FlexGrid>
                        <FlexGrid.Item>
                            <span className="aa-project-bot-preview--title">
                                Files successfully processed
                            </span>

                            <FormattedNumber value={botRunDetails.passedDocumentCount} />
                        </FlexGrid.Item>
                    </FlexGrid>

                    <FlexGrid>
                        <FlexGrid.Item>
                            <span className="aa-project-bot-preview--title">
                                Files sent for validation
                            </span>

                            <FormattedNumber value={botRunDetails.failedDocumentCount} />
                        </FlexGrid.Item>
                    </FlexGrid>

                    <FlexGrid>
                        <FlexGrid.Item>
                            <span className="aa-project-bot-preview--title">
                                Document Accuracy
                            </span>

                            Average: {Math.floor(botRunDetails.documentProcessingAccuracy)}%
                        </FlexGrid.Item>
                    </FlexGrid>

                    <FlexGrid>
                        <FlexGrid.Item>
                            <span className="aa-project-bot-preview--title">
                                Field Accuracy
                            </span>

                            Average: {Math.floor(botRunDetails.fieldAccuracy)}%
                        </FlexGrid.Item>
                    </FlexGrid>

                    {(() => {
                        if (validatorReviewDetails && _isBotInProduction(bot) && [BOT_STATUSES.READY, BOT_STATUSES.ACTIVE].indexOf(bot.status) !== -1) {
                            return (
                                <FlexGrid>
                                    <FlexGrid.Item>
                                        <span className="aa-project-bot-preview--title">
                                            Validation
                                        </span>

                                        <FlexGrid className="aa-project-bot-preview-validations">
                                            <FlexGrid.Item>
                                                Files validated {validatorReviewDetails.reviewFileCount} / {botRunDetails.failedDocumentCount} ({getPercentage(validatorReviewDetails.reviewFileCount, botRunDetails.failedDocumentCount)}%)
                                            </FlexGrid.Item>

                                            <FlexGrid.Item>
                                                Marked as invalid {validatorReviewDetails.invalidFileCount} / {botRunDetails.failedDocumentCount} ({getPercentage(validatorReviewDetails.invalidFileCount, botRunDetails.failedDocumentCount)}%)
                                            </FlexGrid.Item>

                                            <FlexGrid.Item>
                                                Average time spent to validate {validatorReviewDetails.averageReviewTime}s
                                            </FlexGrid.Item>
                                        </FlexGrid>
                                    </FlexGrid.Item>
                                </FlexGrid>
                            );
                        }
                    })()}
                </div>
            </ReadingPane>
        );
    }

    return null;
};

BotsPreviewPane.displayName = 'BotsPreviewPane';

export default BotsPreviewPane;
