/**
 *
 * @param {Object} object
 * @param {{clientX: number, clientY: number, objectX: number, objectY: number}} startPoint
 * @param {{x: number, y: number} mouse
 * @returns {{object: ...{width: number, height: number}, x: number, y: number}}
 */
export default ({object, startPoint, mouse}) => {
    return {
        ...object,
        x: mouse.x - (startPoint.clientX - startPoint.objectX),
        y: mouse.y - (startPoint.clientY - startPoint.objectY),
    };
};
