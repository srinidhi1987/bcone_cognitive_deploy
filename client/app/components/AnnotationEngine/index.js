/**
*
* AnnotationEngine
*
*/

import React from 'react';
import classnames from 'classnames';

import SvgRenderer from '../SvgRenderer';

import Rect from '../Rect';
import * as actions from './activities/';
import {ANNOTATION_MODES} from './constants/enums';
import {register, unregister} from 'common-frontend-utils';
import './styles.scss';
import {FIELD_OPTIONS} from '../../constants/enums';
const initialState = {
    mode: ANNOTATION_MODES.FREE, // this was previosly ANNOTATION_MODES.FREE
    handler: {
        top: 200,
        left: 200,
        width: 50,
        height: 50,
        rotate: 0,
    },
    axisGuide: {
        x: -1,
        y: -1,
    },
    currentObjectIndex: null,
    selectedTool: null,
    aspectRatio: 1, // aspect ratio of the document with it's original dimention.
    layoutRatio: 1, //layout ratio with respect to document original height and doc height
    height:0, //height of the left panel document window.
    width:0,  //width of the left panel document window.
    docWidth: 0, // width of the document (50% of designer window).
    docHeight: 0, // height of the document (docWidth * aspectRatio).
    zoomScaledWidth: 0, // offset for the document width.
    zoomScaledHeight: 0, // offset for the document height.
    staticAnnotations: null,
    scrollPosition: {
        left: 0,
        top: 0,
    },
    imageLoading: false,
};


class AnnotationEngine extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'AnnotationEngine';

    static defaultProps = {
        snapToGrid: 1,
    };

    constructor(props) {
        super(props);
        this.state = initialState;
        this.objectRefs = {};
        this.getOffsetValue = this.getOffsetValue.bind(this);
        this.applyWindowOffset = this.applyWindowOffset.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.createStaticAnnotation = this.createStaticAnnotation.bind(this);
        this.scrollHandler = this.scrollHandler.bind(this);
    }

    render() {
        const canvas = this.getCanvas();
        // const {canvasHeight, canvasOffsetX, canvasOffsetY} = canvas; // for later
        const {
            mode,
            width,
            height,
            axisGuide,
            docHeight,
            docWidth,
            zoomScaledWidth,
            zoomScaledHeight,
        } = this.state;
        const {
            areaClickHandler,
            imageLoaded,
            imageLoadHandler,
            objects,
            staticAnnotations,
            url,
            handleZoomIn,
            handleZoomOut,
        } = this.props;
        const scaledObjects = objects.map(this.applyWindowOffset);
        const scaledStaticAnnotations = staticAnnotations ? staticAnnotations.map(this.applyWindowOffset) : [];

        return (
            <div
                className={classnames('designer', {
                    'designer--crosshair' : this.props.mode === ANNOTATION_MODES.DRAW,
                })}
                onMouseMove={this.onDrag.bind(this)}
                onMouseUp={this.stopDrag.bind(this)}
                onMouseLeave={() => {
                    this.hideAxisGuide();
                }}
                ref={(ref) => {
                    if (ref)
                        this._designerRef = ref;
                    return;
                }}
                onScroll={this.scrollHandler}
                onWheel={(e) => {
                    if (e.altKey) {
                        e.preventDefault();
                        e.deltaY >= 1 ? handleZoomIn() : handleZoomOut();
                        return;
                    }
                }}
            >
                <SvgRenderer
                    axisGuide={axisGuide}
                    canvas={canvas}
                    areaClickHandler={areaClickHandler}
                    docHeight={docHeight}
                    docWidth={docWidth}
                    imageLoaded={imageLoaded}
                    onImageLoad={imageLoadHandler}
                    width={width}
                    height={height}
                    mode={mode}
                    objects={scaledObjects}
                    objectRefs={this.objectRefs}
                    onMouseDown={this.newObject.bind(this)}
                    onRender={(ref) => {
                        this.svgElement = ref;
                        return;
                    }}
                    url = {url}
                    staticAnnotations={scaledStaticAnnotations}
                    zoomScaledWidth={zoomScaledWidth}
                    zoomScaledHeight= {zoomScaledHeight}
                />
            </div>
        );
    }

    componentDidMount() {
        this.resizeListener = register('resize', this.updateWindowDimensions);
        this.rescaleStateBasedOnProps(this.props);
    }

    componentDidUpdate() {
        const {scrollPosition} = this.state;
        if (this._designerRef) {
            this._designerRef.scrollLeft = scrollPosition.left;
            this._designerRef.scrollTop = scrollPosition.top;
        }
    }

    componentWillReceiveProps(nextProps) {
        const {height, width} = nextProps.docMeta.Metadata;
        const {height: prevHeight, width: prevWidth} = this.props.docMeta.Metadata;

        if (this.props.url !== nextProps.url) {
            this.setState({imageLoading: true});
        }

        if (height !== prevHeight || width !== prevWidth) {
            this.rescaleStateBasedOnProps(nextProps, this.props.currentZoomScale, true);
            return;
        }

        if (nextProps.currentZoomScale !== this.props.currentZoomScale) {
            this.rescaleStateBasedOnProps(nextProps, this.props.currentZoomScale);

            return;
        }

        if (this.props.mode !== nextProps.mode) {
            this.setState({mode: nextProps.mode});
        }
    }

    componentWillUnmount() {
        unregister('resize', this.resizeListener);
    }

    scrollHandler() {
        this.setState({
            scrollPosition: {
                left: this._designerRef.scrollLeft,
                top: this._designerRef.scrollTop,
            },
        });
    }
    /**
     * handler for window resize event registered in componentDidMount hook
     */
    updateWindowDimensions() {
        this.rescaleStateBasedOnProps(this.props);
    }

    /**
     * Recalculate the state based on current zoom scale and current window size.
     * @param {object} param
     * @param {object} param.docMeta
     * @param {number} param.currentZoomScale
     * @param {boolean} param.isNavigatingNextPage

     */
    rescaleStateBasedOnProps({docMeta, currentZoomScale, scaleAmount}, prevCurrentZoomScale, isNavigatingNextPage) {
        /**
         * if the designer doesn't take up the entire height of the screen
         * you have to give it a pixel value, such as 85, that represents
         * the height of the screen that the designer doesn't utilize.
         */
        if (!this._designerRef)
            return;
        const {currentZoomScale : currentZoomState, height : heightState, width : widthState} = this.state;
        if (
            currentZoomScale === currentZoomState &&
            heightState === this._designerRef.getBoundingClientRect().height &&
            widthState === this._designerRef.getBoundingClientRect().width && !isNavigatingNextPage
        ) return;

        const height = this._designerRef.getBoundingClientRect().height;
        const aspectRatio = docMeta.Metadata.height / docMeta.Metadata.width;
        const docWidth = this._designerRef.getBoundingClientRect().width;
        const docHeight = Math.round(docWidth * aspectRatio);
        const layoutRatio = docMeta.Metadata.width / docWidth;

        const newzoomScaledWidth = this.getOffsetValue(docWidth, currentZoomScale);
        const newzoomScaledHeight = this.getOffsetValue(docHeight, currentZoomScale);
        const {axisGuide} = this.state;

        const axisGuidDimentionToZoomScaleOne = (prevCurrentZoomScale && prevCurrentZoomScale < currentZoomScale) ? this.getAxisGuidDimentionToZoomScaleOne(axisGuide, currentZoomScale - scaleAmount, scaleAmount)
            : this.getAxisGuidDimentionToZoomScaleOne(axisGuide, currentZoomScale + scaleAmount, scaleAmount);
        const axisX = axisGuidDimentionToZoomScaleOne.x + this.getOffsetValue(axisGuidDimentionToZoomScaleOne.x, currentZoomScale);
        const axisY = axisGuidDimentionToZoomScaleOne.y + this.getOffsetValue(axisGuidDimentionToZoomScaleOne.y, currentZoomScale);
        const top = parseInt(this._designerRef.scrollTop + (axisY - axisGuide.y));
        const left = parseInt(this._designerRef.scrollLeft + (axisX - axisGuide.x));

        this.setState({
            currentZoomScale,
            aspectRatio,
            layoutRatio,
            height,
            width: docWidth,
            docHeight,
            docWidth,
            zoomScaledWidth : newzoomScaledWidth,
            zoomScaledHeight: newzoomScaledHeight,
            axisGuide: {x: parseInt(axisX), y: parseInt(axisY)},
            scrollPosition: {left, top},
        });
        this.props.setLayoutRatio(layoutRatio);
    }

    /**
     * The complete newObject, or one or more components of the newObject.
     *
     * @typedef {Object} AnnotationObject
     * @property {...AnnotationVisualDefinition}
     * @property {number} x - x coordinate of the annotation
     * @property {number} y - y coordinate of the annotation
     * @property {number} layoutRatio - layout ratio of the document
     * @property {number} currentZoomScale - current zoom level
     * @property {number} refHeight - document height at the time annotation was drawn
     * @property {number} refWidth - document width at the time annotation was drawn
     * @property {number} windowXOffset - x offset with respect to current zoom scale and window size
     * @property {number} windowYOffset - y offset with respect to current zoom scale and window size
     * @property {number} windowWidthOffset - width offset with respect to current zoom scale and window size
     * @property {number} windowHeightOffset - height offset with respect to current zoom scale and window size
     */

    applyWindowOffset(object, index) { // eslint-disable-line no-unused-vars
        const {currentZoomScale, docMeta} = this.props;
        const {docWidth, docHeight} = this.state;

        const windowXOffset = this.getOffsetValue(object.x, currentZoomScale * docWidth / (object.refWidth || docMeta.Metadata.width));
        const windowYOffset = this.getOffsetValue(object.y, currentZoomScale * docHeight / (object.refHeight || docMeta.Metadata.height));
        const windowWidthOffset = this.getOffsetValue(object.width, currentZoomScale * docWidth / (object.refWidth || docMeta.Metadata.width));
        const windowHeightOffset = this.getOffsetValue(object.height, currentZoomScale * docHeight / (object.refHeight || docMeta.Metadata.height));

        return {
            ...object,
            windowXOffset,
            windowYOffset,
            windowWidthOffset,
            windowHeightOffset,
        };
    }

    /**
     *
     * @param {number} originalVal
     * @param {number} scale
     * @returns {number}
     */
    getOffsetValue(originalVal, scale) {
        return Math.round(originalVal * scale - originalVal);
    }

    /**
     *
     * @returns {{width: Number, height: Number, canvasWidth: Number, canvasHeight: Number, canvasOffsetX: Number, canvasOffsetY: Number}}
     */
    getCanvas() {
        const {width, height} = this.props;
        const {
            canvasWidth = width,
            canvasHeight = height,
        } = this.props;
        return {
            width, height, canvasWidth, canvasHeight,
            canvasOffsetX: (canvasWidth - width) / 2,
            canvasOffsetY: (canvasHeight - height) / 2,
        };
    }

    //rename this createDrawnAnnotation, rename all object variables 'drawnAnnotation'
    /**
     *
     * @param {Object} event - react synthetic mouse event
     */
    newObject(event) {
        if (this.props.mode !== ANNOTATION_MODES.DRAW) {
            return;
        }


        const {meta} = Rect;
        const mouse = this.getMouseCoords(event);
        const {
            activeField,
            activeFieldId,
            activeFieldOption,
            currentZoomScale,
            objects,
            onUpdate,
        } = this.props;

        this.props.startDragHandler(activeField);

        const {layoutRatio} = this.state;
        const bbox = this.svgElement.getBoundingClientRect();
        const object = {
            ...meta.initial,
            designerData: {
                activeFieldOption,
                activeFieldId,
            },
            type: Rect,
            x: mouse.x,
            y: mouse.y,
            layoutRatio,
            refHeight: bbox.height,
            refWidth: bbox.width,
            windowXOffset:0,
            windowYOffset:0,
            windowWidthOffset: 0,
            windowHeightOffset: 0,
            currentZoomScale,
        };

        onUpdate([...objects, object]);

        this.setState({
            currentObjectIndex: objects.length,
            selectedObjectIndex: objects.length,
            startPoint: this.getStartPointBundle(event, object),
            mode: meta.editor ? ANNOTATION_MODES.EDIT_OBJECT : ANNOTATION_MODES.SCALE,
            selectedTool: null,
        });
    }

    /**
     * Extend objects that were created outside of this components.
     * These objects have x, y, width, and height but are missing other meta that control their scale
     *
     * @param {{x: Number, y: Number, width: Number, height: Number}} staticAnnotation
     * @returns {Object}
     */
    createStaticAnnotation(staticAnnotation) {
        const {meta} = Rect;
        const {currentZoomScale, docMeta} = this.props;
        const {layoutRatio} = this.state;

        return {
            ...meta.initial,
            ...staticAnnotation,
            type: Rect,
            refHeight:  docMeta.Metadata.height,
            refWidth:  docMeta.Metadata.width,
            windowXOffset:0,
            windowYOffset:0,
            windowWidthOffset: 0,
            windowHeightOffset: 0,
            layoutRatio,
            currentZoomScale,
        };
    }

    /**
     * get current mouse coordinates
     *
     * @param {Number} clientX
     * @param {Number} clientY
     * @returns {{x: Number, y: Number}}
     */
    getMouseCoords({clientX, clientY}) {
        const coords = this.applyOffset({
            x: clientX,
            y: clientY,
        });

        return this.snapCoordinates(coords);
    }

    /**
     * applyOffset
     *
     * @param {{x: number, y: number} bundle
     * @returns {{x: number, y: number, offsetX: (Number|*), offsetY: (Number|*)}}
     */
    applyOffset(bundle) {
        const offset = this.getOffset();

        return {
            ...bundle,
            x: bundle.x - offset.x,
            y: bundle.y - offset.y,
            offsetX: offset.x,
            offsetY: offset.y,
        };
    }

    /**
     * snapCoordinates
     *
     * @param {number} x
     * @param {number} y
     * @returns {{x: Number, y: Number}}
     */
    snapCoordinates({x, y}) {
        const {snapToGrid} = this.props;

        return {
            x: x - (x % snapToGrid),
            y: y - (y % snapToGrid),
        };
    }

    /**
     * getOffset
     *
     * @returns {{x: Number, y: Number, width: Number, height: Number}}
     */
    getOffset() {
        const parent = this.svgElement.getBoundingClientRect();
        const {canvasWidth, canvasHeight} = this.getCanvas();
        return {
            x: parent.left,
            y: parent.top,
            width: canvasWidth,
            height: canvasHeight,
        };
    }

    /**
     * getStartPointBundle
     *
     * @param {Object} event
     * @param {Object} obj
     * @returns {{clientX: Number, clientY: Number, objectX: number, objectY: number, width: number, height: number, rotate: (*|number)}}
     */
    getStartPointBundle(event, obj) {
        const {currentObjectIndex} = this.state;
        const {objects} = this.props;
        const mouse = this.getMouseCoords(event);
        const object = obj || objects[currentObjectIndex];
        return {
            clientX: mouse.x,
            clientY: mouse.y,
            objectX: object.x,
            objectY: object.y,
            width: object.width,
            height: object.height,
            rotate: object.rotate,
        };
    }


    /**
     * Handler for onDrag event (used to draw boxes, etc.
     *
     * @param {Object} event - synthetic react event (onDrag)
     */
    onDrag(event) {
        const {currentObjectIndex, startPoint, mode} = this.state;
        const {objects, activeFieldOption} = this.props;
        const object = objects[currentObjectIndex];
        const mouse = this.getMouseCoords(event);

        const {scale, rotate, drag} = actions;

        const map = {
            [ANNOTATION_MODES.SCALE]: scale,
            [ANNOTATION_MODES.ROTATE]: rotate,
            [ANNOTATION_MODES.DRAG]: drag,
        };

        const action = map[mode];

        const parsedIndex = activeFieldOption !== FIELD_OPTIONS.LABEL ? currentObjectIndex : 0;

        if (action) {
            const newObject = action({
                object,
                startPoint,
                mouse,
                objectIndex: parsedIndex,
                objectRefs: this.objectRefs,
            });
            this.updateObject(parsedIndex, newObject);
            this.updateHandler(parsedIndex, newObject);
            this.hideAxisGuide();
            return;
        }
        this.updateAxisGuide(mouse);

        // if (currentObjectIndex !== null) {
        //     this.detectOverlappedObjects(event);
        // }
    }

    /**
     * A 'handler' is an visual UI area that sits on top of the annotation and provides ability to move or
     * change the size. This feature is not currently activated.
     *
     * @param index
     * @param object
     */
    updateHandler(index, object) {
        const target = this.objectRefs[index];
        const bbox = target.getBoundingClientRect();
        const {canvasOffsetX, canvasOffsetY} = this.getCanvas();

        let handler = {
            ...this.state.handler,
            width: (object.width) || bbox.width,
            height:(object.height) || bbox.height,
            top: object.y + canvasOffsetY,
            left: object.x + canvasOffsetX,
            rotate: object.rotate,
        };

        if (!object.width) {
            const offset = this.getOffset();
            handler = {
                ...handler,
                left: bbox.left - offset.x,
                top: bbox.top - offset.y,
            };
        }

        this.setState({
            handler,
        });
    }

    /**
     * Updates target object and provides array or objects as argument

     * @param {Number} objectIndex
     * @param {Object} changes
     * @param {*} updatePath
     */
    updateObject(objectIndex, changes, updatePath) {
        const {objects, onUpdate, annotationClass} = this.props;

        onUpdate(objects.map((object, index) => {
            if (index === objectIndex) {
                const newObject = {
                    ...object,
                    ...changes,
                    annotationClass,
                };
                return updatePath
                    ? this.updatePath(newObject)
                    : newObject;
            }

            return object;
        }));
    }

    /**
     * updates state that creates the crosshairs on hover
     *
     * {{x: Number, y: Number}} mouseCoordinates
     * @param mouseCoordinates
     */
    updateAxisGuide(mouseCoordinates) {
        this.setState({
            axisGuide: mouseCoordinates,
        });
    }

    /**
     * getAxisGuidDimentionToZoomScaleOne - this method returns the axis guide dimention with respect to scale 1 and remaining as offset
     * this is basically helpful to determine new zoom position of axis guide.
     * @param {*} mouseCoordinates current AxisGuide coordinate
     * @param {*} currentZoomScale current ZoomScale
     * @param {*} scaleAmount scaleAmount by which zoom get's change
     */
    getAxisGuidDimentionToZoomScaleOne(mouseCoordinates, currentZoomScale, scaleAmount) {
        const x = mouseCoordinates.x * (1 / scaleAmount) / (currentZoomScale / scaleAmount);
        const xOffset = mouseCoordinates.x - x;
        const y = mouseCoordinates.y * (1 / scaleAmount) / (currentZoomScale / scaleAmount);
        const yOffset = mouseCoordinates.y - y;
        return {
            x, y, xOffset, yOffset,
        };
    }
    /**
     * handler for drag that hides the crosshairs shown on screen
     *
     */
    hideAxisGuide() {
        this.updateAxisGuide({x: -1, y: -1});
    }

    /**
     *
     * @param {Object} object
     * @returns {Object}
     */
    updatePath(object) {
        const {path} = object;
        const diffX = object.x - object.moveX;
        const diffY = object.y - object.moveY;

        const newPath = path.map(({x1, y1, x2, y2, x, y}) => ({
            x1: diffX + x1,
            y1: diffY + y1,
            x2: diffX + x2,
            y2: diffY + y2,
            x: diffX + x,
            y: diffY + y,
        }));

        return {
            ...object,
            path: newPath,
            moveX: object.x,
            moveY: object.y,
        };
    }

    /**
     * handler for mouseUp event. Main purpose is to reset mode to FREE.
     * (If the component stayed in DRAW mode the box would never stop drawing)
     */
    stopDrag() {
        const {mode} = this.state;
        const modesArray = [ANNOTATION_MODES.DRAG, ANNOTATION_MODES.ROTATE, ANNOTATION_MODES.SCALE];

        if (modesArray.includes(mode)) {
            this.setState({
                mode: ANNOTATION_MODES.FREE,
            });
        }

        if (mode === ANNOTATION_MODES.FREE) {
            return;
        }

        const {activeField} = this.props;

        this.props.stopDragHandler(activeField);
    }
}

export default AnnotationEngine;
