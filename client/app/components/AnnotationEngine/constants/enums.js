/**
 * @enum {number}
 * @readonly
 */
export const ANNOTATION_MODES = {
    FREE: 0,
    DRAG: 1,
    SCALE: 2,
    ROTATE: 3,
    DRAW: 4,
    TYPE: 5,
    EDIT_OBJECT: 5,
};

