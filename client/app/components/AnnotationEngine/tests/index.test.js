import React from 'react';
import {shallow, mount} from 'enzyme';
import AnnotationEngine from '../index';
import Rect from '../../Rect';
import {StaticAnnotation1} from '../../../../fixtures/staticAnnotations';

describe('Component: AnnotationEngine', () => {
    const setLayoutRatioSpy = jest.fn();
    const updateSpy = jest.fn();
    const metaData = {
        AcceptRanges:'bytes',
        ContentLength:496077,
        ContentType:'image/png',
        ETag:'8fc4babe8bb0cf4798eea8315f835b75',
        LastModified:'2017-09-06T10:37:32.000Z',
        Metadata:
        {
            width: 1728,
            height: 2104,
        },
    };
    const object1 = {
        blendMode:'normal',
        fill:'rgba(255,101,48,0.30)',
        fillRule:'evenodd',
        height:35,
        layoutRatio:3.5661016949152544,
        radius:0,
        refHeight:2104,
        refWidth:1728,
        rotate:0,
        stroke:'#E36530',
        strokeDasharray:'14,6',
        strokeWidth:2,
        type: {},
        width:72,
        windowHeightOffset:0,
        windowWidthOffset:0,
        windowXOffset:0,
        windowYOffset:0,
        x:241,
        y:179,
        currentZoomScale:1,
    };
    const url = '';

    describe('method: rescaleStateBasedOnProps', () => {
        const wrapper = mount(
            <AnnotationEngine
                currentZoomScale={1}
                docMeta={metaData}
                objects={[]}
                onUpdate={updateSpy}
                setLayoutRatio={setLayoutRatioSpy}
                staticAnnotations={[]}
                url={url}
            />
        );
        wrapper.instance()._designerRef.getBoundingClientRect = jest.fn().mockReturnValue({bottom: 0, height: 768, left: 0, right: 0, top: 0, width: 1024});
        wrapper.update();

        it('it should calculate document dimention (to fit in window) and set it in state.', () => {
            wrapper.instance().rescaleStateBasedOnProps(wrapper.props());
            const currentZoomScale = 1;
            const height = wrapper.instance()._designerRef.getBoundingClientRect().height;
            const aspectRatio = metaData.Metadata.height / metaData.Metadata.width;
            const docWidth = wrapper.instance()._designerRef.getBoundingClientRect().width;
            const width = docWidth;
            const docHeight = Math.round(docWidth * aspectRatio);
            const layoutRatio = metaData.Metadata.width / width;
            const newzoomScaledWidth = wrapper.instance().getOffsetValue(docWidth, currentZoomScale);
            const newzoomScaledHeight = wrapper.instance().getOffsetValue(docHeight, currentZoomScale);

            expect(wrapper.state('height')).toEqual(height);
            expect(wrapper.state('width')).toEqual(width);
            expect(wrapper.state('docHeight')).toEqual(docHeight);
            expect(wrapper.state('docWidth')).toEqual(docWidth);
            expect(wrapper.state('aspectRatio')).toEqual(aspectRatio);
            expect(wrapper.state('layoutRatio')).toEqual(layoutRatio);
            expect(wrapper.state('zoomScaledWidth')).toEqual(newzoomScaledWidth);
            expect(wrapper.state('zoomScaledHeight')).toEqual(newzoomScaledHeight);
        });
        //window height 768 width 1024
        it('it should recalculate dimention and set it in state on window size change.', () => {
            window.resizeTo(900, 700);
            const currentZoomScale = 1;
            const height = wrapper.instance()._designerRef.getBoundingClientRect().height;
            const aspectRatio = metaData.Metadata.height / metaData.Metadata.width;
            const docWidth = wrapper.instance()._designerRef.getBoundingClientRect().width;
            const width = docWidth;
            const docHeight = Math.round(docWidth * aspectRatio);
            const layoutRatio = metaData.Metadata.width / width;
            const newzoomScaledWidth = wrapper.instance().getOffsetValue(docWidth, currentZoomScale);
            const newzoomScaledHeight = wrapper.instance().getOffsetValue(docHeight, currentZoomScale);

            expect(wrapper.state('height')).toEqual(height);
            expect(wrapper.state('width')).toEqual(width);
            expect(wrapper.state('docHeight')).toEqual(docHeight);
            expect(wrapper.state('docWidth')).toEqual(docWidth);
            expect(wrapper.state('aspectRatio')).toEqual(aspectRatio);
            expect(wrapper.state('layoutRatio')).toEqual(layoutRatio);
            expect(wrapper.state('zoomScaledWidth')).toEqual(newzoomScaledWidth);
            expect(wrapper.state('zoomScaledHeight')).toEqual(newzoomScaledHeight);
        });
    });
    describe('method: applyWindowOffset', () => {
        const wrapper = mount(
            <AnnotationEngine
                currentZoomScale = {1}
                docMeta = {metaData}
                objects={[]}
                onUpdate={updateSpy}
                setLayoutRatio={setLayoutRatioSpy}
                staticAnnotations={[]}
                url = {url}
            />
        );
        wrapper.instance()._designerRef.getBoundingClientRect = jest.fn().mockReturnValue({bottom: 0, height: 2104, left: 0, right: 0, top: 0, width: 1728});
        wrapper.update();
        wrapper.instance().rescaleStateBasedOnProps(wrapper.props());
        it('it should have window offset value 0 when window size is not changed since document loading.', () => {
            const scaledObject = [object1].map(wrapper.instance().applyWindowOffset);
            expect(scaledObject[0].windowXOffset).toEqual(0);
            expect(scaledObject[0].windowYOffset).toEqual(0);
            expect(scaledObject[0].windowWidthOffset).toEqual(0);
            expect(scaledObject[0].windowHeightOffset).toEqual(0);
        });

        it('it should rescale document when window size is changed.', () => {
            window.resizeTo(900, 700);
            const docWidth = wrapper.state('docWidth');
            const docHeight = wrapper.state('docHeight');
            const currentZoomScale = 1;
            const windowXOffset = wrapper.instance().getOffsetValue(object1.x, currentZoomScale * docWidth / object1.refWidth);
            const windowYOffset = wrapper.instance().getOffsetValue(object1.y, currentZoomScale * docHeight / object1.refHeight);
            const windowWidthOffset = wrapper.instance().getOffsetValue(object1.width, currentZoomScale * docWidth / object1.refWidth);
            const windowHeightOffset = wrapper.instance().getOffsetValue(object1.height, currentZoomScale * docHeight / object1.refHeight);
            const scaledObject = [object1].map(wrapper.instance().applyWindowOffset);
            expect(scaledObject[0].windowXOffset).toEqual(windowXOffset);
            expect(scaledObject[0].windowYOffset).toEqual(windowYOffset);
            expect(scaledObject[0].windowWidthOffset).toEqual(windowWidthOffset);
            expect(scaledObject[0].windowHeightOffset).toEqual(windowHeightOffset);
        });
        //window height 768 width 1024
    });
    describe('method: getOffsetValue', () => {
        const wrapper = shallow(
            <AnnotationEngine
                docMeta = {metaData}
                objects={[object1]}
                staticAnnotations={[]}
                onUpdate={updateSpy}
                url = {url}
            />
        );
        it('it should calculate offset with respect to the scale passed', () => {
            const originalVal = 100;
            const scale = 0.5;
            const expectedResult = Math.round(originalVal * scale - originalVal);
            const result = wrapper.instance().getOffsetValue(100, 0.5);
            expect(result).toEqual(expectedResult);
        });
    });
    describe('method: hideAxisGuide', () => {
        const wrapper = shallow(
            <AnnotationEngine
                docMeta = {metaData}
                objects={[object1]}
                staticAnnotations={[]}
                onUpdate={updateSpy}
                url = {url}
            />
        );
        it('it should hide axis guide', () => {
            const param = {x: -1, y: -1};
            wrapper.instance().hideAxisGuide();
            expect(wrapper.state('axisGuide')).toEqual(param);
        });
    });
    describe('method: stopDrag', () => {
        const wrapper = shallow(
            <AnnotationEngine
                docMeta = {metaData}
                objects={[object1]}
                staticAnnotations={[]}
                onUpdate={updateSpy}
                url = {url}
            />
        );
        it('it set mode to free', () => {
            wrapper.instance().stopDrag();
            expect(wrapper.state('mode')).toEqual(0);
        });
    });

    describe('when given docMeta and currentZoomScale', () => {
        // given
        const layoutRatio = 1;
        const currentZoomScale = 1;
        const staticAnnotations = [StaticAnnotation1];

        const wrapper = shallow(
            <AnnotationEngine
                currentZoomScale={currentZoomScale}
                docMeta={metaData}
                objects={[]} // needed during mounting
                staticAnnotations={staticAnnotations} // needed during mounting
                url={url}
            />
        );

        const expected = {
            ...Rect.meta.initial,
            ...StaticAnnotation1,
            type: Rect,
            refHeight: metaData.Metadata.height,
            refWidth: metaData.Metadata.width,
            windowXOffset: 0,
            windowYOffset: 0,
            windowWidthOffset: 0,
            windowHeightOffset: 0,
            currentZoomScale,
            layoutRatio,
        };

        describe('method: createStaticAnnotation', () => {
            it('should extend the object with metadata dictating the style and scale of the object', () => {
                // when
                // then
                expect(wrapper.instance().createStaticAnnotation(StaticAnnotation1)).toEqual(expected);
            });
        });
    });
});
