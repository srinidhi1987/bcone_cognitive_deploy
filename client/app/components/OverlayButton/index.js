/**
*
* OverlayButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const OverlayButton = ({label, onClick}) => (
    <button
        className="aa-overlay-btn"
        onClick={onClick}
    >
        {label}
    </button>
);

OverlayButton.displayName = 'OverlayButton';

OverlayButton.propTypes = {
    /**
     * Button label
     */
    label: PropTypes.string,
};

OverlayButton.defaultProps = {
    label: 'Got it!',
};

export default OverlayButton;
