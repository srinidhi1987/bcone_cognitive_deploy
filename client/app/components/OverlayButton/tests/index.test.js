import React from 'react';
import {shallow} from 'enzyme';

import OverlayButton from '../index';

describe('<OverlayButton />', () => {
    // given
    const clickSpy = jest.fn();
    const wrapper = shallow(
        <OverlayButton
            label="Testing"
            onClick={clickSpy}
        />
    );

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render label in button', () => {
        // given
        // when
        const result = wrapper.find('.aa-overlay-btn');
        // then
        expect(result.props().children).toBe('Testing');
    });

    it('should have a onclick when button is clicked', () => {
        // given
        // when
        wrapper.simulate('click');
        // then
        expect(clickSpy).toHaveBeenCalled();
    });
});
