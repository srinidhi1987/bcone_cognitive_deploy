/**
*
* ActionConfirmationModel
*
*/
import React, {Component} from 'react';
import {autobind} from 'core-decorators';
import PropTypes from 'prop-types';
import {
    Confirm,
    Announcement,
    RadioGroup,
    RadioInput,
} from 'common-frontend-components';
import {
    VALIDATION_ERROR_MESSAGE,
    NO_FILES_TO_IMPORT_ERROR_MESSAGE,
} from './constants';

export const themeType = {
    default: 'default',
    info: 'info',
    success: 'success',
    warn: 'warn',
    error: 'error',
};
import './styles.scss';

class ImportArchiveSelectionComponent extends Component {
    static displayName = 'ImportArchiveSelectionComponent';

    PropTypes = {
        /**
         * title for confirmation box
         */
        title: PropTypes.object.isRequired,
        /**
         * paragraphs to be displayed in confirmation box body
         */
        paragraphs: PropTypes.array.isRequired,
        /**
         * Callback for deletion action
         */
        onConfirm: PropTypes.func.isRequired,
        /**
         * Callback to hide modal
         */
        onHide: PropTypes.func.isRequired,
        /**
         * Show deletion modal
         */
        show: PropTypes.bool.isRequired,
        /**
         * shows title on cancel button
         */
        cancelTitle: PropTypes.string.isRequired,
        /**
         * shows title on confim button.
         */
        confirmTitle: PropTypes.string.isRequired,
        /**
         * show modal box theme for different prupose.
         */
        theme: PropTypes.oneOf(['info', 'default', 'success', 'error', 'warn']),
        /**
         * selection option
         */
        selectionOptions: PropTypes.array.isRequired,
    };

    static defaultProps = {
        show: false,
        paragraphs: [],
    };

    componentWillMount() {
        const {selectionOptions} = this.props;

        if (selectionOptions.length) {
            this.setState({selectedVal: selectionOptions[0].archiveName});
        }
    }

    state = {
        selectedVal: '',
        error: null,
    };

    /**
     * Render text paragraphs
     */
    @autobind
    renderParagraphs() {
        const {paragraphs} = this.props;

        if (paragraphs && paragraphs.length) {
            return [
                <div key="1" style={{paddingTop: 15}}>
                    {paragraphs.map((paragraph, i) => (
                        <p key={i}>{paragraph}</p>
                    ))}
                </div>,
                <br key="2" />,
            ];
        }

        // render node for spacing
        return (
            <div style={{paddingTop: 15}}>{null}</div>
        );
    }

    render() {
        const {
            show,
            title,
            cancelTitle,
            confirmTitle,
            theme,
            selectionOptions,
        } = this.props;
        const {selectedVal, error} = this.state;

        return (
            <Confirm
                title={title}
                show={show}
                style={{padding: 15}}
                onHide={this._handleOnCancel}
                onSubmit={this._confirm}
                theme={theme}
                labelCancel={cancelTitle}
                labelAccept={confirmTitle}
            >
                {this.renderParagraphs()}

                {error && (
                    <Announcement
                        theme="error"
                        title={error}
                    />
                )}

                <div className="aa-grid-column option-selection-container">
                    <RadioGroup value={selectedVal} onChange={ this.onChange }>
                        {selectionOptions && selectionOptions.map((option, i) => (
                            <RadioInput key={i} name={option.archiveName}> {option.archiveName} </RadioInput>
                        ))}
                    </RadioGroup>
                </div>
            </Confirm>
        );
    }

    /**
     * Handle hiding the modal
     * @returns void
     */
     @autobind
    _handleOnCancel() {
        const {onCancel} = this.props;
        onCancel();
    }

    /**
     * Confirm project deletion
     * @returns void
     */
     @autobind
    _confirm() {
        const {onConfirm, selectionOptions} = this.props;
        const {selectedVal} = this.state;
        // if the selection options are undefined then it should set validation error to NO_FILES_TO_IMPORT_SELECT_MESSGE
        // or set error to validation message if user have not selected any file for import.
        const error = (selectionOptions && selectionOptions.length !== 0) ? (!selectedVal && VALIDATION_ERROR_MESSAGE) : NO_FILES_TO_IMPORT_ERROR_MESSAGE;
        this.setState({error});
        selectedVal && onConfirm(selectedVal);
    }

    @autobind
    onChange(selectedVal) {
        this.setState({selectedVal, error: !selectedVal && VALIDATION_ERROR_MESSAGE});
    }
}

export default ImportArchiveSelectionComponent;
