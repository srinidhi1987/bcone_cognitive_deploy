import React from 'react';
import {mount} from 'enzyme';
import {
    CONFIRMATION_BOX_CANCEL_TEXT,
    IMPORT_CONFIRMATION_BOX_CONFIRM_TEXT,
} from '../../../containers/MigrationUtilityContainer/constants';
import {
    NO_FILES_TO_IMPORT_ERROR_MESSAGE,
} from '../constants';
import ImportArchiveSelectionComponent from '../index';
import {
    Confirm,
} from 'common-frontend-components';
import {themeType} from '../index';
import {archiveDetails} from '../../../../fixtures/task';

describe('Component: ImportArchiveSelectionComponent', () => {
    //given
    const confirmSpy = jest.fn();
    const cancelSpy = jest.fn();
    const wrapper = mount(
        <ImportArchiveSelectionComponent
            title="Import an archive"
            paragraphs={[
                'Please select the archive file to be imported.',
            ]}
            cancelTitle = {CONFIRMATION_BOX_CANCEL_TEXT}
            confirmTitle = {IMPORT_CONFIRMATION_BOX_CONFIRM_TEXT}
            show={true}
            theme={themeType.default}
            onCancel={cancelSpy}
            onConfirm={confirmSpy}
            selectionOptions={archiveDetails}
        />
    );

    it('should receive all required properties', () => {
        //when
        const {title, paragraphs, show, onCancel, onConfirm, cancelTitle, confirmTitle, theme, selectionOptions} = wrapper.props();
        //then
        expect(title).toBeDefined();
        expect(paragraphs).toBeDefined();
        expect(show).toEqual(true);
        expect(onCancel).toBeDefined();
        expect(onConfirm).toBeDefined();
        expect(cancelTitle).toBeDefined();
        expect(confirmTitle).toBeDefined();
        expect(theme).toBeDefined();
        expect(selectionOptions).toBeDefined();
    });

    describe('check visibility', () => {
        it('should show modal when show props is set to true', () => {
            //when
            const modal = wrapper.find(Confirm);
            //then
            expect(modal.props().show).toEqual(true);
        });

        it('should hide modal when show props is set to false', () => {
            //when
            wrapper.setProps({show:false});
            //then
            const modal = wrapper.find(Confirm);
            expect(modal.props().show).toEqual(false);
        });
    });

    describe('method: _confirm', () => {
        it('should set error in state if user has not selected input and he clicks confirm button', () => {
            const {onConfirm} = wrapper.props();

            //when
            wrapper.instance()._confirm();
            //then
            expect(onConfirm).toHaveBeenCalledWith(archiveDetails[0].archiveName);
        });

        it('should call onConfirm props if user has selected input when user clicks confirm button', () => {
            //given
            const {onConfirm} = wrapper.props();
            wrapper.instance().onChange(archiveDetails[0].archiveName);
            //when
            wrapper.instance()._confirm();
            //then
            expect(onConfirm).toHaveBeenCalledWith(archiveDetails[0].archiveName);
        });

        it('should should show different error message if there is no files to select for import', () => {
            //when
            wrapper.setProps({selectionOptions:[]});
            wrapper.instance()._confirm();
            //then
            const {error} = wrapper.state();
            expect(error).toEqual(NO_FILES_TO_IMPORT_ERROR_MESSAGE);
        });
    });

    describe('method: _handleOnCancel', () => {
        it('should call onHide props when user clicks cancel button', () => {
            //given
            const {onCancel} = wrapper.props();
            //when
            wrapper.instance()._handleOnCancel();
            //then
            expect(onCancel).toHaveBeenCalled();
        });
    });
});
