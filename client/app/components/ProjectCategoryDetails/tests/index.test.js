import ProjectCategoryDetails from '../index';
import {shallow} from 'enzyme';
import React from 'react';
import {
    DataTable,
} from 'common-frontend-components';
import ProgressBar from '../../ProgressBar';
import ProjectClassifyProgressLoader from '../../ProjectClassifyProgressLoader';
import {Project1} from '../../../../fixtures/projects';
import {Category3, Category4} from '../../../../fixtures/categories';

describe('<ProjectCategoryDetails />', () => {
    //given
    const categoriesList1 = [Category3, Category4];
    const categoryPaginationObject = {
        offset: 50,
        limit: 100,
    };
    const shallowwrapper = shallow(
        <ProjectCategoryDetails
            categories={categoriesList1}
            categoryPagination={categoryPaginationObject}
            classifyProgress ={50}
            designerConfig={{
                appConfig:'appConfig',
                authToken:'authToken',
            }}
            loadingCategories={false}
            project={Project1}
            onPageChange={jest.fn().mockImplementation(() => {})}
            categoriesSort={[
                {id: 'priority', orderBy: 'desc'},
            ]}
            sortCategories={jest.fn().mockImplementation(() => {})}
        />
    );
    it('should display training progress', () => {
        //given
        const result = shallowwrapper.find(ProgressBar);
        //then
        expect(result.props().current).toEqual(Project1.currentTrainedPercentage);
    });
    it('should display classification progress', () => {
        //given
        const result = shallowwrapper.find(ProjectClassifyProgressLoader);
        //then
        expect(result.props().progress).toEqual(50);
    });
    it('should display category details', () => {
        //given
        const result = shallowwrapper.find(DataTable);
        const {data, fields} = result.props();
        const {stagingFileDetails, productionFileDetails, categoryDetail} = categoriesList1[0];
        //then
        expect(data.length).toEqual(categoriesList1.length);
        expect(data[0].categoryName).toEqual('Group_629');
        expect(data[0].stagingFilesCount).toEqual(stagingFileDetails.totalCount);
        expect(data[0].stagingFilesStp).toEqual(stagingFileDetails.totalSTPCount);
        expect(data[0].stagingFilesUnproccessed).toEqual(stagingFileDetails.unprocessedCount);
        expect(data[0].productionFilesCount).toEqual(productionFileDetails.totalCount);
        expect(data[0].productionFilesStp).toEqual(productionFileDetails.totalSTPCount);
        expect(data[0].productionFilesUnproccessed).toEqual(productionFileDetails.unprocessedCount);
        expect(data[0].index).toEqual(categoryDetail ? categoryDetail.index : 0);
        expect(data[0].category).toEqual(categoriesList1[0]);
        expect(fields.length).toEqual(10);
    });
});
