/**
*
* ProjectCategoryDetails
*
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FormattedNumber} from 'react-intl';
import {DataField, DataType} from 'common-frontend-utils';
import Pagination from '../../components/Pagination';

import {
    DataTable,
    StaticTable,
} from 'common-frontend-components';
import TooltipAlternative from '../TooltipAlternative';
import ProgressBar from '../ProgressBar';
import ProjectClassifyProgressLoader from '../ProjectClassifyProgressLoader';
import LinkToDesignerEditOrCreateBotConfiguration from '../LinkToDesignerEditOrCreateBotConfiguration';
import InfoTooltip from '../InfoTooltip';
import {TRAINING_PERCENTAGE} from '../../constants/productTerms';

import './styles.scss';

export class ProjectCategoryDetails extends Component {
    static displayName = 'ProjectCategoryDetails';

    static propTypes = {
        project: PropTypes.shape({
            id: PropTypes.string,
            environment: PropTypes.string,
            currentTrainedPercentage: PropTypes.number,
            numberOfCategories: PropTypes.number,
        }),
        /**
         * AnnotationEngine configuration
         */
        appConfig: PropTypes.object.isRequired,
        /**
         * Show classifying progress
         */
        classifyProgress: PropTypes.number,
        /**
         * Set sorting for categories
         */
        categoriesSort: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            orderBy: PropTypes.string,
        })).isRequired,
        categories: PropTypes.arrayOf(PropTypes.shape({
            visionBot: PropTypes.object,
            fileCount: PropTypes.number,
            name: PropTypes.string,
            id: PropTypes.string,
            categoryDetail: PropTypes.shape({
                priority: PropTypes.number,
                noOfDocuments: PropTypes.number,
                allFieldDetail: PropTypes.arrayOf(PropTypes.shape({
                    fieldId: PropTypes.oneOfType([
                        PropTypes.number,
                        PropTypes.string,
                    ]),
                    foundInTotalDocs: PropTypes.number,
                    name: PropTypes.string, // used to show in hover for field names
                })),
            }),
            productionFileDetails: PropTypes.shape({
                totalCount: PropTypes.number,
                totalSTPCount: PropTypes.number,
                unprocessedCount: PropTypes.number,
            }),
            stagingFileDetails: PropTypes.shape({
                totalCount: PropTypes.number,
                totalSTPCount: PropTypes.number,
                unprocessedCount: PropTypes.number,
            }),
        })),
        categoryPagination: PropTypes.shape({
            offset: PropTypes.number,
            limit: PropTypes.number,
        }),
        /**
         * Callback to sort categories
         */
        sortCategories: PropTypes.func.isRequired,
    };

    static defaultProps = {
        classifyProgress: null,
        priorityFields: [],
    };

    render() {
        const {categories, categoriesSort, loadingCategories, project, classifyProgress, sortCategories, changePage, categoryPagination, appConfig} = this.props;
        const folderFields = [
            new DataField({id: 'categoryName', label: 'Name', sortable: true, type: DataType.STRING}),
            new DataField({id: 'inUseBy', label: 'In use by', sortable: false, type: DataType.STRING}),
            new DataField({id: 'stagingFilesCount', label: '# of Training Files', sortable: true, type: DataType.NUMBER, render: (count = 0) => ( // eslint-disable-line react/display-name
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'stagingFilesStp', label: 'Training Success', sortable: true, type: DataType.NUMBER, render: (count = 0) => ( // eslint-disable-line react/display-name
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'stagingFilesUnproccessed', label: 'Training Unprocessed', sortable: true, type: DataType.NUMBER, render: (count = 0) => ( // eslint-disable-line react/display-name
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'productionFilesCount', label: '# of Production Files', sortable: true, type: DataType.NUMBER, render: (count = 0) => ( // eslint-disable-line react/display-name
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'productionFilesStp', label: 'Production Success', sortable: true, type: DataType.NUMBER, render: (count = 0) => ( // eslint-disable-line react/display-name
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'productionFilesUnproccessed', label: 'Production Unprocessed', sortable: true, type: DataType.NUMBER, render: (count = 0) => ( // eslint-disable-line react/display-name
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'index', label: 'Priority', type: DataType.NUMBER, sortable: true, render: (priority, data) => { // eslint-disable-line react/display-name
                if (!data.category.categoryDetail) return null;

                const popupChildren = (
                    <div className="aa-project-details-priority-hover-table">
                        <StaticTable>
                            <StaticTable.Header>
                                {data.category.categoryDetail.allFieldDetail.map((field) => (
                                    <StaticTable.Column
                                        key={`priority-header${field.fieldId}`}
                                    >
                                        {field.name}
                                    </StaticTable.Column>
                                ))}
                            </StaticTable.Header>

                            <StaticTable.Row>
                                {data.category.categoryDetail.allFieldDetail.map((field) => (
                                      <StaticTable.Column
                                          key={`priority-column${field.fieldId}`}
                                      >
                                          {Math.floor((field.foundInTotalDocs / data.category.categoryDetail.noOfDocuments) * 100)}%
                                      </StaticTable.Column>
                                ))}
                            </StaticTable.Row>
                        </StaticTable>
                    </div>
                );

                const dataChildren = (
                    <span>
                        {data.category.categoryDetail.priority}
                    </span>
                );

                return (
                    <TooltipAlternative
                        customPopupCls="aa-project-details-priority-hover-table"
                        dataChildren={dataChildren}
                        popupChildren={popupChildren} />
                );

            }}),
            new DataField({id: 'iqBot', label: 'IQ Bot', type: DataType.STRING, sortable: false, render: ({project, category}) => { // eslint-disable-line react/display-name
                const {id, name, visionBot = {}} = category;
                const isInProduction = visionBot.currentState && visionBot.currentState !== 'training';
                const isDisabled = Boolean(visionBot.lockedUserId || isInProduction);

                return (
                    <LinkToDesignerEditOrCreateBotConfiguration
                        projectId={project.id}
                        appConfig={appConfig}
                        categoryId={id}
                        categoryName={name}
                        disabled={isDisabled}
                        visionBot={visionBot}
                    />
                );
            }}),
        ];

        return (
            <div className="aa-project-details-container">
                {(() => {
                    if (project) {
                        return (
                            <div className="aa-project-details-container-inner">
                                <div className="aa-project-details-container-header">
                                    Groups

                                    <div className="aa-project-details-container-header--traincount">
                                        {TRAINING_PERCENTAGE.label} <InfoTooltip term={TRAINING_PERCENTAGE}/>

                                        <ProgressBar
                                            current={Math.floor(project.currentTrainedPercentage)}
                                        />
                                    </div>

                                    {(() => {
                                        if (typeof classifyProgress === 'number') {
                                            return (
                                                <div className="aa-project-details-container-header--projectclassifying">
                                                    <ProjectClassifyProgressLoader
                                                        progress={classifyProgress}
                                                    />
                                                </div>
                                            );
                                        }
                                    })()}
                                </div>

                                <div className="aa-project-details-container-body">
                                    {(Array.isArray(categories) && categories.length) ?
                                        <div className="inner-body">
                                            <div className="aa-project-details-folders-count">
                                                Groups (
                                                    {/*<FormattedNumber value={(categoryPagination.offset + 1} />*/}
                                                <span>{categoryPagination.offset + 1} - {(categoryPagination.offset + categoryPagination.limit) < project.numberOfCategories ? (categoryPagination.offset) + categoryPagination.limit : project.numberOfCategories} </span>
                                                <span> of </span>
                                                    <FormattedNumber value={project.numberOfCategories} />
                                                )
                                            </div>

                                            <DataTable
                                                data={categories.map((cat) => ({
                                                    id: cat.id,
                                                    categoryName: cat.name,
                                                    stagingFilesCount: cat.stagingFileDetails.totalCount,
                                                    stagingFilesStp: cat.stagingFileDetails.totalSTPCount,
                                                    stagingFilesUnproccessed: cat.stagingFileDetails.unprocessedCount,
                                                    productionFilesCount: cat.productionFileDetails.totalCount,
                                                    productionFilesStp: cat.productionFileDetails.totalSTPCount,
                                                    productionFilesUnproccessed: cat.productionFileDetails.unprocessedCount,
                                                    index: cat.categoryDetail ? cat.categoryDetail.index : 0, // table
                                                    // sorts by index, more fine-grain priority
                                                    category: cat,
                                                    iqBot: {
                                                        category: cat,
                                                        project,
                                                    },
                                                    inUseBy: cat.visionBot && cat.visionBot.lockedUserId ? cat.visionBot.lockedUserId : '--',
                                                }))}
                                                dataId="id"
                                                loading={loadingCategories}
                                                fields={folderFields}
                                                sort={categoriesSort}
                                                onSort={sortCategories}
                                            />
                                        </div>
                                        :
                                        (!loadingCategories ? <div>No current groups in this instance</div> : null)
                                    }
                                </div>
                                <Pagination
                                    pageSize={categoryPagination.limit}
                                    loading={loadingCategories}
                                    offset={categoryPagination.offset}
                                    totalCount={project.numberOfCategories}
                                    desiredLength={5}
                                    onPageChange={changePage}
                                />
                            </div>
                        );
                    }
                })()}
            </div>
        );
    }
}

export default ProjectCategoryDetails;
