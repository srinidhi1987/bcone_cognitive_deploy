import React from 'react';
import {FIELD_TYPES_ENUM} from '../../constants/enums';
import {Icon} from 'common-frontend-components';

import {getFieldMenuItemIcon} from '../../utils/designer';

import './styles.scss';
import classnames from 'classnames';
import routeHelper from '../../utils/routeHelper';


function renderFieldTypeIcon(type) { //eslint-disable-line react/display-name
    if (type === 3) {
        return <img src={routeHelper(`/images/field-type-${FIELD_TYPES_ENUM[3]}.svg`)} />;
    }

    const fieldIcon = getFieldMenuItemIcon(type);

    return <Icon fa={fieldIcon}/>;
}

const DesignerPageWizardMenuItem = ({item, isComplete, selectFieldHandler, areaType}) => {
    const checkmarkIconUrl = routeHelper(`/images/status-${isComplete ? 'success' : 'todo'}.svg`);
    const formattedName = item.Name.replace(/_/g, ' ');

    return (
        <div
            onClick={() => selectFieldHandler(areaType, item.Id)}
            className={classnames(
                'designer-page-wizard-menu-item',
                'aa-grid-row',
                'vertical-center',
                'space-between', {
                    'is-active': item.isActive,
                }
            )}
        >
            <div className="aa-grid-row vertical-center">
                <div style={{width: '20px', paddingRight: '5px', textAlign: 'center'}}>
                    {renderFieldTypeIcon(item.ValueType)}
                </div>
                <div>
                    {formattedName}
                </div>
            </div>
            <div className="designer-page-wizard-menu-item--check-mark aa-grid-row">
                <img src={checkmarkIconUrl} />
            </div>
        </div>
    );
};

DesignerPageWizardMenuItem.displayName = 'DesignerPageWizardMenuItem';

export default DesignerPageWizardMenuItem;
