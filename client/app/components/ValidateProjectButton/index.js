/**
*
* Validate Project Button
*
*/
import React, {PureComponent} from 'react';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import {Tooltip} from 'common-frontend-components';
import classnames from 'classnames';
import routeHelper from '../../utils/routeHelper';

import {constructValidatorProtocolHandler} from '../../utils';

class ValidateProjectButton extends PureComponent {
    static displayName = 'ValidateProjectButton';

    static propTypes = {
        appConfig: PropTypes.object,
        authToken: PropTypes.string,
        environment: PropTypes.string,
        id: PropTypes.string,
    };

    _renderValidatorIcon(withLabel) {
        return (
            withLabel ?
            <div>
                <img className="launch-validator-icon" src={routeHelper('/images/validator-icon.png')} />
                <span>
                    Validate
                </span>
            </div>
            :
            <Tooltip
                icon={(
                    <img className="launch-validator-icon" src={routeHelper('/images/validator-icon.png')} />
                )}
            >
                Launch validator
            </Tooltip>
        );
    }
    render() {
        const {
            appConfig: {encrypted, validator},
            authToken,
            id,
            withLabel,
            enabled,
        } = this.props;
        const className = classnames('aa-link', {disabled: !enabled});

        return (
            validator ?
            <li className="aa-grid-row vertical-center">
                <Link
                    to={routeHelper(`/learning-instances/${id}/validator`)}
                    className={className}
                >
                    {this._renderValidatorIcon(withLabel)}
                </Link>
            </li>
            :
            <li className="aa-grid-row vertical-center">
                <a
                    onClick={(e) => {
                        if (navigator.msLaunchUri) {
                            navigator.msLaunchUri(constructValidatorProtocolHandler(encrypted, authToken, id));
                            e.preventDefault();
                        }
                    }}
                    className={className}
                    href={constructValidatorProtocolHandler(encrypted, authToken, id)}
                >
                    {this._renderValidatorIcon(withLabel)}
                </a>
            </li>
        );
    }
}

export default ValidateProjectButton;
