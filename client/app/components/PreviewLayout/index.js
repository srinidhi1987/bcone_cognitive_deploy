/* eslint-disable */
import React, {PureComponent} from 'react';
import {autobind} from 'core-decorators';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import PageLoadingOverlay from '../PageLoadingOverlay/index';
import VisionBotControlBar from '../DesignerPageControlBar/index';
import {
    CollapsiblePane,
    DataTable,
    Icon,
} from 'common-frontend-components';
import {DataField, DataType} from 'common-frontend-utils';
import {
    FIELD_OPTIONS,
    FIELD_TYPES_ENUM,
    DESIGNER_AREA_TYPES,
    VALIDATION_TYPES,
} from '../../constants/enums';
const {
    LABEL,
    DISPLAY_VALUE,
    IS_REQUIRED,
    VALIDATE,
} = FIELD_OPTIONS;
import './styles.scss';
import {VALIDATE_TABLE_PROPERTIES} from '../../containers/DesignerContainer/utilities';
import {getFieldMenuItemIcon} from '../../utils/designer';
import ValidatorField from '../ValidatorField';
import {NavigationDirections} from '../../constants/enums';

const {HEADERS, COLUMNS} = VALIDATE_TABLE_PROPERTIES;

class FieldsValidationLayout extends PureComponent {
    static displayName = 'FieldsValidationLayout';

    constructor(props) {
        super(props);
        this.state = {
            formCollapsibleState: {
                0: true,
            },
            tableCollapsibleState: {
                0: true,
            },
        };
    }

    getFormCollapsibleState() {
        return this.state.formCollapsibleState[0];
    }

    /**
     * return function to toggles respective tab
     * @param {number} key
     * @returns {Function}
     */
    @autobind toggleFormCollapsibleState() {
        const active = {
            0: !this.getFormCollapsibleState(),
        };
        this.setState({formCollapsibleState:active});
    }

    getTableCollapsibleState(key) {
        return this.state.tableCollapsibleState[key];
    }

    toggleTableCollapsibleState(key) {
        const active = {
            ...this.state.tableCollapsibleState,
            [key] : !this.getTableCollapsibleState(key),
        };
        this.setState({tableCollapsibleState:active});
    }
    /**
     * prepare fields for datatables
     */
    getTablesDataFields({tables, selectFieldHandler, setTableFieldValue}) {
        const tablesDataFields = tables.map((table, index) => {
            return table.columns.map(({fieldId, name, displayName}) => {
                return new DataField({id: name, label: displayName , type: DataType.STRING, render: (value, data) => {
                    return (
                        <ValidatorField
                            field={data[name]}
                            selectFieldHandler={selectFieldHandler}
                            //setFieldValueHandler={setTableFieldValue}
                            isFormField={false}
                            tableIndex={data.tableIndex}
                            rowIndex={data.id}
                            fieldIndex={data[name].fieldIndex}
                            readOnly={true}
                        />
                    );
                }});
            });
        });
        return tablesDataFields;
    }

    @autobind renderTables() {
        const {
            tables,
        } = this.props;
        const tablesDataFields = this.getTablesDataFields(this.props);

        return tables.map((table, tableIndex) => (
            table.showTable ?
            <CollapsiblePane
                label={`Table ${tableIndex + 1}`}
                active={this.getTableCollapsibleState(tableIndex)}
                onActive={this.toggleTableCollapsibleState.bind(this, tableIndex)}
            >
                <CollapsiblePane.Content>
                    <div className="field-box-container field-box-container--table">
                        <DataTable
                            data={table.rows.map((row, rowIndex) => {
                                return row.reduce((acc, field, fieldIndex) => {
                                    return {...acc, [field.name]: {...field, fieldIndex}};
                                }, {id: rowIndex, tableIndex: tableIndex});
                            })}
                            dataId="id"
                            fields={tablesDataFields[tableIndex]}
                        />
                    </div>
                </CollapsiblePane.Content>
            </CollapsiblePane>
            : null
        ))
    }

    render() {
        const {
            botCanNavigate,
            editLoading,
            fields,
            isWaitingForPreviewData,
            selectFieldHandler,
            activeField,
            navigateNextHandler,
            navigatePreviousHandler,
            previewLoading,
        } = this.props;
        return (
            <div className="preview-container">
                {
                    editLoading ?
                    <PageLoadingOverlay
                        show={editLoading}
                        label="Loading Edit Document..."
                        fullscreen={false}
                    /> : null
                }
                {
                    fields.length ?
                    <CollapsiblePane
                        label="Fields"
                        active={this.getFormCollapsibleState()}
                        onActive={this.toggleFormCollapsibleState}
                    >
                        <CollapsiblePane.Content>
                        <div className="field-box-container">
                        {
                            fields.map((field, index) => {
                                return (
                                    <ValidatorField
                                        field={field}
                                        selectFieldHandler={selectFieldHandler}
                                        //setFieldValueHandler={setFieldValueHandler}
                                        isFormField={true}
                                        fieldIndex={index}
                                        readOnly={true}
                                    />
                                );
                            })
                        }
                        </div>
                        </CollapsiblePane.Content>
                    </CollapsiblePane> : null
                }
                {this.renderTables()}
                <VisionBotControlBar>
                     <div className="preview-control-bar aa-grid-row space-between">
                     <div className="preview-control-bar-button-group aa-grid-row horizontal-right vertical-center">
                         </div>
                         <div className="preview-control-bar-button-group aa-grid-row horizontal-right vertical-center">
                             <button
                                className="previous-next-button"
                                disabled={!botCanNavigate[NavigationDirections.PREVIOUS]}
                                onClick={navigatePreviousHandler}
                             >
                                 &#60; Previous
                             </button>
                             <button
                                className="previous-next-button"
                                disabled={!botCanNavigate[NavigationDirections.NEXT]}
                                onClick={navigateNextHandler}
                             >
                                 Next &#62;
                             </button>
                         </div>
                     </div>
                 </VisionBotControlBar>
            </div>
        );
    }
}

export default FieldsValidationLayout;
