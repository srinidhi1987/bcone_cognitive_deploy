/* eslint-disable */

/**
*
* ValidatorPageLayout
*
*/

import './styles.scss';

import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import classnames from 'classnames';
import FontAwesome from 'react-fontawesome';
import PageLoadingOverlay from '../PageLoadingOverlay';
import PageDescription from '../PageDescription';
import {CommandButton,
    Breadcrumbs,
    PageTitle,
} from 'common-frontend-components';
import AnnotationEngine from '../AnnotationEngine';
import DesignerField from '../DesignerField';
import pluralize from 'pluralize';
import ActionConfirmationModal, {themeType} from '../ActionConfirmationModal';
import MarkAsInvalidModal from '../MarkAsInvalidModal';
import {isSelectedDrawTool} from '../../utils/designer';
import routeHelper from '../../utils/routeHelper';

import {
    FIELD_OPTIONS,
    FIELD_TYPES_ENUM,
    DESIGNER_AREA_TYPES,
    VALIDATION_TYPES,
} from '../../constants/enums';
import {ANNOTATION_MODES} from '../AnnotationEngine/constants/enums';
import FieldsValidationLayout from '../FieldsValidationLayout/index';
import {
    SAVE_CONFIRMATION_BOX_TITLE,
} from './constants';
const {
    LABEL,
    DISPLAY_VALUE,
    IS_REQUIRED,
    VALIDATE,
} = FIELD_OPTIONS;

const ValidatorPageLayout = (
    {
        activeField,
        activeFieldId,
        activeFieldOption,
        allValidated,
        annotationClasses,
        areaClickHandler,
        breadCrumbsUrl,
        cancelSaveConfirmationHandler,
        closeMarkAsInvalidHandler,
        confirmMarkAsInvalidHandler,
        classificationId,
        currentZoomScale,
        docMeta,
        fields,
        fileName,
        finishHandler,
        imageLoaded,
        imageLoadHandler,
        loadingScreen,
        mode,
        nextDocumentHandler,
        objects,
        onUpdate,
        params,
        projectName,
        remainingDocumentReviewCount,
        saveConfirmHandler,
        saveClickHandler,
        selectFieldHandler,
        selectedObjects,
        setFieldValueHandler,
        setTableFieldValue,
        setLayoutRatio,
        shouldShowMarkAsInvalidModal,
        showMarkAsInvalidHandler,
        showSaveConfirmationModal,
        staticAnnotations,
        startDragHandler,
        stopDragHandler,
        tables,
        url,
        handleZoomIn,
        handleZoomOut,
        handleResetZoom,
        location,
        tableActionOptions,
        optionSelectHandler,
        insertFirstRow,
    }
) => (
    <div className="aa-project-processing-container">
        <Helmet
            title="Automation Anywhere Validator"
            meta={[
                {name: 'description', content: 'Description of validatorContainer'},
            ]}
        />
        <Breadcrumbs>
            <Link
                to={breadCrumbsUrl.liListingUrl}>Learning instances
            </Link>
            <Link
                to={breadCrumbsUrl.liUrl}
            >
                {projectName}
            </Link>
            <Link
                to={breadCrumbsUrl.validatorUrl}
            >
                Validator
            </Link>
        </Breadcrumbs>
        <div id="aa-main-container">
            <div className="aa-grid-row space-between">
                <PageTitle label={location.query.type === "preview" ? 'Preview on Group 1' : 'Validator'} />
                {/** New project link **/}
                <div className="aa-grid-row vertical-center">
                    <CommandButton
                        onClick={showMarkAsInvalidHandler}
                        theme="warn"
                    >
                        Mark as invalid
                    </CommandButton>
                    <CommandButton
                        onClick={saveClickHandler}
                        theme="warn"
                        recommended
                        disabled={!allValidated}
                    >
                        Save current document
                    </CommandButton>
                </div>
            </div>
            <div className="aa-grid-row">
                <PageDescription>
                        <span>Group {classificationId} - {fileName} - {remainingDocumentReviewCount} {pluralize('document',remainingDocumentReviewCount)} remaining</span>
                </PageDescription>
            </div>

            <div className="aa-project-processing-container">
                <div className="aa-project-processing-container--inner">
                    <div className="aa-grid-column left-view">
                        <div className="body">
                            <FieldsValidationLayout
                                fields={fields}
                                selectFieldHandler={selectFieldHandler}
                                setFieldValueHandler={setFieldValueHandler}
                                setTableFieldValue={setTableFieldValue}
                                activeField={activeField}
                                tables={tables}
                                tableActionOptions={tableActionOptions}
                                optionSelectHandler={optionSelectHandler}
                                insertFirstRow={insertFirstRow}
                            >
                            </FieldsValidationLayout>
                        </div>
                        <div className="aa-grid-row space-between footer">

                            <div className="aa-grid-row vertical-center footer--control-panel">
                                <button
                                    className="control"
                                    disabled={false}
                                    onClick={nextDocumentHandler}
                                    >
                                        Skip to next file >
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="aa-grid-column right-view">
                        <div className="body">
                            <AnnotationEngine
                                activeField={activeField}
                                activeFieldId={activeFieldId}
                                activeFieldOption={activeFieldOption}
                                annotationClass={annotationClasses.length > 0 ? annotationClasses[0] : ''}
                                areaClickHandler={areaClickHandler}
                                currentZoomScale={currentZoomScale}
                                docMeta={docMeta}
                                imageLoaded={imageLoaded}
                                imageLoadHandler={imageLoadHandler}
                                mode={mode}
                                objects={objects}
                                onUpdate={onUpdate}
                                onlyShowActive={true}
                                selectedObjects={selectedObjects}
                                setLayoutRatio={setLayoutRatio}
                                staticAnnotations={staticAnnotations}
                                startDragHandler={startDragHandler}
                                stopDragHandler={stopDragHandler}
                                url={url}
                            />
                        </div>
                        <div className="footer aa-grid-row vertical-center horizontal-center">
                            <div className="control"  onClick={handleZoomIn}>
                            <img src={routeHelper('images/zoom-in.svg')} />
                            <span>Zoom In</span>
                            </div>
                            <div className="control"  onClick={handleZoomOut}>
                            <img src={routeHelper('images/zoom-out.svg')} />
                            <span>Zoom Out</span>
                            </div>
                            <div className="control" onClick={handleResetZoom}>
                            <img src={routeHelper('images/zoom-reset.svg')} />
                            <span>Fit Screen</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ActionConfirmationModal
            title={SAVE_CONFIRMATION_BOX_TITLE}
            cancelTitle = {'Cancel'}
            confirmTitle = {'Save'}
            show={showSaveConfirmationModal}
            onCancel={cancelSaveConfirmationHandler}
            onConfirm={saveConfirmHandler}
            paragraphs={[]}
            theme={themeType.info}
        />
        <MarkAsInvalidModal
            onMarkAsInvalid={confirmMarkAsInvalidHandler}
            onClose={closeMarkAsInvalidHandler}
            show={shouldShowMarkAsInvalidModal}
        />
        <PageLoadingOverlay
            label={'Loading Document...'}
            show={loadingScreen}
            fullscreen={true}
        />
    </div>
);

ValidatorPageLayout.displayName = 'ValidatorPageLayout';

ValidatorPageLayout.propTypes = {
    activeField: PropTypes.object,
    activeFieldOption: PropTypes.string,
    areaClickHandler: PropTypes.func,
    fileUploading: PropTypes.bool.isRequired,
    mode: PropTypes.number,
    params: PropTypes.object.isRequired,
    projectName: PropTypes.string,
    url: PropTypes.string,
    docMeta: PropTypes.object.isRequired,
    objects: PropTypes.arrayOf(PropTypes.object).isRequired,
    fields: PropTypes.arrayOf(PropTypes.object).isRequired,
    tables: PropTypes.arrayOf(PropTypes.object).isRequired,
    staticAnnotations: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedObjectIndex: PropTypes.number.isRequired,
    currentZoomScale: PropTypes.number.isRequired,
    annotationClasses: PropTypes.arrayOf(PropTypes.string).isRequired,
    onUpdate: PropTypes.func.isRequired,
    selectFieldHandler: PropTypes.func.isRequired,
    selectActiveFieldOptionHandler: PropTypes.func,
    setAnnotatorModeHandler: PropTypes.func,
    setFieldValueHandler: PropTypes.func.isRequired,
    fieldColumnTopOffset: PropTypes.number.isRequired,
    toggleFieldCheckbox: PropTypes.func,
    handleZoomIn: PropTypes.func.isRequired,
    handleZoomOut: PropTypes.func.isRequired,
    handleResetZoom: PropTypes.func.isRequired,
};

export default ValidatorPageLayout;
