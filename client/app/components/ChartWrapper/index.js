/**
*
* AutomationBarChart
*
*/

import React from 'react';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

const ChartWrapper = ({chart}) => {
    if (shouldShowChart(chart.props))
        return chart;

    return <img className="aa-bar-chart" width={chart.props.width} src={routeHelper('images/no-graph-placeholder.png')}/>;
};

function shouldShowChart({data}) {
    return (
        Array.isArray(data)
        && Array.isArray(data[0].values)
        && (data[0].values.filter((value) => value.y > 0).length > 0)
    );
}

ChartWrapper.displayName = 'ChartWrapper';

export default ChartWrapper;
