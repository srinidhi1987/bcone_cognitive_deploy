/**
*
* Pagination
*
*/

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

class Pagination extends PureComponent {
    static propTypes = {
        pageSize: PropTypes.number.isRequired,
        offset: PropTypes.number.isRequired,
        totalCount: PropTypes.number.isRequired,
        onPageChange: PropTypes.func.isRequired,
    };

    static displayName = 'Pagination';

    constructor(props) {
        super(props);
        this.desiredLength = props.desiredLength || 5;
    }

    render() {
        const {totalCount, pageSize, offset} = this.props;
        if (!(totalCount > pageSize)) return (<div className="aa-pagination-empty"></div>);

        const activePage = this.activePage(offset, pageSize);
        const paginationArray = this.buildPaginationArray(totalCount, pageSize, activePage);
        const trimmedArray = this.trimArray(paginationArray, activePage - 1, this.desiredLength);

        return (
            <div className="aa-pagination-component">
                <div className="aa-pagination-component-pages">
                    <a
                        className={classnames({
                            hide: paginationArray[0].pageNumber === trimmedArray[0].pageNumber,
                        })}
                        onClick={() => this.selectPage(paginationArray[0].pageStart)}
                    >
                        <div className={classnames('first-page', {
                            active: paginationArray[0].active,
                        })}>
                            <span>
                                1
                            </span>
                        </div>
                    </a>
                    <div
                        className={classnames('page-divider', {
                            hide: paginationArray[0].pageNumber === trimmedArray[0].pageNumber || paginationArray[0].pageNumber === (trimmedArray[0].pageNumber - 1),
                        })}
                    ></div>
                    {trimmedArray.map((page, index) => (
                        <a
                            key={index}
                            onClick={() => this.selectPage(page.pageStart)}
                        >
                            <div className={classnames('middle-page', {
                                'active': page.active,
                                'first-page': page.pageNumber === 1,
                                'last-page': page.pageNumber === paginationArray[paginationArray.length - 1].pageNumber,
                            })}>
                                <span>
                                    {page.pageNumber}
                                </span>
                            </div>
                        </a>
                    ))}
                    <div
                        className={classnames('page-divider', {
                            hide: paginationArray[paginationArray.length - 1].pageNumber === trimmedArray[trimmedArray.length - 1].pageNumber || paginationArray[paginationArray.length - 1].pageNumber === (trimmedArray[trimmedArray.length - 1].pageNumber + 1),
                        })}
                    ></div>
                    <a
                        className={classnames({
                            hide: paginationArray[paginationArray.length - 1].pageNumber === trimmedArray[trimmedArray.length - 1].pageNumber,
                        })}
                        onClick={() => this.selectPage(paginationArray[paginationArray.length - 1].pageStart)}
                    >
                        <div className={classnames('last-page', {
                            active: paginationArray[paginationArray.length - 1].active,
                        })}>
                            <span>
                                {paginationArray[paginationArray.length - 1].pageNumber}
                            </span>
                        </div>
                    </a>
                </div>
                <div className="aa-pagination-component-underline"></div>
            </div>
        );
    }
    /**
     * activePage - determine what page number is the currently selected page
     * @param {number} offset
     * @param {number} pageSize
     * @returns {number}
     */
    activePage(offset, pageSize) {
        if (!offset) return 1;

        const activePage = offset / pageSize;
        return activePage < 1 ? 1 : Math.ceil(activePage) + 1;
    }

    /**
     * buildPaginationArray - construct an array that will be used to render the component's state
     * @param {number} totalCount
     * @param {number} pageSize
     * @param {number} activePage
     * @returns {Array}
     */
    buildPaginationArray(totalCount, pageSize, activePage) {
        const paginationArray = [];
        for (let count = totalCount, page = 1; count > 0; count = count - pageSize, page = page + 1) {
            const pageStart = (page - 1) * pageSize;
            paginationArray.push({
                pageStart,
                pageNumber: page,
                pageEnd: count < pageSize ? pageStart + count : pageStart + pageSize,
                active: (page === activePage),
            });
        }
        return paginationArray;
    }

    /**
     * trimArray - this is used in order to only show some of the total available pages
     * @param {array} paginationArray
     * @param {number} ctivePageIndex
     * @param {number} desiredLength
     * @returns {array}
     */
    trimArray(paginationArray, activePageIndex, desiredLength) {
        if (desiredLength >= paginationArray.length) return paginationArray;

        const bumperSize = Math.floor(desiredLength / 2);

        if (activePageIndex === 0)
            return paginationArray.slice(0, desiredLength + 1);

        if (activePageIndex - bumperSize < 0)
            return paginationArray.slice(0, desiredLength + 1);

        if (activePageIndex - bumperSize === 0)
            return paginationArray.slice(0, desiredLength - activePageIndex + (bumperSize + 1));

        if (activePageIndex === (paginationArray.length - 1))
            return paginationArray.slice((paginationArray.length - 1) - (desiredLength));

        if (activePageIndex + bumperSize > paginationArray.length - 2) {
            return paginationArray.slice((paginationArray.length - 1) - (desiredLength));
        }

        return paginationArray.slice(activePageIndex - bumperSize, (activePageIndex + 1) + bumperSize);
    }

    /**
     * selectPage - fires action to send new query params via callback
     * @param {number} offset
     */
    selectPage(offset) {
        const {onPageChange, pageSize} = this.props;
        onPageChange({offset, limit: pageSize});
    }
}

export default Pagination;
