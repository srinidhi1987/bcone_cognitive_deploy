import React from 'react';
import {mount} from 'enzyme';

import Pagination from '../index';

/*
Component Description
Could look like this
 1   |   4   5   6   7   8   |  10
(F) (D) (M) (M) (M) (M) (M) (D) (L)

F = first page, invisible if first (M) page is also page "1"
D = Dividers that are visible if there is a
M = Middle pages
L = Last page, invisible if final (M) is also the last page

 */

describe('Component: Pagination', () => {

    const desiredLengths = [5, 7, 9, 11, 13, 15];

    describe('when a page is clicked', () => {
        let callback, desiredLength, props, wrapper, pages;

        const firstPageAndDivider = 2;

        beforeEach(() => {
            // given
            callback = jest.fn();

            desiredLength = 5;

            props = {
                pageSize: 10,
                offset: 0,
                totalCount: 50,
                onPageChange: callback,
                desiredLength,
                loading: false,
            };

            wrapper = mount(<Pagination {...props} />);
            pages = wrapper.find('.aa-pagination-component-pages');
        });

        describe('and the clicked page is 2', () => {
            it('should call the callback with and pass an object with limit and skip', () => {
                // when
                pages.childAt(firstPageAndDivider + 1).simulate('click');
                // then
                expect(callback).toHaveBeenCalledWith({offset: 10, limit: props.pageSize});
            });
        });

        describe('and the clicked page is 3', () => {
            it('should call the callback with and pass an object with limit and skip', () => {
                // when
                pages.childAt(firstPageAndDivider + 2).simulate('click');
                // then
                expect(callback).toHaveBeenCalledWith({offset: 20, limit: props.pageSize});
            });
        });

        describe('and the clicked page is 4', () => {
            it('should call the callback with and pass an object with limit and skip', () => {
                // when
                pages.childAt(firstPageAndDivider + 3).simulate('click');
                // then
                expect(callback).toHaveBeenCalledWith({offset: 30, limit: props.pageSize});
            });
        });

        describe('and the clicked page is 5', () => {
            it('should call the callback with and pass an object with limit and skip', () => {
                // when
                pages.childAt(firstPageAndDivider + 4).simulate('click');
                // then
                expect(callback).toHaveBeenCalledWith({offset: 40, limit: props.pageSize});
            });
        });
    });

    describe('when the totalCount is more than the pageSize', () => {
        describe('and the amount of pages (totalCount / pageSize) is LESS than the desiredLength', () => {
            // given
            const props = {
                pageSize: 10,
                offset: 0,
                totalCount: 40,
                onPageChange: () => {},
                loading: false,
            };

            // when
            const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} totalCount={(desiredLength * 10) - 10} />));

            for (let i = 0; i < wrappers.length; i++) {
                const pages = wrappers[i].find('.aa-pagination-component-pages');
                const {totalCount, pageSize} = wrappers[i].props();

                describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {

                    describe('the middle pages', () => {
                        it('should have a length that matches the amount of pages (totalCount / pageSize)', () => {
                            // then
                            expect(wrappers[i].find('.middle-page').length).toBe(Math.ceil(totalCount / pageSize));
                        });
                    });

                    describe('the first page', () => {
                        it('should not be visible', () => {
                            // then
                            expect(
                                pages
                                    .childAt(0)
                                    .hasClass('hide')
                            ).toBe(true);
                        });

                        it('should have a hidden divider after final middle page and it', () => {
                            // then
                            expect(
                                pages
                                    .childAt(1)
                                    .hasClass('page-divider')
                            ).toBe(true);

                            expect(
                                pages
                                    .childAt(1)
                                    .hasClass('hide')
                            ).toBe(true);
                        });
                    });

                    describe('the last page', () => {
                        it('should not be visible', () => {
                            // then
                            expect(
                                pages
                                    .childAt(pages.children().length - 1)
                                    .hasClass('hide')
                            ).toBe(true);
                        });

                        it('should have a hidden divider after final middle page and it', () => {
                            // then
                            expect(
                                pages
                                    .childAt(pages.children().length - 2)
                                    .hasClass('page-divider')
                            ).toBe(true);

                            expect(
                                pages
                                    .childAt(pages.children().length - 2)
                                    .hasClass('hide')
                            ).toBe(true);
                        });
                    });
                });
            }
        });

        describe('and the amount of pages (totalCount / pageSize) is EQUAL to the desiredLength', () => {
            // given
            const props = {
                pageSize: 10,
                offset: 0,
                onPageChange: () => {},
                loading: false,
            };

            // when
            const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} totalCount={desiredLength * 10} />));

            for (let i = 0; i < wrappers.length; i++) {
                const pages = wrappers[i].find('.aa-pagination-component-pages');

                describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {
                    describe('the middle pages', () => {
                        it('should have a length that matches the desiredLength', () => {
                            // then
                            expect(wrappers[i].find('.middle-page').length).toBe(desiredLengths[i]);
                        });
                    });

                    describe('the first page', () => {
                        it('should not be visible', () => {
                            // then
                            expect(
                                pages
                                    .childAt(0)
                                    .hasClass('hide')
                            ).toBe(true);
                        });

                        it('should have a hidden divider after final middle page and it', () => {
                            // then
                            expect(
                                pages
                                    .childAt(1)
                                    .hasClass('page-divider')
                            ).toBe(true);

                            expect(
                                pages
                                    .childAt(1)
                                    .hasClass('hide')
                            ).toBe(true);
                        });
                    });

                    describe('the last page', () => {
                        it('should not be visible', () => {
                            // then
                            expect(
                                pages
                                    .childAt(pages.children().length - 1)
                                    .hasClass('hide')
                            ).toBe(true);
                        });

                        it('should have a hidden divider after final middle page and it', () => {
                            // then
                            expect(
                                pages
                                    .childAt(pages.children().length - 2)
                                    .hasClass('page-divider')
                            ).toBe(true);

                            expect(
                                pages
                                    .childAt(pages.children().length - 2)
                                    .hasClass('hide')
                            ).toBe(true);
                        });
                    });
                });
            }
        });

        describe('and the amount of pages (totalCount / pageSize) is more than the desiredLength', () => {
            describe('and page 1 is active (offset <= pageSize)', () => {
                // given
                const props = {
                    pageSize: 10,
                    offset: 0,
                    totalCount: 500,
                    onPageChange: () => {},
                    loading: false,
                };

                // when
                const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} />));

                for (let i = 0; i < wrappers.length; i++) {
                    const pages = wrappers[i].find('.aa-pagination-component-pages');

                    describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {

                        describe('the first page', () => {
                            it('should be visible', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(0)
                                        .find('div')
                                        .hasClass('first-page')
                                ).toBe(true);

                                expect(
                                    pages
                                        .childAt(0)
                                        .hasClass('hide')
                                ).toBe(true);
                            });

                            it('should have a hidden divider after it and before the next middle page', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(1)
                                        .hasClass('page-divider')
                                ).toEqual(true);

                                expect(
                                    pages
                                        .childAt(1)
                                        .hasClass('hide')
                                ).toEqual(true);
                            });
                        });

                        describe('the initial middle page', () => {
                            it('should have the number 1 as text', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(0)
                                        .find('span')
                                        .text()
                                ).toBe('1');
                            });

                            it('should be active', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(2)
                                        .find('div')
                                        .hasClass('active')
                                ).toBe(true);
                            });
                        });
                    });
                }
            });

            describe('and (the index of the active page) minus ((desiredLength / 2) rounded down) is less than 0', () => {
                // given
                const props = {
                    pageSize: 10,
                    offset: 10,
                    totalCount: 500,
                    onPageChange: () => {},
                    loading: false,
                };

                // when
                const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} />));

                for (let i = 0; i < wrappers.length; i++) {
                    const pages = wrappers[i].find('.aa-pagination-component-pages');
                    describe('the first page', () => {
                        it('should not be visible', () => {
                            // then
                            expect(
                                pages
                                    .childAt(0)
                                    .hasClass('hide')
                            ).toBe(true);
                        });

                        it('should have a hidden divider after final middle page and it', () => {
                            // then
                            expect(
                                pages
                                    .childAt(1)
                                    .hasClass('page-divider')
                            ).toBe(true);

                            expect(
                                pages
                                    .childAt(1)
                                    .hasClass('hide')
                            ).toBe(true);
                        });
                    });

                    describe('the initial middle page', () => {
                        // then
                        expect(
                            pages
                                .childAt(2)
                                .find('span')
                                .text()
                        ).toBe('1');
                    });

                    describe('the second middle page', () => {
                        // then
                        expect(
                            pages
                                .childAt(3)
                                .find('span')
                                .text()
                        ).toBe('2');
                    });

                    describe('the appropriate page (not the center middle page)', () => {
                        // then
                        expect(
                            pages
                                .childAt(3)
                                .find('div')
                                .hasClass('active')
                        ).toBe(true);
                    });

                    describe('the center middle page)', () => {
                        it('should not be active', () => {
                            // then
                            expect(
                                pages
                                    .childAt(desiredLengths[i] - (i + 1))
                                    .find('div')
                                    .hasClass('active')
                            ).toBe(false);
                        });
                    });
                }
            });

            describe('and (the index of the active page) minus ((desiredLength / 2) rounded down) is equal to 0', () => {
                // given
                const props = {
                    pageSize: 10,
                    totalCount: 500,
                    onPageChange: () => {},
                    loading: false,
                };
                // when
                const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} offset={10 * Math.floor(desiredLength / 2)} />));

                for (let i = 0; i < wrappers.length; i++) {
                    const pages = wrappers[i].find('.aa-pagination-component-pages');

                    describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {
                        describe('the appropriate page (not the center middle page)', () => {
                            // then
                            expect(
                                pages
                                    .childAt(2 + Math.floor(desiredLengths[i] / 2))
                                    .find('div')
                                    .hasClass('active')
                            ).toBe(true);
                        });

                        describe('the first page', () => {
                            it('should not be visible', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(0)
                                        .hasClass('hide')
                                ).toBe(true);
                            });

                            it('should have a hidden divider between it and initial middle page', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(1)
                                        .hasClass('page-divider')
                                ).toBe(true);

                                expect(
                                    pages
                                        .childAt(1)
                                        .hasClass('hide')
                                ).toBe(true);
                            });
                        });

                        describe('the last middle page', () => {
                            it('', () => {
                                // then
                                expect(
                                    parseInt(
                                        pages
                                            .childAt(3)
                                            .find('span')
                                            .text()
                                    ) + (desiredLengths[i] - 1)
                                ).toBe(
                                    parseInt(
                                        pages
                                            .childAt(3 + (desiredLengths[i] - 1))
                                            .find('span')
                                            .text()
                                    )
                                );
                            });

                        });

                        describe('second divider', () => {
                        });
                    });
                }
            });

            describe('and (the index of the active page) minus ((desiredLength / 2) rounded down) is more than 0', () => {
                // given
                const props = {
                    pageSize: 10,
                    offset: 480,
                    totalCount: 500,
                    onPageChange: () => {},
                    loading: false,
                };
                // when
                const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} />));

                for (let i = 0; i < wrappers.length; i++) {
                    const pages = wrappers[i].find('.aa-pagination-component-pages');

                    describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {
                        describe('the last page', () => {
                            it('should not be visible', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 1)
                                        .hasClass('hide')
                                ).toBe(true);
                            });

                            it('should have a hidden divider after final middle page and it', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 2)
                                        .hasClass('page-divider')
                                ).toBe(true);

                                expect(
                                    pages
                                        .childAt(pages.children().length - 2)
                                        .hasClass('hide')
                                ).toBe(true);
                            });
                        });

                        describe('the second-to-last middle page (NOT the center middle page)', () => {
                            it('should be active', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 4)
                                        .find('div')
                                        .hasClass('active')
                                ).toBe(true);
                            });
                        });

                        describe('the center middle page', () => {
                            it('should NOT be active', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(desiredLengths[i] - (i + 1))
                                        .find('div')
                                        .hasClass('active')
                                ).toBe(false);
                            });
                        });
                    });
                }
            });

            describe('and last page is active', () => {
                // given
                const props = {
                    pageSize: 10,
                    offset: 490,
                    totalCount: 500,
                    onPageChange: () => {},
                    loading: false,
                };
                // when
                const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} />));

                for (let i = 0; i < wrappers.length; i++) {
                    const pages = wrappers[i].find('.aa-pagination-component-pages');

                    describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {

                        describe('the last page', () => {
                            it('should not be visible', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 1)
                                        .hasClass('hide')
                                ).toBe(true);
                            });

                            it('should have a hidden divider after final middle page and it', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 2)
                                        .hasClass('page-divider')
                                ).toBe(true);

                                expect(
                                    pages
                                        .childAt(pages.children().length - 2)
                                        .hasClass('hide')
                                ).toBe(true);
                            });
                        });

                        describe('the final middle page', () => {
                            it('should be active', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 3)
                                        .find('div')
                                        .hasClass('active')
                                ).toBe(true);
                            });
                        });
                    });
                }
            });

            describe('and none of the qualifications above qualifications fit', () => {
                // given
                const props = {
                    pageSize: 10,
                    offset: 100,
                    totalCount: 500,
                    onPageChange: () => {},
                    loading: false,
                };
                // when
                const wrappers = desiredLengths.map((desiredLength) => mount(<Pagination {...props} desiredLength={desiredLength} />));

                for (let i = 0; i < wrappers.length; i++) {
                    const pages = wrappers[i].find('.aa-pagination-component-pages');

                    describe(`and desiredLength prop is set to ${desiredLengths[i]}`, () => {

                        describe('the center middle page, example 5 of [3,4,5,6,7]', () => {
                            it('should be active', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(desiredLengths[i] - (i + 1))
                                        .find('div')
                                        .hasClass('active')
                                ).toBe(true);
                            });
                        });

                        describe('the middle pages', () => {
                            it('should equal the length of desiredLength', () => {
                                // then
                                expect(wrappers[i].find('.middle-page').length).toBe(desiredLengths[i]);
                            });
                        });

                        describe('the first page', () => {
                            it('should be visible', () => {
                                // then
                                expect(
                                    wrappers[i]
                                        .find('.first-page')
                                        .hasClass('hide')
                                ).toBe(false);
                            });

                            it('should not have consecutive page numbers with the first middle page, example [1, 2]', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(0)
                                        .find('span')
                                        .text()
                                ).not.toEqual(
                                    pages
                                        .childAt(2)
                                        .find('span')
                                        .text()
                                );
                            });

                            it('should have a visible divider after it and before the next middle page', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(1)
                                        .hasClass('page-divider')
                                ).toEqual(true);

                                expect(
                                    pages
                                        .childAt(1)
                                        .hasClass('hide')
                                ).toEqual(false);
                            });
                        });

                        describe('the last page', () => {
                            it('should be visible', () => {
                                // then
                                expect(
                                    pages
                                        .hasClass('hide')
                                ).toBe(false);
                            });

                            it('should not have consecutive page numbers with the last middle page, example [32,' +
                                ' 33]', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(0)
                                        .find('span')
                                        .text()
                                ).not.toEqual(
                                    pages
                                        .childAt(2)
                                        .find('span')
                                        .text()
                                );
                            });

                            it('should have a visible divider after final middle page and it', () => {
                                // then
                                expect(
                                    pages
                                        .childAt(pages.children().length - 2)
                                        .hasClass('page-divider')
                                ).toBe(true);

                                expect(
                                    pages
                                        .childAt(pages.children().length - 2)
                                        .hasClass('hide')
                                ).toBe(false);
                            });
                        });
                    });
                }
            });
        });
    });

    describe('when the totalCount prop is less than the pageSize prop (only 1 page)', () => {
        // given
        const callback = (pageObject) => {
            return pageObject;
        };
        // when
        const wrapper = mount(
            <Pagination
                pageSize={100}
                offset={0}
                totalCount={50}
                onPageChange={callback}
                loading={false}
            />
        );

        it('should visibly hide the component', () => {
            // then
            expect(wrapper.hasClass('aa-pagination-empty')).toBe(true);
            expect(wrapper.hasClass('aa-pagination')).toBe(false);
        });
    });
});

