/**
*
* TutorialOverlay
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

const TutorialOverlay = ({show, children}) => (
    <div className={classnames('aa-tutorial-overlay', {
        hidden: !show,
    })}>
        <div className="aa-tutorial-overlay-content">
            {children}
        </div>
    </div>
);

TutorialOverlay.displayName = 'TutorialOverlay';

TutorialOverlay.propTypes = {
    /**
     * Show overlay
     */
    show: PropTypes.bool,
};

TutorialOverlay.defaultProps = {
    show: false,
};

export default TutorialOverlay;
