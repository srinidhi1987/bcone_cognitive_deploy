import {
    ON_USER_INTERACTION,
    MONITOR_TIMEOUT,
} from './constants';

export function onInteraction() {
    return {
        type: ON_USER_INTERACTION,
    };
}

export function monitorTimeout() {
    return {
        type: MONITOR_TIMEOUT,
    };
}
