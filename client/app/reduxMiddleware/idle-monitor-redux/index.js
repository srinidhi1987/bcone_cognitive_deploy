import {
    monitorTimeout,
} from './actions';
import {
    ON_USER_INTERACTION,
    MONITOR_TIMEOUT,
    LAST_USER_INTERACTION_TIMESTAMP,
    LAST_AUTH_TOKEN_REFRESH_TIMESTAMP,
} from './constants';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;

export const eventList = [
    'mousemove',
    'keydown',
    'wheel',
    'DOMMouseScroll',
    'mouseWheel',
    'mousedown',
    'touchstart',
    'touchmove',
    'MSPointerDown',
    'MSPointerMove',
];

/**
 * returns redux middleware for idle timeout feature
 * @param {number} timelimit timeout in miliseconds
 * @param {Function} sessionTimeout function that issues sessionTimeout action type
 * @param {Function} refreshAuthToken function that issues refreshAuthToken action type
 */
export default function createIdleMonitorReduxMiddleware(timelimit, sessionTimeout, refreshAuthToken) {
    return (store) => {
        let intervalId = startMonitor(timelimit / 2);
        let timeoutId = null;
        localStorage.setItem(LAST_AUTH_TOKEN_REFRESH_TIMESTAMP, Date.now());
        setTimeout(() => {
            // Register user event listeners after a short delay, allowing app bootstrap to complete
            eventList.forEach((eventName) => document.addEventListener(eventName, () => handleUserInteraction()));
        }, 10 * 1000);

        function renewSession() {
            localStorage.setItem(LAST_AUTH_TOKEN_REFRESH_TIMESTAMP, Date.now());
            store.dispatch(refreshAuthToken());
        }

        function sessionExpired() {
            store.dispatch(sessionTimeout());
            clearInterval(intervalId);
            intervalId = null;
        }

        function startMonitor(timelimit) {
            return setInterval(() => {
                store.dispatch(monitorTimeout());
            }, timelimit);
        }

        function handleUserInteraction() {
            const hasToken = Boolean(JSON.parse(localStorage.getItem(APP_STORAGE.token)));
            if (!hasToken) {
                return window.location.reload();
            }

            if (!timeoutId) {
                localStorage.setItem(LAST_USER_INTERACTION_TIMESTAMP, Date.now());

                if (!intervalId && needsRefreshing()) {
                    store.dispatch(refreshAuthToken());
                    intervalId = startMonitor(timelimit / 2);
                }

                // Throttle saving user interaction to just once per frame
                timeoutId = setTimeout(() => (timeoutId = null), 100);
            }
        }

        function needsRefreshing() {
            const lastInteraction = Number(localStorage.getItem(LAST_USER_INTERACTION_TIMESTAMP));
            const lastRefresh = Number(localStorage.getItem(LAST_AUTH_TOKEN_REFRESH_TIMESTAMP));
            const refreshThrottle = lastRefresh + timelimit / 2;
            return lastInteraction > lastRefresh // User has interacted since our last refresh
                && Date.now() > refreshThrottle; // Our last refresh was a while ago
        }

        function elapsedTime() {
            return Date.now() - Number(localStorage.getItem(LAST_USER_INTERACTION_TIMESTAMP));
        }

        // return function that has action in argument
        return (next) => {

            return (action) => {
                switch (action.type) {
                    case ON_USER_INTERACTION: {
                        handleUserInteraction();
                        break;
                    }
                    case MONITOR_TIMEOUT: {

                        if (elapsedTime() >= timelimit) {
                            // Time limit has expired, Show the system session timeout message to the user.
                            // (Token may still be valid, we'll find out on the next user interaction)
                            sessionExpired();
                            break;
                        }
                        else if (needsRefreshing()) {
                            // User has performed some activity recently so we will refresh the token
                            renewSession();
                        }
                        break;
                    }
                    default:
                }
                next(action);
            };
        };
    };
}
