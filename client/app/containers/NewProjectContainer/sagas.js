import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {browserHistory} from 'react-router';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet, requestPost} from '../../utils/request';
import {handleApiError} from '../../utils/apiHandlers';
import logger from '../../utils/logger';
import {CREATE_PROJECT, FETCH_DOMAIN_LANGUAGES} from './constants';
import {
    createProjectSucceeded,
    createProjectFailed,
    createProjectLoading,
    populateDomainLanguage,
} from './actions';
import {
    removeCachedProject,
} from '../ProjectsContainer/actions';
import {ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import {uploadProjectFiles} from '../ProjectsContainer/sagas';
import routeHelper from '../../utils/routeHelper';

// Individual exports for testing
export function createProjectFromServer(data) {
    return requestPost(
        routeHelper('api/projects'),
        {
            headers: HEADERS_JSON,
            body: data,
        },
    ).catch(handleApiError);
}

/**
 * Create new project
 *
 * @param {object} action
 */
export function* createProject(action) {
    try {
        yield put(createProjectLoading(true));
        const project = yield call(createProjectFromServer, action.payload.project);
        // check if project was successful
        if (!project.success) {

            switch (project.statusCode) {
                case 404:
                case 403:
                    return yield put(createProjectFailed('Error creating Learning Instance', ALERT_LEVEL_DANGER));
                default:
                    return yield put(createProjectFailed(project.errors[0] || 'Error creating Learning Instance', ALERT_LEVEL_DANGER));
            }
        }
        if (action.payload.files) {
            yield call(uploadProjectFiles, project.data.id, action.payload.files);
        }
        yield put(createProjectSucceeded());
        yield put(removeCachedProject()); // clear cache
        yield browserHistory.push(routeHelper(`/learning-instances/${project.data.id}`));
    }
    catch (e) {
        logger.error(e);
        yield put(createProjectFailed('Error creating Learning Instance', ALERT_LEVEL_DANGER));
    }
    finally {
        yield put(createProjectLoading(false));
    }
}
export function fetchDomainLanguages() {
    return requestGet(
        routeHelper('api/domain-languages'),
        {
            headers: HEADERS_JSON,
        },
    ).catch(handleApiError);
}
export function* fetchDomainLanguagesFlow() {
    const result = yield call(fetchDomainLanguages);
    yield put(populateDomainLanguage(result));
}
export function* createProjectSaga() {
    yield* takeLatest(CREATE_PROJECT, createProject);
}

// All sagas to be loaded
export default [
    createProjectSaga,
    function* () {
        yield* takeLatest(FETCH_DOMAIN_LANGUAGES, fetchDomainLanguagesFlow);
    },
];
