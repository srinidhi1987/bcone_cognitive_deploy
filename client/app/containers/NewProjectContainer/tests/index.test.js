import {NewProjectContainer} from '../index';
import {Breadcrumbs} from 'common-frontend-components';
import ProjectForm from '../../../containers/ProjectForm';
import Helmet from 'react-helmet';
import {shallow} from 'enzyme';
import React from 'react';
import {fromJS} from 'immutable';
import routeHelper from '../../../utils/routeHelper';

describe('<NewProjectContainer />', () => {
    const wrapper = shallow(<NewProjectContainer />);

    describe('helmet headers', () => {
        const headers = wrapper.find(Helmet).props();

        it('should render page title', () => {
            expect(headers.title).toBe('Automation Anywhere | Create new project');
        });
    });

    describe('<Breadcrumbs />', () => {
        const component = wrapper.find(Breadcrumbs).first().props();

        describe('first crumb: Projects', () => {
            const crumb = component.children[0].props;

            it('should render label', () => {
                expect(crumb.children).toBe('Learning Instances');
            });

            it('should render to route', () => {
                expect(crumb.to).toBe(routeHelper('/learning-instances'));
            });
        });

        describe('second crumb: New Project', () => {
            const crumb = component.children[1].props;

            it('should render label', () => {
                expect(crumb.children).toBe('New Instance');
            });

            it('should render to route', () => {
                expect(crumb.to).toBe(routeHelper('/learning-instances/new'));
            });
        });
    });

    describe('_handleProjectSubmission method', () => {
        afterEach(() => {
            jest.clearAllMocks();
        });

        it('should construct project payload', () => {
            // given
            const createProjectSpy = jest.fn();
            const component = shallow(
                <NewProjectContainer
                    createProject={createProjectSpy}
                />
            );
            const projectData = {
                project: {
                    name: 'Testing',
                    description: 'submiting submission',
                    projectType: 'invoice',
                    primaryLanguage: 'English',
                },
                files: [],
            };
            // when
            component.instance()._handleProjectSubmission(fromJS(projectData));
            // then
            expect(createProjectSpy.mock.calls[0][0]).toEqual(projectData);
        });

        it('should construct and modify if customProjectType is present', () => {
            // given
            const createProjectSpy = jest.fn();
            const component = shallow(
                <NewProjectContainer
                    createProject={createProjectSpy}
                />
            );
            const projectData = {
                project: {
                    name: 'Testing',
                    description: 'submiting submission',
                    projectType: 'other',
                    primaryLanguage: 'English',
                    customProjectType: 'Passport',
                },
                files: [],
            };
            // when
            component.instance()._handleProjectSubmission(fromJS(projectData));
            // then
            expect(createProjectSpy.mock.calls[0][0]).toEqual({
                project: {
                    name: 'Testing',
                    description: 'submiting submission',
                    projectType: 'passport',
                    primaryLanguage: 'English',
                },
                files: [],
            });
        });
    });

    it('should include ProjectFormContainer to render form', () => {
        expect(wrapper.find(ProjectForm).length).toEqual(1);
    });
});
