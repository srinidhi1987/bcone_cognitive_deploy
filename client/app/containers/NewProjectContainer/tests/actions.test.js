import {
    createProject,
    createProjectSucceeded,
    createProjectFailed,
    createProjectLoading,
} from '../actions';
import {
    CREATE_PROJECT,
    CREATE_PROJECT_SUCCEEDED,
    CREATE_PROJECT_FAILED,
    CREATE_PROJECT_LOADING,
} from '../constants';
import {ALERT_LEVEL_DANGER} from '../../AlertsContainer/constants';

describe('NewProjectContainer actions', () => {
    describe('createProject action', () => {
        it('should handle type of CREATE_PROJECT', () => {
            // given
            const projectData = {
                projectName: 'Test 1',
            };
            const expectedState = {
                type: CREATE_PROJECT,
                payload: {...projectData},
            };
            // when
            const result = createProject(projectData);
            // then
            expect(result).toEqual(expectedState);
        });
    });

    describe('createProjectSucceeded action', () => {
        it('should handle type of CREATE_PROJECT_SUCCEEDED', () => {
            // given
            const expectedState = {
                type: CREATE_PROJECT_SUCCEEDED,
            };
            // when
            const result = createProjectSucceeded();
            // then
            expect(result).toEqual(expectedState);
        });
    });

    describe('createProjectFailed action', () => {
        it('should handle type of CREATE_PROJECT_FAILED', () => {
            // given
            const message = 'An Exception occurred';
            const expectedState = {
                type: CREATE_PROJECT_FAILED,
                message,
                level: ALERT_LEVEL_DANGER,
            };
            // when
            const result = createProjectFailed(message, ALERT_LEVEL_DANGER);
            // then
            expect(result).toEqual(expectedState);
        });
    });

    describe('createProjectLoading action', () => {
        it('should handle type of CREATE_PROJECT_LOADING', () => {
            // given
            const expectedState = {
                type: CREATE_PROJECT_LOADING,
                loading: true,
            };
            // when
            const result = createProjectLoading(true);
            // then
            expect(result).toEqual(expectedState);
        });
    });
});
