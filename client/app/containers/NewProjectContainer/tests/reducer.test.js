import newProjectContainerReducer, {initialState} from '../reducer';
import {fromJS} from 'immutable';
import {
    CREATE_PROJECT_LOADING,
    CREATE_PROJECT_SUCCEEDED,
    CREATE_PROJECT_FAILED,
} from '../constants';

describe('newProjectContainerReducer', () => {
    it('returns the initial state', () => {
        expect(newProjectContainerReducer(undefined, {})).toEqual(fromJS(initialState));
    });

    describe('CREATE_PROJECT_LOADING action', () => {
        it('should set loading to true', () => {
            // given
            // when
            const result = newProjectContainerReducer(initialState, {
                type: CREATE_PROJECT_LOADING,
                loading: true,
            });
            // then
            expect(result.get('loading')).toEqual(true);
        });

        it('should set loading to false', () => {
            // given
            const currentState = fromJS({
                ...initialState,
                loading: true,
            });
            // when
            const result = newProjectContainerReducer(currentState, {
                type: CREATE_PROJECT_LOADING,
                loading: false,
            });
            // then
            expect(result.get('loading')).toEqual(false);
        });
    });

    describe('CREATE_PROJECT_SUCCEEDED action', () => {
        it('should set creationSuccess to true', () => {
            // given
            // when
            const result = newProjectContainerReducer(initialState, {
                type: CREATE_PROJECT_SUCCEEDED,
            });
            // then
            expect(result.get('creationSuccess')).toEqual(true);
        });
    });

    describe('CREATE_PROJECT_FAILED action', () => {
        it('should set creationSuccess to true', () => {
            // given
            const currentState = fromJS({
                ...initialState,
                creationSuccess: true,
            });
            // when
            const result = newProjectContainerReducer(currentState, {
                type: CREATE_PROJECT_FAILED,
            });
            // then
            expect(result.get('creationSuccess')).toEqual(false);
        });
    });
});
