/**
 * New Project Sagas
 */

jest.mock('../../../utils/logger');
import {take, call, put} from 'redux-saga/effects';
import {browserHistory} from 'react-router';
import {
    createProjectSaga,
    createProject,
    createProjectFromServer,
} from '../sagas';
import {
    createProjectSucceeded,
    createProjectFailed,
    createProjectLoading,
} from '../actions';
import {removeCachedProject} from '../../ProjectsContainer/actions';
import {CREATE_PROJECT} from '../constants';
import {ALERT_LEVEL_DANGER} from '../../AlertsContainer/constants';
import * as projectSagas from '../../../containers/ProjectsContainer/sagas';
import routeHelper from '../../../utils/routeHelper';

describe('createProjectSaga Saga', () => {
    const createProjectSagaGenerator = createProjectSaga();

    it('should take on CREATE_PROJECT action', () => {
        // given
        // when
        const result = createProjectSagaGenerator.next();
        // then
        expect(result.value).toEqual(take(CREATE_PROJECT, createProject));
    });
});

describe('createProject generator', () => {
    const projectData = {
        payload: {
            project: {
                id: '345653454567547',
                projectName: 'Project Test',
            },
            files: [
                {name: 'filename.jpg'},
            ],
        },
    };

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('success path', () => {
        const createProjectGenerator = createProject(projectData);

        it('should set new project loading to true', () => {
            // given
            // when
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(put(createProjectLoading(true)));
        });

        it('should call server to create project', () => {
            // given
            // when
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(call(createProjectFromServer, projectData.payload.project));
        });

        it('should upload files', () => {
            // given
            const apiResponse = {
                success: true,
                data: {
                    id: '23324-34-f234f3-43f4',
                },
            };
            // when
            const result = createProjectGenerator.next(apiResponse); // should call upload saga
            //then
            expect(result.value).toEqual(call(projectSagas.uploadProjectFiles, apiResponse.data.id, projectData.payload.files));
        });

        it('should call action creator createProjectSucceeded', () => {
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(put(createProjectSucceeded()));
        });

        it('should clear project cache', () => {
            // given
            // when
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(put(removeCachedProject()));
        });

        it('should redirect to /projects after success', () => {
            // given
            // when
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(browserHistory.push(routeHelper('/learning-instances')));
        });

        it('should set new project loading to false', () => {
            // given
            // when
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(put(createProjectLoading(false)));
        });
    });

    describe('failure path', () => {
        const createProjectGenerator = createProject(projectData);

        it('should call action creator createProjectFailed', () => {
            // given
            const expection = new Error('Error creating Learning Instance');
            createProjectGenerator.next();
            // when
            const result = createProjectGenerator.throw(expection);
            // then
            expect(result.value).toEqual(put(createProjectFailed(expection.message, ALERT_LEVEL_DANGER)));
        });

        it('should set new project loading to false', () => {
            // given
            // when
            const result = createProjectGenerator.next();
            // then
            expect(result.value).toEqual(put(createProjectLoading(false)));
        });
    });
});
