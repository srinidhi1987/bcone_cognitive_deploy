import {fromJS} from 'immutable';

import selectNewProjectContainer from '../selectors';

describe('NewProjectContainer selectors', () => {
    describe('selectNewProjectContainer selector', () => {
        it('should return bootstrapping loading state as boolean', () => {
            // given
            const mockState = fromJS({
                newProjectContainer: {},
                appContainer: {
                    isBootstrapping: true,
                    pageNotifications: [],
                },
                language: {
                    supportedLanguages: [],
                },
            });
            // when
            const selector = selectNewProjectContainer();
            // then
            expect(selector(mockState).isBootstrapping).toBe(true);
        });

        it('should return supported languages from selector in supportedLanguages prop', () => {
            // given
            const supportedLanguages = [{
                id: '543645456645',
                name: 'English',
            }];
            const mockState = fromJS({
                newProjectContainer: {},
                appContainer: {
                    pageNotifications: [],
                },
                language: {
                    supportedLanguages,
                },
            });
            // when
            const selector = selectNewProjectContainer();
            // then
            expect(selector(mockState).supportedLanguages).toEqual(supportedLanguages);
        });
    });
});
