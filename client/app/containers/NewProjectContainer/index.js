/*
 *
 * NewProjectContainer
 *
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import selectNewProjectContainer from './selectors';
import {closePageNotification, displayPageNotification} from '../App/actions';
import {createProject} from './actions';
import {
    Breadcrumbs,
    PageTitle,
} from 'common-frontend-components';
import ProjectForm from '../ProjectForm';
import PageDescription from '../../components/PageDescription';
import ProjectProgressBar from '../../components/ProjectProgressBar';
import PageLoadingOverlay from '../../components/PageLoadingOverlay';
import {fetchDomainLanguages} from './actions';
import routeHelper from '../../utils/routeHelper';
import './styles.scss';

export class NewProjectContainer extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'NewProjectContainer';

    state = {};

    componentDidMount() {
        const {displayNotification, fetchDomainLanguages} = this.props;
        fetchDomainLanguages();
        // delay displaying notification for effect
        setTimeout(() => {
            displayNotification(
                'Hello! Please create your learning instance that will <strong>classify</strong>, <strong>train</strong> and <strong>learn</strong> to improve your biggest document challenges. First, start out by filling out the fields below.'
            );
        }, 1000);
    }

    _handleProjectSubmission(data) {
        const submitData = data.toJS();
        const projectData = submitData.project;

        this.props.createProject({
            ...submitData,
            project: Object.keys(projectData).sort().reduce((acc, key) => {
                // over write project type
                if (key === 'customProjectType') {
                    if (projectData[key] && projectData['projectType'] === 'other') {
                        acc['projectType'] = projectData[key].toLowerCase();
                        return acc;
                    }
                    else if (!projectData[key]) {
                        return acc;
                    }
                }
                else if (key === 'projectType' && projectData['customProjectType']) {
                    return acc;
                }

                // create key/value pair for accumulator
                acc[key] = projectData[key];

                return acc;
            }, {}),
        });
    }

    render() {
        const {loading, isBootstrapping} = this.props;

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Create new project"
                />

                <Breadcrumbs>
                    <Link to={routeHelper('/learning-instances')}>Learning Instances</Link>
                    <Link to={routeHelper('/learning-instances/new')}>New Instance</Link>
                </Breadcrumbs>

                <div id="aa-main-container">
                    <div className="aa-grid-row padded">
                        <ProjectProgressBar
                            currentStep={1}
                        />
                    </div>
                    <div className="aa-grid-row padded">
                        <PageTitle label="Create new learning instance" />
                    </div>
                    <div className="new-project-page-description">
                        <PageDescription>
                            Please <strong>fill out</strong> the required fields and <strong>upload files</strong> (ex. Invoices) so your learning instance can classify, train and learn to improve your biggest document challenges.
                        </PageDescription>
                    </div>

                    {(() => {
                        // Don't show form as app is bootstrapping
                        if (!isBootstrapping) {
                            return (
                                <ProjectForm
                                    onSubmit={this._handleProjectSubmission.bind(this)}
                                />
                            );
                        }
                    })()}

                    <PageLoadingOverlay
                        label="Creating Instance..."
                        show={loading}
                        fullscreen={true}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectNewProjectContainer();

function mapDispatchToProps(dispatch) {
    return {
        createProject: (project) => dispatch(createProject(project)),
        displayNotification: (message) => dispatch(displayPageNotification(message)),
        closeNotification: () => dispatch(closePageNotification()),
        fetchDomainLanguages: () => dispatch(fetchDomainLanguages()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProjectContainer);
