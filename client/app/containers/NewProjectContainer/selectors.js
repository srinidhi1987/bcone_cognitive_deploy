import {List} from 'immutable';
import {createSelector} from 'reselect';

/**
 * Direct selector to the newProjectContainer state domain
 */
const selectNewProjectContainerDomain = () => (state) => state.get('newProjectContainer');
const selectAppStateDomain = () => (state) => state.get('appContainer');
const getSupportedLanguages = () => (state) => state.getIn(['language', 'supportedLanguages']);

/**
 * Other specific selectors
 */


/**
 * Default selector used by NewProjectContainer
 */

const selectNewProjectContainer = () => createSelector(
    selectNewProjectContainerDomain(),
    selectAppStateDomain(),
    getSupportedLanguages(),
    (substate, appState, supportedLanguages) => {
        return {
            ...substate.toJS(),
            isBootstrapping: appState.get('isBootstrapping'),
            pageNotification: appState.get('pageNotifications').toArray()[0],
            supportedLanguages: List.isList(supportedLanguages) ? supportedLanguages.toJS() : supportedLanguages,
        };
    }
);

export default selectNewProjectContainer;
export {
    selectNewProjectContainerDomain,
};
