/*
 *
 * NewProjectContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
    CREATE_PROJECT_SUCCEEDED, //eslint-disable-line
    CREATE_PROJECT_FAILED,
    CREATE_PROJECT_LOADING,
    POPULATE_DOMAIN_LANGUAGES,
} from './constants';

const initialState = fromJS({
    loading: false,
    creationSuccess: false,
});

function newProjectContainerReducer(state = initialState, action) {
    switch (action.type) {
        case CREATE_PROJECT_LOADING:
            return state.set('loading', action.loading);
        case CREATE_PROJECT_SUCCEEDED:
            return state.set('creationSuccess', true);
        case CREATE_PROJECT_FAILED:
            return state.set('creationSuccess', false);
        case POPULATE_DOMAIN_LANGUAGES:
            return state.set('domainLanguages', fromJS(action.data));
        default:
            return state;
    }
}

export default newProjectContainerReducer;
export {initialState};
