/*
 *
 * NewProjectContainer constants
 *
 */

export const CREATE_PROJECT = 'app/NewProjectContainer/CREATE_PROJECT';
export const CREATE_PROJECT_SUCCEEDED = 'app/NewProjectContainer/CREATE_PROJECT_SUCCEEDED';
export const CREATE_PROJECT_FAILED = 'app/NewProjectContainer/CREATE_PROJECT_FAILED';
export const CREATE_PROJECT_LOADING = 'app/NewProjectContainer/CREATE_PROJECT_LOADING';
export const FETCH_DOMAIN_LANGUAGES = 'app/NewProjectContainer/FETCH_DOMAIN_LANGUAGES';
export const POPULATE_DOMAIN_LANGUAGES = 'app/NewProjectContainer/POPULATE_DOMAIN_LANGUAGES';
