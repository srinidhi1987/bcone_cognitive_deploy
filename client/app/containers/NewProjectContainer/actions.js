/*
 *
 * NewProjectContainer actions
 *
 */

import {
    CREATE_PROJECT_SUCCEEDED,
    CREATE_PROJECT_FAILED,
    CREATE_PROJECT,
    CREATE_PROJECT_LOADING,
    FETCH_DOMAIN_LANGUAGES,
    POPULATE_DOMAIN_LANGUAGES,
} from './constants';

export function createProject(payload) {
    return {
        type: CREATE_PROJECT,
        payload,
    };
}

export function createProjectSucceeded() {
    return {
        type: CREATE_PROJECT_SUCCEEDED,
    };
}

export function createProjectFailed(message, level) {
    return {
        type: CREATE_PROJECT_FAILED,
        message,
        level,
    };
}

export function createProjectLoading(loading) {
    return {
        type: CREATE_PROJECT_LOADING,
        loading,
    };
}

export function fetchDomainLanguages() {
    return {
        type: FETCH_DOMAIN_LANGUAGES,
    };
}

export function populateDomainLanguage(data) {
    return {
        type: POPULATE_DOMAIN_LANGUAGES,
        data,
    };
}
