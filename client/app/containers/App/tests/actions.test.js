/**
 * Created by iamcutler on 11/29/16.
 */

jest.mock('../../../utils/logger');
import logger from '../../../utils/logger';
import {
    ADD_APP_NOTIFICATION,
    ADD_APP_NOTIFICATIONS,
    BOOTSTRAP_APPLICATION,
    CLOSE_PAGE_NOTIFICATION,
    CRITICAL_APPLICATION_ERROR,
    DISPLAY_PAGE_NOTIFICATION,
    IS_BOOTSTRAPPING_APPLICATION,
    PERSIST_AUTH_TOKEN,
    PERSIST_ENCRYPTED_APP_CONFIG,
    PERSIST_USER,
    PERSIST_USER_AND_AUTH_TOKEN,
    REMOVE_APP_NOTIFICATION_BY_ID,
    VIEW_CONTAINER_LOADING,
    APP_RELOAD_REQUEST,
    APP_RELOAD_CANCELED,
    APP_RELOAD_REQUEST_CONFIRMED,
} from '../constants';
import {
    addAppNotification,
    addAppNotifications,
    bootstrapApp,
    closePageNotification,
    displayPageNotification,
    isBootstrappingApp,
    persistAuthToken,
    persistEncryptedAppConfiguration,
    persistUser,
    persistUserAndAuthToken,
    removeAppNotificationById,
    setCriticalAppError,
    viewContainerLoading,
    reloadAppRequest,
    reloadAppCanceled,
    reloadAppConfirmed,
} from '../actions';
import {APP_NOTIFICATIONS} from '../../../constants/enums';

describe('app actions', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('viewContainerLoading action creator', () => {
        // given
        const expectedAction = {
            type: VIEW_CONTAINER_LOADING,
            loading: true,
        };
        // when
        const result = viewContainerLoading(true);
        // then
        expect(result).toEqual(expectedAction);
    });

    describe('setCriticalAppError', () => {
        it('should pass action to set critical error', () => {
            // given
            const expected = {
                type: CRITICAL_APPLICATION_ERROR,
            };
            // when
            const result = setCriticalAppError();
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('bootstrapApp action creator', () => {
        it('should return with type BOOTSTRAP_APPLICATION to kick off initial actions', () => {
            // given
            const expected = {
                type: BOOTSTRAP_APPLICATION,
            };
            // when
            const result = bootstrapApp();
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('displayPageNotification action creator', () => {
        it('should pass message to display on page', () => {
            // given
            const message = 'Hello Mr. Anderson';
            const expectedAction = {
                type: DISPLAY_PAGE_NOTIFICATION,
                message,
            };
            // when
            const result = displayPageNotification(message);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('closePageNotification action creator', () => {
        it('should pass message to close page notification', () => {
            // given
            const expectedAction = {
                type: CLOSE_PAGE_NOTIFICATION,
            };
            // when
            const result = closePageNotification();
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('isBootstrappingApp action creator', () => {
        // given
        const expectedAction = {
            type: IS_BOOTSTRAPPING_APPLICATION,
            loading: true,
        };
        // when
        const result = isBootstrappingApp(true);
        // then
        expect(result).toEqual(expectedAction);
    });

    describe('persistUser action creator', () => {
        it('should pass user to store in state', () => {
            // given
            const user = {
                id: '64565567576567576',
                name: 'John Smith',
            };
            const expectedAction = {
                type: PERSIST_USER,
                user,
            };
            // when
            const result = persistUser(user);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('addAppNotification action creator', () => {
        it('should pass message and level in action', () => {
            // given
            const message = 'Test';
            const level = APP_NOTIFICATIONS.SUCCESS;
            const expectedAction = {
                type: ADD_APP_NOTIFICATION,
                id: '',
                message,
                level,
            };
            // when
            const result = addAppNotification(message, level);
            // then
            expect(result).toEqual(expectedAction);
        });

        describe('failure', () => {
            let message, level, result, errorSpy;

            beforeEach(() => {
                // given
                message = 'Test';
                level = 'test';
                errorSpy = jest.spyOn(logger, 'error');
                // when
                result = addAppNotification(message, level);
            });

            it('should not return action if level is not correct', () => {
                // given
                const expectedAction = {
                    type: null,
                };
                // when
                // then
                expect(result).toEqual(expectedAction);
            });

            it('should console error', () => {
                // then
                expect(errorSpy).toHaveBeenCalledWith('Must pass the correct level for app notifications');
            });
        });
    });

    describe('addAppNotifications action creator', () => {
        it('should send bulk messages to app notifications', () => {
            // given
            const level = APP_NOTIFICATIONS.SUCCESS;
            const messages = [
                {id: '', message: 'Testing 1'},
                {id: '', message: 'Testing 2'},
                {id: '', message: 'Testing 3'},
                {id: '', message: 'Testing 4'},
                {id: '', message: 'Testing 5'},
            ];
            const expectedAction = {
                type: ADD_APP_NOTIFICATIONS,
                messages: messages.map((msg) => {
                    return {
                        ...msg,
                        level,
                    };
                }),
            };
            // when
            const result = addAppNotifications(messages, level);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('removeAppNotificationById action creator', () => {
        it('should remove the notification by the unique identifier', () => {
            // given
            const id = '5465676789567546';
            const expectedAction = {
                type: REMOVE_APP_NOTIFICATION_BY_ID,
                id,
            };
            // when
            const result = removeAppNotificationById(id);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistEncryptedAppConfiguration action creator', () => {
        it('should save app configuration', () => {
            // given
            const config = {
                appToken: '34564545646465456434432',
                username: 'Testing',
            };
            const expectedAction = {
                type: PERSIST_ENCRYPTED_APP_CONFIG,
                config,
            };
            // when
            const result = persistEncryptedAppConfiguration(config);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistAuthToken action creator', () => {
        it('should return action with token', () => {
            // given
            const token = 'oihrgepoih34t89ht420ihew';
            const expectedAction = {
                type: PERSIST_AUTH_TOKEN,
                token,
            };
            // when
            const result = persistAuthToken(token);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistUserAndToken action creator', () => {
        it('should return action with user and token', () => {
            // given
            const user = {name: 'Testing'};
            const token = '45346556767854643223';
            const expectedAction = {
                type: PERSIST_USER_AND_AUTH_TOKEN,
                user,
                token,
            };
            // when
            const result = persistUserAndAuthToken(user, token);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('reloadAppRequest action creator', () => {
        it('should return action with correct type', () => {
            // given
            const expectedResult = {
                type: APP_RELOAD_REQUEST,
            };
            // when
            const result = reloadAppRequest();
            // then
            expect(result).toEqual(expectedResult);
        });
    });

    describe('reloadAppCanceled action creator', () => {
        it('should return action with correct type', () => {
            // given
            const expectedResult = {
                type: APP_RELOAD_CANCELED,
            };
            // when
            const result = reloadAppCanceled();
            // then
            expect(result).toEqual(expectedResult);
        });
    });

    describe('reloadAppConfirmed action creator', () => {
        it('should return action with correct type', () => {
            // given
            const expectedResult = {
                type: APP_RELOAD_REQUEST_CONFIRMED,
            };
            // when
            const result = reloadAppConfirmed();
            // then
            expect(result).toEqual(expectedResult);
        });
    });
});
