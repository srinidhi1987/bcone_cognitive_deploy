/**
 * Created by iamcutler on 11/29/16.
 */
import {all, call, put, take} from 'redux-saga/effects';
import {browserHistory} from 'react-router';
import {BOOTSTRAP_APPLICATION, APP_RELOAD_REQUEST_CONFIRMED} from '../constants';
import {
    isBootstrappingApp,
    addAppNotifications,
    persistEncryptedAppConfiguration,
    persistUserAndAuthToken,
} from '../actions';
import {
    bootstrapApp,
    bootstrapAppSaga,
    reload,
    reloadApp,
    reloadAppSaga,
    fetchUserDataFlow,
    fetchAppConfigFlow,
    fetchLearningInstancesFlow,
    fetchEncryptedAppConfigurationFromServer,
    testSession,
    refreshAuthTokenFlow,
} from '../sagas';
import {fetchProjectFields, fetchProjectsFromServer} from '../../ProjectsContainer/sagas';
import {MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE} from '../../ProjectsContainer/constants';
import {fetchLanguagesFlow} from '../../LanguageProvider/sagas';
import {APP_NOTIFICATIONS, IQBOT_USER_ROLES} from '../../../constants/enums';
import routeHelper from '../../../utils/routeHelper';

import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;


describe('bootstrapApp Sagas', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should take latest on BOOTSTRAP_APPLICATION action', () => {
        // given
        const generator = bootstrapAppSaga();
        // when
        const result = generator.next();
        // then
        expect(result.value).toEqual(take(BOOTSTRAP_APPLICATION, bootstrapApp));
    });

    describe('bootstrapApp generator', () => {
        const token = '645665756767867867678678';
        const user = {
            firstName: 'Mak',
            roles: [{name: IQBOT_USER_ROLES.SERVICES}],
            username: 'iqbotuser01',
        };

        afterEach(() => {
            // clear local storage
            window.localStorage.removeItem(APP_STORAGE.user);
            window.localStorage.removeItem(APP_STORAGE.token);
        });

        describe('success path', () => {
            // given
            const generator = bootstrapApp();
            beforeEach(() => {
                browserHistory.replace({pathname: routeHelper('/')});
            });

            it('should set bootstrapping loader to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(isBootstrappingApp(true)));
            });

            it('should call generator to handle login flow if local storage data is present', () => {
                // given
                window.localStorage.setItem(APP_STORAGE.token, token);
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchUserDataFlow));
            });

            it('should fetch configuration and data', () => {
                // given
                const effects = {
                    fetchAppConfig: call(fetchAppConfigFlow, user),
                    fetchLearningInstances: call(fetchLearningInstancesFlow),
                    fetchLanguages: call(fetchLanguagesFlow),
                    refreshAuthToken: call(refreshAuthTokenFlow),
                };
                // when
                const result = generator.next(user);
                // then
                expect(result.value).toEqual(all(effects));

            });

            it('should redirect to dashboard', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(browserHistory.replace, routeHelper('/dashboard')));
            });

            it('should set bootstrapping loader to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(isBootstrappingApp(false)));
            });
        });

        describe('FetchLearningInstancesFlow', () => {
            // given
            const generator = fetchLearningInstancesFlow();

            it('should fetch all projects', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchProjectsFromServer));
            });

            it('should iterate and add notification for projects under the training threshold for all projects', () => {
                // given
                const projectsApiResponse = {
                    success: true,
                    data: [
                        {
                            id: '456654756678867',
                            name: 'Project Name',
                            currentTrainedPercentage: 20,
                        },
                    ],
                };
                // when
                const result = generator.next(projectsApiResponse);
                // then
                expect(result.value).toEqual(put(addAppNotifications([
                    {
                        id: `project-threshold-${projectsApiResponse.data[0].id}`,
                        message: `Your instance '${projectsApiResponse.data[0].name}' is below the ${MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE}% training threshold`,
                    },
                ], APP_NOTIFICATIONS.WARNING)));
            });

            it('should fetch project fields', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchProjectFields));
            });
        });

        describe('FetchAppConfigFlow', () => {
            // given
            const generator = fetchAppConfigFlow(user);

            it('should fetch app config', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchEncryptedAppConfigurationFromServer, user.username));
            });

            it('should persist app config', () => {
                // given
                const appConfigApiResponse = {
                    success: true,
                    data: [
                        {
                            fizz: 'buzz',
                        },
                    ],
                };
                // when
                const result = generator.next(appConfigApiResponse);
                // then
                expect(result.value).toEqual(put(persistEncryptedAppConfiguration(appConfigApiResponse.data)));
            });
        });

        describe('when user has validator access', () => {
            it('should redirect to validation', () => {
                // given
                const validator = {
                    ...user,
                    roles: [{name: IQBOT_USER_ROLES.VALIDATOR}],
                };
                browserHistory.replace(routeHelper('/'));
                window.localStorage.setItem(APP_STORAGE.token, token);
                const generator = bootstrapApp();
                generator.next(); // loading state
                generator.next(); // fetch user
                generator.next(validator); // fetch app data
                //when
                const result = generator.next(); // browserHistory
                //then
                expect(result.value).toEqual(call(browserHistory.replace, routeHelper('/learning-instances/validations')));
            });
        });

        describe('when the url path is not root /', () => {

            describe('and user has services access', () => {
                it('should not redirect to dashboard', () => {
                    // given
                    browserHistory.replace(routeHelper('/somepath'));
                    window.localStorage.setItem(APP_STORAGE.token, token);
                    const generator = bootstrapApp();
                    generator.next(); // loading state
                    generator.next(); // fetch user
                    generator.next(user); // fetch app data
                    generator.next(); // loading state
                    //when
                    const result = generator.next();
                    //then
                    expect(result.done).toEqual(true);
                });
            });

            describe('and user has validator access', () => {
                it('should should not redirect to validations', () => {
                    // given
                    const validator = {
                        ...user,
                        roles: [{name: IQBOT_USER_ROLES.VALIDATOR}],
                    };
                    browserHistory.replace(routeHelper('/somepath'));
                    window.localStorage.setItem(APP_STORAGE.token, token);
                    const generator = bootstrapApp();
                    generator.next(); // loading state
                    generator.next(); // fetch user
                    generator.next(validator); // fetch app data
                    generator.next(); // loading state
                    //when
                    const result = generator.next();
                    //then
                    expect(result.done).toEqual(true);
                });
            });

        });

        describe('when IQBot is not registered with the control room', () => {
            let originalCrUrl;

            beforeEach(() => {
                originalCrUrl = iqBotConfig.CONTROL_ROOM_URL;
                iqBotConfig.CONTROL_ROOM_URL = '';
            });

            afterEach(() => {
                iqBotConfig.CONTROL_ROOM_URL = originalCrUrl;
            });

            it('should redirect to the registration required error message', () => {
                // given
                browserHistory.replace(routeHelper('/'));
                const generator = bootstrapApp();
                generator.next(); // loading state
                //when
                const result = generator.next(); // browserHistory
                //then
                expect(result.value).toEqual(call(browserHistory.replace, routeHelper('/registration-required')));
            });
        });
    });

    describe('reloadAppSaga generator', () => {

        it('should take latest on APP_RELOAD_REQUEST_CONFIRMED action', () => {
            // given
            const generator = reloadAppSaga();
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(take(APP_RELOAD_REQUEST_CONFIRMED, reloadApp));
        });

        it('should reload app', () => {
            const generator = reloadApp();
            const result = generator.next();
            expect(result.value).toEqual(call(reload));
        });
    });

    describe('Session Timeout', () => {

        it('should attempt to get the current user data', () => {
            const generator = testSession();
            const result = generator.next(null);
            expect(result.value).toEqual(call(fetchUserDataFlow));
        });
    });

    describe('Session Renew', () => {

        it('should save the updated token and user data', () => {
            localStorage.removeItem(APP_STORAGE.user);
            localStorage.removeItem(APP_STORAGE.token);
            const tokenResponse = {
                user: {
                    firstName: 'Mak',
                    roles: [{name: IQBOT_USER_ROLES.SERVICES}],
                    username: 'iqbotuser01',
                },
                token: 'eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiIyIiwiY2xpZW50VHlwZSI6IldFQiIsImxpY2Vuc2VzIjpbIkRFVkVMT1BNRU5UIl0sImFuYWx5dGljc0xpY2Vuc2VzUHVyY2hhc2VkIjp7IkFuYWx5dGljc0NsaWVudCI6ZmFsc2UsIkFuYWx5dGljc0FQSSI6ZmFsc2V9LCJpYXQiOjE1MzU3NjM3MTksImV4cCI6MTUzNTc2NDkxOSwiaXNzIjoiQXV0b21hdGlvbkFueXdoZXJlIiwibmFub1RpbWUiOjc0NDc3MDYzMjA2ODcwMH0.YMsJwASQI-93hLEvpah32qRF1qGnWVJSYMPeQ5qkgNNlAef3kW9sOUIDovv4dxtYUaN4wvasFAkciLmw_u0TJhqOIOH3rrhLiXPpuD8oA7A0GcDsUGINzWd21qtXibyazt6DDWuh1Cx46WyhTf10ep2-FIgJ9s8kElG2b2NsIueXS_H9b8yNugtqw1CbMMhyQOOkUFy3E4Kpuknj-b4_oETUvaWUylyuEKRjgfgSqkEAYE0ikw4lb9UFVhs9zn-BIZ6s0Nksv5ZvVi_8F1tML_arQaNSXtWmNRZsLEoVLkqtHqvnWuRS-J7m0ZCL1AKFKUZFfFc1Z_wO8SGGppj_YA',
                success: true,
            };

            const generator = refreshAuthTokenFlow();
            generator.next(); // fetchAuthToken
            const result = generator.next(tokenResponse);
            expect(result.value).toEqual(put(persistUserAndAuthToken(tokenResponse.user, tokenResponse.token)));

            generator.next(); // save to local storage
            expect(localStorage.getItem(APP_STORAGE.user)).toEqual(JSON.stringify(tokenResponse.user));
            expect(localStorage.getItem(APP_STORAGE.token)).toEqual(JSON.stringify(tokenResponse.token));
        });

    });
});
