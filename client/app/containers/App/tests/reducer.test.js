/**
 * Created by iamcutler on 11/29/16.
 */

import {fromJS} from 'immutable';
import {
    CLOSE_PAGE_NOTIFICATION,
    CRITICAL_APPLICATION_ERROR,
    DISPLAY_PAGE_NOTIFICATION,
    IS_BOOTSTRAPPING_APPLICATION,
    VIEW_CONTAINER_LOADING,
} from '../constants';
import {APP_NOTIFICATIONS} from '../../../constants/enums';
import appReducer, {initialState} from '../reducer';
import {
    addAppNotification,
    addAppNotifications,
    persistAuthToken,
    persistEncryptedAppConfiguration,
    persistUser,
    persistUserAndAuthToken,
    removeAppNotificationById,
    reloadAppRequest,
    reloadAppCanceled,
} from '../actions';

describe('appReducer', () => {
    it('returns the initial state', () => {
        expect(appReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle CRITICAL_APPLICATION_ERROR action', () => {
        // given
        const action = {
            type: CRITICAL_APPLICATION_ERROR,
        };
        const expectedState = {
            ...initialState.toJS(),
            criticalFailure: true,
        };
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle VIEW_CONTAINER_LOADING action', () => {
        // given
        const action = {
            type: VIEW_CONTAINER_LOADING,
            loading: true,
        };
        const expectedState = {
            ...initialState.toJS(),
            viewContainerLoading: true,
        };
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle DISPLAY_PAGE_NOTIFICATION action and add to stack', () => {
        // given
        const message = 'Hello, We are testing';
        const expectedState = {
            ...initialState.toJS(),
            pageNotifications: [message],
        };
        const action = {
            type: DISPLAY_PAGE_NOTIFICATION,
            message,
        };
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle CLOSE_PAGE_NOTIFICATION action and remove from stack', () => {
        // given
        const message = 'Hello, We are testing';
        const message2 = 'Hello There';
        const currentState = fromJS({
            ...initialState.toJS(),
            pageNotifications: [message, message2],
        });
        const expectedState = {
            ...initialState.toJS(),
            pageNotifications: [message2],
        };
        const action = {
            type: CLOSE_PAGE_NOTIFICATION,
            message,
        };
        // when
        const result = appReducer(currentState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle IS_BOOTSTRAPPING_APPLICATION action as boolean', () => {
        // given
        const action = {
            type: IS_BOOTSTRAPPING_APPLICATION,
            loading: false,
        };
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual({
            ...initialState.toJS(),
            isBootstrapping: false,
        });
    });

    it('should handle PERSIST_USER action and persist user in store', () => {
        // given
        const user = {
            id: '457567688764564352',
            name: 'John Smith',
        };
        const action = persistUser(user);
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.toJS().user).toEqual(user);
    });

    it('should handle ADD_APP_NOTIFICATION action and add app notification to queue', () => {
        // given
        const message = 'Testing';
        const id = '64645657568875';
        const level = APP_NOTIFICATIONS.ERROR;
        const action = addAppNotification(message, level, id);
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.get('appNotifications').toJS()).toEqual([{
            id,
            message,
            level,
        }]);
    });

    it('should handle ADD_APP_NOTIFICATIONS action and add app notification to queue', () => {
        // given
        const level = APP_NOTIFICATIONS.SUCCESS;
        const messages = [
            {
                id: '6545656867886787',
                message: 'Testing 1',
            }, {
                id: '6545656867886788',
                message: 'Testing 2',
            }, {
                id: '6545656867886789',
                message: 'Testing 3',
            },
        ];
        const action = addAppNotifications(messages, level);
        const currentState = fromJS({
            ...initialState.toJS(),
            appNotifications: [
                {
                    id: '75676786787869786',
                    message: 'Testing 0',
                    level: APP_NOTIFICATIONS.WARNING,
                },
            ],
        });
        // when
        const result = appReducer(currentState, action);
        // then
        expect(result.get('appNotifications').toJS()).toEqual([
            ...messages.map((msg) => {
                return {
                    ...msg,
                    level,
                };
            }),
            ...currentState.get('appNotifications').toJS(),
        ]);
    });

    it('should handle REMOVE_APP_NOTIFICATIONS_BY_ID action and remove app notification from queue by id', () => {
        // given
        const id = '765678899865645334';
        const action = removeAppNotificationById(id);
        const currentState = {
            ...initialState.toJS(),
            appNotifications: [
                {
                    id: '75676786787869786',
                    message: 'Testing 0',
                    level: APP_NOTIFICATIONS.WARNING,
                }, {
                    id,
                    message: 'Testing 1',
                    level: APP_NOTIFICATIONS.WARNING,
                }, {
                    id: '756767867878697657',
                    message: 'Testing 2',
                    level: APP_NOTIFICATIONS.WARNING,
                },
            ],
        };
        // when
        const result = appReducer(fromJS(currentState), action);
        // then
        expect(result.get('appNotifications').toJS()).toEqual([
            currentState.appNotifications[0],
            currentState.appNotifications[2],
        ]);
        expect(result.get('appNotifications').size).toEqual(2);
    });

    it('should handle PERSIST_ENCRYPTED_APP_CONFIG action type', () => {
        // given
        const config = {
            encrypted: '3432434233',
            designer: true,
        };
        const action = persistEncryptedAppConfiguration(config);
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.get('appConfig').toJS()).toEqual({...config, authToken: ''});
    });

    it('should handle PERSIST_APP_TOKEN action type', () => {
        // given
        const token = '465567568657453243345';
        const action = persistAuthToken(token);
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.get('authToken')).toEqual(token);
    });

    it('should handle PERSIST_USER_AND_AUTH_TOKEN action type', () => {
        // given
        const token = '657766876788674564';
        const user = {name: 'Testing'};
        const action = persistUserAndAuthToken(user, token);
        // when
        const result = appReducer(initialState, action);
        // then
        expect(result.get('user').toJS()).toEqual(user);
        expect(result.get('authToken')).toEqual(token);
    });

    it('should handle APP_RELOAD_REQUEST action type', () => {
        // when
        const result = appReducer(initialState, reloadAppRequest());
        // then
        expect(result.get('reloadAppRequest')).toEqual(true);
    });

    it('should handle APP_RELOAD_CANCELED action type', () => {
        // when
        const result = appReducer(initialState, reloadAppCanceled());
        // then
        expect(result.get('reloadAppRequest')).toEqual(false);
    });

});
