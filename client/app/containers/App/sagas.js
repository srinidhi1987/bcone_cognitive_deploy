import {takeLatest} from 'redux-saga';
import {browserHistory} from 'react-router';
import {all, call, put} from 'redux-saga/effects';

import {
    requestGet,
    requestPost,
} from '../../utils/request';
import {HEADERS_JSON} from 'common-frontend-utils';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import {
    APP_NOTIFICATIONS,
    IQBOT_USER_ROLES,
} from '../../constants/enums';
import {
    BOOTSTRAP_APPLICATION,
    APP_RELOAD_REQUEST_CONFIRMED,
    APP_LOGOUT,
    APP_SESSION_TIMEOUT,
    APP_REFRESH_AUTH_TOKEN,
} from './constants';
import {MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE} from '../ProjectsContainer/constants';
import {isAllowedAccess} from '../../utils/roleHelpers';
import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {fetchProjectFields, fetchProjectsFromServer} from '../ProjectsContainer/sagas';
import {
    isBootstrappingApp,
    setCriticalAppError,
    persistEncryptedAppConfiguration,
    persistUserAndAuthToken,
    addAppNotifications,
} from './actions';
import {fetchLanguagesFlow} from '../LanguageProvider/sagas';
import {appBackEnd} from '../../resources/appBackEnd';
import routeHelper from '../../utils/routeHelper';

/**
 * Bootstrapping
 */
export function* bootstrapApp() {
    let homePath = routeHelper('/login');
    let forceHome = true;
    try {
        yield put(isBootstrappingApp(true));

        if (!iqBotConfig.CONTROL_ROOM_URL) {
            homePath = routeHelper('/registration-required');
            return;
        }

        const user = yield call(fetchUserDataFlow);
        if (user) {
            const pathname = browserHistory.getCurrentLocation().pathname;
            forceHome = pathname.toLowerCase() === routeHelper('/').toLowerCase();
            const effects = {
                fetchAppConfig: call(fetchAppConfigFlow, user),
                refreshAuthToken: call(refreshAuthTokenFlow),
            };

            if (isAllowedAccess([IQBOT_USER_ROLES.VALIDATOR], user.roles)) {
                homePath = routeHelper('/learning-instances/validations');
            }
            else {
                homePath = routeHelper('/dashboard');
                effects.fetchLearningInstances = call(fetchLearningInstancesFlow);
                effects.fetchLanguages = call(fetchLanguagesFlow);
            }

            yield all(effects);
        }
    }
    catch (err) {
        // set critical application error to display error view
        console.error(err); // eslint-disable-line no-console
        yield put(setCriticalAppError());
    }
    finally {
        if (forceHome) {
            yield call(browserHistory.replace, homePath);
        }
        yield put(isBootstrappingApp(false));
    }
}

/**
 * Fetch encrypted app configuration values
 * @param {string} username - current logged in user
 * @returns {Promise}
 */
export function fetchEncryptedAppConfigurationFromServer(username = '') {
    return requestGet(
        routeHelper('api/app-config'),
        {
            headers: {
                ...HEADERS_JSON,
                username,
            },
        }
    )
        .then(handleApiResponse)
        .catch(handleApiError);
}

/**
 * Fetch current authenticated user details
 * @returns {Promise}
 */
export function fetchCurrentUserDetails() {
    return requestPost(
        routeHelper('api/authentication/current-user'),
        {
            headers: {
                ...HEADERS_JSON,
            },
        }
    )
        .then(handleApiResponse)
        .catch(handleApiError);
}

// Export for testing
export function* fetchUserDataFlow() {
    try {
        // check if a user is logged in from the token saved in localStorage
        const token = JSON.parse(localStorage.getItem(APP_STORAGE.token) || null);
        if (!token) {
            return null;
        }

        // Fetch user details
        const user = yield call(fetchCurrentUserDetails);
        if (!user || user.success === false) {
            return null;
        }
        yield put(persistUserAndAuthToken(user, token));
        localStorage.setItem(APP_STORAGE.user, JSON.stringify(user));
        return user;
    }
    catch (err) {
        return null;
    }
}

// Export for testing
export function* fetchAppConfigFlow(user) {
    const appConfig = yield call(fetchEncryptedAppConfigurationFromServer, user.username);
    if (!appConfig.success) {
        throw new Error('Fetching app configuration failed');
    }
    yield put(persistEncryptedAppConfiguration(appConfig.data));
    return appConfig;
}

// Export for testing
export function* fetchLearningInstancesFlow() {
    // Fetch project threshold notifications
    const projects = yield call(fetchProjectsFromServer);
    if (projects && projects.success) {
        const notificationMsgs = [];
        // Iterate over projects to look at threshold limit
        for (let n = 0; n < projects.data.length; n++) {
            const project = projects.data[n];
            if (project.currentTrainedPercentage < MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE) {
                notificationMsgs.push({
                    id: `project-threshold-${project.id}`,
                    message: `Your instance '${project.name}' is below the ${MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE}% training threshold`,
                });
            }
        }

        if (notificationMsgs.length) {
            yield put(addAppNotifications(notificationMsgs, APP_NOTIFICATIONS.WARNING));
        }
    }

    // call alias service for fields
    yield call(fetchProjectFields);
    return true;
}

/**
 * reload app
 */
export function reload() {
    window.location.reload();
}

/**
 * reload app saga flow
 */
export function* reloadApp() {
    yield call(reload);
}
/**
 * Register actions that need to happen on initial bootstrapping
 */
export function* bootstrapAppSaga() {
    yield* takeLatest(BOOTSTRAP_APPLICATION, bootstrapApp);
}

/**
 * Refresh the app on user's confirmation
 */
export function* reloadAppSaga() {
    yield* takeLatest(APP_RELOAD_REQUEST_CONFIRMED, reloadApp);
}

/**
 * Remove authentication data from local storage
 * @returns void
 */
export function removeStoredData() {
    // Unset app token for authentication and related data
    localStorage.removeItem(APP_STORAGE.token);
    localStorage.removeItem(APP_STORAGE.user);
    localStorage.removeItem(APP_STORAGE.lastUserInteractionTimestamp);
}

function* logoutFlow() {
    try {
        yield call(appBackEnd.logout);
    }
    finally {
        removeStoredData();
        yield call(browserHistory.push, {pathname: routeHelper('/login')});
    }
}

export function* logoutSaga() {
    yield* takeLatest(APP_LOGOUT, logoutFlow);
}

export function* testSession() {
    try {
        // Check that the token has indeed expired - other tabs may be keeping it alive
        const user = yield call(fetchUserDataFlow);
        if (!user) {
            throw 'Session Expired';
        }
    }
    catch (err) {
        yield call(browserHistory.push, {pathname: routeHelper('/login')});
    }
}

export function* sessionTimeoutSaga() {
    yield* takeLatest(APP_SESSION_TIMEOUT, testSession);
}

/**
 * Fetch a new auth token
 * @returns {Promise}
 */
function fetchAuthToken() {
    return requestPost(
        routeHelper('api/authentication/refresh-token'),
        {
            headers: {
                ...HEADERS_JSON,
            },
        }
    )
        .then(handleApiResponse)
        .catch(handleApiError);
}

export function* refreshAuthTokenFlow() {
    try {
        const tokenResponse = yield call(fetchAuthToken);
        if (!tokenResponse.success) {
            throw tokenResponse;
        }
        const token = tokenResponse.token;
        const user = tokenResponse.user;
        const JWT_TOKEN = /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/;
        if (!JWT_TOKEN.test(token)) {
            throw new Error('Invalid token');
        }
        yield put(persistUserAndAuthToken(user, token));
        localStorage.setItem(APP_STORAGE.user, JSON.stringify(user));
        localStorage.setItem(APP_STORAGE.token, JSON.stringify(token));
    }
    catch (err) {
        console.error('Token refresh failed', err); // eslint-disable-line no-console
        yield call(browserHistory.push, {pathname: routeHelper('/login')});
    }
}

export function* refreshAuthTokenSaga() {
    yield* takeLatest(APP_REFRESH_AUTH_TOKEN, refreshAuthTokenFlow);
}

// All sagas to be loaded
export default [
    bootstrapAppSaga,
    reloadAppSaga,
    logoutSaga,
    sessionTimeoutSaga,
    refreshAuthTokenSaga,
];
