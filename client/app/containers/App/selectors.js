import {createSelector} from 'reselect';

const selectAppStateDomain = () => (state) => state.get('appContainer');
const selectAlertsStateDomain = () => (state) => state.get('alertsContainer');

const selectAppState = () => createSelector(
    selectAppStateDomain(),
    selectAlertsStateDomain(),
    (appState, alertsState) => {
        return {
            ...(appState ? appState.toJS() : {}),
            alerts: (alertsState) ? alertsState.get('alerts').toJS() : [],
        };
    }
);

// selectLocationState expects a plain JS object for the routing state
const selectLocationState = () => {
    let prevRoutingState;
    let prevRoutingStateJS;

    return (state) => {
        const routingState = state.get('route'); // or state.route

        if (!routingState.equals(prevRoutingState)) {
            prevRoutingState = routingState;
            prevRoutingStateJS = routingState.toJS();
        }

        return prevRoutingStateJS;
    };
};

export {
    selectLocationState,
    selectAppState,
};
