/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

import {APP_NOTIFICATIONS} from '../../constants/enums';

export const DEFAULT_LOCALE = 'en';
export const BOOTSTRAP_APPLICATION = 'app/AppContainer/BOOTSTRAP_APPLICATION';
export const CRITICAL_APPLICATION_ERROR = 'app/AppContainer/CRITICAL_APPLICATION_ERROR';
export const VIEW_CONTAINER_LOADING = 'app/AppContainer/VIEW_CONTAINER_LOADING';
export const DISPLAY_PAGE_NOTIFICATION = 'app/AppContainer/DISPLAY_PAGE_NOTIFICATION';
export const CLOSE_PAGE_NOTIFICATION = 'app/AppContainer/CLOSE_PAGE_NOTIFICATION';
export const IS_BOOTSTRAPPING_APPLICATION = 'app/AppContainer/IS_BOOTSTRAPPING_APPLICATION';
export const PERSIST_USER = 'app/AppContainer/PERSIST_USER';
export const APP_NOTIFICATION_LEVELS = [APP_NOTIFICATIONS.MESSAGE, APP_NOTIFICATIONS.SUCCESS, APP_NOTIFICATIONS.WARNING, APP_NOTIFICATIONS.ERROR];
export const ADD_APP_NOTIFICATION = 'app/AppContainer/ADD_APP_NOTIFICATION';
export const ADD_APP_NOTIFICATIONS = 'app/AppContainer/ADD_APP_NOTIFICATIONS';
export const REMOVE_APP_NOTIFICATION_BY_ID = 'app/AppContainer/REMOVE_APP_NOTIFICATION_BY_ID';
export const PERSIST_ENCRYPTED_APP_CONFIG = 'app/AppContainer/PERSIST_ENCRYPTED_APP_CONFIG';
export const PERSIST_AUTH_TOKEN = 'app/AppContainer/PERSIST_AUTH_TOKEN';
export const PERSIST_USER_AND_AUTH_TOKEN = 'app/AppContainer/PERSIST_USER_AND_AUTH_TOKEN';
export const APP_RELOAD_REQUEST = 'app/AppContainer/APP_RELOAD_REQUEST';
export const APP_RELOAD_REQUEST_CONFIRMED = 'app/AppContainer/APP_RELOAD_REQUEST_CONFIRMED';
export const APP_RELOAD_CANCELED = 'app/AppContainer/APP_RELOAD_CANCELED';

export const APP_RELOAD_CONFIRMATION_BOX_TITLE = 'Your learning instance has changed.';
export const APP_RELOAD_CONFIRMATION_BOX_BODY = 'You will have to reload and try again.';
export const APP_RELOAD_CONFIRM_TITLE = 'Reload';

export const APP_LOGOUT = 'app/AppContainer/APP_LOGOUT';
export const APP_REFRESH_AUTH_TOKEN = 'app/AppContainer/APP_REFRESH_AUTH_TOKEN';
export const APP_SESSION_TIMEOUT = 'app/AppContainer/APP_SESSION_TIMEOUT';
