/**
 * Created by iamcutler on 11/29/16.
 */

import {
    ADD_APP_NOTIFICATION,
    ADD_APP_NOTIFICATIONS,
    APP_NOTIFICATION_LEVELS,
    BOOTSTRAP_APPLICATION,
    CLOSE_PAGE_NOTIFICATION,
    CRITICAL_APPLICATION_ERROR,
    DISPLAY_PAGE_NOTIFICATION,
    IS_BOOTSTRAPPING_APPLICATION,
    PERSIST_USER,
    REMOVE_APP_NOTIFICATION_BY_ID,
    VIEW_CONTAINER_LOADING,
    PERSIST_ENCRYPTED_APP_CONFIG,
    PERSIST_AUTH_TOKEN,
    PERSIST_USER_AND_AUTH_TOKEN,
    APP_RELOAD_REQUEST,
    APP_RELOAD_CANCELED,
    APP_RELOAD_REQUEST_CONFIRMED,
    APP_LOGOUT,
    APP_SESSION_TIMEOUT,
    APP_REFRESH_AUTH_TOKEN,
} from './constants';
import logger from '../../utils/logger';
import {APP_NOTIFICATIONS} from '../../constants/enums';

export function bootstrapApp() {
    return {
        type: BOOTSTRAP_APPLICATION,
    };
}

export function setCriticalAppError() {
    return {
        type: CRITICAL_APPLICATION_ERROR,
    };
}

export function viewContainerLoading(loading = false) {
    return {
        type: VIEW_CONTAINER_LOADING,
        loading,
    };
}

export function displayPageNotification(message = '') {
    return {
        type: DISPLAY_PAGE_NOTIFICATION,
        message,
    };
}

export function closePageNotification() {
    return {
        type: CLOSE_PAGE_NOTIFICATION,
    };
}

export function isBootstrappingApp(loading = false) {
    return {
        type: IS_BOOTSTRAPPING_APPLICATION,
        loading,
    };
}

/**
 * Add app level notification
 *
 * @param {string} message
 * @param {string} level
 * @param {string} id - optional unique identifier
 * @returns {object}
 * @description Add application notification with message level ('message', 'success', 'warning', 'error')
 */
export function addAppNotification(message, level = APP_NOTIFICATIONS.WARNING, id = '') {
    // Must send a correct expected level
    if (APP_NOTIFICATION_LEVELS.indexOf(level) === -1) {
        // Need to refactor the below and remove logging from actions (impure)
        logger.error('Must pass the correct level for app notifications'); // eslint-disable-line no-console
        return {
            type: null, // pass null to return current state in reducer
        };
    }

    return {
        type: ADD_APP_NOTIFICATION,
        id,
        message,
        level,
    };
}

/**
 * Add app level notification
 *
 * @param {array} messages
 * @param {string} level
 * @returns {object}
 * @description Add application notification with message level ('message', 'success', 'warning', 'error')
 */
export function addAppNotifications(messages, level = APP_NOTIFICATIONS.WARNING) {
    // Must send a correct expected level
    if (APP_NOTIFICATION_LEVELS.indexOf(level) === -1) {
        logger.error('Must pass the correct level for app notifications'); // eslint-disable-line no-console
        return {
            type: null, // pass null to return current state in reducer
        };
    }

    return {
        type: ADD_APP_NOTIFICATIONS,
        messages: messages.map((msg) => {
            return {
                id: 'id' in msg ? msg.id : null,
                message: msg.message,
                level,
            };
        }),
    };
}

/**
 * Persist user in store
 *
 * @param {object} user
 * @returns {object}
 */
export function persistUser(user) {
    return {
        type: PERSIST_USER,
        user,
    };
}

/**
 * Persist app authentication token
 *
 * @param {string} token
 * @returns {object}
 */
export function persistAuthToken(token) {
    return {
        type: PERSIST_AUTH_TOKEN,
        token,
    };
}

/**
 * Persist user and token
 *
 * @param {object} user
 * @param {string} token
 * @returns {object}
 */
export function persistUserAndAuthToken(user, token) {
    return {
        type: PERSIST_USER_AND_AUTH_TOKEN,
        user,
        token,
    };
}

/**
 * Remove app notification by id
 *
 * @param {string} id
 * @returns {object}
 */
export function removeAppNotificationById(id) {
    return {
        type: REMOVE_APP_NOTIFICATION_BY_ID,
        id,
    };
}

/**
 * Save encrypted app configuration data
 * @param {object} config
 * @returns {object}
 */
export function persistEncryptedAppConfiguration(config) {
    return {
        type: PERSIST_ENCRYPTED_APP_CONFIG,
        config,
    };
}

/**
 * refresh the app in case if there is data incosistancy
 */
export function reloadAppRequest() {
    return {
        type: APP_RELOAD_REQUEST,
    };
}

/**
 * cancel reload
 */
export function reloadAppCanceled() {
    return {
        type: APP_RELOAD_CANCELED,
    };
}

/**
 * reload the app
 * @param {string} current url of the app
 */
export function reloadAppConfirmed() {
    return {
        type: APP_RELOAD_REQUEST_CONFIRMED,
    };
}

export function logout() {
    return {
        type: APP_LOGOUT,
    };
}

export function sessionTimeout() {
    return {
        type: APP_SESSION_TIMEOUT,
    };
}

export function refreshAuthToken() {
    return {
        type: APP_REFRESH_AUTH_TOKEN,
    };
}
