/*
 *
 * EditProjectContainer reducer
 *
 */

import {fromJS, List} from 'immutable';
import {
    ADD_APP_NOTIFICATION,
    ADD_APP_NOTIFICATIONS,
    CLOSE_PAGE_NOTIFICATION,
    CRITICAL_APPLICATION_ERROR,
    DISPLAY_PAGE_NOTIFICATION,
    IS_BOOTSTRAPPING_APPLICATION,
    PERSIST_AUTH_TOKEN,
    PERSIST_ENCRYPTED_APP_CONFIG,
    PERSIST_USER,
    PERSIST_USER_AND_AUTH_TOKEN,
    REMOVE_APP_NOTIFICATION_BY_ID,
    VIEW_CONTAINER_LOADING,
    APP_RELOAD_REQUEST,
    APP_RELOAD_CANCELED,
} from './constants';

const initialState = fromJS({
    appConfig: fromJS({
        encrypted: '',
    }),
    authToken: '',
    criticalFailure: false,
    isBootstrapping: true,
    viewContainerLoading: false,
    appNotifications: fromJS([]), // application level notifications
    pageNotifications: [], // page level notifications e.g. notification pull down in new projects
    user: fromJS({
        roles: [],
    }),
    reloadAppRequest: false,
});

function appReducer(state = initialState, action) {
    switch (action.type) {
        case CRITICAL_APPLICATION_ERROR:
            return state.set('criticalFailure', true);
        case VIEW_CONTAINER_LOADING:
            return state.set('viewContainerLoading', action.loading);
        case DISPLAY_PAGE_NOTIFICATION:
            return state.set('pageNotifications', state.get('pageNotifications').push(action.message));
        case CLOSE_PAGE_NOTIFICATION:
            return state.set('pageNotifications', state.get('pageNotifications').shift());
        case IS_BOOTSTRAPPING_APPLICATION:
            return state.set('isBootstrapping', action.loading);
        case PERSIST_AUTH_TOKEN:
            return state.set('authToken', action.token);
        case PERSIST_ENCRYPTED_APP_CONFIG:
            return state.set('appConfig', fromJS(action.config).set('authToken', state.get('authToken')));
        case PERSIST_USER:
            return state.set('user', fromJS(action.user));
        case PERSIST_USER_AND_AUTH_TOKEN:
            return state.set('user', fromJS(action.user))
                        .set('authToken', action.token);
        case ADD_APP_NOTIFICATION:
            return state.set('appNotifications', state.get('appNotifications').reduce((acc, notice) => {
                if (!notice.id || notice.id !== action.id) return acc.push(fromJS(notice));

                return acc;
            }, new List([
                fromJS({
                    id: action.id,
                    message: action.message,
                    level: action.level,
                }),
            ])));
        case ADD_APP_NOTIFICATIONS:
            return state.set('appNotifications', action.messages.reduceRight((acc, notice) => {
                if (!notice.id || !state.get('appNotifications').find((n) => notice.id === n.id)) return acc.unshift(fromJS(notice));

                return acc;
            }, state.get('appNotifications')));
        case REMOVE_APP_NOTIFICATION_BY_ID:
            return state.set('appNotifications', state.get('appNotifications').filter((n) => n.get('id') !== action.id));
        case APP_RELOAD_REQUEST:
            return state.set('reloadAppRequest', true);
        case APP_RELOAD_CANCELED:
            return state.set('reloadAppRequest', false);
        default:
            return state;
    }
}

export default appReducer;
export {initialState, appReducer};
