/**
 *
     * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, {PureComponent, Children} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import DisplayAppNotifications from '../../components/DisplayAppNotifications';
import PageLoadingOverlay from '../../components/PageLoadingOverlay';
import {autobind} from 'core-decorators';
import {selectAppState} from './selectors';
import {removeAlertFromQueue} from '../AlertsContainer/actions';
import {APP_NAME} from '../../constants/productTerms';
import {
    APP_RELOAD_CONFIRM_TITLE,
    APP_RELOAD_CONFIRMATION_BOX_BODY,
    APP_RELOAD_CONFIRMATION_BOX_TITLE,
} from './constants';
import {themeType} from '../../components/ActionConfirmationModal';
import {
    bootstrapApp,
    reloadAppCanceled,
    reloadAppConfirmed,
} from './actions';
import {Alert} from 'common-frontend-components';
import './styles.scss';

class App extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'App';

    static propTypes = {
        children: PropTypes.node,
    };

    componentWillMount() {
        const {bootstrap} = this.props;
        bootstrap();
    }

    /**
     * relaod application
     */
    @autobind
    _reloadApp() {
        const {reloadAppConfirmed} = this.props;
        reloadAppConfirmed();
    }

    /**
     * hide the reload confirmation box and do not reload
     */
    @autobind
    _reloadAppCanceled() {
        const {reloadAppCanceled} = this.props;
        reloadAppCanceled();
    }

    render() {
        const {criticalFailure} = this.props;
        if (criticalFailure) {
            return (
                <div className="app-failure-container">
                    Application Error Occurred!<br />
                    Please refresh and try again.
                </div>
            );
        }

        const {isBootstrapping} = this.props;
        if (isBootstrapping) {
            return (
                <PageLoadingOverlay
                    label={`Loading ${APP_NAME.label} App...`}
                    show={isBootstrapping}
                    fullscreen={true}
                />
            );
        }

        const {
            alerts,
            removeAlert,
            reloadAppRequest,
        } = this.props;
        return (
            <div>
                {Children.toArray(this.props.children)}

                <DisplayAppNotifications
                    alerts={alerts}
                    removeById={removeAlert}
                />

                <Alert
                    labelOk={APP_RELOAD_CONFIRM_TITLE}
                    onHide={this._reloadApp}
                    show={reloadAppRequest}
                    title={APP_RELOAD_CONFIRMATION_BOX_TITLE}
                    theme={themeType.error}
                >
                    {APP_RELOAD_CONFIRMATION_BOX_BODY}
                </Alert>
            </div>
        );
    }
}

const mapStateToProps = selectAppState();

const mapDisppatchToProps = (dispatch) => {
    return {
        bootstrap: () => dispatch(bootstrapApp()),
        removeAlert: (id) => dispatch(removeAlertFromQueue(id)),
        reloadAppCanceled: () => dispatch(reloadAppCanceled()),
        reloadAppConfirmed: () => dispatch(reloadAppConfirmed()),
    };
};

export default connect(
    mapStateToProps,
    mapDisppatchToProps,
)(App);
