import * as jsont from '../../../helpers/json-transforms';
import {guid} from '../../../utils/designer';

/**
 * returns the rule for new row for respecitve table
 */
export function getNewTableFieldRules() {
    return [
        jsont.pathRule(
            '.', (d) => {
                const field = {
                    Id: guid(),
                    Field: d.match,
                    Value: {
                        Value: null,
                        Field: {
                            Id: '',
                            ParentId: null,
                            Text: '',
                            Confidence: 100,
                            SegmentationType: 0,
                            Type: 0,
                            Bounds: '0, 0, 0, 0',
                            Angle: 0,
                        },
                    },
                    ValidationIssue: null,
                };
                return field;
            }
        ),
    ];
}
/**
 * return function that return array of rule function for tables
 */
export function getTableNewRowRules() {

    return function getRules() {

        const tableRules = [
            jsont.pathRule(
                '.', (d) => {
                    const headers = d.match.Headers;
                    return {Fields: jsont.transform(headers, getNewTableFieldRules())};
                }
            ),
        ];
        return tableRules;
    };
}
