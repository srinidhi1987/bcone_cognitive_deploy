import * as jsont from '../../../helpers/json-transforms';
import {getFormFieldRules} from './fields';
const COLUMNS = 'Columns';

/**
 * return function that return array of rule function for table columns
 */
export function getTableColumnRules() {
    return function getRules() {
        const tableColumnsRule = [
            jsont.pathRule(
                `.${COLUMNS}`, (d) => {
                    // bc of this code in the library:
                    // const match = JSPath.apply(path, json);
                    // const unwrappedMatch = match.length === 1 ? match[0] : match;
                    // const rootMatch = unwrappedMatch === json;

                    //we must protect against when there is only one index in the array
                    const columnArray = Array.isArray(d.match) ? d.match : d.context[COLUMNS];

                    return columnArray.reduce((acc, column) => {
                        return [
                            ...acc,
                            {
                                fieldId: column.Id,
                                name: column.Name,
                                displayName: column.Name.replace(/_/g, ' '),
                                valueType: column.ValueType,
                                formula: column.Formula,
                                validationId: column.ValidationID,
                                isRequired: column.IsRequired,
                                defaultValue: column.DefaultValue,
                                startsWith: column.StartsWith,
                                endsWith: column.EndsWith,
                                formatExpression: column.FormatExpression,
                                isDeleted: column.IsDeleted,
                            },
                        ];
                    }, []);
                }
            ),
        ];
        return tableColumnsRule;
    };
}

/**
 * return function that return array of rule function for table rows
 * @param {string} activeFieldId
 * @param {number} confidenceThreshold
 * @param {object} selectedDesignerTable current table for which it is running rules
 * @param {array} validationData validation data
 * @param {array} originalTablesFields all tables' original fields
 */
export function getTableRowRules(activeFieldId, confidenceThreshold, selectedDesignerTable, validationData, originalTablesFields) {
    return function getRules() {
        const tableRowRules = [
            jsont.pathRule('.', (d) => {
                const {Fields:fields} = d.match;
                const rules = getFormFieldRules(activeFieldId,
                    confidenceThreshold,
                    selectedDesignerTable && selectedDesignerTable.Columns,
                    validationData,
                    fields,
                    originalTablesFields
                );

                return jsont.transform(fields, rules());
            }),
        ];

        return tableRowRules;
    };
}

/**
 * return function that return array of rule function for tables
 * @param {string} activeFieldId
 * @param {number} confidenceThreshold
 * @param {array} designerTables layout table data
 * @param {array} validationData validation data
 * @param {array} originalTablesFields all original tables' all fields (unmodified)
 */
export function getTableRules(activeFieldId, confidenceThreshold, designerTables, validationData, originalTablesFields) {

    return function getTableRules() {

        const tableRules = [
            jsont.pathRule(
                '.', (d) => {
                    const selectedDesignerTable = designerTables && designerTables.find((desingerTable) => desingerTable.TableId === d.match.TableDef.Id);
                    const rowRules = getTableRowRules(activeFieldId, confidenceThreshold, selectedDesignerTable, validationData, originalTablesFields);
                    const columns = jsont.transform(d.match.TableDef, getTableColumnRules()());
                    const rows = jsont.transform(d.match.Rows, rowRules());
                    const showTable = columns ? true : false;
                    const restrictDeleteRow = rows && rows.length === 1 && columns.some((column) => column.isRequired);

                    return {
                        tableGuid: d.match.TableDef.Id,
                        tableName: d.match.TableDef.Name,
                        columns: columns ? columns : [],
                        rows: rows ? rows : [],
                        showTable,
                        restrictDeleteRow,
                    };
                }
            ),
        ];
        return tableRules;
    };
}
