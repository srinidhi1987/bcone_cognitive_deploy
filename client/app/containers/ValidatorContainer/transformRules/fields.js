import * as jsont from '../../../helpers/json-transforms';
import {getFieldMenuItemIcon} from '../../../utils/designer';
import {getValidationMessage, getValidationMessageByValidationIssue, isValueConfident} from '../utils/validator';
/**
 * returns a function which return array of rule function for fields
 * @param {string} activeFieldId
 * @param {number} confidenceThreshold
 * @param {array} designerFields fields info from layout
 * @param {array} validationData validation data
 * @param {array} formulaValidationContextFields fields context for formula validation functionality
 * @param {array} originalFields original fields list (unmodified by user)
 * @returns {function}
 */
export function getFormFieldRules(activeFieldId, confidenceThreshold, designerFields, validationData, formulaValidationContextFields, originalFields) {

    return function getRules() {
        const fieldRules = [
            jsont.pathRule(
                '.', (d) => {
                    const {Id, Field, Value, ValidationIssue} = d.match;
                    //find matching designer field
                    const designerField = designerFields && designerFields.find((field) => field.FieldId === Field.Id);
                    // find matching validation data for current field
                    const validationFieldData = validationData && validationData.find((validation) => validation.ID === Field.Id);
                    // finding fields involved in formula validation for given field
                    const formulaFields = formulaValidationContextFields ? formulaValidationContextFields.filter((field) => Field.Formula.includes(field.Field.Name)) : [];
                    const scope = formulaFields.reduce((acc, field) => {
                        return {
                            ...acc,
                            [field.Field.Name] : field.Value.Field.Text,
                        };
                    }, {});

                    const validationMessage = getValidationMessage(
                        Field.IsRequired,
                        Value.Field.Text,
                        Field.ValueType,
                        Field.StartsWith,
                        Field.EndsWith,
                        Field.FormatExpression,
                        designerField,
                        validationFieldData,
                        {
                            scope,
                            formula: Field.Formula,
                        }
                    );
                    //get old value
                    // find by field guid
                    const selectedOriginalField = originalFields && originalFields.find((originalField) => originalField.Id === Id);
                    const oldValue = selectedOriginalField ? selectedOriginalField.Value.Field.Text : '';
                    //if low confidence and value has not changed, set low confidence message
                    const hasValueChanged = oldValue !== Value.Field.Text;
                    const confidence = Value.Field.Confidence;
                    const isConfident = isValueConfident(confidence, confidenceThreshold, hasValueChanged);
                    const lowConfidenceMessage = isConfident ? null : 'Confidence Below User-Defined Threshold.';
                    const validationMessageByValidationIssue = getValidationMessageByValidationIssue(ValidationIssue);
                    // if validation error then mark field as invalid
                    const isValid = validationMessage ? false : true;
                    // if validation error by validation issue then mark field as invalid
                    const isValidByValidationIssue = !validationMessageByValidationIssue;
                    const popupMessage = validationMessage || lowConfidenceMessage;

                    return {
                        fieldGuid: Id,
                        //Field Properties
                        fieldId: Field.Id,
                        name: Field.Name,
                        valueType: Field.ValueType,
                        formula: Field.Formula,
                        validationId: Field.ValidationID,
                        isRequired : Field.IsRequired,
                        defaultValue: Field.DefaultValue,
                        startsWith: Field.StartsWith,
                        endsWith: Field.EndsWith,
                        formatExpression: Field.FormatExpression,
                        isDeleted: Field.IsDeleted,

                        //Values properties
                        value: Value.Value,
                        parentId: Value.Field.ParentId,
                        text: Value.Field.Text,
                        confidence: Value.Field.Confidence,
                        segmentationType: Value.Field.SegmentationType,
                        type: Value.Field.Type,
                        bounds: Value.Field.Bounds,
                        angle: Value.Field.Angle,
                        validationIssue: ValidationIssue,

                        // derived properties
                        formFieldIcon: getFieldMenuItemIcon(Field.ValueType),
                        displayName: Field.Name.replace(/_/g, ' '),
                        isValid,
                        isConfident,
                        isActive: Id === activeFieldId,
                        validationMessage,
                        lowConfidenceMessage,
                        popupMessage,
                        oldValue,
                        validationMessageByValidationIssue,
                        isValidByValidationIssue,
                    };
                }
            ),
            //jsont.identity,
        ];
        return fieldRules;
    };
}
