import * as jsont from '../../../helpers/json-transforms';
import Rect from '../../../components/Rect';

/**
 * returns the function which return array of rule function for Sirs
 * @param {object} docMeta
 * @param {number} layoutRatio
 * @param {number} currentZoomScale
 */
export function getSirFieldRules(docMeta, layoutRatio, currentZoomScale) {
    return function getRules() {
        const fieldRules = [
            jsont.pathRule(
                '.', (d) => {
                    const bounds = d.match.Bounds.split(', ');
                    // the plus + in front of each value is because this is a highly performant way of converting strings to numbers
                    const x = +bounds[0]; //eslint-disable-line
                    const y = +bounds[1]; //eslint-disable-line
                    const width = +bounds[2]; //eslint-disable-line
                    const height = +bounds[3]; //eslint-disable-line
                    const fill = 'rgba(255,188,3,0.15)';
                    const stroke = 'rgba(255,188,3,1)';
                    const opacity = 1;
                    return {
                        x,
                        y,
                        x2: x + width,
                        y2: y + height,
                        width,
                        height,
                        style: {fill, stroke, opacity},
                        stroke: '#02AFD1',
                        strokeDasharray: 'none',
                        textContent: d.match.Text,
                        isStatic: true,
                        Id: d.match.Id,
                        type: Rect,
                        refHeight: docMeta.Metadata.height,
                        refWidth: docMeta.Metadata.width,
                        layoutRatio,
                        currentZoomScale,
                        windowXOffset:0,
                        windowYOffset:0,
                        windowWidthOffset: 0,
                        windowHeightOffset: 0,
                    };
                }
            ),
        ];
        return fieldRules;
    };
}
