import {fieldValidations} from '../../ProjectForm/validations';
import {
    isFormulaSatisfied,
} from './formulaValidation';
import {InvalidReasons} from '../../../components/MarkAsInvalidModal/constants';
/**
 * returns validation error if any based on given input paramters
 * @param {bool} isRequired if the field is required or optional
 * @param {string} value value of the field
 * @param {number} valueType value type
 * @param {string} startWith start with constrain
 * @param {string} endWith end with constrain
 * @param {string} formatExpression value format expression
 * @param {object} designerField designer field to get data for validation purpose
 * @param {object} validationFieldData validation data
 * @param {object} formulaValidation formula validation data
 * @returns {string} validation error message.
 */
export function getValidationMessage(
    isRequired,
    value,
    valueType,
    startWith,
    endWith,
    formatExpression,
    designerField,
    validationFieldData,
    formulaValidation,
) {
    // First validate emtpy value or empty space value
    const valueEmptyError = fieldValidations.validatorEmptyValueValidation(value);
    if (isRequired && valueEmptyError) return valueEmptyError;
    // List validation
    const listValidationError = fieldValidations.validatorListValidation(value, validationFieldData);
    if (listValidationError) return listValidationError;

    // then check format validation
    let formatError;
    switch (valueType) {
        case 0: // string
            //reg expression
            formatError = fieldValidations.validateByRegex(value, formatExpression);
            break;
        case 1: // date formate validation
            //date formate validation
            formatError = fieldValidations.dateValidation(value, designerField && designerField.FormatExpression);
            break;
        case 3: // number
            formatError = fieldValidations.numberFormat(value);
            //formula validation
            if (!formatError && formulaValidation.formula !== '') {
                const errString = `Formula validation failed. Mismatch in formula '${formulaValidation.formula}'`;
                try {
                    const formulaError = isFormulaSatisfied(formulaValidation) ? null : errString;
                    if (formulaError) return formulaError;
                }
                catch (e) {
                    return 'Formula exception';
                }
            }
            break;
        default:
    }
    if (formatError) return formatError;

    // then check validation error
    let validationError;
    if (startWith)
        validationError = value.startsWith(startWith) ? null : `Pattern validation failed. Value does not start with ${startWith}`;
    if (validationError) return validationError;
    if (endWith)
        validationError = value.endsWith(endWith) ? null : `Pattern validation failed. Value does not end with ${endWith}`;
    if (validationError) return validationError;
    return null;
}

/**
 * isValueConfident
 * @param {number} confidence
 * @param {number} confidenceThreshold
 * @param {boolean} hasValueChanged
 */
export function isValueConfident(confidence, confidenceThreshold, hasValueChanged) {
    if (hasValueChanged) return true;
    return (confidence > 0 && confidenceThreshold > 0) ?
    (confidence > confidenceThreshold ? true : false)
    : true;
}


export function getValidationMessageByValidationIssue(validationIssue) {
    if (!validationIssue) return null;

    const {IssueCode} = validationIssue;
    switch (IssueCode) {
        case 0: return 'Value cannot be empty.';
        case 1: return 'Invalid text format.';
        case 2: return 'Invalid date format.';
        case 3: return 'Invalid number format.';
        case 6:
            return validationIssue.ApplicableStartWith ? `Pattern validation failed. Value doesn't start with ${validationIssue.ApplicableStartWith}.` :
                'Pattern validation failed. Invalid "Starts with" value.';
        case 7:
            return validationIssue.ApplicableEndWith ? `Pattern validation failed. Value doesn't end with ${validationIssue.ApplicableEndWith}.` :
                'Pattern validation failed. Invalid "Ends with" value.';
        case 8:
            return validationIssue.Formula ? `Formula validation failed. Either the formula  ${validationIssue.ApplicableEndWith} or atleast one of its value is invalid.` :
                'Formula validation failed. Invalid formula.';
        case 9:
            return (validationIssue.CalculatedValue && validationIssue.CalculatedValue.HasValue) ? `Formula validation failed. Value doesn't match the formula ${validationIssue.Formula} which equates to ${validationIssue.CalculatedValue}.` :
                `Formula validation failed. Mismatch in formula ${validationIssue.Formula}`;
        case 10:
            return 'List validation failed.';
        case 11:
            return validationIssue.ApplicablePattern ? `Pattern validation failed. Value doesn't match the pattern ${validationIssue.ApplicablePattern}.` :
                'Pattern validation failed.';
        case 12:
            return 'Unable to validate given formula.';
        case 100:
            return 'Confidence Below User-Defined Threshold.';
        default:
            return null;
    }
}

export const getArrayOfTruthyIds = (ids) => {
    return Object.keys(ids).reduce((accumulatedIds, id) => {
        const currentId = ids[id];

        return currentId ? accumulatedIds.concat([{reasonId: id, reasonText: InvalidReasons[id]}]) : accumulatedIds;
    }, []);
};
