export function getFormFieldPathToUpdateValue(fieldIndex, fieldName) {
    return ['formFields', fieldIndex, 'Value', 'Field', fieldName];
}

export function getFormFieldValidationIssueCodePath(fieldIndex) {
    return ['formFields', fieldIndex, 'ValidationIssue'];
}
