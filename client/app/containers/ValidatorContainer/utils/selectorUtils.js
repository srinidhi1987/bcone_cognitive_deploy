import {IQBOT_USER_ROLES} from '../../../constants/enums';
import routeHelper from '../../../utils/routeHelper';
import {isAllowedAccess} from '../../../utils/roleHelpers';

export function getBreadcrumbsUrlForValidator(appState, projectId) {
    const roles = appState.getIn(['user', 'roles']).toJS();

    const isServiceRole = isAllowedAccess([IQBOT_USER_ROLES.SERVICES], roles);
    const liListingUrl = routeHelper(isServiceRole ?
        '/learning-instances' :
        '/learning-instances/validations'
    );
    const liUrl = routeHelper(isServiceRole ?
        `/learning-instances/${projectId}` :
        `/learning-instances/${projectId}/validator`
    );
    const validatorUrl = routeHelper(`/learning-instances/${projectId}/validator`);

    return {liListingUrl, liUrl, validatorUrl};
}
