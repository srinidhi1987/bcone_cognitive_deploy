import {sumFuncion} from './validationConstants';

export function runFormulaValidation(formulaAndVariables) {
    return Function(formulaAndVariables)()();
}
export function isFormulaSatisfied(formulaValidation) {
    let scopeEvaluation = getScopeEvaluation(formulaValidation.scope);
    scopeEvaluation = scopeEvaluation.concat(`return ${formulaValidation.formula};`);

    const fnBody = `${sumFuncion} "use strict"; return (function(){${scopeEvaluation}})`;
    return runFormulaValidation(fnBody);
}

export function getScopeEvaluation(scope) {
    return Object.keys(scope).reduce((acc, key) => {
        return acc.concat(`var ${key}=${scope[key]};`);
    }, '');
}
