export function getTableFieldPathToUpdateValue(tableIndex, rowIndex, fieldIndex, fieldName) {
    return ['updatedTables', tableIndex, 'Rows', rowIndex, 'Fields', fieldIndex, 'Value', 'Field', fieldName];
}

export function getTableFieldValidationIssueCodePath(tableIndex, rowIndex, fieldIndex) {
    return ['updatedTables', tableIndex, 'Rows', rowIndex, 'Fields', fieldIndex, 'ValidationIssue'];
}
