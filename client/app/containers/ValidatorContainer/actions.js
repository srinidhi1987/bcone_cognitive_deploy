/*
 *
 * AnnotatorContainer actions
 *
 */

import {
    CLICK_SIR_FIELD,
    DEFAULT_ACTION,
    SET_IMAGE_LOADED,
    START_DRAG,
    STOP_DRAG,
    UPDATE_DRAWN_AREAS,
} from '../DesignerContainer/constants';

import {
    CLEAR_VALIDATOR_STORE,
    FETCH_DOCUMENT_TO_VALIDATE,
    FETCH_PROJECT_METADATA,
    MARK_FILE_AS_INVALID,
    NO_DOCUMENT_TO_VALIDATE,
    POPULATE_DOCUMENT_DATA,
    POPULATE_PROJECT_METADATA,
    SAVE_CONFIRMATION_BOX_VISIBILITY,
    SAVE_VALIDATED_DOCUMENT,
    SET_ACTIVE_FIELD,
    SET_CURRENT_ZOOM_SCALE,
    SET_FORM_FIELD_VALUE,
    SET_LAYOUT_RATIO,
    SET_TABLE_FIELD_VALUE,
    SET_SCALE_AMOUNT,
    LOADING_SCREEN,
    DELETE_TABLE_ROW,
    INSERT_TABLE_ROW_ABOVE,
    INSERT_TABLE_ROW_BELOW,
    DELETE_TABLE_ROW_REQUEST,
    INSERT_TABLE_ROW_REQUEST,
} from './constants';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION,
    };
}

/**
 * clears the redux store state for the validatorContainer
 *
 * @returns {{type: string}}
 */
export function clearValidatorStore() {
    return {
        type: CLEAR_VALIDATOR_STORE,
    };
}

/**
 * sets active field in designer
 * @param {string} id
 * @returns {{type: string, id: string}}
 */
export function setActiveField(field) {
    return {
        type: SET_ACTIVE_FIELD,
        field,
    };
}

/**
 * update a drawn area
 * @param {object} drawnArea
 * @returns {{drawnArea: object, type: string}}
 */
export function updateDrawnAreas(drawnAreas) {
    return {
        type: UPDATE_DRAWN_AREAS,
        drawnAreas,
    };
}

export function setLayoutRatio(ratio) {
    return {
        type: SET_LAYOUT_RATIO,
        ratio,
    };
}

export function clickSirField(area, activeField, labelSuggestionData, isDrawnBox) {
    return {
        type: CLICK_SIR_FIELD,
        area,
        activeField,
        labelSuggestionData,
        isDrawnBox,
    };
}

export function startDrag(activeField) {
    return {
        type: START_DRAG,
        activeField,
    };
}

export function stopDrag(activeField, activeFieldOption, drawnAreas) {
    return {
        type: STOP_DRAG,
        activeField,
        activeFieldOption,
        drawnAreas,
    };
}

export function setImageLoaded(imageLoaded) {
    return {
        type: SET_IMAGE_LOADED,
        imageLoaded,
    };
}

export function setCurrentZoomScale(currentZoomScale) {
    return {
        type: SET_CURRENT_ZOOM_SCALE,
        currentZoomScale,
    };
}

export function setScaleAmount(scaleAmount) {
    return {
        type: SET_SCALE_AMOUNT,
        scaleAmount,
    };
}

//validator container related action
/**
 *
 * @param {string} projectId
 */
export function fetchDocumentToValidate(projectId) {
    return {
        type: FETCH_DOCUMENT_TO_VALIDATE,
        projectId,
    };
}
/**
 * update validator store
 * @param {object} payload
 */
export function loadDocumentDataToStore(payload) {
    return {
        type: POPULATE_DOCUMENT_DATA,
        payload,
    };
}

/**
 * sets dynamic choice of field value
 * @param {object} activeField
 * @param {string} fieldName
 * @param {*} fieldValue
 * @returns {{activeField: object, fieldName: string, fieldValue: *, type: string}}
 */
export function setFormFieldValue(fieldName, fieldValue, fieldIndex) {
    return {
        type: SET_FORM_FIELD_VALUE,
        fieldName,
        fieldValue,
        fieldIndex,
    };
}

/**
 * sets dynamic choice of field value
 * @param {object} activeField
 * @param {string} fieldName
 * @param {*} fieldValue
 * @returns {{activeField: object, fieldName: string, fieldValue: *, type: string}}
 */
export function setTableFieldValue(fieldName, fieldValue, tableIndex, rowIndex, fieldIndex) {
    return {
        type: SET_TABLE_FIELD_VALUE,
        fieldName,
        fieldValue,
        tableIndex,
        rowIndex,
        fieldIndex,
    };
}

/**
 *
 * @param {boolean} show
 */
export function noDocumentToValidateModalVisibility(show) {
    return {
        type:NO_DOCUMENT_TO_VALIDATE,
        show,
    };
}

/**
 * load project metadata to store
 * @param {object} payload
 */
export function populateProjectMetadata(payload) {
    return {
        type: POPULATE_PROJECT_METADATA,
        payload,
    };
}

/**
 * show or hide loading screen
 * @param {boolean} show
 */
export function setLoadingScreenVisibility(show) {
    return {
        type:LOADING_SCREEN,
        show,
    };
}

/**
 * fetch project metadata
 * @param {string} projectId
 */
export function fetchProjectMetadata(projectId) {
    return {
        type:FETCH_PROJECT_METADATA,
        projectId,
    };
}

/**
 * set save confirmation box visibility
 * @param {boolean} show
 */
export function saveConfirmationBoxVisibility(show) {
    return {
        type: SAVE_CONFIRMATION_BOX_VISIBILITY,
        show,
    };
}

export function saveValidatedDocument(projectId, fileId, fields, tables, fieldAccuracyList) {
    return {
        type: SAVE_VALIDATED_DOCUMENT,
        projectId,
        fileId,
        fields,
        tables,
        fieldAccuracyList,
    };
}

/**
 * mark document as invalid
 * @param {string} projectId
 * @param {string} fileId
 * @param {Object[]} arrayOfReasonIds

 */
export function markDocumentAsInvalid(projectId, fileId, reasons, username) {
    return {
        type: MARK_FILE_AS_INVALID,
        projectId,
        fileId,
        reasons,
        username,
    };
}

/**
 * issue insert row action request
 * @param {number} tableIndex
 * @param {number} rowIndex
 * @param {boolean} above
 */
export function insertRowRequest(tableIndex, rowIndex, above) {
    return {
        type:INSERT_TABLE_ROW_REQUEST,
        tableIndex,
        rowIndex,
        above,
    };
}

/**
 * insert new row above given row
 * @param {number} tableIndex
 * @param {number} rowIndex
 * @param {object} newRow
 */
export function insertRowAbove(tableIndex, rowIndex, newRow) {
    return {
        type:INSERT_TABLE_ROW_ABOVE,
        tableIndex,
        rowIndex,
        newRow,
    };
}

/**
 * insert new row below given row
 * @param {number} tableIndex
 * @param {number} rowIndex
 * @param {object} newRow
 */
export function insertRowBelow(tableIndex, rowIndex, newRow) {
    return {
        type:INSERT_TABLE_ROW_BELOW,
        tableIndex,
        rowIndex,
        newRow,
    };
}

/**
 * delete row request
 * @param {number} tableIndex
 * @param {number} rowIndex
 */
export function deleteRowRequest(tableIndex, rowIndex) {
    return {
        type:DELETE_TABLE_ROW_REQUEST,
        tableIndex,
        rowIndex,
    };
}
/**
 * delete row
 * @param {number} tableIndex
 * @param {number} rowIndex
 */
export function deleteRow(tableIndex, rowIndex) {
    return {
        type:DELETE_TABLE_ROW,
        tableIndex,
        rowIndex,
    };
}
