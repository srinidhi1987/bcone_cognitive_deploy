/**
 * ProjectsContainer sagas
 */
jest.mock('../../../utils/logger');
import {call, put} from 'redux-saga/effects';
import {
    fetchDocumentToValidateFlow,
    fetchProjectMetadataFlow,
    markDocumentAsInvalidFlow,
    saveValidatedDocumentFlow,
    addTableRowFlow,
    deleteTableRowFlow,
} from '../sagas';
import {
    clearValidatorStore,
    fetchDocumentToValidate,
    fetchProjectMetadata,
    loadDocumentDataToStore,
    markDocumentAsInvalid,
    noDocumentToValidateModalVisibility,
    populateProjectMetadata,
    saveValidatedDocument,
    setLoadingScreenVisibility,
    insertRowRequest,
    insertRowAbove,
    insertRowBelow,
    deleteRow,
    deleteRowRequest,
} from '../actions';
import {getArrayOfTruthyIds} from '../utils/validator';
import {displayAlert} from '../../AlertsContainer/actions';
import {ALERT_LEVEL_DANGER} from '../../AlertsContainer/constants';
import {validatorAPI} from '../../../resources/validatorBackEnd';
import {projectsBackEnd} from '../../../resources/projectsBackEnd';
import {project1MetaData} from '../../../../fixtures/projects';
import {validatorData} from '../../../../fixtures/validator';
import {fromJS} from 'immutable';

const jsonParseSpy = jest.spyOn(JSON, 'parse');

describe('validatorContainer sagas', () => {
    afterEach(() => {
        jest.clearAllMocks();
        jsonParseSpy.mockReset();
    });
    const projectId = 'project-id';
    const fileId = 'file-guid';
    const fields = [];
    const tables = [];
    const fieldAccuracyList = [];
    describe('fetchProjectMetadataFlow generator', () => {
        describe('successful fetch', () => {
            const generator = fetchProjectMetadataFlow(fetchProjectMetadata(projectId));

            it('should make a call to project Metadata api', () => {
                const result = generator.next().value;

                expect(result).toEqual(call(projectsBackEnd.getMetadata, projectId));
            });

            it('should populate project metadata to store', () => {
                const projectMetadataResult = {
                    success: true,
                    data: project1MetaData,
                    errors: [],
                };
                const result = generator.next(projectMetadataResult).value;

                expect(result).toEqual(put(populateProjectMetadata(projectMetadataResult.data)));
            });
        });
    });

    describe('fetchDocumentToValidateFlow generator', () => {

        describe('successful fetch', () => {
            const generator = fetchDocumentToValidateFlow(fetchDocumentToValidate(projectId));

            it('should show loading screen', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(setLoadingScreenVisibility(true)));
            });

            it('should make a call to get validation data for the document to validate', () => {
                const result = generator.next().value;
                expect(result).toEqual(call(validatorAPI.getdocumentToValidate, projectId));
            });


            it('should populate document validation data to store', () => {
                const validatorDocument = {
                    data: {
                        vBotDocument: validatorData,
                    },
                    success: true,
                    errors: [],
                };
                const result = generator.next(validatorDocument).value;
                expect(result).toEqual(put(loadDocumentDataToStore(validatorDocument.data)));
            });

            it('should hide loading screen', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(setLoadingScreenVisibility(false)));
            });
        });

        describe('fail path', () => {
            const generator = fetchDocumentToValidateFlow(fetchDocumentToValidate(projectId));

            it('should show loading screen', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(setLoadingScreenVisibility(true)));
            });

            it('should make a call to get validation data for the document to validate', () => {
                const result = generator.next().value;
                expect(result).toEqual(call(validatorAPI.getdocumentToValidate, projectId));
            });


            it('on fail to load document, it should clear validator store', () => {
                const validatorDocument = {
                    data: {},
                    success: false,
                    errors: [],
                };
                const result = generator.next(validatorDocument).value;
                expect(result).toEqual(put(clearValidatorStore()));
            });

            it('should hide loading screen', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(setLoadingScreenVisibility(false)));
            });

            it('should display no document to validate message', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(noDocumentToValidateModalVisibility(true)));
            });
        });
    });

    describe('saveValidatedDocumentFlow generator', () => {

        describe('successful path', () => {
            const generator = saveValidatedDocumentFlow(saveValidatedDocument(projectId, fileId, fields, tables, fieldAccuracyList));

            it('should show loading screen', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(setLoadingScreenVisibility(true)));
            });

            it('should save validator document data', () => {
                const result = generator.next().value;
                expect(result).toEqual(call(validatorAPI.saveValidatedDocument, projectId, fileId, fields, tables, fieldAccuracyList));
            });


            it('should move to next document', () => {
                const validatorDocumentSaveResult = {
                    data: {},
                    success: true,
                    errors: [],
                };
                const result = generator.next(validatorDocumentSaveResult).value;
                expect(result).toEqual(put(fetchDocumentToValidate(projectId)));
            });
        });

        describe('fail path', () => {
            const generator = saveValidatedDocumentFlow(saveValidatedDocument(projectId, fileId, fields, tables, fieldAccuracyList));

            it('should show loading screen', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(setLoadingScreenVisibility(true)));
            });

            it('should save validator document data', () => {
                const result = generator.next().value;
                expect(result).toEqual(call(validatorAPI.saveValidatedDocument, projectId, fileId, fields, tables, fieldAccuracyList));
            });


            it('on fail to save, it should hide loading screen', () => {
                const validatorDocumentSaveResult = {
                    data: {},
                    success: false,
                    errors: [],
                };
                const result = generator.next(validatorDocumentSaveResult).value;
                expect(result).toEqual(put(setLoadingScreenVisibility(false)));
            });

            it('should display alert for save error', () => {
                const result = generator.next().value;
                expect(result).toEqual(put(displayAlert('Error while saving document.', ALERT_LEVEL_DANGER)));
            });
        });
    });

    describe('addTableRowFlow: generator', () => {

        describe('insert row above', () => {
            const generator = addTableRowFlow(insertRowRequest(0, 1, true));
            generator.next();

            it('should insert row above', () => {
                const result = generator.next([{}]);
                expect(result.value).toEqual(put(insertRowAbove(0, 1, fromJS({}))));
            });
        });

        describe('insert row below', () => {
            const generator = addTableRowFlow(insertRowRequest(0, 1, false));
            generator.next();

            it('should insert row below', () => {
                const result = generator.next([{}]);
                expect(result.value).toEqual(put(insertRowBelow(0, 1, fromJS({}))));
            });
        });
    });

    describe('deleteTableRowFlow: generator', () => {

        describe('there are more than 1 row in table', () => {
            const generator = deleteTableRowFlow(deleteRowRequest(0, 1));

            it('should delete row if there are more than 1 rows', () => {
                const result = generator.next([{
                    rows:[
                        [{}],
                        [{}],
                    ],
                }]);
                expect(result.value).toEqual(put(deleteRow(0, 1)));
            });
        });
    });
});

describe('markDocumentAsInvalidFlow: generator', () => {
    // given
    const reasons = getArrayOfTruthyIds({1: true, 2: false, 3: true});
    const action = markDocumentAsInvalid(1, 2, reasons, 'andrew');
    const generator = markDocumentAsInvalidFlow(action);
    const projectId = 1;
    const fileId = 2;

    it('should post the reasons to the backend with the id and reason text', () => {
        // when
        const result = generator.next();

        // then
        expect(result.value).toEqual(call(validatorAPI.markAsInvalid, projectId, fileId, reasons, 'andrew'));
    });

    it('should go try to navigate to next file if it is a success', () => {
        const result = generator.next({success: true});

        expect(result.value).toEqual(put(fetchDocumentToValidate(projectId)));
    });
});
