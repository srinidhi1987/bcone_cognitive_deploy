import {fromJS} from 'immutable';

import selectValidatorContainer, {selectOriginalTableFields} from '../selectors';
import {validatorData} from '../../../../fixtures/validator';
import * as jsont from '../../../helpers/json-transforms';
import {getFormFieldRules} from '../transformRules/fields';
import {getTableRules} from '../transformRules/tables';
import {getSirFieldRules} from '../transformRules/sirFields';
import {IQBOT_USER_ROLES} from '../../../constants/enums';

describe('ValidatorContainer selector', () => {
    const activeFieldId = '021b8572-d8f3-44e4-9388-601a5bd68e1b';
    let mockedState, selector, appConfig;
    const metaData = {
        Metadata: {
            height: 300,
            width: 300,
        },
    };
    beforeEach(() => {
        // given
        appConfig = {
            encrypted: '43324234324',
            designer: true,
            validator: true,
        };
        mockedState = fromJS({
            appContainer: {
                appConfig,
                authToken: 'test-token',
                user: {
                    roles: [IQBOT_USER_ROLES.SERVICES],
                },
            },
            validatorContainer: {
                sirFields: fromJS(validatorData.SirFields.Fields),
                documentHeight: 300,
                documentWidth: 300,
                validationData: fromJS([]),
                originalFormFields: fromJS(validatorData.Data.FieldDataRecord.Fields),
                designerFields: fromJS(validatorData.Layout.Fields),
                designerTables: fromJS(validatorData.Layout.Tables),
                formFields: fromJS(validatorData.Data.FieldDataRecord.Fields),
                originaltables:fromJS(validatorData.Data.TableDataRecord),
                updatedTables:fromJS(validatorData.Data.TableDataRecord),
                layoutRatio: 1,
                currentZoomScale: 1,
                projectName: 'xyz',
                confidenceThreshold: 0,
                activeFieldId,
            },
        });
        // when
        selector = selectValidatorContainer();
    });

    it('should return encrypted app configuration', () => {
        // then
        expect(selector(mockedState).appConfig.encrypted).toEqual(appConfig.encrypted);
        expect(selector(mockedState).appConfig.validator).toEqual(true);
    });

    it('shold return transformed form fields as per the active field', () => {
        const transformedFields = jsont.transform(validatorData.Data.FieldDataRecord.Fields, getFormFieldRules(activeFieldId, 0, validatorData.Layout.Fields, [], validatorData.Data.FieldDataRecord.Fields, validatorData.Data.FieldDataRecord.Fields)());
        expect(selector(mockedState).fields).toEqual(transformedFields);
    });

    it('should return transformed tables as per the active field', () => {
        const allTableFields = selectOriginalTableFields()(mockedState);
        const transformedtables = jsont.transform(validatorData.Data.TableDataRecord, getTableRules(activeFieldId, 0, validatorData.Layout.Tables, [], allTableFields)());
        expect(selector(mockedState).tables).toEqual(transformedtables);
    });

    it('should return metaData', () => {
        expect(selector(mockedState).metaData).toEqual(metaData);
    });

    it('should select active static annotation if available', () => {
        const selectedSirField = {
            'Id':'6da9a35c-023b-460a-b770-20ab71cae227',
            'ParentId':'25438dbc-6184-40d7-9ea6-f305fff5b07a',
            'Text':'#: 100.1',
            'Confidence':95,
            'SegmentationType':0,
            'Type':0,
            'Bounds':'466, 640, 142, 30',
            'Angle':0.0,
        };
        const getRules = getSirFieldRules(metaData, 1, 1);
        const transformedSirField = jsont.transform(selectedSirField, getRules());
        expect(selector(mockedState).staticAnnotations).toEqual([transformedSirField]);
    });

    it('should return fieldAccuracyList to empty array if there is validation error in any of the field', () => {
        expect(selector(mockedState).fieldAccuracyList).toEqual([]);
    });

    it('should return allValidated to false if there is a validation error in any of the field', () => {
        expect(selector(mockedState).allValidated).toEqual(false);
    });
});
