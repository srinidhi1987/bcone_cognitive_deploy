import validatorContainerReducer, {initialState} from '../reducer';
import {fromJS} from 'immutable';
import * as jsont from '../../../helpers/json-transforms';
import {

} from '../constants';
import {
    clearValidatorStore,
    populateProjectMetadata,
    loadDocumentDataToStore,
    noDocumentToValidateModalVisibility,
    saveConfirmationBoxVisibility,
    setActiveField,
    setCurrentZoomScale,
    setLayoutRatio,
    setScaleAmount,
    setFormFieldValue,
    setTableFieldValue,
    insertRowAbove,
    insertRowBelow,
    deleteRow,
} from '../actions';
import {validatorData} from '../../../../fixtures/validator';
import {project1MetaData} from '../../../../fixtures/projects';
import {getTableFieldPathToUpdateValue} from '../utils/table';
import {getFormFieldPathToUpdateValue} from '../utils/formField';
import {getTableNewRowRules} from '../transformRules/tableRow';

describe('validatorContainerReducer', () => {

    it('returns the initial state', () => {
        expect(validatorContainerReducer(undefined, {})).toEqual(initialState);
    });

    describe('CLEAR_VALIDATOR_STORE action', () => {
        it('should set state to initialState', () => {
            // when
            const result = validatorContainerReducer(initialState,
                clearValidatorStore()
            );
            // then
            expect(result).toEqual(initialState);
        });
    });

    describe('POPULATE_PROJECT_METADATA action', () => {
        it('should populate metadata of project to store', () => {
            const result = validatorContainerReducer(initialState, populateProjectMetadata(project1MetaData));
            expect(result.get('projectName')).toEqual(project1MetaData.name);
            expect(result.get('confidenceThreshold')).toEqual(project1MetaData.confidenceThreshold);
        });
    });

    describe('POPULATE_DOCUMENT_DATA action', () => {
        it('should populate visionbot document data to store', () => {
            const payload = {
                vBotDocument: validatorData,
            };
            const result = validatorContainerReducer(initialState, loadDocumentDataToStore(payload));

            expect(result.get('sirFields')).toEqual(fromJS(validatorData.SirFields.Fields));
            expect(result.get('documentHeight')).toEqual(fromJS(validatorData.DocumentProperties.Height));
            expect(result.get('documentWidth')).toEqual(fromJS(validatorData.DocumentProperties.Width));
            expect(result.get('originalFormFields')).toEqual(fromJS(validatorData.Data.FieldDataRecord.Fields));
            expect(result.get('formFields')).toEqual(fromJS(validatorData.Data.FieldDataRecord.Fields));
            expect(result.get('originaltables')).toEqual(fromJS(validatorData.Data.TableDataRecord));
            expect(result.get('updatedTables')).toEqual(fromJS(validatorData.Data.TableDataRecord));
        });
    });

    describe('NO_DOCUMENT_TO_VALIDATE action', () => {
        it('should show no document to validate modal', () => {
            const result = validatorContainerReducer(initialState, noDocumentToValidateModalVisibility(true));
            expect(result.get('noDocumentToValidate')).toEqual(true);
        });
    });

    describe('SET_CURRENT_ZOOM_SCALE action', () => {
        it('should set current zoom scale to store', () => {
            const result = validatorContainerReducer(initialState, setCurrentZoomScale(1.5));
            expect(result.get('currentZoomScale')).toEqual(1.5);
        });
    });

    describe('SET_SCALE_AMOUNT action', () => {
        it('should set scale amount to store', () => {
            const result = validatorContainerReducer(initialState, setScaleAmount(0.25));
            expect(result.get('scaleAmount')).toEqual(0.25);
        });
    });

    describe('SET_LAYOUT_RATIO action', () => {
        it('should set layout ratio to store', () => {
            const result = validatorContainerReducer(initialState, setLayoutRatio(1.5));
            expect(result.get('layoutRatio')).toEqual(1.5);
        });
    });

    describe('SET_ACTIVE_FIELD action', () => {
        it('should set active field id to store', () => {
            const activeField = {
                fieldGuid:'active-field-guid',
            };
            const result = validatorContainerReducer(initialState, setActiveField(activeField));
            expect(result.get('activeFieldId')).toEqual(activeField.fieldGuid);
        });
    });
    describe('set values', () => {
        const payload = {
            vBotDocument: validatorData,
        };

        //loading the state before setting values
        const result = validatorContainerReducer(initialState, loadDocumentDataToStore(payload));
        const newValue = 'New Value';
        describe('SET_FORM_FIELD_VALUE action', () => {
            it('should update field value for given field', () => {
                const setFieldResult = validatorContainerReducer(result, setFormFieldValue('Text', newValue, 0));
                expect(setFieldResult.getIn(getFormFieldPathToUpdateValue(0, 'Text'))).toEqual(newValue);
            });
        });

        describe('SET_TABLE_FIELD_VALUE action', () => {
            it('should update table field value for given field', () => {
                const setTableFieldResult = validatorContainerReducer(result, setTableFieldValue('Text', newValue, 0, 0, 0));
                expect(setTableFieldResult.getIn(getTableFieldPathToUpdateValue(0, 0, 0, 'Text'))).toEqual(newValue);
            });
        });
    });

    describe('SAVE_CONFIRMATION_BOX_VISIBILITY action', () => {
        it('should set showSaveConfirmationModal to true', () => {
            const result = validatorContainerReducer(initialState, saveConfirmationBoxVisibility(true));
            expect(result.get('showSaveConfirmationModal')).toEqual(true);
        });

        it('should set showSaveConfirmationModal to false', () => {
            const result = validatorContainerReducer(initialState, saveConfirmationBoxVisibility(false));
            expect(result.get('showSaveConfirmationModal')).toEqual(false);
        });
    });

    describe('edit tables row', () => {
        const payload = {
            vBotDocument: validatorData,
        };

        //loading the state before setting values
        const result = validatorContainerReducer(initialState, loadDocumentDataToStore(payload));
        const tableIndex = 0;
        const newRows = jsont.transform(validatorData.Data.TableDataRecord, getTableNewRowRules()());
        describe('INSERT_TABLE_ROW_ABOVE action', () => {
            it('should insert empty row above given row in respective table', () => {
                const rowIndex = 2;
                const lengthBeforeInsert = result.getIn(['updatedTables', tableIndex, 'Rows']).size;
                const insertResult = validatorContainerReducer(result, insertRowAbove(tableIndex, rowIndex, fromJS(newRows[tableIndex])));
                expect(insertResult.getIn(['updatedTables', tableIndex, 'Rows']).size).toEqual(lengthBeforeInsert + 1);
                expect(insertResult.getIn(getTableFieldPathToUpdateValue(tableIndex, rowIndex, 0, 'Text'))).toEqual('');

            });
        });

        describe('INSERT_TABLE_ROW_BELOW action', () => {
            it('should insert empty row below given row in respective table', () => {
                const rowIndex = 2;
                const lengthBeforeInsert = result.getIn(['updatedTables', tableIndex, 'Rows']).size;
                const insertResult = validatorContainerReducer(result, insertRowBelow(tableIndex, rowIndex, fromJS(newRows[tableIndex])));
                expect(insertResult.getIn(['updatedTables', tableIndex, 'Rows']).size).toEqual(lengthBeforeInsert + 1);
                expect(insertResult.getIn(getTableFieldPathToUpdateValue(tableIndex, rowIndex + 1, 0, 'Text'))).toEqual('');
            });
        });

        describe('DELETE_TABLE_ROW action', () => {
            it('should delete given row in respective table', () => {
                const rowIndex = 2;
                const lengthBeforeInsert = result.getIn(['updatedTables', tableIndex, 'Rows']).size;
                const insertResult = validatorContainerReducer(result, deleteRow(tableIndex, rowIndex));
                expect(insertResult.getIn(['updatedTables', tableIndex, 'Rows']).size).toEqual(lengthBeforeInsert - 1);
            });
        });
    });
});
