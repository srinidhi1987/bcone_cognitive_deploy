import {
    fetchDocumentToValidate,
    fetchProjectMetadata,
    loadDocumentDataToStore,
    setFormFieldValue,
    setTableFieldValue,
    noDocumentToValidateModalVisibility,
    populateProjectMetadata,
    saveConfirmationBoxVisibility,
    setScaleAmount,
    setCurrentZoomScale,
    setLayoutRatio,
    setActiveField,
    clearValidatorStore,
    saveValidatedDocument,
    setLoadingScreenVisibility,
    insertRowRequest,
    insertRowAbove,
    insertRowBelow,
    deleteRowRequest,
    deleteRow,
} from '../actions';
import {
    CLEAR_VALIDATOR_STORE,
    SET_ACTIVE_FIELD,
    SET_LAYOUT_RATIO,
    SET_CURRENT_ZOOM_SCALE,
    SET_SCALE_AMOUNT,
    NO_DOCUMENT_TO_VALIDATE,
    FETCH_DOCUMENT_TO_VALIDATE,
    POPULATE_PROJECT_METADATA,
    POPULATE_DOCUMENT_DATA,
    SAVE_CONFIRMATION_BOX_VISIBILITY,
    SAVE_VALIDATED_DOCUMENT,
    SET_FORM_FIELD_VALUE,
    SET_TABLE_FIELD_VALUE,
    LOADING_SCREEN,
    FETCH_PROJECT_METADATA,
    INSERT_TABLE_ROW_REQUEST,
    INSERT_TABLE_ROW_ABOVE,
    INSERT_TABLE_ROW_BELOW,
    DELETE_TABLE_ROW_REQUEST,
    DELETE_TABLE_ROW,
} from '../constants';
import {project1MetaData} from '../../../../fixtures/projects';
import {validatorData} from '../../../../fixtures/validator';

describe('validatorContainer actions', () => {
    describe('clearValidatorStore action creator', () => {
        it('has a type of CLEAR_VALIDATOR_STORE', () => {
            const expected = {
                type: CLEAR_VALIDATOR_STORE,
            };

            expect(clearValidatorStore()).toEqual(expected);
        });
    });

    describe('setActiveField action creator', () => {
        it('should have a type of SET_ACTIVE_FIELD', () => {
            // given
            const expected = {
                type: SET_ACTIVE_FIELD,
            };
            const field = {
                fieldGuid: 'field-guid',
            };
            // when
            const result = setActiveField(field);
            // then
            expect(result).toMatchObject(expected);
        });
    });

    describe('setLayoutRatio action creator', () => {
        it('should have a type of SET_LAYOUT_RATIO', () => {
            // given
            const expected = {
                type: SET_LAYOUT_RATIO,
                ratio: 1.5,
            };
            // when
            const result = setLayoutRatio(1.5);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('setCurrentZoomScale action creator', () => {
        it('should have a type of SET_CURRENT_ZOOM_SCALE', () => {
            // given
            const expected = {
                type: SET_CURRENT_ZOOM_SCALE,
                currentZoomScale: 1.5,
            };
            // when
            const result = setCurrentZoomScale(1.5);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('setScaleAmount action creator', () => {
        it('should have a type of SET_SCALE_AMOUNT', () => {
            // given
            const expected = {
                type: SET_SCALE_AMOUNT,
                scaleAmount: 0.25,
            };
            // when
            const result = setScaleAmount(0.25);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('noDocumentToValidateModalVisibility action', () => {
        it('should have type: NO_DOCUMENT_TO_VALIDATE', () => {
            // given
            const expected = {
                type: NO_DOCUMENT_TO_VALIDATE,
                show: true,
            };
            // when
            const result = noDocumentToValidateModalVisibility(true);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('fetchDocumentToValidate action creator', () => {
        it('should have type: FETCH_DOCUMENT_TO_VALIDATE', () => {
            const expected = {
                type: FETCH_DOCUMENT_TO_VALIDATE,
                projectId: 'project-id',
            };
            // when
            const result = fetchDocumentToValidate('project-id');
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('populateProjectMetadata action creator', () => {
        it('should have type: POPULATE_PROJECT_METADATA', () => {
            // given
            const expected = {
                type: POPULATE_PROJECT_METADATA,
                payload: project1MetaData,
            };
            // when
            const result = populateProjectMetadata(project1MetaData);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('loadDocumentDataToStore action creator', () => {
        it('should have type: POPULATE_DOCUMENT_DATA', () => {
            // given
            const expected = {
                type: POPULATE_DOCUMENT_DATA,
                payload: {
                    vBotDocument: validatorData,
                },
            };
            // when
            const result = loadDocumentDataToStore({vBotDocument: validatorData});
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('setFormFieldValue action creator', () => {
        it('should have type: SET_FORM_FIELD_VALUE', () => {
            // given
            const fieldName = 'name',
                fieldValue = 'value',
                fieldIndex = 0;
            const expected = {
                type: SET_FORM_FIELD_VALUE,
                fieldName,
                fieldValue,
                fieldIndex,
            };
            // when
            const result = setFormFieldValue(fieldName, fieldValue, fieldIndex);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('setTableFieldValue action creator', () => {
        it('should have type: SET_TABLE_FIELD_VALUE', () => {
            // given
            const fieldName = 'name',
                fieldValue = 'value',
                tableIndex = 0,
                rowIndex = 1,
                fieldIndex = 0;
            const expected = {
                type: SET_TABLE_FIELD_VALUE,
                fieldName,
                fieldValue,
                tableIndex,
                rowIndex,
                fieldIndex,
            };
            // when
            const result = setTableFieldValue(fieldName, fieldValue, tableIndex, rowIndex, fieldIndex);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('setLoadingScreenVisibility action creator', () => {
        it('should have type: LOADING_SCREEN', () => {
            const expected = {
                type: LOADING_SCREEN,
                show: true,
            };
            // when
            const result = setLoadingScreenVisibility(true);
            // then
            expect(result).toEqual(expected);
            // when
            const hideResult = setLoadingScreenVisibility(false);
            // then
            expect(hideResult).toEqual({...expected, show: false});
        });
    });

    describe('fetchProjectMetadata action creator', () => {
        it('should have type: FETCH_PROJECT_METADATA', () => {
            const expected = {
                type: FETCH_PROJECT_METADATA,
                projectId:'project-id',
            };
            // when
            const result = fetchProjectMetadata('project-id');
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('saveConfirmationBoxVisibility action creator', () => {
        it('should have type: SAVE_CONFIRMATION_BOX_VISIBILITY', () => {
            const expected = {
                type: SAVE_CONFIRMATION_BOX_VISIBILITY,
                show: true,
            };
            // when
            const result = saveConfirmationBoxVisibility(true);
            // then
            expect(result).toEqual(expected);
            // when
            const falseResult = saveConfirmationBoxVisibility(false);
            // then
            expect(falseResult).toEqual({...expected, show: false});
        });
    });

    describe('saveValidatedDocument action creator', () => {
        it('should have type: SAVE_VALIDATED_DOCUMENT', () => {
            const payload = {
                projectId: '1',
                fileId: '2',
                fields: [],
                tables: [],
                fieldAccuracyList:[],
            };
            const expected = {
                type: SAVE_VALIDATED_DOCUMENT,
                ...payload,
            };
            // when
            const result = saveValidatedDocument(
                payload.projectId,
                payload.fileId,
                payload.fields,
                payload.tables,
                payload.fieldAccuracyList,
            );
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('insertRowRequest action creator', () => {
        it('should have type: INSERT_TABLE_ROW_REQUEST', () => {
            const payload = {
                tableIndex : 0,
                rowIndex: 2,
                above: true,
            };
            const expected = {
                type: INSERT_TABLE_ROW_REQUEST,
                ...payload,
            };
            // when
            const result = insertRowRequest(
                payload.tableIndex,
                payload.rowIndex,
                payload.above,
            );
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('insertRowAbove action creator', () => {
        it('should have type: INSERT_TABLE_ROW_ABOVE', () => {
            const payload = {
                tableIndex : 0,
                rowIndex: 2,
                newRow: [],
            };
            const expected = {
                type: INSERT_TABLE_ROW_ABOVE,
                ...payload,
            };
            // when
            const result = insertRowAbove(
                payload.tableIndex,
                payload.rowIndex,
                payload.newRow,
            );
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('insertRowBelow action creator', () => {
        it('should have type: INSERT_TABLE_ROW_BELOW', () => {
            const payload = {
                tableIndex : 0,
                rowIndex: 2,
                newRow: [],
            };
            const expected = {
                type: INSERT_TABLE_ROW_BELOW,
                ...payload,
            };
            // when
            const result = insertRowBelow(
                payload.tableIndex,
                payload.rowIndex,
                payload.newRow,
            );
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('deleteRowRequest action creator', () => {
        it('should have type: DELETE_TABLE_ROW_REQUEST', () => {
            const payload = {
                tableIndex : 0,
                rowIndex: 2,
            };
            const expected = {
                type: DELETE_TABLE_ROW_REQUEST,
                ...payload,
            };
            // when
            const result = deleteRowRequest(
                payload.tableIndex,
                payload.rowIndex,
            );
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('deleteRow action creator', () => {
        it('should have type: DELETE_TABLE_ROW', () => {
            const payload = {
                tableIndex : 0,
                rowIndex: 2,
            };
            const expected = {
                type: DELETE_TABLE_ROW,
                ...payload,
            };
            // when
            const result = deleteRow(
                payload.tableIndex,
                payload.rowIndex,
            );
            // then
            expect(result).toEqual(expected);
        });
    });
});
