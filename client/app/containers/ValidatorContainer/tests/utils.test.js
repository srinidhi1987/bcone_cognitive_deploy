import {getArrayOfTruthyIds} from '../utils/validator';

// given
describe('getArrayOfTruthyIds: utility function', () => {
    it('should return an array of ids that are truthy', () => {
        // when
        const truthyIds = getArrayOfTruthyIds({1: false, 2: true, 3: true});

        // then
        expect(truthyIds).toEqual([
            {reasonId: '2', reasonText: 'Tables missing'},
            {reasonId: '3', reasonText: 'Wrong values'},
        ]);
    });
});
