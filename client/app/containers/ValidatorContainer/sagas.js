import {call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {
    clearValidatorStore,
    fetchDocumentToValidate,
    loadDocumentDataToStore,
    noDocumentToValidateModalVisibility,
    populateProjectMetadata,
    setLoadingScreenVisibility,
    deleteRow,
    insertRowAbove,
    insertRowBelow,
} from './actions';
import {
    FETCH_DOCUMENT_TO_VALIDATE,
    FETCH_PROJECT_METADATA,
    MARK_FILE_AS_INVALID,
    SAVE_VALIDATED_DOCUMENT,
    DELETE_TABLE_ROW_REQUEST,
    INSERT_TABLE_ROW_REQUEST,
} from './constants';
import {validatorAPI} from '../../resources/validatorBackEnd';
import {projectsBackEnd} from '../../resources/projectsBackEnd';
import {displayAlert} from '../AlertsContainer/actions';
import {ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import {selectNewRows} from './selectors';
import {fromJS} from 'immutable';
// Individual exports for testing
export function* defaultSaga() { // eslint-disable-line require-yield
    return;
}

export function* fetchProjectMetadataFlow(action) {
    //show loading
    const projectResult = yield call(projectsBackEnd.getMetadata, action.projectId);
    if (!projectResult.success) return;
    yield put(populateProjectMetadata(projectResult.data));
}

export function* fetchDocumentToValidateFlow(action) {
    //show loading
    yield put(setLoadingScreenVisibility(true));
    //fetch document from validator service
    const result = yield call(validatorAPI.getdocumentToValidate, action.projectId);

    if (result.success) {
        yield put(loadDocumentDataToStore(result.data));
        yield put(setLoadingScreenVisibility(false));
    }
    else {
        yield put(clearValidatorStore());
        yield put(setLoadingScreenVisibility(false));
        yield put(noDocumentToValidateModalVisibility(true));
    }
}

export function* saveValidatedDocumentFlow(action) {
    const {
        projectId,
        fileId,
        fields,
        tables,
        fieldAccuracyList,
    } = action;
    //show loading
    yield put(setLoadingScreenVisibility(true));
    //save validated document
    const result = yield call(validatorAPI.saveValidatedDocument,
        projectId,
        fileId,
        fields,
        tables,
        fieldAccuracyList,
    );
    // fail flow
    if (!result.success) {
        yield put(setLoadingScreenVisibility(false));
        yield put(displayAlert(result.errors[0] || 'Error while saving document.', ALERT_LEVEL_DANGER));
        return;
    }
    //success flow
    //move to next document on successful save
    yield put(fetchDocumentToValidate(projectId));
}

export function* markDocumentAsInvalidFlow({reasons, fileId, projectId, username}) {
    const result = yield call(validatorAPI.markAsInvalid, projectId, fileId, reasons, username);

    if (result.success) {
        yield put(fetchDocumentToValidate(projectId));
    }
}

export function* addTableRowFlow({tableIndex, rowIndex, above}) {
    const newRows = yield select(selectNewRows());
    const newRow = newRows[tableIndex];
    yield above ? put(insertRowAbove(tableIndex, rowIndex, fromJS(newRow))) : put(insertRowBelow(tableIndex, rowIndex, fromJS(newRow)));
}

export function* deleteTableRowFlow({tableIndex, rowIndex}) {
    yield put(deleteRow(tableIndex, rowIndex));
}

// All sagas to be loaded
export default [
    defaultSaga,
    //validator sagas
    function* () {
        yield takeLatest(FETCH_PROJECT_METADATA, fetchProjectMetadataFlow);
    },
    function* () {
        yield takeLatest(FETCH_DOCUMENT_TO_VALIDATE, fetchDocumentToValidateFlow);
    },
    function* () {
        yield takeLatest(SAVE_VALIDATED_DOCUMENT, saveValidatedDocumentFlow);
    },
    function* () {
        yield takeLatest(MARK_FILE_AS_INVALID, markDocumentAsInvalidFlow);
    },
    function* () {
        yield takeLatest(INSERT_TABLE_ROW_REQUEST, addTableRowFlow);
    },
    function* () {
        yield takeLatest(DELETE_TABLE_ROW_REQUEST, deleteTableRowFlow);
    },
];
