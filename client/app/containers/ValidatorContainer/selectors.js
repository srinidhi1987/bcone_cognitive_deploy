import {createSelector} from 'reselect';

import * as jsont from '../../helpers/json-transforms';
import {getFormFieldRules} from './transformRules/fields';
import {getTableRules} from './transformRules/tables';
import {getSirFieldRules} from './transformRules/sirFields';
import {getBreadcrumbsUrlForValidator} from './utils/selectorUtils';
import {IQBOT_USER_ROLES} from '../../constants/enums';
import {getTableNewRowRules} from './transformRules/tableRow';
import routeHelper from '../../utils/routeHelper';
import {isAllowedAccess} from '../../utils/roleHelpers';
/**
 * Direct selector to the annotatorContainer state domain
 */
const selectValidatorContainerDomain = () => (state) => state.get('validatorContainer');
const selectAppContainerDomain = () => (state) => state.get('appContainer');
const selectDocumentHeight = () => (state) => state.get('validatorContainer').get('documentHeight');
const selectDocumentWidth = () => (state) => state.get('validatorContainer').get('documentWidth');
const selectFormFieldsState = () => (state) => state.get('validatorContainer').get('formFields');
const selectTablesState = () => (state) => state.get('validatorContainer').get('updatedTables');
const selectOriginalFormFieldsState = () => (state) => state.get('validatorContainer').get('originalFormFields');
const selectOriginalTablesState = () => (state) => state.get('validatorContainer').get('originaltables');
const selectActiveFieldId = () => (state) => state.get('validatorContainer').get('activeFieldId');
const selectConfidenceThreshold = () => (state) => state.get('validatorContainer').get('confidenceThreshold');
const selectSirFieldsState = () => (state) => state.get('validatorContainer').get('sirFields');
const selectProjectId = () => (state) => state.get('validatorContainer').get('projectId');
const selectDesignerFields = () => (state) => state.get('validatorContainer').get('designerFields');
const selectValidationData = () => (state) => state.get('validatorContainer').get('validationData');
const selectDesignerTables = () => (state) => state.get('validatorContainer').get('designerTables');
/**
 * this selector transform original field structure to UI requirement and flattern it.
 */
const selectFieldRules = () => createSelector(
    selectActiveFieldId(),
    selectConfidenceThreshold(),
    selectDesignerFields(),
    selectValidationData(),
    selectFormFieldsState(),
    selectOriginalFormFieldsState(),
    (activeFieldId, confidenceThreshold, designerFields, validationData, formFields, originalFields) => {
        const validationInfo = validationData ? validationData.toJS() : [];
        const originalFieldsInfo = originalFields ? originalFields.toJS() : [];
        const getRules = getFormFieldRules(activeFieldId, confidenceThreshold, designerFields.toJS(), validationInfo, formFields.toJS(), originalFieldsInfo);
        return getRules();
    }
 );
/**
 * this selector transform original field structure to UI requirement and flattern it.
 */
const selectFields = () => createSelector(
    selectFormFieldsState(),
    selectFieldRules(),
    (formFields, fieldRules) => {
        const fields = jsont.transform(formFields.toJS(), fieldRules);
        return fields;
    }
 );

export const selectOriginalTableFields = () => createSelector(
    selectOriginalTablesState(),
    (originalTables) => {
        if (!originalTables) return [];
        const fields = originalTables.toJS().reduce((acc, table) => {
            return table.Rows.reduce((acc, row) => {
                return row.Fields.reduce((acc, field) => {
                    acc.push(field);
                    return acc;
                }, acc);
            }, acc);
        }, []);
        return fields;
    }
);

const selectTableRules = () => createSelector(
    selectActiveFieldId(),
    selectConfidenceThreshold(),
    selectDesignerTables(),
    selectValidationData(),
    selectOriginalTableFields(),
    (activeFieldId, confidenceThreshold, designerTables, validationData, originalTablesFields) => {
        const validationInfo = validationData ? validationData.toJS() : [];
        const getRules = getTableRules(activeFieldId, confidenceThreshold, designerTables.toJS(), validationInfo, originalTablesFields);
        return getRules();
    }
);

export const selectNewRows = () => createSelector(
    selectTablesState(),
    (tables) => {
        return jsont.transform(tables.toJS(), getTableNewRowRules()());
    }
);

export const selectTables = () => createSelector(
    selectTablesState(),
    selectTableRules(),
    (tables, tableRules) => {
        return jsont.transform(tables.toJS(), tableRules);
    }
);

const selectMetaData = () => createSelector(
    selectDocumentHeight(),
    selectDocumentWidth(),
    (height, width) => {
        return {Metadata: {height, width}};
    }
);

const selectAllTablesFields = () => createSelector(
    selectTables(),
    (tables) => {
        return tables.reduce((acc, table) => {
            return table.rows.reduce((acc, row) => {
                return row.reduce((acc, field) => {
                    acc.push(field);
                    return acc;
                }, acc);
            }, acc);
        }, []);
    }
);

const selectActiveField = () => createSelector(
    selectActiveFieldId(),
    selectFields(),
    selectAllTablesFields(),
    (currentFieldId, formFields, tableFields) => {
        const allFields = [...formFields,
            ...tableFields,
        ];
        const currentField = allFields.find((field) => field.fieldGuid === currentFieldId);
        return currentField;
    }
);
const selectAllFields = () => createSelector(
    selectFields(),
    selectAllTablesFields(),
    (formFields, tableFields) => {
        const allFields = [...formFields,
            ...tableFields,
        ];
        return allFields;
    }
);

const selectLayoutRatio = () => createSelector(
    selectValidatorContainerDomain(),
    (validatorState) => {
        return validatorState.get('layoutRatio');
    }
);

const selectCurrentZoomScale = () => createSelector(
    selectValidatorContainerDomain(),
    (validatorState) => {
        return validatorState.get('currentZoomScale');
    }
);
const selectSirFieldRule = () => createSelector(
    selectMetaData(),
    selectLayoutRatio(),
    selectCurrentZoomScale(),
    (docMeta, layoutRatio, currentZoomScale) => {
        const getRules = getSirFieldRules(docMeta, layoutRatio, currentZoomScale);
        return getRules();
    }
);

// pulled this out of the main selector so we can leverage it when needed in other selectors
const selectStaticAnnotations = () => createSelector(
    selectActiveField(),
    selectSirFieldsState(),
    selectSirFieldRule(),
    (activeField, fields, sirRules) => {
        if (!activeField) return null;

        const selectedField = fields.toJS().find((object) => {
            return activeField && object.Bounds === activeField.bounds;
        });
        return selectedField ? [jsont.transform(selectedField, sirRules)] : [];
    }
);

const selectBreadCrumbsUrl = () => createSelector(
    selectAppContainerDomain(),
    selectProjectId(),
    getBreadcrumbsUrlForValidator
);

const selectAllValidated = () => createSelector(
    selectAllFields(),
    (allFields) => {
        if (!allFields) return false;
        const allValidated = !allFields.reduce((acc, field) => {
            return acc || (field.validationMessage ? true : false);
        }, false);
        return allValidated;
    }
);
const selectFieldAccuracyList = () => createSelector(
    selectAllValidated(),
    selectAllFields(),
    (allValidated, allFields) => {
        if (!allFields || !allValidated) return [];
        const fieldAccuracyList = allFields.map((field) => {
            return {
                fieldId: field.fieldId,
                oldValue: field.oldValue,
                newValue: field.text,
            };
        });
        return fieldAccuracyList;
    }
);
const selectDestinationUrl = () => createSelector(
    selectAppContainerDomain(),
    (appState) => {
        const roles = appState.getIn(['user', 'roles']).toJS();
        const isServiceRole = isAllowedAccess([IQBOT_USER_ROLES.SERVICES], roles);
        return routeHelper(isServiceRole ?
            '/learning-instances' :
            '/learning-instances/validations'
        );
    }
);

const selectValidatorContainer = () => createSelector(
    selectValidatorContainerDomain(),
    selectAppContainerDomain(),
    selectFields(),
    selectTables(),
    selectMetaData(),
    selectStaticAnnotations(),
    selectBreadCrumbsUrl(),
    selectFieldAccuracyList(),
    selectAllValidated(),
    selectDestinationUrl(),
    (substate,
        appState,
        formFields,
        tables,
        metaData,
        staticAnnotations,
        breadCrumbsUrl,
        fieldAccuracyList,
        allValidated,
        destinationUrlOnNoDocument,
    ) => {

        return {
            ...substate.toJS(),
            appState: appState.toJS(),
            objects: [],
            staticAnnotations,
            fields: formFields,
            tables,
            //activeField,
            //selectedObjects,
            metaData,
            appConfig: appState.get('appConfig').toJS(),
            breadCrumbsUrl,
            fieldAccuracyList,
            allValidated,
            destinationUrlOnNoDocument,
        };
    }
);

export default selectValidatorContainer;
export {
    selectValidatorContainerDomain,
};
