/* eslint-disable */
/*
 *
 * DesignerContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
  CLEAR_VALIDATOR_STORE,
  NO_DOCUMENT_TO_VALIDATE,
  SAVE_CONFIRMATION_BOX_VISIBILITY,
  SET_ACTIVE_FIELD,
  SET_CURRENT_ZOOM_SCALE,
  SET_FORM_FIELD_VALUE,
  SET_LAYOUT_RATIO,
  SET_SCALE_AMOUNT,
  SET_TABLE_FIELD_VALUE,
  POPULATE_DOCUMENT_DATA,
  POPULATE_PROJECT_METADATA,
  LOADING_SCREEN,
  INSERT_TABLE_ROW_ABOVE,
  DELETE_TABLE_ROW,
  INSERT_TABLE_ROW_BELOW,
} from './constants';
import {FIELD_OPTIONS} from '../../constants/enums';
import {ANNOTATION_MODES} from '../../components/AnnotationEngine/constants/enums';
import {getTableFieldPathToUpdateValue, getTableFieldValidationIssueCodePath} from './utils/table';
import {getFormFieldPathToUpdateValue, getFormFieldValidationIssueCodePath} from './utils/formField';

export const initialState = fromJS({
  activeField: null,
  activeFieldId: '',
  activeFieldOption: FIELD_OPTIONS.LABEL,
  annotationClasses:[],
  aspectRatio: 1,
  classificationId: null,
  currentZoomScale: 1,
  designerFields:[],
  designerTables:[],
  designerTableColumns:[],
  documentHeight: 3508,
  documentWidth: 2481,
  fileName: null,
  fileUploading: false,
  formFields:[],
  height:0,
  imageLoaded: true,
  keyValues: [],
  layoutRatio: 1,
  layouts :[],
  loadingScreen: false,
  metaData: {},
  mode: ANNOTATION_MODES.FREE,
  noDocumentToValidate: false,
  projectName: '',
  remainingDocumentReviewCount: null,
  scaleAmount: 0.25,
  showSaveConfirmationModal: false,
  sirFields:[],
  updatedTables:[],
  url: '',
  validationData:[],
  visionBotId: null,
  width: 0,
  confidenceThreshold: 0,
});

function annotatorContainerReducer(state = initialState, action) {
    switch (action.type) {
        case POPULATE_DOCUMENT_DATA: {
            const {payload: {
                url,
                vBotDocument,
                validationData,
                fileName,
                classificationId,
                remainingDocumentReviewCount,
                fileId,
            }} = action;
            return state.set('url', url)
                .set('fileName', fileName)
                .set('fileId', fileId)
                .set('classificationId', classificationId)
                .set('remainingDocumentReviewCount', remainingDocumentReviewCount)
                .set('sirFields', fromJS(vBotDocument.SirFields.Fields))
                .set('documentHeight', vBotDocument.DocumentProperties.Height)
                .set('documentWidth', vBotDocument.DocumentProperties.Width)
                .set('validationData', fromJS(validationData))
                .set('originalFormFields', fromJS(vBotDocument.Data.FieldDataRecord.Fields))
                .set('designerFields', fromJS(vBotDocument.Layout.Fields))
                .set('designerTables', fromJS(vBotDocument.Layout.Tables))
                .set('formFields', fromJS(vBotDocument.Data.FieldDataRecord.Fields))
                .set('originaltables', fromJS(vBotDocument.Data.TableDataRecord))
                .set('updatedTables', fromJS(vBotDocument.Data.TableDataRecord));
        }
        case CLEAR_VALIDATOR_STORE:
            return initialState;
        case SET_ACTIVE_FIELD:
            return state.set('activeFieldId', action.field.fieldGuid)
              .set('activeField', action.field);
        case SET_LAYOUT_RATIO: {
            return state.set('layoutRatio', action.ratio);
        }
        case SET_SCALE_AMOUNT:
            return state.set('scaleAmount', action.scaleAmount);
        case SET_CURRENT_ZOOM_SCALE:
            return state.set('currentZoomScale', action.currentZoomScale);
        case SET_FORM_FIELD_VALUE: {
          return state.setIn(
                getFormFieldPathToUpdateValue(action.fieldIndex, action.fieldName),
                action.fieldValue
            ).setIn(
                getFormFieldValidationIssueCodePath(action.fieldIndex),
                null
            );
        }
        case SET_TABLE_FIELD_VALUE: {
          return state.setIn(
                getTableFieldPathToUpdateValue(
                    action.tableIndex,
                    action.rowIndex,
                    action.fieldIndex,
                    action.fieldName
                ),
                action.fieldValue
            ).setIn(
                getTableFieldValidationIssueCodePath(
                    action.tableIndex,
                    action.rowIndex,
                    action.fieldIndex
                ),
                null
            );
        }
        case NO_DOCUMENT_TO_VALIDATE:
          return state.set('noDocumentToValidate', action.show);
        case POPULATE_PROJECT_METADATA:
          return state.set('projectName', action.payload.name)
            .set('projectId', action.payload.id)
            .set('confidenceThreshold', action.payload.confidenceThreshold);
        case LOADING_SCREEN:
            return state.set('loadingScreen', action.show);
        case SAVE_CONFIRMATION_BOX_VISIBILITY:
            return state.set('showSaveConfirmationModal', action.show);
        case INSERT_TABLE_ROW_ABOVE: {
            return state.updateIn(['updatedTables', action.tableIndex, "Rows"], rows => rows.insert(action.rowIndex, action.newRow));
        }
        case INSERT_TABLE_ROW_BELOW:{
            return state.updateIn(['updatedTables', action.tableIndex, "Rows"], rows => rows.insert(action.rowIndex + 1, action.newRow));
        }
        case DELETE_TABLE_ROW:
            return state.deleteIn(['updatedTables', action.tableIndex, 'Rows', action.rowIndex]);
        default:
            return state;
    }
}

export default annotatorContainerReducer;
