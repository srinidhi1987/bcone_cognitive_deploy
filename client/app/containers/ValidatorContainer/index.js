/* eslint-disable */

/*
*
* ValidatorContainer
*
*/

import React from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import {autobind} from 'core-decorators';

import selectValidatorContainer from './selectors';
import ValidatorPageLayout from '../../components/ValidatorPageLayout';
import {
    clearValidatorStore,
    fetchDocumentToValidate,
    fetchProjectMetadata,
    markDocumentAsInvalid,
    noDocumentToValidateModalVisibility,
    saveConfirmationBoxVisibility,
    saveValidatedDocument,
    setActiveField,
    setCurrentZoomScale,
    setFormFieldValue,
    setTableFieldValue,
    setImageLoaded,
    setLayoutRatio,
    setScaleAmount,
    insertRowBelow,
    insertRowAbove,
    deleteRowRequest,
    insertRowRequest,
} from './actions';
import {DESIGNER_AREA_TYPES, FIELD_OPTIONS} from '../../constants/enums';
import {
    EMPTY_STRING,
    initialZoomMetadata,
    MIN_ZOOM_SCALE,
    MAX_ZOOM_SCALE,
    ZOOM_ACTION_ENUM,
} from '../../constants/common';
import { auto } from 'async';
import AlertModal from '../../components/AlertModal';
import routeHelper from '../../utils/routeHelper';

export class ValidatorContainer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'ValidatorContainer';

    state = {
        objects: [],
        fields: [],
        fileUploaded: false,
        processedCount: 3,
        fieldColumnTopOffset: 0,
        selectedFieldType: DESIGNER_AREA_TYPES.FIELD, // Temporary state used to mock state or right colum COG-2882
        shouldShowMarkAsInvalidModal: false,
        ...initialZoomMetadata,
        actions: [
            {id:0, title: 'Insert row above'},
            {id:1, title: 'Insert row below'},
            {id:2, title: 'Delete row'},
        ],
    };

    componentWillMount() {
        const {params, fetchDocumentToValidate, fetchProjectMetadata} = this.props;
        //one time call to get project metadata
        fetchProjectMetadata(params.id);
        //fetch Document to validate for given project
        fetchDocumentToValidate(params.id);
    }

    componentWillUnmount() {
        this.props.clearValidatorStore();
    }

    render() {
        const {
            shouldShowMarkAsInvalidModal,
            actions,
        } = this.state;
        const {
            activeField,
            activeFieldId,
            activeFieldOption,
            allValidated,
            breadCrumbsUrl,
            classificationId,
            currentZoomScale,
            fileName,
            fileUploading,
            imageLoaded,
            loadingScreen,
            metaData,
            mode,
            objects,
            params,
            projectName,
            fields,
            selectedObjects,
            showSaveConfirmationModal,
            staticAnnotations,
            tables,
            url,
            noDocumentToValidate,
            setTableFieldValue,
            remainingDocumentReviewCount,
        } = this.props;
        // metaData object format used by the AnnotatorEngine component
        // classes utilized the AnnotatorEngine
        const annotationClasses = ['text', 'number', 'calendar'];
        return (
            noDocumentToValidate ?
                <AlertModal
                    show={noDocumentToValidate}
                    closeHandler={this.closeHandler}
                    title='There are no documents available for validation.'
                    paragraphs={['You will be redirected to the Learning Instance page.']}
                /> :
                <ValidatorPageLayout
                    activeField={activeField}
                    activeFieldId={activeFieldId}
                    activeFieldOption={activeFieldOption}
                    allValidated={allValidated}
                    annotationClasses={annotationClasses}
                    areaClickHandler={this.areaClickHandler}
                    breadCrumbsUrl={breadCrumbsUrl}
                    cancelSaveConfirmationHandler={this.cancelSaveConfirmationHandler}
                    classificationId={classificationId}
                    closeMarkAsInvalidHandler={this.handleCloseMarkAsInvalidModal}
                    confirmMarkAsInvalidHandler={this.confirmMarkAsInvalidHandler}
                    currentZoomScale={currentZoomScale}
                    docMeta={metaData}
                    fields={fields}
                    fileName={fileName}
                    fileUploading={fileUploading}
                    imageLoaded={imageLoaded}
                    imageLoadHandler={this.imageLoadHandler}
                    loadingScreen={loadingScreen}
                    mode={mode}
                    nextDocumentHandler={this.nextDocumentHandler}
                    onUpdate={this.updateObjects}
                    objects={objects}
                    params={params}
                    projectName={projectName}
                    remainingDocumentReviewCount={remainingDocumentReviewCount}
                    saveConfirmHandler={this.saveConfirmHandler}
                    saveClickHandler={this.saveClickHandler}
                    selectFieldHandler={this.selectFieldHandler}
                    selectedObjects={selectedObjects}
                    setFieldValueHandler={this.setFieldValueHandler}
                    setTableFieldValue={setTableFieldValue}
                    setLayoutRatio={this.setLayoutRatio}
                    showMarkAsInvalidHandler={this.handleShowMarkAsInvalidModal}
                    shouldShowMarkAsInvalidModal={shouldShowMarkAsInvalidModal}
                    showSaveConfirmationModal={showSaveConfirmationModal}
                    staticAnnotations={staticAnnotations}
                    startDragHandler={this.startDragHandler}
                    stopDragHandler={this.stopDragHandler}
                    tables={tables}
                    url={url}
                    handleZoomIn={this.zoomIn}
                    handleZoomOut={this.zoomOut}
                    handleResetZoom={this.reset}
                    location={this.props.location}
                    tableActionOptions={actions}
                    optionSelectHandler={this.optionSelectHandler}
                    insertFirstRow={this.insertFirstRow}
                />
        );
    }

    @autobind
    closeHandler() {
        const {destinationUrlOnNoDocument, hideNoDocumentToValidateModal} = this.props;
        hideNoDocumentToValidateModal();
        // go to LI listing page based on user role (decided in selectors)
        browserHistory.push(destinationUrlOnNoDocument);
    }

    @autobind
    getOriginalDimension(value, layoutRatio) {
        return Math.round(value * layoutRatio);
    }

    @autobind
    updateObjects(drawnAreas, areaIndex) {
        //this.props.updateDrawnAreas(drawnAreas, areaIndex);
    }

    @autobind
    setLayoutRatio(ratio) {
        this.props.setLayoutRatio(ratio);
    }

    /**
     * Deletes an annotation object from the objects array
     * @param {number} objectIndexToDelete
     */
    @autobind
    deleteDesignerField(objectIndexToDelete) {
        const {objects} = this.state;

        this.setState({
            objects: objects.filter((obj, index) => index !== objectIndexToDelete),
            selectedObject: objects.length > 1 ? objects.length - 2 : 0,
        });
    }

    calculateProcessWidth(current, total) {
        return (current / total) * 100;
    }

    /**
     * reset zoom scale in state to original values
     */
    @autobind
    reset() {
        this.props.setCurrentZoomScale(initialZoomMetadata.currentZoomScale);
    }

    @autobind
    zoomIn() {
        const {currentZoomScale} = this.props;
        if (currentZoomScale === MAX_ZOOM_SCALE) {
            return;
        }
        const scale = this._getScale(ZOOM_ACTION_ENUM.zoomIn);
        this.props.setCurrentZoomScale(scale);
    }

    @autobind
    zoomOut() {
        const {currentZoomScale} = this.props;
        if (currentZoomScale === MIN_ZOOM_SCALE) {
            return;
        }
        const scale = this._getScale(ZOOM_ACTION_ENUM.zoomOut);

        this.props.setCurrentZoomScale(scale);
    }

    /**
     * handler for selecting a field or table column. Triggers artificial 'scroll'
     *
     * @param {{currentTarget: {offsetTop: Number}}} event
     * @param {string} id
     */
    @autobind
    selectFieldHandler(event, field) {
        this.props.setActiveField(field);
    }

    /**
     * sets a field value on change in the designer
     *
     * @param {Object} fieldGuid
     * @param {string} fieldName
     * @param {*} fieldValue
     */
    @autobind
    setFieldValueHandler(fieldGuid, fieldName, fieldValue, fieldIndex) {
        this.props.setFormFieldValue(fieldGuid, fieldName, fieldValue, fieldIndex);
    }

    /**
     * sets a field value on change in the designer
     *
     * @param {Object} fieldGuid
     * @param {string} fieldName
     * @param {*} fieldValue
     */
    @autobind
    setTableFieldValueHandler(tableIndex, rowIndex, columnIndex, fieldGuid, fieldName, fieldValue) {
        //this.props.setFormFieldValue(fieldGuid, fieldName, fieldValue);
    }

    @autobind
    startDragHandler(activeField) {

    }

    @autobind
    stopDragHandler(activeField) {

    }

    @autobind
    areaClickHandler(area) {

    }

    /**
     * called when image is loaded
     * @param {boolean} isLoaded
     */
    @autobind
    imageLoadHandler(isLoaded) {
        this.props.setImageLoaded(isLoaded);
    }

    /**
     * called when mark as invalid button is clicked
     * @param {boolean} isLoaded
     */

    @autobind
    handleShowMarkAsInvalidModal(showMarkAsInvalidModal) {
        this.setState({shouldShowMarkAsInvalidModal: true});
    }

    /**
     * called when cancel mark as invalid is clicked
     */

    @autobind
    handleCloseMarkAsInvalidModal() {
        this.setState({shouldShowMarkAsInvalidModal: false});
    }

    /**
     * called when mark as invalid button is clicked
     * @param {Object} invalidOptions
     * @param {*} invalidOptionsValue

     */

    @autobind
    confirmMarkAsInvalidHandler(invalidOptions) {
        const {fileId, projectId, appState: {user: {name}}} = this.props;

        this.props.markDocumentAsInvalid(projectId, fileId, invalidOptions, name);
        this.handleCloseMarkAsInvalidModal()
    }

    /**
     * handles completion of bot designing. Currently returns to project page
     */
    @autobind
    finishHandler() {
        browserHistory.push(routeHelper(`/learning-instances`));
    }

    /**
     *
     * @param {ZOOM_ACTION_ENUM} zoomAction
     * @returns {number}
     */
    _getScale(zoomAction) {
        const {currentZoomScale, scaleAmount} = this.props;
        switch (zoomAction) {
            case ZOOM_ACTION_ENUM.zoomIn:
                return currentZoomScale + scaleAmount;
            case ZOOM_ACTION_ENUM.zoomOut:
                return currentZoomScale - scaleAmount;
            default:
                return currentZoomScale;
        }
    }

    //validator navigation handlers
    @autobind
    nextDocumentHandler() {
        const {params, fetchDocumentToValidate} = this.props;
        //fetch Document to validate for given project
        fetchDocumentToValidate(params.id);
    }

    @autobind
    saveClickHandler() {
        this.props.showSaveConfirmation();
    }

    @autobind
    cancelSaveConfirmationHandler() {
        this.props.hideSaveConfirmation();
    }

    @autobind
    saveConfirmHandler() {
        const {
            fieldAccuracyList,
            fileId,
            formFields,
            hideSaveConfirmation,
            projectId,
            saveValidatedDocument,
            updatedTables,
        } = this.props;
        saveValidatedDocument(projectId, fileId, formFields, updatedTables, fieldAccuracyList);
        hideSaveConfirmation();
    }

    @autobind
    optionSelectHandler(option, {tableIndex, rowIndex}) {
        switch(option.id) {
            case 0:
                return this.props.insertRowAbove(tableIndex, rowIndex);
            case 1:
                return this.props.insertRowBelow(tableIndex, rowIndex);
            case 2:
                return this.props.deleteRowRequest(tableIndex, rowIndex);
        }
    }

    @autobind
    insertFirstRow(tableIndex) {
        this.props.insertRowAbove(tableIndex, 0);
    }
}

const mapStateToProps = selectValidatorContainer();

function mapDispatchToProps(dispatch) {
    return {
        clearValidatorStore: () => dispatch(clearValidatorStore()),
        fetchProjectMetadata: (projectId) => dispatch(fetchProjectMetadata(projectId)),
        fetchDocumentToValidate: (projectId) => dispatch(fetchDocumentToValidate(projectId)),
        hideNoDocumentToValidateModal: () => dispatch(noDocumentToValidateModalVisibility(false)),
        markDocumentAsInvalid: (projectId, fileId, reasons, username) => dispatch(markDocumentAsInvalid(projectId, fileId, reasons, username)),
        setActiveField: (field) => dispatch(setActiveField(field)),
        setFormFieldValue: (fieldName, value, fieldIndex) => dispatch(setFormFieldValue(fieldName, value, fieldIndex)),
        setTableFieldValue: (fieldName, value, tableIndex, rowIndex, fieldIndex) => dispatch(setTableFieldValue(fieldName, value, tableIndex, rowIndex, fieldIndex)),
        setLayoutRatio: (ratio) => dispatch(setLayoutRatio(ratio)),
        setImageLoaded: (isLoaded) => dispatch(setImageLoaded(isLoaded)),
        setCurrentZoomScale: (zoomScale) => dispatch(setCurrentZoomScale(zoomScale)),
        setScaleAmount: (scaleAmount) => dispatch(setScaleAmount(scaleAmount)),
        showSaveConfirmation: () => dispatch(saveConfirmationBoxVisibility(true)),
        hideSaveConfirmation: () => dispatch(saveConfirmationBoxVisibility(false)),
        saveValidatedDocument: (projectId, fileId, fields, tables, fieldAccuracyList) => dispatch(saveValidatedDocument(projectId, fileId, fields, tables, fieldAccuracyList)),
        insertRowAbove : (tableIndex, rowIndex) => dispatch(insertRowRequest(tableIndex, rowIndex, true)), 
        insertRowBelow : (tableIndex, rowIndex) => dispatch(insertRowRequest(tableIndex, rowIndex, false)),
        deleteRowRequest: (tableIndex, rowIndex) => dispatch(deleteRowRequest(tableIndex, rowIndex)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ValidatorContainer);
