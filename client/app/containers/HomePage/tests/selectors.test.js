import {fromJS} from 'immutable';

import {APP_NOTIFICATIONS} from '../../../constants/enums';
import selectHomePage from '../selectors';
import {IQBOT_USER_ROLES} from '../../../constants/enums';

describe('NavigationContainer selectors', () => {
    describe('selectHomePage selector', () => {
        describe('props', () => {
            // given
            const mockedState = fromJS({
                appContainer: {
                    appNotifications: fromJS([{
                        message: 'Notification here',
                        level: APP_NOTIFICATIONS.ERROR,
                    }]),
                    user: fromJS({
                        id: '3454565767658756',
                        name: 'John Smith',
                        roles: [IQBOT_USER_ROLES.SERVICES],
                    }),
                },
                homePage: {},
            });
            const selector = selectHomePage();

            it('should return user from app state', () => {
                // given
                // when
                const result = selector(mockedState);
                // then
                expect(result.user).toEqual(mockedState.getIn(['appContainer', 'user']).toJS());
            });

            it('should return app notifications from app container', () => {
                // given
                // when
                const result = selector(fromJS(mockedState));
                // then
                expect(result.appNotifications).toEqual(mockedState.getIn(['appContainer', 'appNotifications']).toJS());
            });
        });
    });
});
