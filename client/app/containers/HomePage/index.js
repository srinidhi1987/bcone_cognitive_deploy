/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import NavigationContainer from '../../containers/NavigationContainer';
import selectHomePage from './selectors';
import {decideNavBarWidth} from '../../utils/helpers';

import './styles.scss';

export class HomePage extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'HomePage';

    static propTypes = {
        children: PropTypes.element,
    };

    render() {
        const {
            appNotifications,
            location,
            routerLocation,
            user,
            userRole,
        } = this.props;

        const navBarWidth = decideNavBarWidth(location.pathname);

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Dashboards"
                />

                <NavigationContainer
                    appNotifications={appNotifications}
                    navBarWidth={navBarWidth}
                    routerLocation={routerLocation}
                    user={user}
                    userRole={userRole}
                />

                <main className="aa-main-container" style={{width: `calc(100vw - ${navBarWidth})`}}>
                    { this.props.children }
                </main>
            </div>
        );
    }
}

const mapStateToProps = selectHomePage();


export default connect(mapStateToProps)(HomePage);
