/*
 *
 * HomePage reducer
 *
 */

import {fromJS} from 'immutable';

export const initialState = fromJS({});

function homePageReducer(state = initialState, action) {
    switch (action.type) {
        case '@@router/LOCATION_CHANGE':
            return state.set('routerLocation', action.payload.pathname);
        default:
            return state;
    }
}

export default homePageReducer;
