import {createSelector} from 'reselect';
import {HOME_PAGE} from '../../constants/stateNames';
import {decideNavBarWidth} from '../../utils/helpers';
import {getAccessLevel} from '../../utils/roleHelpers';

/**
 * Direct selector to the navigationContainer state domain
 */
const selectHomePageDomain = () => (state) => state.get(HOME_PAGE);
const selectAppContainerDomain = () => (state) => state.get('appContainer');

/**
 * Default selector used by NavigationContainer
 */

const selectHomePage = () => createSelector(
    selectHomePageDomain(),
    selectAppContainerDomain(),
    (substate, appState) => {
        return {
            ...substate.toJS(),
            appNotifications: appState.get('appNotifications').toJS(),
            user: appState.get('user').toJS(),
            userRole: getAccessLevel(appState.getIn(['user', 'roles']).toJS()),
            navBarWidth: decideNavBarWidth(substate.get('routerLocation')),
            isBootstrapping: appState.get('isBootstrapping'),
        };
    }
);

export default selectHomePage;
