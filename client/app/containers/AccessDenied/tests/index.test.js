
import React from 'react';
import {shallow} from 'enzyme';
import AccessDenied from '../index';
import {CommandButton} from 'common-frontend-components';
import Helmet from 'react-helmet';

describe('<AccessDenied />', () => {
    const routerProp = {
        goBack: jest.fn(),
    };
    const wrapper = shallow(<AccessDenied router={routerProp} />);

    it('should be defined', () => {
        expect(AccessDenied).toBeDefined();
    });

    describe('helmet headers', () => {
        const {title} = wrapper.find(Helmet).props();

        it('should render page title', () => {
            expect(title).toEqual('Automation Anywhere | Access Denied');
        });
    });

    describe('Back Button', () => {

        it('should navigate to previous browser history', () => {
            const {onClick} = wrapper.find(CommandButton).props();
            onClick();
            expect(routerProp.goBack.mock.calls.length).toEqual(1);
        });
    });
});
