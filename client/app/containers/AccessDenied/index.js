/**
 * AccessDenied
 *
 * This is the page we show when the user visits a url that they are prohibited from viewing
 *
 */

import React, {PureComponent} from 'react';
import {
    CommandButton,
    Message,
    Modal,
    PageTitle,
} from 'common-frontend-components';
import {themeType} from '../../components/ActionConfirmationModal';
import Helmet from 'react-helmet';
import {autobind} from 'core-decorators';

import './styles.scss';

export default class AccessDenied extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'AccessDenied';

    render() {
        return (
            <div className="aa-access-denied-container">

                <Helmet
                    title="Automation Anywhere | Access Denied"
                />

                <Modal
                    className="aa-access-denied-modal"
                    theme="default"
                    show={true}
                    onHide={this.goBack}
                    closable={false}
                >
                    <div className="aa-grid-row space-between vertical-center">
                        <PageTitle label="Access Denied" />

                        <CommandButton
                            onClick={this.goBack}
                        >&lt; Back</CommandButton>
                    </div>

                    <Message
                        theme={themeType.error}
                        title="Oops, you can't access that page"
                    >
                        <div>
                            <p>Possible reasons:</p>
                            <ul>
                                <li>Your bookmarked page no longer exists</li>
                                <li>Although you used to be able to access that page, your permissions have changed and it is not available to you any longer</li>
                                <li>You do not have permission to see that page</li>
                            </ul>
                            <p>If you think you are seeing this message by mistake, please contact your administrator.</p>
                        </div>
                    </Message>
                </Modal>
            </div>
        );
    }

    @autobind
    goBack() {
        this.props.router.goBack();
    }
}
