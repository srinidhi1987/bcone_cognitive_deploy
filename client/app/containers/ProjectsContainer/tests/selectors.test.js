import {fromJS} from 'immutable';

import selectProjectsContainer from '../selectors';
import {languages1} from '../../../../fixtures/languages';
import {Project2, Project1} from '../../../../fixtures/projects';
import {SortDirections} from '../../../constants/enums';
import {PROJECT_SORT_KEY} from '../constants';
import {compareKey} from 'common-frontend-utils';
let mockedState, selector, appConfig;


describe('selectProjectsContainer selector', () => {
    beforeEach(() => {
        // given
        appConfig = {
            encrypted: '43324234324',
            designer: true,
        };

        mockedState = fromJS({
            appContainer: {
                appConfig,
                authToken: 'test-token',
            },
            projectsContainer: {
                // projects out of order by 'name'
                projects: [Project2, Project1],
                sort: [{id: PROJECT_SORT_KEY, orderBy: SortDirections.ASCENDING}],
                sortFn: compareKey(PROJECT_SORT_KEY),
            },
            language: {
                supportedLanguages: languages1,
            },
        });
        // when
        selector = selectProjectsContainer();
    });

    it('should return supported languages via supportedLanguages prop ', () => {
        // then
        expect(selector(mockedState).supportedLanguages).toEqual(languages1);
    });

    it('should return sorted projects asc by "name" via sortedProjects prop', () => {
        // then
        expect(selector(mockedState).sortedProjects[0]).toEqual(Project1);
    });

    it('should return encrypted app configuration', () => {
        // then
        expect(selector(mockedState).appConfig.encrypted).toEqual(appConfig.encrypted);
    });

    it('should return current authentication token', () => {
        // then
        expect(selector(mockedState).authToken).toEqual('test-token');
    });
});
