import {shallow} from 'enzyme';
import React from 'react';
import {ProjectsContainer} from '../index';
import {Project1, Project2} from '../../../../fixtures/projects';
import {
    DataTable,
} from 'common-frontend-components';
import ActionConfirmationModal from '../../../components/ActionConfirmationModal';

describe('<ProjectsContainer />', () => {

    const sortedProjects = [Project1, Project2];
    const cacheProjectSpy = jest.fn();
    const containerWrapper = shallow(
        <ProjectsContainer
            sortedProjects={sortedProjects}
            fetchProjects ={() => {}}
            cacheProject= {cacheProjectSpy}
            selectedProject = {Project1}
    />);
    describe('component method: isPr  ojectInProduction', () => {
        //given
        const containerWrapper = shallow(
            <ProjectsContainer
                sortedProjects={sortedProjects}
                fetchProjects ={() => {}}
        />);
        it('should return false for project in staging', () => {
            //when
            const result = containerWrapper.instance().isProjectInProduction(Project1);
            //then
            expect(result).toEqual(false);
        });
        it('should return true for project in production', () => {
            //when
            const productionProject = {...Project1, environment:'production'};
            const result = containerWrapper.instance().isProjectInProduction(productionProject);
            //then
            expect(result).toEqual(true);
        });
    });
    describe('component method: _goToDetailsView', () => {
        //given
        const cacheProjectSpy = jest.fn();
        const containerWrapper = shallow(
            <ProjectsContainer
                sortedProjects={sortedProjects}
                fetchProjects ={() => {}}
                cacheProject= {cacheProjectSpy}
        />);
        it('should set project to cache', () => {
            //when
            containerWrapper.instance()._goToDetailsView(Project1);
            //then
            expect(cacheProjectSpy).toHaveBeenCalledWith(Project1);
        });
    });
    it('should pass the InitialSortedProjects prop to the data prop of the nested DataTable.', () => {
        //given
        const result = containerWrapper.find(DataTable);
        const {data} = result.props();
        //then
        expect(result.length).toEqual(1);
        expect(data.length).toEqual(2);

    });
    it('should select the project in DataTable', () => {
        //given
        const result = containerWrapper.find(DataTable);
        const {active} = result.props();
        //then
        expect(active).toEqual(Project1.id);
    });

    describe('component method: _handleProjectStateChange', () => {
        //given
        const sortedProjects = [Project2, {...Project1, environment:'production'}];
        const cacheProjectSpy = jest.fn();
        const updateSpy = jest.fn();
        const setProjectStageSpy = jest.fn();

        const wrapper = shallow(
              <ProjectsContainer
                  sortedProjects={sortedProjects}
                  fetchProjects ={() => {}}
                  cacheProject= {cacheProjectSpy}
                  selectedProject = {Project1}
                  setProjectStage={setProjectStageSpy}
                  updateProject={updateSpy}
        />);
        it('should issue update action if project is in production', () => {
            //when
            wrapper.instance()._handleProjectStageChange({...Project1, environment: 'production'}, true);
            //then
            expect(setProjectStageSpy).toHaveBeenCalledWith({...Project1, environment: 'production'}, 'staging');
        });
        it('should show confirmation modal if project is in staging', () => {
            //when
            wrapper.instance()._handleProjectStageChange(Project2, false);
            //then
            expect(wrapper.state().showActionConfirmationDetail).toEqual(true);
            expect(wrapper.state().actionConfirmationDetailParam).toEqual({project: Project2});
        });
    });
    describe('should check confirmation box visibility when project state change', () => {
        //given
        const sortedProjects = [Project2, {...Project1, environment:'production'}];
        const cacheProjectSpy = jest.fn();
        const updateSpy = jest.fn();
        const setProjectStageSpy = jest.fn();

        const wrapper = shallow(
            <ProjectsContainer
                sortedProjects={sortedProjects}
                fetchProjects ={() => {}}
                cacheProject= {cacheProjectSpy}
                selectedProject = {Project1}
                setProjectStage={setProjectStageSpy}
                updateProject={updateSpy}
        />);
        it('should not display confirmation box when project state is changed to staging', () => {
            //when
            wrapper.instance()._handleProjectStageChange({...Project1, environment: 'production'}, true);
            const actionModalProps = wrapper.find(ActionConfirmationModal).props();
            //then
            expect(actionModalProps.show).toEqual(false);
        });
        it('should display confirmation box when project state is changed to production', () => {
            //when
            wrapper.instance()._handleProjectStageChange(Project2, false);
            //then
            const actionModalProps = wrapper.find(ActionConfirmationModal).props();
            expect(actionModalProps.show).toEqual(true);
            expect(actionModalProps.param).toEqual({project: Project2});
        });
    });

    describe('component method: getProjectFields', () => {
        it('should have a method that gets project fields', () => {
            const wrapper = shallow(
                <ProjectsContainer
                    sortedProjects={sortedProjects}
                    fetchProjects ={() => {}}
                    cacheProject= {cacheProjectSpy}
                    selectedProject = {Project1}
            />);

            const fields = wrapper.instance().getProjectFields();

            expect(fields.length).toBeGreaterThan(0);

            const [firstField] = fields;

            expect(firstField).toHaveProperty('id');
            expect(firstField).toHaveProperty('sortable');
        });
    });
});
