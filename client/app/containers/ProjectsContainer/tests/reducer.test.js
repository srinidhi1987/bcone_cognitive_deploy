import projectsContainerReducer, {initialState} from '../reducer';
import {fromJS} from 'immutable';

import {
    LOADING_PROJECTS,
    FETCH_PROJECTS_SUCCEEDED,
    CACHE_PROJECT,
    REMOVE_PROJECT_CACHE,
    CLEAR_CURRENT_PROJECT,
    POPULATE_PROJECT_FIELDS_CACHE,
    FETCH_PROJECT_FAILED,
    FETCH_PROJECT_SUCCEEDED,
    POPULATE_CURRENT_PROJECT,
    UPDATE_CURRENT_PROJECT_BY_PROPERTY,
    UPDATE_PROJECT_PROPERTIES_BY_ID,
    UPDATE_CURRENT_PROJECT_PROPERTIES,
    SET_SORT_VALUES,
} from '../constants';
import {SortDirections} from '../../../constants/enums';
import {
    projectDeleteLoading,
    projectCategoriesLoading,
    setCurrentProjectTransactionId,
    setProjectId,
    fetchSelectedProjectCategoriesSucceeded,
    fetchProjectSummarySucceeded,
    fetchProjectSummaryFailed,
} from '../actions';
import {ProjectFields1} from '../../../../fixtures/project_fields';
import {Project1, Project2} from '../../../../fixtures/projects';
import {Category1, Category2} from '../../../../fixtures/categories';
import {projectSummary} from '../../../../fixtures/project_summary';
import {DataField, DataType} from 'common-frontend-utils';

describe('projectsContainerReducer', () => {
    it('returns the initial state', () => {
        expect(projectsContainerReducer(undefined, {})).toEqual(initialState);
    });

    describe('FETCH_PROJECTS_SUCCEEDED action', () => {
        it('should pass projects to store', () => {
            // given
            const projects = fromJS([
                {}, {}, {},
            ]);
            // when
            const result = projectsContainerReducer(initialState, {
                type: FETCH_PROJECTS_SUCCEEDED,
                payload: {
                    projects,
                },
            });
            // then
            expect(result.get('projects').length).toEqual(projects.length);
        });
    });

    describe('LOADING_PROJECTS action', () => {
        it('should set loading state in store to true', () => {
            // given
            // when
            const result = projectsContainerReducer(initialState, {
                type: LOADING_PROJECTS,
                loading: true,
            });
            // then
            expect(result.get('loadingProjects')).toEqual(true);
        });

        it('should set loading state in store to false', () => {
            // given
            const currentState = fromJS({
                ...initialState,
                loadingProjects: true,
            });
            // when
            const result = projectsContainerReducer(currentState, {
                type: LOADING_PROJECTS,
                loading: false,
            });
            // then
            expect(result.get('loadingProjects')).toEqual(false);
        });
    });

    describe('CACHE_PROJECT action', () => {
        it('should cache passed in project', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            // when
            const result = projectsContainerReducer(initialState, {
                type: CACHE_PROJECT,
                payload: {
                    project,
                },
            });
            // then
            expect(result.get('cachedProject')).toEqual(project);
        });
    });

    describe('REMOVE_PROJECT_CACHE action', () => {
        it('should remove cached project', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            const currentState = fromJS({
                ...initialState,
                cachedProject: {
                    project,
                },
            });
            // when
            const result = projectsContainerReducer(currentState, {
                type: REMOVE_PROJECT_CACHE,
            });
            // then
            expect(result.get('cachedProject')).toBe(null);
        });
    });

    describe('CLEAR_CURRENT_PROJECT action', () => {
        it('should remove current project', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            const currentState = fromJS({
                ...initialState,
                currentProject: {
                    project,
                },
                currentProjectCategories: [Category1, Category2],
            });
            // when
            const result = projectsContainerReducer(currentState, {
                type: CLEAR_CURRENT_PROJECT,
            });
            // then
            expect(result.get('currentProject')).toBe(null);
            expect(result.get('currentProjectCategoryData')).toEqual(fromJS([]));
        });
    });

    describe('PROJECT_CATEGORIES_LOADING action', () => {
        it('should set loading to true', () => {
            // given
            const action = projectCategoriesLoading(true);

            //when
            const result = projectsContainerReducer(initialState, action);

            //then
            expect(result.get('loadingCategories')).toEqual(true);
        });
    });

    describe('POPULATE_PROJECT_FIELDS_CACHE action', () => {
        it('should populate project files cache', () => {
            // given
            const action = {
                type: POPULATE_PROJECT_FIELDS_CACHE,
                payload: {
                    fields: ProjectFields1,
                },
            };
            // when
            const result = projectsContainerReducer(initialState, action);
            // then
            expect(result.get('projectFieldsCache')).toEqual(ProjectFields1);
        });
    });

    describe('FETCH_PROJECT_SUCCEEDED action', () => {
        // given
        const project = {
            projectName: 'Test 1',
        };
        // when
        const result = projectsContainerReducer(initialState, {
            type: FETCH_PROJECT_SUCCEEDED,
            payload: {
                ...{
                    project,
                },
            },
        });

        it('should set success state to true', () => {
            // then
            expect(result.get('success')).toEqual(true);
        });

        it('should set current project state', () => {
            // then
            expect(result.get('currentProject')).toEqual(project);
        });
    });

    describe('FETCH_PROJECT_FAILED action', () => {
        // given
        const exception = new Error('An Exception occurred');
        const currentState = fromJS({
            ...initialState,
            success: true,
        });
        // when
        const result = projectsContainerReducer(currentState, {
            type: FETCH_PROJECT_FAILED,
            message: exception.message,
        });

        it('should set success to false', () => {
            // then
            expect(result.get('success')).toEqual(false);
        });
    });

    describe('POPULATE_CURRENT_PROJECT action', () => {
        it('should set currentProject in store', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            // when
            const result = projectsContainerReducer(initialState, {
                type: POPULATE_CURRENT_PROJECT,
                payload: {
                    project,
                },
            });
            // then
            expect(result.get('currentProject')).toEqual(project);
        });
    });

    xdescribe('UPDATE_CURRENT_PROJECT_BY_PROPERTY action', () => {
        it('should update properties in currentProject', () => {
            // given
            const id = '45564654456564';
            const name = 'Testing Project';
            const updatedName = 'Project Updated';
            const currentProject = {
                id,
                name,
            };
            const currentState = fromJS({
                ...initialState.toJS(),
                currentProject,
            });
            const action = {
                type: UPDATE_CURRENT_PROJECT_BY_PROPERTY,
                key: 'name',
                value: updatedName,
            };
            const expectedState = {
                ...initialState.toJS(),
                currentProject: {
                    ...currentProject,
                    name: updatedName,
                },
            };
            // when
            const result = projectsContainerReducer(currentState, action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });
    });

    describe('UPDATE_PROJECT_PROPERTIES_BY_ID action type', () => {
        it('should update properties in store by id', () => {
            // given
            const projectId = '645465543423';
            const properties = {
                environment: 'production',
                name: 'Testing change',
            };
            const currentState = {
                ...initialState.toJS(),
                projects: [
                    {id: '345465456645', name: 'Testing', environment: 'staging'},
                    {id: projectId, name: 'Testing', environment: 'staging'},
                ],
            };
            const action = {
                type: UPDATE_PROJECT_PROPERTIES_BY_ID,
                projectId,
                properties,
            };
            const expectedState = {
                ...initialState.toJS(),
                projects: [
                    {id: '345465456645', name: 'Testing', environment: 'staging'},
                    {id: projectId, ...properties},
                ],
            };
            // when
            const result = projectsContainerReducer(fromJS(currentState), action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });
    });

    describe('UPDATE_CURRENT_PROJECT_PROPERTIES action type', () => {
        it('should update current project properties', () => {
            // given
            const projectId = '4356435587';
            const action = {
                type: UPDATE_CURRENT_PROJECT_PROPERTIES,
                properties: {
                    name: 'Update project name',
                    description: 'Update project description',
                },
            };
            const currentState = fromJS({
                ...initialState.toJS(),
                currentProject: {
                    id: projectId,
                    name: 'Project name',
                    description: 'Project description',
                },
            });
            const expectedState = {
                ...initialState.toJS(),
                currentProject: {
                    id: projectId,
                    name: action.properties.name,
                    description: action.properties.description,
                },
            };
            // when
            const result = projectsContainerReducer(currentState, action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });
    });

    describe('DELETE_PROJECT action type', () => {
        it('should set projectLoading state', () => {
            // given
            const action = projectDeleteLoading(true);
            // when
            const result = projectsContainerReducer(initialState, action);
            // then
            expect(result.toJS().deletingProject).toBe(true);
        });
    });

    describe('should handle SET_CURRENT_PROJECT_TRANSACTION_ID and set id', () => {
        // given
        const transactionId = '456565675675675678';
        const action = setCurrentProjectTransactionId(transactionId);
        // when
        const result = projectsContainerReducer(initialState, action);
        // then
        expect(result.get('projectTransactionId')).toEqual(transactionId);
    });

    describe('should handle SET_PROJECT_ID and set project id', () => {
        // given
        const projectId = '123123123312';
        const action = setProjectId(projectId);
        // when
        const result = projectsContainerReducer(initialState, action);
        // then
        expect(result.get('selectedProjectId')).toEqual(projectId);
    });

    describe('should handle FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED', () => {
        // given
        const currentState = {
            ...initialState.toJS(),
            projects: [Project1, Project2],
        };
        const categories = [];
        const projectId = Project1.id;

        const action = fetchSelectedProjectCategoriesSucceeded(categories, projectId);
        // when
        const result = projectsContainerReducer(fromJS(currentState), action);

        // then
        expect(result.get('projects').get(1).get('categories')).toEqual(fromJS(categories));
    });

    describe('should handle FETCH_PROJECT_SUMMARY_SUCCEEDED', () => {
        // given
        const currentState = {
            ...initialState.toJS(),
        };
        const action = fetchProjectSummarySucceeded(projectSummary);
        const result = projectsContainerReducer(fromJS(currentState), action);
        expect(result.get('currentProjectSummary').projectSummary).toEqual(projectSummary);
    });

    describe('should handle FETCH_PROJECT_SUMMARY_FAILED', () => {
        // given
        const currentState = {
            ...initialState.toJS(),
        };
        const action = fetchProjectSummaryFailed('');
        const result = projectsContainerReducer(fromJS(currentState), action);
        expect(result.get('currentProjectSummary')).toEqual(undefined);
    });

    describe('should handle setSortValues action', () => {
        const fields = [
            new DataField({id: 'name', label: 'Project Name', sortable: true, type: DataType.STRING}),
            new DataField({id: 'numberOfFiles', label: '# of files', sortable: true, type: DataType.NUMBER}),
        ];

        const sortAscending = [{id: 'name', orderBy: SortDirections.ASCENDING}];
        const sortDescending = [{id: 'name', orderBy: SortDirections.DESCENDING}];

        const actionAscending = {
            type: SET_SORT_VALUES,
            fields,
            sort: sortAscending,
        };

        const actionDescending = {
            ...actionAscending,
            sort: sortDescending,
        };

        const {sortFn: sortFnAscending} = projectsContainerReducer(initialState, actionAscending).toJS();
        const {sortFn: sortFnDescending} = projectsContainerReducer(initialState, actionDescending).toJS();

        it('should be able to sort ascending', () => {
            expect([Project2, Project1].sort(sortFnAscending)).toEqual([Project1, Project2]);
        });

        it('should be able to sort descending', () => {
            expect([Project1, Project2].sort(sortFnDescending)).toEqual([Project2, Project1]);
        });
    });
});
