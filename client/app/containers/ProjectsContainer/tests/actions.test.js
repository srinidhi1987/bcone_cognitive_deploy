import {
    cacheProject,
    clearCurrentProject,
    deleteProject,
    fetchProjectById,
    fetchProjectByIdFailed,
    fetchProjectByIdSucceeded,
    fetchProjects,
    fetchProjectsFailed,
    fetchProjectsSucceeded,
    launchProjectDesigner,
    launchProjectValidator,
    loadingProjects,
    projectDeleteLoading,
    projectCategoriesLoading,
    populateProjectFieldsCache,
    populateCurrentProject,
    pullProjectById,
    removeCachedProject,
    selectProject,
    setProjectId,
    setSortValues,
    setCurrentProjectTransactionId,
    updateCurrentProjectByProperties,
    updateCurrentProjectByProperty,
    updateProjectPropertiesById,
    fetchProjectSummaryById,
    fetchProjectSummarySucceeded,
    fetchProjectSummaryFailed,
    uploadFiles,
} from '../actions';
import {
    CACHE_PROJECT,
    CLEAR_CURRENT_PROJECT,
    DELETE_PROJECT,
    DELETE_PROJECT_LOADING,
    FETCH_PROJECT_BY_ID,
    FETCH_PROJECT_FAILED,
    FETCH_PROJECT_SUCCEEDED,
    FETCH_PROJECTS,
    FETCH_PROJECTS_FAILED,
    FETCH_PROJECTS_SUCCEEDED,
    LAUNCH_PROJECT_DESIGNER,
    LAUNCH_PROJECT_VALIDATOR,
    LOADING_PROJECTS,
    POPULATE_CURRENT_PROJECT,
    POPULATE_PROJECT_FIELDS_CACHE,
    PROJECT_CATEGORIES_LOADING,
    PULL_PROJECT_BY_ID,
    REMOVE_PROJECT_CACHE,
    SELECT_PROJECT,
    SET_CURRENT_PROJECT_TRANSACTION_ID,
    SET_PROJECT_ID,
    UPDATE_CURRENT_PROJECT_BY_PROPERTY,
    UPDATE_CURRENT_PROJECT_PROPERTIES,
    UPDATE_PROJECT_PROPERTIES_BY_ID,
    FETCH_PROJECT_SUMMARY,
    FETCH_PROJECT_SUMMARY_SUCCEEDED,
    FETCH_PROJECT_SUMMARY_FAILED,
    SET_SORT_VALUES,
    UPLOAD_PROJECT_FILES,
} from '../constants';
import {ProjectFields1} from '../../../../fixtures/project_fields';
import {Project1, Project2} from '../../../../fixtures/projects';
import {projectSummary} from '../../../../fixtures/project_summary';
import {DataField, DataType} from 'common-frontend-utils';

describe('ProjectsContainer actions', () => {
    describe('fetchProjects action creator', () => {
        it('has a type of FETCH_PROJECTS', () => {
            const expected = {
                type: FETCH_PROJECTS,
            };

            expect(fetchProjects()).toEqual(expected);
        });
    });

    describe('fetchProjectsSucceeded action creator', () => {
        it('should have a type of FETCH_PROJECTS_SUCCEEDED', () => {
            // given
            const expected = {
                type: FETCH_PROJECTS_SUCCEEDED,
            };
            // when
            const result = fetchProjectsSucceeded();
            // then
            expect(result).toMatchObject(expected);
        });

        it('should pass projects data to creator', () => {
            // given
            const expected = {
                projects: [{}, {}, {}],
            };
            // when
            const result = fetchProjectsSucceeded(expected.projects);
            // then
            expect(result.payload.projects.length).toEqual(expected.projects.length);
        });
    });

    describe('loadingProjects action creator', () => {
        it('should set loading state to true', () => {
            // given
            const expected = {
                type: LOADING_PROJECTS,
                loading: true,
            };
            // when
            const result = loadingProjects(true);
            // then
            expect(result).toEqual(expected);
        });

        it('should set loading state to false', () => {
            // given
            const expected = {
                type: LOADING_PROJECTS,
                loading: false,
            };
            // when
            const result = loadingProjects(false);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('fetchProjectsFailed action', () => {
        it('should have type: FETCH_PROJECTS_FAILED', () => {
            // given
            const expected = {
                type: FETCH_PROJECTS_FAILED,
            };
            // when
            const result = fetchProjectsFailed();
            // then
            expect(result).toEqual({type: expected.type});
        });
    });

    describe('cacheProject action creator', () => {
        it('should pass project to cache in store', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            const expected = {
                type: CACHE_PROJECT,
                payload: {
                    project,
                },
            };
            // when
            const result = cacheProject(project);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('clearCurrentProject action creator', () => {
        it('should have type: CLEAR_CURRENT_PROJECT', () => {
            // given
            const expected = {
                type: CLEAR_CURRENT_PROJECT,
            };
            // when
            const result = clearCurrentProject();
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('removeCachedProject action creator', () => {
        it('should pass type of REMOVE_CACHED_PROJECT', () => {
            // given
            const expected = {
                type: REMOVE_PROJECT_CACHE,
            };
            // when
            const result = removeCachedProject();
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('projectCategoriesLoading action creator when called with boolean', () => {
        it('should pass type of PROJECT_CATEGORIES_LOADING', () => {
            // given
            const expected = {
                type: PROJECT_CATEGORIES_LOADING,
                loading: true,
            };
            // when
            const result = projectCategoriesLoading(true);
            // then
            expect(result).toEqual(expected);
        });
    });


    describe('populateProjectFields action creator', () => {
        it('should have type POPULATE_PROJECT_FIELDS', () => {
            // given
            // when
            const result = populateProjectFieldsCache();
            // then
            expect(result.type).toEqual(POPULATE_PROJECT_FIELDS_CACHE);
        });

        it('should store passed in project fields', () => {
            // given
            const expected = {
                type: POPULATE_PROJECT_FIELDS_CACHE,
                payload: {
                    fields: ProjectFields1,
                },
            };
            // when
            const result = populateProjectFieldsCache(ProjectFields1);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('fetchProjectById action creator', () => {
        it('should pass in projectId', () => {
            // given
            const projectId = 100;
            const expected = {
                type: FETCH_PROJECT_BY_ID,
                projectId,
            };
            // when
            const result = fetchProjectById(projectId);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('fetchProjectByIdSucceeded action creator', () => {
        it('should handle passing project with success', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            const expected = {
                type: FETCH_PROJECT_SUCCEEDED,
                payload: {
                    ...{
                        project,
                    },
                },
            };
            // when
            const result = fetchProjectByIdSucceeded(project);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('fetchProjectByIdSucceeded action creator', () => {
        it('should pass failure message', () => {
            // given
            const message = new Error('An Exception occurred');
            const expected = {
                type: FETCH_PROJECT_FAILED,
                message,
            };
            // when
            const result = fetchProjectByIdFailed(message);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('populateCurrentProject action creator', () => {
        it('should set currentProject in state', () => {
            // given
            const project = {
                projectName: 'Test 1',
            };
            const expected = {
                type: POPULATE_CURRENT_PROJECT,
                payload: {
                    project,
                },
            };
            // when
            const result = populateCurrentProject(project);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('updateCurrentProjectByProperty action creator', () => {
        it('should send property and value to be updated', () => {
            // given
            const key = 'name';
            const value = 'Testing';
            const expectedAction = {
                type: UPDATE_CURRENT_PROJECT_BY_PROPERTY,
                key,
                value,
            };
            // when
            const result = updateCurrentProjectByProperty(key, value);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('pullProjectById action creator', () => {
        it('should send type with projectId to call server for updated project', () => {
            // given
            const projectId = 'r2343453646554t';
            const expectedAction = {
                type: PULL_PROJECT_BY_ID,
                projectId,
            };
            // when
            const result = pullProjectById(projectId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('updateProjectPropertiesById action creator', () => {
        it('should project id and properties', () => {
            // given
            const projectId = '4655878677654';
            const properties = {
                environment: 'production',
                name: 'Testing',
            };
            const expectedAction = {
                type: UPDATE_PROJECT_PROPERTIES_BY_ID,
                projectId,
                properties,
            };
            // when
            const result = updateProjectPropertiesById(projectId, properties);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('updateCurrentProjectByProperties action creator', () => {
        it('should send project to be updated in store', () => {
            // given
            const properties = {
                name: 'Testing Update',
                description: 'Testing description update',
            };
            const expectedAction = {
                type: UPDATE_CURRENT_PROJECT_PROPERTIES,
                properties,
            };
            // when
            const result = updateCurrentProjectByProperties(properties);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('launchValidator action creator', () => {
        it('should pass projectId for action', () => {
            // given
            const projectId = '53475678876876';
            const expectedAction = {
                type: LAUNCH_PROJECT_VALIDATOR,
                projectId,
            };
            // when
            const result = launchProjectValidator(projectId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('launchDesigner action creator', () => {
        it('should pass projectId and categoryId for action', () => {
            // given
            const projectId = '53475678876876';
            const categoryId = '656454585867';
            const expectedAction = {
                type: LAUNCH_PROJECT_DESIGNER,
                projectId,
                categoryId,
            };
            // when
            const result = launchProjectDesigner(projectId, categoryId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('deleteProject action creator', () => {
        it('should pass the correct type and project id', () => {
            // given
            const projectId = '6454567765453345';
            const expectedAction = {
                type: DELETE_PROJECT,
                projectId,
            };
            // when
            const result = deleteProject(projectId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('projectDeleteLoading action creator', () => {
        // given
        const expectedAction = {
            type: DELETE_PROJECT_LOADING,
            loading: true,
        };
        // when
        const result = projectDeleteLoading(true);
        // then
        expect(result).toEqual(expectedAction);
    });

    describe('setCurrentProjectTransactionId action creator', () => {
        it('should set active transactionId to track file and classification processing', () => {
            // given
            const transactionId = '4656545634542323234';
            const expectedAction = {
                type: SET_CURRENT_PROJECT_TRANSACTION_ID,
                transactionId,
            };
            // when
            const result = setCurrentProjectTransactionId(transactionId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('selectProject action creator', () => {
        it('should find project with matching name and return it', () => {
            // given
            const selectedProject = Project2;
            const projects = [Project1, Project2];
            const query = {limit: 250};
            const expectedAction = {
                type: SELECT_PROJECT,
                project: selectedProject,
                query,
            };
            // when
            const result = selectProject(projects, Project2.name, query);

            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('setProjectId action creator', () => {
        it('should pass through projectId', () => {
            // given
            const projectId = Project1.id;
            const expectedAction = {
                type: SET_PROJECT_ID,
                projectId,
            };
            // when
            const result = setProjectId(projectId);

            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('fetchProjectSummaryById action creator', () => {
        it('should pass through projectId', () => {
            // given
            const projectId = Project1.id;
            const expectedAction = {
                type: FETCH_PROJECT_SUMMARY,
                projectId,
            };
            // when
            const result = fetchProjectSummaryById(projectId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('fetchProjectSummarySucceeded action creator', () => {
        it('should pass projectSummary in payload', () => {
            // given
            const expectedAction = {
                type: FETCH_PROJECT_SUMMARY_SUCCEEDED,
                payload: {
                    projectSummary,
                },
            };
            // when
            const result = fetchProjectSummarySucceeded(projectSummary);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('fetchProjectSummaryFailed action creator', () => {
        it('should pass error message with correct action', () => {
            // given
            const expectedAction = {
                type: FETCH_PROJECT_SUMMARY_FAILED,
                message: '',
            };
            // when
            const result = fetchProjectSummaryFailed('');
            // then
            expect(result).toEqual(expectedAction);
        });
    });
    describe('setSortValues action creator', () => {
        it('should create the correct setSortValues action', () => {
            const fields = [
                new DataField({id: 'projectName', label: 'Instance', sortable: true, type: DataType.STRING}),
                new DataField({id: 'status', label: 'Status', sortable: true, type: DataType.STRING}),
            ];

            const sort = [{id: 'name', orderBy: 'desc'}];

            const expectedAction = {
                type: SET_SORT_VALUES,
                fields,
                sort,
            };

            const result = setSortValues(fields, sort);

            expect(result).toEqual(expectedAction);
        });
    });
    describe('uploadFiles action creator', () => {
        it('should create the correct action for upload files', () => {
            const id = '12121', files = [];
            const expectedAction = {
                type: UPLOAD_PROJECT_FILES,
                payload: {
                    id,
                    files,
                },
            };
            const result = uploadFiles(id, files);
            expect(result).toEqual(expectedAction);
        });
    });
});
