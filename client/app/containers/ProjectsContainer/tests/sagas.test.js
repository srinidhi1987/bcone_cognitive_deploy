/**
 * ProjectsContainer sagas
 */
jest.mock('../../../utils/logger');
import {browserHistory} from 'react-router';
import {call, put, take} from 'redux-saga/effects';
import * as cookie from 'react-cookie';

import {
    FETCH_PROJECTS,
    FETCH_PROJECT_CATEGORIES,
    FETCH_PROJECT_DOMAINS,
    FETCH_PROJECT_BY_ID,
    PULL_PROJECT_BY_ID,
    LAUNCH_PROJECT_DESIGNER,
    LAUNCH_PROJECT_VALIDATOR,
    DELETE_PROJECT,
    SELECT_PROJECT,
    FETCH_PROJECT_SUMMARY,
} from '../constants';
import {
    checkFileUploadStatusFromServer,
    checkProjectFileUploadStatus,
    deleteProjectFlow,
    deleteProjectFromServer,
    fetchProject,
    fetchProjectCategoriesFlow,
    fetchProjectCategoriesFromServer,
    fetchProjectFields,
    fetchProjectFieldsFromServer,
    fetchProjectFieldsSaga,
    fetchProjects,
    fetchProjectsFromServer,
    fetchProjectsSaga,
    launchProjectDesigner,
    launchProjectValidator,
    pullProjecyByIdSaga,
    pullProjectByIdFlow,
    selectProjectFlow,
    uploadProjectFiles,
    uploadProjectFilesToServer,
    fetchProjectSummaryByIdSaga,
    fetchProjectSummary,
    fetchProjectSummaryFromServer,
} from '../sagas';
import {
    fetchCurrentProjectCategoriesSucceeded,
    fetchSelectedProjectCategoriesSucceeded,
    fetchProjectByIdFailed,
    fetchProjectByIdSucceeded,
    fetchProjectCategories,
    fetchProjectsFailed,
    fetchProjectsSucceeded,
    loadingProjects,
    populateProjectFieldsCache,
    projectCategoriesLoading,
    projectDeleteLoading,
    selectProject,
    setCurrentProjectTransactionId,
    setProjectId,
    updateQueryParams,
    fetchProjectSummarySucceeded,
} from '../actions';
import {projectsBackEnd} from '../../../resources/projectsBackEnd';
import {displayAlert} from '../../AlertsContainer/actions';
import {ProjectFields1} from '../../../../fixtures/project_fields';
import {Project1, Project2} from '../../../../fixtures/projects';
import {projectSummary} from '../../../../fixtures/project_summary';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import routeHelper from '../../../utils/routeHelper';

const jsonParseSpy = jest.spyOn(JSON, 'parse');

describe('Project sagas', () => {
    afterEach(() => {
        jest.clearAllMocks();
        jsonParseSpy.mockReset();
    });

    describe('fetchProjects generator', () => {
        const projectArray = [Project1, Project2];

        describe('successful fetch', () => {
            const fetchProjectsGenerator = fetchProjects();

            it('should set loading to true', () => {
                const loadingDescriptor = fetchProjectsGenerator.next().value;

                expect(loadingDescriptor).toEqual(put(loadingProjects(true)));
            });

            it('should fetch projects from server', () => {
                const fetchProjectsDescriptor = fetchProjectsGenerator.next();

                expect(fetchProjectsDescriptor.value).toEqual(call(fetchProjectsFromServer));
            });

            it('should pass projects to reducer', () => {
                const projects = {
                    data: projectArray,
                    success: true,
                    errors: [],
                };
                const result = fetchProjectsGenerator.next(projects);
                expect(result.value).toEqual(put(fetchProjectsSucceeded(projects.data)));
            });

            it('should put first project in store and trigger selectProject action (a saga)', () => {
                const result = fetchProjectsGenerator.next();
                expect(result.value).toEqual(put(selectProject(projectArray, projectArray[0].name, {
                    limit: 10,
                    sort: [
                        {
                            id: 'index',
                            orderBy: 'desc',
                        },
                    ],
                })));
            });

            it('should set loading to false', () => {
                const loadingDescriptor = fetchProjectsGenerator.next().value;

                expect(loadingDescriptor).toEqual(put(loadingProjects(false)));
            });
        });

        describe('failure on fetch', () => {
            it('should NOT set selected project if data set is empty', () => {
                const fetchProjectsGenerator = fetchProjects();
                const response = {
                    data: [],
                    success: false,
                };
                // set loading
                fetchProjectsGenerator.next();
                // set as empty projects from server call
                fetchProjectsGenerator.next(response);
                // set succeeded and populate store
                fetchProjectsGenerator.next();
                // condition to check for populated projects
                // will not call put for select project if projects are empty
                // will set loading to false

                expect(fetchProjectsGenerator.next().value).toEqual(put(loadingProjects(false)));
            });

            it('should call failed action creator', () => {
                // given
                const fetchProjectsGenerator = fetchProjects();
                // loading projects
                fetchProjectsGenerator.next();
                // when
                const fetchFailedDescriptor = fetchProjectsGenerator.throw().value;
                // then
                expect(fetchFailedDescriptor).toEqual(put(fetchProjectsFailed()));
            });
        });
    });

    describe('fetchProjectsSaga', () => {
        const fetchProjectsSagaGenerator = fetchProjectsSaga();

        it('should takeLatest on fetchProjects generator', () => {
            // given
            // when
            const result = fetchProjectsSagaGenerator.next();
            // then
            expect(result.value).toEqual(take(FETCH_PROJECTS, fetchProjects));
        });
    });

    describe('fetchProjectFields generator', () => {
        describe('success path', () => {
            const generator = fetchProjectFields();

            it('should fetch project fields from server', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchProjectFieldsFromServer));
            });

            it('should populate fields in store', () => {
                // given
                // when
                const result = generator.next({
                    success: true,
                    data: ProjectFields1,
                });
                // then
                expect(result.value).toEqual(put(populateProjectFieldsCache(ProjectFields1)));
            });
        });
    });

    describe('fetchProject generator', () => {
        // current action will be passed from saga to handler
        const actionData = {
            type: FETCH_PROJECT_BY_ID,
            projectId: 1000,
        };

        describe('success path', () => {
            const generator = fetchProject(actionData);

            it('should set loading state', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingProjects(true)));
            });

            it('should call api to fetch project by id', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(projectsBackEnd.get, actionData.projectId));
            });

            it('should set success on fetching project and pass payload', () => {
                // given
                const projectData = {
                    data: {
                        projectName: 'Test 1',
                    },
                };
                // when
                const result = generator.next(projectData);
                // then
                expect(result.value).toEqual(put(fetchProjectByIdSucceeded(projectData.data)));
            });
        });

        describe('failure path', () => {
            const generator = fetchProject(actionData);

            it('should set edit project as failed when exception is raised', () => {
                // given
                generator.next();
                const exception = new Error('An Exception occurred');
                // when
                const result = generator.throw(exception);
                // then
                expect(result.value).toEqual(put(fetchProjectByIdFailed(exception.message)));
            });
        });
    });

    describe('fetchProjectFieldsSaga', () => {
        const generator = fetchProjectFieldsSaga();

        it('should takeLatest on fetchProjectFields generator', () => {
            // given
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(take(FETCH_PROJECT_DOMAINS, fetchProjectFields));
        });
    });

    describe('pullProjecyByIdSaga Saga', () => {
        it('should listen to PULL_PROJECT_BY_ID action type', () => {
            // given
            const saga = pullProjecyByIdSaga();
            // when
            const result = saga.next();
            // then
            expect(result.value).toEqual(take(PULL_PROJECT_BY_ID, pullProjectByIdFlow));
        });
    });

    describe('pullProjectByIdFlow generator', () => {
        describe('success path', () => {
            // given
            const projectId = '34554456765';
            const action = {
                type: PULL_PROJECT_BY_ID,
                projectId,
            };
            const generator = pullProjectByIdFlow(action);

            it('should call API for project data', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(projectsBackEnd.get, action.projectId));
            });

            it('should set success and persist project', () => {
                // given
                const response = {
                    success: true,
                    data: {
                        id: 'trtyrtyytuuty',
                        name: 'Project Name',
                    },
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(fetchProjectByIdSucceeded(response.data)));
            });
        });
    });

    describe('launchers', () => {
        let cookieSpy, token;

        beforeEach(() => {
            // given
            cookieSpy = jest.spyOn(cookie, 'save');
            jest.spyOn(window.location, 'assign').mockImplementation(() => {});
            token = 234253849834278394;
            localStorage.setItem(APP_STORAGE.token, token);

        });

        describe('launchProjectValidator', () => {
            let action;

            beforeEach(() => {
                //given
                action = {
                    type: LAUNCH_PROJECT_VALIDATOR,
                    projectId: '6565676767',
                };
            });

            it('should save cookie for auth permissions', () => {
                // when
                launchProjectValidator(action);
                // then
                expect(cookieSpy).toHaveBeenCalledWith('aa-validator', {
                    projectId: action.projectId,
                });
            });

            it('should redirect to download validator executable', () => {
                // when
                launchProjectValidator(action);
                // then
                expect(window.location.assign).toHaveBeenCalledWith(
                    routeHelper(`/api/projects/${action.projectId}/validator?token=${token}`)
                );
            });
        });

        describe('launchProjectDesigner', () => {
            let action;

            beforeEach(() => {
                // given
                action = {
                    type: LAUNCH_PROJECT_DESIGNER,
                    projectId: '6565676767',
                    categoryId: '345456567765',
                };
            });

            it('should save cookie for auth permissions', () => {
                // when
                launchProjectDesigner(action);
                // then
                expect(cookieSpy).toHaveBeenCalledWith('aa-designer', {
                    projectId: action.projectId,
                    categoryId: action.categoryId,
                });
            });

            it('should redirect to download designer executable', () => {
                // when
                launchProjectDesigner(action);
                // then
                expect(window.location.assign).toHaveBeenCalledWith(
                    routeHelper(`/api/projects/${action.projectId}/categories/${action.categoryId}/designer?token=${token}`)
                );
            });
        });
    });

    describe('uploadProjectFiles generator', () => {
        // given
        const projectId = '456645546654645';
        const files = [{}, {}];
        const generator = uploadProjectFiles(projectId, files);

        it('should call upload files to server', () => {
            // given
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(call(uploadProjectFilesToServer, projectId, files));
        });

        it('should persist transaction id in store', () => {
            // given
            const transactionId = '6456567567576567';
            const apiResponse = {
                response: {
                    data: {
                        fileTransactionId: transactionId,
                    },
                },
            };
            jsonParseSpy.mockImplementation(() => apiResponse.response);
            // when
            const result = generator.next(JSON.stringify(apiResponse));
            // then
            expect(result.value).toEqual(put(setCurrentProjectTransactionId(transactionId)));
        });
    });

    describe('checkFileUploadStatusFromServer generator', () => {
        // given
        const projectId = '645645456645456645';
        const transactionId = '566767864545453345';
        const files = [{}, {}, {}];
        const generator = checkProjectFileUploadStatus(projectId, files.length, transactionId);

        it('should call API to check for file status', () => {
            // given
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(call(checkFileUploadStatusFromServer, projectId, transactionId));
        });
    });

    describe('deleteProjectFlow generator', () => {
        // given
        const action = {
            type: DELETE_PROJECT,
            projectId: '4354656577656765',
        };

        describe('success path', () => {
            // given
            const user = {
                name: 'Services',
                roles: ['Services'],
                username: 'Services',
            };

            // when
            const generator = deleteProjectFlow(action);

            it('should set delete loading to true', () => {
                // given
                jsonParseSpy.mockImplementation(() => user);
                // when
                const result = generator.next(user);
                // then
                expect(result.value).toEqual(put(projectDeleteLoading(true)));
            });

            it('should call API to remove project', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(deleteProjectFromServer, action.projectId, user.username));
            });

            it('should display success message', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(displayAlert('Learning instance was successfully removed')));
            });

            it('should redirect to project listings', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(browserHistory.push(routeHelper('/learning-instances')));
            });

            it('should set delete loading to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(projectDeleteLoading(false)));
            });
        });

        describe('failure path', () => {
            // given
            // when
            const generator = deleteProjectFlow(action);

            it('should call displayAlert if generator throws an exception', () => {
                // given
                // when
                generator.next();
                const result = generator.throw();
                // then
                expect(result.value).toEqual(put(displayAlert('Removing this project failed. Please try again.')));
            });

            it('should set delete loading to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(projectDeleteLoading(false)));
            });
        });
    });

    describe('selectProjectFlow generator', () => {

        describe('success path', () => {
            //given
            const query = {limit: 10};
            const action = {
                type: SELECT_PROJECT,
                project: Project1,
                query,
            };

            const generator = selectProjectFlow(action);

            it('should put the selected project id in store via setProjectId action', () => {
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(setProjectId(Project1.id)));
            });

            it('should fire action fetchProjectCategories with project id and query params as args', () => {
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(fetchProjectCategories(Project1.id, query)));
            });

            it('should be done on next call', () => {
                // when
                const result = generator.next();
                // then
                expect(result.done).toEqual(true);
            });
        });
    });

    describe('fetchProjectCategoriesFlow generator', () => {
        describe('success path', () => {
            // given
            const data = {
                categories: [],
            };
            const response = {
                data,
                success:true,
                errors: [],
            };

            describe('selected project path', () => {
                // given
                const action = {
                    type: FETCH_PROJECT_CATEGORIES,
                    projectId: '23423423242',
                    params: {
                        limit: 250,
                        offset: 0,
                        sort: [
                            {
                                id: 'index',
                                orderBy: 'desc',
                            },
                        ],
                    },
                };

                const generator = fetchProjectCategoriesFlow(action);

                it('should set category loading to true', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(projectCategoriesLoading(true)));
                });

                it('should call fetchProjectCategoriesFromServer with projectId and query params as argument', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(call(fetchProjectCategoriesFromServer, action.projectId, {...action.params, sort: '-index'}));
                });

                it('should put response data in store via fetchProjectCategoriesSucceeded action', () => {
                    //given

                    //when
                    const result = generator.next(response);
                    //then
                    expect(result.value).toEqual(put(fetchSelectedProjectCategoriesSucceeded(response.data, action.projectId)));
                });

                it('should set category loading to false', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(projectCategoriesLoading(false)));
                });

                it('should be done on next call', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.done).toEqual(true);
                });
            });

            describe('current project path', () => {
                // given
                const action = {
                    type: FETCH_PROJECT_CATEGORIES,
                    projectId: '23423423242',
                    params: {
                        offset: 0,
                        limit: 250,
                        sort: [
                            {
                                id: 'index',
                                orderBy: 'desc',
                            },
                        ],
                    },
                    isCurrentProject: true, // note this property being true
                };
                const generator = fetchProjectCategoriesFlow(action);

                it('should update query params in store', () => {
                    //when
                    generator.next(); // set category loading to true;
                    const result = generator.next();
                    //then
                    expect(result.value).toEqual(put(updateQueryParams(action.params)));
                });

                it('should put response data in store via fetchProjectCategoriesSucceeded action', () => {
                    //when
                    generator.next(); // skip call for data step
                    const result = generator.next(response);
                    //then
                    expect(result.value).toEqual(put(fetchCurrentProjectCategoriesSucceeded(data, action.params.sort)));
                });
            });

            describe('skipLoadingState path', () => {
                const action = {
                    type: FETCH_PROJECT_CATEGORIES,
                    projectId: '23423423242',
                    params: {
                        offset: 5,
                        limit: 20,
                        sort: [
                            {
                                id: 'index',
                                orderBy: 'desc',
                            },
                        ],
                    },
                    skipLoadingState: true,
                };

                const generator = fetchProjectCategoriesFlow(action);

                it('should call fetchProjectCategoriesFromServer with projectId and query params as argument', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(call(fetchProjectCategoriesFromServer, action.projectId, {...action.params, sort: '-index'}));
                });
            });
        });
    });

    describe('fetchProjectSummaryByIdSaga Saga', () => {
        it('should listen to FETCH_PROJECT_SUMMARY action type', () => {
            // given
            const saga = fetchProjectSummaryByIdSaga();
            // when
            const result = saga.next();
            // then
            expect(result.value).toEqual(take(FETCH_PROJECT_SUMMARY, projectSummary));
        });
    });

    describe('fetchProjectSummary generator', () => {
        describe('success path', () => {
            // given
            const data = {
                categories: [],
            };
            const response = {
                data,
                success:true,
                errors: [],
            };
            const action = {
                type: FETCH_PROJECT_SUMMARY,
                projectId: '23423423242',
            };
            const generator = fetchProjectSummary(action);
            it('should issue loading project action with true value', () => {
                const result = generator.next();
                expect(result.value).toEqual(put(loadingProjects(true)));
            });

            it('should call fetchProjectSummaryFromServer with project id', () => {
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchProjectSummaryFromServer, action.projectId));
            });

            it('should put response data in store via fetchProjectSummarySucceeded action', () => {
                //given

                //when
                const result = generator.next(response);
                //then
                expect(result.value).toEqual(put(fetchProjectSummarySucceeded(response.data)));
            });

            it('should issue loading project action with false value', () => {
                const result = generator.next();
                expect(result.value).toEqual(put(loadingProjects(false)));
            });
        });

        describe('fail path', () => {
            // given
            const data = {
                categories: [],
            };
            const response = {
                data,
                success:false,
                errors: [],
            };
            const action = {
                type: FETCH_PROJECT_SUMMARY,
                projectId: '23423423242',
            };
            const generator = fetchProjectSummary(action);
            it('should issue loading project action with true value', () => {
                const result = generator.next();
                expect(result.value).toEqual(put(loadingProjects(true)));
            });

            it('should call fetchProjectSummaryFromServer with project id', () => {
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchProjectSummaryFromServer, action.projectId));
            });

            it('should issue loading project action with false value', () => {
                const result = generator.next(response);
                expect(result.value).toEqual(put(loadingProjects(false)));
            });
        });
    });

});
