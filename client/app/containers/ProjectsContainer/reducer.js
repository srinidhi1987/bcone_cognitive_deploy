/*
 *
 * ProjectsContainer reducer
 *
 */

import {fromJS, List, Map} from 'immutable';

import {DataTable} from 'common-frontend-components';

import {
    CACHE_PROJECT,
    CLEAR_CURRENT_PROJECT,
    DELETE_PROJECT_LOADING,
    FETCH_CURRENT_PROJECT_CATEGORIES_SUCCEEDED,
    FETCH_PROJECT_FAILED,
    FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED,
    FETCH_PROJECT_SUCCEEDED,
    FETCH_PROJECTS_SUCCEEDED,
    LOADING_PROJECTS,
    POPULATE_PROJECT_FIELDS_CACHE,
    POPULATE_CURRENT_PROJECT,
    PROJECT_CATEGORIES_LOADING,
    REMOVE_PROJECT_CACHE,
    SET_CURRENT_PROJECT_TRANSACTION_ID,
    SET_PROJECT_ID,
    SET_SORT_VALUES,
    UPDATE_CURRENT_PROJECT_BY_PROPERTY,
    UPDATE_PROJECT_PROPERTIES_BY_ID,
    UPDATE_QUERY_PARAMS,
    UPDATE_CURRENT_PROJECT_PROPERTIES,
    FETCH_PROJECT_SUMMARY_SUCCEEDED,
    FETCH_PROJECT_SUMMARY_FAILED,
    PROJECT_SORT_KEY,
    CATEGORY_NAME,
    TOGGLE_PROJECT_STAGING_DELAY,
} from './constants';
import {SortDirections} from '../../constants/enums';
import {compareKey} from 'common-frontend-utils';


const initialState = fromJS({
    cachedProject: null,
    currentProject: null,
    currentProjectCategoryData: [],
    currentProjectCategoryOffset: 0,
    currentProjectCategoryLimit: 50,
    currentProjectCategorySort: [
        {
            id: CATEGORY_NAME,
            orderBy: SortDirections.ASCENDING,
        },
    ],
    deletingProject: false, // track if a project is being deleted
    loadingProjects: false,
    loadingCategories: false,
    projectFieldsCache: [],
    projectTransactionId: null, // track progress for files and classification
    projects: fromJS([]),
    projectsDelayingStage: fromJS({}),
    selectedProjectId: null,
    sort: [{id: PROJECT_SORT_KEY, orderBy: SortDirections.ASCENDING}],
    sortFn: compareKey(PROJECT_SORT_KEY),
    success: false, // track record fetching progress
});

function projectsContainerReducer(state = initialState, action) {
    switch (action.type) {
        case CACHE_PROJECT:
            return state.set('cachedProject', action.payload.project);
        case CLEAR_CURRENT_PROJECT:
            return state
                .set('currentProject', initialState.get('currentProject'))
                .set('currentProjectCategoryData', initialState.get('currentProjectCategoryData'));
        case DELETE_PROJECT_LOADING:
            return state.set('deletingProject', action.loading);
        case FETCH_PROJECT_FAILED:
            return state.set('success', false);
        case FETCH_PROJECT_SUCCEEDED:
            return state.set('success', true)
                .set('currentProject', action.payload.project);
        case FETCH_PROJECTS_SUCCEEDED: {
            const [{id}] = state.get('sort').toJS();
            const initiallySortedProjects = action.payload.projects.sort(compareKey(id));

            return state.set('projects', fromJS(initiallySortedProjects));
        }
        case FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED:
            // find project in array of projects and add 'categories' data for matching project
            return state.update('projects', (projects) => {
                return projects.map((project) => {
                    if (project.get('id') !== action.projectId) {
                        return project;
                    }

                    return project.set('categories', fromJS(action.payload.categories));
                });
            });
        case FETCH_CURRENT_PROJECT_CATEGORIES_SUCCEEDED:
            return state.set('currentProjectCategoryData', fromJS(action.payload.categories))
                        .set('currentProjectCategorySort', fromJS(action.sortOrder));
        case LOADING_PROJECTS:
            return state.set('loadingProjects', action.loading);
        case POPULATE_CURRENT_PROJECT:
            return state.set('currentProject', action.payload.project);
        case POPULATE_PROJECT_FIELDS_CACHE:
            return state.set('projectFieldsCache', action.payload.fields);
        case PROJECT_CATEGORIES_LOADING:
            return state.set('loadingCategories', action.loading);
        case REMOVE_PROJECT_CACHE:
            return state.set('cachedProject', null);
        case SET_PROJECT_ID:
            return state.set('selectedProjectId', action.projectId);
        case SET_CURRENT_PROJECT_TRANSACTION_ID:
            return state.set('projectTransactionId', action.transactionId);
        case SET_SORT_VALUES: {
            const {sort, fields} = action;
            const sortFn = DataTable.comparator(fields, sort);

            return state.set('sort', sort).set('sortFn', sortFn);
        }
        case UPDATE_CURRENT_PROJECT_BY_PROPERTY:
            if (state.get('currentProject')) {
                const project = state.get('currentProject');
                project[action.key] = action.value;
                return state.set('currentProject', project);
            }
            return state;
        case UPDATE_PROJECT_PROPERTIES_BY_ID:
            return state.set('projects', new List(state.get('projects').toJS().map((project) => {
                if (project.id === action.projectId) {
                    return fromJS({
                        ...project,
                        ...action.properties,
                    });
                }

                return fromJS(project);
            })));
        case UPDATE_QUERY_PARAMS:
            return state
                .set('currentProjectCategoryOffset', action.queryParams.offset)
                .set('currentProjectCategoryLimit', action.queryParams.limit);
        case UPDATE_CURRENT_PROJECT_PROPERTIES:
            return state.update('currentProject', (proj) => {
                const project = Map.isMap(proj) ? proj.toJS() : proj;

                return {
                    ...project,
                    ...action.properties,
                };
            });
        case TOGGLE_PROJECT_STAGING_DELAY: {
            const {id} = action;

            return state.update(
                'projectsDelayingStage',
                (projectsDelayingStage) => projectsDelayingStage.set(id, !projectsDelayingStage.get(id))
            );
        }
        case FETCH_PROJECT_SUMMARY_SUCCEEDED:
            return state.set('currentProjectSummary', action.payload);
        case FETCH_PROJECT_SUMMARY_FAILED:
            return state;
        default:
            return state;
    }
}

export default projectsContainerReducer;
export {initialState};
