import {takeLatest, delay} from 'redux-saga';
import {browserHistory} from 'react-router';
import {call, put} from 'redux-saga/effects';
import * as cookie from 'react-cookie';
import {HEADERS_JSON, compareKey} from 'common-frontend-utils';
import {requestGet, requestPost, requestDelete} from '../../utils/request';
import {projectsBackEnd} from '../../resources/projectsBackEnd';
import logger from '../../utils/logger';

import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import {CATEGORY_LIMIT_PREVIEW} from '../../containers/ProjectsContainer/constants';
import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {
    DELETE_PROJECT,
    FETCH_PROJECTS,
    FETCH_PROJECT_CATEGORIES,
    FETCH_PROJECT_BY_ID,
    FETCH_PROJECT_SUMMARY,
    FETCH_PROJECT_DOMAINS,
    LAUNCH_PROJECT_DESIGNER,
    LAUNCH_PROJECT_VALIDATOR,
    PULL_PROJECT_BY_ID,
    SELECT_PROJECT,
    SET_PROJECT_STAGE,
    SET_CURRENT_PROJECT_STAGE,
} from './constants';
import {
    projectCategoriesLoading,
    fetchCurrentProjectCategoriesSucceeded,
    fetchSelectedProjectCategoriesSucceeded,
    fetchProjectByIdFailed,
    fetchProjectByIdSucceeded,
    fetchProjectCategories,
    fetchProjectsFailed,
    fetchProjectsSucceeded,
    loadingProjects,
    populateProjectFieldsCache,
    projectDeleteLoading,
    selectProject,
    setCurrentProjectTransactionId,
    setProjectId,
    fetchProjectSummarySucceeded,
    fetchProjectSummaryFailed,
    updateQueryParams,
    toggleProjectStagingDelay,
    updateCurrentProjectByProperties,
    updateProjectPropertiesById,
} from './actions';
import {displayAlert} from '../AlertsContainer/actions';
import {patchProjectUpdateFromServer} from '../EditProjectContainer/sagas';
import {reloadAppRequest} from '../App/actions';
import {ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import routeHelper from '../../utils/routeHelper';
/**
 * Fetch projects from the API
 *
 * @returns {Promise}
 */
export function fetchProjectsFromServer() {
    return requestGet(
        routeHelper('api/projects'),
        {
            headers: HEADERS_JSON,
        },
    ).catch(handleApiError);
}

/**
 * Fetch projects
 *
 * @description This generator is used to control the flow of fetching and persisting projects
 */
export function* fetchProjects() {
    try {
        yield put(loadingProjects(true));

        const projects = yield call(fetchProjectsFromServer);
        if (projects.success) {
            const sortedProjectData = projects.data.sort(compareKey('name'));
            yield put(fetchProjectsSucceeded(sortedProjectData));
            yield put(selectProject(sortedProjectData, sortedProjectData[0].name, {
                limit: CATEGORY_LIMIT_PREVIEW,
                sort: [
                    {
                        id: 'index',
                        orderBy: 'desc',
                    },
                ],
            })); // picks first project; // picks first project;
        }
        else {
            yield put(fetchProjectsFailed());
        }
    }
    catch (e) {
        yield put(fetchProjectsFailed());
    }
    finally {
        yield put(loadingProjects(false));
    }
}

/**
 * Fetch projects from the API
 *
 * @param {string} projectId
 * @returns {Promise}
 */
export function fetchProjectFromServer(projectId) {
    return requestGet(
        routeHelper(`api/projects/${projectId}`),
        {
            headers: HEADERS_JSON,
        }
    ).then(handleApiResponse)
        .catch(handleApiError);
}

/**
 * Fetch a single project
 *
 * @param {object} action
 * @param {string} action.projectId
 * @description This generator is used to control the flow of fetching and persisting a project
 */
export function* fetchProject(action) {
    try {
        yield put(loadingProjects(true));
        const project = yield call(projectsBackEnd.get, action.projectId);
        yield put(fetchProjectByIdSucceeded(project.data));
    }
    catch (e) {
        yield put(fetchProjectByIdFailed(e.message));
    }
    finally {
        yield put(loadingProjects(false));
    }
}

/**
 * Fetch project fields from the API
 *
 * @returns {Promise}
 */
export function fetchProjectFieldsFromServer() {
    return requestGet(
        routeHelper('api/project-fields'),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

/**
 * Fetch project fields based on domain
 *
 * @description Generator to handle the control flow of fetching and persisting fields
 */
export function* fetchProjectFields() {
    try {
        const fields = yield call(fetchProjectFieldsFromServer);
        yield put(populateProjectFieldsCache(fields.data));
    }
    catch (e) {
        throw e;
    }
}

export function* fetchProjectsSaga() {
    yield* takeLatest(FETCH_PROJECTS, fetchProjects);
}

export function* fetchProjectFieldsSaga() {
    yield* takeLatest(FETCH_PROJECT_DOMAINS, fetchProjectFields);
}

export function* fetchProjectByIdSaga() {
    yield* takeLatest(FETCH_PROJECT_BY_ID, fetchProject);
}

/**
 * Check for file upload status by transaction id
 * @param {string} projectId
 * @param {string} transactionId
 * @returns {Promise}
 */
export function checkFileUploadStatusFromServer(projectId, transactionId) {
    return requestGet(
        routeHelper(`api/projects/${projectId}/files/status/${transactionId}`),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

/**
 * Upload project files to API
 *
 * @param {string} projectId
 * @param {FileList} files
 * @returns {Promise}
 */
export function uploadProjectFilesToServer(projectId, files) {
    if (!files) return new Promise((resolve) => resolve());

    const formData = new FormData();

    Object.keys(files).forEach((key) => {
        if (files[key] instanceof File) {
            formData.append('files', files[key], files[key].name);
        }
    });

    return requestPost(
        routeHelper(`api/projects/${projectId}/files`),
        {
            body: formData,
            isRawBody: true,
        }
    ).then(handleApiResponse)
     .catch(handleApiError);
}

/**
 * Check for file status
 *
 * @param {string} projectId
 * @param {string} transactionId
 */
export function* checkProjectFileUploadStatus(projectId, fileCount = 0, transactionId) {
    const status = yield call(checkFileUploadStatusFromServer, projectId, transactionId);
    if (status.errors === 'Data is not available' ||
        (status.hasOwnProperty('data') && status.data.totalFileCount !== fileCount)
    ) {
        yield call(checkProjectFileUploadStatus, projectId, fileCount, transactionId);
    }
}

/**
 * Upload files to a project
 *
 * @param {string} projectId
 * @param {FileList} files
 */
export function* uploadProjectFiles(projectId, files = []) {
    const response = yield call(uploadProjectFilesToServer, projectId, files);
    // track by transaction id
    if (response) {
        const transactionId = JSON.parse(response.response).data.fileTransactionId;
        // store transaction id for processing and file progress
        yield put(setCurrentProjectTransactionId(transactionId));
        yield call(checkProjectFileUploadStatus, projectId, files.length, transactionId);
    }
}

/**
 * Pull project resource
 *
 * @param {object} action
 * @param {string} action.projectId
 * @description Generator to handle control flow of pulling a project resource
 */
export function* pullProjectByIdFlow(action) {
    try {
        const project = yield call(projectsBackEnd.get, action.projectId);
        if (project.success) {
            yield put(fetchProjectByIdSucceeded(project.data));
        }
    }
    catch (e) {
        // Log but fail silently
        logger.error(e);
    }
}

/**
 * fetchProjectCategoriesFromServer
 * @param {string} projectId
 * @param {object} query
 * @returns {Promise.<Object>}
 */
export function fetchProjectCategoriesFromServer(projectId, query = {}) {
    return requestGet(
        routeHelper(`api/projects/${projectId}/categories`),
        {
            headers: HEADERS_JSON,
            query,
        },
    ).catch(handleApiError);
}

/**
 * fetchProjectCategoriesFlow
 * @param {object} action
 * @param {string} action.projectId - project id of categories to fetch
 * @param {object} action.params - query params for category API call
 * @param {boolean} action.isCurrentProject - boolean decides where in redux store data is stored
 * @param {boolean} action.skipLoadingState - boolean decides where in whether to turn loading state on
 */
export function* fetchProjectCategoriesFlow(action) {
    try {
        if (!action.skipLoadingState) yield put(projectCategoriesLoading(true));

        if (action.isCurrentProject) {
            yield put(updateQueryParams(action.params));
        }

        const result = yield call(fetchProjectCategoriesFromServer, action.projectId, reviseParams(action.params));

        if (result.success) {
            // different handling based on whether we're on a specific project page
            (yield action.isCurrentProject
                    ? put(fetchCurrentProjectCategoriesSucceeded(result.data, action.params.sort))
                    : put(fetchSelectedProjectCategoriesSucceeded(result.data, action.projectId))
            );
        }
    }
    finally {
        yield put(projectCategoriesLoading(false));
    }
}

export function reviseParams(params) {
    return {...params, sort: _reviseParams(params.sort)};

    function _reviseParams(sortArray) {
        return sortArray.reduce((acc, sortOrder, index) => {
            return `${acc}${sortOrder.orderBy === 'asc' ? '' : '-'}${sortOrder.id}${index + 1 !== sortArray.length ? ',' : ''}`;
        }, '');
    }
}

/**
 * selectProjectFlow
 * @param {object} action
 * @param {{id: string}} action.project
 * @param {object} action.query - query param object
 */
export function* selectProjectFlow(action) {
    yield put(setProjectId(action.project.id));
    yield put(fetchProjectCategories(action.project.id, action.query)); // action calls a saga
}

export function* pullProjecyByIdSaga() {
    yield* takeLatest(PULL_PROJECT_BY_ID, pullProjectByIdFlow);
}

/**
 * Launch project designer
 *
 * @param {object} action
 * @param {string} action.projectId
 * @param {string} action.categoryId
 * @description Launch external designer as an executable and set dynamic cookie
 * @returns void
 */
export function launchProjectDesigner(action) {
    try {
        // save cookie for authentication
        cookie.save('aa-designer', {
            projectId: action.projectId,
            categoryId: action.categoryId,
        });
        const token = localStorage.getItem(APP_STORAGE.token);
        // hit endpoint to force download designer
        window.location.assign(routeHelper(`/api/projects/${action.projectId}/categories/${action.categoryId}/designer?token=${token}`));
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
}

/**
 * Launch project validator
 *
 * @param {object} action
 * @param {string} action.projectId
 * @description Launch external validator as an executable and set dynamic cookie
 */
export function launchProjectValidator(action) {
    try {
        // save cookie for authentication
        cookie.save('aa-validator', {
            projectId: action.projectId,
        });
        const token = localStorage.getItem(APP_STORAGE.token);
        // hit endpoint to force download validator
        window.location.assign(routeHelper(`/api/projects/${action.projectId}/validator?token=${token}`));
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
}

/**
 * Call API to delete project
 *
 * @param {string} projectId
 * @returns {Promise}
 */
export function deleteProjectFromServer(projectId, username) {
    return requestDelete(
        routeHelper(`api/projects/${projectId}`),
        {
            headers: {
                ...HEADERS_JSON,
                updatedBy: username,
            },
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

/**
 * Delete project
 *
 * @param {object} action
 * @param {string} action.projectId
 * @description Generator to handle control flow for deleting a project
 */
export function* deleteProjectFlow(action) {
    try {
        const user = JSON.parse(localStorage.getItem(APP_STORAGE.user));

        yield put(projectDeleteLoading(true));
        yield call(deleteProjectFromServer, action.projectId, user.username);
        yield put(displayAlert('Learning instance was successfully removed'));
        // redirect after successful removal
        yield browserHistory.push(routeHelper('/learning-instances'));
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
        yield put(displayAlert('Removing this project failed. Please try again.'));
    }
    finally {
        yield put(projectDeleteLoading(false));
    }
}

/**
 * Fetch project summary
 * @param {object} action
 * @param {string} action.projectId
 * @description Generator to handle control flow for fetching a project summary
 */
export function fetchProjectSummaryFromServer(projectId) {
    return requestGet(
        routeHelper(`api/projects/${projectId}/detail-summary`),
        {
            headers: HEADERS_JSON,
        },
    ).catch(handleApiError);
}

/**
 * @description issue fetch summary request and issue respective action based on success or failure.
 * @param {object} action
 */
export function* fetchProjectSummary(action) {
    try {
        yield put(loadingProjects(true));
        const result = yield call(fetchProjectSummaryFromServer, action.projectId);
        if (result.success) {
            yield put(fetchProjectSummarySucceeded(result.data));
        }
    }
    catch (e) {
        yield put(fetchProjectSummaryFailed(e.message));
    }
    finally {
        yield put(loadingProjects(false));
    }
}

export function* fetchProjectSummaryByIdSaga() {
    yield* takeLatest(FETCH_PROJECT_SUMMARY, fetchProjectSummary);
}

/**
 * Fetch project summary
 * @param {object} action
 * @param {string} action.projectId
 * @param {string} action.stage
 * @description Generator to handle control flow for fetching a project summary
 */
export function* setProjectStageFlow(action) {
    const {id, stage, versionId} = action;
    const stageState = {environment: stage};
    yield put(toggleProjectStagingDelay(id));
    yield call(delay, 700);
    try {
        const update = yield call(patchProjectUpdateFromServer, id, stageState, versionId);
        //we have to reload app this for worst case
        if (!update.success) {
            update.statusCode === 409 ?
                yield put(reloadAppRequest()) :
                yield put(displayAlert(update.errors[0] || 'Error updating project', ALERT_LEVEL_DANGER));
            yield put(updateProjectPropertiesById(id, {versionId: update.data.versionId}));
            return;
        }
        //update version id for given projectid
        yield put(updateProjectPropertiesById(id, {...stageState, versionId: update.data.versionId}));
    }
    catch (e) {
        yield put(displayAlert('Error updating project', ALERT_LEVEL_DANGER));
    }
    finally {
        yield put(toggleProjectStagingDelay(id));
    }
}

export function* setCurrentProjectStageFlow(action) {
    const {id, stage, versionId} = action;
    const stageState = {environment: stage};

    yield put(toggleProjectStagingDelay(id));
    yield call(delay, 700);
    try {
        const update = yield call(patchProjectUpdateFromServer, id, stageState, versionId);
        //we have to reload app for worst case
        if (!update.success) {
            update.statusCode === 409 ?
                yield put(reloadAppRequest()) :
                yield put(displayAlert(update.errors[0] || 'Error updating project', ALERT_LEVEL_DANGER));
            yield put(updateCurrentProjectByProperties({
                versionId: update.data.versionId,
            }));
            return;
        }
        yield put(updateCurrentProjectByProperties(update.data)); // Optimistic UI
    }
    catch (e) {
        yield put(displayAlert('Error updating project', ALERT_LEVEL_DANGER));
    }
    finally {
        yield put(toggleProjectStagingDelay(id));
    }
}

// All sagas to be loaded
export default [
    fetchProjectsSaga,
    fetchProjectFieldsSaga,
    fetchProjectByIdSaga,
    pullProjecyByIdSaga,
    fetchProjectSummaryByIdSaga,
    function* () {
        yield* takeLatest(LAUNCH_PROJECT_DESIGNER, launchProjectDesigner);
    },
    function* () {
        yield* takeLatest(LAUNCH_PROJECT_VALIDATOR, launchProjectValidator);
    },
    function* () {
        yield* takeLatest(DELETE_PROJECT, deleteProjectFlow);
    },
    function* () {
        yield* takeLatest(SELECT_PROJECT, selectProjectFlow);
    },
    function* () {
        yield* takeLatest(FETCH_PROJECT_CATEGORIES, fetchProjectCategoriesFlow);
    },
    function* () {
        yield* takeLatest(SET_PROJECT_STAGE, setProjectStageFlow);
    },
    function* () {
        yield* takeLatest(SET_CURRENT_PROJECT_STAGE, setCurrentProjectStageFlow);
    },
];
