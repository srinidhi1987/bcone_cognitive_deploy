/*
 *
 * ProjectsContainer actions
 *
 */

import {
    PROJECT_CATEGORIES_LOADING,
    CACHE_PROJECT,
    CLEAR_CURRENT_PROJECT,
    DELETE_PROJECT,
    DELETE_PROJECT_LOADING,
    FETCH_CURRENT_PROJECT_CATEGORIES_SUCCEEDED,
    FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED,
    FETCH_PROJECT_DOMAINS,
    FETCH_PROJECT_BY_ID,
    FETCH_PROJECT_CATEGORIES,
    FETCH_PROJECT_FAILED,
    FETCH_PROJECT_SUCCEEDED,
    FETCH_PROJECTS,
    FETCH_PROJECTS_SUCCEEDED,
    FETCH_PROJECTS_FAILED,
    LAUNCH_PROJECT_DESIGNER,
    LAUNCH_PROJECT_VALIDATOR,
    LOADING_PROJECTS,
    POPULATE_CURRENT_PROJECT,
    POPULATE_PROJECT_FIELDS_CACHE,
    PULL_PROJECT_BY_ID,
    REMOVE_PROJECT_CACHE,
    SELECT_PROJECT,
    SET_CURRENT_PROJECT_TRANSACTION_ID,
    SET_PROJECT_ID,
    SET_PROJECT_STAGE,
    SET_CURRENT_PROJECT_STAGE,
    SET_SORT_VALUES,
    TOGGLE_PROJECT_STAGING_DELAY,
    UPDATE_CURRENT_PROJECT_BY_PROPERTY,
    UPDATE_CURRENT_PROJECT_PROPERTIES,
    UPDATE_PROJECT_PROPERTIES_BY_ID,
    FETCH_PROJECT_SUMMARY,
    FETCH_PROJECT_SUMMARY_SUCCEEDED,
    FETCH_PROJECT_SUMMARY_FAILED,
    UPDATE_QUERY_PARAMS,
    UPLOAD_PROJECT_FILES,
} from './constants';

export function fetchProjects() {
    return {
        type: FETCH_PROJECTS,
    };
}

/**
 * fetchProjectCategories
 * @param {string} projectId
 * @param {object} params - query params used to later fetch project categories
 * @param {boolean} isCurrentProject - determines where fetched categories are put in redux store
 * @returns {{type: string, projectId: string, params: object, isCurrentProject: boolean}}
 */
export function fetchProjectCategories(projectId, params = {}, isCurrentProject = false, skipLoadingState = false) {
    return {
        type: FETCH_PROJECT_CATEGORIES,
        projectId,
        params,
        isCurrentProject,
        skipLoadingState,
    };
}

export function fetchProjectsSucceeded(projects = []) {
    return {
        type: FETCH_PROJECTS_SUCCEEDED,
        payload: {
            projects,
        },
    };
}

export function fetchCurrentProjectCategoriesSucceeded(payload, sortOrder) {
    return {
        type: FETCH_CURRENT_PROJECT_CATEGORIES_SUCCEEDED,
        payload,
        sortOrder,
    };
}

/**
 * determines loading state of UI for categories
 * @param {boolean} loading
 * @returns {{type: string, loading: boolean}}
 */
export function projectCategoriesLoading(loading) {
    return {
        type: PROJECT_CATEGORIES_LOADING,
        loading,
    };
}
/**
 * fetchSelectedProjectCategoriesSucceeded
 * @param {object} payload
 * @param {string} projectId
 * @returns {{type: string, payload: object, projectId: string}}
 */
export function fetchSelectedProjectCategoriesSucceeded(payload, projectId) {
    return {
        type: FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED,
        payload,
        projectId,
    };
}

export function loadingProjects(loading = true) {
    return {
        type: LOADING_PROJECTS,
        loading,
    };
}

export function fetchProjectsFailed() {
    return {
        type: FETCH_PROJECTS_FAILED,
    };
}

/**
 * selectProject
 * @param {{name: string}[]} projects
 * @param {string} name
 * @param {object} query
 * @returns {{type, project, query}}
 */
export function selectProject(projects, name, query = {}) {
    const project = projects.find((proj) => proj.name === name);
    return {
        type: SELECT_PROJECT,
        project,
        query,
    };
}

export function cacheProject(project) {
    return {
        type: CACHE_PROJECT,
        payload: {
            project,
        },
    };
}

export function clearCurrentProject() {
    return {
        type: CLEAR_CURRENT_PROJECT,
    };
}

export function removeCachedProject() {
    return {
        type: REMOVE_PROJECT_CACHE,
    };
}

export function populateProjectFieldsCache(fields = []) {
    return {
        type: POPULATE_PROJECT_FIELDS_CACHE,
        payload: {
            fields,
        },
    };
}

export function fetchProjectById(projectId) {
    return {
        type: FETCH_PROJECT_BY_ID,
        projectId,
    };
}

export function fetchProjectByIdSucceeded(project = {}) {
    return {
        type: FETCH_PROJECT_SUCCEEDED,
        payload: {
            ...{
                project,
            },
        },
    };
}

export function fetchProjectByIdFailed(message) {
    return {
        type: FETCH_PROJECT_FAILED,
        message,
    };
}

export function populateCurrentProject(project) {
    return {
        type: POPULATE_CURRENT_PROJECT,
        payload: {
            project,
        },
    };
}

export function updateCurrentProjectByProperty(key, value) {
    return {
        type: UPDATE_CURRENT_PROJECT_BY_PROPERTY,
        key,
        value,
    };
}

export function updateProjectPropertiesById(projectId, properties = {}) {
    return {
        type: UPDATE_PROJECT_PROPERTIES_BY_ID,
        projectId,
        properties,
    };
}

export function updateCurrentProjectByProperties(properties) {
    return {
        type: UPDATE_CURRENT_PROJECT_PROPERTIES,
        properties,
    };
}

export function pullProjectById(projectId) {
    return {
        type: PULL_PROJECT_BY_ID,
        projectId,
    };
}

export function launchProjectValidator(projectId) {
    return {
        type: LAUNCH_PROJECT_VALIDATOR,
        projectId,
    };
}

export function launchProjectDesigner(projectId, categoryId) {
    return {
        type: LAUNCH_PROJECT_DESIGNER,
        projectId,
        categoryId,
    };
}

export function deleteProject(projectId) {
    return {
        type: DELETE_PROJECT,
        projectId,
    };
}

export function projectDeleteLoading(loading = false) {
    return {
        type: DELETE_PROJECT_LOADING,
        loading,
    };
}

export function setCurrentProjectTransactionId(transactionId) {
    return {
        type: SET_CURRENT_PROJECT_TRANSACTION_ID,
        transactionId,
    };
}

/**
 * setProjectId
 *
 * @description a project Id to the store
 * @param {string} projectId
 * @returns {{type, projectId: string}}
 */
export function setProjectId(projectId) {
    return {
        type: SET_PROJECT_ID,
        projectId,
    };
}

/**
 * @description issue action to fetch project summary.
 * @param {string} projectId
 */
export function fetchProjectSummaryById(projectId) {
    return {
        type: FETCH_PROJECT_SUMMARY,
        projectId,
    };
}

/**
 * @description set project summary to store.
 * @param {object} projectSummary
 */
export function fetchProjectSummarySucceeded(projectSummary = {}) {
    return {
        type: FETCH_PROJECT_SUMMARY_SUCCEEDED,
        payload: {
            projectSummary,
        },
    };
}

/**
 * @description issue failed action to notify UI.
 * @param {string} message
 */
export function fetchProjectSummaryFailed(message) {
    return {
        type: FETCH_PROJECT_SUMMARY_FAILED,
        message,
    };
}

export function updateQueryParams(queryParams) {
    return {
        type: UPDATE_QUERY_PARAMS,
        queryParams,
    };
}

/**
 * @description set sort config fields
 * @param {Object[]} fields
 * @param {Object[]} sort
 */
export function setSortValues(fields, sort) {
    return {
        type: SET_SORT_VALUES,
        sort,
        fields,
    };
}

/**
 * @description set project stage by id and version Id
 * @param {object} project
 * @param {string} stage
 */
export function setProjectStage(project, stage) {
    return {
        type: SET_PROJECT_STAGE,
        id: project.id,
        stage,
        versionId: project.versionId,
    };
}

/**
 * @description set current project stage by id and version Id
 * @param {object} project
 * @param {string} stage
 */
export function setCurrentProjectStage(project, stage) {
    return {
        type: SET_CURRENT_PROJECT_STAGE,
        id: project.id,
        stage,
        versionId: project.versionId,
    };
}

/**
 * @description toggle project from staging delay object
 * @param {string} id
 */
export function toggleProjectStagingDelay(id) {
    return {
        type: TOGGLE_PROJECT_STAGING_DELAY,
        id,
    };
}

/**
 * @description upload files for the project
 * @param {string} id
 * @param {array} files
 */
export function uploadFiles(id, files) {
    return {
        type: UPLOAD_PROJECT_FILES,
        payload: {
            id,
            files,
        },
    };
}

export function fetchProjectDomains() {
    return {
        type: FETCH_PROJECT_DOMAINS,
    };
}
