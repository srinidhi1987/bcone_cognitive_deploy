/*
 *
 * ProjectsContainer constants
 *
 */

export const CACHE_PROJECT = 'app/ProjectContainer/CACHE_PROJECT';
export const CLEAR_CURRENT_PROJECT = 'app/ProjectContainer/CLEAR_CURRENT_PROJECT';
export const DELETE_PROJECT = 'app/ProjectsContainer/DELETE_PROJECT';
export const DELETE_PROJECT_LOADING = 'app/ProjectsContainer/DELETE_PROJECT_LOADING';
export const FETCH_CURRENT_PROJECT_CATEGORIES_SUCCEEDED = 'app/ProjectsContainer/FETCH_CURRENT_PROJECT_CATEGORIES_SUCCEEDED';
export const FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED = 'app/ProjectsContainer/FETCH_SELECTED_PROJECT_CATEGORIES_SUCCEEDED';
export const FETCH_PROJECT_BY_ID = 'app/ProjectContainer/FETCH_PROJECT_BY_ID';
export const FETCH_PROJECT_FAILED = 'app/ProjectContainer/FETCH_PROJECT_FAILED';
export const FETCH_PROJECT_DOMAINS = 'app/ProjectContainer/FETCH_PROJECT_DOMAINS';
export const FETCH_PROJECT_SUCCEEDED = 'app/ProjectContainer/FETCH_PROJECT_SUCCEEDED';
export const FETCH_PROJECT_CATEGORIES = 'app/ProjectContainer/FETCH_PROJECT_CATEGORIES';
export const FETCH_PROJECTS = 'app/ProjectsContainer/FETCH_PROJECTS';
export const FETCH_PROJECTS_FAILED = 'app/ProjectsContainer/FETCH_PROJECTS_FAILED';
export const FETCH_PROJECTS_SUCCEEDED = 'app/ProjectsContainer/FETCH_PROJECTS_SUCCEEDED';
export const MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE = 30;
export const LAUNCH_PROJECT_DESIGNER = 'app/ProjectsContainer/LAUNCH_PROJECT_DESIGNER';
export const LAUNCH_PROJECT_VALIDATOR = 'app/ProjectsContainer/LAUNCH_PROJECT_VALIDATOR';
export const LOADING_PROJECTS = 'app/ProjectsContainer/LOADING_PROJECTS';
export const POPULATE_PROJECT_FIELDS_CACHE = 'app/ProjectContainer/POPULATE_PROJECT_FIELDS_CACHE';
export const POPULATE_CURRENT_PROJECT = 'app/ProjectContainer/POPULATE_CURRENT_PROJECT';
export const PROJECT_CATEGORIES_LOADING = 'app/ProjectContainer/PROJECT_CATEGORIES_LOADING';
export const PULL_PROJECT_BY_ID = 'app/ProjectsContainer/PULL_PROJECT_BY_ID';
export const REMOVE_PROJECT_CACHE = 'app/ProjectContainer/REMOVE_PROJECT_CACHE';
export const SET_CURRENT_PROJECT_TRANSACTION_ID = 'app/ProjectContainer/SET_CURRENT_PROJECT_TRANSACTION_ID';
export const SET_PROJECT_ID = 'app/ProjectContainer/SET_PROJECT_ID';
export const SELECT_PROJECT = 'app/ProjectContainer/SELECT_PROJECT';
export const UPDATE_CURRENT_PROJECT_BY_PROPERTY = 'app/ProjectsContainer/UPDATE_CURRENT_PROJECT_BY_PROPERTY';
export const UPDATE_CURRENT_PROJECT_PROPERTIES = 'app/ProjectsContainer/UPDATE_CURRENT_PROJECT_PROPERTIES';
export const UPDATE_QUERY_PARAMS = 'app/ProjectsContainer/UPDATE_QUERY_PARAMS';
export const UPDATE_PROJECT_PROPERTIES_BY_ID = 'app/ProjectsContainer/UPDATE_PROJECT_PROPERTIES_BY_ID';
export const FETCH_PROJECT_SUMMARY = 'app/ProjectContainer/FETCH_PROJECT_SUMMARY';
export const FETCH_PROJECT_SUMMARY_SUCCEEDED = 'app/ProjectContainer/FETCH_PROJECT_SUMMARY_SUCCEEDED';
export const FETCH_PROJECT_SUMMARY_FAILED = 'app/ProjectContainer/FETCH_PROJECT_SUMMARY_FAILED';
export const SET_SORT_VALUES = 'app/ProjectsContainer/SET_SORT_VALUES';
export const TOGGLE_PROJECT_STAGING_DELAY = 'app/ProjectsContainer/TOGGLE_PROJECT_STAGING_DELAY';
export const SET_PROJECT_STAGE = 'app/ProjectsContainer/SET_PROJECT_STAGE';
export const SET_CURRENT_PROJECT_STAGE = 'app/ProjectsContainer/SET_CURRENT_PROJECT_STAGE';
export const UPLOAD_PROJECT_FILES = 'app/ProjectsContainer/UPLOAD_PROJECT_FILES';

export const PROJECT_SORT_KEY = 'name';
export const CATEGORY_NAME = 'categoryName';
export const MAX_FILES_UPLOAD = 150;
export const CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1 = 'Once set to Production, the learning instance will be processed using bots that are in production.';
export const CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2 = 'Any processing that requires a bot that is not in production can be done, but only manually.';
export const CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE = 'Do you want to set the Learning Instance to Production?';
export const CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT = 'No, Cancel';
export const CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT = 'Yes, send to production';
export const CATEGORY_LIMIT_PREVIEW = 10;
export const UNCLASSIFIED_REGEX = /unclassified|_-1|Yet to be classified/i;
export const ALLOWED_FILE_TYPES = ['jpg', 'jpeg', 'pdf', 'tiff', 'tif', 'png'];
