/*
 *
 * ProjectsContainer
 *
 */

import React from 'react';
import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import Helmet from 'react-helmet';
import {FormattedNumber} from 'react-intl';
import selectProjectsContainer from './selectors';
import FontAwesome from 'react-fontawesome';
import {autobind} from 'core-decorators';
import {
    fetchProjects,
    selectProject,
    cacheProject,
    setSortValues,
    setProjectStage,
} from './actions';
import {
    CATEGORY_LIMIT_PREVIEW,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT,
} from './constants';
import {DataTableCopyText} from '../../constants/enums';
import {DataField, DataType} from 'common-frontend-utils';
import {
    DataTable,
    Breadcrumbs,
    PageTitle,
    SplitLayout,
    DataFilter,
    CommandButton,
    Tooltip,
} from 'common-frontend-components';

import {partialProjectUpdate} from '../EditProjectContainer/actions';
import PageDescription from '../../components/PageDescription';
import FlexGrid from '../../components/FlexGrid';
import ProgressBar from '../../components/ProgressBar';
import {TRAINING_PERCENTAGE} from '../../constants/productTerms';
import ActionConfirmationModal from '../../components/ActionConfirmationModal';
import {themeType} from '../../components/ActionConfirmationModal';
import ActionWithLoadingState from '../../components/ActionWithLoadingState';
import ValidateProjectButton from '../../components/ValidateProjectButton';
import ViewProjectDetailsButton from '../../components/ViewProjectDetailsButton';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class ProjectsContainer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'ProjectsContainer';

    state = {
        filter: [],
        filteredProjects: [],
        isFiltering: false,
        showActionConfirmationDetail: false,
        actionConfirmationDetailParam: null,
    };

    componentWillMount() {
        this.props.fetchProjects();
    }

    isProjectInProduction(project) {
        return project.environment === 'production';
    }

    _goToDetailsView(project) {
        // cache/pass project data to save on load in edit view
        this.props.cacheProject(project);
        // push new route
        browserHistory.push(routeHelper(`/learning-instances/${project.id}`));
    }

    /**
        * set data table filter
        * @param {string} filter
    */
    @autobind _handleChangeFilter(filter) {
        this.setState({filter});
    }

    /**
        * set stage of project
        * @param {object} proj
        * @param {boolean} isInProduction
    */
    _handleProjectStageChange(proj, isInProduction) {
        const {setProjectStage} = this.props;

        if (isInProduction) {
            setProjectStage(proj, 'staging');

            return;
        }

        this.setState({
            showActionConfirmationDetail: true,
            actionConfirmationDetailParam: {project: proj},
        });

        return;
    }

    /**
        * set new sort configuration
        * @param {Object[]} fields
        * @param {Object[]} sort
    */
    _handleSortProjects(fields, sort) {
        this.props.setSortValues(fields, sort);
    }

    /**
        * render a project stage toggle ActionWithLoadingState component
        * @param {object} project
        * @returns {component} ActionWithLoadingState
    */
    _renderProjectStageToggle(project) {
        const isInProduction = this.isProjectInProduction(project);
        const {projectsDelayingStage} = this.props;
        const working = Boolean(projectsDelayingStage[project.id]);
        const fa = working ? 'gear' : null;
        const aa = working ? null : isInProduction ? 'action-toggle--enabled' : 'action-toggle--disabled';
        const animate = working ? 'spin' : null;
        const label = isInProduction ? 'Set instance to staging' : 'Set instance to production';


        return (
            <li>
                <ActionWithLoadingState
                    aa={aa}
                    animate={animate}
                    fa={fa}
                    label={label}
                    working={working}
                    onClick={() => this._handleProjectStageChange(project, isInProduction)} />
            </li>
        );
    }

    /**
        * render a ViewProjectDetailsButton
        * @param {object} project
        * @returns {component} ViewProjectDetailsButton
    */
    _renderViewProjectDetails(project) {
        return (
            <ViewProjectDetailsButton
                project={project}
                onClick={() => this._goToDetailsView(project)} />
        );
    }

    /**
        * render a ValidateProjectButton
        * @param {object} project
        * @returns {component} ValidateProjectButton
    */
    _renderLaunchValidator({id, environment}) {
        const {appConfig, authToken} = this.props;
        if (environment !== 'staging')
            return (
                <ValidateProjectButton
                    appConfig={appConfig}
                    authToken={authToken}
                    id={id}
                    withLabel={false}
                    enabled={true}
                />
            );
        return null;
    }

    getProjectFields() {
        return [
            new DataField({id: 'name', label: 'Instance name', sortable: true, type: DataType.STRING, render: (name, data) => (
                <a
                  className="aa-link underline"
                  onClick={() => this._goToDetailsView(data)}
                >
                    {name}
                </a>
            )}),
            new DataField({id: 'visionBotCount', label: '# of IQ bots', sortable: true, type: DataType.NUMBER, render: (count) => (
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'numberOfFiles', label: '# of files', sortable: true, type: DataType.NUMBER, render: (count) => (
                <FormattedNumber value={count} />
            )}),
            new DataField({id: 'accuracyPercentage', label: '% accuracy', sortable: true, type: DataType.NUMBER, render: (accuracy) => `${Math.floor(accuracy)}%`}),
            new DataField({id: 'currentTrainedPercentage', label: TRAINING_PERCENTAGE.label, sortable: true, type: DataType.NUMBER, render: (progress) => (
                <ProgressBar current={Math.floor(progress)} />
            )}),
            // The field below specifically has the ID 'environment' in order to work with FieldFilter component.
            // This is a work around because this table doesn't have a visible 'environment' column already
            new DataField({id: 'environment', label: 'Actions', sortable: false, type: DataType.STRING, render: (proj) => (
                <ul className="aa-project-actions">
                    {this._renderProjectStageToggle(proj)}
                    {this._renderViewProjectDetails(proj)}
                    {this._renderLaunchValidator(proj)}
                </ul>
            )}),
        ];
    }

    render() {
        const {
            sortedProjects,
            loadingProjects,
            selectedProject,
            selectProject,
            setProjectStage,
            sort,
        } = this.props;
        const {
            filter,
            isFiltering,
            showActionConfirmationDetail,
            actionConfirmationDetailParam,
        } = this.state;
        const {
            SEARCH_ALL_PLACEHOLDER,
            ALL_COLUMNS_DROPDOWN_VALUE,
        } = DataTableCopyText;

        /**
         * Table fields
         */
        const projectFields = this.getProjectFields();

        const filteredProjects = sortedProjects.filter(DataField.filter(projectFields, filter));
        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Learning Instances"
                />

                <Breadcrumbs>
                    <Link to={routeHelper('/learning-instances')}>Learning Instances</Link>
                </Breadcrumbs>

                <div id="aa-main-container">
                    <div className="aa-grid-row space-between vertical-center">
                        <PageTitle label="My Learning Instances" />
                        {/** New project link **/}

                        <Link className="aa-new-project-btn" to={routeHelper('/learning-instances/new')}>
                            <CommandButton
                                onClick={() => browserHistory.push(routeHelper('/learning-instances/new'))}
                                theme="success"
                                recommended
                            >
                                <span>
                                    <FontAwesome
                                        name="magic" />
                                    <span> New Instance</span>
                                </span>
                            </CommandButton>
                        </Link>
                    </div>

                    {/** Page header section **/}
                    <div className="aa-grid-row">
                        <div>
                            <PageDescription>
                                <span>Create and manage your Learning Instances.</span>
                                <Tooltip>
                                    <div className="learning-instance-popup">
                                        <p>
                                            A <strong> Learning Instance</strong> is a building block that learns how best to extract data from semi-structured situations.
                                        </p>
                                        <span>
                                            Steps to create a Learning Instance:
                                        </span>
                                        <ol>
                                            <li>
                                                <strong>Define</strong> a Learning Instance by uploading sample documents and providing your data needs as input.
                                            </li>
                                            <li>
                                                <strong>Train</strong> bots for your most significant groups and set the bots to production.
                                            </li>

                                            <li>
                                                <strong>Deploy</strong> your Instance to production and watch as it increases precision on results through its own learning.
                                            </li>
                                        </ol>
                                        <div className="aa-grid-row vertical-center learning-instance-popup-call-to-action">
                                            <span>Click</span>
                                            <CommandButton
                                                theme="success"
                                                recommended
                                                onClick={() => {}}>
                                                <span>
                                                    <FontAwesome name="magic" />
                                                    <span> New Instance</span>
                                                </span>
                                            </CommandButton>
                                            <span>to get started.</span>
                                        </div>
                                    </div>
                            </Tooltip>
                            </PageDescription>
                        </div>
                    </div>

                    {/** Projects **/}
                    {(() => {
                        if ((loadingProjects || isFiltering || (Array.isArray(sortedProjects) && sortedProjects.length))) {
                            return (
                                <div>
                                    <SplitLayout defaultSize={70}>
                                        {/** Left column **/}
                                        <SplitLayout.Left>
                                            {/** Filter data **/}
                                            <div>
                                                <DataFilter
                                                    fields={[
                                                        new DataField({id: 'environment', label: 'Environment', type: DataType.STRING}),
                                                        new DataField({id: 'name', label: 'Instance Name', type: DataType.STRING}),
                                                    ]}
                                                    searchAll
                                                    labelColumnsAll={ALL_COLUMNS_DROPDOWN_VALUE}
                                                    labelSearchAll={SEARCH_ALL_PLACEHOLDER}
                                                    value={filter}
                                                    onChange={this._handleChangeFilter}
                                                />
                                            </div>
                                            {/** end Filter data **/}

                                            {/** Existing Projects **/}
                                            <div className="aa-existing-projects-header">
                                                <FlexGrid>
                                                    <FlexGrid.Item itemStyle={{width: '880px', paddingLeft: '0'}}>
                                                        Instances ({filteredProjects.length} of {sortedProjects.length})
                                                    </FlexGrid.Item>
                                                </FlexGrid>
                                            </div>

                                            {loadingProjects || filteredProjects.length ? (
                                                <DataTable
                                                    data={filteredProjects.map((proj) => ({
                                                        ...proj,
                                                        environment: proj,
                                                    }))}
                                                    dataId="id"
                                                    fields={projectFields}
                                                    onSort={(sort) => this._handleSortProjects(projectFields, sort)}
                                                    sort={sort}
                                                    loading={loadingProjects}
                                                    active={selectedProject ? selectedProject.id : null }
                                                    onActive={(id, data) => {
                                                        const selectedProject1 = data.find((element) => element.id === id);
                                                        if (selectedProject1 !== undefined)
                                                            selectProject(filteredProjects, selectedProject1.name);
                                                    }}
                                                />
                                            ) : (
                                                <div className="aa-project-no-results">
                                                    Learning Instance(s) not found
                                                </div>
                                            )}
                                            {/** Existing Projects **/}
                                        </SplitLayout.Left>
                                    </SplitLayout>
                                    <ActionConfirmationModal
                                        title={CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE}
                                        paragraphs={[CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1, CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2]}
                                        cancelTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT}
                                        confirmTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT}
                                        show={showActionConfirmationDetail}
                                        param={actionConfirmationDetailParam}
                                        theme={themeType.info}
                                        onCancel={() => this.setState({showActionConfirmationDetail:false})}
                                        onConfirm={(param) => {
                                            setProjectStage(param.project, 'production');
                                            this.setState({showActionConfirmationDetail: false,
                                                actionConfirmationDetailParam: null});
                                        }}
                                    />
                                </div>
                            );
                        }

                        return (
                            <div className="aa-project-placeholder aa-grid-row vertical-center horizontal-center">
                                <span>No current learning instances.</span>
                                <CommandButton
                                    onClick={() => browserHistory.push(routeHelper('/learning-instances/new'))}
                                    theme="success"
                                    recommended
                                >
                                    Create one now!
                                </CommandButton>
                            </div>
                        );
                    })()}
                    {/** End Projects **/}
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectProjectsContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchProjects: () => dispatch(fetchProjects()),
        selectProject: (projects, name) => dispatch(selectProject(projects, name, {
            limit: CATEGORY_LIMIT_PREVIEW,
            sort: [
                {
                    id:'index',
                    orderBy: 'desc',
                },
            ],
        })),
        cacheProject: (project) => dispatch(cacheProject(project)),
        setSortValues: (fields, sort) => dispatch(setSortValues(fields, sort)),
        updateProject: (project, operations) => dispatch(partialProjectUpdate(project, operations)),
        setProjectStage: (project, stage) => dispatch(setProjectStage(project, stage)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProjectsContainer);
