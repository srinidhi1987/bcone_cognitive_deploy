import {List} from 'immutable';
import {createSelector} from 'reselect';

/**
 * Direct selector to the projectsContainer state domain
 */
const selectAppContainerDomain = () => (state) => state.get('appContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');
const selectSupportedLanguages = () => (state) => state.getIn(['language', 'supportedLanguages']);

/**
 * Other specific selectors
 */
const selectProject = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => projectState.get('projects').toJS().find((p) => p.id === projectState.get('selectedProjectId') || null),
);

const selectSortedProjects = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => {
        const sortFn = projectState.get('sortFn');
        const projects = projectState.get('projects');

        return projects.toJS().sort(sortFn);
    }
);

/**
 * Default selector used by ProjectsContainer
 */

const selectProjectsContainer = () => createSelector(
    selectProjectsContainerDomain(),
    selectAppContainerDomain(),
    selectProject(),
    selectSupportedLanguages(),
    selectSortedProjects(),
    (projectState, appState, selectedProject, supportedLanguages, sortedProjects) => {
        return {
            ...projectState.toJS(),
            selectedProject,
            appConfig: appState.get('appConfig').toJS(),
            authToken: appState.get('authToken'),
            supportedLanguages: List.isList(supportedLanguages) ? supportedLanguages.toJS() : supportedLanguages,
            sortedProjects,
        };
    },
);

export default selectProjectsContainer;
export {
    selectProjectsContainerDomain,
    selectProject,
};
