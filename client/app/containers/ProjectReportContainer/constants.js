/*
 *
 * ProjectReportContainer constants
 *
 */

export const FETCH_REPORT_TOTALS = 'app/ProjectReportContainer/FETCH_REPORT_TOTALS';
export const PERSIST_REPORT_TOTALS = 'app/ProjectReportContainer/PERSIST_REPORT_TOTALS';
export const LOADING_REPORT_TOTALS = 'app/ProjectReportContainer/LOADING_REPORT_TOTALS';
