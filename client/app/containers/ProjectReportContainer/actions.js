/*
 *
 * ProjectReportContainer actions
 *
 */

import {
    FETCH_REPORT_TOTALS,
    PERSIST_REPORT_TOTALS,
    LOADING_REPORT_TOTALS,
} from './constants';

export function fetchReportTotals(projectId) {
    return {
        type: FETCH_REPORT_TOTALS,
        projectId,
    };
}

export function persistReportTotals(payload = {}) {
    return {
        type: PERSIST_REPORT_TOTALS,
        payload,
    };
}

export function loadingReportTotals(loading = false) {
    return {
        type: LOADING_REPORT_TOTALS,
        loading,
    };
}
