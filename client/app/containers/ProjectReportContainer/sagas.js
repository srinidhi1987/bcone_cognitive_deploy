import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet} from '../../utils/request';

import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {FETCH_REPORT_TOTALS} from './constants';
import {
    loadingReportTotals,
    persistReportTotals,
} from './actions';
import routeHelper from '../../utils/routeHelper';

// Individual exports for testing
export function* fetchReportTotalsFlow(action) {
    try {
        yield put(loadingReportTotals(true));
        const totals = yield call(fetchReportTotalsFlowFromServer, action.projectId);
        if (totals.success) {
            yield put(persistReportTotals(totals.data));
        }
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
    finally {
        yield put(loadingReportTotals(false));
    }
}

export function fetchReportTotalsFlowFromServer(projectId) {
    return requestGet(
        routeHelper(`api/reporting/projects/${projectId}/totals`),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

// All sagas to be loaded
export default [
    function* () {
        yield takeLatest(FETCH_REPORT_TOTALS, fetchReportTotalsFlow);
    },
];
