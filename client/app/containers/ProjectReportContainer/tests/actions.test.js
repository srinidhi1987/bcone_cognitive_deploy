import {
    FETCH_REPORT_TOTALS,
    PERSIST_REPORT_TOTALS,
    LOADING_REPORT_TOTALS,
} from '../constants';
import {
    fetchReportTotals,
    persistReportTotals,
    loadingReportTotals,
} from '../actions';

describe('ProjectReportContainer actions', () => {
    describe('fetchReportTotals action creator', () => {
        it('should pass project id and set type', () => {
            // given
            const projectId = '645645567567687678';
            const expectedAction = {
                type: FETCH_REPORT_TOTALS,
                projectId,
            };
            // when
            const result = fetchReportTotals(projectId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistReportTotals action creator', () => {
        it('should pass payload to persist data', () => {
            // given
            const payload = {
                id: '456546556465465',
                currentStraightThroughProcessing: 0,
            };
            const expectedAction = {
                type: PERSIST_REPORT_TOTALS,
                payload,
            };
            // when
            const result = persistReportTotals(payload);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('loadingReportTotals action creator', () => {
        it('should set loading state for fetching report totals', () => {
            // given
            const expectedAction = {
                type: LOADING_REPORT_TOTALS,
                loading: true,
            };
            // when
            const result = loadingReportTotals(true);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
