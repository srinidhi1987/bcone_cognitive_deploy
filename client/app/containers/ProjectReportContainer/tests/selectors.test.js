import {fromJS} from 'immutable';

import {Project1} from '../../../../fixtures/projects';
import selectProjectReportContainer from '../selectors';

describe('ProjectReportContainer selectors', () => {
    describe('selectProjectReportContainer selector', () => {
        it('should return current project', () => {
            // given
            const mockState = {
                projectsContainer: {
                    loadingProjects: true,
                    currentProject: Project1,
                },
                projectReportContainer: {},
            };

            // when
            const result = selectProjectReportContainer()(fromJS(mockState));
            // then
            expect(result.project).toEqual(mockState.projectsContainer.currentProject);
        });

        it('should return project loading state', () => {
            // given
            const mockState = {
                projectsContainer: {
                    loadingProjects: true,
                    currentProject: Project1,
                },
                projectReportContainer: {},
            };

            // when
            const result = selectProjectReportContainer()(fromJS(mockState));
            // then
            expect(result.projectLoading).toEqual(mockState.projectsContainer.loadingProjects);
        });
    });
});
