import React from 'react';
import {shallow} from 'enzyme';

import {ProjectReportContainer} from '../index';

describe('<ProjectReportContainer />', () => {
    describe('_truncateAxisName instance method', () => {
        const wrapper = shallow(
            <ProjectReportContainer
                params={{
                    id: '45664545645623',
                }}
                fetchProjectById={() => {}}
                fetchReportTotals={() => {}}
                reportTotals={{}}
            />
        );

        it('should NOT truncate if string is under 12 characters', () => {
            // given
            const string = 'Invoice';
            // when
            const result = wrapper.instance()._truncateAxisName(string);
            // then
            expect(result).toEqual(string);
        });

        it('should truncate if string is over 12 characters', () => {
            // given
            const string = 'Invoice Description';
            // when
            const result = wrapper.instance()._truncateAxisName(string);
            // then
            expect(result).toEqual(`${string.substring(0, 12)}...`);
        });
    });
});
