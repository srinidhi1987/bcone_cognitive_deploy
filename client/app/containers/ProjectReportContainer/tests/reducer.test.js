import projectReportContainerReducer, {initialState} from '../reducer';
import {PERSIST_REPORT_TOTALS, LOADING_REPORT_TOTALS} from '../constants';

describe('projectReportContainerReducer', () => {
    it('returns the initial state', () => {
        expect(projectReportContainerReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle PERSIST_REPORT_TOTALS and save payload data', () => {
        // given
        const payload = {
            id: '34345343455',
            name: 'Testing Report',
        };
        const action = {
            type: PERSIST_REPORT_TOTALS,
            payload,
        };
        const expectedState = {
            ...initialState.toJS(),
            reportTotals: payload,
        };
        // when
        const result = projectReportContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle LOADING_REPORT_TOTALS and set report totals loading state', () => {
        // given
        const action = {
            type: LOADING_REPORT_TOTALS,
            loading: true,
        };
        const expectedState = {
            ...initialState.toJS(),
            loadingReportTotals: true,
        };
        // when
        const result = projectReportContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });
});
