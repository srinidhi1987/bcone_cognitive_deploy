/**
 * ProjectReportContainer sagas
 */

import {call, put} from 'redux-saga/effects';
import {
    loadingReportTotals,
    persistReportTotals,
} from '../actions';
import {
    fetchReportTotalsFlow,
    fetchReportTotalsFlowFromServer,
} from '../sagas';
import {LOADING_REPORT_TOTALS} from '../constants';

describe('ProjectReport Saga', () => {
    describe('fetchReportTotalsFlow generator', () => {
        describe('success path', () => {
            // given
            const action = {
                type: LOADING_REPORT_TOTALS,
                projectId: '456645564654645645',
            };
            const generator = fetchReportTotalsFlow(action);

            it('should set report totals loading state to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingReportTotals(true)));
            });

            it('should call API to fetch report totals', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchReportTotalsFlowFromServer, action.projectId));
            });

            it('should persist totals', () => {
                // given
                const response = {
                    success: true,
                    data: {
                        id: '345645465564654',
                    },
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(persistReportTotals(response.data)));
            });

            it('should set report totals loading state to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingReportTotals(false)));
            });
        });
    });
});
