/*
 *
 * ProjectReportContainer
 *
 */

import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import {Breadcrumbs} from 'common-frontend-components';
import ChartWrapper from '../../components/ChartWrapper';
import {BarChart} from 'rd3';

import './styles.scss';

import {fetchReportTotals} from './actions';
import {fetchProjectById} from '../ProjectsContainer/actions';
import selectProjectReportContainer from './selectors';
import EnvironmentBadge from '../../components/EnvironmentBadge';
import PercentageTag from '../../components/PercentageTag';
import ProjectReportSection from '../../components/ProjectReportSection';
import ProjectReportStatCard from '../../components/ProjectReportStatCard';
import ProjectReportTotal from '../../components/ProjectReportTotal';
import CircularProgressBar from '../../components/CircularProgressBar';
import routeHelper from '../../utils/routeHelper';

const barMargins = {top: 10, right: 20, bottom: 40, left: 65};

export class ProjectReportContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'ProjectReportContainer';

    componentWillMount() {
        const {params, fetchProjectById, fetchReportTotals} = this.props;

        // fetch project from id parameter
        fetchProjectById(params.id);
        // fetch report totals
        fetchReportTotals(params.id);
    }

    /**
     * Truncate chart x axis names
     *
     * @param {string} string
     * @returns {string}
     */
    _truncateAxisName(string) {
        const max = 12;
        if (!string) return '';

        return string.length > max ? `${string.substring(0, max)}...` : string;
    }

    render() {
        const {params, projectLoading, loadingReportTotals, project, reportTotals} = this.props;

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Performance Report"
                />

                <Breadcrumbs>
                    <Link to={routeHelper('/learning-instances')}>Learning Instances</Link>
                    <Link to={routeHelper(`/learning-instances/${params.id}`)}>{project ? project.name : 'Loading...'}</Link>
                    <Link>Performance Report</Link>
                </Breadcrumbs>

                <div className="aa-project-performance-report-container">
                    <PercentageTag
                        percentage={project ? project.currentTrainedPercentage : 0}
                    />

                    <div className="aa-project-performance-report-wrapper report-header">
                        {(() => {
                            if (!projectLoading && project) {
                                return (
                                    <header className="aa-project-performance-report-header">
                                        <div className="aa-project-performance-report-header-progress">
                                            <CircularProgressBar
                                                progress={project.currentTrainedPercentage}
                                                size="small"
                                                icon={(
                                                    <img src={routeHelper('images/training-icon.svg')} alt="Training Percentage" />
                                                )}
                                            />
                                        </div>

                                        <span className="aa-project-performance-report-header--title">
                                            {project.name}

                                            <EnvironmentBadge>
                                                {project.environment}
                                            </EnvironmentBadge>
                                        </span>

                                        <span className="aa-project-performance-report-header--description">
                                            {project.description}
                                        </span>
                                    </header>
                                );
                            }
                        })()}
                    </div>

                    <ProjectReportSection
                        icon={(
                            <svg width="14px" height="14px" viewBox="8 8 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                <rect stroke="none" fill="#666666" fillRule="evenodd" transform="translate(19.294118, 19.381772) rotate(-45.000000) translate(-19.294118, -19.381772) " x="18.5441183" y="16.7613661" width="1.5" height="5.24081122"></rect>
                                <circle stroke="#666666" strokeWidth="1" fill="none" cx="14" cy="14" r="5"></circle>
                            </svg>
                        )}
                        title="Instance Totals"
                        classes="instance-totals"
                    >
                        <div className="aa-project-performance-report-totals">
                            <div>
                                <ProjectReportTotal
                                    icon={(
                                        <svg width="64px" height="64px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                            <circle stroke="#39ACC7" strokeWidth="2" fill="none" cx="31" cy="31" r="31"></circle>
                                            <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(15.000000, 11.000000)">
                                                <path d="M19.0189595,4.28439995 C26.4121374,5.69611974 32,12.1955022 32,20 C32,25.2815055 29.4409942,29.9653231 25.4952393,32.8791962 L24.8958228,29.4797362 C27.4220325,27.1081951 29,23.7383746 29,20 C29,13.6727096 24.4796926,8.40119356 18.4922836,7.23865761 L19.0189595,4.28439995 Z M10,5.16303409 C4.13633933,7.5365836 0,13.2852449 0,20 C0,28.836556 7.163444,36 16,36 C16.1893686,36 16.3779688,35.9967102 16.5657466,35.9901846 L16.5714229,32.9876666 C16.3819755,32.9958606 16.1914711,33 16,33 C8.82029825,33 3,27.1797017 3,20 C3,14.9845516 5.84021995,10.632472 10,8.46442126 L10,5.16303409 Z" id="Combined-Shape" fill="#39ADC7"></path>
                                                <polygon fill="#39ACC7" transform="translate(10.359462, 7.366073) rotate(65.000000) translate(-10.359462, -7.366073) " points="10.3594617 2.86607261 15.5556141 11.8660726 5.16330926 11.8660726"></polygon>
                                                <polygon fill="#39ACC7" transform="translate(23.756444, 31.838789) rotate(-124.000000) translate(-23.756444, -31.838789) " points="23.7564436 27.3387894 28.9525961 36.3387894 18.5602912 36.3387894"></polygon>
                                            </g>
                                        </svg>
                                    )}
                                    loading={loadingReportTotals}
                                    metric={reportTotals.totalFilesProcessed}
                                    compareMetric={reportTotals.totalFilesUploaded}
                                    title="Files Processed"
                                />
                            </div>

                            {/*Div Line*/}
                            <div></div>

                            <div>
                                <ProjectReportTotal
                                    icon={(
                                        <svg width="64px" height="64px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                            <g id="Icon" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                                <circle stroke="#60C58E" strokeWidth="2" cx="31" cy="31" r="31"></circle>
                                                <path d="M27.1724185,43.1610844 L27.2610562,43.2497221 L47.5983683,22.91241 L46.4362362,21.7502779 L27.1930661,40.993448 L16.6452393,30.4456212 L15.4831071,31.6077534 L27.1044283,43.2290746 L27.1724185,43.1610844 Z" stroke="#60C58E" fill="#60C58E"></path>
                                            </g>
                                        </svg>
                                    )}
                                    loading={loadingReportTotals}
                                    metric={Math.floor(reportTotals.totalSTP)}
                                    metricType="percentage"
                                    compareMetric={100}
                                    title="Straight-through processing"
                                />
                            </div>

                            {/*Div Line*/}
                            <div></div>

                            <div>
                                <ProjectReportTotal
                                    icon={(
                                        <svg width="64px" height="64px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                            <defs>
                                                <path d="M35,34.0702576 L35,18.0028165 C35,15.7979383 33.209139,14 31,14 C28.7953562,14 27,15.792122 27,18.0028165 L27,34.0702576 C24.6087945,35.4534952 23,38.0388706 23,41 C23,45.418278 26.581722,49 31,49 C35.418278,49 39,45.418278 39,41 C39,38.0388706 37.3912055,35.4534952 35,34.0702576 Z" id="path-1"></path>
                                                <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="35" fill="white">
                                                    <use xlinkHref="#path-1"></use>
                                                </mask>
                                            </defs>
                                            <circle stroke="#C92194" strokeWidth="2" fill="none" cx="31" cy="31" r="31"></circle>
                                            <use id="Combined-Shape" stroke="#C92194" mask="url(#mask-2)" strokeWidth="4" fill="none" xlinkHref="#path-1"></use>
                                            <path d="M32,36.1000181 L32,24.9909572 C32,24.4563138 31.5522847,24 31,24 C30.4438648,24 30,24.4436666 30,24.9909572 L30,36.1000181 C27.7177597,36.5632884 26,38.5810421 26,41 C26,43.7614237 28.2385763,46 31,46 C33.7614237,46 36,43.7614237 36,41 C36,38.5810421 34.2822403,36.5632884 32,36.1000181 Z" id="Combined-Shape" stroke="none" fill="#C92094" fillRule="evenodd"></path>
                                        </svg>
                                    )}
                                    loading={loadingReportTotals}
                                    metric={Math.floor(reportTotals.totalAccuracy)}
                                    metricType="percentage"
                                    title="Accuracy"
                                />
                            </div>
                        </div>
                    </ProjectReportSection>

                    <ProjectReportSection
                        icon={(
                            <svg width="17px" height="20px" viewBox="7 5 17 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                <path d="M16.5094798,7.14219998 C20.2060687,7.84805987 23,11.0977511 23,15 C23,17.6407527 21.7204971,19.9826615 19.7476196,21.4395981 L19.4479114,19.7398681 C20.7110162,18.5540976 21.5,16.8691873 21.5,15 C21.5,11.8363548 19.2398463,9.20059678 16.2461418,8.6193288 L16.5094798,7.14219998 Z M12,7.58151705 C9.06816966,8.7682918 7,11.6426224 7,15 C7,19.418278 10.581722,23 15,23 C15.0946843,23 15.1889844,22.9983551 15.2828733,22.9950923 L15.2857115,21.4938333 C15.1909859,21.4979303 15.0957346,21.5 15,21.5 C11.4101491,21.5 8.5,18.5898509 8.5,15 C8.5,12.4922758 9.92010997,10.316236 12,9.23221063 L12,7.58151705 Z" stroke="none" fill="#666666" fillRule="evenodd"></path>
                                <polygon stroke="none" fill="#666666" fillRule="evenodd" transform="translate(12.179731, 8.683036) rotate(65.000000) translate(-12.179731, -8.683036) " points="12.1797308 6.4330363 14.7778071 10.9330363 9.58165463 10.9330363"></polygon>
                                <polygon stroke="none" fill="#666666" fillRule="evenodd" transform="translate(18.878222, 20.919395) rotate(-124.000000) translate(-18.878222, -20.919395) " points="18.8782218 18.6693947 21.476298 23.1693947 16.2801456 23.1693947"></polygon>
                            </svg>
                        )}
                        title="Processing Results"
                        theme="dark"
                    >
                        <div className="aa-project-report-processing-metrics">
                            <ProjectReportStatCard
                                metric={reportTotals.totalFilesUploaded}
                                title="Files uploaded"
                                loading={loadingReportTotals}
                            />

                            <ProjectReportStatCard
                                metric={reportTotals.totalFilesProcessed}
                                title="Files processed"
                                loading={loadingReportTotals}
                            />

                            <ProjectReportStatCard
                                metric={reportTotals.totalFilesProcessed - reportTotals.totalFilesToValidation}
                                title="Files successfully processed"
                                loading={loadingReportTotals}
                            />

                            <ProjectReportStatCard
                                metric={reportTotals.totalFilesToValidation}
                                title="Files sent to validation"
                                theme="purple"
                                loading={loadingReportTotals}
                            />

                            <ProjectReportStatCard
                                metric={reportTotals.totalFilesValidated}
                                title="Files validated"
                                theme="purple"
                                loading={loadingReportTotals}
                            />

                            <ProjectReportStatCard
                                metric={reportTotals.totalFilesUnprocessable}
                                title="Files invalid"
                                theme="purple"
                                loading={loadingReportTotals}
                            />
                        </div>
                    </ProjectReportSection>

                    <ProjectReportSection
                        classes="classification"
                        icon={(
                            <svg width="17px" height="14px" viewBox="0 0 17 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                <ellipse stroke="none" fill="#666666" fillRule="evenodd" cx="13.1764706" cy="11.2" rx="2.82352941" ry="2.8"></ellipse>
                                <rect stroke="none" fill="#666666" fillRule="evenodd" transform="translate(5.200265, 7.017288) rotate(36.000000) translate(-5.200265, -7.017288) " x="4.70026521" y="0.950621135" width="0.941176471" height="12.1333333"></rect>
                                <rect stroke="none" fill="#666666" fillRule="evenodd" transform="translate(10.847324, 6.982712) rotate(-36.000000) translate(-10.847324, -6.982712) " x="10.347324" y="0.916045532" width="0.941176471" height="12.1333333"></rect>
                                <ellipse stroke="none" fill="#666666" fillRule="evenodd" cx="2.82352941" cy="11.2" rx="2.82352941" ry="2.8"></ellipse>
                                <ellipse stroke="none" fill="#666666" fillRule="evenodd" cx="8.32352941" cy="2.8" rx="2.82352941" ry="2.8"></ellipse>
                            </svg>
                        )}
                        title="Classifications"
                    >
                        <div>
                            {(() => {
                                if (loadingReportTotals || !reportTotals.classification) {
                                    return (
                                        <div className="aa-project-report-loading-section">
                                            <img src={routeHelper('images/ring-loader.gif')} />
                                        </div>
                                    );
                                }

                                return (
                                    <ChartWrapper
                                        chart={
                                            <BarChart
                                                chartClassName="rd3-barchart pink"
                                                data={[{
                                                    values: reportTotals.classification.fieldRepresentation
                                                        .slice(0, 6)
                                                        .map((field) => {
                                                            return {
                                                                x: this._truncateAxisName(field.name),
                                                                y: Math.floor(field.representationPercent),
                                                            };
                                                        }),
                                                }]}
                                                width={600}
                                                height={200}
                                                gridHorizontal={true}
                                                yAxisLabel="Field Representation"
                                                yAxisFormatter={(val) => {
                                                    return `${val}%`;
                                                }}
                                                yAxisLabelOffset={50}
                                                margins={barMargins}
                                            />
                                        }
                                    />
                                );
                            })()}
                        </div>
                    </ProjectReportSection>

                    <ProjectReportSection
                        classes="accuracy"
                        icon={(
                            <svg width="30px" height="30px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                <defs>
                                    <path d="M35,34.0702576 L35,18.0028165 C35,15.7979383 33.209139,14 31,14 C28.7953562,14 27,15.792122 27,18.0028165 L27,34.0702576 C24.6087945,35.4534952 23,38.0388706 23,41 C23,45.418278 26.581722,49 31,49 C35.418278,49 39,45.418278 39,41 C39,38.0388706 37.3912055,35.4534952 35,34.0702576 Z" id="path-1"></path>
                                    <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="35" fill="white">
                                        <use xlinkHref="#path-1"></use>
                                    </mask>
                                </defs>
                                <use stroke="#666666" mask="url(#mask-2)" strokeWidth="4" fill="none" xlinkHref="#path-1"></use>
                                <path d="M32,36.1000181 L32,24.9909572 C32,24.4563138 31.5522847,24 31,24 C30.4438648,24 30,24.4436666 30,24.9909572 L30,36.1000181 C27.7177597,36.5632884 26,38.5810421 26,41 C26,43.7614237 28.2385763,46 31,46 C33.7614237,46 36,43.7614237 36,41 C36,38.5810421 34.2822403,36.5632884 32,36.1000181 Z" id="Combined-Shape" stroke="none" fill="#666666" fillRule="evenodd"></path>
                            </svg>
                        )}
                        title="Accuracy"
                        theme="dark"
                    >
                        <div>
                            {(() => {
                                if (loadingReportTotals || !reportTotals.accuracy) {
                                    return (
                                        <div className="aa-project-report-loading-section">
                                            <img src={routeHelper('images/ring-loader.gif')} />
                                        </div>
                                    );
                                }

                                return (
                                    <ChartWrapper
                                        chart={
                                            <BarChart
                                                chartClassName="rd3-barchart purple"
                                                data={[{
                                                    values: reportTotals.accuracy.fields
                                                        .slice(0, 6)
                                                        .map((acc) => {
                                                            return {
                                                                x: this._truncateAxisName(acc.name),
                                                                y: Math.floor(acc.accuracyPercent),
                                                            };
                                                        }),
                                                }]}
                                                width={600}
                                                height={200}
                                                gridHorizontal={true}
                                                yAxisLabel="Field accuracy"
                                                yAxisFormatter={(val) => {
                                                    return `${val}%`;
                                                }}
                                                yAxisLabelOffset={50}
                                                margins={barMargins}
                                            />
                                        }
                                    />
                                );
                            })()}
                        </div>
                    </ProjectReportSection>

                    <ProjectReportSection
                        icon={(
                            <svg width="18px" height="13px" viewBox="6 9 18 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                <path d="M12.8236793,20.9546493 L12.8678391,21 L23,10.5945946 L22.4210194,10 L12.833966,19.8455974 L7.57898062,14.4488953 L7,15.0434899 L12.7898062,20.9894359 L12.8236793,20.9546493 Z" id="Combined-Shape" stroke="#666666" strokeWidth="0.504" fill="#666666" fillRule="evenodd"></path>
                            </svg>
                        )}
                        title="Validation"
                    >
                        <div>
                            {(() => {
                                if (loadingReportTotals || !reportTotals.validation) {
                                    return (
                                        <div className="aa-project-report-loading-section">
                                            <img src={routeHelper('images/ring-loader.gif')} />
                                        </div>
                                    );
                                }

                                return (
                                    <ChartWrapper
                                        chart={
                                            <BarChart
                                                chartClassName="rd3-barchart mint"
                                                data={[{
                                                    values: reportTotals.validation.fields
                                                        .slice(0, 6)
                                                        .map((field) => {
                                                            return {
                                                                x: this._truncateAxisName(field.name),
                                                                y: Math.floor(field.percentValidated),
                                                            };
                                                        }),
                                                }]}
                                                width={500}
                                                height={200}
                                                gridHorizontal={true}
                                                yAxisLabel="Fields"
                                                yAxisFormatter={(val) => {
                                                    return `${val}%`;
                                                }}
                                                yAxisLabelOffset={50}
                                                margins={barMargins}
                                            />
                                        }
                                    />
                                );
                            })()}
                        </div>

                        <div>
                            {(() => {
                                if (loadingReportTotals || !reportTotals.validation) {
                                    return (
                                        <div className="aa-project-report-loading-section">
                                            <img src={routeHelper('images/ring-loader.gif')} />
                                        </div>
                                    );
                                }

                                return (
                                    <ChartWrapper
                                        chart={
                                            <BarChart
                                                chartClassName="rd3-barchart teal"
                                                data={[{
                                                    values: reportTotals.validation.categories
                                                        .slice(0, 6)
                                                        .map((cat) => {
                                                            return {
                                                                x: this._truncateAxisName(cat.name),
                                                                y: (cat.timeSpend / 60).toFixed(1),
                                                            };
                                                        }),
                                                }]}
                                                gridHorizontal={true}
                                                width={500}
                                                height={200}
                                                yAxisLabel="Average time spent (min)"
                                            />
                                        }
                                    />
                                );
                            })()}
                        </div>
                    </ProjectReportSection>
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectProjectReportContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchProjectById: (projectId) => dispatch(fetchProjectById(projectId)),
        fetchReportTotals: (projectId) => dispatch(fetchReportTotals(projectId)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectReportContainer);
