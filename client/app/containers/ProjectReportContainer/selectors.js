import {createSelector} from 'reselect';
import {Map} from 'immutable';

/**
 * Direct selector to the projectReportContainer state domain
 */
const selectProjectReportContainerDomain = () => (state) => state.get('projectReportContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectReportContainer
 */

const selectProjectReportContainer = () => createSelector(
    selectProjectReportContainerDomain(),
    selectProjectsContainerDomain(),
    (substate, projectsState) => {
        const projectData = projectsState.get('currentProject');
        const project = Map.isMap(projectData) ? projectData.toJS() : projectData;

        return {
            ...substate.toJS(),
            project,
            projectLoading: projectsState.get('loadingProjects'),
        };
    }
);

export default selectProjectReportContainer;
export {
    selectProjectReportContainerDomain,
};
