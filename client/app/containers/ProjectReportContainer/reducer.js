/*
 *
 * ProjectReportContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {PERSIST_REPORT_TOTALS, LOADING_REPORT_TOTALS} from './constants';

export const initialState = fromJS({
    reportTotals: {},
    loadingReportTotals: false,
});

function projectReportContainerReducer(state = initialState, action) {
    switch (action.type) {
        case LOADING_REPORT_TOTALS:
            return state.set('loadingReportTotals', action.loading);
        case PERSIST_REPORT_TOTALS:
            return state.set('reportTotals', action.payload);
        default:
            return state;
    }
}

export default projectReportContainerReducer;
