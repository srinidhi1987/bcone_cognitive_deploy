/*
 *
 * LanguageProvider reducer
 *
 */

import {fromJS} from 'immutable';
import {CHANGE_LOCALE, PERSIST_SUPPORTED_LANGUAGES} from './constants';
import {DEFAULT_LOCALE} from '../App/constants'; // eslint-disable-line

export const initialState = fromJS({
    locale: DEFAULT_LOCALE,
    supportedLanguages: [],
});

function languageProviderReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_LOCALE:
            return state
                .set('locale', action.locale);
        case PERSIST_SUPPORTED_LANGUAGES:
            return state.set('supportedLanguages', action.payload);
        default:
            return state;
    }
}

export default languageProviderReducer;
