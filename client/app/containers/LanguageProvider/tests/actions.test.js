/**
 * Created by iamcutler on 3/20/17.
 */

import {FETCH_SUPPORTED_LANGUAGES, PERSIST_SUPPORTED_LANGUAGES} from '../constants';
import {fetchSupportedLanguages, persistSupportedLanguages} from '../actions';

describe('LanguageProvider', () => {
    describe('fetchSupportedLanguages action creator', () => {
        it('should pass correct type for action', () => {
            // given
            const expectedAction = {
                type: FETCH_SUPPORTED_LANGUAGES,
            };
            // when
            const result = fetchSupportedLanguages();
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistSupportedLanguages action creator', () => {
        it('should pass payload into action to save in store', () => {
            // given
            const payload = [
                {
                    id: '456645645456',
                    name: 'English',
                    code: 'Eng',
                },
            ];
            const expectedAction = {
                type: PERSIST_SUPPORTED_LANGUAGES,
                payload,
            };
            // when
            const result = persistSupportedLanguages(payload);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
