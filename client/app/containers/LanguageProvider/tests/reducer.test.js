/**
 * Created by iamcutler on 3/20/17.
 */
import languageProviderReducer, {initialState} from '../reducer';
import {persistSupportedLanguages} from '../actions';

describe('editProjectContainerReducer', () => {
    it('returns the initial state', () => {
        expect(languageProviderReducer(undefined, {})).toEqual(initialState);
    });

    describe('EDIT_PROJECT_LOADING action', () => {
        it('should set loading state in store', () => {
            // given
            const payload = [{}, {}];
            const action = persistSupportedLanguages(payload);
            const expectedState = {
                ...initialState.toJS(),
                supportedLanguages: payload,
            };
            // when
            const result = languageProviderReducer(initialState, action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });
    });
});
