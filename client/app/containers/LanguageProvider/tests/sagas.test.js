/**
 * Created by iamcutler on 3/20/17.
 */
import {call, put} from 'redux-saga/effects';
import {fetchLanguagesFlow, fetchLanguagesFromServer} from '../sagas';
import {persistSupportedLanguages} from '../actions';

describe('LanguageProvider Sagas', () => {
    describe('fetchLanguagesFlow generator', () => {
        describe('success path', () => {
            // given
            const generator = fetchLanguagesFlow();

            it('should call the API to fetch supported languages', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchLanguagesFromServer));
            });

            it('should persist languages in store', () => {
                // given
                const languages = {
                    success: true,
                    data: [{}],
                };
                // when
                const result = generator.next(languages);
                // then
                expect(result.value).toEqual(put(persistSupportedLanguages(languages.data)));
            });
        });
    });
});
