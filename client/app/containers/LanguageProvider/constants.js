/*
 *
 * LanguageProvider constants
 *
 */

export const CHANGE_LOCALE = 'app/LanguageToggle/CHANGE_LOCALE';
export const DEFAULT_LOCALE = 'en';
export const FETCH_SUPPORTED_LANGUAGES = 'app/LanguageProvider/FETCH_SUPPORTED_LANGUAGES';
export const PERSIST_SUPPORTED_LANGUAGES = 'app/LanguageProvider/PERSIST_SUPPORTED_LANGUAGES';
