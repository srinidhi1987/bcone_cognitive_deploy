/**
 * Created by iamcutler on 3/20/17.
 */
import {takeLatest} from 'redux-saga';
import {put, call} from 'redux-saga/effects';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet} from '../../utils/request';

import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {FETCH_SUPPORTED_LANGUAGES} from './constants';
import {persistSupportedLanguages} from './actions';
import routeHelper from '../../utils/routeHelper';

/**
 * Languages
 */
export function fetchLanguagesFromServer() {
    return requestGet(
        routeHelper('api/languages'),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

export function* fetchLanguagesFlow() {
    try {
        const languages = yield call(fetchLanguagesFromServer);
        if (languages.success) {
            yield put(persistSupportedLanguages(languages.data));
        }
        else {
            throw languages.errors;
        }
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
}

// All sagas to be loaded
export default [
    function* () {
        yield* takeLatest(FETCH_SUPPORTED_LANGUAGES, fetchLanguagesFlow);
    },
];
