/*
 *
 * LanguageProvider actions
 *
 */

import {
    CHANGE_LOCALE,
    FETCH_SUPPORTED_LANGUAGES,
    PERSIST_SUPPORTED_LANGUAGES,
} from './constants';

export function changeLocale(languageLocale) {
    return {
        type: CHANGE_LOCALE,
        locale: languageLocale,
    };
}

export function fetchSupportedLanguages() {
    return {
        type: FETCH_SUPPORTED_LANGUAGES,
    };
}

export function persistSupportedLanguages(payload = []) {
    return {
        type: PERSIST_SUPPORTED_LANGUAGES,
        payload,
    };
}
