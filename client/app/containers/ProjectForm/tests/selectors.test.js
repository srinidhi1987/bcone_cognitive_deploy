import {fromJS} from 'immutable';

import selectProjectForm from '../selectors';
import {ProjectFields2, domainLanguages} from '../../../../fixtures/project_fields';

describe('ProjectForm selectors', () => {
    describe('selectProjectForm selector', () => {
        // given
        const currentStandardFields = [1, 2, 3];
        const currentCustomFields = [
            {name: 'testing 1', type: 'FormField'},
            {name: 'testing 2', type: 'TableField'},
        ];

        it('should return props for project form from current form state', () => {
            // given
            const mockedState = fromJS({
                projectsContainer: {
                    projectFieldsCache: ProjectFields2,
                },
                form: fromJS({
                    projectForm: {
                        values: {
                            newCustomFieldName: 'custom name',
                            project: {
                                projectType: 'Invoices',
                                primaryLanguage: 'English',
                                fields: {
                                    standard: currentStandardFields,
                                    custom: currentCustomFields,
                                },
                                newCustomFieldName: '',
                            },
                        },
                        syncErrors: {},
                    },
                }),
                newProjectContainer: {
                    domainLanguages: fromJS(domainLanguages),
                },
                language: fromJS({
                    supportedLanguages: [{id:'1', name:'English'}],
                }),
            });
            // when
            const selector = selectProjectForm();
            // then
            expect(selector(mockedState)).toEqual({
                aliasFields: ProjectFields2,
                availableProjectFields: {
                    form: [],
                    table: [],
                },
                currentCustomFieldName: 'custom name',
                formFields: {
                    standard: currentStandardFields,
                    custom: currentCustomFields,
                },
                isProjectTypeCustom: false,
                currentProjectType: 'Invoices',
                formErrors: {},
                formLevelDisplayErrors: [],
                supportedLanguages: [],
            });
        });

        it('should return formLevelDisplayErrors for form level validations from form errors', () => {
            // given
            const formErrors = [
                'Please upload at least one file',
                'Please select at least one field',
            ];
            const mockedState = fromJS({
                projectsContainer: {
                    projectFieldsCache: ProjectFields2,
                },
                form: fromJS({
                    projectForm: {
                        values: {
                            newCustomFieldName: 'custom name',
                            project: {
                                projectType: 'Invoice',
                                primaryLanguage: 'English',
                                fields: {
                                    standard: currentStandardFields,
                                    custom: currentCustomFields,
                                },
                                newCustomFieldName: '',
                            },
                        },
                        syncErrors: {
                            fileUploadsRequired: formErrors[0],
                            requiredFieldSelection: formErrors[1],
                        },
                    },
                }),
                newProjectContainer: {
                    domainLanguages: fromJS(domainLanguages),
                },
                language: fromJS({
                    supportedLanguages: [{id:'1', name:'English'}],
                }),
            });
            // when
            const selector = selectProjectForm();
            // then
            expect(selector(mockedState).formLevelDisplayErrors).toContain(formErrors[0]);
            expect(selector(mockedState).formLevelDisplayErrors).toContain(formErrors[1]);
        });
    });

    it('should return available project fields based on domain type', () => {
        // given
        const mockedState = fromJS({
            projectsContainer: {
                projectFieldsCache: ProjectFields2,
            },
            form: fromJS({
                projectForm: {
                    values: {
                        newCustomFieldName: 'custom name',
                        project: {
                            projectType: 'Invoice',
                            primaryLanguage: 'English',
                            fields: {
                                standard: [],
                                custom: [],
                            },
                            newCustomFieldName: '',
                        },
                    },
                    syncErrors: {},
                },
            }),
            newProjectContainer: {
                domainLanguages: fromJS(domainLanguages),
            },
            language: fromJS({
                supportedLanguages: [{id:'1', name:'English'}],
            }),
        });
        // when
        const selector = selectProjectForm();
        // then
        expect(selector(mockedState).availableProjectFields).toEqual({
            form: ProjectFields2[0].fields.filter((field) => field.fieldType === 'FormField'),
            table: ProjectFields2[0].fields.filter((field) => field.fieldType === 'TableField'),
        });
    });
});
