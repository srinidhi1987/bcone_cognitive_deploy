import {fromJS} from 'immutable';

import {fieldValidations, formLevelValidations} from '../validations';
import {ALLOWED_FILE_TYPES} from '../../ProjectsContainer/constants';

describe('Form validations', () => {
    describe('field validations', () => {
        describe('required', () => {
            it('should return undefined if value is present', () => {
                // given
                // when
                const result = fieldValidations.required('Testing');
                // then
                expect(result).toBe(undefined);
            });

            it('should return validation string if value is not present', () => {
                // given
                // when
                const result = fieldValidations.required(null);
                // then
                expect(result).toBe('Required');
            });

            it('should return validation string if value is empty space', () => {
                const result = fieldValidations.required(' ');
                expect(result).toBe('Required');
            });
        });

        describe('format', () => {
            it('should return undefined if value is not present', () => {
                // given
                // when
                const result = fieldValidations.format(null);
                // then
                expect(result).toBe(undefined);
            });

            it('should return undefined if value passes formatting', () => {
                // given
                // when
                const result = fieldValidations.format('Project Name_1234');
                // then
                expect(result).toBe(undefined);
            });

            it('should return validation string if value does NOT pass formatting', () => {
                // given
                // when
                const result = fieldValidations.format('Testing %$#');
                // then
                expect(result).toBe('Formatting must be numbers, letters, spaces or (_, -)');
            });
        });

        describe('method: noOutsideWhitespace', () => {
            describe('when value is not present', () => {
                it('should return undefined', () => {
                    // given
                    // when
                    const result = fieldValidations.noOutsideWhitespace(null);
                    // then
                    expect(result).toBe(undefined);
                });
            });

            describe('if value passes formatting', () => {
                // given
                const passingExamples = [
                    'Project Name_1234 Testing',
                    'anything Hello',
                    '23423.$4324',
                    'In  The  Middle.',
                    'a',
                ];

                passingExamples.forEach((exampleString) => {
                    it(`should return undefined, example string: "${exampleString}"`, () => {
                        // when
                        // then
                        expect(fieldValidations.noOutsideWhitespace(exampleString)).toBe(undefined);
                    });
                });
            });

            describe('if value does NOT pass formatting', () => {
                // given
                const failingExamples = [
                    ' Project Name_1234',
                    '  Sample',
                    'At The End ',
                    'Anything 2  ',
                    ' ',
                    '  ',
                    ' a',
                    'a ',
                ];

                failingExamples.forEach((exampleString) => {
                    const errorString = 'Formatting must not have spaces at the beginning or end.';
                    it(`should return validation error string, example string: "${exampleString}"`, () => {
                        // when
                        // then
                        expect(fieldValidations.noOutsideWhitespace(exampleString)).toBe(errorString);
                    });
                });
            });
        });

        describe('method: noRepeatingWhitespace', () => {
            describe('when value is not present', () => {
                it('should return undefined', () => {
                    // given
                    // when
                    const result = fieldValidations.noRepeatingWhitespace(null);
                    // then
                    expect(result).toBe(undefined);
                });
            });

            describe('if value passes formatting', () => {
                // given
                const passingExamples = [
                    'Project Name_1234 Testing ',
                    'anything Hello',
                    ' 23423.$4324',
                    'In The Middle.',
                ];

                passingExamples.forEach((exampleString) => {
                    it(`should return undefined, example string: "${exampleString}"`, () => {
                        // when
                        // then
                        expect(fieldValidations.noRepeatingWhitespace(exampleString)).toBe(undefined);
                    });
                });
            });

            describe('if value does NOT pass formatting', () => {
                // given
                const failingExamples = [
                    ' Project  Name_1234',
                    '  Sample',
                    'At The End  ',
                    '  ',
                ];

                failingExamples.forEach((exampleString) => {
                    const errorString = 'Formatting must not have more than 1 space in a row.';
                    it(`should return validation error string, example string: "${exampleString}"`, () => {
                        // when
                        // then
                        expect(fieldValidations.noRepeatingWhitespace(exampleString)).toBe(errorString);
                    });
                });
            });
        });

        describe('extend format with commas and periods', () => {
            it('should return undefined if value is not present', () => {
                // given
                // when
                const result = fieldValidations.extendedFormat(null);
                // then
                expect(result).toBe(undefined);
            });

            it('should return undefined if value passes formatting', () => {
                // given
                // when
                const result = fieldValidations.extendedFormat('Project Name_1234');
                // then
                expect(result).toBe(undefined);
            });

            it('should return undefined if value passes formatting with periods or commas', () => {
                // given
                // when
                const result = fieldValidations.extendedFormat('Project Description. Go be, great');
                // then
                expect(result).toBe(undefined);
            });

            it('should return validation string if value does NOT pass formatting', () => {
                // given
                // when
                const result = fieldValidations.extendedFormat('Testing %$#');
                // then
                expect(result).toBe('Formatting must be numbers, letters, spaces, periods, commas, or (_, -)');
            });
        });

        describe('fileSetSize', () => {
            it('should return undefined if value is not present', () => {
                // given
                // when
                const result = fieldValidations.fileSetSize(255)(null);
                // then
                expect(result).toBe(undefined);
            });

            it('should return undefined if value is less than file list size', () => {
                // given
                // when
                const result = fieldValidations.fileSetSize(10)([{}, {}, {}]);
                // then
                expect(result).toBe(undefined);
            });

            it('should return undefined if value is equal to the file list size', () => {
                // given
                // when
                const result = fieldValidations.fileSetSize(3)([{}, {}, {}]);
                // then
                expect(result).toBe(undefined);
            });

            it('should return validation error message if value is more than file list size', () => {
                // given
                // when
                const result = fieldValidations.fileSetSize(2)([{}, {}, {}]);
                // then
                expect(result).toBe('File uploads are limited to 2 files');
            });
        });

        describe('length', () => {
            it('should return undefined if value is not present', () => {
                // given
                // when
                const result = fieldValidations.length(255)(null);
                // then
                expect(result).toBe(undefined);
            });

            it('should return undefined if value passes', () => {
                // given
                // when
                const result = fieldValidations.length(255)('Project Name_1234');
                // then
                expect(result).toBe(undefined);
            });

            it('should return validation string if value is out of bounds', () => {
                // given
                // when
                const result = fieldValidations.length(10)('Project Name_1234');
                // then
                expect(result).toBe('Length should be under 10 characters');
            });
        });

        describe('duplicateFieldName', () => {
            // given
            const availableProjectFields = {
                form: [{
                    name: 'Form Name 1',
                }],
                table: [{
                    name: 'Table Name 1',
                }],
            };
            const formFields = {
                standard: [],
                custom: [
                    {
                        name: 'Testing 1',
                        type: 'TableField',
                    },
                ],
            };
            const errorMsg = 'No duplicate field names allowed';

            it('should return undefined if no duplicate is found', () => {
                // given
                // when
                const result = fieldValidations.duplicateFieldName(availableProjectFields, formFields)('Project Name_1234');
                // then
                expect(result).toBe(undefined);
            });

            it('should return validation string if duplicate in form fields', () => {
                // given
                // when
                const result = fieldValidations.duplicateFieldName(availableProjectFields, formFields)('Form Name 1');
                // then
                expect(result).toBe(errorMsg);
            });

            it('should return validation string if duplicate in table fields', () => {
                // given
                // when
                const result = fieldValidations.duplicateFieldName(availableProjectFields, formFields)('Table name 1');
                // then
                expect(result).toBe(errorMsg);
            });

            it('should return validation string if duplicate in custom fields', () => {
                // given
                // when
                const result = fieldValidations.duplicateFieldName(availableProjectFields, formFields)('Testing 1');
                // then
                expect(result).toBe(errorMsg);
            });
        });
    });

    describe('form level validations', () => {
        it('requiredFieldSelection error', () => {
            // given
            const formValues = fromJS({
                project: {
                    fields: {
                        standard: [],
                        custom: [],
                    },
                },
                files: [{}],
            });
            // when
            const result = formLevelValidations(formValues);
            // then
            expect(result).toEqual({
                requiredFieldSelection: 'Please select at least one field',
            });
        });

        it('fileUploadsRequired error', () => {
            // given
            const formValues = fromJS({
                project: {
                    fields: {
                        standard: [1, 2],
                        custom: [],
                    },
                },
                files: [],
            });
            // when
            const result = formLevelValidations(formValues);
            // then
            expect(result).toEqual({
                fileUploadsRequired: 'Please upload at least one file',
            });
        });

        it('all validations passed', () => {
            // given
            const formValues = fromJS({
                project: {
                    fields: {
                        standard: [1, 2],
                        custom: [],
                    },
                },
                files: [{}],
            });
            // when
            const result = formLevelValidations(formValues);
            // then
            expect(result).toEqual({});
        });
    });

    describe('checkFileSize validation', () => {
        it('should return undefined if all files are under file max threshold', () => {
            // given
            const fileList = [
                {
                    lastModified: 1444336188000,
                    lastModifiedDate: new Date(),
                    name: 'testing.tif',
                    size: 4194304, // 4MB
                    type: 'image/tiff',
                }, {
                    lastModified: 1444336188000,
                    lastModifiedDate: new Date(),
                    name: 'testing1.tif',
                    size: 4928307, // 4.7MB
                    type: 'image/tiff',
                }, {
                    lastModified: 1444336188000,
                    lastModifiedDate: new Date(),
                    name: 'testing2.tif',
                    size: 12582912, // 12MB
                    type: 'image/tiff',
                },
            ];
            // when
            const result = fieldValidations.checkFileSize()(fileList);
            // then
            expect(result).toBe(undefined);
        });

        it('should return validation string if any files are over file max threshold', () => {
            // given
            const fileList = [
                {
                    lastModified: 1444336188000,
                    lastModifiedDate: new Date(),
                    name: 'testing.tif',
                    size: 6291456, // 6MB
                    type: 'image/tiff',
                }, {
                    lastModified: 1444336188000,
                    lastModifiedDate: new Date(),
                    name: 'testing1.tif',
                    size: 13631488, // 13MB
                    type: 'image/tiff',
                }, {
                    lastModified: 1444336188000,
                    lastModifiedDate: new Date(),
                    name: 'testing2.tif',
                    size: 1572864, // 1.5MB
                    type: 'image/tiff',
                },
            ];
            // when
            const result = fieldValidations.checkFileSize()(fileList);
            // then
            expect(result).toBe('You have selected files above the max file size. Limit to under 12 MB per file.');
        });
    });

    describe('checkAcceptedFileTypes validation', () => {
        it('should return validation string if one or more files have a invalid extension', () => {
            // given
            const files = [
                {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'JPEGInvoice-1_2_1 copy 11.jpeg',
                    size: 560124,
                    type: 'image/jpeg',
                }, {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'JPEGInvoice-1_2_1 copy 11.exe',
                    size: 560124,
                    type: 'application/x-exe',
                },
            ];
            // when
            const result = fieldValidations.checkAcceptedFileTypes(ALLOWED_FILE_TYPES)(files);
            // then
            expect(result).toEqual(`Allowed file types: ${ALLOWED_FILE_TYPES.join(', ')}`);
        });

        //'.jpg', '.jpeg', '.pdf', '.tiff', '.tif', '.png'
        it('should return undefined if all files have valid extensions', () => {
            // given
            const files = [
                {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'JPEGInvoice-1_2_1 copy 11.jpg',
                    size: 560124,
                    type: 'image/jpeg',
                }, {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'JPEGInvoice-1_2_1 copy 11.jpeg',
                    size: 560124,
                    type: 'image/jpeg',
                }, {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'Invoice1.pdf',
                    size: 560124,
                    type: 'application/pdf',
                }, {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'Invoice2.png',
                    size: 560124,
                    type: 'image/png',
                }, {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'Invoice2.tiff',
                    size: 560124,
                    type: 'image/tiff',
                }, {
                    lastModified: 1439340742000,
                    lastModifiedDate: 1439340742000,
                    name: 'Invoice2.tif',
                    size: 560124,
                    type: 'image/tiff',
                },
            ];
            // when
            const result = fieldValidations.checkAcceptedFileTypes()(files);
            // then
            expect(result).toBe(undefined);
        });

        it('should return undefined if no files are present', () => {
            // given
            // when
            const result = fieldValidations.checkAcceptedFileTypes()([]);
            // then
            expect(result).toBe(undefined);
        });
    });
    describe('cannotBeOnlyWhitespace validation', () => {

        describe('it should pass all the values which are not only white spaces', () => {
            // given
            const passingExamples = [
                '',
                'passing_text',
                'field name',
                ' aa',
                'a ',
                'asas1  ',
            ];

            passingExamples.forEach((exampleString) => {
                it(`should return undefined, example string: "${exampleString}"`, () => {
                    // when
                    // then
                    expect(fieldValidations.cannotBeOnlyWhitespace(exampleString)).toBe(undefined);
                });
            });
        });

        describe('it should fail all the values which are just an white space', () => {
            // given
            const failingExamples = [
                ' ',
                '  ',
            ];

            failingExamples.forEach((exampleString) => {
                const errorString = 'Formating must not have value as empty space';
                it(`should return validation error string, example string: "${exampleString}"`, () => {
                    // when
                    // then
                    expect(fieldValidations.cannotBeOnlyWhitespace(exampleString)).toBe(errorString);
                });
            });
        });
    });
    describe('formatWithoutSpecialCharacter validation', () => {

        describe('it should pass all the values without special character', () => {
            // given
            const passingExamples = [
                '',
                'Invoice Number',
                'Invoice',
                '',
                'asas One1',
            ];

            passingExamples.forEach((exampleString) => {
                it(`should return undefined, example string: "${exampleString}"`, () => {
                    // when
                    // then
                    expect(fieldValidations.formatWithoutSpecialCharacter(exampleString)).toBe(undefined);
                });
            });
        });

        describe('it should fail all the values with special character', () => {
            // given
            const failingExamples = [
                '@2',
                '@a',
                '%invoice',
                '*x',
                '122sd_',
                '_',
                'Invoice_Number',
            ];

            failingExamples.forEach((exampleString) => {
                const errorString = 'Formatting must be numbers, letters or spaces';
                it(`should return validation error string, example string: "${exampleString}"`, () => {
                    // when
                    // then
                    expect(fieldValidations.formatWithoutSpecialCharacter(exampleString)).toBe(errorString);
                });
            });
        });
    });
    describe('formatNotStartWithNumber validation', () => {

        describe('it should pass all the values which does not start with number', () => {
            // given
            const passingExamples = [
                '',
                'passing text',
                'field nameq',
                ' aax23',
                'a_2',
                'asas1',
            ];

            passingExamples.forEach((exampleString) => {
                it(`should return undefined, example string: "${exampleString}"`, () => {
                    // when
                    // then
                    expect(fieldValidations.formatNotStartWithNumber(exampleString)).toBe(undefined);
                });
            });
        });

        describe('it should fail all the values which does start with number', () => {
            // given
            const failingExamples = [
                '1 invoice',
                '12 _23ads',
            ];

            failingExamples.forEach((exampleString) => {
                const errorString = 'Formatting must not start with numbers';
                it(`should return validation error string, example string: "${exampleString}"`, () => {
                    // when
                    // then
                    expect(fieldValidations.formatNotStartWithNumber(exampleString)).toBe(errorString);
                });
            });
        });
    });
    describe('filesSetNameLength validation', () => {
        const max = 150;
        describe('it should pass for all the files having file name lenght under 150 character', () => {
            // given
            const passingExamples = [
                {name:'0123456789.png'},
                {name:'file name.png'},
                {name: '01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345.png'},
            ];
            it('should return undefined for passsing file list', () => {
                // when
                // then
                expect(fieldValidations.filesSetNameLength(max)(passingExamples)).toBe(undefined);
            });
        });

        describe(`it should fail for files having filename more than ${max} character`, () => {
            // given
            const failingExamples = [
                {name:'file name.png'},
                {name: '012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456.png'},
                {name: '01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345filename.png'},
            ];
            const errorString = `Filenames shouldn’t exceed ${max} characters.`;
            it('it should return error string for givine failing file list', () => {
                // when
                // then
                expect(fieldValidations.filesSetNameLength(150)(failingExamples)).toBe(errorString);
            });
        });
    });
});
