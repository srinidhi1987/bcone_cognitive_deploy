/**
 * Created by iamcutler on 3/10/17.
 */

import {formatBytes} from '../../utils/helpers';
import {ALLOWED_FILE_TYPES} from '../../containers/ProjectsContainer/constants';

export const fieldValidations = {
    required(value) {
        return !value || /^\s*$/.test(value) ? 'Required' : undefined;
    },
    format(value) {
        if (!value) return undefined;

        return /[^a-zA-Z0-9\s_\-]+/g.test(value) ? 'Formatting must be numbers, letters, spaces or (_, -)' : undefined;
    },
    noOutsideWhitespace(value) {
        if (!value) return undefined;

        return /^\s.*$/.test(value) || /^.*\s$/.test(value) ? 'Formatting must not have spaces at the beginning or end.' : undefined;
    },
    noRepeatingWhitespace(value) {
        if (!value) return undefined;

        return /.*\s\s+.*/g.test(value) ? 'Formatting must not have more than 1 space in a row.' : undefined;
    },
    extendedFormat(value) {
        if (!value) return undefined;

        return /[^a-zA-Z0-9\s_\-\,\.]+/g.test(value) ? 'Formatting must be numbers, letters, spaces, periods, commas, or (_, -)' : undefined;
    },
    fileSetSize(size = 150) {
        return (value) => {
            if (!value || !value.length) return undefined;

            return value.length <= size ? undefined : `File uploads are limited to ${size} files`;
        };
    },
    length(length = 255) {
        return (value) => {
            if (!value) return undefined;

            return value.length <= length ? undefined : `Length should be under ${length} characters`;
        };
    },
    duplicateFieldName(availableFields, formFields) {
        return (value) => {
            if (!value) return undefined;

            // make sure there are no duplicate all document fields
            if ([
                ...formFields.custom,
                ...availableFields.form.map((field) => {
                    return {
                        name: field.name,
                    };
                }),
                ...availableFields.table.map((field) => {
                    return {
                        name: field.name,
                    };
                }),
            ].find((field) => field.name.toLowerCase() === value.toLowerCase())) {
                return 'No duplicate field names allowed';
            }

            return undefined;
        };
    },
    // file size limit is 12MB by default
    checkFileSize(max = 12582912) {
        return (value) => {
            if (!value || !value.length) return undefined;

            return value.some((file) => file && file.size > max) ?
                `You have selected files above the max file size. Limit to under ${formatBytes(max)} per file.` :
                undefined;
        };
    },
    /**
     * Check for allowed file extensions
     *
     * @param {array} types
     * @returns {string|undefined}
     */
    checkAcceptedFileTypes(types = ALLOWED_FILE_TYPES) {
        return (files) => {
            if (!files) return undefined;

            const fileTypeRegex = new RegExp(`(${ALLOWED_FILE_TYPES.join('|')})$`, 'i');

            for (let i = 0; i < files.length; i++) {
                if (!fileTypeRegex.test(files[i].type) ||
                    !fileTypeRegex.test(files[i].name)
                ) {
                    return `Allowed file types: ${types.join(', ')}`;
                }
            }

            return undefined;
        };
    },
    cannotBeOnlyWhitespace(value) {
        return value && /^\s*$/.test(value) ? 'Formating must not have value as empty space' : undefined;
    },
    formatWithoutSpecialCharacter(value) {
        if (!value) return undefined;

        return /[^a-zA-Z0-9\s]+/g.test(value) ? 'Formatting must be numbers, letters or spaces' : undefined;
    },
    formatNotStartWithNumber(value) {
        if (!value) return undefined;

        return /^[0-9]+/g.test(value) ? 'Formatting must not start with numbers' : undefined;
    },
    filesSetNameLength(max = 150) {
        return (value) => {
            if (!value) return undefined;
            return value.some((file) => file && file.name.length > max) ?
                `Filenames shouldn’t exceed ${max} characters.` :
                undefined;
        };
    },
    numberFormat(value) {
        if (!value) return null;
        return /^[+-]?\d+(\.\d+)?$/.test(value) ? null : 'Invalid number format.';
    },
    validateByRegex(value, expression) {
        if (!value) return null;
        const formatRegEx = new RegExp(expression);
        return formatRegEx.test(value) ? null : 'Invalid value format.';
    },
    /**
     * dateValidation
     *
     * @param {string} value
     * @param {string} pattern
     * @returns {string}
     */
    dateValidation(value, pattern) {
        if (!value) return null;
        if (!pattern) {
            const parseResult = Date.parse(value);
            return isNaN(parseResult) ? 'Invalid date format.' : null;
        }
        const dateValidationMessage = `Pattern validation failed. Value does not match the pattern '${pattern}'`;
        switch (pattern.toLowerCase()) {
            case 'yyyy-mm-dd':
                return /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/.test(value) ? null : dateValidationMessage;
            case 'dd-mm-yyyy':
                return /((0[1-9]|[12]\d|3[01])-(0[1-9]|1[0-2])-[12]\d{3})/.test(value) ? null : dateValidationMessage;
            case 'dd-mmm-yyyy':
                return /^(([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\-\d{4}$/.test(value) ? null : dateValidationMessage;
            case 'yyyy/mm/dd':
                return /([12]\d{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01]))/.test(value) ? null : dateValidationMessage;
            case 'dd/mm/yyyy':
                return /((0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3})/.test(value) ? null : dateValidationMessage;
            case 'dd/mm/yy':
                return /((0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/\d\d)$/.test(value) ? null : dateValidationMessage;
            case 'dd/mmm/yyyy':
                return /^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\/\d{4}$/.test(value) ? null : dateValidationMessage;
            case 'mm-dd-yyyy':
                return /((0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])-[12]\d{3})/.test(value) ? null : dateValidationMessage;
            case 'mm/dd/yyyy':
                return /((0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01])\/[12]\d{3})/.test(value) ? null : dateValidationMessage;
            default:
                return null;
        }
    },
    validatorEmptyValueValidation(value) {
        const validationMessage = 'Value cannot be empty.';
        return value && /^\s*$/.test(value) ? validationMessage : (!value || /^\s*$/.test(value) ? validationMessage : null);
    },
    validatorListValidation(value, validationFieldData) {
        return validationFieldData && validationFieldData.ValidatorData && (validationFieldData.ValidatorData.StaticListItems.includes(value) ? null : 'List validation failed.');
    },
};

export const formLevelValidations = (values) => {
    if (!values) return {};

    const formValues = values.toJS();
    const projectData = formValues.project.fields;
    const files = formValues.files;
    const fields = [
        ...projectData.standard,
        ...projectData.custom,
    ];
    const errors = {};

    if (!files || files.length === 0) {
        errors.fileUploadsRequired = 'Please upload at least one file';
    }

    if (fields.length === 0) {
        errors.requiredFieldSelection = 'Please select at least one field';
    }

    return errors;
};
