/*
 *
 * ProjectFormContainer
 *
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import FontAwesome from 'react-fontawesome';
import {fromJS} from 'immutable';
import {reduxForm, Field, change} from 'redux-form/immutable';
import {
    TextField,
    SelectField,
    CheckboxInput,
    CommandButton,
    FieldLabel,
    Popup,
    Tooltip,
} from 'common-frontend-components';
import PageLoadingOverlay from '../../components/PageLoadingOverlay';

import {fetchProjectDomains} from '../ProjectsContainer/actions';
import {ALLOWED_FILE_TYPES, MAX_FILES_UPLOAD} from '../ProjectsContainer/constants';
import BrowseField from '../../components/BrowseField';
import InfoTooltip from '../../components/InfoTooltip';
import {selectProjectForm} from './selectors';
import {fieldValidations, formLevelValidations} from './validations';
import {INSTANCE_NAME} from '../../constants/productTerms';
import {
    QA_NEW_PROJECT_DOMAIN_WRAPPER,
    QA_NEW_PROJECT_LANGUAGE_WRAPPER,
    QA_NEW_PROJECT_CUSTOM_FIELDS_ADD,
} from '../../constants/qaElementIds';

import {selectInputOption} from './constants';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class ProjectForm extends Component {
    static displayName = 'ProjectForm';

    static propTypes = {
        /**
         * Disable user defined fields if editing project
         */
        disableFields: PropTypes.bool,
        /**
         * Submit button label
         */
        submitBtnLabel: PropTypes.string,
    };

    static defaultProps = {
        disableFields: false,
        submitBtnLabel: 'Create instance and analyze',
    };

    state = {
        submitAttempted: false,
    };

    componentWillMount() {
        this.props.loadProjectDomains();
    }

    componentWillReceiveProps(newProps) {
        const {currentProjectType, changeFieldValue, aliasFields} = this.props;
        // reset document fields if type changes
        if (currentProjectType !== newProps.currentProjectType && newProps.isProjectTypeCustom) {
            changeFieldValue('project.fields.standard', []);
            changeFieldValue('project.fields.custom', []);
        }
        else if (currentProjectType !== newProps.currentProjectType && !newProps.isProjectTypeCustom) {
            // select all available standard fields
            changeFieldValue('project.fields.standard', [
                ...newProps.availableProjectFields.form,
                ...newProps.availableProjectFields.table,
            ].reduce((acc, field) => {
                if (field.isDefaultSelected) acc.push(field.id);
                return acc;
            }, []));
            changeFieldValue('project.fields.custom', []);
            changeFieldValue('project.customProjectType', null);
        }
        // set project type id
        if (currentProjectType !== newProps.currentProjectType) {
            changeFieldValue('project.projectTypeId', aliasFields.reduce((acc, field) => {
                if (field.name === newProps.currentProjectType) {
                    return field.id;
                }
                return acc;
            }, 0));
        }
    }

    createCustomField(type) {
        const {
            currentCustomFieldName, formFields, change, formErrors,
        } = this.props;
        if (currentCustomFieldName) {
            const customFields = formFields.custom;

            // check if custom name has validation issues
            // don't add if validations fail
            if (formErrors && formErrors.hasOwnProperty('newCustomFieldName')) {
                return;
            }
            customFields.push({name: currentCustomFieldName, type});
            change('project.fields.custom', fromJS(customFields));
            change('newCustomFieldName', '');
        }
    }

    removeCustomProjectField(name, type) { // eslint-disable-line camelcase
        const {change, formFields} = this.props;

        const customFields = formFields.custom;
        const index = customFields.findIndex((field) => field.name === name && field.type === type);

        if (index !== -1) {
            customFields.splice(index, 1);
            change('project.fields.custom', fromJS(customFields));
        }
    }

    selectStandardFieldHandler(selectedField) {
        const {change, formFields} = this.props;

        const standardFields = formFields.standard;
        const standardFieldIndex = standardFields.indexOf(selectedField);

        if (standardFieldIndex !== -1) {
            standardFields.splice(standardFieldIndex, 1);
            return change('project.fields.standard', fromJS(standardFields));
        }

        standardFields.push(selectedField);
        change('project.fields.standard', fromJS(standardFields));
    }

    /**
     * Render standard and custom project fields
     **/
    _renderStandardFields(title, fields) {
        const {disableFields, formFields} = this.props;

        if (fields && fields.length) {
            return (
                <div>
                    <div className="aa-well-header">
                        {title}
                    </div>

                    <div className="aa-project-fields">
                        {fields.map((field) => (
                            <div className="aa-project-fields--field" key={field.id}>
                                <CheckboxInput
                                    checked={(formFields.standard.indexOf(field.id) !== -1) ? 'checked' : ''}
                                    onChange={() => this.selectStandardFieldHandler(field.id)}
                                    disabled={disableFields}
                                >
                                    {field.name}
                                </CheckboxInput>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }

    _renderCustomFields() {
        const {disableFields, formFields} = this.props;

        return formFields.custom.map((field, i) => {
            return (
                <div className="aa-project-custom-field aa-grid-row vertical-bottom" key={i}>
                    <div>
                        <FieldLabel label={`${field.type.toLowerCase().replace(/field/, '')} field`}>
                            <Field
                                name="customField[]"
                                component={TextField}
                                placeholder={field.name}
                                readOnly={true}
                            />
                        </FieldLabel>
                    </div>

                    <div>
                        {(!disableFields) ?
                            <CommandButton
                                theme="error"
                                recommended
                                onClick={() => this.removeCustomProjectField(field.name, field.type)}
                            >
                                <FontAwesome
                                    name="close"
                                />
                            </CommandButton>
                            : null}
                    </div>
                </div>
            );
        });
    }

    _renderBrowseField() {
        // this must be a span tag as per IE Edge known bug of infinite loop
        // when input (type=file) wrapped by a label tag

        // reference to bug: https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/8282613/

        // the Edge version where the bug is being experienced is `Microsoft Edge 38.14393.1066.0`

        return (
            <div>
                <FieldLabel tag="span" label={(
                    <div>
                        <span>Upload files (max. {MAX_FILES_UPLOAD})</span>
                        <Tooltip>
                            <div className="project-form-popup">
                                <p>
                                    <strong>Upload files</strong> from your computer to be analyzed. Example files include Invoices, Purchase Orders, etc. Maximum {MAX_FILES_UPLOAD} files at once.
                                </p>
                            </div>
                        </Tooltip>
                    </div>
                )}>
                    <Field
                        type="file"
                        name="files"
                        component={BrowseField}
                        multiple={true}
                        acceptTypes={`.${ALLOWED_FILE_TYPES.join(',.')}`}
                        validate={[
                            fieldValidations.fileSetSize(MAX_FILES_UPLOAD),
                            fieldValidations.checkFileSize(),
                            fieldValidations.checkAcceptedFileTypes(),
                            fieldValidations.filesSetNameLength(150),
                        ]}
                    />
                </FieldLabel>
            </div>
        );
    }

    render() {
        const {
            disableFields, submitBtnLabel, isProjectTypeCustom, aliasFields,
            availableProjectFields, formFields, formLevelDisplayErrors,
            supportedLanguages,
        } = this.props;
        const {submitAttempted} = this.state;

        return (
            <form
                className="aa-project-form"
                onSubmit={(event) => {
                    event.preventDefault();

                    const {
                        handleSubmit, values, asyncValidate, formLevelDisplayErrors,
                    } = this.props;

                    // set attempted state to show form level validations
                    // Due to known issue with redux-form/immutable issue
                    // ignores form level validations and submits
                    // e.g: https://github.com/erikras/redux-form/issues/1356
                    this.setState({submitAttempted: true});

                    if (!formLevelDisplayErrors.length) {
                        return handleSubmit(this.submit, values, this.props, asyncValidate);
                    }
                }}
            >
                {
                    !aliasFields.length ?
                    <PageLoadingOverlay
                        label={'Loading domains...'}
                        show={true}
                        fullscreen={false} /> : null

                }
                <div className="aa-project-form-flex">
                    {/** Form input controls **/}
                    <div>
                        <FieldLabel label={(
                            <div>
                                <span>Instance Name</span>
                                <InfoTooltip term={INSTANCE_NAME}/>
                            </div>
                        )}>
                            <Field
                                name="project.name"
                                component={TextField}
                                validate={[
                                    fieldValidations.required,
                                    fieldValidations.length(50),
                                    fieldValidations.format,
                                    fieldValidations.noOutsideWhitespace,
                                    fieldValidations.noRepeatingWhitespace,
                                ]}
                            />
                        </FieldLabel>
                    </div>

                    <div>
                        <FieldLabel label={(
                            <div>
                                <span>Description (Optional)</span>
                                <Tooltip>
                                    <div className="project-form-popup">
                                        <p>
                                            The <strong>Description</strong> is a little bit more information to help you identify your Learning Instance. Optional.
                                        </p>
                                    </div>
                                </Tooltip>
                            </div>
                        )}>
                            <Field
                                name="project.description"
                                component={TextField}
                                validate={[
                                    fieldValidations.length(255),
                                    fieldValidations.extendedFormat,
                                ]}
                            />
                        </FieldLabel>
                    </div>

                    <div id={QA_NEW_PROJECT_DOMAIN_WRAPPER}>
                        <FieldLabel label={(
                            <div>
                                <span>Domain</span>
                                <Tooltip>
                                    <div className="project-form-popup">
                                        <p>
                                            Select a <strong>Domain</strong> aka a type of file that you will be uploading.
                                        </p>
                                    </div>
                                </Tooltip>
                            </div>
                        )}>
                            <Field
                                name="project.projectType"
                                component={SelectField}
                                options={[
                                    selectInputOption,
                                    ...aliasFields.map((field) => {
                                        return {
                                            value: field.name,
                                            label: field.name,
                                        };
                                    }),
                                    ...[{value: 'other', label: 'Other'}],
                                ]}
                                validate={[fieldValidations.required]}
                                disabled={disableFields}
                            />
                        </FieldLabel>
                    </div>

                    {this._renderBrowseField()}

                    <div id={QA_NEW_PROJECT_LANGUAGE_WRAPPER}>
                        <FieldLabel label={(
                            <div>
                                <span>Primary language of files</span>
                                <Tooltip>
                                    <div className="project-form-popup">
                                        <p>
                                            Select the <strong>primary language of files</strong> that you will be uploading.
                                            Once you choose the language, choose which fields you expect to find on your documents.
                                        </p>
                                    </div>
                                </Tooltip>
                            </div>
                        )}>
                            <Field
                                name="project.primaryLanguage"
                                component={SelectField}
                                options={[
                                    selectInputOption,
                                    ...supportedLanguages.map((lang) => {
                                        return {
                                            value: lang.id.toString(),
                                            label: lang.name,
                                        };
                                    }),
                                ]}
                                validate={[fieldValidations.required]}
                                value={supportedLanguages.length ? supportedLanguages[0].name : ''}
                            />
                        </FieldLabel>
                    </div>

                    <div>
                        {(() => {
                            if (isProjectTypeCustom) {
                                return (
                                    <FieldLabel label={(
                                            <div>
                                                <span>Domain Name</span>
                                            </div>
                                        )}
                                    >
                                        <Field
                                            name="project.customProjectType"
                                            component={TextField}
                                            validate={[
                                                fieldValidations.required,
                                                fieldValidations.length(50),
                                            ]}
                                            disabled={disableFields}
                                        />
                                    </FieldLabel>
                                );
                            }
                        })()}
                    </div>
                    {/** End Form input controls **/}

                    {(() => {
                        if (availableProjectFields.form.length
                            || availableProjectFields.table.length
                            || isProjectTypeCustom
                        ) {
                            return (
                                <div className="aa-fields">
                                    <div className="aa-well">
                                        {this._renderStandardFields('Standard form fields', this.props.availableProjectFields.form)}
                                        {this._renderStandardFields('Standard table fields', this.props.availableProjectFields.table)}

                                        <div className="aa-well-header">
                                            {(isProjectTypeCustom) ? 'Instance Fields' : 'Other fields (Optional)'}
                                        </div>

                                        <div className="aa-project-custom-fields">
                                            {this._renderCustomFields()}

                                            {(!disableFields) ?
                                                <div className="aa-project-custom-fields-add aa-grid-row horizontal-left" id={QA_NEW_PROJECT_CUSTOM_FIELDS_ADD}>
                                                    <div>
                                                        <FieldLabel
                                                            label=""
                                                        >
                                                            <Field
                                                                name="newCustomFieldName"
                                                                component={TextField}
                                                                validate={[
                                                                    fieldValidations.length(50),
                                                                    fieldValidations.duplicateFieldName(availableProjectFields, formFields),
                                                                    fieldValidations.cannotBeOnlyWhitespace,
                                                                    fieldValidations.noOutsideWhitespace,
                                                                    fieldValidations.formatWithoutSpecialCharacter,
                                                                    fieldValidations.formatNotStartWithNumber,
                                                                    fieldValidations.noRepeatingWhitespace,
                                                                ]}
                                                            />
                                                        </FieldLabel>
                                                    </div>

                                                    <div className="aa-grid-column vertical-center">
                                                        <CommandButton
                                                            recommended
                                                            onClick={() => this.createCustomField('FormField')}
                                                        >
                                                            Add as form
                                                        </CommandButton>
                                                    </div>

                                                    <div className="aa-grid-column vertical-center">
                                                        <CommandButton
                                                            recommended
                                                            onClick={() => this.createCustomField('TableField')}
                                                        >
                                                            Add as table
                                                        </CommandButton>
                                                    </div>
                                                </div>
                                                : null}
                                        </div>
                                    </div>
                                </div>
                            );
                        }
                    })()}
                </div>

                {/** Form actions **/}
                <div className="aa-project-actions aa-grid-row horizontal-right">
                    <div>
                        <Link to={routeHelper('/learning-instances')}>
                            <CommandButton
                                theme="error"
                                onClick={() => {}}
                                recommended
                            >
                                <FontAwesome
                                    name="close"
                                />
                                <span>
                                    &nbsp;Cancel
                                </span>
                            </CommandButton>
                        </Link>
                        <CommandButton
                            theme="success"
                            onClick={() => {}}
                            disabled={Boolean(formLevelDisplayErrors.length)}
                            recommended
                            type="submit"
                        >
                            <FontAwesome
                                name="check"
                            />
                            <span>
                                &nbsp;{submitBtnLabel}
                            </span>
                        </CommandButton>

                        {(() => {
                            if (submitAttempted) {
                                return (
                                    <Popup prefer={[Popup.TOP]}
                                           theme="error"
                                           hidden={!formLevelDisplayErrors.length}
                                    >
                                        {formLevelDisplayErrors[0]}
                                    </Popup>
                                );
                            }
                        })()}
                    </div>
                </div>
            </form>
        );
    }
}

const form = 'projectForm';

const mapPropsToState = selectProjectForm();

const mapDispatchToState = (dispatch) => {
    return {
        changeFieldValue(field, value) {
            dispatch(change(form, field, value));
        },
        loadProjectDomains: () => dispatch(fetchProjectDomains()),
    };
};

export default reduxForm({
    form,
    validate: formLevelValidations,
    initialValues: fromJS({
        project: {
            primaryLanguage: '1',
            fields: {
                standard: [],
                custom: [],
            },
        },
    }),
})(connect(
    mapPropsToState,
    mapDispatchToState,
)(ProjectForm));
