import {Map, List} from 'immutable';
import {createSelector} from 'reselect';
import {formValueSelector} from 'redux-form/immutable';
import {OTHER_DOMAIN} from './constants';

const formSelector = formValueSelector('projectForm');

/**
 * Direct selector to state domain
 */
const getProjectFieldsDomain = () => (state) => state.get('projectsContainer').get('projectFieldsCache');
const getCurrentCustomFieldName = () => (state) => formSelector(state, 'newCustomFieldName');
const getProjectFormState = () => (state) => formSelector(state, 'project');
const getFormPrimaryLanguage = () => (state) => formSelector(state, 'project.primaryLanguage');
const getProjectType = () => (state) => formSelector(state, 'project.projectType');
const getProjectFormErrors = () => (state) => state.getIn(['form', 'projectForm', 'syncErrors']);
const selectDomainLanguagesState = () => (state) => state.getIn(['newProjectContainer', 'domainLanguages']);
const selectProjectSelectedDomain = () => (state) => state.getIn(['form', 'projectForm', 'values', 'project', 'projectType']);
const selectSupportedLanguagesState = () => (state) => state.getIn(['language', 'supportedLanguages']);
const selectDomainLanguage = () => createSelector(
    selectDomainLanguagesState(),
    (domainLanguages) => {
        return domainLanguages ? domainLanguages.toJS() : [];
    }
);
const selectSupportedLanguages = () => createSelector(
    selectSupportedLanguagesState(),
    (supportedLanguages) => {
        return List.isList(supportedLanguages) ? supportedLanguages.toJS() : supportedLanguages;
    }
);

//supportedLanguages
const selectDomainSupportedLanguage = () => createSelector(
    selectProjectSelectedDomain(),
    selectDomainLanguage(),
    selectSupportedLanguages(),
    (selectedDomain, domainLanguages, supportedLanguages) => {
        if (!selectedDomain) return [];
        if (selectedDomain === OTHER_DOMAIN) return supportedLanguages;

        const domainLanguage = domainLanguages.find((domain) => domain.name === selectedDomain);
        if (!domainLanguage) return [];
        const languages = supportedLanguages.filter((language) => domainLanguage.languages.includes(language.name));
        return languages;
    }
);
/**
 * Other specific selectors
 */

/**
 * Default selector
 */
const selectProjectForm = () => createSelector(
    getProjectFieldsDomain(),
    getCurrentCustomFieldName(),
    getProjectFormState(),
    getFormPrimaryLanguage(),
    getProjectType(),
    getProjectFormErrors(),
    selectDomainSupportedLanguage(),
    (projectState, customFieldName, formState, primaryLanguage, projectType, projectFormErrors, supportedLanguages) => {
        const formErrors = Map.isMap(projectFormErrors) ? projectFormErrors.toJS() : projectFormErrors;
        const aliasFields = List.isList(projectState) ? projectState.toJS() : projectState;

        return {
            aliasFields,
            availableProjectFields: aliasFields.reduce((acc, projectField) => {
                if (projectField.name === projectType) {
                    projectField.fields.forEach((field) => {
                        switch (field.fieldType) {
                            case 'FormField':
                                acc.form.push(field);
                                break;
                            case 'TableField':
                                acc.table.push(field);
                                break;
                        }
                    });
                }

                return acc;
            }, {
                form: [],
                table: [],
            }),
            currentCustomFieldName: customFieldName,
            formFields: formState.get('fields').toJS(),
            isProjectTypeCustom: formState.get('projectType') === 'other',
            currentProjectType: formState.get('projectType'),
            formErrors,
            formLevelDisplayErrors: (() => {

                if (formErrors) {
                    // listen for errors needed at the form level
                    const {requiredFieldSelection, fileUploadsRequired, files, project} = formErrors;
                    return [
                        ...(requiredFieldSelection ? [requiredFieldSelection] : []),
                        ...(fileUploadsRequired ? [fileUploadsRequired] : []),
                        ...(files ? [files] : []),
                        ...(project ? Object.keys(project).map((errorKey) => project[errorKey]) : []),
                    ];
                }

                return [];
            })(),
            supportedLanguages,
        };
    }
);

export default selectProjectForm;
export {
    selectProjectForm,
    getProjectFieldsDomain,
    getCurrentCustomFieldName,
    getProjectFormState,
    getFormPrimaryLanguage,
    getProjectType,
};
