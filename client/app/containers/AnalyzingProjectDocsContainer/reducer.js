/*
 *
 * AnalyzingProjectFiles reducer
 *
 */

import {fromJS} from 'immutable';
import {
    PERSIST_FILE_PROCESSING_STATUS,
    RESET_FILE_PROCESSING,
} from './constants';

export const initialState = fromJS({
    totalFileCount: 0,
    totalProcessedCount: 0,
    timeRemaining: 0,
    processingComplete: false,
});

function analyzingProjectDocsContainerReducer(state = initialState, action) {
    switch (action.type) {
        case PERSIST_FILE_PROCESSING_STATUS:
            return state.set('totalFileCount', action.payload.totalFileCount)
                        .set('totalProcessedCount', action.payload.totalProcessedCount)
                        .set('timeRemaining', action.payload.estimatedTimeRemainingInMins)
                        .set('processingComplete', action.payload.complete);
        case RESET_FILE_PROCESSING:
            return initialState;
        default:
            return state;
    }
}

export default analyzingProjectDocsContainerReducer;
