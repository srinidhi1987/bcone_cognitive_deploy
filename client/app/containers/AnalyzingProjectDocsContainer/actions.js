/*
 *
 * AnalyzingProjectFiles actions
 *
 */

import {
    FETCH_FILE_PROCESSING_STATUS,
    PERSIST_FILE_PROCESSING_STATUS,
    FILE_PROCESSING_STATUS_FAILED,
    CHECK_FILE_PROCESSING_STATUS,
    RESET_FILE_PROCESSING,
} from './constants';

export function fetchFileProcessingStatus(projectId, transactionId) {
    return {
        type: FETCH_FILE_PROCESSING_STATUS,
        projectId,
        transactionId,
    };
}

export function persistFileProcessingStatus(payload) {
    return {
        type: PERSIST_FILE_PROCESSING_STATUS,
        payload,
    };
}

export function fileProcessingStatusFailed(error) {
    return {
        type: FILE_PROCESSING_STATUS_FAILED,
        error: (error instanceof Error) ? error.message : error,
    };
}

export function checkProcessingStatus(projectId, transactionId) {
    return {
        type: CHECK_FILE_PROCESSING_STATUS,
        projectId,
        transactionId,
    };
}

export function resetProcessingStatus() {
    return {
        type: RESET_FILE_PROCESSING,
    };
}
