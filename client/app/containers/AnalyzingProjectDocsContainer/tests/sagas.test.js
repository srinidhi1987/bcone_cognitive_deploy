/**
 * AnalyzingProjectDocsContainer sagas
 */

import {take, call, put} from 'redux-saga/effects';
import {browserHistory} from 'react-router';
import {
    processingStatusSaga,
    checkProcessingStatusSaga,
    fetchFileProcessingStatusFlow,
    fetchFileProcessingStatusFromServer,
    checkFileProcessingStatusFlow,
} from '../sagas';
import {CHECK_FILE_PROCESSING_STATUS, FETCH_FILE_PROCESSING_STATUS} from '../constants';
import {persistFileProcessingStatus} from '../actions';
import {viewContainerLoading} from '../../App/actions';
import routeHelper from '../../../utils/routeHelper';

describe('AnalyzingProjectDocsContainer Sagas', () => {
    describe('processingStatusSaga Saga', () => {
        // given
        const saga = processingStatusSaga();
        // when
        const result = saga.next();
        // then
        expect(result.value).toEqual(take(FETCH_FILE_PROCESSING_STATUS, fetchFileProcessingStatusFlow));
    });

    describe('checkProcessingStatusSaga Saga', () => {
        // given
        const saga = checkProcessingStatusSaga();
        // when
        const result = saga.next();
        // then
        expect(result.value).toEqual(take(CHECK_FILE_PROCESSING_STATUS, checkFileProcessingStatusFlow));
    });

    describe('fetchFileProcessingStatusFlow generator', () => {
        // given
        const projectId = 'testing';
        const transactionId = '765465785678456465';
        const action = {
            projectId,
            transactionId,
        };

        describe('success path', () => {
            const generator = fetchFileProcessingStatusFlow(action);

            it('should set loading state to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(viewContainerLoading(true)));
            });

            it('should call API for processing status', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchFileProcessingStatusFromServer, action.projectId, action.transactionId));
            });

            it('should persist response', () => {
                // given
                const response = {
                    data: {
                        totalFileCount: 1000,
                    },
                    success: true,
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(persistFileProcessingStatus(response.data)));
            });

            it('should set loading state to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(viewContainerLoading(false)));
            });
        });

        describe('when no transactionId is present', () => {
            const generator = fetchFileProcessingStatusFlow({
                projectId: action.projectId,
            });

            it('should redirect to project details view', () => {
                // given
                generator.next(); // set loading state
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(browserHistory.push, routeHelper(`/learning-instances/${action.projectId}`)));
            });
        });
    });

    describe('checkFileProcessingStatusFlow generator', () => {
        // given
        const projectId = 'testing';
        const transactionId = '765465785678456465';
        const action = {
            projectId,
            transactionId,
        };

        describe('success path', () => {
            const generator = checkFileProcessingStatusFlow(action);

            it('should call API for processing status', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchFileProcessingStatusFromServer, action.projectId, action.transactionId));
            });

            it('should persist response', () => {
                // given
                const response = {
                    data: {
                        totalFileCount: 1000,
                    },
                    success: true,
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(persistFileProcessingStatus(response.data)));
            });
        });
    });
});
