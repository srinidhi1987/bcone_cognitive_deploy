import {fromJS} from 'immutable';

import selectAnalyzingProjectFiles, {getCurrentProject} from '../selectors';
import {Project1} from '../../../../fixtures/projects';

describe('AnalzyingProjectDocsContainer selectors', () => {
    describe('getCurrentProject selector', () => {
        it('should return current fetched project', () => {
            // given
            const mockedState = fromJS({
                projectsContainer: {
                    currentProject: Project1,
                },
            });
            // when
            const selector = getCurrentProject();
            // then
            expect(selector(mockedState)).toEqual(fromJS(Project1));
        });
    });

    describe('selectAnalyzingProjectFiles selector', () => {
        it('should return analyzing container state with current project', () => {
            // given
            const projectTransactionId = '64556757685678867';
            const mockedState = fromJS({
                appContainer: {
                    viewContainerLoading: true,
                },
                analyzingProjectDocsContainer: {},
                projectsContainer: {
                    currentProject: Project1,
                    projectFieldsCache: [],
                    projectTransactionId,
                },
            });
            // when
            const selector = selectAnalyzingProjectFiles();
            // then
            expect(selector(mockedState)).toEqual({
                currentProject: Project1,
                isLoadingView: true,
                projectCustomFields: Project1.fields.custom,
                projectStandardFields: [],
                projectTransactionId,
            });
        });
    });
});
