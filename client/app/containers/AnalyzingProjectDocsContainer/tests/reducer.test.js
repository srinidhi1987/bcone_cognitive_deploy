import {fromJS} from 'immutable';

import {PERSIST_FILE_PROCESSING_STATUS, RESET_FILE_PROCESSING} from '../constants';
import analyzingProjectDocsContainerReducer, {initialState} from '../reducer';

describe('AnalyzingProjectDocsContainerReducer', () => {
    it('returns the initial state', () => {
        expect(analyzingProjectDocsContainerReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle PERSIST_FILE_PROCESSING_STATUS action type', () => {
        // given
        const payload = {
            id: '0c7e15b9-29b9-48d4-adc0-ac4ab21ff295',
            project_id: 'fefe7f15-30e5-46de-8910-ce5be1c6e302', // eslint-disable-line camelcase
            estimatedTimeRemainingInMins: 10,
            totalFileCount: 3643,
            totalProcessedCount: 242,
            complete: true,
        };
        const action = {
            type: PERSIST_FILE_PROCESSING_STATUS,
            payload,
        };
        const expectedState = fromJS({
            ...initialState.toJS(),
            totalFileCount: payload.totalFileCount,
            totalProcessedCount: payload.totalProcessedCount,
            timeRemaining: payload.estimatedTimeRemainingInMins,
            processingComplete: true,
        });
        // when
        const result = analyzingProjectDocsContainerReducer(initialState, action);
        // then
        expect(result).toEqual(expectedState);
    });

    it('should handle RESET_FILE_PROCESSING action type and reset initial state', () => {
        // given
        const action = {
            type: RESET_FILE_PROCESSING,
        };
        const currentState = fromJS({
            ...initialState.toJS(),
            timeRemaining: 10,
            processingComplete: true,
        });
        // when
        const result = analyzingProjectDocsContainerReducer(currentState, action);
        // then
        expect(result).toEqual(initialState);
    });
});
