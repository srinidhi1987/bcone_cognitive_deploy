import {
    FETCH_FILE_PROCESSING_STATUS,
    FILE_PROCESSING_STATUS_FAILED,
    CHECK_FILE_PROCESSING_STATUS,
    RESET_FILE_PROCESSING,
} from '../constants';
import {
    fetchFileProcessingStatus,
    fileProcessingStatusFailed,
    checkProcessingStatus,
    resetProcessingStatus,
} from '../actions';

describe('AnalyzingProjectFiles actions', () => {
    it('should handle fetchFileProcessingStatus action creator', () => {
        // given
        const projectId = '10000';
        const transactionId = '465756867867456345';
        const expectedAction = {
            type: FETCH_FILE_PROCESSING_STATUS,
            projectId,
            transactionId,
        };
        // when
        const result = fetchFileProcessingStatus(projectId, transactionId);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle fileProcessingStatusFailed action creator', () => {
        // given
        const error = new Error('File processing failed');

        const expectedAction = {
            type: FILE_PROCESSING_STATUS_FAILED,
            error: error.message,
        };
        // when
        const result = fileProcessingStatusFailed(error);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle checkProcessingStatus action creator', () => {
        // given
        const projectId = 10000;
        const transactionId = '67568867678687687';
        const expectedAction = {
            type: CHECK_FILE_PROCESSING_STATUS,
            projectId,
            transactionId,
        };
        // when
        const result = checkProcessingStatus(projectId, transactionId);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle resetProcessingStatus action creator', () => {
        // given
        const expectedAction = {
            type: RESET_FILE_PROCESSING,
        };
        // when
        const result = resetProcessingStatus();
        // then
        expect(result).toEqual(expectedAction);
    });
});
