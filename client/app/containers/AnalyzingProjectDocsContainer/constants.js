/*
 *
 * AnalyzingProjectDocsContainer constants
 *
 */
export const FETCH_FILE_PROCESSING_STATUS = 'app/AnalyzingProjectDocsContainer/FETCH_FILE_PROCESSING_STATUS';
export const PERSIST_FILE_PROCESSING_STATUS = 'app/AnalyzingProjectDocsContainer/PERSIST_FILE_PROCESSING_STATUS';
export const FILE_PROCESSING_STATUS_FAILED = 'app/AnalyzingProjectDocsContainer/FILE_PROCESSING_STATUS_FAILED';
export const CHECK_FILE_PROCESSING_STATUS = 'app/AnalyzingProjectDocsContainer/CHECK_FILE_PROCESSING_STATUS';
export const RESET_FILE_PROCESSING = 'app/AnalyzingProjectDocsContainer/RESET_FILE_PROCESSING';
