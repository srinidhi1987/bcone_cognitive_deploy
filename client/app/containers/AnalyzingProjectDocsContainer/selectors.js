import {createSelector} from 'reselect';
import {Map} from 'immutable';

/**
 * Direct selector to the AnalyzingProjectDocsContainer state domain
 */
const selectAnalyzingProjectFilesDomain = () => (state) => state.get('analyzingProjectDocsContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');
const selectProjectsFieldDomain = () => (state) => state.getIn(['projectsContainer', 'projectFieldsCache']);
const selectViewContainerLoadingState = () => (state) => state.getIn(['appContainer', 'viewContainerLoading']);

/**
 * Other specific selectors
 */
const getCurrentProject = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => projectState.get('currentProject')
);

/**
 * Default selector used by AnalyzingProjectFiles
 */

const selectAnalyzingProjectFiles = () => createSelector(
    selectAnalyzingProjectFilesDomain(),
    selectViewContainerLoadingState(),
    selectProjectsContainerDomain(),
    getCurrentProject(),
    selectProjectsFieldDomain(),
    (substate, isLoading, projectState, currentProjectState, projectFields) => {
        const currentProject = Map.isMap(currentProjectState) ? currentProjectState.toJS() : currentProjectState;

        return {
            ...substate.toJS(),
            currentProject,
            isLoadingView: isLoading,
            projectCustomFields: currentProject ? currentProject.fields.custom : [],
            projectStandardFields: (currentProject && projectFields) ? projectFields.reduce((acc, field) => {
                if (field.name === currentProject.projectType) {
                    field.fields.map((f) => {
                        if (currentProject.fields.standard.indexOf(f.id) !== -1) {
                            acc.push({
                                name: f.name,
                            });
                        }
                    });
                }

                return acc;
            }, []) : [],
            projectTransactionId: projectState.get('projectTransactionId'),
        };
    }
);

export default selectAnalyzingProjectFiles;
export {
    selectAnalyzingProjectFilesDomain,
    getCurrentProject,
    selectProjectsFieldDomain,
};
