/*
 *
 * AnalyzingProjectFiles
 *
 */

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import {browserHistory} from 'react-router';
import {
    Breadcrumbs,
    PageTitle,
    CommandButton,
} from 'common-frontend-components';

import selectAnalyzingProjectFiles from './selectors';
import {fetchProjectById} from '../ProjectsContainer/actions';
import {
    fetchFileProcessingStatus,
    checkProcessingStatus,
    resetProcessingStatus,
} from './actions';
import FlexGrid from '../../components/FlexGrid';
import AnalyzingDocsProjectInfo from '../../components/AnalyzingDocsProjectInfo';
import AnalyzingDocsProgressBar from '../../components/AnalyzingDocsProgressBar';
import DocumentProcessingMarquee from '../../components/DocumentProcessingMarquee';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class AnalyzingProjectFiles extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'AnalyzingProjectFiles';

    constructor(props) {
        super(props);

        this.statusCheck = null;
    }

    componentWillMount() {
        const {params, fetchProject, fetchCurrentProcessingStatus, projectTransactionId} = this.props;

        fetchProject(params.id);
        fetchCurrentProcessingStatus(params.id, projectTransactionId);
    }

    componentDidMount() {
        const delay = 10000;
        const {params, checkFileProcessing, projectTransactionId} = this.props;

        this.statusCheck = setInterval(() => {
            checkFileProcessing(params.id, projectTransactionId);
        }, delay);
    }

    componentWillReceiveProps(newProps) {
        // clear status check if processing is complete
        if (newProps.processingComplete) {
            clearInterval(this.statusCheck);
        }
    }

    componentWillUnmount() {
        const {resetProcessingStatus} = this.props;

        clearInterval(this.statusCheck);
        // flush / reset processing state
        resetProcessingStatus();
    }

    render() {
        const {
            params, currentProject, projectCustomFields, projectStandardFields,
            totalProcessedCount, totalFileCount, timeRemaining, isLoadingView,
            processingComplete,
        } = this.props;

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Analyzing Documents"
                />

                <div className="aa-project-processing-background">
                    <video width="100%" height="100%" autoPlay="autoPlay" loop="loop">
                        <source src={routeHelper('/media/project-processing-bg.mp4" type="video/mp4')} />
                        <source src={routeHelper('/media/project-processing-bg.ogv" type="video/ogg')} />
                    </video>
                </div>

                <Breadcrumbs>
                    <Link to={routeHelper('/learning-instances')}>Learning Instances</Link>
                </Breadcrumbs>

                <div id="aa-main-container">
                    <FlexGrid className="aa-analyzing-project-docs-grid">
                        <FlexGrid.Item>
                            <AnalyzingDocsProjectInfo
                                project={currentProject}
                                projectStandardFields={projectStandardFields}
                                projectCustomFields={projectCustomFields}
                            />
                        </FlexGrid.Item>

                        <FlexGrid.Item>
                            <PageTitle label="Analyzing documents..." />

                            {(() => {
                                if (isLoadingView) {
                                    return (
                                        <div>
                                            Loading...
                                        </div>
                                    );
                                }

                                return (
                                    <div className="aa-analyzing-project-docs-inner-container">
                                        {(() => {
                                            if (processingComplete) {
                                                setTimeout(() => {
                                                    browserHistory.push(routeHelper(`/learning-instances/${params.id}`));
                                                }, 2000);
                                                return (
                                                    <div>
                                                        Processing is complete!
                                                    </div>
                                                );
                                            }

                                            return (
                                                <div>
                                                    <AnalyzingDocsProgressBar
                                                        timeRemaining={timeRemaining}
                                                        processedCount={totalProcessedCount}
                                                        totalCount={totalFileCount}
                                                    />

                                                    <DocumentProcessingMarquee
                                                        messages={[
                                                            'Applying classifier...',
                                                            'Performing semantic analysis...',
                                                            'Learning from aliases...',
                                                            'Recognizing patterns...',
                                                            'Extracting data...',
                                                        ]}
                                                    />
                                                </div>
                                            );
                                        })()}
                                    </div>
                                );
                            })()}
                        </FlexGrid.Item>
                    </FlexGrid>

                    <ul className="aa-analyzing-project-docs-actions">
                        <li>
                            <Link to={routeHelper(`/learning-instances/${params.id}`)}>
                                <CommandButton onClick={() => {}}>
                                    {processingComplete ? 'Finish and close' : 'Close and run in background'}
                                </CommandButton>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectAnalyzingProjectFiles();

function mapDispatchToProps(dispatch) {
    return {
        fetchProject: (id) => dispatch(fetchProjectById(id)),
        fetchCurrentProcessingStatus: (projectId, transactionId) => dispatch(fetchFileProcessingStatus(projectId, transactionId)),
        checkFileProcessing: (projectId, transactionId) => dispatch(checkProcessingStatus(projectId, transactionId)),
        resetProcessingStatus: () => dispatch(resetProcessingStatus()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AnalyzingProjectFiles);
