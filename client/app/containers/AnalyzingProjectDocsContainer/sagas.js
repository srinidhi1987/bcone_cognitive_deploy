import {takeEvery, takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {HEADERS_JSON} from 'common-frontend-utils';
import {browserHistory} from 'react-router';
import {requestGet} from '../../utils/request';

import {FETCH_FILE_PROCESSING_STATUS, CHECK_FILE_PROCESSING_STATUS} from './constants';
import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {persistFileProcessingStatus, fileProcessingStatusFailed} from './actions';
import {viewContainerLoading} from '../App/actions';
import routeHelper from '../../utils/routeHelper';

// Individual exports for testing
export function fetchFileProcessingStatusFromServer(projectId, transactionId = null) {
    return requestGet(
        routeHelper(`api/projects/${projectId}/files/status`),
        {
            headers: HEADERS_JSON,
            query: {
                ...((transactionId ? {requestid: transactionId} : {})),
            },
        })
        .then(handleApiResponse)
        .catch(handleApiError);
}

export function* fetchFileProcessingStatusFlow(action) {
    try {
        yield put(viewContainerLoading(true));
        // check if transaction id is present
        // if not, redirect to project details
        if (!action.transactionId) {
            yield call(browserHistory.push, routeHelper(`/learning-instances/${action.projectId}`));
            return;
        }

        const status = yield call(fetchFileProcessingStatusFromServer, action.projectId, action.transactionId);
        if (status.success) {
            yield put(persistFileProcessingStatus(status.data));
        }
        else {
            yield put(fileProcessingStatusFailed('Failed to receive file processing status'));
        }
    }
    catch (e) {
        yield put(fileProcessingStatusFailed(e.message));
    }
    finally {
        yield put(viewContainerLoading(false));
    }
}

export function* checkFileProcessingStatusFlow(action) {
    const status = yield call(fetchFileProcessingStatusFromServer, action.projectId, action.transactionId);
    if (status.success) {
        yield put(persistFileProcessingStatus(status.data));
    }
}

export function* processingStatusSaga() {
    yield* takeEvery(FETCH_FILE_PROCESSING_STATUS, fetchFileProcessingStatusFlow);
}

export function* checkProcessingStatusSaga() {
    yield* takeLatest(CHECK_FILE_PROCESSING_STATUS, checkFileProcessingStatusFlow);
}

// All sagas to be loaded
export default [
    processingStatusSaga,
    checkProcessingStatusSaga,
];
