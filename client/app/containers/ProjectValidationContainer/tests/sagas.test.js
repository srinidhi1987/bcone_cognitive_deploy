/**
 * ProjectValidationContainer sagas
 */

import {call, put} from 'redux-saga/effects';
import {viewContainerLoading} from '../../App/actions';
import {populateValidationProjects} from '../actions';
import {fetchValidationProjects, fetchProjectValidationsFromServer} from '../sagas';

describe('fetchValidationProjects Saga', () => {
    describe('success path', () => {
        // given
        const generator = fetchValidationProjects();

        it('should set view loading state to true', () => {
            // given
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(put(viewContainerLoading(true)));
        });

        it('should call API for projects', () => {
            // given
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(call(fetchProjectValidationsFromServer));
        });

        it('should populate projects', () => {
            // given
            const response = {
                success: true,
                data: [
                    {id: '64565645756678867'},
                    {id: '23453446576889797'},
                ],
            };
            // when
            const result = generator.next(response);
            // then
            expect(result.value).toEqual(put(populateValidationProjects(response.data)));
        });

        it('should set view loading state to false', () => {
            // given
            // when
            const result = generator.next();
            // then
            expect(result.value).toEqual(put(viewContainerLoading(false)));
        });
    });
});
