import {fromJS} from 'immutable';

import {initialState as appInitialState} from '../../App/reducer';
import {initialState as validationInitialState} from '../reducer';
import selectProjectValidationContainer from '../selectors';

describe('selectProjectValidationContainerDomain', () => {
    describe('selectProjectValidationContainer selector', () => {
        // given
        const mockState = fromJS({
            appContainer: {
                ...appInitialState.toJS(),
            },
            projectValidationContainer: {
                ...validationInitialState.toJS(),
            },
        });
        // when
        const selector = selectProjectValidationContainer()(mockState);

        it('should return project validation state and loading container state', () => {
            // then
            expect(selector).toEqual({
                ...validationInitialState.toJS(),
                appConfig: appInitialState.get('appConfig').toJS(),
                authToken: appInitialState.get('authToken'),
                isLoading: appInitialState.get('viewContainerLoading'),
            });
        });
    });
});
