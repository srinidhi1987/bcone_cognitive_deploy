import {shallow} from 'enzyme';
import React from 'react';

import {ValidatorStatus} from '../../../constants/enums';
import {ProjectValidationContainer} from '../index';

describe('<ProjectValidationContainer />', () => {
    describe('instance method: _enableValidator', () => {
        // given
        const wrapper = shallow(
            <ProjectValidationContainer
                fetchProjects={() => {}}
                projects={[]}
            />
        );

        it('should return true if status is processing', () => {
            // given
            const status = ValidatorStatus.PROCESSING;
            // when
            const result = wrapper.instance()._enableValidator(status);
            // then
            expect(result).toBe(true);
        });

        it('should return true if status is validating', () => {
            // given
            const status = ValidatorStatus.VALIDATING;
            // when
            const result = wrapper.instance()._enableValidator(status);
            // then
            expect(result).toBe(true);
        });

        it('should return false if status is reviewed', () => {
            // given
            const status = ValidatorStatus.REVIEWED;
            // when
            const result = wrapper.instance()._enableValidator(status);
            // then
            expect(result).toBe(false);
        });

        it('should return false if status is not provided or one of the enum', () => {
            // given
            const status = 'test';
            // when
            const result = wrapper.instance()._enableValidator(status);
            // then
            expect(result).toBe(false);
        });
    });
});
