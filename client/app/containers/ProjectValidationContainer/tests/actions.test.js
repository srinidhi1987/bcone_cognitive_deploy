import {
    fetchValidationProjects,
    populateValidationProjects,
} from '../actions';
import {
    FETCH_VALIDATION_PROJECTS,
    POPULATE_VALIDATION_PROJECTS,
} from '../constants';

describe('ProjectValidationContainer actions', () => {
    describe('fetchValidationProjects', () => {
        it('should construct action creator', () => {
            const expectedAction = {
                type: FETCH_VALIDATION_PROJECTS,
            };

            expect(fetchValidationProjects()).toEqual(expectedAction);
        });
    });

    describe('populateValidationProjects action creator', () => {
        it('should handle action', () => {
            // given
            const projects = [
                {id: '54345345645465'},
                {id: '67878667845343'},
            ];
            const expectedAction = {
                type: POPULATE_VALIDATION_PROJECTS,
                projects,
            };
            // when
            const result = populateValidationProjects(projects);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
