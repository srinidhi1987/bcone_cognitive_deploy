import {POPULATE_VALIDATION_PROJECTS} from '../constants';
import projectValidationContainerReducer, {initialState} from '../reducer';
import {fromJS} from 'immutable';

describe('projectValidationContainerReducer', () => {
    it('returns the initial state', () => {
        expect(projectValidationContainerReducer(undefined, {})).toEqual(fromJS(initialState));
    });

    it('should populate project state with POPULATE_VALIDATION_PROJECTS action', () => {
        // given
        const action = {
            type: POPULATE_VALIDATION_PROJECTS,
            projects: [
                {id: '45645675675676567'},
                {id: '56456789980890098'},
            ],
        };
        const expectedState = {
            ...initialState.toJS(),
            projects: action.projects,
        };
        // when
        const result = projectValidationContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });
});
