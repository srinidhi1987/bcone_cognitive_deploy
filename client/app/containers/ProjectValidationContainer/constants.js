/*
 *
 * ProjectValidationContainer constants
 *
 */

export const FETCH_VALIDATION_PROJECTS = 'app/ProjectValidationContainer/FETCH_VALIDATION_PROJECTS';
export const POPULATE_VALIDATION_PROJECTS = 'app/ProjectValidationContainer/POPULATE_VALIDATION_PROJECTS';
