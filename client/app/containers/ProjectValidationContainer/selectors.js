import {createSelector} from 'reselect';

/**
 * Direct selector to the projectValidationContainer state domain
 */
const selectProjectValidationContainerDomain = () => (state) => state.get('projectValidationContainer');
const selectAppContainerDomain = () => (state) => state.get('appContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectValidationContainer
 */

const selectProjectValidationContainer = () => createSelector(
    selectProjectValidationContainerDomain(),
    selectAppContainerDomain(),
    (substate, appState) => {
        return {
            ...substate.toJS(),
            appConfig: appState.get('appConfig').toJS(),
            authToken: appState.get('authToken'),
            isLoading: appState.get('viewContainerLoading'),
        };
    },
);

export default selectProjectValidationContainer;
export {
    selectProjectValidationContainer,
    selectProjectValidationContainerDomain,
    selectAppContainerDomain,
};
