/*
 *
 * ProjectValidationContainer actions
 *
 */

import {
    FETCH_VALIDATION_PROJECTS,
    POPULATE_VALIDATION_PROJECTS,
} from './constants';

export function fetchValidationProjects() {
    return {
        type: FETCH_VALIDATION_PROJECTS,
    };
}

export function populateValidationProjects(projects = []) {
    return {
        type: POPULATE_VALIDATION_PROJECTS,
        projects,
    };
}
