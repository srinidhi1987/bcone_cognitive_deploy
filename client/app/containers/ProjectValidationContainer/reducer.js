/*
 *
 * ProjectValidationContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {POPULATE_VALIDATION_PROJECTS} from './constants';

const initialState = fromJS({
    projects: [],
});

function projectValidationContainerReducer(state = initialState, action) {
    switch (action.type) {
        case POPULATE_VALIDATION_PROJECTS:
            return state.set('projects', action.projects);
        default:
            return state;
    }
}

export default projectValidationContainerReducer;
export {initialState};
