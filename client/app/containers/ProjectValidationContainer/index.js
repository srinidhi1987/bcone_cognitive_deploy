/*
 *
 * ProjectValidationContainer
 *
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {
    PageTitle,
    DataTable,
    DataFilter,
} from 'common-frontend-components';
import {DataField, DataType} from 'common-frontend-utils';

import {ValidatorStatus} from '../../constants/enums';
import selectProjectValidationContainer from './selectors';
import {fetchValidationProjects} from './actions';
import {autobind} from 'core-decorators';
import './styles.scss';
import {DataTableCopyText} from '../../constants/enums';
import ValidateProjectButton from '../../components/ValidateProjectButton';
export class ProjectValidationContainer extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'ProjectValidationContainer';

    state = {
        filter: [],
        isFiltering: false,
        projectData: [],
    };

    componentWillMount() {
        this.props.fetchProjects();
    }

    componentWillReceiveProps(newProps) {
        this.setState({projectData: newProps.projects});
    }

    /**
     * Filter projects based on filter
     * @param filter
     * @returns {array} - subset of projects
     */
    @autobind filterProjectData(projects, filter) {
        this.setState({
            filter,
            isFiltering: Boolean(filter),
            projectData: projects.filter(DataField.filter(this.getProjectFields(), filter)),
        });
    }

    _enableValidator(status) {
        switch (status) {
            case ValidatorStatus.PROCESSING:
                return true;
            case ValidatorStatus.VALIDATING:
                return true;
            case ValidatorStatus.REVIEWED:
                return false;
            default:
                return false;
        }
    }

    @autobind getProjectFields() {
        const {appConfig, authToken} = this.props;
        return [
            new DataField({id: 'name', label: 'Instance name', type: DataType.STRING}),
            new DataField({id: 'status', label: 'Status', type: DataType.STRING}),
            new DataField({id: 'filesProcessed', label: 'Files processed', type: DataType.NUMBER}),
            new DataField({id: 'markedForReview', label: 'Sent for validation', type: DataType.NUMBER}),
            new DataField({id: 'reviewed', label: 'Validated', type: DataType.NUMBER}),
            new DataField({id: 'invalid', label: 'Invalid Files', type: DataType.NUMBER}),
            new DataField({id: 'action', label: 'Action', type: DataType.STRING, render: (project) => (
                <ValidateProjectButton
                    appConfig={appConfig}
                    authToken={authToken}
                    id={project.projectId}
                    withLabel={false}
                    enabled={this._enableValidator(project.status) || project.failedFileCount}
                />
            )}),
        ];
    }

    render() {
        const {filter, isFiltering, projectData} = this.state;
        const {isLoading, projects} = this.props;

        /**
         * Table fields
         */
        const projectFields = this.getProjectFields();

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Learning Instance Validation"
                />

                <div id="aa-main-container">
                    <PageTitle label="Validation" />

                    {(() => {
                        if (isLoading || isFiltering || projects.length) {
                            return (
                                <div>
                                    <div className="aa-project-validations-filters">
                                        <DataFilter
                                            fields={[
                                                new DataField({id: 'name', label: 'Instance Name', type: DataType.STRING}),
                                                new DataField({id: 'status', label: 'Status', type: DataType.STRING}),
                                            ]}
                                            value={filter}
                                            data={projects}
                                            onChange={(filter) => this.filterProjectData(projects, filter)}
                                            searchAll
                                            labelColumnsAll={DataTableCopyText.ALL_COLUMNS_DROPDOWN_VALUE}
                                            labelSearchAll={DataTableCopyText.SEARCH_ALL_PLACEHOLDER}
                                        />
                                    </div>

                                    {isLoading || projectData.length ? (
                                        <DataTable
                                            data={projectData.map((proj) => ({
                                                projectId: proj.projectId,
                                                name: proj.name,
                                                status: proj.status,
                                                filesProcessed: proj.processedCount,
                                                markedForReview: proj.failedFileCount,
                                                reviewed: proj.reviewFileCount,
                                                invalid: proj.invalidFileCount,
                                                action: proj,
                                            }))}
                                            dataId="projectId"
                                            fields={projectFields}
                                            loading={isLoading}
                                        />
                                    ) : (
                                        <div className="aa-project-no-results">
                                            This instance is not available for validation.
                                        </div>
                                    )}
                                </div>
                            );
                        }

                        return (
                            <div className="aa-project-no-results">
                                No learning instances available for validation.
                            </div>
                        );
                    })()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectProjectValidationContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchProjects: () => dispatch(fetchValidationProjects()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProjectValidationContainer);
