import {call, put} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet} from '../../utils/request';

import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {FETCH_VALIDATION_PROJECTS} from './constants';
import {populateValidationProjects} from './actions';
import {viewContainerLoading} from '../App/actions';
import routeHelper from '../../utils/routeHelper';

export function fetchProjectValidationsFromServer() {
    return requestGet(
        routeHelper('api/projects/validations'),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

export function* fetchValidationProjects() { // eslint-disable-line require-yield
    try {
        yield put(viewContainerLoading(true));
        const projects = yield call(fetchProjectValidationsFromServer);
        if (projects.success) {
            yield put(populateValidationProjects(projects.data));
        }
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
    finally {
        yield put(viewContainerLoading(false));
    }
}

// All sagas to be loaded
export default [
    function* () {
        yield* takeLatest(FETCH_VALIDATION_PROJECTS, fetchValidationProjects);
    },
];
