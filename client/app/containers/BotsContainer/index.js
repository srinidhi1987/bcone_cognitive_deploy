/*
 *
 * BotsContainer
 *
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import PageDescription from '../../components/PageDescription';

import {
    Breadcrumbs,
    PageTitle,
    SplitLayout,
    DataTable,
    Icon,
    DataFilter,
    Tooltip,
    ToggleInput,
} from 'common-frontend-components';
import {DataField, DataType} from 'common-frontend-utils';

import {ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import {DataTableCopyText} from '../../constants/enums';
import {displayAlert} from '../AlertsContainer/actions';
import {
    fetchBots,
    previewBot,
    runBotOnce,
    setProjectVisionBotState,
    setSortValues,
} from './actions';
import selectBotsContainer from './selectors';
import FlexGrid from '../../components/FlexGrid';
import BotsPreviewPane from '../../components/BotsPreviewPane';
import EditBotLink from '../../components/EditBotLink';
import ActionWithLoadingState from '../../components/ActionWithLoadingState';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class BotsContainer extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'BotsContainer';

    state = {
        filter: [],
        isFiltering: false,
    };

    componentWillMount() {
        const {fetchBots} = this.props;

        fetchBots();
    }

    isBotInProduction(bot) {
        return bot.environment === 'production';
    }

    /**
        * configures sort params
        * @param {Object[]} bot
        * @param {Object[]} isInProduction
    */
    _handleSortBots(fields, sort) {
        this.props.setSortValues(fields, sort);
    }
    /**
        * toggles bot stage
        * @param {boolean} isInProduction
        * @param {object} bot
    */
    _handleStagingClick(isInProduction, {projectId, categoryId, id, environment}) {
        const {displayAlert, runBotOnce} = this.props;

        if (isInProduction) {
            return displayAlert('You can only test a bot in staging.', ALERT_LEVEL_DANGER);
        }

        runBotOnce(projectId, categoryId, id, environment);
    }

    /**
        * render an EditBotLink component
        * @param {object} bot
        * @param {boolean} isInProduction
        * @returns {component} EditBotLink
    */
    _renderEditBot({projectId, categoryId, categoryName}, isNotInProduction) {
        const {appConfig} = this.props;

        return (
            <EditBotLink
                appConfig={appConfig}
                categoryId={categoryId}
                categoryName={categoryName}
                isNotInProduction={isNotInProduction}
                projectId={projectId} />
        );
    }

/**
    * render a run bot once component
    * @param {object} bot
    * @param {boolean} isInProduction
    * @returns {component} ActionWithLoadingState
*/
    _renderRunBotOnce(bot, isInProduction) {
        const {id} = bot;
        const {runBotsLoading} = this.props;
        const working = Boolean(runBotsLoading[id]);

        const fa = working ? 'gear' : 'play';
        const animate = working ? 'spin' : null;

        return (
            <li className="run-bot-container">
                <ActionWithLoadingState
                    animate={animate}
                    disabled={isInProduction}
                    fa={fa}
                    label="Test Bot"
                    working={working}
                    onClick={() => this._handleStagingClick(isInProduction, bot)} />
            </li>
        );
    }

    /**
        * render a BotStageToggle component
        * @param {object} bot
        * @param {boolean} isInProduction
        * @returns {component} ActionWithLoadingState
    */
    _renderBotStageToggle({projectId, categoryId, id}, isInProduction) {
        const {botsDelayingStage, setVisionBotState} = this.props;
        const botState = isInProduction ? 'staging' : 'production';
        const working = Boolean(botsDelayingStage[id]);
        const fa = working ? 'gear' : null;
        const aa = working ? null : isInProduction ? 'action-toggle--enabled' : 'action-toggle--disabled';
        const animate = working ? 'spin' : null;
        const label = isInProduction ? 'Send to Staging' : 'Send to Production';

        return (
            <li>
                <ActionWithLoadingState
                    aa={aa}
                    animate={animate}
                    fa={fa}
                    label={label}
                    working={working}
                    onClick={() => setVisionBotState(projectId, categoryId, id, botState)} />
            </li>
        );
    }

    /**
        * extracts correct bot data for data table
        * @param {object} bot
        * @returns {object} bot table data
    */
    extractBotData(bot) {
        const {categoryName, id, projectName, status} = bot;
        const docsPassed = bot;
        const docsFailed = bot;
        const environment = bot;

        return {
            categoryName,
            id,
            projectName,
            status,
            docsPassed,
            docsFailed,
            environment,
        };
    }

    /**
        * get correct fields for bot data table
        * @param {boolean} isBotInProduction
        * @param {function} selectPreviewBot
        * @returns {array} bot data table Fields Object[Fields]
    */
    getBotFields(isBotInProduction, selectPreviewBot) {
        return [
            new DataField({id: 'categoryName', label: 'Bot', type: DataType.STRING, sortable: true, render: (name, {id}) => {
                return (
                    <a
                        className="aa-project-bot-preview-link"
                        onClick={() => selectPreviewBot(id)}
                    >
                        <div className="aa-project-bot-preview-boticon">
                            <Icon fa="eye" />
                        </div>

                        {name}
                    </a>
                );
            }}),
            new DataField({id: 'projectName', label: 'Instance', sortable: true, type: DataType.STRING}),
            new DataField({id: 'status', label: 'Status', sortable: true, type: DataType.STRING}),
            new DataField({id: 'docsPassed', label: 'Success', sortable: false, type: DataType.STRING, render: ({botRunDetails = {}}) => {
                const {passedDocumentCount, totalFiles} = botRunDetails;

                return (
                    <div>
                        {passedDocumentCount} / {totalFiles}
                    </div>
                );
            }}),
            new DataField({id: 'docsFailed', label: 'Validation', sortable: false, type: DataType.STRING, render: ({botRunDetails = {}}) => {
                const {failedDocumentCount, totalFiles} = botRunDetails;

                return (
                  <div>
                      {failedDocumentCount} / {totalFiles}
                  </div>
                );
            }}),
            // The field below specifically has the ID 'environment' in order to work with FieldFilter component.
            // This is a work around because this table doesn't have a visible 'environment' column already
            new DataField({id: 'environment', label: 'Actions', sortable: false, type: DataType.STRING, render: (bot) => {
                const isInProduction = isBotInProduction(bot);

                return (
                    <ul className="aa-project-bots-actions">
                        {this._renderBotStageToggle(bot, isInProduction)}
                        {this._renderRunBotOnce(bot, isInProduction)}
                        {this._renderEditBot(bot, !isInProduction)}
                    </ul>
                );
            }}),
        ];
    }

    render() {
        const {
            bots,
            previewBot,
            isLoading,
            sort,
            selectPreviewBot,
        } = this.props;
        const {filter, isFiltering} = this.state;
        const {
            SEARCH_ALL_PLACEHOLDER,
            ALL_COLUMNS_DROPDOWN_VALUE,
        } = DataTableCopyText;

        const botFields = this.getBotFields(this.isBotInProduction, selectPreviewBot);
        const filteredData = bots.filter(DataField.filter(botFields, filter));

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Bots"
                />

                <Breadcrumbs>
                    <Link to={routeHelper('/bots')}>Bots</Link>
                </Breadcrumbs>

                <div id="aa-main-container">
                    <div className="aa-grid-row">
                        <PageTitle label="IQ Bots" />
                    </div>
                    <div className="aa-grid-row">
                        <PageDescription>
                            <span>Manage your IQ Bots.</span>
                            <Tooltip>
                                <div className="bots-popup aa-grid-column">
                                    <span>
                                        Each <strong> Bot </strong> has several actions you can take on it:
                                    </span>
                                    <div className="aa-grid-column">
                                        <div className="aa-grid-row padded">
                                            <div className="aa-grid-row horizontal-center">
                                                <Icon fa="pencil" />
                                            </div>
                                            <span><strong>Edit</strong> your bot via the Designer app.</span>
                                        </div>
                                        <div className="aa-grid-row padded">
                                            <div>
                                                <Icon fa="play" />
                                            </div>
                                            <span><strong>Play</strong> your bot once.</span>
                                        </div>
                                        <div className="aa-grid-row padded">
                                            <div>
                                                <ToggleInput
                                                    checked={true}
                                                    onChange={() => {}}
                                                >
                                                    {null}
                                                </ToggleInput>
                                            </div>
                                            <span><strong>Deploy </strong> your bot to production or send it back to staging.</span>
                                        </div>
                                    </div>
                                </div>
                            </Tooltip>
                        </PageDescription>
                    </div>


                    {isLoading || isFiltering || (Array.isArray(bots) && bots.length) ? (
                        <SplitLayout defaultSize={70}>
                            {/** Left column **/}
                            <SplitLayout.Left>
                                <DataFilter
                                    fields={[
                                        new DataField({id: 'environment', label: 'Environment', type: DataType.STRING}),
                                        new DataField({id: 'projectName', label: 'Instance Name', type: DataType.STRING}),
                                        new DataField({id: 'status', label: 'Status', type: DataType.STRING}),
                                    ]}
                                    searchAll
                                    labelColumnsAll={ALL_COLUMNS_DROPDOWN_VALUE}
                                    labelSearchAll={SEARCH_ALL_PLACEHOLDER}
                                    value={filter}
                                    onChange={(filter) => this.setState({filter})}
                                />

                                {/** Existing Bots **/}
                                <div className="aa-project-bots-header">
                                    <FlexGrid>
                                        <FlexGrid.Item itemStyle={{width: '880px', paddingLeft: '0'}}>
                                            Bots ({filteredData.length} of {bots.length})
                                        </FlexGrid.Item>

                                        <FlexGrid.Item>
                                            <ul className="aa-project-bots-header-actions">
                                                <li></li>
                                            </ul>
                                        </FlexGrid.Item>
                                    </FlexGrid>
                                </div>

                                {isLoading || isFiltering || (Array.isArray(filteredData) && filteredData.length) ? (
                                    <DataTable
                                        data={filteredData.map(this.extractBotData)}
                                        dataId="id"
                                        fields={botFields}
                                        loading={isLoading}
                                        sort={sort}
                                        onSort={(sort) => this._handleSortBots(botFields, sort)}
                                        active={previewBot ? previewBot.id : null }
                                        onActive={(id, data) => {
                                            const selectedBot = data.find((element) => element.id === id);
                                            if (selectedBot !== undefined)
                                                selectPreviewBot(id);
                                        }}
                                    />
                                ) : (
                                    <div className="aa-project-no-results">
                                        Bot(s) not found
                                    </div>
                                )}
                                {/** Existing Bots **/}
                            </SplitLayout.Left>

                            {/** Right column **/}
                            <SplitLayout.Right>
                                <BotsPreviewPane
                                    bot={previewBot || filteredData[0]}
                                />
                            </SplitLayout.Right>
                        </SplitLayout>
                    ) : (
                        <div className="aa-bots-placeholder">
                            No current bots.
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectBotsContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchBots: (projectId) => dispatch(fetchBots(projectId)),
        setVisionBotState: (projectId, categoryId, botId, state = 'production') => dispatch(setProjectVisionBotState(projectId, categoryId, botId, state)),
        displayAlert: (message, level) => dispatch(displayAlert(message, level)),
        runBotOnce: (projectId, categoryId, botId, environment) => dispatch(runBotOnce(projectId, categoryId, botId, environment)),
        setSortValues: (field, sort) => dispatch(setSortValues(field, sort)),
        dispatch,
    };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
    const {bots} = stateProps;
    const {dispatch} = dispatchProps;

    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps,
        selectPreviewBot: (id) => {
            const bot = bots.find((bot) => bot.id === id);

            if (bot) {
                dispatch(previewBot(bot));
            }
        },
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
)(BotsContainer);
