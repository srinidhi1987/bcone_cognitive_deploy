import {takeLatest, takeEvery, delay} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet, requestPost} from '../../utils/request';

import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {
    FETCH_BOTS,
    SET_PROJECT_VISIONBOT_STATE,
    RUN_BOT_ONCE,
} from './constants';
import {
    fetchBotsSuccess,
    fetchBotsFailed,
    partialBotUpdateById,
    toggleBotLoading,
} from './actions';
import {displayAlert} from '../AlertsContainer/actions';
import {ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import {viewContainerLoading} from '../App/actions';
import routeHelper from '../../utils/routeHelper';

export function fetchBotsFromServer() {
    return requestGet(
        routeHelper('api/bots'),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

// Individual exports for testing
export function* fetchBots() {
    try {
        yield put(viewContainerLoading(true));
        const bots = yield call(fetchBotsFromServer);
        if (bots.success) {
            yield put(fetchBotsSuccess(bots.data));
        }
        else {
            yield put(fetchBotsFailed());
        }
    }
    catch (e) {
        yield put(fetchBotsFailed());
    }
    finally {
        yield put(viewContainerLoading(false));
    }
}

export function setProjectVisionBotStateFromServer(projectId, categoryId, id, state) {
    return requestPost(
        routeHelper(`api/projects/${projectId}/categories/${categoryId}/bots/${id}/state`),
        {
            headers: HEADERS_JSON,
            body: {
                state,
            },
        })
        .then(handleApiResponse)
        .catch(handleApiError);
}

// we have a delay here because otherwise the toggle state change is immediate,
// the delay here provides us with a delay
export function* setProjectBotVisionState(action) {
    const {categoryId, id, projectId, state} = action;

    try {
        yield put(toggleBotLoading(id, 'botsDelayingStage'));
        const botUpdate = yield call(setProjectVisionBotStateFromServer, projectId, categoryId, id, state);
        yield put(partialBotUpdateById(id, {environment: state})); // Optimistic UI
        yield call(delay, 700);
        yield put(toggleBotLoading(id, 'botsDelayingStage'));
        if (botUpdate && !botUpdate.success) {
            const environment = state === 'production' ? 'staging' : 'production';
            yield put(partialBotUpdateById(id, {environment})); // Optimistic UI
            yield put(displayAlert(botUpdate.errors[0], ALERT_LEVEL_DANGER));
        }
    }
    catch (e) {
        // revert if there are errors
        const environment = state === 'production' ? 'staging' : 'production';
        yield put(partialBotUpdateById(id, {environment})); // Optimistic UI
        yield put(displayAlert('Error updating bot environment', ALERT_LEVEL_DANGER));
    }
}

// Run bot
export function runBotOnceFromServer(projectId, categoryId, id, environment = 'staging') {
    return requestGet(
        routeHelper(`api/projects/${projectId}/categories/${categoryId}/bots/${id}/run`),
        {
            headers: HEADERS_JSON,
            query: {
                environment,
            },
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

export function* runBotOnceFlow(action) {
    try {
        yield put(toggleBotLoading(action.id, 'runBotsLoading'));
        const runBot = yield call(runBotOnceFromServer, action.projectId, action.categoryId, action.id, action.environment);
        if (!runBot.success) {
            yield put(displayAlert(runBot.errors.message || 'Error running bot. Please try again', ALERT_LEVEL_DANGER));
        }
    }
    catch (e) {
        yield console.error(e); // eslint-disable-line no-console
    }
    finally {
        yield put(toggleBotLoading(action.id, 'runBotsLoading'));
    }
}

// All sagas to be loaded
export default [
    function* () {
        yield* takeLatest(FETCH_BOTS, fetchBots);
    },
    function* () {
        yield takeEvery(SET_PROJECT_VISIONBOT_STATE, setProjectBotVisionState);
    },
    function* () {
        yield takeLatest(RUN_BOT_ONCE, runBotOnceFlow);
    },
];
