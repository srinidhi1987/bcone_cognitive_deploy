import {
    BOTS_SORTING_KEY,
    FETCH_BOTS_LOADING,
    FETCH_BOTS,
    FETCH_BOTS_SUCCESS,
    FETCH_BOTS_FAILED,
    PREVIEW_BOT,
    SET_PROJECT_VISIONBOT_STATE,
    PARTIAL_UPDATE_BOT_BY_ID,
    RUN_BOT_ONCE,
    SET_SORT_VALUES,
    TOGGLE_LOADING_STATE,
} from '../constants';
import {
    fetchBotsLoading,
    fetchBots,
    fetchBotsSuccess,
    fetchBotsFailed,
    previewBot,
    setProjectVisionBotState,
    partialBotUpdateById,
    runBotOnce,
    setSortValues,
    toggleBotLoading,
} from '../actions';
import {DataField, DataType} from 'common-frontend-utils';


describe('BotsContainer actions', () => {
    it('should handle fetchProjectBotsLoading action creator', () => {
        // given
        const expectedAction = {
            type: FETCH_BOTS_LOADING,
            loading: true,
        };
        // when
        const result = fetchBotsLoading(true);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle fetchBots action creator', () => {
        // given
        const projectId = 1;
        const expectedAction = {
            type: FETCH_BOTS,
            projectId,
        };
        // when
        const result = fetchBots(projectId);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle fetchBotsSuccess action creator and sort by sorting key asc', () => {
        // given
        const bots = [{[BOTS_SORTING_KEY]: 'shouldBeSecond'}, {[BOTS_SORTING_KEY]: 'shouldBeFirst'}];
        const expectedType = FETCH_BOTS_SUCCESS;
        // when
        const result = fetchBotsSuccess(bots);
        // then
        expect(result.type).toEqual(expectedType);
        expect(result.payload[0]).toEqual(bots[1]);
        expect(result.payload[1]).toEqual(bots[0]);

    });

    it('should handle fetchBotsFailed action creator', () => {
        // given
        const expectedAction = {
            type: FETCH_BOTS_FAILED,
        };
        // when
        const result = fetchBotsFailed();
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle previewProjectBot action creator', () => {
        // given
        const project = {
            name: 'test',
        };
        const expectedAction = {
            type: PREVIEW_BOT,
            project,
        };
        // when
        const result = previewBot(project);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle setProjectVisionBotState action creator', () => {
        // given
        const projectId = '4566455676785867';
        const categoryId = '345453867867';
        const id = '456654456564';
        const state = 'production';
        const expectedAction = {
            type: SET_PROJECT_VISIONBOT_STATE,
            projectId,
            categoryId,
            id,
            state,
        };
        // when
        const result = setProjectVisionBotState(projectId, categoryId, id, state);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle partialBotUpdateById action and update bot in store from id and passed in properties', () => {
        // given
        const id = '64565646565';
        const properties = {
            environment: 'production',
        };
        const expectedAction = {
            type: PARTIAL_UPDATE_BOT_BY_ID,
            id,
            properties,
        };
        // when
        const result = partialBotUpdateById(id, properties);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle runBotOnce action creator', () => {
        // given
        const projectId = '645564567657576';
        const categoryId = '657786879987789';
        const id = '6456776886876678';
        const environment = 'staging';
        const expectedAction = {
            type: RUN_BOT_ONCE,
            projectId,
            categoryId,
            id,
            environment,
        };
        // when
        const result = runBotOnce(projectId, categoryId, id, environment);
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle toggleRunBotOnceLoading action creator', () => {
        // given
        const id = '45665756767756';
        const expectedAction = {
            type: TOGGLE_LOADING_STATE,
            id,
            loadingType: 'runBotsLoading',
        };
        // when
        const result = toggleBotLoading(id, 'runBotsLoading');
        // then
        expect(result).toEqual(expectedAction);
    });

    it('should handle setSortValues action creater', () => {
        const fields = [
            new DataField({id: 'projectName', label: 'Instance', sortable: true, type: DataType.STRING}),
            new DataField({id: 'status', label: 'Status', sortable: true, type: DataType.STRING}),
        ];

        const sort = [{id: 'name', orderBy: 'desc'}];

        const expectedAction = {
            type: SET_SORT_VALUES,
            fields,
            sort,
        };

        const result = setSortValues(fields, sort);

        expect(result).toEqual(expectedAction);
    });
});
