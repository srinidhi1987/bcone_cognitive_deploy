import {fromJS} from 'immutable';
import selectBotsContainer, {getProjectsState, getViewLoadingState} from '../selectors';
import {initialState as appInitialState} from '../../../containers/App/reducer';
import {initialState as botsInitialState} from '../reducer';
import {initialState as projectsInitialState} from '../../../containers/ProjectsContainer/reducer';
import {BOTS_SORTING_KEY} from '../constants';
import {SortDirections} from '../../../constants/enums';
import {compareKey} from 'common-frontend-utils';

describe('botsContainer selectors', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('getProjectsState selector', () => {
        it('should select projects from projectsContainer state', () => {
            // given
            const projects = [
                {name: 'test'},
            ];
            const mockedState = fromJS({
                botsContainer: {
                    ...botsInitialState.toJS(),
                },
                projectsContainer: {
                    ...projectsInitialState.toJS(),
                    projects,
                },
            });
            // when
            const selector = getProjectsState();
            // then
            expect(selector(mockedState)).toEqual(fromJS({
                ...projectsInitialState.toJS(),
                projects,
            }));
        });
    });

    describe('getViewLoadingState selector', () => {
        it('should return view container loading state', () => {
            // given
            const mockedState = fromJS({
                appContainer: {
                    ...appInitialState.toJS(),
                },
            });
            // when
            const selector = getViewLoadingState();
            // then
            expect(selector(mockedState)).toBe(false);
        });
    });

    describe('selectBotsContainer selector', () => {
      // given
        const mockBot1 = {projectName: 'a', status: 'staging'};
        const mockBot2 = {projectName: 'b', status: 'production'};
        const sortFn = compareKey(BOTS_SORTING_KEY);

        const mockedState = fromJS({
            appContainer: {
                ...appInitialState.toJS(),
                authToken: 'test-token',
                user: {
                    ...appInitialState.toJS().user,
                },
            },
            botsContainer: {
                ...botsInitialState.toJS(),
                bots: fromJS([mockBot2, mockBot1]),
                sort: [{id: BOTS_SORTING_KEY, orderBy: SortDirections.ASCENDING}],
                sortFn,
            },
            projectsContainer: {
                ...projectsInitialState.toJS(),
            },
        });
        const expectedState = {
            ...botsInitialState.toJS(),
            appConfig: appInitialState.get('appConfig').toJS(),
            authToken: 'test-token',
            isLoading: false,
            projects: fromJS([]),
            bots: [mockBot1, mockBot2],
            sortFn,
        };
        // when
        const selector = selectBotsContainer();

        it('should return bots state with view loading and loaded projects', () => {
            // then
            expect(selector(mockedState)).toEqual(expectedState);
        });

        it('should sort bots property', () => {
            expect(selector(mockedState).bots).toEqual([mockBot1, mockBot2]);
        });
    });
});
