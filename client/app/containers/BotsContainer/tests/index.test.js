import {mount, shallow} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import Helmet from 'react-helmet';
import {createStore} from 'redux';
import {combineReducers} from 'redux-immutable';
import BotsContainer, {BotsContainer as BotsContainerClass} from '../index';
import appReducer from '../../../containers/App/reducer';
import botsContainerReducer from '../reducer';

describe('<BotsContainer />', () => {
    const store = createStore(combineReducers({
        appContainer: appReducer,
        botsContainer: botsContainerReducer,
    }));

    const wrapper = mount(
        <Provider store={store}>
            <BotsContainer />
        </Provider>
    );
    const component = wrapper.find(BotsContainer);

    it('should render page title', () => {
        // given
        // when
        const result = component.find(Helmet).props();
        // then
        expect(result.title).toEqual('Automation Anywhere | Bots');
    });
});

describe('component method: getBotFields', () => {
    it('should have a method that gets Bot fields', () => {
        const wrapper = shallow(<BotsContainerClass bots={[]} fetchBots={() => {}}/>);

        const fields = wrapper.instance().getBotFields();

        expect(fields.length).toBeGreaterThan(0);

        const [firstField] = fields;

        expect(firstField).toHaveProperty('id');
        expect(firstField).toHaveProperty('sortable');
        expect(firstField).toHaveProperty('label');
    });
});
