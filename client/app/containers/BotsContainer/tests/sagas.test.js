/**
 * BotsContainer sagas
 */
import {delay} from 'redux-saga';
import {call, take, put} from 'redux-saga/effects';
import * as sagas from '../sagas';
import {FETCH_BOTS, SET_PROJECT_VISIONBOT_STATE} from '../constants';
import {
    fetchBotsSuccess,
    fetchBotsFailed,
    partialBotUpdateById,
    toggleBotLoading,
} from '../actions';
import {ALERT_LEVEL_DANGER} from '../../AlertsContainer/constants';
import {displayAlert} from '../../AlertsContainer/actions';
import {viewContainerLoading} from '../../App/actions';

describe('botsContainer sagas', () => {
    describe('main Saga', () => {
        it('should listen for fetch action', () => {
            // given
            const saga = sagas.default[0]();
            // when
            const result = saga.next();
            // then
            expect(result.value).toEqual(take(FETCH_BOTS, sagas.fetchBots));
        });
    });

    describe('fetchBots generator', () => {
        describe('success path', () => {
            const generator = sagas.fetchBots();

            it('should fetch loading bots as true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(viewContainerLoading(true)));
            });

            it('should fetch bots from the server', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(sagas.fetchBotsFromServer));
            });

            it('should set success action and pass results', () => {
                // given
                const response = {
                    success: true,
                    data: [{name: 'test'}],
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(fetchBotsSuccess(response.data)));
            });

            it('should fetch loading bots as false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(viewContainerLoading(false)));
            });
        });

        describe('failure path', () => {
            const generator = sagas.fetchBots();

            it('should NOT be successful on API call and should set failed', () => {
                // given
                const response = {
                    success: false,
                    data: [],
                };
                // when
                generator.next(); // set loading as true
                generator.next(); // calls API
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(fetchBotsFailed()));
            });

            it('should fetch loading bots as false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(viewContainerLoading(false)));
            });
        });
    });

    describe('setProjectBotVisionState generator', () => {
        describe('success path', () => {
            // given
            const action = {
                type: SET_PROJECT_VISIONBOT_STATE,
                id: '4566454565687',
                projectId: '4656546578675',
                categoryId: '64534534556745',
                state: 'production',
            };
            const generator = sagas.setProjectBotVisionState(action);

            it('should call API to set bot state', () => {
                // given
                // when
                generator.next();
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(sagas.setProjectVisionBotStateFromServer, action.projectId, action.categoryId, action.id, action.state));
            });

            it('should do partial update when successful API call', () => {
                // given
                const response = {
                    data: {},
                    success: true,
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(partialBotUpdateById(action.id, {
                    environment: action.state,
                })));
            });

        });

        describe('failure if API is not successful', () => {
            // given
            const action = {
                type: SET_PROJECT_VISIONBOT_STATE,
                id: '4566454565687',
                projectId: '4656546578675',
                categoryId: '64534534556745',
                state: 'production',
            };

            const generator = sagas.setProjectBotVisionState(action);

            it('should add bot to staging delay array', () => {
                const result = generator.next();

                expect(result.value).toEqual(put(toggleBotLoading(action.id, 'botsDelayingStage')));
            });

            it('should call the project vision state on the server', () => {
                const result = generator.next();

                expect(result.value).toEqual(call(sagas.setProjectVisionBotStateFromServer, action.projectId, action.categoryId, action.id, action.state));
            });

            it('should partially update bot by id optimistically', () => {
                const result = generator.next({errors: ['hey this is an error']});

                expect(result.value).toEqual(put(partialBotUpdateById(action.id, {environment: action.state})));
            });

            it('should delay by 700 ms', () => {
                const result = generator.next();

                expect(result.value).toEqual(call(delay, 700));
            });

            it('should remove bot from delay bot stage array', () => {
                const result = generator.next();

                expect(result.value).toEqual(put(toggleBotLoading(action.id, 'botsDelayingStage')));
            });

            it('should set bot state back to opposite state', () => {
                const result = generator.next();
                // when
                // then
                const environment = 'staging';
                expect(result.value).toEqual(put(partialBotUpdateById(action.id, {environment})));
            });

            it('should display first message in error array if API is not successful', () => {
                // given
                const result = generator.next(); // set partial update in store
                // then
                expect(result.value).toEqual(put(displayAlert('hey this is an error', ALERT_LEVEL_DANGER)));
            });
        });
    });

    describe('setProjectVisionBotStateFromServer function', () => {
        it('should set POST call to api', () => {});
    });

    describe('runBotOnceFlow generator', () => {
        describe('success path', () => {
            // given
            const action = {
                projectId: '456656767887',
                categoryId: '67567889789089',
                id: '64556567867678',
                environment: 'staging',
            };
            const generator = sagas.runBotOnceFlow(action);

            it('should set run bot loading to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(toggleBotLoading(action.id, 'runBotsLoading')));
            });

            it('should call run bot API', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(
                    call(sagas.runBotOnceFromServer, action.projectId, action.categoryId, action.id, action.environment)
                );
            });

            it('should set run bot loading to false', () => {
                // given
                const response = {
                    success: true,
                    errors: null,
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(toggleBotLoading(action.id, 'runBotsLoading')));
            });
        });

        describe('failure path', () => {
            // given
            const action = {
                projectId: '456656767887',
                categoryId: '67567889789089',
                id: '64556567867678',
                environment: 'staging',
            };
            const generator = sagas.runBotOnceFlow(action);

            it('should set run bot loading to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(toggleBotLoading(action.id, 'runBotsLoading')));
            });

            it('should call run bot API', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(
                    call(sagas.runBotOnceFromServer, action.projectId, action.categoryId, action.id, action.environment)
                );
            });

            it('should display alert if API failed', () => {
                // given
                const message = 'Vision bot run failed';
                const response = {
                    success: false,
                    errors: {
                        message,
                    },
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(displayAlert(message, ALERT_LEVEL_DANGER)));
            });

            it('should set run bot loading to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(toggleBotLoading(action.id, 'runBotsLoading')));
            });
        });
    });
});
