import {fromJS} from 'immutable';

import {
    FETCH_BOTS_SUCCESS,
    PREVIEW_BOT,
    TOGGLE_LOADING_STATE,
    SET_SORT_VALUES,
} from '../constants';
import {SortDirections} from '../../../constants/enums';
import botsContainerReducer, {initialState} from '../reducer';
import {DataField, DataType} from 'common-frontend-utils';

describe('botsContainerReducer', () => {
    it('returns the initial state', () => {
        expect(botsContainerReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle action FETCH_PROJECT_SUCCESS and populate bot state', () => {
        // given
        const payload = [
            {name: 'Bot 1'},
        ];
        const action = {
            type: FETCH_BOTS_SUCCESS,
            payload,
        };
        const expectedState = {
            ...initialState.toJS(),
            bots: payload,
        };
        // when
        const result = botsContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle action PREVIEW_PROJECT_BOT and set selected preview bot', () => {
        // given
        const project = {
            name: 'testing',
        };
        const action = {
            type: PREVIEW_BOT,
            project,
        };
        const expectedState = {
            ...initialState.toJS(),
            previewBot: project,
        };
        // when
        const result = botsContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    describe('should handle action RUN_BOT_ONCE_LOADING', () => {
        it('and set run bot id in loading state list', () => {
            // given
            const id = '64647566775667';
            const action = {
                type: TOGGLE_LOADING_STATE,
                id,
                loadingType: 'runBotsLoading',
            };
            const expectedState = {
                ...initialState.toJS(),
                runBotsLoading: {[id]: true},
            };
            // when
            const result = botsContainerReducer(initialState, action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });

        it('and remove run bot id from loading state list', () => {
            // given
            const id = '64647566775667';
            const action = {
                type: TOGGLE_LOADING_STATE,
                id,
                loadingType: 'runBotsLoading',
            };
            const currentState = fromJS({
                ...initialState.toJS(),
                runBotsLoading: {
                    '456675567867678': true,
                    '64647566775667': true,
                    '564645567567567': true,
                },
            });
            const expectedState = {
                ...initialState.toJS(),
                runBotsLoading: {
                    '456675567867678': true,
                    '564645567567567': true,
                    '64647566775667': false,
                },
            };
            // when
            const result = botsContainerReducer(currentState, action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });
    });

    describe('should handle setSortValues action', () => {
        const fields = [
            new DataField({id: 'projectName', label: 'Instance', sortable: true, type: DataType.STRING}),
            new DataField({id: 'status', label: 'Status', sortable: true, type: DataType.STRING}),
        ];

        const sortAscending = [{id: 'projectName', orderBy: SortDirections.ASCENDING}];
        const sortDescending = [{id: 'projectName', orderBy: SortDirections.DESCENDING}];

        const bot1 = {projectName: 'a', status: 'staging'};
        const bot2 = {projectName: 'b', status: 'production'};

        const actionAscending = {
            type: SET_SORT_VALUES,
            fields,
            sort: sortAscending,
        };

        const actionDescending = {
            ...actionAscending,
            sort: sortDescending,
        };

        const {sortFn: sortFnAscending} = botsContainerReducer(initialState, actionAscending).toJS();
        const {sortFn: sortFnDescending} = botsContainerReducer(initialState, actionDescending).toJS();

        it('should be able to sort ascending order', () => {
            expect([bot2, bot1].sort(sortFnAscending)).toEqual([bot1, bot2]);
        });

        it('should be able to sort in descending order', () => {
            expect([bot1, bot2].sort(sortFnDescending)).toEqual([bot2, bot1]);
        });
    });
});
