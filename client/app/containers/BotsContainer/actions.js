/*
 *
 * BotsContainer actions
 *
 */

import {
    BOTS_SORTING_KEY,
    FETCH_BOTS_LOADING,
    FETCH_BOTS,
    FETCH_BOTS_SUCCESS,
    FETCH_BOTS_FAILED,
    PREVIEW_BOT,
    SET_PROJECT_VISIONBOT_STATE,
    PARTIAL_UPDATE_BOT_BY_ID,
    RUN_BOT_ONCE,
    SET_SORT_VALUES,
    TOGGLE_LOADING_STATE,
} from './constants';

import {compareKey} from 'common-frontend-utils';


export function fetchBotsLoading(loading = false) {
    return {
        type: FETCH_BOTS_LOADING,
        loading,
    };
}

export function fetchBots(projectId) {
    return {
        type: FETCH_BOTS,
        projectId,
    };
}

export function fetchBotsSuccess(payload) {
    // mocking that BE sorts this for us. Can be replaced later
    return {
        type: FETCH_BOTS_SUCCESS,
        // mocking that BE sorts this for us. Can be replaced later
        payload: [...payload].sort(compareKey(BOTS_SORTING_KEY)),
    };
}

export function fetchBotsFailed() {
    return {
        type: FETCH_BOTS_FAILED,
    };
}

export function previewBot(project) {
    return {
        type: PREVIEW_BOT,
        project,
    };
}

export function setProjectVisionBotState(projectId, categoryId, id, state = 'production') {
    return {
        type: SET_PROJECT_VISIONBOT_STATE,
        projectId,
        categoryId,
        id,
        state,
    };
}

export function partialBotUpdateById(id, properties = {}) {
    return {
        type: PARTIAL_UPDATE_BOT_BY_ID,
        id,
        properties,
    };
}

export function runBotOnce(projectId, categoryId, id, environment) {
    return {
        type: RUN_BOT_ONCE,
        projectId,
        categoryId,
        id,
        environment,
    };
}

/**
 * @description set new sort param config object.
 * @param {array} fields
 * @param {object} sort
  *@returns {{type: String, sort: Object[], fields: Object[]}}
 */
export function setSortValues(fields, sort) {
    return {
        type: SET_SORT_VALUES,
        sort,
        fields,
    };
}

/**
 * @description toggle bot in array of bots being staged
 * @param {string} id
  *@returns {{type: String, id: String}}
 */
export function toggleBotLoading(id, loadingType) {
    return {
        type: TOGGLE_LOADING_STATE,
        id,
        loadingType,
    };
}
