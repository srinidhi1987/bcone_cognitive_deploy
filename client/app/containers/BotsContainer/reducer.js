/*
 *
 * BotsContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {DataTable} from 'common-frontend-components';

import {
    FETCH_BOTS_SUCCESS,
    PREVIEW_BOT,
    PARTIAL_UPDATE_BOT_BY_ID,
    SET_SORT_VALUES,
    BOTS_SORTING_KEY,
    TOGGLE_LOADING_STATE,
} from './constants';
import {SortDirections} from '../../constants/enums';
import {compareKey} from 'common-frontend-utils';


const initialState = fromJS({
    bots: [],
    sort: [{id: BOTS_SORTING_KEY, orderBy: SortDirections.ASCENDING}],
    sortFn: compareKey(BOTS_SORTING_KEY),
    previewBot: null,
    runBotsLoading: {}, // list of ids in run bot state
    botsDelayingStage: {}, // list of bot ids that are simulating delay in stage toggle
});

function botsContainerReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_BOTS_SUCCESS:
            return state.set('bots', fromJS(action.payload));
        case PREVIEW_BOT:
            return state.set('previewBot', action.project);
        case PARTIAL_UPDATE_BOT_BY_ID:
            return state.set('bots', fromJS(state.get('bots').toJS().map((bot) => {
                if (bot.id === action.id) {
                    return {
                        ...bot,
                        ...action.properties,
                    };
                }
                return bot;
            })));
        case SET_SORT_VALUES: {
            const {sort, fields} = action;
            const sortFn = DataTable.comparator(fields, sort);

            return state.set('sort', sort).set('sortFn', sortFn);
        }
        case TOGGLE_LOADING_STATE: {
            const {id, loadingType} = action;

            return state.update(
                 loadingType,
                 (loadingStateType) => loadingStateType.set(id, !loadingStateType.get(id))
            );
        }
        default:
            return state;
    }
}

export default botsContainerReducer;
export {initialState};
