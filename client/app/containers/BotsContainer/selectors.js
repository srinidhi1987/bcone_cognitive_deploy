import {createSelector} from 'reselect';

/**
 * Direct selector to the botsContainer state domain
 */
const selectBotsContainerDomain = () => (state) => state.get('botsContainer');
const selectAppContainerDomain = () => (state) => state.get('appContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');

/**
 * Other specific selectors
 */
const getProjectsState = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => projectState,
);

const getViewLoadingState = () => createSelector(
    selectAppContainerDomain(),
    (appstate) => appstate.get('viewContainerLoading')
);

const getSortedBots = () => createSelector(
    selectBotsContainerDomain(),
    (botsState) => {
        const bots = botsState.get('bots');
        const sortFn = botsState.get('sortFn');

        return bots.toJS().sort(sortFn);
    }
);

const getPreviewBot = () => createSelector(
    selectBotsContainerDomain(),
    (botsState) => {
        const previewBot = botsState.get('previewBot');
        const previewBotID = previewBot ? previewBot.id : null;

        return botsState.get('bots').toJS().find((p) => p.id === previewBotID) || null;
    }
);

/**
 * Default selector used by BotsContainer
 */

const selectBotsContainer = () => createSelector(
    selectBotsContainerDomain(),
    selectAppContainerDomain(),
    getViewLoadingState(),
    getProjectsState(),
    getSortedBots(),
    getPreviewBot(),
    (substate, appState, loadingState, projectState, bots, previewBot) => {
        return {
            ...substate.toJS(),
            appConfig: appState.get('appConfig').toJS(),
            authToken: appState.get('authToken'),
            isLoading: loadingState,
            projects: projectState ? projectState.get('projects') : [],
            bots,
            previewBot,
        };
    }
);

export default selectBotsContainer;
export {
    selectBotsContainerDomain,
    getProjectsState,
    getViewLoadingState,
};
