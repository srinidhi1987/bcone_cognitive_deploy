/*
 *
 * BotsContainer constants
 *
 */

export const FETCH_BOTS_LOADING = 'app/BotsContainer/FETCH_BOTS_LOADING';
export const FETCH_BOTS = 'app/BotsContainer/FETCH_BOTS';
export const FETCH_BOTS_SUCCESS = 'app/BotsContainer/FETCH_BOTS_SUCCESS';
export const FETCH_BOTS_FAILED = 'app/BotsContainer/FETCH_BOTS_FAILED';
export const PREVIEW_BOT = 'app/BotsContainer/PREVIEW_BOT';
export const SET_PROJECT_VISIONBOT_STATE = 'app/BotsContainer/SET_PROJECT_VISIONBOT_STATE';
export const PARTIAL_UPDATE_BOT_BY_ID = 'app/BotsContainer/PARTIAL_UPDATE_BOT_BY_ID';
export const RUN_BOT_ONCE = 'app/BotsContainer/RUN_BOT_ONCE';
export const SORT_BOTS = 'app/BotsContainer/SORT_BOTS';
export const SET_SORT_VALUES = 'app/BotsContainer/SET_SORT_VALUES';
export const TOGGLE_LOADING_STATE = 'app/BotsContainer/TOGGLE_LOADING_STATE';

export const ALL_COLUMNS_DROPDOWN_VALUE = 'All Fields';
export const BOTS_SORTING_KEY = 'projectName';
export const SEARCH_ALL_PLACEHOLDER = 'Search...';
