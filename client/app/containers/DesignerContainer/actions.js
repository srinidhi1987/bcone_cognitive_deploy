/*
 *
 * AnnotatorContainer actions
 *
 */

import {
    CLEAR_ALL_DRAWN_FIELDS,
    AUTO_CLICK_SIR_FIELD,
    CLEAR_DESIGNER_STORE,
    CLEAR_DRAWN_FIELDS,
    CLEAR_DRAWN_AREAS,
    CLICK_SIR_FIELD,
    CLICK_DRAWN_AREA,
    CLICK_FIELD_OPTION_DRAW,
    CREATE_BOT_AND_TRAIN,
    DEFAULT_ACTION,
    EDIT_BOT_AND_TRAIN,
    EXTRACT_SIR_FIELD_TEXT,
    EXPORT_TO_CSV,
    FETCH_ANNOTATION_CLASSES,
    FETCH_ANNOTATION_CLASSES_SUCCESS,
    FETCH_CATEGORIES_FOR_DESIGNER,
    HIDE_ALL_CONFIRMATION_BOX_VISIBILITY,
    SET_PREVIEW_DATA,
    MAP_REF_COLUMN,
    MAP_SIR_FIELD,
    MAP_TABLE,
    NAVIGATE_BOTS,
    NAVIGATE_PREVIEW_DOCUMENT,
    NEXT_CONFIRMATION_BOX_VISIBILITY,
    POPULATE_BOT_DATA,
    POPULATE_BOT_LABELS,
    POPULATE_PREVIEW_DATA,
    POPULATE_PREVIEW_DOCUMENTS,
    POPULATE_SIR_FIELDS,
    PUT_PROJECT_NAME_IN_STORE,
    REDIRECT_IF_FEATURE_TURNED_OFF,
    SAVE_ALL_SIR_FIELDS,
    SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY,
    SAVE_DESIGNER,
    SET_ACTIVE_PREVIEW_FIELD,
    SET_BOT_PAGE_DATA,
    SET_DOC_PAGE_META,
    SET_DOC_PROPERTIES,
    SET_PAGE_INPUT_VALUE,
    SET_CURRENT_PREVIEW_INDEX,
    SET_IS_LOADING_EDIT_DATA,
    SET_IS_WAITING_FOR_PREVIEW_DATA,
    SET_SIR_FIELD_SELECTION,
    SET_UPLOAD_FILE_LOADING_STATE,
    SET_URL,
    SET_SAVE_DESIGNER_SPINNER_STATE,
    UPLOAD_FILE_TO_DESIGNER,
    UPLOAD_FILE_TO_DESIGNER_SUCCESS,
    SAVE_AND_MOVE_TO_PREVIEW,
    SAVE_BOT,
    SAVE_PROJECT_CATEGORIES,
    SET_ACTIVE_FIELD,
    SET_ACTIVE_FIELD_OPTION,
    SET_ANNOTATOR_MODE,
    SET_CURRENT_PAGE_NUMBER,
    SET_CURRENT_PAGE_NUMBER_AND_SET_SIR_FIELDS,
    SET_CURRENT_ZOOM_SCALE,
    SET_DESIGNER_VIEW_MODE,
    SET_FIELD_VALUE,
    SET_IMAGE_LOADED,
    SET_LAYOUT_RATIO,
    SET_PREVIEW_FIELD_VALUE,
    SET_EDIT_LOADING_STATE,
    SET_PREVIEW_LOADING_STATE,
    SET_SCALE_AMOUNT,
    START_DRAG,
    STOP_DRAG,
    SUGGEST_VALUE,
    TOGGLE_EDIT_PREVIEW,
    TOGGLE_FIELD_CHECKBOX,
    UPDATE_DRAWN_AREAS,
    UPDATE_ALL_DRAWN_PREVIEW_AREAS,
    SELECT_ACTIVE_FIELD_THEN_CREATE_MAPPING,
} from './constants';

import {
    transformBotData,
} from './utilities';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION,
    };
}

export function saveDesigner(payload, url) {
    return {
        type: SAVE_DESIGNER,
        payload,
        url,
    };
}

export function uploadFileToDesigner(file) {
    return {
        type: UPLOAD_FILE_TO_DESIGNER,
        file,
    };
}

export function setUploadFileLoadingState(fileUploading) {
    return {
        type: SET_UPLOAD_FILE_LOADING_STATE,
        fileUploading,
    };
}

export function uploadFileToDesignerSuccess({keyValues, url, metadata}) {
    return {
        type: UPLOAD_FILE_TO_DESIGNER_SUCCESS,
        keyValues,
        url,
        metadata,
    };
}

export function fetchAnnotationClasses() {
    return {
        type: FETCH_ANNOTATION_CLASSES,
    };
}

export function fetchAnnotationClassesSuccess(payload) {
    return {
        type: FETCH_ANNOTATION_CLASSES_SUCCESS,
        payload,
    };
}

/**
 *
 * @param {string} projectId
 * @param {string} categoryId
 * @returns {{type: string, projectId: string, categoryId: string}}
 */
export function createBotAndTrain({projectId, categoryId, username, shouldClearStore, mode}) {
    return {
        type: CREATE_BOT_AND_TRAIN,
        projectId,
        categoryId,
        shouldClearStore,
        username,
        mode,
    };
}

/**
 *
 * @param {string} projectId
 * @param {string} categoryId
 * @returns {{type: string, projectId: string, categoryId: string}}
 */
export function editBotAndTrain(projectId, categoryId) {
    return {
        type: EDIT_BOT_AND_TRAIN,
        projectId,
        categoryId,
    };
}

/**
 *
 * @param {string} projectName
 * @returns {{type: string, projectName: string}}
 */
export function putProjectMetadataInStore({name: projectName, confidenceThreshold}) {
    return {
        type: PUT_PROJECT_NAME_IN_STORE,
        projectName,
        confidenceThreshold,
    };
}

/**
 *
 * @param {Object} payload
 * @returns {{type: string, botData: Object}}
 */
export function populateBotData(payload) {
    return {
        type: POPULATE_BOT_DATA,
        botData: transformBotData(payload),
    };
}

/**
 * populates possible bot label aliases or suggestions
 * @param {Object[]} fields
 * @param {Object[]} columns
 * @returns {{type: string, fields: Object[], columns: Object[]}}
 */
export function populateBotLabels(columns, fields) {
    return {
        type: POPULATE_BOT_LABELS,
        columns,
        fields,
    };
}

export function redirectIfFeatureTurnedOff(appConfig) {
    return {
        type: REDIRECT_IF_FEATURE_TURNED_OFF,
        appConfig,
    };
}

/**
 * clears the redux store state for the DesignerContainer
 *
 * @returns {{type: string}}
 */
export function clearDesignerStore() {
    return {
        type: CLEAR_DESIGNER_STORE,
    };
}

/**
 * sets active field in designer
 * @param {string} id
 * @param {boolean} isTableSettings
 * @returns {{type: string, id: string, isTableSettings: boolean}}
 */
export function setActiveField(id, isTableSettings) {
    return {
        type: SET_ACTIVE_FIELD,
        id,
        isTableSettings,
    };
}

/**
 * sets active field Option in designer
 * @param {string} activeFieldOption
 * @returns {{type: string, activeFieldOption: string}}
 */
export function setActiveFieldOption(activeFieldOption) {
    return {
        type: SET_ACTIVE_FIELD_OPTION,
        activeFieldOption,
    };
}

/**
 * toggles dynamic choice of textbox
 * @param {object} activeField
 * @param {string} fieldOption
 * @returns {{type: string, activeField: object, fieldOption: string}}
 */
export function toggleFieldCheckbox(activeField, fieldOption) {
    return {
        type: TOGGLE_FIELD_CHECKBOX,
        activeField,
        fieldOption,
    };
}

/**
 * sets dynamic choice of field value
 * @param {object} activeField
 * @param {string} fieldName
 * @param {*} fieldValue
 * @returns {{activeField: object, fieldName: string, fieldValue: *, type: string}}
 */
export function setFieldValue(activeField, fieldName, fieldValue) {
    return {
        type: SET_FIELD_VALUE,
        activeField,
        fieldName,
        fieldValue,
    };
}

/**
 * update a drawn area
 * @param {object} drawnArea
 * @returns {{drawnArea: object, type: string}}
 */
export function updateDrawnAreas(drawnAreas, isFromBackend) {
    return {
        type: UPDATE_DRAWN_AREAS,
        drawnAreas,
        isFromBackend,
    };
}

export function setLayoutRatio(ratio) {
    return {
        type: SET_LAYOUT_RATIO,
        ratio,
    };
}

export function clickFieldOptionDrawTool(mode, fieldOption, activeField) {
    return {
        type: CLICK_FIELD_OPTION_DRAW,
        activeField,
        fieldOption,
        mode,
    };
}

export function setAnnotatorMode(mode, fieldOption) {
    return {
        type: SET_ANNOTATOR_MODE,
        mode,
        fieldOption,
    };
}

export function clickSirField(area, activeField, labelSuggestionData, isDrawnBox, fieldOption) {
    return {
        type: CLICK_SIR_FIELD,
        area,
        activeField,
        labelSuggestionData,
        isDrawnBox,
        fieldOption,
    };
}

export function autoMapSirField(area, activeField) {
    return {
        type: AUTO_CLICK_SIR_FIELD,
        area,
        activeField,
    };
}


export function mapSirField(activeField, area) {
    return {
        type: MAP_SIR_FIELD,
        activeField,
        area,
    };
}

export function clearDrawnFields(activeField, fieldOption) {
    return {
        type: CLEAR_DRAWN_FIELDS,
        activeField,
        fieldOption,
    };
}

export function clearDrawnAreas(activeField) {
    return {
        type: CLEAR_DRAWN_AREAS,
        activeField,
    };
}

export function extractSirFieldText(activeField, area) {
    return {
        type: EXTRACT_SIR_FIELD_TEXT,
        activeField,
        area,
    };
}

export function clickDrawnArea(area, mode, activeField) {
    return {
        type: CLICK_DRAWN_AREA,
        activeField,
        area,
        mode,
    };
}

export function startDrag(activeField) {
    return {
        type: START_DRAG,
        activeField,
    };
}

export function stopDrag(activeField, activeFieldOption, drawnAreas) {
    return {
        type: STOP_DRAG,
        activeField,
        activeFieldOption,
        drawnAreas,
    };
}

export function setImageLoaded(imageLoaded) {
    return {
        type: SET_IMAGE_LOADED,
        imageLoaded,
    };
}

export function suggestValue(activeField, labelSuggestionData) {
    return {
        type: SUGGEST_VALUE,
        activeField,
        labelSuggestionData,
    };
}

export function setCurrentZoomScale(currentZoomScale) {
    return {
        type: SET_CURRENT_ZOOM_SCALE,
        currentZoomScale,
    };
}

export function setScaleAmount(scaleAmount) {
    return {
        type: SET_SCALE_AMOUNT,
        scaleAmount,
    };
}

/**
 * saves all categories associated with learning instance
 * @param {Object[]} categories
 * @returns {{type: string, categories: Object[]}}
 */
export function saveProjectCategories(categories) {
    return {
        type: SAVE_PROJECT_CATEGORIES,
        categories,
    };
}

/**
 * navigates to edit next or previous bot
 * @param {Object[]} categories
 * @param {number} currentBotIndex
 * @param {string} direction
 * @param {object} params
 * @returns {{type: string, categories: Object[], currentBotIndex: number, direction: string, params: object}}
 */
export function navigateBots(categories, currentBotIndex, direction, params) {
    return {
        type: NAVIGATE_BOTS,
        categories,
        currentBotIndex,
        direction,
        params,
    };
}

export function selectActiveFieldThenCreateMapping(field) {
    return {
        type: SELECT_ACTIVE_FIELD_THEN_CREATE_MAPPING,
        field,
    };
}

export function saveBot(params, successDestination = null, sendToProduction = false) {
    return {
        type: SAVE_BOT,
        successDestination,
        sendToProduction,
        params,
    };
}


export function saveAndCloseConfirmationBoxVisibility(isVisible) {
    return {
        type: SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY,
        show: isVisible,
    };
}


export function nextConfirmationBoxVisibility(isVisible) {
    return {
        type: NEXT_CONFIRMATION_BOX_VISIBILITY,
        show: isVisible,
    };
}

export function hideAllConfirmationBoxVisibility() {
    return {
        type: HIDE_ALL_CONFIRMATION_BOX_VISIBILITY,
    };
}

export function mapTable(table) {
    return {
        type: MAP_TABLE,
        table,
    };
}

export function mapRefColumn({Id, refColumn}) {
    return {
        type: MAP_REF_COLUMN,
        refColumn,
        Id,
    };
}

export function populatePreviewData(payload) {
    return {
        type: POPULATE_PREVIEW_DATA,
        previewData: {...payload},
    };
}

export function populatePreviewDocuments(previewDocuments) {
    return {
        type: POPULATE_PREVIEW_DOCUMENTS,
        previewDocuments,
    };
}

export function setActivePreviewField(id) {
    return {
        type: SET_ACTIVE_PREVIEW_FIELD,
        id,
    };
}

export function setPreviewFieldValue(activeField, property, value) {
    return {
        type: SET_PREVIEW_FIELD_VALUE,
        activeField,
        property,
        value,
    };
}

export function toggleEditPreview(mode, shouldShowLoadingSpinner, shouldReloadBotData, params) {
    return {
        type: TOGGLE_EDIT_PREVIEW,
        mode,
        shouldShowLoadingSpinner,
        shouldReloadBotData,
        params,
    };
}

export function setDesignerViewMode(mode) {
    return {
        type: SET_DESIGNER_VIEW_MODE,
        mode,
    };
}

/**
 * sets preview loading state
 * @param {boolean} isLoading
 * @returns {type: string, isLoading: boolean}
 */
export function setPreviewLoadingState(isLoading) {
    return {
        type: SET_PREVIEW_LOADING_STATE,
        isLoading,
    };
}

/**
 * sets editLoading state
 * @param {boolean} isLoading
 * @returns {type: string, isLoading: boolean}
 */
export function setEditLoadingState(isLoading) {
    return {
        type: SET_EDIT_LOADING_STATE,
        isLoading,
    };
}

/**
 * saves document data before moving to preview
 * @param {Object} params
 * @returns {type: string, params: Object}
 */
export function saveAndMoveToPreview(params, username) {
    return {
        type: SAVE_AND_MOVE_TO_PREVIEW,
        params,
        username,
    };
}

/**
 * navigates to another preview document
 * @param {number} newCurrentIndex
 * @param {string} projectId
 * @param {string} categoryId
 * @param {string} visionBotId
 * @param {string} documentId
 * @returns {type: string, newIndex: newCurrentIndex, projectId: string, categoryId: string, documentId: string}
 */
export function navigatePreviewDocument(newCurrentIndex, projectId, categoryId, visionBotId, documentId) {
    return {
        type: NAVIGATE_PREVIEW_DOCUMENT,
        currentIndex: newCurrentIndex,
        projectId,
        categoryId,
        visionBotId,
        documentId,
    };
}

/**
 * sets currentPreviewIndex property in reducers
 * @param {number} newIndex
 * @returns {type: string, newIndex: number}
 */
export function setCurrentPreviewDocument(newIndex) {
    return {
        type: SET_CURRENT_PREVIEW_INDEX,
        newIndex,
    };
}

/**
 * sets url property in reducers
 * @param {string} url
 * @returns {type: string, url: string}
 */
export function setUrl(url) {
    return {
        type: SET_URL,
        url,
    };
}

/**
 * return action object for type export to csv
 * @param {object} data
 */
export function exportToCSV({projectId, categoryId, data}) {
    return {
        type:EXPORT_TO_CSV,
        projectId,
        categoryId,
        data,
    };
}

export function fetchProjectCategoriesForDesigner(projectId, categoryId, visionBotId) {
    return {
        type: FETCH_CATEGORIES_FOR_DESIGNER,
        projectId,
        categoryId,
        visionBotId,
    };
}

export function clearAllDrawnFields() {
    return {
        type: CLEAR_ALL_DRAWN_FIELDS,
    };
}


export function setBotPageData(pageData) {
    return {
        type: SET_BOT_PAGE_DATA,
        pageData,
    };
}

/**
 * navigates to another preview page and sets meta
 * @param {number} pageNumber
 * @param {string} projectId
 * @param {string} categoryId
 * @param {string} username
 * @param {string} mode
 * @returns {type: string, pageNumber: number, projectId: string, categoryId: string, username: string, mode: string}
 */
export function setCurrentPageNumberAndSetSirFields(pageNumber, projectId, categoryId, username, mode) {
    return {
        type: SET_CURRENT_PAGE_NUMBER_AND_SET_SIR_FIELDS,
        pageNumber,
        projectId,
        categoryId,
        username,
        mode,
    };
}

/**
 * sets page number
 * @param {number} pageNumber
 * @returns {type: string, pageNumber: number}
**/
export function setCurrentPageNumber(pageNumber) {
    return {
        type: SET_CURRENT_PAGE_NUMBER,
        pageNumber,
    };
}

/**
 * sets page number input to value
 * @param {number} pageNumber
 * @returns {type: string, pageNumber: number}
**/
export function setPageInputValue(pageNumber) {
    return {
        type: SET_PAGE_INPUT_VALUE,
        pageNumber,
    };
}

/**
 * navigates to another preview page and sets meta
 * @param {number} pageNumber
 * @param {string} projectId
 * @param {string} categoryId
 * @param {string} username
 * @param {string} mode
 * @returns {type: string, pageNumber: number, projectId: string, categoryId: string, username: string}
 */
export function setSirFieldSelection(pageNumber, projectId, categoryId, username) {
    return {
        type: SET_SIR_FIELD_SELECTION,
        pageNumber,
        projectId,
        categoryId,
        username,
    };
}

/**
 * sets new sir fields
 * @param {Object[]} sirFields
 * @returns {type: string, sirFields: Object[]}
**/
export function populateSirFields(sirFields) {
    return {
        type: POPULATE_SIR_FIELDS,
        sirFields,
    };
}

/**
 * toggles edit save spinner state
 * @param {boolean} shouldShowSpinner
 * @returns {type: string, shouldShowSpinner: boolean}
**/
export function setEditSaveSpinnerState(shouldShowSpinner) {
    return {
        type: SET_SAVE_DESIGNER_SPINNER_STATE,
        shouldShowSpinner,
    };
}

/**
 * navigates to another preview page and sets meta
 * @param {string} projectId
 * @param {string} categoryId
 * @param {string} username
 * @param {number} pageNumber
 * @returns {type: string, projectId: string, categoryId: string, username: string, pageNumber: number,}
 */
export function setPreviewData(projectId, categoryId, username, pageNumber = 1) {
    return {
        type: SET_PREVIEW_DATA,
        projectId,
        categoryId,
        username,
        pageNumber,
    };
}

/**
 * sets current doc meta
 * @param {number} height
 * @param {number} width
 * @returns {type: string, height: number, width: number}
**/
export function setDocPageMeta(height, width) {
    return {
        type: SET_DOC_PAGE_META,
        height,
        width,
    };
}

/**
 * sets current doc properties
 * @param {Object} docProperties
 * @returns {type: string, docProperties: Object}
**/
export function setDocProperties(docProperties) {
    return {
        type: SET_DOC_PROPERTIES,
        docProperties,
    };
}

/**
 * toggles is waiting for preview data spinner state
 * @param {boolean} isWaiting
 * @returns {type: string, isWaiting: boolean}
**/
export function setIsWaitingForPreviewData(isWaiting) {
    return {
        type: SET_IS_WAITING_FOR_PREVIEW_DATA,
        isWaiting,
    };
}

/**
 * toggles is waiting for preview data spinner state
 * @param {boolean} isLoading
 * @returns {type: string, isLoading: boolean}
**/
export function setIsLoadingEditData(isLoading) {
    return {
        type: SET_IS_LOADING_EDIT_DATA,
        isLoading,
    };
}

/**
 * sets all non standard 'drawn' preview areas at once
 * @param {Object[]} drawnPreviewAreas
 * @returns {type: string, sessionId: Object[]}
**/
export function updateAllDrawnPreviewAreas(drawnPreviewAreas) {
    return {
        type: UPDATE_ALL_DRAWN_PREVIEW_AREAS,
        drawnPreviewAreas,
    };
}

/**
 * caches full array of sir fields so we can paginate without calling api
 * @param {Object[]} allSirFields
 * @returns {type: string, allSirFields: Object[]}
**/
export function saveAllSirFields(allSirFields) {
    return {
        type: SAVE_ALL_SIR_FIELDS,
        allSirFields,
    };
}
