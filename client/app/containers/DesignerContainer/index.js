/*
*
* DesignerContainer
*
*/

import React from 'react';
import {connect} from 'react-redux';
import {autobind} from 'core-decorators';
import {browserHistory} from 'react-router';

import selectDesignerContainer from './selectors';
import DesignerPageLayout from '../../components/DesignerPageLayout';
import DesignerPageWizard from '../../components/DesignerPageWizard/index';

import FieldsValidationLayout from '../../components/PreviewLayout/index';
import {
    clearDesignerStore,
    clickDrawnArea,
    clickFieldOptionDrawTool,
    clickSirField,
    createBotAndTrain,
    exportToCSV,
    fetchProjectCategoriesForDesigner,
    navigateBots,
    nextConfirmationBoxVisibility,
    navigatePreviewDocument,
    redirectIfFeatureTurnedOff,
    uploadFileToDesigner,
    saveAndCloseConfirmationBoxVisibility,
    saveBot,
    setActiveField,
    setActiveFieldOption,
    setActivePreviewField,
    setCurrentPageNumberAndSetSirFields,
    setEditLoadingState,
    setPageInputValue,
    setPreviewFieldValue,
    setPreviewLoadingState,
    setCurrentZoomScale,
    setFieldValue,
    setImageLoaded,
    setLayoutRatio,
    setScaleAmount,
    setSirFieldSelection,
    startDrag,
    stopDrag,
    toggleEditPreview,
    toggleFieldCheckbox,
    updateDrawnAreas,
    saveAndMoveToPreview,
} from './actions';

import {getLabelSuggestionData, getMappedFieldDef} from '../../utils/designer';

import {
    DESIGNER_AREA_TYPES,
    DESIGNER_FIELD_THEMES,
    FIELD_OPTIONS,
    VISION_BOT_MODES,
} from '../../constants/enums';
import {EMPTY_BOUNDS, MIN_ZOOM_SCALE, MAX_ZOOM_SCALE} from './constants';
import {ANNOTATION_MODES} from '../../components/AnnotationEngine/constants/enums';

import {SUCCESS_DESTINATION} from './constants';

const {EDIT} = VISION_BOT_MODES;

const initialZoomMetadata = {
    currentZoomScale: 1,
    scaleAmount: 0.25,
};

/**
 * @enum {string}
 * @readonly
 */
const ZOOM_ACTION_ENUM = {
    zoomIn: '+',
    zoomOut: '-',
};

export class DesignerContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DesignerContainer';

    constructor(props) {
        super(props);
        this.navigateNextDocPageHandler = this.navigateDocPageHandler.bind(null, 'next');
        this.navigatePreviousDocPageHandler = this.navigateDocPageHandler.bind(null, 'previous');

        // this.downloadAnnotations = this.downloadAnnotations.bind(this);
        // this.changeClassHandler = this.changeClassHandler.bind(this);
    }

    state = {
        objects: [],
        fields: [],
        fileUploaded: false,
        processedCount: 3,
        fieldColumnTopOffset: 0,
        selectedFieldType: DESIGNER_AREA_TYPES.FIELD, // Temporary state used to mock state or right colum COG-2882
        ...initialZoomMetadata,
    };

    componentWillMount() {
        const {appState: {user: {username}}, createBotAndTrain, params} = this.props;
        const {id: projectId, botid: categoryId, mode} = params;
        // call api that edits bot and creates it if the bot doesn't exist yet
        createBotAndTrain({projectId, categoryId, username, shouldClearStore: false, mode});


        this.getCategories = setInterval(() => {
            this.props.fetchProjectCategoriesForDesigner(this.props.params.id, this.props.params.botid, this.props.visionBotId);
        }, 3000);
    }

    componentWillUnmount() {
        const {clearDesignerStore} = this.props;
        clearDesignerStore();
        clearInterval(this.getCategories);
    }

    componentWillReceiveProps(nextProps) {
        const {
            appState: {user: {username}},
            createBotAndTrain,
            params,
            redirectIfFeatureTurnedOff,
            shouldRedirect,
        } = this.props;

        if (shouldRedirect) {
            redirectIfFeatureTurnedOff();
        }

        // since react doesnt actually remount the component on refresh if it knows you are in the
        // same container, we need to manually trigger a createBotAndTrain with 'shouldClearStore' param set to true;
        // seems like react-router kind of hi jacks the browser refresh and reload fucntionality here
        const nextParams = nextProps.params;

        // we must wait for the browser history to change botid since its not the first prop to change after we do redirect
        if (browserHistory.getCurrentLocation().query.refresh === 'true' && params.botid !== nextParams.botid) {
            createBotAndTrain({projectId: params.id, categoryId: nextParams.botid, username, shouldClearStore: true});

            browserHistory.push({...browserHistory.getCurrentLocation(), query: {refresh: 'false'}});

            return;
        }
    }

    render() {
        const {
            selectedFieldType,
            fieldColumnTopOffset,
            scaleAmount,
        } = this.state;
        const {
            activeField,
            activeFieldId,
            activeFieldOption,
            allMapped,
            appState,
            botCanNavigate,
            canNavigateBotPage,
            currentDocumentPage,
            currentPreviewDocument,
            currentZoomScale,
            editLoading,
            fileUploading,
            imageLoaded,
            isLoadingEditData,
            isSavingEditData,
            isWaitingForPreviewData,
            metaData,
            mode,
            nextConfirmationBoxVisibility,
            objects,
            pageData,
            pageInputValue,
            params,
            previewData,
            previewDocuments,
            previewLoading,
            projectName,
            fields,
            saveAndCloseConfirmationBoxVisibility,
            clickSource,
            staticAnnotations,
            tables,
            url,
            wizardElementIds,
        } = this.props;

        // classes utilized the AnnotatorEngine
        const annotationClasses = [DESIGNER_FIELD_THEMES.TEXT, DESIGNER_FIELD_THEMES.NUMBER, DESIGNER_FIELD_THEMES.DATE];

        return (
            <DesignerPageLayout
                activeField={activeField}
                activeFieldId={activeFieldId}
                activeFieldOption={activeFieldOption}
                allMapped={allMapped}
                allPreviewFieldsExtractedConfidently={previewData.allPreviewFieldsExtractedConfidently}
                annotationClasses={annotationClasses}
                appState={appState}
                areaClickHandler={this.areaClickHandler}
                botCanNavigate={botCanNavigate}
                canNavigateBotPage={canNavigateBotPage}
                cancelNextConfirmationBoxHandler={this.cancelNextConfirmationBoxHandler}
                currentPreviewDocument={currentPreviewDocument}
                cancelSaveAndCloseConfirmationBoxHandler={this.cancelSaveAndCloseConfirmationBoxHandler}
                changePage={this.changePageHandler}
                currentDocPage={currentDocumentPage}
                currentZoomScale={currentZoomScale}
                docMeta={metaData}
                editDocumentHandler={this.goToEdit}
                editLoading={editLoading}
                exportToCSV={this.exportToCSV}
                fieldColumnTopOffset={fieldColumnTopOffset}
                fieldOptionDrawToolClickHandler={this.fieldOptionDrawToolClickHandler}
                fields={fields}
                fileUploading={fileUploading}
                handleResetZoom={this.reset}
                handleZoomIn={this.zoomIn}
                handleZoomOut={this.zoomOut}
                imageLoadHandler={this.imageLoadHandler}
                imageLoaded={imageLoaded}
                isLoadingEditData={isLoadingEditData}
                isSavingEditData={isSavingEditData}
                isWaitingForPreviewData={isWaitingForPreviewData}
                leftSideComponent={this.renderLeftSideComponent()}
                mode={mode}
                navigateBotsHandler={this.navigateBotsHandler}
                navigateNextDocPageHandler={this.navigateNextDocPageHandler}
                navigatePreviousDocPageHandler={this.navigatePreviousDocPageHandler}
                nextConfirmationBoxVisibility={nextConfirmationBoxVisibility}
                navigateDocumentsHandler={this.navigateDocumentsHandler}
                objects={objects}
                pageInputChangeHandler={this.onPageInputChange}
                onUpdate={this.updateObjects}
                pageData={pageData}
                pageInputValue={pageInputValue}
                params={params}
                previewDocumentHandler={this.goToPreview}
                previewDocuments={previewDocuments}
                previewLoading={previewLoading}
                projectName={projectName}
                saveAndCloseClickHandler={this.saveAndCloseClickHandler}
                saveAndCloseConfirmationBoxVisibility={saveAndCloseConfirmationBoxVisibility}
                saveBotAndSendToProductionHandlerForNext={this.saveBotAndSendToProductionHandlerForNext}
                saveBotAndSendToProductionHandlerForSaveAndClose={this.saveBotAndSendToProductionHandlerForSaveAndClose}
                saveHandlerForNext={this.saveHandlerForNext}
                saveHandlerForSaveAndClose={this.saveHandlerForSaveAndClose}
                scaleAmount={scaleAmount}
                selectActiveFieldOptionHandler={this.selectActiveFieldOptionHandler}
                selectFieldHandler={this.selectFieldHandler}
                selectedFieldType={selectedFieldType}
                setFieldValueHandler={this.setFieldValueHandler}
                setLayoutRatio={this.setLayoutRatio}
                showConfirmation={this.showConfirmation}
                startDragHandler={this.startDragHandler}
                staticAnnotations={staticAnnotations}
                stopDragHandler={this.stopDragHandler}
                clickSource={clickSource}
                tables={tables}
                url={url}
                wizardElementIds={wizardElementIds}
                visionBotMode={this.props.params.mode}
            />
        );
    }

    renderLeftSideComponent() {
        const {
            selectedFieldType,
            fieldColumnTopOffset,
        } = this.state;

        const {
          activeField,
          activePreviewField,
          activeFieldOption,
          botCanNavigate,
          editLoading,
          fields,
          isSavingEditData,
          isLoadingEditData,
          isWaitingForPreviewData,
          mode,
          previewData,
          previewLoading,
          tables,
          wizardElementIds,
        } = this.props;

        if (this.props.params.mode !== VISION_BOT_MODES.EDIT) {
            return (
                <FieldsValidationLayout
                    activeField={activePreviewField}
                    botCanNavigate={botCanNavigate}
                    fields={previewData.fields}
                    isWaitingForPreviewData={isWaitingForPreviewData}
                    tables={previewData.tables}
                    navigatePreviousHandler={this.navigatePreviousHandler}
                    navigateNextHandler={this.navigateNextPreviewHandler}
                    editLoading={editLoading}
                    previewLoading={previewLoading}
                    setFieldValueHandler={this.setPreviewFieldValueHandler}
                    selectFieldHandler={this.selectPreviewFieldHandler} />
            );
        }

        return (
            <DesignerPageWizard
                activeField={activeField}
                activeFieldOption={activeFieldOption}
                botCanNavigate={botCanNavigate}
                documentPreviewHandler={this.documentPreviewHandler}
                editLoading={editLoading}
                fieldColumnTopOffset={fieldColumnTopOffset}
                fieldOptionDrawToolClickHandler={this.fieldOptionDrawToolClickHandler}
                fields={fields}
                isLoadingEditData={isLoadingEditData}
                isSavingEditData={isSavingEditData}
                mode={mode}
                navigateDocumentsHandler={this.navigateDocumentsHandler}
                navigateNextBotsHandler={this.navigateNextBotsHandler}
                previewLoading={previewLoading}
                previewDocumentHandler={this.goToPreview}
                selectActiveFieldOptionHandler={this.selectActiveFieldOptionHandler}
                selectFieldHandler={this.selectFieldHandler}
                selectedFieldType={selectedFieldType}
                setFieldValueHandler={this.setFieldValueHandler}
                tables={tables}
                toggleFieldCheckboxHandler={this.checkboxHandler}
                wizardElementIds={wizardElementIds}
            />
        );
    }

    @autobind navigatePreviewHandler(direction) {
        const {id: projectId, botid: categoryId} = this.props.params;
        const {visionBotId, previewNavigationFiles, currentPreviewDocument, url} = this.props;
        const increment = direction === 'next' ? 1 : -1;

        this.props.navigatePreviewDocument(currentPreviewDocument + increment, projectId, categoryId, visionBotId, previewNavigationFiles[direction], url);
    }

    @autobind navigateNextPreviewHandler() {
        this.navigatePreviewHandler('next');
    }

    @autobind navigatePreviousHandler() {
        this.navigatePreviewHandler('previous');
    }

    @autobind navigateDocPageHandler(direction) {
        const {
            appState: {user: {username}},
            params,
        } = this.props;

        const increment = direction === 'next' ? 1 : -1;
        const newPageNumber = this.props.currentDocumentPage + increment;

        this.props.setCurrentPageNumberAndSetSirFields(newPageNumber, params.id, params.botid, username, params.mode);
    }

    @autobind getOriginalDimension(value, layoutRatio) {
        return Math.round(value * layoutRatio);
    }

    @autobind updateObjects(drawnAreas, areaIndex) {
        this.props.updateDrawnAreas(drawnAreas, areaIndex);
    }

    @autobind setLayoutRatio(ratio) {
        this.props.setLayoutRatio(ratio);
    }

    @autobind saveAndCloseClickHandler() {
        this.props.showSaveAndCloseConfirmationBox();
    }

    // Save and close confirmation Modal handler
    @autobind cancelSaveAndCloseConfirmationBoxHandler() {
        this.props.hideSaveAndCloseConfirmationBox();
    }

    @autobind saveHandlerForSaveAndClose() {
        const {params, saveBotAndFinish} = this.props;
        return saveBotAndFinish(params);
    }

    @autobind saveBotAndSendToProductionHandlerForSaveAndClose() {
        const {
            params,
            saveBotAndMoveToProductionAndFinish,
        } = this.props;

        return saveBotAndMoveToProductionAndFinish(params);
    }

    // Next confirmation Modal handler
    @autobind cancelNextConfirmationBoxHandler() {
        this.props.hideNextConfirmationBox();
    }

    @autobind saveHandlerForNext() {
        const {params, saveBotAndMoveToNextBot} = this.props;
        return saveBotAndMoveToNextBot(params);
    }

    @autobind saveBotAndSendToProductionHandlerForNext() {
        const {
            params,
            saveBotAndMoveToProductionAndMoveToNextBot,
        } = this.props;
        // require to check for the last bot in the list
        return saveBotAndMoveToProductionAndMoveToNextBot(params);
    }

    @autobind toggleEditPreview(mode, shouldShowLoadingSpinner, shouldReloadBotData, params) {
        this.props.toggleEditPreview(mode, shouldShowLoadingSpinner, shouldReloadBotData, params);
    }

    @autobind goToEdit() {
        const {
            params,
            appState: {user: {username}},
        } = this.props;

        this.toggleEditPreview(EDIT, false, true, {...params, username});
    }

    @autobind goToPreview() {
        const {appState: {user: {username}}, params, saveBotAndPreview} = this.props;
        saveBotAndPreview(params, username);
    }

    /**
     * navigates to next or previous bot based on direction (previous or next)
     * @param {string} direction
     */
    @autobind navigateDocumentsHandler(direction) {
        const {currentBotIndex, params, projectCategories} = this.props;

        this.props.navigateBots(projectCategories, currentBotIndex, direction, params);
    }

    @autobind navigateNextBotsHandler() {
        this.props.showNextConfirmationBox();
    }

    /**
     * Deletes an annotation object from the objects array
     * @param {number} objectIndexToDelete
     */
    @autobind deleteDesignerField(objectIndexToDelete) {
        const {objects} = this.state;

        this.setState({
            objects: objects.filter((obj, index) => index !== objectIndexToDelete),
            selectedObject: objects.length > 1 ? objects.length - 2 : 0,
        });
    }

    calculateProcessWidth(current, total) {
        return (current / total) * 100;
    }

    /**
     * reset zoom scale in state to original values
     */
    @autobind reset() {
        this.props.setCurrentZoomScale(initialZoomMetadata.currentZoomScale);
        this.props.setScaleAmount(initialZoomMetadata.scaleAmount);
    }

    @autobind zoomIn() {
        const {currentZoomScale} = this.props;
        if (currentZoomScale === MAX_ZOOM_SCALE) {
            return;
        }
        const scale = this._getScale(ZOOM_ACTION_ENUM.zoomIn);
        this.props.setCurrentZoomScale(scale);
    }

    @autobind zoomOut() {
        const {currentZoomScale} = this.props;
        if (currentZoomScale === MIN_ZOOM_SCALE) {
            return;
        }
        const scale = this._getScale(ZOOM_ACTION_ENUM.zoomOut);
        this.props.setCurrentZoomScale(scale);
    }

    @autobind changePageHandler(event) {
        const newPage = Math.round(event.target[0].value) || 1;
        const {
            appState: {user: {username}},
            params,
        } = this.props;
        this.props.setCurrentPageNumberAndSetSirFields(newPage, params.id, params.botid, username, params.mode);

        event.preventDefault();
    }

    @autobind onPageInputChange(event) {
        this.props.setPageInputValue(event.target.value);
    }

    /**
     * handler for selecting a field or table column. Triggers artificial 'scroll'
     *
     * @param {{currentTarget: {offsetTop: Number}}} event
     * @param {string} id
     */
    @autobind selectFieldHandler(selectedFieldType, id) {
        const isTableSettings = selectedFieldType === DESIGNER_AREA_TYPES.TABLE;
        // const fieldColumnTopOffset = -(event.currentTarget.offsetTop) + INITIAL_FIELD_COLUMN_TOP_OFFSET;
        this.setState({selectedFieldType});
        this.props.setActiveField(id, isTableSettings);
    }

    /**
     * handler for selecting a field or table column. Triggers artificial 'scroll'
     *
     * @param {string} fieldOption
     */
    @autobind selectActiveFieldOptionHandler(fieldOption) {
        this.props.setActiveFieldOption(fieldOption);
    }

    @autobind selectPreviewFieldHandler(event, field) {
        this.props.setActivePreviewField(field.fieldGuid);
    }

    @autobind setPreviewFieldValueHandler() {
        // NOTE: since right now preview is read only
        // this.props.setPreviewFieldValue(activeField, fieldProperty, value);
    }
    /**
     * sets a field value on change in the designer
     *
     * @param {Object} activeField
     * @param {string} fieldName
     * @param {*} fieldValue
     */
    @autobind setFieldValueHandler(activeField, fieldName, fieldValue) {
        this.props.setFieldValue(activeField, fieldName, fieldValue);
    }

    @autobind startDragHandler(activeField) {
        this.props.startDrag(activeField);
    }

    @autobind stopDragHandler(activeField) {
        const {activeFieldOption, objects} = this.props;

        this.props.stopDrag(activeField, activeFieldOption, objects);
    }

    @autobind fieldOptionDrawToolClickHandler(mode, fieldOption) {
        this.props.clickFieldOptionDrawTool(mode, fieldOption, this.props.activeField);
    }

    @autobind areaClickHandler(area) {
        const {
            activeField,
            activeFieldOption,
            clickDrawnArea,
            clickSirField,
            layouts,
            mode,
            params,
            sirFields,
            pageData,
            currentDocumentPage,
        } = this.props;

        // do not click when
        // mode is free or preview mode
        // or active field's label input is selected and user have clicked checkbox 'Does not have a field label'
        const doNotClick = mode !== ANNOTATION_MODES.FREE || params.mode === 'preview' ||
        (activeFieldOption === FIELD_OPTIONS.LABEL && activeField.IsLabelOptional);
        if (doNotClick) {
            return;
        }

        // right now, we only want to suggest a value if we are mapping a label, and there is no value already mapped or selected
        const shouldSuggestValue = activeFieldOption === FIELD_OPTIONS.LABEL;
        // since we only hit the label suggestion api if we supply this data, we either supply the label suggestion Data
        // or supply a null value so that the check for labelSuggestionData comes up falsey in our saga

        const {Bounds, Text: sirText} = sirFields[area.index - pageData[currentDocumentPage - 1].startIndex];
        const columns = layouts[0].Tables.reduce((columns, table) => [...columns, ...table.Columns], []);
        const {Fields: fields} = layouts[0];
        const matchingDef = columns.concat(fields).find((fieldDef) => fieldDef.Id === activeField.Id) || getMappedFieldDef({
            labelBounds: Bounds,
            DisplayValue: null,
            EndsWith: null,
            Label: sirText,
            StartsWith: null,
            Id: activeField.Id,
            mappedSirDisplayValue: true,
            matchingField: {},
            relativeBounds: EMPTY_BOUNDS,
        });
        const labelSuggestionData = shouldSuggestValue ?
        // move all these properties to getLabelSuggestionData
        {
            ...getLabelSuggestionData(this.props),
            fieldDefId: activeField.Id,
            fieldLayout: {...matchingDef, Bounds},
            sirFields,
        } : null;


        // if we click on a static annotation, we want to fire the clickSirField action,
        // otherwise we know the user clicked on a drawn annotation and we want to fire a different action
        area.isStatic ?
            clickSirField(area, activeField, labelSuggestionData, false, activeFieldOption) :
            clickDrawnArea(area, activeField);
    }

    /**
     * sets a field value on change in the designer
     *
     * @param {Object} activeField
     * @param {string} fieldOption
     */
    @autobind checkboxHandler(activeField, fieldOption) {
        this.props.toggleFieldCheckbox(activeField, fieldOption);
    }

    @autobind imageLoadHandler(isLoaded) {
        this.props.setImageLoaded(isLoaded);
    }

    @autobind exportToCSV() {
        const {exportToCSV, originalPreviewData, params} = this.props;
        exportToCSV({projectId: params.id, categoryId: params.botid, data: originalPreviewData});
    }
    /**
     *
     * @param {ZOOM_ACTION_ENUM} zoomAction
     * @returns {number}
     */
    _getScale(zoomAction) {
        const {currentZoomScale, scaleAmount} = this.props;
        switch (zoomAction) {
            case ZOOM_ACTION_ENUM.zoomIn:
                return currentZoomScale + scaleAmount;
            case ZOOM_ACTION_ENUM.zoomOut:
                return currentZoomScale - scaleAmount;
            default:
                return currentZoomScale;
        }
    }
}

const mapStateToProps = selectDesignerContainer();

function mapDispatchToProps(dispatch) {
    const {NEXT, LI_DETAIL} = SUCCESS_DESTINATION;
    return {
        saveBotAndMoveToProductionAndFinish: (params) => dispatch(saveBot(params, LI_DETAIL, true)),
        saveBotAndMoveToProductionAndMoveToNextBot: (params) => dispatch(saveBot(params, NEXT, true)),
        saveBotAndMoveToNextBot: (params) => dispatch(saveBot(params, NEXT, false)),
        saveBotAndFinish: (params) => dispatch(saveBot(params, LI_DETAIL, false)),
        saveBotAndPreview: (params, name) => dispatch(saveAndMoveToPreview(params, name)),
        uploadFileToDesigner: (file) => dispatch(uploadFileToDesigner(file)),
        createBotAndTrain: (options) => dispatch(createBotAndTrain(options)),
        redirectIfFeatureTurnedOff: () => dispatch(redirectIfFeatureTurnedOff()),
        clearDesignerStore: () => dispatch(clearDesignerStore()),
        setActiveField: (fieldId, isTableSettings) => dispatch(setActiveField(fieldId, isTableSettings)),
        setActiveFieldOption: (activeFieldOption) => dispatch(setActiveFieldOption(activeFieldOption)),
        setFieldValue: (activeField, fieldName, value) => dispatch(setFieldValue(activeField, fieldName, value)),
        setLayoutRatio: (ratio) => dispatch(setLayoutRatio(ratio)),
        startDrag: (activeField) => dispatch(startDrag(activeField)),
        stopDrag: (activeField, activeFieldOption, drawnAreas) => dispatch(stopDrag(activeField, activeFieldOption, drawnAreas)),
        toggleFieldCheckbox: (activeField, fieldOption) => dispatch(toggleFieldCheckbox(activeField, fieldOption)),
        clickFieldOptionDrawTool: (mode, fieldOption, activeField) => dispatch(clickFieldOptionDrawTool(mode, fieldOption, activeField)),
        updateDrawnAreas: (drawnAreas) => dispatch(updateDrawnAreas(drawnAreas)),
        clickSirField: (area, activeField, labelSuggestionData) => dispatch(clickSirField(area, activeField, labelSuggestionData)),
        clickDrawnArea: (area, mode, index, activeField) => dispatch(clickDrawnArea(area, mode, index, activeField)),
        navigatePreviewDocument: (newCurrentIndex, projectId, categoryId, visionBotId, documentId, url) => dispatch(navigatePreviewDocument(newCurrentIndex, projectId, categoryId, visionBotId, documentId, url)),
        setActivePreviewField: (id) => dispatch(setActivePreviewField(id)),
        setPreviewFieldValue: (activeField, property, value) => dispatch(setPreviewFieldValue(activeField, property, value)),
        setImageLoaded: (isLoaded) => dispatch(setImageLoaded(isLoaded)),
        setScaleAmount: (scaleAmount) => dispatch(setScaleAmount(scaleAmount)),
        setCurrentZoomScale: (zoomScale) => dispatch(setCurrentZoomScale(zoomScale)),
        toggleEditPreview: (mode, shouldShowLoadingSpinner, shouldReloadBotData, params) => dispatch(toggleEditPreview(mode, shouldShowLoadingSpinner, shouldReloadBotData, params)),
        navigateBots: (categories, currentBotIndex, direction, params) => dispatch(navigateBots(categories, currentBotIndex, direction, params)),
        setCurrentPageNumberAndSetSirFields: (pageNumber, projectId, categoryId, username, mode) => dispatch(setCurrentPageNumberAndSetSirFields(pageNumber, projectId, categoryId, username, mode)),
        setPageInputValue: (pageNumber) => dispatch(setPageInputValue(pageNumber)),
        showSaveAndCloseConfirmationBox: () => dispatch(saveAndCloseConfirmationBoxVisibility(true)),
        showNextConfirmationBox: () => dispatch(nextConfirmationBoxVisibility(true)),
        hideSaveAndCloseConfirmationBox: () => dispatch(saveAndCloseConfirmationBoxVisibility(false)),
        hideNextConfirmationBox: () => dispatch(nextConfirmationBoxVisibility(false)),
        setPreviewLoadingState: (isLoading) => dispatch(setPreviewLoadingState(isLoading)),
        setEditLoadingState: (isLoading) => dispatch(setEditLoadingState(isLoading)),
        exportToCSV: (data) => dispatch(exportToCSV(data)),
        fetchProjectCategoriesForDesigner: (projectId, categoryId, visionBotId) => dispatch(fetchProjectCategoriesForDesigner(projectId, categoryId, visionBotId)),
        setSirFieldSelection: (newPage, projectId, categoryId, username) => dispatch(setSirFieldSelection(newPage, projectId, categoryId, username)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DesignerContainer);
