import flatten from 'lodash/flatten';

//
// type FieldDef = {
//   $type: string,
//   DefaultValue: string | number,
//   EndsWith: string,
//   FormatExpression: string,
//   Formula: string,
//   Id: string,
//   IsDeleted: boolean,
//   IsRequired: boolean,
//   Name: string,
//   StartsWith: string,
//   ValidationID: string,
//   ValueType: number,
// };
//
// type Value = {
//    $type: string,
//    Angle: number,
//    Bounds: string,
//    Confidence: number,
//    Id: string,
//    ParentId: string,
//    SegmentationType: number,
//    Text: string,
//    Type: number,
// }
//
// type ValidationIssue = {
//    $type: string,
//    issueCode: number,
// }
//
// type Field = {
//     Field: FieldDef,
//     Id: string,
//     ValidationIssue: ValidationIssue,
//     Value: Value,
// }
//
// type TableDef = {
//     Id: string,
//     Name: string,
//     isDeleted: boolean,
//     Columns: FieldDef[],
// }
//
// type Row = {
//     Fields: Field[],
// }
//
// type Table = {
//     Field: Field,
//     Headers: Field[],
//     Rows: Row[],
//     TableDef: TableDef,
// }
//
// type PreviewData = {
//     tables: Table[],
//     fields: Field[],
// }

/**
 * Transforms the bot data we get from the back end into a flatteded version where we also add important view model
 * properties to objects.
 * @param {Object} param
 * @returns {{url: string, visionBotId: string, documentHeight: Number, documentWidth: Number, layouts: Object[], sirFields: Object[], fields: (Object[]), tables: Object[], columns: Object[], activeFieldId: String, language: String}}
 */
export const transformBotData = ({
    url,
    visionBotData: {
        Id: id,
        Layouts: layouts,
        Language: language,
        DataModel: {Fields: fields, Tables: tables},
    },
    validationData: validatorData,
}) => {
    const layout = layouts[0];
    // because we dont wanna display deleted tables (backend never deletes fieldDefs, only creates a new
    // one and marks the old one as IsDeleted)
    const nonDeletedTables = tables.filter(({IsDeleted, Columns}) => !IsDeleted && Columns.length);

    const pageRange = {start: 0, end: layout.DocProperties.PageProperties[0].Height};
    const sirFields = layout.SirFields.Fields;
    const selectedSirFields = sirFields.filter((field) => {
        const [, y] = field.Bounds.split(', ');

        return y >= pageRange.start && y < pageRange.end;
    });


    return {
        url,
        visionBotId: id,
        documentHeight: layout.DocProperties.PageProperties[0].Height,
        documentWidth: layout.DocProperties.PageProperties[0].Width,
        docProperties: layout.DocProperties,
        layouts: [{Fields: layout.Fields, Tables: layout.Tables, SirFields: selectedSirFields, DocProperties: layout.DocProperties, Id: layout.Id}],
        sirFields: selectedSirFields,
        allSirFields: layout.SirFields.Fields,
        fields: fields.map(extendField, {validatorData}),
        tables: nonDeletedTables.map(extendTable),
        columns: extendColumnsAndFlatten(nonDeletedTables, validatorData),
        activeFieldId: findActiveFieldId(fields, nonDeletedTables),
        language,
        // added these so we can compare against them later when we save bot...
        originalTables: tables,
        originalFields: fields,
    };
};


export const transformPreviewField = ({Field, Id, Value, ValidationIssue}) => ({
    Field: {...Field, isActive: false},
    Id,
    Value: Value.Field,
    ValidationIssue,
});

export const transformPreviewRows = (previewRows) => {
    return previewRows.$values.map((row) => ({Fields: row.Fields.$values.map(({Field, Id, ValidationIssue, Value}) => {
        return {
            Field,
            Id,
            ValidationIssue,
            Value: Value.Field,
        };
    })}));
};

export const transformPreviewTableDef = ({Columns, Id, isDeleted, Name}) => {
    return {
        Id,
        isDeleted,
        Name,
        Columns: Columns.$values,
    };
};

export const VALIDATE_TABLE_PROPERTIES = {
    FIELD: 'Field',
    HEADERS: 'Headers',
    ROWS: 'Rows',
    TABLE_DEF: 'TableDef',
};

export const transformPreviewTables = {
    [VALIDATE_TABLE_PROPERTIES.TABLE_DEF]: transformPreviewTableDef,
    [VALIDATE_TABLE_PROPERTIES.ROWS]: transformPreviewRows,
    [VALIDATE_TABLE_PROPERTIES.FIELD]: (field) => field,
    [VALIDATE_TABLE_PROPERTIES.HEADERS]: (headers) => headers.$values,
};

export const transformPreviewData = (previewData) => {
    const {FieldDataRecord, TableDataRecord} = previewData.data.Data;
    const fields = FieldDataRecord.Fields.$values.map(transformPreviewField);
    const tables = TableDataRecord.$values.map((table) => {
        return Object.keys(VALIDATE_TABLE_PROPERTIES).reduce((accumulator, tableSection) => {
            // accessor would be equal to a string such as "Headers", or "Columns" (any of the four properties in a table we want to flatten)
            const accessor = VALIDATE_TABLE_PROPERTIES[tableSection];

            // says for example: set the "Headers" property as the value returned by transformPreviewTables.Headers(table.Headers)
            // or in other words {...acc, Headers: transformPreviewTables.Headers(table.Headers)}
            return {...accumulator, [accessor]: transformPreviewTables[accessor](table[accessor])};
        }, {});
    }).filter(({Headers}) => Headers.length);

    return {fields: fields || [], tables: tables || []};
};

/**
 * Finds the sir box that should be considered active. The default is based on whether there are tables and/or fields.
 * @param fields
 * @param tables
 * @returns {String}
 */
export const findActiveFieldId = (fields, tables) => (
    fields.length ?
        // if theres a field, lets assign that Id to be the first field we are editing by default
        fields[0].Id :
        // if theres no fields, lets see if theres any tables
        tables.length ?
            // if there is a table, lets use the first column as the default editable field, otherwise theres
            // no editable field
            tables[0].Columns[0].Id : ''
);

/**
 * Extends column with specific view values needed for the Designer feature
 * @param {Object} column
 * @param {String} tableId
 * @returns {...Object, DisplayValue: String, Label: String, mappedSirLabel: String, mappedSirDisplayValue: String, tableId: String}
 */
export const extendColumn = (column, tableId = null, validatorData = []) => {
    const List = getListValidationValue(validatorData, column.Id);
    return {
        ...column,
        DisplayValue: '',
        Label: '',
        mappedSirLabel: null,
        mappedSirDisplayValue: null,
        drawnDisplayValue: false,
        drawnLabel: false,
        tableId,
        List,
    };
};

/**
 * return string value for list validation
 * @param {Array} validationData validation data from state
 * @param {*} id field id
 * @returns {string}
 */
export function getListValidationValue(validationData, id) {
    if (!validationData) return undefined;
    const fieldValidation = validationData.find((validation) => validation.ID === id);
    const List = fieldValidation && fieldValidation.ValidatorData.StaticListItems.reduce((acc, item) => {
        if (!item) return acc;
        return acc.concat(`${item},`);
    }, '');
    return List;
}
/**
 * Extends field with specific view values needed for the Designer feature
 * @param {Object} field
 * @returns {...Object, DisplayValue: String, Label: String, mappedSirLabel: String, mappedSirDisplayValue: String}
 */
export function extendField(field) {
    const List = getListValidationValue(this.validatorData, field.Id); //eslint-disable-line

    return {
        ...field,
        DisplayValue: '',
        Label: '',
        mappedSirLabel: null,
        mappedSirDisplayValue: null,
        drawnDisplayValue: false,
        drawnLabel: false,
        IsLabelOptional: false,
        List,
    };
}

/**
 * Extends table with specific view value needed for the Designer feature
 * @param {Object} field
 * @returns {...Object, Column: []}
 */
export const extendTable = (table) => ({...table, Columns: [], refColumn: null, mappedSirFooter: null, Footer: '', ValueType: 4});

/**
 *
 * @param {Object[]} tables
 * @returns {Object[]}
 */
export const extendColumnsAndFlatten = (tables, validatorData) => flatten(tables.map((table) => table.Columns.map((column) => extendColumn(column, table.Id, validatorData))));

export const getPageData = (layout, pageNumber, existingPageData) => {
    const {PageProperties: pages} = layout.DocProperties;
    const {Fields: sirFields} = layout.SirFields;

    const pagesWithSirFields = pages.reduce((acc, page, i) => {
        const startPx = i === 0 ? 0 : acc[acc.length - 1].endPx;
        const startIndex = existingPageData ? existingPageData[i].startIndex : i === 0 ? 0 : acc[acc.length - 1].endIndex;
        const endPx = startPx + page.Height;

        const relevantFields = sirFields.filter((field) => {
            const yVal = Number(field.Bounds.split(', ')[1]);

            return yVal >= startPx && yVal < endPx;
        }).map((field) => {
            const [x, y, width, height] = field.Bounds.split(', ');
            const newY = Number(y) - startPx;

            return {
                ...field,
                Bounds: `${x}, ${newY}, ${width}, ${height}`,
            };
        });
        const endIndex = existingPageData ? existingPageData[i].endIndex : relevantFields.length + startIndex;

        return acc.concat({
            ...page,
            startPx,
            endPx,
            sirFields: pageNumber !== i + 1 ? [] : relevantFields,
            startIndex,
            endIndex,
        });
    }, []);

    return pagesWithSirFields;
};
