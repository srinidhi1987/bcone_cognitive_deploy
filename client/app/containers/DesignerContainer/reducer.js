/*
 *
 * DesignerContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
    CLEAR_ALL_DRAWN_FIELDS,
    CLEAR_DRAWN_AREAS,
    CLEAR_DESIGNER_STORE,
    DEFAULT_ACTION,
    EXTRACT_SIR_FIELD_TEXT,
    FETCH_ANNOTATION_CLASSES_SUCCESS,
    FIELD_PROPERTY_NAMES,
    HIDE_ALL_CONFIRMATION_BOX_VISIBILITY,
    MAP_REF_COLUMN,
    MAP_SIR_FIELD,
    NEXT_CONFIRMATION_BOX_VISIBILITY,
    POPULATE_BOT_DATA,
    POPULATE_BOT_LABELS,
    POPULATE_LANGUAGE_ALIASES,
    POPULATE_PREVIEW_DATA,
    POPULATE_PREVIEW_DOCUMENTS,
    POPULATE_SIR_FIELDS,
    PUT_PROJECT_NAME_IN_STORE,
    SET_UPLOAD_FILE_LOADING_STATE,
    UPLOAD_FILE_TO_DESIGNER_SUCCESS,
    SAVE_ALL_SIR_FIELDS,
    SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY,
    SAVE_PROJECT_CATEGORIES,
    SET_ACTIVE_FIELD,
    SET_ACTIVE_FIELD_OPTION,
    SET_ACTIVE_PREVIEW_FIELD,
    SET_ANNOTATOR_MODE,
    SET_BOT_PAGE_DATA,
    SET_DOC_PAGE_META,
    SET_CURRENT_PREVIEW_INDEX,
    SET_CURRENT_ZOOM_SCALE,
    SET_CURRENT_PAGE_NUMBER,
    SET_DESIGNER_VIEW_MODE,
    SET_DOC_PROPERTIES,
    SET_FIELD_VALUE,
    SET_IMAGE_LOADED,
    SET_IS_LOADING_EDIT_DATA,
    SET_IS_WAITING_FOR_PREVIEW_DATA,
    SET_LAYOUT_RATIO,
    SET_PAGE_INPUT_VALUE,
    SET_PREVIEW_FIELD_VALUE,
    SET_EDIT_LOADING_STATE,
    SET_PREVIEW_LOADING_STATE,
    SET_SCALE_AMOUNT,
    SET_URL,
    START_DRAG,
    SET_SAVE_DESIGNER_SPINNER_STATE,
    TOGGLE_FIELD_CHECKBOX,
    UPDATE_DRAWN_AREAS,
    UPDATE_ALL_DRAWN_PREVIEW_AREAS,
} from './constants';
import {FIELD_OPTIONS} from '../../constants/enums';
import {EMPTY_STRING} from '../../constants/common';
import {
    QA_LBL_LABEL,
    QA_LBL_VALUE,
    QA_TXT_LABEL,
    QA_TXT_VALUE,
    QA_LST_TYPE,
} from '../../constants/qaElementIds';
import {ANNOTATION_MODES} from '../../components/AnnotationEngine/constants/enums';

import {
    getFieldType,
    getMatchingAliases,
    isMatchingId,
} from '../../utils/designer';
import {getPageData} from './utilities';
const {MAPPED_SIR_DISPLAY_VALUE, MAPPED_SIR_LABEL, MAPPED_SIR_FOOTER} = FIELD_PROPERTY_NAMES;

export const initialState = fromJS({
    activeFieldOption: FIELD_OPTIONS.LABEL,
    activeFieldId: '1',
    activePreviewId: '1',
    allSirFields: [],
    annotationClasses:[],
    aspectRatio: 1,
    columns: [],
    currentDocumentPage: 1,
    currentZoomScale: 1,
    currentPreviewDocument: 0,
    designerViewMode: 'edit',
    documentHeight: 0,
    documentWidth: 0,
    docProperties: {},
    editLoading: false,
    fields: [],
    fileUploading: false,
    flattenedPreviewFields: {},
    height:0,
    iamgeLoaded: false,
    isLoadingEditData: false,
    isWaitingForPreviewData: false,
    mode: ANNOTATION_MODES.FREE,
    keyValues: [],
    layoutRatio: 1,
    layouts :[],
    metaData: {},
    drawnAreas:[],
    originalFields: [],
    originalTables: [],
    originalPreviewData: {fields: [], tables: []},
    pageData: [],
    previewDocuments: [],
    pageInputValue: '',
    previewLoading: false,
    projectName: '',
    scaleAmount: 0.25,
    sirFields: [],
    tables: [],
    url: '',
    visionBotId: null,
    width: 0,
    wizardElementIds: {
        QA_LBL_LABEL,
        QA_LBL_VALUE,
        QA_TXT_LABEL,
        QA_TXT_VALUE,
        QA_LST_TYPE,
    },
    projectCategories: [],
    saveAndCloseConfirmationBoxVisibility: false,
    nextConfirmationBoxVisibility: false,
    clickSource: null,
});

function annotatorContainerReducer(state = initialState, action) {
    switch (action.type) {
        case CLEAR_DRAWN_AREAS:
            return state.set('drawnAreas', state.get('drawnAreas').filter((area) => {
                const designerData = area.get('designerData');

                return designerData.get('activeFieldId') !== action.activeField.Id || (designerData.get('activeFieldId') === action.activeField.Id && designerData.get('activeFieldOption') !== state.get('activeFieldOption'));
            }));
        case DEFAULT_ACTION:
            return state;
        case UPLOAD_FILE_TO_DESIGNER_SUCCESS: {
            return state.set('url', action.url)
                .set('keyValues', action.keyValues)
                .set('metaData', action.metadata);
        }
        case SET_UPLOAD_FILE_LOADING_STATE:
            return state.set('fileUploading', action.fileUploading);
        case SET_PREVIEW_LOADING_STATE:
            return state.set('previewLoading', action.isLoading);
        case SET_EDIT_LOADING_STATE:
            return state.set('editLoading', action.isLoading);
        case EXTRACT_SIR_FIELD_TEXT: {
            const {activeField, area} = action;
            const {areaType, Id: fieldId, fieldOption} = activeField;

            const type = getFieldType(areaType);

            return state.update(type, (fields) => {
                return fields.map((field) => {
                    if (!isMatchingId(fieldId, field.get('Id'))) {
                        return field;
                    }

                    return field.update(fieldOption || state.get('activeFieldOption'), () => area.textContent);
                });
            });
        }
        case FETCH_ANNOTATION_CLASSES_SUCCESS: {
            return state.set('annotationClasses', action.payload);
        }
        case MAP_SIR_FIELD: {
            const {areaType, Id: fieldId, fieldOption} = action.activeField;
            const area = action.area;

            const selectedFieldOption = fieldOption || state.get('activeFieldOption');

            const type = getFieldType(areaType);
            const changedIndex = state.get('drawnAreas').findIndex((area) => {
                const designerData = area.get('designerData');

                return designerData.get('activeFieldOption') === FIELD_OPTIONS.DISPLAY_VALUE && designerData.get('activeFieldId') === fieldId;
            });

            return state.update(type, (fields) => {
                return fields.map((field) => {
                    if (!isMatchingId(fieldId, field.get('Id'))) return field;

                    switch (selectedFieldOption) {
                        case 'Label':
                            return field.set(MAPPED_SIR_LABEL, area.index).set(MAPPED_SIR_DISPLAY_VALUE, null).set('DisplayValue', '').set('drawnDisplayValue', false).set('drawnLabel', false);
                        case 'DisplayValue':
                            return field.set(MAPPED_SIR_DISPLAY_VALUE, area.index).set('drawnDisplayValue', false).set('IsValueBoundAuto', action.activeField.isAuto);
                        case 'Footer':
                            return field.set(MAPPED_SIR_FOOTER, area.index);
                        default:
                            return field;
                    }
                });
            })
            .update('drawnAreas', (drawnAreas) => changedIndex !== -1 ? drawnAreas.delete(changedIndex) : drawnAreas);
        }
        case MAP_REF_COLUMN: {
            return state.update('tables', (tables) => {
                return tables.map((table) => {
                    if (table.get('Id') !== action.Id) {
                        return table;
                    }

                    return table.set('refColumn', action.refColumn);
                });
            });
        }

        case SAVE_PROJECT_CATEGORIES:
            return state.set('projectCategories', fromJS(action.categories.filter((category) => category.id && category.id !== '-1')));
        case PUT_PROJECT_NAME_IN_STORE:
            return state.set('projectName', action.projectName).set('confidenceThreshold', action.confidenceThreshold);
        case POPULATE_BOT_DATA:
            /**
             * magical, but not in the bad way. sets the following properties on the state tree:
             * url, visionBotId, documentHeight, documentWidth, layouts, sirFields, fields, tables,
             * columns, activeFieldId, language
             */
            return state.merge(action.botData);

        case POPULATE_PREVIEW_DATA:
            return state.set('originalPreviewData', fromJS(action.previewData));
        case POPULATE_PREVIEW_DOCUMENTS:
            return state.set('previewDocuments', fromJS(action.previewDocuments));
        case POPULATE_LANGUAGE_ALIASES: {
            return state.set('languageAliases', fromJS(action.payload.aliases));
        }
        case CLEAR_DESIGNER_STORE:
            return initialState;
        case SET_ACTIVE_FIELD: {
            const activeFieldOption = action.isTableSettings ? FIELD_OPTIONS.FOOTER : initialState.get('activeFieldOption');

            return state.set('activeFieldId', action.id)
                        .set('activeFieldOption', activeFieldOption);
        }
        case TOGGLE_FIELD_CHECKBOX: {
            const {fieldOption} = action;
            const {Id: activeId, areaType} = action.activeField;
            // type would be "Column" or "Field"
            const type = getFieldType(areaType);

            // update the correct part of the state (columns or fields)
            return state.update(type, (areaClass) => {
                // area class here would be either "Fields" array or "Columns" array
                return areaClass.map((field) => {
                    // field Option would be in this case "IsRequired" or "Validation"
                    // find the fieldOption with the matching ID of the activeFieldOption, toggle it
                    return field.update(fieldOption, (optionValue) => !isMatchingId(field.get('Id'), activeId) ? optionValue : !optionValue);
                });
            });
        }
        case SET_FIELD_VALUE: {
            const {activeField, fieldName, fieldValue} = action;
            const {Id: activeId, areaType} = activeField;
            // we add this area type in the selector, so that we know which part of the state to edit
            const type = getFieldType(areaType);
            // type can be a "Field" or "column"
            return state.update(type, (areaClass) => {
                // areaClass will be either state.Fields or state.Columns
                return areaClass.map((area) => {
                    // if its not the matching Id, dont edit it
                    if (!isMatchingId(area.get('Id'), activeId)) {
                        return area;
                    }
                    // if it is the field we are editing (mathcing id), go ahead and edit the field Name
                    // fieldName could be for example "Label" or "Display Value"
                    return area.update(fieldName, () => fieldValue);
                });
            });
        }

        case POPULATE_BOT_LABELS: {
            // once we get aliases from the backend, assign them to the appropriate column or field
            return state.update('columns', (columns) => {
                return columns.map((field) => field.update('aliases', () => {
                    return fromJS(getMatchingAliases(action.columns, field.get('Id')));
                }));
            }).update('fields', (fields) => {
                return fields.map((field) => field.update('aliases', () => {
                    return fromJS(getMatchingAliases(action.fields, field.get('Id')));
                }));
            });
        }
        case SET_ACTIVE_FIELD_OPTION: {
            // a field option would be an editable property such as "Value", "DisplayValue", "IsRequired"
            // when we change the activeField option we want to switch the mode back to "free" mode
            return state.set('mode', action.activeFieldOption === state.get('activeFieldOption') ? state.get('mode') : initialState.get('mode'))
                        // sets the field option you are mapping
                        .set('activeFieldOption', action.activeFieldOption);
        }
        case SET_LAYOUT_RATIO: {
            // exposes the layoutRatio used in SVG renderer so that we can calculate
            // intersecting SIR boxes on the top level and not in the annotationEngine
            return state.set('layoutRatio', action.ratio);
        }
        case START_DRAG: {
            const {areaType, Id} = action.activeField;
            const activeFieldOption = state.get('activeFieldOption') === FIELD_OPTIONS.DISPLAY_VALUE ? MAPPED_SIR_DISPLAY_VALUE : MAPPED_SIR_LABEL;

            const changedIndex = state.get('drawnAreas').findIndex((area) => {
                const designerData = area.get('designerData');

                return designerData.get('activeFieldOption') === FIELD_OPTIONS.DISPLAY_VALUE && designerData.get('activeFieldId') === Id;
            });

            return state.update('drawnAreas', (drawnAreas) => changedIndex !== -1 ? drawnAreas.delete(changedIndex) : drawnAreas).update(getFieldType(areaType), (areaClass) => {
                return areaClass.map((area) => {
                    if (!isMatchingId(area.get('Id'), Id)) return area;
                    // when we start drawing a new area, unmap any selected SIR fields we had mapped to
                    // that field or column fieldOption
                    return area.set(activeFieldOption, null)
                        .set('DisplayValue', EMPTY_STRING)
                        .set('drawnDisplayValue', false)
                        .set(MAPPED_SIR_DISPLAY_VALUE, null)
                        .set('IsValueBoundAuto', false);
                });
            });
        }
        case SET_ANNOTATOR_MODE: {
            // when we click on the draw icon, if we are already editing that fieldOption, toggle the mode... otherwise set the mode to "draw"
            return state.set('mode', state.get('mode') === action.mode && state.get('activeFieldOption') === action.fieldOption ? ANNOTATION_MODES.FREE : action.mode);
        }
        case SET_IMAGE_LOADED: {
            const pageNumber = state.get('currentDocumentPage');
            const docProps = state.get('docProperties').toJS();
            const currentPage = docProps.PageProperties[pageNumber - 1];
            const {Height: height, Width: width} = currentPage;

            return state.set('imageLoaded', action.imageLoaded)
                        .set('previewLoading', false)
                        .set('editLoading', false)
                        .set('documentHeight', height)
                        .set('documentWidth', width);
        }
        // moving the drawing logic from annotationEngine to reducer
        case UPDATE_DRAWN_AREAS: {
            if (!action.drawnAreas.length) {
                return state;
            }

            // if it came from the backend, we want to just create the drawn area as the last one in the array
            // since we dont want to have to do a bunch of state setup for activeFieldOption and activeFieldId
            const changedIndex = action.isFromBackend ? -1 : state.get('drawnAreas').findIndex((area) => {
                const designerData = area.get('designerData');

                return designerData.get('activeFieldOption') === state.get('activeFieldOption') && designerData.get('activeFieldId') === state.get('activeFieldId');
            });

            const areaIndex = changedIndex > -1 ? changedIndex : state.get('drawnAreas').size;
            const areaToDraw = action.drawnAreas[action.drawnAreas.length - 1];
            const adjustedY = areaToDraw.designerData.isFromClick ?
                areaToDraw.y :
                state.get('pageData').toJS()[state.get('currentDocumentPage') - 1].startPx + areaToDraw.y;
            const adjustedYAreaToDraw = {
                ...areaToDraw,
                y: adjustedY,
            };

            return state.update('drawnAreas', (drawnAreas) => drawnAreas.set(areaIndex, fromJS(adjustedYAreaToDraw)));
        }
        case UPDATE_ALL_DRAWN_PREVIEW_AREAS: {
            return state.set('drawnAreas', fromJS(action.drawnPreviewAreas));
        }
        case SET_SCALE_AMOUNT:
            return state.set('scaleAmount', action.scaleAmount);
        case SET_CURRENT_ZOOM_SCALE:
            return state.set('currentZoomScale', action.currentZoomScale);
        case SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY:
            return state.set('saveAndCloseConfirmationBoxVisibility', action.show);
        case NEXT_CONFIRMATION_BOX_VISIBILITY:
            return state.set('nextConfirmationBoxVisibility', action.show);
        case HIDE_ALL_CONFIRMATION_BOX_VISIBILITY:
            return state.set('saveAndCloseConfirmationBoxVisibility', false)
                .set('nextConfirmationBoxVisibility', false);
        case SET_ACTIVE_PREVIEW_FIELD:
            return state.set('activePreviewId', String(action.id));
        case SET_PREVIEW_FIELD_VALUE:
            return state.update('flattenedPreviewFields', (fields) => {
                const fieldsJS = fields.toJS();

                const activeField = fieldsJS[action.activeField.Id];
                const modifiedActiveField = {...activeField, Value: {...activeField.Value, Text: action.value}};

                return fields.set(action.activeField.Id, fromJS(modifiedActiveField));
            });
        case SET_DESIGNER_VIEW_MODE:
            return state.set('designerViewMode', action.mode);
        case SET_CURRENT_PREVIEW_INDEX:
            return state.set('currentPreviewDocument', action.newIndex);
        case SET_URL:
            return state.set('url', action.url);
        case SET_IS_WAITING_FOR_PREVIEW_DATA:
            return state.set('isWaitingForPreviewData', action.isWaiting);
        case CLEAR_ALL_DRAWN_FIELDS:
            return state.set('drawnAreas', fromJS([]));
        case SET_BOT_PAGE_DATA:
            return state
                .set('pageData', fromJS(action.pageData));
        case SET_CURRENT_PAGE_NUMBER: {
            const pageNumber = action.pageNumber === '' ? 1 : Math.min(Math.max(Number(action.pageNumber), 1), state.get('pageData').toJS().length);
            const shouldReload = pageNumber === state.get('currentDocumentPage') ? false : true;

            return state
                .set('imageLoaded', !shouldReload)
                .set(`${state.get('designerViewMode')}Loading`, shouldReload)
                .set('currentDocumentPage', pageNumber)
                .set('pageInputValue', pageNumber);
        }
        case SET_PAGE_INPUT_VALUE:
            return state.set('pageInputValue', action.pageNumber === '' ? '' : action.pageNumber);
        case POPULATE_SIR_FIELDS: {
            const docProperties = state.get('docProperties').toJS();
            const existingPageData = state.get('pageData').toJS();

            const pagesInfo = getPageData({DocProperties: docProperties, SirFields: {Fields: action.sirFields}}, state.get('currentDocumentPage'), existingPageData);

            return state.set('pageData', fromJS(pagesInfo));
        }
        case SET_SAVE_DESIGNER_SPINNER_STATE:
            return state.set('isSavingEditData', action.shouldShowSpinner);
        case SET_DOC_PAGE_META:
            return state.set('documentHeight', action.height).set('documentWidth', action.width);
        case SET_DOC_PROPERTIES:
            return state.set('docProperties', fromJS(action.docProperties));
        case SET_IS_LOADING_EDIT_DATA:
            return state.set('isLoadingEditData', action.isLoading);
        case SAVE_ALL_SIR_FIELDS:
            return state.set('allSirFields', fromJS(action.allSirFields));
        default:
            return state;
    }
}

export default annotatorContainerReducer;
