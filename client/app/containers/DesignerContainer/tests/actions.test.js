import expect from 'expect';
import {
    clearDesignerStore,
    defaultAction,
    hideAllConfirmationBoxVisibility,
    nextConfirmationBoxVisibility,
    setActiveField,
    toggleFieldCheckbox,
    setFieldValue,
    populateBotData,
    navigateBots,
    saveAndCloseConfirmationBoxVisibility,
    saveBot,
} from '../actions';
import {
    CLEAR_DESIGNER_STORE,
    DEFAULT_ACTION,
    HIDE_ALL_CONFIRMATION_BOX_VISIBILITY,
    NAVIGATE_BOTS,
    NEXT_CONFIRMATION_BOX_VISIBILITY,
    POPULATE_BOT_DATA,
    SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY,
    SAVE_BOT,
    SET_ACTIVE_FIELD,
    SET_FIELD_VALUE,
    TOGGLE_FIELD_CHECKBOX,
} from '../constants';
import {
    transformBotData,
} from '../utilities';
import {visionBotData1} from '../../../../fixtures/visionBotData';
import {NavigationDirections} from '../../../constants/enums';
import {SUCCESS_DESTINATION} from '../constants';

describe('AnnotatorContainer actions', () => {
    const params = {id:'project-id', botid: '1'};

    describe('Default Action', () => {
        it('has a type of DEFAULT_ACTION', () => {
            const expected = {
                type: DEFAULT_ACTION,
            };

            expect(defaultAction()).toEqual(expected);
        });

        it('has a type of DEFAULT_ACTION', () => {
            const expected = {
                type: DEFAULT_ACTION,
            };

            expect(defaultAction()).toEqual(expected);
        });

        it('has a type of CLEAR_DESIGNER_STORE', () => {
            const expected = {
                type: CLEAR_DESIGNER_STORE,
            };

            expect(clearDesignerStore()).toEqual(expected);
        });

        it('has a type of SET_ACTIVE_FIELD', () => {
            const id = 'banana';
            const isTableSettings = true;

            const expected = {
                type: SET_ACTIVE_FIELD,
                id,
                isTableSettings,
            };

            expect(setActiveField(id, isTableSettings)).toEqual(expected);
        });

        it('has a type of TOGGLE_FIELD_CHECKBOX', () => {
            const activeField = {Id: '3', isActive: true, IsRequired: false};
            const fieldOption = 'IsRequired';

            const expected = {
                type: TOGGLE_FIELD_CHECKBOX,
                activeField,
                fieldOption,
            };

            expect(toggleFieldCheckbox(activeField, fieldOption)).toEqual(expected);
        });

        it('has a type of SET_FIELD_VALUE', () => {
            const activeField = {Id: '3', isActive: true, IsRequired: false};
            const fieldName = 'Label';
            const fieldValue = 'accordian';

            const expected = {
                type: SET_FIELD_VALUE,
                activeField,
                fieldName,
                fieldValue,
            };

            expect(setFieldValue(activeField, fieldName, fieldValue)).toEqual(expected);
        });
    });
    describe('action populateBotData', () => {
        it('will transform the payload from the backend into a structure we can easily merge into our reducer', () => {
            // given
            // when
            // then
            expect(populateBotData(visionBotData1)).toEqual({type: POPULATE_BOT_DATA, botData: transformBotData(visionBotData1)});
        });
    });

    describe('action navigateBots', () => {
        it('will dispatch action to nagivate bot', () => {
            // given
            const expectedResult = {
                type: NAVIGATE_BOTS,
                categories:[],
                currentBotIndex:1,
                direction: NavigationDirections.NEXT,
                params,
            };
            // when
            // then
            expect(navigateBots([], 1, NavigationDirections.NEXT, {id:'project-id', botid: '1'})).toEqual(expectedResult);
        });
    });

    describe('action saveBot', () => {
        it('will dispatch action for saving bot', () => {
            // given
            const expectedResult = {
                type: SAVE_BOT,
                successDestination: SUCCESS_DESTINATION.NEXT,
                sendToProduction: true,
                params,
            };
            // when
            // then
            expect(saveBot(params, SUCCESS_DESTINATION.NEXT, true)).toEqual(expectedResult);
        });
    });

    describe('action saveAndCloseConfirmationBoxVisibility', () => {
        it('will dispatch action for making save modal visible', () => {
            // given
            const expectedResult = {
                type: SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY,
                show: true,
            };
            // when
            // then
            expect(saveAndCloseConfirmationBoxVisibility(true)).toEqual(expectedResult);
        });
    });

    describe('action nextConfirmationBoxVisibility', () => {
        it('will dispatch action for making save modal visible', () => {
            // given
            const expectedResult = {
                type: NEXT_CONFIRMATION_BOX_VISIBILITY,
                show: true,
            };
            // when
            // then
            expect(nextConfirmationBoxVisibility(true)).toEqual(expectedResult);
        });
    });

    describe('action hideAllConfirmationBoxVisibility', () => {
        it('will dispatch action for making save modal visible', () => {
            // given
            const expectedResult = {
                type: HIDE_ALL_CONFIRMATION_BOX_VISIBILITY,
            };
            // when
            // then
            expect(hideAllConfirmationBoxVisibility()).toEqual(expectedResult);
        });
    });

});
