/**
 * AnnotatorContainer sagas
 */
import {call, put} from 'redux-saga/effects';
import {
    createBotAndTrainFlow,
    downloadContent,
    exportCSVFlow,
    navigateDocumentFlow,
    navigateToBotFlow,
    redirectIfFeatureTurnedOffFlow,
    saveBotFlow,
    transformPreviewResult,
} from '../sagas';
import {
    clearAllDrawnFields,
    mapTable,
    populateBotData,
    populatePreviewDocuments,
    putProjectMetadataInStore,
    selectActiveFieldThenCreateMapping,
    setActiveField,
    setActiveFieldOption,
    setBotPageData,
    setCurrentPreviewDocument,
    setCurrentPageNumber,
    setDocPageMeta,
    setDocProperties,
    setIsWaitingForPreviewData,
    setPreviewLoadingState,
    saveAllSirFields,
    saveBot,
    setEditSaveSpinnerState,
    hideAllConfirmationBoxVisibility,
    navigateBots,
    populatePreviewData,
    populateSirFields,
    updateAllDrawnPreviewAreas,
} from '../actions';
import {
    projectsBackEnd,
    projectCategoriesBackEnd,
    botsBackEnd,
    designerBackEnd,
} from '../../../resources';
import {browserHistory} from 'react-router';
import {
    visionBotData1,
    fields1,
} from '../../../../fixtures/visionBotData';
import {
    projectCategory1,
    projectCategory2,
} from '../../../../fixtures/designer';
import {
    findMappedLabels,
    findMappedTables,
    findMappedValues,
    findNonStandardPreviewBoxes,
} from '../../../utils/designer';
import {
    getPageData,
    transformPreviewData,
} from '../utilities';

import {
    previewData,
    previewDocs,
    previewLayoutData,
} from '../../../../fixtures/preview';
import {
    NavigationDirections,
    FIELD_OPTIONS,
} from '../../../constants/enums';
import {SUCCESS_DESTINATION} from '../constants';
import {setProjectVisionBotStateFromServer} from '../../BotsContainer/sagas';
import routeHelper from '../../../utils/routeHelper';

describe('defaultSaga Saga', () => {
    it('Expect to have unit tests specified', () => {
        expect(true).toEqual(true);
    });
});

describe('redirect if designer not disabled', () => {
    const saga = redirectIfFeatureTurnedOffFlow();

    it('should redirect the user to app root', () => {
        const result = saga.next();

        expect(result.value).toEqual(call(browserHistory.push, routeHelper('/')));
    });
});

describe('navigateToBotFlow', () => {
    it('should navigate to next bot', () => {
        const navigateNextAction = {
            direction: NavigationDirections.NEXT,
            categories: [
                projectCategory1,
                projectCategory2,
            ],
            currentBotIndex: 0,
            params: {id: '123'},
        };

        const saga = navigateToBotFlow(navigateNextAction);
        const result = saga.next(NavigationDirections.NEXT);
        const expectedUrl = routeHelper(`/learning-instances/${navigateNextAction.params.id}/bots/${projectCategory2.id}/edit?refresh=true`);

        expect(result.value).toEqual(call(browserHistory.push, expectedUrl));
    });

    it('should navigate to previous bot', () => {
        const navigatePreviousAction = {
            direction: NavigationDirections.PREVIOUS,
            categories: [
                projectCategory1,
                projectCategory2,
            ],
            currentBotIndex: 1,
            params: {id: '123'},

        };

        const saga = navigateToBotFlow(navigatePreviousAction);
        const result = saga.next(NavigationDirections.PREVIOUS);
        const expectedUrl = routeHelper(`/learning-instances/${navigatePreviousAction.params.id}/bots/${projectCategory1.id}/edit`);

        expect(result.value).toEqual(call(browserHistory.push, expectedUrl));
    });
});

describe('createBotAndTrainFlow Saga', () => {
    const actionData = {categoryId: '12', projectId: 'abcd', shouldClearStore: null, username: 'andrew'};
    const saga = createBotAndTrainFlow(actionData);

    const {projectId, categoryId} = actionData;

    it('should clear all drawn fields every time so we dont have duplicates and circles dont get darker', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(clearAllDrawnFields()));
    });

    it('should get projectData', () => {
        const result = saga.next();

        expect(result.value).toEqual(call(projectsBackEnd.get, projectId));
    });

    it('then it should put project name in store if call to project back end is success', () => {
        const result = saga.next({success: true, data: {name: 'gary', confidenceThreshold: 90}});

        expect(result.value).toEqual(put(putProjectMetadataInStore({name: 'gary', confidenceThreshold: 90})));
    });

    it('then it should get bot data', () => {
        const botResult = saga.next();

        expect(botResult.value).toEqual(call(botsBackEnd.get, projectId, categoryId, 'andrew'));
    });

    it('then it should populate the store with the bot data', () => {
        const result = saga.next(visionBotData1);

        expect(result.value).toEqual(put(populateBotData(visionBotData1)));
    });


    const sirFields = visionBotData1.visionBotData.Layouts[0].SirFields.Fields;

    it('should save bot page data', () => {
        const result = saga.next();
        const pagesInfo = getPageData(visionBotData1.visionBotData.Layouts[0], 1);

        expect(result.value).toEqual(put(setBotPageData(pagesInfo)));
    });

    it('then it should automap labels when given suggested labels and a list of sirFields ', () => {
        const mappedLabels = findMappedLabels(visionBotData1, sirFields);

        const result = saga.next();

        expect(result.value).toEqual(mappedLabels.map((field) => put(selectActiveFieldThenCreateMapping(field))));
    });

    it('then it should automap values when given suggested field values and a list of sirFields', () => {
        const mappedValues = findMappedValues(visionBotData1, sirFields);

        const result = saga.next();

        expect(result.value).toEqual(mappedValues.map((field) => put(selectActiveFieldThenCreateMapping(field))));
    });

    it('then it should automap tables when given suggested field values and a list of sirFields', () => {
        const mappedTables = findMappedTables(visionBotData1, sirFields);

        const result = saga.next();

        expect(result.value).toEqual(mappedTables.map((table) => put(mapTable(table))));
    });

    it('should fetch list of preview documents', () => {
        const previewDocumentsResult = saga.next();

        expect(previewDocumentsResult.value).toEqual(call(botsBackEnd.fetchPreviewDocuments, projectId, categoryId));
    });


    it('should populate store with list of preview documetnts', () => {
        const result = saga.next({data: previewDocs}, visionBotData1.visionBotData.Layouts[0]);

        expect(result.value).toEqual(put(populatePreviewDocuments([{fileId: '12'}, ...previewDocs])));
    });

    it('then it should reset the active field option to label', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setActiveFieldOption(FIELD_OPTIONS.LABEL)));
    });

    it('then it should set activeField to first table or column', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setActiveField(fields1[0].Id)));
    });

    it('then it should populate the store with project categories', () => {
        const result = saga.next();

        expect(result.value).toEqual(call(projectCategoriesBackEnd, projectId, {sort: '-index'}));
    });
});

describe('saveBotFlow saga', () => {
    const params = {
        id: 'project-id',
        botid: '1',
    };
    const currentBotIndex = 1;
    const projectCategories = [];
    const visionBotId = 'vision-bot-id';

    const botSaveData = {Id: '123'};

    describe('Save and move to next bot', () => {
        const action = saveBot(params, SUCCESS_DESTINATION.NEXT, false);
        describe('fail save path', () => {
            const saga = saveBotFlow(action);

            saga.next();
            saga.next();
            saga.next();
            saga.next(botSaveData);
            saga.next(currentBotIndex);
            saga.next(projectCategories);

            it('should save visionbot data', () => {
                const result = saga.next(visionBotId);
                expect(result.value).toEqual(call(designerBackEnd.saveBot, botSaveData));
            });
            it('should hide confirmation modal', () => {
                const result = saga.next({success:false, errors:[], data:null});
                expect(result.value).toEqual(put(hideAllConfirmationBoxVisibility()));
            });
        });

        describe('success path', () => {
            const saga = saveBotFlow(action);

            saga.next();
            saga.next();
            saga.next();
            saga.next(botSaveData);
            saga.next(currentBotIndex);
            saga.next(projectCategories);

            it('should save visionbot data', () => {
                const result = saga.next(visionBotId);
                expect(result.value).toEqual(call(designerBackEnd.saveBot, botSaveData));
            });
            it('should hide confirmation modal', () => {
                const result = saga.next({success:true, errors:[], data:null});
                expect(result.value).toEqual(put(setEditSaveSpinnerState(false)));
            });
            it('should redirect to next bot', () => {
                const result = saga.next();
                expect(result.value).toEqual(put(navigateBots(projectCategories, currentBotIndex, NavigationDirections.NEXT, action.params)));
            });
        });
    });

    describe('Save and move to production and move to next bot', () => {
        const action = saveBot(params, SUCCESS_DESTINATION.NEXT, true);
        describe('success path', () => {
            const saga = saveBotFlow(action);

            saga.next();
            saga.next();
            saga.next();
            saga.next(botSaveData);
            saga.next(currentBotIndex);
            saga.next(projectCategories);

            it('should save visionbot data', () => {
                const result = saga.next(visionBotId);
                expect(result.value).toEqual(call(designerBackEnd.saveBot, botSaveData));
            });
            it('should move bot to production', () => {
                const result = saga.next({success:true, errors:[], data:null});
                expect(result.value).toEqual(call(setProjectVisionBotStateFromServer, params.id, params.botid, visionBotId, 'production'));
            });
            it('should hide confirmation modal', () => {
                const result = saga.next({success:true, errors:[], data:null});
                expect(result.value).toEqual(put(setEditSaveSpinnerState(false)));
            });
            it('should redirect to next bot', () => {
                const result = saga.next();
                expect(result.value).toEqual(put(navigateBots(projectCategories, currentBotIndex, NavigationDirections.NEXT, action.params)));
            });
        });
    });

    describe('Save and move to learning instance detail page', () => {
        const action = saveBot(params, SUCCESS_DESTINATION.LI_DETAIL, false);

        describe('success path', () => {
            const saga = saveBotFlow(action);

            saga.next();
            saga.next();
            saga.next();
            saga.next(botSaveData);
            saga.next(currentBotIndex);
            saga.next(projectCategories);

            it('should save visionbot data', () => {
                const result = saga.next(visionBotId);
                expect(result.value).toEqual(call(designerBackEnd.saveBot, botSaveData));
            });

            it('should redirect to learning instance detail page', () => {
                const result = saga.next({success: true});
                expect(result.value).toEqual(call(browserHistory.push, `/iqbot/learning-instances/${params.id}`));
            });
        });
    });
});

describe('saga exportCSVFlow', () => {
    const previewResult = {data: {...previewLayoutData, Data: previewData}};
    const transformedPreviewData = transformPreviewResult(previewResult);
    const exportAction = {
        projectId:'1212',
        categoryId: '1',
        data: transformedPreviewData,
    };
    const saga = exportCSVFlow(exportAction);

    it('should get csv data', () => {
        const {projectId, categoryId, data} = exportAction;
        const result = saga.next();
        expect(result.value).toEqual(call(designerBackEnd.exportToCSV, projectId, categoryId, data));
    });

    it('should download data if result is success', () => {
        const previeousResult = {
            success: true,
            data: '',
            errors: [],
        };
        const result = saga.next(previeousResult);
        expect(result.value).toEqual(call(downloadContent, previeousResult.data));
    });
});

describe(' go to next preview doc', () => {
    const saga = navigateDocumentFlow({currentIndex: 0, projectId: 1, categoryId: 3});
    // select preview docs
    saga.next();
    // select doc properties
    saga.next([{fileId: '123'}]);
    // select vision bot id
    saga.next({PageProperties: [{Height: 123, Width: 200}]});
    // select preview session id
    it('should set preview loading state to true', () => {
        const result = saga.next('345');

        expect(result.value).toEqual(put(setPreviewLoadingState(true)));
    });

    it('should set preview data loading state to true', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setIsWaitingForPreviewData(true)));
    });

    it('should set preview staging document to current index', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setCurrentPreviewDocument(0)));
    });

    it('should clear all drawn areas', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(clearAllDrawnFields()));
    });

    it('should set current page meta data', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setDocPageMeta(123, 200)));
    });

    it('should set current page number to 1', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setCurrentPageNumber(1)));
    });

    it('should fetch preview data', () => {
        const result = saga.next();

        expect(result.value).toEqual(call(botsBackEnd.fetchPreviewDataDocument, 1, 3, '345', '123'));
    });

    const previewResult = {
        data: {
            ...previewLayoutData,
            Data: previewData,
            SirFields: {
                Fields:{
                    $values: [
                        {
                            'Id': 'fb7765de-0363-4ebf-a6be-3482d6d0e544',
                            'ParentId': '604bbc48-1262-472b-a52c-d6fbb5b5aaf2',
                            'Text': 'NuVasive,',
                            'Confidence': 91,
                            'SegmentationType':0,
                            'Type':0,
                            'Bounds':'321, 88, 155, 30',
                            'Angle':0,
                        },
                        {
                            'Id': 'f03469a7-d068-418a-bcbd-270a93ac8da6',
                            'ParentId': '604bbc48-1262-472b-a52c-d6fbb5b5aaf2',
                            'Text': 'Inc.',
                            'Confidence': 96,
                            'SegmentationType': 0,
                            'Type': 0,
                            'Bounds': '489, 88, 53, 25',
                            'Angle': 0,
                        },
                        {
                            'Id': '5cb0db72-4eb6-40fb-848b-7a5d9ef0ad0b',
                            'ParentId': '1d4759b7-066c-4910-978c-ed9665eac834',
                            'Text': 'Invoice',
                            'Confidence': 86,
                            'SegmentationType': 0,
                            'Type': 0,
                            'Bounds': '931, 87, 103, 26',
                            'Angle': 0,
                        },
                    ],
                },
            },
        },
    };
    it('should populate doc properties', () => {
        const result = saga.next(previewResult);

        expect(result.value).toEqual(put(setDocProperties({...previewResult.data.DocumentProperties, PageProperties: previewResult.data.DocumentProperties.PageProperties.$values})));
    });

    it('should populate previewData', () => {
        const result = saga.next();
        const transformedPreviewData = transformPreviewResult(previewResult);

        expect(result.value).toEqual(put(populatePreviewData(transformedPreviewData)));
    });

    it('should populate all drawn preview fields at once', () => {
        const result = saga.next();
        const data = transformPreviewData(previewResult);
        const nonStandardPreviewBoxes = findNonStandardPreviewBoxes(previewResult.data.SirFields.Fields.$values, data);

        expect(result.value).toEqual(put(updateAllDrawnPreviewAreas(nonStandardPreviewBoxes)));
    });

    it('should populate the sir fields selection to proper page data index', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(populateSirFields(previewResult.data.SirFields.Fields.$values)));
    });

    it('should cache all the sir fields so we have them later', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(saveAllSirFields(previewResult.data.SirFields.Fields.$values)));
    });

    it('should turn off preview loading state', () => {
        const result = saga.next();

        expect(result.value).toEqual(put(setIsWaitingForPreviewData(false)));
    });
});
