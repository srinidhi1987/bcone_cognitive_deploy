import selectAnnotatorContainer, {selectAppConfig, selectSirFields} from '../selectors';
import {fromJS} from 'immutable';
import {
    field1,
    field2,
    column1,
    column2,
    column3,
    projectCategory1,
    projectCategory2,
} from '../../../../fixtures/designer';
import {DESIGNER_AREA_TYPES} from '../../../constants/enums';

let appConfig, mockedState, selector;

describe('selectAnnotatorContainerDomain', () => {
    beforeEach(() => {
        // given
        appConfig = {
            encrypted: '43324234324',
            designer: true,
        };

        mockedState = fromJS({
            appContainer: {
                appConfig,
                authToken: 'test-token',
            },
            designerContainer: {
                activePreviewId: '123',
                flattenedPreviewFields: [],
                previewData: {fields: [], tables: []},
                keyValues: [],
                url: '',
                fileUploading: false,
                metaData: {},
                aspectRatio: 1,
                layoutRatio: 1,
                height:0,
                width:0,
                objects:[],
                annotationClasses:[],
                projectCategories: [projectCategory1, projectCategory2],
                projectName: '',
                documentHeight: 0,
                documentWidth: 0,
                columns: [
                    column1, column2, column3,
                ],
                fields: [field1, field2],
                tables: [{Id: '16', metaData: 'some metadata here'}, {Id: '17', metaData: 'some more metadata'}],
                sirFields: [],
                activeFieldId: '2',
                drawnAreas: [],
                visionBotId: '4',
                currentPreviewDocument: 0,
                previewDocuments: [{fileId: 'horse12'}, {fileId: 'light bulbs'}, {fileId: 'iberian peninsula'}],
                designerViewMode: 'edit',
                originalPreviewData: {fields: [], tables: []},
                pageData: [{
                    startPx: 0,
                    endPx: 10,
                    startIndex: 0,
                    sirFields: [
                        {Bounds: '0, 0, 5, 5'},
                        {Bounds: '5, 5, 10, 10'},
                    ],
                }, {
                    startPx: 10,
                    endPx: 20,
                    startIndex: 0,
                    sirFields: [
                        {Bounds: '0, 15, 5, 5'},
                        {Bounds: '5, 20, 10, 10'},
                    ],
                }],
                currentDocumentPage: 2,
            },
        });
        // when
        selector = selectAnnotatorContainer();
    });

    it('should create fields prop with added metadata', () => {
        expect(selector(mockedState).fields).toEqual([
            {...field1, areaType: DESIGNER_AREA_TYPES.FIELD}, {...field2, areaType: DESIGNER_AREA_TYPES.FIELD},
        ]);
    });

    it('should create an activeField prop', () => {
        expect(selector(mockedState).activeField).toEqual({...field2, areaType: DESIGNER_AREA_TYPES.FIELD});
    });

    it('should create a should redirect prop of false if designer is enabled', () => {
        expect(selector(mockedState).shouldRedirect).toEqual(false);
    });

    it('should create a shouldRedirect prop of true if designer is disabled', () => {
        mockedState = mockedState.update('appContainer', (appState) => {
            return appState.set('appConfig', fromJS({encrypted: '23414313', designer: false}));
        });

        expect(selector(mockedState).shouldRedirect).toEqual(true);
    });

    it('should create an appConfig prop with a selector', () => {
        const appConfigSelector = selectAppConfig();

        expect(appConfigSelector(mockedState)).toEqual(appConfig);
    });

    it('should create a select current bot index', () => {
        expect(selector(mockedState).currentBotIndex).toEqual(1);
    });

    it('should not be allowed to navigate to next bot if it is last bot', () => {
        expect(selector(mockedState).botCanNavigate).toEqual({next: false, previous: true});
    });

    it('should not be allowed to navigate to next bot if there are only bots being used left in the queue', () => {
        const categoriesListWithBotBeingUsed = mockedState.get('designerContainer').get('projectCategories').toJS().concat({visionBot: {lockedUserId: 'andrew'}});

        mockedState.update('designerContainer', (designerContainer) => designerContainer.set('projectCategories', fromJS(categoriesListWithBotBeingUsed)));
        expect(selector(mockedState).botCanNavigate).toEqual({next: false, previous: true});
    });

    it('should not be allowd to navigate to previous bot if it is first bot', () => {
        mockedState = mockedState.update('designerContainer', (designerState) => {
            return designerState.set('visionBotId', '1');
        });

        expect(selector(mockedState).botCanNavigate).toEqual({next: true, previous: false});
    });

    it('should contain preview docs navigation info if in preview mode', () => {
        mockedState = mockedState.update('designerContainer', (designerState) => {
            return designerState.set('designerViewMode', 'preview');
        });
        const currentIndex = mockedState.get('designerContainer').get('currentPreviewDocument');
        const docs = mockedState.get('designerContainer').get('previewDocuments').toJS();

        expect(selector(mockedState).botCanNavigate).toEqual({previous: false, next: docs[currentIndex + 1].fileId});
    });

    it('should only give preview and next documents if not on first or last documents', () => {
        mockedState = mockedState.update('designerContainer', (designerState) => {
            return designerState.set('currentPreviewDocument', 1).set('designerViewMode', 'preview');
        });

        const currentIndex = mockedState.get('designerContainer').get('currentPreviewDocument');
        const docs = mockedState.get('designerContainer').get('previewDocuments').toJS();

        expect(selector(mockedState).botCanNavigate).toEqual({previous: docs[currentIndex - 1].fileId, next: docs[currentIndex + 1].fileId});
    });

    it('should create tables prop from selected columns prop', () => {
        const expectedTables = [
            {
                Id: '16',
                metaData: 'some metadata here',
                areaType: DESIGNER_AREA_TYPES.TABLE,
                isMapped: false,
                isActive: false,
                Columns: [
                    {...column1, areaType: DESIGNER_AREA_TYPES.COLUMN}, {...column3, areaType: DESIGNER_AREA_TYPES.COLUMN},
                ],
            },
            {
                Id: '17',
                metaData: 'some more metadata',
                areaType: DESIGNER_AREA_TYPES.TABLE,
                isMapped: false,
                isActive: false,
                Columns: [
                    {...column2, areaType: DESIGNER_AREA_TYPES.COLUMN},
                ],
            },
        ];

        expect(selector(mockedState).tables).toEqual(expectedTables);
    });

    describe('should create sirFields that are adjusted to the current page view', () => {
        const selectSirFieldSelector = selectSirFields();

        it('should subtract the startPx from the y coord of each sir Field on the relevant page', () => {
            expect(selectSirFieldSelector(mockedState)).toEqual([
                {Bounds: '0, 15, 5, 5'},
                {Bounds: '5, 20, 10, 10'},
            ]);
        });
    });

    it('should create an object that tells whether or not a user can navigate to next or previous doc page (last page)', () => {
        expect(selector(mockedState).canNavigateBotPage).toEqual({next: false, previous: true});
    });

    it('should create an object that tells whether or not a user can navigate to next or previous doc page (first page)', () => {
        mockedState = mockedState.update('designerContainer', (designerContainer) => designerContainer.set('currentDocumentPage', 1));

        expect(selector(mockedState).canNavigateBotPage).toEqual({next: true, previous: false});
    });
});
