import expect from 'expect';
import annotatorContainerReducer from '../reducer';
import {fromJS} from 'immutable';
import {initialState} from '../reducer';
import {
    clearDesignerStore,
    populateBotLabels,
    setActiveField,
    setCurrentPageNumber,
    setFieldValue,
    toggleFieldCheckbox,
    populateBotData,
    saveProjectCategories,
    hideAllConfirmationBoxVisibility,
    saveAndCloseConfirmationBoxVisibility,
    nextConfirmationBoxVisibility,
} from '../actions';
import {visionBotData1} from '../../../../fixtures/visionBotData';

import {DESIGNER_AREA_TYPES} from '../../../constants/enums';

import {
    field1,
    field2,
    column1,
    column2,
    fieldAlias1,
    fieldAlias2,
    columnAlias1,
    columnAlias2,
    projectCategory1,
    projectCategory2,
    projectCategory3,
} from '../../../../fixtures/designer';

describe('annotatorContainerReducer', () => {
    it('returns the initial state', () => {
        expect(annotatorContainerReducer(undefined, {})).toEqual(fromJS(initialState));
    });

    describe('when clearDesignerStore is called', () => {
        it('should return the initial state (shallow comparison)', () => {
            expect(annotatorContainerReducer(fromJS({}), clearDesignerStore())).toEqual(initialState);
        });
    });

    describe('when setActiveField is called', () => {
        it('should return the state with the proper activeFieldId', () => {
            const activeId = '3';
            const expected = fromJS({...initialState.toJS(), activeFieldId: activeId});

            expect(annotatorContainerReducer(initialState, setActiveField(activeId))).toEqual(expected);
        });
    });

    describe('when populateBotData is called', () => {
        it('should merge the botData into the state tree, populating many properties of the store', () => {
            const action = populateBotData(visionBotData1);
            const expected = fromJS({
                ...initialState.toJS(),
                ...(action.botData),
            });
            expect(annotatorContainerReducer(initialState, action)).toEqual(expected);
        });
    });

    describe('when toggleFieldCheckbox is called', () => {
        it('should toggle the specified checkbox in the specified field', () => {
            const stateWithFields = fromJS({
                ...initialState.toJS(),
                fields: [field1],
            });

            const activeField = {...field1, areaType: DESIGNER_AREA_TYPES.FIELD};
            const fieldOption = 'IsRequired';

            const expected = fromJS({...stateWithFields.toJS(), fields: [{...field1, IsRequired: !field1.IsRequired}]});

            expect(annotatorContainerReducer(stateWithFields, toggleFieldCheckbox(activeField, fieldOption))).toEqual(expected);
        });

        it('should toggle the specified checkbox in the specified column', () => {
            const stateWithColumns = fromJS({
                ...initialState.toJS(),
                columns: [column1],
            });

            const activeField = {...column1, areaType: DESIGNER_AREA_TYPES.COLUMN};
            const fieldOption = 'ValidationId';

            const expected = fromJS({...stateWithColumns.toJS(), columns: [{...column1, ValidationId: !column1.ValidationField}]});

            expect(annotatorContainerReducer(stateWithColumns, toggleFieldCheckbox(activeField, fieldOption))).toEqual(expected);
        });
    });

    describe('when setFieldValue is called', () => {
        it('should set a field value for a specified field', () => {
            const stateWithFields = fromJS({
                ...initialState.toJS(),
                fields: [
                    field1,
                    field2,
                ],
            });

            const activeField = {...field1, areaType: DESIGNER_AREA_TYPES.FIELD, isActive: true};
            const fieldName = 'Label';
            const fieldValue = 'Mongolian Rhinocerous';

            const expected = fromJS({
                ...stateWithFields.toJS(),
                fields: [
                    {...field1, Label: fieldValue},
                    field2,
                ],
            });

            expect(annotatorContainerReducer(stateWithFields, setFieldValue(activeField, fieldName, fieldValue))).toEqual(expected);
        });
        it('should set a field value for a specified column', () => {
            const stateWithColumns = fromJS({
                ...initialState.toJS(),
                columns: [
                    column1,
                    column2,
                ],
            });

            const activeField = {...column1, areaType: DESIGNER_AREA_TYPES.COLUMN};
            const fieldName = 'DisplayValue';
            const fieldValue = 'bbq sauce';

            const expected = fromJS({
                ...stateWithColumns.toJS(),
                columns: [
                    {...column1, DisplayValue: fieldValue},
                    column2,
                ],
            });

            expect(annotatorContainerReducer(stateWithColumns, setFieldValue(activeField, fieldName, fieldValue))).toEqual(expected);
        });
    });
    describe('when populateBotLabels is called', () => {
        it('should update columns and fields with potential label aliases', () => {
            const fields = [
                fieldAlias1,
                fieldAlias2,
            ];

            const columns = [
                columnAlias1,
                columnAlias2,
            ];

            const stateWithColumnsAndFields = fromJS({
                ...initialState.toJS(),
                fields: [
                    field1,
                    field2,
                ],
                columns: [
                    column1,
                    column2,
                ],
            });

            const expected = fromJS({
                ...initialState.toJS(),
                fields: [
                    {
                        ...field1,
                        ...fieldAlias1,
                    },
                    {
                        ...field2,
                        ...fieldAlias2,
                    },
                ],
                columns: [
                    {
                        ...column1,
                        ...columnAlias1,
                    },
                    {
                        ...column2,
                        ...columnAlias2,
                    },
                ],
            });

            expect(annotatorContainerReducer(stateWithColumnsAndFields, populateBotLabels(columns, fields))).toEqual(expected);
        });
    });

    describe('when projectsCategories is set in store', () => {
        const projectCategories = [projectCategory1, projectCategory2, {id: '-1'}, projectCategory3];
        const expectedState = {...initialState.toJS(), projectCategories: [projectCategory1, projectCategory2, projectCategory3]};

        expect(annotatorContainerReducer(initialState, saveProjectCategories(projectCategories))).toEqual(fromJS(expectedState));
    });

    describe('when user click on save and close', () => {
        const expectedState = {
            ...initialState.toJS(),
            saveAndCloseConfirmationBoxVisibility: true,
        };
        expect(annotatorContainerReducer(initialState, saveAndCloseConfirmationBoxVisibility(true))).toEqual(fromJS(expectedState));
    });

    describe('when user click on next', () => {
        const expectedState = {
            ...initialState.toJS(),
            nextConfirmationBoxVisibility: true,
        };
        expect(annotatorContainerReducer(initialState, nextConfirmationBoxVisibility(true))).toEqual(fromJS(expectedState));
    });

    describe('hide all confirmation modal', () => {
        const expectedState = {
            ...initialState.toJS(),
            nextConfirmationBoxVisibility: false,
            saveAndCloseConfirmationBoxVisibility: false,
        };
        expect(annotatorContainerReducer(initialState, hideAllConfirmationBoxVisibility())).toEqual(fromJS(expectedState));
    });

    describe('set current page number', () => {
        it('should have set the current pages meta data into various property in the store', () => {
            const initialStateWithPageData = initialState.set('pageData', fromJS([{
                Height: 100,
                Width: 300,
                startPx: 0,
                endPx: 10,
                startIndex: 0,
                sirFields: [
                    {Bounds: '0, 0, 5, 5'},
                    {Bounds: '5, 5, 10, 10'},
                ],
            }, {
                Height: 200,
                Width: 500,
                startPx: 10,
                endPx: 20,
                startIndex: 0,
                sirFields: [
                    {Bounds: '0, 15, 5, 5'},
                    {Bounds: '5, 20, 10, 10'},
                ],
            }]));

            const expectedState = {
                ...initialStateWithPageData.toJS(),
                currentDocumentPage: 2,
                pageInputValue: 2,
                imageLoaded: false,
                editLoading: true,
            };

            expect(annotatorContainerReducer(initialStateWithPageData, setCurrentPageNumber(2))).toEqual(fromJS(expectedState));
        });
    });
});
