import {
    extendColumn,
    extendColumnsAndFlatten,
    extendField,
    extendTable,
    findActiveFieldId,
    getPageData,
    transformBotData,
} from '../utilities';
import {visionBotData1, tables2, validationData} from '../../../../fixtures/visionBotData';

describe('DesignerContainer utilities:', () => {
    // given
    const displayDefaultValues = {
        DisplayValue: '',
        Label: '',
        mappedSirLabel: null,
        mappedSirDisplayValue: null,
        drawnLabel: false,
    };

    describe('function extendField:', () => {
        it('return the same object given via param extended with the default values needed for the view', () => {
            // given
            const field = {
                Id: '234234234234',
                otherExampleField: [],
            };

            // when
            // then
            expect(extendField.call({validatorData: validationData}, field)).toEqual({
                ...field,
                ...displayDefaultValues,
                drawnDisplayValue: false,
                IsLabelOptional: false,
                List: undefined,
            });
        });
    });

    describe('function extendColumn:', () => {
        it('return the same object given via param extended with the default values needed for the view (w/ tableId)', () => {
            // given
            const field = {
                Id: '234234234234',
                otherExampleField: [],
            };
            const tableId = 'h34hj334hj34';

            // when
            // then
            expect(extendColumn(field, tableId)).toEqual({
                ...field,
                ...displayDefaultValues,
                drawnDisplayValue: false,
                tableId,
            });
        });
    });

    describe('function extendTable:', () => {
        it('return the same object extended with a property called Columns that is an empty array', () => {
            //given
            const id = '234234234234';

            const table = {
                Id: id,
                couldBeAnythingReally: [],
            };

            // when
            // then
            expect(extendTable(table)).toEqual({
                ...table,
                Columns: [],
                Footer: '',
                Id: id,
                ValueType: 4,
                mappedSirFooter: null,
                refColumn: null,
            });
        });
    });

    describe('function extendColumnsAndFlatten:', () => {
        it('flatten the columns of each table into a single array and extend each column with the tables id', () => {
            // given
            // when
            // then
            const result = extendColumnsAndFlatten(tables2);
            expect(result[0]).toEqual(extendColumn(tables2[0].Columns[0], tables2[0].Id));
            expect(result[1]).toEqual(extendColumn(tables2[0].Columns[1], tables2[0].Id));
            expect(result[2]).toEqual(extendColumn(tables2[1].Columns[0], tables2[1].Id));
            expect(result[3]).toEqual(extendColumn(tables2[1].Columns[1], tables2[1].Id));
        });
    });

    describe('function findActiveFieldId:', () => {
        // given
        const fields = [
            {
                Id: '1',
            },
        ];
        const tables = [
            {
                Columns: [
                    {
                        Id: '2',
                    },
                ],
            },
        ];
        const emptyFields = [];
        const emptyTables = [];

        describe('when there is at least 1 field in the fields array', () => {
            it('should return the id of the 1st field in the array', () => {
                // when
                // then
                expect(findActiveFieldId(fields, tables)).toEqual(fields[0].Id);
            });
        });

        describe('when there are 0 fields in the fields array', () => {
            describe('and at least 1 table in the tables array', () => {
                it('should return the id of the 1st column of the 1st table in the table array', () => {
                    // when
                    // then
                    expect(findActiveFieldId(emptyFields, tables)).toEqual(tables[0].Columns[0].Id);
                });
            });
            describe('and 0 tables in the tables array', () => {
                it('should return an empty string', () => {
                    // when
                    // then
                    expect(findActiveFieldId(emptyFields, emptyTables)).toEqual('');
                });
            });
        });

        describe('function transformVisionBotData:', () => {
            // given
            const visionBotData = visionBotData1.visionBotData;
            const url = visionBotData1.url;
            const layout = visionBotData.Layouts[0];
            const visionBotId = visionBotData.Id;
            const language = visionBotData.Language;
            const tables = visionBotData.DataModel.Tables;
            const fields = visionBotData.DataModel.Fields;

            const documentWidth = layout.DocProperties.Width;
            const documentHeight = layout.DocProperties.Height;

            // when
            const botData = {
                url,
                visionBotId,
                documentHeight,
                documentWidth,
                layouts: [{...layout, SirFields: layout.SirFields.Fields}],
                docProperties: {
                    Height: 200,
                    Width: 300,
                    Id: '12',
                    PageProperties: [
                        {
                            Height: 200,
                            Width: 300,
                        },
                    ],
                },
                sirFields: layout.SirFields.Fields,
                fields: fields.map(extendField, {validatorData: validationData}),
                tables: tables.map(extendTable),
                columns: extendColumnsAndFlatten(tables, validationData),
                activeFieldId: findActiveFieldId(fields, tables),
                language,
                originalTables: tables,
                originalFields: fields,
                allSirFields: [
                    {
                        Bounds: '6, 8, 7, 8',
                        Text: 'value text',
                    },
                    {
                        Bounds: '1, 2, 3, 4',
                        Text: 'label text',
                    },
                    {
                        Bounds: '11, 23, 40, 50',
                        Text: 'TABLE',
                    },
                ],
            };
            // then
            it('return the same object given via param extended with the default values needed for the view', () => {
                expect(transformBotData(visionBotData1)).toEqual(botData);
            });
        });
    });

    describe('getPageData', () => {
        const layout = {
            SirFields: {
                Fields: [
                    {Bounds: '0, 0, 10, 10'},
                    {Bounds: '20, 20, 10, 10'},
                    {Bounds: '120, 120, 10, 10'},
                    {Bounds: '150, 150, 10, 10'},
                ],
            },
            DocProperties: {
                PageProperties: [
                    {
                        Height: 100,
                        Width: 500,
                    },
                    {
                        Height: 100,
                        Width: 500,
                    },
                ],
            },
        };
        const pageNumber = 2;

        expect(getPageData(layout, pageNumber)).toEqual([
            {
                Height: 100,
                Width: 500,
                startPx: 0,
                endPx: 100,
                sirFields: [],
                startIndex: 0,
                endIndex: 2,
            },
            {
                Height: 100,
                Width: 500,
                startPx: 100,
                endPx: 200,
                sirFields: [
                    {Bounds: '120, 20, 10, 10'},
                    {Bounds: '150, 50, 10, 10'},
                ],
                startIndex: 2,
                endIndex: 4,
            },
        ]);
    });
});
