import {shallow} from 'enzyme';
import React from 'react';

import {DesignerContainer} from '../index';
import {
    previewData,
} from '../../../../fixtures/preview';
describe('Container: DesignerContainer', () => {
    describe('when componentWillUnmount is called', () => {
        const clearDesignerStoreSpy = jest.fn();

        const props = {
            clearDesignerStore: clearDesignerStoreSpy,
            redirectIfFeatureTurnedOff: jest.fn(),
            createBotAndTrain: jest.fn(),
            params: {id: '132323'},
            fileUploading: false,
            fields: [],
            tables: [],
            staticAnnotations: [],
            toggleEditPreview: jest.fn(),
            setPreviewLoadingState: jest.fn(),
            setEditLoadingState: jest.fn(),
            previewData,
            appState: {
                user: {name: 'andrew'},
            },
        };

        const wrapper = shallow(
            <DesignerContainer
                {...props}
            />
        );

        it('should call the prop clearDesignerStore', () => {
            wrapper.instance().componentWillUnmount();
            expect(clearDesignerStoreSpy).toHaveBeenCalled();
        });
    });

    describe('when navigateDocumentsHandler is called', () => {
        const navigateBotsSpy = jest.fn();

        const props = {
            clearDesignerStore: jest.fn(),
            redirectIfFeatureTurnedOff: jest.fn(),
            createBotAndTrain: jest.fn(),
            navigateBots: navigateBotsSpy,
            params: {id: '132323'},
            fileUploading: false,
            fields: [],
            tables: [],
            staticAnnotations: [],
            toggleEditPreview: jest.fn(),
            setPreviewLoadingState: jest.fn(),
            setEditLoadingState: jest.fn(),
            setCurrentPageNumberAndSetSirFields: jest.fn(),
            previewData,
            appState: {
                user: {name: 'andrew'},
            },
        };

        const wrapper = shallow(
            <DesignerContainer
                {...props}
            />
        );

        it('should call navigateDocumentsHandler when a user clicks on next button', () => {
            wrapper.instance().navigateDocumentsHandler('next');

            expect(navigateBotsSpy).toHaveBeenCalled();
        });
    });

    describe('when changePageHandler is called', () => {
        const setCurrentPageNumber = jest.fn();

        const props = {
            setCurrentPageNumber,
            clearDesignerStore: jest.fn(),
            redirectIfFeatureTurnedOff: jest.fn(),
            createBotAndTrain: jest.fn(),
            params: {id: '132323', botid: '456', mode: 'edit'},
            fileUploading: false,
            fields: [],
            tables: [],
            staticAnnotations: [],
            setCurrentPageNumberAndSetSirFields: jest.fn(),
            toggleEditPreview: jest.fn(),
            previewData,
            appState: {
                user: {username: 'andrew'},
            },
        };

        const wrapper = shallow(
            <DesignerContainer
                {...props}
            />
        );

        it('should call navigateDocumentsHandler when a user clicks on next button', () => {
            wrapper.instance().changePageHandler({preventDefault: jest.fn(), target: [{value: 3}]});

            expect(props.setCurrentPageNumberAndSetSirFields).toHaveBeenCalledWith(3, '132323', '456', 'andrew', 'edit');
        });
    });
});
