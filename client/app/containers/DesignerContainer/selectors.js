import {createSelector} from 'reselect';
import {browserHistory} from 'react-router';

// import Rect from '../../components/Rect';
import {findField} from '../../utils/selectors';
import {
    getBotSaveData,
    getNewAnnotatorImageUrl,
    getTransformedSirField,
    getTransformedSirPreviewField,
    findActivePreviewField,
} from '../../utils/designer';
import {
    DESIGNER_AREA_TYPES,
    NavigationDirections,
    VISION_BOT_MODES,
} from '../../constants/enums';
import {FIELD_PROPERTY_NAMES} from './constants';
import Rect from '../../components/Rect';
import * as jsont from '../../helpers/json-transforms';
import {getFormFieldRules} from '../ValidatorContainer/transformRules/fields';
import {getTableRules} from '../ValidatorContainer/transformRules/tables';

const {MAPPED_SIR_LABEL, MAPPED_SIR_DISPLAY_VALUE, MAPPED_SIR_FOOTER} = FIELD_PROPERTY_NAMES;

/**
 * Direct selector to the annotatorContainer state domain
 */
const selectAnnotatorContainerDomain = () => (state) => state.get('designerContainer');

export const selectCurrentPreviewIndex = () => (state) => state.get('designerContainer').get('currentPreviewDocument');
export const selectActivePreviewId = () => (state) => state.get('designerContainer').get('activePreviewId');
export const selectFlattenedFields = () => (state) => state.get('designerContainer').get('flattenedPreviewFields').toJS();
export const selectAppContainerDomain = () => (state) => state.get('appContainer');
export const selectPreviewDocuments = () => (state) => state.get('designerContainer').get('previewDocuments').toJS();
export const selectVisionBotMode = () => (state) => state.get('designerContainer').get('designerViewMode');
export const selectOriginalUrl = () => (state) => state.get('designerContainer').get('url');
export const selectLayout = () => (state) => state.get('designerContainer').get('layouts').toJS();
export const selectBotTables = () => (state) => state.get('designerContainer').get('tables').toJS();
export const selectBotFields = () => (state) => state.get('designerContainer').get('fields').toJS();
export const selectBotColumns = () => (state) => state.get('designerContainer').get('columns').toJS();
export const selectPreviewDataState = () => (state) => state.get('designerContainer').get('originalPreviewData').toJS();
export const selectActivePreviewFieldId = () => (state) => state.get('designerContainer').get('activePreviewId');
export const selectConfidenceThreshold = () => (state) => state.get('designerContainer').get('confidenceThreshold');
export const selectCurrentZoomScale = () => (state) => state.get('designerContainer').get('currentZoomScale');
export const selectCurrentDocPage = () => (state) => state.get('designerContainer').get('currentDocumentPage');
export const selectPageData = () => (state) => state.get('designerContainer').get('pageData').toJS();
export const selectActiveFieldId = () => (state) => state.get('designerContainer').get('activeFieldId');
export const selectDocumentHeight = () => (state) => state.get('designerContainer').get('documentHeight');
export const selectDocumentWidth = () => (state) => state.get('designerContainer').get('documentWidth');
export const selectActiveFieldOption = () => (state) => state.get('designerContainer').get('activeFieldOption');
export const selectLayoutRatio = () => (state) => state.get('designerContainer').get('layoutRatio');
export const selectDrawnAreas = () => (state) => state.get('designerContainer').get('drawnAreas').toJS();
export const selectVisionBotId = () => (state) => state.get('designerContainer').get('visionBotId');
export const selectProjectCategories = () => (state) => state.get('designerContainer').get('projectCategories').toJS();
export const selectDocProperties = () => (state) => state.get('designerContainer').get('docProperties').toJS();
export const selectAllSirFields = () => (state) => state.get('designerContainer').get('allSirFields').toJS();

export const selectUserName = () => createSelector(
    selectAppContainerDomain(),
    (appData) => appData.toJS().user.username
);

export const selectSirFields = () => createSelector(
    selectCurrentDocPage(),
    selectPageData(),
    (currentPage, pageData) => {
        const {sirFields} = pageData[currentPage - 1] || {sirFields: []};

        return sirFields;
    },
);

// handles redirect logic
export const selectAppConfig = () => createSelector(
    selectAppContainerDomain(),
    (appState) => {
        return appState.get('appConfig').toJS();
    },
);

const selectShouldRedirect = () => createSelector(
    selectAppConfig(),
    (appConfig) => {
        return appConfig && appConfig.encrypted && !appConfig.designer;
    },
);

export const selectPreviewDocs = () => createSelector(
    selectPreviewDocuments(),
    (docs) => docs,
);

/**
 * check if the field is mapped (or complete) in the eyes of the system.
 *
 * @param {{mappedSirDisplayValue: Number, mappedSirLabel: Number, IsLabelOptional: Boolean}}
 * @returns {Boolean}
 */
const isFieldMapped = ({drawnDisplayValue, mappedSirDisplayValue, IsRequired}) => {
    const hasDisplayValue = Boolean(drawnDisplayValue || mappedSirDisplayValue);

    return Boolean(!IsRequired || hasDisplayValue);
};
/**
 * Other specific selectors
 */
const selectFields = () => createSelector(
    selectActiveFieldId(),
    selectBotFields(),
    (activeField, fields) => {
        return fields.reduce((acc, field) => {
            const isMapped = isFieldMapped(field);
            if (!isMapped) acc.allMapped = false;

            acc.values.push(
                {
                    ...field,
                    // lets create an isActive property so we dont have to iterate or search
                    // in the UI
                    isActive: activeField === field.Id,
                    isMapped,
                    // this lets us know which part of the state to edit, saves us some iterations
                    areaType: DESIGNER_AREA_TYPES.FIELD,
                }
            );
            return acc;
        }, {allMapped: true, values: []});
    }
 );

const isColumnMapped = (column) => Boolean(column.mappedSirLabel || column.drawnLabel || !column.IsRequired);

const selectColumns = () => createSelector(
    selectActiveFieldId(),
    selectBotColumns(),
    (activeField, columns) => {
        return columns.reduce((acc, column) => {
            const isMapped = isColumnMapped(column);
            if (!isMapped) acc.allMapped = false;

            acc.values.push(
                {
                    ...column,
                    // lets create an isActive property so we dont have to iterate or search
                    // in the UI
                    isActive: activeField === column.Id,
                    isMapped,
                    // this lets us know which part of the state to edit, saves us some iterations
                    areaType: DESIGNER_AREA_TYPES.COLUMN,
                }
            );

            return acc;
        }, {allMapped: true, values: []});
    }
 );

const selectTables = () => createSelector(
    selectActiveFieldId(),
    selectBotTables(),
    selectColumns(),
    (activeField, tables, columns) => {
        // this puts the tables back together for use in the designer page layout
        const values = tables.map((table) => {
            // this finds the appropriate columns per each table in the flattented Column array
            const Columns = columns.values.filter((column) => column.tableId === table.Id);
            // once we find the correct columns, put them back in the table as the designerPageLayout consumes them
            return {
                ...table,
                Columns,
                areaType: DESIGNER_AREA_TYPES.TABLE,
                isActive: table.Id === activeField,
                isMapped: Boolean(table.drawnFooter || table.mappedSirFooter),
            };
        });

        return {allMapped: columns.allMapped, values};
    }
);

const selectActiveField = () => createSelector(
    selectActiveFieldId(),
    selectFields(),
    selectColumns(),
    selectTables(),
    (activeFieldId, fields, columns, tables) => {
        const defaultField = fields.length ? fields.values[0] : columns.values[0];
        // this just returns the whole activeField or activeColumn object with
        // all its editable properties
        return findField(activeFieldId, fields.values, columns.values, tables.values) || defaultField || {};
    }
);

const selectActivePreviewField = () => createSelector(
    selectActivePreviewId(),
    selectFlattenedFields(),
    (activeFieldId, flattenedFields) => {
        const activeField = findActivePreviewField(activeFieldId, flattenedFields);
        const {Field, Value, Id} = activeField;

        return {...activeField, Value, Id, Field: {...Field, isActive: true}};
    }
);

const selectMetaData = () => createSelector(
    selectDocumentHeight(),
    selectDocumentWidth(),
    (height, width) => {
        return {Metadata: {height, width}};
    }
);

const selectMappedObjects = () => createSelector(
    selectActiveField(),
    selectSirFields(),
    selectCurrentDocPage(),
    selectPageData(),
    (activeField, sirFields, currentPage, pageData) => {
        const arrayOfMappings = [activeField[MAPPED_SIR_LABEL], activeField[MAPPED_SIR_DISPLAY_VALUE], activeField[MAPPED_SIR_FOOTER]];
        const currentPageIndexStart = (pageData[currentPage - 1] || {startIndex: 0}).startIndex;
        const matchingFieldsArray = sirFields.reduce((matchingFields, field, index) => {
            return !arrayOfMappings.includes(index + currentPageIndexStart) ? matchingFields : matchingFields.concat([{...field, index: index + currentPageIndexStart}]);
        }, []);

        return matchingFieldsArray;
    }
);

const selectIsPreview = () => createSelector(
    selectVisionBotMode(),
    (mode) => {
        return mode === VISION_BOT_MODES.PREVIEW;
    }
);

const selectSirEditTransformer = () => createSelector(
    selectAnnotatorContainerDomain(),
    selectMetaData(),
    selectMappedObjects(),
    selectActiveField(),
    selectCurrentDocPage(),
    selectPageData(),
    (...selectors) => {
        return ({Bounds, Text: textContent, Id}, index) => {
            const args = [
                ...selectors,
                Bounds,
                textContent,
                Id,
                index,
            ];

            return getTransformedSirField(...args);
        };
    }
);

const selectMappedPreviewObjects = () => createSelector(
    selectSirFields(),
    selectPreviewActiveField(),
    selectCurrentDocPage(),
    selectPageData(),
    (sirFields, activeField, currentPage, pageData) => {
        const currentPageData = (pageData[currentPage - 1] || {startIndex: 0});

        const [x, y, h, w] = (activeField && activeField.bounds.split(', ')) || [];
        const activeBounds = `${x}, ${Number(y) - currentPageData.startPx}, ${h}, ${w}`;

        const matchingFieldsArray = sirFields.reduce((fields, field, index) => {
            return field.Bounds === activeBounds ? fields.concat({...field, index: index + currentPageData.startIndex}) : fields;
        }, []);

        return matchingFieldsArray;
    }
);
const selectSirPreviewTransformer = () => createSelector(
    selectAnnotatorContainerDomain(),
    selectMetaData(),
    selectMappedPreviewObjects(),
    selectCurrentDocPage(),
    selectPageData(),
    (...selectors) => {
        return ({Bounds, Text: textContent, Id}, index) => {
            return getTransformedSirPreviewField(Bounds, textContent, Id, index, ...selectors);
        };
    }

);

// pulled this out of the main selector so we can leverage it when needed in other selectors
const selectStaticAnnotations = () => createSelector(
    selectAnnotatorContainerDomain(),
    selectSirFields(),
    selectSirEditTransformer(),
    selectSirPreviewTransformer(),
    selectIsPreview(),
    (annotatorState, sirFields, transformFieldsEdit, transformFieldsPreview, isPreview) => {
        const ifLoading = annotatorState.get('previewLoading') || annotatorState.get('editLoading');
        const sirFieldsToDisplay = ifLoading ? [] : sirFields;
        const transformFields = isPreview ? transformFieldsPreview : transformFieldsEdit;

        return sirFieldsToDisplay.map(transformFields);
    }
);

export const selectDrawnObjects = () => createSelector(
    selectActiveFieldOption(),
    selectLayoutRatio(),
    selectDrawnAreas(),
    selectActivePreviewId(),
    selectActiveFieldId(),
    selectStaticAnnotations(),
    selectCurrentDocPage(),
    selectPageData(),
    (selectedFieldOption, ratioToOriginalSize, drawnAreas, selectedPreviewId, selectedFieldId, sirFields, currentPage, pageData) => {
        const isPreview = browserHistory.getCurrentLocation().pathname.includes('preview');
        // this could be a find instead of a filter, since we have an AC that the user can draw only one area at a time
        // using find instead of filter would save iterations

        const objects = drawnAreas.filter(({designerData}) => {
            const {activeFieldId} = designerData;
            // since all drawn areas for all fields and columns go in the same array,
            // find the drawn area that matches the activeField, and the field option we are editing

            return isPreview ? selectedPreviewId === activeFieldId : selectedFieldId === activeFieldId;
        }).map((object) => {
            const adjustedDrawnObject = {...object, y: object.y - pageData[currentPage - 1].startPx};
            const {x: fieldX, y: fieldY, width, height, currentZoomScale, designerData} = adjustedDrawnObject;

            const fieldX2 = fieldX + width;
            const fieldY2 = fieldY + height;
            const left = Math.min(fieldX, fieldX2) / currentZoomScale;
            const right = Math.max(fieldX, fieldX2) / currentZoomScale;
            const top = Math.min(fieldY, fieldY2) / currentZoomScale;
            const bottom = Math.max(fieldY, fieldY2) / currentZoomScale;

            const layoutRatio = designerData.isAuto === false || designerData.isFromClick ? 1 : ratioToOriginalSize;

            const overlappedStaticAnnotations = sirFields.filter(({x, x2, y, y2}) => {
                return !(Math.max(x, x2) / layoutRatio < left || Math.min(x, x2) / layoutRatio > right || Math.min(y, y2) / layoutRatio > bottom || Math.max(y, y2) / layoutRatio < top);
            });

            const {activeFieldOption} = designerData;
            const isSameFieldOption = selectedFieldOption === activeFieldOption;
            const fill = !isSameFieldOption ? 'rgba(255, 188, 3, 0.15)' : Rect.meta.initial.fill;
            const stroke = 'rgba(255, 188, 3, 1)';
            const opacity = !isSameFieldOption ? 0.5 : 1;

            return {
                ...adjustedDrawnObject,
                overlappedStaticAnnotations,
                style: {fill, stroke, opacity},
            };
        });

        return objects;
    }
);

export const selectCurrentZoom = () => createSelector(
    selectCurrentZoomScale(),
    (zoomScale) => zoomScale
);

export const selectCurrentBotIndex = () => createSelector(
    selectVisionBotId(),
    selectProjectCategories(),
    (visionBotId, projectCategories) => {
        return projectCategories.findIndex(({visionBot}) => {
            return visionBot && visionBot.id === visionBotId;
        });
    },
);

const selectPreviewNavigationFiles = () => createSelector(
    selectCurrentPreviewIndex(),
    selectPreviewDocuments(),
    selectVisionBotMode(),
    (currentIndex, previewDocuments, visionBotMode) => {
        if (visionBotMode !== 'preview') {
            return null;
        }

        return {
            previous: currentIndex > 0 && previewDocuments[currentIndex - 1].fileId,
            next: currentIndex !== previewDocuments.length - 1 && previewDocuments[currentIndex + 1] && previewDocuments[currentIndex + 1].fileId,
        };
    }
);

// we use this to tell us whether or not the previous and next buttons should be
// disabled at any particular time. for example, if the previous property is true,
//the previous button will be ENABLED, (meaning we
// are NOT looking at the first bot in the projectCategories array since the currentBotIndex will not be 0)
const selectBotCanNavigateInfo = () => createSelector(
    selectProjectCategories(),
    selectCurrentBotIndex(),
    selectVisionBotMode(),
    selectPreviewNavigationFiles(),
    (categories, currentBotIndex, visionBotMode, previewNavigation) => {
        if (visionBotMode === 'edit') {
            const doesNextAvailableBotExist = categories.find(({visionBot = {}}, index) => {
                const isNotInProduction = !visionBot.currentState || visionBot.currentState === 'training';
                const isNotLocked = !visionBot.lockedUserId && isNotInProduction;

                return index > currentBotIndex && isNotLocked;
            });

            return {
                [NavigationDirections.PREVIOUS]: currentBotIndex !== 0,
                [NavigationDirections.NEXT]: currentBotIndex !== categories.length - 1 && Boolean(doesNextAvailableBotExist),
            };
        }

        return previewNavigation || {};
    },
);

export const selectCanNavigateBotPage = () => createSelector(
    selectCurrentDocPage(),
    selectPageData(),
    (currentPage, pageData) => {
        const canNavigateNextPage = currentPage !== pageData.length;
        const canNavigatePreviousPage = currentPage !== 1;

        return {
            next: canNavigateNextPage,
            previous: canNavigatePreviousPage,
        };
    }
);

export const selectBotSaveData = () => createSelector(
    selectAnnotatorContainerDomain(),
    (annotatorState) => getBotSaveData(annotatorState.toJS())
);

export const selectCurrentPageData = () => createSelector(
    selectCurrentDocPage(),
    selectPageData(),
    (currentPage, pageData) => pageData[currentPage - 1]
);

export const selectBotData = () => createSelector(
    selectLayout(),
    selectBotFields(),
    selectTables(),
    (layouts, fields, tables) => ({layouts, tables: tables.values, fields}),
);

export const selectAnnotationEngineUrl = () => createSelector(
    selectOriginalUrl(),
    selectCurrentPreviewIndex(),
    selectPreviewDocuments(),
    selectCurrentDocPage(),
    (originalUrl, currentIndex, previewDocuments, currentDocPage) => {
        if (!originalUrl) {
            return null;
        }

        return getNewAnnotatorImageUrl(originalUrl, currentIndex, previewDocuments, currentDocPage);
    }
);

//applied transformation rules for PREVIEW data
const selectFieldRules = () => createSelector(
    selectActivePreviewFieldId(),
    selectConfidenceThreshold(),
    (activeFieldId, confidenceThreshold) => {
        const getRules = getFormFieldRules(activeFieldId, confidenceThreshold);
        return getRules();
    }
 );

 /**
 * this selector transform original field structure to UI requirement and flattern it.
 */
const selectPreviewFormFields = () => createSelector(
    selectPreviewDataState(),
    selectFieldRules(),
    (previewData, fieldRules) => {
        const {fields: originalPreviewFields} = previewData;
        const fields = jsont.transform(originalPreviewFields, fieldRules);
        return fields;
    }
 );

const selectTableRules = () => createSelector(
    selectActivePreviewFieldId(),
    selectConfidenceThreshold(),
    (activeFieldId, confidenceThreshold) => {
        const getRules = getTableRules(activeFieldId, confidenceThreshold);
        return getRules();
    }
);

const selectPreviewTables = () => createSelector(
    selectPreviewDataState(),
    selectTableRules(),
    (previewData, tableRules) => {
        const {tables: originalTables} = previewData;
        return jsont.transform(originalTables, tableRules);
    }
);

const selectPreviewData = () => createSelector(
    selectPreviewFormFields(),
    selectPreviewTables(),
    (fields, tables) => {
        const allFieldsAreExtractedConfidently = fields.every((field) => field.isConfident && !field.validationIssue);
        const allTablesAreExtractedConfidently = !allFieldsAreExtractedConfidently ? false : tables.every((table) => table.rows.every((row) => row.every((column) => column.isConfident && !column.validationIssue)));

        return {
            fields,
            tables,
            allPreviewFieldsExtractedConfidently: allFieldsAreExtractedConfidently && allTablesAreExtractedConfidently,
        };
    }
);

// combine all transformed fields
const selectAllTablesFields = () => createSelector(
    selectPreviewTables(),
    (tables) => {
        return tables.reduce((acc, table) => {
            return table.rows.reduce((acc, row) => {
                return row.reduce((acc, field) => {
                    return [...acc, field];
                }, acc);
            }, acc);
        }, []);
    }
);


const selectAllFields = () => createSelector(
    selectPreviewFormFields(),
    selectAllTablesFields(),
    (formFields, tableFields) => {
        const allFields = [...formFields,
            ...tableFields,
        ];
        return allFields;
    }
);

const selectPreviewActiveField = () => createSelector(
    selectActivePreviewFieldId(),
    selectAllFields(),
    (currentFieldId, allFields) => {
        const currentField = allFields.find((field) => field.fieldGuid === currentFieldId);
        return currentField;
    }
);
/**
 * Default selector used by AnnotatorContainer
 */


const selectAnnotatorContainer = () => createSelector(
    selectAppContainerDomain(),
    selectAnnotatorContainerDomain(),
    selectFields(),
    selectTables(),
    selectActiveField(),
    selectDrawnObjects(),
    selectStaticAnnotations(),
    selectMetaData(),
    selectCurrentBotIndex(),
    selectShouldRedirect(),
    selectBotCanNavigateInfo(),
    selectActivePreviewField(),
    selectPreviewNavigationFiles(),
    selectAnnotationEngineUrl(),
    selectPreviewData(),
    selectCurrentDocPage(),
    selectCanNavigateBotPage(),
    selectSirFields(),
    (
        appState,
        substate,
        designerFields,
        tables,
        activeField,
        drawnObjects,
        staticAnnotations,
        metaData,
        currentBotIndex,
        shouldRedirect,
        botCanNavigate,
        activePreviewField,
        previewNavigationFiles,
        url,
        previewData,
        currentDocumentPage,
        canNavigateBotPage,
        sirFields,
    ) => {
        return {
            ...substate.toJS(),
            objects: drawnObjects,
            staticAnnotations,
            fields: designerFields.values,
            metaData,
            tables: tables.values,
            activeField,
            activePreviewField,
            currentBotIndex,
            currentDocumentPage,
            shouldRedirect,
            botCanNavigate,
            allMapped: Boolean(designerFields.allMapped && tables.allMapped),
            previewData,
            appState: appState.toJS(),
            previewNavigationFiles,
            url,
            canNavigateBotPage,
            sirFields,
        };
    }
);

export default selectAnnotatorContainer;
export {
    selectAnnotatorContainerDomain,
};
