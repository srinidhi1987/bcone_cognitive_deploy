
export function getMatchValue(match) {
    return match.$values ? match.$values : match;
}
/**
 * this recursive function returns the object without property name $types for given object
 * @param {object} acc
 * @param {string} key object's property name
 */
export function normalizePreviewBackendStructure(acc, key) {
    if (key === '$type') {
        return acc;
    }
    if (typeof this.data[key] === 'object') { //eslint-disable-line no-invalid-this
        return {
            ...acc,
            [key] : this.data[key] ? Object.keys(this.data[key]).reduce(normalizePreviewBackendStructure.bind({data:this.data[key]}), {}) : this.data[key], //eslint-disable-line no-invalid-this
        };
    }
    return {
        ...acc,
        [key]: this.data[key], //eslint-disable-line no-invalid-this
    };
}

