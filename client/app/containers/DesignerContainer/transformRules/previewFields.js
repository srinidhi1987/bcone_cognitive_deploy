import * as jsont from '../../../helpers/json-transforms';

import {getMatchValue, normalizePreviewBackendStructure} from './utils';

export function getPreviewFieldRules() {
    return function getRules() {
        const fieldRules = [
            jsont.pathRule(
                '.', (d) => {
                    const {Id, Field, Value, ValidationIssue} = getMatchValue(d.match);
                    return {
                        Id,
                        Field: Object.keys(Field).reduce(normalizePreviewBackendStructure.bind({data: Field}), {}),
                        Value: Object.keys(Value).reduce(normalizePreviewBackendStructure.bind({data: Value}), {}),
                        ValidationIssue: ValidationIssue ? Object.keys(ValidationIssue).reduce(normalizePreviewBackendStructure.bind({data: ValidationIssue}), {}) : ValidationIssue,
                    };
                }
            ),
        ];
        return fieldRules;
    };
}

