import * as jsont from '../../../helpers/json-transforms';
import {getPreviewFieldRules} from './previewFields';
import {getMatchValue, normalizePreviewBackendStructure} from './utils';
/**
 * return function that return array of rule function for table columns
 */

/**
 * rule for preview table
 * @returns {Function} that return array of rule function for table rows
 */
export function getTableRowRules() {
    return function getRules() {
        const tableRowRules = [
            jsont.pathRule('.', (d) => {
                const {Fields} = d.match;
                return {Fields: jsont.transform(getMatchValue(Fields), getPreviewFieldRules()())};
            }),
        ];

        return tableRowRules;
    };
}

/**
 * @returns {Function} that return rules for table headers
 */
export function getTableHeaderRules() {
    return function getRules() {
        const tableHeaderRules = [
            jsont.pathRule('.', (d) => {
                return Object.keys(d.match).reduce(normalizePreviewBackendStructure.bind({data: d.match}), {});
            }),
        ];
        return tableHeaderRules;
    };
}

/**
 * @returns {Function} that return rules for table Column defination
 */
export function getTableColumnDefinationRules() {
    return function getRules() {
        const tableHeaderRules = [
            jsont.pathRule('.', (d) => {
                return Object.keys(d.match).reduce(normalizePreviewBackendStructure.bind({data: d.match}), {});
            }),
        ];
        return tableHeaderRules;
    };
}

/**
 * @returns {Function} that return rules for table defination
 */
export function getTableDefinationRules() {
    return function getRules() {
        const tableHeaderRules = [
            jsont.pathRule('.', (d) => {
                const {Id, Columns, IsDeleted, Name} = d.match;
                return {
                    Id,
                    Columns: jsont.transform(Columns.$values, getTableColumnDefinationRules()()),
                    IsDeleted,
                    Name,
                };
            }),
        ];
        return tableHeaderRules;
    };
}

/**
 * @returns {Function} that return array of rule function for tables
 */
export function getPreviewTableRules() {

    return function getTableRules() {

        const tableRules = [
            jsont.pathRule(
                '.', (d) => {
                    const {TableDef, Headers, Rows, Field} = d.match;
                    return {
                        TableDef: jsont.transform(TableDef, getTableDefinationRules()()),
                        Headers: jsont.transform(Headers.$values, getTableHeaderRules()()),
                        Rows: jsont.transform(Rows.$values, getTableRowRules()()),
                        Field : Field ? Object.keys(Field).reduce(normalizePreviewBackendStructure.bind({data: Field}), {}) : Field,
                    };
                }
            ),
        ];
        return tableRules;
    };
}
