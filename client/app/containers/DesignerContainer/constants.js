import {EMPTY_STRING} from '../../constants/common';
/*
 *
 * AnnotatorContainer constants
 *
 */

// ACTION TYPES
export const AUTO_CLICK_SIR_FIELD = 'app/DesignerContainer/AUTO_CLICK_SIR_FIELD';
export const CLEAR_DRAWN_FIELDS = 'app/DesignerContainer/CLEAR_DRAWN_FIELDS';
export const CLEAR_DRAWN_AREAS = 'app/DesignerContainer/CLEAR_DRAWN_AREAS';
export const CLICK_DRAWN_AREA = 'app/DesignerContainer/CLICK_DRAWN_AREA';
export const CLICK_SIR_FIELD = 'app/DesignerContainer/CLICK_SIR_FIELD';
export const CLEAR_DESIGNER_STORE = 'app/DesignerContainer/CLEAR_DESIGNER_STORE';
export const CREATE_BOT_AND_TRAIN = 'app/DesignerContainer/CREATE_BOT_AND_TRAIN';
export const DEFAULT_ACTION = 'app/DesignerContainer/DEFAULT_ACTION';
export const EDIT_BOT_AND_TRAIN = 'app/DesignerContainer/EDIT_BOT_AND_TRAIN';
export const FETCH_ANNOTATION_CLASSES = 'app/DesignerContainer/FETCH_ANNOTATION_CLASSES';
export const FETCH_ANNOTATION_CLASSES_SUCCESS = 'app/DesignerContainer/FETCH_ANNOTATION_CLASSES_SUCCESS';
export const MAP_SIR_FIELD = 'app/DesignerContainer/MAP_SIR_FIELD';
export const EXTRACT_SIR_FIELD_TEXT = 'app/DesignerContainer/EXTRACT_SIR_FIELD_TEXT';
export const POPULATE_BOT_DATA = 'app/DesignerContainer/POPULATE_BOT_DATA';
export const PUT_PROJECT_NAME_IN_STORE = 'app/DesignerContainer/PUT_PROJECT_NAME_IN_STORE';
export const REDIRECT_IF_FEATURE_TURNED_OFF = 'app/DesignerContainer/REDIRECT_IF_FEATURE_TURNED_OFF';
export const SAVE_DESIGNER = 'app/DesignerContainer/SAVE_DESIGNER';
export const SET_UPLOAD_FILE_LOADING_STATE = 'app/DesignerContainer/SET_UPLOAD_FILE_LOADING_STATE';
export const UPLOAD_FILE_TO_DESIGNER = 'app/DesignerContainer/UPLOAD_FILE_TO_DESIGNER';
export const UPLOAD_FILE_TO_DESIGNER_SUCCESS = 'app/DesignerContainer/UPLOAD_FILE_TO_DESIGNER_SUCCESS';
export const SET_ACTIVE_FIELD = 'app/DesignerContainer/SET_ACTIVE_FIELD';
export const TOGGLE_FIELD_CHECKBOX = 'app/DesignerContainer/TOGGLE_FIELD_CHECKBOX';
export const SET_FIELD_VALUE = 'app/DesignerContainer/SET_FIELD_VALUE';
export const POPULATE_BOT_LABELS = 'app/DesignerContainer/POPULATE_BOT_LABELS';
export const POPULATE_LANGUAGE_ALIASES = 'app/DesignerContainer/POPULATE_LANGUAGE_ALIASES';
export const POPULATE_PREVIEW_DATA = 'app/DesignerContainer/POPULATE_PREVIEW_DATA';
export const SET_ACTIVE_FIELD_OPTION = 'app/DesignerContainer/SET_ACTIVE_FIELD_OPTION';
export const UPDATE_DRAWN_AREAS = 'app/DesignerContainer/UPDATE_DRAWN_AREAS';
export const SET_LAYOUT_RATIO = 'app/DesignerContainer/SET_LAYOUT_RATIO';
export const START_DRAG = 'app/DesignerContainer/START_DRAG';
export const STOP_DRAG = 'app/DesignerContainer/STOP_DRAG';
export const SET_ANNOTATOR_MODE = 'app/DesignerContainer/SET_ANNOTATOR_MODE';
export const CLICK_FIELD_OPTION_DRAW = 'app/DesignerContainer/CLICK_FIELD_OPTION_DRAW';
export const SET_IMAGE_LOADED = 'app/DesignerContainer/SET_IMAGE_LOADED';
export const SUGGEST_VALUE = 'app/DesignerContainer/SUGGEST_VALUE';
export const SET_CURRENT_ZOOM_SCALE = 'app/DesignerContainer/SET_CURRENT_ZOOM_SCALE';
export const SET_SCALE_AMOUNT = 'app/DesignerContainer/SET_SCALE_AMOUNT';
export const SAVE_PROJECT_CATEGORIES = 'app/DesignerContainer/SAVE_PROJECT_CATEGORIES';
export const NAVIGATE_BOTS = 'app/DesignerContainer/NAVIGATE_BOTS';
export const SELECT_ACTIVE_FIELD_THEN_CREATE_MAPPING = 'app/DesignerContainer/SELECT_ACTIVE_FIELD_THEN_CREATE_MAPPING';
export const SAVE_BOT = 'app/DesignerContainer/SAVE_BOT';
export const SAVE_AND_CLOSE_CONFIRMATION_BOX_VISIBILITY = 'app/DesignerContainer/SAVE_CONFIRMATION_BOX_VISIBILITY';
export const SAVE_AND_MOVE_TO_PREVIEW = 'app/DesignerContainer/SAVE_AND_MOVE_TO_PREVIEW';
export const FINISH_CONFIRMATION_BOX_VISIBILITY = 'app/DesignerContainer/FINISH_CONFIRMATION_BOX_VISIBILITY';
export const NEXT_CONFIRMATION_BOX_VISIBILITY = 'app/DesignerContainer/NEXT_CONFIRMATION_BOX_VISIBILITY';
export const HIDE_ALL_CONFIRMATION_BOX_VISIBILITY = 'app/DesignerContainer/HIDE_ALL_CONFIRMATION_BOX_VISIBILITY';
export const MAP_TABLE = 'app/DesignerContainer/MAP_TABLE';
export const MAP_REF_COLUMN = 'app/DesignerContainer/MAP_REF_COLUMN';
export const SET_ACTIVE_PREVIEW_FIELD = 'app/DesignerContainer/SET_ACTIVE_PREVIEW_FIELD';
export const SET_PREVIEW_FIELD_VALUE = 'app/DesignerContainer/SET_PREVIEW_FIELD_VALUE';
export const SET_PREVIEW_LOADING_STATE = 'app/DesignerContainer/SET_PREVIEW_LOADING_STATE';
export const SET_DESIGNER_VIEW_MODE = 'app/DesignerContainer/SET_DESIGNER_VIEW_MODE';
export const TOGGLE_EDIT_PREVIEW = 'app/DesignerContainer/TOGGLE_EDIT_PREVIEW';
export const POPULATE_PREVIEW_DOCUMENTS = 'app/DesignerContainer/POPULATE_PREVIEW_DOCUMENTS';
export const NAVIGATE_PREVIEW_DOCUMENT = 'app/DesignerContainer/NAVIGATE_PREVIEW_DOCUMENT';
export const SET_CURRENT_PREVIEW_INDEX = 'app/DesignerContainer/SET_CURRENT_PREVIEW_INDEX';
export const SET_URL = 'app/DesignerContainer/SET_URL';
export const SET_EDIT_LOADING_STATE = 'app/DesignerContainer/SET_EDIT_LOADING_STATE';
export const SET_IS_WAITING_FOR_PREVIEW_DATA = 'app/DesignerContainer/SET_IS_WAITING_FOR_PREVIEW_DATA';
export const SET_IS_LOADING_EDIT_DATA = 'app/DesignerContainer/SET_IS_LOADING_EDIT_DATA';
export const EXPORT_TO_CSV = 'app/DesignerContainer/EXPORT_TO_CSV';
export const FETCH_CATEGORIES_FOR_DESIGNER = 'app/DesignerContainer/FETCH_CATEGORIES_FOR_DESIGNER';
export const CLEAR_ALL_DRAWN_FIELDS = 'app/DesignerContainer/CLEAR_ALL_DRAWN_FIELDS';
export const NAVIGATE_DOC_PAGE = 'app/DesignerContainer/NAVIGATE_DOC_PAGE';
export const SET_BOT_PAGE_DATA = 'app/DesignerContainer/SET_BOT_PAGE_DATA';
export const SET_CURRENT_PAGE_NUMBER_AND_SET_SIR_FIELDS = 'app/DesignerContainer/SET_CURRENT_PAGE_NUMBER_AND_SET_SIR_FIELDS';
export const SET_CURRENT_PAGE_NUMBER = 'app/DesignerContainer/SET_CURRENT_PAGE_NUMBER';
export const SET_PAGE_INPUT_VALUE = 'app/DesignerContainer/SET_PAGE_INPUT_VALUE';
export const SET_SIR_FIELD_SELECTION = 'app/DesignerContainer/SET_SIR_FIELD_SELECTION';
export const POPULATE_SIR_FIELDS = 'app/DesignerContainer/POPULATE_SIR_FIELDS';
export const SET_SAVE_DESIGNER_SPINNER_STATE = 'app/DesignerContainer/SET_SAVE_DESIGNER_SPINNER_STATE';
export const SET_PREVIEW_DATA = 'app/DesignerContainer/SET_PREVIEW_DATA';
export const SET_DOC_PAGE_META = 'app/DesignerContainer/SET_DOC_PAGE_META';
export const SET_DOC_PROPERTIES = 'app/DesignerContainer/SET_DOC_PROPERTIES';
export const UPDATE_ALL_DRAWN_PREVIEW_AREAS = 'app/DesignerContainer/UPDATE_ALL_DRAWN_PREVIEW_AREAS';
export const SAVE_ALL_SIR_FIELDS = 'app/DesignerContainer/SAVE_ALL_SIR_FIELDS';
// CONSTANTS
export const MIN_ZOOM_SCALE = 0.5;
export const MAX_ZOOM_SCALE = 2.0;
export const EMPTY_BOUNDS = '0, 0, 0, 0';
export const DEFAULT_SIMILARITY_FACTOR = 0.6999999880;

/**
 * Indexes for Mapped SIRs for Labels and Values
 *
 * @enum {String}
 * @readonly
 */
export const FIELD_PROPERTY_NAMES = {
    MAPPED_SIR_LABEL: 'mappedSirLabel',
    MAPPED_SIR_DISPLAY_VALUE: 'mappedSirDisplayValue',
    MAPPED_SIR_FOOTER: 'mappedSirFooter',
};

export const DEFAULT_EMPTY_FIELD = {
    Bounds: EMPTY_BOUNDS,
    DisplayValue: EMPTY_STRING,
    EndsWith: EMPTY_STRING,
    FieldDirection: 2,
    FieldId: null,
    FormatExpression: EMPTY_STRING,
    Id: '',
    IsMultiline: false,
    IsValueBoundAuto: false,
    Label: null,
    MergeRatio: 1,
    SimilarityFactor: DEFAULT_SIMILARITY_FACTOR,
    StartsWith: EMPTY_STRING,
    Type: 0,
    ValueBounds: EMPTY_BOUNDS,
    ValueType: 0,
    XDistant: 0,
};

export const SUCCESS_DESTINATION = {
    NEXT: 'next-bot',
    LI_DETAIL: 'li-detail',
    PREVIEW: 'preview',
};
