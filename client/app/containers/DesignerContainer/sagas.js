import {call, put, select} from 'redux-saga/effects';
import {takeLatest, takeEvery} from 'redux-saga';
import {requestPost} from '../../utils/request';
import {isJSON} from '../../utils/helpers';
import * as jsont from '../../helpers/json-transforms';
import {getPreviewFieldRules} from './transformRules/previewFields';
import {getPreviewTableRules} from './transformRules/previewTables';
import {
    findMappedLabels,
    findMappedTables,
    findMappedValues,
    findNonStandardPreviewBoxes,
    syncDocsWithCurrentLayout,
} from '../../utils/designer';

import {transformPreviewData} from './utilities';
import {
    autoMapSirField,
    clearAllDrawnFields,
    clearDesignerStore,
    clearDrawnFields,
    clearDrawnAreas,
    clickSirField,
    extractSirFieldText,
    hideAllConfirmationBoxVisibility,
    mapRefColumn,
    mapSirField,
    mapTable,
    navigateBots,
    putProjectMetadataInStore,
    populateBotData,
    populatePreviewData,
    populatePreviewDocuments,
    populateSirFields,
    saveAllSirFields,
    saveProjectCategories,
    setActiveField,
    setActiveFieldOption,
    setAnnotatorMode,
    setBotPageData,
    setCurrentPreviewDocument,
    setCurrentPageNumber,
    setDesignerViewMode,
    setDocPageMeta,
    setDocProperties,
    setFieldValue,
    setEditLoadingState,
    setIsLoadingEditData,
    setIsWaitingForPreviewData,
    setPreviewLoadingState,
    setUploadFileLoadingState,
    stopDrag,
    suggestValue,
    setEditSaveSpinnerState,
    uploadFileToDesignerSuccess,
    selectActiveFieldThenCreateMapping,
    updateDrawnAreas,
    updateAllDrawnPreviewAreas,
    // fetchAnnotationClassesSuccess,
} from './actions';

import {
    selectAllSirFields,
    selectBotData,
    selectBotSaveData,
    selectCurrentBotIndex,
    selectCurrentPreviewIndex,
    selectCurrentPageData,
    selectCurrentZoom,
    selectDocProperties,
    selectDrawnObjects,
    selectPreviewDocs,
    selectProjectCategories,
    selectVisionBotId,
} from './selectors';
import {browserHistory} from 'react-router';
import {HEADER_ACCEPT_JSON} from 'common-frontend-utils';
import {
    botsBackEnd,
    designerBackEnd,
    projectsBackEnd,
    projectCategoriesBackEnd,
} from '../../resources';

import {
    AUTO_CLICK_SIR_FIELD,
    CLICK_SIR_FIELD,
    CREATE_BOT_AND_TRAIN,
    // CLICK_DRAWN_AREA,
    CLEAR_DRAWN_FIELDS,
    CLICK_FIELD_OPTION_DRAW,
    EXPORT_TO_CSV,
    FETCH_CATEGORIES_FOR_DESIGNER,
    SET_PREVIEW_DATA,
    MAP_TABLE,
    NAVIGATE_BOTS,
    NAVIGATE_PREVIEW_DOCUMENT,
    REDIRECT_IF_FEATURE_TURNED_OFF,
    SAVE_AND_MOVE_TO_PREVIEW,
    SAVE_BOT,
    SET_CURRENT_PAGE_NUMBER_AND_SET_SIR_FIELDS,
    STOP_DRAG,
    SUGGEST_VALUE,
    TOGGLE_EDIT_PREVIEW,
    UPLOAD_FILE_TO_DESIGNER,
    SELECT_ACTIVE_FIELD_THEN_CREATE_MAPPING,
} from './constants';

import {ANNOTATION_MODES} from '../../components/AnnotationEngine/constants/enums';
import {
    FIELD_OPTIONS,
    NavigationDirections,
    VISION_BOT_MODES,
    DESIGNER_AREA_TYPES,
} from '../../constants/enums';
import {SUCCESS_DESTINATION} from './constants';
import {setProjectVisionBotStateFromServer} from '../BotsContainer/sagas';
import routeHelper from '../../utils/routeHelper';
import {getPageData} from './utilities';
const {EDIT, PREVIEW} = VISION_BOT_MODES;

// Individual exports for testing
export function* defaultSaga() { // eslint-disable-line require-yield
    return;
}

export function* saveDesignerFlow(action) { // eslint-disable-line require-yield
    const splitUrlArray = action.url.split('/');
    const fileNameArray = splitUrlArray[splitUrlArray.length - 1].split('?');

    const data = {
        annotations: action.payload,
        filename: fileNameArray[0],
        'class': 'image',
    };

    window.location.assign(routeHelper(`/api/designer/save?fields=${encodeURIComponent(JSON.stringify([data]))}`));
}

export function uploadFileToDesigner(files) {
    const formData = new FormData();

    Object.keys(files).forEach((key) => {
        if (files[key] instanceof File) {
            formData.append('files', files[key], files[key].name);
        }
    });
    return requestPost(
        routeHelper('api/designer/upload'),
        {
            headers: HEADER_ACCEPT_JSON,
            body: formData,
            isRawBody: true,
        }
    );
}

export function* UploadFileToDesignerFlow(action) {
    yield put(setUploadFileLoadingState(true));
    const response = yield call(uploadFileToDesigner, action.file);
    yield put(uploadFileToDesignerSuccess(response));
    yield put(setUploadFileLoadingState(false));
}


/**
 * Check for file upload status by transaction id
 * @param {string} action.projectId
 * @param {string} action.categoryId
 * @returns {Promise}
 */
export function* createBotAndTrainFlow(action) {
    try {
      // if we are navigating bots, we want to clear out the pre-existing bot data
      // since we are not refreshing the page on navigation
        if (action.shouldClearStore) {
            yield put(clearDesignerStore());
        }

        yield put(clearAllDrawnFields());

        const result = yield call(projectsBackEnd.get, action.projectId);

        if (result.success) {
            yield put(putProjectMetadataInStore(result.data));
        }
        if (action.mode === EDIT) {
            yield put(setEditLoadingState(true));
        }

        if (action.mode === PREVIEW) {
            yield put(setIsWaitingForPreviewData(true));
        }

        const botResult = yield call(botsBackEnd.get, action.projectId, action.categoryId, action.username);

        yield put(populateBotData(botResult));

        const pagesInfo = getPageData(botResult.visionBotData.Layouts[0], 1);
        yield put(setBotPageData(pagesInfo));

        const sirFields = botResult.visionBotData.Layouts[0].SirFields.Fields;

        const mappedLabels = findMappedLabels(botResult, sirFields);
        const mappedValues = findMappedValues(botResult, sirFields);
        const mappedTables = findMappedTables(botResult, sirFields);

        // if we found an automap for any value or field, lets "click on it", or use the code that we use to map an sir field
        // to auto map the sir fields, as if the system had gone through and "clicked" or "autosuggested" fields and labels for
        //each field
        const automapField = (field) => put(selectActiveFieldThenCreateMapping(field));
        const automapTable = (table) => put(mapTable(table));
        // these need to go one after another and cant be combined since redux saga
        // schedules these counter intuitively (if we combine them, it will do all of the setAvtiveField puts
        // and THEN all the mapping, instead of setting activeField and then mapping for every iteration)
        yield mappedLabels.map(automapField);
        yield mappedValues.map(automapField);
        yield mappedTables.map(automapTable);

        const documentsResult = yield call(botsBackEnd.fetchPreviewDocuments, action.projectId, action.categoryId);
        // syncedDocSet makes sure that the document selection is ordered so that the
        // current document being viewed is the current layout
        const syncedDocSet = syncDocsWithCurrentLayout(documentsResult.data, botResult.visionBotData.Layouts[0]);
        yield put(populatePreviewDocuments(syncedDocSet));


        yield put(setActiveFieldOption(FIELD_OPTIONS.LABEL));
        const {Fields, Tables} = botResult.visionBotData.DataModel;
        const activeField = Fields.length ? Fields[0].Id : Tables[0].Columns[0].Id;
        yield put(setActiveField(activeField));

        const categories = yield call(projectCategoriesBackEnd, action.projectId, {sort: '-index'});

        if (categories.data && categories.data.categories) {
            yield put(saveProjectCategories(categories.data.categories));
        }

        if (action.mode === PREVIEW) {
            yield put(setDesignerViewMode(PREVIEW));
            yield call(
                setPreviewDataFlow,
                {
                    projectId: action.projectId,
                    categoryId: action.categoryId,
                    username: action.username,
                    pageNumber: 1,
                    shouldResetPreviewData: true,
                },
            );
        }

        return true;
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
}

export function* fetchProjectCategoriesForDesignerAndKeepAlive(action) {
    const categories = yield call(projectCategoriesBackEnd, action.projectId, {sort: '-index'});

    if (categories.data && categories.data.categories) {
        yield put(saveProjectCategories(categories.data.categories));
    }

    if (action.visionBotId) {
        yield call(botsBackEnd.keepDesignerAlive, action.projectId, action.categoryId, action.visionBotId);
    }
}

export function* selectFieldAndClickFlow(arg) {
    const {field} = arg;
    const {area, activeArea, isDrawn, isFromClick} = field;

    const {Id, fieldOption: activeFieldOption} = activeArea;
    yield put(setActiveFieldOption(activeFieldOption));

    if (!isDrawn) {
        if (isFromClick) {
            yield put(autoMapSirField(area, activeArea));
        }
        else {
            yield put(clickSirField(area, activeArea));
        }
    }
    else {
        const [x, y, width, height] = area.Bounds.split(',');
        const numberX = Number(x);
        const numberY = Number(y);
        const numberWidth = Number(width);
        const numberHeight = Number(height);
        const currentZoom = yield select(selectCurrentZoom());

        const drawnArea = [
            {
                currentZoomScale: currentZoom,
                x: numberX,
                y: numberY,
                width: numberWidth,
                height: numberHeight,
                overlappedStaticAnnotations: [],
                designerData: {
                    isAuto: !isFromClick,
                    isFromClick,
                    activeFieldId: Id,
                    activeFieldOption,
                },
            },
        ];

        yield call(updateDrawnAreasAndStopDraw, drawnArea, activeFieldOption, activeArea, Id);
    }
}

function* updateDrawnAreasAndStopDraw(drawnArea, activeFieldOption, activeArea, Id) {
    yield put(setActiveField(Id));

    yield put(updateDrawnAreas(drawnArea, true));

    if (activeFieldOption !== 'Preview') {
        if (activeFieldOption === 'Label' || activeFieldOption === 'DisplayValue') {
            yield put(setFieldValue(activeArea, `drawn${activeFieldOption}`, true));
        }

        yield put(setActiveField(Id));
        const drawnAreas = yield select(selectDrawnObjects());

        yield put(stopDrag(
            activeArea,
            activeFieldOption,
            drawnAreas,
        ));
    }
}

export function* redirectIfFeatureTurnedOffFlow() {
    yield call(browserHistory.push, routeHelper('/'));
}

export function* navigateToBotFlow({direction, categories, currentBotIndex, params}) {
    if (direction === NavigationDirections.NEXT) {
        const nextBot = categories.find(({visionBot = {}}, index) => {
            const isBotAfterCurrentBot = index > currentBotIndex;
            const isNotInProduction = !visionBot.currentState || visionBot.currentState === 'training';

            return isBotAfterCurrentBot && !visionBot.lockedUserId && isNotInProduction;
        });

        yield call(browserHistory.push, routeHelper(`/learning-instances/${params.id}/bots/${nextBot.id}/edit?refresh=true`));
    }
    if (direction === NavigationDirections.PREVIOUS) {
        const previousBot = categories[currentBotIndex - 1];
        yield call(browserHistory.push, routeHelper(`/learning-instances/${params.id}/bots/${previousBot.id}/edit`));
    }
}

export function* clickSirFieldFlow({area = {}, activeField, fieldOption, labelSuggestionData}) {
    const pageData = yield select(selectCurrentPageData());
    const indexAdjustmentForMultiPage = activeField.isAuto ? pageData.startIndex : 0;
    const adjustedArea = {...area, index: area.index + indexAdjustmentForMultiPage};

    yield put(extractSirFieldText(activeField, adjustedArea));
    yield put(mapSirField(activeField, adjustedArea));
    yield put(clearDrawnFields(activeField, fieldOption));

    if (labelSuggestionData && activeField.areaType !== DESIGNER_AREA_TYPES.COLUMN) {
        yield put(suggestValue(activeField, labelSuggestionData));
    }
}

export function* autoClickSirFieldFlow({area = {}, activeField, fieldOption, labelSuggestionData}) {
    const pageData = yield select(selectCurrentPageData());

    const adjustedArea = {...area, index: area.index + pageData.indexStart};

    yield put(extractSirFieldText(activeField, adjustedArea));
    yield put(mapSirField(activeField, adjustedArea));
    yield put(clearDrawnFields(activeField, fieldOption));
    if (labelSuggestionData) {
        yield put(suggestValue(activeField, labelSuggestionData));
    }
}

export function* clearDrawnFieldsSaga({activeField, fieldOption}) {
    yield put(clearDrawnAreas(activeField));
    yield put(setFieldValue(activeField, `drawn${fieldOption}`, false));
}

export function* suggestValueSaga({activeField, labelSuggestionData}) {
    // destructure these values
    const pageData = yield select(selectCurrentPageData());

    const [fieldX, fieldY, fieldWidth, fieldHeight] = labelSuggestionData.fieldLayout.Bounds.split(',');
    const numberFieldY = Number(fieldY);
    const numberFieldX = Number(fieldX);
    const numberFieldWidth = Number(fieldWidth);
    const numberFieldHeight = Number(fieldHeight);
    const adjustedSuggestionDataY = numberFieldY + pageData.startPx;
    const adjustedLabelSuggestionDataField = {...labelSuggestionData.fieldLayout, Bounds: `${numberFieldX}, ${adjustedSuggestionDataY}, ${numberFieldWidth}, ${numberFieldHeight}`};
    const valueSuggestion = yield call(designerBackEnd.suggestValueForLabel, labelSuggestionData.projectId, labelSuggestionData.categoryId, labelSuggestionData.layoutId, labelSuggestionData.visionBotId, labelSuggestionData.documentId, labelSuggestionData.fieldDefId, adjustedLabelSuggestionDataField);

    if (valueSuggestion) {
        yield put(setActiveFieldOption(FIELD_OPTIONS.DISPLAY_VALUE));
        // move this to the node layer
        const parsedValueSuggestion = isJSON(valueSuggestion.data) ? JSON.parse(valueSuggestion.data) : {Text: '', Bounds: '0, 0, 0, 0'};
        // look through our sir fields, and find one that matches the text content (this should probably match the bounds string)
        const [x, y, width, height] = parsedValueSuggestion.Bounds.split(',');
        const numberY = Number(y);
        const numberX = Number(x);
        const numberWidth = Number(width);
        const numberHeight = Number(height);


        const adjustedY = numberY - pageData.startPx;
        const adjustedBounds = `${numberX}, ${adjustedY}, ${numberWidth}, ${numberHeight}`;
        const index = labelSuggestionData.sirFields.findIndex((field) => adjustedBounds === field.Bounds);

        const suggestedArea = {
            ...parsedValueSuggestion,
            index: index === -1 ? null : index,
            textContent: parsedValueSuggestion.Text,
        };

        if (index !== -1) {
            // once we have the sir field we want to suggest, we can map it to the DisplayValue by "clicking" on it
            // or in other words, firing off an action that fires off a saga that clicks on it
            yield put(clickSirField(suggestedArea, {isAuto: true, ...activeField}));

            return;
        }

        yield put(selectActiveFieldThenCreateMapping({
            isDrawn: true,
            activeArea: {...activeField, fieldOption: FIELD_OPTIONS.DISPLAY_VALUE},
            area: suggestedArea,
            isFromClick: true,
        }));
    }
}

export function* mapTableFlow({table}) {
    if (table.refColumn) {
        yield put(mapRefColumn(table.refColumn));
    }

    if (table.footer) {
        yield put(selectActiveFieldThenCreateMapping(table.footer));
    }
}

export function* goToEdit(shouldShowLoadingSpinner, shouldReloadBotData, params) {
    yield put(setEditLoadingState(true));
    yield put(setIsLoadingEditData(true));

    yield put(setCurrentPreviewDocument(0));
    yield put(setCurrentPageNumber(1));

    if (shouldReloadBotData) {
        yield call(createBotAndTrainFlow, {projectId: params.id, categoryId: params.botid, username: params.username});
    }

    const docProperties = yield select(selectDocProperties());
    const {Height, Width} = docProperties.PageProperties[0];
    yield put(setDocPageMeta(Height, Width));

    const botData = yield select(selectBotData());

    const {layouts, tables, fields} = botData;

    if (!layouts.length) {
        return;
    }

    yield put(setActiveFieldOption(FIELD_OPTIONS.LABEL));
    const activeField = fields.length ? fields[0].Id : tables[0].Columns[0].Id;
    yield put(setActiveField(activeField, false));
    yield put(setIsLoadingEditData(false));

    return;
}

export function* toggleEditPreviewFlow({mode, shouldShowLoadingSpinner, shouldReloadBotData, params}) {
    yield put(setDesignerViewMode(mode));

    const oldMode = mode === EDIT ? PREVIEW : EDIT;
    browserHistory.push(browserHistory.getCurrentLocation().pathname.replace(oldMode, mode));

    if (mode === EDIT) {
        yield call(goToEdit, shouldShowLoadingSpinner, shouldReloadBotData, params);
    }

}

export function* clickFieldOptionDrawFlow({activeField, fieldOption, mode}) {
    yield put(setActiveFieldOption(fieldOption, true));
    yield put(clearDrawnFields(activeField, fieldOption));
    yield put(setAnnotatorMode(mode, fieldOption));
}

export function* stopDrawFlow({activeField, activeFieldOption: selectedFieldOption = 'DisplayValue', drawnAreas}) {
    const {Id} = activeField;

    // since we only have one drawn area per fieldOption for a given field or column,
    // find the matching drawn area we just drew
    const area = drawnAreas.find((({designerData}) => {
        const {activeFieldOption, activeFieldId} = designerData;

        return Id === activeFieldId && selectedFieldOption === activeFieldOption;
    }));

    if (!area) return;

    // capture the text in teh SIR boxes we are overlapping
    const textContent = !area ? '' : area.designerData && area.designerData.isAuto ? activeField[activeField.fieldOption] : area.overlappedStaticAnnotations.reduce((text, annotation) => {
        return `${text} ${annotation.textContent} `;
    }, '');

    // once we capture the concatonated text within the SIR fields, set the text value
    // to the appropriate fieldOption we are editing...

    // this could be moved to the stopDrag saga, and just call the setFieldValue action followed
    // by a setAnnotatorMode action
    const shouldMarkAsDrawn = area.x !== 0 && area.y !== 0 && area.width !== 0;
    yield put(setFieldValue(activeField, selectedFieldOption, textContent));
    yield put(setFieldValue(activeField, `drawn${selectedFieldOption}`, shouldMarkAsDrawn));
    yield put(setAnnotatorMode(ANNOTATION_MODES.FREE));
}

export function* saveBotFlow(action) {
    const {params, sendToProduction, successDestination} = action;
    const {id, botid} = params;
    yield put(hideAllConfirmationBoxVisibility());
    yield put(setEditSaveSpinnerState(true));

    const botSaveInfo = yield select(selectBotSaveData());
    const currentBotIndex = yield select(selectCurrentBotIndex());
    const projectCategories = yield select(selectProjectCategories());
    const visionBotId = yield select(selectVisionBotId());

    try {
        const saveResult = yield call(designerBackEnd.saveBot, botSaveInfo);
        if (!saveResult.success) throw new Error();
        if (sendToProduction) {
            const result = yield call(setProjectVisionBotStateFromServer, id, botid, visionBotId, 'production');
            if (!result.success) throw new Error();
        }
        switch (successDestination) {
            case SUCCESS_DESTINATION.LI_DETAIL:
                return yield call(browserHistory.push, routeHelper(`/learning-instances/${id}`));
            case SUCCESS_DESTINATION.NEXT:
                yield put(setEditSaveSpinnerState(false));

                return yield put(navigateBots(projectCategories, currentBotIndex, NavigationDirections.NEXT, action.params));
            case SUCCESS_DESTINATION.PREVIEW:
                yield put(setEditSaveSpinnerState(false));

                return;
            default:
                yield put(setEditSaveSpinnerState(false));

                return;
        }
    }
    catch (e) {
        yield put(hideAllConfirmationBoxVisibility());
        // display alert error message
    }
}

export function* saveAndMoveToPreviewFlow({params, username}) {
    yield put(setIsWaitingForPreviewData(true));
    yield call(saveBotFlow, {params});
    yield call(
        setPreviewDataFlow,
        {
            projectId: params.id,
            categoryId: params.botid,
            username,
            pageNumber: 1,
            shouldResetPreviewData: true,
        },
    );

    return yield call(toggleEditPreviewFlow, {mode: VISION_BOT_MODES.PREVIEW});
}
/**
 * return transformed field and table data (removed $type and $values kind of property)
 * @param {object} previewResult
 */
export function transformPreviewResult(previewResult) {
    const previewTransformFields = jsont.transform(previewResult.data.Data.FieldDataRecord.Fields.$values, getPreviewFieldRules()());
    const previewTransTables = jsont.transform(previewResult.data.Data.TableDataRecord.$values, getPreviewTableRules()());
    return {fields: previewTransformFields, tables: previewTransTables};
}

export function* navigateDocumentFlow({currentIndex, projectId, categoryId}) {
    const previewDocs = yield select(selectPreviewDocs());
    const docProperties = yield select(selectDocProperties());
    const vbotId = yield select(selectVisionBotId());
    yield put(setPreviewLoadingState(true));
    yield put(setIsWaitingForPreviewData(true));
    yield put(setCurrentPreviewDocument(currentIndex));
    yield put(clearAllDrawnFields());

    const {Height, Width} = docProperties.PageProperties[0];
    yield put(setDocPageMeta(Height, Width));

    yield put(setCurrentPageNumber(1));

    const previewResult = yield call(
        botsBackEnd.fetchPreviewDataDocument,
        projectId,
        categoryId,
        vbotId,
        previewDocs[currentIndex].fileId,
    );

    yield put(setDocProperties({...previewResult.data.DocumentProperties, PageProperties: previewResult.data.DocumentProperties.PageProperties.$values}));

    const transformedPreviewResult = transformPreviewResult(previewResult);
    yield put(populatePreviewData(transformedPreviewResult));

    const data = transformPreviewData(previewResult);
    const nonStandardPreviewBoxes = findNonStandardPreviewBoxes(previewResult.data.SirFields.Fields.$values, data);

    yield put(updateAllDrawnPreviewAreas(nonStandardPreviewBoxes));
    yield put(populateSirFields(previewResult.data.SirFields.Fields.$values));

    yield put(saveAllSirFields(previewResult.data.SirFields.Fields.$values));

    yield put(setIsWaitingForPreviewData(false));

    return;
}

export function* setPreviewDataFlow(action) {
    const previewDocs = yield select(selectPreviewDocs());
    const currentPreviewDocIndex = yield select(selectCurrentPreviewIndex());
    const vbotId = yield select(selectVisionBotId());
    yield put(setCurrentPageNumber(action.pageNumber));

    if (action.shouldResetPreviewData) {
        yield put(setIsWaitingForPreviewData(true));

        const previewResult = yield call(botsBackEnd.fetchPreviewDataDocument, action.projectId, action.categoryId, vbotId, previewDocs[currentPreviewDocIndex].fileId);
        yield put(setDocProperties({...previewResult.data.DocumentProperties, PageProperties: previewResult.data.DocumentProperties.PageProperties.$values}));

        const transformedPreviewResult = transformPreviewResult(previewResult);
        yield put(populatePreviewData(transformedPreviewResult));

        const data = transformPreviewData(previewResult);
        const nonStandardPreviewBoxes = findNonStandardPreviewBoxes(previewResult.data.SirFields.Fields.$values, data);

        yield put(updateAllDrawnPreviewAreas(nonStandardPreviewBoxes));
        yield put(saveAllSirFields(previewResult.data.SirFields.Fields.$values));
    }

    const docProperties = yield select(selectDocProperties());
    const allSirFields = yield select(selectAllSirFields());

    const pageRange = docProperties.PageProperties.reduce((acc, page, i) => {
        return i >= action.pageNumber ? acc : {start: acc.end, end: acc.end + page.Height};
    }, {start: 0, end: 0});

    const selectedSirFields = allSirFields.filter((field) => {
        const [, y] = field.Bounds.split(', ');

        return y >= pageRange.start && y < pageRange.end;
    });

    yield put(populateSirFields(selectedSirFields));

    yield put(setIsWaitingForPreviewData(false));

    return;
}

/**
 * download csv file for given content
 * @param {string} content
 */
export function downloadContent(content) {
    window.location.assign(routeHelper(`/api/download-csv?csvdata=${encodeURIComponent(content)}`));
}
/**
 * saga flow for preview data export to csv
 * @param {object} action
 */
export function* exportCSVFlow(action) {
    const {projectId, categoryId, data} = action;
    try {
        const result = yield call(designerBackEnd.exportToCSV, projectId, categoryId, data);
        if (result.success) {
            return yield call(downloadContent, result.data);
        }
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
}

export function* setCurrentPageNumberAndSetSirFieldsFlow(action) {
    const {
        pageNumber,
        projectId,
        categoryId,
        username,
        mode,
    } = action;

    if (mode === EDIT) {
        const allSirFields = yield select(selectAllSirFields());
        const docProperties = yield select(selectDocProperties());
        yield put(setEditLoadingState(true));
        yield put(setIsLoadingEditData(true));
        yield put(setCurrentPageNumber(pageNumber));

        const pageRange = docProperties.PageProperties.reduce((acc, page, i) => {
            return i >= pageNumber ? acc : {start: acc.end, end: acc.end + page.Height};
        }, {start: 0, end: 0});

        const selectedSirFields = allSirFields.filter((field) => {
            const [, y] = field.Bounds.split(', ');

            return y >= pageRange.start && y < pageRange.end;
        });

        yield put(populateSirFields(selectedSirFields));
        yield put(setIsLoadingEditData(false));
    }
    else {
        yield put(setPreviewLoadingState(true));
        yield put(setIsWaitingForPreviewData(true));
        yield call(
            setPreviewDataFlow,
            {
                projectId,
                categoryId,
                username,
                pageNumber,
                shouldResetPreviewData: false,
            },
        );
    }
}

// All sagas to be loaded
export default [
    defaultSaga,
    function* () {
        yield* takeLatest(AUTO_CLICK_SIR_FIELD, autoClickSirFieldFlow);
    },
    function* () {
        yield* takeLatest(UPLOAD_FILE_TO_DESIGNER, UploadFileToDesignerFlow);
    },
    function* () {
        yield takeLatest(CREATE_BOT_AND_TRAIN, createBotAndTrainFlow);
    },
    function* () {
        yield takeLatest(REDIRECT_IF_FEATURE_TURNED_OFF, redirectIfFeatureTurnedOffFlow);
    },
    function* () {
        yield takeEvery(CLICK_SIR_FIELD, clickSirFieldFlow);
    },
    function* () {
        yield takeLatest(CLICK_FIELD_OPTION_DRAW, clickFieldOptionDrawFlow);
    },
    function* () {
        yield takeLatest(STOP_DRAG, stopDrawFlow);
    },
    function* () {
        yield takeLatest(SUGGEST_VALUE, suggestValueSaga);
    },
    function* () {
        yield takeLatest(NAVIGATE_BOTS, navigateToBotFlow);
    },
    function* () {
        yield takeEvery(SELECT_ACTIVE_FIELD_THEN_CREATE_MAPPING, selectFieldAndClickFlow);
    },
    function* () {
        yield takeLatest(CLEAR_DRAWN_FIELDS, clearDrawnFieldsSaga);
    },
    function* () {
        yield takeLatest(SAVE_AND_MOVE_TO_PREVIEW, saveAndMoveToPreviewFlow);
    },
    function* () {
        yield takeLatest(SAVE_BOT, saveBotFlow);
    },
    function* () {
        yield takeLatest(MAP_TABLE, mapTableFlow);
    },
    function* () {
        yield takeLatest(TOGGLE_EDIT_PREVIEW, toggleEditPreviewFlow);
    },
    function*() {
        yield takeLatest(NAVIGATE_PREVIEW_DOCUMENT, navigateDocumentFlow);
    },
    function*() {
        yield takeLatest(EXPORT_TO_CSV, exportCSVFlow);
    },
    function*() {
        yield takeLatest(FETCH_CATEGORIES_FOR_DESIGNER, fetchProjectCategoriesForDesignerAndKeepAlive);
    },
    function*() {
        yield takeLatest(SET_CURRENT_PAGE_NUMBER_AND_SET_SIR_FIELDS, setCurrentPageNumberAndSetSirFieldsFlow);
    },
    function*() {
        yield takeLatest(SET_PREVIEW_DATA, setPreviewDataFlow);
    },
];
