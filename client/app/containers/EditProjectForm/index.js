/*
 *
 * EditProjectFormContainer
 *
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm, Field, change} from 'redux-form/immutable';
import {connect} from 'react-redux';
import {fromJS} from 'immutable';
import {
    CommandButton,
    TextField,
    PageTitle,
    FieldLabel,
    Icon,
    CheckboxInput,
} from 'common-frontend-components';
import FontAwesome from 'react-fontawesome';
import {autobind} from 'core-decorators';
import {ALLOWED_FILE_TYPES, MAX_FILES_UPLOAD} from '../ProjectsContainer/constants';
import {fieldValidations} from '../ProjectForm/validations';
import BrowseField from '../../components/BrowseField';
import EnvironmentBadge from '../../components/EnvironmentBadge';
import selectEditProjectForm from './selectors';
import {
    CONFIRMATION_BOX_CANCEL_TEXT,
    EDIT_CONFIRMATION_BOX_CONFIRM_TEXT,
    EDIT_CONFIRMATION_BOX_DESCRIPTION,
    EDIT_CONFIRMATION_BOX_TITLE,
} from './constants';
import {themeType} from '../../components/ActionConfirmationModal';
import ActionConfirmationModal from '../../components/ActionConfirmationModal';
import {QA_EDIT_PROJECT_CUSTOM_FIELDS_ADD} from '../../constants/qaElementIds';
import PageLoadingOverlay from '../../components/PageLoadingOverlay';
import {fetchProjectDomains} from '../ProjectsContainer/actions';

import './styles.scss';
import '../ProjectForm/styles.scss';

class EditProjectForm extends Component {
    static displayName = 'EditProjectForm';

    static propTypes = {
        onCancel: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        initialValues: PropTypes.shape({
            description: PropTypes.string,
            files:PropTypes.array,
            fields: PropTypes.shape({
                standard: PropTypes.array,
                custom: PropTypes.array,
            }),
        }).isRequired,
    };

    state = {
        showEditActionConfirmationDetail:false,
    };

    componentWillMount() {
        this.props.loadProjectDomains();
    }

    /**
     * _renderStandardFields function Render standard form and table fields for current project
     **/
    _renderStandardFields(title, fields) {
        if (fields && fields.length) {
            return (
                <div>
                    <div className="aa-well-header">
                        {title}
                    </div>
                    <div className="aa-project-fields">
                        {fields.map((field) => (
                            <div className="aa-project-fields--field" key={field.id}>
                                <CheckboxInput
                                    checked={field.checked}
                                    onChange={() => this.selectStandardFieldHandler(field.id)}
                                    disabled={field.disabled}
                                >
                                    {field.name}
                                </CheckboxInput>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }

    /**
     * _renderCustomFields function render custom fields for the current projects
    */
    _renderCustomFields() {
        const {currentProjectCustomFields} = this.props;

        return currentProjectCustomFields.map((field, i) => {

            return (
                <div className="aa-project-custom-field aa-grid-row vertical-bottom" key={i}>
                    <div>
                        <FieldLabel label={`${field.type.toLowerCase().replace(/field/, '')} field`}>
                            <Field
                                name="customField[]"
                                component={TextField}
                                placeholder={field.name}
                                readOnly={true}
                            />
                        </FieldLabel>
                    </div>

                    <div>
                        {field.allowDelete ?
                            <CommandButton
                                theme="error"
                                recommended
                                onClick={() => this.removeCustomProjectField(field.name, field.type)}
                            >
                                <FontAwesome
                                    name="close"
                                />
                            </CommandButton>
                            : null}
                    </div>
                </div>
            );
        });
    }

    /**
     * createCustomField function create custom fields for current project
     * @param {string} type type of the fields needs to be created.(form field or table field)
     */
    createCustomField(type) {
        const {
            currentCustomFieldName, formFields, change, formErrors,
        } = this.props;
        if (currentCustomFieldName) {
            const customFields = formFields.custom;

            // check if custom name has validation issues
            // don't add if validations fail
            if (formErrors && formErrors.hasOwnProperty('newCustomFieldName')) {
                return;
            }
            customFields.push({name: currentCustomFieldName, type});
            change('fields.custom', fromJS(customFields));
            change('newCustomFieldName', '');
        }
    }

    /**
     * removeCustomProjectField function remove field from redux form store
     * @param {string} name name of the field
     * @param {string} type type of the field
     */
    removeCustomProjectField(name, type) {
        const {change, formFields} = this.props;
        const customFields = formFields.custom;
        const index = customFields.findIndex((field) => field.name === name && field.type === type);
        if (index !== -1) {
            customFields.splice(index, 1);
            change('fields.custom', fromJS(customFields));
        }
    }

    /**
     * select handler for standard fields (form, table)
     * @param {string} selectedField
     */
    selectStandardFieldHandler(selectedField) {
        const {change, formFields} = this.props;
        const standardFields = formFields.standard;
        const standardFieldIndex = standardFields.indexOf(selectedField);

        if (standardFieldIndex !== -1) {
            standardFields.splice(standardFieldIndex, 1);
            return change('fields.standard', fromJS(standardFields));
        }

        standardFields.push(selectedField);
        return change('fields.standard', fromJS(standardFields));
    }

    /**
     * isNewFieldAdded checks if user has added any new field while editing learning instance.
     * @param {object} originalFields
     * @param {object} formFields
     * @returns {boolean}
     */
    isNewFieldAdded(originalFields, formFields) {
        const isStandardFieldAdded = formFields.standard.length > 0;
        const isCustomFieldAdded = formFields.custom.length > 0;
        return isStandardFieldAdded || isCustomFieldAdded;
    }

    /**
     * save click handler
    */
    @autobind handleSave() {
        const {existingFields, formFields} = this.props;

        this.isNewFieldAdded(existingFields, formFields)
            ? this.setState({showEditActionConfirmationDetail:true})
            : this.submitForm();
    }

    /**
     * confirmation click handler
    */
    @autobind handleActionConfirmationSubmit() {
        this.setState({showEditActionConfirmationDetail:false});
        this.submitForm();
    }

    /**
     * this function submits the form
    */
    @autobind submitForm() {
        const submitter = this.props.handleSubmit(this.props.onSubmit);
        submitter();
    }

    render() {
        const {
            handleSubmit,
            onDelete,
            onCancel,
            projectName,
            environment,
            availableProjectFields,
            isProjectTypeCustom,
            formErrors,
            allFields,
        } = this.props;
        const {showEditActionConfirmationDetail} = this.state;
        const isFieldsAvailable = availableProjectFields.form.length
            || availableProjectFields.table.length
            || isProjectTypeCustom;

        return (
            <form
                className="aa-project-form aa-grid-column"
                onSubmit={handleSubmit}
            >
            {
                !isFieldsAvailable ?
                <PageLoadingOverlay
                    label={'Loading domain fields...'}
                    show={true}
                    fullscreen={false} /> : null
            }
                <div className="aa-grid-row padded space-between aa-view-project-header-container edit">
                    <div className="aa-grid-row vertical-center">
                        <PageTitle label={projectName} />
                        <EnvironmentBadge>{environment}</EnvironmentBadge>
                    </div>
                    <div className="aa-grid-row vertical-center horizontal-right aa-view-project-header-actions ">
                        <div className="aa-grid-row vertical-center">
                            <CommandButton
                                onClick={this.handleSave}
                                theme="success"
                                recommended
                                disabled={formErrors}
                            >
                                <FontAwesome
                                    name="floppy-o"
                                />
                                <span> Save</span>
                            </CommandButton>
                        </div>
                        <div className="aa-grid-row vertical-center">
                                <CommandButton
                                    onClick={onDelete}
                                    theme="error"
                                    recommended
                                >
                                    <FontAwesome
                                        name="trash"
                                    />
                                    <span> Delete Instance</span>
                                </CommandButton>
                        </div>
                        <a
                            id="edit-cancel-project-btn"
                            className="aa-grid-row vertical-center action"
                            onMouseDown={onCancel}
                        >
                            <Icon
                                fa="close"
                            />
                            <span>
                                Cancel
                            </span>
                        </a>
                    </div>
                </div>
                <div className="aa-grid-column vertical-left aa-edit-project-fields">
                    <div className="aa-width-50">
                        <FieldLabel label="Description (Optional)">
                            <Field
                                name="description"
                                component={TextField}
                                validate={[
                                    fieldValidations.length(255),
                                    fieldValidations.extendedFormat,
                                ]}
                            />
                        </FieldLabel>
                    </div>
                    <div className="aa-grid-row vertical-bottom space-between">
                        <FieldLabel
                            tag="span"
                            label="Upload files (Optional)"
                        >
                            <Field
                                type="file"
                                name="files"
                                component={BrowseField}
                                multiple={true}
                                acceptTypes={`.${ALLOWED_FILE_TYPES.join(',.')}`}
                                validate={[
                                    fieldValidations.checkFileSize(),
                                    fieldValidations.fileSetSize(MAX_FILES_UPLOAD),
                                    fieldValidations.checkAcceptedFileTypes(),
                                    fieldValidations.filesSetNameLength(150),
                                ]}
                            />
                        </FieldLabel>
                    </div>
                    <div className="aa-grid-row vertical-bottom space-between">
                        {(() => {
                            if (isFieldsAvailable) {
                                return (
                                    <div className="aa-fields">
                                        <div className="aa-well">
                                            {this._renderStandardFields('Standard form fields', this.props.availableProjectFields.form)}
                                            {this._renderStandardFields('Standard table fields', this.props.availableProjectFields.table)}

                                            <div className="aa-well-header">
                                                {(isProjectTypeCustom) ? 'Instance Fields' : 'Other fields (Optional)'}
                                            </div>

                                            <div className="aa-project-custom-fields">
                                                {this._renderCustomFields()}
                                                <div className="aa-project-custom-fields-add aa-grid-row horizontal-left" id={QA_EDIT_PROJECT_CUSTOM_FIELDS_ADD}>
                                                    <div>
                                                        <FieldLabel
                                                            label=""
                                                        >
                                                            <Field
                                                                component={TextField}
                                                                name="newCustomFieldName"
                                                                validate={[
                                                                    fieldValidations.length(50),
                                                                    fieldValidations.duplicateFieldName(availableProjectFields, allFields),
                                                                    fieldValidations.cannotBeOnlyWhitespace,
                                                                    fieldValidations.noOutsideWhitespace,
                                                                    fieldValidations.formatWithoutSpecialCharacter,
                                                                    fieldValidations.formatNotStartWithNumber,
                                                                    fieldValidations.noRepeatingWhitespace,
                                                                ]}
                                                            />
                                                        </FieldLabel>
                                                    </div>

                                                    <div className="aa-grid-column vertical-center">
                                                        <CommandButton
                                                            recommended
                                                            onClick={() => this.createCustomField('FormField')}
                                                        >
                                                            Add as form
                                                        </CommandButton>
                                                    </div>

                                                    <div className="aa-grid-column vertical-center">
                                                        <CommandButton
                                                            recommended
                                                            onClick={() => this.createCustomField('TableField')}
                                                        >
                                                            Add as table
                                                        </CommandButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            }
                        })()}
                    </div>
                    <ActionConfirmationModal
                        cancelTitle = {CONFIRMATION_BOX_CANCEL_TEXT}
                        confirmTitle = {EDIT_CONFIRMATION_BOX_CONFIRM_TEXT}
                        onCancel={() => this.setState({showEditActionConfirmationDetail:false})}
                        onConfirm={this.handleActionConfirmationSubmit}
                        paragraphs={[EDIT_CONFIRMATION_BOX_DESCRIPTION]}
                        show={showEditActionConfirmationDetail}
                        theme={themeType.info}
                        title={EDIT_CONFIRMATION_BOX_TITLE}
                    />
                </div>
            </form>
        );
    }
}

const form = 'editProjectForm';

const mapPropsToState = selectEditProjectForm();

const mapDispatchToState = (dispatch) => {
    return {
        changeFieldValue(field, value) {
            dispatch(change(form, field, value));
        },
        loadProjectDomains: () => dispatch(fetchProjectDomains()),
    };
};

export default reduxForm({
    form,
})(connect(mapPropsToState, mapDispatchToState)(EditProjectForm));
export {
    EditProjectForm,
};
