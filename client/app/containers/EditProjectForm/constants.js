export const EDIT_CONFIRMATION_BOX_TITLE = 'Do you wish to proceed?';
export const EDIT_CONFIRMATION_BOX_DESCRIPTION = 'You are about to add additional field(s)/table columns(s) to this learning instance. This will necessitate re-training of the bots before you can get any desired output from this addition.';
export const CONFIRMATION_BOX_CANCEL_TEXT = 'No, Cancel';
export const EDIT_CONFIRMATION_BOX_CONFIRM_TEXT = 'Yes, Proceed with Field addition';
