/**
 * Copyright (c) 2017 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

import React from 'react';
import {shallow} from 'enzyme';
import {EditProjectForm} from '../';
import {currentProjectData} from '../../../../fixtures/projects';
import {initialValuesForEditProject} from '../../../../fixtures/form';

describe('Container: EditProjectForm', () => {
    it('should exist', () => {
        expect(EditProjectForm).toBeDefined();
    });

    describe('props', () => {
        // given
        const cancelFn = jest.fn();
        const deleteFn = jest.fn();
        const submitFn = jest.fn();

        const container = shallow(
            <EditProjectForm
                initialValues={initialValuesForEditProject}
                environment={'staging'}
                onCancel={cancelFn}
                onDelete={deleteFn}
                onSubmit={submitFn}
                projectName="2373_test"
                availableProjectFields={{
                    form: [],
                    table: [],
                }}
                formFields={{
                    standard: [],
                    custom: [],
                }}
                existingFields={currentProjectData.fields}
                loadProjectDomains={jest.fn()}
                handleSubmit={() => {
                    return submitFn;
                }}
            />
        );
        it('should receive all the props required', () => {
            const {projectName,
                environment,
                initialValues,
                existingFields,
            } = container.instance().props;
            expect(projectName).toEqual(currentProjectData.name);
            expect(environment).toEqual(currentProjectData.environment);
            expect(initialValues).toEqual(initialValuesForEditProject);
            expect(existingFields).toEqual(currentProjectData.fields);
            expect(container.find('CommandButton')).toHaveLength(2);
        });

        it('should call cancel prop function when cancel is clicked', () => {
            // given
            // when
            container.find('#edit-cancel-project-btn').simulate('mousedown');
            // then
            expect(cancelFn).toHaveBeenCalled();
        });

        it('should call delete prop function when cancel is clicked', () => {
            // given
            // when
            container.find('CommandButton').at(1).simulate('click'); // delete button
            // then
            expect(deleteFn).toHaveBeenCalled();
        });

        it('should submit form if there is no change in field set', () => {
            //container.setProps({formFields:{...currentProjectData.fields}});
            container.find('CommandButton').at(0).simulate('click'); //save button
            expect(submitFn).toHaveBeenCalled();
        });

        it('should show confirmation box if there is change in field set', () => {
            container.setProps({formFields:{
                standard: ['1'],
                custom: []}});
            container.find('CommandButton').at(0).simulate('click'); // save button
            const {showEditActionConfirmationDetail} = container.state();
            expect(showEditActionConfirmationDetail).toEqual(true);
        });
    });
});
