import {fromJS} from 'immutable';

import selectEditProjectForm from '../selectors';
import {ProjectFields2} from '../../../../fixtures/project_fields';
import {currentProjectData} from '../../../../fixtures/projects';

describe('ProjectForm selectors', () => {
    describe('selectProjectForm selector', () => {

        it('should return props for edit project form from current form state', () => {
            // given
            const mockedState = fromJS({
                projectsContainer: {
                    projectFieldsCache: ProjectFields2,
                    currentProject: currentProjectData,
                },
                form: fromJS({
                    editProjectForm: {
                        values: {
                            description: 'desc',
                            newCustomFieldName: 'custom name',
                            fields: {
                                standard: [],
                                custom: [],
                            },
                        },
                        syncErrors: {},
                    },
                }),
            });
            // when
            const selector = selectEditProjectForm();
            // then
            expect(selector(mockedState)).toEqual({
                availableProjectFields: {
                    form: [],
                    table: [],
                },
                currentCustomFieldName: 'custom name',
                formFields: {
                    standard: [],
                    custom: [],
                },
                isProjectTypeCustom: false,
                formErrors: {},
                existingFields: currentProjectData.fields,
                projectName: currentProjectData.name,
                environment: currentProjectData.environment,
                currentProjectCustomFields:[{...currentProjectData.fields.custom[0], allowDelete: false}],
                allFields: {
                    standard: currentProjectData.fields.standard,
                    custom: [{...currentProjectData.fields.custom[0], allowDelete: false}],
                },
            });
        });
    });
});
