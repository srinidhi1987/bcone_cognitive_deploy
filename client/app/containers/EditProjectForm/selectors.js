import {Map, List} from 'immutable';
import {createSelector} from 'reselect';
import {formValueSelector} from 'redux-form/immutable';

const formSelector = formValueSelector('editProjectForm');

/**
 * Direct selector to state domain
 */
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');
const getProjectFieldsDomain = () => (state) => state.getIn(['projectsContainer', 'projectFieldsCache']);

const getCurrentProject = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => {
        return projectState.get('currentProject');
    }
);

const getCurrentCustomFieldName = () => (state) => formSelector(state, 'newCustomFieldName');
const getCurrentProjectFormFieldsState = () => (state) => formSelector(state, 'fields');

const getProjectType = () => createSelector(getCurrentProject(), (currentProjectState) => {
    const currentProject = Map.isMap(currentProjectState) ? currentProjectState.toJS() : currentProjectState;
    return {
        projectType:currentProject.projectType,
        projectTypeId: currentProject.projectTypeId,
    };
});

const getProjectFormErrors = () => (state) => state.getIn(['form', 'editProjectForm', 'syncErrors']);

const getExistingFields = () => createSelector(getCurrentProject(),
    (currentProjectState) => {
        const currentProject = Map.isMap(currentProjectState) ? currentProjectState.toJS() : currentProjectState;
        return currentProject.fields;
    }
);

const getAllCurrentProjectStandardFields = () => createSelector(
    getExistingFields(),
    getCurrentProjectFormFieldsState(),
    (existingFields, fields) => {
        const formFields = fields.toJS();
        return [...existingFields.standard, ...formFields.standard];
    }
);

const getAllCurrentProjectCustomFields = () => createSelector(
    getExistingFields(),
    getCurrentProjectFormFieldsState(),
    (existingFields, fields) => {
        const formFields = fields.toJS();
        return [...existingFields.custom, ...formFields.custom].reduce((acc, field) => {
            const allowDelete = existingFields.custom.findIndex((customField) => customField.name === field.name && customField.type === field.type) === -1;
            acc.push({...field, allowDelete});
            return acc;
        }, []);
    }
);

const getAllCurrentProjectFields = () => createSelector(
    getAllCurrentProjectStandardFields(),
    getAllCurrentProjectCustomFields(),
    (standard, custom) => {
        return {
            custom: custom ? custom : [],
            standard: standard ? standard : [],
        };
    }
);

const getAvailableProjectFields = () => createSelector(
    getProjectFieldsDomain(),
    getProjectType(),
    getExistingFields(),
    getAllCurrentProjectStandardFields(),
(projectState, projectType, existingFields, currentProjectAllStandardFields) => {
    const aliasFields = List.isList(projectState) ? projectState.toJS() : projectState;
    return aliasFields.reduce((acc, projectField) => {
        if (projectField.name === projectType.projectType) {
            projectField.fields.forEach((field) => {
                const checked = (currentProjectAllStandardFields.indexOf(field.id) !== -1) ? 'checked' : '';
                const disabled = existingFields.standard.indexOf(field.id) !== -1;
                switch (field.fieldType) {
                    case 'FormField':
                        acc.form.push({...field, checked, disabled});
                        break;
                    case 'TableField':
                        acc.table.push({...field, checked, disabled});
                        break;
                }
            });
        }
        return acc;
    }, {
        form: [],
        table: [],
    });
});
/**
 * Other specific selectors
 */

/**
 * Default selector
 */
const selectEditProjectForm = () => createSelector(
    getCurrentCustomFieldName(),
    getCurrentProjectFormFieldsState(),
    getProjectType(),
    getProjectFormErrors(),
    getAvailableProjectFields(),
    getExistingFields(),
    getCurrentProject(),
    getAllCurrentProjectCustomFields(),
    getAllCurrentProjectFields(),
    (
        customFieldName,
        fields,
        projectType,
        projectFormErrors,
        availableProjectFields,
        existingFields,
        currentProjectState,
        currentProjectCustomFields,
        allFields,
    ) => {
        const formErrors = Map.isMap(projectFormErrors) ? projectFormErrors.toJS() : projectFormErrors;
        const currentProject = Map.isMap(currentProjectState) ? currentProjectState.toJS() : currentProjectState;
        return {
            isProjectTypeCustom: projectType.projectTypeId === '0',
            availableProjectFields,
            currentCustomFieldName: customFieldName,
            formFields: fields.toJS(),
            formErrors,
            existingFields,
            projectName: currentProject.name,
            environment: currentProject.environment,
            currentProjectCustomFields,
            allFields,
        };
    }
);

export default selectEditProjectForm;
export {
    selectEditProjectForm,
    getProjectFieldsDomain,
    getCurrentCustomFieldName,
    getCurrentProjectFormFieldsState,
    getProjectType,
};
