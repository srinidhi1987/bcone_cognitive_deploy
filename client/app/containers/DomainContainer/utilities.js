import React from 'react';
import {IconButton} from 'common-frontend-components';
import {DataField, DataType} from 'common-frontend-utils';

const IMAGES_AND_PDF = 'Images and PDFs';

export const transformDomains = (domains) => domains.map((domain) => { // eslint-disable-line
    const outputDictionary = Math.random() > 0.5 ? 'User selection' : 'Fixed';
    const extractionGenerator = Math.random();
    const extractionType = extractionGenerator < 0.33 ? 'Computer vision'
        : extractionGenerator > 0.66 ? 'Computer vision + advanced tables'
        : 'Computer vision + pattern recognition';

    return {...domain, outputDictionary, extractionType, sourceType: IMAGES_AND_PDF};
});

export const getDomainDataTableFields = (exportDomain) => { // eslint-disable-line react/display-name
    return [
        new DataField({id: 'name', type: DataType.STRING, label: 'DOMAIN NAME'}),
        new DataField({id: 'languages', type: DataType.STRING, label: 'LANGUAGE'}),
        new DataField({id: 'export', type: DataType.STRING, label: 'EXPORT', render: (id, data) => { // eslint-disable-line react/display-name
            return (
                <IconButton className="domain-listing-icons-menu-icon" aa="action-export" onClick={() => exportDomain(data)}/>
            );
        }}),
    ];
};

export const getDomainDataFilterFields = () => {
    return [
        new DataField({id: 'name', type: DataType.STRING, label: 'DOMAIN NAME'}),
        new DataField({id: 'languages', type: DataType.STRING, label: 'LANGUAGE'}),
    ];
};

export const getFilteredDomains = (domains, filter) => {
    return domains.filter(DataField.filter(getDomainDataTableFields(), filter));
};

export const isServerConnectionError = (result) => {
    return result.errors[0] === 'server connection error';
};
