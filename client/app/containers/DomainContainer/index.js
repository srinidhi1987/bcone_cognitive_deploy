/*
 *
 * DomainContainer
 *
 */

import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {autobind} from 'core-decorators';
import {
    Breadcrumbs,
    DataTable,
    DataFilter,
    Icon,
    PageTitle,
} from 'common-frontend-components';
import selectDomainContainer from './selectors';
import {
    exportDomain,
    fetchDomains,
    importNewDomain,
    setReducerStoreProperty,
} from './actions';
import {getDomainDataFilterFields, getDomainDataTableFields} from './utilities';
import {DataTableCopyText} from '../../constants/enums';
const {
    SEARCH_ALL_PLACEHOLDER,
    ALL_COLUMNS_DROPDOWN_VALUE,
} = DataTableCopyText;
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class DomainContainer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DomainContainer';

    constructor(props) {
        super(props);

        this.state = {selected: []};
        this.domainDataTableFields = getDomainDataTableFields(this.props.exportDomain);
        this.domainDataFilterFields = getDomainDataFilterFields();
    }
    componentWillMount() {
        this.props.fetchDomains();
    }

    render() {
        return (
            <div>
                <Breadcrumbs>
                    <Link to={routeHelper('/domains')}>Domains</Link>
                </Breadcrumbs>
                <div id="aa-main-container">
                    <div className="aa-grid-row">
                        <PageTitle label="Domains" />
                        {this.renderCreateAndImportButtons()}
                    </div>
                    {this.renderDomainListing(this.props)}
                </div>
            </div>
        );
    }

    renderDomainListing({domains, domainFilter}) {
        return (
            <div className="domain-container">
                <DataFilter
                    fields={this.domainDataFilterFields}
                    searchAll
                    value={domainFilter}
                    labelColumnsAll={ALL_COLUMNS_DROPDOWN_VALUE}
                    labelSearchAll={SEARCH_ALL_PLACEHOLDER}
                    onChange={this.handleChangeFilter}
                />
                {this.renderDomainListingsIconsMenu()}
                <DataTable
                    onSelect={(selected) => this.setState({selected})}
                    selected={this.state.selected}
                    data={domains}
                    fields={this.domainDataTableFields}
                />
            </div>
        );
    }

    renderCreateAndImportButtons() {
        return (
            <span className="import-create-buttons">
                <label>
                    <Icon aa="action-import" />
                    Import new Domain
                    <input ref="ref" className="domain-import-input" accept=".dom" type="file" onChange={this.handleImportNewDomain} />
                </label>
                <label>
                    <Icon aa="action-create" />
                    Create new Domain
                </label>
            </span>
        );

    }

    renderDomainListingsIconsMenu() {
        return (
            <span className="domain-listing-icons-menu">
                <Icon className="domain-listing-icons-menu-icon" aa="action-refresh" />
                <Icon className="domain-listing-icons-menu-icon" aa="action-delete" />
                <Icon className="domain-listing-icons-menu-icon" aa="action-customize-table" />
            </span>
        );
    }

    @autobind handleChangeFilter(filter) {
        this.props.setReducerStoreProperty('domainFilter', filter);
    }

    @autobind handleImportNewDomain(event) {
        const files = event.target.files;

        this.props.importNewDomain(files);
    }
}

const mapStateToProps = selectDomainContainer();

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        fetchDomains: () => dispatch(fetchDomains()),
        importNewDomain: (domainFile) => dispatch(importNewDomain(domainFile)),
        exportDomain: (domain) => dispatch(exportDomain(domain)),
        setReducerStoreProperty: (property, value) => dispatch(setReducerStoreProperty(property, value)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DomainContainer);
