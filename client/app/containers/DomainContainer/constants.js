/*
 *
 * DomainContainer constants
 *
 */

export const DEFAULT_ACTION = 'app/DomainContainer/DEFAULT_ACTION';
export const FETCH_DOMAINS = 'app/DomainContainer/FETCH_DOMAINS';
export const POPULATE_DOMAINS = 'app/DomainContainer/POPULATE_DOMAINS';
export const SET_REDUCER_STORE_PROPERTY = 'app/DomainContainer/SET_REDUCER_STORE_PROPERTY';
export const IMPORT_DOMAIN = 'app/DomainContainer/IMPORT_NEW_DOMAIN';
export const EXPORT_DOMAIN = 'app/DomainContainer/EXPORT_DOMAIN';
