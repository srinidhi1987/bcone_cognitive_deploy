/*
 *
 * DomainContainer actions
 *
 */

import {transformDomains} from './utilities';

import {
  DEFAULT_ACTION,
  EXPORT_DOMAIN,
  FETCH_DOMAINS,
  IMPORT_DOMAIN,
  POPULATE_DOMAINS,
  SET_REDUCER_STORE_PROPERTY,
} from './constants';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION,
    };
}

export function fetchDomains() {
    return {
        type: FETCH_DOMAINS,
    };
}

export function populateDomains(domains) {
    return {
        type: POPULATE_DOMAINS,
        domains: transformDomains(domains),
    };
}

export function setReducerStoreProperty(property, value) {
    return {
        type: SET_REDUCER_STORE_PROPERTY,
        property,
        value,
    };
}

export function importNewDomain(files) {
    return {
        type: IMPORT_DOMAIN,
        files,
    };
}

export function exportDomain(domain) {
    return {
        type: EXPORT_DOMAIN,
        domain,
    };
}
