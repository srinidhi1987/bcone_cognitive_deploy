import {isServerConnectionError} from '../utilities';

describe('domain utilities', () => {
    describe('isServerConnectionError', () => {
        it('should give error if sql db is down', () => {
            const result = {errors: ['server connection error']};

            expect(isServerConnectionError(result)).toEqual(true);
        });
    });
});
