import {call, put} from 'redux-saga/effects';
import {displayAlert} from '../../AlertsContainer/actions';
import {ALERT_LEVEL_DANGER} from '../../AlertsContainer/constants';
import domainsBackEnd from '../../../resources/domainsBackEnd';
/**
 * DomainContainer sagas
 */

import {importDomainFlow} from '../sagas';

describe('importDomainFlow Saga errors', () => {
    const mockFile = new File([''], 'file1', {type: 'text/html'});
    const action = {files: {file1: mockFile}};
    const formData = new FormData();
    const {files} = action;

    Object.keys(files).forEach((key) => {
        if (files[key] instanceof File) {
            formData.append('files', files[key], files[key].name);
        }
    });

    describe('alias service is down', () => {
        const importGenerator = importDomainFlow(action);

        it('should call import service', () => {
            const result = importGenerator.next();

            expect(result.value).toEqual(call(domainsBackEnd.importDomain, formData));
        });

        it('should give connection error message if there is service connection error', () => {
            const result = importGenerator.next({errors: ['server connection error']});

            expect(result.value).toEqual(put(displayAlert(`Error: Domain ${files[Object.keys(files)[0]].name.split('.')[0]} couldn't be imported due to service failure`, ALERT_LEVEL_DANGER)));
        });
    });

    describe('multiple errors are received', () => {
        const importGenerator = importDomainFlow(action);

        it('should call import service', () => {
            const result = importGenerator.next();

            expect(result.value).toEqual(call(domainsBackEnd.importDomain, formData));
        });

        it('should give ... x more errors message first since it will appear on bottom', () => {
            const result = importGenerator.next({errors: ['alias should not match field name', 'another error']});

            expect(result.value).toEqual(put(displayAlert('...1 more errors', ALERT_LEVEL_DANGER)));
        });

        it('should give first error message after x more errors message', () => {
            const result = importGenerator.next();

            expect(result.value).toEqual(put(displayAlert('Error: alias should not match field name', ALERT_LEVEL_DANGER)));
        });
    });
});
