import {call, put} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {
    populateDomains,
} from './actions';
import {
    EXPORT_DOMAIN,
    FETCH_DOMAINS,
    IMPORT_DOMAIN,
} from './constants';
import domainsBackEnd from '../../resources/domainsBackEnd';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import {displayAlert} from '../AlertsContainer/actions';
import {ALERT_LEVEL_COMMON, ALERT_LEVEL_DANGER} from '../../containers/AlertsContainer/constants';
import routeHelper from '../../utils/routeHelper';
import {isServerConnectionError} from './utilities';

// Individual exports for testing
export function* defaultSaga() { // eslint-disable-line require-yield
    return;
}

export function* fetchDomainsFlow() {
    const result = yield call(domainsBackEnd.get);
    const domainData = result.data.map((data) => {
        return {
            ...data,
            languages: data.languages.reduce((acc, lang) => {
                return `${acc}${acc.length ? ', ' : ''}${lang.name}`;
            }, '')};
    });

    yield put(populateDomains(domainData));
}

export function* importDomainFlow({files}) {
    if (!files) return new Promise((resolve) => resolve());

    const formData = new FormData();

    Object.keys(files).forEach((key) => {
        if (files[key] instanceof File) {
            formData.append('files', files[key], files[key].name);
        }
    });

    const result = yield call(domainsBackEnd.importDomain, formData);

    if (result.errors && result.errors.length) {
        if (isServerConnectionError(result)) {
            yield put(displayAlert(`Error: Domain ${files[Object.keys(files)[0]].name.split('.')[0]} couldn't be imported due to service failure`, ALERT_LEVEL_DANGER));
        }
        else if (Array.isArray(result.errors)) {
            const length = result.errors.length;

            if (length > 1) {
                yield put(displayAlert(`...${length - 1} more errors`, ALERT_LEVEL_DANGER));
            }

            yield put(displayAlert(`Error: ${result.errors[0]}`, ALERT_LEVEL_DANGER));
        }

        return;
    }
    const parsedText = JSON.parse(result.response);
    yield put(displayAlert(`Domain ${parsedText.name} imported succesfully`, ALERT_LEVEL_COMMON));
    yield call(fetchDomainsFlow);
}

export function* exportDomainFlow({domain}) {
    const exportResult = yield call(domainsBackEnd.exportDomain, domain.name);

    if (!exportResult.errors) {
        const token = JSON.parse(localStorage.getItem(APP_STORAGE.token));
        yield put(displayAlert(`Domain ${domain.name} exported succesfully`, ALERT_LEVEL_COMMON));

        window.location.assign(routeHelper(`/api/domains/export/${domain.name}?token=${token}`));
    }
    else {
        if (exportResult.errors.length && isServerConnectionError(exportResult)) {
            yield put(displayAlert(`Error: Domain ${domain.name} couldn't be exported due to service failure`, ALERT_LEVEL_DANGER));
        }
    }
}

// All sagas to be loaded
export default [
    defaultSaga,
    function* () {
        yield* takeLatest(FETCH_DOMAINS, fetchDomainsFlow);
    },
    function* () {
        yield* takeLatest(IMPORT_DOMAIN, importDomainFlow);
    },
    function* () {
        yield* takeLatest(EXPORT_DOMAIN, exportDomainFlow);
    },
];
