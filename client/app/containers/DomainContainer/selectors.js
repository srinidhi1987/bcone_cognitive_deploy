import {createSelector} from 'reselect';
import {getFilteredDomains} from './utilities';

/**
 * Direct selector to the domainContainer state domain
 */
const selectDomains = () => (state) => state.get('domainContainer').get('domains').toJS();
const selectDomainFilter = () => (state) => state.get('domainContainer').get('domainFilter').toJS();

/**
 * Other specific selectors
 */
const selectFilteredDomains = () => createSelector(
    selectDomains(),
    selectDomainFilter(),
    getFilteredDomains
);

/**
 * Default selector used by DomainContainer
 */
const selectDomainContainer = () => createSelector(
    selectFilteredDomains(),
    selectDomainFilter(),
    (domains, domainFilter) => ({
        domains,
        domainFilter,
    })
);

export default selectDomainContainer;
