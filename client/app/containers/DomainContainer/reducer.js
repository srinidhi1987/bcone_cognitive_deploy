/*
 *
 * DomainContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
  DEFAULT_ACTION,
  POPULATE_DOMAINS,
  SET_REDUCER_STORE_PROPERTY,
} from './constants';

const initialState = fromJS({
    domains: [],
    domainFilter: [],
});

function domainContainerReducer(state = initialState, action) {
    switch (action.type) {
        case DEFAULT_ACTION:
            return state;
        case POPULATE_DOMAINS:
            return state.set('domains', fromJS(action.domains));
        case SET_REDUCER_STORE_PROPERTY: {
            const parsedVal = action.value instanceof Object ? fromJS(action.value) : action.value;
            return state.set(action.property, parsedVal);
        }

        default:
            return state;
    }
}

export default domainContainerReducer;
