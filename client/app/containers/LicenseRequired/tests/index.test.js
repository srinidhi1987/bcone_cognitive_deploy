jest.mock('iqBotConfig');

import React from 'react';
import {shallow} from 'enzyme';
import LicenseRequired from '../index';
import {CommandButton} from 'common-frontend-components';
import Helmet from 'react-helmet';

describe('<LicenseRequired />', () => {
    const routeProp = {
        name: 'fizzBuzz',
        path: '/fizz/buzz',
    };
    const wrapper = shallow(<LicenseRequired route={routeProp} />);

    it('should be defined', () => {
        expect(LicenseRequired).toBeDefined();
    });

    describe('helmet headers', () => {
        const {title} = wrapper.find(Helmet).props();

        it('should render page title', () => {
            expect(title).toEqual('Automation Anywhere | License Required');
        });
    });

    describe('Control Room Button', () => {
        const {onClick} = wrapper.find(CommandButton).props();

        it('should navigate to the Control Room', () => {
            window.location.assign = jest.fn();
            onClick();
            expect(location.assign.mock.calls.length).toEqual(1);
        });
    });
});
