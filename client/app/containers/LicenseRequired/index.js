/**
 * LicenseRequired
 *
 * This is the page we show when the user does not have an appropriate iqbot license
 *
 */

import React, {PureComponent} from 'react';
import {
    CommandButton,
    Message,
    Modal,
    PageTitle,
} from 'common-frontend-components';
import {themeType} from '../../components/ActionConfirmationModal';
import Helmet from 'react-helmet';
import {autobind} from 'core-decorators';
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {CONTROL_ROOM_URL} = iqBotConfig;

import './styles.scss';

export default class LicenseRequired extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'LicenseRequired';

    render() {
        return (
            <div className="aa-license-required-container">

                <Helmet
                    title="Automation Anywhere | License Required"
                />

                <Modal
                    className="aa-license-required-modal"
                    theme="default"
                    show={true}
                    onHide={this.goToControlRoom}
                    closable={false}
                >
                    <div className="aa-grid-row space-between vertical-center">
                        <PageTitle label="Access Denied" />
                    </div>

                    <Message
                        theme={themeType.error}
                        title=""
                    >
                        <div>
                            <p>You are not authorized to login to IQ Bot Platform. Please contact you administrator for access.</p>

                            <CommandButton
                                onClick={this.goToControlRoom}
                                theme={themeType.error}
                            >
                                Go to Control Room
                            </CommandButton>
                        </div>
                    </Message>
                </Modal>
            </div>
        );
    }

    @autobind
    goToControlRoom() {
        window.location.assign(CONTROL_ROOM_URL);
    }
}
