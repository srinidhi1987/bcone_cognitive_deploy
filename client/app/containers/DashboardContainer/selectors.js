import {createSelector} from 'reselect';
import {sortByEnvironmentAndName} from '../../utils/dashboard';
/**
 * Direct selector to the dashboardContainer state domain
 */
const selectDashboardContainerDomain = () => (state) => state.get('dashboardContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');

/**
 * Other specific selectors
 */
const selectProjects = () => createSelector(
    selectDashboardContainerDomain(),
    (dashboardState) => {
        return dashboardState.get('projects').toJS().sort(sortByEnvironmentAndName);
    }
);

/**
 * Default selector used by DashboardContainer
 */

const selectDashboardContainer = () => createSelector(
    selectDashboardContainerDomain(),
    selectProjectsContainerDomain(),
    selectProjects(),
    (substate, projectState, projects) => {
        return {
            ...substate.toJS(),
            projects,
            projectFields: projectState.get('projectFieldsCache'),
        };
    }
);

export default selectDashboardContainer;
export {
    selectDashboardContainer,
    selectDashboardContainerDomain,
};
