/*
 *
 * DashboardContainer constants
 *
 */
export const FETCH_DASHBOARD_TOTALS = 'app/DashboardContainer/FETCH_DASHBOARD_TOTALS';
export const LOADING_DASHBOARD_TOTALS = 'app/DashboardContainer/LOADING_DASHBOARD_TOTALS';
export const PERSIST_DASHBOARD_TOTALS = 'app/DashboardContainer/PERSIST_DASHBOARD_TOTALS';
export const LOADING_DASHBOARD_PROJECTS = 'app/DashboardContainer/LOADING_DASHBOARD_PROJECTS';
export const FETCH_DASHBOARD_PROJECTS = 'app/DashboardContainer/FETCH_DASHBOARD_PROJECTS';
export const PERSIST_DASHBOARD_PROJECTS = 'app/DashboardContainer/PERSIST_DASHBOARD_PROJECTS';
