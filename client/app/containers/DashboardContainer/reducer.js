/*
 *
 * DashboardContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
    LOADING_DASHBOARD_TOTALS,
    PERSIST_DASHBOARD_TOTALS,
    LOADING_DASHBOARD_PROJECTS,
    PERSIST_DASHBOARD_PROJECTS,
} from './constants';

export const initialState = fromJS({
    dashboardTotalsLoading: false,
    dashboardTotals: {},
    projectsLoading: false,
    projects: [],
});

function dashboardContainerReducer(state = initialState, action) {
    switch (action.type) {
        case LOADING_DASHBOARD_TOTALS:
            return state.set('dashboardTotalsLoading', action.loading);
        case PERSIST_DASHBOARD_TOTALS:
            return state.set('dashboardTotals', action.payload);
        case LOADING_DASHBOARD_PROJECTS:
            return state.set('projectsLoading', action.loading);
        case PERSIST_DASHBOARD_PROJECTS:
            return state.set('projects', fromJS(action.payload));
        default:
            return state;
    }
}

export default dashboardContainerReducer;
