import {fromJS} from 'immutable';

import {
    LOADING_DASHBOARD_TOTALS,
    PERSIST_DASHBOARD_TOTALS,
    LOADING_DASHBOARD_PROJECTS,
    PERSIST_DASHBOARD_PROJECTS,
} from '../constants';
import dashboardContainerReducer, {initialState} from '../reducer';

describe('dashboardContainerReducer', () => {
    it('returns the initial state', () => {
        expect(dashboardContainerReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle LOADING_DASHBOARD_TOTALS and set loading state', () => {
        // given
        const action = {
            type: LOADING_DASHBOARD_TOTALS,
            loading: true,
        };
        const expectedState = fromJS({
            ...initialState.toJS(),
            dashboardTotalsLoading: true,
        });
        // when
        const result = dashboardContainerReducer(initialState, action);
        // then
        expect(result).toEqual(expectedState);
    });

    it('should handle PERSIST_DASHBOARD_TOTAL and set totals', () => {
        // given
        const payload = {
            totalCategoriesCount: 52,
            totalFilesCount: 245,
            totalFilesUnprocessed: 0,
            totalFilesFailed: 0,
        };
        const action = {
            type: PERSIST_DASHBOARD_TOTALS,
            payload,
        };
        const expectedState = {
            ...initialState.toJS(),
            dashboardTotals: payload,
        };
        // when
        const result = dashboardContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle LOADING_DASHBOARD_PROJECTS and set loading state', () => {
        // given
        const action = {
            type: LOADING_DASHBOARD_PROJECTS,
            loading: true,
        };
        const expectedState = {
            ...initialState.toJS(),
            projectsLoading: true,
        };
        // when
        const result = dashboardContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle PERSIST_DASHBOARD_PROJECTS and popular projects in store', () => {
        // given
        const payload = [{}, {}, {}];
        const action = {
            type: PERSIST_DASHBOARD_PROJECTS,
            payload,
        };
        const expectedState = {
            ...initialState.toJS(),
            projects: payload,
        };
        // when
        const result = dashboardContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });
});
