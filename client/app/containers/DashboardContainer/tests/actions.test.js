import {
    FETCH_DASHBOARD_TOTALS,
    LOADING_DASHBOARD_TOTALS,
    PERSIST_DASHBOARD_TOTALS,
    FETCH_DASHBOARD_PROJECTS,
    LOADING_DASHBOARD_PROJECTS,
    PERSIST_DASHBOARD_PROJECTS,
} from '../constants';
import {
    fetchDashboardTotals,
    loadingDashboardTotals,
    persistDashboardTotals,
    fetchDashboardProjects,
    loadingDashboardProjects,
    persistDashboardProjects,
} from '../actions';

describe('DashboardContainer actions', () => {
    describe('fetchDashboardTotals action creator', () => {
        it('should return type FETCH_DASHBOARD_TOTALS', () => {
            // given
            const domain = 'invoice';
            const expectedAction = {
                type: FETCH_DASHBOARD_TOTALS,
                domain,
            };
            // when
            const result = fetchDashboardTotals(domain);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('loadingDashboardTotals action creator', () => {
        it('should return type LOADING_DASHBOARD_TOTALS and set loading state', () => {
            // given
            const expectedAction = {
                type: LOADING_DASHBOARD_TOTALS,
                loading: true,
            };
            // when
            const result = loadingDashboardTotals(true);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistDashboardTotals action creator', () => {
        it('should return type PERSIST_DASHBOARD_TOTALS and persist payload', () => {
            // given
            const payload = {
                totalsFilesUploaded: 100,
            };
            const expectedAction = {
                type: PERSIST_DASHBOARD_TOTALS,
                payload,
            };
            // when
            const result = persistDashboardTotals(payload);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('fetchDashboardProjects action creator', () => {
        it('should return type to fetch projects', () => {
            // given
            const expectedAction = {
                type: FETCH_DASHBOARD_PROJECTS,
            };
            // when
            const result = fetchDashboardProjects();
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('loadingDashboardProjects action creator', () => {
        it('should return expected type and loading boolean', () => {
            // given
            const loading = true;
            const expectedAction = {
                type: LOADING_DASHBOARD_PROJECTS,
                loading,
            };
            // when
            const result = loadingDashboardProjects(loading);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('persistDashboardProjects action creator', () => {
        it('should return payload and correct type', () => {
            // given
            const payload = [{}, {}];
            const expectedAction = {
                type: PERSIST_DASHBOARD_PROJECTS,
                payload,
            };
            // when
            const result = persistDashboardProjects(payload);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
