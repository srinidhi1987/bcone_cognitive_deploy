import {fromJS} from 'immutable';

import {initialState as projectsInitialState} from '../../ProjectsContainer/reducer';
import {initialState as dashboardInitialState} from '../../DashboardContainer/reducer';
import selectDashboardContainer from '../selectors';

describe('selectDashboardContainerDomain selectors', () => {
    describe('selectDashboardContainer selector', () => {
        // given
        const mockState = fromJS({
            projectsContainer: projectsInitialState.toJS(),
            dashboardContainer: dashboardInitialState.toJS(),
        });
        // when
        const selector = selectDashboardContainer();

        it('should return dashboard state from store', () => {
            // then
            Object.keys(dashboardInitialState.toJS()).forEach((e) => {
                expect(selector(mockState)).toHaveProperty(e);
            });
        });

        it('should return project fields from store', () => {
            // then
            expect(selector(mockState).projectFields).toEqual(projectsInitialState.get('projectFieldsCache'));
        });
    });
});
