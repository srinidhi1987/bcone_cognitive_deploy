/**
 * DashboardContainer sagas
 */

import {call, put} from 'redux-saga/effects';

import {FETCH_DASHBOARD_TOTALS} from '../constants';
import {
    loadingDashboardTotals,
    persistDashboardTotals,
    loadingDashboardProjects,
    persistDashboardProjects,
} from '../actions';
import {
    fetchDashboardTotalsFlow,
    fetchDashboardTotalsFromServer,
    fetchDashboardProjectsFlow,
    fetchDashboardProjectsFromServer,
} from '../sagas';

describe('DashboardContainer Sagas', () => {
    describe('fetchDashboardTotalsFlow generator', () => {
        describe('success path', () => {
            // given
            const action = {
                type: FETCH_DASHBOARD_TOTALS,
                domain: 'invoice',
            };
            const generator = fetchDashboardTotalsFlow(action);

            it('should set dashboard loading to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingDashboardTotals(true)));
            });

            it('should call API to get dashboard totals', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchDashboardTotalsFromServer, action.domain));
            });

            it('should persist totals', () => {
                // given
                const response = {
                    success: true,
                    data: {
                        totalFilesUploaded: 100,
                    },
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(persistDashboardTotals(response.data)));
            });

            it('should set dashboard loading to false', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingDashboardTotals(false)));
            });
        });
    });

    describe('fetchDashboardProjectsFlow generator', () => {
        describe('success path', () => {
            // given
            const generator = fetchDashboardProjectsFlow();

            it('should set project loading to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingDashboardProjects(true)));
            });

            it('should call API to fetch projects from server', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(fetchDashboardProjectsFromServer));
            });

            it('should persist project results', () => {
                // given
                const response = {
                    data: [{}],
                    success: true,
                };
                // when
                const result = generator.next(response);
                // then
                expect(result.value).toEqual(put(persistDashboardProjects(response.data)));
            });

            it('should set project loading to true', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(loadingDashboardProjects(false)));
            });
        });
    });
});
