/*
 *
 * DashboardContainer actions
 *
 */

import {
    FETCH_DASHBOARD_TOTALS,
    LOADING_DASHBOARD_TOTALS,
    PERSIST_DASHBOARD_TOTALS,
    FETCH_DASHBOARD_PROJECTS,
    LOADING_DASHBOARD_PROJECTS,
    PERSIST_DASHBOARD_PROJECTS,
} from './constants';

export function fetchDashboardTotals(domain) {
    return {
        type: FETCH_DASHBOARD_TOTALS,
        domain,
    };
}

export function loadingDashboardTotals(loading = false) {
    return {
        type: LOADING_DASHBOARD_TOTALS,
        loading,
    };
}

export function persistDashboardTotals(payload = {}) {
    return {
        type: PERSIST_DASHBOARD_TOTALS,
        payload,
    };
}

export function loadingDashboardProjects(loading = false) {
    return {
        type: LOADING_DASHBOARD_PROJECTS,
        loading,
    };
}

export function fetchDashboardProjects() {
    return {
        type: FETCH_DASHBOARD_PROJECTS,
    };
}

export function persistDashboardProjects(payload = []) {
    return {
        type: PERSIST_DASHBOARD_PROJECTS,
        payload,
    };
}
