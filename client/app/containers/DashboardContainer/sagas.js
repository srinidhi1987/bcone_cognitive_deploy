import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestGet} from '../../utils/request';

import {handleApiResponse, handleApiError} from '../../utils/apiHandlers';
import {FETCH_DASHBOARD_TOTALS, FETCH_DASHBOARD_PROJECTS} from './constants';
import {
    loadingDashboardTotals,
    persistDashboardTotals,
    loadingDashboardProjects,
    persistDashboardProjects,
} from './actions';
import routeHelper from '../../utils/routeHelper';

// Individual exports for testing
export function fetchDashboardTotalsFromServer(domain = 'invoice') {
    return requestGet(
        routeHelper('api/reporting/project-totals'),
        {
            headers: HEADERS_JSON,
            query: {
                domain,
            },
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

export function* fetchDashboardTotalsFlow(action) { // eslint-disable-line require-yield
    try {
        yield put(loadingDashboardTotals(true));
        const totals = yield call(fetchDashboardTotalsFromServer, action.domain);
        if (totals.success) {
            yield put(persistDashboardTotals(totals.data));
        }
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
    finally {
        yield put(loadingDashboardTotals(false));
    }
}

/**
 * Dashboard projects
 */
export function fetchDashboardProjectsFromServer() {
    return requestGet(
        routeHelper('api/reporting/projects'),
        {
            headers: HEADERS_JSON,
        },
    ).then(handleApiResponse)
     .catch(handleApiError);
}

export function* fetchDashboardProjectsFlow() {
    try {
        yield put(loadingDashboardProjects(true));
        const projects = yield call(fetchDashboardProjectsFromServer);
        if (projects.success) {
            yield put(persistDashboardProjects(projects.data));
        }
    }
    catch (e) {
        console.error(e); // eslint-disable-line no-console
    }
    finally {
        yield put(loadingDashboardProjects(false));
    }
}

// All sagas to be loaded
export default [
    function* () {
        yield takeLatest(FETCH_DASHBOARD_TOTALS, fetchDashboardTotalsFlow);
    },
    function* () {
        yield takeLatest(FETCH_DASHBOARD_PROJECTS, fetchDashboardProjectsFlow);
    },
];
