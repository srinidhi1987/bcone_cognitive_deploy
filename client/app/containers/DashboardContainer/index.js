/*
 *
 * DashboardContainer
 *
 */

import React from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {Breadcrumbs} from 'common-frontend-components';

import {fetchDashboardTotals, fetchDashboardProjects} from './actions';
import selectDashboardContainer from './selectors';
import ProjectReportSection from '../../components/ProjectReportSection';
import ProjectReportTotal from '../../components/ProjectReportTotal';
import ProjectStatsCard from '../../components/ProjectStatsCard';
import {QA_DASHBOARD_REPORT_ACCURACY} from '../../constants/qaElementIds';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class DashboardContainer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'DashboardContainer';

    componentDidMount() {
        const {fetchDashboardProjects, fetchDashboardTotals, projectFields} = this.props;

        // pass the first alias to fetch by domain
        fetchDashboardTotals(projectFields[0]);
        fetchDashboardProjects();
    }

    state = {
        selectedDomain: null,
    };

    render() {
        const {
            projects, dashboardTotals, dashboardTotalsLoading, projectsLoading,
        } = this.props;

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Dashboards"
                />

                <Breadcrumbs>
                    <Link to={routeHelper('/dashboard')}>Dashboard</Link>
                </Breadcrumbs>

                <div className="aa-project-performance-report-container">
                    <ProjectReportSection
                        icon={(
                            <svg width="14px" height="14px" viewBox="8 8 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                <rect stroke="none" fill="#666666" fillRule="evenodd" transform="translate(19.294118, 19.381772) rotate(-45.000000) translate(-19.294118, -19.381772) " x="18.5441183" y="16.7613661" width="1.5" height="5.24081122"></rect>
                                <circle stroke="#666666" strokeWidth="1" fill="none" cx="14" cy="14" r="5"></circle>
                            </svg>
                        )}
                        title="My Totals"
                        subtitle="for all production instances"
                        classes="instance-totals"
                    >
                        <div className="aa-project-performance-report-totals">
                            <div>
                                <ProjectReportTotal
                                    icon={(
                                        <svg width="64px" height="64px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                            <circle stroke="#39ACC7" strokeWidth="2" fill="none" cx="31" cy="31" r="31"></circle>
                                            <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(15.000000, 11.000000)">
                                                <path d="M19.0189595,4.28439995 C26.4121374,5.69611974 32,12.1955022 32,20 C32,25.2815055 29.4409942,29.9653231 25.4952393,32.8791962 L24.8958228,29.4797362 C27.4220325,27.1081951 29,23.7383746 29,20 C29,13.6727096 24.4796926,8.40119356 18.4922836,7.23865761 L19.0189595,4.28439995 Z M10,5.16303409 C4.13633933,7.5365836 0,13.2852449 0,20 C0,28.836556 7.163444,36 16,36 C16.1893686,36 16.3779688,35.9967102 16.5657466,35.9901846 L16.5714229,32.9876666 C16.3819755,32.9958606 16.1914711,33 16,33 C8.82029825,33 3,27.1797017 3,20 C3,14.9845516 5.84021995,10.632472 10,8.46442126 L10,5.16303409 Z" id="Combined-Shape" fill="#39ADC7"></path>
                                                <polygon fill="#39ACC7" transform="translate(10.359462, 7.366073) rotate(65.000000) translate(-10.359462, -7.366073) " points="10.3594617 2.86607261 15.5556141 11.8660726 5.16330926 11.8660726"></polygon>
                                                <polygon fill="#39ACC7" transform="translate(23.756444, 31.838789) rotate(-124.000000) translate(-23.756444, -31.838789) " points="23.7564436 27.3387894 28.9525961 36.3387894 18.5602912 36.3387894"></polygon>
                                            </g>
                                        </svg>
                                    )}
                                    metric={dashboardTotals.totalFilesProcessed}
                                    compareMetric={dashboardTotals.totalFilesCount}
                                    title="Files Processed"
                                    loading={dashboardTotalsLoading}
                                />
                            </div>

                            {/*Div Line*/}
                            <div></div>

                            <div>
                                <ProjectReportTotal
                                    icon={(
                                        <svg width="64px" height="64px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                            <g id="Icon" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                                <circle stroke="#60C58E" strokeWidth="2" cx="31" cy="31" r="31"></circle>
                                                <path d="M27.1724185,43.1610844 L27.2610562,43.2497221 L47.5983683,22.91241 L46.4362362,21.7502779 L27.1930661,40.993448 L16.6452393,30.4456212 L15.4831071,31.6077534 L27.1044283,43.2290746 L27.1724185,43.1610844 Z" stroke="#60C58E" fill="#60C58E"></path>
                                            </g>
                                        </svg>
                                    )}
                                    metric={dashboardTotals.totalSTP}
                                    metricType="percentage"
                                    title="Straight-through processing"
                                    compareMetric={100}
                                    loading={dashboardTotalsLoading}
                                />
                            </div>

                            {/*Div Line*/}
                            <div></div>

                            <div id={QA_DASHBOARD_REPORT_ACCURACY}>
                                <ProjectReportTotal
                                    icon={(
                                        <svg width="64px" height="64px" viewBox="-1 -1 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                            <defs>
                                                <path d="M35,34.0702576 L35,18.0028165 C35,15.7979383 33.209139,14 31,14 C28.7953562,14 27,15.792122 27,18.0028165 L27,34.0702576 C24.6087945,35.4534952 23,38.0388706 23,41 C23,45.418278 26.581722,49 31,49 C35.418278,49 39,45.418278 39,41 C39,38.0388706 37.3912055,35.4534952 35,34.0702576 Z" id="path-1"></path>
                                                <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="35" fill="white">
                                                    <use xlinkHref="#path-1"></use>
                                                </mask>
                                            </defs>
                                            <circle stroke="#C92194" strokeWidth="2" fill="none" cx="31" cy="31" r="31"></circle>
                                            <use id="Combined-Shape" stroke="#C92194" mask="url(#mask-2)" strokeWidth="4" fill="none" xlinkHref="#path-1"></use>
                                            <path d="M32,36.1000181 L32,24.9909572 C32,24.4563138 31.5522847,24 31,24 C30.4438648,24 30,24.4436666 30,24.9909572 L30,36.1000181 C27.7177597,36.5632884 26,38.5810421 26,41 C26,43.7614237 28.2385763,46 31,46 C33.7614237,46 36,43.7614237 36,41 C36,38.5810421 34.2822403,36.5632884 32,36.1000181 Z" id="Combined-Shape" stroke="none" fill="#C92094" fillRule="evenodd"></path>
                                        </svg>
                                    )}
                                    metric={dashboardTotals.totalAccuracy}
                                    previousMetric="90"
                                    metricType="percentage"
                                    title="Accuracy"
                                    loading={dashboardTotalsLoading}
                                />
                            </div>
                        </div>
                    </ProjectReportSection>

                    <ProjectReportSection
                        icon={(
                            <img src={routeHelper('images/projects-icon.svg')} alt="Learning Instances" />
                        )}
                        title="My Learning Instances"
                        theme="dark"
                        classes="dashboard-learning-instances"
                    >
                        {(() => {
                            if (projectsLoading) {
                                return (
                                    <div className="aa-dashboard-instance-loading">
                                        <img src={routeHelper('images/ring-loader.gif')} />
                                    </div>
                                );
                            }

                            // If there are no current projects
                            // display notice
                            if (!projects.length) {
                                return (
                                    <div className="aa-dashboard-no-instances">
                                        <div className="aa-dashboard-no-instances-icon">
                                            <img src={routeHelper('images/projects-icon.svg')} width="40px" alt="Learning Instances" />
                                        </div>

                                        No current learning instances

                                        <Link to={routeHelper('/learning-instances/new')}>
                                            Create an Instance
                                        </Link>
                                    </div>
                                );
                            }

                            return (
                                <div className="aa-dashboard-instance-grid">
                                    {projects
                                        .map((project) => (
                                        <div
                                            key={project.id}
                                        >
                                            <ProjectStatsCard
                                                project={project}
                                            />
                                        </div>
                                    ))}
                                </div>
                            );
                        })()}
                    </ProjectReportSection>
                </div>
            </div>
        );
    }
}

const mapStateToProps = selectDashboardContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchDashboardTotals: (domain) => dispatch(fetchDashboardTotals(domain)),
        fetchDashboardProjects: () => dispatch(fetchDashboardProjects()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);
