import {NavigationContainer} from '../index';
import {shallow} from 'enzyme';
import React from 'react';
import {IQBOT_USER_ROLES} from '../../../constants/enums';
import {APP_NAME} from '../../../constants/productTerms';
import HeaderBar from '../../../components/HeaderBar';
// import {Navigation} from 'common-frontend-components';
describe('<NavigationContainer />', () => {
    //given
    const notifications = [
        {
            id:'111',
            level: 'warn',
            message: 'Your instance story_2030 is below the 30% training threshold',
        },
    ];

    it('should display HeaderBar', () => {
        //given
        const userJson = {
            id: null,
            name: 'xxx',
            roles: [IQBOT_USER_ROLES.SERVICES],
            username: 'xxx',
        };
        const wrapper = shallow(
            <NavigationContainer
                appNotifications={notifications}
                routerLocation={'/dashboard'}
                userRole={IQBOT_USER_ROLES.SERVICES}
                user={userJson}
                removeAppNotificationById = {() => {}}
                onLogout = {() => {}}
            />
        );
        const result = wrapper.find(HeaderBar);
        const {title, appNotifications, user} = result.props();
        //then
        expect(result.length).toEqual(1);
        expect(title).toEqual(APP_NAME.label);
        expect(appNotifications).toEqual(notifications);
        expect(user).toEqual(userJson);
    });

    // it('should display navigation for service role', () => {
    //     //given
    //     const userJson = {
    //         id: null,
    //         name: 'xxx',
    //         roles: [IQBOT_USER_ROLES.SERVICES],
    //         username: 'xxx',
    //     };
    //     const wrapper = shallow(
    //         <NavigationContainer
    //             appNotifications={notifications}
    //             routerLocation={'/dashboard'}
    //             userRole={IQBOT_USER_ROLES.SERVICES}
    //             user={userJson}
    //             removeAppNotificationById = {() => {}}
    //             onLogout = {() => {}}
    //         />
    //     );
    //     //when
    //     const result = wrapper.find(Navigation.PrimaryOption);
    //     //then
    //     expect(result.length).toEqual(3);
    // });

    // it('should display navigation for validator role', () => {
    //     //given
    //     const userJson = {
    //         id: null,
    //         name: 'xxx',
    //         roles: [IQBOT_USER_ROLES.VALIDATOR],
    //         username: 'xxx',
    //     };
    //     const wrapper = shallow(
    //         <NavigationContainer
    //             appNotifications={notifications}
    //             routerLocation={'/dashboard'}
    //             userRole={IQBOT_USER_ROLES.VALIDATOR}
    //             user={userJson}
    //             removeAppNotificationById = {() => {}}
    //             onLogout = {() => {}}
    //         />
    //     );
    //     //when
    //     const result = wrapper.find(Navigation.PrimaryOption);
    //     //then
    //     expect(result.length).toEqual(1);
    // });
});
