/*
 *
 * NavigationContainer
 *
 */

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {removeAppNotificationById} from '../App/actions';
import {Icon, Navigation} from 'common-frontend-components';
import HeaderBar from '../../components/HeaderBar';
import {logout} from '../App/actions';
import {APP_NAME} from '../../constants/productTerms';
import {IQBOT_USER_ROLES} from '../../constants/enums';
import NavigationTooltip from '../../components/NavigationTooltip';
import {navBarWidths} from '../HomePage/constants';
import {autobind} from 'core-decorators';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class NavigationContainer extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'NavigationContainer';

    render() {
        const {
            appNotifications,
            logout,
            removeAppNotificationById,
            user,
            navBarWidth,
        } = this.props;

        return (
            <div>
                <div id="aa-main-navigation" style={{width:navBarWidth}}>
                    {
                        (() => {
                            switch (navBarWidth) {
                                case navBarWidths.REGULAR_WIDTH:
                                    return this.renderRegularNav(this.props);
                                case navBarWidths.SKINNY_WIDTH:
                                    return this.renderSkinnyNav(this.props);
                            }
                        })()
                    }
                </div>
                <div style={{marginLeft: navBarWidth}}>
                    <HeaderBar
                        appNotifications={appNotifications}
                        title={APP_NAME.label}
                        onLogout={logout}
                        removeAppNotificationById={removeAppNotificationById}
                        user={user}
                    />
                </div>
            </div>
        );
    }

    /**
     * Renders a full sized navigation bar
     *
     * @param {Object} props
     * @returns {XML}
     */
    @autobind
    renderRegularNav(props) {
        switch (props.userRole) {
            case IQBOT_USER_ROLES.SERVICES:
                return this.renderRegularNavServicesRole(props);
            case IQBOT_USER_ROLES.VALIDATOR:
                return this.renderRegularNavValidatorRole(props);
            default:
                return null;
        }
    }

    /**
     * Renders Services role section of full sized navigation bar
     *
     * @param {{routerLocation: String}}
     * @returns {XML}
     */
    @autobind
    renderRegularNavServicesRole({routerLocation}) {
        return (
            <Navigation
                currentPath={routerLocation || '/'}
            >
                <Navigation.PrimaryOption
                    icon={(
                        <Icon aa="dashboard-navigation"/>
                    )}
                    title="Dashboards"
                    to={routeHelper('/dashboard')}
                    key="dashboard"
                />

                <Navigation.PrimaryOption
                    icon={(
                        <Icon fa="connectdevelop"/>
                    )}
                    title="Learning Instances"
                    to={routeHelper('/learning-instances')}
                    key="projects"
                />

                <Navigation.PrimaryOption
                    icon={(
                        <Icon aa="dashboard--system-created" />
                    )}
                    title="Domains"
                    to={routeHelper('/domains')}
                    key="domains"
                />

                <Navigation.PrimaryOption
                    icon={(
                        <Icon aa="bot-navigation"/>
                    )}
                    title="Bots"
                    to={routeHelper('/bots')}
                    key="bots"
                />
            </Navigation>
        );

    }

    /**
     * Renders Validator role section of full sized navigation bar
     *
     * @param {{routerLocation: String}}
     * @returns {XML}
     */
    @autobind
    renderRegularNavValidatorRole({routerLocation}) {
        return (
            <Navigation
                currentPath={routerLocation || '/'}
            >
                <Navigation.PrimaryOption
                    icon={(
                        <Icon fa="connectdevelop"/>
                    )}
                    title="Learning Instances"
                    to={routeHelper('/learning-instances/validations')}
                    key="Validations"
                />
            </Navigation>
        );
    }

    /**
     * Renders 'skinny' sized navigation bar
     *
     * @param {IQBOT_USER_ROLES} userRole
     * @returns {XML}
     */
    @autobind
    renderSkinnyNav({userRole}) {
        switch (userRole) {
            case IQBOT_USER_ROLES.SERVICES:
                return this.renderSkinnyNavServicesRole();
            case IQBOT_USER_ROLES.VALIDATOR:
                return this.renderSkinnyNavValidatorRole();
            default:
                return this.renderSkinnyNavDefaultedRole();
        }
    }

    /**
     * Renders Services role section of 'skinny' sized navigation bar
     *
     * @returns {XML}
     */
    @autobind
    renderSkinnyNavServicesRole() {
        return (
            <div className="iq-bot-navigation">
                <div className="iq-bot-navigation--header">
                    <Link to={routeHelper()}>
                        <img src={routeHelper('images/auth-logo.svg')} />
                    </Link>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
                <div className="iq-bot-navigation--option dashboard">
                    <NavigationTooltip label="Dashboard">
                        <Link to={routeHelper('/dashboard')}>
                            <Icon aa="dashboard-navigation"/>
                        </Link>
                    </NavigationTooltip>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
                <div className="iq-bot-navigation--option">
                    <NavigationTooltip label="Learning Instances">
                        <Link to={routeHelper('/learning-instances')}>
                            <Icon fa="connectdevelop" />
                        </Link>
                    </NavigationTooltip>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
                <div className="iq-bot-navigation--option">
                    <NavigationTooltip label="Domains">
                        <Link to="/domains">
                            <Icon aa="dashboard--system-created" />
                        </Link>
                    </NavigationTooltip>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
                <div className="iq-bot-navigation--option bots">
                    <NavigationTooltip label="Bots">
                        <Link to={routeHelper('/bots')}>
                            <Icon aa="bot-navigation" />
                        </Link>
                    </NavigationTooltip>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
            </div>
        );

    }

    /**
     * Renders Services role section of 'skinny' sized navigation bar
     *
     * @returns {XML}
     */
    @autobind
    renderSkinnyNavValidatorRole() {
        return (
            <div className="iq-bot-navigation">
                <div className="iq-bot-navigation--header">
                    <Link to={routeHelper()}>
                        <img src={routeHelper('images/auth-logo.svg')} />
                    </Link>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
                <div className="iq-bot-navigation--option">
                    <NavigationTooltip label="Learning Instances">
                        <Link to={routeHelper('/learning-instances/validations')}>
                            <Icon fa="connectdevelop" />
                        </Link>
                    </NavigationTooltip>
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
            </div>
        );
    }
    /**
     * Renders empty 'skinny' sized navigation bar
     *
     * @returns {XML}
     */
    @autobind
    renderSkinnyNavDefaultedRole() {
        return (
            <div className="iq-bot-navigation">
                <div className="iq-bot-navigation--header">
                    <img src={routeHelper('images/auth-logo.svg')} />
                </div>
                <div className="iq-bot-navigation--border dark"/>
                <div className="iq-bot-navigation--border light"/>
                <div className="iq-bot-navigation--option">
                </div>
            </div>
        );
    }

}

function mapDispatchToProps(dispatch) {
    return {
        logout: () => dispatch(logout()),
        removeAppNotificationById: (id) => dispatch(removeAppNotificationById(id)),
    };
}

export default connect(null, mapDispatchToProps)(NavigationContainer);
