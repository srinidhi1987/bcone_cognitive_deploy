/**
 * RegistrationRequired
 *
 * This is the page we show when the user when iqbot has not been registered with a control room instance
 *
 */

import React, {PureComponent} from 'react';
import {
    Message,
    Modal,
    PageTitle,
} from 'common-frontend-components';
import {themeType} from '../../components/ActionConfirmationModal';
import Helmet from 'react-helmet';

import './styles.scss';

export default class RegistrationRequired extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'RegistrationRequired';

    render() {
        return (
            <div className="aa-registration-required-container">

                <Helmet
                    title="Automation Anywhere | Registration Required"
                />

                <Modal
                    className="aa-registration-required-modal"
                    theme="default"
                    show={true}
                    onHide={this.goToControlRoom}
                    closable={false}
                >
                    <div className="aa-grid-row space-between vertical-center">
                        <PageTitle label="Register IQ Bot" />
                    </div>

                    <Message
                        theme={themeType.error}
                        title="IQ Bot is not registered with Automation Anywhere Enterprise Control Room"
                    >
                        <div>
                            <p>To continue, please contact your administrator.</p>
                        </div>
                    </Message>
                </Modal>
            </div>
        );
    }
}
