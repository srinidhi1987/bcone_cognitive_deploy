import React from 'react';
import {shallow} from 'enzyme';
import RegistrationRequired from '../index';
import Helmet from 'react-helmet';

describe('<RegistrationRequired />', () => {
    const wrapper = shallow(<RegistrationRequired />);

    it('should be defined', () => {
        expect(RegistrationRequired).toBeDefined();
    });

    describe('helmet headers', () => {
        const {title} = wrapper.find(Helmet).props();

        it('should render page title', () => {
            expect(title).toEqual('Automation Anywhere | Registration Required');
        });
    });
});
