/*
 *
 * ViewProjectContainer
 *
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {Link} from 'react-router';
import {
    Breadcrumbs,
    CommandButton,
    PageTitle,
    Icon,
    Tabs,
} from 'common-frontend-components';
import classnames from 'classnames';
import FontAwesome from 'react-fontawesome';
import selectViewProjectContainer from './selectors';
import {browserHistory} from 'react-router';
import {
    clearCurrentProject,
    deleteProject,
    fetchProjectById,
    fetchProjectCategories,
    populateCurrentProject,
    pullProjectById,
    fetchProjectSummaryById,
    setCurrentProjectStage,
} from '../ProjectsContainer/actions';
import {getFirstBotIndex} from './utilities';
import {
    MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT,
    CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT,
} from '../ProjectsContainer/constants';
import {
    setEditFormVisibility,
} from './actions';
import {APP_NOTIFICATIONS} from '../../constants/enums';
import {addAppNotification, removeAppNotificationById} from '../App/actions';
import {checkProcessingStatus, resetProcessingStatus} from '../AnalyzingProjectDocsContainer/actions';
import {updateProject} from '../EditProjectContainer/actions';
import EditProjectForm from '../EditProjectForm';
import EnvironmentBadge from '../../components/EnvironmentBadge';
import ProjectCategoryDetails from '../../components/ProjectCategoryDetails';
import ProjectInfoDetails from '../../components/ProjectInfoDetails';
import PageDescription from '../../components/PageDescription';
import ProjectProgressBar from '../../components/ProjectProgressBar';
import {displayAlert} from '../AlertsContainer/actions';
import {ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import PageLoadingOverlay from '../../components/PageLoadingOverlay';
import ProjectClassifyProgressLoader from '../../components/ProjectClassifyProgressLoader';
import ConfirmProjectDeletionModal from '../../components/ConfirmProjectDeletionModal';
import ActionConfirmationModal from '../../components/ActionConfirmationModal';
import {themeType} from '../../components/ActionConfirmationModal';
import ActionWithLoadingState from '../../components/ActionWithLoadingState';
import './styles.scss';
import {autobind} from 'core-decorators';
import ValidateProjectButton from '../../components/ValidateProjectButton';
import routeHelper from '../../utils/routeHelper';


export class ViewProjectContainer extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'ViewProjectContainer';

    constructor(props) {
        super(props);

        this.pullProject = null;
    }

    state = {
        categoriesSort: [
            {id: 'index', orderBy: 'desc'},
        ],
        showDeletionModal: false,
        showActionConfirmationDetail: false,
        actionConfirmationDetailParam: null,
        activeId: '1',
    };

    componentWillMount() {
        // fetch project by id
        this.props.fetchProjectById(this.props.params.id);
        this.props.fetchProjectSummaryById(this.props.params.id);
        this.props.fetchProjectCategories(this.props.params.id, {...this.props.categoryPagination, sort:this.state.categoriesSort}, false);
        this.props.checkFileProcessing(this.props.params.id);
    }

    componentDidMount() {
        const delay = 10000;
        const {params, checkFileProcessing, pullProject, fetchProjectCategories, setEditFormVisibility} = this.props;

        this.pullProjectAndCategories = setInterval(() => {
            if (!this.props.editMode) {
                pullProject(params.id);
                fetchProjectCategories(params.id, {...this.props.categoryPagination}, false);
            }
        }, 30000);

        this.statusCheck = setInterval(() => {
            checkFileProcessing(params.id);
        }, delay);

        setEditFormVisibility(false);

        this.checkProjectCategories = setInterval(() => {
            this.props.fetchProjectCategories(this.props.params.id, {...this.props.categoryPagination, sort:this.state.categoriesSort}, true);
        }, 3000);
    }

    componentWillReceiveProps(newProps) {
        const {currentProject} = this.props;

        if (currentProject && newProps.currentProject.currentTrainedPercentage !== currentProject.currentTrainedPercentage) {
            this._checkForThresholdNotification(newProps.currentProject);
        }
    }

    /**
     * changePage - extend pagination params with latest offset and limit, fetch categories
     *
     * @param {object} offsetAndLimit
     * @param {number} offsetAndLimit.offset
     * @param {number} offsetAndLimit.limit
     */
    changePage(offsetAndLimit) {
        this.props.fetchProjectCategories(this.props.params.id, {...this.props.categoryPagination, ...offsetAndLimit});
    }

    /**
     * isProjectInProduction
     *
     * @param project
     * @returns {boolean}
     */
    isProjectInProduction(project) {
        return project.environment === 'production';
    }

    /**
     * sortCategories - extend pagination params with new sort array, switch to first page (offset 0), fetch categories
     * @param sort
     */
    sortCategories(sort) {
        this.props.fetchProjectCategories(this.props.params.id, {...this.props.categoryPagination, sort, offset: 0});
    }

    /**
     * Check if threshold notification is needed to alert the user
     *
     * @param {object} project
     * @param {string} project.name
     * @param {string} project.id
     * @private
     */
    _checkForThresholdNotification(project) {
        const {addAppNotification, removeAppNotificationById} = this.props;

        if (project && this._isUnderTrainingThreshold(project)) {
            addAppNotification(
                `Your ${project.name} instance is below the ${MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE}% training threshold`,
                APP_NOTIFICATIONS.WARNING,
                `project-threshold-${project.id}`,
            );
        }
        else if (project && !this._isUnderTrainingThreshold(project)) {
            removeAppNotificationById(`project-threshold-${project.id}`);
        }
    }

    /**
     * Check if project is under threshold
     *
     * @param {object} project
     * @param {number} project.currentTrainedPercentage
     * @returns {boolean}
     * @private
     */
    _isUnderTrainingThreshold(project) {
        return Boolean(project.currentTrainedPercentage < MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE);
    }

    componentWillUnmount() {
        const {clearCurrentProject, resetProcessingStatus} = this.props;
        this._clearIntervals();
        clearCurrentProject(); //clears project categories as well
        resetProcessingStatus();
    }

    _clearIntervals() {
        clearInterval(this.pullProjectAndCategories);
        clearInterval(this.statusCheck);
        clearInterval(this.checkProjectCategories);
    }

    _handleSave(data) {
        const {currentProject, updateProject} = this.props;
        const projectData = data.toJS();
        // Send all editable properties require to be updated
        updateProject(currentProject, Object.keys(projectData).reduce((acc, key) => {
            if (['files'].includes(key)) {
                return acc;
            }
            acc[key] = projectData[key];
            return acc;
        }, {}),
            projectData.files,
        );
    }

    _handleProjectStateChange(project) {
        const {setCurrentProjectStage} = this.props;

        if (this.isProjectInProduction(project)) {
            setCurrentProjectStage(project, 'staging');

            return;
        }

        this.setState({
            showActionConfirmationDetail: true,
            actionConfirmationDetailParam : {project},
        });

        return;
    }

    /**
        * render a project stage toggle ActionWithLoadingState component
        * @param {object} project
        * @returns {component} ActionWithLoadingState
    */
    _renderProjectStageToggle(project) {
        const isInProduction = this.isProjectInProduction(project);
        const {projectsDelayingStage} = this.props;
        const working = Boolean(projectsDelayingStage[project.id]);
        const fa = working ? 'gear' : null;
        const aa = working ? null : isInProduction ? 'action-toggle--enabled' : 'action-toggle--disabled';
        const animate = working ? 'spin' : null;
        const label = working ? null : isInProduction ? 'Set instance to staging' : 'Set instance to production';

        return (
            <ActionWithLoadingState
                aa={aa}
                animate={animate}
                component="span"
                fa={fa}
                label={label}
                working={working}
                onClick={() => this._handleProjectStateChange(project)} />
        );
    }

    _renderValidatorButton(appConfig, authToken, currentProject, isProjectInProduction) {
        return (
            <ValidateProjectButton
                appConfig={appConfig}
                authToken={authToken}
                id={currentProject.id}
                withLabel={true}
                enabled={isProjectInProduction}
            />
        );
    }

    renderClassifyingProgressSpinner(projectStatus) {
        if (!projectStatus || !projectStatus.totalFileCount || projectStatus.totalProcessedCount === projectStatus.totalFileCount) {
            return null;
        }

        const progress = Math.round(projectStatus.totalProcessedCount / projectStatus.totalFileCount * 100);

        return (
            <div className="progress-classifying-spinner">
                <ProjectClassifyProgressLoader
                    progress={progress}
                />
            </div>
        );
    }

    @autobind startTrainingClickHandler() {
        const {currentProjectCategories, currentProject} = this.props;
        const firstBotIndex = getFirstBotIndex(currentProjectCategories);

        if (firstBotIndex === null) {
            return;
        }

        browserHistory.push(routeHelper(`/learning-instances/${currentProject.id}/bots/${currentProjectCategories[firstBotIndex].id}/edit`));
    }

    render() {
        const {
            appConfig,
            authToken,
            currentProject,
            currentProjectCategories,
            deleteProject,
            displayAlert,
            isDeletingProject,
            isProjectInProduction,
            isUpdating,
            loadingCategories,
            priorityFields,
            projectStatus,
            stateProgress,
            categoryPagination,
            currentProjectSummary,
            languages,
            setCurrentProjectStage,
            setEditFormVisibility,
            startTrainingEnabled,
            editMode,
        } = this.props;
        const {
            activeId,
            showDeletionModal,
            showActionConfirmationDetail,
            actionConfirmationDetailParam,
        } = this.state;

        const currentProjectLanguage = languages.find((language) => {
            if (currentProject) {
                return language.id.toString() === currentProject.primaryLanguage;
            }
        });

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Edit Learning Instance"
                />

                <Breadcrumbs>
                    <Link
                        to={routeHelper('/learning-instances')}>Learning Instances
                    </Link>
                    <Link
                        to={routeHelper(`/learning-instances/${currentProject ? currentProject.id : ''}`)}
                    >
                        {currentProject ? currentProject.name : ''}
                    </Link>
                </Breadcrumbs>

                {(() => {
                    // If project is present
                    if (currentProject) {
                        return (
                            <div id="aa-main-container" >
                                <ProjectProgressBar
                                    currentStep={stateProgress}
                                    complete={currentProject.projectState === 'complete'}
                                />

                                {/** Page header **/}
                                {(() => {
                                    if (editMode) {
                                        return (
                                            <EditProjectForm
                                                initialValues={{
                                                    description: currentProject.description,
                                                    files: [],
                                                    fields: {standard:[], custom:[]},
                                                }}
                                                projectName={currentProject.name}
                                                environment={currentProject.environment}
                                                onSubmit={this._handleSave.bind(this)}
                                                onDelete={() => this.setState({showDeletionModal: true})}
                                                onCancel={() => setEditFormVisibility(false)}
                                            />
                                        );
                                    }

                                    return (
                                        <div className="aa-view-project-header-container aa-grid-column">
                                            <div className="aa-grid-row space-between vertical-center padded">
                                                <div>
                                                    <div className="aa-grid-row vertical-center">
                                                        <PageTitle label={currentProject.name} />
                                                        <EnvironmentBadge>{currentProject.environment}</EnvironmentBadge>
                                                        {this.renderClassifyingProgressSpinner(projectStatus)}
                                                    </div>
                                                </div>
                                                <div className="aa-grid-row vertical-center horizontal-right aa-view-project-header-actions">
                                                    <div className="aa-grid-row vertical-center toggle-switch">
                                                        {this._renderProjectStageToggle(currentProject)}
                                                    </div>
                                                    <div>
                                                        {this._renderValidatorButton(appConfig, authToken, currentProject, isProjectInProduction)}
                                                    </div>
                                                    <div>
                                                        <div className={classnames({
                                                            'aa-project-edit-disabled': isProjectInProduction,
                                                        })}>
                                                            {(() => {
                                                                if (editMode) {
                                                                    return (
                                                                        <a onClick={() => this._handleSave()}>
                                                                            <Icon
                                                                                fa="floppy-o"
                                                                            /> Save
                                                                        </a>
                                                                    );
                                                                }

                                                                return (
                                                                    <a
                                                                        className="aa-link aa-grid-row vertical-center"
                                                                        onClick={() => {
                                                                            if (isProjectInProduction) {
                                                                                return displayAlert(
                                                                                    'Can only edit instances in staging. Feel free to move this instance back to edit.',
                                                                                    ALERT_LEVEL_DANGER,
                                                                                );
                                                                            }
                                                                            return setEditFormVisibility(true);
                                                                        }}
                                                                    >
                                                                        <Icon
                                                                            fa="pencil"
                                                                        />
                                                                        <span>
                                                                            Edit
                                                                        </span>
                                                                    </a>
                                                                );
                                                            })()}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="aa-grid-row horizontal-left padded">
                                                <div>
                                                    <PageDescription>
                                                       <span>
                                                            {currentProject.description}
                                                       </span>
                                                    </PageDescription>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })()}
                                {/** End Page header **/}

                                {/** Project information tables **/}
                                <div>
                                    <div className="tab-content-area">
                                        <Tabs
                                            activeId={activeId}
                                            onActive={(activeId) => this.setState({activeId})}
                                            tabs={[
                                                {
                                                    id:'1',
                                                    label:'Summary',
                                                    component: () => {
                                                        if (currentProjectSummary && currentProjectLanguage) {
                                                            return (
                                                                <ProjectInfoDetails
                                                                    project={currentProject}
                                                                    projectSummary={currentProjectSummary.projectSummary}
                                                                    language={currentProjectLanguage.name}
                                                                />
                                                            );
                                                        }
                                                        return null;
                                                    },
                                                },
                                                {
                                                    id:'2',
                                                    label:'Groups',
                                                    component: () => {
                                                        return (
                                                            <ProjectCategoryDetails
                                                                categories={currentProjectCategories}
                                                                categoryPagination={categoryPagination}
                                                                appConfig={appConfig}
                                                                loadingCategories={loadingCategories}
                                                                priorityFields={priorityFields}
                                                                project={currentProject}
                                                                changePage={this.changePage.bind(this)}
                                                                categoriesSort={categoryPagination.sort}
                                                                sortCategories={this.sortCategories.bind(this)}
                                                            />
                                                        );
                                                    },
                                                },
                                            ]}
                                        >
                                        </Tabs>
                                    </div>
                                </div>
                                {/** End Project information tables **/}
                                <div className="training-button">
                                    <CommandButton
                                        disabled={!startTrainingEnabled}
                                        theme="success"
                                        recommended
                                        onClick={this.startTrainingClickHandler}>
                                        <span>
                                            <FontAwesome name="magic" />
                                            <span> Start Training</span>
                                        </span>
                                    </CommandButton>
                                </div>

                                <PageLoadingOverlay
                                    show={isUpdating}
                                    label="Updating instance and uploading files..."
                                    fullscreen={true}
                                />

                                <PageLoadingOverlay
                                    show={isDeletingProject}
                                    label="Deleting instance..."
                                    fullscreen={true}
                                />

                            </div>
                        );
                    }

                    return null;
                })()}

                {(() => {
                    // Delete project modal
                    if (currentProject) {
                        return (
                            <ConfirmProjectDeletionModal
                                currentProject={currentProject}
                                show={showDeletionModal}
                                onHide={() => this.setState({showDeletionModal: false})}
                                onDelete={(id) => {
                                    this.setState({showDeletionModal: false});
                                    // call dispatch event
                                    deleteProject(id);
                                }}
                            />
                        );
                    }
                })()}
                {(() => {
                    if (currentProject) {
                        return (
                            <ActionConfirmationModal
                                title={CONFIRMATION_ON_MOVE_TO_PRODUCTION_TITLE}
                                paragraphs={[CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE1, CONFIRMATION_ON_MOVE_TO_PRODUCTION_MESSAGE2]}
                                cancelTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CANCEL_TEXT}
                                confirmTitle = {CONFIRMATION_ON_MOVE_TO_PRODUCTION_CONFIRM_TEXT}
                                show={showActionConfirmationDetail}
                                param={actionConfirmationDetailParam}
                                theme={themeType.info}
                                onCancel={() => this.setState({showActionConfirmationDetail: false})}
                                onConfirm={(param) => {
                                    setCurrentProjectStage(param.project, 'production');
                                    this.setState({showActionConfirmationDetail: false,
                                        actionConfirmationDetailParam:null});
                                }}
                            />
                        );
                    }
                })()}
            </div>
        );
    }
}

const mapStateToProps = selectViewProjectContainer();

function mapDispatchToProps(dispatch) {
    return {
        addAppNotification: (message, level, id) => dispatch(addAppNotification(message, level, id)),
        checkFileProcessing: (projectId) => dispatch(checkProcessingStatus(projectId)),
        deleteProject: (projectId) => dispatch(deleteProject(projectId)),
        clearCurrentProject: () => dispatch(clearCurrentProject()),
        displayAlert: (message, level) => dispatch(displayAlert(message, level)),
        fetchProjectById: (projectId) => dispatch(fetchProjectById(projectId)),
        fetchProjectSummaryById: (projectId) => dispatch(fetchProjectSummaryById(projectId)),
        fetchProjectCategories: (projectId, params, skipLoadingState) => (
            dispatch(fetchProjectCategories(projectId, params, true, skipLoadingState))
        ),
        pullProject: (projectId) => dispatch(pullProjectById(projectId)),
        removeAppNotificationById: (id) => dispatch(removeAppNotificationById(id)),
        resetProcessingStatus: () => dispatch(resetProcessingStatus()),
        setCurrentProject: (project) => dispatch(populateCurrentProject(project)),
        updateProject: (project, ops, files) => dispatch(updateProject(project, ops, files)),
        setCurrentProjectStage: (project, stage) => dispatch(setCurrentProjectStage(project, stage)),
        setEditFormVisibility: (isVisible) => dispatch(setEditFormVisibility(isVisible)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewProjectContainer);
