import {EMPTY_STRING} from '../../constants/common';

/**
 * @description calculates percentage of progress
 * @param {object} progress
  *@returns {string}
 */
export const calculateProgressPercentage = (progress = {}) => {
    const {processingComplete, totalFileCount, totalProcessedCount} = progress;

    if (processingComplete || !totalFileCount || isNaN(totalProcessedCount)) return null;

    return Math.floor((totalProcessedCount / totalFileCount) * 100);
};

/**
 * @description decides whether or not to check for classifications status again
 * @param {boolean} isNewProject
 * @param {object} status
 * @param {number} fileCount
  *@returns {boolean}
 */
export const checkIfNeedsAnotherCheck = (isNewProject, status, fileCount) => {
    const hasData = status.hasOwnProperty('data');

    if (!hasData) return false;

    const {totalFileCount, complete} = status.data;

    return isNewProject ? totalFileCount !== fileCount : !complete;
};

const isTopPriorityCategoryNotInProduction = (cat) => {
    if (['-1', EMPTY_STRING].includes(cat.id)) {
        return false;
    }

    if (cat.visionBot.lockedUserId) return false;

    return !cat.visionBot.currentState || cat.visionBot.currentState === 'training';
};

/**
 * @description gets first bot index
  *@returns {number}
 */
export const getFirstBotIndex = (currentProjectCategories) => {
    if (!currentProjectCategories || currentProjectCategories === 0) {
        return null;
    }

    return currentProjectCategories.findIndex(isTopPriorityCategoryNotInProduction);
};
