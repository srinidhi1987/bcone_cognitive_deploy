import {createSelector} from 'reselect';
import {Map} from 'immutable';
import {getFirstBotIndex} from './utilities';

/**
 * Direct selector to the viewProjectContainer state domain
 */
const selectAppContainerDomain = () => (state) => state.get('appContainer');
const selectViewProjectContainerDomain = () => (state) => state.get('viewProjectContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');
const selectAnalyzingProjectFilesDomain = () => (state) => state.get('analyzingProjectDocsContainer');
const selectLanguage = () => (state) => state.get('language');
/**
 * Other specific selectors
 */
const getProjectsState = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => projectState,
);

const getProcessingStatus = () => createSelector(
    selectAnalyzingProjectFilesDomain(),
    (processingState) => processingState,
);

const getProjectCategories = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => {
        const state = projectState.toJS();
        const project = state.currentProject;
        const categories = state.currentProjectCategoryData;
        const fields = state.projectFieldsCache;

        if (!project) return [];

        // find fields relevant to Project domain ex. 'Purchase Order' or default to empty map with fields
        const fieldsOfProjectDomain = fields.find((field) => {
            return field.name.toLowerCase() === project.projectType.toLowerCase();
        }) || {fields: []};

        // map fields array to object. Keys are field id, values are field objects
        const fieldsByDomainObject = fieldsOfProjectDomain.fields.reduce((acc, field) => {
            acc[field.id] = field;
            return acc;
        }, {});

        // adds 'name' property to each standard field. see '<-'
        return categories.map((category) => {
            if (!category.categoryDetail) return category;

            return {
                ...category,
                categoryDetail: {
                    ...category.categoryDetail,
                    allFieldDetail: [
                        ...category.categoryDetail.allFieldDetail.reduce((acc, detail) => {
                            const findStandardField = fieldsByDomainObject[detail.fieldId];

                            if (findStandardField) {
                                acc.push({
                                    ...detail,
                                    name: findStandardField.name, // <-
                                });
                            }
                            else {
                                const findCustomField = project.fields.custom.find((custom) => custom.id === detail.fieldId);

                                if (findCustomField) {
                                    acc.push({
                                        ...detail,
                                        name: findCustomField.name,
                                    });
                                }
                            }

                            return acc;
                        }, []),
                    ],
                },
            };
        });
    }
);

const getStateProgress = () => createSelector(
    getProjectsState(),
    (projectState) => {
        if (!projectState) return null;

        const currentProject = Map.isMap(projectState.get('currentProject'))
            ? projectState.get('currentProject').toJS()
            : projectState.get('currentProject');
        const projectStateList = ['classified', 'training', 'inDesign'];

        // if the project is in production, set to review state
        if (currentProject && currentProject.environment === 'production') {
            return 4;
        }
        return projectStateList.findIndex((state) => currentProject && currentProject.projectState === state) + 2;
    }
);

const selectStartTrainingButtonEnabled = () => createSelector(
    getProjectCategories(),
    (categories) => {
        const index = getFirstBotIndex(categories);

        return index === -1 ? false : true;
    }
);
/**
 * Default selector used by ViewProjectContainer
 */

const selectViewProjectContainer = () => createSelector(
    selectViewProjectContainerDomain(),
    selectAppContainerDomain(),
    selectLanguage(),
    getProjectsState(),
    getStateProgress(),
    getProcessingStatus(),
    getProjectCategories(),
    selectStartTrainingButtonEnabled(),
    (substate, appState, language, projectState, stateProgress, progressState, currentProjectCategories, startTrainingEnabled) => {
        return {
            ...substate.toJS(),
            appConfig: appState.get('appConfig').toJS(),
            authToken: appState.get('authToken'),
            cachedProject: projectState.get('cachedProject'),
            currentProject: projectState.get('currentProject'),
            projectsDelayingStage: projectState.get('projectsDelayingStage').toJS(),
            currentProjectSummary: projectState.get('currentProjectSummary'),
            currentProjectCategories,
            categoryPagination: {
                offset: projectState.get('currentProjectCategoryOffset'),
                limit: projectState.get('currentProjectCategoryLimit'),
                sort: projectState.get('currentProjectCategorySort').toJS(),
            },
            isDeletingProject: projectState.get('deletingProject'),
            isProjectInProduction: projectState.get('currentProject') && projectState.get('currentProject').environment === 'production',
            projectStatus: progressState.toJS(),
            loadingCategories: projectState.get('loadingCategories'),
            stateProgress,
            languages: language.get('supportedLanguages'),
            startTrainingEnabled,
        };
    },
);

export default selectViewProjectContainer;
export {
    selectViewProjectContainerDomain,
    getStateProgress,
    getProjectCategories,
    getProcessingStatus,
};
