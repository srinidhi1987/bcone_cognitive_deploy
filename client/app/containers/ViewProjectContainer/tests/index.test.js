import {shallow} from 'enzyme';
import React from 'react';
import {fromJS} from 'immutable';

import {ViewProjectContainer} from '../index';
import EditProjectForm from '../../EditProjectForm';
import ConfirmProjectDeletionModal from '../../../components/ConfirmProjectDeletionModal';
import ActionConfirmationModal from '../../../components/ActionConfirmationModal';
import {APP_NOTIFICATIONS} from '../../../constants/enums';
import {MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE,
} from '../../../containers/ProjectsContainer/constants';
import {Project1, Project2, Project3, currentProjectData} from '../../../../fixtures/projects';
import {languages1} from '../../../../fixtures/languages';
import {projectSummary} from '../../../../fixtures/project_summary';
import {Breadcrumbs} from 'common-frontend-components';
import routeHelper from '../../../utils/routeHelper';

describe('<ViewProjectContainer />', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    const appConfig = {
        encrypted: '234234234',
    };

    describe('instance method: _handleSave', () => {
        // given
        const params = {
            id: '567756675765',
        };
        const updateSpy = jest.fn();
        const formData = {
            name: 'Testing',
            description: 'Testing saving funciotnality',
            files: [],
        };
        const categoryPaginationObject = {
            offset: 50,
            limit: 100,
            sort:[{id: 'priority', orderBy: 'desc'}],
        };

        const wrapper = shallow(
            <ViewProjectContainer
                updateProject={updateSpy}
                params={params}
                routeParams={params}
                fetchProjectById={() => {}}
                fetchProjectSummaryById={() => {}}
                fetchProjectCategories={() => {}}
                checkFileProcessing={() => {}}
                appConfig={appConfig}
                documentBody={{
                    className: '',
                }}
                languages={languages1}
                projectsDelayingStage={[]}
                currentProject={currentProjectData}
                categoryPagination={categoryPaginationObject}
            />
        );

        it('should filter files from project update call', () => {
            // when
            wrapper.instance()._handleSave(fromJS(formData));
            // then
            expect(updateSpy).toHaveBeenCalledWith(currentProjectData, {
                name: formData.name,
                description: formData.description,
            }, formData.files);
        });
    });

    describe('component method: _checkForThresholdNotification', () => {
        // given
        const params = {
            id: '567756675765',
        };
        const updateSpy = jest.fn();
        const appNoticeSpy = jest.fn();
        const removeNoticeSpy = jest.fn();
        const wrapper = shallow(
            <ViewProjectContainer
                addAppNotification={appNoticeSpy}
                isUnderTrainingThreshold={() => true}
                fetchProjectById={() => {}}
                fetchProjectSummaryById={() => {}}
                fetchProjectCategories={() => {}}
                checkFileProcessing={() => {}}
                params={params}
                removeAppNotificationById={removeNoticeSpy}
                routeParams={params}
                updateProject={updateSpy}
                languages={languages1}
                appConfig={appConfig}
                projectsDelayingStage={[]}
            />
        );

        it('should check if project threshold is under and call action to add app notification', () => {
            // given
            // when
            wrapper.instance()._checkForThresholdNotification(Project2);
            // then
            expect(appNoticeSpy).toHaveBeenCalledWith(
                `Your ${Project2.name} instance is below the ${MINIMUM_PROJECT_PRODUCTION_THRESHOLD_PERCENTAGE}% training threshold`,
                APP_NOTIFICATIONS.WARNING,
                `project-threshold-${Project2.id}`,
            );
        });

        it('should check if project threshold is over and call action to remove app notification by id', () => {
            // given
            // when
            wrapper.instance()._checkForThresholdNotification(Project1);
            // then
            expect(removeNoticeSpy).toHaveBeenCalledWith(`project-threshold-${Project1.id}`);
        });
    });

    describe('component method: _handleProjectStateChange', () => {
        //given
        const params = {
            project: Project1,
        };
        const updateSpy = jest.fn();
        const appNoticeSpy = jest.fn();
        const removeNoticeSpy = jest.fn();
        const setCurrentProjectStageSpy = jest.fn();

        const wrapper = shallow(
            <ViewProjectContainer
                addAppNotification={appNoticeSpy}
                isUnderTrainingThreshold={() => true}
                fetchProjectById={() => {}}
                fetchProjectCategories={() => {}}
                checkFileProcessing={() => {}}
                removeAppNotificationById={removeNoticeSpy}
                routeParams={params}
                params={params}
                updateProject={updateSpy}
                currentProject={{...Project1, environment:'production'}}
                categoryPagination={{sort:[
                    {id: 'priority', orderBy: 'desc'},
                ]}}
                languages={languages1}
                fetchProjectSummaryById={() => {}}
                appConfig={appConfig}
                projectsDelayingStage={[]}
                setCurrentProjectStage={setCurrentProjectStageSpy}
            />
        );

        it('should issue update action if project is in production', () => {
            const project = {...Project1, environment:'production'};
            //when
            wrapper.instance()._handleProjectStateChange(project);
            //then
            expect(setCurrentProjectStageSpy).toHaveBeenCalledWith(project, 'staging');
        });

        it('should show confirmation modal if project is in staging', () => {
            //given
            wrapper.setProps({currentProject:{...Project1}});
            //when
            wrapper.instance()._handleProjectStateChange(Project1);
            //then
            expect(wrapper.state().showActionConfirmationDetail).toEqual(true);
            expect(wrapper.state().actionConfirmationDetailParam).toEqual({project: Project1});
        });

    });

    describe('component method: _isUnderTrainingThreshold', () => {
        //given
        const params = {
            id: Project1.id,
        };
        const updateSpy = jest.fn();
        const appNoticeSpy = jest.fn();
        const removeNoticeSpy = jest.fn();
        const wrapper = shallow(
            <ViewProjectContainer
                addAppNotification={appNoticeSpy}
                isUnderTrainingThreshold={() => true}
                fetchProjectById={() => {}}
                fetchProjectCategories={() => {}}
                checkFileProcessing={() => {}}
                removeAppNotificationById={removeNoticeSpy}
                routeParams={params}
                params={params}
                updateProject={updateSpy}
                currentProject={Project2}
                categoryPagination={{sort:[
                    {id: 'priority', orderBy: 'desc'},
                ]}}
                languages={languages1}
                fetchProjectSummaryById={() => {}}
                appConfig={appConfig}
                projectsDelayingStage={[]}
            />
        );
        it('should return true if project current trained % is less than threshold.', () => {
            //when
            const result = wrapper.instance()._isUnderTrainingThreshold(Project2);
            //then
            expect(result).toEqual(true);
        });
        it('should return false if project current trained % is equal or above threshold.', () => {
            const result = wrapper.instance()._isUnderTrainingThreshold(Project1);
            expect(result).toEqual(false);
        });
    });

    describe('check display component', () => {
        //given
        const params = {
            id: Project3.id,
        };
        const updateSpy = jest.fn();
        const appNoticeSpy = jest.fn();
        const removeNoticeSpy = jest.fn();
        const wrapper = shallow(
            <ViewProjectContainer
                addAppNotification={appNoticeSpy}
                isUnderTrainingThreshold={() => true}
                fetchProjectById={() => {}}
                fetchProjectCategories={() => {}}
                checkFileProcessing={() => {}}
                removeAppNotificationById={removeNoticeSpy}
                routeParams={params}
                params={params}
                updateProject={updateSpy}
                currentProject={Project3}
                categoryPagination={{sort:[
                    {id: 'priority', orderBy: 'desc'},
                ]}}
                languages={languages1}
                fetchProjectSummaryById={() => {}}
                currentProjectSummary = {{
                    projectSummary,
                }}
                appConfig={appConfig}
                projectsDelayingStage={[]}
            />
        );

        it('should display edit project form', () => {
            //when
            wrapper.setProps({editMode: true});
            //then
            const result = wrapper.find(EditProjectForm);
            const resultProps = result.props();
            expect(resultProps.projectName).toEqual(Project3.name);
            expect(resultProps.environment).toEqual(Project3.environment);
        });

        it('should render delete confirmation and action confirmation box', () => {
            //given
            const deleteModalProps = wrapper.find(ConfirmProjectDeletionModal).props();
            const actionModalProps = wrapper.find(ActionConfirmationModal).props();
            //then
            expect(deleteModalProps.currentProject).toEqual(Project3);
            expect(deleteModalProps.show).toEqual(false);
            expect(actionModalProps.show).toEqual(false);
        });

        it('should display delete confirmation box', () => {
            //when
            wrapper.setState({showDeletionModal : true});
            //then
            const deleteModalProps = wrapper.find(ConfirmProjectDeletionModal).props();
            expect(deleteModalProps.show).toEqual(true);
        });

        it('should display project state change to production confirmation-box', () => {
            //given
            const actionModal = wrapper.find(ActionConfirmationModal);
            expect(actionModal.props().show).toEqual(false);
            //when
            wrapper.instance()._handleProjectStateChange(Project3);
            //then
            const actionModalProps = wrapper.find(ActionConfirmationModal).props();
            expect(actionModalProps.param).toEqual({project: Project3});
            expect(actionModalProps.show).toEqual(true);
        });

        it('should display breadcrumbs with links to Learning Instance page and Current Page', () => {
            // given
            // when
            const breadcrumbsWrapper = wrapper.find(Breadcrumbs);
            const linksInBreadcrumbWrapper = breadcrumbsWrapper.children();

            // then
            const link1 = linksInBreadcrumbWrapper.first();
            expect(link1.prop('to')).toEqual(routeHelper('/learning-instances'));
            expect(link1.children().text()).toEqual('Learning Instances');

            // then
            const link2 = linksInBreadcrumbWrapper.last();
            expect(link2.prop('to')).toEqual(routeHelper(`/learning-instances/${Project3.id}`));
            expect(link2.children().text()).toEqual(Project3.name);
        });
    });
});
