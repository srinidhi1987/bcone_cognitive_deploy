import {calculateProgressPercentage, getFirstBotIndex} from '../utilities';

describe('component method: _calculateProgressPercentage', () => {
    it('should return null if complete is true', () => {
        // given
        const progress = {
            processingComplete: true,
            currentProcessingObject: null,
            estimatedTimeRemainingInMins: 0,
            projectId: '9e704b9e-a68f-44d4-a977-f73e16b50644',
            totalFileCount: 5,
            totalProcessedCount: 5,
        };
        // when
        const result = calculateProgressPercentage(progress);
        // then
        expect(result).toBe(null);
    });

    it('should return percentage if complete is false', () => {
        // given
        const progress = {
            processingComplete: false,
            currentProcessingObject: null,
            estimatedTimeRemainingInMins: 0,
            projectId: '9e704b9e-a68f-44d4-a977-f73e16b50644',
            totalFileCount: 10,
            totalProcessedCount: 5,
        };
        // when
        const result = calculateProgressPercentage(progress);
        // then
        expect(result).toBe(50);
    });
});

describe('util method: getFirstBotIndex', () => {
    it('should return -1 if there is no categories', () => {
        const categories = [];

        const result = getFirstBotIndex(categories);

        expect(result).toBe(-1);
    });
    it('should return -1 if there only locked category', () => {
        const categories = [{id: '1234', visionBot: {lockedUserId: 'andrew'}}];

        const result = getFirstBotIndex(categories);

        expect(result).toBe(-1);
    });
});
