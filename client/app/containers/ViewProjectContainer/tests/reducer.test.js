import viewProjectContainerReducer, {initialState} from '../reducer';
import {IS_UPDATING_PROJECT, SET_EDIT_FORM_VISIBILITY} from '../constants';

describe('viewProjectContainerReducer', () => {
    it('returns the initial state', () => {
        expect(viewProjectContainerReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle IS_UPDATING_PROJECT action type and set isUpdating state', () => {
        // given
        const action = {
            type: IS_UPDATING_PROJECT,
            updating: true,
        };
        const expectedState = {
            ...initialState.toJS(),
            isUpdating: true,
        };
        // when
        const result = viewProjectContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });

    it('should handle SET_EDIT_FORM_VISIBILITY action type and set isVisible state', () => {
        // given
        const action = {
            type: SET_EDIT_FORM_VISIBILITY,
            isVisible: true,
        };
        const expectedState = {
            ...initialState.toJS(),
            editMode: true,
        };
        // when
        const result = viewProjectContainerReducer(initialState, action);
        // then
        expect(result.toJS()).toEqual(expectedState);
    });
});
