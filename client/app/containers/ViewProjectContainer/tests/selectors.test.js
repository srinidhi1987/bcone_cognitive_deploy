import {fromJS} from 'immutable';
import selectViewProjectContainerDomain, {
    getStateProgress,
    getProcessingStatus,
    getProjectCategories,
} from '../selectors';
import {Project2} from '../../../../fixtures/projects';
import {Category2} from '../../../../fixtures/categories';
import {ProjectFields2} from '../../../../fixtures/project_fields';
import {initialState as viewProjectInitialState} from '../reducer';
import {initialState as appInitialState} from '../../../containers/App/reducer';
import {initialState as projectsInitialState} from '../../../containers/ProjectsContainer/reducer';
import {initialState as analzyingInitialState} from '../../../containers/AnalyzingProjectDocsContainer/reducer';
import {initialState as languageInitialState} from '../../LanguageProvider/reducer';
import {CATEGORY_NAME} from '../../../containers/ProjectsContainer/constants';
import {SortDirections} from '../../../constants/enums';

describe('selectViewProjectContainerDomain', () => {
    describe('selectViewProjectContainer selector', () => {
        // given
        const cachedProject = {
            projectName: 'Test 1',
            projectState: 'inDesign',
            environment: 'staging',
        };
        const currentProject = cachedProject;
        const mockedState = fromJS({
            appContainer: {
                ...appInitialState.toJS(),
                authToken: 'test-token',
                user: {
                    ...appInitialState.toJS().user,
                },
            },
            viewProjectContainer: {
                ...viewProjectInitialState.toJS(),
            },
            projectsContainer: {
                ...projectsInitialState.toJS(),
                cachedProject,
                currentProject,
                currentProjectSummary: {},
            },
            analyzingProjectDocsContainer: {
                ...analzyingInitialState.toJS(),
            },
            language: languageInitialState.toJS(),
        });
        const expectedState = {
            ...viewProjectInitialState.toJS(),
            appConfig: appInitialState.get('appConfig').toJS(),
            authToken: 'test-token',
            cachedProject: fromJS(cachedProject),
            currentProject: fromJS(currentProject),
            categoryPagination: {
                limit: 50,
                offset: 0,
                sort: [
                    {
                        id: CATEGORY_NAME,
                        orderBy: SortDirections.ASCENDING,
                    },
                ],
            },
            currentProjectCategories: [],
            isDeletingProject: false,
            isProjectInProduction: false,
            loadingCategories: false,
            projectStatus: analzyingInitialState.toJS(),
            stateProgress: 4,
            currentProjectSummary: fromJS({}),
            languages: fromJS(languageInitialState.toJS().supportedLanguages),
            projectsDelayingStage: {},
            startTrainingEnabled: false,
        };
        // when
        const selector = selectViewProjectContainerDomain();

        it('should return store state', () => {
            // then
            expect(selector(mockedState)).toEqual(expectedState);
        });
    });

    describe('getStateProgress selector', () => {
        it('should return number for progress bar', () => {
            // given
            const mockedState = fromJS({
                projectsContainer: {
                    ...projectsInitialState.toJS(),
                    currentProject: {
                        projectState: 'classified',
                    },
                },
            });
            // when
            const selector = getStateProgress();
            // then
            expect(selector(mockedState)).toEqual(2);
        });

        it('should return 4th step if project is in production for progress bar', () => {
            // given
            const mockedState = fromJS({
                projectsContainer: {
                    ...projectsInitialState.toJS(),
                    currentProject: {
                        projectState: 'classified',
                        environment: 'production',
                    },
                },
            });
            // when
            const selector = getStateProgress();
            // then
            expect(selector(mockedState)).toEqual(4);
        });
    });

    describe('getProcessingStatus selector', () => {
        it('should return project analyzing state', () => {
            // given
            const mockState = fromJS({
                analyzingProjectDocsContainer: {
                    ...analzyingInitialState.toJS(),
                },
            });
            // when
            const selector = getProcessingStatus();
            // then
            expect(selector(mockState)).toEqual(analzyingInitialState);
        });
    });

    describe('getProjectCategories selector', () => {
        it('should add name to fields from id by fieldDetails of each category', () => {
            // given
            const project = Project2;
            const mockState = fromJS({
                projectsContainer: {
                    currentProject: Project2,
                    projectFieldsCache: ProjectFields2,
                    currentProjectCategoryData: [Category2],
                    currentProjectCategoryOffset: 0,
                    currentProjectCategoryLimit: 20,
                },
            });
            // when
            const selector = getProjectCategories();
            // then
            expect(selector(mockState)).toEqual([
                {
                    ...Category2,
                    categoryDetail: {
                        ...Category2.categoryDetail,
                        allFieldDetail: [
                            {
                                ...Category2.categoryDetail.allFieldDetail[0],
                                name: ProjectFields2[0].fields[0].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[1],
                                name: ProjectFields2[0].fields[1].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[2],
                                name: ProjectFields2[0].fields[2].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[3],
                                name: ProjectFields2[0].fields[3].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[4],
                                name: ProjectFields2[0].fields[4].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[5],
                                name: ProjectFields2[0].fields[5].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[6],
                                name: project.fields.custom[0].name,
                            }, {
                                ...Category2.categoryDetail.allFieldDetail[7],
                                name: project.fields.custom[1].name,
                            },
                        ],
                    },
                },
            ]);
        });
    });
});
