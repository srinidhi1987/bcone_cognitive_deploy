import {IS_UPDATING_PROJECT} from '../constants';
import {isUpdatingProject} from '../actions';

describe('ViewProjectContainer actions', () => {
    describe('isUpdatingProject action creator', () => {

        // given
        const updating = true;
        const expectedAction = {
            type: IS_UPDATING_PROJECT,
            updating,
        };

        it('should return an action object with type IS_UPDATING_PROJECT and property updating', () => {
            // when
            const result = isUpdatingProject(updating);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
