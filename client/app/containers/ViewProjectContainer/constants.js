/*
 *
 * ViewProjectContainer constants
 *
 */

export const IS_UPDATING_PROJECT = 'app/ViewProjectContainer/IS_UPDATING_PROJECT';
export const SET_EDIT_FORM_VISIBILITY = 'app/ViewProjectContainer/SET_EDIT_FORM_VISIBILITY';

