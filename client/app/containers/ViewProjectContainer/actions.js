/*
 *
 * ViewProjectContainer actions
 *
 */

import {IS_UPDATING_PROJECT, SET_EDIT_FORM_VISIBILITY} from './constants';

export function isUpdatingProject(updating = false) {
    return {
        type: IS_UPDATING_PROJECT,
        updating,
    };
}

export function setEditFormVisibility(isVisible = false) {
    return {
        type: SET_EDIT_FORM_VISIBILITY,
        isVisible,
    };
}
