/*
 *
 * ViewProjectContainer reducer
 *
 */

import {fromJS} from 'immutable';

import {IS_UPDATING_PROJECT, SET_EDIT_FORM_VISIBILITY} from './constants';

const initialState = fromJS({
    isUpdating: false,
    editMode: false,
});

function viewProjectContainerReducer(state = initialState, action) {
    switch (action.type) {
        case IS_UPDATING_PROJECT:
            return state.set('isUpdating', action.updating);
        case SET_EDIT_FORM_VISIBILITY:
            return state.set('editMode', action.isVisible);
        default:
            return state;
    }
}

export default viewProjectContainerReducer;
export {initialState};
