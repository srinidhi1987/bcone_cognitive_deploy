/*
 *
 * EditProjectContainer constants
 *
 */

export const EDIT_PROJECT_LOADING = 'app/EditProjectContainer/EDIT_PROJECT_LOADING';
export const UPDATE_PROJECT = 'app/EditProjectContainer/UPDATE_PROJECT';
export const PARTIAL_PROJECT_UPDATE = 'app/EditProjectContainer/PARTIAL_PROJECT_UPDATE';
export const UPDATE_STORE_PROJECT = 'app/EditProjectContainer/UPDATE_STORE_PROJECT';

export const ALERT_EDIT_PROJECT_SUCCESS = 'Learning instance is updated successfully';
export const ALERT_EDIT_PROJECT_FAIL = 'Error updating learning instance';
