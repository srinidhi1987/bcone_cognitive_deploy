/*
 *
 * EditProjectContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
    EDIT_PROJECT_LOADING,
} from './constants';

const initialState = fromJS({
    loading: false,
});

function editProjectContainerReducer(state = initialState, action) {
    switch (action.type) {
        case EDIT_PROJECT_LOADING:
            return state.set('loading', action.loading);
        default:
            return state;
    }
}

export default editProjectContainerReducer;
export {initialState};
