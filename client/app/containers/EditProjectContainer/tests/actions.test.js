import {
    updateProject,
    partialProjectUpdate,
    updateStoreProject,
} from '../actions';
import {
    UPDATE_PROJECT,
    PARTIAL_PROJECT_UPDATE,
    UPDATE_STORE_PROJECT,
} from '../constants';
import {
    Project1,
} from '../../../../fixtures/projects';

describe('EditProjectContainer actions', () => {
    describe('updateProject action creator', () => {
        it('should send project to server', () => {
            // given
            const operations = {
                name: 'Project Updated',
                description: 'Changing stuff',
            };
            const files = [];
            const expected = {
                type: UPDATE_PROJECT,
                projectId: Project1.id,
                operations,
                files,
                existingStandardFields: Project1.fields.standard,
                existingCustomFields: Project1.fields.custom,
                versionId: Project1.versionId,
            };
            // when
            const result = updateProject(Project1, operations, files);
            // then
            expect(result).toEqual(expected);
        });
    });

    describe('partialProjectUpdate action creator', () => {
        it('should construct operations for path', () => {
            // given
            const operations = {
                environment: 'production',
            };
            const expectedAction = {
                type: PARTIAL_PROJECT_UPDATE,
                projectId: Project1.id,
                operations,
                versionId: Project1.versionId,
            };
            // when
            const result = partialProjectUpdate(Project1.id, operations, Project1.versionId);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('updateStoreProject action creator', () => {
        it('should send project to be updated in store', () => {
            // given
            const payload = {
                id: '43553445354345',
                name: 'Project Name',
            };
            const expectedAction = {
                type: UPDATE_STORE_PROJECT,
                payload,
            };
            // when
            const result = updateStoreProject(payload);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
