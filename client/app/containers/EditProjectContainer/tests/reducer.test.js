import editProjectContainerReducer, {initialState} from '../reducer';
import {
    EDIT_PROJECT_LOADING,
} from '../constants';

describe('editProjectContainerReducer', () => {
    it('returns the initial state', () => {
        expect(editProjectContainerReducer(undefined, {})).toEqual(initialState);
    });

    describe('EDIT_PROJECT_LOADING action', () => {
        it('should set loading state in store', () => {
            // given
            const loading = true;
            // when
            const result = editProjectContainerReducer(initialState, {
                type: EDIT_PROJECT_LOADING,
                loading,
            });
            // then
            expect(result.get('loading')).toEqual(loading);
        });
    });
});
