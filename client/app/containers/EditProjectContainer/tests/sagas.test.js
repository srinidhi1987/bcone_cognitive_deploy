/**
 * Test  sagas
 */

import {call, put} from 'redux-saga/effects';
import {
    updateProject,
    patchProjectSagaFlow,
    patchProjectUpdateFromServer,
} from '../sagas';
import {
    ALERT_EDIT_PROJECT_SUCCESS,
    PARTIAL_PROJECT_UPDATE,
    UPDATE_PROJECT,
} from '../constants';
import {
    ALERT_LEVEL_DANGER,
    ALERT_LEVEL_COMMON,
} from '../../AlertsContainer/constants';
import {displayAlert} from '../../AlertsContainer/actions';
import {
    updateProjectPropertiesById,
    updateCurrentProjectByProperties,
} from '../../ProjectsContainer/actions';
import {isUpdatingProject, setEditFormVisibility} from '../../ViewProjectContainer/actions';
import {Project1} from '../../../../fixtures/projects';
import {operations} from '../../../../fixtures/form';
import * as projectSagas from '../../ProjectsContainer/sagas';

import {
    reloadAppRequest,
} from '../../App/actions';

describe('EditProjectContainer sagas', () => {
    describe('updateProject generator', () => {
        afterEach(() => {
            jest.clearAllMocks();
        });

        const updatedVersionId = Project1.versionId + 1;

        describe('without files present', () => {
            // given
            const action = {
                type: UPDATE_PROJECT,
                projectId: Project1.id,
                operations,
                existingStandardFields:[],
                existingCustomFields: [],
                versionId: Project1.versionId,
            };

            describe('success flow', () => {
                const generator = updateProject(action);

                it('should show project update loader', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(true)));
                });

                it('should raise patch request to update project properties', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
                });

                it('should hide edit project form', () => {
                    // given
                    const expectedResult = {
                        success: true,
                        data: {
                            versionId: updatedVersionId,
                        },
                        errors: [],
                    };
                    // when
                    const result = generator.next(expectedResult);
                    // then
                    expect(result.value).toEqual(put(setEditFormVisibility(false)));
                });

                it('should send properties to update current project', () => {
                    // given
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(updateCurrentProjectByProperties({...action.operations, versionId: updatedVersionId})));
                });

                it('should hide project update loader', () => {
                    // given
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(false)));
                });

                it('should display alert for project update success', () => {
                    // given
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(displayAlert(ALERT_EDIT_PROJECT_SUCCESS, ALERT_LEVEL_COMMON)));
                });
            });

            describe('fail flow with 409 status code', () => {
                const generator = updateProject(action);
                // given
                const expectedResult = {
                    success: false,
                    data: null,
                    errors: ['error'],
                    statusCode: 409,
                };

                it('should show project update loader', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(true)));
                });

                it('should raise patch request to update project properties', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
                });

                it('should hide project update loader on patch failure', () => {
                    // when
                    const result = generator.next(expectedResult);
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(false)));
                });

                it('should display fail alert for project update failure', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(reloadAppRequest()));
                });
            });

            describe('fail flow', () => {
                const generator = updateProject(action);
                // given
                const expectedResult = {
                    success: false,
                    data: null,
                    errors: ['error'],
                    statusCode: 200,
                };

                it('should show project update loader', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(true)));
                });

                it('should raise patch request to update project properties', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
                });

                it('should hide project update loader on patch failure', () => {
                    // when
                    const result = generator.next(expectedResult);
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(false)));
                });

                it('should display fail alert for project update failure', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(displayAlert(expectedResult.errors[0], ALERT_LEVEL_DANGER)));
                });
            });
        });

        describe('with files present', () => {
            // given
            const action = {
                type: UPDATE_PROJECT,
                projectId: Project1.id,
                operations,
                files: [
                    {
                        name:'6733282-1132.tif',
                    },
                ],
                existingStandardFields:[],
                existingCustomFields: [],
            };

            describe('success flow', () => {
                const generator = updateProject(action);

                it('should show project update loader', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(true)));
                });

                it('should raise patch request to update project properties', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
                });

                it('should hide edit project form', () => {
                     // given
                    const expectedResult = {
                        success: true,
                        data: {
                            versionId: updatedVersionId,
                        },
                        error: null,
                    };
                    // when
                    const result = generator.next(expectedResult);
                    // then
                    expect(result.value).toEqual(put(setEditFormVisibility(false)));
                });

                it('should send properties to update current project', () => {
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(updateCurrentProjectByProperties({...action.operations, versionId: updatedVersionId})));

                });

                it('should upload files', () => {
                    // when
                    const result = generator.next(action);
                    // then
                    expect(result.value).toEqual(call(projectSagas.uploadProjectFiles, action.projectId, action.files));
                });

                it('should call fetch project summary to refresh the project info', () => {
                    const fetchProjectSummary = jest.spyOn(projectSagas, 'fetchProjectSummary').mockImplementation(function*() {
                        yield; // mock generator to upload files
                    });

                    // when
                    generator.next(action);
                    // then
                    expect(fetchProjectSummary).toHaveBeenCalledWith({projectId: action.projectId});
                });

                it('should hide project update loader', () => {
                    // given
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(isUpdatingProject(false)));
                });

                it('should display alert for project update success', () => {
                    // given
                    // when
                    const result = generator.next();
                    // then
                    expect(result.value).toEqual(put(displayAlert(ALERT_EDIT_PROJECT_SUCCESS, ALERT_LEVEL_COMMON)));
                });
            });
        });
    });

    describe('patchProjectSagaFlow generator', () => {
        // given
        const projectId = '34566456754567';
        const versionId = 111;
        const action = {
            type: PARTIAL_PROJECT_UPDATE,
            projectId,
            operations: {
                environment: 'production',
            },
            versionId,
        };

        describe('success path', () => {
            const generator = patchProjectSagaFlow(action);

            it('should update project in store with optimistic UI', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(updateProjectPropertiesById(action.projectId, action.operations)));
            });

            it('should call api to patch update', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
            });
        });

        describe('fail path with 409', () => {
            const generator = patchProjectSagaFlow(action);

            it('should update project in store with optimistic UI', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(updateProjectPropertiesById(action.projectId, action.operations)));
            });

            it('should call api to patch update', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
            });

            it('should issue reload app request', () => {
                const expectedResult = {
                    success: false,
                    data: [],
                    errors: ['err'],
                    statusCode: 409,
                };
                const result = generator.next(expectedResult);
                expect(result.value).toEqual(put(reloadAppRequest()));
            });
        });

        describe('fail path', () => {
            const generator = patchProjectSagaFlow(action);

            it('should update project in store with optimistic UI', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(put(updateProjectPropertiesById(action.projectId, action.operations)));
            });

            it('should call api to patch update', () => {
                // given
                // when
                const result = generator.next();
                // then
                expect(result.value).toEqual(call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId));
            });

            it('should issue reload app request', () => {
                const expectedResult = {
                    success: false,
                    data: [],
                    errors: ['err'],
                    statusCode: 200,
                };
                const result = generator.next(expectedResult);
                expect(result.value).toEqual(put(displayAlert(expectedResult.errors[0], ALERT_LEVEL_DANGER)));
            });
        });
    });
});
