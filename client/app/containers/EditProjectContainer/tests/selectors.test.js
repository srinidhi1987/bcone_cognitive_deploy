import {fromJS} from 'immutable';
import selectEditProjectContainerDomain from '../selectors';
import {initialState as editProjectInitialState} from '../reducer';
import {initialState as projectsInitialState} from '../../../containers/ProjectsContainer/reducer';

describe('selectEditProjectContainerDomain', () => {
    describe('selectEditProjectContainer selector', () => {
        // given
        const cachedProject = {
            projectName: 'Test 1',
        };
        const currentProject = cachedProject;
        const mockedState = fromJS({
            editProjectContainer: {
                ...editProjectInitialState.toJS(),
            },
            projectsContainer: {
                ...projectsInitialState.toJS(),
                cachedProject,
                currentProject,
            },
        });
        const expectedState = {
            ...editProjectInitialState.toJS(),
            cachedProject: fromJS(cachedProject),
            currentProject: fromJS(currentProject),
        };
        // when
        const selector = selectEditProjectContainerDomain();

        it('should return store state with cachedProject', () => {
            // then
            expect(selector(mockedState)).toEqual(expectedState);
        });
    });
});
