/*
 *
 * EditProjectContainer
 *
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helmet from 'react-helmet';
import {
    Breadcrumbs,
    Title,
    Icon,
    ToggleInput,
} from 'common-frontend-components';

import selectEditProjectContainer from './selectors';
import {updateProject, partialProjectUpdate} from './actions';
import {fetchProjectById, populateCurrentProject} from '../../containers/ProjectsContainer/actions';
import FlexGrid from '../../components/FlexGrid';
import ProjectForm from '../ProjectForm';
import ProjectDisplayHeader from '../../components/ProjectDisplayHeader';
import ProjectCategoryDetails from '../../components/ProjectCategoryDetails';
import ProjectInfoDetails from '../../components/ProjectInfoDetails';
import routeHelper from '../../utils/routeHelper';

import './styles.scss';

export class EditProjectContainer extends Component { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'EditProjectContainer';

    componentWillMount() {
        // check if data was passed from previous page
        if (this.props.cachedProject) {
            // populate edit view with cache data
            this.props.setCurrentProject(this.props.cachedProject);
        }
        else {
            // fetch by id if cache is empty
            this.props.fetchProjectById(this.props.routeParams.id);
        }
    }

    _handleUpdate(data) {
        this.props.updateProject(data.toJS());
    }

    render() {
        const {currentProject, partialUpdate} = this.props;

        return (
            <div>
                <Helmet
                    title="Automation Anywhere | Edit Project"
                />

                <Breadcrumbs>
                    <Link to={routeHelper('/projects')}>Projects</Link>
                    <Link to={routeHelper(`/projects/${this.props.routeParams.id}/edit`)}>Edit project</Link>
                </Breadcrumbs>

                {(() => {
                    // If project is present
                    if (currentProject) {
                        return (
                            <div id="aa-main-container">
                                {/** Page header **/}
                                <FlexGrid>
                                    <FlexGrid.Item>
                                        <Title>
                                            Edit {currentProject.name}
                                        </Title>
                                    </FlexGrid.Item>

                                    <FlexGrid.Item>
                                        <div className="aa-edit-project-header-actions">
                                            <FlexGrid>
                                                <FlexGrid.Item>
                                                    <FlexGrid>
                                                        <FlexGrid.Item>
                                                            <ToggleInput
                                                                checked={currentProject.environment === 'production'}
                                                                onChange={() => partialUpdate(currentProject, {
                                                                    environment: (currentProject.environment === 'production') ? 'staging' : 'production',
                                                                })}
                                                            >
                                                                {null}
                                                            </ToggleInput>
                                                        </FlexGrid.Item>

                                                        <FlexGrid.Item>
                                                            Move to {currentProject.environment === 'production' ? 'Staging' : 'Production'}
                                                        </FlexGrid.Item>
                                                    </FlexGrid>
                                                </FlexGrid.Item>

                                                {(() => {
                                                    if (currentProject.environment === 'production') {
                                                        return (
                                                            <FlexGrid.Item>
                                                                <a href={routeHelper(`/api/projects/${currentProject.id}/validator`)}>
                                                                    <Icon
                                                                        fa="check-square-o"
                                                                    /> Validator
                                                                </a>
                                                            </FlexGrid.Item>
                                                        );
                                                    }
                                                })()}
                                            </FlexGrid>
                                        </div>
                                    </FlexGrid.Item>
                                </FlexGrid>
                                {/** End Page header **/}

                                <div className="aa-edit-project-container form">
                                    <ProjectDisplayHeader
                                        project={currentProject}
                                    />

                                    <ProjectForm
                                        disableFields={true}
                                        initialValues={{project: currentProject}}
                                        onSubmit={this._handleUpdate.bind(this)}
                                        submitBtnLabel="Save changes and reanalyze"
                                    />
                                </div>

                                {/** Project information tables **/}
                                <FlexGrid>
                                    <FlexGrid.Item itemStyle={{width: '40%', paddingLeft: 0}}>
                                        <ProjectInfoDetails
                                            project={currentProject}
                                        />
                                    </FlexGrid.Item>

                                    <FlexGrid.Item itemStyle={{width: '60%', paddingRight: 0}}>
                                        <ProjectCategoryDetails
                                            project={currentProject}
                                        />
                                    </FlexGrid.Item>
                                </FlexGrid>
                                {/** End Project information tables **/}
                            </div>
                        );
                    }
                })()}
            </div>
        );
    }
}

const mapStateToProps = selectEditProjectContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchProjectById: (projectId) => dispatch(fetchProjectById(projectId)),
        setCurrentProject: (project) => dispatch(populateCurrentProject(project)),
        updateProject: (project) => dispatch(updateProject(project)),
        partialUpdate: (project, operations) => dispatch(partialProjectUpdate(project, operations)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditProjectContainer);
