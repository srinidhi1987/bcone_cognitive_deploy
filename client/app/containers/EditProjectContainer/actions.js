/*
 *
 * EditProjectContainer actions
 *
 */

import {
    UPDATE_PROJECT,
    PARTIAL_PROJECT_UPDATE,
    UPDATE_STORE_PROJECT,
} from './constants';

export function updateProject(project, operations = {}, files = []) {
    return {
        type: UPDATE_PROJECT,
        projectId: project.id,
        operations,
        files,
        existingStandardFields: project.fields.standard,
        existingCustomFields: project.fields.custom,
        versionId: project.versionId,
    };
}

export function partialProjectUpdate(projectId, operations = {}, versionId = null) {
    return {
        type: PARTIAL_PROJECT_UPDATE,
        projectId,
        operations,
        versionId,
    };
}

export function updateStoreProject(payload) {
    return {
        type: UPDATE_STORE_PROJECT,
        payload,
    };
}
