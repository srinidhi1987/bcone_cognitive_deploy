import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {
    ALERT_EDIT_PROJECT_SUCCESS,
    ALERT_EDIT_PROJECT_FAIL,
    PARTIAL_PROJECT_UPDATE,
    UPDATE_PROJECT,
} from './constants';
import {HEADERS_JSON} from 'common-frontend-utils';
import {requestPut, requestPatch} from '../../utils/request';

import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE} = iqBotConfig;
import {updateProjectPropertiesById,
    updateCurrentProjectByProperties,
} from '../ProjectsContainer/actions';
import {handleApiResponse, handleApiError} from '../../utils';
import {ALERT_LEVEL_DANGER, ALERT_LEVEL_COMMON} from '../AlertsContainer/constants';
import {displayAlert} from '../AlertsContainer/actions';
import {isUpdatingProject, setEditFormVisibility} from '../ViewProjectContainer/actions';
import {getProjectPatchBody} from '../../utils/transformers/projects';
import {uploadProjectFiles, fetchProjectSummary} from '../ProjectsContainer/sagas';
import {
    reloadAppRequest,
} from '../App/actions';
import routeHelper from '../../utils/routeHelper';

/**
 * Update project
 */
export function updateProjectFromServer(project) {
    return requestPut(
        routeHelper(`api/projects/${project.id}`),
        {
            headers: HEADERS_JSON,
            body: project,
        }
    ).then(handleApiResponse)
     .then((project) => project.data)
     .catch(handleApiError);
}

export function* updateProject(action) {
    try {
        yield put(isUpdatingProject(true));
        const hasFiles = Boolean(action.files && action.files.length);
        const update = yield call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId);

        if (!update.success) {
            yield put(isUpdatingProject(false));
            switch (update.statusCode) {
                case 409:
                    return yield put(reloadAppRequest());
                case 404:
                case 403:
                    return yield put(displayAlert(ALERT_EDIT_PROJECT_FAIL, ALERT_LEVEL_DANGER));
                default:
                    return yield put(displayAlert(update.errors[0] || ALERT_EDIT_PROJECT_FAIL, ALERT_LEVEL_DANGER));
            }
        }
        // optimistic UI updates along with merging existing fields
        yield put(setEditFormVisibility(false));
        yield put(updateCurrentProjectByProperties({
            ...action.operations,
            versionId: update.data.versionId,
            fields:{
                standard: [...action.existingStandardFields, ...action.operations.fields.standard],
                custom: [...action.existingCustomFields, ...action.operations.fields.custom],
            },
        }));
        if (hasFiles) {
            yield call(uploadProjectFiles, action.projectId, action.files);
            yield* fetchProjectSummary({projectId: action.projectId});
        }
        yield put(isUpdatingProject(false));
        yield put(displayAlert(ALERT_EDIT_PROJECT_SUCCESS, ALERT_LEVEL_COMMON));
    }
    catch (e) {
        yield put(isUpdatingProject(false));
        yield put(displayAlert(ALERT_EDIT_PROJECT_FAIL, ALERT_LEVEL_DANGER));
        console.error(e); // eslint-disable-line no-console
    }
}

// Patch partial updates
/**
 * post data to project path api
 * @param {string} projectId
 * @param {object} operations
 * @param {number} versionId
 */
export function patchProjectUpdateFromServer(projectId, operations = {}, versionId = null) {
    const user = JSON.parse(localStorage.getItem(APP_STORAGE.user));
    return requestPatch(
        routeHelper(`api/projects/${projectId}`),
        {
            headers: {
                ...HEADERS_JSON,
                updatedBy: user.username,
            },
            body: getProjectPatchBody(operations, versionId),
        }
    ).then(handleApiResponse)
     .catch(handleApiError);
}

export function* patchProjectSagaFlow(action) {
    try {
        const operations = action.operations;

        yield put(updateProjectPropertiesById(action.projectId, operations)); // Optimistic UI
        const update = yield call(patchProjectUpdateFromServer, action.projectId, action.operations, action.versionId);
        if (!update.success) {
            update.statusCode === 409 ?
                yield put(reloadAppRequest()) :
                yield put(displayAlert(update.errors[0] || 'Error updating project', ALERT_LEVEL_DANGER));
        }
    }
    catch (e) {
        yield put(displayAlert('Error updating project', ALERT_LEVEL_DANGER));
        console.error(e); // eslint-disable-line no-console
    }
}

// All sagas to be loaded
export default [
    function* () {
        yield* takeLatest(UPDATE_PROJECT, updateProject);
    },
    function* () {
        yield* takeLatest(PARTIAL_PROJECT_UPDATE, patchProjectSagaFlow);
    },
];
