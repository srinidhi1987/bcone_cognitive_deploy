import {createSelector} from 'reselect';

/**
 * Direct selector to the editProjectContainer state domain
 */
const selectEditProjectContainerDomain = () => (state) => state.get('editProjectContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');

/**
 * Other specific selectors
 */
const getProjectsState = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => projectState,
);

/**
 * Default selector used by EditProjectContainer
 */

const selectEditProjectContainer = () => createSelector(
    selectEditProjectContainerDomain(),
    getProjectsState(),
    (substate, projectState) => {
        return {
            ...substate.toJS(),
            cachedProject: projectState.get('cachedProject'),
            currentProject: projectState.get('currentProject'),
        };
    },
);

export default selectEditProjectContainer;
export {
    selectEditProjectContainerDomain,
};
