/*
 *
 * MigrationUtilityContainer
 *
 */
import React, {Component} from 'react';
import {autobind} from 'core-decorators';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {Link} from 'react-router';
import classnames from 'classnames';

import {
    fetchProjects,
} from '../ProjectsContainer/actions';
import {DataType, DataField} from 'common-frontend-utils';
import selectUtilityContainer from './selectors';
import {FormattedNumber} from 'react-intl';
import './styles.scss';
import {
    exportProjects,
    fetchMigrationUtilityStatus,
    importProjects,
    fetchArchiveDetails,
    hideImportArchiveSelectionModal,
    navigateToImportOptionsScreen,
    getLastMigrationInfo,
    selectionChange,
    setArchiveName,
    setMigrationImportOption,
    toggleOptionsConfirmationModal,
} from './actions';
import {
    EXPORT_CONFIRMATION_BOX_TITLE,
    EXPORT_CONFIRMATION_BOX_DESCRIPTION,
    EXPORT_CONFIRMATION_BOX_INPUT_LABEL,
    CONFIRMATION_BOX_CANCEL_TEXT,
    EXPORT_CONFIRMATION_BOX_CONFIRM_TEXT,
    EXPORT_LOADING_SCREEN_MESSAGE,
    FETCH_ARCHIVE_DETAILS_LOADING_SCREEN_MESSAGE,
    IMPORT_CONFIRMATION_BOX_CONFIRM_TEXT,
    IMPORT_ARCHIVE_CONFIRMATION_BOX_CONFIRM_TEXT,
    IMPORT_LOADING_SCREEN_MESSAGE,
    IMPORT_OPTION_CONFIRMATION_MODAL_TITLE,
    IMPORT_OPTIONS_CONFIRMATION_MODAL_TEXT,
    IMPORT_OPTIONS_COPY_TEXT_A,
    EXPORT_DEFAULT_ARCHIVE_NAME,
    MIGRATION_IMPORT_OPTIONS,
} from './constants';
import {fieldValidations} from '../ProjectForm/validations';
import {themeType} from '../../components/ActionConfirmationModal';
import ActionConfirmationModal from '../../components/ActionConfirmationModal';
import FlexGrid from '../../components/FlexGrid';
import PageLoadingOverlay from '../../components/PageLoadingOverlay';
import {
    Breadcrumbs,
    CommandButton,
    DataTable,
    PageTitle,
    RadioGroup,
    RadioInput,
} from 'common-frontend-components';
import PageDescription from '../../components/PageDescription';
import ImportArchiveSelectionComponent from '../../components/ImportArchiveSelectionComponent';
import routeHelper from '../../utils/routeHelper';

const {
    APPEND,
    CLEAR,
    MERGE,
    OVERWRITE,
} = MIGRATION_IMPORT_OPTIONS;

export class MigrationUtilityContainer extends Component {
    static displayName = 'MigrationUtilityContainer';

    state = {
        showExportActionConfirmationDetail: false,
        migrationLoadingMessage: '',
    };

    constructor() {
        super();

        this.projectFile = this.getProjectFields();
    }

    componentWillMount() {
        this.props.fetchMigrationUtilityStatus();
        this.props.fetchProjects();
        this.props.getLastMigrationInfo();
    }

    render() {
        const {
            archiveDetails,
            hideImportArchiveSelectionModal,
            loading,
            showImportArchiveSelectionDetail,
        } = this.props;
        const {
            migrationLoadingMessage,
        } = this.state;

        return (
            <div className="migration-utility-layout">
                <Helmet
                    title="Automation Anywhere | Migration Utility"
                />

                <Breadcrumbs>
                    <Link
                        to={routeHelper('/migration-utility')}>Migration Utility
                    </Link>
                </Breadcrumbs>
                <div id="aa-main-container">
                    {this.renderMigrationUtilityLayout(this.props)}
                </div>
                {
                    this.renderImportArchiveSelectionComponent(
                        archiveDetails,
                        hideImportArchiveSelectionModal,
                        this.importConfirmHandler,
                        showImportArchiveSelectionDetail
                    )
                }
                <PageLoadingOverlay
                    label={migrationLoadingMessage}
                    show={loading}
                    fullscreen={true}
                />
            </div>
        );
    }

    componentDidMount() {
        const delay = 2000;
        this.statusCheck = setInterval(() => {
            this.props.fetchMigrationUtilityStatus();
        }, delay);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.showImportMigrationStatus === true && nextProps.showImportMigrationStatus === false) {
            this.props.fetchProjects();
            this.props.getLastMigrationInfo();
        }
    }

    componentWillUnmount() {
        clearInterval(this.statusCheck);
    }

    @autobind
    onSelectHandler(selected) {
        const {selectionChange} = this.props;
        selectionChange(selected);
    }

    @autobind
    importClickHandler() {
        this.props.setMigrationImportOption('append');
        const {fetchArchiveDetails} = this.props;
        fetchArchiveDetails();
        this.setState({
            migrationLoadingMessage: FETCH_ARCHIVE_DETAILS_LOADING_SCREEN_MESSAGE,
        });
    }

    /**
     * exportConfirmHandler initiate export on user's confirmation
     * @param {array} param array of string of selected project's id
     * @param {string} archiveName archive name given by user
     */
    @autobind exportConfirmHandler(param, archiveName) {
        const {user, exportProjects} = this.props;
        exportProjects(param, archiveName, user.username);
        this.setState({showExportActionConfirmationDetail: false});
    }

    /**
     * importConfirmHandler initiate import on user's confirmation
     * @param {string} archiveName file name to be imported
     */
    @autobind importConfirmHandler(archiveName) {
        const {
            hideImportArchiveSelectionModal,
            navigateToImportOptionsScreen,
            // user,
        } = this.props;
        this.setState({migrationLoadingMessage:IMPORT_LOADING_SCREEN_MESSAGE});
        hideImportArchiveSelectionModal();
        navigateToImportOptionsScreen();
        this.setArchiveName(archiveName);
    }

    @autobind importOptionsPageConfirmHandler() {
        const {
            archiveName,
            importProjects,
            migrationImportOption,
            user,
        } = this.props;

        this.hideImportOptionsConfirmationModel();
        importProjects(archiveName, user.username, migrationImportOption);
    }

    @autobind showImportOptionsConfirmationModal() {
        this.props.toggleOptionsConfirmationModal(true);
    }

    @autobind hideImportOptionsConfirmationModel() {
        this.props.toggleOptionsConfirmationModal(false);
    }

    @autobind exportClickHandler() {
        this.setState({showExportActionConfirmationDetail : true, migrationLoadingMessage: EXPORT_LOADING_SCREEN_MESSAGE});
    }

    @autobind cancelExportHandler() {
        this.setState({showExportActionConfirmationDetail:false});
    }

    @autobind selectMigrationImportOption(value) {
        this.props.setMigrationImportOption(value);
    }

    @autobind setArchiveName(archiveName) {
        this.props.setArchiveName(archiveName);
    }

    renderImportMigrationStatus() {
        return (
            <div className="aa-grid-row vertical-center horizontal-center aa-message-container">
                <div className="aa-grid-row padding vertical-center">
                    <img src={routeHelper('images/ring-loader.gif')} alt="loading" />
                    <p>
                    <strong>Learning Instance Import is in progress. This is a resource intensive activity and will take some time. Go grab a cup of coffee!!</strong>
                    </p>
                </div>
                <div className="aa-grid-column padding">
                    <p>
                        <i>
                            <strong>Note:</strong> The IQBot Platform will be available for normal usage after finishing of the Import.
                        </i>
                    </p>
                </div>
            </div>
        );
    }

    renderExportMigrationStatus(exportArchiveFileName, exportPath) {
        return (
            <div className="aa-grid-row vertical-center horizontal-center aa-message-container">
                <div className="aa-grid-row padding vertical-center">
                    <img src={routeHelper('images/ring-loader.gif')} alt="loading" />
                    <p>
                    <strong>Learning Instance Export is in progress. This is a resource intensive activity and will take some time. Go grab a cup of coffee!!</strong>
                    </p>
                </div>
                <div className="aa-grid-column padding">
                    <p>
                    Your exported Learning instance archive "<span className="path">{exportArchiveFileName}</span>" will be available on the IQ Bot Server at:<br/>"<span className="path">{exportPath}</span>"
                    </p>
                    <p>
                        <i>
                            <strong>Note:</strong> The IQBot Platform will be available for normal usage after finishing of the Export.
                        </i>
                    </p>
                </div>
            </div>
        );
    }

    renderMainMigrationUtilityPage({
        cancelExportHandler,
        exportClickHandler,
        exportConfirmHandler,
        disabled,
        importClickHandler,
        initialSortedProjects,
        lastMigrationStatus,
        loadingProjects,
        onSelectHandler,
        projectFields,
        selected,
        showExportActionConfirmationDetail,
    }) {
        return (
            <div>
                <div className="aa-grid-column">
                    <div className="aa-grid-row space-between vertical-center">
                        <div className="aa-grid-row vertical-center">
                            <PageTitle label="Migration Utility" />
                        </div>
                        <div className="aa-grid-row vertical-center horizontal-right">
                            <CommandButton
                                onClick={importClickHandler}
                                theme="success"
                                recommended
                            >
                                <span>Import</span>
                            </CommandButton>
                        </div>
                    </div>
                    <div className="aa-grid-row horizontal-left">
                        <div>
                            <PageDescription>
                                <span>select instances to export all related data</span>
                                <br/>
                                <span>or click on import to begin importing a learning instance data backup file</span>
                            </PageDescription>
                        </div>
                    </div>
                </div>
                {
                    this.renderProjects({
                        cancelExportHandler,
                        exportClickHandler,
                        exportConfirmHandler,
                        disabled,
                        initialSortedProjects,
                        lastMigrationStatus,
                        loadingProjects,
                        onSelectHandler,
                        projectFields,
                        selected,
                        showExportActionConfirmationDetail,
                    })
                }
            </div>
        );
    }

    renderProjects({
        cancelExportHandler,
        exportClickHandler,
        exportConfirmHandler,
        disabled,
        initialSortedProjects,
        lastMigrationStatus,
        loadingProjects,
        onSelectHandler,
        projectFields,
        selected,
        showExportActionConfirmationDetail,
    }) {
        if (!(Array.isArray(initialSortedProjects) && initialSortedProjects.length)) {
            return null;
        }

        return (
            <div>
                <div className="aa-existing-projects-header">
                    <FlexGrid>
                        <FlexGrid.Item itemStyle={{width: '400px', paddingLeft: '0'}}>
                            <span>
                                Instances ({initialSortedProjects.length} of {initialSortedProjects.length})
                            </span>
                        </FlexGrid.Item>
                        <FlexGrid.Item itemStyle={{width: '880px', paddingLeft: '0'}}>
                                {
                                    (() => {
                                        if (!lastMigrationStatus) return null;

                                        const date = new Date(lastMigrationStatus.endTime);
                                        const dateString = `${date.toLocaleString()}`;

                                        return (
                                            <div className="aa-grid-row horizontal-right">
                                                <span className="migration-status--message">
                                                    Last Migration: {lastMigrationStatus.status && lastMigrationStatus.status.toUpperCase()} {dateString}
                                                </span>
                                                <div
                                                    className={
                                                        classnames(
                                                            'migration-status--badge', {
                                                                'success': lastMigrationStatus.status === 'complete',
                                                                'failure': lastMigrationStatus.status === 'failed',
                                                            }
                                                        )
                                                    }
                                                >
                                                </div>
                                            </div>
                                        );
                                    })()
                                }
                        </FlexGrid.Item>
                    </FlexGrid>
                </div>
                <div className="aa-grid-row">
                    <DataTable
                        data={initialSortedProjects.map((proj) => ({
                            id: proj.id,
                            name: proj.name,
                            botCount: proj.visionBotCount,
                            environment: proj.environment,
                        }))}
                        dataId="id"
                        fields={projectFields}
                        loading={loadingProjects}
                        selected={selected}
                        onSelect={onSelectHandler}
                    />
                </div>
                <div className="aa-export-project-placeholder aa-grid-row vertical-center horizontal-center space-between">
                    <div className="utility-button-container">
                        <CommandButton
                            onClick={exportClickHandler}
                            theme="info"
                            recommended
                            disabled={disabled}
                        >
                            <span>Export</span>
                        </CommandButton>
                    </div>
                </div>
                <ActionConfirmationModal
                    title={EXPORT_CONFIRMATION_BOX_TITLE}
                    paragraphs={[EXPORT_CONFIRMATION_BOX_DESCRIPTION]}
                    cancelTitle = {CONFIRMATION_BOX_CANCEL_TEXT}
                    confirmTitle = {EXPORT_CONFIRMATION_BOX_CONFIRM_TEXT}
                    show={showExportActionConfirmationDetail}
                    param={selected}
                    inputLabel= {EXPORT_CONFIRMATION_BOX_INPUT_LABEL}
                    validations= {[
                        fieldValidations.required,
                        fieldValidations.format,
                        fieldValidations.noOutsideWhitespace,
                        fieldValidations.noRepeatingWhitespace,
                        fieldValidations.length(50),
                    ]}
                    theme={themeType.default}
                    onCancel={cancelExportHandler}
                    onConfirm={exportConfirmHandler}
                    defaultInputValue={EXPORT_DEFAULT_ARCHIVE_NAME}
                />
            </div>
        );
    }

    renderImportOptionsPage(props) {
        const {archiveName, migrationImportOption, isShowingImportOptionsConfirmationModal} = props;

        return (
            <div>
                <div className="aa-grid-row space-between vertical-center">
                    <div className="aa-grid-row vertical-center">
                        <PageTitle label="Import" />
                    </div>
                </div>
                <div className="migration-import-options-copy-text aa-page-description">
                    <div>
                        {IMPORT_OPTIONS_COPY_TEXT_A}
                    </div>
                </div>
                <div className="migration-import-options-copy-text aa-page-description">
                    <strong> Archived file to import :</strong> {archiveName}
                </div>
                {/*<div className="aa-page-description">*/}
                {/*</div>*/}
                <div className="migration-import-options-label">Import Options</div>
                <RadioGroup value={this.props.migrationImportOption} onChange={this.selectMigrationImportOption}>
                    <RadioInput name={APPEND.value}>{APPEND.copyText}</RadioInput>
                    <RadioInput name={MERGE.value}>{MERGE.copyText}</RadioInput>
                    <RadioInput name={OVERWRITE.value}>{OVERWRITE.copyText}</RadioInput>
                    <RadioInput name={CLEAR.value}>{CLEAR.copyText}</RadioInput>
                </RadioGroup>
                <div className="import-button-wrapper">
                    <CommandButton
                        onClick={this.showImportOptionsConfirmationModal}
                        theme="success"
                        recommended
                    >
                        <span>Import</span>
                    </CommandButton>
                </div>
                {this.renderImportOptionsConfirmationModal(isShowingImportOptionsConfirmationModal, migrationImportOption)}
            </div>
        );
    }

    renderImportOptionsConfirmationModal(isShowing, migrationImportOption) {
        if (!isShowing) {
            return null;
        }

        return (
            <ActionConfirmationModal
                title={IMPORT_OPTION_CONFIRMATION_MODAL_TITLE}
                paragraphs={[IMPORT_OPTIONS_CONFIRMATION_MODAL_TEXT[migrationImportOption]]}
                cancelTitle="No, Cancel"
                confirmTitle={IMPORT_CONFIRMATION_BOX_CONFIRM_TEXT}
                show={isShowing}
                theme={themeType.default}
                onCancel={this.props.hideImportArchiveSelectionModal}
                onConfirm={this.importOptionsPageConfirmHandler}
            />
        );
    }

    renderMigrationUtilityLayout(props) {
        const {
            isShowingImportOptions,
            showImportMigrationStatus,
            showExportMigrationStatus,
        } = props;

        if (isShowingImportOptions) {
            return this.renderImportOptionsPage(props);
        }

        if (showImportMigrationStatus) {
            return this.renderImportMigrationStatus();
        }

        if (showExportMigrationStatus) {
            const {
                exportArchiveFileName,
                exportPath,
            } = props;

            return this.renderExportMigrationStatus(exportArchiveFileName, exportPath);
        }

        const {
            initialSortedProjects,
            loadingProjects,
            selected,
            lastMigrationStatus,
        } = props;

        const {showExportActionConfirmationDetail} = this.state;

        return this.renderMainMigrationUtilityPage({
            cancelExportHandler: this.cancelExportHandler,
            exportClickHandler: this.exportClickHandler,
            exportConfirmHandler: this.exportConfirmHandler,
            disabled: !(Array.isArray(selected) && selected.length),
            importClickHandler: this.importClickHandler,
            initialSortedProjects,
            lastMigrationStatus,
            loadingProjects,
            onSelectHandler: this.onSelectHandler,
            projectFields: this.getProjectFields(),
            selected,
            showExportActionConfirmationDetail,
        });
    }

    renderImportArchiveSelectionComponent(
        archiveDetails,
        hideImportArchiveSelectionModal,
        importConfirmHandler,
        showImportArchiveSelectionDetail,
    ) {
        return showImportArchiveSelectionDetail ? (
            <ImportArchiveSelectionComponent
                title="Select the archive file to open"
                cancelTitle = {CONFIRMATION_BOX_CANCEL_TEXT}
                confirmTitle = {IMPORT_ARCHIVE_CONFIRMATION_BOX_CONFIRM_TEXT}
                show={showImportArchiveSelectionDetail}
                theme={themeType.default}
                onCancel={hideImportArchiveSelectionModal}
                onConfirm={importConfirmHandler}
                selectionOptions={archiveDetails}
            />
        ) : null;
    }

    getProjectFields() {
        return [
            new DataField({id: 'name', label: 'Instance name', sortable: true, type: DataType.STRING, render: (name) => (
                <div>{name}</div>
            )}),
            new DataField({id: 'environment', label: 'Environment', sortable: false, type: DataType.STRING, render: (environment) => (
                <div> {environment}</div>
            )}),
            new DataField({id: 'botCount', label: '# of IQ bots', sortable: true, type: DataType.NUMBER, render: (count) => (
                <FormattedNumber value={count} />
            )}),
        ];
    }
}

const mapStateToProps = selectUtilityContainer();

function mapDispatchToProps(dispatch) {
    return {
        fetchProjects: () => dispatch(fetchProjects()),
        exportProjects: (selectedProjects, exportTo, userId) => dispatch(exportProjects(selectedProjects, exportTo, userId)),
        fetchMigrationUtilityStatus: () => dispatch(fetchMigrationUtilityStatus()),
        fetchArchiveDetails: () => dispatch(fetchArchiveDetails()),
        importProjects: (archiveName, userId, option) => dispatch(importProjects(archiveName, userId, option)),
        hideImportArchiveSelectionModal: () => dispatch(hideImportArchiveSelectionModal()),
        navigateToImportOptionsScreen: () => dispatch(navigateToImportOptionsScreen()),
        selectionChange: (selected) => dispatch(selectionChange(selected)),
        setArchiveName: (archiveName) => dispatch(setArchiveName(archiveName)),
        setMigrationImportOption: (value) => dispatch(setMigrationImportOption(value)),
        toggleOptionsConfirmationModal: (isShowing) => dispatch(toggleOptionsConfirmationModal(isShowing)),
        getLastMigrationInfo: () => dispatch(getLastMigrationInfo()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MigrationUtilityContainer);
