import {
    exportProjects,
    exportProjectsSucceeded,
    exportCallFailed,
    migrationloadingScreen,
    fetchMigrationUtilityStatus,
    fetchMigrationUtilityStatusSucceeded,
    fetchArchiveDetails,
    fetchArchiveDetailsSucceeded,
    importProjects,
    importInitiateSucceeded,
    hideImportArchiveSelectionModal,
    selectionChange,
} from '../actions';
import {
    EXPORT_PROJECTS,
    EXPORT_PROJECTS_CALL_SUCCEEDED,
    EXPORT_PROJECTS_CALL_FAILED,
    MIGRATION_PROGRESS_STATUS,
    MIGRATION_PROGRESS_STATUS_SUCCEEDED,
    MIGRATION_LOADING_SCREEN,
    FETCH_ARCHIVE_DETAILS,
    FETCH_ARCHIVE_DETAILS_SUCCEEDED,
    IMPORT_PROJECTS,
    IMPORT_INITIATE_SUCCEEDED,
    HIDE_IMPORT_ARCHIVE_SELECTION_MODAL,
    PROJECT_SELECTION_CHANGE,
} from '../constants';
import {task, selectedProjects, archiveName, userId, archiveDetails} from '../../../../fixtures/task';
describe('MigrationUtilityContainer actions', () => {

    describe('export projects action', () => {
        it('has a type of EXPORT_PROJECTS with correct parameters in payload', () => {
            //then
            const expected = {type: EXPORT_PROJECTS, payload: {
                selectedProjects, archiveName, userId,
            }};
            expect(exportProjects(selectedProjects, archiveName, userId)).toEqual(expected);
        });

        it('should issue action EXPORT_PROJECTS_CALL_SUCCEEDED with task in payload', () => {
            //then
            const expected = {
                type: EXPORT_PROJECTS_CALL_SUCCEEDED,
                payload: task,
            };
            expect(exportProjectsSucceeded(task)).toEqual(expected);
        });

        it('should issue action EXPORT_PROJECTS_CALL_FAILED', () => {
            //then
            const expected = {
                type: EXPORT_PROJECTS_CALL_FAILED,
            };
            expect(exportCallFailed()).toEqual(expected);
        });

    });

    describe('progress loader actions', () => {
        it('should dispatch action MIGRATION_LOADING_SCREEN with payload true(show)', () => {
            //then
            const expected = {
                type: MIGRATION_LOADING_SCREEN,
                payload: true,
            };
            expect(migrationloadingScreen(true)).toEqual(expected);
        });

        it('should dispatch action MIGRATION_LOADING_SCREEN with payload false(hide)', () => {
            //then
            const expected = {
                type: MIGRATION_LOADING_SCREEN,
                payload: false,
            };
            expect(migrationloadingScreen(false)).toEqual(expected);
        });
    });

    describe('migration status actions', () => {

        it('should dispatch action MIGRATION_PROGRESS_STATUS', () => {
            //then
            const expected = {
                type: MIGRATION_PROGRESS_STATUS,
            };
            expect(fetchMigrationUtilityStatus()).toEqual(expected);
        });

        it('should dispatch action MIGRATION_PROGRESS_STATUS_SUCCEEDED with task in payload', () => {
            //then
            const expected = {
                type: MIGRATION_PROGRESS_STATUS_SUCCEEDED,
                payload: task,
            };
            expect(fetchMigrationUtilityStatusSucceeded(task)).toEqual(expected);
        });
    });

    describe('fetch archive details actions', () => {

        it('should dispatch action FETCH_ARCHIVE_DETAILS', () => {
            const expected = {
                type: FETCH_ARCHIVE_DETAILS,
            };
            expect(fetchArchiveDetails()).toEqual(expected);
        });

        it('should dispatch action FETCH_ARCHIVE_DETAILS_SUCCEEDED', () => {
            const expected = {
                type: FETCH_ARCHIVE_DETAILS_SUCCEEDED,
                payload: archiveDetails,
            };
            expect(fetchArchiveDetailsSucceeded(archiveDetails)).toEqual(expected);
        });
    });

    describe('import actions', () => {

        it('should disptach action IMPORT_PROJECTS', () => {
            const expected = {
                type: IMPORT_PROJECTS,
                payload: {
                    archiveName,
                    userId,
                },
            };
            expect(importProjects(archiveName, userId)).toEqual(expected);
        });

        it('should dispatch action IMPORT_INITIATE_SUCCEEDED', () => {
            const expected = {
                type: IMPORT_INITIATE_SUCCEEDED,
                payload: task,
            };
            expect(importInitiateSucceeded(task)).toEqual(expected);
        });
    });

    it('should dispatch action HIDE_IMPORT_ARCHIVE_SELECTION_MODAL', () => {
        const expected = {
            type: HIDE_IMPORT_ARCHIVE_SELECTION_MODAL,
        };
        expect(hideImportArchiveSelectionModal()).toEqual(expected);
    });

    it('should dispatch action PROJECT_SELECTION_CHANGE', () => {
        const selected = ['aaa'];
        const expected = {
            type: PROJECT_SELECTION_CHANGE,
            selected,
        };
        expect(selectionChange(selected)).toEqual(expected);
    });
});
