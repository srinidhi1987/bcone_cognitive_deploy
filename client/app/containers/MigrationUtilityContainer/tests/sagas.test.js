import {call, put} from 'redux-saga/effects';
import {
    exportProjectsSucceeded,
    migrationloadingScreen,
    fetchMigrationUtilityStatusSucceeded,
    fetchArchiveDetailsSucceeded,
    importInitiateSucceeded,
    selectionChange,
    resetMigrationUtilityToMainPage,
} from '../actions';
import {
    EXPORT_PROJECTS,
    EXPORT_PROJECTS_CALL_SUCCEEDED_ALERT,
    IMPORT_PROJECTS,
    IMPORT_PROJECTS_CALL_SUCCEEDED_ALERT,
} from '../constants';
import {
    exportProjectsFlow,
    fetchMigrationUtilityStatusFlow,
    fetchArchiveDetailsFlow,
    importFlow,
} from '../sagas';
import {displayAlert} from '../../AlertsContainer/actions';
import {task, selectedProjects, archiveName, userId, archiveDetails} from '../../../../fixtures/task';
import {ALERT_LEVEL_COMMON, ALERT_LEVEL_DANGER} from '../../AlertsContainer/constants';
import {migrationUtilityBackEnd} from '../../../resources/migrationUtilityBackEnd';

const jsonParseSpy = jest.spyOn(JSON, 'parse');

describe('migration utility container saga', () => {
    afterEach(() => {
        jest.clearAllMocks();
        jsonParseSpy.mockReset();
    });
    describe('exportProjectsFlow generator', () => {
        //given
        const exportAction = {
            type: EXPORT_PROJECTS,
            payload : {
                selectedProjects,
                archiveName,
                userId,
            },
        };

        describe('export projects success flow', () => {
            //given
            const exportProjectGenerator = exportProjectsFlow(exportAction);

            it('should display loading window', () => {
                //when
                const loadingDescriptor = exportProjectGenerator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(true)));
            });

            it('should export data for selected projects', () => {
                //when
                const exportDescriptor = exportProjectGenerator.next().value;
                //then
                expect(exportDescriptor).toEqual(call(
                    migrationUtilityBackEnd.exportProjects,
                    exportAction.payload.selectedProjects,
                    exportAction.payload.archiveName,
                    exportAction.payload.userId,
                ));
            });

            it('should pass success result to reducer', () => {
                //given
                const exportResult = {
                    data: task,
                    success: true,
                    errors: [],
                };
                //when
                const result = exportProjectGenerator.next(exportResult);
                //then
                expect(result.value).toEqual(put(exportProjectsSucceeded(exportResult.data)));
            });

            it('should display loading screen', () => {
                //when
                const loadingDescriptor = exportProjectGenerator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(false)));
            });

            it('should display alert for export success', () => {
                //when
                const alertDescriptor = exportProjectGenerator.next().value;
                //then
                expect(alertDescriptor).toEqual(put(displayAlert(EXPORT_PROJECTS_CALL_SUCCEEDED_ALERT, ALERT_LEVEL_COMMON)));
            });

            it('should clear selection', () => {
                //when
                const selctionChangeDescriptor = exportProjectGenerator.next().value;
                //then
                expect(selctionChangeDescriptor).toEqual(put(selectionChange([])));
            });
        });

        describe('export projects fail flow', () => {
            //given
            const exportProjectGenerator = exportProjectsFlow(exportAction);
            exportProjectGenerator.next();
            exportProjectGenerator.next();
            const exportResult = {
                data: null,
                success: false,
                errors: ['error'],
            };
            it('should hide loading screen', () => {
                //when
                const result = exportProjectGenerator.next(exportResult);
                //then
                expect(result.value).toEqual(put(migrationloadingScreen(false)));
            });

            it('should display alert for export failure', () => {
                //when
                const alertDescriptor = exportProjectGenerator.next().value;
                //then
                expect(alertDescriptor).toEqual(put(displayAlert(exportResult.errors[0], ALERT_LEVEL_DANGER)));
            });
        });
    });
    describe('fetchMigrationUtilityStatusFlow generator', () => {
        //given
        const statusGenerator = fetchMigrationUtilityStatusFlow();
        describe('fetch migrtion status success flow', () => {

            it('should fetch status form backend server', () => {
                //when
                const statusCallDescriptor = statusGenerator.next().value;
                //then
                expect(statusCallDescriptor).toEqual(call(migrationUtilityBackEnd.fetchMigrationUtilityStatusCall));
            });

            it('pass task info to reducer', () => {
                //given
                const statusResult = {
                    data: task,
                    success: true,
                    errors: [],
                };
                //when
                const result = statusGenerator.next(statusResult);
                //then
                expect(result.value).toEqual(put(fetchMigrationUtilityStatusSucceeded(statusResult.data)));
            });
        });
    });
    describe('fetchArchiveDetailsFlow generator', () => {

        describe('fetch archive details success flow', () => {
            //given
            const generator = fetchArchiveDetailsFlow();
            it('should display loading screen', () => {
                //when
                const loadingDescriptor = generator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(true)));
            });
            it('should get archive result', () => {
                const callDescriptor = generator.next().value;
                expect(callDescriptor).toEqual(call(migrationUtilityBackEnd.fetchArchiveDetailsCall));
            });
            it('pass archive details info to reducer', () => {
                //given
                const statusResult = {
                    data: archiveDetails,
                    success: true,
                    errors: [],
                };
                //when
                const result = generator.next(statusResult);
                //then
                expect(result.value).toEqual(put(fetchArchiveDetailsSucceeded(statusResult.data)));
            });
            it('should hide loading screen', () => {
                //when
                const loadingDescriptor = generator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(false)));
            });
        });

        describe('fetch archive details fail flow', () => {
            const generator = fetchArchiveDetailsFlow();
            it('should display loading screen', () => {
                //when
                const loadingDescriptor = generator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(true)));
            });
            it('should get archive result', () => {
                const callDescriptor = generator.next().value;
                expect(callDescriptor).toEqual(call(migrationUtilityBackEnd.fetchArchiveDetailsCall));
            });
            it('should hide loading screen', () => {
                //given
                const statusResult = {
                    data: [],
                    success: false,
                    errors: [],
                };
                //when
                const loadingDescriptor = generator.next(statusResult).value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(false)));
            });
        });
    });

    describe('importFlow generator', () => {
        //given
        const importAction = {
            type: IMPORT_PROJECTS,
            payload: {
                archiveName,
                userId,
                option: 'clear',
            },
        };
        describe('import project success flow', () => {
            //given
            const generator = importFlow(importAction);

            it('should reset migration utility to main page', () => {
                //when
                const result = generator.next().value;
                //then
                expect(result).toEqual(put(resetMigrationUtilityToMainPage()));
            });

            it('should display loading screen', () => {
                //when
                const loadingDescriptor = generator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(true)));
            });

            it('should post import request', () => {
                //when
                const callDescriptor = generator.next().value;
                //then
                expect(callDescriptor).toEqual(call(migrationUtilityBackEnd.importProjectsCall, archiveName, userId, importAction.payload.option));
            });

            it('should pass import result to reducer', () => {
                const result = {
                    success: true,
                    data: task,
                    error: [],
                };
                const descriptor = generator.next(result).value;
                expect(descriptor).toEqual(put(importInitiateSucceeded(result.data)));
            });

            it('should hide loading screen', () => {
                //when
                const loadingDescriptor = generator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(false)));
            });

            it('should display success alert', () => {
                const alertDescriptor = generator.next().value;
                expect(alertDescriptor).toEqual(put(displayAlert(IMPORT_PROJECTS_CALL_SUCCEEDED_ALERT, ALERT_LEVEL_COMMON)));
            });

            it('should clear selection', () => {
                //when
                const selctionChangeDescriptor = generator.next().value;
                //then
                expect(selctionChangeDescriptor).toEqual(put(selectionChange([])));
            });
        });
        describe('import project fail flow', () => {

            //given
            const generator = importFlow(importAction);
            const result = {
                success: false,
                data: [],
                errors: ['error'],
            };

            it('should reset migration utility to main page', () => {
                //when
                const result = generator.next().value;
                //then
                expect(result).toEqual(put(resetMigrationUtilityToMainPage()));
            });

            it('should display loading screen', () => {
                //when
                const loadingDescriptor = generator.next().value;
                //then
                expect(loadingDescriptor).toEqual(put(migrationloadingScreen(true)));
            });

            it('should post import request', () => {
                //when
                const callDescriptor = generator.next().value;
                //then
                expect(callDescriptor).toEqual(call(migrationUtilityBackEnd.importProjectsCall, archiveName, userId, importAction.payload.option));
            });

            it('should hide loading screen', () => {
                //when
                const descriptor = generator.next(result).value;
                //then
                expect(descriptor).toEqual(put(migrationloadingScreen(false)));
            });

            it('should display fail alert', () => {
                //when
                const alertDescriptor = generator.next().value;
                //then
                expect(alertDescriptor).toEqual(put(displayAlert(result.errors[0], ALERT_LEVEL_DANGER)));
            });
        });
    });
});
