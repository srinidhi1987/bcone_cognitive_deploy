import {shallow} from 'enzyme';
import React from 'react';
import {MigrationUtilityContainer} from '../index';
import {Project1, Project2} from '../../../../fixtures/projects';
import {task, archiveDetails} from '../../../../fixtures/task';
import {
    DataTable,
    CommandButton,
    PageTitle,
} from 'common-frontend-components';
import ActionConfirmationModal from '../../../components/ActionConfirmationModal';
import ImportArchiveSelectionComponent from '../../../components/ImportArchiveSelectionComponent';
import {
    EXPORT_CONFIRMATION_BOX_TITLE,
    EXPORT_CONFIRMATION_BOX_DESCRIPTION,
    EXPORT_CONFIRMATION_BOX_INPUT_LABEL,
    CONFIRMATION_BOX_CANCEL_TEXT,
    EXPORT_CONFIRMATION_BOX_CONFIRM_TEXT,
    EXPORT_DEFAULT_ARCHIVE_NAME,
} from '../constants';
import {themeType} from '../../../components/ActionConfirmationModal';
import PageDescription from '../../../components/PageDescription';

describe('<MigrationUtilityContainer />', () => {

    describe('when projects are available', () => {
        //given
        const wrapper = shallow(<MigrationUtilityContainer
            fetchProjects ={() => {}}
            fetchMigrationUtilityStatus ={() => {}}
            getLastMigrationInfo={jest.fn()}
            initialSortedProjects = {[Project2, Project1]}
            lastMigrationStatus={{endTime: 23423423423}}
        />);

        it('should have page title & description', () => {
            //when
            const pageDescriptionWrapper = wrapper.find(PageDescription);
            const titleWrapper = wrapper.find(PageTitle);
            //then
            expect(pageDescriptionWrapper.length).toEqual(1);
            expect(titleWrapper.length).toEqual(1);
        });
        it('should display project listing', () => {
            //when
            const datatableWrapper = wrapper.find(DataTable);
            //then
            expect(datatableWrapper.length).toEqual(1);
        });

        it('should disabled button when no project is selected', () => {
            //when
            const commandButtonsWrapper = wrapper.find(CommandButton);
            const {disabled} = commandButtonsWrapper.at(1).props();
            //then
            expect(commandButtonsWrapper.length).toEqual(2);
            expect(disabled).toEqual(true);
        });

        it('should render but hide ActionConfirmationModal', () => {
            //when
            const confirmationWrapper = wrapper.find(ActionConfirmationModal);
            const {show} = confirmationWrapper.at(0).props();
            //then
            expect(show).toEqual(false);
        });
        it('should show import button enabled', () => {
            //when
            const buttonWrapper = wrapper.find(CommandButton);
            const {disabled} = buttonWrapper.at(0).props();
            //then
            expect(disabled).toEqual(false);
        });
        it('should not render on hide ImportArchiveSelectionComponent', () => {
            //when
            const archiveSelectionWrapper = wrapper.find(ImportArchiveSelectionComponent);
            //then
            expect(archiveSelectionWrapper.length).toEqual(0);
        });
    });

    describe('when prjects are selected', () => {
        //given
        const wrapper = shallow(<MigrationUtilityContainer
            getLastMigrationInfo={jest.fn()}
            fetchProjects ={() => {}}
            fetchMigrationUtilityStatus ={() => {}}
            initialSortedProjects = {[Project2, Project1]}
            selected={[Project1.id]}
            lastMigrationStatus={{endTime: 23423423423}}
        />);
        it('should enable export button when atleast 1 project is selected', () => {
            //when
            const commandButtonsWrapper = wrapper.find(CommandButton);
            const {disabled} = commandButtonsWrapper.at(1).props();
            //then
            expect(disabled).toEqual(false);
        });
        it('should show Confirmation box when state is set to true by click on export', () => {
            //when
            wrapper.setState({showExportActionConfirmationDetail: true});
            const confirmationWrapper = wrapper.find(ActionConfirmationModal);
            const {show} = confirmationWrapper.at(0).props();
            //then
            expect(show).toEqual(true);
        });
        it('should pass right parameters to confirmation modal', () => {
            //when
            const confirmationWrapper = wrapper.find(ActionConfirmationModal);
            const {title, paragraphs, cancelTitle, confirmTitle, param, inputLabel, theme, defaultInputValue} = confirmationWrapper.at(0).props();
            //then
            expect(title).toEqual(EXPORT_CONFIRMATION_BOX_TITLE);
            expect(paragraphs).toEqual([EXPORT_CONFIRMATION_BOX_DESCRIPTION]);
            expect(cancelTitle).toEqual(CONFIRMATION_BOX_CANCEL_TEXT);
            expect(confirmTitle).toEqual(EXPORT_CONFIRMATION_BOX_CONFIRM_TEXT);
            expect(param).toEqual([Project1.id]);
            expect(inputLabel).toEqual(EXPORT_CONFIRMATION_BOX_INPUT_LABEL);
            expect(theme).toEqual(themeType.default);
            expect(defaultInputValue).toEqual(EXPORT_DEFAULT_ARCHIVE_NAME);
        });
    });

    describe('when there is no projects', () => {
        //given
        const wrapper = shallow(<MigrationUtilityContainer
            fetchProjects ={() => {}}
            fetchMigrationUtilityStatus ={() => {}}
            initialSortedProjects = {[]}
            lastMigrationStatus={{endTime: 23423423423}}
            getLastMigrationInfo={jest.fn()}
        />);
        it('should only show single button for import', () => {
            //when
            const buttonWrapper = wrapper.find(CommandButton);
            //then
            expect(buttonWrapper.at(0).length).toEqual(1);
        });
    });

    describe('when migration is in progress', () => {
        //given
        const wrapper = shallow(<MigrationUtilityContainer
            fetchProjects ={() => {}}
            fetchMigrationUtilityStatus ={() => {}}
            initialSortedProjects = {[Project2, Project1]}
            currentTaskInfo={task}
            showExportMigrationStatus={true}
            showImportMigrationStatus={false}
            getLastMigrationInfo={jest.fn()}
            lastMigrationStatus={{endTime: 23423423423}}
        />);
        it('should show message', () => {
            //when
            const messageWrapper = wrapper.find({className: 'aa-grid-row vertical-center horizontal-center aa-message-container'});
            //then
            expect(messageWrapper.length).toEqual(1);
        });
        it('should not display prject listing', () => {
            //when
            const datatableWrapper = wrapper.find(DataTable);
            //then
            expect(datatableWrapper.length).toEqual(0);
        });
        it('should not show any import/export button', () => {
            //when
            const buttonWrapper = wrapper.find(CommandButton);
            //then
            expect(buttonWrapper.length).toEqual(0);
        });
    });

    describe('import functionality', () => {
        describe('import selection modal', () => {
            //given
            const wrapper = shallow(<MigrationUtilityContainer
                fetchProjects ={() => {}}
                fetchMigrationUtilityStatus ={() => {}}
                initialSortedProjects = {[Project2, Project1]}
                showImportArchiveSelectionDetail={true}
                archiveDetails={archiveDetails}
                getLastMigrationInfo={jest.fn()}
                lastMigrationStatus={{endTime: 23423423423}}
            />);
            const archiveSelectionWrapper = wrapper.find(ImportArchiveSelectionComponent);
            it('should show archive selection modal', () => {
                //when
                const {show} = archiveSelectionWrapper.at(0).props();
                //then
                expect(show).toEqual(true);
            });
            it('should pass archive options to selection modal', () => {
                //when
                const {selectionOptions} = archiveSelectionWrapper.at(0).props();
                //then
                expect(selectionOptions).toEqual(archiveDetails);
            });
        });
    });

});
