import migrationUtilityContainerReducer, {initialState} from '../reducer';

import {
    exportProjectsSucceeded,
    migrationloadingScreen,
    fetchMigrationUtilityStatusSucceeded,
    fetchArchiveDetailsSucceeded,
    importInitiateSucceeded,
    hideImportArchiveSelectionModal,
    selectionChange,
} from '../actions';
import {task, archiveDetails} from '../../../../fixtures/task';

describe('migrationUtilityContainerReducer', () => {

    it('should return initial state', () => {
        //then
        expect(migrationUtilityContainerReducer(undefined, {})).toEqual(initialState);
    });

    describe('EXPORT_PROJECTS_CALL_SUCCEEDED action', () => {

        it('should pass task info to store', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, exportProjectsSucceeded(task));
            //then
            expect(resultState.get('currentTaskInfo').toJS()).toEqual(task);
        });
    });

    describe('MIGRATION_PROGRESS_STATUS_SUCCEEDED action', () => {

        it('should pass task info to store', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, fetchMigrationUtilityStatusSucceeded(task));
            //then
            expect(resultState.get('currentTaskInfo').toJS()).toEqual(task);
        });
    });

    describe('IMPORT_INITIATE_SUCCEEDED action', () => {

        it('should pass task info to store', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, importInitiateSucceeded(task));
            //then
            expect(resultState.get('currentTaskInfo').toJS()).toEqual(task);
        });
    });

    describe('MIGRATION_LOADING_SCREEN action', () => {

        it('should set loading state to true', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, migrationloadingScreen(true));
            //then
            expect(resultState.get('loading')).toEqual(true);
        });

        it('should set loading state to false', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, migrationloadingScreen(false));
            //then
            expect(resultState.get('loading')).toEqual(false);
        });
    });

    describe('FETCH_ARCHIVE_DETAILS_SUCCEEDED action', () => {

        it('should set archive details to payload and make selection confirmation modal visible', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, fetchArchiveDetailsSucceeded(archiveDetails));
            //then
            expect(resultState.get('archiveDetails')).toEqual(archiveDetails);
            expect(resultState.get('showImportArchiveSelectionDetail')).toEqual(true);
        });
    });

    describe('HIDE_IMPORT_ARCHIVE_SELECTION_MODAL action', () => {
        it('should hide archive selection confirmation modal', () => {
            //when
            const resultState = migrationUtilityContainerReducer(initialState, hideImportArchiveSelectionModal());
            //then
            expect(resultState.get('showImportArchiveSelectionDetail')).toEqual(false);
        });
    });

    describe('PROJECT_SELECTION_CHANGE action', () => {
        it('should set the selected project in state', () => {
            //when
            const selected = ['aaa'];
            const resultState = migrationUtilityContainerReducer(initialState, selectionChange(selected));
            //then
            expect(resultState.get('selected').toJS()).toEqual(selected);
        });
    });
});
