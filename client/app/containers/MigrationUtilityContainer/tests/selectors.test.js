import {fromJS} from 'immutable';

import selectUtilityContainer from '../selectors';
import {task} from '../../../../fixtures/task';
import {Project2, Project1} from '../../../../fixtures/projects';
let mockedState, selector;

describe('selectUtilityContainer selector', () => {
    //given
    mockedState = fromJS({
        appContainer: {
            user: {
                username: 'services',
            },
        },
        projectsContainer: {
            projects: [Project2, Project1],
            loadingProjects: true,
        },
        initialSortedProjects: [Project1, Project2],
        migrationUtilityContainer: {
            currentTaskInfo: task,
            loading: false,
        },
    });
    // when
    selector = selectUtilityContainer();

    it('should return projects', () => {
        //then
        expect(selector(mockedState).projects).toEqual([Project2, Project1]);
    });

    it('should return initial Sorted Projects', () => {
        //then
        expect(selector(mockedState).initialSortedProjects).toEqual([Project1, Project2]);
    });

    it('should return currentTaskInfo', () => {
        //then
        expect(selector(mockedState).currentTaskInfo).toEqual(task);
    });

    it('should return loading state', () => {
        //then
        expect(selector(mockedState).loading).toEqual(false);
    });

    it('should return loadingProjects state', () => {
        //then
        expect(selector(mockedState).loadingProjects).toEqual(true);
    });

    it('should return user from state', () => {
        //then
        expect(selector(mockedState).user).toEqual({
            username: 'services',
        });
    });

    it('should return showImportMigrationStatus from state', () => {
        //then
        expect(selector(mockedState).showImportMigrationStatus).toEqual(false);
    });

    it('should return showExportMigrationStatus from state', () => {
        //then
        expect(selector(mockedState).showExportMigrationStatus).toEqual(true);
    });
    it('should return path and file name form state', () => {
        //then
        expect(selector(mockedState).exportPath).toEqual('E:\\AA\IQBot_Output\\BackupData\\');
        expect(selector(mockedState).exportArchiveFileName).toEqual('File Name');
    });
});
