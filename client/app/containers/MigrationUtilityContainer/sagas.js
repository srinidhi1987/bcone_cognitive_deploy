import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {
    EXPORT_PROJECTS,
    MIGRATION_PROGRESS_STATUS,
    EXPORT_PROJECTS_CALL_SUCCEEDED_ALERT,
    EXPORT_PROJECTS_CALL_FAILED_ALERT,
    FETCH_ARCHIVE_DETAILS,
    IMPORT_PROJECTS,
    IMPORT_PROJECTS_CALL_SUCCEEDED_ALERT,
    IMPORT_PROJECTS_CALL_FAILED_ALERT,
    GET_LAST_MIGRATION,
} from './constants';
import {
    exportProjectsSucceeded,
    fetchMigrationUtilityStatusSucceeded,
    migrationloadingScreen,
    fetchArchiveDetailsSucceeded,
    importInitiateSucceeded,
    resetMigrationUtilityToMainPage,
    selectionChange,
    populateLastMigrationStatus,
} from './actions';
import {displayAlert} from '../AlertsContainer/actions';
import {ALERT_LEVEL_COMMON, ALERT_LEVEL_DANGER} from '../AlertsContainer/constants';
import {migrationUtilityBackEnd} from '../../resources/migrationUtilityBackEnd';
/**
 * initiate export process
 * @param {object} action action detail
 */
export function* exportProjectsFlow(action) {
    try {
        yield put(migrationloadingScreen(true));
        const {
            selectedProjects,
            archiveName,
            userId,
        } = action.payload;
        const result = yield call(migrationUtilityBackEnd.exportProjects, selectedProjects, archiveName, userId);
        if (result.success) {
            yield put(exportProjectsSucceeded(result.data));
            yield put(migrationloadingScreen(false));
            yield put(displayAlert(EXPORT_PROJECTS_CALL_SUCCEEDED_ALERT, ALERT_LEVEL_COMMON));
            yield put(selectionChange([]));
            return;
        }
        yield put(migrationloadingScreen(false));
        yield put(displayAlert(result.errors[0], ALERT_LEVEL_DANGER));
    }
    catch (e) {
        yield put(migrationloadingScreen(false));
        yield put(displayAlert(EXPORT_PROJECTS_CALL_FAILED_ALERT, ALERT_LEVEL_DANGER));
    }
}

/**
 * migration status flow
 * @param {object} action  action detail
 */
export function* fetchMigrationUtilityStatusFlow() {
    const result = yield call(migrationUtilityBackEnd.fetchMigrationUtilityStatusCall);
    if (result.success) {
        yield put(fetchMigrationUtilityStatusSucceeded(result.data));
    }
}

/**
 * fetchArchiveDetailsFlow - saga
 */
export function* fetchArchiveDetailsFlow() {
    try {
        yield put(migrationloadingScreen(true));
        const result = yield call(migrationUtilityBackEnd.fetchArchiveDetailsCall);
        if (result.success) {
            yield put(fetchArchiveDetailsSucceeded(result.data));
        }
    }
    finally {
        yield put(migrationloadingScreen(false));
    }
}

/**
 * importFlow
 * @param {object} action action type and payload
 */
export function* importFlow(action) {
    try {
        yield put(resetMigrationUtilityToMainPage());
        yield put(migrationloadingScreen(true));
        const result = yield call(migrationUtilityBackEnd.importProjectsCall, action.payload.archiveName, action.payload.userId, action.payload.option);
        if (result.success) {
            yield put(importInitiateSucceeded(result.data));
            yield put(migrationloadingScreen(false));
            yield put(displayAlert(IMPORT_PROJECTS_CALL_SUCCEEDED_ALERT, ALERT_LEVEL_COMMON));
            yield put(selectionChange([]));
            return;
        }
        yield put(migrationloadingScreen(false));
        yield put(displayAlert(result.errors[0], ALERT_LEVEL_DANGER));
    }
    catch (e) {
        yield put(migrationloadingScreen(false));
        yield put(displayAlert(IMPORT_PROJECTS_CALL_FAILED_ALERT, ALERT_LEVEL_DANGER));
    }
}

export function* refreshLastMigrationFlow() {
    try {
        const result = yield call(migrationUtilityBackEnd.getLastMigrationInfo);
        if (!result.success) throw new Error('There was an issue getting the last migration status');
        yield put(populateLastMigrationStatus(result.data));
    }
    catch (e) {
        yield put(displayAlert(e.message, ALERT_LEVEL_DANGER));
    }
}

// All sagas to be loaded
export default [
    function* () {
        yield* takeLatest(EXPORT_PROJECTS, exportProjectsFlow);
    },
    function* () {
        yield* takeLatest(MIGRATION_PROGRESS_STATUS, fetchMigrationUtilityStatusFlow);
    },
    function* () {
        yield* takeLatest(FETCH_ARCHIVE_DETAILS, fetchArchiveDetailsFlow);
    },
    function* () {
        yield takeLatest(IMPORT_PROJECTS, importFlow);
    },
    function* () {
        yield takeLatest(GET_LAST_MIGRATION, refreshLastMigrationFlow);
    },
];
