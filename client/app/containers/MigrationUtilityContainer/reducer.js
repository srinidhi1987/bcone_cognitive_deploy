/*
 *
 * MigrationUtilityContainer reducer
 *
 */
import {fromJS} from 'immutable';
import {
    EXPORT_PROJECTS_CALL_SUCCEEDED,
    MIGRATION_PROGRESS_STATUS_SUCCEEDED,
    MIGRATION_LOADING_SCREEN,
    FETCH_ARCHIVE_DETAILS_SUCCEEDED,
    HIDE_IMPORT_ARCHIVE_SELECTION_MODAL,
    IMPORT_INITIATE_SUCCEEDED,
    IS_SHOWING_IMPORT_OPTIONS,
    POPULATE_LAST_MIGRATION_STATUS,
    PROJECT_SELECTION_CHANGE,
    SET_ARCHIVE_NAME,
    SET_MIGRATION_IMPORT_OPTION,
    TOGGLE_OPTIONS_CONFIRMATION_MODAL,
} from './constants';

const initialState = fromJS({
    archiveDetails: null,
    archiveName: null,
    currentTaskInfo: null,
    isShowingImportOptions: false,
    isShowingImportOptionsConfirmationModal: false,
    loading: false,
    migrationImportOption: 'append',
    showImportArchiveSelectionDetail: false,
    selected:[],
    lastMigrationStatus: null,
});

function migrationUtilityContainerReducer(state = initialState, action) {
    switch (action.type) {
        case EXPORT_PROJECTS_CALL_SUCCEEDED:
        case IMPORT_INITIATE_SUCCEEDED:
        case MIGRATION_PROGRESS_STATUS_SUCCEEDED:
            return state.set('currentTaskInfo', fromJS(action.payload));
        case MIGRATION_LOADING_SCREEN:
            return state.set('loading', action.payload);
        case FETCH_ARCHIVE_DETAILS_SUCCEEDED:
            return state.set('archiveDetails', action.payload).set('showImportArchiveSelectionDetail', true);
        case HIDE_IMPORT_ARCHIVE_SELECTION_MODAL:
            return state
                .set('showImportArchiveSelectionDetail', false)
                .set('isShowingImportOptionsConfirmationModal', false);
        case POPULATE_LAST_MIGRATION_STATUS:
            return state
                .set('lastMigrationStatus', action.payload);
        case PROJECT_SELECTION_CHANGE:
            return state.set('selected', fromJS(action.selected));
        case IS_SHOWING_IMPORT_OPTIONS:
            return state.set('isShowingImportOptions', action.isShowing);
        case SET_ARCHIVE_NAME:
            return state.set('archiveName', action.value);
        case SET_MIGRATION_IMPORT_OPTION:
            return state.set('migrationImportOption', action.value);
        case TOGGLE_OPTIONS_CONFIRMATION_MODAL:
            return state.set('isShowingImportOptionsConfirmationModal', action.isShowing);
        default:
            return state;
    }
}

export default migrationUtilityContainerReducer;
export {initialState};
