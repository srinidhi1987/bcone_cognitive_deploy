import {createSelector} from 'reselect';
import {compareKey} from 'common-frontend-utils';

const selectAppContainerDomain = () => (state) => state.get('appContainer');
const selectProjectsContainerDomain = () => (state) => state.get('projectsContainer');
const selectMigrationUtilityContainer = () => (state) => state.get('migrationUtilityContainer');
/**
 * Initially sorts project to match DataTable component sort - workaround that may not always be needed
 */
const initiallySortProjects = () => createSelector(
    selectProjectsContainerDomain(),
    (projectState) => projectState.get('projects').toJS().sort(compareKey('name'))
);

/**
 * Set show import progress message to true or false
 */
const importMigrationStatus = () => createSelector(
    selectMigrationUtilityContainer(),
    (migrationUtilityState) => {
        const task = migrationUtilityState.get('currentTaskInfo');
        return task && task.get('taskType') === 'imports' && task.get('status') === 'inprogress' ? true : false;
    }
);

/**
 * Set show export progress message to true or false
 */
const exportMigrationStatus = () => createSelector(
    selectMigrationUtilityContainer(),
    (migrationUtilityState) => {
        const task = migrationUtilityState.get('currentTaskInfo');
        return task && task.get('taskType') === 'exports' && task.get('status') === 'inprogress' ? true : false;
    }
);

/**
 * parse export path to split
 */
const exportPath = () => createSelector(
    exportMigrationStatus(),
    selectMigrationUtilityContainer(),
    (exportMigrationStatus, migrationUtilityState) => {
        if (!exportMigrationStatus) return {};
        const currentTaskInfo = migrationUtilityState.get('currentTaskInfo');
        const fullPath = currentTaskInfo.get('description');
        const lastIndex = fullPath.length - fullPath.split('').reverse().indexOf('\\');
        const exportPath = fullPath.slice(0, lastIndex);
        const exportArchiveFileName = fullPath.slice(lastIndex, fullPath.length);
        return {exportPath, exportArchiveFileName};
    }
);

const selectUtilityContainer = () => createSelector(
    selectAppContainerDomain(),
    selectProjectsContainerDomain(),
    initiallySortProjects(),
    selectMigrationUtilityContainer(),
    importMigrationStatus(),
    exportMigrationStatus(),
    exportPath(),
    (
        appState,
        projectState,
        initialSortedProjects,
        migrationUtilityState,
        showImportMigrationStatus,
        showExportMigrationStatus,
        exportPath,
    ) => {
        return {
            user: appState.get('user').toJS(),
            ...projectState.toJS(),
            initialSortedProjects,
            ...migrationUtilityState.toJS(),
            showImportMigrationStatus,
            showExportMigrationStatus,
            ...exportPath,
        };
    }
);

export default selectUtilityContainer;
