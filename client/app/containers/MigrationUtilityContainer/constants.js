import React from 'react';

export const EXPORT_PROJECTS = 'app/containers/MigrationUtilityContainer/EXPORT_PROJECTS';
export const EXPORT_PROJECTS_CALL_SUCCEEDED = 'app/containers/MigrationUtilityContainer/EXPORT_PROJECTS_CALL_SUCCEEDED';
export const EXPORT_PROJECTS_CALL_FAILED = 'app/containers/MigrationUtilityContainer/EXPORT_PROJECTS_CALL_FAILED';
export const MIGRATION_PROGRESS_STATUS = 'app/containers/MigrationUtilityContainer/MIGRATION_PROGRESS_STATUS';
export const MIGRATION_PROGRESS_STATUS_SUCCEEDED = 'app/containers/MigrationUtilityContainer/MIGRATION_PROGRESS_STATUS_SUCCEEDED';
export const MIGRATION_LOADING_SCREEN = 'app/containers/MigrationUtilityContainer/MIGRATION_LOADING_SCREEN';
export const FETCH_ARCHIVE_DETAILS = 'app/containers/MigrationUtilityContainer/FETCH_ARCHIVE_DETAILS';
export const FETCH_ARCHIVE_DETAILS_SUCCEEDED = 'app/containers/MigrationUtilityContainer/FETCH_ARCHIVE_DETAILS_SUCCEEDED';
export const IMPORT_PROJECTS = 'app/containers/MigrationUtilityContainer/IMPORT_PROJECTS';
export const HIDE_IMPORT_ARCHIVE_SELECTION_MODAL = 'app/containers/MigrationUtilityContainer/HIDE_IMPORT_ARCHIVE_SELECTION_MODAL';
export const IMPORT_INITIATE_SUCCEEDED = 'app/containers/MigrationUtilityContainer/IMPORT_INITIATE_SUCCEEDED';
export const PROJECT_SELECTION_CHANGE = 'app/containers/MigrationUtilityContainer/PROJECT_SELECTION_CHANGE';
export const IS_SHOWING_IMPORT_OPTIONS = 'app/containers/MigrationUtilityContainer/IS_SHOWING_IMPORT_OPTIONS';
export const SET_MIGRATION_IMPORT_OPTION = 'app/containers/MigrationUtilityContainer/SET_MIGRATION_IMPORT_OPTION';
export const SET_ARCHIVE_NAME = 'app/containers/MigrationUtilityContainer/SET_ARCHIVE_NAME';
export const POPULATE_LAST_MIGRATION_STATUS = 'app/containers/MigrationUtilityContainer/POPULATE_LAST_MIGRATION_STATUS';
export const TOGGLE_OPTIONS_CONFIRMATION_MODAL = 'app/containers/MigrationUtilityContainer/TOGGLE_OPTIONS_CONFIRMATION_MODAL';
export const GET_LAST_MIGRATION = 'app/containers/MigrationUtilityContainer/GET_LAST_MIGRATION';

export const EXPORT_CONFIRMATION_BOX_TITLE = 'Are you sure you\'d like to export?';
export const EXPORT_CONFIRMATION_BOX_DESCRIPTION = 'This action will collect and export all selected learning instances and their bots along with classification data. This action will not delete existing data.';
export const EXPORT_CONFIRMATION_BOX_INPUT_LABEL = 'Backup Data File Name';
export const CONFIRMATION_BOX_CANCEL_TEXT = 'Cancel';
export const EXPORT_CONFIRMATION_BOX_CONFIRM_TEXT = 'I understand, please export';
export const EXPORT_PROJECTS_CALL_SUCCEEDED_ALERT = 'Export process initiated';
export const EXPORT_PROJECTS_CALL_FAILED_ALERT = 'Error exporting projects';

export const EXPORT_LOADING_SCREEN_MESSAGE = 'Initiating export...';
export const EXPORT_DEFAULT_ARCHIVE_NAME = 'Learning Instance Backup';
export const FETCH_ARCHIVE_DETAILS_LOADING_SCREEN_MESSAGE = 'Fetching archive details...';
export const IMPORT_CONFIRMATION_BOX_CONFIRM_TEXT = 'Yes, import';
export const IMPORT_ARCHIVE_CONFIRMATION_BOX_CONFIRM_TEXT = 'Import...';
export const IMPORT_PROJECTS_CALL_SUCCEEDED_ALERT = 'Import process initiated';
export const IMPORT_PROJECTS_CALL_FAILED_ALERT = 'Error importing projects';
export const IMPORT_LOADING_SCREEN_MESSAGE = 'Initiating import...';

export const IMPORT_OPTIONS_COPY_TEXT_A = 'Migrate archived learning instances using one of import options';
export const MIGRATION_IMPORT_OPTIONS = {
    CLEAR: {
        value: 'clear',
        copyText: 'Remove all existing learning instances and replace with imported learning instances',
    },
    APPEND: {
        value: 'append',
        copyText: 'Append imported groups and trained bots to duplicate existing learning instances',
    },
    MERGE: {
        value: 'merge',
        copyText: 'Import learning instances, ignoring duplicate existing learning instances',
    },
    OVERWRITE: {
        value: 'overwrite',
        copyText: 'Overwrite duplicate existing learning instances with imported learning instances',
    },
};

export const IMPORT_OPTION_CONFIRMATION_MODAL_TITLE = 'Are you sure you want to import the archived file?';
export const IMPORT_OPTIONS_CONFIRMATION_MODAL_TEXT = {
    merge: <span>This action will import learning instances and ignore any existing learning instances with the same name. No data will be lost.</span>,
    append: <span>This action will append the imported groups and trained bots to any existing learning instances with the same name. New learning instances will also be added. No data will be lost.</span>,
    clear: 'This action will wipe all of the existing learning instances and import the new learning instances. The' +
    ' existing learning instances will be unavailable.',
    overwrite: <span>This action will overwrite existing learning instances if learning instances with the same name are imported. The existing duplicate learning instances will be unavailable.</span>,
};
