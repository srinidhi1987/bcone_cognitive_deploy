import {
    EXPORT_PROJECTS,
    EXPORT_PROJECTS_CALL_SUCCEEDED,
    EXPORT_PROJECTS_CALL_FAILED,
    FETCH_ARCHIVE_DETAILS,
    FETCH_ARCHIVE_DETAILS_SUCCEEDED,
    HIDE_IMPORT_ARCHIVE_SELECTION_MODAL,
    IMPORT_PROJECTS,
    IS_SHOWING_IMPORT_OPTIONS,
    MIGRATION_PROGRESS_STATUS,
    MIGRATION_PROGRESS_STATUS_SUCCEEDED,
    MIGRATION_LOADING_SCREEN,
    IMPORT_INITIATE_SUCCEEDED,
    POPULATE_LAST_MIGRATION_STATUS,
    PROJECT_SELECTION_CHANGE,
    GET_LAST_MIGRATION,
    SET_ARCHIVE_NAME,
    SET_MIGRATION_IMPORT_OPTION,
    TOGGLE_OPTIONS_CONFIRMATION_MODAL,
} from './constants';

/**
 * exportProject
 * @param {Array} selectedProjects the project id list that user selects
 * @param {string} archieveName  the file name that user enters
 */
export function exportProjects(selectedProjects, archiveName, userId) {
    return {
        type: EXPORT_PROJECTS,
        payload: {
            selectedProjects,
            archiveName,
            userId,
        },
    };
}

/**
 * exportProjectsSucceeded
 * @param {*} task contain task metadata
 */
export function exportProjectsSucceeded(task) {
    return {
        type: EXPORT_PROJECTS_CALL_SUCCEEDED,
        payload: task,
    };
}

/**
 * exportCallFailed
 */
export function exportCallFailed() {
    return {
        type: EXPORT_PROJECTS_CALL_FAILED,
    };
}

/**
 * migrationloadingScreen
 * @param {boolean} loading to show loading screen or hide
 */
export function migrationloadingScreen(loading) {
    return {
        type: MIGRATION_LOADING_SCREEN,
        payload: loading,
    };
}

/**
 * fetchMigrationUtilityStatus
 */
export function fetchMigrationUtilityStatus() {
    return {
        type: MIGRATION_PROGRESS_STATUS,
    };
}

/**
 * fetchMigrationUtilityStatusSucceeded
 * @param {*} task task meta data realted to export or import
 */
export function fetchMigrationUtilityStatusSucceeded(task) {
    return {
        type: MIGRATION_PROGRESS_STATUS_SUCCEEDED,
        payload: task,
    };
}

/**
 * fetchArchiveDetails - issue action to fetch arcive names from backend
 */
export function fetchArchiveDetails() {
    return {
        type: FETCH_ARCHIVE_DETAILS,
    };
}

/**
 * fetchArchiveDetailsSucceeded - pass archive file details to store
 * @param {object} data - archive file Details
 */
export function fetchArchiveDetailsSucceeded(data) {
    return {
        type: FETCH_ARCHIVE_DETAILS_SUCCEEDED,
        payload: data,
    };
}

/**
 * importProjects
 * @param {string} archiveName
 * @param {string} userId
 */
export function importProjects(archiveName, userId, option) {
    return {
        type: IMPORT_PROJECTS,
        payload: {
            archiveName,
            option,
            userId,
        },
    };
}

/**
 * hideImportArchiveSelectionModal - hide archive name selection modal
 */
export function hideImportArchiveSelectionModal() {
    return {
        type: HIDE_IMPORT_ARCHIVE_SELECTION_MODAL,
    };
}

/**
 * importInitiateSucceeded - update task to store
 * @param {object} task - task meta data
 */
export function importInitiateSucceeded(task) {
    return {
        type: IMPORT_INITIATE_SUCCEEDED,
        payload: task,
    };
}

/**
 * selectionChange - update selected projects
 * @param {array} selected is array of project ids
 */
export function selectionChange(selected) {
    return {
        type: PROJECT_SELECTION_CHANGE,
        selected,
    };
}

export function navigateToImportOptionsScreen() {
    return {
        type: IS_SHOWING_IMPORT_OPTIONS,
        isShowing: true,
    };
}

export function setMigrationImportOption(value) {
    return {
        type: SET_MIGRATION_IMPORT_OPTION,
        value,
    };
}

export function setArchiveName(value) {
    return {
        type: SET_ARCHIVE_NAME,
        value,
    };
}

export function resetMigrationUtilityToMainPage() {
    return {
        type: IS_SHOWING_IMPORT_OPTIONS,
        isShowing: false,
    };
}

export function toggleOptionsConfirmationModal(isShowing) {
    return {
        type: TOGGLE_OPTIONS_CONFIRMATION_MODAL,
        isShowing,
    };
}

export function getLastMigrationInfo() {
    return {
        type: GET_LAST_MIGRATION,
    };
}

export function populateLastMigrationStatus(payload) {
    return {
        type: POPULATE_LAST_MIGRATION_STATUS,
        payload,
    };
}
