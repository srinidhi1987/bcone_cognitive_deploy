/*
 *
 * AlertsContainer actions
 *
 */

import {
    DISPLAY_ALERT,
    ALERT_LEVEL_COMMON,
    REMOVE_ALERT_FROM_QUEUE,
} from './constants';

export function displayAlert(message, level = ALERT_LEVEL_COMMON) {
    return {
        type: DISPLAY_ALERT,
        message,
        level,
    };
}

export function removeAlertFromQueue(id) {
    return {
        type: REMOVE_ALERT_FROM_QUEUE,
        id,
    };
}
