import {createSelector} from 'reselect';

/**
 * Direct selector to the alertsContainer state domain
 */
const selectAlertsContainerDomain = () => (state) => state.get('alertsContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AlertsContainer
 */

const selectAlertsContainer = () => createSelector(
    selectAlertsContainerDomain(),
    (substate) => substate.toJS()
);

export default selectAlertsContainer;
export {
    selectAlertsContainerDomain,
};
