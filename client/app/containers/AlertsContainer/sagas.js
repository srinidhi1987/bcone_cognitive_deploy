import {takeEvery} from 'redux-saga';
import {put} from 'redux-saga/effects';
import {displayAlert} from './actions';
import {CREATE_PROJECT_FAILED} from '../NewProjectContainer/constants';

export function* sendAlert(action) {
    yield put(displayAlert(action.message, action.level));
}

export function* alertsSaga() {
    yield* takeEvery(CREATE_PROJECT_FAILED, sendAlert);
}

// All sagas to be loaded
export default [
    alertsSaga,
];
