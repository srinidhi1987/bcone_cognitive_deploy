/*
 *
 * AlertsContainer reducer
 *
 */

import {fromJS, List} from 'immutable';
import {
    DISPLAY_ALERT,
    REMOVE_ALERT_FROM_QUEUE,
} from './constants';

const initialState = fromJS({
    alerts: [],
});

const s4 = () => {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};

function alertsContainerReducer(state = initialState, action) {
    switch (action.type) {
        case DISPLAY_ALERT:
            return state.set('alerts', state.get('alerts').unshift({
                message: action.message,
                level: action.level,
                id: `${s4() + s4()}-${s4()}-${s4()}-${s4()}-${s4()}-${s4() + s4() + s4()}`,
                createdAt: new Date(),
            }));
        case REMOVE_ALERT_FROM_QUEUE:
            return state.set('alerts', state.get('alerts').reduce((acc, alert) => {
                if (alert.id !== action.id) {
                    acc.push(alert);
                }

                return acc;
            }, new List()));
        default:
            return state;
    }
}

export default alertsContainerReducer;
export {initialState};
