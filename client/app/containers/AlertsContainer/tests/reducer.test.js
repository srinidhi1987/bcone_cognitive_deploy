import alertsContainerReducer, {initialState} from '../reducer';
import {REMOVE_ALERT_FROM_QUEUE} from '../constants';
import {fromJS} from 'immutable';

describe('alertsContainerReducer', () => {
    it('returns the initial state', () => {
        expect(alertsContainerReducer(undefined, {})).toEqual(initialState);
    });

    describe('REMOVE_ALERT_FROM_QUEUE action type', () => {
        it('should pass id to remove from queue', () => {
            // given
            const id = '54334-345345-345345-34553453454';
            const currentState = fromJS({
                ...initialState.toJS(),
                alerts: [
                    {
                        id,
                        message: 'Testing',
                        level: 'DANGER',
                    },
                ],
            });
            const action = {
                type: REMOVE_ALERT_FROM_QUEUE,
                id,
            };
            const expectedState = {
                ...initialState.toJS(),
                alerts: [],
            };
            // when
            const result = alertsContainerReducer(currentState, action);
            // then
            expect(result.toJS()).toEqual(expectedState);
        });
    });
});
