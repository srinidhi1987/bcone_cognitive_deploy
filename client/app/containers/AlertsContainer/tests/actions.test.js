import {
    displayAlert,
    removeAlertFromQueue,
} from '../actions';
import {
    DISPLAY_ALERT,
    ALERT_LEVEL_URGENT,
    REMOVE_ALERT_FROM_QUEUE,
} from '../constants';

describe('AlertsContainer actions', () => {
    describe('displayAlert Action', () => {
        it('should return expected action', () => {
            // given
            const message = 'Alerting you!';
            const level = ALERT_LEVEL_URGENT;
            const expectedAction = {
                type: DISPLAY_ALERT,
                message,
                level,
            };
            // when
            const result = displayAlert(message, level);
            // then
            expect(result).toEqual(expectedAction);
        });
    });

    describe('removeAlertFromQueue action creator', () => {
        it('should pass id to remove from alerts queue', () => {
            // given
            const id = '567766-456654-456645-456456545645';
            const expectedAction = {
                type: REMOVE_ALERT_FROM_QUEUE,
                id,
            };
            // when
            const result = removeAlertFromQueue(id);
            // then
            expect(result).toEqual(expectedAction);
        });
    });
});
