/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, {PureComponent} from 'react';

import './styles.scss';

export default class NotFound extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    static displayName = 'NotFound';

    render() {
        return (
            <div className="aa-not-found-page-container">
                <h1 className="aa-not-found-title">
                    This page was either not found or is coming soon.
                </h1>
            </div>
        );
    }
}
