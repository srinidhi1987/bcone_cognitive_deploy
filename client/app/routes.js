// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import {getAsyncInjectors} from 'utils/asyncInjectors'; //eslint-disable-line import/no-unresolved
import iqBotConfig from 'iqBotConfig'; // eslint-disable-line import/no-unresolved
const {APP_STORAGE, CONTROL_ROOM_URL} = iqBotConfig;
import {isAllowedAccess, isLicensed} from './utils/roleHelpers';
import {IQBOT_USER_ROLES, IQBOT_LICENSES} from './constants/enums';
import {HOME_PAGE} from './constants/stateNames';
import routeHelper from './utils/routeHelper';

const errorLoading = (err) => {
    console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
    cb(null, componentModule.default);
};

export default function createRoutes(store) {
    // Create reusable async injectors using getAsyncInjectors factory
    const {injectReducer, injectSagas} = getAsyncInjectors(store); // eslint-disable-line no-unused-vars
    const paths = {
        rootPath: routeHelper('/'),
        registrationRequired: routeHelper('registration-required'),
        licenseRequired: routeHelper('license-required'),
        accessDenied: routeHelper('access-denied'),
    };

    function requireAuth(...allowedRoles) {
        return function(nextState, replace) {
            if (!CONTROL_ROOM_URL) {
                // IQ Bot has not been registered with Control Room
                if (nextState.location.pathname !== paths.registrationRequired) {
                    replace({pathname: paths.registrationRequired});
                }
                return;
            }

            if (allowedRoles.length) {
                try {
                    // Test if User roles have been loaded yet
                    // If not, then we are still bootstrapping
                    const {licenseFeatures, roles} = JSON.parse(localStorage.getItem(APP_STORAGE.user));
                    if (!licenseFeatures || !roles) {
                        if (nextState.location.pathname !== paths.rootPath) {
                            replace({pathname: paths.rootPath});
                        }
                        return;
                    }
                    if (!isLicensed([IQBOT_LICENSES.CREATOR, IQBOT_LICENSES.RUNNER], licenseFeatures)) {
                        // User does not have a valid IQBot license
                        if (nextState.location.pathname !== paths.licenseRequired) {
                            replace({pathname: paths.licenseRequired});
                        }
                        return;
                    }

                    if (!isAllowedAccess(allowedRoles, roles)) {
                        // User does not have permissions to access this route
                        if (nextState.location.pathname !== paths.accessDenied) {
                            replace({pathname: paths.accessDenied});
                        }
                    }
                }
                catch (err) {
                    if (nextState.location.pathname !== paths.rootPath) {
                        replace({pathname: paths.rootPath});
                    }
                }
            }
        };
    }


    return [
        {
            path: paths.accessDenied,
            name: 'Access Denied',
            getComponent(nextState, cb) {
                // User does not have permission for the desired resource
                System.import('containers/AccessDenied')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: paths.licenseRequired,
            name: 'License Required',
            getComponent(nextState, cb) {
                // User does not have a valid IQ Bot license
                System.import('containers/LicenseRequired')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: paths.registrationRequired,
            name: 'Registration Required',
            getComponent(nextState, cb) {
                // IQ Bot has not been registered with CR
                System.import('containers/RegistrationRequired')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: routeHelper('login'),
            name: 'login',
            component() {
                window.location.replace(`${CONTROL_ROOM_URL}?redirect=${paths.rootPath}`);
                return null;
            },
        }, {
            path: paths.rootPath,
            name: 'home',
            onEnter: requireAuth(IQBOT_USER_ROLES.LOGGED_IN),
            getComponent(nextState, cb) {
                const importModules = Promise.all([
                    System.import('containers/App/reducer'),
                    System.import('containers/App/sagas'),
                    System.import('containers/AlertsContainer/reducer'),
                    System.import('containers/NavigationContainer/sagas'),
                    System.import('containers/AlertsContainer/sagas'),
                    System.import('containers/LanguageProvider/sagas'),
                    System.import('containers/HomePage/reducer'),
                    System.import('containers/HomePage'),
                ]);

                const renderRoute = loadModule(cb);

                importModules.then(([
                    appReducer,
                    appSagas,
                    alertsReducer,
                    navigationSagas,
                    alertsSagas,
                    languageSagas,
                    reducer,
                    component,
                ]) => {
                    injectReducer('appContainer', appReducer.default);
                    injectReducer('alertsContainer', alertsReducer.default);
                    injectSagas(appSagas.default);
                    injectSagas(navigationSagas.default);
                    injectSagas(alertsSagas.default);
                    injectSagas(languageSagas.default);
                    injectReducer(HOME_PAGE, reducer.default);
                    renderRoute(component);
                });

                importModules.catch(errorLoading);
            },
            indexRoute: {},
            childRoutes: [
                {
                    path: 'dashboard',
                    name: 'dashboard',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/DashboardContainer/reducer'),
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/DashboardContainer/sagas'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/DashboardContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            projectReducer,
                            sagas,
                            projectSagas,
                            component,
                        ]) => {
                            injectReducer('dashboardContainer', reducer.default);
                            injectReducer('projectsContainer', projectReducer.default);
                            injectSagas(sagas.default);
                            injectSagas(projectSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances',
                    name: 'projects',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/EditProjectContainer/sagas'),
                            System.import('containers/ProjectsContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            sagas,
                            editSagas,
                            component,
                        ]) => {
                            injectReducer('projectsContainer', reducer.default);
                            injectSagas(sagas.default);
                            injectSagas(editSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/new',
                    name: 'newProject',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/NewProjectContainer/reducer'),
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/NewProjectContainer/sagas'),
                            System.import('containers/NewProjectContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            projectsReducer,
                            sagas,
                            component,
                        ]) => {
                            injectReducer('newProjectContainer', reducer.default);
                            injectReducer('projectsContainer', projectsReducer.default);
                            injectSagas(sagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/validations',
                    name: 'projectValidationContainer',
                    onEnter: requireAuth(IQBOT_USER_ROLES.VALIDATOR),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/ProjectValidationContainer/reducer'),
                            System.import('containers/ProjectValidationContainer/sagas'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/ProjectValidationContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            sagas,
                            projectsSagas,
                            component,
                        ]) => {
                            injectReducer('projectValidationContainer', reducer.default);
                            injectSagas(sagas.default);
                            injectSagas(projectsSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/:id',
                    name: 'viewProject',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/ViewProjectContainer/reducer'),
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/AnalyzingProjectDocsContainer/reducer'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/EditProjectContainer/sagas'),
                            System.import('containers/AnalyzingProjectDocsContainer/sagas'),
                            System.import('containers/ViewProjectContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            projectsReducer,
                            projectClassifyProgressReducer,
                            projectSagas,
                            editProjectSaga,
                            projectClassifyProgressSagas,
                            component,
                        ]) => {
                            injectReducer('viewProjectContainer', reducer.default);
                            injectReducer('projectsContainer', projectsReducer.default);
                            injectReducer('analyzingProjectDocsContainer', projectClassifyProgressReducer.default);
                            injectSagas(projectSagas.default);
                            injectSagas(editProjectSaga.default);
                            injectSagas(projectClassifyProgressSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/:id/bots/:botid/:mode',
                    name: 'designerContainer',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/DesignerContainer/reducer'),
                            System.import('containers/DesignerContainer/sagas'),
                            System.import('containers/DesignerContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            sagas,
                            component,
                        ]) => {
                            injectReducer('designerContainer', reducer.default);
                            injectSagas(sagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/:id/processing',
                    name: 'analyzingProjectFiles',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/AnalyzingProjectDocsContainer/reducer'),
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/AnalyzingProjectDocsContainer/sagas'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/AnalyzingProjectDocsContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            projectsReducer,
                            sagas,
                            projectsSagas,
                            component,
                        ]) => {
                            injectReducer('analyzingProjectDocsContainer', reducer.default);
                            injectReducer('projectsContainer', projectsReducer.default);
                            injectSagas(sagas.default);
                            injectSagas(projectsSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/:id/report',
                    name: 'projectReportContainer',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/ProjectReportContainer/reducer'),
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/ProjectReportContainer/sagas'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/ProjectReportContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            projectsReducer,
                            sagas,
                            projectsSagas,
                            component,
                        ]) => {
                            injectReducer('projectReportContainer', reducer.default);
                            injectReducer('projectsContainer', projectsReducer.default);
                            injectSagas(sagas.default);
                            injectSagas(projectsSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'learning-instances/:id/validator',
                    name: 'validatorContainer',
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/ValidatorContainer/reducer'),
                            System.import('containers/ValidatorContainer/sagas'),
                            System.import('containers/ValidatorContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            sagas,
                            component,
                        ]) => {
                            injectReducer('validatorContainer', reducer.default);
                            injectSagas(sagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'bots',
                    name: 'botsContainer',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/BotsContainer/reducer'),
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/BotsContainer/sagas'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/BotsContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            projectsReducer,
                            sagas,
                            projectsSagas,
                            component,
                        ]) => {
                            injectReducer('botsContainer', reducer.default);
                            injectReducer('projectsContainer', projectsReducer.default);
                            injectSagas(sagas.default);
                            injectSagas(projectsSagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'migration-utility',
                    name: 'migrationutility',
                    onEnter: requireAuth(IQBOT_USER_ROLES.SERVICES),
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/ProjectsContainer/reducer'),
                            System.import('containers/ProjectsContainer/sagas'),
                            System.import('containers/MigrationUtilityContainer/reducer'),
                            System.import('containers/MigrationUtilityContainer/sagas'),
                            System.import('containers/MigrationUtilityContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            projectsReducer,
                            projectsSagas,
                            utilityReducer,
                            utilitySagas,
                            component,
                        ]) => {
                            injectReducer('projectsContainer', projectsReducer.default);
                            injectSagas(projectsSagas.default);
                            injectReducer('migrationUtilityContainer', utilityReducer.default);
                            injectSagas(utilitySagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: 'domains',
                    name: 'domainContainer',
                    getComponent(nextState, cb) {
                        const importModules = Promise.all([
                            System.import('containers/DomainContainer/reducer'),
                            System.import('containers/DomainContainer/sagas'),
                            System.import('containers/DomainContainer'),
                        ]);

                        const renderRoute = loadModule(cb);

                        importModules.then(([
                            reducer,
                            sagas,
                            component,
                        ]) => {
                            injectReducer('domainContainer', reducer.default);
                            injectSagas(sagas.default);
                            renderRoute(component);
                        });

                        importModules.catch(errorLoading);
                    },
                }, {
                    path: '*',
                    name: 'notfound',
                    getComponent(nextState, cb) {
                        System.import('containers/NotFoundPage')
                            .then(loadModule(cb))
                            .catch(errorLoading);
                    },
                },
            ],
        },
    ];
}
