/**
* This script runs prior to commit and makes sure the latest yarn.lock file is always commited when there are changes to package.json
*/
const {exec, exit} = require('shelljs');

// Find what files are staged for commit
const gitDiffExecResult = exec('git diff --cached --name-only');
// If git commit fails rollback commit
if (gitDiffExecResult.code === 1) exit(1);

// check if package.json is staged for commit
if (gitDiffExecResult.stdout.search('package.json') > -1) {
    // if so run yarn install (which triggers changes to yarn.lock if there is anything to be changed)
    const yarnInstallExecResult = exec('yarn install --ignore-scripts');
    // if yarn install fails rollback commit
    if (yarnInstallExecResult.code === 1) exit(1);
    // stage the newly modified yarn.lock file for commit
    exec('git add yarn.lock');
}

if (gitDiffExecResult.stdout.search('yarn.lock') > -1) {
    // later we can have scripts here that commit new dependency license assets when lockfiles are modified
}

// exit with no error. Commit will take place successfully.
exit(0);
