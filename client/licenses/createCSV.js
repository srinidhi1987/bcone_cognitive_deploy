const json2csv = require('json2csv');
const fs = require('fs');

const file = fs.readFileSync(`${__dirname}/generated_assets/summary.json`);
const parsed = JSON.parse(file);
const keys = Object.keys(parsed);
const array = keys.map((key) => Object.assign({}, parsed[key], {package: key}));
const fields = ['package', 'licenses', 'repository', 'publisher', 'email', 'url', 'licenseFile', 'copyrightLines'];
const output = json2csv({data: array, fields});

fs.writeFile(`${__dirname}/generated_assets/file.csv`, output, () => {});
console.log(`License assets created in: ${__dirname}/generated_assets/`); // eslint-disable-line no-console
