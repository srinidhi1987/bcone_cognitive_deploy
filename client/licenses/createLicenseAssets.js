console.time('timer'); // eslint-disable-line no-console
console.log('\n *STARTING* \n'); // eslint-disable-line no-console
const fs = require('fs');
const es = require('event-stream');
const contents = fs.readFileSync(`${__dirname}/licenses.json`);
const jsonContent = JSON.parse(contents);
const jsonCopy = Object.assign({}, jsonContent);

const jsonContentKeys = es.readArray(Object.keys(jsonContent));

if (!fs.existsSync(`${__dirname}/generated_assets`)) {
    fs.mkdirSync(`${__dirname}/generated_assets`);
}

if (!fs.existsSync(`${__dirname}/generated_assets/found_license_files`)) {
    fs.mkdirSync(`${__dirname}/generated_assets/found_license_files`);
}

const finalWritableStream = fs.createWriteStream(`${__dirname}/delete.txt`);

finalWritableStream.on('finish', () => {
    fs.writeFileSync(`${__dirname}/generated_assets/summary.json`, JSON.stringify(jsonCopy, null, 4), 'utf-8');
    fs.unlinkSync(`${__dirname}/delete.txt`);
    fs.unlinkSync(`${__dirname}/licenses.json`);
    console.timeEnd('timer'); // eslint-disable-line no-console
});

jsonContentKeys
    .pipe(es.map((key, next) => {
        if (!jsonContent[key]['licenseFile']) {
            jsonCopy[key]['note'] = 'INVESTIGATE';
            return next(null, jsonContent[key]);
        }

        const writableStream = fs.createWriteStream(`${__dirname}/generated_assets/found_license_files/${key}.txt`);
        writableStream.on('finish', () => {
            next(null, jsonContent[key]);
        });
        writableStream.on('error', () => {});

        jsonCopy[key]['copyrightLines'] = '';
        fs.createReadStream(jsonContent[key]['licenseFile'])
        .pipe(es.split()) // split the input file into lines
        .pipe(es.map((line, cb) => {
            if (/^.*Copyright.*|.*GPL.*|.*LGPL.*|.*LGPL.*|.*Affero.*|.*AGPL.*|.*Sleepycat.*|.*GNU Documentation License.*|.*Creative Commons ShareAlike.*|.*Open Software License.*|.*Academic Free License.*|.*Mozilla Public License.*|.*MLP.*|.*CDDL.*|.*CPL.*|.*Eclipse Public License.*|.*Apache 1\.0*$/i.test(line)) {
                jsonCopy[key]['copyrightLines'] = jsonCopy[key]['copyrightLines'].concat(line);
            }
            cb(null, line);
        }))
        .pipe(writableStream);
    }))
    .pipe(es.stringify())
    .pipe(finalWritableStream);


console.log('\n *DONE* \n'); // eslint-disable-line no-console
