:: Help ::
:: MasterBuildScript.bat %databasehostname% %databaseport% %databaseusername% %databasepassword% %controlroomhost% %controlroomport% %outputdir% %mqpassword% %authorizationheader% %reposlug% %pullrequestid% %installationdirectory% %gatewayhostname%

@echo off
cd Scripts
set sqlserverhost=%1
set sqlserverport=%2
set dbusername=%3
set databasepassword=%4
set controlroomhost=%5
set controlroomport=%6
set outputdir=%7
set mqpassword=%8
set authtoken=%9
shift
shift
shift
shift
shift
shift
shift
shift
set reposlug=%2
set pullrequestid=%3
set installdir=%4
set gatewayhostname=%5
set testintegrationserverusername=%6
set testintegrationserverpassword=%7
set dbpasswordforautomation=%8
set privatekey=%9
SET rootDir=%~dp0
echo "Root Dir:" %rootDir%
echo "Reposlug:" %reposlug%
echo "pullrequestid:" %pullrequestid%
echo "installdir:" %installdir%
echo "gatewayhostname:" %gatewayhostname%
echo "testintegrationserverusername:" %testintegrationserverusername%
echo "testintegrationserverpassword:" %testintegrationserverpassword%
echo "dbusername:" %dbusername%
echo "databasepassword:" %databasepassword%
echo "mqpassword:" %mqpassword%
echo "outputdir:" %outputdir%
echo "privatekey:" %privatekey%
echo "testautomationsuite_home:" %testautomationsuite_home%
echo "dbpasswordforautomation:" %dbpasswordforautomation%
@rem call "%rootDir%test_automation_status_check.bat" %testautomationsuite_home%
@rem if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%stop_and_uninstall_services_remotely.bat" %gatewayhostname% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
call "%rootDir%remove_older_build_artifacts_remotely.bat" %gatewayhostname% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
:: delete the workspace folder
RMDIR "%rootDir%../../buildartifacts" /S /Q
cd %rootDir%
call "%rootDir%client_projects_buildscript.bat"
echo "installdir after client build run:" %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%java_projects_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
echo "installdir after java project build run:" %installdir%
call "%rootDir%machine_learning_buildscript.bat"
echo "installdir after ML build run:" %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%worker_buildscript.bat"
echo "installdir after Worker build run:" %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred ) 
call "%rootDir%configuration_ci.bat" %sqlserverhost% %dbusername% %sqlserverport% %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%setup_database_rabbitmq_and_configurations.bat" %sqlserverhost% %dbusername% %databasepassword% %sqlserverport% %mqpassword% %controlroomhost% %controlroomport% %outputdir% %gatewayhostname% %installdir% %dbpasswordforautomation%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call powershell "%rootDir%frontend_js_hostname_updatescript.ps1" %gatewayhostname%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call powershell "%rootDir%create_private_key_file.ps1" %privatekey%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%copy_build_artifacts.bat" %controlroomhost% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%install_and_start_services_remotely.bat" %gatewayhostname% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
cd %rootDir%
call powershell "%rootDir%update_testautomation_propertiesfile.ps1" %gatewayhostname% %dbusername% %dbpasswordforautomation% %outputdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call powershell "%rootDir%update_smokespec_health_url.ps1" %gatewayhostname%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )  
call powershell "%rootDir%update_testautomation_outputfoldername.ps1" %outputdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
::call "%rootDir%RunTestAutomationSuite.bat"	
::if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
::call "%rootDir%PostBuildActions.bat" %authtoken% %reposlug% %pullrequestid%
::call powershell "%rootDir%PostBuildActions.ps1" %authtoken% %reposlug% %pullrequestid%
::if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
if %ERRORLEVEL% == 0 ( goto :COMPLETED )
:ErrorOccurred
echo "Errors encountered during installing services. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog "
exit /b %errorlevel%
:COMPLETED
exit /b 0
