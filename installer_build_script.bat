@echo off
SET workspaceDir=%~dp0
echo %workspaceDir%
:: delete the workspace folder
RMDIR "%workspaceDir%../buildartifacts" /S /Q
RMDIR "%workspaceDir%installer/Setup/Resources" /S /Q
RMDIR "%workspaceDir%installer/Setup/InstallShield2015/setup-support-files" /S /Q
call "%workspaceDir%scripts\client_projects_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
echo %workspaceDir%
call "%workspaceDir%scripts\java_projects_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
echo %workspaceDir%
call "%workspaceDir%scripts\machine_learning_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
echo %workspaceDir%
call "%workspaceDir%scripts\worker_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
echo %workspaceDir%
call "%workspaceDir%scripts\configuration_installer.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

if %ERRORLEVEL% == 0 ( goto :COMPLETED )
:ErrorOccurred
echo "Errors encountered building and packaging artifacts. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog "
exit /b %errorlevel%
:COMPLETED
exit /b 0