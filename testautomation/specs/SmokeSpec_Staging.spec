Created on 8th September - 2017

IQ Bot Smoke Test Automation Suit
================================
Health Checkup
---------------
Tags: Critical
* Wait for Services to start
* Navitage to url "http://localhost:8100/health"
* Check health

Heartbeat Check up
-------------------
Tags: Critical
* Navitage to url "http://localhost:9996/heartbeat"
* Check heartbeat for "File Manager"
* Refresh the page
* Navitage to url "http://localhost:9997/heartbeat"
* Check heartbeat for "Alias"
* Refresh the page
* Navitage to url "http://localhost:9998/heartbeat"
* Check heartbeat for "VisionbotManager"
* Refresh the page
* Navitage to url "http://localhost:9999/heartbeat"
* Check heartbeat for "Project Instance"

