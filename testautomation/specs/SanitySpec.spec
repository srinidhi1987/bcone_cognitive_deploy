Created By Saeed Saiyed - 24th OCT-2017
IQ Bot Sanity Test Automation Suite
==================================
1. Login Verification with IQBotService Role - Valid & Invalid Scenarios
Verify that, user is allowed to login into IQBot platform with IQBotServices role
---------------------------------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with user name as "InvalidUser" and password as "12345678"
* Then error message "Invalid Username or Password." should be displayed
* Refresh the page
* When user login into cognitive console with user name as "Services" and password as "InvalidPassword"
* Then error message "Invalid Username or Password." should be displayed
* Refresh the page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Ensure user with service role get all "links" access

2. Create New Instance Page Verification
Smoke Checkpoints - Languages, Default Selected Fields, Domains
Sanity Checkpoints - File size, Max # of Allowed files, File Type, Duplicate Fields warning,
Additional Fields, Cancel, Duplicate Instance Name warning
Instance creation verification for the English language with Invoice Domain, using 3 documents and negative scenarios
---------------------------------------------------------------------------------------------------------------------
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Verify values of Domain DropDown
* Default value of Domain DropDown is Select Input
* Verify values of Primary language DropDown
* Default Value of Primary language is English
* Verify values displayed for each dropdown item
* Click on browse button of create new instance page and upload 151 files,  limit exceed warning message should be displayed
* Refresh the page
* Click on browse button of create new instance page and upload unsupported file, file type error message should be displayed
* Refresh the page
* Click on browse button of create new instance page and upload above the max file size , limit to under 5 MB per file message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "Invoice Number" as "Form" Field
* Error "No duplicate field names allowed" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "Quantity" as "Table" Field
* Error "No duplicate field names allowed" message should be displayed
* Refresh the page
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Click on Cancel Button to cancel the Instance Creation
* Learning Instance Page is displayed

Smoke Checkpoints - Total Uploaded Files, Progress Bar, Close & Run button
Instance creation verification for the English language with Invoice Domain, using 3 documents
----------------------------------------------------------------------------------------------
 Tags: Critical
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Enter all the details to create New Instance with uploading "CreateInstanceDoc" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Check total number of files in analyzing are same as uploaded during creating new instance
* Progress bar exist
* Check Instance Details in Analyze Page
* Close and run in background button exist

3. Analyzing Documents Page Verification
Sanity Checkpoints - On page refresh landing to details page
Finish & Close Button -- need to add
Verify Analyzing Documents Page
-------------------------------
* Refresh the page
* Instance details page exist

4. Classification
Document Classification Verification
------------------------------------
 Tags: Critical
* Wait for documents processing in Learning Instance Details Page
* Get group id for "trainingGroup"
* Get group id for "successGroup"
* Get group id for "fileCountGroup"

5. Learning Instance Details Page Verification [Staging]
Smoke Checkpoints - # of Training Files, Training Success, Training Unprocessed, Priority Column,
% Accuracy, # of Visionbots, Total Files
Sanity Checkpoints - Priority column, Sorting, General Details, Create Bot Link
Verify documents count in Learning Instance Detail page, When Instance is in Staging
------------------------------------------------------------------------------------
* On learning instance details page documents should be classified in groups
* Then classified documents count should be match with uploaded documents count
* Sorting Validation for the header - "# of Training Files"
* Sorting Validation for the header - "Training Success"
* Sorting Validation for the header - "Training Unprocessed"
* Sorting Validation for the header - "# of Production Files"
* Sorting Validation for the header - "Production Success"
* Sorting Validation for the header - "Production Unprocessed"
* Sorting Validation for the header - "Priority"

6. Edit Instance Verification
Smoke Checkpoints - Total Files, Close and Run Button
Verify edit Instance functionality by uploading 2 documents
-----------------------------------------------------------
* When user edit instance and upload files
* Then on save button click uploaded files should be classified

7. Close & Run In Background
Close & Run In Background Button functionality Verification
-----------------------------------------------------------
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page

8. Learning Instance Details Page Verification [updated count after edit]
Smoke Checkpoints - Total Files after edit, Classification spinner, Total Groups
Sanity Checkpoints - Unclassified files count - Need to add
In Learning Instance Detail page, verify staging documents counts after edit instance
-------------------------------------------------------------------------------------
* Refresh the page
* Ensure that after document classification total files and groups should be updated
* Refresh the page
* Check staging result and production result value when on "Staging"
* Check the value of General Information when on "Staging"
* Check the value of "Staging" result when on "Staging" for "Sanity"
* Check the value of "Production" result when on "Staging" for "Sanity"

9. Train Visionbot
Smoke Checkpoints - Training, Preview, IQTest, IQTest Run
Visionbot Training Verification (Train, Preview & IQTest Run)
-------------------------------------------------------------
 Tags: Critical
* When user click on CreateBot link visionbot designer should be launched "trainingGroup"
* Ensure after training data should be extracted in preview & in IQTest
* When user click on CreateBot link visionbot designer should be launched "successGroup"
* Train Group for successful processing

Try to create instance with same instance name
Verify Duplicate instance name validation
-----------------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button
* Duplicate error message will be displayed

10. Bot Listing Page Verification
Smoke Checkpoints - Search, Status
Sanity Checkpoints - Search with all options
Search with Instance Name and check the status of the bot
---------------------------------------------------------
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Bot should be in "training" status
* Move bot from staging to production
* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Bot should be in "ready" status
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Bot should be in "active" status
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from production to staging
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from production to staging

11. Bot Listing Page after run once verification
Smoke Checkpoints - Total Files Processed, Successfully processed, Sent to Validation, % Document Accuracy, % Field Accuracy
Sanity Checkpoints - Sent to Production warning, Success count, Multiple Run warning, Launch warning
Run trained bot and verify document processing using Run Once functionality
---------------------------------------------------------------------------
* When user go to bot listing page and validate bot run summary before bot run for group "trainingGroup" then all details should be available
* Refresh the page
* When user go to bot listing page and validate bot run summary before bot run for group "successGroup" then all details should be available
* Run once bot for "trainingGroup"
* Run once bot for "successGroup"
* Ensure bot details for "trainingGroup" is updated after  bot run
* Ensure bot details for "successGroup" is updated after  bot run

12. Learning Instance Listing Page Verification [After Bot Run]
Smoke Checkpoints - # of IQBots, # of Files, % Accuracy, % Files Trained, Enviroment, Languages, Search
Sanity Checkpoints - Sorting, Search with all options, Group table Summary
Learning Instances Listing page Verification after Bot run in staging
---------------------------------------------------------------------
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Validate all data in Learning Instance Listing Page after Bot run for "Sanity"
* Refresh the page
* Click on Learning Instance Link
* Sorting Validation for the header - "# of IQ bots"
* Sorting Validation for the header - "# of files"
* Sorting Validation for the header - "% accuracy"
* Sorting Validation for the header - "Training %"

13. Bot Listing Page Verification - After move bot to production
Smoke Checkpoints - Status, Total Files Processed
Sanity Checkpoints - Sorting
Bot Details Verification after Moving Bot from Staging to Production
--------------------------------------------------------------------
* Click on Bot Link
* Sorting Validation for the header - "Bot"
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Validate all data after group move from Staging to production "trainingGroup"
* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Validate all data after group move from Staging to production "successGroup"


14. Dashboard Page Verification
Sanity Checkpoints - Domain, Name % Description, Traning %, Staging data verification
Validate dashboard data summary when instance is in staging
------------------------------------------------------------------
* Click on Dashboard Link
* Refresh the page
* Validate values of learning instance when instance is in staging

15. Learning Instance Listing Page Verification- After move LI to production
Smoke Checkpoints - # of Files, % Files Trained, Enviroment, Launch Validator
Learning Instance Listing page varification after moving instance from Staging to Production
--------------------------------------------------------------------------------------------
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Refresh the page
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Validate Learning Instance Listing data after move instance from staging to production for "Sanity"

16. IQBot Lite Command Verification
Smoke Checkpoints - LI Listing, Success & Invalid Path, Output Folder, Spinner
IQBot Lite Command & Production File Upload Verification
--------------------------------------------------------
  Tags: Critical
* Click on Instance Name
* When instance is created then it should be listed in IQBot Lite command & Success and Invalid path should be listed in IQBot Lite command
* Then uploaded documents should be classified for selected Instance
* Ensure that uploaded production files should be classified for selected Instance

17. Learning Instance Detail Page Verification - After production upload
Smoke Checkpoints - # Of Production Files, Production Success, Production Unprocessed, % Accuracy, Enviroment, Total Files
Verify Learning Instance Details values after production upload
---------------------------------------------------------------
* Refresh the page
* Check staging result and production result value when on "Production"
* Check the value of General Information when on "Production"
* Check the value of "Staging" result when on "Production" for "Sanity"
* Check the value of "Production" result when on "Production" for "Sanity"

18. Bot Listing Page Verification - After production upload
Smoke Checkpoints - Search, Status
Sanity Checkpoint - Validation Data
Bot Listing Page Search, Status and Summary Verification
---------------------------------------------------------
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Group name should be same as the group name for which bot was created for that particular instance
* Bot should be in "active" status

* Select "All Fields" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Instance name should be listed on Bot page list
* Search result should include the Group name "trainingGroup"
* Refresh the page

* Click on Learning Instance Link
* Enter the instance name in search textbox
* Move instance from production to staging

* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from production to staging
* Refresh the page

* Select "Environment" from search DropDown in Bot Page
* Select Bots which are in "Staging"
* Instance name should be listed on Bot page list
* Search result should include the Group name "trainingGroup"
* Refresh the page

* Click on Learning Instance Link
* Enter the instance name in search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"

* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from staging to production
* Refresh the page

* Click on Bot Link
* Select "Environment" from search DropDown in Bot Page
* Select Bots which are in "Production"
* Instance name should be listed on Bot page list
* Search result should include the Group name "trainingGroup"

* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Instance name should be listed on Bot page list
* Group name should be same as the group name for which bot was created for that particular instance

* Click on Learning Instance Link
* Enter the instance name in search textbox
* Move instance from production to staging

* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from production to staging
* Refresh the page

* Select "Status" from search DropDown in Bot Page
* Select Bots which are in "training"
* Instance name should be listed on Bot page list
* Search result should include the Group name "trainingGroup"

* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from staging to production
* Refresh the page

* Select "Status" from search DropDown in Bot Page
* Select Bots which are in "ready"
* Instance name should be listed on Bot page list
* Search result should include the Group name "trainingGroup"

* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Click on Bot Link

* Select "Status" from search DropDown in Bot Page
* Select Bots which are in "active"
* Instance name should be listed on Bot page list
* Search result should include the Group name "trainingGroup"

* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* validate files after bot and instance both are in production

Reliability Check2
---------------------------
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user

19. Dashboard Page Verification
Smoke Checkpoints - Total Processed Files, STP, % Fields Accuracy
Total Processed Files and STP Verification on Dashboard
-------------------------------------------------------
* Click on Dashboard Link
* Get the total number of files processed on dashboard page
* Get all the file processed from each domain
* Check total number of files Processed should be same as sum of file processed in particular domain which are in production
* Get the STP value from Dashboard page
* Click on each instance which are in production and get sum of file successfully Processed and sum of files uploaded and calculate STP

20. Performance Report Page Verification
Smoke Checkpoints - Total Processed Files, STP, % Fields Accuracy, Processing Results
Total Processed Files, STP & Accuracy Verification for Performance Report
-------------------------------------------------------------------------
* Click on Dashboard Link
* Get all the instance name which are in production
* Click on instance name
* User will be landed to Performance Report Page
* Validate Processing result values for "Sanity"
* Click on Dashboard Link
* Click on each instance which are in production to validate STP Accuracy and Total files processed for version "5.2"

21. Output Folder Verification
Smoke Checkpoints - Instance Name Folder, Output CSV
Output folder creation Verification for Production instance
-----------------------------------------------------------
* Check output folder for created instance and validate unclassified files

22. Validator Verification
Validator Launch, Document count, Error count & Image load Verification for IQBotService user
---------------------------------------------------------------------------------------------
 Tags: Critical
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Launch Validator from Learning Instance Listing Page
* Validate total number of files, error count and image view in validator

23. Validation Page Verification
Sanity Checkpoints - Status Verification [Processing, Reviewed & Validating], Production Unprocessed, Total Files Processed,
Total Files Validated, Total Files Invalid, Action, verifying dropdown values
Verify Validation page for Status, Total Files Processsed and Total Files Validated
-----------------------------------------------------------------------------------
* Click on Learning Instance Link
* Enter the instance name in search textbox
* Upload multiple documents to production from IQBot lite command for "Validator"
* Click on Instance Name
* Wait for page Load
* Then uploaded documents should be classified for selected Instance
* Wait for production document processing
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page
* When user login into cognitive console with valid user name as "Validator" and valid password as "12345678"
* Verify values of search field DropDown when login with "validator" role
* Check status of Instance, it should be "validating"
* Validate files state before validating, where validated is "0" and invalid is "0"
* Launch validator and validate
* Close validator and refresh validation page.
* Refresh the page
* Enter the instance name in search textbox
* Validate files state after validating, where validated is "1" and invalid is "2"

24. Logout Verification - IQBotValidator
Smoke Checkpoints - Logged in username, Logout
Validator Logout Verification
-----------------------------
* When user click on profile button in home page
* Then logged in user name as "Validator" should be displayed
* When user click on logout button
* Ensure user should be logout and landed to login page

25. Machine Learning Suggestions verification
Validate Machine Learning Suggestions and document validations
--------------------------------------------------------------
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Enter the instance name in search textbox
* Upload multiple documents to production from IQBot lite command for "Machine Learning"
* Click on Instance Name
* Then uploaded documents should be classified for selected Instance
* Wait for production document processing
* Refresh the page
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Launch Validator from Learning Instance Listing Page
* Validate documents and verify suggestion for machine learning

Reliability Check3
---------------------------
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user

26. Learning Instance Details Page Verification
Smoke Checkpoints - Delete Instance
Delete Instance Verification
-------------------------------
* Click on Learning Instance Link
* Get the total number of instance created
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* On Learning Instance page click on instance name
* Instance details page exist
* By clicking on Edit button Delete Instance button should be displayed
* By clicking on deleteInstance button content modal should appear with enabled Cancel button and disabled I understand,please delete button
* Enter the instance name on the Textbox which you want to delete
* Click on I understand, please delete
* If no more instance created before "No current learning instances." message will be displayed else search the instance name on search textbox and validate the result
* Refresh the page

27. Invoice domain with Additional fields
Sanity Checkpoints : Additional fields addition verification for Invoice domain
Validate Additional Form Fields & Table Fields for Domain-Invoices
------------------------------------------------------------------
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Update instance name
* Enter all the details to create new instance with uploading "Sample" file and domain "Invoices" primary language "English" and add form field "Name" and table field "Serial Number"
* Then click on create instance and analyze button,uploaded documents should be classified
* Check Instance Details in Analyze Page
* Ensure uploaded documents should be classified
* Instance details page exist
* After classification click on Create Bot Link
* Ensure the added form field and table field for Domain "Invoices" and Language "English" are displayed in designing window

28. Purchase order domain with Additional fields addition verification
Sanity Checkpoints : Additional fields addition verification for Purchase Domain domain
Validate Additional Form Fields & Table Fields for Domain- Purchase Order
-------------------------------------------------------------------------
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Update instance name
* Enter all the details to create new instance with uploading "CreateInstanceDoc" file and domain "Purchase Orders" primary language "English" and add form field "Name" and table field "Serial Number"
* Then click on create instance and analyze button,uploaded documents should be classified
* Check Instance Details in Analyze Page
* Ensure uploaded documents should be classified
* Instance details page exist
* After classification click on Create Bot Link
* Ensure the added form field and table field for Domain "Purchase Orders" and Language "English" are displayed in designing window

29. Other domain with Additional fields addition verification
Sanity Checkpoints : Additional fields addition verification for Other domain
Validate Additional Form Fields & Table Fields for Domain- Other
-----------------------------------------------------------------
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Update instance name
* Enter all the details to create new instance with uploading "CreateInstanceDoc" file and domain "Other" primary language "English" and add form field "Name" and table field "Serial Number"
* Then click on create instance and analyze button,uploaded documents should be classified
* Check Instance Details in Analyze Page
* Ensure uploaded documents should be classified
* Instance details page exist
* After classification click on Create Bot Link
* Ensure the added form field and table field for Domain "Other" and Language "English" are displayed in designing window

30. Invoice domain with German language Additional fields addition verification
Sanity Checkpoints : Multi language instance verification
Validate Multi-language Instance Creation - German
--------------------------------------------------
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Update instance name
* Enter all the details to create new instance with uploading "CreateDoc" file and domain "Invoices" primary language "German" and add form field "Name" and table field "Serial Number"
* Then click on create instance and analyze button,uploaded documents should be classified
* Check Instance Details in Analyze Page
* Ensure uploaded documents should be classified
* Instance details page exist
* After classification click on Create Bot Link
* Ensure the added form field and table field for Domain "Invoices" and Language "German" are displayed in designing window

31. Logout Verification - IQBotServices
Smoke Checkpoints - Logged in username, Logout
Logout Verification
-------------------
* When user click on profile button in home page
//* Then logged in user name as "Services" should be displayed
* When user click on logout button
* Ensure user should be logout and landed to login page
* Quit browser