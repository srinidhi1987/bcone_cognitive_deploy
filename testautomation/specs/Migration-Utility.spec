Migration Utility Page Specification
=================================
1.Migration Utility Export features with Services login
-------------------------------------------------------
* Open cognitive console with Migration-Utility should be redirected to Login-URL
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Open cognitive console with Migration-Utility URL after Login
* User is verifying browser title as "Automation Anywhere | Migration Utility"
* Breadcrumb check for Migration Utility page
* Page label check for Migration Utility page above instance table
* User should be on same page and same url after click on Migration Utility page lebel
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Enter all the details to create New Instance with uploading "3" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Open cognitive console with Migration-Utility URL after Login
* Select checkbox for current automation running instances
* Click on Export button to open pop-up
* Type your file name to export "Latest_Driver_Instance_Export"
* Click on I understand button to export or import
* Verify the toastr notification showing "Document classification for one or more Learning Instances is currently in progress. Please retry this action after some time."
* Click on Learning Instance Link
* Total Learning instances count should match migration page with LI page
* Click on Bot Link
* Total Bots count should match migration page with Bot page
* Select checkbox for "All" instances
* Click on Export button to open pop-up
* Click on Cancel button on export or import pop-up
* Refresh the page
* Select checkbox for "Test1111" instances
* Click on Export button to open pop-up
* Type your file name to export "1st_Export"
* Click on I understand button to export or import
* User is verifying text present in export pop-up after click on export button
* Verify the toastr notification showing "Export process initiated"
* Refresh the page for migration page
* Select checkbox for "First LI" instances
* Select checkbox for "Test1111" instances
* Select checkbox for "All" instances
* Click on Export button to open pop-up
* Type your file name to export " Export_filename  "
* Verify the error message on import or export pop-up "Formatting must not have spaces at the beginning or end."
* Refresh the page for migration page
* Select checkbox for "IQ Bot" instances
* Select checkbox for "First LI" instances
* Select checkbox for "All" instances
* Click on Export button to open pop-up
* Type your file name to export "   "
* Verify the error message on import or export pop-up "Required"
* Refresh the page for migration page
* Select checkbox for "Second Li" instances
* Select checkbox for "First LI" instances
* Select checkbox for "All" instances
* Click on Export button to open pop-up
* Type your file name to export "Export_filename"
* Click on I understand button to export or import
* User is verifying text present in export pop-up after click on export button
* Verify the toastr notification showing "Export process initiated"
* When user click on profile button in home page
* Then logged in user name as "services" should be displayed
* When user click on logout button
* Ensure user should be logout and landed to login page

2. Migration Utility with validator login
-----------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Validator" and valid password as "12345678"
* Open cognitive console with Migration-Utility should be redirected to Login-URL

3. Migration utility Import features with Services login
--------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Open cognitive console with Migration-Utility URL after Login
* Click on Import button to open pop-up
* Click on I understand button to export or import
* Verify the error message on import or export pop-up "select archive name for import."
* Click on Cancel button on export or import pop-up
* Refresh the page
* Click on Import button to open pop-up
* Check check-box for Export file to Import
* Click on I understand button to export or import
* Verify the toastr notification showing "Import process initiated"
* Refresh the page
* Click on Learning Instance Link
* Total Learning instances count should match migration page with LI page
* Click on Bot Link
* Total Bots count should match migration page with Bot page
* When user click on profile button in home page
* Then logged in user name as "services" should be displayed
* When user click on logout button
* Ensure user should be logout and landed to login page

4. Migration Utility Import-Export features for fresh(instance or .iqba file are not available) installed
---------------------------------------------------------------------------------------------------------
* Open cognitive console with Migration-Utility should be redirected to Login-URL
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Open cognitive console with Migration-Utility URL after Login
* User is verifying browser title as "Automation Anywhere | Migration Utility"
* Breadcrumb check for Migration Utility page
* Page label check for Migration Utility page above instance table
* User should be on same page and same url after click on Migration Utility page lebel
* Validate text when no instance available in UI
* Refresh the page
* Click on Import button to open pop-up
* Click on I understand button to export or import
* Verify the error message on import or export pop-up "select archive name for import."
* Click on Cancel button on export or import pop-up
* Quit browser