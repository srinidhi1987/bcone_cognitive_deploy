Learning Instance Listing Specification
=======================================
Verify value of search field drop down in learning instance page
----------------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Verify values of search field DropDown when login with "services" role

Validate Search Functionality on learning instance page
-------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Learning Instance Link
* Select "All Fields" from search DropDown in LearningInstance Page
* Instance name should be listed on list
* Refresh the page
* Select "Environment" from search DropDown in LearningInstance Page
* Select Bots which are in "Staging"
* Instance name should be listed on list
* Refresh the page
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Instance name should be listed on list
* Move instance from staging to production
* Click on button "Yes, send to production"
* Refresh the page
* Select "Environment" from search DropDown in LearningInstance Page
* Select Bots which are in "Production"
* Instance name should be listed on list