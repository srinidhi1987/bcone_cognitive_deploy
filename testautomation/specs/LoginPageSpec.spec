Login Page Specification
=================================
1.Log In with IQBotService Role
Verify that, user is allowed to login into IQBot platform with IQBotServices role
---------------------------------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Ensure user with service role get all "tabs" access
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page

2.Log in with Invalid Username
Verify invalid username error message
------------------------------------
* When user login into cognitive console with user name as "invaliduser" and password as "12345678"
* Then error message "Invalid Username or Password." should be displayed
* Quit browser