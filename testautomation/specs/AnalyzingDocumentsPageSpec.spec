Analyzing Documents Specification
=================================
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button

Total Files,progressbar and Close and Run Button exist
------------------------------------------------------
* Enter all the details to create New Instance with uploading "CreateInstanceDoc" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Check total number of files in analyzing are same as uploaded during creating new instance
* Progress bar exist
* Check Instance Details in Analyze Page
* Close and run in background button exist
* Click on Close and Run In Background button
* Instance details page exist

Validate on page refresh user should move to LI detail page
-----------------------------------------------------------
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Refresh the page
* Instance details page exist

Validate Finish and Close Button Exist
--------------------------------------
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure Finish and close button exist