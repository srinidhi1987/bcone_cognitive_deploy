Verify Bot Listing Page
=======================
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user


Verify values of Search field DropDown
--------------------------------------
* Click on Bot Link
* Verify values of search field DropDown

Search with Instance Name and check the status of the bot
---------------------------------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* After classification click on Create Bot Link
* Launch and Close Designer

//* Close the Designer window
* Instance name on learning instance detail page is same as the instance name entered while creating instance
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Group name should be same as the group name for which bot was created for that particular instance
* Bot should be in "training" status
* Move bot from staging to production
* Bot should be in "ready" status
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Bot should be in "active" status

Validate Search Functionality
------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist

* After classification click on Create Bot Link
* Launch and Close Designer

//* Close the Designer window
* Instance name on learning instance detail page is same as the instance name entered while creating instance
* Click on Bot Link
* User Landed to Bot Page

* Select "All Fields" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Instance name should be listed on Bot page list
* Search result should include the Group name "Group_22"
* Refresh the page

* Select "Environment" from search DropDown in Bot Page
* Select Bots which are in "Staging"
* Instance name should be listed on Bot page list
* Search result should include the Group name "Group_22"
* Refresh the page
* Enter the instance name in search textbox
* Move bot from staging to production
* Refresh the page

* Select "Environment" from search DropDown in Bot Page
* Select Bots which are in "Production"
* Instance name should be listed on Bot page list
* Search result should include the Group name "Group_22"

* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Instance name should be listed on Bot page list
* Group name should be same as the group name for which bot was created for that particular instance

* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from production to staging
* Refresh the page

* Select "Status" from search DropDown in Bot Page
* Select Bots which are in "training"
* Instance name should be listed on Bot page list
* Search result should include the Group name "Group_22"

* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from staging to production
* Refresh the page

* Select "Status" from search DropDown in Bot Page
* Select Bots which are in "ready"
* Instance name should be listed on Bot page list
* Search result should include the Group name "Group_22"

* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Click on Bot Link

* Select "Status" from search DropDown in Bot Page
* Select Bots which are in "active"
* Instance name should be listed on Bot page list
* Search result should include the Group name "Group_22"

Validate warning message on bot run
------------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist

* After classification click on Create Bot Link
* Launch and Close Designer

* Ensure after training data should be extracted in preview & in IQTest for all pass fields
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Run the bot once
* Run the bot once
* Error message should be displayed "Staging document for this visionbot is already in running state."

Validate warning message on set bot to production
-------------------------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist

* After classification click on Create Bot Link
* Launch and Close Designer

* Ensure after training data should be extracted in preview & in IQTest for all pass fields
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Run the bot once and move bot from staging to production
* Error message should be displayed "Visionbot is in running state."

Validate Success count of documents
------------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist

* After classification click on Create Bot Link
//* Launch and Close Designer

* Ensure after training data should be extracted in preview & in IQTest for all pass fields
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Run the bot once
* Wait for bot run process to be completed
* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Count of file successfully processed should be "1"
* Move bot from staging to production
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
//* Upload documents from IQBot Lite command
//* Click on Instance Name
//* Wait for page Load
//* Then uploaded documents should be classified for selected Instance
//* Wait for production document processing
//* Refresh the page
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Count of file successfully processed should be "1"
//-------------------------------------------------------------

Validate warning message on launch designer
--------------------------------------------
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist

* After classification click on Create Bot Link
* Launch and Close Designer

* Ensure after training data should be extracted in preview & in IQTest for all pass fields
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Run the bot once
* Click on edit button of bot page
* Launch and Close Designer
//* Close the Designer window
7. Validate warning message for bot run in progress