Specification Heading
=====================
Created by vandana.fatwani on 1/22/2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
When user is deleted it should not be able to login
---------------------------------------------------
//* Create user with user name 'NewUser' and password '12345678'
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "NewUser" and valid password as "12345678"
* Then dashboard should be displayed for service user
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page
* Delete user "NewUser"
* When user login into cognitive console with valid user name as "NewUser" and valid password as "12345678"
* Then error message "Invalid Username or Password." should be displayed

     
Validate Login when Authentication service is stopped
------------------------------------------------------
* Open cognitive console logIn page
* Stop "Authentication" service
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Ensure user should not be able to login
* Start "Authentication" service
* Refresh the page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user

When SQL Server Service is Stopped
-----------------------------------
* Open cognitive console logIn page
* Stop "SQLServer" service
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Ensure user should not be able to login
* Start "SQLServer" service
* Refresh the page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user

When Gateway is Stopped
-----------------------------------
* Open cognitive console logIn page
* Stop "Gateway" service
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Ensure user should not be able to login
* Start "Gateway" service
* Refresh the page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user

Dashboard Page when Project Service is Stopped
-------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* Click on Dashboard Link
* Stop "Project" service
* Refresh the page
* Previously created Instance should not be displayed and user will be asked to "Create an Instance"
* Start "Project" service
* Click on Learning Instance Link
* Click on Dashboard Link
* Refresh the page
* Previously created Instance should be displayed

Dashboard Page when Project Service is stopped , user should not be able to create instance
--------------------------------------------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Stop "Project" service
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button
* Ensure user should not be allowed to Create Instance and error message "" should be displayed
* Start "Project" service
* Refresh the page
* Enter all the details to create New Instance with uploading "1" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist


If File Service is stopped
--------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Stop "File" service
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button
* Ensure user should not be allowed to Create Instance and error message "Error creating project" should be displayed
* Start "File" service
* Refresh the page
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist