Delete Instance Specification
=============================
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Get the total number of instance created
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* After classification click on Create Bot Link
* Launch and Close Designer
* Check Edit button exist
* By clicking on Edit button Delete Instance button should be displayed
* By clicking on deleteInstance button content modal should appear with enabled Cancel button and disabled I understand,please delete button
* Enter the instance name on the Textbox which you want to delete

Delete Instance
----------------
* Click on I understand, please delete
* If no more instance created before "No current learning instances." message will be displayed else search the instance name on search textbox and validate the result

Cancel Delete Instance
-----------------------
* By Clicking on Cancel button to cancel the delete instance user will be on same learning instance detail page