Performance Report Page Specification
======================================
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"

Check the STP Accuracy and Total files processed of Performance Report Page
---------------------------------------------------------------------------
* Click on each instance which are in production to validate STP Accuracy and Total files processed for version "5.2"

Check Performance Report Processing Result Values
-------------------------------------------------
* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Move instance to production on Learning instance detail page
* Click on button "Yes, send to production"
* Click on Dashboard Link
* Get all the instance name which are in production
* Click on instance name
* User will be landed to Performance Report Page
* Validate Processing result values for "Performance Report Spec"