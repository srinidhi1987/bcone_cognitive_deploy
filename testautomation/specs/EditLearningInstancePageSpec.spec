Add fields
=====================

Login
-----
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user

User is adding some additional fields
-------------------------------------
//* Open cognitive console with a learning instance page URL
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Enter all the details to create New Instance with uploading "AddFieldsDocs" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Check total number of files in analyzing are same as uploaded during creating new instance
* Progress bar exist
* Check Instance Details in Analyze Page
* Close and run in background button exist


Verify Analyzing Documents Page
-------------------------------
* Refresh the page
* Instance details page exist

4. Classification
Document Classification Verification
------------------------------------
 Tags: Critical
* Wait for documents processing in Learning Instance Details Page
* Get group id for "trainingGroup"
* Get group id for "successGroup"

Verify documents count in Learning Instance Detail page, When Instance is in Staging
------------------------------------------------------------------------------------
* On learning instance details page documents should be classified in groups
* Then classified documents count should be match with uploaded documents count
* Sorting Validation for the header - "# of Training Files"
* Sorting Validation for the header - "Training Success"
* Sorting Validation for the header - "Training Unprocessed"
* Sorting Validation for the header - "# of Production Files"
* Sorting Validation for the header - "Production Success"
* Sorting Validation for the header - "Production Unprocessed"
* Sorting Validation for the header - "Priority"


Visionbot Training Verification (Train, Preview & IQTest Run)
-------------------------------------------------------------
 Tags: Critical
* When user click on CreateBot link visionbot designer should be launched "trainingGroup"
* Ensure after training data should be extracted in preview & in IQTest
* When user click on CreateBot link visionbot designer should be launched "successGroup"
* Train Group for successful processing


Run trained bot and verify document processing using Run Once functionality
---------------------------------------------------------------------------
* When user go to bot listing page and validate bot run summary before bot run for group "trainingGroup" then all details should be available
* Refresh the page
* When user go to bot listing page and validate bot run summary before bot run for group "successGroup" then all details should be available
* Run once bot for "trainingGroup"
* Run once bot for "successGroup"
* Ensure bot details for "trainingGroup" is updated after  bot run
* Ensure bot details for "successGroup" is updated after  bot run


Learning Instances Listing page Verification after Bot run in staging
---------------------------------------------------------------------
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Validate all data in Learning Instance Listing Page after Bot run for "Sanity"
* Refresh the page
* Click on Learning Instance Link
* Sorting Validation for the header - "# of IQ bots"
* Sorting Validation for the header - "# of files"
* Sorting Validation for the header - "% accuracy"
* Sorting Validation for the header - "Training %"

Bot Details Verification after Moving Bot from Staging to Production
--------------------------------------------------------------------
* Click on Bot Link
* Sorting Validation for the header - "Bot"
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Validate all data after group move from Staging to production "trainingGroup"
* Refresh the page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Validate all data after group move from Staging to production "successGroup"


Edit Learning story verification
---------------------------------

* Click on Dashboard Link
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Click on Instance Name
* Click on Edit button on Learning Instance Details page
* To add a standard form/tc field "Item Tax" on edit learning instance
* To add a standard form/tc field "Unit Price" on edit learning instance
* Click on Save button to update that Learning Instance
* Click on OK button on pop-up to update that Learning Instance
* verify the message "Learning instance is updated successfully" on UI after learning instance modification
* Click on Edit button on Learning Instance Details page
* To add a custom standard "Item Version" field "table" on edit learning instance
* To add a custom standard "Shop Number" field "table" on edit learning instance
* Click on Save button to update that Learning Instance
* Click on OK button on pop-up to update that Learning Instance
* verify the message "Learning instance is updated successfully" on UI after learning instance modification
* Click on Edit button on Learning Instance Details page
* When user edit instance and upload files for add fields story
* To add a standard form/tc field "Account Name" on edit learning instance
* To add a standard form/tc field "Account Number" on edit learning instance
* Click on Save button to update that Learning Instance
* Click on OK button on pop-up to update that Learning Instance
* verify the message "Learning instance is updated successfully" on UI after learning instance modification
* Click on Close and Run In Background button
* Wait for spinner on Learning Instance page
* Click on Edit button on Learning Instance Details page
* To add a custom standard "Customer Reference Name" field "form" on edit learning instance
* To add a custom standard "Customer Reference No" field "form" on edit learning instance
* Click on Save button to update that Learning Instance
* Click on OK button on pop-up to update that Learning Instance
* verify the message "Learning instance is updated successfully" on UI after learning instance modification
* Get group id for Addfields story "addFieldsGroup"
* When user click on CreateBot link visionbot designer should be launched "addFieldsGroup" for addFields story
* Ensure after training data should be extracted in preview & in IQTest for addFields story
* Click on Dashboard Link
* Click on Bot Link
* Run once bot for "addFieldsGroup" for add fields story
* Click on Dashboard Link
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from staging to production
* Refresh the page
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* When instance is created then it should be listed in IQBot Lite command & Success and Invalid path should be listed in IQBot Lite command for add fields
* Click on Bot Link
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Click on Instance Name
* Refresh the page
* Wait for spinner on Learning Instance page
* Wait for production document processing
* Refresh the page

Output folder creation Verification for Production instance
-----------------------------------------------------------
* Check output folder for created instance and validate unclassified files add fields