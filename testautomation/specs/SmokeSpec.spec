Created on 8th September - 2017

IQ Bot Smoke Test Automation Suit
================================
Health Checkup
---------------
Tags: Critical
* Wait for Services to start
* Navitage to url "http://localhost:8100/health"
* Check health

Heartbeat Check up
-------------------
Tags: Critical
* Navitage to url "http://localhost:9996/heartbeat"
* Check heartbeat for "File Manager"
* Refresh the page
* Navitage to url "http://localhost:9997/heartbeat"
* Check heartbeat for "Alias"
* Refresh the page
* Navitage to url "http://localhost:9998/heartbeat"
* Check heartbeat for "VisionbotManager"
* Refresh the page
* Navitage to url "http://localhost:9999/heartbeat"
* Check heartbeat for "Project Instance"

1.Log In with IQBotService Role
Verify that, user is allowed to login into IQBot platform with IQBotServices role
---------------------------------------------------------------------------------
Tags: Critical
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Ensure user with service role get all "links" access

2.Create New Instance & 3.Analyzing Documents
Instance creation verification for the English language with Invoice Domain, using 3 documents
----------------------------------------------------------------------------------------------
Tags: Critical
* Click on Learning Instance Link
* Learning Instance Page is displayed
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Enter all the details to create New Instance with uploading "CreateInstanceDoc" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Check total number of files in analyzing are same as uploaded during creating new instance
* Progress bar exist
* Check Instance Details in Analyze Page
* Close and run in background button exist

3. Classification
Document Classification Verification
------------------------------------
Tags: Critical
* Ensure uploaded documents should be classified
* Get group id for "trainingGroup"
* Get group id for "successGroup"
* Get group id for "fileCountGroup"

4.Learning Instance Detail Page [Staging file count]
Verify documents count in Learning Instance Detail page, When Instance is in Staging
------------------------------------------------------------------------------------
* On learning instance details page documents should be classified in groups
* Then classified documents count should be match with uploaded documents count

5.Edit Instance [Analyzing Document]
Verify edit Instance functionality by uploading 2 documents
-----------------------------------------------------------
* When user edit instance and upload files
* Then on save button click uploaded files should be classified

6.Close & Run In Background
Close & Run In Background Button functionality Verification
-----------------------------------------------------------
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page

7.Learning Instance Detail Page [updated count]
In Learning Instance Detail page, verify staging documents counts after edit instance
-------------------------------------------------------------------------------------
* Refresh the page
* Ensure that after document classification total files and groups should be updated
* Check staging result and production result value when on "Staging"
* Check the value of General Information when on "Staging"
* Check the value of "Staging" result when on "Staging" for "Smoke"
* Check the value of "Production" result when on "Staging" for "Smoke"

8.Train Visionbot
Visionbot Training Verification (Train, Preview & IQTest Run)
-------------------------------------------------------------
 Tags: Critical
* When user click on CreateBot link visionbot designer should be launched "trainingGroup"
* Ensure after training data should be extracted in preview & in IQTest

9.Bot Listing Page [Status Check]
Search with Instance Name and check the status of the bot
---------------------------------------------------------
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Group name should be same as the group name for which bot was created for that particular instance
* Bot should be in "training" status
* Move bot from staging to production
* Bot should be in "ready" status
* Refresh the page
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Bot should be in "active" status
* Refresh the page
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from production to staging
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Move bot from production to staging

10.Run once Bot [Group staging document processing]
Run trained bot and verify document processing using Run Once functionality
---------------------------------------------------------------------------
* When user go to bot listing page and validate bot run summary then all details should be available
* And when user click on bot run, details should be updated after bot processing

11.Learning Instance Listing page [Updated Count]
Learning Instances Listing page Verification after Bot run in staging
---------------------------------------------------------------------
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Validate all data in Learning Instance Listing Page after Bot run for "Smoke"

12.Bot Listing Page [Move bot to production]
Bot Details Verification after Moving Bot from Staging to Production
--------------------------------------------------------------------
* Validate all data after group move from Staging to production

13.Learning Instance Listing Page [Move LI to pro]
Learning Instance Listing page varification after moving instance from Staging to Production
--------------------------------------------------------------------------------------------
* When user click on profile button in home page
//* Then logged in user name as "Services" should be displayed
* When user click on logout button
* Ensure user should be logout and landed to login page
* Open cognitive console logIn page with service role user between executing scripts
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Refresh the page
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Validate Learning Instance Listing data after move instance from staging to production for "Smoke"

14.IQBot Lite command & Learning Instance Details page [Production values]
IQBot Lite Command & Production File Upload Verification
--------------------------------------------------------
Tags: Critical
* Click on Instance Name
* When instance is created then it should be listed in IQBot Lite command & Success and Invalid path should be listed in IQBot Lite command
* Then uploaded documents should be classified for selected Instance
* Ensure that uploaded production files should be classified for selected Instance
* Refresh the page
* Check staging result and production result value when on "Production"
* Check the value of General Information when on "Production"
* Check the value of "Staging" result when on "Production" for "Smoke"
* Check the value of "Production" result when on "Production" for "Smoke"

17. Output Folder Verification
Output folder creation Verification for Production instance
-----------------------------------------------------------
* Check output folder for created instance and validate unclassified files

18. Validator Verification
Validator Launch, Document count, Error count & Image load Verification
-----------------------------------------------------------------------
Tags : Critical
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Launch Validator from Learning Instance Listing Page
* Validate total number of files, error count and image view in validator

19. Delete Instance
Delete Instance Verification
-----------------------------
* When user click on profile button in home page
* When user click on logout button
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Get the total number of instance created
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* On Learning Instance page click on instance name
* Instance details page exist
* By clicking on Edit button Delete Instance button should be displayed
* By clicking on deleteInstance button content modal should appear with enabled Cancel button and disabled I understand,please delete button
* Enter the instance name on the Textbox which you want to delete
* Click on I understand, please delete
* If no more instance created before "No current learning instances." message will be displayed else search the instance name on search textbox and validate the result

20. Logout
Logout Verification
-------------------
* When user click on profile button in home page
//* Then logged in user name as "Services" should be displayed
* When user click on logout button
* Ensure user should be logout and landed to login page

21.Log In with IQBotValidator Role
Verify that, user is allowed to login to IQBot platform with IQBotValidator role
--------------------------------------------------------------------------------
* When user login into cognitive console with valid user name as "validator" and valid password as "12345678"
* Then user with Validator role get only Learning Instance link access
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page
* Quit browser