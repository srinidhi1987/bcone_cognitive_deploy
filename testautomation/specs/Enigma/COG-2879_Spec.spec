COG-2879 - Designer application integrated to the IQ Bot platform
=================================================================
Created by Rajesh.Kunapareddy on 10/27/2017

User login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user
This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
Pre-Requisite(creating instance for test)
-----------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Get the total number of files uploaded
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link


Learning Instance breadcrumb is available in Instance listing page
------------------------------------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Ensure learning instances breadcrumb is available

Learning Instance name breadcrumb is available in Instance details page
-----------------------------------------------------------------------
* Enter the instance name in LearningInstance Search textbox
* Click on Instance Name
* Ensure learning instances breadcrumb is available
* Ensure learning instance name breadcrumb is available

Train bot breadcrumb is available in Train bot page
---------------------------------------------------
* When user click on CreateBot link trainbot should get opened
* Ensure learning instances breadcrumb is available
* Ensure learning instance name breadcrumb is available
* Ensure train bot breadcrumb is available

Bots breadcrumb is available in Bots listing page
-------------------------------------------------
* Click on Bot Link
* User Landed to Bot Page
* Ensure Bots breadcrumb is available

User navigates to Train bot from Bots page sees breadcrumbs
-----------------------------------------------------------
* Enter the instance name in search textbox
* Click on edit button of bot page
* Ensure learning instances breadcrumb is available
* Ensure learning instance name breadcrumb is available
* Ensure train bot breadcrumb is available

User clicks on Learning Instance name breadcrumb in Train bot page lands in Instance details page
-------------------------------------------------------------------------------------------------
* When user click on Learing Instance breadcrumb lands in Learning Instance details page

User clicks on Learning Instances breadcrumb in Instance details page lands in Instance listing page
----------------------------------------------------------------------------------------------------
* When user click on Learing Instances breadcrumb lands in Learning Instance listing page

User gets navigated to Train bot from choosing to edit/create from right widget of Learning instance lisiting page
------------------------------------------------------------------------------------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Enter the instance name in LearningInstance Search textbox
* Click on Instance Name column
* Click on Edit Bot in right widget
* Ensure learning instances breadcrumb is available
* Ensure learning instance name breadcrumb is available
* Ensure train bot breadcrumb is available
* Quit browser