COG-2881 - Pre-populate the fields from the learning instance configuration
===========================================================================
Created by Rajesh.Kunapareddy on 17-11-2017

User Login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user

Verify Invoice domain default fields
------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Click on New Instance Button
* User Should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-2881" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure all default fields of "Invoices Default" were available in train bot

Verfiy Other domain fields
--------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Click on New Instance Button
* User Should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-2881" in Domain of "Other" and Language as "English"
* add form field "Payment Term" and table field "Product Description"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure all default fields of "Other" were available in train bot

Verify Invoice domain with additional fields
--------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Click on New Instance Button
* User Should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-2881" in Domain of "Invoices" and Language as "English"
* add form field "Payment Term" and table field "Product Description"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure all default fields of "Invoices Additional" were available in train bot

Verify option of field details were available in Trainbot
---------------------------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Enter the instance name in LearningInstance Search textbox
* Click on Instance Name
* User clicks on first group CreateBot link
* User clicks on Field Options dropdown
* Ensure Field "Type" section is available
* Verify values of Type DropDown
* Ensure Field "Label" section is available
Ensure Field Label section has a input area to enter label
* Ensure Field "Label" section has a picker to select region from document
* Ensure Field "Value" section is available
Ensure Field Value section has a input area to enter label
* Ensure Field "Value" section has a picker to select region from document
* Ensure Field "Options" section is available
Ensure Field "Formula" section is available

Verify user lands in Instance details page after finishing training
-------------------------------------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Enter the instance name in LearningInstance Search textbox
* Click on Instance Name
* User clicks on first group CreateBot link
* User clicks on Finish button in Trainbot page lands on Instance details page
* Quit browser