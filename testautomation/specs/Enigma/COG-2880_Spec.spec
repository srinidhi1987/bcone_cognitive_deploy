COG-2880 - Load one file from the Learning Instance group
=========================================================
Created by Rajesh.Kunapareddy on 22-11-2017

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
User login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user

Test PNG type docuement is loaded into cognitive web designer
-------------------------------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "0_PNG.png" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test JPG type docuement is loaded into cognitive web designer
-------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "0_JPG" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test JPEG type docuement is loaded into cognitive web designer
--------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "0_JPEG" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test TIF type docuement is loaded into cognitive web designer
-------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "0_TIF" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test TIFF type docuement is loaded into cognitive web designer
--------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "0_TIFF" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test 90° right rotated document is loaded into cognitive web designer properly
------------------------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "90°+" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test 90° left rotated document is loaded into cognitive web designer properly
-----------------------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "90°-" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test 180° right/left rotated document is loaded into cognitive web designer properly
------------------------------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "180°" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

Test 0° rotated document is loaded into cognitive web designer properly
-----------------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "0°" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated
* Quit browser