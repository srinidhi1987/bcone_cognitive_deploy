COOG-3127-Map detected fields in the document
=============================================
Created by Rajesh.Kunapareddy on 28-03-2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
User login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user

Pre-Requisite(creating instance for test)
-----------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "1_Invoice.tiff" with test data "COG-3127" in Domain of "Invoices" and Language as "English"
* Then "Uncheck" all the checked fields
* add form field "Rebmun Eciovni" and table field "Ytitnauq"
* add form field "Etad Eciovni" and table field "Noitpircsed Meti"
* add form field "Latot Eciovni" and table field "Latot Meti"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link

//todo: Manual test
COG-3966-User see's blue boxes(SIR) around every word in the document

COG-3967-Selected SIR gets assigned as label for selected field
---------------------------------------------------------------
* Select field "Rebmun Eciovni"
* Select SIR "11"
* Then label box should get a value of "Inv. Number"

COG-3968-User is able to map value for a field with out label mapped
--------------------------------------------------------------------
* Select field "Etad Eciovni"
* Select value box
* Select SIR "14"
* Then value box should get a value of "05/16/16"

COG-3969-User is able to draw region for value
----------------------------------------------
* Select field "Latot Eciovni"
* Select "Value" picker
* draw from "109", "84" to "143", "61"
* Then value box should get a value of " ST JOHN'S MEDICAL CENTER  1900 S WHEELING ST  PO#  72007000008903  GLN 1100004089991  TULSA OK 74104 "

Duplicate of COG-3969
COG-3970-User is able to draw region for value by selecting a field's value picker

COG-3971-Ensure with picker next to label/value user is able to draw a region
-----------------------------------------------------------------------------
* Select label picker
* draw from "113", "66" to "75", "20"
* Then value box should get a value of " SHIP TO #: "