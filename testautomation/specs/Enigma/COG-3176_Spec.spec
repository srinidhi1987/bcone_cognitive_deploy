COG-3176 - Configure fields
===========================
Created by Rajesh.Kunapareddy on 02-01-2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
User login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user

Ensure field type dropdown has all field types
----------------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-2881" in Domain of "Invoices" and Language as "English"
* add form field "Payment Term" and table field "Product Description"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* User clicks on Field Options dropdown
* Ensure Field "Type" section is available
* Verify values of Type DropDown

Ensure field types for invoice domain were coming from LI
---------------------------------------------------------
* Ensure domain "Invoices" fields has correct field types

Ensure default value region for required/optional fields is disabled/enabled
----------------------------------------------------------------------------
* Ensure for required field default value is disabled
* Ensure for optional field default value is enabled

Ensure on selecting Validate Pattern all patterns shows up
----------------------------------------------------------
* Ensure on selecting Validate Pattern all patterns shows up

Ensure field types for Purchase Orders domain were coming from LI
-----------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "PO_1.pdf" with test data "COG-3176" in Domain of "Purchase Orders" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* User clicks on Field Options dropdown
* Ensure domain "Purchase Orders" fields has correct field types

Ensure field types for others domain were coming from LI
--------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-3176" in Domain of "Other" and Language as "English"
* add form field "Payment Term" and table field "Product Description"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* User clicks on Field Options dropdown
* Ensure domain "Others" fields has correct field types

Ensure training is getting saved on front end
---------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-3176" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure changes made in webdesigner were saved automatically