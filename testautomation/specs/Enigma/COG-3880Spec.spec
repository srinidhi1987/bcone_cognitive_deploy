Training Specification
=====================

Create Instance and navigate to Training Page
---------------------------------------------
* Open "Enigma" console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Ensure user with service role get all "links" access
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "ZoomIn" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Ensure uploaded documents should be classified
* Instance details page exist
* After classification click on Create Bot Link
* Wait for page Load
* User Landed to "Training" Page
* Wait for page Load


User is able to see Zoom option in web designer (COG-4132)
-----------------------------------------------------------
* Ensure user is able to see "Zoom in" option in web designer
* Ensure user is able to see "Zoom out" option in web designer
* Ensure user is able to see "Fit to screen" option in web designer

If the document size is small user should not be able to scroll (COG-4317)
--------------------------------------------------------------------------
* Click on "Zoom out text"
* Click on "Zoom out text"
* Ensure document size is small and user should not be able to scroll

User is able to use Zoom In functionality properly (COG-4313)
------------------------------------------------------------
* Click on "Next" button
* Wait for page Load
* Click on "Zoom in icon"
* Ensure user is able to use "Zoom in icon" functionality properly
* Click on "Zoom in text"
* Ensure user is able to use "Zoom in text" functionality properly

User is able to use Zoom Out functionality properly (COG-4314)
----------------------------------------------------
* Click on "Fit to screen text"
* Click on "Zoom out icon"
* Ensure user is able to use "Zoom out icon" functionality properly
* Click on "Zoom out text"
* Ensure user is able to use "Zoom out text" functionality properly

Document is fitted to screen when opened for edit (COG-4315)
-------------------------------------------------------------
* Click on "Fit to screen text"
* Ensure document is fitted to screen
* Click on "Zoom out icon"
* Click on "Fit to screen icon"
* Ensure document is fitted to screen
* Click on "Zoom in icon"
* Click on "Fit to screen text"
* Ensure document is fitted to screen

User is able to scroll bar while document is larger than document layout (COG-4316)
------------------------------------------------------------------------------------
* Click on "Fit to screen text"
* Ensure user is able to use "Scroll Up and Down" functionality properly


