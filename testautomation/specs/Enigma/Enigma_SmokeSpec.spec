IQ Bot Smoke test Enigma - Web Designer
=======================================
Created by Rajesh.Kunapareddy on 22-01-2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.

Log In with IQBotService Role
Verify that, user is allowed to login into IQBot platform with IQBotServices role
---------------------------------------------------------------------------------
Tags: Critical
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Ensure user with service role get all "links" access

1 & 2. Verify document is loaded and rotated in web designer
Test 90° right rotated document is loaded into cognitive web designer properly
------------------------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "90°+" with test data "COG-2880" in Domain of "Invoices" and Language as "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure that document is rotated

3. Verify field types were available
Verify Invoice domain with additional fields
--------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* Click on New Instance Button
* User Should be landed to "Create New Learning Instance" page
* Enter all the details to create New Instance with uploading "1" file and Domain "Invoices" and Primary language "English"
* add form field "Payment Term" and table field "Product Description"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Ensure all default fields of "Invoices Additional" were available in train bot

4. Verify field options were available
Verify option of field details were available in Trainbot
---------------------------------------------------------
* Ensure Field "Type" section is available
* Verify values of Type DropDown
* Ensure Field "Label" section is available
Ensure Field Label section has a input area to enter label
* Ensure Field "Label" section has a picker to select region from document
* Ensure Field "Value" section is available
Ensure Field Value section has a input area to enter label
* Ensure Field "Value" section has a picker to select region from document
* Ensure Field "Options" section is available

5. Verify table options were available
Verify option of table details were available in Trainbot
---------------------------------------------------------
TODO: add test to verify value region for table

6. Verify training is saved
Ensure training is getting saved
--------------------------------
* Ensure changes made in webdesigner were saved automatically

7. Verify on clicking finish user navigates to Instance details page
Verify user lands in Instance details page after finishing training
-------------------------------------------------------------------
* User clicks on Finish button in Trainbot page lands on Instance details page
* Quit browser