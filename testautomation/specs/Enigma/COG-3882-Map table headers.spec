COG-3882-Map table headers
==========================
Created by Rajesh.Kunapareddy on 09-04-2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
User login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user
This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.

Pre-Requisite(creating instance for test)
-----------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "1_Invoice.tiff" with test data "COG-3127" in Domain of "Invoices" and Language as "English"
* Then "Uncheck" all the checked fields
* add form field "Rebmun Eciovni" and table field "Ytitnauq"
* add form field "Etad Eciovni" and table field "Noitpircsed Meti"
* add form field "Latot Eciovni" and table field "Latot Meti"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link

COG-4318-User is able to see list of table fields which were selected & added while creating instance
-----------------------------------------------------------------------------------------------------
* Ensure all default fields of "Custom Fields" were available in train bot

COG-4319-User selectes a table field see all details of field in fields details widget
--------------------------------------------------------------------------------------
* Select field "Noitpircsed Meti"
* User clicks on Field Options dropdown
* Ensure Field "Type" section is available
* Verify values of Type DropDown
* Ensure Field "Label" section is available
* Ensure Field Label section has a input area to enter label
* Ensure Field "Label" section has a picker to select region from document
* Ensure for table field value section is disabled
* Ensure Field "Options" section is available

COG-4320-User selects to map next field, all mapping/training happened with this will get saved
-----------------------------------------------------------------------------------------------
* Select field "Ytitnauq"
* Select SIR "11"
* Then label box should get a value of "Inv. Number"
* Select field type as "date"
* Select field "Noitpircsed Meti"
* Select SIR "12"
* Then label box should get a value of "Type"
* Select field "Ytitnauq"
* Then label box should get a value of "Inv. Number"

// Covered in COG-4320 test
COG-4321-User choose a SIR as table header, the value of that SIR become lable for table header

// Cannot be automated
COG-4328-When user draw UDR all the SIR which are in this region will be displayed as label name

// Cannot be automated
COG-4329-When select blank space/non SIR label as an SIR label, label name should be blank

// Cannot be automated
COG-4330-only selected SIR label should be highlighted, select the arrow to draw to any other SIR the current SIR should be highlighted

COG-4327-User should be able to edit the existing table label name mapping
--------------------------------------------------------------------------
* Select field "Ytitnauq"
* Select label box
* Enter label "New label"
* Then label box should get a value of "New label"

COG-4326-When user create instance with domian -other and adding 3 custom table fields there should be no value section
-----------------------------------------------------------------------------------------------------------------------
* Update instance name
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "1.tiff" with test data "COG-2881" in Domain of "Other" and Language as "English"
* add form field "Payment Term" and table field "Product Description"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link
* Select field "Product Description"
* User clicks on Field Options dropdown
* Ensure for table field value section is disabled