COG-4267-Highlight mapped fields
================================
Created by Rajesh.Kunapareddy on 11-04-2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
User login
----------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Then dashboard should be displayed for service user

Pre-Requisite(creating instance for test)
-----------------------------------------
* When user click on Learning Instance Tab
* Then on learning instance tab selection user should be landed to "My Learning Instances" page
* When user click on New Instance button
* Then user should be landed to "Create new learning instance" page
* Create Instance with File "COG-4267-Highlight mapped fields.tiff" with test data "COG-4267" in Domain of "Invoices" and Language as "English"
* Then "Uncheck" all the checked fields
* add form field "Page" and table field "TAX"
* add form field "Inv Number" and table field "Ytitnauq"
* add form field "Rebmun Eciovni" and table field "SHIPPED QUANTITY"
* Then click on create instance and analyze button,uploaded documents should be classified
* And user landed into "Analyzing documents..." page
* Ensure uploaded documents should be classified
* User clicks on first group CreateBot link

COG-4867-User see's field mapping indicator turned green if field label not available check box is checked while value is mapped
--------------------------------------------------------------------------------------------------------------------------------
// When
* Select field "Rebmun Eciovni"
* Check, field label not available check box
* Select value box
* Select SIR "8"
// Then
* Then value box should get a value of "Inv. Number"
// And
* Ensure field training indicator shows field trained

COG-4868-User see's field mapping is still green if user edits/removes label of a field
---------------------------------------------------------------------------------------
// When
* Select field "Inv Number"
* Enter label "Test Label"
// Then
* Then value box should get a value of "Test Label"
// When
* Ensure field training indicator shows field trained
* Clear label
// Then
* Then value box should get a value of ""

COG-4870-User see's field label not available check box is disabled for table field
-----------------------------------------------------------------------------------
// When
* Select field "Ytitnauq"
// Then
* Ensure field label not available check box is not available for table field

COG-4865-User see's a field mapping indicator turned green if automapping was done for field and value
------------------------------------------------------------------------------------------------------
// When
* Select field "Inv Number"
// Then
* Ensure field training indicator shows field trained

COG-4866-User see's a field mapping indicator turned green if automapping was done for table field
--------------------------------------------------------------------------------------------------
// When
* Select field "SHIPPED QUANTITY"
// Then
* Ensure field training indicator shows field trained

COG-4869-User see's field which were having auto mapped for the field and value were having green indicator while designer completes loading
--------------------------------------------------------------------------------------------------------------------------------------------
// When
* Select field "Inv Number"
// Then
* Ensure field training indicator shows field trained


// Cannot be automated
COG-4864-User see's a field mapping indicator in fields listing widget for every field
COG-5019-Ensure that while Field not available check box is checked, user is not able to edit/draw the label