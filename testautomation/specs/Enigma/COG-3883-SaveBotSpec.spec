Specification Heading
=====================
Created by vandana.fatwani on 4/23/2018

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.

Create Instance and navigate to Training Page
---------------------------------------------
// Given
* Open "Enigma" console logIn page
* When user login into cognitive console with valid user name as "Vandana" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Ensure user with service role get all "links" access
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create new instance with uploading "SaveBot" file and domain "Invoices" primary language "English" and add form field "Order Number" and table field "Tax"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Ensure uploaded documents should be classified
* Instance details page exist
* Get all the group names created
* After classification click on Create Bot Link
* Wait for page Load

// When
* User Landed to "Training" Page
* Wait for page Load

After create instance when user go to training page(fileds are partially automapped) user see "Save and close" button is disabled.  
-----------------------------------------------------------------
// Then
* All the required fields are "not mapped"
* User see  "Save and close" is "disabled"

After create instance when user go to training page (map all the required fields) user see "Save and close" button is enabled.
----------------------------------------------------------------------------------------------------------------
// Given
* Click "Order Number" from "Form" field
// When
* Select SIR "22"
* User can see "label" textbox extracted with text "Customer PO:"
* Wait for "720070000007076" to be extracted
* User can see "value" textbox extracted with text "720070000007076"
// Then
* User see  "Save and close" is "enabled"

A Modal popup with options "Cancel" "Save" "Save and send" will appear when click on "Next" button (Save and close is disabled if partial fields are are mapped)
-------------------------------------------------------------------------------------------------------------------------
// Given
* Click on Instance name breadcrum
* Wait for page Load
* Get all the group names created
* Click on Create Bot Link with highest priority
* Wait for page Load
* User Landed to "Training" Page
* Wait for page Load
* All the required fields are "not mapped"

// When
* Click on "Next" button

// Then
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Where button "Save and send to production" is "disabled"

A Modal popup with options "Cancel" "Save" "Save and Send" will appear when click on "Next" (Save and close is enabled if all the required fileds are mapped)
-------------------------------------------------------------------------------------------------------------------------
// When
* Click on "Cancel" button

// Then
* User Landed to "Training" Page

// Given
* Wait for page Load
* Click "Order Number" from "Form" field
* Select SIR "22"
* User can see "label" textbox extracted with text "Customer PO:"
* Wait for "720070000007076" to be extracted
* User can see "value" textbox extracted with text "720070000007076"
* All the required fields are "mapped"
* User see  "Save and close" is "enabled"

// When
* Click on "Next" button

// Then
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Where button "Save and send to production" is "enabled"

When click on “Next” user will be on same training page when selects "Cancel" from modal popup.
-----------------------------------------------------------------------------------------------
// When
* Click on "Cancel" button

// Then
* User Landed to "Training" Page
* User can see "label" textbox extracted with text "Customer PO:"
* User can see "value" textbox extracted with text "720070000007076"

When click on “Next” user will be on next group of staging when selects "Save" from modal popup.
------------------------------------------------------------------------------------------------
// Given
* Click on Instance name breadcrum
* Get all the group names created
* Click on Create Bot Link with highest priority
* Wait for page Load
* User Landed to "Training" Page
* Click "Invoice Number" from "Form" field
* User can see "label" textbox extracted with text "Inv. Number"
* User can see "value" textbox extracted with text "Type"
* Select "value" textbox extracted with text "Type"
* Select SIR "12"
* Wait for "11402402" to be extracted
* User can see "value" textbox extracted with text "11402402"

// When
* Click on "Next" button
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Click on "Save" button
* Wait for page Load

// Then
* User landed to "Second" higher priority staging group

Once the value for the staging bot are saved, user should be able to see the saved data when redirecting to same bot.
--------------------------------------------------------------------------------------------------------------------
// Given
* Click on Instance name breadcrum
* Get all the group names created

// When
* Click on Create Bot Link with highest priority
* Wait for page Load

// Then
* User Landed to "Training" Page
* Click "Invoice Number" from "Form" field
* User can see "label" textbox extracted with text "Inv. Number"
* User can see "value" textbox extracted with text "11402402"

// When all the required fileds are mapped, user click on “Next” and select "Save and send" from modal popup, bot will be moved to "production" and user will be landed to next highest priority staging group.
// ---------------------------------------------------------------------------------------------------------
// Given
* Click "Order Number" from "Form" field
* Select SIR "22"
* User can see "label" textbox extracted with text "Customer PO:"
* User can see "value" textbox extracted with text "720070000007076"

// When
* Click on "Next" button
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Click on "Save and send to production" button
* Wait for page Load

// Then
* User landed to "Second" higher priority staging group

// User should be able to see bot on "production" on bot listing page when moved to “Production” from Training page.
// ----------------------------------------------------------------------------------------------------------------
// Given
* Click on Instance name breadcrum
* Get all the group names created
* Click on Bot Link
* User Landed to Bot Page

// When
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Select "Environment" from search DropDown in Bot Page
* Enter "Production" in search textbox

// Then
* Instance name should be listed on Bot page list
* User can see "First" group is on "Production"

// User should be able to see all saved information on training page when moved back  from "Production" to "Satging" from Bot page.
//  -------------------------------------------------------------------------------------------------------------------------
// Given
* Move "First" group from "Production" to staging
* Enter "Staging" in search textbox
* Select "All Fields" from search DropDown in Bot Page
* Enter "First" group name on search textbox

// When
* Click on "edit" icon of "First" group

// Then
* User Landed to "Training" Page
* Wait for page Load
* User landed to "First" higher priority staging group

* Click "Invoice Number" from "Form" field
* User can see "label" textbox extracted with text "Inv. Number"
* User can see "value" textbox extracted with text "11402402"

* Click "Order Number" from "Form" field
* User can see "label" textbox extracted with text "Customer PO:"
* User can see "value" textbox extracted with text "720070000007076"

// A modal pop up with "Cancel" "Save" "Save and send" will appear where "save and send" will be disabled when click on "Finish Later" button
// ----------------------------------------------------------------------------------------------------------------
* Wait for page Load
// When
* Click on "Finish Later" button

// Then
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Where button "Save and send to production" is "disabled"

// When user select "Save" from modal popup after click on "Finish Later " button all the information will be saved and user will be landed to learning instance detail page
// -------------------------------------------------------------------------------------------------------------------
// Given
* Click on "Cancel" button
* Wait for page Load
* Click "Item Total" from "Table" field
* Select SIR "70"
* User can see "label" textbox extracted with text "EXTENSION"

// When
* Click on "Finish Later" button
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Where button "Save and send to production" is "disabled"
* Click on "Save" button
* Wait for page Load

// Then
* User landed to learning instance detail page

// A modal popup with options “Cancel” “Save” “Save and send” will appear when user click on “Save and close” button.
// ---------------------------------------------------------------------------------------------------------
// Given
* Wait for page Load
* Get all the group names created
* Click on Create Bot Link with highest priority
* Wait for page Load
* User Landed to "Training" Page
* Click "Invoice Number" from "Form" field
* User can see "label" textbox extracted with text "Inv. Number"
* User can see "value" textbox extracted with text "11402402"
* Click "Order Number" from "Form" field
* User can see "label" textbox extracted with text "Customer PO:"
* User can see "value" textbox extracted with text "720070000007076"
* Click "Item Total" from "Table" field
* User can see "label" textbox extracted with text "EXTENSION"
* All the required fields are "mapped"

// When
* User see  "Save and close" is "enabled"
* Click on "Save and close" button
* Modal popup is displayed

// Then
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"

// Given
* Click on "Cancel" button
* Wait for page Load
* Click on "Next" button
* Modal popup is displayed
* Click on "Save" button
* Wait for page Load
* User landed to "Second" higher priority staging group

// When user click on "Save and close" and select “Save” button from modal popup, all the information will be saved, and user will be landed to learning instance detail page.
// --------------------------------------------------------------------------------------
* Click "Quantity" from "Table" field
* Select SIR "57"
* User can see "label" textbox extracted with text "Rela"
* Rename "Label" textbox value "Rela" to "Qty"
* Wait for page Load
* Click "Invoice Date" from "Form" field
* Click on "down arrow" "field" options
* Click on radio button of "Continue"
* Click "Tax" from "Table" field
* Click on radio button of "Continue"
* All the required fields are "mapped"
* User see  "Save and close" is "enabled"

// When
* Click on "Save and close" button
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Click on "Save" button

// Then
* User landed to learning instance detail page

// asserting the saved value
* Get all the group names created
* Click on Create Bot Link with highest priority
* Wait for page Load
* User Landed to "Training" Page
* Wait for page Load
* Click on "Next" button
* Modal popup is displayed
* Click on "Save" button
* Wait for page Load
* User landed to "Second" higher priority staging group
//* Wait for page Load
* Click "Quantity" from "Table" field
* User can see "label" textbox extracted with text "Qty"
* Click "Invoice Date" from "Form" field
* Click on "down arrow" "field" options
* Radio button "Continue" should be selected
* Click "Tax" from "Table" field
* Radio button "Continue" should be selected
* Click on "Next" button
* Modal popup is displayed
* Click on "Save" button
* Wait for page Load
* User landed to "Third" higher priority staging group
//* Wait for page Load

// Given
* Click "Order Number" from "Form" field
* Select SIR "45"
* User can see "label" textbox extracted with text "Purchase Order:"
* Wait for "72007000008964" to be extracted
* User can see "value" textbox extracted with text "72007000008964"
* Click "Item Description" from "Table" field
* Click on "down arrow" "field" options
* Click on radio button of "Continue"
* Click "Tax" from "Table" field
* Click on radio button of "Continue"
* Click "Table Settings" from "settings" field
* Select "Item_Total" from "Primary Column" Drop Down
* All the required fields are "mapped"
* User see  "Save and close" is "enabled"

// When user click on “Save and close” and select "Save and Send" from modal popup , all the information will be saved, bot will be send to to production and user will be navigated to learning instance detail page.
// ----------------------------------------------------------------------------------------------------------------
// When
* Click on "Save and close" button
* Modal popup is displayed
* User can see Modal popup with options "Cancel" "Save" "Save and send to production"
* Click on "Save and send to production" button

// Then
* User landed to learning instance detail page

// When click on “edit bot” of bot listing page, user can retrieve all the saved information which were saved from training page.
// ----------------------------------------------------------------------------
// Given
* Get all the group names created
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Select "Environment" from search DropDown in Bot Page
* Enter "Production" in search textbox
* Instance name should be listed on Bot page list
* User can see "Third" group is on "Production"
* Move "Third" group from "Production" to staging
* Refresh the page
* Enter the instance name in search textbox
//* Enter "Staging" in search textbox
* Enter "Third" group name on search textbox

// When
* Click on "edit" icon of "Third" group
* User Landed to "Training" Page
* Wait for page Load

// Then
* User landed to "Third" higher priority staging group
* Click "Order Number" from "Form" field
* User can see "label" textbox extracted with text "Purchase Order:"
* User can see "value" textbox extracted with text "72007000008964"
* Click "Item Description" from "Table" field
* User can see "label" textbox extracted with text ""
* Click "Tax" from "Table" field
* User can see "label" textbox extracted with text ""
* Click "Table Settings" from "settings" field
* User can see "Item_Total" is default selected value of "Primary Column" dropdown