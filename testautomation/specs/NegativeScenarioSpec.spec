Created on 8th December - 2017

IQ Bot  - Negative Scenarios
============================

Login Page Negative Scenarios
Login with invalid credentials
------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with user name as "InvalidUser" and password as "12345678"
* Then error message "Invalid Username or Password." should be displayed
* Refresh the page
* When user login into cognitive console with user name as "Services" and password as "InvalidPassword"
* Then error message "Invalid Username or Password." should be displayed

Try to enter username and password with invalid license error message should be displayed.
------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
* Refresh the page
* When user login into cognitive console with valid user name as "NoIQBot" and valid password as "12345678"
* Then error message "You do not have valid IQ Bot License. Please contact Administrator." should be displayed

Login with valid credentials and click on back browser user should be landed to login page
------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
* Refresh the page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Click on browser "back" button
* User should be redirected to login page

Login with valid credentials and click on back browser user should be landed to login page and click to navigate forward user should be on same login page
------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
* Refresh the page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Then dashboard should be displayed for service user
* Click on browser "back" button
* User should be redirected to login page
* Click on browser "forward" button
* User should be redirected to login page

Login with validator credentials and try to navigate to dashboard page user should be landed to login page
-----------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
* Refresh the page
* When user login into cognitive console with valid user name as "validator" and valid password as "12345678"
* Try to navigate to "Dashbard" page
* User should be redirected to login page
* When user login into cognitive console with valid user name as "validator" and valid password as "12345678"
* Try to navigate to "bots" page
* User should be redirected to login page
* When user login into cognitive console with valid user name as "validator" and valid password as "12345678"
* Try to navigate to "report" page
* User should be redirected to login page

Login with Service role and try to navigate to validations page user should be landed to login page
----------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
* Refresh the page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Click on Learning Instance Link
* Try to navigate to "validations" page
* User should be redirected to login page


DashBoard Page Negative Scenario's
Try to click instance which are on staging mode it should not redirect to any other page. [performance page]
------------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
* Refresh the page
* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Dashboard Link
* Click on instance which is on "staging"

When clicking on page reference "Dashboard" user should not be redirect to any other page.
------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
* Click on Dashboard Link

* Click on page reference "Dashboard"
* Ensure user is on "Dashboard" page
* Page title of "Dashboard" Page

//Check Hover text for all Links
//-------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
//* Hover text for "Dashboard" Link
//* Hover text for "Learning Instances" Link
//* Hover text for "Bots" Link

// Temporarily commented the assert because it is redirecting to report page ( will be fixed after 5.2 release)
When user paste url "http://ta1.unicornmangorage.com:3000/learning-instances/20404e03-30fa-43be-a470-5ff193185f11/report" for staging it should not redirected to performance report page
-------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* Navigate to Report Page
* It should not navigate to report Page

Learning Instance Listing Page Negative scenarios
If selecting staging - Learning instances which are in production should not be populated on datatable and vice-versa.
--------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Refresh the page
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Check Instance Details in Analyze Page
* Ensure uploaded documents should be classified
* Instance details page exist
* Click on Learning Instance Link
* Select "Environment" from search DropDown in LearningInstance Page
* Select Bots which are in "Staging"
* Instance name should be listed on list
* List of Instance which are on "Staging" should be displayed
* Refresh the page
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Refresh the page
* Select "Environment" from search DropDown in LearningInstance Page
* Select Bots which are in "Production"
* Instance name should be listed on list
* List of Instance which are on "Production" should be displayed


Validate Upload file - Type, limit, size, Count
----------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button

//* Click on browse button of create new instance page and upload 151 files,  limit exceed warning message should be displayed
//* Refresh the page
//* Click on browse button of create new instance page and upload unsupported file, file type error message should be displayed
//* Refresh the page
//* Click on browse button of create new instance page and upload above the max file size , limit to under 5 MB per file message should be displayed
//* Refresh the page
* After uploading upload texbox should not be blank

Modify learning instance listing url, error message should be displayed
-----------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Navitage to url "http://ta1.unicornmangorage.com:3000/learning-instance"
* Error Message "This page was either not found or is coming soon." should be displayed

Create New Instance Page
Try to enter instance name starting with space ,Blank Warning,Special Characters Warning,length warning in Instance Name field
------------------------------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Refresh the page
* Click on Learning Instance Link
* Click on New Instance Button
* New Learning Instance TextBox Should give message for Blank Warning,Special Characters Warning,length warning

Try to enter instance description with length more then 255 characters [BVA]
----------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
* Description textBox should give warning message for special charecters and length warning message if chracters more than 255
* Refresh the page

//Try to Select "Select Input" From Primary languages and Domain dropdown error message should be dispalyed
//-------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
//* Click on Learning Instance Link
//* Click on New Instance Button
//* Try to select "Select Input:" from "primary langauge" dropdown error message should be displayed
//* Refresh the page
//* Try to select "Select Input:" from "Domain" dropdown error message should be displayed




Validate Duplicate Field Name
------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Refresh the page
* Click on Learning Instance Link
* Click on New Instance Button
* On Create New Instance Page select "Invoices" From Domain DropDown, add "Invoice Number" as "Form" Field
* Error "No duplicate field names allowed" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "Quantity" as "Table" Field
* Error "No duplicate field names allowed" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "invoice Number" as "Form" Field
* Error "No duplicate field names allowed" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "quantity" as "Table" Field
* Error "No duplicate field names allowed" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "1234" as "Form" Field
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "abcdefghijklmnopqrstuvwxyz-012345678910111213141516a" as "Form" Field
* Error "Length should be under 50 characters" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "@2" as "Form" Field
* Error "Formatting must be numbers, letters or spaces" message should be displayed
* Refresh the page
* On Create New Instance Page select "Other" From Domain DropDown, add "abcdefghijklmnopqrstuvwxyz-012345678910111213141516a" as "Form" Field
* Error "Length should be under 50 characters" message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "InvoiceFormField" as "Form" Field, try to edit the form field added, it should not be editable

Check duplicate field name with adding underscore at the end of field/table name
---------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
//* Refresh the page

* On Create New Instance Page select "Invoices" From Domain DropDown, add "Invoice Number " as "Form" Field
* Error "Formatting must not have spaces at the beginning or end." message should be displayed
* Refresh the page
* On Create New Instance Page select "Invoices" From Domain DropDown, add "Quantity " as "Table" Field
* Error "Formatting must not have spaces at the beginning or end." message should be displayed

When all values are not inserted - create & analyze button should be disabled
If either of values are not inserted (Instance Name,Lang,Domain,files uploaded) - create & analyze button should be disabled
------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
//* Refresh the page

* Ensure "Create instance and analyze" button is "disabled"
* Refresh the page
* Enter the "Instance name" in "Instance Name" required Field
* Ensure "Create instance and analyze" button is "disabled"
* Refresh the page
* Select "English" from "Primary Language" dropdown
* Ensure "Create instance and analyze" button is "disabled"
* Refresh the page
* Select "Invoices" from "Domain" dropdown
* Ensure "Create instance and analyze" button is "disabled"
* Select "Sample" file by clicking on "Browse" button


When all values are inserted but no field is selected [all fields are Unchecked] then create & analyze button should be disabled
---------------------------------------------------------------------------------------------------------------------------------
* Open cognitive console logIn page
* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"

* Refresh the page
* Click on Learning Instance Link
* Click on New Instance Button
//* Refresh the page

* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then "Uncheck" all the checked fields
* Ensure "Create instance and analyze" button is "disabled"

When all values are inserted but no field is selected [all fields are Unchecked] and additional field is added then create & analyze button should be enabled
-------------------------------------------------------------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
//* Refresh the page

* Enter all the details to create new instance with uploading "Sample" file and domain "Invoices" primary language "English" and add form field "Name" and table field "Serial Number"
* Then "Uncheck" all the checked fields
* Ensure "Create instance and analyze" button is "Enabled"

Create new instance with single page document which have more then 50+ pages
-----------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
//* Refresh the page
* Update instance name

* Enter all the details to create new instance with uploading "SinglePageWithMultiPageDocs" file and domain "Other" primary language "English" and add form field "Account Number" and table field "Billing Usage"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* Check all the files are classified

On Loading PDF Documents in the staging, they are all falling in unclassified category (COG-2133)
-----------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "SinglePDF" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* Check all the files are classified

Bot Page Neagtive scenarios
while clicking on learning group name from data table it should be on same page.
---------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* After classification click on Create Bot Link
* Launch and Close Designer
* Click on Bot Link
* Click on Group name from datatable it should be on same page

If selecting staging/Production - Bots which are in production/staging should not be populated on datatable.
----------------------------------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Check Instance Details in Analyze Page
* Ensure uploaded documents should be classified
* Instance details page exist
* After classification click on Create Bot Link
* Launch and Close Designer
* Click on Bot Link
* Select "Environment" from search DropDown in LearningInstance Page
* Select Bots which are in "Staging"
* Instance name should be listed on Bot page list
* List of Instance which are on "Staging" should be displayed
* Refresh the page
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move bot from staging to production
* Refresh the page
* Select "Environment" from search DropDown in LearningInstance Page
* Select Bots which are in "Production"
* Instance name should be listed on Bot page list
* List of Instance which are on "Production" should be displayed

Search with Instance Name and check the status of the bot
---------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "Sample" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* After classification click on Create Bot Link
* Launch and Close Designer
* Click on Bot Link
* User Landed to Bot Page
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Group name should be same as the group name for which bot was created for that particular instance
* Bot should be in "training" status
* Move bot from staging to production
* Bot should be in "ready" status
* Refresh the page
* Click on Learning Instance Link
* Select "Instance Name" from search DropDown in LearningInstance Page
* Enter the instance name in LearningInstance Search textbox
* Move instance from staging to production
* Click on button "Yes, send to production"
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Bot should be in "active" status

Clicking on 'Run Bot Once' is not processing all the documents uploaded in staging	(COG-2229)
----------------------------------------------------------------------------------
//* Open cognitive console logIn page
//* When user login into cognitive console with valid user name as "Services" and valid password as "12345678"
//* Then dashboard should be displayed for service user
* Click on Learning Instance Link
* Click on New Instance Button
* Update instance name

* Enter all the details to create New Instance with uploading "SimilarFiles" file and Domain "Invoices" and Primary language "English"
* Then click on create instance and analyze button,uploaded documents should be classified
* Instance is successfully created and user is landed to "Analyzing documents..." page
* Click on Close and Run In Background button
* Wait for documents processing in Learning Instance Details Page
* Instance details page exist
* After classification click on Create Bot Link
* Train Group for successful processing
* Click on Bot Link
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Run the bot once
* Refresh the page
* Refresh the page
* Wait for page Load
* Select "Instance Name" from search DropDown in Bot Page
* Enter the instance name in search textbox
* Count of file successfully processed should be "5"

Validation Page Negative Scenario
Hover on Learning Instances icon , 'Learning Instance' text should be displayed
---------------------------------
//* Open cognitive console logIn page
* When user click on profile button in home page
* When user click on logout button
* Ensure user should be logout and landed to login page

* When user login into cognitive console with valid user name as "validator" and valid password as "12345678"
* Then user with Validator role get only Learning Instance link access