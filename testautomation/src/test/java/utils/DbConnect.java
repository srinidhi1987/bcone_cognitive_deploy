package utils;

import driver.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnect {
    // init database constants
    private static final String DATABASE_DRIVER = System.getenv("databaseDriver");
    private static final String DATABASE_URL ="jdbc:sqlserver://"+System.getenv("hostName")+":"+System.getenv("port")+";databaseName=AliasData";
    private static final String USERNAME = System.getenv("dbUserName");
    private static final String PASSWORD = System.getenv("dbpassword");
    private static final String MAX_POOL = "250";

    // init connection object
    public static Connection connection;
    // init properties object
    public static Properties properties;


    // connect database
    public static Connection connect() throws SQLException {
        //if (connection == null) {
        try {
            Class.forName(DATABASE_DRIVER);
            connection = DriverManager.getConnection(DATABASE_URL,USERNAME,PASSWORD );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        //}
        return connection;

    }

    // disconnect database
    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}