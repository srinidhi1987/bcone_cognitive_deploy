package utils;

import driver.Driver;
import exceptions.DriverException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

import java.io.*;

import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.apache.poi.xssf.usermodel.XSSFCell;

public class TestBase {
    private final Logger logger = LogManager.getLogger();

    private int documentProcessingTimeoutSec = Integer.parseInt((System.getenv("documentProcessingTimeoutSec")));
    private int pageLoadWaitTimeoutSec = Integer.parseInt((System.getenv("pageLoadWaitTimeoutSec")));
    private int elementWaitTimeoutSec = Integer.parseInt((System.getenv("elementWaitTimeoutSec")));
    private int explicitWaitTimeoutSec = Integer.parseInt((System.getenv("explicitWaitTimeoutSec")));
    private int pollingTimeoutMs = Integer.parseInt((System.getenv("pollingTimeoutMs")));

    private WebDriverWait wait = new WebDriverWait(Driver.webDriver, explicitWaitTimeoutSec);

    // Fluent Wait implementation : wait for page load
    public void waitForPageLoad() {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(pageLoadWaitTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(Exception.class);
        wait.until((ExpectedCondition<Boolean>) driver -> {
            By pageLoader = By.xpath("//div[@class='aa-page-loading-overlay-loader']");
            if ((driver != null ? driver.findElements(pageLoader).size() : 0) != 0) {
                Boolean pageIsLoading = driver.findElement(pageLoader).isEnabled();
                logger.trace("Page is loading..");
                return false;
            } else {
                logger.trace("Size 0 - Loader not available");
                return true;
            }
        });
    }

    // Fluent Wait implementation : wait till progress bar exist
    public void waitForDocumentProcessing() {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(documentProcessingTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        wait.until((ExpectedCondition<Boolean>) driver -> {
            By progressBar = By.xpath("//div[@class='aa-analyzing-project-progress-bar']");
            return driver != null && driver.findElements(progressBar).size() == 0;
        });
    }

    // Fluent Wait implementation : wait till progress bar appear in Analyze page
    public void waitForAnalyzePage() {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(pageLoadWaitTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        wait.until((ExpectedCondition<Boolean>) driver -> {
            By progressBar = By.xpath("//div[@class='aa-analyzing-project-progress-bar']");
            logger.trace("Waiting for progress bar to appear");
            return driver != null && driver.findElement(progressBar).isEnabled();
        });
    }

    // Fluent Wait implementation : wait till classifier / production document processing spinner appear in Learning Instance Details page
    public void waitForSpinnerToAppear() {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(documentProcessingTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        wait.until((ExpectedCondition<Boolean>) driver -> {
            By spinner = By.xpath("//DIV[@class=\"aa-project-details-container-header--projectclassifying\"]");
            logger.trace("Spinner is available - wait for spinner to appear");
            return driver != null && driver.findElement(spinner).isEnabled();
        });
    }

    // Fluent Wait implementation : wait till classification / production document processing complete in Learning Instance Details page
    public void waitForSpinner() {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(documentProcessingTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        wait.until((ExpectedCondition<Boolean>) driver -> {
            By spinner = By.xpath("//DIV[@class=\"aa-project-details-container-header--projectclassifying\"]");
            if ((driver != null ? driver.findElements(spinner).size() : 0) != 0) {
                Boolean spinnerIsVisible = driver.findElement(spinner).isEnabled();
                logger.trace("Spinner is available");
                return false;
            } else {
                logger.trace("Size 0 - Spinner  not available");
                return true;
            }
        });
    }

    // Fluent Wait implementation : wait for element to be clickable
    public void waitForElementClick(final By element) throws InterruptedException {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(elementWaitTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        final Boolean until = wait.until((ExpectedCondition<Boolean>) driver -> {
            if (driver != null) {
                driver.findElement(element).click();
            }
            return true;
        });
    }

    // Fluent Wait implementation : wait for element to be visible
    public void waitForElementDisplay(final By element) throws InterruptedException {
        Wait<WebDriver> wait = new FluentWait<>(Driver.webDriver).withTimeout(elementWaitTimeoutSec, TimeUnit.SECONDS)
                .pollingEvery(pollingTimeoutMs, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        final Boolean until = wait.until((ExpectedCondition<Boolean>) driver -> {
            if (driver != null) {
                driver.findElement(element).isDisplayed();
            }
            return true;
        });
    }


    /**
     * Use to upload files to instance
     *
     * @param folderPath    source directory
     * @param filesToUpload comma separated list of files
     * @throws Exception exception
     */
    public void fileUpload(String folderPath, String filesToUpload) throws Exception {
        String commandLine = String.format("\"%s\" \"%s\"", System.getenv("fileUploadBatchFile"), folderPath);
        String fileName = "UploadedFiles.txt";
        String information = String.format("FileNames=%s", filesToUpload);
        logInformationIntoTextFile(information, fileName);
        logger.trace("COMMAND LINE: " + commandLine);
        runBatchFile(commandLine);
    }

    public void uploadAllFiles(String folderPath, String testcase) throws Exception {
        String commandLine = String.format("\"%s\" \"%s\" \"%s\"", System.getenv("multipleFileUploadBatchFile"), folderPath, testcase);
        logger.trace("COMMAND LINE: " + commandLine);
        runBatchFile(commandLine);
    }

    // Command runner
    public void runBatchFile(String command) throws IOException, InterruptedException, DriverException {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
        if (process.exitValue() != 0) {
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line;
            String result = "";
            while ((line = reader.readLine()) != null) {
                logger.trace("Batch command line" + line);
            }
            reader.close();
            logger.trace("Result: " + result);
            throw new DriverException("Failed to execute batch command (" + command + ") with error code: " + process.exitValue() + process.getErrorStream());
        }
    }

    // Read designer log file which is generated during smoke execution
    public String validateDesignerLogs(String filename) throws Exception {
        StringBuilder results = new StringBuilder();
        Scanner in = new Scanner(new File(filename));
        while (in.hasNext()) { // Iterates each line in the file
            results.append(in.nextLine());
        }
        in.close();
        return results.toString();
    }

    // Function will be used for File Wait Creation
    public void waitForFileCreation(String filename) throws Exception {
        File f = new File(filename);
        while (!f.exists()) {
            f = new File(filename);
            logger.trace("File not Created");
        }
    }

    // Get web elements initialized by @FindBy
    public WebElement getWebElement(WebElement element) throws Exception {
        return element;
    }

    // Explicit Wait Implementation : Pass web element xpath and method will wait for element to be clickable - Default time out is 200 Seconds
    public void explicitWait(String elementPath) {
        By locator = By.xpath(elementPath);
        WebDriverWait wait = new WebDriverWait(Driver.webDriver, explicitWaitTimeoutSec);
        logger.trace("waiting for element: " + locator);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    // Excel data reading methods get data
    public static String getData(String file, String Sheet, int rowNum, int colNum) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sheet1 = wb.getSheet(Sheet);
        XSSFRow row = sheet1.getRow(rowNum);
        String data = row.getCell(colNum).getStringCellValue();
        wb.close();
        return data;
    }

    public void writeDataToExcelSheet(String file, String Sheet, int rowNum, int colNum) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook (fis);
        XSSFSheet sheet = workbook.getSheet(Sheet);
        XSSFRow row1 = sheet.getRow(rowNum);
        XSSFCell cell1 = row1.getCell(colNum);
        DateFormat dateInstance = new SimpleDateFormat("MMMM dd, yyyy");
        cell1.setCellValue(dateInstance.format(Calendar.getInstance().getTime()));
        fis.close();
        FileOutputStream fos =new FileOutputStream(file);
        workbook.write(fos);
        fos.close();
        System.out.println("Done");
    }

    // wait for classification
    public void waitForYetToBeClassifiedToDisappear() {
        // wait.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector(".datatable-column"), "Yet to be classified"));
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath("//*[text()='Yet to be classified']"),"Yet to be classified"));
        System.out.println("Yet to be classified");
    }

    // Log data into file
    private void logInformationIntoTextFile(String information, String fileName) throws Exception {
        String path = System.getenv("test_Documents_FolderPath");
        String fullPath = path + "\\" + fileName;
        File file = new File(fullPath);
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        Writer w = new BufferedWriter(osw);
        w.write(information);
        w.close();
    }

    // Get group id for uploaded documents
    public void setGroupId(String groupType) throws SQLException {
        String classificationId = null;
        String trainingGroupId = null;
        String groupId = null;

        switch (groupType) {
            case "trainingGroup":
                trainingGroupId = String.format("SELECT classificationid FROM [FileManager].[dbo].[FileDetails]" +
                        " WHERE projectid IN (SELECT Id FROM [FileManager].[dbo].[ProjectDetail] WHERE Name='%s')" +
                        " AND filename='Group3_Noisy_documents.tiff'", Driver.instName);
                break;
            case "fileCountGroup":
                trainingGroupId = String.format("SELECT classificationid FROM [FileManager].[dbo].[FileDetails]" +
                        " WHERE projectid IN (SELECT Id FROM [FileManager].[dbo].[ProjectDetail] WHERE Name='%s')" +
                        " AND filename='Group1_SinglePage.tif'", Driver.instName);
                break;
            case "successGroup":
                trainingGroupId = String.format("SELECT classificationid FROM [FileManager].[dbo].[FileDetails]" +
                        " WHERE projectid IN (SELECT Id FROM [FileManager].[dbo].[ProjectDetail] WHERE Name='%s')" +
                        " AND filename='Group2_Vector_Rotated_PDF.pdf'", Driver.instName);
                break;

            }

        Statement sqlQueryStatement = DbConnect.connect().createStatement();
        ResultSet queryResult = sqlQueryStatement.executeQuery(trainingGroupId);
        while (queryResult.next()) {
            classificationId = queryResult.getString(1);
        }
        assert classificationId != null;
        groupId = "Group_" + classificationId;
        switch (groupType) {
            case "trainingGroup":
                Driver.trainingGroupName = groupId;
                break;
            case "fileCountGroup":
                Driver.documentCountGroupName = groupId;
                break;
            case "successGroup":
                Driver.trainingSuccessGroupName = groupId;
                break;
        }
        DbConnect.connection.close();
        logger.trace("Classification Id: " + classificationId);
    }


    public static void deleteUser(String userName) throws SQLException {

        Connection conn = null;
        Statement stmt = null;

        String dbURL = "jdbc:sqlserver://"+System.getenv("hostName")+":"+System.getenv("port")+";DatabaseName=CRDB";
        conn = DriverManager.getConnection(dbURL, System.getenv("dbUserName"), System.getenv("password"));

        stmt = conn.createStatement();
        String sql = "Delete from [CRDB].[dbo].[Users] where [UserName] = '" + userName + "'";
        stmt.executeUpdate(sql);
    }


    // Return list from PropertyName
    public static ArrayList<String> propertyNameAsList(String propertyName) {
        ArrayList<String> propertyNames = new ArrayList<String>();
        String[] array = System.getenv((propertyName)).split(",");
        for (String property : array) {
            propertyNames.add(property.trim());
        }
        return propertyNames;

    }

    public void waitForTextToBeExtracted(String textValue) {
        WebElement element = Driver.webDriver.findElement(By.xpath("//INPUT[@value='" + textValue + "']"));
        wait.until(ExpectedConditions.textToBePresentInElementValue(element, textValue));
    }
}
