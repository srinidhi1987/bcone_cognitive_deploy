package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.openqa.selenium.support.PageFactory;
import pageobjects.*;
import utils.TestBase;

import java.util.ArrayList;
import java.util.List;


import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.assertThat;

public class PerformanceReportPageStepImplementation {

    private PerformanceReportPage performancePage = PageFactory.initElements(Driver.webDriver, PerformanceReportPage.class);
    private DashboardPage dashBoardPage = PageFactory.initElements(Driver.webDriver, DashboardPage.class);
    private DashboardPageStepImplementation dashboardstepImplementation = PageFactory.initElements(Driver.webDriver, DashboardPageStepImplementation.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    private List<String> actualResult = new ArrayList<>();
    private List<String> listOfInstanceOnProduction = new ArrayList<>();
    List<String> filteredListWithFPAndSTPandAccuracyValuesFromDashboard = new ArrayList<>();

    @ContinueOnFailure
    @Step("Get all the instance name which are in production")
    public void clickOnInstanceNameToCheckPerformanceReport() {
        // Given
        baseClass.waitForPageLoad();

        // When
        listOfInstanceOnProduction = performancePage.getDashBoardInstanceName();
        //System.out.println(listOfInstanceOnProduction);

        // Then

    }

    @ContinueOnFailure
    @Step("Click on instance name")
    public void clcikOnInstanceName() throws InterruptedException {
        // Given
        baseClass.explicitWait("//span[text()='My Learning Instances']");

        // When
        performancePage.clickOnInstance(Driver.instName);

        // Then

    }

    @ContinueOnFailure
    @Step("User will be landed to Performance Report Page")
    public void performanceReportPageTitle() throws InterruptedException {
        // Given

        // When
        //  performancePage.waitForPerformanceReportHeading();
        baseClass.explicitWait("//a[text()='Performance Report']");

        // Then
        assertThat(performancePage.getPerformanceReportpageTitle().getText()).isEqualTo("Performance Report");
        actualResult = performancePage.getPerformanceReportFilesProcessedSTPAccuracyAndValuesOfProcessingResults();

    }

    // used to extract data from the returned list when using validateWithExpectedProcessingResult() method.
    private static final int FILES_UPLOADED = 0, FILES_UPLOADED_IN_PROCESSINGRESULTS = 1, FILES_SUCCESSFULLY_PROCESSED = 2;
    private static final int FILES_SENT_TO_VALIDATION = 3, FILES_VALIDATED = 4, FILES_INVALID = 5;

    @ContinueOnFailure
    @Step("Validate Processing result values for <testPhase>")
    public void validateWithExpectedProcessingResult(String testPhase) throws Exception {
        // Given

        //  String expectedFilesProcessed,expectedSTP,expectedAccuracy;
        String expectedFilesUploaded = "", expectedFilesProcessedInProcesseingResult = "", expectedFilesSuccessfullyProcessed = "";
        String expectedFileSentToValidation = "", expectedFilesValidated = "", expectedFilesInvalid = "";
        // String expectedResultsSheetName = "PerformanceReportActualResults";

        System.out.println("getSpecFile = " + testPhase);
        if (testPhase.equals("Performance Report Spec")) {
            expectedFilesUploaded = "0";
            expectedFilesProcessedInProcesseingResult = "0";
            expectedFilesSuccessfullyProcessed = "0";
            expectedFileSentToValidation = "0";
            expectedFilesValidated = "0";
            expectedFilesInvalid = "0";
        } else if (testPhase.equals("Smoke")) {
            expectedFilesUploaded = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportActualResults", 1, 1);
            expectedFilesProcessedInProcesseingResult = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportActualResults", 2, 1);
            expectedFilesSuccessfullyProcessed = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportActualResults", 3, 1);
            expectedFileSentToValidation = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportActualResults", 4, 1);
            expectedFilesValidated = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportActualResults", 5, 1);
            expectedFilesInvalid = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportActualResults", 6, 1);
        } else if (testPhase.equals("Sanity")) {
            expectedFilesUploaded = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportResults_Sanity", 1, 1);
            expectedFilesProcessedInProcesseingResult = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportResults_Sanity", 2, 1);
            expectedFilesSuccessfullyProcessed = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportResults_Sanity", 3, 1);
            expectedFileSentToValidation = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportResults_Sanity", 4, 1);
            expectedFilesValidated = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportResults_Sanity", 5, 1);
            expectedFilesInvalid = TestBase.getData(System.getenv("TestDataPath"), "PerformanceReportResults_Sanity", 6, 1);
        }

        // When

        // Then
        //PERFORMANCE : 6PERFORMANCE : 1PERFORMANCE : 0PERFORMANCE : 1PERFORMANCE : 0PERFORMANCE : 0Actual result -[6, 1, 0, 1, 0, 0]
        assertThat(actualResult.get(FILES_UPLOADED)).isEqualTo(expectedFilesUploaded);
        assertThat(actualResult.get(FILES_UPLOADED_IN_PROCESSINGRESULTS)).isEqualTo(expectedFilesProcessedInProcesseingResult);
        assertThat(actualResult.get(FILES_SUCCESSFULLY_PROCESSED)).isEqualTo(expectedFilesSuccessfullyProcessed);
        assertThat(actualResult.get(FILES_SENT_TO_VALIDATION)).isEqualTo(expectedFileSentToValidation);
        assertThat(actualResult.get(FILES_VALIDATED)).isEqualTo(expectedFilesValidated);
        assertThat(actualResult.get(FILES_INVALID)).isEqualTo(expectedFilesInvalid);
    }


    @ContinueOnFailure
    @Step("Click on each instance which are in production to validate STP Accuracy and Total files processed for version <5.2>")
    public void clickOnEachInstanceWhichAreInProduction(String version) throws Exception {
        // Given
        String instancedetailsDashboardPage;
        int i;

        // When
        // Get the list of instance which are in production
        listOfInstanceOnProduction = performancePage.getDashBoardInstanceName();

        // Then

        //System.out.println(listOfInstanceOnProduction);
        if (listOfInstanceOnProduction.size() > 0) {
            for (i = 0; i < listOfInstanceOnProduction.size(); i++) {
                baseClass.waitForPageLoad();
                baseClass.waitForElementClick(dashBoardPage.getHeaderOfInstance());
                // Step-2 Get instance detail from Dashboard
                // Given
                String productionInstance = listOfInstanceOnProduction.get(i);

                // When
                instancedetailsDashboardPage = dashBoardPage.getInstanceDetail(productionInstance);
                // Step-3 Click on instance from list
                String productionInstanceName = listOfInstanceOnProduction.get(i);
                dashBoardPage.clickOnInstance(productionInstanceName);
                baseClass.waitForPageLoad();

                // Step-4 we are on performance page
                // Then
                assertThat(performancePage.getPerformanceReportpageTitle().getText()).isEqualTo("Performance Report");
                // Step-5 get the instance detail from performance page
                //System.out.println("Performance report Page Instance Values" + performancePage.getInstanceTotals());

                final String[] performanceReportPageInstanceDetails = {""};
                performancePage.getInstanceTotals().forEach(text -> {
                    performanceReportPageInstanceDetails[0] = performanceReportPageInstanceDetails[0] + text;
                });
                //System.out.println(performanceReportPageInstanceDetails[0]);

                // Then
                assertThat(instancedetailsDashboardPage).isEqualTo(performanceReportPageInstanceDetails[0]);

                // Given

                // When
                // dashPage.clickOnDashboardTab();
                liListingPage.clickonLinks("Dashboards");
                Driver.webDriver.navigate().refresh();
            }
        } else {
            writeMessage("There are no instance on production");
        }
    }
}
