package implementation;

import com.thoughtworks.gauge.Step;
import driver.Driver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageobjects.*;

import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.*;

import com.thoughtworks.gauge.ContinueOnFailure;
import utils.TestBase;


public class CreateNewInstanceStepImplementation {

    private CreateNewInstancePage newInstancePage = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private AnalyzingDocumentPage analyzingPage = PageFactory.initElements(Driver.webDriver, AnalyzingDocumentPage.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private CreateNewInstancePage newInstance = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private LearningInstanceDetailPage liDetailsPage = PageFactory.initElements(Driver.webDriver, LearningInstanceDetailPage.class);
    private BotListingPage botPage = PageFactory.initElements(Driver.webDriver, BotListingPage.class);
    private BotListingPageStepImplementation botPageStepImplementation = PageFactory.initElements(Driver.webDriver, BotListingPageStepImplementation.class);
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    private String instanceName, instDesc, totalNumberOfFilesUploaded, uniqueInstanceName;
    private List<String> uniqueList = new ArrayList<String>();
    private String smokeTestDocumentsFolder = System.getenv("test_Documents_FolderPath");
    private String totalFilesUploadedNewInstancePage;
    private String storyTestDocumentsFolder = System.getenv("test_Documents_Story_FolderPath");


    @ContinueOnFailure
    @Step("Click on Learning Instance Link")
    public void clickLearningInstanceLink() throws Exception {
        // Given

        // When
        liListingPage.clickonLinks("Learning Instance");//  newInstancePage.clickLearningInstanceLink();
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(liListingPage.getLielement());
        String beforeLearningInstancePageTitle = newInstancePage.getlearningInstancePageTitle();

        // Then
        assertThat(beforeLearningInstancePageTitle).isEqualTo("My Learning Instances");
    }


    // COG-889  By default Domain- Select Input is selected
    @ContinueOnFailure
    @Step("Default value of Domain DropDown is Select Input")
    public void defaultDomainType() throws InterruptedException {
        // Given

        //When
        String actualDomainValue = newInstancePage.domainType();

        //Then
        assertThat(actualDomainValue).isEqualTo("Select Input:");
        writeMessage("Default Domain Type is SelectInput : Completed");
    }

    // COG-889  By default Language- English is selected
    @ContinueOnFailure
    @Step("Verify values of Primary language DropDown")
    public void listPrimaryLanguage() throws InterruptedException {
        // Given
        List<String> expectedPrimaryLanguageDDList = newInstancePage.propertyNameAsList("listLanguage");

        // When
        List<String> actalPrimaryLanguageDDList = newInstancePage.verifyPrimaryLanguageList();

        //Then
        assertThat(expectedPrimaryLanguageDDList).isNotNull();
        assertThat(actalPrimaryLanguageDDList).isNotNull();
        assertThat(actalPrimaryLanguageDDList).isEqualTo(expectedPrimaryLanguageDDList);
        assertThat(actalPrimaryLanguageDDList).containsAll(expectedPrimaryLanguageDDList);
        assertThat(actalPrimaryLanguageDDList.size()).isEqualTo(expectedPrimaryLanguageDDList.size());
    }

    @ContinueOnFailure
    @Step("Default Value of Primary language is English")
    public void defaultPrimaryLanguage() throws InterruptedException {
        // Given

        // When
        String primaryLanguageActualValue = newInstancePage.primaryLanguageDefaultType();

        // Then
        assertThat(primaryLanguageActualValue).isEqualTo("English");
        writeMessage("By Default Primary Language selected is - English");
    }

    // COG-886 On selecting any "Type", all corresponding fields should come by default
    @ContinueOnFailure
    @Step("Verify values of Domain DropDown")
    public void domainDropDownList() throws InterruptedException {
        // Verify Domain DropDown have values.
        // Given
        List<String> expectedDomainDDList = newInstancePage.propertyNameAsList("listDomainType");

        // When
        List<String> actualDomainDDList = newInstancePage.verifyDomainList();

        //Then
        assertThat(expectedDomainDDList).isNotNull();
        assertThat(actualDomainDDList).isNotNull();
        assertThat(actualDomainDDList).containsAll(expectedDomainDDList);
        assertThat(actualDomainDDList.size()).isEqualTo(expectedDomainDDList.size());
    }

    // used to extract data from the returned list when using getFormFields(...) method.
    private static final int FORM_FIELDS = 0;
    private static final int TABLE_FIELDS = 1;
    private static final int FORM_LIST = 2;

    @ContinueOnFailure
    @Step("Verify values displayed for each dropdown item")
    public void selectingTypeOfFile() throws InterruptedException {
        // Given

        // When
        for (String domainName : newInstancePage.propertyNameAsList("listOfDomain")) {
            String selectedDomain = newInstancePage.getSelectedDomain(domainName);
            List<List<String>> actualStandardFormFieldList = newInstancePage.getStandardFormFields();
            List<String> actualStandardFormFields, actualStandardTableFields, actualCheckedFormList;
            actualStandardFormFields = actualStandardFormFieldList.get(FORM_FIELDS);
            actualStandardTableFields = actualStandardFormFieldList.get(TABLE_FIELDS);
            actualCheckedFormList = actualStandardFormFieldList.get(FORM_LIST);
            List<String> expectedStandardFormFields = newInstancePage.getFormFields(selectedDomain).get(FORM_FIELDS);
            List<String> expectedStandardTableFields = newInstancePage.getFormFields(selectedDomain).get(TABLE_FIELDS);
            List<String> expectedCheckedFormList = newInstancePage.getFormFields(selectedDomain).get(FORM_LIST);

            // Then
            writeMessage("======================= Running test for " + selectedDomain + " =======================");
            assertThat(actualStandardFormFields).isEqualTo(expectedStandardFormFields);
            assertThat(actualStandardTableFields).isEqualTo(expectedStandardTableFields);
            assertThat(actualCheckedFormList).isEqualTo(expectedCheckedFormList);
            assertThat(actualStandardFormFields).containsAll(expectedStandardFormFields);
            assertThat(actualStandardTableFields).containsAll(expectedStandardTableFields);
            assertThat(actualCheckedFormList).containsAll(expectedCheckedFormList);
            assertThat(actualStandardFormFields.size()).isEqualTo(expectedStandardFormFields.size());
            assertThat(actualStandardTableFields.size()).isEqualTo(expectedStandardTableFields.size());
            assertThat(actualCheckedFormList.size()).isEqualTo(actualCheckedFormList.size());
        }
    }

    @ContinueOnFailure
    @Step("New Learning Instance TextBox Should give message for Blank Warning,Special Characters Warning,length warning")
    public void getInstanceNameText() throws InterruptedException {
        // Given
        String textboxValue, actualInstanceNameValue;
        WebElement textboxInstanceNameRequired;
        textboxValue = "";

        // When
        actualInstanceNameValue = newInstancePage.validateInstanceNameTextBox(textboxValue);
        textboxInstanceNameRequired = newInstancePage.textboxInstanceNameRequired();

        // Then
        assertThat(textboxInstanceNameRequired.isDisplayed()).isTrue();

        // Given

        // When
        textboxInstanceNameRequired.click();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo("Required");


        writeMessage("Value for TextBox Instance Name is always Required" + textboxValue);
        // Given
        Driver.webDriver.navigate().refresh();
        textboxValue = " AutoInstance";

        //  When
        actualInstanceNameValue = newInstancePage.validateInstanceNameTextBox(textboxValue);

        // Then
        assertThat(textboxInstanceNameRequired.isDisplayed()).isTrue();

        // Given

        // When
        textboxInstanceNameRequired.click();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo("Formatting must not have spaces at the beginning or end.");

        writeMessage("Formatting must not have spaces at the beginning or end." + actualInstanceNameValue);

        // Given
        Driver.webDriver.navigate().refresh();
        textboxValue = "~@#$%^&*:;<>.,/}{+";

        //  When
        actualInstanceNameValue = newInstancePage.validateInstanceNameTextBox(textboxValue);

        // Then
        assertThat(textboxInstanceNameRequired.isDisplayed()).isTrue();

        // Given

        // When
        textboxInstanceNameRequired.click();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo("Formatting must be numbers, letters, spaces or (_, -)");

        writeMessage("Special Characters not allowed " + actualInstanceNameValue);

        // Given
        Driver.webDriver.navigate().refresh();
        textboxValue = "abcdefghijklmnopqrstuvwxyz-012345678910111213141516";

        // When
        actualInstanceNameValue = newInstancePage.validateInstanceNameTextBox(textboxValue);

        // Then
        assertThat(textboxInstanceNameRequired.isDisplayed()).isTrue();
        assertThat(actualInstanceNameValue.length()).isGreaterThan(50);

        // Given

        // When
        textboxInstanceNameRequired.click();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo("Length should be under 50 characters");

        writeMessage("Length Should be under 50 characters " + actualInstanceNameValue);

    }


    @ContinueOnFailure
    @Step("Enter all the details to create New Instance with uploading <3> file and Domain <Invoices> and Primary language <English>")
    public void createNewInstance(String filesUploaded, String domainName, String primaryLanguage) throws Exception {
        // Given
        instanceName = Driver.instName;
        uniqueInstanceName = instanceName;
        instDesc = System.getenv("instance_Desc");

        // When
        newInstancePage.insertRequiedFields(instanceName, instDesc, domainName, primaryLanguage);
        newInstancePage.clickOnBrowseButton();
        Thread.sleep(500);
        newInstancePage.uploadFiles(primaryLanguage, domainName, filesUploaded);
        totalNumberOfFilesUploaded = newInstancePage.inputFileTextBox();
        List<String> listCheckedItems = newInstancePage.getCheckedListForCreateNewLI();
        uniqueList.add(domainName);
        uniqueList.add(uniqueInstanceName);
        uniqueList.addAll(listCheckedItems);

        // Then
    }

    @ContinueOnFailure
    @Step("Then click on create instance and analyze button,uploaded documents should be classified")
    public void clickOnCreateNewInstanceButton() throws Exception {
        // Given

        // When
        newInstance.clickOnCreateAndAnalyzeButton();
        Thread.sleep(3000);
        baseClass.waitForAnalyzePage();
        baseClass.writeDataToExcelSheet(System.getenv("TestDataPath"), "GeneralInformation", 3, 1);

        // Then
        assertThat(analyzingPage.getProgressbar().isDisplayed()).isTrue();
    }


    @ContinueOnFailure
    @Step("Check total number of files in analyzing are same as uploaded during creating new instance")
    public void checkNumberOfTotalFilesInAnalyzingDocument() throws Exception {
        // Given
        baseClass.waitForElementDisplay(analyzingPage.progressbar());
        String totalFilesDisplayedInAnalyzingPage = analyzingPage.totalFilesInAnalyzingDocuments();

        // When
        baseClass.waitForAnalyzePage();
        baseClass.explicitWait("//div[@class='aa-analyzing-project-progress-bar']/following-sibling::span[2]");
        System.out.println("Actual TotalFiles " + totalFilesDisplayedInAnalyzingPage);

        // Then
        assertThat(totalFilesUploadedNewInstancePage.trim()).isEqualTo(totalFilesDisplayedInAnalyzingPage.trim());
    }

    @ContinueOnFailure
    @Step("Check Instance Details in Analyze Page")
    public void checkInstanceDetails() throws Exception {
        // Given

        // When
        List<String> analyzePageDetailList = analyzingPage.learningInstanceValueDetails();

        // Then
        System.out.println("uniqueList = " + uniqueList);
        System.out.println("analyzePageDetailList = " + analyzePageDetailList);
        assertThat(uniqueList.containsAll(analyzePageDetailList));
        assertThat(uniqueList.equals(analyzePageDetailList));
        assertThat(uniqueList).doesNotContainSequence(analyzePageDetailList);
        // assertThat(analyzePageDetailList.size()).isEqualTo(uniqueList.size());
        assertThat(analyzePageDetailList).isNotNull();
        assertThat(uniqueList).isNotNull();
    }

    @ContinueOnFailure
    @Step("Create New Instance With Same Instance Name with uploading <1> file and Domain <Invoices> and primary language <English>")
    public void checkForDuplicateInstanceName(int noOfFiles, String domainName, String primaryLanguage) throws Exception {
        // Given
        instanceName = Driver.instName; //uniqueInstanceName;
        instDesc = System.getenv("instance_Desc");

        // When
        newInstancePage.insertRequiedFields(instanceName, instDesc, domainName, primaryLanguage);
        newInstancePage.clickOnBrowseButton();
        Thread.sleep(500);
        String filePath = "C:/IQBot_TestData/SinglePage.cmd";
        String multipleFilenames = newInstancePage.getallFilenames(filePath);
        baseClass.fileUpload(smokeTestDocumentsFolder + "\\" + "Sample", multipleFilenames);

        // Then
    }


    @ContinueOnFailure
    @Step("Then click on create instance and analyze button")
    public void clickOnCreateNewInstanceButtonForDuplicateInstanceName() throws InterruptedException {
        // Given

        // When
        newInstancePage.clickOnCreateNewInstanceButtonForDuplicateInstanceName();

        // Then
    }

    @ContinueOnFailure
    @Step("Duplicate error message will be displayed")
    public void duplicateError() throws InterruptedException {
        // Given

        // When
        liListingPage.waitForMessageToBeDisplayed();
        String duplicateErrorMessage = newInstancePage.duplicateErrorMessage().getText();

        // Then
        assertThat(duplicateErrorMessage).isEqualTo("Duplicate Instance Name");
    }

    @ContinueOnFailure
    @Step("If none of the value is selected Domain DropDown Should give Warning Message")
    public void domainDDWarningMessage() throws InterruptedException {
        // Given
        WebElement domainDDErrorMessage = newInstancePage.domainDDErrorMessage();

        // When
        Thread.sleep(5000);
        newInstancePage.validateDomainDDErrorMessage();

        // Then
        assertThat(domainDDErrorMessage.isDisplayed()).isTrue();
        writeMessage("Length Should be under 50 characters ");
    }

    @ContinueOnFailure
    @Step("Description textBox should give warning message for special charecters and length warning message if chracters more than 255")
    public void descriptionTextBoxSpecialCharacterErrorMsg() {
        // Given
        String textboxValue;
        textboxValue = "~@#$%^&*:;<>.,/}{+";
        String actualDescriptionTextboxValue;
        WebElement descriptionErrorMessage = newInstancePage.textboxInstanceNameRequired();// newInstancePage.descriptionErrorMessage();

        //  When
        actualDescriptionTextboxValue = newInstancePage.validateDescriptionTextBox(textboxValue);

        // Then
        assertThat(descriptionErrorMessage.isDisplayed()).isTrue();

        // Given

        // When
        descriptionErrorMessage.click();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo("Formatting must be numbers, letters, spaces, periods, commas, or (_, -)");
        writeMessage("Special Characters are not allowed " + actualDescriptionTextboxValue);

        // Given
        Driver.webDriver.navigate().refresh();
        textboxValue = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

        // When
        actualDescriptionTextboxValue = newInstancePage.validateDescriptionTextBox(textboxValue);

        // Then
        assertThat(descriptionErrorMessage.isDisplayed());
        assertThat(actualDescriptionTextboxValue.length()).isGreaterThan(255);

        // Given

        // When
        descriptionErrorMessage.click();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo("Length should be under 255 characters");


        writeMessage("Length should be under 255 characters " + actualDescriptionTextboxValue);
    }

    @ContinueOnFailure
    @Step("Click on Cancel Button to cancel the Instance Creation")
    public void clickCancelButton() throws InterruptedException {
        // Given

        // When
        newInstancePage.clickCancelButton();

        // Then
    }

    @ContinueOnFailure
    @Step("Learning Instance Page is displayed")
    public void learningInstancePageDisplayed() throws InterruptedException {
        // Given

        // When
        baseClass.waitForPageLoad();

        // Then
        assertThat(liListingPage.getMyLearningInstancePageTitle()).isEqualTo("My Learning Instances");
    }

    @ContinueOnFailure
    @Step("Click on Instance Name")
    public void clickOnSearchedInstanceName() {
        // Given

        // When
        liListingPage.clickOnSearchedInstanceName();

        // Then
    }

    @ContinueOnFailure
    @Step("Get the total number of files uploaded")
    public void getTotalNumberOfFilesUploaded() {
        // Given
        totalFilesUploadedNewInstancePage = totalNumberOfFilesUploaded.substring(15, totalNumberOfFilesUploaded.length() - 18);

        // When

        // Then
        System.out.println("Total files" + totalFilesUploadedNewInstancePage);
    }

    private static final int INSTANCE_NAME = 0;

    @ContinueOnFailure
    @Step("Check the Instance name in analyze page is same as Instance name was given while creating instance")
    public void matchInstanceName() {
        // Given

        // When
        List<String> analyzePageDetailList = analyzingPage.learningInstanceValueDetails();

        // Then
        assertThat(analyzePageDetailList).isNotNull();
        assertThat(uniqueList).isNotNull();
        assertThat(analyzePageDetailList.indexOf(INSTANCE_NAME)).isEqualTo(uniqueList.indexOf(INSTANCE_NAME));
    }

    @ContinueOnFailure
    @Step("Enter all the details to create new instance with uploading <1> file and domain <Invoices> primary language <English> and add form field <Name> and table field <Serial Number>")
    public void createInstanceWithAdditionalFormAndTableFields(String filesUploaded, String domainName, String primaryLanguage, String formFieldName, String tableFieldName) throws Exception {

        // Given
        List<String> addedFormFieldAndTableField = new ArrayList<String>();

        // When
        createNewInstance(filesUploaded, domainName, primaryLanguage);
        addedFormFieldAndTableField = newInstancePage.getAddedFormField(formFieldName, tableFieldName);
        uniqueList.addAll(addedFormFieldAndTableField);

        // Then
    }

    /*@ContinueOnFailure
    @Step("After classification click on Create Bot Link")
    public void clickOnCreateBotLink() throws Exception {
        // Given

        // When
        botPageStepImplementation.countNumberOfGroupsCreatedAndClickONCreateBot();

        // Then
    }*/

    @ContinueOnFailure
    @Step("Ensure the added form field and table field for Domain <Invoices> and Language <Language> are displayed in designing window")
    public void getTheAddedFormAndTableFieldsValueFromDesigner(String domainName, String language) throws Exception {
        String results;
        switch (domainName) {
            case "Other":
                // Given
                baseClass.runBatchFile(System.getenv("getAddedFormAndTableFieldFileForDomainOther"));

                // When
                results = baseClass.validateDesignerLogs(System.getenv("getAddedFormAndTableFieldLogFileForDomainOther"));

                // Then
                assertThat(results).isEqualTo("Designer Launched : Pass :Additional Field Pass :Additional Table Pass :");
                break;
            case "Invoices":
                if (language.equalsIgnoreCase("German")) {
                    // Given
                    baseClass.runBatchFile(System.getenv("germanInvoiceVerificationBatchFile"));

                    // When
                    String results_German = baseClass.validateDesignerLogs(System.getenv("germanInvoiceVerificationLogFile"));
                    String results_Additional = baseClass.validateDesignerLogs(System.getenv("getAddedFormAndTableFieldLogFile"));

                    // Then
                    assertThat(results_German).isEqualTo("Unicode Number Detection : Pass :Unicode Text Detection : Pass :");
                    assertThat(results_Additional).isEqualTo("Designer Launched : Pass :Additional Field Pass :Additional Table Pass :");

                } else {
                    // Given
                    baseClass.runBatchFile(System.getenv("getAddedFormAndTableFieldFile"));

                    // When
                    results = baseClass.validateDesignerLogs(System.getenv("getAddedFormAndTableFieldLogFile"));

                    // Then
                    assertThat(results).isEqualTo("Designer Launched : Pass :Additional Field Pass :Additional Table Pass :");
                }
                break;

            case "Purchase Orders":
                // Given
                baseClass.runBatchFile(System.getenv("getAddedFormAndTableFieldFile"));

                // When
                results = baseClass.validateDesignerLogs(System.getenv("getAddedFormAndTableFieldLogFile"));

                // Then
                assertThat(results).isEqualTo("Designer Launched : Pass :Additional Field Pass :Additional Table Pass :");
                break;
        }


    }

    @ContinueOnFailure
    @Step("Click on browse button of create new instance page and upload 151 files,  limit exceed warning message should be displayed")
    public void UploadMoreThen150files() throws Exception {
        // Given

        // When
        newInstancePage.clickOnBrowseButton();
        Thread.sleep(500);

        baseClass.uploadAllFiles(smokeTestDocumentsFolder + "\\" + "MaxDocs", "MaxDocsCase");

        // Then
        assertThat(newInstancePage.getBrowseButtonErrorMessage()).isEqualTo("File uploads are limited to 150 files");
    }

    @ContinueOnFailure
    @Step("Refresh the page")
    public void refreshThePageAndWaitForPageLoad() {
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
    }

    @ContinueOnFailure
    @Step("Click on browse button of create new instance page and upload unsupported file, file type error message should be displayed")
    public void uploadUnsupportedFile() throws Exception {
        // Given

        // When
        newInstancePage.clickOnBrowseButton();
        baseClass.uploadAllFiles(smokeTestDocumentsFolder + "\\" + "BatchFiles", "UnsupportedCase");

        // Then
        assertThat(newInstancePage.getBrowseButtonErrorMessage()).isEqualTo("Allowed file types: jpg, jpeg, pdf, tiff, tif, png");
    }

    @ContinueOnFailure
    @Step("Click on browse button of create new instance page and upload above the max file size , limit to under 5 MB per file message should be displayed")
    public void uploadMaxSizeFile() throws Exception {
        // Given

        // When
        newInstancePage.clickOnBrowseButton();
        baseClass.uploadAllFiles(smokeTestDocumentsFolder + "\\" + "TaskBots", "MaxSizeCase");

        // Then
        assertThat(newInstancePage.getBrowseButtonErrorMessage()).isEqualTo("You have selected files above the max file size. Limit to under 5 MB per file.");

    }

    @ContinueOnFailure
    @Step("On Create New Instance Page select <Invoices> From Domain DropDown, add <Invoice No> as <Form> Field")
    public void addFormFieldToCheckDuplicateFieldName(String domainName, String fieldName, String fieldType) {
        // Given

        // When
        newInstancePage.selectDomain(domainName);
        newInstancePage.addField(fieldName, fieldType);

        // Then
        if (fieldName.equals("1234")) {
            assertThat(newInstancePage.otherFieldsTextBox().getText().isEmpty());
        } else {
            assertThat(newInstancePage.otherFieldsTextBoxErrorBox().isDisplayed()).isTrue();
        }
    }

    @ContinueOnFailure
    @Step("Error <No duplicate field names allowed> message should be displayed")
    public void duplicateFieldNameErrorMessage(String errorMessage) {
        // Given

        // When
        newInstancePage.clickOnFieldNameTextBox();

        // Then
        assertThat(newInstancePage.getDuplicateFieldNameErrorMessage()).isEqualTo(errorMessage);

    }

    @ContinueOnFailure
    @Step("Update instance name")
    public void updateInstanceName() throws Exception {
        newInstancePage.updateInstanceName();
    }

    @Step("Ensure user should not be allowed to Create Instance and error message <Error creating project> should be displayed")
    public void discardCreateInstance(String errorMessage) {
        // Given

        // When
        if (errorMessage.equals("Error creating project")) {
            assertThat(newInstancePage.getErrorMessage().isDisplayed());
            liListingPage.waitForMessageToBeDisplayed();
            assertThat(newInstancePage.getErrorCreatingprojectMessage().getText()).isEqualTo("Error creating project");
        } else {

            // Then
            assertThat(newInstancePage.getErrorMessage().isDisplayed());
        }
    }

    @Step("Ensure <Create instance and analyze> button is <disabled>")
    public void buttonIsDisabled(String createAndAnalyzeButton, String status) throws InterruptedException {
        // Given

        // When
        baseClass.waitForPageLoad();
        if (status.equals("disabled")) {
            // Then
            assertThat(newInstancePage.getCreateNewInstanceButtonDisableWebElement().isDisplayed());
        } else {
            assertThat(newInstancePage.getCreateAndanaylzeWebElement().isDisplayed());
        }

    }

    @Step({"Enter the <InstanceName> in <value> required Field","Select <English> from <Primary Language> dropdown","Select <Sample> file by clicking on <Browse> button"})
    public void checkForAllRequiredFields(String value,String fieldName) throws Exception {
        // Given
        instDesc = System.getenv("instance_Desc");

        // When
        switch (fieldName) {
            case "Instance Name":
                newInstancePage.insertRequiedFields(Driver.instName, "", "Select Input:", "Select Input:");
                break;
            case "Primary Language":
                newInstancePage.insertRequiedFields("", "", "Select Input:", value);
                break;
            case "Invoices":
                newInstancePage.insertRequiedFields("", "", value, "Select Input:");
                break;
            case "Browse":
                newInstancePage.insertRequiedFields("", "", "Select Input:", "Select Input:");
                newInstancePage.clickOnBrowseButton();
                Thread.sleep(500);
                newInstancePage.uploadFiles("English", "Invoices", value);
                break;
        }
    }

    @Step("Then <Uncheck> all the checked fields")
    public void uncheckAllFields(String unCheck) {
        // Given

        // When
        baseClass.waitForPageLoad();
        newInstancePage.unCheckAllFields();
    }

    @Step("Try to select <Select Input> from <primary langauge> dropdown error message should be displayed")
    public void getErrorMessageForDD(String lang, String dropDownName) {
        // Given

        // When
        newInstancePage.selectInputForPrimaryLanguage(lang, dropDownName);

        // Then
        assertThat(newInstancePage.getRequiredErrorMessage()).isEqualTo("Required");
    }

    @Step("After uploading upload texbox should not be blank")
    public void uploadTextBox() throws Exception {
        // Given

        // When
        newInstance.clickOnBrowseButton();

        // Then

        // Given

        // When
        newInstancePage.uploadFiles("English", "Invoices", "EditInstanceDocs");

        // Then
        assertThat(newInstancePage.inputFileTextBox()).isNotBlank();
        assertThat(newInstancePage.inputFileTextBox()).isEqualTo("You've selected 2 file(s) to upload");
    }

    @Step("On Create New Instance Page select <Invoices> From Domain DropDown, add <InvoiceFormField> as <Form> Field, try to edit the form field added, it should not be editable")
    public void editAddedFormField(String domainName, String fieldName, String fieldType) {
        // Given

        // When
        newInstancePage.selectDomain(domainName);
        newInstancePage.addField(fieldName, fieldType);

        // Then
        assertThat(newInstancePage.getAddedFormFieldElement()).isEqualTo("true");

    }

    @Step("Create Instance with File <fileName> with test data <Folder> in Domain of <domainName> and Language as <PrimaryLanguage>")
    public void createNewInstance(String fileName, String fileLocation, String domainName, String primaryLanguage) throws Exception {
        // Given
        instDesc = System.getenv("instance_Desc");
        instanceName = Driver.instName;
        // When

        newInstancePage.insertRequiedFields(instanceName, instDesc, domainName, primaryLanguage);
        newInstancePage.clickOnBrowseButton();
        Thread.sleep(500);
        baseClass.fileUpload(storyTestDocumentsFolder + fileLocation, fileName);
        // Then

    }

    @Step("add form field <form field> and table field <table field>")
    public void addadditionalfields(String fieldName, String tableFieldName){
        newInstancePage.addField(fieldName, "Form");
        newInstancePage.addField(tableFieldName, "TableField");
    }
}
