package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import pageobjects.*;
import org.openqa.selenium.support.PageFactory;
import utils.TestBase;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;


public class BotListingPageStepImplementation {
    private CreateNewInstancePage newInstancePage = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private LearningInstanceDetailPage liDetailsPage = PageFactory.initElements(Driver.webDriver, LearningInstanceDetailPage.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private BotListingPage botPage = PageFactory.initElements(Driver.webDriver, BotListingPage.class);
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    private static String groupName;


   /* @ContinueOnFailure
    @Step("Close the Designer window")
    public void closeTheDesignerWindow() throws Exception {
        botPage.closeDesigner();

    }*/

    @ContinueOnFailure
    @Step("Instance name on learning instance detail page is same as the instance name entered while creating instance")
    public void getInstancenameForWhichBotIsCreated() {
        // Given

        // when
        String learningInstancePageInstanceName = liDetailsPage.getInstanceName().getText();

        // Then
        assertThat(learningInstancePageInstanceName).isEqualTo(Driver.instName);
    }

    @ContinueOnFailure
    @Step("Click on Bot Link")
    public void clickOnBotLink() throws Exception {
        // Given

        // When
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(botPage.getBotsLink());
        liListingPage.clickonLinks("Bots");

        // Then
    }

    @ContinueOnFailure
    @Step("User Landed to Bot Page")
    public void checkBotPageTitle() throws InterruptedException {
        // Given

        // When
        botPage.waitUntilPageHeaderIsDisplayed();
        String actualTitle = botPage.getBotPageTitle().getText();

        // Then
        assertThat(actualTitle).contains("Bots");
        //System.out.println(actualTitle);
    }

    @ContinueOnFailure
    @Step("Verify values of search field DropDown")
    public void valuesOfSearchDropDown() {
        // Verify Domain DropDown have values.
        // Given
        List<String> expectedDomainDDList = newInstancePage.propertyNameAsList("botPageSearchDropDownValues");

        // When
        List<String> actualDomainDDList = botPage.getBotPageSearchDropDownValue();

        //Then
        assertThat(expectedDomainDDList).isNotNull();
        assertThat(actualDomainDDList).isNotNull();
        assertThat(actualDomainDDList).containsAll(expectedDomainDDList);
        assertThat(actualDomainDDList.size()).isEqualTo(expectedDomainDDList.size());
    }

    @ContinueOnFailure
    @Step("Select <Instance Name> from search DropDown in Bot Page")
    public void searchDDBotPage(String InstanceName) throws Exception {
        // Given

        // When
        botPage.selectInstanceName(InstanceName);
        String actualSelectedValue = botPage.getSearchFieldDropDown().getText();
        //System.out.print("Actual selected " + actualSelectedValue);

        // Then
    }

    @ContinueOnFailure
    @Step("Enter the instance name in search textbox")
    public void enterInstanceName() {
        // Given

        // When
        String actualName = botPage.searchForInstanceName(Driver.instName);

        // Then

    }
    @Step("Enter <Production> in search textbox")
    public void enterEnvironmentName(String environmentName) {
        // Given

        // When
        String actualName = botPage.searchForInstanceName(environmentName);

        // Then

    }

    @ContinueOnFailure
    @Step("Group name should be same as the group name for which bot was created for that particular instance")
    public void verifyInstanceAndGroupName() throws Exception {
        // Given

        // When
        int totalRows = liDetailsPage.totalRows();
        groupName = liDetailsPage.columnData(1, 1);

        // Then
        assertThat(totalRows).isNotZero();

        // Given
        if (totalRows > 0) {
//            baseClass.explicitWait("(//td[contains(text(),'AutoInstance')])[1]");

            // When
            //System.out.println("Group Name---" + liDetailsPage.columnData(1, 1));
            baseClass.explicitWait("(//tr[1])/td[1]");
            Thread.sleep(5000);
            String botPageGroupName = botPage.allBotsTableDataPath(1, 1);

            // Then
            assertThat(groupName).isEqualTo(botPageGroupName);
        }

    }

    @ContinueOnFailure
    @Step("Bot should be in <training> status")
    public void statusOfBotJustCreated(String statusTraining) throws Exception {
        // Given

        // When
        int totalRows = liDetailsPage.totalRows();

        // Then
        assertThat(totalRows).isNotZero();

        // Given
        if (totalRows > 0) {

            // When
            String botPageGroupName = liDetailsPage.columnData(1, 3);

            // Then
            assertThat(botPageGroupName).isEqualTo(statusTraining);
        }
    }

    @ContinueOnFailure
    @Step("Move bot from staging to production")
    public void moveBotFromStagingtoProduction() throws Exception {
        // Given

        // When
        int totalRows = liDetailsPage.totalRows();
        //System.out.print("Total Rows" + totalRows);

        // Then
        assertThat(totalRows).isNotZero();

        // Given
        if (totalRows > 0) {
            int rowNum = 1;

            // When
            botPage.moveFromStagingToProduction(rowNum);
            Driver.webDriver.navigate().refresh();
            botPage.waitForBotTableToBeDisplayed();
            botPage.searchForInstanceName(Driver.instName);
            System.out.println("=======================---------==============");

            // Then
        }
    }

    @ContinueOnFailure
    @Step("Move bot from production to staging")
    public void moveBotFromProductionToStaging() throws Exception {
        // Given

        // When
        int totalRows = liDetailsPage.totalRows();
        //System.out.print("Total Rows" + totalRows);

        // Then
        assertThat(totalRows).isNotZero();
        // Given

        if (totalRows > 0) {
            int rowNum = 1;

            // When
            botPage.moveProductionToStaging(rowNum);
            Driver.webDriver.navigate().refresh();
            baseClass.waitForPageLoad();
        }
    }

    @ContinueOnFailure
    @Step("When user go to bot listing page and validate bot run summary then all details should be available")
    public void checkAllDetailsBeforeRun() throws Exception {
        // Given

        // When
        liListingPage.clickonLinks("Bots");
        botPage.searchBots(Driver.instName);
        // baseClass.explicitWait("(//td[contains(text(),'AutoInstance')])[1]");

        // Then
        botPage.getAllDetailsOfBotSummary("Staging_BLP_Before_BotRun_B");

        // Given

        // When
        botPage.clickOnRunButton();
    }

    @ContinueOnFailure
    @Step("When user go to bot listing page and validate bot run summary before bot run for group <group_Name> then all details should be available")
    public void checkAllDetailsBeforeRunSanity(String groupType) throws Exception {
        // Given

        // When
        liListingPage.clickonLinks("Bots");
        botPage.searchBots(Driver.instName);
        baseClass.explicitWait("(//div[contains(text(),'AutoInstance')])[1]");

        if (groupType.equalsIgnoreCase("trainingGroup")) {
            botPage.clickOnGroupName(Driver.trainingGroupName);
        } else if (groupType.equalsIgnoreCase("successGroup")) {
            botPage.clickOnGroupName(Driver.trainingSuccessGroupName);
        }
        // Then
        botPage.getAllDetailsOfBotSummary("Staging_BLP_Before_BotRun_B");
    }

    @ContinueOnFailure
    @Step("And when user click on bot run, details should be updated after bot processing")
    public void checkAllDetailsAfteRun() throws Exception {
        // Given

        // When
        Thread.sleep(10000); //need to provide sleep as bot is in processing and we don't have any locator for that.
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(botPage.getSearchBox());
        botPage.searchBots(Driver.instName);
        baseClass.explicitWait("//span[text()='Files Processed']/following-sibling::span[1]");

        // Then
        botPage.getAllDetailsOfBotSummary("Staging_BLP_After_BotRun_B");
    }

    @ContinueOnFailure
    @Step("Validate all data after group move from Staging to production")
    public void validateAllDataAfterGroupMoveToStaging() throws Exception {
        // Given

        // When
        liListingPage.clickonLinks("Bots");
        botPage.searchBots(Driver.instName);
        botPage.moveGroupToProduction(Driver.trainingGroupName);
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        botPage.searchBots(Driver.instName);
//        baseClass.explicitWait("(//td[contains(text(),'AutoInstance')])[1]");
        botPage.clickOnGroupName(Driver.trainingGroupName);

        // Then
        botPage.getAllDetailsAfterMoveProduction("Production_BLP_B", Driver.trainingGroupName);
    }

    @ContinueOnFailure
    @Step("On Learning Instance page click on instance name")
    public void clickOnInstanceNameLinkOnLisingPage() throws Exception {
        // Given

        // When
        int totalRows = liDetailsPage.totalRows();

        // Then
        assertThat(totalRows).isNotZero();

        // Given
        if (totalRows > 0) {
            int rowNum = 1;

            // When
            liListingPage.clickOnInstanceNameLink(rowNum);
        }
    }

    @ContinueOnFailure
    @Step("Instance name should be listed on Bot page list")
    public void botPageInstanceNameList() {
        // Given

        // When
        List<String> getListOfInstanceFromTable = botPage.getListOfInstanceNameonBotPage();

        // Then
        assertThat(getListOfInstanceFromTable).contains(Driver.instName);
    }

    @ContinueOnFailure
    @Step("Search result should include the Group name <groupType>")
    public void tableListDisplayGroupName(String groupType) {
        // Given

        // When

        // Then
//        if(groupName2.equals("Group_2")){
//            System.out.println("Group 2 If True");
//            assertThat(botPage.getGroupNameFromBotTableList().contains(groupName2) || botPage.getGroupNameFromBotTableList().contains("Group_1"));
//
//        } else{
//            System.out.println("Group 2 Else True: " + groupName);
//            assertThat(botPage.getGroupNameFromBotTableList()).contains(groupName);
//        }

        if (groupType.equalsIgnoreCase("trainingGroup")) {
            assertThat(botPage.getGroupNameFromBotTableList().contains(Driver.trainingGroupName) || botPage.getGroupNameFromBotTableList().contains(Driver.trainingSuccessGroupName));

        } else {
            assertThat(botPage.getGroupNameFromBotTableList()).contains(Driver.trainingGroupName);

        }
        //

    }

    @ContinueOnFailure
    @Step("Run the bot once")
    public void runBotOnce() {
        // Given

        // When
        botPage.clickOnRunButton();

        // Then

    }

    @ContinueOnFailure
    @Step("Error message should be displayed <Staging document for this visionbot is already in running state.>")
    public void errorMessage(String errorMessage) {
        // Given

        // When
        liListingPage.waitForMessageToBeDisplayed();

        // Then
        assertThat(botPage.getErrorMessageAfterBotRunMoreTheOnce()).isEqualTo(errorMessage);

    }

    @ContinueOnFailure
    @Step("Run the bot once and move bot from staging to production")
    public void runBotAndMoveBotFromStagingToProduction() throws InterruptedException {
        // Given

        // When
        botPage.clickOnRunButton();
        int totalRows = liDetailsPage.totalRows();
        System.out.print("Total Rows" + totalRows);

        // Then
        assertThat(totalRows).isNotZero();

        // Given
        if (totalRows > 0) {
            int rowNum = 1;

            // When
            botPage.moveFromStagingToProduction(rowNum);

            // Then
        }

    }

    @ContinueOnFailure
    @Step("Ensure after training data should be extracted in preview & in IQTest for all pass fields")
    public void trainDocsWithAllFieldPass() throws Exception {
        // Given
        baseClass.runBatchFile(System.getenv("designerTrainingBatchFileWithAllfieldPass"));

        // When
        String results = baseClass.validateDesignerLogs(System.getenv("designerTrainingBatchFileWithAllfieldPassLogFile"));
        System.out.print("Output result: " + results);
        // Then
        assertThat(results).isEqualTo("Designer Launched : Pass :Designer Preview : Pass :");
    }

    @ContinueOnFailure
    @Step("Count of file successfully processed should be <1>")
    public void totalFileSuccessfullyProcesssed(String fileSuccessfullyProcessed) {
        // Given

        // When

        // Then
        assertThat(botPage.getTotalFileSuccessfullyProcessed()).isEqualTo(fileSuccessfullyProcessed);

    }

    @ContinueOnFailure
    @Step("Click on edit button of bot page")
    public void clickOnEditButtonOfBotPage() {
        // Given

        // When

        // Then
        botPage.clickOnEditButton(1);

    }

    @ContinueOnFailure
    @Step("Wait for bot run process to be completed")
    public void waitBotRunProcessToBeCompleted() throws InterruptedException {
        // Given

        // When
        Thread.sleep(5000);

    }

    @ContinueOnFailure
    @Step("Upload documents from IQBot Lite command")
    public void uploadDocsFromIQBotLiteCommand() throws Exception {
        baseClass.runBatchFile(System.getenv("iqBotLiteForAllFieldPassBatchFile"));

    }
    //..........................................................................................
    //Sanity
    //..........................................................................................

    @ContinueOnFailure
    @Step("Validate all data after group move from Staging to production <Group_Name>")
    public void validateAllDataAfterGroupMoveToStaging(String groupType) throws Exception {
        // Given

        // When
        ////  liListingPage.clickonLinks("Bots");
        //botPage.searchBots(Driver.instName);
        // When
        ///       botPage.searchBots(Driver.instName);
        if (groupType.equalsIgnoreCase("trainingGroup")) {
            botPage.moveGroupToProduction(Driver.trainingGroupName);
        } else if (groupType.equalsIgnoreCase("successGroup")) {
            botPage.moveGroupToProduction(Driver.trainingSuccessGroupName);
        }
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(botPage.getSearchBox());
        botPage.searchBots(Driver.instName);
////        baseClass.explicitWait("//span[text()='Files Processed']/following-sibling::span[1]");

        // Then
        if (groupType.equalsIgnoreCase("trainingGroup")) {
            botPage.clickOnGroupName(Driver.trainingGroupName);
            botPage.getAllDetailsAfterMoveProduction("Production_BLP_B", Driver.trainingGroupName);
        } else if (groupType.equalsIgnoreCase("successGroup")) {
            botPage.clickOnGroupName(Driver.trainingSuccessGroupName);
            botPage.getAllDetailsAfterMoveProduction("Production_BLP_B", Driver.trainingSuccessGroupName);
        }

    }

    @ContinueOnFailure
    @Step("validate files after bot and instance both are in production")
    public void validatedValidInvalidData() {
        //Given

        //When
        baseClass.waitForPageLoad();
        String validateFiles = botPage.fileValidateForBotAfterInstanceMoveProduction();
        if (botPage.getGroupNameFromHeader().equals(Driver.trainingGroupName)) {
            //Then
            assertThat(validateFiles).isEqualTo("Files validated 0 / 1 (0%)");

            // Given

            // When
            String validateInvalidFiles = botPage.invalidFileValidateForBotAfterInstanceMoveProduction();

            // Then
            assertThat(validateInvalidFiles).isEqualTo("Marked as invalid 0 / 1 (0%)");
        } else if (botPage.getGroupNameFromHeader().equals("Group_1")) {
            //Then
            assertThat(validateFiles).isEqualTo("Files validated 0 / 0 (0%)");

            // Given

            // When
            String validateInvalidFiles = botPage.invalidFileValidateForBotAfterInstanceMoveProduction();

            // Then
            assertThat(validateInvalidFiles).isEqualTo("Marked as invalid 0 / 0 (0%)");
        }
        botPage.timeSpendToValidate();
    }

    @ContinueOnFailure
    @Step("Run once bot for <group>")
    public void runBotGroupWise(String groupType) throws Exception {
        //Given

        //When
        baseClass.waitForPageLoad();

        //Then
        liListingPage.clickonLinks("Bots");
        botPage.searchBots(Driver.instName);
        if (groupType.equalsIgnoreCase("trainingGroup")) {
            botPage.runBotOnceByGroup(Driver.trainingGroupName);
        } else if (groupType.equalsIgnoreCase("successGroup")) {
            botPage.runBotOnceByGroup(Driver.trainingSuccessGroupName);
        }
        Driver.webDriver.navigate().refresh();
    }

    //...........................................................................................
    //...........................................................................................
    @ContinueOnFailure
    @Step("Ensure bot details for <group> is updated after  bot run")
    public void checkAllDetailsAfteRunGroupWise(String groupType) throws Exception {
        // Given

        // When
        Thread.sleep(10000); //need to provide sleep as bot is in processing and we don't have any locator for that.
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(botPage.getSearchBox());
        botPage.searchBots(Driver.instName);
        baseClass.explicitWait("//span[text()='Files Processed']/following-sibling::span[1]");

        // Then
        if (groupType.equalsIgnoreCase("trainingGroup")) {
            botPage.clickOnGroupName(Driver.trainingGroupName);
            botPage.getAllDetailsOfBotSummary("Staging_BLP_After_BotRun_B");
        } else if (groupType.equalsIgnoreCase("successGroup")) {
            botPage.clickOnGroupName(Driver.trainingSuccessGroupName);
            botPage.getAllDetailsOfBotSummary("Staging_BLP_After_BotRun_B_Gr2");
        }
    }

    @ContinueOnFailure
    @Step("Click on group <group>")
    public void click(String Group_Name) {
        botPage.clickOnGroupName(Group_Name);
    }

    @Step("Click on Group name from datatable it should be on same page")
    public void clickOnGroupName() {
        // Given
        String expectedValue =System.getenv("APP_URL");;

        // When
        botPage.clickOnGroupName(Driver.trainingGroupName);

        // Then
        assertThat(Driver.webDriver.getCurrentUrl()).isEqualTo(expectedValue+"/bots");
    }

    //----------------------------------------------------COG-2879------------------------------------------------------
    @Step("Ensure Bots breadcrumb is available")
    public void validateBotsBreadcrumbLink() {
        //By Rajesh Babu
        // Given
        baseClass.waitForPageLoad();
        // When
        //baseClass.explicitWait("//[@class = 'breadcrumbs-breadcrumb'][2]");
        // Then
        assertThat("Bots").isEqualTo(botPage.getBotsBreadcrumbtext());
    }

    @Deprecated
    @Step("Edit bot")
    public void editBotGroupWise()throws Exception {
        //Given

        //When

        //Then
        botPage.editBotByGroup(System.getenv("training_GroupName"));
        Thread.sleep(5000);
        //Driver.webDriver.navigate().refresh();
    }

    @ContinueOnFailure
    @Step({"Check for number of groups created and click on Create Bot with highest priority","After classification click on Create Bot Link","Click on Create Bot Link with highest priority"})
    public void countNumberOfGroupsCreatedAndClickONCreateBot() throws Exception {
        // Given
        int totalRows = liDetailsPage.totalRows();
        if (totalRows > 0) {
            groupName = liDetailsPage.columnData(1, 1);
            liDetailsPage.columnData(1, 9);

            // When
            liDetailsPage.clickOnBotLink(1);
            } else {
            System.out.println("Group not created");
        }
    }

}
