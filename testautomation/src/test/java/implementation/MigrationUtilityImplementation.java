package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageobjects.BotListingPage;
import pageobjects.LearningInstanceListingPage;
import pageobjects.LoginPage;
import pageobjects.MigrationUtilityPage;
import utils.TestBase;

import java.util.List;

import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MigrationUtilityImplementation {
    MigrationUtilityPage migration = PageFactory.initElements(Driver.webDriver, MigrationUtilityPage.class);
    LoginPage login = PageFactory.initElements(Driver.webDriver, LoginPage.class);
    LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    BotListingPage botPage = PageFactory.initElements(Driver.webDriver, BotListingPage.class);
    TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    public static String exportFilename;

    @ContinueOnFailure
    @Step("Open cognitive console with Migration-Utility should be redirected to Login-URL")
    public void migrationURL() {
        // Given
        String app_url = System.getenv("MigrationUtility_URL");

        // When
        Driver.webDriver.get(app_url);

        // Then
        assertThat(login.getPageText()).isEqualTo("Log in");
        assertThat(Driver.webDriver.getTitle()).isEqualTo("Login | IQ Bot");
        writeMessage("Page title is %s", Driver.webDriver.getTitle());

    }

    @ContinueOnFailure
    @Step("Open cognitive console with Migration-Utility URL after Login")
    public void migrationUtilityURL() throws Exception {

        //Given
        String app_url = System.getenv("MigrationUtility_URL");

        // When
        Driver.webDriver.get(app_url);
        baseClass.waitForPageLoad();

        // Then
        writeMessage("Page title is %s", Driver.webDriver.getTitle());
        assertThat(Driver.webDriver.getTitle()).isEqualTo("Automation Anywhere | Migration Utility");

    }

    @ContinueOnFailure
    @Step("User is verifying browser title as <pageTitle>")
    public void pageTitle(String pageTitle) throws Exception {

        //Given
        // when
        String title = Driver.webDriver.getTitle();
        // Then
        writeMessage("Page title is %s", title);
        assertThat(title).isEqualTo(pageTitle);

    }

    @ContinueOnFailure
    @Step("Total Learning instances count should match migration page with LI page")
    public void liCount() throws Exception {
        //Given
        String noOfInstancesTextOnLI = liListingPage.instanceCountsText();
        System.out.println(noOfInstancesTextOnLI);
        writeMessage("Total instances available on LI page : ", noOfInstancesTextOnLI);

        //When
        Driver.webDriver.get(System.getenv("MigrationUtility_URL"));
        baseClass.waitForPageLoad();
        String noOfInstancesTextOnMU = migration.totalInstancesCount();
        System.out.println(noOfInstancesTextOnMU);
        writeMessage("Total instances available on MU page : ", noOfInstancesTextOnLI);

        //Then
        assertThat(noOfInstancesTextOnLI).isEqualTo(noOfInstancesTextOnMU);

    }

    @ContinueOnFailure
    @Step("Total Bots count should match migration page with Bot page")
    public void botCount() throws Exception {
        //Given
        baseClass.waitForPageLoad();
        String noOfBotTextOnBotPage = botPage.botCountsText();
        System.out.println(noOfBotTextOnBotPage);
        writeMessage("Total bots are available on Bot page : ", noOfBotTextOnBotPage);

        //When
        Driver.webDriver.get(System.getenv("MigrationUtility_URL"));
        baseClass.waitForPageLoad();
        String noOfBotsTextOnMU = String.valueOf(migration.botCount());
        System.out.println(noOfBotsTextOnMU);
        writeMessage("Total bots are available on MU page : ");

        //Then
        assertThat(noOfBotTextOnBotPage).containsIgnoringCase(noOfBotsTextOnMU);
        System.out.println(noOfBotsTextOnMU);

    }


    @Step("All instances, Environemnt and no of bots should be listed in UI as shown in Learning instances listing page")
    public void tableComparision() throws Exception {
        //Given
        List lipageTable = liListingPage.instancelistToValidateMigrationPage();
        Driver.webDriver.get(System.getenv("MigrationUtility_URL"));
        Thread.sleep(5000);
        List migrationPageTable = migration.tablelistToValidateMigrationPage();

        //When
        Boolean a = lipageTable.equals(migrationPageTable);
        System.out.print(lipageTable);
        System.out.print(migrationPageTable);

        //Then
        assertThat(a).isTrue();
        System.out.println("List is same");

    }

    @ContinueOnFailure
    @Step("Breadcrumb check for Migration Utility page")
    public void breadcrumbCheck() {
        // Given

        //When
        String textLebelMU = migration.pageLebelText();
        writeMessage("The Migration Utility page lebel is : " + textLebelMU);
        System.out.println("The page lebel : " + textLebelMU);

        //Then
        assertThat("Migration Utility").isEqualTo(textLebelMU);

    }

    @ContinueOnFailure
    @Step("Page label check for Migration Utility page above instance table")
    public void pageLebelCheckAboveInstanceTable() {
        // Given

        //When
        String textLebelMU = migration.pageLabelAboveTable();
        writeMessage("The Migration Utility page label is : " + textLebelMU);
        System.out.println("The page label : " + textLebelMU);

        //Then
        assertThat("Migration Utility").isEqualTo(textLebelMU);

    }

    @ContinueOnFailure
    @Step("User should be on same page and same url after click on Migration Utility page lebel")
    public void pageLebelClickVerification() {
        //Given

        //When
        migration.clickOnPageLebel();
        baseClass.waitForPageLoad();
        String url = Driver.webDriver.getCurrentUrl();
        String textLebelMU = migration.pageLebelText();

        //Then
        writeMessage("The Migration Utility page label is : " + textLebelMU);
        System.out.println("The Migration Utility page label is : " + textLebelMU);
        assertThat("Migration Utility").isEqualTo(textLebelMU);
        System.out.println("The page URL is : " + url);
        assertThat(url).containsIgnoringCase("/migration-utility");
        System.out.println();


    }

    @ContinueOnFailure
    @Step("Select checkbox for <instance> instances")
    public void selectCheckbox(String instance) throws Exception {

        //Given

        //When
        migration.clickCheckBoxForAParticularInstance(instance);


        //Then

    }

    @ContinueOnFailure
    @Step("Table have three column(s) such as <instance Name>, <Envoronment> and other is <#of IQ Bots>")
    public void tableHeader(String col1, String col2, String col3) throws Exception {
        //Given

        //When
        String header = migration.tableHeader().getText();

        //Then
        assertThat(header).containsIgnoringCase(col1);
        assertThat(header).containsIgnoringCase(col2);
        assertThat(header).containsIgnoringCase(col3);

    }

    @ContinueOnFailure
    @Step("Click on Export button to open pop-up")
    public void export() {

        //Given

        //When
        baseClass.explicitWait("(//button)[4]");
        migration.clickOnExportButton();
        //Then


    }

    @ContinueOnFailure
    @Step("Export button is in disabled state")
    public void exportButtonDisabledState() {
        //Given
        //When
        baseClass.explicitWait("(//button)[4]");
        String all_classes = migration.stateOfExportButton();
        //Then
        assertThat(all_classes).containsIgnoringCase("commandbutton-button--disabled");
    }

    @ContinueOnFailure
    @Step("Export button is in Enabled state")
    public void exportButtonEnabledState() {
        //Given
        //When
        baseClass.explicitWait("(//button)[4]");
        String all_classes = migration.stateOfExportButton();
        //Then
        assertThat(all_classes).containsIgnoringCase("commandbutton-button--clickable");
    }


    @ContinueOnFailure
    @Step("Click on Cancel button on export or import pop-up")
    public void cancel() {

        //Given

        //When
        baseClass.explicitWait("//button[@name='cancel']");
        migration.clickOnCancelButton();

        //Then


    }

    @ContinueOnFailure
    @Step("Type your file name to export <1st_export>")
    public void fillTextBox(String nameOfFile) {

        //Given

        //When
        baseClass.explicitWait("//input[@name='confirmation-input-name']");
        migration.typeFileName(nameOfFile);


        //Then

    }

    @ContinueOnFailure
    @Step("Click on I understand button to export or import")
    public void toExport() throws Exception {

        //Given

        //When
        baseClass.explicitWait("//button[@name='accept']");
        Thread.sleep(3000);
        migration.clickOnAcceptButton();

        //Then


    }

    @ContinueOnFailure
    @Step("User is verifying text present in export pop-up after click on export button")
    public void paragraphText() {
        //Given

        //When
        baseClass.explicitWait("//p");
        exportFilename = migration.exportFileName().trim();
        String paragraph = migration.exportTextPara().trim();
        //Then
        assertThat(paragraph).isEqualToIgnoringCase("Learning Instance Export is in progress. This is a resource intensive activity and will take some time. Go grab a cup of coffee!!");

    }

    @ContinueOnFailure
    @Step("Title of the pop up, and the header of the text box")
    public void exportPopUPLabel() {
        //Given

        //When
        String paragraph = migration.exportPopUPLabel().trim();
        //Then
        assertThat(paragraph).isEqualToIgnoringCase("Are you sure you'd like to export?");
    }

    @ContinueOnFailure
    @Step("TextBox Should give error message as <errormsg> when text box is given value as <Value>")
    public void exportPopUPLabel(String errormsg, String Value) {
        //Given

        //When
        migration.typeFileName(Value);
        String errorOnTextBox = migration.textBoxError();
        //Then
        assertThat(errorOnTextBox).contains(errormsg);

    }

    @ContinueOnFailure
    @Step("TextBox does not give any message when text box is given value as <Value>")
    public void exportPopUPLabel(String Value) {
        //Given

        //When
        migration.typeFileName(Value);
        //String errorOnTextBox = migration.textBoxError();
        //Then
        WebElement button = Driver.webDriver.findElement(By.xpath("//DIV[@class='announcement-title']"));
        Assert.assertEquals(false, button.isDisplayed());
    }

    @ContinueOnFailure
    @Step("Default Value of Text box is <Learning Instance Backup>")
    public void exportPopUPTextBoxValue(String Value) {
        //Given

        //When
        //migration.typeFileName(Value);
        String typeFileName = migration.typeFileNameValue();
        //Then
        assertThat(typeFileName).contains(Value);

    }

    @ContinueOnFailure
    @Step("Verify the progress page and it's text")
    public void exportProgress() {
        //Given
        baseClass.explicitWait("//div[@class='aa-grid-row padding horizontal-center vertical-center']");
        String filename = "suresh";
        //When

        String exportProgressDefaulttext = migration.exportProgressDefaulttext();
        String exportfilepath = migration.exportfilepath();
        String exportProgressNote = migration.exportProgressNote();
        //Then
        assertThat(exportProgressDefaulttext).isEqualToIgnoringCase("Learning Instance Export is in progress. This is a resource intensive activity and will take some time. Go grab a cup of coffee!!");
        assertThat(exportfilepath).contains("Your exported Learning instance archive will be available at");
        assertThat(exportProgressNote).isEqualToIgnoringCase("Note: The IQBot Platform will be available for normal usage after finishing of the Export.");

    }

    @ContinueOnFailure
    @Step("Refresh the page for migration page")
    public void refreshAndWait() {
        //Given

        //When
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        //Then
        baseClass.explicitWait("(//button)[3]");

    }

    @ContinueOnFailure
    @Step("Select checkbox for current automation running instances")
    public void selectCheckbox() throws Exception {

        //Given

        //When
        migration.clickCheckBoxForAParticularInstance(Driver.instName);


        //Then

    }

    @ContinueOnFailure
    @Step("on click all Check box should be <state> against every instance.")
    public void verifyCheckBox(String state) throws Exception {

        //Given

        //When

        int a = Integer.parseInt(migration.totalInstancesCount().substring(11, 13));
        for (int i = 1; i <= a; i++) {
            migration.exportTableCheckBoxAction(i);

        }
        //Then
        if (state.equals("selectable"))
            for (int i = 1; i <= a; i++) {
                String state1 = migration.exportTableCheckBoxVerification(i);
                assertThat(state1).containsIgnoringCase("datatable-row--selected");
            }
    }

    @ContinueOnFailure
    @Step("Verify the scroll bar is present or not")
    public void verifyScrollBar() {
        //Given
        Driver.webDriver.manage().window().setSize(new Dimension(400, 400));
        //When
        Boolean horizontalScrollbar = migration.verifyHorizontalScrollBar();
        Boolean verticalScrollbar = migration.verifyVerticalScrollBar();
        //Then
        assertThat(horizontalScrollbar).isTrue();
        assertThat(verticalScrollbar).isTrue();


    }

    @ContinueOnFailure
    @Step("Verify the toastr notification showing <message>")
    public void toastrNotification(String expected) {
        //Given

        //When
        baseClass.explicitWait("//div[contains(@class,'app-notification-container')]");
        String message = migration.toastrNotification();


        //Then
        assertThat(message).isEqualToIgnoringCase(expected);
    }
    @ContinueOnFailure
    @Step("Click on Import button to open pop-up")
    public void importButton() {
        //Given

        // When
        migration.clickOnImportButton();
        //Then

    }
    @ContinueOnFailure
    @Step("Check check-box for Export file to Import")
    public void selectFileToImport() throws Exception {
        //Given

        //When
        baseClass.explicitWait("//button[@name='cancel']");
        migration.clickOnCheckbox(exportFilename);
        //Then

    }
    @ContinueOnFailure
    @Step("Verify the error message on import or export pop-up <Formatting must not have spaces at the beginning or end.>")
    public void errorMessageOnPopup(String msg) {
        //Given

        //When
        baseClass.explicitWait("//button[@name='cancel']");
        migration.clickOnAcceptButton();
        String str = migration.popupErrorMessage();
        System.out.println("Error Message on pop-up : " + str);

        //Then
        assertThat(msg).isEqualToIgnoringCase(str);

    }
    @ContinueOnFailure
    @Step("Validate text when no instance available in UI")
    public void textNoInstance() {
        //Given

        //When
        baseClass.waitForPageLoad();
        String msg=migration.textForNoInstance().trim();

        //Then
        assertThat(msg).isEqualToIgnoringCase("No current learning instances.");

    }
}
