package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.assertj.core.util.Compatibility;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import pageobjects.*;
import utils.TestBase;

import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LoginPageStepImplementation {

    private LoginPage login = PageFactory.initElements(Driver.webDriver, LoginPage.class);

    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);

    @ContinueOnFailure
    @Step("Open cognitive console logIn page")
    public void implementation1() {
        // Given
        String app_url = System.getenv("APP_URL");

        // When
        Driver.webDriver.get(app_url + "/");
        baseClass.waitForPageLoad();

        // Then
        assertThat(login.getPageText()).isEqualTo("Log in");
        writeMessage("Page title is %s", Driver.webDriver.getTitle());
    }

    @ContinueOnFailure
    @Step("When user login into cognitive console with valid user name as <username> and valid password as <password>")
    public void logIntoCognitiveServices(String username, String password) throws Exception {
        // Username & Password are given
        // Given
        baseClass.explicitWait("//input[@name='username']");

        // When
        login.doLogin(username, password);

        // Then
        baseClass.waitForPageLoad();
    }

    @ContinueOnFailure
    @Step("When user login into cognitive console with user name as <username> and password as <password>")
    public void logIntoCognitiveServices_CheckError(String username, String password) throws Exception {
        // Username & Password are Given
        baseClass.explicitWait("//input[@name='username']");

        // When
        login.doLogin(username, password);

        // Then
        baseClass.explicitWait("//ul[@class='aa-auth-error-message']");

    }

    @ContinueOnFailure
    @Step("Then error message <errorMessage> should be displayed")
    public void invalidLoginError(String errorMessage) {
        // Given

        // When

        // Then
        assertThat(login.getLoginErrorMessage()).isEqualToIgnoringCase(errorMessage);
    }

    @ContinueOnFailure
    @Step("Open cognitive console logIn page with service role user between executing scripts")
    public void implementationLogin() {
        // Given
        String app_url = System.getenv("APP_URL");

        // When
        Driver.webDriver.get(app_url + "/");

        // Then
        //   assertThat(Driver.webDriver.getTitle()).contains("Login | IQ Bot");

        writeMessage("Page title is %s", Driver.webDriver.getTitle());
    }

    @ContinueOnFailure
    @Step("Quit browser")
    public void quitBrowser() {
        Driver.webDriver.quit();
    }

    @Step("Ensure user should not be able to login")
    public void notLogin() {
        // Given

        // When

        // Then
        assertThat(login.getPageText()).isEqualTo("Log in");
        assertThat(Driver.webDriver.getCurrentUrl()).isEqualTo(System.getenv("APP_URL") + "/login");
    }

    @ContinueOnFailure
    @Step("Open <Enigma> console logIn page")
    public void loginUrl(String url) {
        // Given
        String app_url = System.getenv("Enigma_URL");

        // When
        Driver.webDriver.get(app_url + "/");
        baseClass.waitForPageLoad();

        // Then
        assertThat(login.getPageText()).isEqualTo("Log in");

    }
}
