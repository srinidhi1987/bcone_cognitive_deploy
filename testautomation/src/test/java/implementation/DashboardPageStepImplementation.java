package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.openqa.selenium.support.PageFactory;
import pageobjects.*;
import utils.TestBase;

import java.util.ArrayList;
import java.util.List;
import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.assertThat;

public class DashboardPageStepImplementation {

    private PerformanceReportPage performanceReportPage = PageFactory.initElements(Driver.webDriver, PerformanceReportPage.class);
    private DashboardPage dashBoardPage = PageFactory.initElements(Driver.webDriver, DashboardPage.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private long sum = 0, totalProcessed;
    private List<String> filteredListWithFPAndSTPandAccuracyValuesFromDashboard = new ArrayList<>();
    private String getSTPValueForAllInstanceFromDashboardPage;
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);

    @ContinueOnFailure
    @Step("Then dashboard should be displayed for service user")
    public void waitForDashboard() throws InterruptedException {
        // Given

        // When

        // Then
//        assertThat(dashBoardPage.getDashboardLink().getText()).isEqualTo("Dashboard");
        assertThat(Driver.webDriver.getTitle()).contains("Automation Anywhere | Dashboards");
        writeMessage("Page title is %s", Driver.webDriver.getTitle());
    }

    @ContinueOnFailure
    @Step("Get the total number of files processed on dashboard page")
    public void getTotalNumberOfFilesProcessed() {
        // Given
        totalProcessed = Long.valueOf(dashBoardPage.getTotalFilesProcessed().replaceAll(",", ""));
        //System.out.println("Total Files Processed" + totalProcessed);

        // When

        // Then

    }

    @ContinueOnFailure
    @Step("Get all the file processed from each domain")
    public void getallFileProcessedFromEachDomain() {
        // Given

        // When
        List<String> allFP = dashBoardPage.getListFP();
        for (String number : allFP) {
            number = number.replaceAll(",", "");
            sum += Integer.parseInt(number);
        }
        //System.out.println("sum of all elements=" + sum);

        // Then
    }

    @ContinueOnFailure
    @Step("Check total number of files Processed should be same as sum of file processed in particular domain which are in production")
    public void totalNumberOfFilesProcessedIsEqualToSumOfFPInParticulardomainWhichAreInProduction() {
        // Given

        // When

        // Then
        System.out.println("Total Processed" + totalProcessed+ "Sum" +sum);
        assertThat(totalProcessed).isEqualTo(sum);
    }

    @ContinueOnFailure
    @Step("Click on Dashboard Link")
    public void clickOnDashBoardLink() throws Exception {
        // Given
        baseClass.waitForPageLoad();

        // When
        liListingPage.clickonLinks("Dashboards");
    }

    public List<String> displayValueOfAllInstance(int i) {
        // Given

        // When
        List<String> dashBoardInstanceDetailList = dashBoardPage.getAllInstanceDetail();
        filteredListWithFPAndSTPandAccuracyValuesFromDashboard.add(dashBoardInstanceDetailList.get(i).substring(dashBoardInstanceDetailList.get(i).indexOf("production") + 10));
        return filteredListWithFPAndSTPandAccuracyValuesFromDashboard;
    }

    @ContinueOnFailure
    @Step("Get the STP value from Dashboard page")
    public void getSTPValueFromDashboardpage() {
        // Given

        // When
        getSTPValueForAllInstanceFromDashboardPage = dashBoardPage.getSTPValuefromDashboardPageForAllInstance();
    }

    private static final int FILES_UPLOADED = 0, FILES_UPLOADED_IN_PROCESSINGrESULTS = 1, FILES_SUCCESSFULLY_PROCESSED = 2;

    @ContinueOnFailure
    @Step("Click on each instance which are in production and get sum of file successfully Processed and sum of files uploaded and calculate STP")
    public void getSumOfFilesSPAndFilesUploadedByClickingOnEachInstanceWhichAreInProduction() throws Exception {
        // Given
        List<String> instancedetailsDashboardPage = new ArrayList<>();
        List<String> dashboardPageInstanceDetails = new ArrayList<>();
        List<String> instanceTotalList = new ArrayList<>();
        double sumOffileSuccesssfullyPocessed = 0, sumOfFilesUploaded = 0;
        int i;

        // When
        // Get the list of instance which are in production
        List<String> listOfInstanceOnProduction = performanceReportPage.getDashBoardInstanceName();
        //System.out.println(listOfInstanceOnProduction);

        // if we have instance which are in production
        if (listOfInstanceOnProduction.size() > 0) {
            for (i = 0; i < listOfInstanceOnProduction.size(); i++) {
                baseClass.waitForPageLoad();
                baseClass.waitForElementClick(dashBoardPage.getHeaderOfInstance());
                // Step-3 Click on instance from list
                String productionInstanceName = listOfInstanceOnProduction.get(i);
                dashBoardPage.clickOnInstance(productionInstanceName);
                baseClass.waitForPageLoad();
                // Then
                assertThat(performanceReportPage.getPerformanceReportpageTitle().getText()).isEqualTo("Performance Report");

                // Given

                // Get the list of file processed
                // When
                List<String> getAllValuesFormPerformanceReportPage = performanceReportPage.getPerformanceReportFilesProcessedSTPAccuracyAndValuesOfProcessingResults();
                sumOffileSuccesssfullyPocessed += Integer.parseInt(getAllValuesFormPerformanceReportPage.get(FILES_SUCCESSFULLY_PROCESSED).replaceAll(",", "").trim());
                sumOfFilesUploaded = sumOfFilesUploaded + Integer.parseInt(getAllValuesFormPerformanceReportPage.get(FILES_UPLOADED).replaceAll(",", "").trim());
                liListingPage.clickonLinks("Dashboards");
            }
            double calculateSTP = (sumOffileSuccesssfullyPocessed) / (sumOfFilesUploaded);
            //System.out.println("Files Successfully Processed =" + sumOffileSuccesssfullyPocessed + "  Files Uploaded =" + sumOfFilesUploaded);

            String STP = Double.toString(calculateSTP * 100);
            STP = STP.substring(0, STP.indexOf("."));
            String expectedSTP;
            if (sumOffileSuccesssfullyPocessed == 0.0 && sumOfFilesUploaded == 0.0) {
                expectedSTP = "0%";
            } else {
                expectedSTP = STP + "%";
            }
            //System.out.println("STP = " + expectedSTP);

            // Then
            assertThat(getSTPValueForAllInstanceFromDashboardPage).isEqualTo(expectedSTP);
        }
    }

    @ContinueOnFailure
    @Step("Validate values of learning instance when instance is in staging")
    public void learningInstanceDetailOnStaging() {
        // Given
        String expectedListStagingvalues;

        // When
        baseClass.waitForPageLoad();
        expectedListStagingvalues = "[40%" + Driver.instName + "stagingThis instance is created using automation script.Domain: InvoicesFiles Uploaded: 5training, " + Driver.instName + "]";

        // Then
        //System.out.println(dashBoardPage.getStagingInstanceValuesFromDashBoardPage().toString());
        assertThat(dashBoardPage.getStagingInstanceValuesFromDashBoardPage().toString()).isEqualTo(expectedListStagingvalues);

    }

    @ContinueOnFailure
    @Step("Previously created Instance should not be displayed and user will be asked to <Create an instance>")
    public void noInstanceDisplayed(String createInstanceText) {
        // Given

        // When

        // Then
        assertThat(dashBoardPage.getDashboardPageInstanceIcon().isDisplayed());
        assertThat(dashBoardPage.getTextCreateInstance()).isEqualTo(createInstanceText);

    }

    @ContinueOnFailure
    @Step("Previously created Instance should be displayed")
    public void instanceDisplayed() {
        // Given

        // When

        // Then
        assertThat(dashBoardPage.getDashboardPageInstanceNameText().isDisplayed());
    }

    @Step("Click on browser <forward> button")
    public void clickOnBrowserBackButton(String navigateForwardOrBack) {
        // Given
        if (navigateForwardOrBack == "forward") {
            Driver.webDriver.navigate().forward();
        } else {
            Driver.webDriver.navigate().back();
        }

        // Then

    }

    @Step("Click on instance which is on <staging>")
    public void clickOnStagingInstance(String environmentName) {
        // Given
        String expectedValue = System.getenv("APP_URL");

        // When
        dashBoardPage.clickOnStagingInstance();

        // Then
        assertThat(Driver.webDriver.getCurrentUrl()).isEqualTo(expectedValue+"/dashboard");
        assertThat(dashBoardPage.getDashboardPageReference().getText()).isEqualTo("Dashboard");
        assertThat(Driver.webDriver.getTitle()).contains("Automation Anywhere | Dashboards");


    }

    @Step("Hover text for <Dashboard> Link")
    public void getHoverTextforAllLiinks(String elementHover) {
        // Given

        // When

        // Then
        switch (elementHover) {
            case "Dashboard":
                assertThat(dashBoardPage.getHoverText(elementHover)).isEqualTo("Dashboard");
                break;
            case "Learning Instances":
                assertThat(dashBoardPage.getHoverText(elementHover)).isEqualTo("Learning Instances");
                break;
            case "Bots":
                assertThat(dashBoardPage.getHoverText(elementHover)).isEqualTo("Bots");
                break;
        }
    }

    @Step("Click on page reference <Dashboard>")
    public void clickOnPageReference(String pageName) {
        // Given

        // When
        dashBoardPage.clickOnPageReference(pageName);

    }

    @Step("Ensure user is on <Dashboard> page")
    public void currentPage(String currrentPage) {
        // Given
        String expectedValue = System.getenv("APP_URL");

        // When

        // Then
        assertThat(dashBoardPage.getDashboardPageReference().getText()).isEqualTo(currrentPage);
        assertThat(Driver.webDriver.getCurrentUrl()).isEqualTo(expectedValue+"/dashboard");
    }

    @Step("Page title of <Dashboard> Page")
    public void pageTitle(String expectedPageTitle) {
        // Given

        // When

        // Then
        assertThat(dashBoardPage.getPageReferenceText()).isEqualTo(expectedPageTitle);
    }

    @Step("Navigate to Report Page")
    public void navigateToReportpage() {
        // Given
        String currenturl = Driver.webDriver.getCurrentUrl();

        // When
        String url = currenturl + "/report";
        Driver.webDriver.navigate().to(url);

        // Then
    }

    @Step("It should not navigate to report Page")
    public void assertFailureTonavigateToReportpage() {
        // Given

        // When

        // Then
        // assertThat(performanceReportPage.getPerformanceReportpageTitle().isDisplayed()).isFalse();
    }

}
