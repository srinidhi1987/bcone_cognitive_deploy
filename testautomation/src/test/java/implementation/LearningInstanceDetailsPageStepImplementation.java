package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.openqa.selenium.support.PageFactory;
import pageobjects.*;
import utils.TestBase;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.assertThat;

public class LearningInstanceDetailsPageStepImplementation {

    private CreateNewInstancePage newInstancePage = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private LearningInstanceDetailPage liDetailsPage = PageFactory.initElements(Driver.webDriver, LearningInstanceDetailPage.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private CreateNewInstancePage newInstance = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private BotListingPage botPage = PageFactory.initElements(Driver.webDriver, BotListingPage.class);
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    private TrainingPage trainingPage = PageFactory.initElements(Driver.webDriver, TrainingPage.class);
    private static String filesCount_Before_EditInstance;
    private String filesCount_After_EditInstance;
    private String smokeTestDocumentsFolder = System.getenv("test_Documents_FolderPath");
    private java.util.List<String> actualGeneralInformationValues = new ArrayList<>();
    private java.util.List<String> actualStagingAndProductionResult = new ArrayList<>();
    private List<String> actualHeaderStagingAndProductionResult = new ArrayList<>();


    @ContinueOnFailure
    @Step("When user edit instance and upload files")
    public void editInstanceAndUploadFiles() throws Exception {
        // Given

        // When
        baseClass.waitForElementClick(liDetailsPage.getStagingLabel());
        filesCount_Before_EditInstance = liDetailsPage.getClassifiedGroupFilesCount(Driver.documentCountGroupName);
        System.out.println("Before count" + filesCount_Before_EditInstance);
        liDetailsPage.editInstance();
        newInstance.clickOnBrowseButton();
        Thread.sleep(500);
        newInstancePage.uploadFiles("English", "Invoices", "EditInstanceDocs");

        // Then

        // Given
     /*   String filePath = "C:/IQBot_TestData/Edit.cmd";

        // When
        String multipleFilenames = newInstancePage.getallFilenames(filePath);
        baseClass.fileUpload(smokeTestDocumentsFolder + "\\" + "EditInstanceDocs", multipleFilenames);*/
    }

    @ContinueOnFailure
    @Step("Then on save button click uploaded files should be classified")
    public void addNewFilesToInstance() {
        // Given

        // When
        liDetailsPage.clickOnSave();

        // Then

    }

    @ContinueOnFailure
    @Step("Ensure that after document classification total files and groups should be updated")
    public void validateGroupFilesCount() throws Exception {
        // Given
        //System.out.print("Files Count Before Edit Instance: " + filesCount_Before_EditInstance);
        filesCount_After_EditInstance = liDetailsPage.getClassifiedGroupFilesCount(Driver.documentCountGroupName);
        filesCount_After_EditInstance = filesCount_After_EditInstance.trim();
        filesCount_Before_EditInstance = filesCount_Before_EditInstance.trim();
        int before, after;
        //System.out.println("Files Count After Edit Instance:" + filesCount_After_EditInstance);
        before = Integer.parseInt(filesCount_Before_EditInstance);
        after = Integer.parseInt((filesCount_After_EditInstance));
        //System.out.print("Before:" + before);
        //System.out.print("After:" + after);

        // When

        // Then
        assertThat(after).isGreaterThan(before);
    }

    @ContinueOnFailure
    @Step("When user click on CreateBot link visionbot designer should be launched")
    public void launchDesignerAndTrainVisionbot() throws Exception {
        // Given

        // When
        baseClass.waitForElementDisplay(liListingPage.getLielement());
        liDetailsPage.launchDesignerForSelectedGroup(Driver.trainingGroupName);
        // baseClass.closePopUp();

        // Then

    }
    //................................................................................................
    //Sanity

    //.................................................................................................
    @ContinueOnFailure
    @Step("When user click on CreateBot link visionbot designer should be launched <training_group>")
    public void launchDesignerAndTrainVisionbot(String groupType) throws Exception {
        // Given

        // When
        baseClass.waitForElementDisplay(liListingPage.getLielement());

        // Then
        if (groupType.equalsIgnoreCase("trainingGroup")) {
            liDetailsPage.launchDesignerForSelectedGroup(Driver.trainingGroupName);
        } else if (groupType.equalsIgnoreCase("successGroup")) {
            liDetailsPage.launchDesignerForSelectedGroup(Driver.trainingSuccessGroupName);
        }
    }

    @ContinueOnFailure
    @Step("Train Group for successful processing")
    public void trainAnotherGroup() throws Exception {
        //Given

        //When

        //Then
        baseClass.runBatchFile(System.getenv("designerTrainingBatchFileTwo"));
    }

    @ContinueOnFailure
    @Step("Ensure that uploaded production files should be classified for selected Instance sanity")
    public void validateProductionFilesUploadAndClassificationSanity() throws Exception {
        // Given

        // When
        baseClass.waitForSpinner();

        // Then
        // liDetailsPage.getAllValueInTable(System.getenv("fileCount_GroupName"), "production", "Production_LIDP_After_Upload_I");

    }

    @ContinueOnFailure
    @Step("Ensure that uploaded production files should be classified for selected Instance")
    public void validateProductionFilesUploadAndClassification() throws Exception {
        // Given

        // When
        baseClass.waitForSpinner();

        // Then
        liDetailsPage.getAllValueInTable(Driver.documentCountGroupName, "production", "Production_LIDP_After_Upload_I");

    }


    //................................................................................................
    //SanityEnd
    //................................................................................................
    @ContinueOnFailure
    @Step("Ensure after training data should be extracted in preview & in IQTest")
    public void trainVisionbotAndValidatePreview() throws Exception {
        Thread.sleep(3000);
        // Given
        baseClass.runBatchFile(System.getenv("designerTrainingBatchFile"));

        // When
        String results = baseClass.validateDesignerLogs(System.getenv("designerLogFile"));

        // Then
        assertThat(results).isEqualTo("Designer Launched : Pass :Designer Preview : Pass :Designer IQTest : Pass :");
    }

    @ContinueOnFailure
    @Step("Then uploaded documents should be classified for selected Instance")
    public void validateProductionDocumentsClassification() throws Exception {
        // Given

        // When
        //  baseClass.waitForElementClick(liListingPage.getLielement());
        baseClass.waitForPageLoad();
        //    liListingPage.searchForInstanceName(Driver.instName);
        //     liListingPage.clickOnInstanceName(Driver.instName);
        //    baseClass.waitForSpinnerToAppear();

        // Then
        assertThat(liDetailsPage.checkSpinner()).isTrue();
    }

    @ContinueOnFailure
    @Step("On learning instance details page documents should be classified in groups")
    public void validateLearningInstancePage() throws Exception {
        // Given

        // When

        // Then
        assertThat(liDetailsPage.getTotalClassifiedGroupCount()).isEqualToIgnoringCase("3");
    }

    @ContinueOnFailure
    @Step("Then classified documents count should be match with uploaded documents count")
    public void validateTrainingDocumentsCountGroupWise() throws Exception {
        // Given

        // When
        Thread.sleep(5000);
        // Then
        liDetailsPage.getAllValueInTable(Driver.documentCountGroupName, "staging", "Staging_LIDP_CreateInstance_I");
    }

    @ContinueOnFailure
    @Step("Ensure that Training Summary is also updated based on staging data")
    public void validateTrainingSummaryForStaging() throws Exception {
        // Given

        // When

        // Then
        liDetailsPage.validateTrainingSummary("Staging_LIDP_CreateInstance_I");
    }

    @ContinueOnFailure
    @Step("Ensure that Training Summary is also updated based on staging data after edit Instance")
    public void validateTrainingSummaryAfterEdit() throws Exception {
        // Given

        // When

        // Then
        liDetailsPage.validateTrainingSummary("Staging_BLP_Before_BotRun_I");
    }


    @ContinueOnFailure
    @Step("Ensure that Training Summary is also updated based on production data for <testPhase>")
    public void validateTrainingSummaryForProduction(String testPhase) throws Exception {
        // Given

        // When

        // Then
        if (testPhase.equalsIgnoreCase("Smoke")) {
            liDetailsPage.validateTrainingSummary("Production_LIDP_After_Upload_I");
        } else if (testPhase.equalsIgnoreCase("Sanity")) {
            liDetailsPage.validateTrainingSummary("Production_LIDP_After_Upload_G2");
        }
    }

    @ContinueOnFailure
    @Step("Wait for documents processing in Learning Instance Details Page")
    public void waitForDocumentProcessing() throws InterruptedException {
        // Given

        // When
        Thread.sleep(5000);
        baseClass.waitForSpinnerToAppear();
        baseClass.waitForSpinner();
        baseClass.waitForYetToBeClassifiedToDisappear();
        //   Driver.webDriver.navigate().refresh();
        // baseClass.waitForPageLoad();

        // Then

    }

    @ContinueOnFailure
    @Step("Wait for page Load")
    public void waitForPageLoad() {
        baseClass.waitForPageLoad();
    }


    public static final int INSTANCE_NAME_TITLE = 0, INSTANCE_NAME = 1;

    @ContinueOnFailure
    @Step({"Instance details page exist","User landed to learning instance detail page"})
    public void getDetailsPage() throws InterruptedException {
        // Given

        // When
        baseClass.explicitWait("//a[@class='aa-link aa-grid-row vertical-center']");

        // Then
        assertThat(liDetailsPage.getDetailsPageHeading().get(INSTANCE_NAME_TITLE)).isEqualTo("Learning Instances");
        assertThat(liDetailsPage.getDetailsPageHeading().get(INSTANCE_NAME)).isEqualTo(Driver.instName);
    }

    @ContinueOnFailure
    @Step("Check Edit button exist")
    public void editButtonExist() {
        // Given

        // When
        String editButtonText = liDetailsPage.getEditbuttonText();

        // Then
        assertThat(editButtonText).isEqualTo("Edit");
    }

    @ContinueOnFailure
    @Step("By clicking on Edit button Delete Instance button should be displayed")
    public void clickOnEditButton() throws InterruptedException {
        // Given

        // When
        liDetailsPage.editInstance();

        // Then
        assertThat(liDetailsPage.getDeleteButtonText()).isEqualTo("Delete Instance");

    }

    @ContinueOnFailure
    @Step("By clicking on deleteInstance button content modal should appear with enabled Cancel button and disabled I understand,please delete button")
    public void clickOnDeleteInstanceButton() throws InterruptedException {
        // Given

        // When
        liDetailsPage.clickOnDeleteInstanceButton();
        liDetailsPage.waitForElementDisplay(liDetailsPage.getModalContent());

        // Then
        assertThat(liDetailsPage.checkForContentModalExist()).isTrue();
        assertThat(liDetailsPage.checkFordeleteConfirmNameTextboxExist()).isTrue();
        assertThat(liDetailsPage.checkForCancelButtonExistAndEnabled()).isTrue();
        assertThat(liDetailsPage.checkForConfirmDeleteButtonExist()).isTrue();
    }

    @ContinueOnFailure
    @Step("Enter the instance name on the Textbox which you want to delete")
    public void enterTheInstanceName() {
        // Given

        // When
        liDetailsPage.enterTheInstanceName(Driver.instName);

        // Then
        assertThat(liDetailsPage.checkForConfirmDeleteButtonIsEnabled()).isTrue();
    }

    @ContinueOnFailure
    @Step("Click on I understand, please delete")
    public void clickOnIUnderstandPleaseDeleteButton() throws InterruptedException {
        // Given

        // When
        Thread.sleep(5000);
        liDetailsPage.clickOnConfirmDeleteButton();

        // Then
        assertThat(newInstancePage.getlearningInstancePageTitle()).isEqualTo("My Learning Instances");

        // Given

        // When
        liListingPage.waitForMessageToBeDisplayed();

        // Then
        assertThat(liListingPage.getMessageInstanceSuccessfullyDeleted()).isEqualTo("Learning instance was successfully removed");
        Thread.sleep(5000);
    }

    @ContinueOnFailure
    @Step("Enter the instance name on LearningInstance Search textbox which is currently deleted")
    public void putDeletedInstanceNameOnSearchTextBox() throws AWTException {
        // Given

        // When
        liListingPage.searchForDeteletedInstanceName(Driver.instName);
        liListingPage.hitEnterKey();
    }

    @ContinueOnFailure
    @Step("Message displayed <getMessageDisplayed>")
    public void getMessagedDisplayed(String getMessageDisplayed) throws InterruptedException {
        // Given
        String getMessage;

        // When
        getMessage = liListingPage.getMessagedDisplayedForSearchedInstance();

        // Then
        assertThat(getMessage).isEqualTo(getMessageDisplayed);
    }

    @ContinueOnFailure
    @Step("Enter the instance name in Bot page search text box which is currently deleted")
    public void putDeletedInstanceNameOnSearchTextBox1() throws AWTException {
        // Given

        // When
        botPage.searchForDeteletedInstanceNameOnBotPage(Driver.instName);
        liListingPage.hitEnterKey();

    }

    @ContinueOnFailure
    @Step("Message displayed in bot page for deleted search instance <Bot(s) not found>")
    public void msgInBotPageForDeletedInstance(String getMessageDisplayed) throws InterruptedException {
        // Given
        String getMessage;

        // When
        getMessage = botPage.getMessagedisplayedFromBotPageForSearchInstance();

        // Then
        assertThat(getMessage).isEqualTo(getMessageDisplayed);
    }

    @ContinueOnFailure
    @Step("Move instance from production to staging from learning instance detail page")
    public void moveInstanceFromProductionToStagingFromLearningInstanceDetailPage() throws InterruptedException {
        // Given

        // When
        liDetailsPage.editInstance();

        // Then

    }

    @ContinueOnFailure
    @Step("Move instance to production on Learning instance detail page")
    public void moveInstanceToProductionFromLearningInstanceDetailPage() {
        // Given

        // When
        liDetailsPage.clickOnToggleToMoveInstanceToProduction();

        // Then

    }

    @ContinueOnFailure
    @Step("Enter the instance name in Bot page search textbox which is currently deleted")
    public void putdeletedInstanceNameOnSearchtextBox() throws AWTException {
        // Given

        // When
        botPage.searchForDeteletedInstanceNameOnBotPage(Driver.instName);
        liListingPage.hitEnterKey();

    }

    @ContinueOnFailure
    @Step("By Clicking on Cancel button to cancel the delete instance user will be on same learning instance detail page")
    public void clickOnCancelButtonToCancelDeleting() {
        // Given

        // When
        liDetailsPage.clickOnCancelButtonToCancelDeleting();

        // Then
        assertThat(liDetailsPage.getviewDetailsPageHeading()).isEqualTo("View details");
    }

    @ContinueOnFailure
    @Step("Instance description on learning instance detail page is same as the instance description entered while creating instance")
    public void instanceDescriptionOnLIDetailPage() {
        // Given

        // when
        String learningInstancePageInstanceDescription = liDetailsPage.getInstanceDescription().getText();

        // Then
        assertThat(learningInstancePageInstanceDescription).isEqualTo(System.getenv("instance_Desc"));
    }

    //...............................Sanity.........................
    @ContinueOnFailure
    @Step("Sorting Validation for the header - <headerName>")
    public void sortingValidationForViewDetailsPage(String headerName) throws Exception {
        //Given
        baseClass.waitForPageLoad();
        String[] sortAsc = LearningInstanceDetailPage.bubblesortASC(liDetailsPage.noOfFileEachGroup(liDetailsPage.columnIndexForHeader(headerName)));
        String[] sortDec = LearningInstanceDetailPage.bubblesortDSC(liDetailsPage.noOfFileEachGroup(liDetailsPage.columnIndexForHeader(headerName)));

        //When
        liDetailsPage.clickOnColumnHeader(liDetailsPage.columnIndexForHeader(headerName));
        String[] strFirst = liDetailsPage.noOfFileEachGroup(liDetailsPage.columnIndexForHeader(headerName));

        //Then
        assertThat(strFirst).isEqualTo(sortAsc);

        //Given

        //When
        liDetailsPage.clickOnColumnHeader(liDetailsPage.columnIndexForHeader(headerName));
        String[] sortLast = liDetailsPage.noOfFileEachGroup(liDetailsPage.columnIndexForHeader(headerName));

        //Then
        assertThat(sortLast).isEqualTo(sortDec);


    }

    @ContinueOnFailure
    @Step("checking create bot or edit bot link availability for <nameGroup> <expResult>")
    public void checkCreateEditBotLink(String nameGroup, String expResult) throws Exception {
        String str = liDetailsPage.getAllCreateBotLink(nameGroup);
        assertThat(str).isEqualToIgnoringCase(expResult);
    }

    @ContinueOnFailure
    @Step("Wait for production document processing")
    public void waitForProductionDocumentProcessing() {
        baseClass.waitForSpinner();

    }

    @ContinueOnFailure
    @Step("Click on button <Yes, send to production>")
    public void implementation1(String clickOnYes) throws InterruptedException {
        // Given

        // When
        liDetailsPage.clickOnYesButton();

    }


    // used to extract data from the returned list when using actualGeneralInformationValues() method.
    private static final int Environment = 0, Domain = 1, Language = 2, Last_ModifiedDate = 3;

    @Step("Check the value of General Information when on <environment>")
    public void getValueOfGeneralInformation(String environment) throws Exception {
        // Given
        String expectedEnvironment, expectedDomain, expectedLanguage, expectedLastModifiedDate;
        String expectedSheetName = "GeneralInformation";
        if (environment.equals("Staging")) {
            expectedEnvironment = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 1);
        } else {
            expectedEnvironment = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 2);
        }

        expectedDomain = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 1);
        expectedLanguage = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 1);
        expectedLastModifiedDate = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 1);

        // When
        actualGeneralInformationValues = liDetailsPage.getActualGeneralInformationValue();
        // DateFormat dateInstance = new SimpleDateFormat("MMMM dd, yyyy");

        //System.out.println("General Information" + actualGeneralInformationValues);
        // Then
        assertThat(actualGeneralInformationValues.get(Environment)).isEqualTo(expectedEnvironment);
        assertThat(actualGeneralInformationValues.get(Domain)).isEqualTo(expectedDomain);
        assertThat(actualGeneralInformationValues.get(Language)).isEqualTo(expectedLanguage);
        assertThat(actualGeneralInformationValues.get(Last_ModifiedDate)).isEqualTo(expectedLastModifiedDate);
    }


    private static final int GroupsFound = 0, TestedFiles = 1, BotsCreated = 2, Success = 3, DocumentsUnclassified = 4, Failed = 5, Accuracy = 6, STP = 7;
    private static final int ProductionGroupsFound = 8, ProcessedFiles = 9, BotsInProduction = 10, ProductionSuccess = 11, ProductionDUnclassified = 12, Reviewed = 13, Invalid = 14, PendingReview = 15, ProductionAccuracy = 16, ProductionSTP = 17;

    @ContinueOnFailure
    @Step("Check the value of <environment> result when on <environmentName> for <specName>")
    public void
    getValueOfStagingAndProductionResult(String environment, String environmentName, String specName) throws Exception {

        // Given
        String expectedSheetName;
        String expectedGroupsFound, expectedTestedFiles, expectedBotsCreated, expectedSuccess, expectedDocumentsUnclassified, expectedFailed, expectedAccuracy, expectedSTP;
        String expectedProductionGroupsFound, expectedProcessedFiles, expectedBotsInProduction, expectedProductionSuccess, expectedProductionDUnclassified, expectedReviewed, expectedInvalid, expectedPendingReview, expectedProductionAccuracy, expectedProductionSTP;
        //System.out.println("Environment" + environment);
        //System.out.println("Environment Name" + environmentName);
        //System.out.println("Spec name" + specName);
        baseClass.waitForPageLoad();
        if (specName.equals("Smoke")) {
            if (environment.equals("Staging")) {

                expectedSheetName = "StagingResult";
                // Staging Environment for Smoke Suite
                if (environmentName.equals("Staging")) {
                    expectedGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 1);
                    expectedTestedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 1);
                    expectedBotsCreated = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 1);
                    expectedSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 1);
                    expectedDocumentsUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 1);
                    expectedFailed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 1);
                    expectedAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 1);
                    expectedSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 1);
                } else { // Production result
                    expectedGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 2);
                    expectedTestedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 2);
                    expectedBotsCreated = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 2);
                    expectedSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 2);
                    expectedDocumentsUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 2);
                    expectedFailed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 2);
                    expectedAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 2);
                    expectedSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 2);
                }

                // When
                actualStagingAndProductionResult = liDetailsPage.getActualStagingAndProductionResult();

                System.out.println("Staging Results" + actualStagingAndProductionResult);
                // Then
                assertThat(actualStagingAndProductionResult.get(GroupsFound)).isEqualTo(expectedGroupsFound);
                assertThat(actualStagingAndProductionResult.get(TestedFiles)).isEqualTo(expectedTestedFiles);
                assertThat(actualStagingAndProductionResult.get(BotsCreated)).isEqualTo(expectedBotsCreated);
                assertThat(actualStagingAndProductionResult.get(Success)).isEqualTo(expectedSuccess);
                assertThat(actualStagingAndProductionResult.get(DocumentsUnclassified)).isEqualTo(expectedDocumentsUnclassified);
                assertThat(actualStagingAndProductionResult.get(Failed)).isEqualTo(expectedFailed);
                assertThat(actualStagingAndProductionResult.get(Accuracy)).isEqualTo(expectedAccuracy);
                assertThat(actualStagingAndProductionResult.get(STP)).isEqualTo(expectedSTP);

            } else {
                // Given

                expectedSheetName = "ProductionResult";
                if (environmentName.equals("Staging")) {
                    expectedProductionGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 1);
                    expectedProcessedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 1);
                    expectedBotsInProduction = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 1);
                    expectedProductionSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 1);
                    expectedProductionDUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 1);
                    expectedReviewed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 1);
                    expectedInvalid = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 1);
                    expectedPendingReview = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 1);
                    expectedProductionAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 8, 1);
                    expectedProductionSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 9, 1);
                } else {
                    expectedProductionGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 2);
                    expectedProcessedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 2);
                    expectedBotsInProduction = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 2);
                    expectedProductionSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 2);
                    expectedProductionDUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 2);
                    expectedReviewed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 2);
                    expectedInvalid = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 2);
                    expectedPendingReview = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 2);
                    expectedProductionAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 8, 2);
                    expectedProductionSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 9, 2);
                }

                // When
                actualStagingAndProductionResult = liDetailsPage.getActualStagingAndProductionResult();
                //System.out.println("Production Results" + actualStagingAndProductionResult);

                // Then
                assertThat(actualStagingAndProductionResult.get(ProductionGroupsFound)).isEqualTo(expectedProductionGroupsFound);
                assertThat(actualStagingAndProductionResult.get(ProcessedFiles)).isEqualTo(expectedProcessedFiles);
                assertThat(actualStagingAndProductionResult.get(BotsInProduction)).isEqualTo(expectedBotsInProduction);
                assertThat(actualStagingAndProductionResult.get(ProductionSuccess)).isEqualTo(expectedProductionSuccess);
                assertThat(actualStagingAndProductionResult.get(ProductionDUnclassified)).isEqualTo(expectedProductionDUnclassified);
                assertThat(actualStagingAndProductionResult.get(Reviewed)).isEqualTo(expectedReviewed);
                assertThat(actualStagingAndProductionResult.get(Invalid)).isEqualTo(expectedInvalid);
                assertThat(actualStagingAndProductionResult.get(PendingReview)).isEqualTo(expectedPendingReview);
                assertThat(actualStagingAndProductionResult.get(ProductionAccuracy)).isEqualTo(expectedProductionAccuracy);
                assertThat(actualStagingAndProductionResult.get(ProductionSTP)).isEqualTo(expectedProductionSTP);
            }

            //-------------- Sanity values ------------------------------//
        } else {
            if (environment.equals("Staging")) {

                expectedSheetName = "StagingResult";
                // Staging Environment for Sanity Suite
                if (environmentName.equals("Staging")) {
                    expectedGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 3);
                    expectedTestedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 3);
                    expectedBotsCreated = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 3);
                    expectedSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 3);
                    expectedDocumentsUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 3);
                    expectedFailed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 3);
                    expectedAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 3);
                    expectedSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 3);
                } else { // Production result
                    expectedGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 4);
                    expectedTestedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 4);
                    expectedBotsCreated = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 4);
                    expectedSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 4);
                    expectedDocumentsUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 4);
                    expectedFailed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 4);
                    expectedAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 4);
                    expectedSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 4);
                }

                // When
                actualStagingAndProductionResult = liDetailsPage.getActualStagingAndProductionResult();

                //System.out.println("Staging Results" + actualStagingAndProductionResult);
                // Then
                assertThat(actualStagingAndProductionResult.get(GroupsFound)).isEqualTo(expectedGroupsFound);
                assertThat(actualStagingAndProductionResult.get(TestedFiles)).isEqualTo(expectedTestedFiles);
                assertThat(actualStagingAndProductionResult.get(BotsCreated)).isEqualTo(expectedBotsCreated);
                assertThat(actualStagingAndProductionResult.get(Success)).isEqualTo(expectedSuccess);
                assertThat(actualStagingAndProductionResult.get(DocumentsUnclassified)).isEqualTo(expectedDocumentsUnclassified);
                assertThat(actualStagingAndProductionResult.get(Failed)).isEqualTo(expectedFailed);
                assertThat(actualStagingAndProductionResult.get(Accuracy)).isEqualTo(expectedAccuracy);
                assertThat(actualStagingAndProductionResult.get(STP)).isEqualTo(expectedSTP);

            } else {
                // Given

                expectedSheetName = "ProductionResult";
                if (environmentName.equals("Staging")) {
                    expectedProductionGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 3);
                    expectedProcessedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 3);
                    expectedBotsInProduction = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 3);
                    expectedProductionSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 3);
                    expectedProductionDUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 3);
                    expectedReviewed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 3);
                    expectedInvalid = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 3);
                    expectedPendingReview = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 3);
                    expectedProductionAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 8, 3);
                    expectedProductionSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 9, 3);
                } else {
                    expectedProductionGroupsFound = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 4);
                    expectedProcessedFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 4);
                    expectedBotsInProduction = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 4);
                    expectedProductionSuccess = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 3, 4);
                    expectedProductionDUnclassified = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 4, 4);
                    expectedReviewed = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 5, 4);
                    expectedInvalid = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 6, 4);
                    expectedPendingReview = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 7, 4);
                    expectedProductionAccuracy = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 8, 4);
                    expectedProductionSTP = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 9, 4);
                }

                // When
                actualStagingAndProductionResult = liDetailsPage.getActualStagingAndProductionResult();
                //System.out.println("Production Results" + actualStagingAndProductionResult);

                // Then
                assertThat(actualStagingAndProductionResult.get(ProductionGroupsFound)).isEqualTo(expectedProductionGroupsFound);
                assertThat(actualStagingAndProductionResult.get(ProcessedFiles)).isEqualTo(expectedProcessedFiles);
                assertThat(actualStagingAndProductionResult.get(BotsInProduction)).isEqualTo(expectedBotsInProduction);
                assertThat(actualStagingAndProductionResult.get(ProductionSuccess)).isEqualTo(expectedProductionSuccess);
                assertThat(actualStagingAndProductionResult.get(ProductionDUnclassified)).isEqualTo(expectedProductionDUnclassified);
                assertThat(actualStagingAndProductionResult.get(Reviewed)).isEqualTo(expectedReviewed);
                assertThat(actualStagingAndProductionResult.get(Invalid)).isEqualTo(expectedInvalid);
                assertThat(actualStagingAndProductionResult.get(PendingReview)).isEqualTo(expectedPendingReview);
                assertThat(actualStagingAndProductionResult.get(ProductionAccuracy)).isEqualTo(expectedProductionAccuracy);
                assertThat(actualStagingAndProductionResult.get(ProductionSTP)).isEqualTo(expectedProductionSTP);
            }
        }
    }


    private static final int GENERAL = 0, STAGING = 1, PRODUCTION = 2;

    @Step("Check staging result and production result value when on <environment>")
    public void getHeaderOfGeneralStagingProductionResult(String environment) throws Exception {
        // Given
        String expectedSheetName = "HeaderOfGeneralInformation";
        String expectedGeneral, expectedStagingFiles, expectedProductionFiles;

        expectedGeneral = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 0, 0);
        expectedStagingFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 1, 0);
        if (environment.equals("Staging")) {
            expectedProductionFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 0);
        } else {
            expectedProductionFiles = TestBase.getData(System.getenv("TestDataPath"), expectedSheetName, 2, 1);
        }

        // When
        actualHeaderStagingAndProductionResult = liDetailsPage.getHeaderOfGeneralStagingProductionResult();

        // Then
        //System.out.println("General" + actualHeaderStagingAndProductionResult.get(GENERAL));
        //System.out.println("STAGING" + actualHeaderStagingAndProductionResult.get(STAGING));
        //System.out.println("PRODUCTION" + actualHeaderStagingAndProductionResult.get(PRODUCTION));
        assertThat(actualHeaderStagingAndProductionResult.get(GENERAL)).isEqualTo(expectedGeneral);
        assertThat(actualHeaderStagingAndProductionResult.get(STAGING)).isEqualTo(expectedStagingFiles);
        assertThat(actualHeaderStagingAndProductionResult.get(PRODUCTION)).isEqualTo(expectedProductionFiles);

    }

    @Step("Launch designer")
    public void implementation2() throws Exception {
        liDetailsPage.launchDesignerForSelectedGroup(Driver.trainingGroupName);
        baseClass.runBatchFile(System.getenv("launchDesigner"));
    }

    @Step("Launch and Close Designer")
    public void launchAndCloseDesigner() throws IOException, InterruptedException {
        // Given
        baseClass.runBatchFile(System.getenv("getLaunchAndCloseDesigner"));

    }

    @Step("Ensure learning instance name breadcrumb is available")
    public void validateLINameInBreadcrumbLink() {
        // Given
        baseClass.waitForPageLoad();
        // When
        baseClass.explicitWait("//div[@class='breadcrumbs-breadcrumb'][2]");
        // Then
        assertThat(Driver.instName).isEqualTo(liDetailsPage.getLINameBreadcrumbtext());
    }

    @Step("When user click on CreateBot link trainbot should get opened")
    public void openTrainbot() throws Exception {
        // Given

        // When
        baseClass.waitForElementDisplay(liListingPage.getLielement());
        // Then
        liDetailsPage.openTrainbotForFirstGroup();

    }

    //---------------------COG-2880-------------------------------------------------------------------------------------
    @ContinueOnFailure
    @Step("User clicks on first group CreateBot link")
    public void openFirstGroupTrainbot() throws Exception {
        // Given

        // When
        baseClass.waitForElementDisplay(liListingPage.getLielement());

        // Then
        liDetailsPage.openTrainbotForFirstGroup();
        baseClass.waitForElementDisplay(trainingPage.getFirstSIR());
    }

    @Step("Check all the files are classified")
    public void CheckFileClassified() throws InterruptedException {
        // Given

        // When

        // Then
        Thread.sleep(5000);
        assertThat(liDetailsPage.checkGroupExist()).isEqualTo(1);
        assertThat(liDetailsPage.checkCreateBotLinkExist()).isEqualTo("Create Bot");
    }

}

