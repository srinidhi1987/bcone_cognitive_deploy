package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.assertj.core.api.AssertionsForClassTypes;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.*;
import utils.TestBase;

import java.sql.SQLException;

import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.assertThat;

public class EditLearningInstancePageImplementation {

    private CreateNewInstancePage newInstancePage = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private LearningInstanceDetailPage liDetailsPage = PageFactory.initElements(Driver.webDriver, LearningInstanceDetailPage.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private CreateNewInstancePage newInstance = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    private BotListingPage botPage = PageFactory.initElements(Driver.webDriver, BotListingPage.class);
    private ValidationPage validatorPage = PageFactory.initElements(Driver.webDriver, ValidationPage.class);
    private EditLearningInstancePage addfields = PageFactory.initElements(Driver.webDriver, EditLearningInstancePage.class);
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    //private static String filesCount_Before_EditInstance;


    @ContinueOnFailure
    @Step("When user edit instance and upload files for add fields story")
    public void editInstanceAndUploadFiles() throws Exception {
        // Given

        // When

        newInstance.clickOnBrowseButton();
        Thread.sleep(500);
        newInstancePage.uploadFiles("English","Invoices","AddFieldsDocs_Edit");

        //Then

    }
    @ContinueOnFailure
    @Step("To add a standard form/tc field <> on edit learning instance")
    public void ensureServiceUserHaveAllAccess(String fieldName) throws Exception {
        //Given


        //When
        addfields.clickOnCheckBox(fieldName);
        baseClass.waitForPageLoad();

        //Then
        assertThat(addfields.pageLabeLIListing()).contains("Learning Instances");

    }

    @ContinueOnFailure
    @Step("To add a custom standard <> field <> on edit learning instance")
    public void addCustomFields(String fieldName, String fieldType) throws Exception {
        //Given

        addfields.addACustomField(fieldName,fieldType);
        //When

        //Then
    }
    @ContinueOnFailure
    @Step("Click on Learning instace name on LI listing page")
    public void clickOnLearningInstance() throws Exception {

        //Given


        //When
        addfields.clickOnInstaceLink();
        baseClass.waitForPageLoad();

        //Then


    }
    @ContinueOnFailure
    @Step("Click on Edit button on Learning Instance Details page")
    public void clickOnEditButton()throws Exception {

        //Given


        //When
       addfields.editInstanceButton.click();
        baseClass.waitForPageLoad();

        //Then

    }

    @ContinueOnFailure
    @Step("Click on Save button to update that Learning Instance")
    public void clickOnSaveButton()throws Exception {

        //Given


        //When
        addfields.saveButton.click();
        baseClass.waitForPageLoad();

        //Then

    }

    @ContinueOnFailure
    @Step("Click on OK button on pop-up to update that Learning Instance")
    public void clickOnOkButton()throws Exception {

        //Given


        //When
        addfields.yesToSaveOnPopup.click();

        //Then

    }


    @ContinueOnFailure
    @Step("Open cognitive console with a learning instance page URL")
    public void implementation1() {
        // Given
        String app_url = System.getenv("Instance_URL");

        // When
        Driver.webDriver.get(app_url + "/");
        baseClass.waitForPageLoad();

        // Then
       writeMessage("Page title is %s", Driver.webDriver.getTitle());
    }

    @ContinueOnFailure
    @Step("verify the message <> on UI after learning instance modification ")
    public void messageVerification(String message) throws Exception {
        // Given


        // When
        WebDriverWait wait = new WebDriverWait(Driver.webDriver, 30);
        By locator = By.xpath("//div[contains(@class,'app-notification-container')]");
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        String actualMessage=addfields.messageAfterSaveYes();
        System.out.println("######0000000#######"+actualMessage);

        // Then
        assertThat(actualMessage).contains(message);

    }

    @ContinueOnFailure
    @Step("Get group id for Addfields story <training>")
    public void getClassificationIdForUploadedDocuments(String groupType) throws SQLException {
        addfields.setGroupId(groupType);
    }

    @ContinueOnFailure
    @Step("Wait for spinner on Learning Instance page")
    public void validateProductionFilesUploadAndClassification() throws Exception {
        // Given

        // When
        baseClass.waitForSpinner();

        // Then


    }

    @ContinueOnFailure
    @Step("When user click on CreateBot link visionbot designer should be launched <training_group> for addFields story")
    public void launchDesignerAndTrainVisionbot(String groupType) throws Exception {
        // Given

        // When
        baseClass.waitForElementDisplay(liListingPage.getLielement());
        System.out.println("Add fields group name :"+Driver.addFieldsGroupName);

        // Then
        if (groupType.equalsIgnoreCase("addFieldsGroup")) {
            liDetailsPage.launchDesignerForSelectedGroup(Driver.addFieldsGroupName);
        }


    }

    @ContinueOnFailure
    @Step("Ensure after training data should be extracted in preview & in IQTest for addFields story")
    public void trainVisionbotAndValidatePreview() throws Exception {
        // Given
        baseClass.runBatchFile(System.getenv("addFieldsTrainingBatchFile"));

        // When
       System.out.println("Trained bot completed"+Driver.addFieldsGroupName);

        // Then

    }
    @ContinueOnFailure
    @Step("Ensure bot details for <group> is updated after  bot run for addFields story")
    public void checkAllDetailsAfteRunGroupWise(String groupType) throws Exception {
        // Given

        // When
        Thread.sleep(10000); //need to provide sleep as bot is in processing and we don't have any locator for that.
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(botPage.getSearchBox());
        botPage.searchBots(Driver.instName);
        baseClass.explicitWait("//span[text()='Files Processed']/following-sibling::span[1]");

        // Then
        if (groupType.equalsIgnoreCase("addFieldsGroup")) {
            botPage.clickOnGroupName(Driver.addFieldsGroupName);
            botPage.getAllDetailsOfBotSummary("Staging_BLP_After_BotRun_B_Gr2");
        } else {
            System.out.println("ADD Fields Group name is not available......");
        }

    }

    @ContinueOnFailure
    @Step("Validate all data after group move from Staging to production <Group_Name> for add field story")
    public void validateAllDataAfterGroupMoveToStaging(String groupType) throws Exception {
        // Given

        // When
        ////  liListingPage.clickonLinks("Bots");
        //botPage.searchBots(Driver.instName);
        // When
        ///       botPage.searchBots(Driver.instName);
        if(groupType.equalsIgnoreCase("addFieldsGroup")) {
            botPage.moveGroupToProduction(Driver.addFieldsGroupName);
        }
        Driver.webDriver.navigate().refresh();
        baseClass.waitForPageLoad();
        baseClass.waitForElementClick(botPage.getSearchBox());
        botPage.searchBots(Driver.instName);
////        baseClass.explicitWait("//span[text()='Files Processed']/following-sibling::span[1]");

        // Then
        if (groupType.equalsIgnoreCase("addFieldsGroup")) {
            botPage.clickOnGroupName(Driver.addFieldsGroupName);
            botPage.getAllDetailsAfterMoveProduction("Production_BLP_B", Driver.addFieldsGroupName);
        }

    }

    @ContinueOnFailure
    @Step("When instance is created then it should be listed in IQBot Lite command & Success and Invalid path should be listed in IQBot Lite command for add fields")
    public void launchTaskEditorAndValidateIQBotLiteCommand() throws Exception {
        // Given

        // When
        baseClass.runBatchFile(System.getenv("iqBotLiteVerificationBatchFile_AddFields"));


        // Then


    }



    @ContinueOnFailure
    @Step("Run once bot for <group> for add fields story")
    public void runBotGroupWise(String groupType) throws Exception {
        //Given

        //When
        baseClass.waitForPageLoad();

        //Then
        liListingPage.clickonLinks("Bots");
        botPage.searchBots(Driver.instName);
        if (groupType.equalsIgnoreCase("addFieldsGroup")) {
            botPage.runBotOnceByGroup(Driver.addFieldsGroupName);
            System.out.println(Driver.addFieldsGroupName);
        }
        Driver.webDriver.navigate().refresh();

    }

    @ContinueOnFailure
    @Step("Check output folder for created instance and validate unclassified files add fields")
    public void validateOutputFolder() throws Exception {
        // Given

        // When
        baseClass.runBatchFile(System.getenv("outputFolderVerificationAddFields"));
        String results = baseClass.validateDesignerLogs(System.getenv("outputFolderLogFile"));

        // Then
        assertThat(results).isEqualTo("Output Folder Creation : Pass : Output File Creation : Pass");
    }


}
