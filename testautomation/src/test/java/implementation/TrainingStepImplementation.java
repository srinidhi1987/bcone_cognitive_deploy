package implementation;

import com.thoughtworks.gauge.Step;
import driver.Driver;
import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.assertThat;
import org.openqa.selenium.WebElement;
import pageobjects.BotListingPage;
import pageobjects.TrainingPage;
import utils.TestBase;
import com.thoughtworks.gauge.ContinueOnFailure;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pageobjects.LearningInstanceDetailPage;
import pageobjects.LearningInstanceListingPage;
import java.util.Arrays;
import java.util.List;

public class TrainingStepImplementation {

    private TrainingPage trainingPage = PageFactory.initElements(Driver.webDriver, TrainingPage.class);
    private BotListingPage botPage = PageFactory.initElements(Driver.webDriver, BotListingPage.class);
    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    private LearningInstanceDetailPage liDetailsPage =
            PageFactory.initElements(Driver.webDriver, LearningInstanceDetailPage.class);
    private LearningInstanceListingPage liListingPage =
            PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);

    // Default field types
    private final String[] fieldTypes = {"text", "date", "number"};

    // Defaults fields & Additional fields
    private final String[] InvFields = {"Invoice Number", "Invoice Date", "Invoice Total"};
    private final String[] InvAdditionalFields = {"Payment Term"};
    private final String[] InvTable = {"Item Description", "Quantity", "Item Total"};
    private final String[] InvAdditionalTable = {"Product Description"};
    private final String[] POFields = {"Purchase Order_Date", "Purchase Order Number", "Total"};
    private final String[] POAdditionalFields = {"Warehouse"};
    private final String[] POTable = {"item_description", "quantity", "Item Total"};
    private final String[] POAdditionalTable = {"Unit Cost"};
    private final String[] OthersFields = {"Payment Term"};
    private final String[] OthersTable = {"Product Description"};
    // Fields which wont get auto mapped while bot creation
    private final String[] CustomFields = {"Rebmun Eciovni", "Etad Eciovni", "Latot Eciovni"};
    private final String[] CustomTable = {"Ytitnauq", "Noitpircsed Meti", "Latot Meti"};
    private static int countUnMapped = 0;
    private String group1, group2, group3;

    //==================================================================================================================
    @Step("User Landed to <Training> Page")
    public void getPageElements(String page) {
        // Given

        // When

        // Then
        assertThat(trainingPage.getFinishButton()).isEqualTo("Finish Later");
    }

    @Step("Ensure user is able to see <ZoomIn> option in web designer")
    public void getZoomIn(String zoom) {
        // Given

        // When

        // Then
        if (zoom.equals("Zoom in")) {
            assertThat(trainingPage.getZoomOption(zoom)).isEqualTo("Zoom in");
        } else if (zoom.equals("Zoom out")) {
            assertThat(trainingPage.getZoomOption(zoom)).isEqualTo("Zoom out");
        } else {
            assertThat(trainingPage.getZoomOption(zoom)).isEqualTo("Fit to screen");
        }
    }

    @Step("Ensure user is able to use <Zoom in> functionality properly")
    public void clickOnZoomIn(String zoomOption) throws Exception {
        // Given
        String results_Additional;
        switch (zoomOption) {
            case "Zoom in icon":
                // Given
                baseClass.runBatchFile(System.getenv("ZoomInFirstClickBatchFile"));

                // When
                results_Additional = baseClass.validateDesignerLogs(System.getenv("ZoomInLogFile"));

                // Then
                assertThat(results_Additional).isEqualTo("SecondClick : Pass :");
                break;
            case "Zoom in text":
                baseClass.runBatchFile(System.getenv("ZoomInThirdClickBatchFile"));

                // When
                results_Additional = baseClass.validateDesignerLogs(System.getenv("ZoomInLogFile"));

                // Then
                assertThat(results_Additional).isEqualTo("ThirdClick : Pass :");
                break;
            case "Zoom out icon":
                // Given
                baseClass.runBatchFile(System.getenv("ZoomOutFirstClickBatchFile"));

                // When
                results_Additional = baseClass.validateDesignerLogs(System.getenv("ZoomOutLogFile"));

                // Then
                assertThat(results_Additional).isEqualTo("FirstClick : Pass :");
                break;
            case "Zoom out text":
                // Given
                baseClass.runBatchFile(System.getenv("ZoomOutSecondClickBatchFile"));

                // When
                results_Additional = baseClass.validateDesignerLogs(System.getenv("ZoomOutLogFile"));

                // Then
                assertThat(results_Additional).isEqualTo("SecondClick : Pass :");
                break;
            case "Scroll Up and Down":
                // Given
                baseClass.runBatchFile(System.getenv("MouseScrollBatchFile"));

                // When
                results_Additional = baseClass.validateDesignerLogs(System.getenv("MouseScrollLogFile"));

                // Then
                assertThat(results_Additional).isEqualTo("ScrollUp : Pass :ScrollDown : Pass :");
                break;

        }
    }

    @Step("Click on <Zoom in icon>")
    public void clickOnZoom(String zoomOptions) {
        // Given

        // When
        switch (zoomOptions) {
            case "Zoom in icon":
                trainingPage.clickOnZoomInIcon();
                break;
            case "Zoom in text":
                trainingPage.clickOnZoomInButton();
                break;
            case "Zoom out icon":
                trainingPage.clickOnZoomOutIcon();
                break;
            case "Zoom out text":
                trainingPage.clickOnZoomOutButton();
                break;
            case "Fit to screen icon":
                trainingPage.clickOnFitToScreenIcon();
                break;
            case "Fit to screen text":
                trainingPage.clickOnFitToScreenText();
                break;
        }
    }

    @Step("Ensure document is fitted to screen")
    public void getDocument() throws Exception {
        // Given
        baseClass.runBatchFile(System.getenv("FitToScreenBatchFile"));

        // When
        String results_Additional = baseClass.validateDesignerLogs(System.getenv("FitToScreenLogFile"));

        // Then
        assertThat(results_Additional).isEqualTo("FitToScreen : Pass :");

    }

    @Step("Click on Instance name breadcrum")
    public void clickOnInstancenameBreadCrum() throws InterruptedException {
        // Given

        // When
        trainingPage.clickOnInstancenameBreadCrum();

    }

    @Step("Ensure document size is small and user should not be able to scroll")
    public void checkDocumentSize() {
        // Given

        // When

        // Then
        assertThat(trainingPage.getSizeOfDocument().equals("313>>>333"));

    }

    @Step("User should able to see the document")
    public void checkDocumentDisplay() {
        // Given

        // When
        // boolean ImagePresent = webDesignerPage.checkDocumentDisplay();
       /*  ImagePresent = if (!ImagePresent) {
            System.out.println("Image not displayed.");
        }
        else {
            System.out.println("Image displayed.");
        }*/

    }

    @Step("Ensure train bot breadcrumb is available")
    public void ValidateTrainBotGroupBreadcrumbLink() {
        // Given
        baseClass.waitForPageLoad();

        // When
        String groupNameInCreatBotPage = trainingPage.getTrainBotGorupNameBreadcrumbtext();
        liListingPage.clickOnLearningInstanceBreadcrumb();
        String groupNameLIDetailPage = liDetailsPage.getGroupName(1);

        // Then
        assertThat(groupNameInCreatBotPage).containsIgnoringCase(groupNameLIDetailPage);
    }


    //-----------------------------------------COG-2881  &  COG-2882----------------------------------------------------

    @Step("Ensure all default fields of <Domain> were available in train bot")
    public void ValidateFieldsAvailabilityinTrainBot(String Domain) {
        // Given

        // When

        // Then
        switch (Domain) {
            case "Invoices Default":
                for (String item : InvFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : InvTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                break;
            case "Invoices Additional":
                for (String item : InvFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : InvAdditionalFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : InvTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : InvAdditionalTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                break;
            case "Purchase Orders Default":
                for (String item : POFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : POTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                break;
            case "Purchase Order Additional":
                for (String item : POFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : POAdditionalFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : POTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : POAdditionalTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                break;
            case "Other":
                for (String item : OthersFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : OthersTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                break;
            case "Custom Fields":
                for (String item : CustomFields) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                for (String item : CustomTable) {
                    Assert.assertEquals(trainingPage.isFieldFound(item), true);
                }
                break;
            default:
                System.out.println("None of the Domain matched");
                break;
        }
    }

    @ContinueOnFailure
    @Step("Ensure that document is rotated")
    public void ValidateImageRotationinTrainbot() throws Exception {
        // Given
        baseClass.runBatchFile(System.getenv("imageRotationVerificaionBatchFile"));
        // When
        String results = baseClass.validateDesignerLogs(System.getenv("imageRotationLogFile"));
        // Then
        assertThat(results).isEqualTo("IsSampleImageFound : Yes");
    }

    @Step("User clicks on Finish button in Trainbot page lands on Instance details page")
    public void finishTraining() {
        trainingPage.cilckOnTainingFinishButton();
        baseClass.explicitWait("//*[@class = 'pagetitle-label']");
        assertThat(liDetailsPage.getInstanceName().getText()).isEqualTo(Driver.instName);
    }

    @ContinueOnFailure
    @Step("Ensure Field <Detail> section is available")
    public void fieldDetail(String fieldDetails) {
        switch (fieldDetails) {
            case "Type":
                trainingPage.isElementFound("Type");
                break;
            case "Label":
                trainingPage.isElementFound("Label");
                break;
            case "Value":
                trainingPage.isElementFound("Value");
                break;
            case "Options":
                trainingPage.isElementFound("Options");
                break;
            case "Formula":
                trainingPage.isElementFound("Formula");
                break;
            default:
                System.out.println("Specified field is not a valid one !!");
                break;
        }
    }

    @Step("Ensure Field Label section has a input area to enter label")
    public void CheckFieldLabelInput() {
        boolean b;
        b = trainingPage.isElementPresent("Label Box");
        assertThat(b).isTrue();
    }

    @Step("Ensure Field Value section has a input area to enter label")
    public void CheckFieldValueInput() {
        boolean b;
        b = trainingPage.isElementPresent("Value Box");
        assertThat(b).isTrue();
    }

    // Select table column before executing this step
    @Step("Ensure Table column has no Value region")
    public void CheckTableValueregion() {
        boolean b;
        b = trainingPage.isElementPresent("Value Box");
        assertThat(b).isFalse();
    }

    // Verify Type DropDown have values.
    @ContinueOnFailure
    @Step("Verify values of Type DropDown")
    public void VerifyFieldTypeItems() {
        // Given
        List<String> expectedTypeDDList = TestBase.propertyNameAsList("listFieldType");

        // When
        List<String> actualTypeDDList = trainingPage.getTypeDDList();

        //Then
        assertThat(expectedTypeDDList).isNotNull();
        assertThat(actualTypeDDList).isNotNull();
        assertThat(actualTypeDDList).containsAll(expectedTypeDDList);
        assertThat(actualTypeDDList.size()).isEqualTo(expectedTypeDDList.size());
    }

    @Step("Ensure Field <Label> section has a picker to select region from document")
    public void verifyPicker(String picker) {
        switch (picker) {
            case "Label":
                trainingPage.isElementFound("Label Picker");
                break;
            case "Value":
                trainingPage.isElementFound("Value Picker");
                break;
            default:
                System.out.println("Specified field is not a valid one !!");
                break;
        }
    }

    //----------------------------------------------------COG-3176------------------------------------------------------

    @ContinueOnFailure
    @Step("Ensure domain <Domain name> fields has correct field types")
    public void verifyFieldType(String Domain) {

        switch (Domain) {
            case "Invoices Default":
                List<String> invoiceDefaultFieldsList = trainingPage.concat(Arrays.asList(InvFields, InvTable));
                //List<String> expectedFieldType = Arrays.asList("text", "date", "number", "text", "number", "number");
                //List<String> actualFieldType = new ArrayList<>();
                for (String item : invoiceDefaultFieldsList) {
                    if (item == "Invoice Number" || item == "Item Description" || item == "Payment Term" ||
                            item == "Product Description") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[0]);
                        //actualFieldType.add(trainingPage.getType());
                    } else if (item == "Invoice Date") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[1]);
                        //actualFieldType.add(trainingPage.getType());
                    } else if (item == "Invoice Total" || item == "Quantity" || item == "Item Total") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[2]);
                        //actualFieldType.add(trainingPage.getType());
                    }
                }
                //Assert.assertEquals(expectedFieldType, actualFieldType);
                break;

            case "Invoices":
                List<String> invoiceDefaultandAdditionalFieldsList =
                        trainingPage.concat(Arrays.asList(InvFields, InvAdditionalFields, InvTable, InvAdditionalTable));
                for (String item : invoiceDefaultandAdditionalFieldsList) {
                    if (item == "Invoice Number" || item == "Item Description" || item == "Payment Term" ||
                            item == "Product Description") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[0]);
                    } else if (item == "Invoice Date") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[1]);
                    } else if (item == "Invoice Total" || item == "Quantity" || item == "Item Total") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[2]);
                    }
                }
                break;

            case "Purchase Orders":
                List<String> purchaseOrdersDefaultandAdditionalFieldsList =
                        trainingPage.concat(Arrays.asList(POFields, POTable));
                for (String item : purchaseOrdersDefaultandAdditionalFieldsList) {
                    if (item == "Purchase Order Number" || item == "item description" ||
                            item == "Warehouse" || item == "Unit Cost") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[0]);
                    } else if (item == "Purchase Order Date") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[1]);
                    } else if (item == "Total" || item == "quantity" || item == "Item Total") {
                        trainingPage.selectField(item);
                        Assert.assertEquals(trainingPage.getType(), fieldTypes[2]);
                    }
                }
                break;
            case "Others":
                List<String> otherFieldsList =
                        trainingPage.concat(Arrays.asList(OthersFields, OthersTable));
                for (String item : otherFieldsList) {
                    trainingPage.selectField(item);
                    Assert.assertEquals(trainingPage.getType(), fieldTypes[0]);
                }
                break;
        }
    }

    @Step("Ensure for required field default value is disabled")
    public void verifyDefaultValueRegionisDisabledforRequiredField() {

        // When
        trainingPage.isDefaultValueFieldEnabled("Invoice Number");

        // Then
        assertThat(trainingPage.isDefaultValueFieldEnabled("Invoice Number")).isFalse();
    }

    @Step("Ensure for optional field default value is enabled")
    public void verifyDefaultValueRegionisEnabledforOptionalField() {

        // When
        trainingPage.selectField("Invoice Number");
        trainingPage.checkOptionalField();
        trainingPage.isDefaultValueFieldEnabled("Invoice Number");

        // Then
        assertThat(trainingPage.isDefaultValueFieldEnabled("Invoice Number")).isTrue();
    }

    @Step("Ensure for table field value section is disabled")
    public void verifyValueRegionisiDisabledforTableField() {

        // When


        // Then
        assertThat(trainingPage.isValueEnabled()).isFalse();
        assertThat(trainingPage.isValueBoxEnabled()).isFalse();
    }

    @Step("Ensure on selecting Validate Pattern all patterns shows up")
    public void verifyValidatePatternOptions() {

        // When
        trainingPage.checkValidatePattern();

        // Then
        trainingPage.isElementFound("Validate Patterns");
    }

    @Step("Ensure changes made in webdesigner were saved automatically")
    public void verifyChangeSavedAutomatically() {

        // When
        trainingPage.selectField("Invoice Number");
        trainingPage.clickOnFieldOptionsDropDown();
        trainingPage.setType("number");
        trainingPage.setFieldLabel("Invoice Number");
        trainingPage.setFieldValue("ND8746952");
        trainingPage.checkOptionalField();
        trainingPage.setOptionalFieldDefaultValueElement("Default value");
        //trainingPage.checkValidatePattern();
        trainingPage.selectField("Invoice Date");
        trainingPage.setType("date");

        // Then
        trainingPage.selectField("Invoice Number");
        assertThat(trainingPage.getType().equalsIgnoreCase("number"));
        assertThat(trainingPage.getFieldLabel().equalsIgnoreCase("Invoice Number"));
        assertThat(trainingPage.getFieldValue().equalsIgnoreCase("ND8746952"));
        assertThat(trainingPage.getOptionalFieldDefaultValueElement().equalsIgnoreCase("Default value"));
        trainingPage.selectField("Invoice Date");
        assertThat(trainingPage.getType().equalsIgnoreCase("date"));
    }

    @Step("User clicks on Field Options dropdown")
    public void clickOnFieldOptionsDropDown() {
        trainingPage.clickOnFieldOptionsDropDown();
    }

    @Step("Select field <Field name>")
    public void selectField(String fieldName) {
        trainingPage.selectField(fieldName);
    }

    @Step("Select label box")
    public void selectLabelBox() {
        trainingPage.selectLabelBox();
    }

    @Step("Enter label <Label>")
    public void enterLabel(String label) {
        trainingPage.setFieldLabel(label);
    }

    @Step("Clear label")
    public void enterLabel() {
        trainingPage.clearFieldLabel();
    }

    @Step("Select value box")
    public void selectValueBox() {
        trainingPage.selectValueBox();
    }

    @Step("Select SIR <SIR Id>")
    public void selectSIR(String SIRId) {
        trainingPage.clickOnSIR(SIRId);
    }

    @Step("Then label box should get a value of <expected label>")
    public void verifyLabel(String expectedLabel) {
        assertThat(trainingPage.getFieldLabel().equalsIgnoreCase(expectedLabel));
    }

    @Step("Then value box should get a value of <expected value>")
    public void verifyValue(String expectedValue) {
        assertThat(trainingPage.getFieldValue().equalsIgnoreCase(expectedValue));
    }

    @Step("Select <picker> picker")
    public void clickOnPicker(String picker) {
        if (picker == "Label") {
            trainingPage.clickOnLabelPicker();
        } else {
            trainingPage.clickOnValuePicker();
        }
    }

    @Step("Select label picker")
    public void clickOnLabelPicker() {
        trainingPage.clickOnLabelPicker();
    }

    @Step("draw from <x-start>, <y-start> to <x-end>, <y-end>")
    public void draw(int x1, int y1, int x2, int y2) {
        trainingPage.draw(x1, y1, x2, y2);
    }

    @Step("Select field type as <fieldType>")
    public void setFieldType(String fieldType) {
        trainingPage.selectFieldType(fieldType);
    }

    @Step("Check, field label not available check box")
    public void checkFieldLabelNotAvailableCheckbox() {
        trainingPage.checkFieldLabelNotAvailable();
    }

    @Step("Ensure field label not available check box is not available for table field")
    public void checkFieldNotAvailableCheckbox() {
        assertThat(trainingPage.checkAvailibilityofFieldLabelNotAvailableCheckbox()).isFalse();
    }

    @Step("Ensure field training indicator shows field trained")
    public void fieldTrained() {
        assertThat(trainingPage.getFieldTrainingStatus()).isTrue();
    }

    @Step("Ensure field training indicator shows field not trained")
    public void fieldNotTrained() {
        assertThat(trainingPage.getFieldTrainingStatus()).isFalse();
    }

    //----------------Save Bot steps---------------------------

    @Step("User see  <Save and close> is <disabled>")
    public void btnSaveandClose(String btnSaveandClose, String status) {
        // Given

        // When
        if (status.equals("disabled")) {

            // Then
            if (countUnMapped == 1) {
                assertThat(trainingPage.getSaveAndCloseStatus()).isTrue();
                writeMessage("Form fields Order number is not mapped");
            } else if (countUnMapped > 1) {
                assertThat(trainingPage.getSaveAndCloseStatus()).isTrue();
                writeMessage("Few fields are not automapped");
            }
        } else {
            assertThat(trainingPage.getSaveAndCloseStatus()).isFalse();
        }
    }


    @Step("All the required fields are <not mapped>")
    public void countRequiredfields(String notMappedFields) {
        // Given

        // when
        countUnMapped = trainingPage.countNotMappedFields();

    }

    @Step("Click <Order Number> from <Form> field")
    public void clickOnFormField(String fieldName, String fieldType) {
        // Given

        // When
        trainingPage.clickOnFieldName(fieldName);

    }

    @Step("User can see <label> textbox extracted with text <Customer PO:>")
    public void labelTextBoxValue(String textboxType, String textValue) throws InterruptedException {
        // Given

        // When

        // Then
        assertThat(trainingPage.getLabelText(textValue)).isEqualTo(textValue);
    }

    @Step("Modal popup is displayed")
    public void modalPopUpIsDisplayed() {
        // Given

        // When

        // Then
        assertThat(trainingPage.getModalPopUp().isDisplayed());
    }

    private static final int CANCEL = 0;
    private static final int SAVE = 1;
    private static final int SAVEANDSEND = 2;

    @Step("User can see Modal popup with options <Cancel> <Save> <Save and send to production>")
    public void modalPopUpOptions(String buttonCancel, String buttonSave, String buttonSaveAndSend) {
        // Given

        // When

        // Then
        assertThat(trainingPage.getModalPopUpOptions().get(CANCEL)).isEqualTo(buttonCancel);
        assertThat(trainingPage.getModalPopUpOptions().get(SAVE)).isEqualTo(buttonSave);
        assertThat(trainingPage.getModalPopUpOptions().get(SAVEANDSEND)).isEqualTo(buttonSaveAndSend);

    }

    @Step("Where button <Save and send to production> is <disabled>")
    public void getStatusSaveAndSend(String buttonSaveAndSend, String status) {
        // Given

        // When
        if (status.equals("disabled")) {
            // Then
            assertThat(trainingPage.getStatusOfSaveAndSend()).isTrue();
        } else {
            assertThat(trainingPage.getStatusOfSaveAndSend()).isFalse();
        }
    }

    @Step("Click on <Cancel> button")
    public void clickOnModalPopUpButton(String btnName) throws InterruptedException {
        // Given

        // When
       trainingPage.clickOnButton(btnName);
    }

    private static final int GROUP_1 = 0, GROUP_2 = 1, GROUP_3 = 2;

    @Step("Get all the group names created")
    public void getAllGroupNamesCreated() {
        // Given

        // When
        List<String> getAllGroupNames = liDetailsPage.getAllGroupNames();

        // Then

        // Given
        group1 = getAllGroupNames.get(GROUP_1);
        group2 = getAllGroupNames.get(GROUP_2);
        group3 = getAllGroupNames.get(GROUP_3);
        System.out.println("group1=" + group1 + "2-" + group2 + "3-" + group3);
    }

    @Step("User landed to <second> higher priority staging group")
    public void nextGroup(String groupName) {
        // Given
        String group_1 = group1.replace("_", " ");
        String group_2 = group2.replace("_", " ");
        String group_3 = group3.replace("_", " ");

        // When

        // Then
        if (groupName.equals("First")) {
            assertThat(trainingPage.getGroupNameBreadCrum()).isEqualToIgnoringCase("Train bot " + group_1);
        } else if (groupName.equals("Second")) {
            assertThat(trainingPage.getGroupNameBreadCrum()).isEqualToIgnoringCase("Train bot " + group_2);
        } else if (groupName.equals("Third")) {
            assertThat(trainingPage.getGroupNameBreadCrum()).isEqualToIgnoringCase("Train bot " + group_3);
        }
    }

    @Step("Select <value> textbox extracted with text <Type>")
    public void selectDrawbox(String drawBox, String textValue) {
        // Given

        // When
        trainingPage.selectDrawBox(drawBox, textValue);
    }

    @Step("User can see <First> group is on <Production>")
    public void checkGroupEnvironment(String groupName, String environment) {
        // Given
        String groupNumber;
        if (groupName.equals("First")) {
            groupNumber = group1;

        } else if (groupName.equals("Second")) {
            groupNumber = group2;

        } else {
            groupNumber = group3;

        }
        // When

        // Then
        assertThat(botPage.getGroupName()).isEqualToIgnoringCase(groupNumber);
        if (environment.equals("Production")) {
            assertThat(botPage.getEnvironment(environment, groupNumber, 1).isEnabled()).isTrue();
        } else {
            assertThat(botPage.getEnvironment(environment, groupNumber, 1).isEnabled()).isFalse();
        }
    }

    @Step("Move <first> group from <Production> to staging")
    public void moveGroupProductionToStaging(String groupName, String environment) {
        // Given
        String groupNumber;

        if (groupName.equals("First")) {
            groupNumber = group1;

        } else if (groupName.equals("Second")) {
            groupNumber = group2;

        } else {
            groupNumber = group3;

        }

        // When
        WebElement element = botPage.getEnvironment(environment, groupNumber, 1);
        element.click();
        // Then
    }

    @Step("Click on <edit> icon of <first> group")
    public void clickOnEditIcon(String editIcon, String groupName) {
        // Given
        int row = 1;

        // When
        botPage.clickOnEditButton(row);
    }

    @Step("Select <Tax> from <Primary Column> Drop Down")
    public void selectFromDropDown(String value, String dropDownName) {
        // Given

        // When
        trainingPage.selectValueFromDropDown(value, dropDownName);

    }

    @Step("Rename <Label> textbox value <extractedValue> to <Qty>")
    public void editLabelTextBoxValue(String labelText, String extractedValue, String newValue) {
        // Given

        // When
        trainingPage.renameLabelValue(extractedValue, newValue);
    }

    @Step("Click on <down arrow> <field> options")
    public void clickOnArrowFieldOptions(String arrow, String field) {
        // Given

        // When
        trainingPage.cliclOnDownArrowFieldOptions();

    }

    @Step("Wait for <text> to be extracted")
    public void waitForTextToBeExtracted(String textValue) {
        baseClass.waitForTextToBeExtracted(textValue);

    }

    @Step("Click on radio button of <Continue>")
    public void clickOnRadioButton(String radionButtonnContinue) {
        // Given

        // When
        trainingPage.selectRadioButton(radionButtonnContinue);

    }

    @Step("Radio button <Continue> should be selected")
    public void implementation1(String radioContinue) {
        // Given

        // When

        // Then
        System.out.println(trainingPage.GetStatusContinueRadioButton(radioContinue));
      //  assertThat(trainingPage.GetStatusContinueRadioButton(radioContinue)).isTrue();

    }

    @Step("User can see <Item Total> is default selected value of <Primary Column> dropdown")
    public void getSelectedValue(String value, String dropdownPrimaryColumn) {
        // Given

        // When

        // Then
        assertThat(trainingPage.getSelectedValue()).isEqualTo(value);
    }

    @Step("Enter <First> group name on search textbox")
    public void enterGroupName(String groupName) {
        // Given
        if (groupName.equals("First")) {
            groupName = group1;

        } else if (groupName.equals("Second")) {
            groupName = group2;

        } else {
            groupName = group3;

        }

        // When
        botPage.searchForInstanceName(groupName);
    }

    //----------- End of SaveBot steps-----------------------
}
