package implementation;

import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.openqa.selenium.support.PageFactory;
import pageobjects.*;
import utils.TestBase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import static com.thoughtworks.gauge.Gauge.writeMessage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertEquals;


public class StepImplementation {

    private LoginPage login = PageFactory.initElements(Driver.webDriver, LoginPage.class);
    private LearningInstanceListingPage liListingPage = PageFactory.initElements(Driver.webDriver, LearningInstanceListingPage.class);
    private CreateNewInstancePage newInstance = PageFactory.initElements(Driver.webDriver, CreateNewInstancePage.class);
    TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    String error, loginUserName, loginPassword, filesCount_Before_EditInstance, filesCount_After_EditInstance;
    String instDesc, domainLanguage, domainType, file;
    String smokeTestDocumentsFolder = System.getenv("test_Documents_FolderPath");

    @ContinueOnFailure
    @Step("Then logged in user name as <username> should be displayed")
    public void validateLogoutButton(String username) throws InterruptedException {
        System.out.println("Signed in as " + username + ".");
        // Given

        // When

        // Then
        assertThat(liListingPage.getLoggedinusername()).isEqualTo("Signed in as " + username + ".");
    }

    @ContinueOnFailure
    @Step("When user click on logout button")
    public void clickOnLogoutButton() throws InterruptedException {
        // Given

        // When
        liListingPage.clickOnlogout();
    }

    @ContinueOnFailure
    @Step("Ensure user should be logout and landed to login page")
    public void validateLogoutFunctionality() throws InterruptedException {
        // Given

        // When
        baseClass.waitForElementDisplay(login.getLoginButton());
        Thread.sleep(3000);

        // Then
        assertThat(Driver.webDriver.getTitle()).contains("Login | IQ Bot");

        writeMessage("Page title is %s", Driver.webDriver.getTitle());
    }

    @ContinueOnFailure
    @Step({"Then user should be landed to <pageHeading> page", "User Should be landed to <newInstance> page"})
    public void validateNewInstancePage(String pageHeading) throws InterruptedException {
        // Given

        // When

        // Then
        assertThat(newInstance.getNewInstancePageTitle()).isEqualTo(pageHeading);
    }

    @ContinueOnFailure
    @Step("When instance is created then it should be listed in IQBot Lite command & Success and Invalid path should be listed in IQBot Lite command")
    public void launchTaskEditorAndValidateIQBotLiteCommand() throws Exception {
        // Given

        // When
        baseClass.runBatchFile(System.getenv("iqBotLiteVerificationBatchFile"));
        String results, output_Path, success_Path, invalid_Path, expectedResults;
        results = baseClass.validateDesignerLogs(System.getenv("iqBotLiteLogFile"));
        output_Path = System.getenv("output_Path") + "\\" + Driver.instName;
        success_Path = output_Path + "\\" + "Success";
        invalid_Path = output_Path + "\\" + "Invalid";
        expectedResults = success_Path + " " + invalid_Path;
        //System.out.println("Expected Results: " + expectedResults);
        //System.out.println("Actual Results: " + results);

        // Then
        assertThat(results).isEqualTo(expectedResults);

    }

    @ContinueOnFailure
    @Step("Check output folder for created instance and validate unclassified files")
    public void validateOutputFolder() throws Exception {
        // Given

        // When
        baseClass.runBatchFile(System.getenv("outputFolderVerificationBatchFile"));
        String results = baseClass.validateDesignerLogs(System.getenv("outputFolderLogFile"));

        // Then
        assertThat(results).isEqualTo("Output Folder Creation : Pass : Output File Creation : Pass");
    }

    @ContinueOnFailure
    @Step("Validate total number of files, error count and image view in validator")
    public void verifyFilesInValidator() throws Exception {
        // Given

        // When
        baseClass.runBatchFile(System.getenv("validatorVerificationBatchFile"));
        String results = baseClass.validateDesignerLogs(System.getenv("validatorLogFile"));

        // Then
        assertThat(results).isEqualTo("File Count : Pass : Error Count : Pass : Image Load : Pass");

    }

    //.....................................................................................
    //.....................................................................................
    @ContinueOnFailure
    @Step("When instance is created then it should be listed in IQBot Lite command & Success and Invalid path should be listed in IQBot Lite command sanity")
    public void launchTaskEditorAndValidateIQBotLiteCommandSanity() throws Exception {
        // Given

        // When
        baseClass.runBatchFile(System.getenv("iqBotLiteVerificationBatchFile"));
        String results, output_Path, success_Path, invalid_Path, expectedResults;
        results = baseClass.validateDesignerLogs(System.getenv("iqBotLiteLogFile"));
        output_Path = System.getenv("output_Path") + "\\" + Driver.instName;
        success_Path = output_Path + "\\" + "Success";
        invalid_Path = output_Path + "\\" + "Invalid";
        expectedResults = success_Path + " " + invalid_Path;
        System.out.println("Expected Results: " + expectedResults);
        System.out.println("Actual Results: " + results);

        // Then
        //  assertThat(results).isEqualTo(expectedResults);

    }

    @ContinueOnFailure
    @Step("Upload multiple documents to production from IQBot lite command for <scenario>")
    public void uploadProductionDocuments(String scenario) throws Exception {
        // Given

        // When

        // Then
        if (scenario.equalsIgnoreCase("Machine Learning"))
            baseClass.runBatchFile(System.getenv("uploadProductionDocumentBatchFile_MachineLearning"));
        else if (scenario.equalsIgnoreCase("Validator")) {
            baseClass.runBatchFile(System.getenv("uploadProductionDocumentBatchFile_Validator"));
        }

    }

    @Step("Stop <serviceName> service")
    public void stopIQBotService(String serviceName) throws IOException, InterruptedException {
        String commandLine = "\"" + System.getenv("stopIQBotServiceBatchFile") + "\"" + " " + "\"" + serviceName;
        System.out.print("COMMAND LINE: " + commandLine);
        baseClass.runBatchFile(commandLine);
    }

    @Step("Start <serviceName> service")
    public void startIQBotService(String serviceName) throws IOException, InterruptedException {
        String commandLine = "\"" + System.getenv("startIQBotServiceBatchFile") + "\"" + " " + "\"" + serviceName;
        System.out.print("COMMAND LINE: " + commandLine);
        baseClass.runBatchFile(commandLine);
    }

    @Step("Get group id for <training>")
    public void getClassificationIdForUploadedDocuments(String groupType) throws SQLException {
        baseClass.setGroupId(groupType);
    }

    @Step("User should be redirected to login page")
    public void getLoginPageText() {
        // Given

        // When

        // Then
        assertThat(login.getPageText()).isEqualTo("Log in");
    }

    @Step("Try to navigate to <Dashboard> page")
    public void navigateToPage(String page) {
        // Given
        String url;
        if (page.equals("validations")) {
            url = Driver.webDriver.getCurrentUrl() + "/validations";

        } else {
            url = Driver.webDriver.getCurrentUrl().replace("validations", page);
        }
        // When
        Driver.webDriver.navigate().to(url);
        System.out.println(url);
    }

    @Step("Delete user <NewUser>")
    public void deleteUser(String userName) throws SQLException {
        // Given

        // When
        baseClass.deleteUser(userName);

    }

    @Step("Navitage to url <urlpath>")
    public void navigateTo(String url) {
        // Given

        // When
        Driver.webDriver.navigate().to(url);
    }

    @Step("Error Message <errorMessage> should be displayed")
    public void errorMessage(String errorMessage) {
        // Given

        // When

        // Then
        assertThat(liListingPage.getPageErrorMessage().getText()).isEqualTo(errorMessage);

    }

    @Step("Check health")
    public void healthCheckUp() {
        // Given
        String expectedSuccessValue = "\"success\":true";
        String expectedAliasValue = "\"name\":\"Alias\",\"statusCode\":200";
        String expectedFileManagerValue = "name\":\"FileManager\",\"statusCode\":200";
        String expectedProjectValue = "\"name\":\"Project\",\"statusCode\":200";
        String expectedVisionBotValue = "\"name\":\"VisionBot\",\"statusCode\":200";
        String expectedValidatorValue = "\"name\":\"Validator\",\"statusCode\":200";
        String expectedReportValue = "\"name\":\"Report\",\"statusCode\":200";

        // When

        // Then
        assertThat(login.getBodyText())
            .contains(expectedSuccessValue)
            .contains(expectedAliasValue)
            .contains(expectedFileManagerValue)
            .contains(expectedProjectValue)
            .contains(expectedVisionBotValue)
            .contains(expectedValidatorValue)
            .contains(expectedReportValue);
    }

    @Step("Check heartbeat for <name>")
    public void checkHeartBeat(String portNumber) {
        // Given
        String expectedResult;
        switch (portNumber) {
            case "File Manager":
                // Given
                expectedResult = "Application: filemanager\n" +
                        "Status: OK\n" +
                        "Reason: OK\n" +
                        "Version: 1.2.0-SNAPSHOT";
                // When

                // Then
                assertThat(login.getBodyText().substring(0, login.getBodyText().indexOf("Branch:") - 1)).isEqualTo(expectedResult);
                break;

            case "Alias":
                // Given
                expectedResult = "Application: alias\n" +
                        "Status: OK\n" +
                        "Reason: OK\n" +
                        "Version: 1.2.0-SNAPSHOT";

                // When

                // Then
                assertThat(login.getBodyText().substring(0, login.getBodyText().indexOf("Branch:") - 1)).isEqualTo(expectedResult);
                break;
            case "VisionbotManager":
                // Given
                expectedResult = "Application: visionbotmanager\n" +
                        "Status: OK\n" +
                        "Reason: OK\n" +
                        "Version: 1.2.0-SNAPSHOT";

                // When

                // Then
                assertThat(login.getBodyText().substring(0, login.getBodyText().indexOf("Branch:") - 1)).isEqualTo(expectedResult);
                break;
            case "Project Instance":
                // Given
                expectedResult = "Application: project\n" +
                        "Status: OK\n" +
                        "Reason: OK\n" +
                        "Version: 1.2.0-SNAPSHOT";

                // When

                // Then
                assertThat(login.getBodyText().substring(0, login.getBodyText().indexOf("Branch:") - 1)).isEqualTo(expectedResult);
                break;


        }
    }

    @Step("Wait for Services to start")
    public void waitForServiceToStart() throws InterruptedException {
        TimeUnit.SECONDS.sleep(Long.parseLong(System.getenv("Wait_Time")));

    }
}
