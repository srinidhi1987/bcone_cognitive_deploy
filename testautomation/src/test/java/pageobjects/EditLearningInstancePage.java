package pageobjects;

import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DbConnect;
import utils.TestBase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EditLearningInstancePage {

    // Edit Instance Button
    @FindBy(how = How.XPATH, using = "//a[@class='aa-link aa-grid-row vertical-center']")
    public WebElement editInstanceButton;

    //Cancel Button on Edit instance page
    @FindBy(how = How.XPATH, using = "//span[@class='icon fa fa-close icon--animate-none']/following::span[text()='Cancel']")
    private WebElement cancelButtonOnEditInstance;


    // Save Button
    @FindBy(how = How.XPATH, using = "//div[@class='commandbutton commandbutton--theme-success commandbutton--recommended']//button")
    public WebElement saveButton;


    //Cancel button on POP_UP
    @FindBy(how = How.XPATH, using = "//button[@name='cancel']")
    private WebElement cancelOnPopup;

    //Yes button to save on POP_UP
    @FindBy(how = How.XPATH, using = "//button[@name='accept']")
    public WebElement yesToSaveOnPopup;

    //Description text field on Edit
    @FindBy(how = How.XPATH, using = "//input[@name='description']")
    private WebElement descriptionOnEdit;

    //Browse button on edit
    @FindBy(how = How.XPATH, using = "//button[@type='button']//div[text()='Browse...']")
    private WebElement browseButton;

    //learning instance page lavel
    @FindBy(how = How.XPATH, using = "//div[@class='breadcrumbs']//a[@href='/learning-instances']")
    private WebElement pageLebelLIListingPage;

    //click on learning instance on LI listing page
    @FindBy(how = How.XPATH, using = "//td//a[@class='aa-link underline']")
    private WebElement learlingInstanceLink;


    // message on UI
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'app-notification-container')]//div")
    private WebElement messagePopup;

    //add custom field path for type field name
    @FindBy(how = How.XPATH, using = "//input[@name='newCustomFieldName']")
    private WebElement customFieldTextBox;


    //custom form field button path
    @FindBy(how = How.XPATH, using = "(//div[contains(@class,'commandbutton commandbutton--theme-default commandbutton--recommended')])[2]")
    public WebElement customFormFieldButton;

    //custom table field button
    @FindBy(how = How.XPATH, using = "(//div[contains(@class,'commandbutton commandbutton--theme-default commandbutton--recommended')])[3]")
    public WebElement customTableFieldButton;

    //method for type your new additional field
    public void typeCustomFieldName(String fieldName) throws Exception {
        customFieldTextBox.sendKeys(fieldName);

    }

    //message pop-up
    public String messageAfterSaveYes() throws Exception {
        return messagePopup.getText().trim();
    }

    //get page label for LI Listing page
    public String pageLabeLIListing() throws Exception {
        return pageLebelLIListingPage.getText().trim();
    }


    public void clickOnInstaceLink() throws Exception {
        Thread.sleep(3000);
        if (Driver.instName.equalsIgnoreCase(learlingInstanceLink.getText().trim())) {
            learlingInstanceLink.click();
        }
    }

    // All standard form fields and table fields path
    public String fieldsPath(String fieldsName) throws Exception {
        String path1 = "//DIV[@class='checkboxinput-content'][text()='";
        String pathLast = "']/preceding-sibling::label[@class='checkboxinput-container']//span";
        String path = path1 + fieldsName + pathLast;
        return path;
    }

    //Click on checkbox for standard form and table fields
    public void clickOnCheckBox(String fieldsName) throws Exception {
        Driver.webDriver.findElement(By.xpath(fieldsPath(fieldsName))).click();

    }

    WebDriverWait wait = new WebDriverWait(Driver.webDriver, 30);

    public void addACustomField(String fieldName, String fieldType) throws Exception {
        if (fieldType.equalsIgnoreCase("form")) {
            System.out.println("===================>>>>>" + fieldName);
            System.out.println("===================>>>>>" + fieldType);
            typeCustomFieldName(fieldName);
            By locator = By.xpath("(//div[contains(@class,'commandbutton commandbutton--theme-default commandbutton--recommended')])[2]");
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            customFormFieldButton.click();
            Thread.sleep(3000);
        } else if (fieldType.equalsIgnoreCase("table")) {
            System.out.println("===================>>>>>" + fieldName);
            System.out.println("===================>>>>>" + fieldType);
            typeCustomFieldName(fieldName);
            By locator = By.xpath("(//div[contains(@class,'commandbutton commandbutton--theme-default commandbutton--recommended')])[3]");
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            customTableFieldButton.click();
            Thread.sleep(3000);
        } else {
            System.out.println("Fields type no found" + fieldType);
        }
    }
        // Get group id for uploaded documents

    public void setGroupId(String groupType) throws SQLException {
        String classificationId = null;
        String addFieldsGroupId = null;
        String groupId = null;

        switch (groupType) {
            case "addFieldsGroup":
                addFieldsGroupId = String.format("SELECT classificationid FROM [FileManager].[dbo].[FileDetails]" +
                        " WHERE projectid IN (SELECT Id FROM [FileManager].[dbo].[ProjectDetail] WHERE Name='%s')" +
                        " AND filename='Sample_Invoice Documents.pdf'", Driver.instName);
        }

        Statement sqlQueryStatement = DbConnect.connect().createStatement();
        ResultSet queryResult = sqlQueryStatement.executeQuery(addFieldsGroupId);
        while (queryResult.next()) {
            classificationId = queryResult.getString(1);
        }
        assert classificationId != null;
        groupId = "Group_" + classificationId;
        System.out.println("XXXXXXXXXXXXXGROUPIDXXXXXXXX"+groupId);
        switch (groupType) {
            case "addFieldsGroup":
                Driver.addFieldsGroupName = groupId;
                break;

        }
        DbConnect.connection.close();
    }


}
