package pageobjects;

import driver.Driver;
import implementation.MigrationUtilityImplementation;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utils.TestBase;

import java.util.ArrayList;
import java.util.List;

public class MigrationUtilityPage {

    TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);
    //no instances available on UI, text path
    @FindBy(how = How.XPATH, using = "//span[text()='No current learning instances.']")
    private WebElement noInstancesAvailableText;

    //page lebel check
    @FindBy(how = How.XPATH, using = "//a[@href='/migration-utility' or text()='Migration Utility']")
    private WebElement migrationPageLebel;

    //page lebel check above instance label
    @FindBy(how = How.XPATH, using = " //div[@title='Migration Utility']")
    private WebElement migrationPageLebelAboveTable;


    //import button
    @FindBy(how = How.XPATH, using = "(//button)[3]")
    private WebElement importButton;

    //Error message path without select any .iqba file click on I understand , please import
    @FindBy(how = How.XPATH, using = "//div[@class='announcement-title']")
    private WebElement errorMessageImport;

    //Export button
    @FindBy(how = How.XPATH, using = "(//button)[4]")
    private WebElement exportButton;

    @FindBy(how = How.XPATH, using = "//DIV[@class='datatable-header']")
    private WebElement header;

    //Check box path for select all
    @FindBy(how = How.XPATH, using = "(//label[@class='checkboxinput-container'])[1]")
    private WebElement selectAllCheckBox;

    //pop-up text box
    @FindBy(how = How.XPATH, using = "//input[@name='confirmation-input-name']")
    private WebElement popupTextBox;


    //paragraph export inprogress text
    @FindBy(how = How.XPATH, using = "//p")
    private WebElement exportInProgressText;

    //paragraph export inprogress text
    @FindBy(how = How.XPATH, using = "//div[@class='message-title']")
    private WebElement exportPopUptitle;

    // text box error message
    @FindBy(how = How.XPATH, using = "//DIV[@class='announcement-title']")
    private WebElement textBoxError;

    //pop-up cancel button
    @FindBy(how = How.XPATH, using = "//button[@name='cancel']")
    private WebElement popupCancelButton;

    //error message on pop-up
    @FindBy(how = How.XPATH, using = "//div[@class='announcement-title']")
    private WebElement errorMessageOnPopup;


    //pop-up "I understand, please export button"
    @FindBy(how = How.XPATH, using = "//button[@name='accept']")
    private WebElement popupAcceptButton;

    //paragraph for export inprogress state
    public String exportTextPara(){
        return  exportInProgressText.getText();
    }

    public String exportPopUPLabel(){
        return  exportPopUptitle.getText();
    }

    public String textBoxError(){
        return  textBoxError.getText();
    }

    public String textForNoInstance(){
        return  noInstancesAvailableText.getText();
    }


    //Error message as text on pop-up import or export
    public String popupErrorMessage(){
        return  errorMessageOnPopup.getText().trim();
    }


    //type to file name
    public void typeFileName(String fileName){
        baseClass.explicitWait("//input[@name='confirmation-input-name']");
        popupTextBox.clear();
        popupTextBox.click();
        popupTextBox.sendKeys(fileName);
    }

    public String typeFileNameValue(){
        return popupTextBox.getAttribute("value");
    }

    //on export progress default text
    @FindBy(how = How.XPATH, using = "//div[@class='aa-grid-row padding horizontal-center vertical-center']/p/strong")
    private WebElement defaultText;

    //on export progress, exported file path
    @FindBy(how = How.XPATH, using = "//div[@class='aa-grid-column padding']/p[1]")
    private WebElement exportfilepath;

    //on export progress Note
    @FindBy(how = How.XPATH, using = "//div[@class='aa-grid-column padding']/p[2]")
    private WebElement exportProgressNote;


    public String exportProgressDefaulttext(){
        return defaultText.getText();
    }

    public String exportfilepath(){
        return exportfilepath.getText();
    }

    public String exportProgressNote(){
        return exportProgressNote.getText();
    }



    //paragraph for export inprogress state
    public String pageLabelAboveTable(){
        return  migrationPageLebelAboveTable.getText();
    }

    public List tablelistToValidateMigrationPage(){
        List<List<String>> TableInstances = new ArrayList<List<String>>();


        int a = Integer.parseInt(totalInstancesCount().substring(11,13));
        for (int i=1; i<=a; i++) {
            List<String> EachRow = new ArrayList<>();
            String instanceName = Driver.webDriver.findElement(By.xpath("(//DIV[@class='datatable datatable--theme-default']//TR["+i+"]//TD[@class='datatable-column datatable-column--align-left'])[1]")).getText();
            String botCount = Driver.webDriver.findElement(By.xpath("(//DIV[@class='datatable datatable--theme-default']//TR["+i+"]//TD[@class='datatable-column datatable-column--align-left'])[3]")).getText();
            String State = Driver.webDriver.findElement(By.xpath("(//DIV[@class='datatable datatable--theme-default']//TR["+i+"]//TD[@class='datatable-column datatable-column--align-left'])[2]")).getText();
            String environment;
            EachRow.add(instanceName);
            EachRow.add(State);
            EachRow.add(botCount);
            TableInstances.add(EachRow);
        }
        return TableInstances;
    }




    //click on Export button state
    public String stateOfExportButton(){
        if(exportButton.isDisplayed()) {
            return exportButton.getAttribute("class");
        }
        else{
            return ("=============Export button does not exist============");
        }
    }
    //click on Export button
    public void clickOnExportButton(){
        if(exportButton.isDisplayed()) {
            exportButton.click();
        }
        else{
            System.out.println("=============Export button============");
        }
    }

    //click on cancel button
    public void clickOnCancelButton(){
        if(popupCancelButton.isDisplayed()) {
            popupCancelButton.click();
        }
        else{
            System.out.println("=============Cancel button is not available============");
        }
    }



    //click on accept button
    public void clickOnAcceptButton(){
        if(popupAcceptButton.isDisplayed()) {
            popupAcceptButton.click();
        }
        else{
            System.out.println("============Accept button is not available============");
        }
    }
    //click on Import button
    public void clickOnImportButton(){
        importButton.click();
    }

    //to str Notification on success or error
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'app-notification-container')]")
    private WebElement toastrNotification;

    public String toastrNotification(){
        return toastrNotification.getText();
    }


    //instance count
    @FindBy(how = How.XPATH, using = "(//div[@class='aa-flex-grid-item'])[1]")
    private WebElement instanceCount;
    public String totalInstancesCount(){
        return instanceCount.getText();
    }

    //page lebel text
    public String pageLebelText(){
        return migrationPageLebel.getText().trim();
    }

    //click on page lebel
    public void clickOnPageLebel(){
        migrationPageLebel.click();
    }




    //Select Check box for All select operation
    public void exportTableSelectAllCheckBox()
    {
        selectAllCheckBox.click();
    }

    //Select Check box for All select operation
    public void exportTableCheckBoxAction(int i)
    {
        WebElement checkBox = Driver.webDriver.findElement(By.xpath("//DIV[@class='datatable datatable--theme-default']//TR["+i+"]//TD[1]"));
        checkBox.click();
    }
    //Verification of Check box so see if it is selected or not
    public String exportTableCheckBoxVerification(int i)
    {
        WebElement checkBox1 = Driver.webDriver.findElement(By.xpath("//DIV[@class='datatable datatable--theme-default']//TR["+i+"]"));
        return checkBox1.getAttribute("class");
    }

    //path for a particular element in table
    public String exportTablePath(int rowNum,String columnName)
    {
        String path1="(//td[contains(@class,'datatable-column')][";
        String path2="])[";
        String last="]";
        String fullPath=path1+exportTableColumnNumber(columnName)+path2+rowNum+last;
        // System.out.println(fullPath);
        return fullPath;
    }

    //for select a particular column
    public int exportTableColumnNumber(String columnName) {
        switch (columnName) {
            case "Check_Boxes":
                return 1;
            case "Instance_Name":
                return 2;
            case "Status":
                return 3;
            case "#of Bots":
                return 4;
            default:
                return 0;
        }
    }

    //row count in migrationUtility page instances list table
    By rowCountMigrationUtilityPage=By.xpath(" (//td[1])");
    public int rowCount(){
        List<WebElement> list= Driver.webDriver.findElements(rowCountMigrationUtilityPage);
        return list.size();
    }

    //for a particular row number
    public int rowNumber(String passInstanceName){
        for(int i=1;i<=rowCount();i++){
            String instanceName=Driver.webDriver.findElement(By.xpath(exportTablePath(i,"Instance_Name"))).getText().trim();
            if(passInstanceName.equalsIgnoreCase(instanceName)){
                return i;
            }
        }
        return rowCount();
    }

    //for a particular instance name
    public String instanceNameChecking(int i) {
        return Driver.webDriver.findElement(By.xpath(exportTablePath(i, "Instance_Name"))).getText().trim();
    }

    static int count;
    //total Bots counts
    public int botCount(){
        count=0;
        for(int i=1;i<=rowCount();i++){
            String botText=(Driver.webDriver.findElement(By.xpath(exportTablePath(i,"#of Bots"))).getText().trim());
            int botNumber=Integer.parseInt(botText);
            count=count+botNumber;
        }
        return count;
    }
    public WebElement tableHeader(){
        return header;
    }

    public Boolean verifyVerticalScrollBar(){
        JavascriptExecutor javascript;
        javascript = (JavascriptExecutor) Driver.webDriver;
        Boolean VertscrollStatus = (Boolean) javascript.executeScript("return document.documentElement.scrollHeight>document.documentElement.clientHeight;");
        return VertscrollStatus;
    }
    public Boolean verifyHorizontalScrollBar(){
        JavascriptExecutor javascript;
        javascript = (JavascriptExecutor) Driver.webDriver;
        Boolean horzscrollStatus = (Boolean) javascript.executeScript("return document.documentElement.scrollWidth>document.documentElement.clientWidth;");
        return horzscrollStatus;
    }




    //Click on check-box for a particular instance
    public void clickCheckBoxForAParticularInstance(String instanceName) {
        List<WebElement> checkBoxes = Driver.webDriver.findElements(By.xpath("(//label[@class='checkboxinput-container'])"));
        for (int i = 1; i < checkBoxes.size(); i++) {
            if (instanceName.equalsIgnoreCase("All")) {
                if (checkBoxes.get(0).isSelected()) {
                    checkBoxes.get(0).clear();
                    checkBoxes.get(0).click();
                    break;
                } else {
                    checkBoxes.get(0).click();
                    break;
                }
            } else
            if (instanceName.equalsIgnoreCase(instanceNameChecking(i))) {
                System.out.println("trying to checkbox select or deselect");
                Driver.webDriver.findElement(By.xpath(exportTablePath(i,"Check_Boxes"))).click();
                break;
            } else {

                System.out.println("=====Instance name is not found======");

            }
        }
    }

    //Check box paths on import pop-up
    public String importFilecheckboxPath()throws Exception{
        String path1="//span[@class='radioinput-label' and text()='";
        String last=".iqba']";
        String fullPath=path1+ MigrationUtilityImplementation.exportFilename.trim()+last;
        return fullPath;
    }


    public static String exportFileName(){
        String text=Driver.webDriver.findElement(By.xpath("(//span[@class='path'])[1]")).getText();
        //String str=text.replace("\\","99");
        //String[] stringPath=str.split("99");
        return text;
    }


    //Select a check box on import pop-up
    public void clickOnCheckbox(String fileName)throws  Exception {
        List<WebElement> list = Driver.webDriver.findElements(By.xpath("//span[@class='radioinput-label']"));
        System.out.println("Total number of export file to import ===> " + list.size());
        System.out.println("Export file name in specification ===> " + fileName);
        Driver.webDriver.findElement(By.xpath(importFilecheckboxPath())).click();
    }

}
