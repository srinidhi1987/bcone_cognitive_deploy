package pageobjects;


import com.thoughtworks.gauge.Step;
import driver.Driver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utils.TestBase;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TrainingPage extends TestBase {

    private TestBase baseClass = PageFactory.initElements(Driver.webDriver, TestBase.class);

    //----------------------------------------------------COG-2879------------------------------------------------------
    @FindBy(how = How.XPATH, using = "//div[@class='breadcrumbs-breadcrumb'][3]")
    private WebElement breadcrumbTrainBotGroupName;
    //----------------------------------------------------COG-2880------------------------------------------------------
    @FindBy(how = How.XPATH, using = "//div[@class='field']//input")
    private WebElement fields;
    //----------------------------------------------------COG-2881------------------------------------------------------
    @FindBy(how = How.XPATH, using =
            "//button[@class='commandbutton-button commandbutton-button--clickable']//div[text()='Finish Later']")
    private WebElement finishButton;

    @FindBy(how = How.XPATH, using = "(//div[@class='right-view-body-fields-row']//label[text()='Type'])[2]")
    private WebElement fieldType;

    @FindBy(how = How.XPATH, using = "//div[@class='right-view-body-fields-row']//label[text()='Label']")
    private WebElement label;

    @FindBy(how = How.XPATH, using = "//div[@class='right-view-body-fields-row']//label[text()='Value']")
    private WebElement value;

    @FindBy(how = How.XPATH, using = "//span[text()='Field Options']")
    private WebElement fieldOptions;

    @FindBy(how = How.XPATH, using = "//div[@class='right-view-body-fields-row']//label[text()='Value Formula']")
    private WebElement fieldValueFormula;

    @FindBy(how = How.XPATH, using = "(//div[@class='designer-class aa-grid-row designer-class--active']//select)[2]")
    private WebElement fieldTypesDropDown;

    @FindBy(how = How.XPATH, using = "(//div[@class='aa-grid-row']//img)[1]")
    private WebElement labelPicker;

    @FindBy(how = How.XPATH, using = "(//div[@class='aa-grid-row']//img)[2]")
    private WebElement valuePicker;

    //----------------------------------------------------COG-3176------------------------------------------------------
    @FindBy(how = How.XPATH, using =
            "//div[@class='right-view-body-fields-row']//label[text()='Label']//div//div//input")
    private WebElement fieldLabelBox;

    @FindBy(how = How.XPATH, using =
            "//div[@class='right-view-body-fields-row']//label[text()='Value']//div//div//input")
    private WebElement fieldValueBox;

    // Domain DropDown
    @FindBy(how = How.XPATH, using =
            "(//div[@class='right-view-body-fields-row']//label[text()='Type']//div//select)[2]")
    private WebElement typeDropDown;

    // Required filed checkbox
    @FindBy(how = How.XPATH, using = "(//div[@class='radioinput-control'])[1]")
    private WebElement requiredFieldCheckBox;

    // Optional filed checkbox
    @FindBy(how = How.XPATH, using = "(//div[@class='radioinput-control'])[2]")
    private WebElement optionalFieldCheckBox;

    // Validation patterns checkbox
    @FindBy(how = How.XPATH, using = "//div[@class='checkboxinput-control']")
    private WebElement validatePatternCheckBox;

    // Starts with validation pattern label
    @FindBy(how = How.XPATH, using = "//label[@class='validation-options' and text()='Starts With']")
    private WebElement startsWithValidatePatternLabel;

    // Starts with validation pattern region
    @FindBy(how = How.XPATH, using = "//label[@class='validation-options' and text()='Starts With']//input")
    private WebElement startsWithValidatePatternValue;

    // Ends with validation pattern label
    @FindBy(how = How.XPATH, using = "//label[@class='validation-options' and text()='Ends With']")
    private WebElement endsWithValidatePatternLabel;

    // Ends with validation pattern region
    @FindBy(how = How.XPATH, using = "//label[@class='validation-options' and text()='Ends With']//input")
    private WebElement endsWithValidatePatternValue;

    // Contains with validation pattern label
    @FindBy(how = How.XPATH, using = "//label[@class='validation-options' and text()='Contains']")
    private WebElement containsValidatePatternLabel;

    // Contains with validation pattern region
    @FindBy(how = How.XPATH, using = "//label[@class='validation-options' and text()='Contains']//input")
    private WebElement containsValidatePatternValue;

    // Field options required fields default value box
    @FindBy(how = How.XPATH, using = "//label[@class='not-required-default']//input")
    private WebElement optionalFieldDefaultValueElement;

    // Field options dropdown button
    @FindBy(how = How.XPATH, using =
            "//span[@class='icon fa fa-angle-double-left icon--animate-none double-chevron-down']")
    private WebElement fieldOptionsDropDown;

    // Field label picker
    @FindBy(how = How.XPATH, using = "(//img[@class='select-draw-icon'])[1]")
    private WebElement fieldLabelPicker;

    // Field value picker
    @FindBy(how = How.XPATH, using = "(//img[@class='select-draw-icon'])[2]")
    private WebElement fieldValuePicker;

    // First SIR
    By firstSIR = By.xpath("//*[name()='svg']/*[name()='g']/*[name()='rect' and @class='hoverable-field'][1]");

    //----------------------------------------------------COG-3880------------------------------------------------------
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Finish Later')]//parent :: button")
    private WebElement btnFinish;
    By btnFinishBy = By.xpath("//div[contains(text(),'Finish Later')]//parent :: button");
    @FindBy(how = How.XPATH, using = "//img[@src='/images/zoom-in.svg']")
    private WebElement iconZoomIn;
    @FindBy(how = How.XPATH, using = "//SPAN[text()='Zoom in']")
    private WebElement btnZoomIn;
    @FindBy(how = How.XPATH, using = "//img[@src='/images/zoom-out.svg']")
    private WebElement iconZoomOut;
    @FindBy(how = How.XPATH, using = "//SPAN[text()='Zoom out']")
    private WebElement btnZoomOut;
    @FindBy(how = How.XPATH, using = "//IMG[@src='/images/zoom-reset.svg']")
    private WebElement iconResize;
    @FindBy(how = How.XPATH, using = "//SPAN[text()='Fit to screen']")
    private WebElement btnFitToScreen;
    @FindBy(how = How.XPATH, using = "//*[@id=\"app\"]/div[1]/div/main/div/div[1]/div[2]/a")
    private WebElement instanceNameBreadCrum;
    @FindBy(how = How.XPATH, using = "//*[@id=\"app\"]/div/div/main/div/div[1]/div[3]/a")
    private WebElement groupNameBreadCrum;
    @FindBy(how = How.XPATH, using = "//BUTTON[@class='previous-next-button'][contains(text(),'Next')]")
    private WebElement nextButton;
    @FindBy(how = How.XPATH, using = "//DIV[@class='designer']")
    private WebElement designer;
    //----------------------------------------------------COG-4267------------------------------------------------------
    @FindBy(how = How.XPATH, using = "(//div[@class='right-view-body-fields-row']//img)[1]")
    private WebElement fieldTrainedIndicator;
    @FindBy(how = How.XPATH, using = "//DIV[@class='designer']")
    private WebElement fieldNotTrained;

    // Check box for field label not available
    @FindBy(how = How.XPATH, using = "//span[@class='checkboxinput-label' and text()='Does not have a field label']")
    private WebElement fieldLabelNotAvailableCheckbox;

    //---------------------------- COG-3883 Save bot-------------------------------------------
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Save and close')]//parent :: button")
    private WebElement btnSaveAndClose;
    @FindBy(how = How.XPATH, using = "//div[@class='modal-content']")
    private WebElement modalPopUp;
    @FindBy(how = How.XPATH, using = "(//DIV[@class='gridlayout-row'])[2]")
    private List<WebElement> modalPopUpOptions;
    @FindBy(how = How.XPATH, using = "//button[contains(.,'Cancel')]/parent::div")
    private WebElement modalPopUpCancelButton;
    @FindBy(how = How.XPATH, using = "(//BUTTON[@type='button'])[6]")
    private WebElement modalPopUpSaveButton;
    @FindBy(how = How.XPATH, using = "//button[contains(.,'Save and send to production')]/parent::div")
    private WebElement modalPopUpSaveAndSendButton;
    @FindBy(how = How.XPATH, using = "//IMG[@src='/images/status-success.svg']")
    private WebElement successFlag;
    @FindBy(how = How.XPATH, using = "//IMG[@src='/images/status-todo.svg']")
    private WebElement unSuccessFlag;
    @FindBy(how = How.XPATH, using = "//div[@class='designer-page-wizard-menu']")
    private WebElement formandTableNames;
    @FindBy(how = How.XPATH, using = "//div[@class='designer-class aa-grid-row designer-class--active']//select")
    private WebElement primaryCloumnDD;
    @FindBy(how = How.XPATH, using = "//SPAN[@class='icon fa fa-angle-double-left icon--animate-none double-chevron-down']")
    private WebElement downArrowFieldOptions;
    @FindBy(how = How.XPATH, using = "//LABEL[@class='radioinput radioinput--interactive']")
    private WebElement radioButtonContinue;
    @FindBy(how = How.XPATH, using = "//LABEL[@class='radioinput radioinput--interactive radioinput--checked']")
    private WebElement checkedRadioButton;



    //----------------------------------------------------COG-2879------------------------------------------------------
    public String getTrainBotGorupNameBreadcrumbtext() {
        return breadcrumbTrainBotGroupName.getText();
    }

    //----------------------------------------------------COG-2880------------------------------------------------------
    public int getAvailableFieldsCount() {
        List<WebElement> field = Driver.webDriver.findElements(By.xpath("//div[@class='field']//div"));
        System.out.println(field.size() / 3);
        return field.size() / 3;
    }

    public String webDesignerFieldPath(String FieldName) {
        String part1 = "//div[@class='designer-page-wizard-fields']//div[text()='";
        String part2 = "']";
        String path = part1 + FieldName + part2;
        return path;
    }

    public boolean isFieldFound(String FieldName) {
        String FieldXpath = webDesignerFieldPath(FieldName);
        Driver.webDriver.findElement(By.xpath(FieldXpath)).click();
        boolean b = Driver.webDriver.findElement(By.xpath(FieldXpath)).isDisplayed();
        return b;
    }

    //----------------------------------------------------COG-2881------------------------------------------------------
    public void cilckOnTainingFinishButton() {
        ExpectedConditions.visibilityOf(finishButton);
        ExpectedConditions.elementToBeClickable(finishButton);
        JavascriptExecutor js = (JavascriptExecutor) Driver.webDriver;
        js.executeScript("arguments[0].click();", finishButton);
        //finishButton.click();
    }

    public void isElementFound(String element) {
        switch (element) {
            case "Type":
                Assert.assertEquals(true, fieldType.isDisplayed());
                break;
            case "Label":
                Assert.assertEquals(true, label.isDisplayed());
                break;
            case "Value":
                Assert.assertEquals(true, value.isDisplayed());
                break;
            case "Options":
                Assert.assertEquals(true, fieldOptions.isDisplayed());
                break;
            case "Formula":
                Assert.assertEquals(true, fieldValueFormula.isDisplayed());
                break;
            case "Label Picker":
                Assert.assertEquals(true, labelPicker.isDisplayed());
                break;
            case "Value Picker":
                Assert.assertEquals(true, valuePicker.isDisplayed());
                break;
            case "Validate Patterns":
                Assert.assertEquals(true, startsWithValidatePatternLabel.isDisplayed());
                Assert.assertEquals(true, startsWithValidatePatternValue.isDisplayed());
                Assert.assertEquals(true, endsWithValidatePatternLabel.isDisplayed());
                Assert.assertEquals(true, endsWithValidatePatternValue.isDisplayed());
                Assert.assertEquals(true, containsValidatePatternLabel.isDisplayed());
                Assert.assertEquals(true, containsValidatePatternValue.isDisplayed());
                break;
            default:
                System.out.println("No such element defined");
                break;
        }
    }

    public boolean isValueEnabled() {
        boolean b = false;
        try {
            b = value.isDisplayed();
        } catch (NoSuchElementException e) {
            b = false;
        }
        return b;
    }


    public boolean isValueBoxEnabled() {
        boolean b = false;
        try {
            b = fieldValueBox.isDisplayed();
        } catch (NoSuchElementException e) {
            b = false;
        }
        return b;
    }

    // Get List of Types
    public List<String> getTypeDDList() {
        ExpectedConditions.visibilityOf(fieldTypesDropDown);
        List<String> uiTypeDDList = new ArrayList<String>();
        Select selectDomainType = new Select(fieldTypesDropDown);
        for (int i = 0; i < selectDomainType.getOptions().size(); i++)
            uiTypeDDList.add(selectDomainType.getOptions().get(i).getAttribute("text"));
        return uiTypeDDList; //return of values from type DropDown List
    }

    //----------------------------------------------------COG-3176------------------------------------------------------

    // Select type for field from dropdown
    public void setType(String type) {
        Select objType = new Select(typeDropDown);
        objType.selectByVisibleText(type);
    }

    public String getType() {
        Select objType = new Select(typeDropDown);
        return objType.getFirstSelectedOption().getText();
    }

    // Gets label of selected field
    public String getFieldLabel() {
        return fieldLabelBox.getText();
    }

    public boolean isElementPresent(String element) {
        boolean b = false;
        if (element == "Label Box") {
            if (fieldLabelBox.isDisplayed()) {
                b = true;
            }
        } else if (element == "Value Box") {
            if (fieldValueBox.isDisplayed()) {
                b = true;
            }
        }
        return b;
    }

    // Sets label for selected field
    public void setFieldLabel(String valuelabel) {
        fieldLabelBox.clear();
        fieldLabelBox.sendKeys(valuelabel);
    }

    public void clearFieldLabel() {
        fieldLabelBox.clear();
    }

    // Gets value of selected field
    public String getFieldValue() {
        return fieldValueBox.getText();
    }

    // Sets value for selected field
    public void setFieldValue(String valueValue) {
        fieldValueBox.clear();
        fieldValueBox.sendKeys(valueValue);
    }

    // Select field
    public void selectField(String fieldName) {
        String fieldPath = webDesignerFieldPath(fieldName);
        ExpectedConditions.elementToBeClickable(By.xpath(fieldPath));
        Driver.webDriver.findElement(By.xpath(fieldPath)).click();

    }

    // Check required field check box
    public void checkRequiredField() {
        requiredFieldCheckBox.click();
    }

    // Check optional field check box
    public void checkOptionalField() {
        optionalFieldCheckBox.click();
    }

    // Check validate pattern check box
    public void checkValidatePattern() {
        if (isCheckBoxChecked("Validate Pattern")) {

        } else {
            JavascriptExecutor js = (JavascriptExecutor) Driver.webDriver;
            js.executeScript("arguments[0].scrollIntoView(true);", validatePatternCheckBox);
            validatePatternCheckBox.click();
        }
    }

    // Is checkbox checked
    public Boolean isCheckBoxChecked(String name) {
        Boolean b = false;
        if (name == "Required Field") {
            if (requiredFieldCheckBox.isSelected()) {
                b = true;
            }
        } else if (name == "Validate Pattern") {
            if (validatePatternCheckBox.isSelected()) {
                b = true;
            }
        }
        return b;
    }

    // Set default value of optional field
    public void setOptionalFieldDefaultValueElement(String value) {
        optionalFieldDefaultValueElement.sendKeys(value);
    }

    // Get default value of optional field
    public String getOptionalFieldDefaultValueElement() {
        return optionalFieldDefaultValueElement.getText();
    }

    // Provides concatenated list from array
    public List<String> concat(List<String[]> arrayOfData) {
        List<String> lstOfString = new ArrayList<>();
        for (String[] strArray : arrayOfData) {
            for (String str : strArray) {
                lstOfString.add(str);
            }
        }
        return lstOfString;
    }

    public Boolean isDefaultValueFieldEnabled(String fieldName) {
        Boolean b = false;
        if (fieldName == "Invoice Number") {
            selectField(fieldName);
            b = optionalFieldDefaultValueElement.isEnabled();
        } else if (fieldName == "Payment Term") {
            selectField(fieldName);
            b = optionalFieldDefaultValueElement.isEnabled();
        }
        return b;
    }

    public String getStartsWithValidatePatternValue() {
        return startsWithValidatePatternValue.getText();
    }

    public void setStartsWithValidatePatternValue(String value) {
        startsWithValidatePatternValue.sendKeys(value);
    }

    public String getEndsWithValidatePatternValue() {
        return endsWithValidatePatternValue.getText();
    }

    public void setEndsWithValidatePatternValue(String value) {
        endsWithValidatePatternValue.sendKeys(value);
    }

    public String getContainsValidatePatternValue() {
        return containsValidatePatternValue.getText();
    }

    public void setContainsValidatePatternValue(String value) {
        containsValidatePatternValue.sendKeys(value);
    }

    public void clickOnFieldOptionsDropDown() {
        fieldOptionsDropDown.click();
        ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Type']"));
    }

    public String SIRPath(String SIRId) {
        //*[name()='svg']/*[name()='g']/*[name()='rect' and @class='hoverable-field'][1]
        //todo: get accurate xpath from dev and change this method accordingly
        String path1 = "//*[name()='svg']/*[name()='g']/*[name()='rect' and @class='hoverable-field'][";
        String path2 = "]";
        return path1 + SIRId + path2;

    }

    public void clickOnSIR(String SIRId) {
        Driver.webDriver.findElement(By.xpath(SIRPath(SIRId))).click();
    }

    public void selectLabelBox() {
        fieldLabelBox.click();
    }

    public void selectValueBox() {
        fieldValueBox.click();
    }

    public void clickOnLabelPicker() {
        fieldLabelPicker.click();
    }

    public void clickOnValuePicker() {
        fieldValuePicker.click();
    }

    public void draw(int x1, int y1, int x2, int y2) {
        WebElement element = Driver.webDriver.findElement(By.xpath("(//img[@class='select-draw-icon'])[2]"));
        Actions builder = new Actions(Driver.webDriver);
        Action drawAction = builder.moveToElement(element, x1, y1)  // start point
                .clickAndHold()
                //.click()
                .moveByOffset(x2, y2) // second point
                .release()
                //.doubleClick()
                .build();
        drawAction.perform();
    }

    public By getFirstSIR() {
        return firstSIR;
    }

    public String getFinishButton() {
        return btnFinish.getText();
    }

    public String getZoomOption(String zoomOption) {
        if (zoomOption.equals("Zoom in")) {
            ExpectedConditions.visibilityOf(iconZoomIn);
            ExpectedConditions.visibilityOf(btnZoomIn);
            ExpectedConditions.elementToBeClickable(iconZoomIn);
            ExpectedConditions.elementToBeClickable(btnZoomIn);
            return btnZoomIn.getText();
        } else if (zoomOption.equals("Zoom out")) {
            ExpectedConditions.visibilityOf(iconZoomOut);
            ExpectedConditions.visibilityOf(btnZoomOut);
            ExpectedConditions.elementToBeClickable(iconZoomOut);
            ExpectedConditions.elementToBeClickable(btnZoomOut);
            return btnZoomOut.getText();
        } else {
            ExpectedConditions.visibilityOf(iconResize);
            ExpectedConditions.visibilityOf(btnFitToScreen);
            ExpectedConditions.elementToBeClickable(iconResize);
            ExpectedConditions.elementToBeClickable(btnFitToScreen);
            return btnFitToScreen.getText();
        }

    }

    public void clickOnZoomInIcon() {
        ExpectedConditions.elementToBeClickable(iconZoomIn);
        iconZoomIn.click();
    }

    public void clickOnZoomInButton() {
        ExpectedConditions.elementToBeClickable(btnZoomIn);
        btnZoomIn.click();
    }

    public void clickOnZoomOutIcon() {
        ExpectedConditions.elementToBeClickable(iconZoomOut);
        iconZoomOut.click();
    }

    public void clickOnZoomOutButton() {
        ExpectedConditions.elementToBeClickable(btnZoomOut);
        btnZoomOut.click();
    }

    public void clickOnFitToScreenIcon() {
        ExpectedConditions.elementToBeClickable(iconResize);
        iconResize.click();
    }

    public void clickOnFitToScreenText() {
        ExpectedConditions.elementToBeClickable(btnFitToScreen);
        btnFitToScreen.click();
    }

    public void clickOnInstancenameBreadCrum() {
        instanceNameBreadCrum.click();
    }

    public String getSizeOfDocument() {
        int width = Driver.webDriver.findElement(By.tagName("image")).getSize().getWidth();
        int hight = Driver.webDriver.findElement(By.tagName("image")).getSize().getHeight();
        System.out.println(width + ">>>" + hight);
        return width + ">>>" + hight;

    }

   /* public boolean checkDocumentDisplay() {
        Boolean ImagePresent = (Boolean) ((JavascriptExecutor)Driver.webDriver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", designer);
        System.out.println(ImagePresent);
        return ImagePresent;
    }*/

    // Select field type from type dropdown
    public void selectFieldType(String fieldType) {
        ExpectedConditions.visibilityOf(fieldTypesDropDown);
        ExpectedConditions.elementToBeClickable(fieldTypesDropDown);
        Select objSel = new Select(fieldTypesDropDown);
        objSel.selectByVisibleText(fieldType);
    }

    public void checkFieldLabelNotAvailable() {
        fieldLabelNotAvailableCheckbox.click();
    }

    public boolean checkAvailibilityofFieldLabelNotAvailableCheckbox() {
        boolean status;
        try {
            status = fieldLabelNotAvailableCheckbox.isDisplayed();
        } catch (NoSuchElementException e) {
            status = false;
        }
        return status;
    }

    public boolean getFieldTrainingStatus() {
        boolean status;
        if (fieldTrainedIndicator.getAttribute("src").equals(System.getenv("APP_URL") + "/images/status-success.svg"))
            status = true;
        else
            status = false;
        return status;
    }

    // ----- Save bot methods------------------------
    public boolean getSaveAndCloseStatus() {
        String classes = btnSaveAndClose.getAttribute("class");
        boolean isDisabled = classes.contains("commandbutton-button--disabled");
        return isDisabled;
    }

    public int countNotMappedFields() {
        int iCount = 0;
        iCount = Driver.webDriver.findElements(By.xpath("//IMG[@src='/images/status-todo.svg']")).size();
        return iCount;
    }

    public void clickOnFieldName(String fieldName) {
        String path = "//div[text()='" + fieldName + "']";
        Driver.webDriver.findElement(By.xpath(path)).click();
    }

    public String getLabelText(String textValue) {
        return Driver.webDriver.findElement(By.xpath("//INPUT[@value='" + textValue + "']")).getAttribute("value");
    }

    public WebElement getModalPopUp() {
        return modalPopUp;
    }

    public List<String> getModalPopUpOptions() {

        List<WebElement> optionListWebElement = modalPopUpOptions;
        List<String> optionList = new ArrayList<String>();
        for (WebElement modalPopUpOptionsWebElement : optionListWebElement) {
            optionList.addAll(Arrays.asList(modalPopUpOptionsWebElement.getText().split("\\r?\\n")));
        }

        return optionList;
    }

    public boolean getStatusOfSaveAndSend() {
        boolean isDisabled = modalPopUpSaveAndSendButton.getAttribute("class").contains("disabled");
        return isDisabled;
    }

    public void clickOnButton(String btnName) {
        switch (btnName) {
            case "Cancel":
                ExpectedConditions.visibilityOf(modalPopUpCancelButton);
                ExpectedConditions.elementToBeClickable(modalPopUpCancelButton);
                modalPopUpCancelButton.click();
                break;
            case "Save":
                ExpectedConditions.visibilityOf(modalPopUpSaveButton);
                ExpectedConditions.elementToBeClickable(modalPopUpSaveButton);
                modalPopUpSaveButton.click();
                break;
            case "Save and send to production":
                ExpectedConditions.visibilityOf(modalPopUpSaveAndSendButton);
                ExpectedConditions.elementToBeClickable(modalPopUpSaveAndSendButton);
                modalPopUpSaveAndSendButton.click();
                break;
            case "Next":
                ExpectedConditions.visibilityOf(nextButton);
                ExpectedConditions.elementToBeClickable(nextButton);
                nextButton.click();
                break;
            case "Finish Later":
                ExpectedConditions.visibilityOf(btnFinish);
                ExpectedConditions.elementToBeClickable(btnFinish);
                btnFinish.click();
                break;
            case "Save and close":
                ExpectedConditions.visibilityOf(btnSaveAndClose);
                ExpectedConditions.elementToBeClickable(btnSaveAndClose);
                btnSaveAndClose.click();
                break;
        }
    }

    public String getGroupNameBreadCrum() {
        return groupNameBreadCrum.getText();
    }

    public void selectDrawBox(String drawBox, String textValue) {
        Driver.webDriver.findElement(By.xpath("//INPUT[@value='" + textValue + "']")).click();
    }

    public void selectValueFromDropDown(String value, String dropDownName) {
        Select ddPrimaryColumn = new Select(primaryCloumnDD);
        ddPrimaryColumn.selectByVisibleText(value);
    }

    public void renameLabelValue(String extractedValue, String newValue) {
        WebElement textboxValue = Driver.webDriver.findElement(By.xpath("//INPUT[@value='" + extractedValue + "']"));
        textboxValue.getAttribute("value");
        textboxValue.clear();
        textboxValue.sendKeys(newValue);
    }


    public void cliclOnDownArrowFieldOptions() {
        downArrowFieldOptions.click();
    }

    public By getFinishLaterButton() {
        return btnFinishBy;
    }

    public void selectRadioButton(String radionButtonnContinue) {
        radioButtonContinue.click();
    }

    public boolean GetStatusContinueRadioButton(String radioContinue) {
        return checkedRadioButton.isSelected();
    }

    public String getSelectedValue() {
        ExpectedConditions.visibilityOf(primaryCloumnDD);
        Select primaryDD = new Select(primaryCloumnDD);
        return primaryDD.getFirstSelectedOption().getText();

    }
}