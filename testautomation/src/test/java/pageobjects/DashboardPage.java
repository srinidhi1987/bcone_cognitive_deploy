package pageobjects;

import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.TestBase;

import java.util.ArrayList;
import java.util.List;

public class DashboardPage extends TestBase {

    // Dashboard Link
    @FindBy(how = How.XPATH, using = "//a[@href='/dashboard']")
    private WebElement dashboardLink;

    @FindBy(how = How.XPATH, using = " //A[@href='/learning-instances']")
    private WebElement learningInstancesLink;
    @FindBy(how = How.XPATH, using = " //A[@href='/bots']")
    private WebElement botsLink;

    @FindBy(how = How.CSS, using = ".aa-project-report-totals-comparison--metric>span")
    private WebElement totalFileProcessed;
    @FindBy(how = How.CSS, using = ".aa-project-stats-card-progress-metrics--metric>span")
    private WebElement fileProcessedForEachDomain;
    @FindBy(how = How.CSS, using = ".aa-project-stats-card")
    private List<WebElement> allDetailsOfParticularInstance;
    @FindBy(how = How.XPATH, using = "//SPAN[@class='navigation-primaryoption-button-title'][text()='Dashboards']")
    private WebElement dashboardTab;
    @FindBy(how = How.CSS, using = ".aa-project-stats-card-header")
    private List<WebElement> instanceNameHeaderWithStatus;
    private By instanceNameHeaderWithStatusForwaitMethod = By.xpath("//span[@class='icon aa-icon aa-icon-tray-profile icon--animate-none']");
    @FindBy(how = How.XPATH, using = "//*[@id=\"app\"]/div/div/main/div/div[2]/section[1]/div/div[3]/div/div[2]/div[1]")
    private WebElement stpValueFromDashboardPageForAllInstance;
    // Dashboard Page Instance Icon
    @FindBy(how = How.XPATH, using = "//div[@class='aa-dashboard-no-instances-icon']")
    private WebElement dashboardPageNewInstanceIcon;
    @FindBy(how = How.XPATH, using = "//A[@href='/learning-instances/new'][text()='Create an Instance']")
    private WebElement dashboardPageCraeteInstanceText;
    // Dashboard Page reference
    @FindBy(how = How.XPATH, using = "//A[@href='/dashboard'][text()='Dashboard']")
    private WebElement dashboardPageReference;
    @FindBy(how = How.XPATH, using = "//*[@id=\"aa-main-navigation\"]/div/div[4]/div/div[1]")
    private WebElement textOfDashboardHoverLink;

    @FindBy(how = How.XPATH, using = "//*[@id=\"aa-main-navigation\"]/div/div[7]/div")
    private WebElement textOfLIHoverLink;
    @FindBy(how = How.XPATH, using = "//*[@id=\"aa-main-navigation\"]/div/div[10]/div")
    private WebElement textOfBotsHoverLink;
    Actions builder = new Actions(Driver.webDriver);

    List<String> actualResult = new ArrayList<String>();

    public DashboardPage() {
    }
    //----

    // Get Dashboard Link
    /*public WebElement getDashboardLink() {
        return dashboardLink;
    }*/

    // Get Total Files Processed value
    public String getTotalFilesProcessed() {
        return totalFileProcessed.getText();
    }

    public List<String> getListFP() {
        List<String> fileProcessed = new ArrayList<>();
        List<WebElement> we = Driver.webDriver.findElements(By.cssSelector(".aa-project-stats-card-progress-metrics--metric>span"));
        for (WebElement w : we) {
            fileProcessed.add(w.getText());
        }
        return fileProcessed;
    }


   /* public void clickOnDashboardTab() {
        dashboardTab.click();
    }*/

    // Get details from all production instances
    public List<String> getAllInstanceDetail() {
        List<String> instanceDetail = new ArrayList<>();
        List<String> instanceDetailWhichAreOnProduction = new ArrayList<>();
        for (WebElement anAllDetailsOfParticularInstance : allDetailsOfParticularInstance) {
            instanceDetail.add(anAllDetailsOfParticularInstance.getText().replaceAll("[\\r\\n" +
                    "]", ""));
        }
        for (String s : instanceDetail)
            if (s.contains("production"))
                instanceDetailWhichAreOnProduction.add(s);

        return instanceDetailWhichAreOnProduction;

    }

    // Get instance header
    List<WebElement> instanceNameHeaderWithStatus() {
        return instanceNameHeaderWithStatus;
    }

    // Get STP value
    public String getSTPValuefromDashboardPageForAllInstance() {
        return stpValueFromDashboardPageForAllInstance.getText();
    }


    public By getHeaderOfInstance() {
        return instanceNameHeaderWithStatusForwaitMethod;
    }


    public List<String> getStagingInstanceValuesFromDashBoardPage() {
        List<String> stagingInstanceDetails = new ArrayList<>();
        String pathPart1 = "//DIV[contains(@class,'aa-project-stats-card') and contains(.,'";
        String pathPart2 = Driver.instName;
        String pathLastPart = "')]";
        List<WebElement> getStagingValues = Driver.webDriver.findElements(By.xpath(pathPart1 + pathPart2 + pathLastPart));


        for (WebElement anAllDetailsOfParticularInstance : getStagingValues) {
            stagingInstanceDetails.add(anAllDetailsOfParticularInstance.getText().replaceAll("[\\n" +
                    "]", ""));
        }
        return stagingInstanceDetails;
    }

    public void clickOnInstance(String productionInstanceName) throws InterruptedException {
        productionInstanceName = productionInstanceName.substring(0,productionInstanceName.length() - 10);
        productionInstanceName = "'"+productionInstanceName+"')]";
        productionInstanceName = productionInstanceName.replaceAll("[\n\r]", "");
        WebElement instanceNameOnProduction = Driver.webDriver.findElement(By.xpath("//HEADER[contains(.,"+productionInstanceName));
        instanceNameOnProduction.click();
    }

    public String getInstanceDetail(String productionInstanceName)throws InterruptedException
    {
        productionInstanceName = productionInstanceName.substring(0,productionInstanceName.length() - 10);
        productionInstanceName = "'"+productionInstanceName+"')]";
        productionInstanceName = productionInstanceName.replaceAll("[\n\r]", "");
        WebElement instanceNameOnProduction = Driver.webDriver.findElement(By.xpath("//div[contains(@class, 'aa-project-stats-card') and contains(.,"+productionInstanceName));
        String value = instanceNameOnProduction.getText();
        value = value.substring(value.indexOf("production")).replaceAll("[^0-9.,%]+", "").trim();
        System.out.println("Instance name" + productionInstanceName+ "Value" +value);
        return value;
    }

    public WebElement getDashboardPageInstanceIcon() {
        return dashboardPageNewInstanceIcon;
    }

    public String getTextCreateInstance() {
        return dashboardPageCraeteInstanceText.getText();
    }

    public WebElement getDashboardPageInstanceNameText() {
        String pathPart1 = "//DIV[@class='aa-project-stats-card-header--name'][text()='"+Driver.instName+"']";
        WebElement stagingInstance = Driver.webDriver.findElement(By.xpath(pathPart1));
        return stagingInstance;
    }

    public void clickOnStagingInstance() {

        String pathPart1 = "//DIV[@class='aa-project-stats-card-header--name'][text()='"+Driver.instName+"']";
        WebElement stagingInstance = Driver.webDriver.findElement(By.xpath(pathPart1));
        stagingInstance.click();
    }

    // Get Dashboard Link
    public WebElement getDashboardPageReference() {
        return dashboardPageReference;
    }

    public String getHoverText(String element) {
        String text = null;
        // Mouse hover to that text message
        switch (element) {
            case "Dashboard":
                builder.moveToElement(dashboardLink).perform();
                // return tooltip message
                text = textOfDashboardHoverLink.getText();
                break;
            case "Learning Instances":
                builder.moveToElement(learningInstancesLink).perform();
                // return tooltip message
                text = textOfLIHoverLink.getText();
                break;
            case "Bots":
                builder.moveToElement(botsLink).perform();
                text = textOfBotsHoverLink.getText();
                break;
        }
        return text;
    }

    public void clickOnPageReference(String pageName) {
        dashboardPageReference.click();
    }

    public String getPageReferenceText(){
        return  dashboardPageReference.getText();
    }
}
