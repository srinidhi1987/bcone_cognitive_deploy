package driver;

import com.thoughtworks.gauge.*;

import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import exceptions.DriverException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Driver {

    // Holds the WebDriver instance
    public static WebDriver webDriver;
    public static int a = 1;
    // Holds Instance Name Throughout The Suite
    public static String instName, scenarioName, trainingGroupName, trainingSuccessGroupName, documentCountGroupName,addFieldsGroupName;
    private final Logger logger = LogManager.getLogger();
    private LocalDateTime now = LocalDateTime.now();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static String stepStartTime;
    private static String suiteStartTime;


    // Prepare prerequisites for test automation suite
    static {
        Path path = Paths.get("C:\\IQBot_TestData");
        try {
            Files.walk(path)
                    .map(Path::toFile)
                    .sorted(Comparator.comparing(File::isDirectory))
                    .forEach(File::delete);
        } catch (Exception e) {
            e.printStackTrace();
        }
        copyResources(new File(System.getenv("source_testDocument_Path")), new File(System.getenv("test_Documents_FolderPath")));

    }

    // Copy Test Documents and resources before test run
    private static void copyResources(File sourcePath, File destinationPath) {
        try {
            org.apache.commons.io.FileUtils.copyDirectory(sourcePath, destinationPath);
        } catch (FileAlreadyExistsException exp) {
            //destination file already exists
        } catch (IOException exp) {
        /* something else went wrong */
            exp.printStackTrace();
        }
    }

    // Create File to store Instance Name
    private void logInstanceNameIntoFile(String instName, String fileName) throws Exception {
        String path = System.getenv("test_Documents_FolderPath");
        String fullPath = path + "\\" + fileName;
        File file = new File(fullPath);
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        Writer w = new BufferedWriter(osw);
        w.write("Instance_Name=" + instName);
        w.close();
    }

    // Initialize a webDriver instance of required browser
    // Since this does not have a significance in the application's business
    // domain, the BeforeSuite hook is used to instantiate the webDriver
    @BeforeSuite
    public void initializeDriver() throws Exception {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd_MM_yyyy");
        LocalDate localDate = LocalDate.now();
        webDriver = DriverFactory.getDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        String currentWindow = webDriver.getWindowHandle();
        webDriver.switchTo().window(currentWindow);
        PageFactory.initElements(webDriver, this);
        instName = System.getenv("instance_Name");
        instName = instName + "_" + localDate + "_" + System.currentTimeMillis();
        String fileName = "Instance.txt";
        logInstanceNameIntoFile(instName, fileName);
        suiteStartTime = now.format(formatter);
    }

    @BeforeStep
    public void getStartTime() {
        stepStartTime = now.format(formatter);
    }

    @AfterStep
    public void getStepStatus(ExecutionContext context) throws Exception {
        String stepEndTime = now.format(formatter);
        String stepName = context.getCurrentStep().getText();
        boolean stepStatus = context.getCurrentStep().getIsFailing();
        String scenarioName = context.getCurrentScenario().getName();
        String specificationName = context.getCurrentSpecification().getName();
        logExecutionStatus(Driver.instName, scenarioName, stepName, stepStatus, specificationName, stepStartTime, stepEndTime, browserDetails());
    }

    @AfterScenario
    public void UpdateTestCaseStatusInJira(ExecutionContext context) throws Exception {
        Scenario scenarioName = context.getCurrentScenario();
        boolean scenarioStatus = scenarioName.getIsFailing();
        String scenarioTag = String.valueOf(scenarioName.getTags());
        logger.trace("Scenario Name: " + scenarioName.getName() + "Scenario Status:" + scenarioStatus + "Tag: " + scenarioName.getTags());

        if (scenarioStatus) {
            if (scenarioTag.equals("[Critical]")) {
                tearDown();
                logger.trace("SUITE FAILURE");
                Runtime.getRuntime().halt(0);
            }
        }

        // Jira integration code : To update test case status in jira > will you this code later when jira integration requirement
        /*StepDetails stepName = context.getCurrentStep();
        boolean stepStatus = stepName.getIsFailing();
        Scenario scenarioName = context.getCurrentScenario();
        int methodExecutionStatus;
        if (!stepStatus)
            methodExecutionStatus = 1;
        else
            methodExecutionStatus = 2;
         logger.trace( "Scenario Name: " + scenarioName.getName() + " ExecutionStatus: " + methodExecutionStatus)
        UpdateExecutionStatusToJira updateTestCaseExecutionStatusToZephyrForJira = new UpdateExecutionStatusToJira();
        updateTestCaseExecutionStatusToZephyrForJira.update(scenarioName.getName(), methodExecutionStatus); */

    }

    // Close the webDriver instance
    @AfterSuite
    public void tearDown() throws Exception {
        String suiteEndTime = now.format(formatter);
        String fileName = "TestSuiteDuration.txt";
        String totalSuiteDuration = calculateSuiteExecutionTime(suiteStartTime, suiteEndTime);
        logInstanceNameIntoFile(totalSuiteDuration, fileName);
        runBatchFile(System.getenv("reportGenerationBatchFile"));
        browserDetails();
        webDriver.quit();
    }

    // Create File to store execution status
    private void logExecutionStatus(String instName, String scenarioName, String stepName, Boolean stepStatus, String specificationName, String stepStartTime, String stepEndTime, String browserDetails) throws Exception {
        scenarioName = scenarioName.replaceAll(",", "");
        stepName = stepName.replaceAll(",", "and");
        logger.info("Info Message!", instName, scenarioName, stepName, stepStatus, specificationName, stepStartTime, stepEndTime, browserDetails);
    }

    private String calculateSuiteExecutionTime(String suiteStartTime, String suiteEndTime) throws Exception {
        Date d1 = format.parse(suiteStartTime);
        Date d2 = format.parse(suiteEndTime);
        long diff = d2.getTime() - d1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long totalDiffHours = diff / (60 * 60 * 1000);
        return totalDiffHours + ":" + diffMinutes + ":" + diffSeconds;
    }

    // Command runner
    private void runBatchFile(String command) throws IOException, InterruptedException, DriverException {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
        if (process.exitValue() != 0) {
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line;
            String result = "";
            while ((line = reader.readLine()) != null) {
                logger.trace("Batch command line" + line);
            }
            reader.close();
            logger.trace("Result: " + result);
            throw new DriverException("Failed to execute batch command (" + command + ") with error code: " + process.exitValue());
        }
    }

    // Get Browser Details
    private String browserDetails() {
        Capabilities cap = ((RemoteWebDriver) webDriver).getCapabilities();
        String browserName = cap.getBrowserName();
        String browserVersion = cap.getVersion();
        return browserName + " : v" + browserVersion;
    }

}
