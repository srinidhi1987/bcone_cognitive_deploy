#!/usr/bin/python


import sys
import os
import shutil
from os import path

def build_service():
    os.chdir(sys.argv[1])
    os.system('gradle clean build')

def copy_files(service,dst):
    # copying jar file
    os.chdir(sys.argv[1]+"/build/libs")
    #os.chdir('./build/libs')
    for fname in os.listdir('.'):
        if fname.endswith('.jar'):
            print("copying "+fname+" to "+dst)
            shutil.copy(fname,dst)
    
    # copying log4j file
    os.chdir(sys.argv[1]+"/src/main/resources")
    #os.chdir('./src/main/resources')
    for fname in os.listdir('.'):
        if(fname == 'log4j2.xml'):
            new_name = fname.split(',',maxsplit=1)[0]+"_"+service+".xml"
            os.rename(fname,new_name)
            print("copying "+new_name+" to "+dst)
            shutil.copy(new_name,dst)

    # copying json file
    os.chdir(sys.argv[1])
    for fname in os.listdir('.'):
        if fname.endswith('.json'):
            print("copying "+fname+" to "+dst)
            shutil.copy(fname,dst)
    
    # copying yaml file
    os.chdir(sys.argv[1])
    for fname in os.listdir('.'):
        if fname.endswith('.yaml'):
            print("copying "+fname+" to "+dst)
            shutil.copy(fname,dst)

def s3_copy():
    print("S3 implementation not started")

if __name__ == "__main__":
    dst = "/buildartifacts/services/"
    build_service()
    copy_files(sys.argv[2],dst)
    s3_copy()
    
# put the jar files in common location , which is taken as volume mount