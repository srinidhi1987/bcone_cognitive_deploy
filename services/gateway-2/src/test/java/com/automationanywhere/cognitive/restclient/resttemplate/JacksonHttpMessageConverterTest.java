package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.mock.http.MockHttpInputMessage;
import org.springframework.web.client.HttpClientErrorException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class JacksonHttpMessageConverterTest {
  private ObjectMapper om;
  private JacksonHttpMessageConverter c;

  @BeforeMethod
  public void setUp() throws Exception {
    om = spy(new ObjectMapper());
    c = spy(new JacksonHttpMessageConverter(om));
  }

  @Test
  public void readJavaTypeShouldReadNotStandardResponses() throws Exception {
    // given

    JavaType jt = om.constructType(SomeData.class);
    MockHttpInputMessage msg = new MockHttpInputMessage(
        om.writeValueAsBytes(new SomeData("id1"))
    );

    // when

    SomeData o = (SomeData) c.readJavaType(jt, msg);

    // then

    assertThat(o.id).isEqualTo("id1");
  }

  @Test
  public void readJavaTypeShouldReadNotStandardResponsesWithBadSuccessType() throws Exception {
    // given

    Map<String, Object> map = new HashMap<>();
    map.put("success", "true");
    map.put("data", null);

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    Map<String, Object> o = (Map<String, Object>) c.readInternal(Map.class, msg);

    // then

    assertThat(o).isEqualTo(map);
  }

  @Test
  public void readJavaTypeShouldReadNotStandardResponsesWithBadHttpStatusCodeType()
      throws Exception {
    // given

    Map<String, Object> map = new HashMap<>();
    map.put("success", false);
    map.put("httpStatusCode", "400");

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    Map<String, Object> o = (Map<String, Object>) c.readInternal(Map.class, msg);

    // then

    assertThat(o).isEqualTo(map);
  }

  @Test
  public void readJavaTypeShouldReadNotStandardResponsesWithBadHttpStatusCode()
      throws Exception {
    // given

    Map<String, Object> map = new HashMap<>();
    map.put("success", false);
    map.put("httpStatusCode", Integer.MAX_VALUE);

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    Map<String, Object> o = (Map<String, Object>) c.readInternal(Map.class, msg);

    // then

    assertThat(o).isEqualTo(map);
  }

  @Test
  public void readJavaTypeShouldReadNotStandardResponsesWithBadErrorsType()
      throws Exception {
    // given

    Map<String, Object> map = new HashMap<>();
    map.put("success", false);
    map.put("errors", 1);

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    Map<String, Object> o = (Map<String, Object>) c.readInternal(Map.class, msg);

    // then

    assertThat(o).isEqualTo(map);
  }

  @Test
  public void readJavaTypeShouldReadNotStandardResponsesWithIntErrorElements()
      throws Exception {
    // given

    Map<String, Object> map = new HashMap<>();
    map.put("success", false);
    map.put("errors", new ArrayList<>(Arrays.asList(1, 2)));

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    Map<String, Object> o = (Map<String, Object>) c.readInternal(Map.class, msg);

    // then

    assertThat(o).isEqualTo(map);
  }

  @Test
  public void readJavaTypeShouldReadStandardResponsesWithoutData() throws Exception {
    // given

    JavaType jt = om.constructType(SomeData.class);
    MockHttpInputMessage msg = new MockHttpInputMessage(
        om.writeValueAsBytes(
            new StdResp(null, null, null, null)
        )
    );

    // when

    SomeData o = (SomeData) c.readJavaType(jt, msg);

    // then

    assertThat(o).isNull();
  }

  @Test
  public void readJavaTypeShouldReadStandardResponsesWithoutSuccess() throws Exception {
    // given

    JavaType jt = om.constructType(SomeData.class);
    MockHttpInputMessage msg = new MockHttpInputMessage(
        om.writeValueAsBytes(new HashMap<String, Object>())
    );

    // when

    SomeData o = (SomeData) c.readJavaType(jt, msg);

    // then

    assertThat(o).isNull();
  }

  @Test
  public void readJavaTypeShouldReadStandardResponsesWithData() throws Exception {
    // given

    JavaType jt = om.constructType(SomeData.class);
    MockHttpInputMessage msg = new MockHttpInputMessage(
        om.writeValueAsBytes(
            new StdResp(null, new SomeData("id2"), null, null)
        )
    );

    // when

    SomeData o = (SomeData) c.readJavaType(jt, msg);

    // then

    assertThat(o.id).isEqualTo("id2");
  }

  @Test
  public void readJavaTypeShouldReadStandardResponses() throws Exception {
    // given

    MockHttpInputMessage msg = new MockHttpInputMessage(
        om.writeValueAsBytes(new StandardResponse<>(
          true, null, null
        ))
    );

    // when

    StandardResponse o = (StandardResponse) c.readInternal(StandardResponse.class, msg);

    // then

    assertThat(o.success).isTrue();
    assertThat(o.data).isNull();
  }

  @Test
  public void readJavaTypeShouldReadStandardResponsesWithEmptyErrors() throws Exception {
    // given

    JavaType jt = om.constructType(SomeData.class);

    Map<String, Object> map = new HashMap<>();
    map.put("success", true);
    map.put("data", null);
    map.put("errors", new String[]{""});

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    SomeError o = (SomeError) c.readJavaType(jt, msg);

    // then

    assertThat(o).isNull();
  }

  @Test
  public void readJavaTypeShouldReadStandardResponsesWithNullErrorElements() throws Exception {
    // given

    JavaType jt = om.constructType(SomeData.class);

    Map<String, Object> map = new HashMap<>();
    map.put("success", true);
    map.put("data", null);
    map.put("errors", new String[]{null});

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(map));

    // when

    SomeData o = (SomeData) c.readJavaType(jt, msg);

    // then

    assertThat(o).isNull();
  }

  @Test
  public void readJavaTypeShouldConvertErrorJsonsToExceptions() throws Exception {
    // given

    JavaType jt = om.constructType(SomeError.class);
    SomeError er = new SomeError(false, null, Collections.singletonList("er1"));
    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(er));

    // when

    try {
      c.readJavaType(jt, msg);
      fail();
    } catch (final HttpClientErrorException ex) {
      // then
      assertThat(ex.getRawStatusCode()).isEqualTo(500);
      assertThat(ex.getStatusText()).isEqualTo(HttpStatus.valueOf(500).getReasonPhrase());
      assertThat(ex.getResponseHeaders()).containsOnly(new SimpleEntry<>(
          "Content-Type",
          Collections.singletonList("application/json;charset=UTF-8")
      ));
      SomeError error = om.readValue(ex.getResponseBodyAsByteArray(), SomeError.class);
      assertThat(error.httpStatusCode).isNull();
      assertThat(error.success).isFalse();
      assertThat(error.errors).isEqualTo(Collections.singletonList("er1"));
    }
  }

  @Test
  public void readJavaTypeShouldConvertErrorJsonsToExceptionsWithStringErrors() throws Exception {
    // given

    JavaType jt = om.constructType(SomeError.class);

    Map<String, Object> er = new HashMap<>();
    er.put("success", false);
    er.put("data", null);
    er.put("errors", "er1");

    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(er));

    // when

    try {
      c.readJavaType(jt, msg);
      fail();
    } catch (final HttpClientErrorException ex) {
      // then
      assertThat(ex.getRawStatusCode()).isEqualTo(400);
      assertThat(ex.getStatusText()).isEqualTo(HttpStatus.valueOf(400).getReasonPhrase());
      assertThat(ex.getResponseHeaders()).containsOnly(new SimpleEntry<>(
          "Content-Type",
          Collections.singletonList("application/json;charset=UTF-8")
      ));
      Map<String, Object> error = om.readValue(ex.getResponseBodyAsByteArray(), Map.class);
      assertThat(error.get("httpStatusCode")).isNull();
      assertThat(error.get("success")).isEqualTo(false);
      assertThat(error.get("errors")).isEqualTo("er1");
    }
  }

  @Test
  public void readJavaTypeShouldConvertErrorJsonsWithoutErrorsToNulls() throws Exception {
    // given

    JavaType jt = om.constructType(SomeError.class);
    SomeError er = new SomeError(false, null, null);
    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(er));

    // when

    Object r = c.readJavaType(jt, msg);

    // then
    assertThat(r).isNull();
  }

  @Test
  public void readJavaTypeShouldUseProvidedHttpStatusCode() throws Exception {
    // given

    JavaType jt = om.constructType(SomeError.class);
    SomeError er = new SomeError(false, 404, Collections.singletonList("error"));
    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(er));

    // when

    try {
      c.readJavaType(jt, msg);
      fail();
    } catch (final HttpClientErrorException ex) {
      // then
      assertThat(ex.getRawStatusCode()).isEqualTo(404);
      assertThat(ex.getStatusText()).isEqualTo(HttpStatus.valueOf(404).getReasonPhrase());
      assertThat(ex.getResponseHeaders()).containsOnly(new SimpleEntry<>(
          "Content-Type",
          Collections.singletonList("application/json;charset=UTF-8")
      ));
      assertThat(om.readValue(ex.getResponseBodyAsByteArray(), SomeError.class).errors)
          .isEqualTo(Collections.singletonList("error"));
    }
  }

  @Test
  public void readJavaTypeShouldRejectProvidedHttpStatusCode() throws Exception {
    // given

    JavaType jt = om.constructType(SomeError.class);
    SomeError er = new SomeError(false, 200, Collections.singletonList("error"));
    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(er));

    // when

    try {
      c.readJavaType(jt, msg);
      fail();
    } catch (final HttpClientErrorException ex) {
      // then
      assertThat(ex.getRawStatusCode()).isEqualTo(400);
      assertThat(ex.getStatusText()).isEqualTo(HttpStatus.valueOf(400).getReasonPhrase());
      assertThat(ex.getResponseHeaders()).containsOnly(new SimpleEntry<>(
          "Content-Type",
          Collections.singletonList("application/json;charset=UTF-8")
      ));
      assertThat(om.readValue(ex.getResponseBodyAsByteArray(), SomeError.class).errors)
          .isEqualTo(Collections.singletonList("error"));
    }
  }

  @Test
  public void readJavaTypeShouldHandleJsonProcessingException() throws Exception {
    // given

    Class<SomeError> t = SomeError.class;
    SomeError er = new SomeError(true, 404, Collections.singletonList("er1"));
    byte[] content = om.writeValueAsBytes(er);
    JsonNode jsonNode = om.readTree(content);
    MockHttpInputMessage msg = new MockHttpInputMessage(content);
    JsonProcessingException e = new JsonProcessingException("") {};

    doThrow(e).when(c).convertToException(
        jsonNode, HttpStatus.NOT_FOUND
    );

    // when

    try {
      c.readInternal(t, msg);
      fail();
    } catch (final Exception ex) {
      // then
      assertThat(ex.getCause()).isSameAs(e);
    }
  }

  @Test
  public void readJavaTypeShouldHandleHttpMessageNotReadableException() throws Exception {
    // given

    Class<SomeError> t = SomeError.class;
    SomeError er = new SomeError(true, 404, Collections.singletonList("er1"));
    MockHttpInputMessage msg = new MockHttpInputMessage(om.writeValueAsBytes(er));
    IOException e = new IOException("");

    doThrow(e).when(om).readTree(any(InputStream.class));

    // when

    try {
      c.readInternal(t, msg);
      fail();
    } catch (final Exception ex) {
      // then
      assertThat(ex.getCause()).isSameAs(e);
    }
  }
}

class SomeData {
  public final String id;

  public SomeData(@JsonProperty("id") final String id) {
    this.id = id;
  }
}

class StdResp {
  public final Boolean success;
  public final SomeData data;
  public final Integer httpStatusCode;
  public final List<String> errors;

  StdResp(
      @JsonProperty("success") final Boolean success,
      @JsonProperty("data") final SomeData data,
      @JsonProperty("httpStatusCode") final Integer httpStatusCode,
      @JsonProperty("errors") final List<String> errors
  ) {
    this.success = success;
    this.data = data;
    this.httpStatusCode = httpStatusCode;
    this.errors = errors;
  }
}

class SomeError {
  public final Boolean success;
  public final Integer httpStatusCode;
  public final List<String> errors;

  SomeError(
      @JsonProperty("success") final Boolean success,
      @JsonProperty("httpStatusCode") final Integer httpStatusCode,
      @JsonProperty("errors") final List<String> errors
  ) {
    this.success = success;
    this.httpStatusCode = httpStatusCode;
    this.errors = errors;
  }
}