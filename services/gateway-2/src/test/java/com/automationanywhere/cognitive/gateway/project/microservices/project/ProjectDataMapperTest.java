package com.automationanywhere.cognitive.gateway.project.microservices.project;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.Category;
import com.automationanywhere.cognitive.gateway.project.models.CustomFieldDetail;
import com.automationanywhere.cognitive.gateway.project.models.FieldDetails;
import com.automationanywhere.cognitive.gateway.project.models.File;
import com.automationanywhere.cognitive.gateway.project.models.FileCount;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.OCREngineDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.gateway.project.models.VisionBot;
import com.automationanywhere.cognitive.id.string.StringId;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import org.testng.annotations.Test;

public class ProjectDataMapperTest {
  private final ProjectDataMapper mapper = new ProjectDataMapper();

  @Test
  public void mapProjectShouldMapDataToModel1() {
    // given
    OffsetDateTime updatedAt = OffsetDateTime.now();
    OffsetDateTime createdAt = OffsetDateTime.now();

    ProjectDetails p1 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            new VisionBot(new StringId("vb"), "vbA", "cs"),
            -19, Collections.singletonList(new File(
                new StringId("f1"), "fnm", "loc", "fmt", true
            )),
            new FileCount(90, 9),
            new FileCount(91, 8)
        )), new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", updatedAt, createdAt, Collections.singletonList(
            new OCREngineDetail(new StringId("add"), "addnm", "engineType")
        ), 1
    );

    ProjectDetails p2 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetails(
            null,
            null
        ), "pst", "env", updatedAt, createdAt, null,1
    );

    ProjectDetails p3 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetails(
            null,
            Collections.singletonList(null)
        ), "pst", "env", updatedAt, createdAt,
        Collections.singletonList(null), 1
    );

    ProjectDetailsData pd1 = new ProjectDetailsData(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryData(
            new StringId("c1"), "c",
            new VisionBotData(new StringId("vb"), "vbA", "cs"),
            -19, Collections.singletonList(new FileData(
                new StringId("f1"), "fnm", "loc", "fmt", true
            )),
            new FileCountData(90, 9),
            new FileCountData(91, 8)
        )), new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailData(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", updatedAt, createdAt, Collections.singletonList(
            new OCREngineDetailData(new StringId("add"), "addnm", "engineType")
        ),1
    );

    ProjectDetailsData pd2 = new ProjectDetailsData(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryData(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetailsData(
            null,
            null
        ), "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetailsData pd3 = new ProjectDetailsData(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryData(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetailsData(
            null,
            Collections.singletonList(null)
        ), "pst", "env", updatedAt, createdAt,
        Collections.singletonList(null), 1
    );

    // when
    ProjectDetails r1 = mapper.mapProjectDetails(pd1);
    ProjectDetails r2 = mapper.mapProjectDetails(pd2);
    ProjectDetails r3 = mapper.mapProjectDetails(pd3);

    // then
    assertThat(r1).isEqualTo(p1);
    assertThat(r2).isEqualTo(p2);
    assertThat(r3).isEqualTo(p3);
  }

  @Test
  public void mapProjectShouldMapDataToModel2() {
    // given
    OffsetDateTime updatedAt = OffsetDateTime.now();
    OffsetDateTime createdAt = OffsetDateTime.now();

    ProjectDetails p3 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        null, null, "pst", "env", updatedAt, createdAt,
        null, 1
    );

    ProjectDetails p4 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            null,
            -19, Collections.singletonList(null),
            null,
            null
        )), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetails p5 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(null), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetailsData pd3 = new ProjectDetailsData(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        null, null, "pst", "env",
        updatedAt, createdAt, null, 1
    );

    ProjectDetailsData pd4 = new ProjectDetailsData(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryData(
            new StringId("c1"), "c",
            null,
            -19, Collections.singletonList(null),
            null,
            null
        )), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetailsData pd5 = new ProjectDetailsData(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(null), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    // when
    ProjectDetails r3 = mapper.mapProjectDetails(pd3);
    ProjectDetails r4 = mapper.mapProjectDetails(pd4);
    ProjectDetails r5 = mapper.mapProjectDetails(pd5);
    ProjectDetails r6 = mapper.mapProjectDetails(null);

    // then
    assertThat(r3).isEqualTo(p3);
    assertThat(r4).isEqualTo(p4);
    assertThat(r5).isEqualTo(p5);
    assertThat(r6).isNull();
  }


  @Test
  public void mapNewProjectDetailsShouldMapDataToModel() {
    // given
    NewProjectDetailsData pd1 = new NewProjectDetailsData(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailData(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env"
    );

    NewProjectDetailsData pd2 = new NewProjectDetailsData(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env"
    );

    NewProjectDetailsData pd3 = new NewProjectDetailsData(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env"
    );

    NewProjectDetailsData pd4 = new NewProjectDetailsData(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env"
    );

    NewProjectDetails p1 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env"
    );

    NewProjectDetails p2 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env"
    );

    NewProjectDetails p3 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env"
    );

    NewProjectDetails p4 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env"
    );

    // when
    NewProjectDetailsData r1 = mapper.mapNewProjectDetails(p1);
    NewProjectDetailsData r2 = mapper.mapNewProjectDetails(p2);
    NewProjectDetailsData r3 = mapper.mapNewProjectDetails(p3);
    NewProjectDetailsData r4 = mapper.mapNewProjectDetails(p4);
    NewProjectDetailsData r5 = mapper.mapNewProjectDetails(null);

    // then
    assertThat(r1).isEqualTo(pd1);
    assertThat(r2).isEqualTo(pd2);
    assertThat(r3).isEqualTo(pd3);
    assertThat(r4).isEqualTo(pd4);
    assertThat(r5).isNull();
  }

  @Test
  public void mapProjectDetailChangesShouldMapDataToModel() {
    // given
    ProjectDetailChanges p1 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetail(
          new StringId("z"), "onm", "engineType")), 1
    );

    ProjectDetailChanges p2 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChanges p3 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChanges p4 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    ProjectDetailChangesData pd1 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailData(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetailData(
          new StringId("z"), "onm", "engineType"
        )), 1
    );

    ProjectDetailChangesData pd2 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChangesData pd3 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChangesData pd4 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    // when
    ProjectDetailChanges r1 = mapper.mapProjectDetailChanges(pd1);
    ProjectDetailChanges r2 = mapper.mapProjectDetailChanges(pd2);
    ProjectDetailChanges r3 = mapper.mapProjectDetailChanges(pd3);
    ProjectDetailChanges r4 = mapper.mapProjectDetailChanges(pd4);
    ProjectDetailChanges r5 = mapper.mapProjectDetailChanges((ProjectDetailChangesData) null);

    // then
    assertThat(r1).isEqualTo(p1);
    assertThat(r2).isEqualTo(p2);
    assertThat(r3).isEqualTo(p3);
    assertThat(r4).isEqualTo(p4);
    assertThat(r5).isNull();
  }

  @Test
  public void mapProjectDetailChangesShouldMapModelToData() {
    // given
    ProjectDetailChanges p1 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetail(
          new StringId("z"), "onm", "engineType")) ,1
    );

    ProjectDetailChanges p2 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChanges p3 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChanges p4 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    ProjectDetailChangesData pd1 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailData(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetailData(
          new StringId("z"), "onm", "engineType"
        )), 1
    );

    ProjectDetailChangesData pd2 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChangesData pd3 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsData(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChangesData pd4 = new ProjectDetailChangesData(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    // when
    ProjectDetailChangesData r1 = mapper.mapProjectDetailChanges(p1);
    ProjectDetailChangesData r2 = mapper.mapProjectDetailChanges(p2);
    ProjectDetailChangesData r3 = mapper.mapProjectDetailChanges(p3);
    ProjectDetailChangesData r4 = mapper.mapProjectDetailChanges(p4);
    ProjectDetailChangesData r5 = mapper.mapProjectDetailChanges((ProjectDetailChanges) null);

    // then
    assertThat(r1).isEqualTo(pd1);
    assertThat(r2).isEqualTo(pd2);
    assertThat(r3).isEqualTo(pd3);
    assertThat(r4).isEqualTo(pd4);
    assertThat(r5).isNull();
  }

  @Test
  public void mapArchiveDetailsShouldMapDataToModel() {
    // given
    ArchiveDetailsData d1 = new ArchiveDetailsData("n1");
    ArchiveDetails m1 = new ArchiveDetails("n1");

    // when
    ArchiveDetails r1 = mapper.mapArchiveDetails(d1);
    ArchiveDetails r2 = mapper.mapArchiveDetails((ArchiveDetailsData) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapArchiveDetailsShouldMapModelToData() {
    // given
    ArchiveDetails m1 = new ArchiveDetails("n1");
    ArchiveDetailsData d1 = new ArchiveDetailsData("n1");

    // when
    ArchiveDetailsData r1 = mapper.mapArchiveDetails(m1);
    ArchiveDetailsData r2 = mapper.mapArchiveDetails((ArchiveDetails) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectImportRequestShouldMapModelToData() {
    // given
    ProjectImportRequest m1 = new ProjectImportRequest(
        new ArchiveDetails("nm1"), new StringId("1"), "a"
    );
    ProjectImportRequest m2 = new ProjectImportRequest(
        null, new StringId("1"), "a"
    );
    ProjectImportRequestData d1 = new ProjectImportRequestData(
        new ArchiveDetailsData("nm1"), new StringId("1"), "a"
    );
    ProjectImportRequestData d2 = new ProjectImportRequestData(
        null, new StringId("1"), "a"
    );

    // when
    ProjectImportRequestData r1 = mapper.mapProjectImportRequest(m1);
    ProjectImportRequestData r2 = mapper.mapProjectImportRequest(m2);
    ProjectImportRequestData r3 = mapper.mapProjectImportRequest(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapTaskStatusShouldMapDataToModel() {
    // given
    TaskStatusData d1 = new TaskStatusData(
        new StringId("1"), "tt", "s", "d"
    );
    TaskStatus m1 = new TaskStatus(
        new StringId("1"), "tt", "s", "d"
    );

    // when
    TaskStatus r1 = mapper.mapTaskStatus(d1);
    TaskStatus r2 = mapper.mapTaskStatus(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectPatchDetailMapModelToData() {
    // given
    ProjectPatchDetailData d1 = new ProjectPatchDetailData(
        "tt", "s", "d", 1
    );

    ProjectPatchDetail m1 = new ProjectPatchDetail(
        "tt", "s", "d", 1
    );

    // when
    ProjectPatchDetailData r1 = mapper.mapProjectPatchDetail(m1);
    ProjectPatchDetailData r2 = mapper.mapProjectPatchDetail(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectExportRequestShouldMapModelToData() {
    // given
    ProjectExportRequestData d1 = new ProjectExportRequestData(
        "nm", Collections.singletonList("d"), new StringId("1")
    );

    ProjectExportRequestData d2 = new ProjectExportRequestData(
        "nm", null, new StringId("1")
    );

    ProjectExportRequest m1 = new ProjectExportRequest(
        "nm", Collections.singletonList("d"), new StringId("1")
    );

    ProjectExportRequest m2 = new ProjectExportRequest(
        "nm", null, new StringId("1")
    );

    // when
    ProjectExportRequestData r1 = mapper.mapProjectExportRequest(m1);
    ProjectExportRequestData r2 = mapper.mapProjectExportRequest(m2);
    ProjectExportRequestData r3 = mapper.mapProjectExportRequest(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapProjectPatchResponseShouldMapDataTomodel() {
    //given
    ProjectPatchResponseData d1 = new ProjectPatchResponseData(1);
    ProjectPatchResponse m1 = new ProjectPatchResponse(1);

    //when
    ProjectPatchResponse r1 = mapper.mapProjectPatchResponse(d1);
    ProjectPatchResponse r2 = mapper.mapProjectPatchResponse(null);

    //then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapTaskShouldMapDataToModel() {
    //given
    OffsetDateTime dateTime = OffsetDateTime.now();
    TaskData d1 = new TaskData(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        dateTime);
    TaskData d2 = new TaskData(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        null);
    Task m1 = new Task(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        dateTime);
    Task m2 = new Task(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        null);

    //when
    Task r1 = mapper.mapTask(d1);
    Task r2 = mapper.mapTask(d2);
    Task r3 = mapper.mapTask(null);

    //then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }
}
