package com.automationanywhere.cognitive.gateway.ml.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.ml.MlService;
import com.automationanywhere.cognitive.gateway.ml.ml.MlDataMapper;
import com.automationanywhere.cognitive.gateway.ml.ml.MlMicroservice;
import com.automationanywhere.cognitive.gateway.ml.ml.ValueData;
import com.automationanywhere.cognitive.gateway.ml.model.Value;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.core.ParameterizedTypeReference;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MLControllerTest {
  private MlController c;
  private RestClient rc;
  private ApplicationConfiguration cfg;

  @BeforeMethod
  public void setUp() throws Exception {
    rc = mock(RestClient.class);
    cfg = mock(ApplicationConfiguration.class);
    c = new MlController(
        new MlService(new MlMicroservice(new MlDataMapper(), rc, cfg)), new MlDtoMapper()
    );
  }

  @Test
  public void testPredict() throws Exception {
    // given

    doReturn("url").when(cfg).mlServiceURL();
    doReturn("prj{0}{1}").when(cfg).predict();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .build();

    byte[] result = new byte[]{};

    CompletableFuture<byte[]> future =
        CompletableFuture.completedFuture(result);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<byte[]>() {
        }
    );

    // when

    CompletableFuture<byte[]> f
        = c.predict("1", "2");

    byte[] r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r).isSameAs(result);
  }

  @Test
  public void testSetValue() throws Exception {
    // given

    ValueDto dto = new ValueDto(new StringId("1"), "cv", "ov");
    ValueData data = new ValueData(new StringId("1"), "cv", "ov");

    doReturn("url").when(cfg).mlServiceURL();
    doReturn("prj").when(cfg).value();

    RequestDetails rd = Builder.post("url")
        .request("prj")
        .content(Collections.singletonList(data), Types.JSON)
        .accept(Types.JSON)
        .build();

    byte[] result = new byte[]{};

    CompletableFuture<byte[]> future =
        CompletableFuture.completedFuture(result);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<byte[]>() {
        }
    );

    // when

    CompletableFuture<byte[]> f = c.setValue(Collections.singletonList(dto));

    byte[] r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r).isSameAs(result);
  }

  @Test
  public void testSetValueWithNullList() throws Exception {
    // given

    doReturn("url").when(cfg).mlServiceURL();
    doReturn("prj").when(cfg).value();

    RequestDetails rd = Builder.post("url")
        .request("prj")
        .content(Collections.emptyList(), Types.JSON)
        .accept(Types.JSON)
        .build();

    byte[] result = new byte[]{};

    CompletableFuture<byte[]> future =
        CompletableFuture.completedFuture(result);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<byte[]>() {
        }
    );

    // when

    CompletableFuture<byte[]> f = c.setValue(null);

    byte[] r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r).isSameAs(result);
  }

  @Test
  public void testSetValueWithNullElement() throws Exception {
    // given

    doReturn("url").when(cfg).mlServiceURL();
    doReturn("prj").when(cfg).value();

    RequestDetails rd = Builder.post("url")
        .request("prj")
        .content(Collections.singletonList(null), Types.JSON)
        .accept(Types.JSON)
        .build();

    byte[] result = new byte[]{};

    CompletableFuture<byte[]> future =
        CompletableFuture.completedFuture(result);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<byte[]>() {
        }
    );

    // when

    CompletableFuture<byte[]> f = c.setValue(Collections.singletonList(null));

    byte[] r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r).isSameAs(result);
  }

  @Test
  public void testValue() {
    // given
    Value v1 = new Value(new StringId("1"), "cv", "ov");
    Value v2 = new Value(new StringId("1"), "cv", "ov");
    Value v3 = new Value(new StringId("2"), "cv", "ov");
    Value v4 = new Value(new StringId("1"), "cva", "ov");
    Value v5 = new Value(new StringId("1"), "cv", "ovb");

    // when
    // then
    assertThat(v1.equals(null)).isFalse();
    assertThat(v1.equals(new Object())).isFalse();
    assertThat(v1.equals(v1)).isTrue();
    assertThat(v1.equals(v2)).isTrue();
    assertThat(v1.equals(v3)).isFalse();
    assertThat(v1.equals(v4)).isFalse();
    assertThat(v1.equals(v5)).isFalse();

    assertThat(v1.hashCode()).isEqualTo(v1.hashCode());
    assertThat(v1.hashCode()).isEqualTo(v2.hashCode());
    assertThat(v1.hashCode()).isNotEqualTo(v3.hashCode());
    assertThat(v2.hashCode()).isNotEqualTo(v4.hashCode());
    assertThat(v3.hashCode()).isNotEqualTo(v5.hashCode());
  }

  @Test
  public void testValueData() {
    // given
    ValueData v1 = new ValueData(new StringId("1"), "cv", "ov");
    ValueData v2 = new ValueData(new StringId("1"), "cv", "ov");
    ValueData v3 = new ValueData(new StringId("2"), "cv", "ov");
    ValueData v4 = new ValueData(new StringId("1"), "cva", "ov");
    ValueData v5 = new ValueData(new StringId("1"), "cv", "ovb");

    // when
    // then
    assertThat(v1.equals(null)).isFalse();
    assertThat(v1.equals(new Object())).isFalse();
    assertThat(v1.equals(v1)).isTrue();
    assertThat(v1.equals(v2)).isTrue();
    assertThat(v1.equals(v3)).isFalse();
    assertThat(v1.equals(v4)).isFalse();
    assertThat(v1.equals(v5)).isFalse();

    assertThat(v1.hashCode()).isEqualTo(v1.hashCode());
    assertThat(v1.hashCode()).isEqualTo(v2.hashCode());
    assertThat(v1.hashCode()).isNotEqualTo(v3.hashCode());
    assertThat(v2.hashCode()).isNotEqualTo(v4.hashCode());
    assertThat(v3.hashCode()).isNotEqualTo(v5.hashCode());
  }

  @Test
  public void testValueDto() {
    // given
    ValueDto v1 = new ValueDto(new StringId("1"), "cv", "ov");
    ValueDto v2 = new ValueDto(new StringId("1"), "cv", "ov");
    ValueDto v3 = new ValueDto(new StringId("2"), "cv", "ov");
    ValueDto v4 = new ValueDto(new StringId("1"), "cva", "ov");
    ValueDto v5 = new ValueDto(new StringId("1"), "cv", "ovb");

    // when
    // then
    assertThat(v1.equals(null)).isFalse();
    assertThat(v1.equals(new Object())).isFalse();
    assertThat(v1.equals(v1)).isTrue();
    assertThat(v1.equals(v2)).isTrue();
    assertThat(v1.equals(v3)).isFalse();
    assertThat(v1.equals(v4)).isFalse();
    assertThat(v1.equals(v5)).isFalse();

    assertThat(v1.hashCode()).isEqualTo(v1.hashCode());
    assertThat(v1.hashCode()).isEqualTo(v2.hashCode());
    assertThat(v1.hashCode()).isNotEqualTo(v3.hashCode());
    assertThat(v2.hashCode()).isNotEqualTo(v4.hashCode());
    assertThat(v3.hashCode()).isNotEqualTo(v5.hashCode());
  }
}
