package com.automationanywhere.cognitive.app.threadpool;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.mockito.Mockito;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ThreadPoolFactoryTest {

  private ThreadPoolFactory tpf;

  @BeforeMethod
  public void setUp() throws Exception {
    tpf = Mockito.spy(new ThreadPoolFactory());
  }

  @Test
  public void testCreate() {
    ThreadPoolTaskExecutor o = mock(ThreadPoolTaskExecutor.class);
    doReturn(o).when(tpf).create();

    assertThat(tpf.create(5, 10, 60, 5, "pref")).isSameAs(o);

    verify(o).setThreadNamePrefix("pref");
    verify(o).setCorePoolSize(5);
    verify(o).setMaxPoolSize(10);
    verify(o).setKeepAliveSeconds(60);
    verify(o).setWaitForTasksToCompleteOnShutdown(false);
    verify(o).setAwaitTerminationSeconds(5);
  }
}
