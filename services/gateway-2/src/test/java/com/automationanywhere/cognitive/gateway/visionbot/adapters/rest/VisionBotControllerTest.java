package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.visionbot.VisionBotService;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.FieldValueDefinitionData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.JsonPatchActionData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.JsonPatchActionRemoveData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.ProcessedDocumentDetailsData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.SummaryData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.TestSetData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.ValidationDetailsData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotDataMapper;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotDetailsData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotMicroservice;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotResponseData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotStateAndIdsData;
import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotStateData;
import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.core.ParameterizedTypeReference;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VisionBotControllerTest {

  private ApplicationConfiguration cfg;
  private VisionBotDataMapper datam;
  private VisionBotDtoMapper dtom;
  private VisionBotController c;
  private RestClient rc;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    rc = mock(RestClient.class);
    datam = spy(new VisionBotDataMapper());
    dtom = spy(new VisionBotDtoMapper());
    c = new VisionBotController(
        dtom, new VisionBotService(new VisionBotMicroservice(datam, rc, cfg))
    );
  }

  @Test
  public void testGetVisionBotForEdit() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}").when(cfg).getVisionBotForEdit();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .setHeader("userName", "nm")
        .accept(Types.JSON)
        .build();

    VisionBotData visionBotData = mock(VisionBotData.class);
    VisionBot visionBot = mock(VisionBot.class);
    VisionBotDto visionBotDto = mock(VisionBotDto.class);

    CompletableFuture<VisionBotData> future = CompletableFuture.completedFuture(visionBotData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<VisionBotData>() {
        }
    );

    doReturn(visionBot).when(datam).mapVisionBot(visionBotData);
    doReturn(visionBotDto).when(dtom).mapVisionBot(visionBot);

    // when

    CompletableFuture<StandardResponse<VisionBotDto>> f = c
        .getVisionBotForEdit(new StringId("1"), new StringId("2"), new StringId("3"), "nm");

    StandardResponse<VisionBotDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(visionBotDto);
  }

  @Test
  public void testGetVisionBot() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}").when(cfg).getVisionBot();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    VisionBotData visionBotData = mock(VisionBotData.class);
    VisionBot visionBot = mock(VisionBot.class);
    VisionBotDto visionBotDto = mock(VisionBotDto.class);

    CompletableFuture<VisionBotData> future = CompletableFuture.completedFuture(visionBotData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<VisionBotData>() {
        }
    );

    doReturn(visionBot).when(datam).mapVisionBot(visionBotData);
    doReturn(visionBotDto).when(dtom).mapVisionBot(visionBot);

    // when

    CompletableFuture<StandardResponse<VisionBotDto>> f = c
        .getVisionBot(new StringId("1"), new StringId("2"), new StringId("3"));

    StandardResponse<VisionBotDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(visionBotDto);
  }

  @Test
  public void testGetVisionBotById() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).getVisionBotById();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .accept(Types.JSON)
        .build();

    VisionBotData visionBotData = mock(VisionBotData.class);
    VisionBot visionBot = mock(VisionBot.class);
    VisionBotDto visionBotDto = mock(VisionBotDto.class);

    CompletableFuture<VisionBotData> future = CompletableFuture.completedFuture(visionBotData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<VisionBotData>() {
        }
    );

    doReturn(visionBot).when(datam).mapVisionBot(visionBotData);
    doReturn(visionBotDto).when(dtom).mapVisionBot(visionBot);

    // when

    CompletableFuture<StandardResponse<VisionBotDto>> f = c.getVisionBotById(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4")
    );

    StandardResponse<VisionBotDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(visionBotDto);
  }

  @Test
  public void testPatchVisionBot() throws Exception {
    // given
    List<JsonPatchActionData> actionsData = Collections.singletonList(
        new JsonPatchActionRemoveData("/abc")
    );

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).patchVisionBot();

    RequestDetails rd = Builder.patch("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .setHeader("username", "5")
        .content(actionsData, Types.JSON_PATCH)
        .accept(Types.JSON_PATCH)
        .build();

    CompletableFuture<StandardResponse<Boolean>> future = CompletableFuture.completedFuture(
        new StandardResponse<>(true)
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<Boolean>>() {
        }
    );

    List<JsonPatchActionDto> actionsDto = Collections.singletonList(new JsonPatchActionDto(
            JsonPatchActionOperation.REMOVE,
            "/abc",
            null,
            null
        )
    );

    // when
    CompletableFuture<StandardResponse<Boolean>> f = c.patchVisionBot(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4"),
        new StringId("5"), actionsDto
    );

    StandardResponse<Boolean> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isNull();
    assertThat(r.success).isTrue();
  }


  @Test
  public void testPatchVisionBotWithNullList() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).patchVisionBot();

    RequestDetails rd = Builder.patch("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .setHeader("username", "5")
        .content(Collections.emptyList(), Types.JSON_PATCH)
        .accept(Types.JSON_PATCH)
        .build();

    CompletableFuture<StandardResponse<Boolean>> future = CompletableFuture.completedFuture(
        new StandardResponse<>(true)
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<Boolean>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Boolean>> f = c.patchVisionBot(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4"),
        new StringId("5"),
        null
    );

    StandardResponse<Boolean> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.success).isTrue();
    assertThat(r.data).isNull();
  }

  @Test
  public void testGetVisionBotMetadata() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}").when(cfg).getVisionBotMetadata();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    VisionBotDetailsData visionBotDetailsData = mock(VisionBotDetailsData.class);
    VisionBotDetails visionBotDetails = mock(VisionBotDetails.class);
    VisionBotDetailsDto visionBotDetailsDto = mock(VisionBotDetailsDto.class);

    CompletableFuture<List<VisionBotDetailsData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(visionBotDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    );

    doReturn(visionBotDetails).when(datam).mapVisionBotDetails(visionBotDetailsData);
    doReturn(visionBotDetailsDto).when(dtom).mapVisionBotDetails(visionBotDetails);

    // when

    CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> f = c
        .getVisionBotMetadata(new StringId("1"), new StringId("2"), new StringId("3"));

    StandardResponse<List<VisionBotDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.singletonList(visionBotDetailsDto));
  }

  @Test
  public void testGetProjectVisionBotMetadata() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}").when(cfg).getProjectVisionBotMetadata();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    VisionBotDetailsData visionBotDetailsData = mock(VisionBotDetailsData.class);
    VisionBotDetails visionBotDetails = mock(VisionBotDetails.class);
    VisionBotDetailsDto visionBotDetailsDto = mock(VisionBotDetailsDto.class);

    CompletableFuture<List<VisionBotDetailsData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(visionBotDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    );

    doReturn(visionBotDetails).when(datam).mapVisionBotDetails(visionBotDetailsData);
    doReturn(visionBotDetailsDto).when(dtom).mapVisionBotDetails(visionBotDetails);

    // when

    CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> f = c
        .getProjectVisionBotMetadata(new StringId("1"), new StringId("2"));

    StandardResponse<List<VisionBotDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.singletonList(visionBotDetailsDto));
  }

  @Test
  public void testGetProjectVisionBot() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}").when(cfg).getProjectVisionBot();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    VisionBotDetailsData visionBotDetailsData = mock(VisionBotDetailsData.class);
    VisionBotDetails visionBotDetails = mock(VisionBotDetails.class);
    VisionBotDetailsDto visionBotDetailsDto = mock(VisionBotDetailsDto.class);

    CompletableFuture<List<VisionBotDetailsData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(visionBotDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    );

    doReturn(visionBotDetails).when(datam).mapVisionBotDetails(visionBotDetailsData);
    doReturn(visionBotDetailsDto).when(dtom).mapVisionBotDetails(visionBotDetails);

    // when

    CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> f = c
        .getProjectVisionBot(new StringId("1"), new StringId("2"));

    StandardResponse<List<VisionBotDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.singletonList(visionBotDetailsDto));
  }

  @Test
  public void testGetOrganizationVisionBot() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}").when(cfg).getOrganizationVisionBot();

    RequestDetails rd = Builder.get("url")
        .request("req{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    Map<String, Object> map = new HashMap<>();

    CompletableFuture<List<Map<String, Object>>> future = CompletableFuture
        .completedFuture(Collections.singletonList(map));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<Map<String, Object>>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<Map<String, Object>>>> f = c
        .getOrganizationVisionBot(new StringId("1"));

    StandardResponse<List<Map<String, Object>>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.singletonList(map));
  }

  @Test
  public void testGetOrganizationVisionBotById() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}").when(cfg).getOrganizationVisionBotById();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    VisionBotDetailsData visionBotDetailsData = mock(VisionBotDetailsData.class);
    VisionBotDetails visionBotDetails = mock(VisionBotDetails.class);
    VisionBotDetailsDto visionBotDetailsDto = mock(VisionBotDetailsDto.class);

    CompletableFuture<List<VisionBotDetailsData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(visionBotDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    );

    doReturn(visionBotDetails).when(datam).mapVisionBotDetails(visionBotDetailsData);
    doReturn(visionBotDetailsDto).when(dtom).mapVisionBotDetails(visionBotDetails);

    // when

    CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> f = c
        .getOrganizationVisionBotById(new StringId("1"), new StringId("2"));

    StandardResponse<List<VisionBotDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.singletonList(visionBotDetailsDto));
  }

  @Test
  public void testGetOrganizationVisionBotMetadata() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}").when(cfg).getOrganizationVisionBotMetadata();

    RequestDetails rd = Builder.get("url")
        .request("req{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    VisionBotDetailsData visionBotDetailsData = mock(VisionBotDetailsData.class);
    VisionBotDetails visionBotDetails = mock(VisionBotDetails.class);
    VisionBotDetailsDto visionBotDetailsDto = mock(VisionBotDetailsDto.class);

    CompletableFuture<List<VisionBotDetailsData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(visionBotDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    );

    doReturn(visionBotDetails).when(datam).mapVisionBotDetails(visionBotDetailsData);
    doReturn(visionBotDetailsDto).when(dtom).mapVisionBotDetails(visionBotDetails);

    // when

    CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> f = c
        .getOrganizationVisionBotMetadata(new StringId("1"));

    StandardResponse<List<VisionBotDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.singletonList(visionBotDetailsDto));
  }

  @Test
  public void testUpdateVisionBot() throws Exception {
    // given

    VisionBotData visionBotData = mock(VisionBotData.class);
    VisionBotData visionBotData2 = mock(VisionBotData.class);
    VisionBot visionBot = mock(VisionBot.class);
    VisionBot visionBot2 = mock(VisionBot.class);
    VisionBotDto visionBotDto = mock(VisionBotDto.class);
    VisionBotDto visionBotDto2 = mock(VisionBotDto.class);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}").when(cfg).updateVisionBot();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .content(visionBotData2, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<VisionBotData> future = CompletableFuture.completedFuture(visionBotData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<VisionBotData>() {
        }
    );

    doReturn(visionBot).when(datam).mapVisionBot(visionBotData);
    doReturn(visionBotDto).when(dtom).mapVisionBot(visionBot);

    doReturn(visionBot2).when(dtom).mapVisionBot(visionBotDto2);
    doReturn(visionBotData2).when(datam).mapVisionBot(visionBot2);

    // when

    CompletableFuture<StandardResponse<VisionBotDto>> f = c
        .updateVisionBot(new StringId("1"), new StringId("2"), new StringId("3"), visionBotDto2);

    StandardResponse<VisionBotDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(visionBotDto);
  }

  @Test
  public void testUpdateValidationDocument() throws Exception {
    // given

    ProcessedDocumentDetailsData data1 = mock(ProcessedDocumentDetailsData.class);
    ProcessedDocumentDetailsData data2 = mock(ProcessedDocumentDetailsData.class);
    ProcessedDocumentDetails model1 = mock(ProcessedDocumentDetails.class);
    ProcessedDocumentDetails model2 = mock(ProcessedDocumentDetails.class);
    ProcessedDocumentDetailsDto dto1 = mock(ProcessedDocumentDetailsDto.class);
    ProcessedDocumentDetailsDto dto2 = mock(ProcessedDocumentDetailsDto.class);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}{5}").when(cfg).updateValidationDocument();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}{4}{5}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .param("6")
        .content(data2, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<ProcessedDocumentDetailsData> future = CompletableFuture
        .completedFuture(data1);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ProcessedDocumentDetailsData>() {
        }
    );

    doReturn(model1).when(datam).mapProcessedDocumentDetails(data1);
    doReturn(dto1).when(dtom).mapProcessedDocumentDetails(model1);

    doReturn(model2).when(dtom).mapProcessedDocumentDetails(dto2);
    doReturn(data2).when(datam).mapProcessedDocumentDetails(model2);

    // when

    CompletableFuture<StandardResponse<ProcessedDocumentDetailsDto>> f = c.updateValidationDocument(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5"), new StringId("6"),
        dto2);

    StandardResponse<ProcessedDocumentDetailsDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(dto1);
  }

  @Test
  public void testUpdateValidationDetails() throws Exception {
    // given

    ValidationDetailsData data = mock(ValidationDetailsData.class);
    ValidationDetails model = mock(ValidationDetails.class);
    ValidationDetailsDto dto = mock(ValidationDetailsDto.class);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).updateValidationData();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .content(data, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    doReturn(model).when(dtom).mapValidationDetails(dto);
    doReturn(data).when(datam).mapValidationDetails(model);

    // when

    CompletableFuture<StandardResponse<String>> f = c
        .updateValidationDetails(new StringId("1"), new StringId("2"), new StringId("3"),
            new StringId("4"), dto);

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testUpdateAutoMappingData() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).updateAutoMappingData();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .content("automappingdata", Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // WHEN
    CompletableFuture<StandardResponse<String>> f = c.updateAutoMappingData(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4"), "automappingdata"
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGenerateVisionBotData() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).generateVisionBotData();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.generateVisionBotData(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5")
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGenerateVisionBotDataPaginated() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}{5}").when(cfg).generateVisionBotDataPaginated();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}{5}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .param(0)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.generateVisionBotDataPaginated(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5"), 0
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testUpdateVisionBotById() throws Exception {
    // given

    VisionBotData visionBotData = mock(VisionBotData.class);
    VisionBot visionBot = mock(VisionBot.class);
    VisionBotDto visionBotDto = mock(VisionBotDto.class);
    Map<String, Object> map = new HashMap<>();

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).updateVisionBotById();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .content(map, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<VisionBotData> future = CompletableFuture.completedFuture(visionBotData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<VisionBotData>() {
        }
    );

    doReturn(visionBot).when(datam).mapVisionBot(visionBotData);
    doReturn(visionBotDto).when(dtom).mapVisionBot(visionBot);

    // when

    CompletableFuture<StandardResponse<VisionBotDto>> f = c.updateVisionBotById(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4"), map
    );

    StandardResponse<VisionBotDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(visionBotDto);
  }

  @Test
  public void testUnlock() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).unlock();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .setHeader("username", "5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<Id> future = CompletableFuture.completedFuture(new StringId("a"));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Id>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Id>> f = c
        .unlock(
            new StringId("1"), new StringId("2"), new StringId("3"),
            new StringId("4"), new StringId("5")
        );

    StandardResponse<Id> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(new StringId("a"));
  }

  @Test
  public void testRunVisionBot() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).runVisionBot();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("staging")
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c
        .runVisionBot(
            new StringId("1"), new StringId("2"), new StringId("3"),
            new StringId("4"), "staging"
        );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGetValueField() throws Exception {
    // given

    FieldValueDefinitionData fieldValueDefinitionData = mock(FieldValueDefinitionData.class);
    FieldValueDefinition fieldValueDefinition = mock(FieldValueDefinition.class);
    FieldValueDefinitionDto fieldValueDefinitionDto = mock(FieldValueDefinitionDto.class);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}{5}{6}").when(cfg).getValueField();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}{4}{5}{6}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .param("6")
        .param("7")
        .content(fieldValueDefinitionData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture
        .completedFuture(new StandardResponse<>("data"));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    doReturn(fieldValueDefinition).when(dtom).mapFieldValueDefinition(fieldValueDefinitionDto);
    doReturn(fieldValueDefinitionData).when(datam).mapFieldValueDefinition(fieldValueDefinition);

    // when

    CompletableFuture<StandardResponse<String>> f = c.getValueField(
        new StringId("1"),
        new StringId("2"),
        new StringId("3"),
        new StringId("4"),
        new StringId("5"),
        new StringId("6"),
        new StringId("7"),
        fieldValueDefinitionDto
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGetValidationData() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).getValidationData();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.getValidationData(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4")
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGetVisionBotByLayout() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).getVisionBotByLayout();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.getVisionBotByLayout(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5")
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGetVisionBotByLayoutAndPage() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}{5}").when(cfg).getVisionBotByLayoutAndPage();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}{5}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .param(0)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // WHEN
    CompletableFuture<StandardResponse<String>> f = c.getVisionBotByLayoutAndPage(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5"), 0
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testGetSummary() throws Exception {
    // given

    SummaryData summaryData = mock(SummaryData.class);
    Summary summary = mock(Summary.class);
    SummaryDto summaryDto = mock(SummaryDto.class);

    doReturn(summary).when(datam).mapSummary(summaryData);
    doReturn(summaryDto).when(dtom).mapSummary(summary);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}").when(cfg).getSummary();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    CompletableFuture<SummaryData> future = CompletableFuture.completedFuture(summaryData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<SummaryData>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<SummaryDto>> f = c.getSummary(
        new StringId("1"), new StringId("2")
    );

    StandardResponse<SummaryDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(summaryDto);
  }

  @Test
  public void testGetTestSet() throws Exception {
    // given

    TestSetData testSetData = mock(TestSetData.class);
    TestSet testSet = mock(TestSet.class);
    TestSetDto testSetDto = mock(TestSetDto.class);

    doReturn(testSet).when(datam).mapTestSet(testSetData);
    doReturn(testSetDto).when(dtom).mapTestSet(testSet);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).getTestSet();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<TestSetData> future = CompletableFuture.completedFuture(testSetData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<TestSetData>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<TestSetDto>> f = c.getTestSet(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5")
    );

    StandardResponse<TestSetDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(testSetDto);
  }

  @Test
  public void testUpdateTestSet() throws Exception {
    // given

    TestSetData data1 = mock(TestSetData.class);
    TestSet model1 = mock(TestSet.class);
    TestSetDto dto1 = mock(TestSetDto.class);

    TestSetData data2 = mock(TestSetData.class);
    TestSet model2 = mock(TestSet.class);
    TestSetDto dto2 = mock(TestSetDto.class);

    doReturn(model1).when(dtom).mapTestSet(dto1);
    doReturn(data1).when(datam).mapTestSet(model1);
    doReturn(model2).when(datam).mapTestSet(data2);
    doReturn(dto2).when(dtom).mapTestSet(model2);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).updateTestSet();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .content(data1, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<TestSetData> future = CompletableFuture.completedFuture(data2);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<TestSetData>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<TestSetDto>> f = c.updateTestSet(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5"), dto1
    );

    StandardResponse<TestSetDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(dto2);
  }

  @Test
  public void testDeleteTestSet() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).deleteTestSet();

    RequestDetails rd = Builder.delete("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .build();

    CompletableFuture<Void> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Void>> f = c.deleteTestSet(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), new StringId("5")
    );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isNull();
  }

  @Test
  public void testGetPageIndex() throws Exception {
    // given

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}{4}").when(cfg).getPageImage();

    RequestDetails rd = Builder.get("url")
        .request("req{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.getPageImage(
        new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), "5"
    );

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testSetState() throws Exception {
    // given

    VisionBotStateData data = mock(VisionBotStateData.class);
    VisionBotStateDto dto = mock(VisionBotStateDto.class);
    VisionBotState model = mock(VisionBotState.class);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).setState();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .content(data, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    doReturn(model).when(dtom).mapVisionBotState(dto);
    doReturn(data).when(datam).mapVisionBotState(model);

    // when

    CompletableFuture<StandardResponse<String>> f = c.setState(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4"), dto);

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }

  @Test
  public void testUpdateMultipleBotState() throws Exception {
    // given
    VisionBotElementDto visionBotElementDto = mock(VisionBotElementDto.class);
    VisionBotStateAndIdsData visionBotStateAndIdsData = mock(VisionBotStateAndIdsData.class);
    VisionBotElements visionBotElements = mock(VisionBotElements.class);
    VisionBotResponseData visionBotResponseData = mock(VisionBotResponseData.class);
    VisionBotResponseDto visionBotResponseDto = mock(VisionBotResponseDto.class);
    VisionBotResponse visionBotResponse = mock(VisionBotResponse.class);

    doReturn(visionBotElements).when(dtom).mapVisionBotStateDetails(visionBotElementDto);
    doReturn(visionBotStateAndIdsData).when(datam).mapVisionBotElements(visionBotElements);
    doReturn(visionBotResponse).when(datam).mapVisionBotResponse(visionBotResponseData);
    doReturn(visionBotResponseDto).when(dtom).mapVisionBotResponseDto(visionBotResponse);

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}").when(cfg).getMutipleState();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}")
        .param("1")
        .param("2")
        .content(visionBotStateAndIdsData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<VisionBotResponse> future = CompletableFuture.completedFuture(
        new VisionBotResponse("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<VisionBotResponseData>() {
        }
    );

    CompletableFuture<VisionBotResponseDto> a =
        c.updateMultipleBotState(new StringId("1"), new StringId("2"), visionBotElementDto);

    // then
    assertThat(a.isDone()).isTrue();

  }

  @Test
  public void testExportToCSV() throws Exception {
    // given

    Map<String, Object> map = new HashMap<>();

    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}").when(cfg).exportToCSV();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .content(map, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<String>> future = CompletableFuture.completedFuture(
        new StandardResponse<>("data")
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.exportToCSV(
        new StringId("1"), new StringId("2"), new StringId("3"), map);

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("data");
  }


  @Test
  public void testKeepAliveLockedVisionBot() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).visionBotURL();
    doReturn("req{0}{1}{2}{3}").when(cfg).keepAliveLockedVisionBot();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .accept(Types.JSON)
        .build();

    doReturn(CompletableFuture.completedFuture(new StandardResponse<>(new byte[0]))).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<byte[]>>() {
        }
    );

    // WHEN
    CompletableFuture<StandardResponse<Void>> f = c.keepAliveLockedVisionBot(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4")
    );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.success).isTrue();
  }
}
