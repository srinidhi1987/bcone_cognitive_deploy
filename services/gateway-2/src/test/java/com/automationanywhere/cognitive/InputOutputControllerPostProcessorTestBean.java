package com.automationanywhere.cognitive;

import com.automationanywhere.cognitive.validation.SchemaValidation;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InputOutputControllerPostProcessorTestBean {

  @SchemaValidation(input = "in", output = "out")
  public String pub(final Object p1) {
    return null;
  }
}
