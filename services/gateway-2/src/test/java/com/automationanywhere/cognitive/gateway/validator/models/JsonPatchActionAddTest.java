package com.automationanywhere.cognitive.gateway.validator.models;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.node.TextNode;
import org.testng.annotations.Test;

public class JsonPatchActionAddTest {

  @Test
  public void testJsonPatchActionAddEquals() {
    // GIVEN
    JsonPatchActionAdd data1 = new JsonPatchActionAdd("/abc", new TextNode("123"));
    JsonPatchActionAdd data2 = new JsonPatchActionAdd("/abc", new TextNode("123"));
    JsonPatchActionAdd data3 = new JsonPatchActionAdd("/def", new TextNode("123"));
    JsonPatchActionAdd data4 = new JsonPatchActionAdd("/abc", new TextNode("456"));

    // WHEN
    boolean equals = data1.equals(data2);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(data3);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data2.equals(data4);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(null);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(data1);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(new Object());

    // THEN
    assertThat(equals).isFalse();
  }

  @Test
  public void testJsonPatchActionAddHashCode() {
    // GIVEN
    JsonPatchActionAdd data1 = new JsonPatchActionAdd("/abc", new TextNode("123"));
    JsonPatchActionAdd data2 = new JsonPatchActionAdd("/abc", new TextNode("123"));
    JsonPatchActionAdd data3 = new JsonPatchActionAdd("/def", new TextNode("123"));
    JsonPatchActionAdd data4 = new JsonPatchActionAdd("/abc", new TextNode("456"));

    // WHEN
    int data1Hash = data1.hashCode();
    int data2Hash = data2.hashCode();
    int data3Hash = data3.hashCode();
    int data4Hash = data4.hashCode();

    // THEN
    assertThat(data1Hash).isEqualTo(data2Hash);
    assertThat(data1Hash).isNotEqualTo(data3Hash);
    assertThat(data2Hash).isNotEqualTo(data4Hash);
  }
}
