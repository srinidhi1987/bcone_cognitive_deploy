package com.automationanywhere.cognitive;

import static com.xebialabs.restito.semantics.Condition.custom;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebialabs.restito.semantics.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

public class BaseControllerTest extends AbstractTestNGSpringContextTests {

  protected final ExceptionHandlerFactory ehf = new ExceptionHandlerFactory();

  @Autowired
  protected ObjectMapper om;

  protected <T> Condition withBody(final T c, final Class<T> type) {
    return custom(input -> ehf.call(() ->
        om.readValue(input.getPostBody(), type).equals(c)
    ));
  }
}
