package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.CharArrayWriter;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

public class CustomAccessLogValveTest {

  @Test
  public void testLog() throws Exception {
    CharArrayWriter msg = new CharArrayWriter();
    msg.write("TesT");

    Logger l = mock(Logger.class);

    CustomAccessLogValve v = spy(new CustomAccessLogValve());
    doReturn(l).when(v).getAccessLogger();

    v.log(msg);

    verify(l).trace("TesT");
  }

  @Test
  public void testLogFailure() throws Exception {
    IOException e = mock(IOException.class);

    CharArrayWriter msg = mock(CharArrayWriter.class);
    doThrow(e).when(msg).writeTo(any());

    CustomAccessLogValve v = spy(new CustomAccessLogValve());

    v.log(msg);

    verify(msg, never()).flush();
  }
}
