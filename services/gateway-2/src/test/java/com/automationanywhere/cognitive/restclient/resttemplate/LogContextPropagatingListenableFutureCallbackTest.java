package com.automationanywhere.cognitive.restclient.resttemplate;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.concurrent.SettableListenableFuture;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LogContextPropagatingListenableFutureCallbackTest {
  @Test
  public void testSuccess() {
    ClientHttpResponse r = mock(ClientHttpResponse.class);
    SettableListenableFuture<ClientHttpResponse> f = mock(SettableListenableFuture.class);
    LogContextPropagatingListenableFutureCallback cb = new LogContextPropagatingListenableFutureCallback(f);

    cb.onSuccess(r);

    verify(f).set(r);
  }

  @Test
  public void testFailure() {
    Exception e = mock(Exception.class);
    SettableListenableFuture<ClientHttpResponse> f = mock(SettableListenableFuture.class);
    LogContextPropagatingListenableFutureCallback cb = new LogContextPropagatingListenableFutureCallback(f);

    cb.onFailure(e);

    verify(f).setException(e);
  }
}
