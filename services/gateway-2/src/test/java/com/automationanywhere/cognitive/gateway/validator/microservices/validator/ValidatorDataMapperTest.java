package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ClassificationFieldDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotdata;
import com.automationanywhere.cognitive.gateway.validator.models.FieldAccuracyDashboardDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FieldValidation;
import com.automationanywhere.cognitive.gateway.validator.models.GroupTimeSpentDetails;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.id.string.StringId;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.Test;

public class ValidatorDataMapperTest {

  private final ValidatorDataMapper mapper = new ValidatorDataMapper();

  @Test
  public void mapReviewedFileCountDetailsShouldMapDtoToData() {
    // given
    ReviewedFileCountDetailsData d1 = new ReviewedFileCountDetailsData(
        new StringId("1"), 1, 2, 3, 4
    );

    ReviewedFileCountDetails m1 = new ReviewedFileCountDetails(
        new StringId("1"), 1, 2, 3, 4
    );

    // when
    ReviewedFileCountDetails r1 = mapper.mapReviewedFileCountDetails(d1);
    ReviewedFileCountDetails r2 = mapper.mapReviewedFileCountDetails(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }


  @Test
  public void mapSuccessShouldMapDataToModel() {
    // given5
    SuccessData d1 = new SuccessData(
        true
    );

    Success m1 = new Success(
        true
    );

    // when
    Success r1 = mapper.mapSuccess(d1);
    Success r2 = mapper.mapSuccess(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }


  @Test
  public void mapInvalidFileTypeShouldMapDataToModel() {
    // given
    InvalidFileTypeData d1 = new InvalidFileTypeData(
        new StringId("1"), "ok"
    );

    InvalidFileType m1 = new InvalidFileType(
        new StringId("1"), "ok"
    );

    // when
    InvalidFileType r1 = mapper.mapInvalidFileType(d1);
    InvalidFileType r2 = mapper.mapInvalidFileType(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }


  @Test
  public void mapProductionFileSummaryShouldMapDataToModel() {
    // given
    ProductionFileSummaryData d1 = new ProductionFileSummaryData(
        new StringId("1"), 1, 2, 3, 4, 5, 6.0
    );

    ProductionFileSummary m1 = new ProductionFileSummary(
        new StringId("1"), 1, 2, 3, 4, 5, 6.0
    );

    // when
    ProductionFileSummary r1 = mapper.mapProductionFileSummary(d1);
    ProductionFileSummary r2 = mapper.mapProductionFileSummary(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }


  @Test
  public void mapInvalidFileShouldMapModelToData() {
    // given
    InvalidFileData d1 = new InvalidFileData(
        new StringId("1"), "ok", "tx"
    );

    InvalidFile m1 = new InvalidFile(
        new StringId("1"), "ok", "tx"
    );

    // when
    InvalidFileData r1 = mapper.mapInvalidFile(m1);
    InvalidFileData r2 = mapper.mapInvalidFile(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFailedVBotdata() {
    // GIVEN
    int fileId = 1;
    int orgId = 1;
    int projectId = 1;
    String vBotDocument = "vBotDocument";
    String correctedData = "correctedData";
    String validationStatus = "validationStatus";
    String createdAt = "createdAt";
    String updatedBy = "updatedBy";
    String updatedAt = "updatedAt";
    String visionBotId = "visionBotId";
    String lockedUserId = "lockedUserId";
    int failedFieldCounts = 1;
    int passFieldCounts = 1;
    int totalFieldCounts = 1;
    String startTime = "startTime";
    String endTime = "endTime";
    int averageTimeInSeconds = 1;
    String lastExportTime = "lastExportTime";

    FailedVbotdataData d1 = new FailedVbotdataData(
        fileId, orgId, projectId, vBotDocument, correctedData, validationStatus, createdAt,
        updatedBy, updatedAt, visionBotId, lockedUserId, failedFieldCounts, passFieldCounts,
        totalFieldCounts, startTime, endTime, averageTimeInSeconds, lastExportTime
    );
    FailedVbotdata m1 = new FailedVbotdata(
        fileId, orgId, projectId, vBotDocument, correctedData, validationStatus, createdAt,
        updatedBy, updatedAt, visionBotId, lockedUserId, failedFieldCounts, passFieldCounts,
        totalFieldCounts, startTime, endTime, averageTimeInSeconds, lastExportTime
    );

    // WHEN
    FailedVbotdata r1 = mapper.mapFailedVBotData(d1);
    FailedVbotdata r2 = mapper.mapFailedVBotData(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByField(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapAccuracyProcessingDetails() {
    // GIVEN
    String id = "id";
    double failedFieldCounts = 1.0;
    double passFieldCounts = 1.0;
    double totalFieldCounts = 1.0;
    double fieldAccuracy = 1.0;
    long reviewFileCount = 1L;
    long invalidFileCount = 1L;
    long failedDocumentCount = 1L;
    long passedDocumentCounts = 1L;
    long processedDocumentCounts = 1L;
    long documentAccuracy = 1L;
    long averageReviewTimeInSeconds = 1L;

    AccuracyProcessingDetailsData d1 = new AccuracyProcessingDetailsData(
        id, failedFieldCounts, passFieldCounts, totalFieldCounts, fieldAccuracy, reviewFileCount,
        invalidFileCount, failedDocumentCount, passedDocumentCounts, processedDocumentCounts,
        documentAccuracy, averageReviewTimeInSeconds
    );
    AccuracyProcessingDetails m1 = new AccuracyProcessingDetails(
        id, failedFieldCounts, passFieldCounts, totalFieldCounts, fieldAccuracy, reviewFileCount,
        invalidFileCount, failedDocumentCount, passedDocumentCounts, processedDocumentCounts,
        documentAccuracy, averageReviewTimeInSeconds
    );

    // WHEN
    AccuracyProcessingDetails r1 = mapper.mapAccuracyProcessingDetails(d1);
    AccuracyProcessingDetails r2 = mapper.mapAccuracyProcessingDetails(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByField(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectTotalsAccuracyDetails() {
    // GIVEN
    int totalFilesProcessed = 1;
    int totalSTP = 2;
    int totalAccuracy = 3;
    int totalFilesUploaded = 4;
    int totalFilesToValidation = 5;
    int totalFilesValidated = 6;
    int totalFilesUnprocessable = 7;

    ClassificationFieldDetailsData[] classificationData = new ClassificationFieldDetailsData[]{
        new ClassificationFieldDetailsData("Classification", 2.2)
    };

    ClassificationFieldDetails[] classification = new ClassificationFieldDetails[]{
        new ClassificationFieldDetails("Classification", 2.2)
    };

    FieldAccuracyDashboardDetailsData[] accuracyData = new FieldAccuracyDashboardDetailsData[]{
        new FieldAccuracyDashboardDetailsData("Accuracy", 3.3)
    };
    FieldAccuracyDashboardDetails[] accuracy = new FieldAccuracyDashboardDetails[]{
        new FieldAccuracyDashboardDetails("Accuracy", 3.3)
    };

    FieldValidationData[] validationData = new FieldValidationData[]{
        new FieldValidationData("Validation", 4.4)
    };
    FieldValidation[] validation = new FieldValidation[]{
        new FieldValidation("Validation", 4.4)
    };

    GroupTimeSpentDetailsData[] categoriesData = new GroupTimeSpentDetailsData[]{
        new GroupTimeSpentDetailsData("Categories", 5.5)
    };
    GroupTimeSpentDetails[] categories = new GroupTimeSpentDetails[]{
        new GroupTimeSpentDetails("Categories", 5.5)
    };

    ProjectTotalsAccuracyDetailsData d1 = new ProjectTotalsAccuracyDetailsData(
        totalFilesProcessed, totalSTP, totalAccuracy, totalFilesUploaded, totalFilesToValidation,
        totalFilesValidated, totalFilesUnprocessable, classificationData, accuracyData,
        validationData, categoriesData
    );
    ProjectTotalsAccuracyDetails m1 = new ProjectTotalsAccuracyDetails(
        totalFilesProcessed, totalSTP, totalAccuracy, totalFilesUploaded, totalFilesToValidation,
        totalFilesValidated, totalFilesUnprocessable, classification, accuracy, validation,
        categories
    );

    // WHEN
    ProjectTotalsAccuracyDetails r1 = mapper.mapPrjTotAccuracyDetails(d1);
    ProjectTotalsAccuracyDetails r2 = mapper.mapPrjTotAccuracyDetails(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFailedVbotStatus() {
    // GIVEN
    FailedVbotStatusData d1 = new FailedVbotStatusData(true, 1, 0);
    FailedVbotStatus m1 = new FailedVbotStatus(true, 1, 0);

    // WHEN
    FailedVbotStatus r1 = mapper.mapFailedVBotStatus(d1);
    FailedVbotStatus r2 = mapper.mapFailedVBotStatus(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByField(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapJsonPatchDocumentAdd() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionAdd("/abc", v1))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionAddData("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentReplace() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionReplace("/abc", v1))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionReplaceData("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentRemove() {
    // GIVEN
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionRemove("/abc"))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionRemoveData("/abc"));
  }

  @Test
  public void mapJsonPatchDocumentTest() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionTest("/abc", v1))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionTestData("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentCopy() {
    // GIVEN
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionCopy("/abc", "123"))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionCopyData("/abc", "123"));
  }

  @Test
  public void mapJsonPatchDocumentMove() {
    // GIVEN
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionMove("/abc", "123"))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionMoveData("/abc", "123"));
  }
}
