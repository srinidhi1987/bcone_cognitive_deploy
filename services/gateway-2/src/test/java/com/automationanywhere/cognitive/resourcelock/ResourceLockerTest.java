package com.automationanywhere.cognitive.resourcelock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectMicroservice;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.id.string.StringId;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiConsumer;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ResourceLockerTest {
  private ResourceLocker rl;
  private ProjectMicroservice pm;

  @BeforeMethod
  public void setUp() throws Exception {
    ApplicationConfiguration cfg = mock(ApplicationConfiguration.class);
    doReturn(100L).when(cfg).resourceLockerPollPeriod();

    pm = mock(ProjectMicroservice.class);

    rl = new ResourceLocker(pm, cfg);
    rl.init();
  }

  @AfterMethod
  public void tearDown() throws Exception {
    rl.destroy();
  }

  @Test
  public void lockShouldReturnTrueWhenAResourceIsLocked() {
    // given
    ExclusiveResource r = new ExclusiveResource("act", new StringId("a"), "/p");
    rl.lock(r);

    // when
    boolean l = rl.isLocked(r);

    // then
    assertThat(l).isTrue();
  }

  @Test
  public void lockShouldReturnFalseWhenAResourceIsNotLocked() {
    // given
    ExclusiveResource r1 = new ExclusiveResource("act", new StringId("a"), "/p");
    ExclusiveResource r2 = new ExclusiveResource("act", new StringId("a"), "/p");
    rl.lock(r1);

    // when
    boolean l = rl.isLocked(r2);

    // then
    assertThat(l).isFalse();
  }

  @Test
  public void lockShouldThrowIfResourceIsNull() {
    // given

    // when
    Throwable t = catchThrowable(() -> rl.lock(null));

    // then
    assertThat(t).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void unlockShouldThrowIfResourceIsNull() {
    // given

    // when
    Throwable t = catchThrowable(() -> rl.unlock(null));

    // then
    assertThat(t).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void startShouldThrowIfResourceIsNull() {
    // given

    // when
    Throwable t = catchThrowable(() -> rl.start(null));

    // then
    assertThat(t).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void lockShouldThrowIfAnotherExclusiveResourceIsLocked() {
    // given
    Resource r1 = new Resource("/p");
    ExclusiveResource r2 = new ExclusiveResource("act2", new StringId("a"), "/p");
    rl.lock(r1);

    // when
    Throwable t = catchThrowable(() -> rl.lock(r2));

    // then
    assertThat(((LockedException) t).activityName).isEqualTo("activity");
  }

  @Test
  public void startShouldStartPolling() throws Exception {
    // given
    ExclusiveResource r = new ExclusiveResource("act", new StringId("a"), "/p");

    CountDownLatch latch = new CountDownLatch(1);
    CompletableFuture<List<TaskStatus>> f = mock(CompletableFuture.class);
    doAnswer(ctx -> {
      ((BiConsumer<List<TaskStatus>, Throwable>) ctx.getArgument(0)).accept(
          null, new Exception()
      );
      return null;
    }).doAnswer(ctx -> {
      ((BiConsumer<List<TaskStatus>, Throwable>) ctx.getArgument(0)).accept(
          Collections.singletonList(mock(TaskStatus.class)), null
      );
      return null;
    }).doAnswer(ctx -> {
      ((BiConsumer<List<TaskStatus>, Throwable>) ctx.getArgument(0)).accept(
          Collections.emptyList(), null
      );
      latch.countDown();
      return null;
    }).when(f).whenComplete(any());

    doReturn(f).when(pm).getOrganizationProjectTaskStatus(new StringId("a"));

    rl.lock(r);

    // when
    rl.start(r);
    latch.await();

    // then
    assertThat(rl.isLocked(r)).isFalse();
  }
}
