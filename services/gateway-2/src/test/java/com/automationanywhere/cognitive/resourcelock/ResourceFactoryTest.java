package com.automationanywhere.cognitive.resourcelock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.path.PathManager;
import java.lang.reflect.Method;
import org.springframework.web.method.HandlerMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ResourceFactoryTest {
  private ResourceFactory rf;
  private PathManager pm;
  private ApplicationConfiguration cfg;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    doReturn("/block").when(cfg).resourceLockerImportResource();
    doReturn("/unblock").when(cfg).resourceLockerExportResource();

    pm = mock(PathManager.class);
    rf = new ResourceFactory(cfg, pm);
  }

  @Test
  public void resourceShouldThrowIfActivityNameIsNull() {
    // given

    // when
    Throwable t = catchThrowable(() -> new Resource(null, "/p"));

    // then
    assertThat(t).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void resourceShouldThrowIfPathNameIsNull() {
    // given

    // when
    Throwable t = catchThrowable(() -> new Resource("an", null));

    // then
    assertThat(t).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void exclusiveResourceShouldThrowIfPathOrgIdIsNull() {
    // given

    // when
    Throwable t = catchThrowable(() -> new ExclusiveResource("an", null, "/p"));

    // then
    assertThat(t).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void createShouldCreateExclusiveResourceForImport() throws Exception {
    // given
    HandlerMethod h = mock(HandlerMethod.class);
    Method m = ResourceFactoryTest.class.getMethod("setUp");
    doReturn(m).when(h).getMethod();
    doReturn("/block").when(pm).create(m);

    // when
    ExclusiveResource r = (ExclusiveResource) rf.create("/p1", h);

    // then
    assertThat(r.activityName).isEqualTo("import");
    assertThat(r.orgId).isEqualTo(new StringId("1"));
    assertThat(r.path).isEqualTo("/p1");
  }

  @Test
  public void createShouldCreateExclusiveResourceForExport() throws Exception {
    // given
    HandlerMethod h = mock(HandlerMethod.class);
    Method m = ResourceFactoryTest.class.getMethod("setUp");
    doReturn(m).when(h).getMethod();
    doReturn("/unblock").when(pm).create(m);

    // when
    ExclusiveResource r = (ExclusiveResource) rf.create("/p2", h);

    // then
    assertThat(r.activityName).isEqualTo("export");
    assertThat(r.orgId).isEqualTo(new StringId("1"));
    assertThat(r.path).isEqualTo("/p2");
  }

  @Test
  public void createShouldThrowIfResourceIsUnknown() throws Exception {
    // given
    doReturn("/bad").when(cfg).resourceLockerExportResource();
    doReturn("/unbad").when(cfg).resourceLockerImportResource();

    HandlerMethod h = mock(HandlerMethod.class);
    Method m = ResourceFactoryTest.class.getMethod("setUp");
    doReturn(m).when(h).getMethod();
    doReturn("/unblock").when(pm).create(m);

    // when
    Throwable t = catchThrowable(() -> rf.create("/p2", h));

    // then
    assertThat(t).isInstanceOf(IllegalStateException.class);
  }

  @Test
  public void createShouldCreateResource() throws Exception {
    // given
    HandlerMethod h = mock(HandlerMethod.class);
    Method m = ResourceFactoryTest.class.getMethod("setUp");
    doReturn(m).when(h).getMethod();
    doReturn("/resource").when(pm).create(m);

    // when
    Resource r = rf.create("/p", h);

    // then
    assertThat(r.getClass()).isEqualTo(Resource.class);
    assertThat(r.activityName).isEqualTo("activity");
    assertThat(r.path).isEqualTo("/p");
  }
}
