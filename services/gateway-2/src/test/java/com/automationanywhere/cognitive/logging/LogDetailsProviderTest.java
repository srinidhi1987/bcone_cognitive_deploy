package com.automationanywhere.cognitive.logging;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.net.UnknownHostException;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogDetailsProviderTest {

  private LogDetailsProvider p;

  @BeforeMethod
  public void setUp() throws Exception {
    p = Mockito.spy(new LogDetailsProvider(mock(ApplicationConfiguration.class)));
  }

  @Test
  public void testGetHostNameError() throws Exception {
    UnknownHostException e = new UnknownHostException();

    doThrow(e).when(p).getLocalHost();

    assertThat(p.getHostName()).isEqualTo("<unknown>");
  }

  @Test
  public void testExistingValue() {
    Map<String, String> ctx = new HashMap<>();
    ctx.put(LogContextKeys.APPLICATION_NAME.key, "b");

    p.addTrackingInfo(null, ctx, LogContextKeys.APPLICATION_NAME, null, () -> "a");

    assertThat(ctx).containsExactly(new SimpleEntry<>(LogContextKeys.APPLICATION_NAME.key, "a"));
  }

  @Test
  public void testRequestValue() {
    Map<String, String> ctx = new HashMap<>();

    HttpServletRequest req = mock(HttpServletRequest.class);

    doReturn("b").when(req).getHeader("x-forwarded-for");

    p.addTrackingInfo(req, ctx, LogContextKeys.USER_ID, CognitivePlatformHeaders.CLIENT_IP, null);

    HashMap<String, String> m = new HashMap<>();
    m.put("uid", "b");
    assertThat(ctx).isEqualTo(m);
  }

  @Test
  public void testGetRequestPath() {
    // given
    HttpServletRequest req1 = mock(HttpServletRequest.class);
    doReturn("").when(req1).getContextPath();
    doReturn("").when(req1).getServletPath();
    doReturn("/").when(req1).getPathInfo();

    HttpServletRequest req2 = mock(HttpServletRequest.class);
    doReturn("/").when(req2).getContextPath();
    doReturn("").when(req2).getServletPath();
    doReturn("/").when(req2).getPathInfo();

    HttpServletRequest req3 = mock(HttpServletRequest.class);
    doReturn("/").when(req3).getContextPath();
    doReturn("/").when(req3).getServletPath();
    doReturn("/").when(req3).getPathInfo();

    HttpServletRequest req4 = mock(HttpServletRequest.class);
    doReturn("/").when(req4).getContextPath();
    doReturn("a").when(req4).getServletPath();
    doReturn("/").when(req4).getPathInfo();

    HttpServletRequest req5 = mock(HttpServletRequest.class);
    doReturn("/").when(req5).getContextPath();
    doReturn("a/").when(req5).getServletPath();
    doReturn("b").when(req5).getPathInfo();

    HttpServletRequest req6 = mock(HttpServletRequest.class);
    doReturn("/").when(req6).getContextPath();
    doReturn("a").when(req6).getServletPath();
    doReturn("b").when(req6).getPathInfo();

    // when
    String pth1 = p.getRequestPath(req1);
    String pth2 = p.getRequestPath(req2);
    String pth3 = p.getRequestPath(req3);
    String pth4 = p.getRequestPath(req4);
    String pth5 = p.getRequestPath(req5);
    String pth6 = p.getRequestPath(req6);

    // then
    assertThat(pth1).isEqualTo("/");
    assertThat(pth2).isEqualTo("/");
    assertThat(pth3).isEqualTo("/");
    assertThat(pth4).isEqualTo("/a/");
    assertThat(pth5).isEqualTo("/a/b");
    assertThat(pth6).isEqualTo("/a/b");
  }
}
