package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.StagingFilesSummary;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorProperties;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorReviewDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotCount;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotRunDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.id.string.StringId;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.Test;

public class VisionBotDataMapperTest {
  private final VisionBotDataMapper mapper = new VisionBotDataMapper();

  @Test
  public void mapVisionBotShouldMapDataToModel() {
    // given
    VisionBotData d1 = new VisionBotData(
        new StringId("1"), "d", "v"
    );

    VisionBot m1 = new VisionBot(
        new StringId("1"), "d", "v"
    );

    // when
    VisionBot r1 = mapper.mapVisionBot(d1);
    VisionBot r2 = mapper.mapVisionBot((VisionBotData) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotStateShouldMapModelToData() {
    // given
    VisionBotStateData d1 = new VisionBotStateData("data");

    VisionBotState m1 = new VisionBotState("data");

    // when
    VisionBotStateData r1 = mapper.mapVisionBotState(m1);
    VisionBotStateData r2 = mapper.mapVisionBotState(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotElementsShouldMapModelToData() {
    // given
    VisionBotStateAndIdsData d1 = new VisionBotStateAndIdsData("data", new ArrayList<>());

    VisionBotElements m1 = new VisionBotElements("data", new ArrayList<>());

    // when
    VisionBotStateAndIdsData r1 = mapper.mapVisionBotElements(m1);
    VisionBotStateAndIdsData r2 = mapper.mapVisionBotElements(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotResponseShouldMapModelToData() {
    // given
    VisionBotResponse d1 = new VisionBotResponse("data");

    VisionBotResponseData m1 = new VisionBotResponseData("data");

    // when
    VisionBotResponse r1 = mapper.mapVisionBotResponse(m1);
    VisionBotResponse r2 = mapper.mapVisionBotResponse(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotShouldMapModelToData() {
    // given
    VisionBotData d1 = new VisionBotData(
        new StringId("1"), "d", "v"
    );

    VisionBot m1 = new VisionBot(
        new StringId("1"), "d", "v"
    );

    // when
    VisionBotData r1 = mapper.mapVisionBot(m1);
    VisionBotData r2 = mapper.mapVisionBot((VisionBot) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapValidationDetailsShouldMapDataToModel() {
    // given
    ValidationDetailsData d1 = new ValidationDetailsData(
        Collections.singletonList(new ValidatorDetailsData(
            new StringId("1"),
            new ValidatorPropertiesData("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );
    ValidationDetails m1 = new ValidationDetails(
        Collections.singletonList(new ValidatorDetails(
            new StringId("1"),
            new ValidatorProperties("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );

    // when
    ValidationDetails r1 = mapper.mapValidationDetails(d1);
    ValidationDetails r2 = mapper.mapValidationDetails((ValidationDetailsData) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapValidationDetailsShouldMapModelToData() {
    // given
    ValidationDetailsData d1 = new ValidationDetailsData(
        Collections.singletonList(new ValidatorDetailsData(
            new StringId("1"),
            new ValidatorPropertiesData("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );
    ValidationDetails m1 = new ValidationDetails(
        Collections.singletonList(new ValidatorDetails(
            new StringId("1"),
            new ValidatorProperties("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );

    // when
    ValidationDetailsData r1 = mapper.mapValidationDetails(m1);
    ValidationDetailsData r2 = mapper.mapValidationDetails((ValidationDetails) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFieldValueDefinitionShouldMapModelToData() {
    // given
    FieldValueDefinitionData d1 = new FieldValueDefinitionData(
        new StringId("a"), new StringId("b"), "l", "w", "e",
        "exp", "dv", true, "b", "vb",
        1, 2, 3.0, 4, 5, true, 6, "idc"
    );
    FieldValueDefinition m1 = new FieldValueDefinition(
        new StringId("a"), new StringId("b"), "l", "w", "e",
        "exp", "dv", true, "b", "vb",
        1, 2, 3.0, 4, 5, true, 6, "idc"
    );

    // when
    FieldValueDefinitionData r1 = mapper.mapFieldValueDefinition(m1);
    FieldValueDefinitionData r2 = mapper.mapFieldValueDefinition(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotDetailsShouldMapDtoToModel() {
    // given
    VisionBotDetailsData d1 = new VisionBotDetailsData(
        new StringId("1"), "nm",
        new StringId("2"), new StringId("2"), "pn", new StringId("3"), "cn",
        "e", "s", true, new StringId("3"),
        new VisionBotRunDetailsData(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            new ValidatorReviewDetailsData(
                "tfr", "tfmi", "tff", "art"
            )
        ),
        new VisionBotRunDetailsData(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            null
        ),
        null
    );

    VisionBotDetails m1 = new VisionBotDetails(
        new StringId("1"), "nm",
        new StringId("2"), new StringId("2"), "pn", new StringId("3"), "cn",
        "e", "s", true, new StringId("3"),
        new VisionBotRunDetails(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            new ValidatorReviewDetails(
                "tfr", "tfmi", "tff", "art"
            )
        ),
        new VisionBotRunDetails(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            null
        ),
        null
    );

    // when
    VisionBotDetails r1 = mapper.mapVisionBotDetails(d1);
    VisionBotDetails r2 = mapper.mapVisionBotDetails(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapTestSetShouldMapDataToModel() {
    // given
    TestSetData d1 = new TestSetData(new StringId("1"), "nm", "i");
    TestSet m1 = new TestSet(new StringId("1"), "nm", "i");

    // when
    TestSet r1 = mapper.mapTestSet(d1);
    TestSet r2 = mapper.mapTestSet((TestSetData) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapTestSetShouldMapModelToData() {
    // given
    TestSetData d1 = new TestSetData(new StringId("1"), "nm", "i");
    TestSet m1 = new TestSet(new StringId("1"), "nm", "i");

    // when
    TestSetData r1 = mapper.mapTestSet(m1);
    TestSetData r2 = mapper.mapTestSet((TestSet) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapSummaryDataToModel() {
    // given
    SummaryData d1 = new SummaryData(
        new StagingFilesSummaryData(
            1, 2, 3, 4, 5
        ), new VisionBotCountData(
            1, 2, 3
        )
    );
    SummaryData d2 = new SummaryData(null, null);

    Summary m1 = new Summary(
        new StagingFilesSummary(
            1, 2, 3, 4, 5
        ), new VisionBotCount(
            1, 2, 3
        )
    );
    Summary m2 = new Summary(null, null);

    // when
    Summary r1 = mapper.mapSummary(d1);
    Summary r2 = mapper.mapSummary(d2);
    Summary r3 = mapper.mapSummary(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapProcessedDocumentDetailsShouldMapDataToModel() {
    // given
    ProcessedDocumentDetailsData d1 = new ProcessedDocumentDetailsData(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );
    ProcessedDocumentDetails m1 = new ProcessedDocumentDetails(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );

    // when
    ProcessedDocumentDetails r1 = mapper.mapProcessedDocumentDetails(d1);
    ProcessedDocumentDetails r2 = mapper.mapProcessedDocumentDetails((ProcessedDocumentDetailsData) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProcessedDocumentDetailsShouldMapModelToData() {
    // given
    ProcessedDocumentDetailsData d1 = new ProcessedDocumentDetailsData(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );
    ProcessedDocumentDetails m1 = new ProcessedDocumentDetails(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );

    // when
    ProcessedDocumentDetailsData r1 = mapper.mapProcessedDocumentDetails(m1);
    ProcessedDocumentDetailsData r2 = mapper.mapProcessedDocumentDetails((ProcessedDocumentDetails) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapJsonPatchDocumentAdd() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionAdd("/abc", v1))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionAddData("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentReplace() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionReplace("/abc", v1))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionReplaceData("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentRemove() {
    // GIVEN
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionRemove("/abc"))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionRemoveData("/abc"));
  }

  @Test
  public void mapJsonPatchDocumentTest() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionTest("/abc", v1))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionTestData("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentCopy() {
    // GIVEN
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionCopy("/abc", "123"))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionCopyData("/abc", "123"));
  }

  @Test
  public void mapJsonPatchDocumentMove() {
    // GIVEN
    JsonPatchDocument document = new JsonPatchDocument(Collections.singletonList(
        new JsonPatchActionMove("/abc", "123"))
    );

    // WHEN
    List<JsonPatchActionData> actionsData = mapper.mapJsonPatchDocument(document);

    // THEN
    assertThat(actionsData).isNotNull();
    assertThat(actionsData).hasSize(1);
    assertThat(actionsData.get(0)).isEqualTo(new JsonPatchActionMoveData("/abc", "123"));
  }
}
