package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.CharArrayWriter;
import java.util.Date;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.testng.annotations.Test;

public class AccessLogElementWrapperTest {

  @Test
  public void testAddElement() {
    CharArrayWriter b = new CharArrayWriter();
    Date d = new Date();
    CustomAccessLogValve.CustomAccessLogElement ale = mock(
        CustomAccessLogValve.CustomAccessLogElement.class);

    Request request = mock(Request.class);
    Response resp = mock(Response.class);
    CustomAccessLogValve v = new CustomAccessLogValve();
    CustomAccessLogValve.AccessLogElementWrapper w = v.new AccessLogElementWrapper(ale);

    w.addElement(b, d, request, resp, -3);

    verify(ale).addElement(b, d, request, resp, -3);
  }
}
