package com.automationanywhere.cognitive.restclient;


import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.APPLICATION_NAME;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.APPLICATION_VERSION;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.CID;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.CLIENT_IP;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.CLIENT_KEY;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.CLIENT_TOKEN;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.HOST_NAME;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.SESSION_ID;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.TENANT_ID;
import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.USERNAME;
import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class CognitivePlatformHeadersTest {

  @Test
  public void testValues() {
    assertThat(CognitivePlatformHeaders.values()).containsExactlyInAnyOrder(
        APPLICATION_NAME,
        APPLICATION_VERSION,
        CID,
        CLIENT_IP,
        CLIENT_KEY,
        CLIENT_TOKEN,
        HOST_NAME,
        SESSION_ID,
        TENANT_ID,
        USERNAME
    );
  }

  @Test
  public void testValueOf() {
    assertThat(CognitivePlatformHeaders.valueOf(CID.name())).isEqualTo(CID);
    assertThat(CognitivePlatformHeaders.valueOf(HOST_NAME.name())).isEqualTo(HOST_NAME);
    assertThat(CognitivePlatformHeaders.valueOf(CLIENT_IP.name())).isEqualTo(CLIENT_IP);
    assertThat(CognitivePlatformHeaders.valueOf(CLIENT_KEY.name())).isEqualTo(CLIENT_KEY);
    assertThat(CognitivePlatformHeaders.valueOf(CLIENT_TOKEN.name())).isEqualTo(CLIENT_TOKEN);
    assertThat(CognitivePlatformHeaders.valueOf(APPLICATION_NAME.name()))
        .isEqualTo(APPLICATION_NAME);
    assertThat(CognitivePlatformHeaders.valueOf(APPLICATION_VERSION.name()))
        .isEqualTo(APPLICATION_VERSION);
    assertThat(CognitivePlatformHeaders.valueOf(SESSION_ID.name())).isEqualTo(SESSION_ID);
    assertThat(CognitivePlatformHeaders.valueOf(TENANT_ID.name())).isEqualTo(TENANT_ID);
    assertThat(CognitivePlatformHeaders.valueOf(USERNAME.name())).isEqualTo(USERNAME);
  }
}
