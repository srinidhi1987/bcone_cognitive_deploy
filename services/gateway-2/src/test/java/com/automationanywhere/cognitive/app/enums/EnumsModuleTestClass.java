package com.automationanywhere.cognitive.app.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EnumsModuleTestClass {

  public final EnumsModuleTestType type;

  public EnumsModuleTestClass(
      @JsonProperty("type") final EnumsModuleTestType type
  ) {
    this.type = type;
  }
}