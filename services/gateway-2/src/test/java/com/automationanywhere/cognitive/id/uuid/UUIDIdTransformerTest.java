package com.automationanywhere.cognitive.id.uuid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringId;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.UUID;
import org.testng.annotations.Test;

public class UUIDIdTransformerTest {

  @Test
  public void testTransformations() {
    UUIDIdTransformer t = new UUIDIdTransformer();
    UUID uuid = UUID.randomUUID();

    assertThat(t.fromNode(NullNode.getInstance())).isNull();
    assertThat(t.fromNode(null)).isNull();
    assertThat(t.fromString(null)).isNull();
    assertThat(t.toString(null)).isNull();
    assertThat(t.toNode(null)).isSameAs(NullNode.getInstance());

    assertThat(t.fromString(uuid.toString())).isEqualTo(new UUIDId(uuid));
    assertThat(t.fromNode(new TextNode(uuid.toString()))).isEqualTo(new UUIDId(uuid));

    assertThat(t.toNode(new UUIDId(uuid))).isEqualTo(new TextNode(uuid.toString()));
    assertThat(t.toString(new UUIDId(uuid))).isEqualTo(uuid.toString());

    try {
      t.fromString("1");
      fail();
    } catch (final UnsupportedOperationException ex) {
      assertThat(ex.getMessage()).isEqualTo("1 is not supported");
    }

    try {
      t.fromNode(new IntNode(1));
      fail();
    } catch (final UnsupportedOperationException ex) {
      assertThat(ex.getMessage()).isEqualTo(
          "class com.fasterxml.jackson.databind.node.IntNode is not supported"
      );
    }

    try {
      t.fromNode(new TextNode("2"));
      fail();
    } catch (final UnsupportedOperationException ex) {
      assertThat(ex.getMessage()).isEqualTo("2 is not supported");
    }
  }

  @Test
  public void testSupports() {
    UUIDIdTransformer t = new UUIDIdTransformer();
    assertThat(t.supports(Id.class)).isFalse();
    assertThat(t.supports(StringId.class)).isFalse();
    assertThat(t.supports(UUIDId.class)).isTrue();

    try {
      t.supports(null);
      fail();
    } catch (final NullPointerException ex) {
      //As expected
    }
  }
}
