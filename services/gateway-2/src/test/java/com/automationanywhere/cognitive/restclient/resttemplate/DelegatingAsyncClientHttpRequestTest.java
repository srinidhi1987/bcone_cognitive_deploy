package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.AsyncClientHttpRequest;
import org.testng.annotations.Test;

public class DelegatingAsyncClientHttpRequestTest {

  @Test
  public void testDelegation() throws IOException {
    AsyncClientHttpRequest d = mock(AsyncClientHttpRequest.class);

    DelegatingAsyncClientHttpRequest r = new DelegatingAsyncClientHttpRequest(d);

    r.executeAsync();
    verify(d).executeAsync();

    OutputStream b = mock(OutputStream.class);
    doReturn(b).when(d).getBody();
    assertThat(r.getBody()).isSameAs(b);

    HttpHeaders h = new HttpHeaders();
    doReturn(h).when(d).getHeaders();
    assertThat(r.getHeaders()).isSameAs(h);

    HttpMethod m = HttpMethod.PATCH;
    doReturn(m).when(d).getMethod();
    assertThat(r.getMethod()).isSameAs(m);

    URI u = URI.create("http://www.example.com");
    doReturn(u).when(d).getURI();
    assertThat(r.getURI()).isSameAs(u);
  }
}
