package com.automationanywhere.cognitive.gateway.visionbot.models;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class JsonPatchActionRemoveTest {

  @Test
  public void testJsonPatchActionRemoveEquals() {
    // GIVEN
    JsonPatchActionRemove data1 = new JsonPatchActionRemove("/abc");
    JsonPatchActionRemove data2 = new JsonPatchActionRemove("/abc");
    JsonPatchActionRemove data3 = new JsonPatchActionRemove("/def");

    // WHEN
    boolean equals = data1.equals(data2);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(data3);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(null);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(data1);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(new Object());

    // THEN
    assertThat(equals).isFalse();
  }

  @Test
  public void testJsonPatchActionRemoveHashCode() {
    // GIVEN
    JsonPatchActionRemove data1 = new JsonPatchActionRemove("/abc");
    JsonPatchActionRemove data2 = new JsonPatchActionRemove("/abc");
    JsonPatchActionRemove data3 = new JsonPatchActionRemove("/def");

    // WHEN
    int data1Hash = data1.hashCode();
    int data2Hash = data2.hashCode();
    int data3Hash = data3.hashCode();

    // THEN
    assertThat(data1Hash).isEqualTo(data2Hash);
    assertThat(data1Hash).isNotEqualTo(data3Hash);
  }
}
