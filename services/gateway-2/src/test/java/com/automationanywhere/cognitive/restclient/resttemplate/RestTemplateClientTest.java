package com.automationanywhere.cognitive.restclient.resttemplate;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.resttemplate.RestTemplateClient.CallBack;
import java.net.SocketTimeoutException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class RestTemplateClientTest {
  private RestTemplateClient cl;
  private ApplicationConfiguration cfg;
  private AsyncRestTemplate rt;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    rt = spy(AsyncRestTemplate.class);
    cl = new RestTemplateClient(rt, new ExceptionHandlerFactory(), cfg);
  }

  @Test
  public void testSend() throws Exception {
    RequestDetails rd = RequestDetails.Builder.get("http://example.com").build();
    ParameterizedTypeReference<String> type = new ParameterizedTypeReference<String>() {
    };
    cl.send(rd, type);

    MultiValueMap<String, String> h = new HttpHeaders();
    verify(rt).exchange(rd.url, rd.method, new HttpEntity<>(rd.body, h), type);
  }

  @Test
  public void onFailureShouldTryOneMoreTime() throws Exception {
    // given
    doReturn(1L).when(cfg).restTemplateMaxRetryCount();
    doReturn(1L).when(cfg).restTemplateRetryDelay();
    CompletableFuture<Object> cf = mock(CompletableFuture.class);
    Supplier<ListenableFuture<ResponseEntity<Object>>> s = mock(Supplier.class);
    ListenableFuture<ResponseEntity<Object>> lf = mock(ListenableFuture.class);
    Throwable th = new SocketTimeoutException();
    CallBack<Object> cb = cl.new CallBack<>(0, cf, s);
    doReturn(lf).when(s).get();

    // when
    cb.onFailure(th);

    // then
    verify(lf).addCallback(Mockito.any());
  }

  @Test
  public void onFailureShouldCompleteExceptionallyIfFailsToRetry() throws Exception {
    // given
    doReturn(1L).when(cfg).restTemplateMaxRetryCount();
    doReturn(1L).when(cfg).restTemplateRetryDelay();
    CompletableFuture<Object> cf = mock(CompletableFuture.class);
    Supplier<ListenableFuture<ResponseEntity<Object>>> s = mock(Supplier.class);
    ListenableFuture<ResponseEntity<Object>> lf = mock(ListenableFuture.class);
    Throwable th = new SocketTimeoutException();
    RuntimeException ex = new RuntimeException();
    CallBack<Object> cb = cl.new CallBack<>(0, cf, s);
    doThrow(ex).when(s).get();

    // when
    cb.onFailure(th);

    // then
    verify(cf).completeExceptionally(ex);
  }

  @Test
  public void onFailureShouldCompleteExceptionallyOnUnknownExceptions() throws Exception {
    // given
    doReturn(1L).when(cfg).restTemplateMaxRetryCount();
    doReturn(1L).when(cfg).restTemplateRetryDelay();
    CompletableFuture<Object> cf = mock(CompletableFuture.class);
    Supplier<ListenableFuture<ResponseEntity<Object>>> s = mock(Supplier.class);
    Throwable th = new Throwable();
    CallBack<Object> cb = cl.new CallBack<>(0, cf, s);

    // when
    cb.onFailure(th);

    // then
    verify(cf).completeExceptionally(th);
  }

  @Test
  public void onFailureShouldCompleteExceptionallyIfNoMoreRetriesLeft() throws Exception {
    // given
    doReturn(1L).when(cfg).restTemplateMaxRetryCount();
    doReturn(1L).when(cfg).restTemplateRetryDelay();
    CompletableFuture<Object> cf = mock(CompletableFuture.class);
    Supplier<ListenableFuture<ResponseEntity<Object>>> s = mock(Supplier.class);
    Throwable th = new Throwable();
    CallBack<Object> cb = cl.new CallBack<>(1, cf, s);

    // when
    cb.onFailure(th);

    // then
    verify(cf).completeExceptionally(th);
  }

  @Test
  public void onSuccessShouldCompleteNormally() throws Exception {
    // given
    CompletableFuture<Object> cf = mock(CompletableFuture.class);
    CallBack<Object> cb = cl.new CallBack<>(1, cf, null);
    Object o = new Object();
    ResponseEntity<Object> rs = new ResponseEntity<>(o, HttpStatus.OK);

    // when
    cb.onSuccess(rs);

    // then
    verify(cf).complete(o);
  }
}
