package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.auth.microservices.auth.AuthenticationData;
import com.automationanywhere.cognitive.gateway.auth.models.Credentials;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@Test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class AuthenticationTimeoutTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private TestRestTemplate rtpl;

  @MockBean
  @Autowired
  private RestClient restClient;

  @Autowired
  private ApplicationConfiguration cfg;

  @Test
  public void testTimeout() throws Exception {
    CompletableFuture f = CompletableFuture.runAsync(() -> {
      throw new RuntimeException();
    });

    doReturn(f).when(restClient).send(
            RequestDetails.Builder.post(cfg.authURL())
                    .request(cfg.authenticate())
                    .content(new Credentials("u", "p"), RequestDetails.Types.JSON)
                    .accept(RequestDetails.Types.JSON)
                    .build(),
            new ParameterizedTypeReference<AuthenticationData>() {
            }
    );

    CredentialsDto c = new CredentialsDto("u", "p");

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));

    assertThat(rtpl.exchange(
        "/authentication", HttpMethod.POST, new HttpEntity<>(c, headers),
        String.class
    ).getStatusCode().value()).isEqualTo(500);
  }
}
