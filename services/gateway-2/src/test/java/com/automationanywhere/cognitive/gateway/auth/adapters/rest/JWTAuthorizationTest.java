package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.auth.models.JWTAuthorization;
import org.testng.annotations.Test;

public class JWTAuthorizationTest {
  @Test
  public void testEquals() {
    JWTAuthorization kta1 = new JWTAuthorization("ck", "ct");
    JWTAuthorization kta2 = new JWTAuthorization("ck", "ct");
    JWTAuthorization kta3 = new JWTAuthorization("cka", "ct");
    JWTAuthorization kta4 = new JWTAuthorization("ck", "cta");

    assertThat(kta1.equals(kta1)).isTrue();
    assertThat(kta1.equals(kta2)).isTrue();
    assertThat(kta1.equals(kta3)).isFalse();
    assertThat(kta1.equals(kta4)).isFalse();
    assertThat(kta1.equals(null)).isFalse();
    assertThat(kta1.equals(new Object())).isFalse();
  }
}
