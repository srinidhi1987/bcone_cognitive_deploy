package com.automationanywhere.cognitive.auth;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.path.PathManager;
import java.lang.reflect.Method;
import org.springframework.web.bind.annotation.RequestMapping;
import org.testng.annotations.Test;

public class PathManagerTest {
  private final PathManager as = new PathManager();

  @Test
  public void testGetAction() {
    assertThat(as.create("/", "/")).isEqualTo("/");
    assertThat(as.create("//", "//")).isEqualTo("/");
    assertThat(as.create("/", "")).isEqualTo("/");
    assertThat(as.create("//", "")).isEqualTo("/");
    assertThat(as.create("", "/")).isEqualTo("/");
    assertThat(as.create("", "//")).isEqualTo("/");
    assertThat(as.create("", "")).isEqualTo("/");
    assertThat(as.create("a", "c")).isEqualTo("/a/c");
    assertThat(as.create("/a", "/c")).isEqualTo("/a/c");
    assertThat(as.create("/a/", "/c/")).isEqualTo("/a/c/");
    assertThat(as.create("a/", "c/")).isEqualTo("/a/c/");
    assertThat(as.create("a/b", "c/d")).isEqualTo("/a/b/c/d");
    assertThat(as.create("/a/b", "/c/d")).isEqualTo("/a/b/c/d");
    assertThat(as.create("/a//{b/", "/c//d}/")).isEqualTo("/a/{b/c/d}/");
    assertThat(as.create("/a/{xx}/b/", "{yy}//d/{zz}")).isEqualTo("/a/{xx}/b/{yy}/d/{zz}");
    assertThat(as.create("{a", "")).isEqualTo("/{a");
    assertThat(as.create("", "a}")).isEqualTo("/a}");
  }

  @Test
  public void testChangeParams() {
    assertThat(as.create("/")).isEqualTo("/");
    assertThat(as.create("/a/{xx}/b/{yy}/d/{zz}")).isEqualTo("/a/:xx/b/:yy/d/:zz");
    assertThat(as.create("/a/{xx/b/yy}/d/{zz}")).isEqualTo("/a/{xx/b/yy}/d/:zz");
  }

  @Test
  public void testGetParamNames() {
    assertThat(as.getParamNames(null)).isEmpty();
    assertThat(as.getParamNames("")).isEmpty();
    assertThat(as.getParamNames("/a")).isEmpty();
    assertThat(as.getParamNames("/:a/b/:c")).containsExactlyInAnyOrder("a", "c");
  }

  @Test
  public void testGetPath() throws Exception {
    Throwable th = catchThrowable(() -> as.create((Method) null));
    assertThat(th).isInstanceOf(NullPointerException.class);

    assertThat(as.create(AttrTest1.class.getMethod("m"))).isEqualTo("/");

    assertThat(as.create(AttrTest2.class.getMethod("m"))).isEqualTo("/p0/p2/p3");

    assertThat(as.create(AttrTest3.class.getMethod("m"))).isEqualTo("/");

    th = catchThrowable(() -> as.create(BadAttrTest.class.getMethod("m")));
    assertThat(th).hasMessage("Multiple paths are not supported");
  }
}

@RequestMapping
class AttrTest1 {
  @RequestMapping()
  public void m() {}
}

@RequestMapping(path = "p0")
class AttrTest2 {
  @RequestMapping(path = "p2/p3")
  public void m() {}
}

@RequestMapping(path = "")
class AttrTest3 {
  @RequestMapping(path = "")
  public void m() {}
}

@RequestMapping({"/a", "b"})
class BadAttrTest {
  @RequestMapping(path = "")
  public void m() {}
}
