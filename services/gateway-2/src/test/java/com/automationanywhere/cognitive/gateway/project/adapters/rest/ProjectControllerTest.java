package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.project.ProjectService;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ArchiveDetailsData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.NewProjectDetailsData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectDataMapper;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectDetailChangesData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectDetailsData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectExportRequestData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectImportRequestData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectMicroservice;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectPatchDetailData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectPatchResponseData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.TaskData;
import com.automationanywhere.cognitive.gateway.project.microservices.project.TaskStatusData;
import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.core.ParameterizedTypeReference;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ProjectControllerTest {

  private ApplicationConfiguration cfg;
  private ProjectDataMapper datam;
  private ProjectDtoMapper dtom;
  private ProjectController c;
  private RestClient rc;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    rc = mock(RestClient.class);
    datam = spy(new ProjectDataMapper());
    dtom = spy(new ProjectDtoMapper());
    c = new ProjectController(
        new ProjectService(new ProjectMicroservice(datam, rc, cfg)), dtom
    );
  }

  @Test
  public void testGetProjects() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjects();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("")
        .accept(Types.JSON)
        .build();

    ProjectDetailsData projectDetailsData = mock(ProjectDetailsData.class);
    ProjectDetailsDto projectDetailsDto = mock(ProjectDetailsDto.class);
    ProjectDetails projectDetails = mock(ProjectDetails.class);

    CompletableFuture<List<ProjectDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(projectDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    );

    doReturn(projectDetails).when(datam).mapProjectDetails(projectDetailsData);
    doReturn(projectDetailsDto).when(dtom).mapProjectDetails(projectDetails);

    // when

    CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> f
        = c.getOrganizationProjects(new StringId("1"), null);

    StandardResponse<List<ProjectDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(projectDetailsDto);
  }

  @Test
  public void testGetProjectMetadataList() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).getOrganizationProjectMetadataList();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    ProjectDetailsData projectDetailsData = mock(ProjectDetailsData.class);
    ProjectDetailsDto projectDetailsDto = mock(ProjectDetailsDto.class);
    ProjectDetails projectDetails = mock(ProjectDetails.class);

    CompletableFuture<List<ProjectDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(projectDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    );

    doReturn(projectDetails).when(datam).mapProjectDetails(projectDetailsData);
    doReturn(projectDetailsDto).when(dtom).mapProjectDetails(projectDetails);

    // when

    CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> f
        = c.getProjectMetadataList(new StringId("1"));

    StandardResponse<List<ProjectDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(projectDetailsDto);
  }

  @Test
  public void testGetNullProjectList() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjects();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<ProjectDetailsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> f
        = c.getOrganizationProjects(new StringId("1"), null);

    StandardResponse<List<ProjectDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.emptyList());
  }

  @Test
  public void testGetNullProjectMetadataList() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).getOrganizationProjectMetadataList();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<ProjectDetailsData>> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> f
        = c.getProjectMetadataList(new StringId("1"));

    StandardResponse<List<ProjectDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testGetProjectsWithExcludeFields() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjects();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("a,b")
        .accept(Types.JSON)
        .build();

    ProjectDetailsData projectDetailsData = mock(ProjectDetailsData.class);
    ProjectDetailsDto projectDetailsDto = mock(ProjectDetailsDto.class);
    ProjectDetails projectDetails = mock(ProjectDetails.class);

    CompletableFuture<List<ProjectDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(projectDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    );

    doReturn(projectDetails).when(datam).mapProjectDetails(projectDetailsData);
    doReturn(projectDetailsDto).when(dtom).mapProjectDetails(projectDetails);

    // when

    CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> f
        = c.getOrganizationProjects(new StringId("1"), "a,,b");

    StandardResponse<List<ProjectDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(projectDetailsDto);
  }

  @Test
  public void testGetProject() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}?e={2}").when(cfg).getOrganizationProject();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}?e={2}")
        .param("1")
        .param("2")
        .param(null)
        .accept(Types.JSON)
        .build();

    ProjectDetailsData projectDetailsData = mock(ProjectDetailsData.class);
    ProjectDetailsDto projectDetailsDto = mock(ProjectDetailsDto.class);
    ProjectDetails projectDetails = mock(ProjectDetails.class);

    CompletableFuture<ProjectDetailsData> future =
        CompletableFuture.completedFuture(projectDetailsData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ProjectDetailsData>() {
        }
    );

    doReturn(projectDetails).when(datam).mapProjectDetails(projectDetailsData);
    doReturn(projectDetailsDto).when(dtom).mapProjectDetails(projectDetails);

    // when

    CompletableFuture<StandardResponse<ProjectDetailsDto>> f
        = c.getOrganizationProject(new StringId("1"), new StringId("2"), null);

    StandardResponse<ProjectDetailsDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(projectDetailsDto);
  }

  @Test
  public void testGetProjectMetadata() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectMetadata();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    ProjectDetailChangesData projectDetailChangesData = mock(ProjectDetailChangesData.class);
    ProjectDetailChangesDto projectDetailChangesDto = mock(ProjectDetailChangesDto.class);
    ProjectDetailChanges projectDetailChanges = mock(ProjectDetailChanges.class);

    CompletableFuture<ProjectDetailChangesData> future =
        CompletableFuture.completedFuture(projectDetailChangesData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ProjectDetailChangesData>() {
        }
    );

    doReturn(projectDetailChanges).when(datam).mapProjectDetailChanges(projectDetailChangesData);
    doReturn(projectDetailChangesDto).when(dtom).mapProjectDetailChanges(projectDetailChanges);

    // when

    CompletableFuture<StandardResponse<ProjectDetailChangesDto>> f
        = c.getProjectMetadata(new StringId("1"), new StringId("2"));

    StandardResponse<ProjectDetailChangesDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(projectDetailChangesDto);
  }

  @Test
  public void testCreateProject() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).createOrganizationProject();

    NewProjectDetailsDto newProjectDetailsDto = mock(NewProjectDetailsDto.class);
    NewProjectDetails newProjectDetails = mock(NewProjectDetails.class);
    NewProjectDetailsData newProjectDetailsData = mock(NewProjectDetailsData.class);

    ProjectDetailChangesData projectDetailChangesData = mock(ProjectDetailChangesData.class);
    ProjectDetailChanges projectDetailChanges = mock(ProjectDetailChanges.class);
    ProjectDetailChangesDto projectDetailChangesDto = mock(ProjectDetailChangesDto.class);

    RequestDetails rd = Builder.post("url")
        .request("prj{0}")
        .param("1")
        .content(newProjectDetailsData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<ProjectDetailChangesData> future =
        CompletableFuture.completedFuture(projectDetailChangesData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ProjectDetailChangesData>() {
        }
    );

    doReturn(newProjectDetails).when(dtom).mapNewProjectDetails(newProjectDetailsDto);
    doReturn(newProjectDetailsData).when(datam).mapNewProjectDetails(newProjectDetails);
    doReturn(projectDetailChanges).when(datam)
        .mapProjectDetailChanges(projectDetailChangesData);
    doReturn(projectDetailChangesDto).when(dtom).mapProjectDetailChanges(projectDetailChanges);

    // when

    CompletableFuture<StandardResponse<ProjectDetailChangesDto>> f
        = c.createOrganizationProject(new StringId("1"), newProjectDetailsDto);

    StandardResponse<ProjectDetailChangesDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(projectDetailChangesDto);
  }

  @Test
  public void testDeleteProject() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).deleteOrganizationProject();

    RequestDetails rd = Builder.delete("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .setHeader("updatedBy", "un")
        .build();

    StandardResponse<Void> sr = new StandardResponse<>(null);

    CompletableFuture<StandardResponse<Void>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Void>> f
        = c.deleteOrganizationProject(new StringId("1"), new StringId("2"), "un");

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r).isEqualTo(sr);
  }

  @Test
  public void testUpdateProject() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).updateOrganizationProject();

    StandardResponse<Void> sr = new StandardResponse<>(null);

    ProjectDetailChangesDto projectDetailChangesDto = mock(ProjectDetailChangesDto.class);
    ProjectDetailChanges projectDetailChanges = mock(ProjectDetailChanges.class);
    ProjectDetailChangesData projectDetailChangesData = mock(ProjectDetailChangesData.class);

    RequestDetails rd = Builder.put("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .content(projectDetailChangesData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<Void> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    doReturn(projectDetailChanges).when(dtom).mapProjectDetailChanges(projectDetailChangesDto);
    doReturn(projectDetailChangesData).when(datam)
        .mapProjectDetailChanges(projectDetailChanges);

    // when

    CompletableFuture<StandardResponse<Void>> f
        = c.updateOrganizationProject(
        new StringId("1"), new StringId("2"), projectDetailChangesDto
    );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r).isEqualTo(sr);
  }

  @Test
  public void testImportProjects() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).importOrganizationProjects();

    ProjectImportRequestDto projectImportRequestDto = mock(ProjectImportRequestDto.class);
    ProjectImportRequest projectImportRequest = mock(ProjectImportRequest.class);
    ProjectImportRequestData projectImportRequestData = mock(ProjectImportRequestData.class);

    TaskStatusData taskStatusData = mock(TaskStatusData.class);
    TaskStatus taskStatus = mock(TaskStatus.class);
    TaskStatusDto taskStatusDto = mock(TaskStatusDto.class);

    RequestDetails rd = Builder.post("url")
        .request("prj{0}")
        .param("1")
        .content(projectImportRequestData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<TaskStatusData> future = CompletableFuture.completedFuture(taskStatusData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<TaskStatusData>() {
        }
    );

    doReturn(projectImportRequest).when(dtom).mapProjectImportRequest(projectImportRequestDto);
    doReturn(projectImportRequestData).when(datam).mapProjectImportRequest(projectImportRequest);
    doReturn(taskStatus).when(datam).mapTaskStatus(taskStatusData);
    doReturn(taskStatusDto).when(dtom).mapTaskStatus(taskStatus);

    // when

    CompletableFuture<StandardResponse<TaskStatusDto>> f = c
        .importOrganizationProject(new StringId("1"), projectImportRequestDto);

    StandardResponse<TaskStatusDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(taskStatusDto);
  }

  @Test
  public void testGetArchiveDetailList() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).getArchiveDetailList();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    ArchiveDetailsData archiveDetailsData = mock(ArchiveDetailsData.class);
    ArchiveDetails archiveDetails = mock(ArchiveDetails.class);
    ArchiveDetailsDto archiveDetailsDto = mock(ArchiveDetailsDto.class);

    CompletableFuture<List<ArchiveDetailsData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(archiveDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ArchiveDetailsData>>() {
        }
    );

    doReturn(archiveDetails).when(datam).mapArchiveDetails(archiveDetailsData);
    doReturn(archiveDetailsDto).when(dtom).mapArchiveDetails(archiveDetails);

    // when

    CompletableFuture<StandardResponse<List<ArchiveDetailsDto>>> f = c
        .getArchiveDetailList(new StringId("1"));

    StandardResponse<List<ArchiveDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo(Collections.singletonList(archiveDetailsDto));
  }

  @Test
  public void testGetEmptyArchiveDetailList() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).getArchiveDetailList();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<ArchiveDetailsData>> future = CompletableFuture
        .completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ArchiveDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<ArchiveDetailsDto>>> f = c
        .getArchiveDetailList(new StringId("1"));

    StandardResponse<List<ArchiveDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo(Collections.emptyList());
  }

  @Test
  public void testPatchOrganizationProject() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}").when(cfg).patchOrganizationProject();

    ProjectPatchDetailData requestData = mock(ProjectPatchDetailData.class);
    ProjectPatchDetail requestModel = mock(ProjectPatchDetail.class);
    ProjectPatchDetailDto requestDto = mock(ProjectPatchDetailDto.class);

    ProjectDetailsDto responseDto = mock(ProjectDetailsDto.class);
    ProjectDetails responseModel = mock(ProjectDetails.class);
    ProjectDetailsData responseData = mock(ProjectDetailsData.class);

    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .setHeader("X-HTTP-Method-Override", "PATCH")
        .content(Collections.singletonList(requestData), Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<ProjectDetailsData>> future = CompletableFuture
        .completedFuture(new StandardResponse<>(responseData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<ProjectDetailsData>>() {
        }
    );

    doReturn(requestModel).when(dtom).mapProjectPatchDetail(requestDto);
    doReturn(requestData).when(datam).mapProjectPatchDetail(requestModel);

    doReturn(responseDto).when(dtom).mapProjectDetails(responseModel);
    doReturn(responseModel).when(datam).mapProjectDetails(responseData);

    // WHEN
    CompletableFuture<StandardResponse<ProjectDetailsDto>> f = c.patchOrganizationProject(
        new StringId("1"), new StringId("2"), Collections.singletonList(requestDto)
    );

    StandardResponse<ProjectDetailsDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(responseDto);
  }

  @Test
  public void testExportProjects() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).exportOrganizationProjects();

    ProjectExportRequestDto projectExportRequestDto = mock(ProjectExportRequestDto.class);
    ProjectExportRequest projectExportRequest = mock(ProjectExportRequest.class);
    ProjectExportRequestData projectExportRequestData = mock(ProjectExportRequestData.class);

    TaskStatusData taskStatusData = mock(TaskStatusData.class);
    TaskStatus taskStatus = mock(TaskStatus.class);
    TaskStatusDto taskStatusDto = mock(TaskStatusDto.class);

    RequestDetails rd = Builder.post("url")
        .request("prj{0}")
        .param("1")
        .content(projectExportRequestData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<TaskStatusData> future = CompletableFuture.completedFuture(taskStatusData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<TaskStatusData>() {
        }
    );

    doReturn(projectExportRequest).when(dtom).mapProjectExportRequest(projectExportRequestDto);
    doReturn(projectExportRequestData).when(datam).mapProjectExportRequest(projectExportRequest);
    doReturn(taskStatus).when(datam).mapTaskStatus(taskStatusData);
    doReturn(taskStatusDto).when(dtom).mapTaskStatus(taskStatus);

    // when

    CompletableFuture<StandardResponse<TaskStatusDto>> f = c
        .exportOrganizationProjects(new StringId("1"), projectExportRequestDto);

    StandardResponse<TaskStatusDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(taskStatusDto);
  }

  @Test
  public void testGetOrganizationProjectTaskStatus() throws Exception {
    // given

    TaskStatusData taskStatusData = mock(TaskStatusData.class);
    TaskStatus taskStatus = mock(TaskStatus.class);
    TaskStatusDto taskStatusDto = mock(TaskStatusDto.class);

    doReturn(taskStatus).when(datam).mapTaskStatus(taskStatusData);
    doReturn(taskStatusDto).when(dtom).mapTaskStatus(taskStatus);

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).getOrganizationProjectTaskStatus();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<TaskStatusData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(taskStatusData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<TaskStatusData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<TaskStatusDto>>> f = c
        .getOrganizationProjectTaskStatus(new StringId("1"));

    StandardResponse<List<TaskStatusDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo(Collections.singletonList(taskStatusDto));
  }

  @Test
  public void testGetNullOrganizationProjectTaskStatus() throws Exception {
    // given

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}").when(cfg).getOrganizationProjectTaskStatus();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<TaskStatusData>> future = CompletableFuture
        .completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<TaskStatusData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<TaskStatusDto>>> f = c
        .getOrganizationProjectTaskStatus(new StringId("1"));

    StandardResponse<List<TaskStatusDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo(Collections.emptyList());
  }

  @Test
  public void testGetOrganizationProjectTasks() throws Exception {
    // given

    TaskData taskData = mock(TaskData.class);
    Task task = mock(Task.class);
    TaskDto taskDto = mock(TaskDto.class);

    doReturn(task).when(datam).mapTask(taskData);
    doReturn(taskDto).when(dtom).mapTask(task);

    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectTasks();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<TaskData>> future = CompletableFuture
        .completedFuture(Collections.singletonList(taskData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<TaskData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<TaskDto>>> f = c
        .getOrganizationProjectTasks(new StringId("1"), "2", "3");

    StandardResponse<List<TaskDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo(Collections.singletonList(taskDto));
  }

  @Test
  public void testGetNullOrganizationProjectTasks() throws Exception {
    // given
    doReturn("url").when(cfg).projectURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectTasks();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<TaskData>> future = CompletableFuture
        .completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<TaskData>>() {
        }
    );

    // when
    CompletableFuture<StandardResponse<List<TaskDto>>> f = c
        .getOrganizationProjectTasks(new StringId("1"), "2", "3");

    StandardResponse<List<TaskDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualTo(Collections.emptyList());
  }
}
