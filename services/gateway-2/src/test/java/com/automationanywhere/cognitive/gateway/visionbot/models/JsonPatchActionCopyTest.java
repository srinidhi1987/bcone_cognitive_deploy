package com.automationanywhere.cognitive.gateway.visionbot.models;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class JsonPatchActionCopyTest {

  @Test
  public void testJsonPatchActionCopyEquals() {
    // GIVEN
    JsonPatchActionCopy data1 = new JsonPatchActionCopy("/abc", "/ghi");
    JsonPatchActionCopy data2 = new JsonPatchActionCopy("/abc", "/ghi");
    JsonPatchActionCopy data3 = new JsonPatchActionCopy("/def", "/ghi");
    JsonPatchActionCopy data4 = new JsonPatchActionCopy("/abc", "/jkl");

    // WHEN
    boolean equals = data1.equals(data2);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(data3);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data2.equals(data4);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(null);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(data1);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(new Object());

    // THEN
    assertThat(equals).isFalse();
  }

  @Test
  public void testJsonPatchActionCopyHashCode() {
    // GIVEN
    JsonPatchActionCopy data1 = new JsonPatchActionCopy("/abc", "/ghi");
    JsonPatchActionCopy data2 = new JsonPatchActionCopy("/abc", "/ghi");
    JsonPatchActionCopy data3 = new JsonPatchActionCopy("/def", "/ghi");
    JsonPatchActionCopy data4 = new JsonPatchActionCopy("/abc", "/jkl");

    // WHEN
    int data1Hash = data1.hashCode();
    int data2Hash = data2.hashCode();
    int data3Hash = data3.hashCode();
    int data4Hash = data4.hashCode();

    // THEN
    assertThat(data1Hash).isEqualTo(data2Hash);
    assertThat(data1Hash).isNotEqualTo(data3Hash);
    assertThat(data2Hash).isNotEqualTo(data4Hash);
  }
}
