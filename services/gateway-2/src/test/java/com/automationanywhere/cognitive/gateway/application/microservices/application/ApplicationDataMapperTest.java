package com.automationanywhere.cognitive.gateway.application.microservices.application;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import org.testng.annotations.Test;

public class ApplicationDataMapperTest {

  private final ApplicationDataMapper mapper = new ApplicationDataMapper();

  @Test
  public void mapAcknowledgementShouldMapModelToData() {
    // given
    AcknowledgementData d1 = new AcknowledgementData(
        "a"
    );

    Acknowledgement m1 = new Acknowledgement(
        "a"
    );

    // when
    AcknowledgementData r1 = mapper.mapAcknowledgement(m1);
    AcknowledgementData r2 = mapper.mapAcknowledgement(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapRegistrationShouldMapModelToData() {
    // given
    RegistrationData d1 = new RegistrationData(
        "a", "b", "c"
    );

    Registration m1 = new Registration(
        "a", "b", "c"
    );

    // when
    RegistrationData r1 = mapper.mapRegistration(m1);
    RegistrationData r2 = mapper.mapRegistration(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapConfigurationShouldMapModelToData() {
    // given
    ConfigurationData d1 = new ConfigurationData(
        "a", "b"
    );

    Configuration m1 = new Configuration(
        "a", "b"
    );

    // when
    Configuration r1 = mapper.mapConfiguration(d1);
    Configuration r2 = mapper.mapConfiguration(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapHandshakeShouldMapModelToData() {
    // given
    HandshakeData d1 = new HandshakeData(
        "a", "b", "c"
    );

    Handshake m1 = new Handshake(
        "a", "b", "c"
    );

    // when
    Handshake r1 = mapper.mapHandshake(d1);
    Handshake r2 = mapper.mapHandshake(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(m1);
    assertThat(r2).isNull();
  }
}
