package com.automationanywhere.cognitive.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.common.configuration.FileConfigurationProvider;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.UUID;
import org.testng.annotations.Test;

public class ApplicationConfigurationTest {

  @Test
  public void testBuildProperties() {
    FileConfigurationProvider provider = new FileConfigurationProvider(
        new YAMLMapper(),
        "application.yaml"
    );
    provider.refresh();

    ApplicationConfiguration ac = new ApplicationConfiguration(
        provider,
        "build.version"
    );
    assertThat(ac.applicationVersion()).isNotBlank();
    assertThat(ac.applicationName()).isEqualTo("gateway-2");

    assertThat(ac.projectURL()).isNotBlank();
    assertThat(ac.getOrganizationProject()).isNotBlank();
    assertThat(ac.getOrganizationProjectMetadata()).isNotBlank();
    assertThat(ac.getOrganizationProjects()).isNotBlank();
    assertThat(ac.getOrganizationProjectMetadataList()).isNotBlank();
    assertThat(ac.createOrganizationProject()).isNotBlank();
    assertThat(ac.updateOrganizationProject()).isNotBlank();
    assertThat(ac.deleteOrganizationProject()).isNotBlank();
    assertThat(ac.getArchiveDetailList()).isNotBlank();
    assertThat(ac.importOrganizationProjects()).isNotBlank();
    assertThat(ac.patchOrganizationProject()).isNotBlank();
    assertThat(ac.exportOrganizationProjects()).isNotBlank();
    assertThat(ac.fileURL()).isNotBlank();
    assertThat(ac.getOrganizationProjectTaskStatus()).isNotBlank();
    assertThat(ac.getOrganizationProjectCategories()).isNotBlank();
    assertThat(ac.getOrganizationProjectCategoryFiles()).isNotBlank();
    assertThat(ac.getOrganizationFileCount()).isNotBlank();
    assertThat(ac.getOrganizationProjectFileCount()).isNotBlank();
    assertThat(ac.getOrganizationProjectFiles()).isNotBlank();
    assertThat(ac.getOrganizationProjectStagingFiles()).isNotBlank();
    assertThat(ac.getOrganizationProjectFile()).isNotBlank();
    assertThat(ac.patchOrganizationProjectFile()).isNotBlank();
    assertThat(ac.uploadOrganizationProjectFiles()).isNotBlank();
    assertThat(ac.uploadOrganizationProjectStagingFiles()).isNotBlank();
    assertThat(ac.deleteOrganizationProjectFile()).isNotBlank();
    assertThat(ac.getOrganizationProjectFileBlobs()).isNotBlank();
    assertThat(ac.getOrganizationProjectReport()).isNotBlank();
    assertThat(ac.getOrganizationProjectFileStatus()).isNotBlank();
    assertThat(ac.getOrganizationProjectClassificationSummary()).isNotBlank();
    assertThat(ac.updateOrganizationProjectClassification()).isNotBlank();
    assertThat(ac.updateOrganizationProjectClassificationById()).isNotBlank();
    assertThat(ac.createSegmentedDocument()).isNotBlank();
    assertThat(ac.getSegmentedDocument()).isNotBlank();
    assertThat(ac.getVisionBot()).isNotBlank();
    assertThat(ac.getVisionBotById()).isNotBlank();
    assertThat(ac.patchVisionBot()).isNotBlank();
    assertThat(ac.getVisionBotMetadata()).isNotBlank();
    assertThat(ac.getProjectVisionBotMetadata()).isNotBlank();
    assertThat(ac.getProjectVisionBot()).isNotBlank();
    assertThat(ac.getOrganizationVisionBot()).isNotBlank();
    assertThat(ac.getOrganizationVisionBotById()).isNotBlank();
    assertThat(ac.getVisionBotForEdit()).isNotBlank();
    assertThat(ac.updateVisionBot()).isNotBlank();
    assertThat(ac.updateVisionBotById()).isNotBlank();
    assertThat(ac.unlock()).isNotBlank();
    assertThat(ac.getValueField()).isNotBlank();
    assertThat(ac.runVisionBot()).isNotBlank();
    assertThat(ac.getValidationData()).isNotBlank();
    assertThat(ac.getVisionBotByLayout()).isNotBlank();
    assertThat(ac.getVisionBotByLayoutAndPage()).isNotBlank();
    assertThat(ac.getPageImage()).isNotBlank();
    assertThat(ac.getSummary()).isNotBlank();
    assertThat(ac.getTestSet()).isNotBlank();
    assertThat(ac.setState()).isNotBlank();
    assertThat(ac.exportToCSV()).isNotBlank();
    assertThat(ac.updateValidationData()).isNotBlank();
    assertThat(ac.updateAutoMappingData()).isNotBlank();
    assertThat(ac.updateValidationDocument()).isNotBlank();
    assertThat(ac.generateVisionBotData()).isNotBlank();
    assertThat(ac.generateVisionBotDataPaginated()).isNotBlank();
    assertThat(ac.keepAliveLockedVisionBot()).isNotBlank();
    assertThat(ac.updateTestSet()).isNotBlank();
    assertThat(ac.deleteTestSet()).isNotBlank();
    assertThat(ac.validatorURL()).isNotBlank();
    assertThat(ac.validatorHealthCheck()).isNotBlank();
    assertThat(ac.getReviewedFileCountDetails()).isNotBlank();
    assertThat(ac.getFailedVisionBotCount()).isNotBlank();
    assertThat(ac.unlockProject()).isNotBlank();
    assertThat(ac.getInvalidFileTypeList()).isNotBlank();
    assertThat(ac.getProductionFileSummary()).isNotBlank();
    assertThat(ac.markFileInvalid()).isNotBlank();
    assertThat(ac.resourceLockerImportResource()).isNotBlank();
    assertThat(ac.resourceLockerExportResource()).isNotBlank();
    assertThat(ac.mlServiceURL()).isNotBlank();
    assertThat(ac.predict()).isNotBlank();
    assertThat(ac.value()).isNotBlank();
    assertThat(ac.aliasServiceURL()).isNotBlank();
    assertThat(ac.getDomainsUri()).isNotBlank();
    assertThat(ac.importDomainsUri()).isNotBlank();
    assertThat(ac.getDomainsLanguagesUri()).isNotBlank();
    assertThat(ac.validatorKeepAlive()).isNotBlank();
    assertThat(ac.getOrganizationProjectTasks()).isNotBlank();
    assertThat(ac.getMutipleState()).isNotBlank();
    assertThat(ac.getFailedVbotData()).isNotBlank();
    assertThat(ac.getAccuracyProcessingDetails()).isNotBlank();
    assertThat(ac.applicationServiceURL()).isNotBlank();
    assertThat(ac.handshake()).isNotBlank();
    assertThat(ac.acknowledge()).isNotBlank();
    assertThat(ac.configuration()).isNotBlank();
  }

  @Test
  public void testFatalError() {
    try {
      new ApplicationConfiguration(null, UUID.randomUUID().toString());
      fail();
    } catch (final Exception e) {
      assertThat(e.getCause()).isInstanceOf(NullPointerException.class);
    }
  }
}
