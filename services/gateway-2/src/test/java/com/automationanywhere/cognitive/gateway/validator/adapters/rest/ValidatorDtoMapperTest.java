package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.validator.microservices.validator.FailedVbotdataData;
import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ClassificationFieldDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotdata;
import com.automationanywhere.cognitive.gateway.validator.models.FieldAccuracyDashboardDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FieldValidation;
import com.automationanywhere.cognitive.gateway.validator.models.GroupTimeSpentDetails;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.id.string.StringId;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.Test;

public class ValidatorDtoMapperTest {
  private final ValidatorDtoMapper mapper = new ValidatorDtoMapper();

  @Test
  public void mapReviewedFileCountDetailsShouldMapModelToDto() {
    // given
    ReviewedFileCountDetailsDto d1 = new ReviewedFileCountDetailsDto(
        new StringId("1"), 1, 2, 3, 4
    );

    ReviewedFileCountDetails m1 = new ReviewedFileCountDetails(
        new StringId("1"), 1, 2, 3, 4
    );

    // when
    ReviewedFileCountDetailsDto r1 = mapper.mapReviewedFileCountDetails(m1);
    ReviewedFileCountDetailsDto r2 = mapper.mapReviewedFileCountDetails(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapSuccessShouldMapModelToDto() {
    // given
    SuccessDto d1 = new SuccessDto(
        true
    );

    Success m1 = new Success(
        true
    );

    // when
    SuccessDto r1 = mapper.mapSuccess(m1);
    SuccessDto r2 = mapper.mapSuccess(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapInvalidFileTypeShouldMapModelToDto() {
    // given
    InvalidFileTypeDto d1 = new InvalidFileTypeDto(
        new StringId("1"), "ok"
    );

    InvalidFileType m1 = new InvalidFileType(
        new StringId("1"), "ok"
    );

    // when
    InvalidFileTypeDto r1 = mapper.mapInvalidFileType(m1);
    InvalidFileTypeDto r2 = mapper.mapInvalidFileType(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProductionFileSummaryShouldMapModelToDto() {
    // given
    ProductionFileSummaryDto d1 = new ProductionFileSummaryDto(
        new StringId("1"), 1, 2,3, 4, 5, 6.0
    );

    ProductionFileSummary m1 = new ProductionFileSummary(
        new StringId("1"), 1, 2,3, 4, 5, 6.0
    );

    // when
    ProductionFileSummaryDto r1 = mapper.mapProductionFileSummary(m1);
    ProductionFileSummaryDto r2 = mapper.mapProductionFileSummary(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapInvalidFileShouldMapDtoToModel() {
    // given
    InvalidFileDto d1 = new InvalidFileDto(
        new StringId("1"), "ok", "tx"
    );

    InvalidFile m1 = new InvalidFile(
        new StringId("1"), "ok", "tx"
    );

    // when
    InvalidFile r1 = mapper.mapInvalidFile(d1);
    InvalidFile r2 = mapper.mapInvalidFile(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFailedVBotdata() {
    // GIVEN
    int fileId = 1;
    int orgId = 1;
    int projectId = 1;
    String vBotDocument = "vBotDocument";
    String correctedData = "correctedData";
    String validationStatus = "validationStatus";
    String createdAt = "createdAt";
    String updatedBy = "updatedBy";
    String updatedAt = "updatedAt";
    String visionBotId = "visionBotId";
    String lockedUserId = "lockedUserId";
    int failedFieldCounts = 1;
    int passFieldCounts = 1;
    int totalFieldCounts = 1;
    String startTime = "startTime";
    String endTime = "endTime";
    int averageTimeInSeconds = 1;
    String lastExportTime = "lastExportTime";

    FailedVbotdata d1 = new FailedVbotdata(
        fileId, orgId, projectId, vBotDocument, correctedData, validationStatus, createdAt,
        updatedBy, updatedAt, visionBotId, lockedUserId, failedFieldCounts, passFieldCounts,
        totalFieldCounts, startTime, endTime, averageTimeInSeconds, lastExportTime
    );
    FailedVbotdataData m1 = new FailedVbotdataData(
        fileId, orgId, projectId, vBotDocument, correctedData, validationStatus, createdAt,
        updatedBy, updatedAt, visionBotId, lockedUserId, failedFieldCounts, passFieldCounts,
        totalFieldCounts, startTime, endTime, averageTimeInSeconds, lastExportTime
    );

    // WHEN
    FailedVbotdataDto r1 = mapper.mapFailedVBotdata(d1);
    FailedVbotdataDto r2 = mapper.mapFailedVBotdata(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByField(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapAccuracyProcessingDetails() {
    // GIVEN
    String id = "id";
    double failedFieldCounts = 1.0;
    double passFieldCounts = 1.0;
    double totalFieldCounts = 1.0;
    double fieldAccuracy = 1.0;
    long reviewFileCount = 1L;
    long invalidFileCount = 1L;
    long failedDocumentCount = 1L;
    long passedDocumentCounts = 1L;
    long processedDocumentCounts = 1L;
    long documentAccuracy = 1L;
    long averageReviewTimeInSeconds = 1L;

    AccuracyProcessingDetails d1 = new AccuracyProcessingDetails(
        id, failedFieldCounts, passFieldCounts, totalFieldCounts, fieldAccuracy, reviewFileCount,
        invalidFileCount, failedDocumentCount, passedDocumentCounts, processedDocumentCounts,
        documentAccuracy, averageReviewTimeInSeconds
    );
    AccuracyProcessingDetailsDto m1 = new AccuracyProcessingDetailsDto(
        id, failedFieldCounts, passFieldCounts, totalFieldCounts, fieldAccuracy, reviewFileCount,
        invalidFileCount, failedDocumentCount, passedDocumentCounts, processedDocumentCounts,
        documentAccuracy, averageReviewTimeInSeconds
    );

    // WHEN
    AccuracyProcessingDetailsDto r1 = mapper.mapAccuracyProcessingDetails(d1);
    AccuracyProcessingDetailsDto r2 = mapper.mapAccuracyProcessingDetails(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByField(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectTotalsAccuracyDetails() {
    // GIVEN
    int totalFilesProcessed = 1;
    int totalSTP = 2;
    int totalAccuracy = 3;
    int totalFilesUploaded = 4;
    int totalFilesToValidation = 5;
    int totalFilesValidated = 6;
    int totalFilesUnprocessable = 7;

    ClassificationFieldDetails[] classification = new ClassificationFieldDetails[]{
        new ClassificationFieldDetails("Classification", 2.2)
    };

    ClassificationFieldDetailsDto[] classificationDto = new ClassificationFieldDetailsDto[]{
        new ClassificationFieldDetailsDto("Classification", 2.2)
    };

    FieldAccuracyDashboardDetails[] accuracy = new FieldAccuracyDashboardDetails[]{
        new FieldAccuracyDashboardDetails("Accuracy", 3.3)
    };
    FieldAccuracyDashboardDetailsDto[] accuracyDto = new FieldAccuracyDashboardDetailsDto[]{
        new FieldAccuracyDashboardDetailsDto("Accuracy", 3.3)
    };

    FieldValidation[] validation = new FieldValidation[]{
        new FieldValidation("Validation", 4.4)
    };
    FieldValidationDto[] validationDto = new FieldValidationDto[]{
        new FieldValidationDto("Validation", 4.4)
    };

    GroupTimeSpentDetails[] categories = new GroupTimeSpentDetails[]{
        new GroupTimeSpentDetails("Categories", 5.5)
    };
    GroupTimeSpentDetailsDto[] categoriesDto = new GroupTimeSpentDetailsDto[]{
        new GroupTimeSpentDetailsDto("Categories", 5.5)
    };

    ProjectTotalsAccuracyDetails d1 = new ProjectTotalsAccuracyDetails(
        totalFilesProcessed, totalSTP, totalAccuracy, totalFilesUploaded, totalFilesToValidation,
        totalFilesValidated, totalFilesUnprocessable, classification, accuracy,
        validation, categories
    );
    ProjectTotalsAccuracyDetailsDto m1 = new ProjectTotalsAccuracyDetailsDto(
        totalFilesProcessed, totalSTP, totalAccuracy, totalFilesUploaded, totalFilesToValidation,
        totalFilesValidated, totalFilesUnprocessable, classificationDto, accuracyDto, validationDto,
        categoriesDto
    );

    // WHEN
    ProjectTotalsAccuracyDetailsDto r1 = mapper.mapPrjTotAccuracyDetails(d1);
    ProjectTotalsAccuracyDetailsDto r2 = mapper.mapPrjTotAccuracyDetails(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFailedVbotStatus() {
    // GIVEN
    FailedVbotStatus d1 = new FailedVbotStatus(true, 1, 0);
    FailedVbotStatusDto m1 = new FailedVbotStatusDto(true, 1, 0);

    // WHEN
    FailedVbotStatusDto r1 = mapper.mapFailedVBotStatus(d1);
    FailedVbotStatusDto r2 = mapper.mapFailedVBotStatus(null);

    // THEN
    assertThat(r1).isEqualToComparingFieldByField(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapJsonPatchDocumentAdd() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.ADD, "/abc", v1, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionAdd("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentReplace() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.REPLACE, "/abc", v1, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionReplace("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentRemove() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.REMOVE, "/abc", null, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionRemove("/abc"));
  }

  @Test
  public void mapJsonPatchDocumentTest() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.TEST, "/abc", v1, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionTest("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentCopy() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.COPY, "/abc", null, "/def")
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionCopy("/abc", "/def"));
  }

  @Test
  public void mapJsonPatchDocumentMove() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.MOVE, "/abc", null, "/def")
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionMove("/abc", "/def"));
  }

  @Test
  public void mapJsonPatchDocumentNullOperation() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(null, "/abc", null, "/def")
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(0);
  }
}
