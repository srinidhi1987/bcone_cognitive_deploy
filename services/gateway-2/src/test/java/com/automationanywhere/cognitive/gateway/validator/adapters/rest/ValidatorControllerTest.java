package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.USERNAME_HEADER_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.validator.ValidatorService;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.AccuracyProcessingDetailsData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.FailedVbotStatusData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.InvalidFileData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.InvalidFileTypeData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.JsonPatchActionData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.JsonPatchActionReplaceData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.ProductionFileSummaryData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.ProjectTotalsAccuracyDetailsData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.ReviewedFileCountDetailsData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.SuccessData;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.ValidatorDataMapper;
import com.automationanywhere.cognitive.gateway.validator.microservices.validator.ValidatorMicroservice;
import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.core.ParameterizedTypeReference;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ValidatorControllerTest {

  private ApplicationConfiguration cfg;
  private ValidatorDataMapper datam;
  private ValidatorDtoMapper dtom;
  private ValidatorController c;
  private RestClient rc;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    rc = mock(RestClient.class);
    datam = spy(new ValidatorDataMapper());
    dtom = spy(new ValidatorDtoMapper());
    c = new ValidatorController(
        dtom, new ValidatorService(new ValidatorMicroservice(datam, rc, cfg))
    );
  }

  @Test
  public void testGetReviewedFileCountDetails() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj{0}").when(cfg).getReviewedFileCountDetails();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    ReviewedFileCountDetailsData data = mock(ReviewedFileCountDetailsData.class);
    ReviewedFileCountDetailsDto dto = mock(ReviewedFileCountDetailsDto.class);
    ReviewedFileCountDetails model = mock(ReviewedFileCountDetails.class);

    CompletableFuture<List<ReviewedFileCountDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(data));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ReviewedFileCountDetailsData>>() {
        }
    );

    doReturn(model).when(datam).mapReviewedFileCountDetails(data);
    doReturn(dto).when(dtom).mapReviewedFileCountDetails(model);

    // when

    CompletableFuture<StandardResponse<List<ReviewedFileCountDetailsDto>>> f
        = c.getReviewedFileCountDetails(new StringId("1"));

    StandardResponse<List<ReviewedFileCountDetailsDto>> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(dto);
  }

  @Test
  public void testGetNullProjectList() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj{0}").when(cfg).getReviewedFileCountDetails();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<ReviewedFileCountDetailsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<ReviewedFileCountDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<ReviewedFileCountDetailsDto>>> f
        = c.getReviewedFileCountDetails(new StringId("1"));

    StandardResponse<List<ReviewedFileCountDetailsDto>> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.emptyList());
  }

  @Test
  public void testGetFailedVisionBotCount() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj{0}{1}").when(cfg).getFailedVisionBotCount();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<Long>> future = CompletableFuture.completedFuture(
        new StandardResponse<>(16L)
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<Long>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Long>> f = c
        .getFailedVisionBotCount(new StringId("1"), new StringId("2"));

    StandardResponse<Long> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(16);
  }

  @Test
  public void testMarkFileInvalid() throws Exception {
    // given

    InvalidFileData data1 = mock(InvalidFileData.class);
    InvalidFileDto dto1 = mock(InvalidFileDto.class);
    InvalidFile model1 = mock(InvalidFile.class);
    SuccessData data2 = mock(SuccessData.class);
    SuccessDto dto2 = mock(SuccessDto.class);
    Success model2 = mock(Success.class);

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj{0}{1}{2}").when(cfg).markFileInvalid();

    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}{2}")
        .setHeader(USERNAME_HEADER_NAME, "services")
        .param("1")
        .param("2")
        .param("3")
        .content(Collections.singletonList(data1), Types.JSON)
        .accept(Types.JSON)
        .build();

    doReturn(model2).when(datam).mapSuccess(data2);
    doReturn(dto2).when(dtom).mapSuccess(model2);
    doReturn(model1).when(dtom).mapInvalidFile(dto1);
    doReturn(data1).when(datam).mapInvalidFile(model1);

    CompletableFuture<SuccessData> future = CompletableFuture.completedFuture(
        data2
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<SuccessData>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<SuccessDto>> f = c.markFileInvalid(
        new StringId("1"), new StringId("2"), new StringId("3"), "services",
        Collections.singletonList(dto1)
    );

    StandardResponse<SuccessDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(dto2);
  }

  @Test
  public void testUnlockProject() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj{0}{1}").when(cfg).unlockProject();

    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}")
        .setHeader("username", "un")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    SuccessData data = mock(SuccessData.class);
    SuccessDto dto = mock(SuccessDto.class);
    Success model = mock(Success.class);

    CompletableFuture<SuccessData> future = CompletableFuture.completedFuture(data);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<SuccessData>() {
        }
    );

    doReturn(model).when(datam).mapSuccess(data);
    doReturn(dto).when(dtom).mapSuccess(model);

    // when

    CompletableFuture<StandardResponse<SuccessDto>> f = c
        .unlockProject(new StringId("1"), new StringId("2"), "un");

    StandardResponse<SuccessDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(dto);
  }


  @Test
  public void testGetProductionFileSummary() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj{0}{1}").when(cfg).getProductionFileSummary();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    ProductionFileSummaryData data = mock(ProductionFileSummaryData.class);
    ProductionFileSummaryDto dto = mock(ProductionFileSummaryDto.class);
    ProductionFileSummary model = mock(ProductionFileSummary.class);

    CompletableFuture<ProductionFileSummaryData> future = CompletableFuture.completedFuture(data);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ProductionFileSummaryData>() {
        }
    );

    doReturn(model).when(datam).mapProductionFileSummary(data);
    doReturn(dto).when(dtom).mapProductionFileSummary(model);

    // when

    CompletableFuture<StandardResponse<ProductionFileSummaryDto>> f = c
        .getProductionFileSummary(new StringId("1"), new StringId("2"));

    StandardResponse<ProductionFileSummaryDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(dto);
  }

  @Test
  public void testGetInvalidFileTypeList() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj").when(cfg).getInvalidFileTypeList();

    RequestDetails rd = Builder.get("url")
        .request("prj")
        .accept(Types.JSON)
        .build();

    InvalidFileTypeData data = mock(InvalidFileTypeData.class);
    InvalidFileTypeDto dto = mock(InvalidFileTypeDto.class);
    InvalidFileType model = mock(InvalidFileType.class);

    CompletableFuture<List<InvalidFileTypeData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(data));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<InvalidFileTypeData>>() {
        }
    );

    doReturn(model).when(datam).mapInvalidFileType(data);
    doReturn(dto).when(dtom).mapInvalidFileType(model);

    // when

    CompletableFuture<StandardResponse<List<InvalidFileTypeDto>>> f
        = c.getInvalidFileTypeList();

    StandardResponse<List<InvalidFileTypeDto>> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(dto);
  }

  @Test
  public void testGetNullInvalidFileTypeList() throws Exception {
    // given

    doReturn("url").when(cfg).validatorURL();
    doReturn("prj").when(cfg).getInvalidFileTypeList();

    RequestDetails rd = Builder.get("url")
        .request("prj")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<InvalidFileTypeData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<InvalidFileTypeData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<InvalidFileTypeDto>>> f
        = c.getInvalidFileTypeList();

    StandardResponse<List<InvalidFileTypeDto>> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo(Collections.emptyList());
  }

  @Test
  public void testGetFailedVbotData() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).validatorURL();
    doReturn("orgid{0}prjid{1}").when(cfg).getFailedVbotData();

    RequestDetails rd = Builder.get("url")
        .request("orgid{0}prjid{1}")
        .setHeader("username", "un")
        .param("1")
        .param("1")
        .accept(Types.JSON)
        .build();

    Map<String, Object> map = new HashMap<>();

    CompletableFuture<Map<String, Object>> future = CompletableFuture
        .completedFuture(map);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Map<String, Object>>() {
        }
    );

    // WHEN
    CompletableFuture<StandardResponse<Map<String, Object>>> f
        = c.getFailedVbotData(new StringId("1"), new StringId("1"), "un");

    StandardResponse<Map<String, Object>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isSameAs(map);
  }

  @Test
  public void testGetDocumentProcessingDetails() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).validatorURL();
    doReturn("orgid{0}typeid{1}").when(cfg).getAccuracyProcessingDetails();

    RequestDetails rd = Builder.get("url")
        .request("orgid{0}typeid{1}")
        .param("1")
        .param("1")
        .accept(Types.JSON)
        .build();

    AccuracyProcessingDetailsData data = mock(AccuracyProcessingDetailsData.class);
    AccuracyProcessingDetails model = mock(AccuracyProcessingDetails.class);
    AccuracyProcessingDetailsDto dto = mock(AccuracyProcessingDetailsDto.class);

    CompletableFuture<StandardResponse<AccuracyProcessingDetailsData>> future = CompletableFuture
        .completedFuture(new StandardResponse<>(data));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<AccuracyProcessingDetailsData>>() {
        }
    );

    doReturn(model).when(datam).mapAccuracyProcessingDetails(data);
    doReturn(dto).when(dtom).mapAccuracyProcessingDetails(model);

    // WHEN
    CompletableFuture<StandardResponse<AccuracyProcessingDetailsDto>> f
        = c.getDocumentProcessingDetails(new StringId("1"), new StringId("1"));

    StandardResponse<AccuracyProcessingDetailsDto> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualToComparingFieldByFieldRecursively(dto);
  }

  @Test
  public void testGetDashboardAccuracyDetails() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).validatorURL();
    doReturn("orgid{0}projid{1}").when(cfg).getProjectTotalsAccuracyDetails();

    RequestDetails rd = Builder.get("url")
        .request("orgid{0}projid{1}")
        .param("1")
        .param("1")
        .accept(Types.JSON)
        .build();

    ProjectTotalsAccuracyDetailsData data = mock(ProjectTotalsAccuracyDetailsData.class);
    ProjectTotalsAccuracyDetails model = mock(ProjectTotalsAccuracyDetails.class);
    ProjectTotalsAccuracyDetailsDto dto = mock(ProjectTotalsAccuracyDetailsDto.class);

    CompletableFuture<StandardResponse<ProjectTotalsAccuracyDetailsData>> future = CompletableFuture
        .completedFuture(new StandardResponse<>(data));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<ProjectTotalsAccuracyDetailsData>>() {
        }
    );

    doReturn(model).when(datam).mapPrjTotAccuracyDetails(data);
    doReturn(dto).when(dtom).mapPrjTotAccuracyDetails(model);

    // WHEN
    CompletableFuture<StandardResponse<ProjectTotalsAccuracyDetailsDto>> f
        = c.getDashboardAccuracyDetails(new StringId("1"), new StringId("1"));

    StandardResponse<ProjectTotalsAccuracyDetailsDto> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isEqualToComparingFieldByFieldRecursively(dto);
  }

  @Test
  public void testSendCorrectDataValues() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).validatorURL();
    doReturn("orgid{0}projid{1}fileid{2}").when(cfg).getSendCorrectDataValues();

    RequestDetails rd = Builder.post("url")
        .request("orgid{0}projid{1}fileid{2}")
        .setHeader("username", "un")
        .param("1")
        .param("1")
        .param("1")
        .content("additionInfo", Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<StandardResponse<SuccessData>> future = CompletableFuture
        .completedFuture(new StandardResponse<>(new SuccessData(true)));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<SuccessData>>() {
        }
    );

    // WHEN
    CompletableFuture<StandardResponse<SuccessDto>> f
        = c.sendCorrectDataValues(new StringId("1"), new StringId("1"), new StringId("1"),
        "un", "additionInfo");

    StandardResponse<SuccessDto> r
        = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data.success).isTrue();
  }

  @Test
  public void testUpdateValidationStatus() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).validatorURL();
    doReturn("orgid{0}projid{1}visionbotid{2}fileid{3}validationstatus{4}")
        .when(cfg).getUpdateValidationStatus();

    RequestDetails rd = Builder.get("url")
        .request("orgid{0}projid{1}visionbotid{2}fileid{3}validationstatus{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param(Boolean.TRUE)
        .accept(Types.JSON)
        .build();

    FailedVbotStatusData data = new FailedVbotStatusData(true, 1, 0);
    FailedVbotStatus model = new FailedVbotStatus(true, 1, 0);
    FailedVbotStatusDto dto = new FailedVbotStatusDto(true, 1, 0);

    CompletableFuture<FailedVbotStatusData> future = CompletableFuture.completedFuture(data);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<FailedVbotStatusData>() {
        }
    );

    doReturn(model).when(datam).mapFailedVBotStatus(data);
    doReturn(dto).when(dtom).mapFailedVBotStatus(model);

    // WHEN
    CompletableFuture<FailedVbotStatusDto> f
        = c.updateValidationStatus(new StringId("1"), new StringId("2"), new StringId("3"),
        new StringId("4"), Boolean.TRUE);

    FailedVbotStatusDto r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r).isEqualToComparingFieldByFieldRecursively(dto);
  }

  @Test
  public void patchCorrectDataValuesWithValidParamsReturnSuccess() throws Exception {
    // GIVEN
    TextNode v1 = new TextNode("def");
    List<JsonPatchActionData> actionsData = Collections.singletonList(
        new JsonPatchActionReplaceData("/abc", v1)
    );

    String username = "services";

    doReturn("url").when(cfg).validatorURL();
    doReturn("/req/{0}/{1}/{2}").when(cfg).getPatchCorrectDataValues();

    RequestDetails rd = Builder.patch("url")
        .request("/req/{0}/{1}/{2}")
        .setHeader("username", username)
        .param("1")
        .param("2")
        .param("3")
        .content(actionsData, Types.JSON_PATCH)
        .accept(Types.JSON_PATCH)
        .build();

    CompletableFuture<StandardResponse<SuccessData>> future = CompletableFuture.completedFuture(
        new StandardResponse<>(new SuccessData(true))
    );

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<StandardResponse<SuccessData>>() {
        }
    );

    List<JsonPatchActionDto> actionsDto = Collections.singletonList(new JsonPatchActionDto(
            JsonPatchActionOperation.REPLACE,
            "/abc",
            v1,
            null
        )
    );

    // WHEN
    CompletableFuture<StandardResponse<SuccessDto>> f = c.patchCorrectDataValues(
        new StringId("1"), new StringId("2"), new StringId("3"), username, actionsDto
    );

    StandardResponse<SuccessDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.data).isNull();
    assertThat(r.success).isTrue();
  }

  @Test
  public void testKeepAliveValidator() throws Exception {
    // GIVEN
    doReturn("url").when(cfg).validatorURL();
    doReturn("req{0}{1}{2}").when(cfg).validatorKeepAlive();

    RequestDetails rd = Builder.post("url")
        .request("req{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    doReturn(CompletableFuture.completedFuture(new byte[0])).when(rc).send(
        rd, new ParameterizedTypeReference<byte[]>() {
        }
    );

    // WHEN
    CompletableFuture<StandardResponse<Void>> f = c.keepAlive(
        new StringId("1"), new StringId("2"), new StringId("3")
    );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // THEN
    assertThat(f.isDone()).isTrue();
    assertThat(r.success).isTrue();
  }
}


