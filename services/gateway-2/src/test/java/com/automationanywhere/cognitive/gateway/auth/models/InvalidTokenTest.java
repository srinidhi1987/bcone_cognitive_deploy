package com.automationanywhere.cognitive.gateway.auth.models;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class InvalidTokenTest {

  @Test
  public void testInvalidateTokenEquals() {
    // GIVEN
    InvalidToken token1 = new InvalidToken("abc");
    InvalidToken token2 = new InvalidToken("abc");

    // WHEN
    boolean result = token1.equals(token2);

    // THEN
    assertThat(result).isTrue();

    // WHEN
    result = token1.equals(token1);

    // THEN
    assertThat(result).isTrue();
  }

  @Test
  public void testInvalidateTokenNotEquals() {
    // GIVEN
    InvalidToken token1 = new InvalidToken("abc");
    InvalidToken token2 = new InvalidToken("abd");

    // WHEN
    boolean result = token1.equals(token2);

    // THEN
    assertThat(result).isFalse();

    // WHEN
    result = token1.equals(new Object());

    // THEN
    assertThat(result).isFalse();
  }

  @Test
  public void testInvalidateTokenHashCode() {
    // GIVEN
    InvalidToken token1 = new InvalidToken("abc");
    InvalidToken token2 = new InvalidToken("abc");
    InvalidToken token3 = new InvalidToken("abd");

    // WHEN
    boolean resultEquals = token1.hashCode() == token2.hashCode();
    boolean resultNotEquals = token1.hashCode() == token3.hashCode();

    // THEN
    assertThat(resultEquals).isTrue();
    assertThat(resultNotEquals).isFalse();
  }
}
