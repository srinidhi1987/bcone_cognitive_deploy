package com.automationanywhere.cognitive.restclient;

import static com.automationanywhere.cognitive.restclient.RequestDetails.Types.ATTACHMENT;
import static com.automationanywhere.cognitive.restclient.RequestDetails.Types.JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.entry;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.dataresource.DataContainer;
import com.automationanywhere.cognitive.dataresource.DataResource;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import java.io.InputStream;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.assertj.core.api.AbstractThrowableAssert;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.testng.annotations.Test;

public class RequestDetailsBuilderTest {

  @Test
  public void testURL() {
    assertThat(RequestDetails.Builder.get("p").build().url).isEqualTo("p");
    assertThat(RequestDetails.Builder.get("p").request("/r").build().url).isEqualTo("p/r");

    String url = Builder.get("p").request("/r?p1={0}").param("0").build().url;
    assertThat(url).isEqualTo("p/r?p1=0");

    url = Builder.get("p").request("/r?p1={0}&p2={1}").param("0").param("00").build().url;
    assertThat(url).isEqualTo("p/r?p1=0&p2=00");
  }

  @Test
  public void testCreateMediaTypeHeader() {
    Set<String> mediaTypeHeader = Builder.get("/")
        .createMediaTypeHeader(MediaType.APPLICATION_JSON, 13);

    assertThat(mediaTypeHeader).containsExactly("application/vnd.cognitive.v13+json");

    mediaTypeHeader = Builder.get("/")
        .createMediaTypeHeader(MediaType.APPLICATION_JSON, null);

    assertThat(mediaTypeHeader).containsExactly("application/json");
  }

  @Test
  public void testCreate() {
    assertThat(RequestDetails.Builder.get("/").build().method).isSameAs(HttpMethod.GET);
    assertThat(RequestDetails.Builder.post("/").build().method).isSameAs(HttpMethod.POST);
    assertThat(RequestDetails.Builder.put("/").build().method).isSameAs(HttpMethod.PUT);
    assertThat(RequestDetails.Builder.delete("/").build().method).isSameAs(HttpMethod.DELETE);
  }

  @Test
  public void testAcceptJSON() {
    HashMap<String, List<String>> headers = new HashMap<>(
        Builder.get("/").accept(JSON).build().headers
    );

    assertThat(headers).containsExactly(
        entry("Accept", Collections.singletonList("application/json;charset=UTF-8"))
    );
  }

  @Test
  public void setHeaderShouldSetAHeader() {
    // given

    HttpHeaders h = new HttpHeaders();
    h.put("h", Collections.singletonList("v"));

    // when
    RequestDetails r = Builder.delete("/").setHeader("h", "v").build();

    // then
    assertThat(r.headers).isEqualTo(h);
  }

  @Test
  public void setHeaderShouldReplaceAHeader() {
    // given

    HttpHeaders h = new HttpHeaders();
    h.put("h", Collections.singletonList(" "));

    // when
    RequestDetails r = Builder.delete("/")
        .setHeader("h", "v")
        .setHeader("h", "v2")
        .setHeader("h", null)
        .setHeader("h", " ")
        .build();

    // then
    assertThat(r.headers).isEqualTo(h);
  }

  @Test
  public void addHeaderShouldAddHeaders() {
    // when
    RequestDetails r = Builder.patch("/")
        .setHeader("h", "v")
        .addHeader("h", "v2")
        .addHeader("h", null)
        .addHeader("h", " ")
        .build();

    // then
    assertThat(r.headers).size().isEqualTo(1);
    assertThat(r.headers.get("h")).containsExactlyInAnyOrder("v", "", " ", "v2");
  }

  @Test
  public void normalizeHeaderShouldThrowAnErrorIfTheAcceptHeaderIsAdded() {
    // when
    try {
      Builder.delete("/").normalizeHeader("acCept");
    } catch (final IllegalArgumentException ex) {
      // then
      assertThat(ex.getMessage()).isEqualTo(
          "Please use appropriate method to set the acCept header"
      );
    }
  }

  @Test
  public void normalizeHeaderShouldThrowAnErrorIfTheContentTypeHeaderIsAdded() {
    // when
    try {
      Builder.delete("/").normalizeHeader("content-Type");
    } catch (final IllegalArgumentException ex) {
      // then
      assertThat(ex.getMessage()).isEqualTo(
          "Please use appropriate method to set the content-Type header"
      );
    }
  }

  @Test
  public void normalizeHeaderShouldThrowAnErrorIfABlankHeaderIsPassed() {
    // when
    try {
      Builder.delete("/").normalizeHeader("  ");
    } catch (final IllegalArgumentException ex) {
      // then
      assertThat(ex.getMessage()).isEqualTo("Header name cannot be blank");
    }
  }

  @Test
  public void normalizeHeaderShouldThrowAnErrorIfANullHeaderIsPassed() {
    // when
    try {
      Builder.delete("/").normalizeHeader(null);
    } catch (final IllegalArgumentException ex) {
      // then
      assertThat(ex.getMessage()).isEqualTo("Header name cannot be null");
    }
  }

  @Test
  public void testJson() {
    Object o = new Object();

    RequestDetails rd = Builder.get("/").content(o, JSON).build();
    assertThat(rd.body).isSameAs(o);
    assertThat(rd.headers).contains(new SimpleEntry<>(
        "Content-Type", Collections.singletonList("application/json;charset=UTF-8")
    ));

    rd = Builder.get("/").content(o, JSON, 3).build();
    assertThat(rd.body).isSameAs(o);
    assertThat(rd.headers).contains(new SimpleEntry<>(
        "Content-Type", Collections.singletonList("application/vnd.cognitive.v3+json;charset=UTF-8")
    ));
  }

  @Test
  public void testAttachment() throws Exception {
    // given

    InputStream is = mock(InputStream.class);
    DataContainer dc = new DataContainer(
        is, "image/png", "nm", 23L
    );

    HttpHeaders h = new HttpHeaders();
    h.add("Content-Type", "multipart/form-data");

    HttpHeaders h2 = new HttpHeaders();
    h2.add("Content-Type", "image/png");

    // when
    RequestDetails rd = Builder.post("/").content(dc).build();

    // then
    assertThat(rd.headers).isEqualTo(h);

    LinkedMultiValueMap<String, Object> body = (LinkedMultiValueMap<String, Object>) rd.body;
    assertThat(body).hasSize(1);

    assertThat(body.get("file")).hasSize(1);

    HttpEntity<DataResource> e = (HttpEntity<DataResource>) body.get("file").get(0);
    assertThat(e.getHeaders()).isEqualTo(h2);
    assertThat(e.getBody().contentLength()).isEqualTo(23L);
    assertThat(e.getBody().getFilename()).isEqualTo("nm");
    assertThat(e.getBody().getInputStream()).isSameAs(is);
  }

  @Test
  public void contentShouldThrowAnExceptionIfAttachmentHasVersion() throws Exception {
    // given
    DataContainer dc = new DataContainer(
        mock(InputStream.class), "image/png", "nm", 23L
    );

    // when
    AbstractThrowableAssert<?, ? extends Throwable> a = assertThatThrownBy(
        () -> Builder.post("/").content(dc, ATTACHMENT, 1));

    //then
    a.isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Multipart form data doesn't support versioning");
  }

  @Test
  public void testQuery() {
    assertThat(Builder.get("/").request("{1}//{2}?&&a={2}&&b={1}&&c={2}&&")
        .param("b").param("").build().url
    ).isEqualTo("///{2}");

    assertThat(Builder.get("/").request("?a={0}").param("b").build().url).isEqualTo("/?a=b");
    assertThat(Builder.get("/").request("?a={0}&").param("b").build().url).isEqualTo("/?a=b");

    assertThat(Builder.get("/").request("?a={0}&b={0}").param("b").build().url)
        .isEqualTo("/?a=b&b=b");

    assertThat(Builder.get("/").request("?a={0}&&b={0}").param("b").build().url)
        .isEqualTo("/?a=b&b=b");

    assertThat(Builder.get("/").request("?a={0}&&b={0}&").param("b").build().url)
        .isEqualTo("/?a=b&b=b");

    assertThat(Builder.get("/").request("?&a={0}&&b={0}&").param("b").build().url)
        .isEqualTo("/?a=b&b=b");

    assertThat(Builder.get("/").request("?a={0}&b={1}").param("b").build().url)
        .isEqualTo("/?a=b");

    assertThat(Builder.get("/").request("?a={0}&b={1}&c&d=").param("b").build().url)
        .isEqualTo("/?a=b");

    assertThat(Builder.get("/").request("?a={0}&b={1}&c=&&d=&e={0}").param("b").build().url)
        .isEqualTo("/?a=b&e=b");

    assertThat(Builder.get("/").request("?a={0}&b={1}&c={0}").param("b").build().url).
        isEqualTo("/?a=b&c=b");

    assertThat(Builder.get("/").request("?a={0}&b={1}&c={0}").param("b").param("c").build().url)
        .isEqualTo("/?a=b&b=c&c=b");

    assertThat(Builder.get("/").request("{1}//{2}?&&a={0}&&b={1}&&c={0}&&").param("b").build().url)
        .isEqualTo("/{1}//{2}?a=b&c=b");

    assertThat(Builder.get("/").request("{1}//{2}?&&a={2}&&b={1}&&c={2}&&").param("b").build().url)
        .isEqualTo("/{1}//{2}");

    assertThat(Builder.get("/").request("{1}//{2}?&&a={2}&&b={1}&&c={2}&&")
        .param("b").param(null).build().url
    ).isEqualTo("///{2}");

    assertThat(Builder.get("/").request("{1}//{2}?&&a={2}&&b={1}&&c={2}&&")
        .param("b").param(" ").build().url
    ).isEqualTo("/ //{2}");

    assertThat(Builder.get("/").request("{1}//{2}?&&a={2}&&b={1}&&c={2}&&")
        .param("b").param(null).param("A").build().url).isEqualTo("///A?a=A&c=A");

    assertThat(
        RequestDetails.Builder
            .get("http://www.{0}.com")
            .request("/path/{1}")
            .param("example")
            .param(new UUIDId(UUID.fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb")))
            .build()
            .url
    ).isEqualTo("http://www.example.com/path/2c076351-774b-49e4-8e55-ce0ab4251ceb");

    assertThat(
        RequestDetails.Builder
            .get("http://www.{0}.com")
            .request("/path/{1}")
            .param("example")
            .param(null)
            .build()
            .url
    ).isEqualTo("http://www.example.com/path/");

    assertThat(
        RequestDetails.Builder
            .get("http://www.{0}.com")
            .request("/path/{1}")
            .param("example")
            .build()
            .url
    ).isEqualTo("http://www.example.com/path/{1}");

    assertThat(
        RequestDetails.Builder
            .get("http://www.{0}.com")
            .request("?path={1}")
            .param("example")
            .param(new UUIDId(UUID.fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb")))
            .build()
            .url
    ).isEqualTo("http://www.example.com?path=2c076351-774b-49e4-8e55-ce0ab4251ceb");

    assertThat(
        RequestDetails.Builder
            .get("http://www.{0}.com")
            .request("?path={1}")
            .param("example")
            .param(null)
            .build()
            .url
    ).isEqualTo("http://www.example.com");

    assertThat(
        RequestDetails.Builder
            .get("http://www.{0}.com")
            .request("?path={1}")
            .param("example")
            .build()
            .url
    ).isEqualTo("http://www.example.com");
  }

  @Test
  public void testEquals() {
    Object o1 = new Object();
    Object o2 = new Object();

    HttpMethod m1 = HttpMethod.GET;
    HttpMethod m2 = HttpMethod.POST;

    HttpHeaders h1 = new HttpHeaders();
    HttpHeaders h2 = new HttpHeaders();
    h2.put("h", Collections.singletonList("v"));

    RequestDetails rs1 = new RequestDetails("u1", o1, m1, h1);
    RequestDetails rs2 = new RequestDetails("u1", o1, m1, h1);
    RequestDetails rs3 = new RequestDetails("u2", o1, m1, h1);
    RequestDetails rs4 = new RequestDetails("u1", o2, m1, h1);
    RequestDetails rs5 = new RequestDetails("u1", o1, m2, h1);
    RequestDetails rs6 = new RequestDetails("u1", o1, m1, h2);

    assertThat(rs1.equals(rs1)).isTrue();
    assertThat(rs1.equals(rs2)).isTrue();
    assertThat(rs1.equals(rs3)).isFalse();
    assertThat(rs1.equals(rs4)).isFalse();
    assertThat(rs1.equals(rs5)).isFalse();
    assertThat(rs1.equals(rs6)).isFalse();
    assertThat(rs1.equals(null)).isFalse();
    assertThat(rs1.equals(new Object())).isFalse();

    assertThat(rs1.hashCode()).isEqualTo(rs2.hashCode());
    assertThat(rs1.hashCode()).isNotEqualTo(rs3.hashCode());
    assertThat(rs1.hashCode()).isNotEqualTo(rs4.hashCode());
    assertThat(rs1.hashCode()).isNotEqualTo(rs5.hashCode());
    assertThat(rs1.hashCode()).isNotEqualTo(rs6.hashCode());
  }
}
