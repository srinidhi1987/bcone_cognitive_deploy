package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class JsonPatchActionRemoveDataTest {

  @Test
  public void testJsonPatchActionRemoveDataEquals() {
    // GIVEN
    JsonPatchActionRemoveData data1 = new JsonPatchActionRemoveData("/abc");
    JsonPatchActionRemoveData data2 = new JsonPatchActionRemoveData("/abc");
    JsonPatchActionRemoveData data3 = new JsonPatchActionRemoveData("/def");

    // WHEN
    boolean equals = data1.equals(data2);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(data3);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(null);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(data1);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(new Object());

    // THEN
    assertThat(equals).isFalse();
  }

  @Test
  public void testJsonPatchActionRemoveDataHashCode() {
    // GIVEN
    JsonPatchActionRemoveData data1 = new JsonPatchActionRemoveData("/abc");
    JsonPatchActionRemoveData data2 = new JsonPatchActionRemoveData("/abc");
    JsonPatchActionRemoveData data3 = new JsonPatchActionRemoveData("/def");

    // WHEN
    int data1Hash = data1.hashCode();
    int data2Hash = data2.hashCode();
    int data3Hash = data3.hashCode();

    // THEN
    assertThat(data1Hash).isEqualTo(data2Hash);
    assertThat(data1Hash).isNotEqualTo(data3Hash);
  }
}
