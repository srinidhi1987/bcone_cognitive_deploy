package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.dataresource.DataContainer;
import com.automationanywhere.cognitive.gateway.file.FileService;
import com.automationanywhere.cognitive.gateway.file.microservices.file.CategoriesData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.ClassificationAnalysisReportData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.CountStatisticsData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FileBlobsData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FileDataMapper;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FileDetailsData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FileMicroservice;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FilePatchDetailData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FileStatusData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.LearningInstanceSummaryData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.RawFileReportFromClassifierData;
import com.automationanywhere.cognitive.gateway.file.microservices.file.UploadResponseData;
import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FileControllerTest {

  private ApplicationConfiguration cfg;
  private FileDataMapper datam;
  private FileDtoMapper dtom;
  private FileController c;
  private RestClient rc;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    rc = mock(RestClient.class);
    datam = spy(new FileDataMapper());
    dtom = spy(new FileDtoMapper());
    c = new FileController(
        dtom, new FileService(new FileMicroservice(datam, rc, cfg))
    );
  }

  @Test
  public void testGetProjectCategories() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{3}{4}{5}").when(cfg).getOrganizationProjectCategories();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{3}{4}{5}")
        .param("1")
        .param("2")
        .param(3)
        .param(4)
        .param("5")
        .accept(Types.JSON)
        .build();

    CategoriesData categoriesData = mock(CategoriesData.class);
    Categories categories = mock(Categories.class);
    CategoriesDto categoriesDto = mock(CategoriesDto.class);

    CompletableFuture<CategoriesData> future =
        CompletableFuture.completedFuture(categoriesData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<CategoriesData>() {
        }
    );

    doReturn(categories).when(datam).mapCategories(categoriesData);
    doReturn(categoriesDto).when(dtom).mapCategories(categories);

    // when

    CompletableFuture<StandardResponse<CategoriesDto>> f
        = c.getProjectCategories(new StringId("1"), new StringId("2"), 3, 4, "5");

    StandardResponse<CategoriesDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(categoriesDto);
  }

  @Test
  public void testGetProjectCategoryFiles() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectCategoryFiles();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("5")
        .accept(Types.JSON)
        .build();

    FileDetailsData fileDetailsData = mock(FileDetailsData.class);
    FileDetails fileDetails = mock(FileDetails.class);
    FileDetailsDto fileDetailsDto = mock(FileDetailsDto.class);

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(fileDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    doReturn(fileDetails).when(datam).mapFileDetails(fileDetailsData);
    doReturn(fileDetailsDto).when(dtom).mapFileDetails(fileDetails);

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectCategoryFiles(new StringId("1"), new StringId("2"), new StringId("5"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(fileDetailsDto);
  }

  @Test
  public void testGetNullProjectCategoryFiles() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectCategoryFiles();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectCategoryFiles(new StringId("1"), new StringId("2"), new StringId("5"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }


  @Test
  public void testGetProjectFile() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectFile();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("5")
        .accept(Types.JSON)
        .build();

    FileDetailsData fileDetailsData = mock(FileDetailsData.class);
    FileDetails fileDetails = mock(FileDetails.class);
    FileDetailsDto fileDetailsDto = mock(FileDetailsDto.class);

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(fileDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    doReturn(fileDetails).when(datam).mapFileDetails(fileDetailsData);
    doReturn(fileDetailsDto).when(dtom).mapFileDetails(fileDetails);

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectFile(new StringId("1"), new StringId("2"), new StringId("5"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(fileDetailsDto);
  }

  @Test
  public void testGetNullProjectFile() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectFile();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("5")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectFile(new StringId("1"), new StringId("2"), new StringId("5"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testGetProjectFiles() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectFiles();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    FileDetailsData fileDetailsData = mock(FileDetailsData.class);
    FileDetails fileDetails = mock(FileDetails.class);
    FileDetailsDto fileDetailsDto = mock(FileDetailsDto.class);

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(fileDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    doReturn(fileDetails).when(datam).mapFileDetails(fileDetailsData);
    doReturn(fileDetailsDto).when(dtom).mapFileDetails(fileDetails);

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectFiles(new StringId("1"), new StringId("2"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(fileDetailsDto);
  }

  @Test
  public void testGetNullProjectFiles() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectFiles();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectFiles(new StringId("1"), new StringId("2"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testGetProjectStagingFiles() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectStagingFiles();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    FileDetailsData fileDetailsData = mock(FileDetailsData.class);
    FileDetails fileDetails = mock(FileDetails.class);
    FileDetailsDto fileDetailsDto = mock(FileDetailsDto.class);

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(fileDetailsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    doReturn(fileDetails).when(datam).mapFileDetails(fileDetailsData);
    doReturn(fileDetailsDto).when(dtom).mapFileDetails(fileDetails);

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectStagingFiles(new StringId("1"), new StringId("2"), new StringId("3"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(fileDetailsDto);
  }

  @Test
  public void testGetNullProjectStagingFiles() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectStagingFiles();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<FileDetailsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<FileDetailsDto>>> f
        = c.getProjectStagingFiles(new StringId("1"), new StringId("2"), new StringId("3"));

    StandardResponse<List<FileDetailsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testGetFileCount() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}").when(cfg).getOrganizationFileCount();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CountStatisticsData countStatisticsData = mock(CountStatisticsData.class);
    CountStatistics countStatistics = mock(CountStatistics.class);
    CountStatisticsDto countStatisticsDto = mock(CountStatisticsDto.class);

    CompletableFuture<List<CountStatisticsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(countStatisticsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<CountStatisticsData>>() {
        }
    );

    doReturn(countStatistics).when(datam).mapCountStatistics(countStatisticsData);
    doReturn(countStatisticsDto).when(dtom).mapCountStatistics(countStatistics);

    // when

    CompletableFuture<StandardResponse<List<CountStatisticsDto>>> f
        = c.getFileCount(new StringId("1"));

    StandardResponse<List<CountStatisticsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(countStatisticsDto);
  }

  @Test
  public void testGetNullFileCount() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}").when(cfg).getOrganizationFileCount();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<CountStatisticsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<CountStatisticsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<CountStatisticsDto>>> f
        = c.getFileCount(new StringId("1"));

    StandardResponse<List<CountStatisticsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testGetProjectFileCount() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectFileCount();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    CountStatisticsData countStatisticsData = mock(CountStatisticsData.class);
    CountStatistics countStatistics = mock(CountStatistics.class);
    CountStatisticsDto countStatisticsDto = mock(CountStatisticsDto.class);

    CompletableFuture<List<CountStatisticsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(countStatisticsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<CountStatisticsData>>() {
        }
    );

    doReturn(countStatistics).when(datam).mapCountStatistics(countStatisticsData);
    doReturn(countStatisticsDto).when(dtom).mapCountStatistics(countStatistics);

    // when

    CompletableFuture<StandardResponse<List<CountStatisticsDto>>> f
        = c.getProjectFileCount(new StringId("1"), new StringId("2"));

    StandardResponse<List<CountStatisticsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(countStatisticsDto);
  }

  @Test
  public void testGetNullProjectFileCount() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectFileCount();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<CountStatisticsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<CountStatisticsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<CountStatisticsDto>>> f
        = c.getProjectFileCount(new StringId("1"), new StringId("2"));

    StandardResponse<List<CountStatisticsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testPatchProjectFile() throws Exception {
    // given

    FilePatchDetailDto filePatchDetailDto = mock(FilePatchDetailDto.class);
    FilePatchDetail filePatchDetail = mock(FilePatchDetail.class);
    FilePatchDetailData filePatchDetailData = mock(FilePatchDetailData.class);

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).patchOrganizationProjectFile();

    RequestDetails rd = Builder.patch("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .content(Collections.singletonList(filePatchDetailData), Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<Void> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    doReturn(filePatchDetailData).when(datam).mapFilePatchDetails(filePatchDetail);
    doReturn(filePatchDetail).when(dtom).mapFilePatchDetails(filePatchDetailDto);

    // when

    CompletableFuture<StandardResponse<Void>> f = c.patchProjectFile(
        new StringId("1"), new StringId("2"), new StringId("3"),
        Collections.singletonList(filePatchDetailDto)
    );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isNull();
  }

  @Test
  public void testDeleteProjectFile() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}{3}").when(cfg).deleteOrganizationProjectFile();

    RequestDetails rd = Builder.delete("url")
        .request("prj{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .accept(Types.JSON)
        .build();

    CompletableFuture<Void> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Void>> f = c.deleteProjectFile(
        new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4")
    );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isNull();
  }

  @Test
  public void testGetProjectFileBlobs() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectFileBlobs();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    FileBlobsData fileBlobsData = mock(FileBlobsData.class);
    FileBlobs fileBlobs = mock(FileBlobs.class);
    FileBlobsDto fileBlobsDto = mock(FileBlobsDto.class);

    CompletableFuture<List<FileBlobsData>> future =
        CompletableFuture.completedFuture(Collections.singletonList(fileBlobsData));

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileBlobsData>>() {
        }
    );

    doReturn(fileBlobs).when(datam).mapFileBlobs(fileBlobsData);
    doReturn(fileBlobsDto).when(dtom).mapFileBlobs(fileBlobs);

    // when

    CompletableFuture<StandardResponse<List<FileBlobsDto>>> f
        = c.getFileBlobs(new StringId("1"), new StringId("2"), new StringId("3"));

    StandardResponse<List<FileBlobsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).containsExactly(fileBlobsDto);
  }

  @Test
  public void testGetNullProjectFileBlobs() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectFileBlobs();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .accept(Types.JSON)
        .build();

    CompletableFuture<List<FileBlobsData>> future =
        CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<List<FileBlobsData>>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<List<FileBlobsDto>>> f
        = c.getFileBlobs(new StringId("1"), new StringId("2"), new StringId("3"));

    StandardResponse<List<FileBlobsDto>> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEmpty();
  }

  @Test
  public void testUploadFile() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}{3}").when(cfg).uploadOrganizationProjectFiles();

    InputStream data = mock(InputStream.class);
    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .content(Collections.singletonList(new DataContainer(
            data, "image/png", "nm", 23L
        )), Types.ATTACHMENT)
        .accept(Types.JSON)
        .build();

    UploadResponseData uploadResponseData = mock(UploadResponseData.class);
    UploadResponse uploadResponse = mock(UploadResponse.class);
    UploadResponseDto uploadResponseDto = mock(UploadResponseDto.class);

    CompletableFuture<UploadResponseData> future =
        CompletableFuture.completedFuture(uploadResponseData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<UploadResponseData>() {
        }
    );

    doReturn(uploadResponse).when(datam).mapUploadResponse(uploadResponseData);

    doReturn(uploadResponseDto).when(dtom).mapUploadResponse(uploadResponse);

    MultipartFile file = mock(MultipartFile.class);
    doReturn(data).when(file).getInputStream();
    doReturn("nm").when(file).getOriginalFilename();
    doReturn("image/png").when(file).getContentType();
    doReturn(23L).when(file).getSize();

    MultipartHttpServletRequest req = mock(MultipartHttpServletRequest.class);
    doReturn(Collections.singletonList("file").iterator()).when(req).getFileNames();
    doReturn(Collections.singletonList(file)).when(req).getFiles("file");

    // when

    CompletableFuture<StandardResponse<UploadResponseDto>> f = c.uploadProjectFiles(
        new StringId("1"), new StringId("2"), "3", new StringId("4"), req
    );

    StandardResponse<UploadResponseDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(uploadResponseDto);
  }


  @Test
  public void testUploadStagingFile() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).uploadOrganizationProjectStagingFiles();

    InputStream data = mock(InputStream.class);
    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("3")
        .content(Collections.singletonList(new DataContainer(
            data, "image/png", "nm", 23L
        )), Types.ATTACHMENT)
        .accept(Types.JSON)
        .build();

    UploadResponseData uploadResponseData = mock(UploadResponseData.class);
    UploadResponse uploadResponse = mock(UploadResponse.class);
    UploadResponseDto uploadResponseDto = mock(UploadResponseDto.class);

    CompletableFuture<UploadResponseData> future =
        CompletableFuture.completedFuture(uploadResponseData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<UploadResponseData>() {
        }
    );

    doReturn(uploadResponse).when(datam).mapUploadResponse(uploadResponseData);

    doReturn(uploadResponseDto).when(dtom).mapUploadResponse(uploadResponse);

    MultipartFile file = mock(MultipartFile.class);
    doReturn(data).when(file).getInputStream();
    doReturn("nm").when(file).getOriginalFilename();
    doReturn("image/png").when(file).getContentType();
    doReturn(23L).when(file).getSize();

    MultipartHttpServletRequest req = mock(MultipartHttpServletRequest.class);
    doReturn(Collections.singletonList("file").iterator()).when(req).getFileNames();
    doReturn(Collections.singletonList(file)).when(req).getFiles("file");

    // when

    CompletableFuture<StandardResponse<UploadResponseDto>> f = c.uploadProjectStagingFiles(
        new StringId("1"), new StringId("2"), new StringId("3"), req
    );

    StandardResponse<UploadResponseDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(uploadResponseDto);
  }

  @Test
  public void testGetProjectReport() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectReport();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    ClassificationAnalysisReportData classificationAnalysisReportData
        = mock(ClassificationAnalysisReportData.class);
    ClassificationAnalysisReport classificationAnalysisReport
        = mock(ClassificationAnalysisReport.class);
    ClassificationAnalysisReportDto classificationAnalysisReportDto
        = mock(ClassificationAnalysisReportDto.class);

    CompletableFuture<ClassificationAnalysisReportData> future =
        CompletableFuture.completedFuture(classificationAnalysisReportData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ClassificationAnalysisReportData>() {
        }
    );

    doReturn(classificationAnalysisReport).when(datam)
        .mapClassificationAnalysisReport(classificationAnalysisReportData);
    doReturn(classificationAnalysisReportDto).when(dtom)
        .mapClassificationAnalysisReport(classificationAnalysisReport);

    // when

    CompletableFuture<StandardResponse<ClassificationAnalysisReportDto>> f
        = c.getProjectReport(new StringId("1"), new StringId("2"));

    StandardResponse<ClassificationAnalysisReportDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(classificationAnalysisReportDto);
  }

  @Test
  public void testGetProjectFileStatus() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}").when(cfg).getOrganizationProjectFileStatus();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}{2}")
        .param("1")
        .param("2")
        .param("A")
        .accept(Types.JSON)
        .build();

    FileStatusData fileStatusData = mock(FileStatusData.class);
    FileStatus fileStatus = mock(FileStatus.class);
    FileStatusDto fileStatusDto = mock(FileStatusDto.class);

    CompletableFuture<FileStatusData> future = CompletableFuture.completedFuture(fileStatusData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<FileStatusData>() {
        }
    );

    doReturn(fileStatus).when(datam).mapFileStatus(fileStatusData);
    doReturn(fileStatusDto).when(dtom).mapFileStatus(fileStatus);

    // when

    CompletableFuture<StandardResponse<FileStatusDto>> f
        = c.getProjectFileStatus(new StringId("1"), new StringId("2"), new StringId("A"));

    StandardResponse<FileStatusDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(fileStatusDto);
  }

  @Test
  public void testCreateSegmentedDocument() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}").when(cfg).createSegmentedDocument();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .content("text", Types.TEXT)
        .accept(Types.JSON)
        .build();

    CompletableFuture<String> future = CompletableFuture.completedFuture("resp");

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<String>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f
        = c.createSegmentedDocument(new StringId("1"), "text");

    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("resp");
  }

  @Test
  public void testGetSegmentedDocument() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}").when(cfg).getSegmentedDocument();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}")
        .param("1")
        .accept(Types.JSON)
        .build();

    CompletableFuture<String> future = CompletableFuture.completedFuture("resp");

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<String>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<String>> f = c.getSegmentedDocument(new StringId("1"));
    StandardResponse<String> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isEqualTo("resp");
  }

  @Test
  public void testGetProjectClassificationSummary() throws Exception {
    // given

    LearningInstanceSummaryData data = mock(LearningInstanceSummaryData.class);
    LearningInstanceSummary model = mock(LearningInstanceSummary.class);
    LearningInstanceSummaryDto dto = mock(LearningInstanceSummaryDto.class);

    doReturn(model).when(datam).mapLearningInstanceSummary(data);
    doReturn(dto).when(dtom).mapLearningInstanceSummary(model);

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}").when(cfg).getOrganizationProjectClassificationSummary();

    RequestDetails rd = Builder.get("url")
        .request("prj{0}{1}")
        .param("1")
        .param("2")
        .accept(Types.JSON)
        .build();

    CompletableFuture<LearningInstanceSummaryData> future = CompletableFuture.completedFuture(data);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<LearningInstanceSummaryData>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<LearningInstanceSummaryDto>> f = c
        .getProjectClassificationSummary(
            new StringId("1"), new StringId("2")
        );
    StandardResponse<LearningInstanceSummaryDto> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isSameAs(dto);
  }

  @Test
  public void testUpdateProjectClassificationById() throws Exception {
    // given

    RawFileReportFromClassifierData data = mock(RawFileReportFromClassifierData.class);
    RawFileReportFromClassifier model = mock(RawFileReportFromClassifier.class);
    RawFileReportFromClassifierDto dto = mock(RawFileReportFromClassifierDto.class);

    doReturn(model).when(dtom).mapRawFileReportFromClassifierData(dto);
    doReturn(data).when(datam).mapRawFileReportFromClassifierData(model);

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}{3}{4}").when(cfg).updateOrganizationProjectClassificationById();

    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}{2}{3}{4}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .param("5")
        .content(data, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<Void> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Void>> f = c
        .updateProjectClassificationById(
            new StringId("1"), new StringId("2"), new StringId("3"),
            new StringId("4"), new StringId("5"),
            dto
        );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isNull();
  }

  @Test
  public void testUpdateProjectClassification() throws Exception {
    // given

    doReturn("url").when(cfg).fileURL();
    doReturn("prj{0}{1}{2}{3}").when(cfg).updateOrganizationProjectClassification();

    RequestDetails rd = Builder.post("url")
        .request("prj{0}{1}{2}{3}")
        .param("1")
        .param("2")
        .param("3")
        .param("4")
        .accept(Types.JSON)
        .build();

    CompletableFuture<Void> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<Void>() {
        }
    );

    // when

    CompletableFuture<StandardResponse<Void>> f = c
        .updateProjectClassification(
            new StringId("1"), new StringId("2"), new StringId("3"), new StringId("4")
        );

    StandardResponse<Void> r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();

    assertThat(r.data).isNull();
  }
}
