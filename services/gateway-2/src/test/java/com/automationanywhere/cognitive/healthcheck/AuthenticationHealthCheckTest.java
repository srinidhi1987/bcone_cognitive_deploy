package com.automationanywhere.cognitive.healthcheck;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.xebialabs.restito.server.StubServer;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class AuthenticationHealthCheckTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private TestRestTemplate rtpl;

  private StubServer server;

  @BeforeMethod
  public void setUp() throws Exception {
    server = new StubServer().run();
  }

  @AfterMethod
  public void tearDown() throws Exception {
    server.stop();
  }

  @Test
  public void testHealthCheck() throws Exception {
    assertThat(rtpl.exchange(
        new URI("/healthcheck"), HttpMethod.GET, new HttpEntity<>(null), String.class
    ).getStatusCode().getReasonPhrase()).isNotEmpty();
  }
}
