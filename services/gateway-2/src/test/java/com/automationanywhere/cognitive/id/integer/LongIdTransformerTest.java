package com.automationanywhere.cognitive.id.integer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.fasterxml.jackson.databind.node.DecimalNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.NullNode;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;
import org.testng.annotations.Test;

public class LongIdTransformerTest {

  @Test
  public void testCreate() throws Exception {
    UUID uuid = UUID.randomUUID();
    assertThat(new UUIDId(uuid).uuid).isEqualTo(uuid);

    try {
      new UUIDId(null);
      fail();
    } catch (final IllegalArgumentException ex) {
      //As expected
    }
  }

  @Test
  public void testTransformations() {
    LongIdTransformer t = new LongIdTransformer();
    long lId = 5_747_553_489_175_945_943L;
    String sId = "5747553489175945943";

    assertThat(t.fromNode(NullNode.getInstance())).isNull();
    assertThat(t.fromNode(null)).isNull();
    assertThat(t.fromString(null)).isNull();
    assertThat(t.toString(null)).isNull();
    assertThat(t.toNode(null)).isSameAs(NullNode.getInstance());

    assertThat(t.fromString(sId)).isEqualTo(new LongId(lId));
    assertThat(t.fromNode(new IntNode(2))).isEqualTo(new LongId(2));
    assertThat(t.fromNode(new LongNode(lId))).isEqualTo(new LongId(lId));
    assertThat(t.fromNode(new BigIntegerNode(BigInteger.TEN))).isEqualTo(new LongId(10));

    assertThat(t.toNode(new LongId(lId))).isEqualTo(new LongNode(lId));
    assertThat(t.toString(new LongId(lId))).isEqualTo(sId);

    try {
      t.fromString("id1");
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo("id1 is not supported");
    }

    try {
      t.fromNode(new DecimalNode(BigDecimal.TEN));
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo(
          "class com.fasterxml.jackson.databind.node.DecimalNode is not supported"
      );
    }

    try {
      t.fromNode(new BigIntegerNode(new BigInteger("15747553489175945943")));
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo(
          "15747553489175945943 is not supported"
      );
    }

    try {
      t.toNode(new StringId("1"));
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo(
          "class com.automationanywhere.cognitive.id.string.StringId is not supported"
      );
    }
  }

  @Test
  public void testSupports() {
    LongIdTransformer t = new LongIdTransformer();
    assertThat(t.supports(Id.class)).isFalse();
    assertThat(t.supports(LongId.class)).isTrue();
    assertThat(t.supports(UUIDId.class)).isFalse();

    try {
      t.supports(null);
      fail();
    } catch (final NullPointerException ex) {
      //As expected
    }
  }
}
