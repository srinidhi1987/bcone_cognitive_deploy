package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.Category;
import com.automationanywhere.cognitive.gateway.file.models.CategoryDetail;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.EnvironmentCountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FieldDetail;
import com.automationanywhere.cognitive.gateway.file.models.File;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileCount;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFieldDetailFromClasssifier;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.Summary;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.gateway.file.models.VisionBot;
import com.automationanywhere.cognitive.id.string.StringId;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import org.testng.annotations.Test;

public class FileDtoMapperTest {
  private final FileDtoMapper mapper = new FileDtoMapper();
  private static final Date date = new Date();
  private static final String username = "test";

  @Test
  public void mapCategoriesShouldMapModelToDto() {
    // given
    CategoriesDto d1 = new CategoriesDto(
        1, 2, "s", Collections.singletonList(new CategoryDto(
            new StringId("1"), "nm", new CategoryDetailDto(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(new FieldDetailDto(
                  "f1", BigDecimal.TEN
              ))
            ), new VisionBotDto(
                new StringId("15"), "nm2", "cs", true, "lu",
                username, date.toString()
            ), 67, Collections.singletonList(new FileDto(
                new StringId("f1"), "nmF", "l", "f", true
            )),
            new FileCountDto(17, 89, 99),
            new FileCountDto(177, 189, 999)
        ))
    );

    CategoriesDto d2 = new CategoriesDto(
        1, 2, "s", Collections.singletonList(new CategoryDto(
            new StringId("1"), "nm", new CategoryDetailDto(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(null)
            ), null, 67, Collections.singletonList(null),
            null,
            null
        ))
    );

    Categories m1 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", new CategoryDetail(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(new FieldDetail(
                  "f1", BigDecimal.TEN
              ))
            ), new VisionBot(
                new StringId("15"), "nm2", "cs", true, "lu",
                username, date.toString()
            ), 67, Collections.singletonList(new File(
                new StringId("f1"), "nmF", "l", "f", true
            )),
            new FileCount(17, 89, 99),
            new FileCount(177, 189, 999)
        ))
    );

    Categories m2 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", new CategoryDetail(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(null)
            ), null, 67, Collections.singletonList(null),
            null,
            null
        ))
    );

    // when
    CategoriesDto r1 = mapper.mapCategories(m1);
    CategoriesDto r2 = mapper.mapCategories(m2);
    CategoriesDto r3 = mapper.mapCategories(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapCategoriesShouldMapModelToDto2() {
    // given
    CategoriesDto d1 = new CategoriesDto(
        1, 2, "s", Collections.singletonList(new CategoryDto(
            new StringId("1"), "nm", new CategoryDetailDto(
              new StringId("id"), 2, 3, 4,
              null
            ), null, 67, null,
            null,
            null
        ))
    );

    CategoriesDto d2 = new CategoriesDto(
        1, 2, "s", Collections.singletonList(new CategoryDto(
            new StringId("1"), "nm", null, null, 67, null,
            null,
            null
        ))
    );

    CategoriesDto d3 = new CategoriesDto(
        1, 2, "s", Collections.singletonList(null)
    );

    CategoriesDto d4 = new CategoriesDto(
        1, 2, "s", null
    );

    Categories m1 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", new CategoryDetail(
              new StringId("id"), 2, 3, 4,
              null
            ), null, 67, null,
            null,
            null
        ))
    );

    Categories m2 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", null, null, 67, null,
            null,
            null
        ))
    );

    Categories m3 = new Categories(
        1, 2, "s", Collections.singletonList(null)
    );

    Categories m4 = new Categories(
        1, 2, "s", null
    );

    // when
    CategoriesDto r1 = mapper.mapCategories(m1);
    CategoriesDto r2 = mapper.mapCategories(m2);
    CategoriesDto r3 = mapper.mapCategories(m3);
    CategoriesDto r4 = mapper.mapCategories(m4);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isEqualTo(d3);
    assertThat(r4).isEqualTo(d4);
  }

  @Test
  public void mapFileDetailsShouldMapModelToDto() {
    // given
    FileDetailsDto d1 = new FileDetailsDto(
        new StringId("f1"), new StringId("p2"), "fn", "fl", 8, 9, 10,
        "for", true, new StringId("cid"), new StringId("uid"), new StringId("lid"), "prod"
    );

    FileDetails m1 = new FileDetails(
        new StringId("f1"), new StringId("p2"), "fn", "fl", 8, 9, 10,
        "for", true, new StringId("cid"), new StringId("uid"), new StringId("lid"), "prod"
    );


    // when
    FileDetailsDto r1 = mapper.mapFileDetails(m1);
    FileDetailsDto r2 = mapper.mapFileDetails(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapCountStatisticsShouldMapModelToDto() {
    // given
    CountStatisticsDto d1 = new CountStatisticsDto(
        new StringId("1"), new StringId("2"), 1,
        new EnvironmentCountStatisticsDto(12, 89),
        new EnvironmentCountStatisticsDto(112, 899)
    );

    CountStatisticsDto d2 = new CountStatisticsDto(
        new StringId("1"), new StringId("2"), 1,
        null,
        null
    );

    CountStatistics m1 = new CountStatistics(
        new StringId("1"), new StringId("2"), 1,
        new EnvironmentCountStatistics(12, 89),
        new EnvironmentCountStatistics(112, 899)
    );

    CountStatistics m2 = new CountStatistics(
        new StringId("1"), new StringId("2"), 1,
        null,
        null
    );

    // when
    CountStatisticsDto r1 = mapper.mapCountStatistics(m1);
    CountStatisticsDto r2 = mapper.mapCountStatistics(m2);
    CountStatisticsDto r3 = mapper.mapCountStatistics(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapFilePatchDetailsShouldMapDtoToModel() {
    // given
    FilePatchDetailDto d1 = new FilePatchDetailDto(
        "a", "b", "c"
    );

    FilePatchDetail m1 = new FilePatchDetail(
        "a", "b", "c"
    );

    // when
    FilePatchDetail r1 = mapper.mapFilePatchDetails(d1);
    FilePatchDetail r2 = mapper.mapFilePatchDetails(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFileBlobsShouldMapModelToDto() {
    // given
    FileBlobsDto d1 = new FileBlobsDto(
        "a", "b"
    );

    FileBlobs m1 = new FileBlobs(
        "a", "b"
    );

    // when
    FileBlobsDto r1 = mapper.mapFileBlobs(m1);
    FileBlobsDto r2 = mapper.mapFileBlobs(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapClassificationAnalysisReportShouldMapModelToDto() {
    // given
    ClassificationAnalysisReportDto d1 = new ClassificationAnalysisReportDto(
        new StringId("a"), "b", Collections.singletonList(null)
    );

    ClassificationAnalysisReportDto d2 = new ClassificationAnalysisReportDto(
        new StringId("a"), "b", null
    );

    ClassificationAnalysisReport m1 = new ClassificationAnalysisReport(
        new StringId("a"), "b", Collections.singletonList(null)
    );

    ClassificationAnalysisReport m2 = new ClassificationAnalysisReport(
        new StringId("a"), "b", null
    );

    // when
    ClassificationAnalysisReportDto r1 = mapper.mapClassificationAnalysisReport(m1);
    ClassificationAnalysisReportDto r2 = mapper.mapClassificationAnalysisReport(m2);
    ClassificationAnalysisReportDto r3 = mapper.mapClassificationAnalysisReport(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapUploadResponseShouldMapModelToDto() {
    // given
    UploadResponseDto d1 = new UploadResponseDto("k");

    UploadResponse m1 = new UploadResponse("k");

    // when
    UploadResponseDto r1 = mapper.mapUploadResponse(m1);
    UploadResponseDto r2 = mapper.mapUploadResponse(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFileStatusShouldMapModelToDto() {
    // given
    FileStatusDto d1 = new FileStatusDto(
        new StringId("k"), "v", 1, 2, 3, true
    );

    FileStatus m1 = new FileStatus(
        new StringId("k"), "v", 1, 2, 3, true
    );

    // when
    FileStatusDto r1 = mapper.mapFileStatus(m1);
    FileStatusDto r2 = mapper.mapFileStatus(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapLearningInstanceSummaryShouldMapDataToModel() {
    // given
    LearningInstanceSummaryDto d1 = new LearningInstanceSummaryDto(
        new SummaryDto(1, 2, 3),
        new SummaryDto(1, 2, 3)
    );

    LearningInstanceSummaryDto d2 = new LearningInstanceSummaryDto(
        null,
        null
    );

    LearningInstanceSummary m1 = new LearningInstanceSummary(
        new Summary(1, 2, 3),
        new Summary(1, 2, 3)
    );

    LearningInstanceSummary m2 = new LearningInstanceSummary(
        null,
        null
    );

    // when
    LearningInstanceSummaryDto r1 = mapper.mapLearningInstanceSummary(m1);
    LearningInstanceSummaryDto r2 = mapper.mapLearningInstanceSummary(m2);
    LearningInstanceSummaryDto r3 = mapper.mapLearningInstanceSummary(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapRawFileReportFromClassifierDataShouldMapDtoToModel() {
    // given
    RawFileReportFromClassifierDto d1 = new RawFileReportFromClassifierDto(
        new StringId("1"),
        Collections.singletonList(new RawFieldDetailFromClassifierDto(
            new StringId("2"), "nm1", true
        ))
    );

    RawFileReportFromClassifierDto d2 = new RawFileReportFromClassifierDto(
        new StringId("1"),
        Collections.singletonList(null)
    );

    RawFileReportFromClassifierDto d3 = new RawFileReportFromClassifierDto(
        new StringId("1"),
        null
    );

    RawFileReportFromClassifier m1 = new RawFileReportFromClassifier(
        new StringId("1"),
        Collections.singletonList(new RawFieldDetailFromClasssifier(
            new StringId("2"), "nm1", true
        ))
    );

    RawFileReportFromClassifier m2 = new RawFileReportFromClassifier(
        new StringId("1"),
        Collections.singletonList(null)
    );

    RawFileReportFromClassifier m3 = new RawFileReportFromClassifier(
        new StringId("1"), Collections.emptyList()
    );

    // when
    RawFileReportFromClassifier r1 = mapper
        .mapRawFileReportFromClassifierData(d1);
    RawFileReportFromClassifier r2 = mapper.mapRawFileReportFromClassifierData(d2);
    RawFileReportFromClassifier r3 = mapper.mapRawFileReportFromClassifierData(d3);
    RawFileReportFromClassifier r4 = mapper.mapRawFileReportFromClassifierData(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isEqualTo(m3);
    assertThat(r4).isNull();
  }
}
