package com.automationanywhere.cognitive.gateway.application.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.application.ApplicationService;
import com.automationanywhere.cognitive.gateway.application.microservices.application.AcknowledgementData;
import com.automationanywhere.cognitive.gateway.application.microservices.application.ApplicationDataMapper;
import com.automationanywhere.cognitive.gateway.application.microservices.application.ApplicationMicroservice;
import com.automationanywhere.cognitive.gateway.application.microservices.application.ConfigurationData;
import com.automationanywhere.cognitive.gateway.application.microservices.application.HandshakeData;
import com.automationanywhere.cognitive.gateway.application.microservices.application.RegistrationData;
import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.core.ParameterizedTypeReference;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ApplicationControllerTest {

  private ApplicationConfiguration cfg;
  private ApplicationDataMapper datam;
  private ApplicationDtoMapper dtom;
  private ApplicationController c;
  private RestClient rc;

  @BeforeMethod
  public void setUp() throws Exception {
    cfg = mock(ApplicationConfiguration.class);
    rc = mock(RestClient.class);
    datam = spy(new ApplicationDataMapper());
    dtom = spy(new ApplicationDtoMapper());
    c = new ApplicationController(
        new ApplicationService(new ApplicationMicroservice(datam, rc, cfg)), dtom
    );
  }

  @Test
  public void testDoHandshake() throws Exception {
    // given

    doReturn("url").when(cfg).applicationServiceURL();
    doReturn("req").when(cfg).handshake();

    RegistrationDto inDto = mock(RegistrationDto.class);
    Registration inModel = mock(Registration.class);
    RegistrationData inData = mock(RegistrationData.class);

    HandshakeData outData = mock(HandshakeData.class);
    Handshake outModel = mock(Handshake.class);
    HandshakeDto outDto = mock(HandshakeDto.class);

    RequestDetails rd = Builder.post("url")
        .request("req")
        .content(inData, Types.JSON)
        .accept(Types.JSON)
        .build();

    CompletableFuture<HandshakeData> future =
        CompletableFuture.completedFuture(outData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<HandshakeData>() {
        }
    );

    doReturn(inModel).when(dtom).mapRegistration(inDto);
    doReturn(inData).when(datam).mapRegistration(inModel);
    doReturn(outModel).when(datam).mapHandshake(outData);
    doReturn(outDto).when(dtom).mapHandshake(outModel);

    // when

    CompletableFuture<HandshakeDto> f = c.doHandshake(inDto);

    HandshakeDto r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r).isSameAs(outDto);
  }

  @Test
  public void testAcknowledge() throws Exception {
    // given

    doReturn("url").when(cfg).applicationServiceURL();
    doReturn("req").when(cfg).acknowledge();

    AcknowledgementDto inDto = mock(AcknowledgementDto.class);
    Acknowledgement inModel = mock(Acknowledgement.class);
    AcknowledgementData inData = mock(AcknowledgementData.class);

    RequestDetails rd = Builder.post("url")
        .request("req")
        .content(inData, Types.JSON)
        .build();

    CompletableFuture<Void> future = CompletableFuture.completedFuture(null);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<String>() {
        }
    );

    doReturn(inModel).when(dtom).mapAcknowledgement(inDto);
    doReturn(inData).when(datam).mapAcknowledgement(inModel);

    // when

    CompletableFuture<Void> f = c.acknowledge(inDto);

    Void r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r).isNull();
  }

  @Test
  public void testRetrieveConfiguration() throws Exception {
    // given

    doReturn("url").when(cfg).applicationServiceURL();
    doReturn("req").when(cfg).configuration();

    ConfigurationData outData = mock(ConfigurationData.class);
    Configuration outModel = mock(Configuration.class);
    ConfigurationDto outDto = mock(ConfigurationDto.class);

    RequestDetails rd = Builder.get("url")
        .request("req")
        .accept(Types.JSON)
        .build();

    CompletableFuture<ConfigurationData> future =
        CompletableFuture.completedFuture(outData);

    doReturn(future).when(rc).send(
        rd, new ParameterizedTypeReference<ConfigurationData>() {
        }
    );

    doReturn(outModel).when(datam).mapConfiguration(outData);
    doReturn(outDto).when(dtom).mapConfiguration(outModel);

    // when

    CompletableFuture<ConfigurationDto> f = c.retrieveConfiguration();

    ConfigurationDto r = f.get(1000, TimeUnit.MILLISECONDS);

    // then

    assertThat(f.isDone()).isTrue();
    assertThat(r).isSameAs(outDto);
  }
}
