package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.auth.models.KeyTokenAuthorization;
import org.testng.annotations.Test;

public class KeyTokenAuthorizationTest {
  @Test
  public void testEquals() {
    KeyTokenAuthorization kta1 = new KeyTokenAuthorization("ck", "ct");
    KeyTokenAuthorization kta2 = new KeyTokenAuthorization("ck", "ct");
    KeyTokenAuthorization kta3 = new KeyTokenAuthorization("cka", "ct");
    KeyTokenAuthorization kta4 = new KeyTokenAuthorization("ck", "cta");

    assertThat(kta1.equals(kta1)).isTrue();
    assertThat(kta1.equals(kta2)).isTrue();
    assertThat(kta1.equals(kta3)).isFalse();
    assertThat(kta1.equals(kta4)).isFalse();
    assertThat(kta1.equals(null)).isFalse();
    assertThat(kta1.equals(new Object())).isFalse();
  }
}
