package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.net.URI;
import java.util.Arrays;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class StandardHeaderRestTemplateTest {

  private RestTemplate d;
  private StandardHeaderRestTemplate t;

  @BeforeMethod
  public void setUp() throws Exception {
    d = mock(RestTemplate.class);
    t = Mockito.spy(new StandardHeaderRestTemplate(d));
  }

  @Test
  public void testHttpEntityCallback() throws Exception {
    Object body = new Object();

    HttpHeaders headers = new HttpHeaders();
    headers.put("a", Arrays.asList("b", "c"));

    HttpHeaders h = new HttpHeaders();

    RequestEntity<Object> re = new RequestEntity<>(
        body, headers, HttpMethod.POST, new URI("file:///")
    );

    RequestEntity<?> nre = mock(RequestEntity.class);
    RequestCallback r = mock(RequestCallback.class);

    doReturn(h).when(t).createHttpHeaders();
    doReturn(nre).when(t).createRequestEntity(h, re);
    doReturn(r).when(t).createRequestCallback(nre, null);

    assertThat(t.httpEntityCallback(re)).isSameAs(r);
  }

  @Test
  public void testHttpEntityCallbackWithObjectBody() throws Exception {
    Object re = new Object();
    HttpHeaders h = new HttpHeaders();
    RequestCallback r = mock(RequestCallback.class);
    RequestEntity<?> nre = mock(RequestEntity.class);

    doReturn(h).when(t).createHttpHeaders();
    doReturn(nre).when(t).createHttpEntity(re, h);
    doReturn(r).when(t).createRequestCallback(nre, null);

    assertThat(t.httpEntityCallback(re)).isSameAs(r);
  }

  @Test
  public void testHttpEntityCallbackWithNullBody() throws Exception {
    HttpHeaders h = new HttpHeaders();
    RequestCallback r = mock(RequestCallback.class);
    RequestEntity<?> nre = mock(RequestEntity.class);

    doReturn(h).when(t).createHttpHeaders();
    doReturn(nre).when(t).createHttpEntity(null, h);
    doReturn(r).when(t).createRequestCallback(nre, null);

    assertThat(t.httpEntityCallback(null)).isSameAs(r);
  }

  @Test
  public void testCreateRequestEntity() throws Exception {
    Object o = new Object();
    URI u = new URI("file:///");
    RequestEntity<Object> re = new RequestEntity<>(o, HttpMethod.GET, u);
    HttpHeaders h = new HttpHeaders();
    h.put("h", Arrays.asList("v1", "v2"));

    RequestEntity<?> e = t.createRequestEntity(h, re);
    assertThat(e.getBody()).isSameAs(o);
    assertThat(e.getMethod()).isSameAs(HttpMethod.GET);
    assertThat(e.getUrl()).isSameAs(u);
    assertThat(e.getHeaders()).isEqualTo(h);
  }

  @Test
  public void testAcceptHeaderRequestCallback() throws Exception {
    ClientHttpRequest req = mock(ClientHttpRequest.class);
    HttpHeaders hs = new HttpHeaders();
    hs.put("h", Arrays.asList("v1", "v2"));

    doReturn(hs).when(req).getHeaders();

    t.acceptHeaderRequestCallback(null).doWithRequest(req);

    verify(t).addHeaders(hs);
  }
}
