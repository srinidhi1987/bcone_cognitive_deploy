package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringId;
import java.util.Arrays;
import org.testng.annotations.Test;

public class AuthenticationTest {

  @Test
  public void testEquals() {
    Id id1 = new StringId("id");
    Id id2 = new StringId("id");
    Id id3 = new StringId("aid");
    UserDto u1 = new UserDto(id1, "nm1", Arrays.asList("e1", "e2"));
    UserDto u2 = new UserDto(id2, "nm1", Arrays.asList("e1", "e2"));
    UserDto u3 = new UserDto(id3, "nm1", Arrays.asList("e1", "e2"));
    UserDto u4 = new UserDto(id2, "nm2", Arrays.asList("e1", "e2"));
    UserDto u5 = new UserDto(id2, "nm1", Arrays.asList("e1"));
    AuthenticationDto d1 = new AuthenticationDto(u1, "tk1");
    AuthenticationDto d2 = new AuthenticationDto(u2, "tk1");
    AuthenticationDto d3 = new AuthenticationDto(u2, "atk");
    AuthenticationDto d4 = new AuthenticationDto(u3, "tk1");

    assertThat(d1.equals(d1)).isTrue();
    assertThat(d1.equals(d2)).isTrue();
    assertThat(d1.equals(d3)).isFalse();
    assertThat(d1.equals(d4)).isFalse();
    assertThat(d1.equals(null)).isFalse();
    assertThat(d1.equals(new Object())).isFalse();

    assertThat(u1.equals(u1)).isTrue();
    assertThat(u1.equals(u2)).isTrue();
    assertThat(u1.equals(u3)).isFalse();
    assertThat(u1.equals(u4)).isFalse();
    assertThat(u1.equals(u5)).isFalse();
    assertThat(u1.equals(null)).isFalse();
    assertThat(u1.equals(new Object())).isFalse();
  }
}
