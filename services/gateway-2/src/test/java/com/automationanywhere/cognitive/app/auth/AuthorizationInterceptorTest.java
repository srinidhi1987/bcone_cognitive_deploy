package com.automationanywhere.cognitive.app.auth;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.BaseControllerTest;
import com.automationanywhere.cognitive.auth.AuthorizationSupport;
import com.automationanywhere.cognitive.auth.ForbiddenException;
import com.automationanywhere.cognitive.gateway.auth.microservices.auth.AuthMicroservice;
import com.automationanywhere.cognitive.gateway.auth.models.JWTAuthorization;
import com.automationanywhere.cognitive.gateway.auth.models.KeyTokenAuthorization;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.lang.reflect.Method;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.method.HandlerMethod;
import org.testng.annotations.Test;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class AuthorizationInterceptorTest extends BaseControllerTest {

  @MockBean
  @Autowired
  private AuthMicroservice microservice;

  @Autowired
  private AuthorizationSupport authorizationSupport;

  @Test
  public void testAuthorizationWithTokenOnClass() {
    // GIVEN
    JWTAuthorization a = new JWTAuthorization("/test/:id", "SUCCESS_A");
    when(microservice.authorize(a)).thenReturn(null);

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("Bearer SUCCESS_A").when(req).getHeader(HttpHeaders.AUTHORIZATION);

    TestControllerClass testController = new TestControllerClass();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void testAuthorizationWithTokenOnMethod() {
    // GIVEN
    JWTAuthorization a = new JWTAuthorization("/test/:id", "SUCCESS_B");
    when(microservice.authorize(a)).thenReturn(null);

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("Bearer SUCCESS_B").when(req).getHeader(HttpHeaders.AUTHORIZATION);

    TestControllerMethod testController = new TestControllerMethod();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void testAuthorizationWithClientKeyOnClass() {
    // GIVEN
    KeyTokenAuthorization k = new KeyTokenAuthorization("CLIENT_KEY", "SUCCESS_C");
    when(microservice.authorize(k)).thenReturn(null);

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("CLIENT_KEY").when(req).getHeader(CognitivePlatformHeaders.CLIENT_KEY.headerName);
    doReturn("SUCCESS_C").when(req).getHeader(CognitivePlatformHeaders.CLIENT_TOKEN.headerName);

    TestControllerClass testController = new TestControllerClass();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void testAuthorizationWithClientKeyOnMethod() {
    // GIVEN
    KeyTokenAuthorization k = new KeyTokenAuthorization("CLIENT_KEY", "SUCCESS_D");
    when(microservice.authorize(k)).thenReturn(null);

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("CLIENT_KEY").when(req).getHeader(CognitivePlatformHeaders.CLIENT_KEY.headerName);
    doReturn("SUCCESS_D").when(req).getHeader(CognitivePlatformHeaders.CLIENT_TOKEN.headerName);

    TestControllerMethod testController = new TestControllerMethod();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  // Temporarily excluded for testing iq bot lite command without authorization
  @Test(enabled = false)
  public void testAuthorizationFailWithTokenOnClass() {
    // GIVEN
    JWTAuthorization a = new JWTAuthorization("/test/:id", "FAIL_A");
    when(microservice.authorize(a)).thenThrow(new HttpServerErrorException(HttpStatus.FORBIDDEN));

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("Bearer FAIL_A").when(req).getHeader(HttpHeaders.AUTHORIZATION);

    TestControllerClass testController = new TestControllerClass();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(ExecutionException.class)
        .hasCauseInstanceOf(HttpStatusCodeException.class);
    HttpStatusCodeException cause = (HttpStatusCodeException) throwable.getCause();
    assertThat(cause.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
  }
  // Temporarily excluded for testing iq bot lite command without authorization
  @Test(enabled = false)
  public void testAuthorizationFailWithTokenOnMethod() {
    // GIVEN
    JWTAuthorization a = new JWTAuthorization("/test/:id", "FAIL_B");
    when(microservice.authorize(a)).thenThrow(new HttpServerErrorException(HttpStatus.FORBIDDEN));

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("Bearer FAIL_B").when(req).getHeader(HttpHeaders.AUTHORIZATION);

    TestControllerMethod testController = new TestControllerMethod();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(ExecutionException.class)
        .hasCauseInstanceOf(HttpStatusCodeException.class);
    HttpStatusCodeException cause = (HttpStatusCodeException) throwable.getCause();
    assertThat(cause.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
  }
  // Temporarily excluded for testing iq bot lite command without authorization
  @Test(enabled = false)
  public void testAuthorizationFailWithClientKeyOnClass() {
    // GIVEN
    KeyTokenAuthorization k = new KeyTokenAuthorization("CLIENT_KEY", "FAIL_C");
    when(microservice.authorize(k)).thenThrow(new HttpServerErrorException(HttpStatus.FORBIDDEN));

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("CLIENT_KEY").when(req).getHeader(CognitivePlatformHeaders.CLIENT_KEY.headerName);
    doReturn("FAIL_C").when(req).getHeader(CognitivePlatformHeaders.CLIENT_TOKEN.headerName);

    TestControllerClass testController = new TestControllerClass();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(ExecutionException.class)
        .hasCauseInstanceOf(HttpStatusCodeException.class);
    HttpStatusCodeException cause = (HttpStatusCodeException) throwable.getCause();
    assertThat(cause.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
  }
  // Temporarily excluded for testing iq bot lite command without authorization
  @Test(enabled = false)
  public void testAuthorizationFailWithClientKeyOnMethod() {
    // GIVEN
    KeyTokenAuthorization k = new KeyTokenAuthorization("CLIENT_KEY", "FAIL_D");
    when(microservice.authorize(k)).thenThrow(new HttpServerErrorException(HttpStatus.FORBIDDEN));

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("CLIENT_KEY").when(req).getHeader(CognitivePlatformHeaders.CLIENT_KEY.headerName);
    doReturn("FAIL_D").when(req).getHeader(CognitivePlatformHeaders.CLIENT_TOKEN.headerName);

    TestControllerMethod testController = new TestControllerMethod();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(ExecutionException.class)
        .hasCauseInstanceOf(HttpStatusCodeException.class);
    HttpStatusCodeException cause = (HttpStatusCodeException) throwable.getCause();
    assertThat(cause.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
  }
  // Temporarily excluded for testing iq bot lite command without authorization
  @Test(enabled = false)
  public void testAuthorizationTimeout() {
    // GIVEN
    KeyTokenAuthorization k = new KeyTokenAuthorization(null, "TIMEOUT_A");
    when(microservice.authorize(k)).thenThrow(new RuntimeException());

    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();

    doReturn("TIMEOUT_A").when(req).getHeader(CognitivePlatformHeaders.CLIENT_TOKEN.headerName);

    TestControllerMethod testController = new TestControllerMethod();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(ExecutionException.class)
        .hasCauseInstanceOf(RuntimeException.class);
  }
  // Temporarily excluded for testing iq bot lite command without authorization
  @Test(enabled = false)
  public void testAuthorizationFailWithInvalidToken() {
    // GIVEN
    AuthorizationInterceptor ai = new AuthorizationInterceptor(authorizationSupport);

    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();

    doReturn("FAIL_E").when(req).getHeader(HttpHeaders.AUTHORIZATION);

    TestControllerMethod testController = new TestControllerMethod();
    Method method = testController.getClass().getMethods()[0];
    HandlerMethod handlerMethod = new HandlerMethod(testController, method);

    // WHEN
    Throwable throwable = catchThrowable(() -> ai.preHandle(req, resp, handlerMethod));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(ForbiddenException.class)
        .hasNoCause()
        .hasMessage("Invalid authorization header: FAIL_E");
  }

  @RequestMapping(path = "/test/{id}")
  private class TestControllerClass {

    public CompletableFuture<Void> test() {
      return CompletableFuture.completedFuture(null);
    }
  }

  private class TestControllerMethod {

    @RequestMapping(path = "/test/{id}")
    public CompletableFuture<Void> test() {
      return CompletableFuture.completedFuture(null);
    }
  }
}