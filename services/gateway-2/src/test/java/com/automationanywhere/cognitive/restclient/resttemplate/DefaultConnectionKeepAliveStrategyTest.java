package com.automationanywhere.cognitive.restclient.resttemplate;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultConnectionKeepAliveStrategyTest {
  @Test
  public void testGetKeepAliveDuration() {
    DefaultConnectionKeepAliveStrategy s = new DefaultConnectionKeepAliveStrategy(100);
    assertThat(s.getKeepAliveDuration(null, null)).isEqualTo(100);
  }
}
