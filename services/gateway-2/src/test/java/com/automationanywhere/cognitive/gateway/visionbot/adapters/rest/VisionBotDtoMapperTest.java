package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.StagingFilesSummary;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorProperties;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorReviewDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotCount;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotRunDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.id.string.StringId;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.Test;

public class VisionBotDtoMapperTest {

  private final VisionBotDtoMapper mapper = new VisionBotDtoMapper();

  @Test
  public void mapVisionBotShouldMapModelToDto() {
    // given
    VisionBotDto d1 = new VisionBotDto(
        new StringId("1"), "d", "v"
    );

    VisionBot m1 = new VisionBot(
        new StringId("1"), "d", "v"
    );

    // when
    VisionBotDto r1 = mapper.mapVisionBot(m1);
    VisionBotDto r2 = mapper.mapVisionBot((VisionBot) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotShouldMapDtoToModel() {
    // given
    VisionBotDto d1 = new VisionBotDto(
        new StringId("1"), "d", "v"
    );

    VisionBot m1 = new VisionBot(
        new StringId("1"), "d", "v"
    );

    // when
    VisionBot r1 = mapper.mapVisionBot(d1);
    VisionBot r2 = mapper.mapVisionBot((VisionBotDto) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotStateShouldMapDtoToModel() {
    // given
    VisionBotStateDto d1 = new VisionBotStateDto("data");

    VisionBotState m1 = new VisionBotState("data");

    // when
    VisionBotState r1 = mapper.mapVisionBotState(d1);
    VisionBotState r2 = mapper.mapVisionBotState(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotDetailShouldMapDtoToModel() {
    // given
    VisionBotElementDto d1 = new VisionBotElementDto("data" , new ArrayList<>());

    VisionBotElements m1 = new VisionBotElements("data", new ArrayList<>());

    // when
    VisionBotElements r1 = mapper.mapVisionBotStateDetails(d1);
    VisionBotElements r2 = mapper.mapVisionBotStateDetails(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapVisionBotResponseDtoShouldMapDtoToVisionBotResponse() {
    // given
    VisionBotResponse d1 = new VisionBotResponse(true, "data" , "error");

    VisionBotResponseDto m1 = new VisionBotResponseDto(true, "data" , "error");

    // when
    VisionBotResponseDto r1 = mapper.mapVisionBotResponseDto(d1);
    VisionBotResponseDto r2 = mapper.mapVisionBotResponseDto(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFieldValueDefinitionShouldMapDtoToModel() {
    // given
    FieldValueDefinitionDto d1 = new FieldValueDefinitionDto(
        new StringId("a"), new StringId("b"), "l", "w", "e",
        "exp", "dv", true, "b", "vb",
        1, 2, 3.0, 4, 5, true, 6, "idc"
    );
    FieldValueDefinition m1 = new FieldValueDefinition(
        new StringId("a"), new StringId("b"), "l", "w", "e",
        "exp", "dv", true, "b", "vb",
        1, 2, 3.0, 4, 5, true, 6, "idc"
    );

    // when
    FieldValueDefinition r1 = mapper.mapFieldValueDefinition(d1);
    FieldValueDefinition r2 = mapper.mapFieldValueDefinition(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapValidationDetailsShouldMapModelToDto() {
    // given
    ValidationDetailsDto d1 = new ValidationDetailsDto(
        Collections.singletonList(new ValidatorDetailsDto(
            new StringId("1"),
            new ValidatorPropertiesDto("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );
    ValidationDetails m1 = new ValidationDetails(
        Collections.singletonList(new ValidatorDetails(
            new StringId("1"),
            new ValidatorProperties("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );

    // when
    ValidationDetailsDto r1 = mapper.mapValidationDetails(m1);
    ValidationDetailsDto r2 = mapper.mapValidationDetails((ValidationDetails) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapValidationDetailsShouldMapDtoToModel() {
    // given
    ValidationDetailsDto d1 = new ValidationDetailsDto(
        Collections.singletonList(new ValidatorDetailsDto(
            new StringId("1"),
            new ValidatorPropertiesDto("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );
    ValidationDetails m1 = new ValidationDetails(
        Collections.singletonList(new ValidatorDetails(
            new StringId("1"),
            new ValidatorProperties("fp", Collections.singletonList("a")),
            "tod", "sd"
        ))
    );

    // when
    ValidationDetails r1 = mapper.mapValidationDetails(d1);
    ValidationDetails r2 = mapper.mapValidationDetails((ValidationDetailsDto) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProcessedDocumentDetailsShouldMapModelToDto() {
    // given
    ProcessedDocumentDetailsDto d1 = new ProcessedDocumentDetailsDto(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );
    ProcessedDocumentDetails m1 = new ProcessedDocumentDetails(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );

    // when
    ProcessedDocumentDetailsDto r1 = mapper.mapProcessedDocumentDetails(m1);
    ProcessedDocumentDetailsDto r2 = mapper
        .mapProcessedDocumentDetails((ProcessedDocumentDetails) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }


  @Test
  public void mapProcessedDocumentDetailsShouldMapDtoToModel() {
    // given
    ProcessedDocumentDetailsDto d1 = new ProcessedDocumentDetailsDto(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );
    ProcessedDocumentDetails m1 = new ProcessedDocumentDetails(
        new StringId("1"), new StringId("2"),
        "st", "tt", "ff", "pf"
    );

    // when
    ProcessedDocumentDetails r1 = mapper.mapProcessedDocumentDetails(d1);
    ProcessedDocumentDetails r2 = mapper
        .mapProcessedDocumentDetails((ProcessedDocumentDetailsDto) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapTestSetShouldMapModelToDto() {
    // given
    TestSetDto d1 = new TestSetDto(new StringId("1"), "nm", "i");
    TestSet m1 = new TestSet(new StringId("1"), "nm", "i");

    // when
    TestSetDto r1 = mapper.mapTestSet(m1);
    TestSetDto r2 = mapper.mapTestSet((TestSet) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapTestSetShouldMapDtoToModel() {
    // given
    TestSetDto d1 = new TestSetDto(new StringId("1"), "nm", "i");
    TestSet m1 = new TestSet(new StringId("1"), "nm", "i");

    // when
    TestSet r1 = mapper.mapTestSet(d1);
    TestSet r2 = mapper.mapTestSet((TestSetDto) null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapSummaryModelToDto() {
    // given
    SummaryDto d1 = new SummaryDto(
        new StagingFilesSummaryDto(
            1, 2, 3, 4, 5
        ), new VisionBotCountDto(
        1, 2, 3
    )
    );
    SummaryDto d2 = new SummaryDto(null, null);

    Summary m1 = new Summary(
        new StagingFilesSummary(
            1, 2, 3, 4, 5
        ), new VisionBotCount(
        1, 2, 3
    )
    );
    Summary m2 = new Summary(null, null);

    // when
    SummaryDto r1 = mapper.mapSummary(m1);
    SummaryDto r2 = mapper.mapSummary(m2);
    SummaryDto r3 = mapper.mapSummary(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapVisionBotDetailsShouldMapModelToDto() {
    // given
    VisionBotDetailsDto d1 = new VisionBotDetailsDto(
        new StringId("1"), "nm",
        new StringId("2"), new StringId("2"), "pn", new StringId("3"), "cn",
        "e", "s", false, new StringId("3"),
        new VisionBotRunDetailsDto(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            new ValidatorReviewDetailsDto(
                "tfr", "tfmi", "tff", "art"
            )
        ),
        new VisionBotRunDetailsDto(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            null
        ),
        null
    );

    VisionBotDetails m1 = new VisionBotDetails(
        new StringId("1"), "nm",
        new StringId("2"), new StringId("2"), "pn", new StringId("3"), "cn",
        "e", "s", false, new StringId("3"),
        new VisionBotRunDetails(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            new ValidatorReviewDetails(
                "tfr", "tfmi", "tff", "art"
            )
        ),
        new VisionBotRunDetails(
            new StringId("1"), "st", "et", "rm", "tf",
            "pdc", "fdc", "pdc",
            "dpa", "fa",
            null
        ),
        null
    );

    // when
    VisionBotDetailsDto r1 = mapper.mapVisionBotDetails(m1);
    VisionBotDetailsDto r2 = mapper.mapVisionBotDetails(m1);

    // then
    assertThat(r1).isEqualTo(d1);
  }

  @Test
  public void mapJsonPatchDocumentAdd() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.ADD, "/abc", v1, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionAdd("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentReplace() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.REPLACE, "/abc", v1, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionReplace("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentRemove() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.REMOVE, "/abc", null, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionRemove("/abc"));
  }

  @Test
  public void mapJsonPatchDocumentTest() {
    // GIVEN
    TextNode v1 = new TextNode("123");
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.TEST, "/abc", v1, null)
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionTest("/abc", v1));
  }

  @Test
  public void mapJsonPatchDocumentCopy() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.COPY, "/abc", null, "/def")
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionCopy("/abc", "/def"));
  }

  @Test
  public void mapJsonPatchDocumentMove() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(JsonPatchActionOperation.MOVE, "/abc", null, "/def")
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(1);
    assertThat(doc.actions.get(0)).isEqualTo(new JsonPatchActionMove("/abc", "/def"));
  }

  @Test
  public void mapJsonPatchDocumentNullOperation() {
    // GIVEN
    List<JsonPatchActionDto> actions = Collections.singletonList(
        new JsonPatchActionDto(null, "/abc", null, "/def")
    );

    // WHEN
    JsonPatchDocument doc = mapper.mapJsonPatchDocument(actions);

    // THEN
    assertThat(doc).isNotNull();
    assertThat(doc.actions).isNotNull().hasSize(0);
  }
}
