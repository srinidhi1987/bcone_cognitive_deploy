package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.automationanywhere.cognitive.BaseControllerTest;
import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.auth.AuthorizationSupport;
import com.automationanywhere.cognitive.gateway.auth.microservices.auth.AuthenticationData;
import com.automationanywhere.cognitive.gateway.auth.microservices.auth.UserData;
import com.automationanywhere.cognitive.gateway.auth.models.Credentials;
import com.automationanywhere.cognitive.gateway.auth.models.InvalidToken;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.testng.annotations.Test;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class AuthenticationControllerTest extends BaseControllerTest {

  @MockBean
  @Autowired
  private AuthorizationSupport authorizationSupport;

  @MockBean
  @Autowired
  private RestClient restClient;

  @Autowired
  private TestRestTemplate rtpl;

  @Autowired
  private ApplicationConfiguration cfg;

  @Test
  public void testAuthenticate() throws Exception {
    // GIVEN
    AuthenticationData data = new AuthenticationData(
        new UserData(new StringId("id1"), "nm", Arrays.asList("r1", "r2")),
        "tk"
    );

    doReturn(CompletableFuture.completedFuture(data)).when(restClient).send(
        RequestDetails.Builder.post(cfg.authURL())
            .request(cfg.authenticate())
            .content(new Credentials("un", "pw"), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<AuthenticationData>() {
        }
    );

    CredentialsDto credentials = new CredentialsDto("un", "pw");

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));

    // WHEN
    ResponseEntity<StandardResponse> result = rtpl.exchange(
        "/authentication", HttpMethod.POST, new HttpEntity<>(credentials, headers),
        StandardResponse.class
    );

    // THEN
    assertThat(result.getBody().success).isTrue();
    assertThat(om.treeToValue(om.valueToTree(result.getBody().data), AuthenticationDto.class))
        .isEqualTo(new AuthenticationDto(
            new UserDto(new StringId("id1"), "nm", Arrays.asList("r1", "r2")),
            "tk"
        ));
  }

  @Test
  public void testAuthenticateFailure() throws Exception {
    // GIVEN
    CredentialsDto c = new CredentialsDto("u", "p");

    HttpHeaders headers = new HttpHeaders();
    headers.put(
        "accept",
        Collections.singletonList("application/json")
    );

    doThrow(new HttpServerErrorException(UNAUTHORIZED)).when(restClient).send(
        RequestDetails.Builder.post(cfg.authURL())
            .request(cfg.authenticate())
            .content(new Credentials("u", "p"), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<AuthenticationData>() {
        }
    );

    // WHEN
    ResponseEntity<String> resp = rtpl.exchange(
        "/authentication", HttpMethod.POST, new HttpEntity<>(c, headers),
        new ParameterizedTypeReference<String>() {
        }
    );

    // THEN
    assertThat(resp.getStatusCode().value()).isEqualTo(401);
  }

  @Test
  public void testNotFound() {
    assertThat(rtpl.getForEntity(
        "/bad-url?a=b", String.class
    ).getStatusCode().value()).isEqualTo(404);
  }

  @Test
  public void testValidation() {
    HttpHeaders headers = new HttpHeaders();
    headers.put(
        "accept",
        Collections.singletonList("application/json")
    );
    headers.put(
        "content-type",
        Collections.singletonList("application/json;charset=UTF-8")
    );

    ResponseEntity<String> resp = rtpl.exchange(
        "/authentication", HttpMethod.POST, new HttpEntity<>("{}", headers),
        new ParameterizedTypeReference<String>() {
        }
    );

    assertThat(resp.getStatusCode().value()).isEqualTo(400);
  }

  @Test
  public void logoutWithValidTokenAndUsername() {
    // GIVEN
    String token = "Bearer abc123";
    String username = "services";
    InvalidToken invalidToken = new InvalidToken(token);

    doReturn(CompletableFuture.completedFuture(new byte[0])).when(restClient).send(
        RequestDetails.Builder.get(cfg.authURL())
            .request(cfg.invalidateSession())
            .setHeader(HttpHeaders.AUTHORIZATION, invalidToken.token)
            .accept(RequestDetails.Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    );

    doReturn(CompletableFuture.completedFuture(new byte[0])).when(restClient).send(
        RequestDetails.Builder.post(cfg.validatorURL())
            .request(cfg.unlockDocuments())
            .setHeader("username", username)
            .accept(RequestDetails.Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    );

    doReturn(CompletableFuture.completedFuture(new byte[0])).when(restClient).send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.unlockVisionBots())
            .setHeader("username", username)
            .accept(RequestDetails.Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));
    headers.add(HttpHeaders.AUTHORIZATION, token);
    headers.add("username", username);

    // WHEN
    ResponseEntity<String> resp = rtpl.exchange(
        "/logout", HttpMethod.POST, new HttpEntity<>(null, headers),
        new ParameterizedTypeReference<String>() {
        }
    );

    // THEN
    assertThat(resp.getStatusCode().value()).isEqualTo(200);
    assertThat(resp.getBody().replaceAll("\n", ""))
        .isEqualToIgnoringWhitespace("{\"success\":true,\"data\":null}");
  }
}
