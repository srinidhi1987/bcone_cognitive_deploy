package com.automationanywhere.cognitive.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.errors.Mapper;
import java.util.Arrays;
import java.util.List;
import org.testng.annotations.Test;

public class BaseMapperTest {

  private BaseMapper bm = new BaseMapper() {
  };

  @Test
  public void mapListShouldMapElements() {
    // given

    Mapper<String, Integer> m = Integer::parseInt;

    List<String> l = Arrays.asList("1", "2");

    // when

    List<Integer> r = bm.mapList(l, m);

    // then

    assertThat(r).containsExactly(1, 2);
  }

  @Test
  public void mapListShouldMapNullList() {
    // given

    Mapper<String, Integer> m = Integer::parseInt;

    // when

    List<Integer> r = bm.mapList(null, m);

    // then

    assertThat(r).isEmpty();
  }
}
