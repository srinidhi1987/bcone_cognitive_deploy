package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import static com.automationanywhere.cognitive.gateway.visionbot.adapters.rest.JsonPatchActionOperation.ADD;
import static com.automationanywhere.cognitive.gateway.visionbot.adapters.rest.JsonPatchActionOperation.COPY;
import static com.automationanywhere.cognitive.gateway.visionbot.adapters.rest.JsonPatchActionOperation.REPLACE;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.node.TextNode;
import org.testng.annotations.Test;

public class JsonPatchActionDtoTest {

  @Test
  public void testJsonPatchActionDtoEquals() {
    // GIVEN
    JsonPatchActionDto dto1 = new JsonPatchActionDto(ADD, "/abc", new TextNode("123"), null);
    JsonPatchActionDto dto2 = new JsonPatchActionDto(ADD, "/abc", new TextNode("123"), null);
    JsonPatchActionDto dto3 = new JsonPatchActionDto(REPLACE, "/abc", new TextNode("123"), null);
    JsonPatchActionDto dto4 = new JsonPatchActionDto(REPLACE, "/abc", new TextNode("456"), null);
    JsonPatchActionDto dto5 = new JsonPatchActionDto(COPY, "/abc", null, "/def");
    JsonPatchActionDto dto6 = new JsonPatchActionDto(COPY, "/abc", null, "/def");
    JsonPatchActionDto dto7 = new JsonPatchActionDto(COPY, "/abc", null, "/ghi");
    JsonPatchActionDto dto8 = new JsonPatchActionDto(COPY, "/def", null, "/ghi");

    // WHEN
    boolean equals = dto1.equals(dto2);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = dto1.equals(dto3);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = dto3.equals(dto4);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = dto5.equals(dto6);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = dto6.equals(dto7);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = dto7.equals(dto8);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = dto1.equals(null);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = dto1.equals(dto1);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = dto1.equals(new Object());

    // THEN
    assertThat(equals).isFalse();
  }

  @Test
  public void testJsonPatchActionDtoHashCode() {
    // GIVEN
    JsonPatchActionDto dto1 = new JsonPatchActionDto(ADD, "/abc", new TextNode("123"), null);
    JsonPatchActionDto dto2 = new JsonPatchActionDto(ADD, "/abc", new TextNode("123"), null);
    JsonPatchActionDto dto3 = new JsonPatchActionDto(REPLACE, "/abc", new TextNode("123"), null);
    JsonPatchActionDto dto4 = new JsonPatchActionDto(REPLACE, "/abc", new TextNode("456"), null);
    JsonPatchActionDto dto5 = new JsonPatchActionDto(COPY, "/abc", null, "/def");
    JsonPatchActionDto dto6 = new JsonPatchActionDto(COPY, "/abc", null, "/def");
    JsonPatchActionDto dto7 = new JsonPatchActionDto(COPY, "/abc", null, "/ghi");

    // WHEN
    int dto1Hash = dto1.hashCode();
    int dto2Hash = dto2.hashCode();
    int dto3Hash = dto3.hashCode();
    int dto4Hash = dto4.hashCode();
    int dto5Hash = dto5.hashCode();
    int dto6Hash = dto6.hashCode();
    int dto7Hash = dto7.hashCode();

    // THEN
    assertThat(dto1Hash).isEqualTo(dto2Hash);
    assertThat(dto1Hash).isNotEqualTo(dto3Hash);
    assertThat(dto3Hash).isNotEqualTo(dto4Hash);
    assertThat(dto5Hash).isEqualTo(dto6Hash);
    assertThat(dto6Hash).isNotEqualTo(dto7Hash);
  }
}
