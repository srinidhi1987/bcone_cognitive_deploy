package com.automationanywhere.cognitive.app.errors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.path.PathManager;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DefaultControllerTest {
  private DefaultController defaultController;

  @BeforeMethod
  public void setUp() throws Exception {
    defaultController = new DefaultController(new PathManager());
  }

  @Test
  public void handlerShouldThrowNoHandlerFoundException() {
    // given
    HttpServletRequest req = mock(HttpServletRequest.class);

    doReturn(Collections.enumeration(Arrays.asList("h1", "h2")))
        .when(req).getHeaderNames();

    doReturn(Collections.enumeration(Collections.singletonList("v")))
        .when(req).getHeaders("h1");

    doReturn(Collections.enumeration(Arrays.asList("v1", "v2")))
        .when(req).getHeaders("h2");

    doReturn("MTHD").when(req).getMethod();
    doReturn("/p").when(req).getContextPath();
    doReturn("/a").when(req).getServletPath();
    doReturn("/b").when(req).getPathInfo();

    // when
    Throwable th = catchThrowable(() -> defaultController.handle(req));

    // then
    assertThat(th).isInstanceOf(NoHandlerFoundException.class);
    assertThat(((NoHandlerFoundException) th).getHttpMethod()).isEqualTo("MTHD");
    assertThat(((NoHandlerFoundException) th).getRequestURL()).isEqualTo("/p/a/b");
    assertThat(((NoHandlerFoundException) th).getHeaders()).containsExactly(
        new SimpleEntry<>("h1", Collections.singletonList("v")),
        new SimpleEntry<>("h2", Arrays.asList("v1", "v2"))
    );
  }
}
