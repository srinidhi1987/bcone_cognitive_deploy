package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.testng.annotations.Test;

public class DateTimeAccessLogElementTest {

  @Test
  public void testAddElement() throws IOException {
    Date d = new Date(1);
    CharArrayWriter buf = mock(CharArrayWriter.class);
    DateTimeAccessLogElement el = new DateTimeAccessLogElement(TimeZone.getTimeZone("GMT-5"),
        Locale.ENGLISH, "yyyy-MM-dd HH:mm:ss.SSS Z");

    el.addElement(buf, d, null, null, -1);

    verify(buf).write("1969-12-31 19:00:00.001 -0500");
  }

  @Test
  public void testAddElementFailure() throws IOException {
    Date d = new Date(1);
    CharArrayWriter buf = mock(CharArrayWriter.class);

    IOException e = new IOException();

    doThrow(e).when(buf).write("1970-01-01T00:00:00.001Z");

    DateTimeAccessLogElement el = new DateTimeAccessLogElement();

    el.addElement(buf, d, null, null, -1);

    verify(buf).write("1970-01-01T00:00:00.001Z");
  }
}
