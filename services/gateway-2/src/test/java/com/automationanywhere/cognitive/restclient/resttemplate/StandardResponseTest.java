package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.Collections;
import org.testng.annotations.Test;

public class StandardResponseTest {

  @Test
  public void createShouldFailIfThereIsAnError() {
    // given

    // when
    try {
      new StandardResponse<>(null, null, Collections.singletonList("a"));
      fail();
    } catch (final Exception ex) {
      // then
      assertThat(ex.getMessage()).isEqualTo("Error elements should be blank or empty");
    }
  }

  @Test
  public void createShouldSucceedIfErrorListIsEmpty() {
    // given

    // when
    StandardResponse<Object> r
        = new StandardResponse<>(false, null, Collections.emptyList());

    // then

    assertThat(r.success).isFalse();
    assertThat(r.data).isNull();
  }

  @Test
  public void createShouldSucceedIfErrorElementsAreBlank() {
    // given

    // when
    StandardResponse<Object> r
        = new StandardResponse<>(null, null, Collections.singletonList(" "));

    // then

    assertThat(r.success).isNull();
    assertThat(r.data).isNull();
  }

  @Test
  public void createShouldSucceedIfErrorElementsAreNull() {
    // given

    // when
    StandardResponse<Object> r
        = new StandardResponse<>(true, null, Collections.singletonList(null));

    // then

    assertThat(r.success).isTrue();
    assertThat(r.data).isNull();
  }

  @Test
  public void testEquals() {
    StandardResponse<Object> r1 = new StandardResponse<>(null);
    StandardResponse<Object> r2 = new StandardResponse<>(null);
    StandardResponse<Object> r3 = new StandardResponse<>("");

    assertThat(r1.equals(r1)).isTrue();
    assertThat(r1.equals(r2)).isTrue();
    assertThat(r1.equals(r3)).isFalse();
    assertThat(r1.equals(null)).isFalse();
    assertThat(r1.equals(new Object())).isFalse();
  }
}
