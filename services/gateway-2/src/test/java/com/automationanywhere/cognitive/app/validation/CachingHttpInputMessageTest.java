package com.automationanywhere.cognitive.app.validation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CachingHttpInputMessageTest {

  private CachingHttpInputMessage m;
  private HttpInputMessage im;
  private HttpHeaders h;
  private ByteArrayInputStream is;

  @BeforeMethod
  public void setUp() throws Exception {
    h = new HttpHeaders();
    is = new ByteArrayInputStream("test".getBytes(StandardCharsets.UTF_8));
    im = mock(HttpInputMessage.class);

    doReturn(h).when(im).getHeaders();
    doReturn(is).when(im).getBody();

    m = new CachingHttpInputMessage(im, new ExceptionHandlerFactory());
  }

  @Test
  public void testGetHeaders() throws Exception {
    assertThat(m.getHeaders()).isSameAs(h);
  }

  @Test
  public void testGetBody() throws Exception {
    String c = "";
    try (
        Scanner s = new Scanner(m.getBody());
    ) {
      while (s.hasNext()) {
        c += s.next();
      }
    }

    assertThat(c).isEqualTo("test");
  }

  @Test
  public void testGetContents() throws Exception {
    assertThat(m.getContents()).isEqualTo("test");
  }
}

