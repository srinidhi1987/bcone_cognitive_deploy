package com.automationanywhere.cognitive.gateway.alias.model;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Unit tests for the Shareable Domain Data Transfer Object.
 *
 * @author Erik K. Worth
 */
public class ShareableDomainTest {

  private static final String TEST_DOMAIN_NAME = "TestDomain";
  private static final String TEST_DOMAIN_METADATA = "TestDomainMetadata";
  private static final int TEST_VERSION = 1;

  @DataProvider(name = "dataConstructWithNulls")
  public Object[][] dataConstructWithNulls() {
    return new Object[][] {
            {"name", null, TEST_DOMAIN_METADATA},
            {"domain", TEST_DOMAIN_NAME, null},
    };
  }

  @Test(dataProvider = "dataConstructWithNulls")
  public void construct_withNullsAndConfirmException(
          final String argName,
          final String name,
          final String domain) {
    // When
    try {
      new ShareableDomain(name, domain, TEST_VERSION);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(argName + " must not be null");
    }
  }

  @SuppressWarnings("EqualsWithItself")
  @Test
  public void equals_withSameObjectAndConfirmTrue() {
    // When
    final ShareableDomain shareableDomain = new ShareableDomain(
            TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION);
    // Then
    assertThat(shareableDomain.equals(shareableDomain)).isTrue();
  }

  @SuppressWarnings("EqualsBetweenInconvertibleTypes")
  @Test
  public void equals_withWrongTypeAndConfirmFalse() {
    // When
    final ShareableDomain shareableDomain = new ShareableDomain(
            TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION);
    // Then
    assertThat(shareableDomain.equals("Not Shareable Domain")).isFalse();
  }

  @Test
  public void equals_withSameDataAndConfirmTrue() {
    // When
    final ShareableDomain shareableDomain1 = new ShareableDomain(
            TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION);
    final ShareableDomain shareableDomain2 = new ShareableDomain(
            TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION);
    // Then
    assertThat(shareableDomain1.equals(shareableDomain2)).isTrue();
    assertThat(shareableDomain1.toString())
            .isEqualTo(shareableDomain2.toString());
    assertThat(shareableDomain1.hashCode())
            .isEqualTo(shareableDomain2.hashCode());
  }

  @DataProvider(name = "dataElementsWithDifferentValues")
  public Object[][] dataElementsWithDifferentValues() {
    return new Object[][] {
            {"Not" + TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION},
            {TEST_DOMAIN_NAME, "Not" + TEST_DOMAIN_METADATA, TEST_VERSION},
            {TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, -TEST_VERSION}
    };
  }

  @Test(dataProvider = "dataElementsWithDifferentValues")
  public void equals_withDifferentFieldValuesAndConfirmFalse(
          final String name,
          final String domain,
          final int version) {
    // When
    final ShareableDomain shareableDomain1 = new ShareableDomain(
            TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION);
    final ShareableDomain shareableDomain2 = new ShareableDomain(
            name, domain, version);
    // Then
    assertThat(shareableDomain1.equals(shareableDomain2)).isFalse();
  }

    @Test
  public void get_fieldsAndConfirmEqualsConstructedValues() {
    // When
    final ShareableDomain shareableDomain = new ShareableDomain(
            TEST_DOMAIN_NAME, TEST_DOMAIN_METADATA, TEST_VERSION);
    // Then
    assertThat(shareableDomain.getName()).isEqualTo(TEST_DOMAIN_NAME);
    assertThat(shareableDomain.getDomain()).isEqualTo(TEST_DOMAIN_METADATA);
    assertThat(shareableDomain.getVersion()).isEqualTo(TEST_VERSION);
  }

}
