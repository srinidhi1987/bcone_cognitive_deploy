package com.automationanywhere.cognitive.app.filters;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.testng.annotations.Test;

public class LogFilterTest {

  @Test
  public void testErrorInChain() throws Exception {
    LogFilter f = new LogFilter(null);

    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    FilterChain c = mock(FilterChain.class);
    Map<String, String> map = new HashMap<>();
    RuntimeException e = new RuntimeException();

    doReturn(map).when(req).getAttribute("LOG_CONTEXT_REQ_ATTR_NAME");
    doThrow(e).when(c).doFilter(req, resp);

    try {
      f.doFilter(req, resp, c);
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  public void testEmptyURL() throws Exception {
    LogFilter f = new LogFilter(null);

    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    FilterChain c = mock(FilterChain.class);
    Map<String, String> map = new HashMap<>();

    doReturn("").when(req).getServletPath();
    doReturn(map).when(req).getAttribute("LOG_CONTEXT_REQ_ATTR_NAME");

    f.doFilter(req, resp, c);

    verify(c).doFilter(req, resp);
  }
}
