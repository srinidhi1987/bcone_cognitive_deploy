package com.automationanywhere.cognitive.gateway.alias.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.alias.AliasService;
import com.automationanywhere.cognitive.gateway.alias.DomainCodec;
import com.automationanywhere.cognitive.gateway.alias.microservices.alias.AliasDataMapper;
import com.automationanywhere.cognitive.gateway.alias.microservices.alias.AliasMicroservice;
import com.automationanywhere.cognitive.gateway.alias.microservices.alias.DomainLanguagesData;
import com.automationanywhere.cognitive.gateway.alias.microservices.alias.LanguageData;
import com.automationanywhere.cognitive.gateway.alias.microservices.alias.ProjectFieldData;
import com.automationanywhere.cognitive.gateway.alias.microservices.alias.ProjectTypeData;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.google.common.collect.ImmutableSet;
import org.springframework.core.ParameterizedTypeReference;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AliasControllerTest {

  private static ApplicationConfiguration appConfig;
  private static AliasController aliasController;
  private static RestClient restClient;
  private static ProjectTypeData projectTypeData1;
  private static ProjectFieldData projectFieldData1;
  private static List<ProjectFieldData> fieldsData;
  private static List<String> aliases1;
  private static ProjectTypeDto projectTypeDto1;
  private static ProjectFieldDto projectFieldDto1;
  private static ProjectFieldDto projectFieldDto2;
  private static List<LanguageData> languageEntities;
  private static LanguageData languageData1;
  private static LanguageDto languageDto1;
  private static LanguageDto languageDto2;

  @BeforeClass
  public static void setUp() {
    appConfig = mock(ApplicationConfiguration.class);
    restClient = mock(RestClient.class);
    aliasController = new AliasController(new AliasService(new AliasMicroservice(
        new AliasDataMapper(), restClient, appConfig)), new AliasDtoMapper());

    aliases1 = new ArrayList<>();
    aliases1.add("Alias11");
    aliases1.add("Alias12");

    projectFieldData1 = new ProjectFieldData(
      new StringId("1"),
      "field_name_1",
      null,
      "field_type",
      aliases1,
      true,
      "data_type"
    );

    List<String> aliases2 = new ArrayList<>();
    aliases2.add("Alias21");
    aliases2.add("Alias22");

    ProjectFieldData projectFieldData2 = new ProjectFieldData(
      new StringId("2"),
      "field_name_2",
      null,
      "field_type",
      aliases2,
      false,
      "data_type"
    );

    fieldsData = new ArrayList<>();
    fieldsData.add(projectFieldData1);
    fieldsData.add(projectFieldData2);

    languageData1 = new LanguageData(new StringId("1"), "English", "en");
    LanguageData languageData2 = new LanguageData(new StringId("1"), "Spanish", "es");

    languageEntities = new ArrayList<>();
    languageEntities.add(languageData1);
    languageEntities.add(languageData2);

    languageDto1 = new LanguageDto(new StringId("1"), "English", "en");
    languageDto2 = new LanguageDto(new StringId("1"), "Spanish", "es");

    projectTypeData1 = new ProjectTypeData(
        new StringId("1"), "project_type_name_1", null, fieldsData, Collections.singletonList(languageData1)
    );

    projectFieldDto1 = new ProjectFieldDto(
        new StringId("1"), "field_name_1", null, "field_type", aliases1, true, "data_type"
    );

    projectFieldDto2 = new ProjectFieldDto(
        new StringId("2"), "field_name_2", null, "field_type", aliases2, false, "data_type"
    );

    List<ProjectFieldDto> fieldsDto = new ArrayList<>();
    fieldsDto.add(projectFieldDto1);
    fieldsDto.add(projectFieldDto2);

    projectTypeDto1 = new ProjectTypeDto(
        new StringId("1"), "project_type_name_1", null, fieldsDto, Collections.singletonList(languageDto1)
    );
  }

  @Test
  public void getDomainsLanguages()
          throws ExecutionException, InterruptedException {
    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getDomainsLanguagesUri()).thenReturn("/domains/languages");

    RequestDetails rd = Builder.get("url")
            .request("/domains/languages")
            .accept(Types.JSON)
            .build();
    when(restClient.send(rd,
            new ParameterizedTypeReference<List<DomainLanguagesData>>() {}))
            .thenReturn(
                    CompletableFuture.completedFuture(
                            Collections.singletonList(new DomainLanguagesData(
                                    "test-domain-name",
                                    ImmutableSet.of("English", "Spanish"))))
            );

    // WHEN
    CompletableFuture<List<DomainLanguagesDto>> domainLanguagesFuture =
            aliasController.getDomainsLanguages();

    // THEN
    assertThat(domainLanguagesFuture.isDone()).isTrue();
    assertThat(domainLanguagesFuture.isCancelled()).isFalse();
    assertThat(domainLanguagesFuture.isCompletedExceptionally()).isFalse();

    List<DomainLanguagesDto> responseDtoList = domainLanguagesFuture.get();
    assertThat(responseDtoList.size()).isEqualTo(1);
    DomainLanguagesDto domainLanguagesDto = responseDtoList.get(0);
    assertThat(domainLanguagesDto.getName()).isEqualTo("test-domain-name");
    assertThat(domainLanguagesDto.getLanguages())
            .containsExactly("English", "Spanish");
  }

  @Test
  public void getShareableDomainWithDomainName()
          throws ExecutionException, InterruptedException {
    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getDomainsUri()).thenReturn("/domains?name={0}");

    RequestDetails rd = Builder.get("url")
            .request("/domains?name={0}")
            .param("test-domain-name")
            .accept(Types.JSON)
            .build();
    when(restClient.send(rd, new ParameterizedTypeReference<String>() {}))
            .thenReturn(
                    CompletableFuture.completedFuture("test-domain-metadata")
            );

    // WHEN
    CompletableFuture<ShareableDomainDto> shareableDomainFuture =
            aliasController.getShareableDomain("test-domain-name");

    // THEN
    assertThat(shareableDomainFuture.isDone()).isTrue();
    assertThat(shareableDomainFuture.isCancelled()).isFalse();
    assertThat(shareableDomainFuture.isCompletedExceptionally()).isFalse();

    ShareableDomainDto responseDto = shareableDomainFuture.get();
    assertThat(responseDto.getName()).isEqualTo("test-domain-name");
    assertThat(DomainCodec.decodeDomain(responseDto.getDomain())).isEqualTo(
            "test-domain-metadata");
    assertThat(responseDto.getVersion()).isEqualTo(1);
  }

  @Test
  public void importShareableDomainWithValidDomainMetadata() {
    // GIVEN
    final String testDomainJson = "{\"name\":\"test-domain\"}";
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.importDomainsUri()).thenReturn("/domains");

    RequestDetails rd = Builder.post("url")
            .request("/domains")
            .content(testDomainJson, Types.JSON)
            .build();
    when(restClient.send(rd, new ParameterizedTypeReference<Void>() {}))
            .thenReturn(CompletableFuture.completedFuture(null));

    // WHEN
    final ShareableDomainDto shareableDomainDto = new ShareableDomainDto(
            "test-domain",
            DomainCodec.encodeDomain(testDomainJson),
            1);
    final CompletableFuture<Void> result =
            aliasController.importShareableDomain(shareableDomainDto);

    // THEN
    assertThat(result).isNotNull();
    assertThat(result.isDone()).isTrue();
    assertThat(result.isCancelled()).isFalse();
    assertThat(result.isCompletedExceptionally()).isFalse();
  }

  @Test
  public void getAllProjectTypesWithoutLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getProjectTypeUri()).thenReturn("/projecttypes");

    RequestDetails rd = Builder.get("url").request("/projecttypes").accept(Types.JSON).build();
    when(
        restClient.send(rd, new ParameterizedTypeReference<List<ProjectTypeData>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(Collections.singletonList(projectTypeData1))
    );

    // WHEN
    CompletableFuture<StandardResponse<List<ProjectTypeDto>>> projectTypesFuture =
        aliasController.getAllProjectTypes(null);

    // THEN
    assertThat(projectTypesFuture.isDone()).isTrue();
    assertThat(projectTypesFuture.isCancelled()).isFalse();
    assertThat(projectTypesFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<ProjectTypeDto>> responseDto = projectTypesFuture.get();
    assertThat(responseDto.success).isTrue();

    assertThat(responseDto.data.size()).isEqualTo(1);
    assertThat(responseDto.data.get(0)).isEqualToComparingFieldByFieldRecursively(projectTypeDto1);
  }

  @Test
  public void getAllProjectTypesWithLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getAllProjectTypeUri()).thenReturn("/projecttypes?languageid={0}");

    RequestDetails rd = Builder.get("url")
        .request("/projecttypes?languageid={0}")
        .accept(Types.JSON)
        .param(1)
        .build();
    when(
        restClient.send(rd, new ParameterizedTypeReference<List<ProjectTypeData>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(Collections.singletonList(projectTypeData1))
    );

    // WHEN
    CompletableFuture<StandardResponse<List<ProjectTypeDto>>> projectTypesFuture =
        aliasController.getAllProjectTypes("1");

    // THEN
    assertThat(projectTypesFuture.isDone()).isTrue();
    assertThat(projectTypesFuture.isCancelled()).isFalse();
    assertThat(projectTypesFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<ProjectTypeDto>> responseDto = projectTypesFuture.get();
    assertThat(responseDto.success).isTrue();

    assertThat(responseDto.data.size()).isEqualTo(1);
    assertThat(responseDto.data.get(0)).isEqualToComparingFieldByFieldRecursively(projectTypeDto1);
  }

  @Test
  public void getProjectTypeWithoutLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getProjectTypeUri()).thenReturn("/projecttypes/{0}?languageid={1}");

    RequestDetails rd = Builder.get("url")
        .request("/projecttypes/{0}?languageid={1}")
        .param(1)
        .param(null)
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<ProjectTypeData>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(projectTypeData1)
    );

    // WHEN
    CompletableFuture<StandardResponse<ProjectTypeDto>> projectTypeFuture =
        aliasController.getProjectType("1", null);

    // THEN
    assertThat(projectTypeFuture.isDone()).isTrue();
    assertThat(projectTypeFuture.isCancelled()).isFalse();
    assertThat(projectTypeFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<ProjectTypeDto> responseDto = projectTypeFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).isEqualToComparingFieldByFieldRecursively(projectTypeDto1);
  }

  @Test
  public void getProjectTypeWithLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getProjectTypeUri()).thenReturn("/projecttypes/{0}?languageid={1}");

    RequestDetails rd = Builder.get("url")
        .request("/projecttypes/{0}?languageid={1}")
        .param("1")
        .param("2")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<ProjectTypeData>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(projectTypeData1)
    );

    // WHEN
    CompletableFuture<StandardResponse<ProjectTypeDto>> projectTypeFuture =
        aliasController.getProjectType("1", "2");

    // THEN
    assertThat(projectTypeFuture.isDone()).isTrue();
    assertThat(projectTypeFuture.isCancelled()).isFalse();
    assertThat(projectTypeFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<ProjectTypeDto> responseDto = projectTypeFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).isEqualToComparingFieldByFieldRecursively(projectTypeDto1);
  }

  @Test
  public void getAllProjectFieldsWithoutLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getProjectFieldsUri()).thenReturn("/projecttypes/{0}/fields?languageid={1}");

    RequestDetails rd = Builder.get("url")
        .request("/projecttypes/{0}/fields?languageid={1}")
        .param("1")
        .param(null)
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<List<ProjectFieldData>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(fieldsData)
    );

    // WHEN
    CompletableFuture<StandardResponse<List<ProjectFieldDto>>> projectFieldsFuture =
        aliasController.getAllFields("1", null);

    // THEN
    assertThat(projectFieldsFuture.isDone()).isTrue();
    assertThat(projectFieldsFuture.isCancelled()).isFalse();
    assertThat(projectFieldsFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<ProjectFieldDto>> responseDto = projectFieldsFuture.get();
    assertThat(responseDto.success).isTrue();

    assertThat(responseDto.data.size()).isEqualTo(2);
    assertThat(responseDto.data.get(0)).isEqualToComparingFieldByFieldRecursively(projectFieldDto1);
    assertThat(responseDto.data.get(1)).isEqualToComparingFieldByFieldRecursively(projectFieldDto2);
  }

  @Test
  public void getAllProjectFieldsWithLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getProjectFieldsUri()).thenReturn("/projecttypes/{0}/fields?languageid={1}");

    RequestDetails rd = Builder.get("url")
        .request("/projecttypes/{0}/fields?languageid={1}")
        .param("1")
        .param("2")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<List<ProjectFieldData>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(fieldsData)
    );

    // WHEN
    CompletableFuture<StandardResponse<List<ProjectFieldDto>>> projectFieldsFuture =
        aliasController.getAllFields("1", "2");

    // THEN
    assertThat(projectFieldsFuture.isDone()).isTrue();
    assertThat(projectFieldsFuture.isCancelled()).isFalse();
    assertThat(projectFieldsFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<ProjectFieldDto>> responseDto = projectFieldsFuture.get();
    assertThat(responseDto.success).isTrue();

    assertThat(responseDto.data.size()).isEqualTo(2);
    assertThat(responseDto.data.get(0)).isEqualToComparingFieldByFieldRecursively(projectFieldDto1);
    assertThat(responseDto.data.get(1)).isEqualToComparingFieldByFieldRecursively(projectFieldDto2);
  }

  @Test
  public void getProjectFieldWithoutLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getFieldUri()).thenReturn("/fields/{0}");

    RequestDetails rd = Builder.get("url")
        .request("/fields/{0}")
        .param("1")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<ProjectFieldData>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(projectFieldData1)
    );

    // WHEN
    CompletableFuture<StandardResponse<ProjectFieldDto>> projectFieldFuture =
        aliasController.getField("1", null);

    // THEN
    assertThat(projectFieldFuture.isDone()).isTrue();
    assertThat(projectFieldFuture.isCancelled()).isFalse();
    assertThat(projectFieldFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<ProjectFieldDto> responseDto = projectFieldFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).isEqualToComparingFieldByFieldRecursively(projectFieldDto1);
  }

  @Test
  public void getProjectFieldWithLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getFieldUri()).thenReturn("/fields/{0}?languageid={1}");

    RequestDetails rd = Builder.get("url")
        .request("/fields/{0}?languageid={1}")
        .param("1")
        .param("2")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<ProjectFieldData>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(projectFieldData1)
    );

    // WHEN
    CompletableFuture<StandardResponse<ProjectFieldDto>> projectFieldFuture =
        aliasController.getField("1", "2");

    // THEN
    assertThat(projectFieldFuture.isDone()).isTrue();
    assertThat(projectFieldFuture.isCancelled()).isFalse();
    assertThat(projectFieldFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<ProjectFieldDto> responseDto = projectFieldFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).isEqualToComparingFieldByFieldRecursively(projectFieldDto1);
  }

  @Test
  public void getAliasesWithoutLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getAliasesUri()).thenReturn("/fields/{0}/alias");

    RequestDetails rd = Builder.get("url")
        .request("/fields/{0}/alias")
        .param("1")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<List<String>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(aliases1)
    );

    // WHEN
    CompletableFuture<StandardResponse<List<String>>> aliasesFuture =
        aliasController.getAliases("1", null);

    // THEN
    assertThat(aliasesFuture.isDone()).isTrue();
    assertThat(aliasesFuture.isCancelled()).isFalse();
    assertThat(aliasesFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<String>> responseDto = aliasesFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).containsExactlyElementsOf(aliases1);
  }

  @Test
  public void getAliasesWithLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getAliasesUri()).thenReturn("/fields/{0}/alias?languageid={1}");

    RequestDetails rd = Builder.get("url")
        .request("/fields/{0}/alias?languageid={1}")
        .param("1")
        .param("2")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<List<String>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(aliases1)
    );

    // WHEN
    CompletableFuture<StandardResponse<List<String>>> aliasesFuture =
        aliasController.getAliases("1", "2");

    // THEN
    assertThat(aliasesFuture.isDone()).isTrue();
    assertThat(aliasesFuture.isCancelled()).isFalse();
    assertThat(aliasesFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<String>> responseDto = aliasesFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).containsExactlyElementsOf(aliases1);
  }

  @Test
  public void getLanguages() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getLanguagesUri()).thenReturn("/languages");

    RequestDetails rd = Builder.get("url")
        .request("/languages")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<List<LanguageData>>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(languageEntities)
    );

    // WHEN
    CompletableFuture<StandardResponse<List<LanguageDto>>> languagesFuture =
        aliasController.getLanguages();

    // THEN
    assertThat(languagesFuture.isDone()).isTrue();
    assertThat(languagesFuture.isCancelled()).isFalse();
    assertThat(languagesFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<List<LanguageDto>> responseDto = languagesFuture.get();
    assertThat(responseDto.success).isTrue();

    assertThat(responseDto.data.size()).isEqualTo(2);
    assertThat(responseDto.data.get(0)).isEqualToComparingFieldByField(languageDto1);
    assertThat(responseDto.data.get(1)).isEqualToComparingFieldByField(languageDto2);
  }

  @Test
  public void getLanguage() throws ExecutionException, InterruptedException {

    // GIVEN
    when(appConfig.aliasServiceURL()).thenReturn("url");
    when(appConfig.getLanguageUri()).thenReturn("/languages/{0}");

    RequestDetails rd = Builder.get("url")
        .request("/languages/{0}")
        .param("1")
        .accept(Types.JSON).build();

    when(
        restClient.send(rd, new ParameterizedTypeReference<LanguageData>() {
        })
    ).thenReturn(
        CompletableFuture.completedFuture(languageData1)
    );

    // WHEN
    CompletableFuture<StandardResponse<LanguageDto>> languageFuture =
        aliasController.getLanguage("1");

    // THEN
    assertThat(languageFuture.isDone()).isTrue();
    assertThat(languageFuture.isCancelled()).isFalse();
    assertThat(languageFuture.isCompletedExceptionally()).isFalse();

    StandardResponse<LanguageDto> responseDto = languageFuture.get();
    assertThat(responseDto.success).isTrue();
    assertThat(responseDto.data).isEqualToComparingFieldByField(languageDto1);
  }
}
