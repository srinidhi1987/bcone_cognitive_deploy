package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URI;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoggingResponseListenableFutureTest {

  private HttpRequest req;
  private LoggingResponseListenableFuture f;

  @BeforeMethod
  public void setUp() throws Exception {
    req = mock(HttpRequest.class);
    f = Mockito.spy(new LoggingResponseListenableFuture(req));
  }

  @Test
  public void testOnFailure() {
    Throwable ex = new Exception("test");

    f.onFailure(ex);

    Mockito.verify(f).log(ex);
  }

  @Test
  public void testOnSuccess() throws Exception {
    IOException ex = new IOException("test");

    ClientHttpResponse res = mock(ClientHttpResponse.class);

    doThrow(ex).when(res).getRawStatusCode();

    f.onSuccess(res);

    Mockito.verify(f).log(ex);
    Mockito.verify(f).log(any());

    doReturn(111).when(res).getRawStatusCode();

    f.onSuccess(res);

    Mockito.verify(f).log(ex);
    Mockito.verify(f).log(any());
  }

  @Test
  public void testLogError() throws Exception {
    Throwable ex = new Exception("test");
    HttpMethod m = HttpMethod.GET;
    URI u = new URI("file:///");

    doReturn(m).when(req).getMethod();
    doReturn(u).when(req).getURI();

    f.log(ex);

    verify(req).getMethod();
    verify(req).getURI();
  }
}
