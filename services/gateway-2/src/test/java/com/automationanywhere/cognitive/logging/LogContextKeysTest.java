package com.automationanywhere.cognitive.logging;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class LogContextKeysTest {

  @Test
  public void testValues() {
    assertThat(LogContextKeys.values()).containsExactlyInAnyOrder(
        LogContextKeys.CID,
        LogContextKeys.HOST_NAME,
        LogContextKeys.CLIENT_IP,
        LogContextKeys.APPLICATION_NAME,
        LogContextKeys.APPLICATION_VERSION,
        LogContextKeys.SESSION_ID,
        LogContextKeys.USER_ID,
        LogContextKeys.TENANT_ID,
        LogContextKeys.REQUEST_DETAILS
    );
  }

  @Test
  public void testValueOf() {
    assertThat(LogContextKeys.valueOf(LogContextKeys.CID.name()))
        .isEqualTo(LogContextKeys.CID);
    assertThat(LogContextKeys.valueOf(LogContextKeys.HOST_NAME.name()))
        .isEqualTo(LogContextKeys.HOST_NAME);
    assertThat(LogContextKeys.valueOf(LogContextKeys.CLIENT_IP.name()))
        .isEqualTo(LogContextKeys.CLIENT_IP);
    assertThat(LogContextKeys.valueOf(LogContextKeys.APPLICATION_NAME.name()))
        .isEqualTo(LogContextKeys.APPLICATION_NAME);
    assertThat(LogContextKeys.valueOf(LogContextKeys.APPLICATION_VERSION.name()))
        .isEqualTo(LogContextKeys.APPLICATION_VERSION);
    assertThat(LogContextKeys.valueOf(LogContextKeys.SESSION_ID.name()))
        .isEqualTo(LogContextKeys.SESSION_ID);
    assertThat(LogContextKeys.valueOf(LogContextKeys.USER_ID.name()))
        .isEqualTo(LogContextKeys.USER_ID);
    assertThat(LogContextKeys.valueOf(LogContextKeys.TENANT_ID.name()))
        .isEqualTo(LogContextKeys.TENANT_ID);
    assertThat(LogContextKeys.valueOf(LogContextKeys.REQUEST_DETAILS.name()))
        .isEqualTo(LogContextKeys.REQUEST_DETAILS);
  }
}
