package com.automationanywhere.cognitive.app.resourcelock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectDataMapper;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectMicroservice;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.path.PathManager;
import com.automationanywhere.cognitive.resourcelock.ExclusiveResource;
import com.automationanywhere.cognitive.resourcelock.ResourceFactory;
import com.automationanywhere.cognitive.resourcelock.ResourceLocker;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ResourceLockInterceptorTest {
  private ResourceLockInterceptor i;
  private ResourceLocker rl;

  @BeforeMethod
  public void setUp() throws Exception {
    ApplicationConfiguration cfg = mock(ApplicationConfiguration.class);
    doReturn("/block").when(cfg).resourceLockerImportResource();
    doReturn(1000L).when(cfg).resourceLockerPollPeriod();

    RestClient rc = mock(RestClient.class);

    PathManager pathManager = new PathManager();
    ProjectDataMapper pdm = new ProjectDataMapper();
    ProjectMicroservice pm = new ProjectMicroservice(pdm, rc, cfg);
    ObjectMapper om = new ObjectMapper();
    ResourceFactory rf = new ResourceFactory(cfg, pathManager);

    rl = spy(new ResourceLocker(pm, cfg));
    rl.init();

    i = spy(new ResourceLockInterceptor(
        pathManager, rf, rl, om
    ));
  }

  @AfterMethod
  public void tearDown() throws Exception {
    rl.destroy();
  }

  @Test
  public void preHandleShouldCancelExecution() throws Exception {
    // given
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse res = mock(HttpServletResponse.class);
    HandlerMethod h = mock(HandlerMethod.class);
    Method m = Controller.class.getMethod("m");
    PrintWriter w = mock(PrintWriter.class);

    doReturn(w).when(res).getWriter();
    doReturn(DispatcherType.REQUEST).when(req).getDispatcherType();
    doReturn("POST").when(req).getMethod();
    doReturn("").when(req).getContextPath();
    doReturn("").when(req).getServletPath();
    doReturn("/block").when(req).getPathInfo();
    doReturn(m).when(h).getMethod();

    rl.lock(new ExclusiveResource("act", new StringId("1"), "/p"));

    // when
    boolean ok = i.preHandle(req, res, h);

    // then
    assertThat(ok).isFalse();
  }

  @Test
  public void afterCompletionShouldStartLocker() throws Exception {
    // given
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse res = mock(HttpServletResponse.class);
    HandlerMethod h = mock(HandlerMethod.class);

    ExclusiveResource er = new ExclusiveResource("an", new StringId("1"), "/p");
    doReturn(er).when(req).getAttribute("LOCKED_RESOURCE_ATTR");
    doReturn(200).when(res).getStatus();

    rl.lock(er);

    // when
    i.afterCompletion(req, res, h, null);

    // then
    verify(rl).start(er);
    verify(rl, never()).unlock(any());
  }

  @Test
  public void afterCompletionShouldUnlockOnErrors() throws Exception {
    // given
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse res = mock(HttpServletResponse.class);
    HandlerMethod h = mock(HandlerMethod.class);

    ExclusiveResource er = new ExclusiveResource("an", new StringId("1"), "/p");
    doReturn(er).when(req).getAttribute("LOCKED_RESOURCE_ATTR");
    doReturn(200).when(res).getStatus();

    doThrow(new RuntimeException()).when(rl).unlock(er);

    // when
    i.afterCompletion(req, res, h, null);

    // then
    verify(rl).unlock(any());
  }
}

@RequestMapping
class Controller {
  @RequestMapping("/block")
  public void m(){}
}