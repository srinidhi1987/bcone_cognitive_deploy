package com.automationanywhere.cognitive.gateway.report.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

import com.automationanywhere.cognitive.BaseControllerTest;
import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.auth.models.JWTAuthorization;
import com.automationanywhere.cognitive.gateway.report.adapters.AccuracyDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ClassificationDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ClassificationFieldDto;
import com.automationanywhere.cognitive.gateway.report.adapters.DashboardProdProjectTotalsDto;
import com.automationanywhere.cognitive.gateway.report.adapters.FieldAccuracyDto;
import com.automationanywhere.cognitive.gateway.report.adapters.FieldValidationDto;
import com.automationanywhere.cognitive.gateway.report.adapters.GroupTimeSpentDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ReportingProjectDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ResponseDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ValidationDto;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.AccuracyData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ClassificationData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ClassificationFieldData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.DashboardProdProjectTotalsData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.FieldAccuracyData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.FieldValidationData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.GroupTimeSpentData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ProjectTotalsData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ReportingProjectData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ValidationData;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotalsDto;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportResourceTest extends BaseControllerTest {

  @MockBean
  @Autowired
  private RestClient restClient;

  @MockBean
  @Autowired
  private ApplicationConfiguration cfg;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void getDashboardTotals() {
    // GIVEN
    String orgId = "1";
    long totalFilesProcessed = 1;
    long totalSTP = 2;
    long totalAccuracy = 3;
    long totalFilesCount = 4;

    DashboardProdProjectTotalsData dashboardData = new DashboardProdProjectTotalsData();
    dashboardData.setTotalFilesProcessed(totalFilesProcessed);
    dashboardData.setTotalSTP(totalSTP);
    dashboardData.setTotalAccuracy(totalAccuracy);
    dashboardData.setTotalFilesCount(totalFilesCount);

    DashboardProdProjectTotalsDto dto = new DashboardProdProjectTotalsDto();
    dto.setTotalFilesProcessed(totalFilesProcessed);
    dto.setTotalSTP(totalSTP);
    dto.setTotalAccuracy(totalAccuracy);
    dto.setTotalFilesCount(totalFilesCount);

    ResponseDto<DashboardProdProjectTotalsDto> responseDto = new ResponseDto<>(dto);

    doReturn("url").when(cfg).reportServiceURL();
    doReturn("prj{0}").when(cfg).getDashboardReportTotals();
    doReturn(CompletableFuture.completedFuture(dashboardData))
        .when(restClient)
        .send(Builder.get("url")
                .request("prj{0}")
                .param(orgId)
                .accept(Types.JSON)
                .build(),
            new ParameterizedTypeReference<DashboardProdProjectTotalsData>() {
            }
        );

    JWTAuthorization authorization = new JWTAuthorization(
        "/organizations/:orgId/reporting/projects/totals",
        "TOKEN");

    doReturn("authUrl").when(cfg).authURL();
    doReturn("authorize").when(cfg).authorize();
    doReturn(CompletableFuture.completedFuture(new byte[]{}))
        .when(restClient)
        .send(Builder.post("authUrl")
                .request("authorize")
                .content(authorization, Types.JSON)
                .accept(Types.JSON)
                .build(),
            new ParameterizedTypeReference<byte[]>() {
            }
        );

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.AUTHORIZATION, "Bearer TOKEN");

    // WHEN
    ResponseEntity<ResponseDto<DashboardProdProjectTotalsDto>> responseEntity = restTemplate
        .exchange(
            "/organizations/" + orgId + "/reporting/projects/totals",
            HttpMethod.GET,
            new HttpEntity<>(headers),
            new ParameterizedTypeReference<ResponseDto<DashboardProdProjectTotalsDto>>() {
            }
        );

    // THEN
    assertThat(responseEntity.getBody()).isEqualToComparingFieldByFieldRecursively(responseDto);
  }

  @Test
  public void getReportTotals() {
    // GIVEN
    String orgId = "1";
    String prjId = "1";
    long totalFilesProcessed = 1;
    long totalSTP = 2;
    long totalAccuracy = 3;
    long totalFilesUploaded = 5;
    long totalFilesToValidation = 6;
    long totalFilesValidated = 7;
    long totalFilesUnprocessable = 8;

    ClassificationFieldData classificationField = new ClassificationFieldData();
    classificationField.setName("classification");
    classificationField.setRepresentationPercent(11L);

    ClassificationData classification = new ClassificationData();
    classification.setFieldRepresentation(Collections.singletonList(classificationField));

    FieldAccuracyData fieldAccuracy = new FieldAccuracyData();
    fieldAccuracy.setName("accuracy");
    fieldAccuracy.setAccuracyPercent(22L);

    AccuracyData accuracy = new AccuracyData();
    accuracy.setFields(Collections.singletonList(fieldAccuracy));

    FieldValidationData fieldValidation = new FieldValidationData();
    fieldValidation.setFieldId("fieldId_1");
    fieldValidation.setPercentValidated(33L);

    GroupTimeSpentData groupTimeSpent = new GroupTimeSpentData();
    groupTimeSpent.setName("GroupTimeSpent");
    groupTimeSpent.setTimeSpent(44L);

    ValidationData validation = new ValidationData();
    validation.setFields(Collections.singletonList(fieldValidation));
    validation.setCategories(Collections.singletonList(groupTimeSpent));

    ProjectTotalsData projectTotals = new ProjectTotalsData();
    projectTotals.setTotalFilesProcessed(totalFilesProcessed);
    projectTotals.setTotalSTP(totalSTP);
    projectTotals.setTotalAccuracy(totalAccuracy);
    projectTotals.setTotalFilesUploaded(totalFilesUploaded);
    projectTotals.setTotalFilesToValidation(totalFilesToValidation);
    projectTotals.setTotalFilesValidated(totalFilesValidated);
    projectTotals.setTotalFilesUnprocessable(totalFilesUnprocessable);
    projectTotals.setClassification(classification);
    projectTotals.setAccuracy(accuracy);
    projectTotals.setValidation(validation);

    ClassificationFieldDto classificationFieldDto = new ClassificationFieldDto();
    classificationFieldDto.setName("classification");
    classificationFieldDto.setRepresentationPercent(11L);

    ClassificationDto classificationDto = new ClassificationDto();
    classificationDto.setFieldRepresentation(Collections.singletonList(classificationFieldDto));

    FieldAccuracyDto fieldAccuracyDto = new FieldAccuracyDto();
    fieldAccuracyDto.setName("accuracy");
    fieldAccuracyDto.setAccuracyPercent(22L);

    AccuracyDto accuracyDto = new AccuracyDto();
    accuracyDto.setFields(Collections.singletonList(fieldAccuracyDto));

    FieldValidationDto fieldValidationDto = new FieldValidationDto();
    fieldValidationDto.setFieldId("fieldId_1");
    fieldValidationDto.setPercentValidated(33L);

    GroupTimeSpentDto groupTimeSpentDto = new GroupTimeSpentDto();
    groupTimeSpentDto.setName("GroupTimeSpent");
    groupTimeSpentDto.setTimeSpent(44L);

    ValidationDto validationDto = new ValidationDto();
    validationDto.setFields(Collections.singletonList(fieldValidationDto));
    validationDto.setCategories(Collections.singletonList(groupTimeSpentDto));

    ProjectTotalsDto dto = new ProjectTotalsDto();
    dto.setTotalFilesProcessed(totalFilesProcessed);
    dto.setTotalSTP(totalSTP);
    dto.setTotalAccuracy(totalAccuracy);
    dto.setTotalFilesUploaded(totalFilesUploaded);
    dto.setTotalFilesToValidation(totalFilesToValidation);
    dto.setTotalFilesValidated(totalFilesValidated);
    dto.setTotalFilesUnprocessable(totalFilesUnprocessable);
    dto.setClassification(classificationDto);
    dto.setAccuracy(accuracyDto);
    dto.setValidation(validationDto);

    ResponseDto<ProjectTotalsDto> responseDto = new ResponseDto<>(dto);

    doReturn("url").when(cfg).reportServiceURL();
    doReturn("prj{0}{1}").when(cfg).getReportTotals();
    doReturn(CompletableFuture.completedFuture(projectTotals))
        .when(restClient)
        .send(Builder.get("url")
                .request("prj{0}{1}")
                .param(orgId)
                .param(prjId)
                .accept(Types.JSON)
                .build(),
            new ParameterizedTypeReference<ProjectTotalsData>() {
            }
        );

    JWTAuthorization authorization = new JWTAuthorization(
        "/organizations/:orgId/reporting/projects/:projectId/totals",
        "TOKEN");

    doReturn("authUrl").when(cfg).authURL();
    doReturn("authorize").when(cfg).authorize();
    doReturn(CompletableFuture.completedFuture(new byte[]{}))
        .when(restClient)
        .send(Builder.post("authUrl")
                .request("authorize")
                .content(authorization, Types.JSON)
                .accept(Types.JSON)
                .build(),
            new ParameterizedTypeReference<byte[]>() {
            }
        );

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.AUTHORIZATION, "Bearer TOKEN");

    // WHEN
    ResponseEntity<ResponseDto<ProjectTotalsDto>> responseEntity = restTemplate
        .exchange(
            "/organizations/" + orgId + "/reporting/projects/" + prjId + "/totals",
            HttpMethod.GET,
            new HttpEntity<>(headers),
            new ParameterizedTypeReference<ResponseDto<ProjectTotalsDto>>() {
            }
        );

    // THEN
    assertThat(responseEntity.getBody()).isEqualToComparingFieldByFieldRecursively(responseDto);
  }

  @Test
  public void getReport() {
    // GIVEN
    String orgId = "1";
    String id = "1";
    String organizationId = "1";
    String name = "name";
    String description = "description";
    String environment = "environment";
    String primaryLanguage = "EN";
    String projectType = "type";
    String projectState = "state";
    long totalFilesProcessed = 1;
    long totalSTP = 2;
    long numberOfFiles = 3;
    long totalAccuracy = 4;
    long currentTrainedPercentage = 5;

    ReportingProjectData data = new ReportingProjectData();
    data.setId(id);
    data.setOrganizationId(organizationId);
    data.setName(name);
    data.setDescription(description);
    data.setEnvironment(environment);
    data.setPrimaryLanguage(primaryLanguage);
    data.setProjectType(projectType);
    data.setProjectState(projectState);
    data.setTotalFilesProcessed(totalFilesProcessed);
    data.setTotalSTP(totalSTP);
    data.setNumberOfFiles(numberOfFiles);
    data.setTotalAccuracy(totalAccuracy);
    data.setCurrentTrainedPercentage(currentTrainedPercentage);

    List<ReportingProjectData> reports = new ArrayList<>();
    reports.add(data);

    ReportingProjectDto dto = new ReportingProjectDto();
    dto.setId(id);
    dto.setOrganizationId(organizationId);
    dto.setName(name);
    dto.setDescription(description);
    dto.setEnvironment(environment);
    dto.setPrimaryLanguage(primaryLanguage);
    dto.setProjectType(projectType);
    dto.setProjectState(projectState);
    dto.setTotalFilesProcessed(totalFilesProcessed);
    dto.setTotalSTP(totalSTP);
    dto.setNumberOfFiles(numberOfFiles);
    dto.setTotalAccuracy(totalAccuracy);
    dto.setCurrentTrainedPercentage(currentTrainedPercentage);

    List<ReportingProjectDto> dtos = new ArrayList<>();
    dtos.add(dto);
    ResponseDto<List<ReportingProjectDto>> responseDto = new ResponseDto<>(dtos);

    doReturn("url").when(cfg).reportServiceURL();
    doReturn("prj{0}").when(cfg).getReports();
    doReturn(CompletableFuture.completedFuture(reports))
        .when(restClient)
        .send(Builder.get("url")
                .request("prj{0}")
                .param(orgId)
                .accept(Types.JSON)
                .build(),
            new ParameterizedTypeReference<List<ReportingProjectData>>() {
            }
        );

    JWTAuthorization authorization = new JWTAuthorization(
        "/organizations/:orgId/reporting/projects",
        "TOKEN");

    doReturn("authUrl").when(cfg).authURL();
    doReturn("authorize").when(cfg).authorize();
    doReturn(CompletableFuture.completedFuture(new byte[]{}))
        .when(restClient)
        .send(Builder.post("authUrl")
                .request("authorize")
                .content(authorization, Types.JSON)
                .accept(Types.JSON)
                .build(),
            new ParameterizedTypeReference<byte[]>() {
            }
        );

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.AUTHORIZATION, "Bearer TOKEN");

    // WHEN
    ResponseEntity<ResponseDto<List<ReportingProjectDto>>> responseEntity = restTemplate
        .exchange(
            "/organizations/" + orgId + "/reporting/projects",
            HttpMethod.GET,
            new HttpEntity<>(headers),
            new ParameterizedTypeReference<ResponseDto<List<ReportingProjectDto>>>() {
            }
        );

    // THEN
    assertThat(responseEntity.getBody()).isEqualToComparingFieldByFieldRecursively(responseDto);
  }
}
