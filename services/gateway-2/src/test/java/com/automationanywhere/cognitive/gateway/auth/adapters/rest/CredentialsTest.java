package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class CredentialsTest {

  @Test
  public void testCredentialsJson() {
    CredentialsDto o1 = new CredentialsDto("un", "pw");
    CredentialsDto o2 = new CredentialsDto("un", "pw");
    CredentialsDto o3 = new CredentialsDto("un", "pw1");
    CredentialsDto o4 = new CredentialsDto("un1", "pw");

    assertThat(o1.equals(o1)).isTrue();
    assertThat(o1.equals(o2)).isTrue();
    assertThat(o1.equals(o3)).isFalse();
    assertThat(o1.equals(o4)).isFalse();
    assertThat(o1.equals(null)).isFalse();
    assertThat(o1.equals(new Object())).isFalse();
  }
}
