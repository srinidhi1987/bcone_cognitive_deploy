package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.project.microservices.project.TaskData;
import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.Category;
import com.automationanywhere.cognitive.gateway.project.models.CustomFieldDetail;
import com.automationanywhere.cognitive.gateway.project.models.FieldDetails;
import com.automationanywhere.cognitive.gateway.project.models.File;
import com.automationanywhere.cognitive.gateway.project.models.FileCount;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.OCREngineDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.gateway.project.models.VisionBot;
import com.automationanywhere.cognitive.id.string.StringId;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import org.testng.annotations.Test;

public class ProjectDtoMapperTest {

  private final ProjectDtoMapper mapper = new ProjectDtoMapper();

  @Test
  public void mapProjectDetailsShouldMapDataToModel1() {
    // given
    OffsetDateTime updatedAt = OffsetDateTime.now();
    OffsetDateTime createdAt = OffsetDateTime.now();

    ProjectDetails p1 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            new VisionBot(new StringId("vb"), "vbA", "cs"),
            -19, Collections.singletonList(new File(
            new StringId("f1"), "fnm", "loc", "fmt", true
        )),
            new FileCount(90, 9),
            new FileCount(91, 8)
        )), new FieldDetails(
        Arrays.asList("1", "2"),
        Collections.singletonList(
            new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
        )
    ), "pst", "env", updatedAt, createdAt, Collections.singletonList(
        new OCREngineDetail(new StringId("add"), "addnm", "engineType")
    ), 1
    );

    ProjectDetails p2 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetails(
        null,
        null
    ), "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetails p3 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetails(
        null,
        Collections.singletonList(null)
    ), "pst", "env", updatedAt, createdAt,
        Collections.singletonList(null), 1
    );

    ProjectDetailsDto pdto1 = new ProjectDetailsDto(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryDto(
            new StringId("c1"), "c",
            new VisionBotDto(new StringId("vb"), "vbA", "cs"),
            -19, Collections.singletonList(new FileDto(
            new StringId("f1"), "fnm", "loc", "fmt", true
        )),
            new FileCountDto(90, 9),
            new FileCountDto(91, 8)
        )), new FieldDetailsDto(
        Arrays.asList("1", "2"),
        Collections.singletonList(
            new CustomFieldDetailDto(new StringId("cf1"), "cfnm", "tp")
        )
    ), "pst", "env", updatedAt, createdAt, Collections.singletonList(
        new OCREngineDetailDto(new StringId("add"), "addnm", "engineType")
    ), 1
    );

    ProjectDetailsDto pdto2 = new ProjectDetailsDto(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryDto(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetailsDto(
        null,
        null
    ), "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetailsDto pdto3 = new ProjectDetailsDto(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryDto(
            new StringId("c1"), "c",
            null,
            -19, null,
            null,
            null
        )), new FieldDetailsDto(
        null,
        Collections.singletonList(null)
    ), "pst", "env", updatedAt, createdAt,
        Collections.singletonList(null), 1
    );

    // when
    ProjectDetailsDto r1 = mapper.mapProjectDetails(p1);
    ProjectDetailsDto r2 = mapper.mapProjectDetails(p2);
    ProjectDetailsDto r3 = mapper.mapProjectDetails(p3);

    // then
    assertThat(r1).isEqualTo(pdto1);
    assertThat(r2).isEqualTo(pdto2);
    assertThat(r3).isEqualTo(pdto3);
  }

  @Test
  public void mapProjectDetailsShouldMapDataToModel2() {
    // given
    OffsetDateTime updatedAt = OffsetDateTime.now();
    OffsetDateTime createdAt = OffsetDateTime.now();

    ProjectDetails p3 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        null, null, "pst", "env", updatedAt, createdAt,
        null, 1
    );

    ProjectDetails p4 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new Category(
            new StringId("c1"), "c",
            null,
            -19, Collections.singletonList(null),
            null,
            null
        )), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetails p5 = new ProjectDetails(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(null), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetailsDto pdto3 = new ProjectDetailsDto(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        null, null, "pst", "env",
        updatedAt, createdAt, null, 1
    );

    ProjectDetailsDto pdto4 = new ProjectDetailsDto(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(new CategoryDto(
            new StringId("c1"), "c",
            null,
            -19, Collections.singletonList(null),
            null,
            null
        )), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    ProjectDetailsDto pdto5 = new ProjectDetailsDto(
        new StringId("p"), "nm", "d", new StringId("1"), new StringId("pt"), "pt",
        -1, 23, 89, 9,
        "pl", 8, 12, 89,
        Collections.singletonList(null), null, "pst", "env", updatedAt, createdAt, null, 1
    );

    // when
    ProjectDetailsDto r3 = mapper.mapProjectDetails(p3);
    ProjectDetailsDto r4 = mapper.mapProjectDetails(p4);
    ProjectDetailsDto r5 = mapper.mapProjectDetails(p5);
    ProjectDetailsDto r6 = mapper.mapProjectDetails(null);

    // then
    assertThat(r3).isEqualTo(pdto3);
    assertThat(r4).isEqualTo(pdto4);
    assertThat(r5).isEqualTo(pdto5);
    assertThat(r6).isNull();
  }

  @Test
  public void mapNewProjectDetailsShouldMapDataToModel() {
    // given
    NewProjectDetailsDto pjson1 = new NewProjectDetailsDto(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailDto(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env"
    );

    NewProjectDetailsDto pjson2 = new NewProjectDetailsDto(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env"
    );

    NewProjectDetailsDto pjson3 = new NewProjectDetailsDto(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env"
    );

    NewProjectDetailsDto pjson4 = new NewProjectDetailsDto(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env"
    );

    NewProjectDetails p1 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env"
    );

    NewProjectDetails p2 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env"
    );

    NewProjectDetails p3 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env"
    );

    NewProjectDetails p4 = new NewProjectDetails(
        "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env"
    );

    // when
    NewProjectDetails r1 = mapper.mapNewProjectDetails(pjson1);
    NewProjectDetails r2 = mapper.mapNewProjectDetails(pjson2);
    NewProjectDetails r3 = mapper.mapNewProjectDetails(pjson3);
    NewProjectDetails r4 = mapper.mapNewProjectDetails(pjson4);
    NewProjectDetails r5 = mapper.mapNewProjectDetails(null);

    // then
    assertThat(r1).isEqualTo(p1);
    assertThat(r2).isEqualTo(p2);
    assertThat(r3).isEqualTo(p3);
    assertThat(r4).isEqualTo(p4);
    assertThat(r5).isNull();
  }

  @Test
  public void mapProjectDetailChangesShouldMapDataToModel() {
    // given
    ProjectDetailChanges p1 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetail(
        new StringId("z"), "onm", "engineType")), 1
    );

    ProjectDetailChanges p2 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChanges p3 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChanges p4 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    ProjectDetailChangesDto pdto1 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailDto(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetailDto(
        new StringId("z"), "onm", "engineType")), 1
    );

    ProjectDetailChangesDto pdto2 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChangesDto pdto3 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChangesDto pdto4 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    // when
    ProjectDetailChangesDto r1 = mapper.mapProjectDetailChanges(p1);
    ProjectDetailChangesDto r2 = mapper.mapProjectDetailChanges(p2);
    ProjectDetailChangesDto r3 = mapper.mapProjectDetailChanges(p3);
    ProjectDetailChangesDto r4 = mapper.mapProjectDetailChanges(p4);
    ProjectDetailChangesDto r5 = mapper.mapProjectDetailChanges((ProjectDetailChanges) null);

    // then
    assertThat(r1).isEqualTo(pdto1);
    assertThat(r2).isEqualTo(pdto2);
    assertThat(r3).isEqualTo(pdto3);
    assertThat(r4).isEqualTo(pdto4);
    assertThat(r5).isNull();
  }

  @Test
  public void mapProjectDetailChangesShouldMapModelToData() {
    // given
    ProjectDetailChanges p1 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetail(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetail(
        new StringId("z"), "onm", "engineType")), 1
    );

    ProjectDetailChanges p2 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChanges p3 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetails(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChanges p4 = new ProjectDetailChanges(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    ProjectDetailChangesDto pdto1 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            Collections.singletonList(
                new CustomFieldDetailDto(new StringId("cf1"), "cfnm", "tp")
            )
        ), "pst", "env", Collections.singletonList(new OCREngineDetailDto(
        new StringId("z"), "onm", "engineType")), 1
    );

    ProjectDetailChangesDto pdto2 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            Collections.singletonList(null)
        ), "pst", "env", Collections.singletonList(null), 1
    );

    ProjectDetailChangesDto pdto3 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        new FieldDetailsDto(
            Arrays.asList("1", "2"),
            null
        ), "pst", "env", null, 1
    );

    ProjectDetailChangesDto pdto4 = new ProjectDetailChangesDto(
        new StringId("a"), "nm", "d", new StringId("1"), new StringId("pt"), "pt", "pl",
        null, "pst", "env", null, 1
    );

    // when
    ProjectDetailChanges r1 = mapper.mapProjectDetailChanges(pdto1);
    ProjectDetailChanges r2 = mapper.mapProjectDetailChanges(pdto2);
    ProjectDetailChanges r3 = mapper.mapProjectDetailChanges(pdto3);
    ProjectDetailChanges r4 = mapper.mapProjectDetailChanges(pdto4);
    ProjectDetailChanges r5 = mapper.mapProjectDetailChanges((ProjectDetailChangesDto) null);

    // then
    assertThat(r1).isEqualTo(p1);
    assertThat(r2).isEqualTo(p2);
    assertThat(r3).isEqualTo(p3);
    assertThat(r4).isEqualTo(p4);
    assertThat(r5).isNull();
  }

  @Test
  public void mapArchiveDetailsShouldMapModelToData() {
    // given
    ArchiveDetails m1 = new ArchiveDetails("n");
    ArchiveDetailsDto d1 = new ArchiveDetailsDto("n");

    // when
    ArchiveDetailsDto r1 = mapper.mapArchiveDetails(m1);
    ArchiveDetailsDto r2 = mapper.mapArchiveDetails((ArchiveDetails) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapArchiveDetailsShouldMapDataToModel() {
    // given
    ArchiveDetailsDto m1 = new ArchiveDetailsDto("n");
    ArchiveDetails d1 = new ArchiveDetails("n");

    // when
    ArchiveDetails r1 = mapper.mapArchiveDetails(m1);
    ArchiveDetails r2 = mapper.mapArchiveDetails((ArchiveDetailsDto) null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectImportRequestShouldMapDataToModel() {
    // given
    ProjectImportRequestDto d1 = new ProjectImportRequestDto(
        new ArchiveDetailsDto("nm1"), new StringId("1"), "a"
    );
    ProjectImportRequestDto d2 = new ProjectImportRequestDto(
        null, new StringId("1"), "a"
    );
    ProjectImportRequest m1 = new ProjectImportRequest(
        new ArchiveDetails("nm1"), new StringId("1"), "a"
    );
    ProjectImportRequest m2 = new ProjectImportRequest(
        null, new StringId("1"), "a"
    );

    // when
    ProjectImportRequest r1 = mapper.mapProjectImportRequest(d1);
    ProjectImportRequest r2 = mapper.mapProjectImportRequest(d2);
    ProjectImportRequest r3 = mapper.mapProjectImportRequest(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapTaskStatusShouldMapModelToData() {
    // given
    TaskStatus m1 = new TaskStatus(
        new StringId("1"), "tt", "s", "d"
    );

    TaskStatusDto d1 = new TaskStatusDto(
        new StringId("1"), "tt", "s", "d"
    );

    // when
    TaskStatusDto r1 = mapper.mapTaskStatus(m1);
    TaskStatusDto r2 = mapper.mapTaskStatus(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectPatchDetailMapDtoToModel() {
    // given
    ProjectPatchDetailDto d1 = new ProjectPatchDetailDto(
        "tt", "s", "d", 1
    );

    ProjectPatchDetail m1 = new ProjectPatchDetail(
        "tt", "s", "d", 1
    );

    // when
    ProjectPatchDetail r1 = mapper.mapProjectPatchDetail(d1);
    ProjectPatchDetail r2 = mapper.mapProjectPatchDetail(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapProjectExportRequestShouldMapDtoToModel() {
    // given
    ProjectExportRequestDto d1 = new ProjectExportRequestDto(
        "nm", Collections.singletonList("d"), new StringId("1")
    );

    ProjectExportRequestDto d2 = new ProjectExportRequestDto(
        "nm", null, new StringId("1")
    );

    ProjectExportRequest m1 = new ProjectExportRequest(
        "nm", Collections.singletonList("d"), new StringId("1")
    );

    ProjectExportRequest m2 = new ProjectExportRequest(
        "nm", null, new StringId("1")
    );

    // when
    ProjectExportRequest r1 = mapper.mapProjectExportRequest(d1);
    ProjectExportRequest r2 = mapper.mapProjectExportRequest(d2);
    ProjectExportRequest r3 = mapper.mapProjectExportRequest(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapProjectPatchResponseShouldMapModelToDto() {
    //given
    ProjectPatchResponse d1 = new ProjectPatchResponse(1);
    ProjectPatchResponseDto m1 = new ProjectPatchResponseDto(1);

    //when
    ProjectPatchResponseDto r1 = mapper.mapProjectPatchResponse(d1);
    ProjectPatchResponseDto r2 = mapper.mapProjectPatchResponse(null);

    //then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapTaskShouldMapModelToDto() {
    //given
    OffsetDateTime dateTime = OffsetDateTime.now();
    Task d1 = new Task(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        dateTime);
    Task d2 = new Task(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        null);
    TaskDto m1 = new TaskDto(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        dateTime);
    TaskDto m2 = new TaskDto(new StringId("1"), "2", "3", "4", "5", "6", dateTime,
        null);

    //when
    TaskDto r1 = mapper.mapTask(d1);
    TaskDto r2 = mapper.mapTask(d2);
    TaskDto r3 = mapper.mapTask(null);

    //then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }
}
