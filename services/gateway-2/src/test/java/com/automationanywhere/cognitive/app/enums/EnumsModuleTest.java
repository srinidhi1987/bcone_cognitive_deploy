package com.automationanywhere.cognitive.app.enums;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.testng.annotations.Test;

public class EnumsModuleTest {

  @Test
  public void deserializeSimpleClassWithEnum() throws IOException {
    // GIVEN
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new EnumsModule());

    // WHEN
    EnumsModuleTestClass value = mapper.readValue(
        "{\"type\": \"Type_1\"}", EnumsModuleTestClass.class);

    // THEN
    assertThat(value.type).isEqualTo(EnumsModuleTestType.TYPE_1);

    // WHEN
    value = mapper.readValue("{\"type\": \"type_2\"}", EnumsModuleTestClass.class);

    // THEN
    assertThat(value.type).isEqualTo(EnumsModuleTestType.TYPE_2);

    // WHEN
    value = mapper.readValue("{\"type\": \"TyPe_1\"}", EnumsModuleTestClass.class);

    // THEN
    assertThat(value.type).isEqualTo(EnumsModuleTestType.TYPE_1);

    // WHEN
    value = mapper.readValue("{\"type\": \"TYPE_2\"}", EnumsModuleTestClass.class);

    // THEN
    assertThat(value.type).isEqualTo(EnumsModuleTestType.TYPE_2);
  }
}
