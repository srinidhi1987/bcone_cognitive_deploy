package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class JsonPatchActionCopyDataTest {

  @Test
  public void testJsonPatchActionCopyDataEquals() {
    // GIVEN
    JsonPatchActionCopyData data1 = new JsonPatchActionCopyData("/abc", "/ghi");
    JsonPatchActionCopyData data2 = new JsonPatchActionCopyData("/abc", "/ghi");
    JsonPatchActionCopyData data3 = new JsonPatchActionCopyData("/def", "/ghi");
    JsonPatchActionCopyData data4 = new JsonPatchActionCopyData("/abc", "/jkl");

    // WHEN
    boolean equals = data1.equals(data2);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(data3);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data2.equals(data4);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(null);

    // THEN
    assertThat(equals).isFalse();

    // WHEN
    equals = data1.equals(data1);

    // THEN
    assertThat(equals).isTrue();

    // WHEN
    equals = data1.equals(new Object());

    // THEN
    assertThat(equals).isFalse();
  }

  @Test
  public void testJsonPatchActionCopyDataHashCode() {
    // GIVEN
    JsonPatchActionCopyData data1 = new JsonPatchActionCopyData("/abc", "/ghi");
    JsonPatchActionCopyData data2 = new JsonPatchActionCopyData("/abc", "/ghi");
    JsonPatchActionCopyData data3 = new JsonPatchActionCopyData("/def", "/ghi");
    JsonPatchActionCopyData data4 = new JsonPatchActionCopyData("/abc", "/jkl");

    // WHEN
    int data1Hash = data1.hashCode();
    int data2Hash = data2.hashCode();
    int data3Hash = data3.hashCode();
    int data4Hash = data4.hashCode();

    // THEN
    assertThat(data1Hash).isEqualTo(data2Hash);
    assertThat(data1Hash).isNotEqualTo(data3Hash);
    assertThat(data2Hash).isNotEqualTo(data4Hash);
  }
}
