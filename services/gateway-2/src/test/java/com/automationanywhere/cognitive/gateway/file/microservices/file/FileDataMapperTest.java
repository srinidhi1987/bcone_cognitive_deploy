package com.automationanywhere.cognitive.gateway.file.microservices.file;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.Category;
import com.automationanywhere.cognitive.gateway.file.models.CategoryDetail;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.EnvironmentCountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FieldDetail;
import com.automationanywhere.cognitive.gateway.file.models.File;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileCount;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFieldDetailFromClasssifier;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.Summary;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.gateway.file.models.VisionBot;
import com.automationanywhere.cognitive.id.string.StringId;
import java.math.BigDecimal;
import java.util.Collections;
import org.testng.annotations.Test;
import java.util.Date;

public class FileDataMapperTest {
  private final FileDataMapper mapper = new FileDataMapper();
  private static final Date date = new Date();
  private static final String username = "test";
  @Test
  public void mapCategoriesShouldMapDataToModel() {
    // given
    CategoriesData d1 = new CategoriesData(
        1, 2, "s", Collections.singletonList(new CategoryData(
            new StringId("1"), "nm", new CategoryDetailData(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(new FieldDetailData(
                  "f1", BigDecimal.TEN
              ))
            ), new VisionBotData(
                new StringId("15"), "nm2", "cs", true, "lu",
                username,date.toString()
            ), 67, Collections.singletonList(new FileData(
                new StringId("f1"), "nmF", "l", "f", true
            )),
            new FileCountData(17, 89, 99),
            new FileCountData(177, 189, 999)
        ))
    );

    CategoriesData d2 = new CategoriesData(
        1, 2, "s", Collections.singletonList(new CategoryData(
            new StringId("1"), "nm", new CategoryDetailData(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(null)
            ), null, 67, Collections.singletonList(null),
            null,
            null
        ))
    );

    Categories m1 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", new CategoryDetail(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(new FieldDetail(
                  "f1", BigDecimal.TEN
              ))
            ), new VisionBot(
                new StringId("15"), "nm2", "cs", true, "lu",
                username,date.toString()
            ), 67, Collections.singletonList(new File(
                new StringId("f1"), "nmF", "l", "f", true
            )),
            new FileCount(17, 89, 99),
            new FileCount(177, 189, 999)
        ))
    );

    Categories m2 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", new CategoryDetail(
              new StringId("id"), 2, 3, 4,
              Collections.singletonList(null)
            ), null, 67, Collections.singletonList(null),
            null,
            null
        ))
    );

    // when
    Categories r1 = mapper.mapCategories(d1);
    Categories r2 = mapper.mapCategories(d2);
    Categories r3 = mapper.mapCategories(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapCategoriesShouldMapDataToModel2() {
    // given
    CategoriesData d1 = new CategoriesData(
        1, 2, "s", Collections.singletonList(new CategoryData(
            new StringId("1"), "nm", new CategoryDetailData(
              new StringId("id"), 2, 3, 4,
              null
            ), null, 67, null,
            null,
            null
        ))
    );

    CategoriesData d2 = new CategoriesData(
        1, 2, "s", Collections.singletonList(new CategoryData(
            new StringId("1"), "nm", null, null, 67, null,
            null,
            null
        ))
    );

    CategoriesData d3 = new CategoriesData(
        1, 2, "s", Collections.singletonList(null)
    );

    CategoriesData d4 = new CategoriesData(
        1, 2, "s", null
    );

    Categories m1 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", new CategoryDetail(
              new StringId("id"), 2, 3, 4,
              null
            ), null, 67, null,
            null,
            null
        ))
    );

    Categories m2 = new Categories(
        1, 2, "s", Collections.singletonList(new Category(
            new StringId("1"), "nm", null, null, 67, null,
            null,
            null
        ))
    );

    Categories m3 = new Categories(
        1, 2, "s", Collections.singletonList(null)
    );

    Categories m4 = new Categories(
        1, 2, "s", null
    );

    // when
    Categories r1 = mapper.mapCategories(d1);
    Categories r2 = mapper.mapCategories(d2);
    Categories r3 = mapper.mapCategories(d3);
    Categories r4 = mapper.mapCategories(d4);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isEqualTo(m3);
    assertThat(r4).isEqualTo(m4);
  }

  @Test
  public void mapFileDetailsShouldMapDataToModel() {
    // given
    FileDetailsData d1 = new FileDetailsData(
        new StringId("f1"), new StringId("p2"), "fn", "fl", 8, 9, 10,
        "for", true, new StringId("cid"), new StringId("uid"), new StringId("lid"), "prod"
    );

    FileDetails m1 = new FileDetails(
        new StringId("f1"), new StringId("p2"), "fn", "fl", 8, 9, 10,
        "for", true, new StringId("cid"), new StringId("uid"), new StringId("lid"), "prod"
    );


    // when
    FileDetails r1 = mapper.mapFileDetails(d1);
    FileDetails r2 = mapper.mapFileDetails(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapCountStatisticsShouldMapDataToModel() {
    // given
    CountStatisticsData d1 = new CountStatisticsData(
        new StringId("1"), new StringId("2"), 1,
        new EnvironmentCountStatisticsData(12, 89),
        new EnvironmentCountStatisticsData(112, 899)
    );

    CountStatisticsData d2 = new CountStatisticsData(
        new StringId("1"), new StringId("2"), 1,
        null,
        null
    );

    CountStatistics m1 = new CountStatistics(
        new StringId("1"), new StringId("2"), 1,
        new EnvironmentCountStatistics(12, 89),
        new EnvironmentCountStatistics(112, 899)
    );

    CountStatistics m2 = new CountStatistics(
        new StringId("1"), new StringId("2"), 1,
        null,
        null
    );

    // when
    CountStatistics r1 = mapper.mapCountStatistics(d1);
    CountStatistics r2 = mapper.mapCountStatistics(d2);
    CountStatistics r3 = mapper.mapCountStatistics(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapFilePatchDetailsShouldMapModelToData() {
    // given
    FilePatchDetailData d1 = new FilePatchDetailData(
        "a", "b", "c"
    );

    FilePatchDetail m1 = new FilePatchDetail(
        "a", "b", "c"
    );

    // when
    FilePatchDetailData r1 = mapper.mapFilePatchDetails(m1);
    FilePatchDetailData r2 = mapper.mapFilePatchDetails(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFileBlobsShouldMapModelToDto() {
    // given
    FileBlobsData d1 = new FileBlobsData(
        "a", "b"
    );

    FileBlobs m1 = new FileBlobs(
        "a", "b"
    );

    // when
    FileBlobs r1 = mapper.mapFileBlobs(d1);
    FileBlobs r2 = mapper.mapFileBlobs(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapClassificationAnalysisReportShouldMapDataToModel() {
    // given
    ClassificationAnalysisReportData d1 = new ClassificationAnalysisReportData(
        new StringId("a"), "b", Collections.singletonList(null)
    );

    ClassificationAnalysisReportData d2 = new ClassificationAnalysisReportData(
        new StringId("a"), "b", null
    );

    ClassificationAnalysisReport m1 = new ClassificationAnalysisReport(
        new StringId("a"), "b", Collections.singletonList(null)
    );

    ClassificationAnalysisReport m2 = new ClassificationAnalysisReport(
        new StringId("a"), "b", null
    );

    // when
    ClassificationAnalysisReport r1 = mapper.mapClassificationAnalysisReport(d1);
    ClassificationAnalysisReport r2 = mapper.mapClassificationAnalysisReport(d2);
    ClassificationAnalysisReport r3 = mapper.mapClassificationAnalysisReport(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }

  @Test
  public void mapUploadResponseShouldMapDataToModel() {
    // given
    UploadResponseData d1 = new UploadResponseData("k");

    UploadResponse m1 = new UploadResponse("k");

    // when
    UploadResponse r1 = mapper.mapUploadResponse(d1);
    UploadResponse r2 = mapper.mapUploadResponse(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapFileStatusShouldMapDataToModel() {
    // given
    FileStatusData d1 = new FileStatusData(
        new StringId("k"), "v", 1, 2, 3, true
    );

    FileStatus m1 = new FileStatus(
        new StringId("k"), "v", 1, 2, 3, true
    );

    // when
    FileStatus r1 = mapper.mapFileStatus(d1);
    FileStatus r2 = mapper.mapFileStatus(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapLearningInstanceSummaryShouldMapDataToModel() {
    // given
    LearningInstanceSummaryData d1 = new LearningInstanceSummaryData(
        new SummaryData(1, 2, 3),
        new SummaryData(1, 2, 3)
    );

    LearningInstanceSummaryData d2 = new LearningInstanceSummaryData(
        null,
        null
    );

    LearningInstanceSummary m1 = new LearningInstanceSummary(
        new Summary(1, 2, 3),
        new Summary(1, 2, 3)
    );

    LearningInstanceSummary m2 = new LearningInstanceSummary(
        null,
        null
    );

    // when
    LearningInstanceSummary r1 = mapper.mapLearningInstanceSummary(d1);
    LearningInstanceSummary r2 = mapper.mapLearningInstanceSummary(d2);
    LearningInstanceSummary r3 = mapper.mapLearningInstanceSummary(null);

    // then
    assertThat(r1).isEqualTo(m1);
    assertThat(r2).isEqualTo(m2);
    assertThat(r3).isNull();
  }


  @Test
  public void mapRawFileReportFromClassifierDataShouldMapModelToData() {
    // given
    RawFileReportFromClassifierData d1 = new RawFileReportFromClassifierData(
        new StringId("1"),
        Collections.singletonList(new RawFieldDetailFromClasssifierData(
            new StringId("2"), "nm1", true
        ))
    );

    RawFileReportFromClassifierData d2 = new RawFileReportFromClassifierData(
        new StringId("1"),
        Collections.singletonList(null)
    );

    RawFileReportFromClassifierData d3 = new RawFileReportFromClassifierData(
        new StringId("1"),
        Collections.emptyList()
    );

    RawFileReportFromClassifier m1 = new RawFileReportFromClassifier(
        new StringId("1"),
        Collections.singletonList(new RawFieldDetailFromClasssifier(
            new StringId("2"), "nm1", true
        ))
    );

    RawFileReportFromClassifier m2 = new RawFileReportFromClassifier(
        new StringId("1"),
        Collections.singletonList(null)
    );

    RawFileReportFromClassifier m3 = new RawFileReportFromClassifier(
        new StringId("1"), null
    );

    // when
    RawFileReportFromClassifierData r1 = mapper.mapRawFileReportFromClassifierData(m1);
    RawFileReportFromClassifierData r2 = mapper.mapRawFileReportFromClassifierData(m2);
    RawFileReportFromClassifierData r3 = mapper.mapRawFileReportFromClassifierData(m3);
    RawFileReportFromClassifierData r4 = mapper.mapRawFileReportFromClassifierData(null);

    // then
    assertThat(r1).isEqualTo(d1);
    assertThat(r2).isEqualTo(d2);
    assertThat(r3).isEqualTo(d3);
    assertThat(r4).isNull();
  }
}
