package com.automationanywhere.cognitive.validation;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.automationanywhere.cognitive.app.id.IdModule;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.auth.adapters.rest.CredentialsDto;
import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.integer.LongIdTransformer;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator.JsonSchemaValidator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.FileSystems;
import java.util.Collections;
import org.springframework.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import sun.misc.URLClassPath;

public class ClasspathSchemaValidatorTest {

  private ObjectMapper om;

  @BeforeMethod
  public void setUp() {
    om = new ObjectMapper();
    UUIDIdTransformer uuidIdTransformer = new UUIDIdTransformer();
    StringIdTransformer stringIdTransformer = new StringIdTransformer();
    LongIdTransformer longIdTransformer = new LongIdTransformer();
    om.registerModule(new IdModule(new CompositeIdTransformer(
        uuidIdTransformer, longIdTransformer, stringIdTransformer
    ), uuidIdTransformer, longIdTransformer, stringIdTransformer));
  }

  @Test
  public void testChecks() throws Exception {
    // GIVEN
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator", ".json", om, new ExceptionHandlerFactory()
    );

    // WHEN
    v.loadJsonSchemas();

    // THEN
    assertThat(v.checkKey("Credentials")).isTrue();
    assertThat(v.checkInput(om.writeValueAsString(
        new CredentialsDto("un", "pw")
    ), "Credentials")).isTrue();
    assertThat(v.checkOutput(
        new CredentialsDto("un", "pw"),
        "Credentials"
    )).isTrue();
  }

  @Test
  public void testErrors() {
    // GIVEN
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/com/automationanywhere/cognitive/validation", ".class", om,
        new ExceptionHandlerFactory());

    Exception e = new Exception("Test");
    JsonSchemaValidator vv = v.new JsonSchemaValidator() {
      @Override
      protected JsonNode load() throws Exception {
        throw e;
      }
    };

    // WHEN
    Throwable throwable = catchThrowable(v::loadJsonSchemas);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessageStartingWith("Failed to load schema from");

    // WHEN
    throwable = catchThrowable(() -> v.checkKey("key"));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessage("Key key is unknown");

    // WHEN
    throwable = catchThrowable(() -> vv.check("k", HttpStatus.BAD_REQUEST));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasCause(e);
  }

  @Test
  public void testChecksFromJar() throws Exception {
    // GIVEN
    ClassLoader classLoader = getClass().getClassLoader();
    Field ucpField = classLoader.getClass().getDeclaredField("ucp");
    ucpField.setAccessible(true);
    URLClassPath ucp = (URLClassPath) ucpField.get(classLoader);
    URL jar = classLoader.getResource("classpath-schema-validator-test.jar");
    ucp.addURL(jar);

    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator-jar",
        ".json",
        om,
        new ExceptionHandlerFactory()
    );

    // WHEN
    v.loadJsonSchemas();

    // THEN
    assertThat(v.checkKey("Credentials")).isTrue();
    assertThat(v.checkInput(om.writeValueAsString(
        new CredentialsDto("un", "pw")
    ), "Credentials")).isTrue();
    assertThat(v.checkOutput(
        new CredentialsDto("un", "pw"),
        "Credentials"
    )).isTrue();
  }

  @Test
  public void testErrorsFromJar() throws Exception {
    // GIVEN
    ClassLoader classLoader = getClass().getClassLoader();
    Field ucpField = classLoader.getClass().getDeclaredField("ucp");
    ucpField.setAccessible(true);
    URLClassPath ucp = (URLClassPath) ucpField.get(classLoader);
    URL jar = classLoader.getResource("classpath-schema-validator-test.jar");
    ucp.addURL(jar);

    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator-jar", ".class", om,
        new ExceptionHandlerFactory());

    Exception e = new Exception("Test");
    JsonSchemaValidator vv = v.new JsonSchemaValidator() {
      @Override
      protected JsonNode load() throws Exception {
        throw e;
      }
    };

    // WHEN
    Throwable throwable = catchThrowable(v::loadJsonSchemas);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessageStartingWith("Failed to load schema from");

    // WHEN
    throwable = catchThrowable(() -> v.checkKey("key"));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessage("Key key is unknown");

    // WHEN
    throwable = catchThrowable(() -> vv.check("k", HttpStatus.BAD_REQUEST));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasCause(e);
  }

  @Test
  public void testResourceNotFoundThrowException() {
    // GIVEN
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/not-found-prefix", ".json", om,
        new ExceptionHandlerFactory());

    // WHEN
    Throwable throwable = catchThrowable(v::loadJsonSchemas);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(IOException.class)
        .hasMessage("Resource not found with prefix: /not-found-prefix");
  }

  @Test
  public void testFileSystemAlreadyExistsException() throws Exception {
    // GIVEN
    ClassLoader classLoader = getClass().getClassLoader();
    Field ucpField = classLoader.getClass().getDeclaredField("ucp");
    ucpField.setAccessible(true);
    URLClassPath ucp = (URLClassPath) ucpField.get(classLoader);
    URL jar = classLoader.getResource("classpath-schema-validator-test.jar");
    ucp.addURL(jar);

    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator-jar", ".class", om,
        new ExceptionHandlerFactory());

    URL resource = ClasspathSchemaValidator.class.getResource("/classpath-schema-validator-jar");
    try (FileSystem fileSystem = FileSystems.newFileSystem(resource.toURI(), Collections.emptyMap())) {

      // WHEN
      Throwable throwable = catchThrowable(v::loadJsonSchemas);

      // THEN
      assertThat(throwable).isNotNull().isInstanceOf(FileSystemAlreadyExistsException.class);
    }
  }
}
