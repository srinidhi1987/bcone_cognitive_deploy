package com.automationanywhere.cognitive.gateway.application.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.gateway.application.microservices.application.AcknowledgementData;
import com.automationanywhere.cognitive.gateway.application.microservices.application.ApplicationDataMapper;
import com.automationanywhere.cognitive.gateway.application.microservices.application.ConfigurationData;
import com.automationanywhere.cognitive.gateway.application.microservices.application.HandshakeData;
import com.automationanywhere.cognitive.gateway.application.microservices.application.RegistrationData;
import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import org.testng.annotations.Test;

public class ApplicationDtoMapperTest {

  private final ApplicationDtoMapper mapper = new ApplicationDtoMapper();

  @Test
  public void mapAcknowledgementShouldMapModelToData() {
    // given
    AcknowledgementDto d1 = new AcknowledgementDto(
        "a"
    );

    Acknowledgement m1 = new Acknowledgement(
        "a"
    );

    // when
    Acknowledgement r1 = mapper.mapAcknowledgement(d1);
    Acknowledgement r2 = mapper.mapAcknowledgement(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapRegistrationShouldMapModelToData() {
    // given
    RegistrationDto d1 = new RegistrationDto(
        "a", "b", "c"
    );

    Registration m1 = new Registration(
        "a", "b", "c"
    );

    // when
    Registration r1 = mapper.mapRegistration(d1);
    Registration r2 = mapper.mapRegistration(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(m1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapConfigurationShouldMapModelToData() {
    // given
    ConfigurationDto d1 = new ConfigurationDto(
        "a", "b"
    );

    Configuration m1 = new Configuration(
        "a", "b"
    );

    // when
    ConfigurationDto r1 = mapper.mapConfiguration(m1);
    ConfigurationDto r2 = mapper.mapConfiguration(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(d1);
    assertThat(r2).isNull();
  }

  @Test
  public void mapHandshakeShouldMapModelToData() {
    // given
    HandshakeDto d1 = new HandshakeDto(
        "a", "b", "c"
    );

    Handshake m1 = new Handshake(
        "a", "b", "c"
    );

    // when
    HandshakeDto r1 = mapper.mapHandshake(m1);
    HandshakeDto r2 = mapper.mapHandshake(null);

    // then
    assertThat(r1).isEqualToComparingFieldByFieldRecursively(d1);
    assertThat(r2).isNull();
  }
}
