package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.log4j2;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.automationanywhere.cognitive.gateway.auth.models.JWTAuthorization;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Log4J2AuditTest {
  private Log4J2Audit a;
  private Log4J2AuditingDetailsProvider p;

  @BeforeMethod
  public void setUp() throws Exception {
    p = spy(new Log4J2AuditingDetailsProvider());
    a = new Log4J2Audit(p);
  }

  @Test
  public void testSupportedAuditEvent() {
    // given
    JWTAuthorization e = new JWTAuthorization(
        "act", "token"
    );

    // when
    a.audit(e);

    // then
    verify(p).extract(e);
  }

  @Test
  public void testUnsupportedAuditEvent() {
    // given
    Object e = new Object();

    // when
    a.audit(e);

    // then
    verify(p).extract(e);
  }
}
