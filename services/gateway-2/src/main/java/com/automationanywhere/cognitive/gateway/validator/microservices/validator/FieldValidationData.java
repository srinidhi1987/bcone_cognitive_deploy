package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FieldValidationData {

  private final String fieldId;
  private final double percentValidated;

  public FieldValidationData(
      @JsonProperty("fieldId") String fieldId,
      @JsonProperty("percentValidated") double percentValidated
  ) {
    this.fieldId = fieldId;
    this.percentValidated = percentValidated;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the percentValidated.
   *
   * @return the value of percentValidated
   */
  public double getPercentValidated() {
    return percentValidated;
  }
}
