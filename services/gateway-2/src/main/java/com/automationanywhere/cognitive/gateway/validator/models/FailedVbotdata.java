package com.automationanywhere.cognitive.gateway.validator.models;

public class FailedVbotdata {

  private final int fileId;
  private final int orgId;
  private final int projectId;
  private final String vBotDocument;
  private final String correctedData;
  private final String validationStatus;
  private final String createdAt;
  private final String updatedBy;
  private final String updatedAt;
  private final String visionBotId;
  private final String lockedUserId;
  private final int failedFieldCounts;
  private final int passFieldCounts;
  private final int totalFieldCounts;
  private final String startTime;
  private final String endTime;
  private final int averageTimeInSeconds;
  private final String lastExportTime;

  public FailedVbotdata(
      int fileId,
      int orgId,
      int projectId,
      String vBotDocument,
      String correctedData,
      String validationStatus,
      String createdAt,
      String updatedBy,
      String updatedAt,
      String visionBotId,
      String lockedUserId,
      int failedFieldCounts,
      int passFieldCounts,
      int totalFieldCounts,
      String startTime,
      String endTime,
      int averageTimeInSeconds,
      String lastExportTime
  ) {
    this.fileId = fileId;
    this.orgId = orgId;
    this.projectId = projectId;
    this.vBotDocument = vBotDocument;
    this.correctedData = correctedData;
    this.validationStatus = validationStatus;
    this.createdAt = createdAt;
    this.updatedBy = updatedBy;
    this.updatedAt = updatedAt;
    this.visionBotId = visionBotId;
    this.lockedUserId = lockedUserId;
    this.failedFieldCounts = failedFieldCounts;
    this.passFieldCounts = passFieldCounts;
    this.totalFieldCounts = totalFieldCounts;
    this.startTime = startTime;
    this.endTime = endTime;
    this.averageTimeInSeconds = averageTimeInSeconds;
    this.lastExportTime = lastExportTime;
  }

  /**
   * Returns the fileId.
   *
   * @return the value of fileId
   */
  public int getFileId() {
    return fileId;
  }

  /**
   * Returns the orgId.
   *
   * @return the value of orgId
   */
  public int getOrgId() {
    return orgId;
  }

  /**
   * Returns the projectId.
   *
   * @return the value of projectId
   */
  public int getProjectId() {
    return projectId;
  }

  /**
   * Returns the vBotDocument.
   *
   * @return the value of vBotDocument
   */
  public String getvBotDocument() {
    return vBotDocument;
  }

  /**
   * Returns the correctedData.
   *
   * @return the value of correctedData
   */
  public String getCorrectedData() {
    return correctedData;
  }

  /**
   * Returns the validationStatus.
   *
   * @return the value of validationStatus
   */
  public String getValidationStatus() {
    return validationStatus;
  }

  /**
   * Returns the createdAt.
   *
   * @return the value of createdAt
   */
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   * Returns the updatedBy.
   *
   * @return the value of updatedBy
   */
  public String getUpdatedBy() {
    return updatedBy;
  }

  /**
   * Returns the updatedAt.
   *
   * @return the value of updatedAt
   */
  public String getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Returns the visionBotId.
   *
   * @return the value of visionBotId
   */
  public String getVisionBotId() {
    return visionBotId;
  }

  /**
   * Returns the lockedUserId.
   *
   * @return the value of lockedUserId
   */
  public String getLockedUserId() {
    return lockedUserId;
  }

  /**
   * Returns the failedFieldCounts.
   *
   * @return the value of failedFieldCounts
   */
  public int getFailedFieldCounts() {
    return failedFieldCounts;
  }

  /**
   * Returns the passFieldCounts.
   *
   * @return the value of passFieldCounts
   */
  public int getPassFieldCounts() {
    return passFieldCounts;
  }

  /**
   * Returns the totalFieldCounts.
   *
   * @return the value of totalFieldCounts
   */
  public int getTotalFieldCounts() {
    return totalFieldCounts;
  }

  /**
   * Returns the startTime.
   *
   * @return the value of startTime
   */
  public String getStartTime() {
    return startTime;
  }

  /**
   * Returns the endTime.
   *
   * @return the value of endTime
   */
  public String getEndTime() {
    return endTime;
  }

  /**
   * Returns the averageTimeInSeconds.
   *
   * @return the value of averageTimeInSeconds
   */
  public int getAverageTimeInSeconds() {
    return averageTimeInSeconds;
  }

  /**
   * Returns the lastExportTime.
   *
   * @return the value of lastExportTime
   */
  public String getLastExportTime() {
    return lastExportTime;
  }
}
