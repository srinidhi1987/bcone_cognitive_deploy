package com.automationanywhere.cognitive.healthcheck;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.common.healthcheck.commands.MicroserviceTextHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Alias Service health check command.
 */
@Component
@SuppressWarnings("unused")
public class AliasHealthCheck extends MicroserviceTextHealthCheckCommand {

  @Autowired
  public AliasHealthCheck(
      final ApplicationConfiguration config,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory,
      final AsyncRestTemplate restTemplate
  ) {
    super(
        "Alias",
        Builder.get(config.aliasServiceURL()).build().url,
        buildInfoFactory,
        appInfoFactory,
        restTemplate
    );
  }
}
