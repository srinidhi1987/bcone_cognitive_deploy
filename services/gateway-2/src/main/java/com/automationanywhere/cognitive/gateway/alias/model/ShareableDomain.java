package com.automationanywhere.cognitive.gateway.alias.model;

import com.automationanywhere.cognitive.gateway.alias.ShareableDomainInfo;

import javax.annotation.Nonnull;

import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable data transfer object holding the shareable representation of
 * the domain metadata for a Domain.
 *
 * @author Erik K. Worth
 */
public class ShareableDomain implements ShareableDomainInfo {

  /** The name of the domain such as "Invoices" */
  private final String name;

  /** The base64 encoded representation of the binary domain metadata */
  private final String domain;

  /** The version of the domain metadata encoding */
  private final int version;

  /**
   * Construct from elements.
   *
   * @param name the name of the domain such as "Invoices"
   * @param domain the base64 encoded representation of the binary domain metadata
   * @param version the version of the domain metadata encoding
   */
  public ShareableDomain(
          @Nonnull final String name,
          @Nonnull final String domain,
          final int version) {
    this.name = checkNotNull(name, "name must not be null");
    this.domain = checkNotNull(domain, "domain must not be null");
    this.version = version;
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    final ShareableDomain that = (ShareableDomain) other;
    return version == that.version
            && Objects.equals(name, that.name)
            && Objects.equals(domain, that.domain);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, domain, version);
  }

  /**
   * Returns the domain name such as "Invoices".
   *
   * @return the domain name such as "Invoices"
   */
  @Override
  @Nonnull
  public String getName() {
    return name;
  }

  /**
   * Returns the base64 encoded representation of the binary domain metadata.
   *
   * @return the base64 encoded representation of the binary domain metadata
   */
  @Override
  @Nonnull
  public String getDomain() {
    return domain;
  }

  /**
   * Returns the version of the encoded content as an integer.
   *
   * @return the version of the encoded content as an integer
   */
  @Override
  public int getVersion() {
    return version;
  }
}
