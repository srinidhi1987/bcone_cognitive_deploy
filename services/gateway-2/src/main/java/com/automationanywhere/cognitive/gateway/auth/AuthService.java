package com.automationanywhere.cognitive.gateway.auth;

import com.automationanywhere.cognitive.gateway.auth.microservices.auth.AuthMicroservice;
import com.automationanywhere.cognitive.gateway.auth.microservices.validator.AuthValidatorMicroservice;
import com.automationanywhere.cognitive.gateway.auth.microservices.visionbot.AuthVisionBotMicroservice;
import com.automationanywhere.cognitive.gateway.auth.models.Authentication;
import com.automationanywhere.cognitive.gateway.auth.models.Authorization;
import com.automationanywhere.cognitive.gateway.auth.models.Credentials;
import com.automationanywhere.cognitive.gateway.auth.models.InvalidToken;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

  private final AuthMicroservice authMicroservice;
  private final AuthValidatorMicroservice validatorMicroservice;
  private final AuthVisionBotMicroservice visionBotMicroservice;

  @Autowired
  public AuthService(
      final AuthMicroservice authMicroservice,
      final AuthValidatorMicroservice validatorMicroservice,
      final AuthVisionBotMicroservice visionBotMicroservice
  ) {
    this.authMicroservice = authMicroservice;
    this.validatorMicroservice = validatorMicroservice;
    this.visionBotMicroservice = visionBotMicroservice;
  }

  @Async
  public CompletableFuture<Authentication> authenticate(final Credentials credentials) {
    return authMicroservice.authenticate(credentials);
  }

  @Async
  public CompletableFuture<Void> authorize(final Authorization authorization) {
    return authMicroservice.authorize(authorization);
  }

  @Async
  public CompletableFuture<Void> logout(
      final InvalidToken token,
      final String username
  ) {
    return CompletableFuture.allOf(
        authMicroservice.invalidateSession(token),
        validatorMicroservice.unlockDocuments(username),
        visionBotMicroservice.unlockVisionBots(username)
    );
  }
}
