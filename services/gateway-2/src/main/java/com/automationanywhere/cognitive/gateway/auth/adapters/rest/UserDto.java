package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class UserDto {

  public final Id id;
  public final String name;
  public final List<String> roles;

  public UserDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("roles") final List<String> roles
  ) {
    this.id = id;
    this.name = name;
    this.roles = roles;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      UserDto user = (UserDto) o;

      result = Objects.equals(id, user.id)
          && Objects.equals(name, user.name)
          && Objects.equals(roles, user.roles);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(roles);
  }
}
