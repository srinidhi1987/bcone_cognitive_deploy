package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.dataresource.DataContainer;
import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class FileMicroservice {

  private final FileDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public FileMicroservice(
      final FileDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  RestClient getRestClient() {
    return restClient;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Categories> getOrganizationProjectCategories(
      final Id organizationId, final Id projectId,
      final Integer offset, final Integer limit, final String sort
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectCategories())
            .param(organizationId)
            .param(projectId)
            .param(offset)
            .param(limit)
            .param(sort)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<CategoriesData>() {
        }
    ).thenApply(mapper::mapCategories);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<FileDetails>> getProjectCategoryFiles(
      final Id organizationId,
      final Id projectId,
      final Id classificationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectCategoryFiles())
            .param(organizationId)
            .param(projectId)
            .param(classificationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<CountStatistics>> getFileCount(
      final Id organizationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationFileCount())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<CountStatisticsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapCountStatistics).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<CountStatistics>> getProjectFileCount(
      final Id organizationId, final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectFileCount())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<CountStatisticsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapCountStatistics).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<FileDetails>> getProjectFiles(
      final Id organizationId, final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectFiles())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<FileDetails>> getProjectStagingFiles(
      final Id organizationId, final Id projectId, final Id categoryId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectStagingFiles())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<FileDetails>> getProjectFile(
      final Id organizationId, final Id projectId, final Id fileId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectFile())
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<FileDetailsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> patchProjectFile(
      final Id organizationId, final Id projectId, final Id fileId,
      final List<FilePatchDetail> filePatchDetails
  ) {
    return getRestClient().send(
        RequestDetails.Builder.patch(cfg.fileURL())
            .request(cfg.patchOrganizationProjectFile())
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .content(
                filePatchDetails.stream().map(mapper::mapFilePatchDetails)
                    .collect(Collectors.toList()),
                Types.JSON
            )
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> deleteProjectFile(
      final Id organizationId, final Id projectId, final Id categoryId, final Id fileId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.delete(cfg.fileURL())
            .request(cfg.deleteOrganizationProjectFile())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(fileId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<FileBlobs>> getFileBlobs(
      final Id organizationId,
      final Id projectId,
      final Id fileId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectFileBlobs())
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<FileBlobsData>>() {
        }
    ).thenApply(list -> list == null ? Collections.emptyList()
        : list.stream().map(mapper::mapFileBlobs).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ClassificationAnalysisReport> getProjectReport(
      final Id organizationId,
      final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectReport())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ClassificationAnalysisReportData>() {
        }
    ).thenApply(mapper::mapClassificationAnalysisReport);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<FileStatus> getProjectFileStatus(
      final Id organizationId,
      final Id projectId,
      final Id requestId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectFileStatus())
            .param(organizationId)
            .param(projectId)
            .param(requestId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<FileStatusData>() {
        }
    ).thenApply(mapper::mapFileStatus);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> createSegmentedDocument(
      final Id fileId,
      final String fileContent
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.createSegmentedDocument())
            .param(fileId)
            .content(fileContent, Types.TEXT)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<String>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> getSegmentedDocument(final Id fileId) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getSegmentedDocument())
            .param(fileId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<String>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<UploadResponse> uploadFiles(
      final Id organizationId,
      final Id projectId,
      final String isProduction,
      final Id requestId,
      final List<DataContainer> data
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.fileURL())
            .request(cfg.uploadOrganizationProjectFiles())
            .param(organizationId)
            .param(projectId)
            .param(isProduction)
            .param(requestId)
            .content(data)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<UploadResponseData>() {
        }
    ).thenApply(mapper::mapUploadResponse);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<UploadResponse> uploadStagingFiles(
      final Id organizationId,
      final Id projectId,
      final Id requestId,
      final List<DataContainer> data
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.fileURL())
            .request(cfg.uploadOrganizationProjectStagingFiles())
            .param(organizationId)
            .param(projectId)
            .param(requestId)
            .content(data)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<UploadResponseData>() {
        }
    ).thenApply(mapper::mapUploadResponse);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<LearningInstanceSummary> getProjectClassificationSummary(
      final Id organizationId, final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.fileURL())
            .request(cfg.getOrganizationProjectClassificationSummary())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<LearningInstanceSummaryData>() {
        }
    ).thenApply(mapper::mapLearningInstanceSummary);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> updateProjectClassification(
      final Id organizationId,
      final Id projectId,
      final Id fileId,
      final Id layoutId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.fileURL())
            .request(cfg.updateOrganizationProjectClassification())
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .param(layoutId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> updateProjectClassificationById(
      final Id organizationId,
      final Id projectId,
      final Id fileId,
      final Id layoutId,
      final Id id,
      final RawFileReportFromClassifier data
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.fileURL())
            .request(cfg.updateOrganizationProjectClassificationById())
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .param(layoutId)
            .param(id)
            .content(mapper.mapRawFileReportFromClassifierData(data), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    );
  }
}
