package com.automationanywhere.cognitive.gateway.validator.models;

public class FieldValidation {

  private final String fieldId;
  private final double percentValidated;

  public FieldValidation(
      String fieldId,
      double percentValidated
  ) {
    this.fieldId = fieldId;
    this.percentValidated = percentValidated;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the percentValidated.
   *
   * @return the value of percentValidated
   */
  public double getPercentValidated() {
    return percentValidated;
  }
}
