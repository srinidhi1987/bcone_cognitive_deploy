package com.automationanywhere.cognitive.gateway.validator.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class InvalidFileType {

  public final Id id;
  public final String reason;

  public InvalidFileType(
      final Id id,
      final String reason
  ) {
    this.id = id;
    this.reason = reason;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      InvalidFileType that = (InvalidFileType) o;

      result = Objects.equals(id, that.id)
          && Objects.equals(reason, that.reason);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(reason);
  }
}
