package com.automationanywhere.cognitive.dataresource;

import java.io.IOException;
import org.springframework.core.io.InputStreamResource;

/**
 * This resource implementation takes data from a data container.
 */
public class DataResource extends InputStreamResource {
  private final DataContainer dataContainer;

  public DataResource(final DataContainer dataContainer) {
    super(dataContainer.data);

    this.dataContainer = dataContainer;
  }

  @Override
  public String getFilename() {
    return dataContainer.name;
  }

  @Override
  public long contentLength() throws IOException {
    return dataContainer.contentLength == null ? -1 : dataContainer.contentLength;
  }
}
