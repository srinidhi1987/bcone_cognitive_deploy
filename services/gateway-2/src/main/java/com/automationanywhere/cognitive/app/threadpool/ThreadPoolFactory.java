package com.automationanywhere.cognitive.app.threadpool;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Creates a thread pool.
 */
public class ThreadPoolFactory {

  private static final int DEFAULT_QUEUE_CAPACITY = 1024;

  public ThreadPoolTaskExecutor create(
      final int corePoolSize,
      final int maxPoolSize,
      final int keepAliveTimeout,
      final int shutdownTimeout,
      final String prefix
  ) {
    ThreadPoolTaskExecutor threadPoolTaskExecutor = create();
    threadPoolTaskExecutor.setTaskDecorator(new LogContextPropagatingTaskDecorator());
    threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
    threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
    threadPoolTaskExecutor.setKeepAliveSeconds(keepAliveTimeout);
    threadPoolTaskExecutor.setQueueCapacity(DEFAULT_QUEUE_CAPACITY);
    threadPoolTaskExecutor.setThreadNamePrefix(prefix);
    threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(false);
    threadPoolTaskExecutor.setAwaitTerminationSeconds(shutdownTimeout);
    threadPoolTaskExecutor.initialize();
    return threadPoolTaskExecutor;
  }

  ThreadPoolTaskExecutor create() {
    return new ThreadPoolTaskExecutor();
  }
}
