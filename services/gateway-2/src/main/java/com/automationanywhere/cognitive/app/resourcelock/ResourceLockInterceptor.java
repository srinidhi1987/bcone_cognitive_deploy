package com.automationanywhere.cognitive.app.resourcelock;

import com.automationanywhere.cognitive.path.PathManager;
import com.automationanywhere.cognitive.resourcelock.ExclusiveResource;
import com.automationanywhere.cognitive.resourcelock.LockedException;
import com.automationanywhere.cognitive.resourcelock.Resource;
import com.automationanywhere.cognitive.resourcelock.ResourceFactory;
import com.automationanywhere.cognitive.resourcelock.ResourceLocker;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.MessageFormat;
import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ResourceLockInterceptor extends HandlerInterceptorAdapter {
  private static final Logger LOGGER = LogManager.getLogger(ResourceLockInterceptor.class);

  private static final String LOCKED_RESOURCE_ATTR_NAME = "LOCKED_RESOURCE_ATTR";

  private final PathManager pathManager;
  private final ResourceFactory resourceFactory;
  private final ResourceLocker resourceLocker;
  private final ObjectMapper om;

  public ResourceLockInterceptor(
      final PathManager pathManager,
      final ResourceFactory resourceFactory,
      final ResourceLocker resourceLocker,
      final ObjectMapper om
  ) {
    this.pathManager = pathManager;
    this.resourceFactory = resourceFactory;
    this.resourceLocker = resourceLocker;
    this.om = om;
  }

  @Override
  public boolean preHandle(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final Object handler
  ) throws Exception {
    boolean cancel = false;

    if (request.getDispatcherType() == DispatcherType.REQUEST) {
      HandlerMethod handlerMethod = (HandlerMethod) handler;

      String path = pathManager.create(request);
      Resource resource = resourceFactory.create(path, handlerMethod);
      HttpMethod httpMethod = HttpMethod.resolve(request.getMethod().toUpperCase());

      if (resource instanceof ExclusiveResource || httpMethod != HttpMethod.GET) {
        try {
          resourceLocker.lock(resource);

          request.setAttribute(LOCKED_RESOURCE_ATTR_NAME, resource);
        } catch (final LockedException ex) {
          LOGGER.error("Failed to acquire lock for " + resource.path);

          response.setStatus(HttpStatus.LOCKED.value());
          response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

          LockErrorResponse lockErrorResponse = new LockErrorResponse(
              MessageFormat.format(
                  "Learning Instance {0} is currently in progress. Please retry this action after some time.",
                  ex.activityName
              ), HttpStatus.LOCKED.value()
          );
          response.getWriter().write(om.writeValueAsString(lockErrorResponse));

          cancel = true;
        }
      }
    }

    return !cancel;
  }

  @Override
  public void afterCompletion(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final Object handler,
      final Exception error
  ) throws Exception {
    Object resource = request.getAttribute(LOCKED_RESOURCE_ATTR_NAME);
    if (resource != null) {
      if (!(resource instanceof ExclusiveResource)
          || !HttpStatus.valueOf(response.getStatus()).is2xxSuccessful()) {
        unlock((Resource) resource);
      } else {
        ExclusiveResource exclusiveResource = (ExclusiveResource) resource;
        try {
          resourceLocker.start(exclusiveResource);
        } catch (final Exception ex) {
          LOGGER.error("Failed to start task status polling for " + exclusiveResource.path, ex);
          unlock((Resource) resource);
        }
      }
    }
  }

  private void unlock(final Resource resource) {
    try {
      resourceLocker.unlock(resource);
    } catch (final Exception ex) {
      LOGGER.error("Failed to unlock " + resource.path, ex);
    }
  }
}
