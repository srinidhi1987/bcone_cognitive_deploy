package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

public enum JsonPatchActionOperation {

  ADD, REPLACE, REMOVE, TEST, COPY, MOVE;
}
