package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotData {

  public final Id id;
  public final String name;
  public final String currentState;

  public VisionBotData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("currentState") final String currentState
  ) {
    this.id = id;
    this.name = name;
    this.currentState = currentState;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotData visionBot = (VisionBotData) o;

      result = Objects.equals(id, visionBot.id)
          && Objects.equals(name, visionBot.name)
          && Objects.equals(currentState, visionBot.currentState);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(currentState);
  }
}

