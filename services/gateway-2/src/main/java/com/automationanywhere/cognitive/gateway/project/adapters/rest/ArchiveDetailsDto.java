package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ArchiveDetailsDto {
  public final String archiveName;

  public ArchiveDetailsDto(
      @JsonProperty("archiveName") final String archiveName
  ) {
    this.archiveName = archiveName;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ArchiveDetailsDto that = (ArchiveDetailsDto) o;

      result = Objects.equals(archiveName, that.archiveName);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archiveName) ^ 32;
  }
}
