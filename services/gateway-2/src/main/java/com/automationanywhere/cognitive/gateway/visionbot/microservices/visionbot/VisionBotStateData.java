package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotStateData {

  public final String state;

  public VisionBotStateData(
      @JsonProperty("state") final String state
  ) {
    this.state = state;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotStateData visionBotState = (VisionBotStateData) o;

      result = Objects.equals(state, visionBotState.state);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(state);
  }
}

