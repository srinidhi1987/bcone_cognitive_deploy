package com.automationanywhere.cognitive.app.id.converters;

import com.automationanywhere.cognitive.id.integer.LongId;
import com.automationanywhere.cognitive.id.integer.LongIdTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LongIdConverter extends BaseIdConverter<LongId> {

  @Autowired
  public LongIdConverter(final LongIdTransformer idTransformer) {
    super(idTransformer);
  }
}
