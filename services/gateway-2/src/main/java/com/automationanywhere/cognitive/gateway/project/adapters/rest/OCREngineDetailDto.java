package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class OCREngineDetailDto {

  public final Id id;
  public final String name;
  public final String engineType;

  public OCREngineDetailDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("engineType") final String engineType
  ) {
    this.id = id;
    this.name = name;
    this.engineType = engineType;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      OCREngineDetailDto oCREngineDetail = (OCREngineDetailDto) o;

      result = Objects.equals(id, oCREngineDetail.id)
          && Objects.equals(engineType, oCREngineDetail.engineType)
          && Objects.equals(name, oCREngineDetail.name);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(engineType)
        ^ Objects.hashCode(name);
  }
}

