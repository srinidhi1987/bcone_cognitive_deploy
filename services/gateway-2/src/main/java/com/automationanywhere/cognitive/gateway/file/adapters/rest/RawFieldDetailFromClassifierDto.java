package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class RawFieldDetailFromClassifierDto {

  public final Id fieldId;
  public final String aliasName;
  @JsonProperty("isFound")
  public final boolean found;

  public RawFieldDetailFromClassifierDto(
      @JsonProperty("fieldId") final Id fieldId,
      @JsonProperty("aliasName") final String aliasName,
      @JsonProperty("isFound") final boolean found
  ) {
    this.fieldId = fieldId;
    this.aliasName = aliasName;
    this.found = found;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      RawFieldDetailFromClassifierDto that = (RawFieldDetailFromClassifierDto) o;

      result = Objects.equals(found, that.found)
          && Objects.equals(fieldId, that.fieldId)
          && Objects.equals(aliasName, that.aliasName);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fieldId)
        ^ Objects.hashCode(aliasName)
        ^ Objects.hashCode(found);
  }
}
