package com.automationanywhere.cognitive.gateway.project;

import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectMicroservice;
import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

  private final ProjectMicroservice projectMicroservice;

  @Autowired
  public ProjectService(final ProjectMicroservice projectMicroservice) {
    this.projectMicroservice = projectMicroservice;
  }

  @Async
  public CompletableFuture<List<ProjectDetails>> getOrganizationProjects(
      final Id organizationId, final List<String> excludeFields
  ) {
    return projectMicroservice.getOrganizationProjects(organizationId, excludeFields);
  }

  @Async
  public CompletableFuture<List<ProjectDetails>> getOrganizationProjectMetadataList(
      final Id organizationId
  ) {
    return projectMicroservice.getOrganizationProjectMetadataList(organizationId);
  }

  @Async
  public CompletableFuture<ProjectDetails> getOrganizationProject(
      final Id organizationId, final Id projectId, final String excludeFields
  ) {
    return projectMicroservice.getOrganizationProject(organizationId, projectId, excludeFields);
  }

  @Async
  public CompletableFuture<ProjectDetailChanges> getOrganizationProjectMetadata(
      final Id organizationId, final Id projectId
  ) {
    return projectMicroservice.getOrganizationProjectMetadata(organizationId, projectId);
  }

  @Async
  public CompletableFuture<ProjectDetailChanges> createOrganizationProject(
      final Id organizationId, final NewProjectDetails projectDetails
  ) {
    return projectMicroservice.createOrganizationProject(organizationId, projectDetails);
  }

  @Async
  public CompletableFuture<Void> updateOrganizationProject(
      final Id organizationId, final Id projectId, final ProjectDetailChanges projectDetails
  ) {
    return projectMicroservice.updateOrganizationProject(
        organizationId, projectId, projectDetails
    );
  }

  @Async
  public CompletableFuture<ProjectDetails> patchOrganizationProject(
      final Id organizationId,
      final Id projectId,
      final List<ProjectPatchDetail> projectPatchDetails
  ) {
    return projectMicroservice.patchOrganizationProject(
        organizationId, projectId, projectPatchDetails
    );
  }

  @Async
  public CompletableFuture<Void> deleteOrganizationProject(
      final Id organizationId,
      final Id projectId,
      final String userName
  ) {
    return projectMicroservice.deleteOrganizationProject(
        organizationId, projectId, userName
    );
  }

  @Async
  public CompletableFuture<List<ArchiveDetails>> getArchiveDetailList(final Id organizationId) {
    return projectMicroservice.getArchiveDetailList(organizationId);
  }

  @Async
  public CompletableFuture<TaskStatus> importOrganizationProject(
      final Id organizationId,
      final ProjectImportRequest projectImportRequest
  ) {
    return projectMicroservice.importOrganizationProject(organizationId, projectImportRequest);
  }

  @Async
  public CompletableFuture<TaskStatus> exportOrganizationProjects(
      final Id organizationId,
      final ProjectExportRequest projectExportRequest
  ) {
    return projectMicroservice.exportOrganizationProjects(organizationId, projectExportRequest);
  }

  @Async
  public CompletableFuture<List<TaskStatus>> getOrganizationProjectTaskStatus(
      final Id organizationId
  ) {
    return projectMicroservice.getOrganizationProjectTaskStatus(organizationId);
  }

  @Async
  public CompletableFuture<List<Task>> getOrganizationProjectTasks(
      final Id organizationId,
      final String sort,
      final String order
  ) {
    return projectMicroservice.getOrganizationProjectTasks(organizationId, sort, order);
  }
}
