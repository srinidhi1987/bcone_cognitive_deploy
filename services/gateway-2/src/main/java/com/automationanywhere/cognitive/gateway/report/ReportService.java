package com.automationanywhere.cognitive.gateway.report;

import com.automationanywhere.cognitive.gateway.report.microservice.report.ReportMicroservice;
import com.automationanywhere.cognitive.gateway.report.model.DashboardProdProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.ReportingProject;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportService {

  private ReportMicroservice microservice;

  @Autowired
  public ReportService(ReportMicroservice microservice) {
    this.microservice = microservice;
  }

  public CompletableFuture<DashboardProdProjectTotals> getDashboardTotals(String orgId) {
    return microservice.getDashboardReportTotals(orgId);
  }

  public CompletableFuture<ProjectTotals> getReportTotals(String orgId, String projectId) {
    return microservice.getReportTotals(orgId, projectId);
  }

  public CompletableFuture<List<ReportingProject>> getReports(String orgId) {
    return microservice.getReports(orgId);
  }
}
