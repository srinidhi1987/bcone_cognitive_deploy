package com.automationanywhere.cognitive.gateway.alias.model;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import org.apache.commons.codec.language.bm.Languages;

public class ProjectType {

  public final Id id;
  public final String name;
  public final String description;
  public final List<ProjectField> fields;
  public final List<Language> languages;

  public ProjectType(
      final Id id,
      final String name,
      final String description,
      final List<ProjectField> fields,
      final List<Language> languages
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.fields = fields;
    this.languages = languages;
  }
}
