package com.automationanywhere.cognitive.gateway.auth.microservices.auth;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class UserData {

  public final Id id;
  public final String name;
  public final List<String> roles;

  public UserData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("roles") final List<String> roles
  ) {
    this.id = id;
    this.name = name;
    this.roles = roles;
  }
}
