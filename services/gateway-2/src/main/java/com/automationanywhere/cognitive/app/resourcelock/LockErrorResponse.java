package com.automationanywhere.cognitive.app.resourcelock;

import java.util.Collections;
import java.util.List;

public class LockErrorResponse<T> {
  public final Integer httpStatusCode;
  public final Boolean success = false;
  public final Object data = null;
  public final List<String> errors;

  public LockErrorResponse(
      final String error,
      final Integer httpStatusCode
  ) {
    errors = Collections.singletonList(error);
    this.httpStatusCode = httpStatusCode;
  }
}
