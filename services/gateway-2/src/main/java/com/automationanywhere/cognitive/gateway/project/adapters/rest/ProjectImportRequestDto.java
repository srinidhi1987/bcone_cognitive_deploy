package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProjectImportRequestDto {
  public final ArchiveDetailsDto archive;
  public final Id userId;
  public final String option;

  public ProjectImportRequestDto(
      @JsonProperty("archive") final ArchiveDetailsDto archive,
      @JsonProperty("userId") final Id userId,
      @JsonProperty("option") final String option
  ) {
    this.archive = archive;
    this.userId = userId;
    this.option = option;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ProjectImportRequestDto that = (ProjectImportRequestDto) o;

      result = Objects.equals(archive, that.archive)
          && Objects.equals(userId, that.userId)
          && Objects.equals(option, that.option);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archive)
        ^ Objects.hashCode(userId)
        ^ Objects.hashCode(option);
  }
}
