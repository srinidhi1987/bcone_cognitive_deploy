package com.automationanywhere.cognitive.gateway.validator.models;

public class ProjectTotalsAccuracyDetails {

  private final int totalFilesProcessed;
  private final int totalSTP;
  private final int totalAccuracy;
  private final int totalFilesUploaded;
  private final int totalFilesToValidation;
  private final int totalFilesValidated;
  private final int totalFilesUnprocessable;
  private final ClassificationFieldDetails[] classification;
  private final FieldAccuracyDashboardDetails[] accuracy;
  private final FieldValidation[] validation;
  private final GroupTimeSpentDetails[] categories;

  public ProjectTotalsAccuracyDetails(
      int totalFilesProcessed,
      int totalSTP,
      int totalAccuracy,
      int totalFilesUploaded,
      int totalFilesToValidation,
      int totalFilesValidated,
      int totalFilesUnprocessable,
      ClassificationFieldDetails[] classification,
      FieldAccuracyDashboardDetails[] accuracy,
      FieldValidation[] validation,
      GroupTimeSpentDetails[] categories
  ) {
    this.totalFilesProcessed = totalFilesProcessed;
    this.totalSTP = totalSTP;
    this.totalAccuracy = totalAccuracy;
    this.totalFilesUploaded = totalFilesUploaded;
    this.totalFilesToValidation = totalFilesToValidation;
    this.totalFilesValidated = totalFilesValidated;
    this.totalFilesUnprocessable = totalFilesUnprocessable;
    this.classification = classification;
    this.accuracy = accuracy;
    this.validation = validation;
    this.categories = categories;
  }

  /**
   * Returns the totalFilesProcessed.
   *
   * @return the value of totalFilesProcessed
   */
  public int getTotalFilesProcessed() {
    return totalFilesProcessed;
  }

  /**
   * Returns the totalSTP.
   *
   * @return the value of totalSTP
   */
  public int getTotalSTP() {
    return totalSTP;
  }

  /**
   * Returns the totalAccuracy.
   *
   * @return the value of totalAccuracy
   */
  public int getTotalAccuracy() {
    return totalAccuracy;
  }

  /**
   * Returns the totalFilesUploaded.
   *
   * @return the value of totalFilesUploaded
   */
  public int getTotalFilesUploaded() {
    return totalFilesUploaded;
  }

  /**
   * Returns the totalFilesToValidation.
   *
   * @return the value of totalFilesToValidation
   */
  public int getTotalFilesToValidation() {
    return totalFilesToValidation;
  }

  /**
   * Returns the totalFilesValidated.
   *
   * @return the value of totalFilesValidated
   */
  public int getTotalFilesValidated() {
    return totalFilesValidated;
  }

  /**
   * Returns the totalFilesUnprocessable.
   *
   * @return the value of totalFilesUnprocessable
   */
  public int getTotalFilesUnprocessable() {
    return totalFilesUnprocessable;
  }

  /**
   * Returns the classification.
   *
   * @return the value of classification
   */
  public ClassificationFieldDetails[] getClassification() {
    return classification;
  }

  /**
   * Returns the accuracy.
   *
   * @return the value of accuracy
   */
  public FieldAccuracyDashboardDetails[] getAccuracy() {
    return accuracy;
  }

  /**
   * Returns the validation.
   *
   * @return the value of validation
   */
  public FieldValidation[] getValidation() {
    return validation;
  }

  /**
   * Returns the categories.
   *
   * @return the value of categories
   */
  public GroupTimeSpentDetails[] getCategories() {
    return categories;
  }
}
