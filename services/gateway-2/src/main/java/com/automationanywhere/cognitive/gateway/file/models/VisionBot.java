package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class VisionBot {

  public final Id id;
  public final String name;
  public final String currentState;
  public final boolean running;
  public final String lockedUserId;
  public final String lastModifiedByUser;
  public final String lastModifiedTimestamp;

  public VisionBot(
      final Id id,
      final String name,
      final String currentState,
      final boolean running,
      final String lockedUserId,
      final String lastModifiedByUser,
      final String lastModifiedTimestamp
  ) {
    this.id = id;
    this.name = name;
    this.currentState = currentState;
    this.running = running;
    this.lockedUserId = lockedUserId;
    this.lastModifiedByUser = lastModifiedByUser;
    this.lastModifiedTimestamp = lastModifiedTimestamp;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBot visionBot = (VisionBot) o;

      result = Objects.equals(id, visionBot.id)
          && Objects.equals(name, visionBot.name)
          && Objects.equals(currentState, visionBot.currentState)
          && Objects.equals(running, visionBot.running)
          && Objects.equals(lockedUserId, visionBot.lockedUserId)
          && Objects.equals(lastModifiedByUser, visionBot.lastModifiedByUser)
          && Objects.equals(lastModifiedTimestamp, visionBot.lastModifiedTimestamp);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(currentState)
        ^ Objects.hashCode(running)
        ^ Objects.hashCode(lockedUserId)
        ^ Objects.hashCode(lastModifiedByUser)
        ^ Objects.hashCode(lastModifiedTimestamp);
  }
}

