package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class JsonPatchDocument {

  public final List<JsonPatchAction> actions;

  public JsonPatchDocument(final List<JsonPatchAction> actions) {
    this.actions = Optional.ofNullable(actions)
        .map(Collections::unmodifiableList)
        .orElse(null);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchDocument)) {
      return false;
    }
    JsonPatchDocument that = (JsonPatchDocument) o;
    return Objects.equals(actions, that.actions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(actions);
  }
}
