package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class AuthenticationDto {

  public final UserDto user;
  public final String token;

  public AuthenticationDto(
      @JsonProperty("user") final UserDto user,
      @JsonProperty("token") final String token
  ) {
    this.user = user;
    this.token = token;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      AuthenticationDto data = (AuthenticationDto) o;

      result = Objects.equals(user, data.user)
          && Objects.equals(token, data.token);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(user)
        ^ Objects.hashCode(token);
  }

}
