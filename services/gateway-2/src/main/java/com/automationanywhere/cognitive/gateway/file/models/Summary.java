package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class Summary {

  public final Integer totalGroups;
  public final Integer documentsUnclassified;
  public final Integer totalFiles;

  public Summary(
      final Integer totalGroups,
      final Integer documentsUnclassified,
      final Integer totalFiles
  ) {
    this.totalGroups = totalGroups;
    this.documentsUnclassified = documentsUnclassified;
    this.totalFiles = totalFiles;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      Summary summary = (Summary) o;

      result = Objects.equals(totalGroups, summary.totalGroups)
          && Objects.equals(documentsUnclassified, summary.documentsUnclassified)
          && Objects.equals(totalFiles, summary.totalFiles);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalGroups)
        ^ Objects.hashCode(documentsUnclassified)
        ^ Objects.hashCode(totalFiles);
  }
}

