package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import java.util.Objects;

public class JsonPatchActionMoveData extends JsonPatchActionData {

  public final String op;
  public final String from;

  public JsonPatchActionMoveData(
      final String path,
      final String from
  ) {
    super(path);
    this.op = "move";
    this.from = from;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionMoveData)) {
      return false;
    }
    JsonPatchActionMoveData that = (JsonPatchActionMoveData) o;
    return Objects.equals(path, that.path) && Objects.equals(from, that.from);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path, from);
  }
}
