package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileCountData {

  public final Integer totalCount;
  public final Integer unprocessedCount;

  public FileCountData(
      @JsonProperty("totalCount") final Integer totalCount,
      @JsonProperty("unprocessedCount") final Integer unprocessedCount
  ) {
    this.totalCount = totalCount;
    this.unprocessedCount = unprocessedCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileCountData fileCount = (FileCountData) o;

      result = Objects.equals(totalCount, fileCount.totalCount)
          && Objects.equals(unprocessedCount, fileCount.unprocessedCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalCount)
        ^ Objects.hashCode(unprocessedCount);
  }
}

