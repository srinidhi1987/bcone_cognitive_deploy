package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.Objects;

public class ProjectExportRequest {

  public final String archiveName;
  public final List<String> projectExportDetails;
  public final Id userId;

  public ProjectExportRequest(
      final String archiveName,
      final List<String> projectExportDetails,
      final Id userId
  ) {
    this.archiveName = archiveName;
    this.projectExportDetails = projectExportDetails;
    this.userId = userId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectExportRequest projectExportRequest = (ProjectExportRequest) o;

      result = Objects.equals(archiveName, projectExportRequest.archiveName)
          && Objects.equals(projectExportDetails, projectExportRequest.projectExportDetails)
          && Objects.equals(userId, projectExportRequest.userId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archiveName)
        ^ Objects.hashCode(projectExportDetails)
        ^ Objects.hashCode(userId);
  }
}

