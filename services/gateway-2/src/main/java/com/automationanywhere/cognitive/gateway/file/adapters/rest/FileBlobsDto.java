package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileBlobsDto {

  public final String fileId;
  @JsonProperty("fileBlob")
  public final String fileBlobs;

  public FileBlobsDto(
      @JsonProperty("fileId") final String fileId,
      @JsonProperty("fileBlob") final String fileBlobs
  ) {
    this.fileId = fileId;
    this.fileBlobs = fileBlobs;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileBlobsDto fileBLOb = (FileBlobsDto) o;

      result = Objects.equals(fileId, fileBLOb.fileId)
          && Objects.equals(fileBlobs, fileBLOb.fileBlobs);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
        ^ Objects.hashCode(fileBlobs);
  }
}

