package com.automationanywhere.cognitive.gateway.application;

import com.automationanywhere.cognitive.gateway.application.microservices.application.ApplicationMicroservice;
import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ApplicationService {

  private final ApplicationMicroservice microservice;

  @Autowired
  public ApplicationService(final ApplicationMicroservice microservice) {
    this.microservice = microservice;
  }

  @Async
  public CompletableFuture<Handshake> register(final Registration registration) {
    return microservice.register(registration);
  }

  @Async
  public CompletableFuture<Void> acknowledge(final Acknowledgement acknowledgement) {
    return microservice.acknowledge(acknowledgement);
  }

  @Async
  public CompletableFuture<Configuration> retrieveConfiguration() {
    return microservice.retrieveConfiguration();
  }
}
