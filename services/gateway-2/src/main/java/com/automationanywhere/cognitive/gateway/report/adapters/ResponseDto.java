package com.automationanywhere.cognitive.gateway.report.adapters;

public class ResponseDto<T> {

  private boolean success;
  private T data;
  private String errors;

  public ResponseDto() {
  }

  public ResponseDto(T data) {
    success = true;
    this.data = data;
  }

  /**
   * Returns the success.
   *
   * @return the value of success
   */
  public boolean isSuccess() {
    return success;
  }

  /**
   * Sets the success.
   *
   * @param success the success to be set
   */
  public void setSuccess(boolean success) {
    this.success = success;
  }

  /**
   * Returns the data.
   *
   * @return the value of data
   */
  public T getData() {
    return data;
  }

  /**
   * Sets the data.
   *
   * @param data the data to be set
   */
  public void setData(T data) {
    this.data = data;
  }

  /**
   * Returns the errors.
   *
   * @return the value of errors
   */
  public String getErrors() {
    return errors;
  }

  /**
   * Sets the errors.
   *
   * @param errors the errors to be set
   */
  public void setErrors(String errors) {
    this.errors = errors;
  }
}
