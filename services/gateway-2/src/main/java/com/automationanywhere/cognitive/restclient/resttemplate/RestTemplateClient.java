package com.automationanywhere.cognitive.restclient.resttemplate;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.net.SocketTimeoutException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Default implementation of the REST client.
 */
@Component
public class RestTemplateClient implements RestClient {

  private final AsyncRestTemplate asyncRestTemplate;
  private final ExceptionHandlerFactory exceptionHandlerFactory;
  private final ApplicationConfiguration cfg;

  @Autowired
  public RestTemplateClient(
      final AsyncRestTemplate asyncRestTemplate,
      final ExceptionHandlerFactory exceptionHandlerFactory,
      final ApplicationConfiguration cfg
  ) {
    this.asyncRestTemplate = asyncRestTemplate;
    this.exceptionHandlerFactory = exceptionHandlerFactory;
    this.cfg = cfg;
  }

  @Override
  public <T> CompletableFuture<T> send(final RequestDetails rd,
      final ParameterizedTypeReference<T> type) {
    CompletableFuture<T> future = new CompletableFuture<>();

    send(1, future, () -> {
      HttpHeaders headers = new HttpHeaders();
      headers.putAll(rd.headers);

      return asyncRestTemplate
          .exchange(rd.url, rd.method, new HttpEntity<>(rd.body, headers), type);
    });

    return future;
  }

  private <T> void send(
      final int index, final CompletableFuture<T> future,
      final Supplier<ListenableFuture<ResponseEntity<T>>> sender
  ) {
    sender.get().addCallback(this.new CallBack<>(index, future, sender));
  }

  public class CallBack<T> implements ListenableFutureCallback<ResponseEntity<T>> {

    private final int index;
    private final CompletableFuture<T> future;
    private final Supplier<ListenableFuture<ResponseEntity<T>>> sender;

    public CallBack(
        final int index, final CompletableFuture<T> future,
        final Supplier<ListenableFuture<ResponseEntity<T>>> sender
    ) {
      this.index = index;
      this.future = future;
      this.sender = sender;
    }

    @Override
    public void onFailure(final Throwable th) {
      if (index == cfg.restTemplateMaxRetryCount()
          || !(th instanceof SocketTimeoutException)) {
        future.completeExceptionally(th);
      } else {
        try {
          exceptionHandlerFactory.execute(() -> {
            Thread.sleep(cfg.restTemplateRetryDelay());
            send(index + 1, future, sender);
          });
        } catch (final Exception ex) {
          future.completeExceptionally(ex);
        }
      }
    }

    @Override
    public void onSuccess(final ResponseEntity<T> result) {
      future.complete(result.getBody());
    }
  }
}
