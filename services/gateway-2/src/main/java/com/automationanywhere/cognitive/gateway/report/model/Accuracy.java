package com.automationanywhere.cognitive.gateway.report.model;

import java.util.List;

public class Accuracy {

  private List<FieldAccuracy> fields;

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  public List<FieldAccuracy> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the fields to be set
   */
  public void setFields(
      List<FieldAccuracy> fields) {
    this.fields = fields;
  }
}
