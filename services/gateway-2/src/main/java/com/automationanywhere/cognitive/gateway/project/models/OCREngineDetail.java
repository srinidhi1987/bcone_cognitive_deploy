package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class OCREngineDetail {

  public final Id id;
  public final String name;
  public final String engineType;

  public OCREngineDetail(
      final Id id,
      final String name,
      final String engineType
  ) {
    this.id = id;
    this.name = name;
    this.engineType = engineType;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      OCREngineDetail oCREngineDetail = (OCREngineDetail) o;

      result = Objects.equals(id, oCREngineDetail.id)
          && Objects.equals(engineType, oCREngineDetail.engineType)
          && Objects.equals(name, oCREngineDetail.name);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(engineType)
        ^ Objects.hashCode(name);
  }
}

