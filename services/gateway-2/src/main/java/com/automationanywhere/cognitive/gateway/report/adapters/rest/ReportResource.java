package com.automationanywhere.cognitive.gateway.report.adapters.rest;

import static com.automationanywhere.cognitive.validation.Schemas.V1;

import com.automationanywhere.cognitive.gateway.report.ReportService;
import com.automationanywhere.cognitive.gateway.report.adapters.DashboardProdProjectTotalsDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ReportDtoMapper;
import com.automationanywhere.cognitive.gateway.report.adapters.ReportingProjectDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ResponseDto;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotalsDto;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReportResource {

  private ReportService service;
  private ReportDtoMapper mapper;

  @Autowired
  public ReportResource(ReportService service, ReportDtoMapper mapper) {
    this.service = service;
    this.mapper = mapper;
  }

  @RequestMapping(
      path = "/organizations/{orgId}/reporting/projects/totals",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<ResponseDto<DashboardProdProjectTotalsDto>> getDashboardTotals(
      @PathVariable("orgId") String orgId
  ) {
    return service.getDashboardTotals(orgId)
        .thenApply(entities -> new ResponseDto<>(mapper.getDashboardTotalsDto(entities)));
  }

  @RequestMapping(
      path = "/organizations/{orgId}/reporting/projects/{projectId}/totals",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<ResponseDto<ProjectTotalsDto>> getReportTotals(
      @PathVariable("orgId") String orgId,
      @PathVariable("projectId") String projectId
  ) {
    return service.getReportTotals(orgId, projectId)
        .thenApply(entities -> new ResponseDto<>(mapper.getProjectTotalsDto(entities)));
  }

  @RequestMapping(
      path = "/organizations/{orgId}/reporting/projects",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<ResponseDto<List<ReportingProjectDto>>> getReport(
      @PathVariable("orgId") String orgId
  ) {
    return service.getReports(orgId).thenApply(entities -> {
      List<ReportingProjectDto> reports = mapper.getReportingProject(entities);
      return new ResponseDto<>(reports);
    });
  }
}
