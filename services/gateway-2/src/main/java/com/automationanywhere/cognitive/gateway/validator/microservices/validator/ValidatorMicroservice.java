package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.USERNAME_HEADER_NAME;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ValidatorMicroservice {

  private final ValidatorDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public ValidatorMicroservice(
      final ValidatorDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  RestClient getRestClient() {
    return restClient;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<ReviewedFileCountDetails>> getReviewedFileCountDetails(
      final Id organizationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getReviewedFileCountDetails())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<ReviewedFileCountDetailsData>>() {
        }
    ).thenApply(
        data -> data == null ?
            Collections.emptyList() :
            data.stream().map(mapper::mapReviewedFileCountDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Long> getFailedVisionBotCount(
      final Id organizationId,
      final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getFailedVisionBotCount())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<Long>>() {
        }
    ).thenApply(r -> r.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProductionFileSummary> getProductionFileSummary(
      final Id organizationId,
      final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getProductionFileSummary())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ProductionFileSummaryData>() {
        }
    ).thenApply(mapper::mapProductionFileSummary);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Success> markFileInvalid(
      final Id organizationId,
      final Id projectId,
      final Id fileId,
      final String username,
      final List<InvalidFile> invalidFiles
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.validatorURL())
            .request(cfg.markFileInvalid())
            .setHeader(USERNAME_HEADER_NAME, username)
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .content(
                invalidFiles.stream().map(mapper::mapInvalidFile).collect(Collectors.toList()),
                Types.JSON
            ).accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<SuccessData>() {
        }
    ).thenApply(mapper::mapSuccess);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Success> unlockProject(
      final Id organizationId,
      final Id projectId,
      final String userName
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.validatorURL())
            .request(cfg.unlockProject())
            .setHeader(USERNAME_HEADER_NAME, userName)
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<SuccessData>() {
        }
    ).thenApply(mapper::mapSuccess);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<InvalidFileType>> getInvalidFileTypeList() {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getInvalidFileTypeList())
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<InvalidFileTypeData>>() {
        }
    ).thenApply(l -> l == null ? Collections.emptyList() :
        l.stream().map(mapper::mapInvalidFileType).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Map<String, Object>> getFailedVbotData(
      Id organizationId,
      Id projectId,
      final String userName
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getFailedVbotData())
            .setHeader(USERNAME_HEADER_NAME, userName)
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Map<String, Object>>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<AccuracyProcessingDetails> getDocumentProcessingDetails(
      Id organizationId,
      Id typeId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getAccuracyProcessingDetails())
            .param(organizationId)
            .param(typeId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<AccuracyProcessingDetailsData>>() {
        }
    ).thenApply(r -> mapper.mapAccuracyProcessingDetails(r.data));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProjectTotalsAccuracyDetails> getDashboardAccuracyDetails(
      Id organizationId,
      Id projectID
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getProjectTotalsAccuracyDetails())
            .param(organizationId)
            .param(projectID)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<ProjectTotalsAccuracyDetailsData>>() {
        }
    ).thenApply(r -> mapper.mapPrjTotAccuracyDetails(r.data));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Success> sendCorrectDataValues(
      Id organizationId,
      Id projectId,
      Id fileId,
      String userName,
      String additionInfo
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.validatorURL())
            .request(cfg.getSendCorrectDataValues())
            .setHeader(USERNAME_HEADER_NAME, userName)
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .content(additionInfo, Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<SuccessData>>() {
        }
    ).thenApply(r -> new Success(r.data.success));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Success> patchCorrectDataValues(
      Id organizationId,
      Id projectId,
      Id fileId,
      String userName,
      JsonPatchDocument jsonPatchDocument
  ) {
    return getRestClient().send(
        RequestDetails.Builder.patch(cfg.validatorURL())
            .request(cfg.getPatchCorrectDataValues())
            .setHeader(USERNAME_HEADER_NAME, userName)
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .content(mapper.mapJsonPatchDocument(jsonPatchDocument), Types.JSON_PATCH)
            .accept(Types.JSON_PATCH)
            .build(),
        new ParameterizedTypeReference<StandardResponse<SuccessData>>() {
        }
    ).thenApply(r -> new Success(r.success));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<FailedVbotStatus> updateValidationStatus(
      Id organizationId,
      Id projectId,
      Id visionBotId,
      Id fileId,
      Boolean validationStatus
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.validatorURL())
            .request(cfg.getUpdateValidationStatus())
            .param(organizationId)
            .param(projectId)
            .param(visionBotId)
            .param(fileId)
            .param(validationStatus)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<FailedVbotStatusData>() {
        }
    ).thenApply(mapper::mapFailedVBotStatus);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> keepAlive(
      final Id organizationId,
      final Id projectId,
      final Id fileId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.validatorURL())
            .request(cfg.validatorKeepAlive())
            .param(organizationId)
            .param(projectId)
            .param(fileId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    ).thenApply(r -> null);
  }
}
