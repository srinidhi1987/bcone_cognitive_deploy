package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class FieldValueDefinition {
    public final Id id;
    public final Id fieldId;
    public final String label;
    public final String startsWith;
    public final String endsWith;
    public final String formatExpression;
    public final String displayValue;
    public final Boolean isMultiline;
    public final String bounds;
    public final String valueBounds;
    public final Integer fieldDirection;
    public final Integer mergeRatio;
    public final Double similarityFactor;
    public final Integer type;
    public final Integer xDistant;
    public final Boolean isValueBoundAuto;
    public final Integer valueType;
    public final String dollarCurrencyUsed;

  public FieldValueDefinition(
    final Id id,
    final Id fieldId,
    final String label,
    final String startsWith,
    final String endsWith,
    final String formatExpression,
    final String displayValue,
    final Boolean isMultiline,
    final String bounds,
    final String valueBounds,
    final Integer fieldDirection,
    final Integer mergeRatio,
    final Double similarityFactor,
    final Integer type,
    final Integer xDistant,
    final Boolean isValueBoundAuto,
    final Integer valueType,
    final String dollarCurrencyUsed
  ) {
    this.id = id;
    this.fieldId = fieldId;
    this.label = label;
    this.startsWith = startsWith;
    this.endsWith = endsWith;
    this.formatExpression = formatExpression;
    this.displayValue = displayValue;
    this.isMultiline = isMultiline;
    this.bounds = bounds;
    this.valueBounds = valueBounds;
    this.fieldDirection = fieldDirection;
    this.mergeRatio = mergeRatio;
    this.similarityFactor = similarityFactor;
    this.type = type;
    this.xDistant = xDistant;
    this.isValueBoundAuto = isValueBoundAuto;
    this.valueType = valueType;
    this.dollarCurrencyUsed = dollarCurrencyUsed;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      FieldValueDefinition that = (FieldValueDefinition) o;

      result = Objects.equals(id, that.id)
          && Objects.equals(fieldId, that.fieldId)
          && Objects.equals(label, that.label)
          && Objects.equals(startsWith, that.startsWith)
          && Objects.equals(endsWith, that.endsWith)
          && Objects.equals(formatExpression, that.formatExpression)
          && Objects.equals(displayValue, that.displayValue)
          && Objects.equals(isMultiline, that.isMultiline)
          && Objects.equals(bounds, that.bounds)
          && Objects.equals(valueBounds, that.valueBounds)
          && Objects.equals(fieldDirection, that.fieldDirection)
          && Objects.equals(mergeRatio, that.mergeRatio)
          && Objects.equals(similarityFactor, that.similarityFactor)
          && Objects.equals(type, that.type)
          && Objects.equals(xDistant, that.xDistant)
          && Objects.equals(isValueBoundAuto, that.isValueBoundAuto)
          && Objects.equals(valueType, that.valueType)
          && Objects.equals(dollarCurrencyUsed, that.dollarCurrencyUsed);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(fieldId)
        ^ Objects.hashCode(label)
        ^ Objects.hashCode(startsWith)
        ^ Objects.hashCode(endsWith)
        ^ Objects.hashCode(formatExpression)
        ^ Objects.hashCode(displayValue)
        ^ Objects.hashCode(isMultiline)
        ^ Objects.hashCode(bounds)
        ^ Objects.hashCode(valueBounds)
        ^ Objects.hashCode(fieldDirection)
        ^ Objects.hashCode(mergeRatio)
        ^ Objects.hashCode(similarityFactor)
        ^ Objects.hashCode(type)
        ^ Objects.hashCode(xDistant)
        ^ Objects.hashCode(isValueBoundAuto)
        ^ Objects.hashCode(valueType)
        ^ Objects.hashCode(dollarCurrencyUsed);
  }
}

