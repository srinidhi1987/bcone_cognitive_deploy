package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class VisionBotDetails {

  public final Id id;
  public final String name;
  public final Id organizationId;
  public final Id projectId;
  public final String projectName;
  public final Id categoryId;
  public final String categoryName;
  public final String environment;
  public final String status;
  public final boolean running;
  public final Id lockedUserId;
  public final VisionBotRunDetails botRunDetails;
  public final VisionBotRunDetails productionBotRunDetails;
  public final VisionBotRunDetails stagingBotRunDetails;

  public VisionBotDetails(
      final Id id,
      final String name,
      final Id organizationId,
      final Id projectId,
      final String projectName,
      final Id categoryId,
      final String categoryName,
      final String environment,
      final String status,
      final boolean running,
      final Id lockedUserId,
      final VisionBotRunDetails botRunDetails,
      final VisionBotRunDetails productionBotRunDetails,
      final VisionBotRunDetails stagingBotRunDetails
  ) {
    this.id = id;
    this.name = name;
    this.organizationId = organizationId;
    this.projectId = projectId;
    this.projectName = projectName;
    this.categoryId = categoryId;
    this.categoryName = categoryName;
    this.environment = environment;
    this.status = status;
    this.running = running;
    this.lockedUserId = lockedUserId;
    this.botRunDetails = botRunDetails;
    this.productionBotRunDetails = productionBotRunDetails;
    this.stagingBotRunDetails = stagingBotRunDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotDetails visionbotData = (VisionBotDetails) o;

      result = Objects.equals(id, visionbotData.id)
        && Objects.equals(name, visionbotData.name)
        && Objects.equals(organizationId, visionbotData.organizationId)
        && Objects.equals(projectId, visionbotData.projectId)
        && Objects.equals(projectName, visionbotData.projectName)
        && Objects.equals(categoryId, visionbotData.categoryId)
        && Objects.equals(categoryName, visionbotData.categoryName)
        && Objects.equals(environment, visionbotData.environment)
        && Objects.equals(status, visionbotData.status)
        && Objects.equals(running, visionbotData.running)
        && Objects.equals(lockedUserId, visionbotData.lockedUserId)
        && Objects.equals(botRunDetails, visionbotData.botRunDetails)
        && Objects.equals(productionBotRunDetails, visionbotData.productionBotRunDetails)
        && Objects.equals(stagingBotRunDetails, visionbotData.stagingBotRunDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(name)
      ^ Objects.hashCode(organizationId)
      ^ Objects.hashCode(projectId)
      ^ Objects.hashCode(projectName)
      ^ Objects.hashCode(categoryId)
      ^ Objects.hashCode(categoryName)
      ^ Objects.hashCode(environment)
      ^ Objects.hashCode(status)
      ^ Objects.hashCode(running)
      ^ Objects.hashCode(lockedUserId)
      ^ Objects.hashCode(botRunDetails)
      ^ Objects.hashCode(productionBotRunDetails)
      ^ Objects.hashCode(stagingBotRunDetails);
  }
}

