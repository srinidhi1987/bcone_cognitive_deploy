package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class SuccessDto {

  public final boolean success;

  public SuccessDto(
      @JsonProperty("success") final boolean success
  ) {
    this.success = success;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      SuccessDto that = (SuccessDto) o;

      result = Objects.equals(success, that.success);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(success) ^ 32;
  }
}
