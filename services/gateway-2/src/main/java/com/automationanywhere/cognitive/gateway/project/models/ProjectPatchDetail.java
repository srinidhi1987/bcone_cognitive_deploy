package com.automationanywhere.cognitive.gateway.project.models;

import java.util.Objects;

public class ProjectPatchDetail {

  public final String op;
  public final String path;
  public final Object value;
  public final Integer versionId;

  public ProjectPatchDetail(
      final String op,
      final String path,
      final Object value,
      final Integer versionId
  ) {
    this.op = op;
    this.path = path;
    this.value = value;
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectPatchDetail projectPatchDetail
          = (ProjectPatchDetail) o;

      result = Objects.equals(op, projectPatchDetail.op)
          && Objects.equals(path, projectPatchDetail.path)
          && Objects.equals(value, projectPatchDetail.value)
          && Objects.equals(versionId, projectPatchDetail.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(op)
        ^ Objects.hashCode(path)
        ^ Objects.hashCode(value)
        ^ Objects.hashCode(versionId);
  }
}

