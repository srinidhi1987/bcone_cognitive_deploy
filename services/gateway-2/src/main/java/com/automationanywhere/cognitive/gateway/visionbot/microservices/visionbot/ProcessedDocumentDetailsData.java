package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProcessedDocumentDetailsData {

  public final Id id;
  @JsonProperty("docid")
  public final Id docId;
  public final String processingStatus;
  public final String totalFieldCount;
  public final String failedFieldCount;
  public final String passedFieldCount;

  public ProcessedDocumentDetailsData(
      @JsonProperty("id") final Id id,
      @JsonProperty("docid") final Id docId,
      @JsonProperty("processingStatus") final String processingStatus,
      @JsonProperty("totalFieldCount") final String totalFieldCount,
      @JsonProperty("failedFieldCount") final String failedFieldCount,
      @JsonProperty("passedFieldCount") final String passedFieldCount
  ) {
    this.id = id;
    this.docId = docId;
    this.processingStatus = processingStatus;
    this.totalFieldCount = totalFieldCount;
    this.failedFieldCount = failedFieldCount;
    this.passedFieldCount = passedFieldCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProcessedDocumentDetailsData processedDocumentDetails = (ProcessedDocumentDetailsData) o;

      result = Objects.equals(id, processedDocumentDetails.id)
        && Objects.equals(docId, processedDocumentDetails.docId)
        && Objects.equals(processingStatus, processedDocumentDetails.processingStatus)
        && Objects.equals(totalFieldCount, processedDocumentDetails.totalFieldCount)
        && Objects.equals(failedFieldCount, processedDocumentDetails.failedFieldCount)
        && Objects.equals(passedFieldCount, processedDocumentDetails.passedFieldCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(docId)
      ^ Objects.hashCode(processingStatus)
      ^ Objects.hashCode(totalFieldCount)
      ^ Objects.hashCode(failedFieldCount)
      ^ Objects.hashCode(passedFieldCount);
  }
}

