package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.List;
import java.util.Objects;

public class ValidationDetails {

  public final List<ValidatorDetails> validatorData;

  public ValidationDetails(
      final List<ValidatorDetails> validatorData
  ) {
    this.validatorData = validatorData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ValidationDetails validationData = (ValidationDetails) o;

      result = Objects.equals(validatorData, validationData.validatorData);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(validatorData);
  }
}

