package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Objects;

public class Summary {

  public final StagingFilesSummary stagingSummary;
  public final VisionBotCount botDetails;

  public Summary(
      final StagingFilesSummary stagingSummary,
      final VisionBotCount botDetails
  ) {
    this.stagingSummary = stagingSummary;
    this.botDetails = botDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      Summary summary = (Summary) o;

      result = Objects.equals(stagingSummary, summary.stagingSummary)
        && Objects.equals(botDetails, summary.botDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(stagingSummary)
      ^ Objects.hashCode(botDetails);
  }
}

