package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class UploadResponseData {

  public final String fileUploadJobId;

  public UploadResponseData(
      @JsonProperty("fileUploadJobId") final String fileUploadJobId
  ) {
    this.fileUploadJobId = fileUploadJobId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      UploadResponseData uploadResponse = (UploadResponseData) o;

      result = Objects.equals(fileUploadJobId, uploadResponse.fileUploadJobId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileUploadJobId);
  }
}

