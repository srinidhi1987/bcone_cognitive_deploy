package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccuracyProcessingDetailsDto {

  private final String id;
  private final double failedFieldCounts;
  private final double passFieldCounts;
  private final double totalFieldCounts;
  private final double fieldAccuracy;
  private final long reviewFileCount;
  private final long invalidFileCount;
  private final long failedDocumentCount;
  private final long passedDocumentCounts;
  private final long processedDocumentCounts;
  private final long documentAccuracy;
  private final long averageReviewTimeInSeconds;

  public AccuracyProcessingDetailsDto(
      @JsonProperty(value = "id") String id,
      @JsonProperty(value = "failedFieldCount") double failedFieldCounts,
      @JsonProperty(value = "passedFieldCount") double passFieldCounts,
      @JsonProperty(value = "totalFieldCount") double totalFieldCounts,
      @JsonProperty(value = "fieldAccuracy") double fieldAccuracy,
      @JsonProperty(value = "reviewFileCount") long reviewFileCount,
      @JsonProperty(value = "invalidFileCount") long invalidFileCount,
      @JsonProperty(value = "failedDocumentCount") long failedDocumentCount,
      @JsonProperty(value = "passedDocumentCount") long passedDocumentCounts,
      @JsonProperty(value = "processedDocumentCount") long processedDocumentCounts,
      @JsonProperty(value = "documentAccuracy") long documentAccuracy,
      @JsonProperty(value = "averageReviewTimeInSeconds") long averageReviewTimeInSeconds
  ) {
    this.id = id;
    this.failedFieldCounts = failedFieldCounts;
    this.passFieldCounts = passFieldCounts;
    this.totalFieldCounts = totalFieldCounts;
    this.fieldAccuracy = fieldAccuracy;
    this.reviewFileCount = reviewFileCount;
    this.invalidFileCount = invalidFileCount;
    this.failedDocumentCount = failedDocumentCount;
    this.passedDocumentCounts = passedDocumentCounts;
    this.processedDocumentCounts = processedDocumentCounts;
    this.documentAccuracy = documentAccuracy;
    this.averageReviewTimeInSeconds = averageReviewTimeInSeconds;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  public String getId() {
    return id;
  }

  /**
   * Returns the failedFieldCounts.
   *
   * @return the value of failedFieldCounts
   */
  public double getFailedFieldCounts() {
    return failedFieldCounts;
  }

  /**
   * Returns the passFieldCounts.
   *
   * @return the value of passFieldCounts
   */
  public double getPassFieldCounts() {
    return passFieldCounts;
  }

  /**
   * Returns the totalFieldCounts.
   *
   * @return the value of totalFieldCounts
   */
  public double getTotalFieldCounts() {
    return totalFieldCounts;
  }

  /**
   * Returns the fieldAccuracy.
   *
   * @return the value of fieldAccuracy
   */
  public double getFieldAccuracy() {
    return fieldAccuracy;
  }

  /**
   * Returns the reviewFileCount.
   *
   * @return the value of reviewFileCount
   */
  public long getReviewFileCount() {
    return reviewFileCount;
  }

  /**
   * Returns the invalidFileCount.
   *
   * @return the value of invalidFileCount
   */
  public long getInvalidFileCount() {
    return invalidFileCount;
  }

  /**
   * Returns the failedDocumentCount.
   *
   * @return the value of failedDocumentCount
   */
  public long getFailedDocumentCount() {
    return failedDocumentCount;
  }

  /**
   * Returns the passedDocumentCounts.
   *
   * @return the value of passedDocumentCounts
   */
  public long getPassedDocumentCounts() {
    return passedDocumentCounts;
  }

  /**
   * Returns the processedDocumentCounts.
   *
   * @return the value of processedDocumentCounts
   */
  public long getProcessedDocumentCounts() {
    return processedDocumentCounts;
  }

  /**
   * Returns the documentAccuracy.
   *
   * @return the value of documentAccuracy
   */
  public long getDocumentAccuracy() {
    return documentAccuracy;
  }

  /**
   * Returns the averageReviewTimeInSeconds.
   *
   * @return the value of averageReviewTimeInSeconds
   */
  public long getAverageReviewTimeInSeconds() {
    return averageReviewTimeInSeconds;
  }
}
