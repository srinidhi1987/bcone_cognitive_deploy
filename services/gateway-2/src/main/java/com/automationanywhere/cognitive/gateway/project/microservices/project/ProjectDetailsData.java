package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

public class ProjectDetailsData {

  public final Id id;
  public final String name;
  public final String description;
  public final Id organizationId;
  public final Id projectTypeId;
  public final String projectType;
  public final Integer confidenceThreshold;
  public final Integer numberOfFiles;
  public final Integer numberOfCategories;
  public final Integer unprocessedFileCount;
  public final String primaryLanguage;
  public final Integer accuracyPercentage;
  public final Integer visionBotCount;
  public final Integer currentTrainedPercentage;
  public final List<CategoryData> categories;
  public final FieldDetailsData fields;
  public final String projectState;
  public final String environment;
  public final OffsetDateTime updatedAt;
  public final OffsetDateTime createdAt;
  public final List<OCREngineDetailData> ocrEngineDetails;
  public final Integer versionId;

  public ProjectDetailsData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("description") final String description,
      @JsonProperty("organizationId") final Id organizationId,
      @JsonProperty("projectTypeId") final Id projectTypeId,
      @JsonProperty("projectType") final String projectType,
      @JsonProperty("confidenceThreshold") final Integer confidenceThreshold,
      @JsonProperty("numberOfFiles") final Integer numberOfFiles,
      @JsonProperty("numberOfCategories") final Integer numberOfCategories,
      @JsonProperty("unprocessedFileCount") final Integer unprocessedFileCount,
      @JsonProperty("primaryLanguage") final String primaryLanguage,
      @JsonProperty("accuracyPercentage") final Integer accuracyPercentage,
      @JsonProperty("visionBotCount") final Integer visionBotCount,
      @JsonProperty("currentTrainedPercentage") final Integer currentTrainedPercentage,
      @JsonProperty("categories") final List<CategoryData> categories,
      @JsonProperty("fields") final FieldDetailsData fields,
      @JsonProperty("projectState") final String projectState,
      @JsonProperty("environment") final String environment,
      @JsonProperty("updatedAt") final OffsetDateTime updatedAt,
      @JsonProperty("createdAt") final OffsetDateTime createdAt,
      @JsonProperty("ocrEngineDetails") final List<OCREngineDetailData> ocrEngineDetails,
      @JsonProperty("versionId") final Integer versionId
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.organizationId = organizationId;
    this.projectTypeId = projectTypeId;
    this.projectType = projectType;
    this.confidenceThreshold = confidenceThreshold;
    this.numberOfFiles = numberOfFiles;
    this.numberOfCategories = numberOfCategories;
    this.unprocessedFileCount = unprocessedFileCount;
    this.primaryLanguage = primaryLanguage;
    this.accuracyPercentage = accuracyPercentage;
    this.visionBotCount = visionBotCount;
    this.currentTrainedPercentage = currentTrainedPercentage;
    this.categories = categories;
    this.fields = fields;
    this.projectState = projectState;
    this.environment = environment;
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;
    this.ocrEngineDetails = ocrEngineDetails;
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectDetailsData projectDetail = (ProjectDetailsData) o;

      result = Objects.equals(id, projectDetail.id)
          && Objects.equals(name, projectDetail.name)
          && Objects.equals(description, projectDetail.description)
          && Objects.equals(organizationId, projectDetail.organizationId)
          && Objects.equals(projectTypeId, projectDetail.projectTypeId)
          && Objects.equals(projectType, projectDetail.projectType)
          && Objects.equals(confidenceThreshold, projectDetail.confidenceThreshold)
          && Objects.equals(numberOfFiles, projectDetail.numberOfFiles)
          && Objects.equals(numberOfCategories, projectDetail.numberOfCategories)
          && Objects.equals(unprocessedFileCount, projectDetail.unprocessedFileCount)
          && Objects.equals(primaryLanguage, projectDetail.primaryLanguage)
          && Objects.equals(accuracyPercentage, projectDetail.accuracyPercentage)
          && Objects.equals(visionBotCount, projectDetail.visionBotCount)
          && Objects.equals(currentTrainedPercentage, projectDetail.currentTrainedPercentage)
          && Objects.equals(categories, projectDetail.categories)
          && Objects.equals(fields, projectDetail.fields)
          && Objects.equals(projectState, projectDetail.projectState)
          && Objects.equals(environment, projectDetail.environment)
          && Objects.equals(updatedAt, projectDetail.updatedAt)
          && Objects.equals(createdAt, projectDetail.createdAt)
          && Objects.equals(ocrEngineDetails, projectDetail.ocrEngineDetails)
          && Objects.equals(versionId, projectDetail.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(description)
        ^ Objects.hashCode(organizationId)
        ^ Objects.hashCode(projectTypeId)
        ^ Objects.hashCode(projectType)
        ^ Objects.hashCode(confidenceThreshold)
        ^ Objects.hashCode(numberOfFiles)
        ^ Objects.hashCode(numberOfCategories)
        ^ Objects.hashCode(unprocessedFileCount)
        ^ Objects.hashCode(primaryLanguage)
        ^ Objects.hashCode(accuracyPercentage)
        ^ Objects.hashCode(visionBotCount)
        ^ Objects.hashCode(currentTrainedPercentage)
        ^ Objects.hashCode(categories)
        ^ Objects.hashCode(fields)
        ^ Objects.hashCode(projectState)
        ^ Objects.hashCode(environment)
        ^ Objects.hashCode(updatedAt)
        ^ Objects.hashCode(createdAt)
        ^ Objects.hashCode(ocrEngineDetails)
        ^ Objects.hashCode(versionId);
  }
}

