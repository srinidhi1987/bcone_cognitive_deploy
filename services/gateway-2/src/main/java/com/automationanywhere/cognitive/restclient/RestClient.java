package com.automationanywhere.cognitive.restclient;

import java.util.concurrent.CompletableFuture;
import org.springframework.core.ParameterizedTypeReference;

/**
 * Implementations of the interface are responsible for
 * Creating, Reading, Updating and Deleting REST resources.
 */
public interface RestClient {

  /**
   * Gets a resource
   *
   * @param rd - request details like the url, method and headers.
   * @param type - type of the resource
   * @return a future which when completes holds the requested resource or an error.
   */
  <T> CompletableFuture<T> send(RequestDetails rd, ParameterizedTypeReference<T> type);
}
