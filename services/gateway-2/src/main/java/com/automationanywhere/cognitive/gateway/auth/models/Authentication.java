package com.automationanywhere.cognitive.gateway.auth.models;

public class Authentication {

  public final User user;
  public final String token;

  public Authentication(final User user, final String token) {
    this.user = user;
    this.token = token;
  }

}
