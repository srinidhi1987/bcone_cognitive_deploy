package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotDto {

  public final Id id;
  public final String name;
  public final String currentState;
  public final boolean running;
  public final String lockedUserId;
  public final String lastModifiedByUser;
  public final String lastModifiedTimestamp;

  public VisionBotDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("currentState") final String currentState,
      @JsonProperty("running") final boolean running,
      @JsonProperty("lockedUserId") final String lockedUserId,
      @JsonProperty("lastModifiedByUser") final String lastModifiedByUser,
      @JsonProperty("lastModifiedTimestamp") final String lastModifiedTimestamp
  ) {
    this.id = id;
    this.name = name;
    this.currentState = currentState;
    this.running = running;
    this.lockedUserId = lockedUserId;
    this.lastModifiedByUser = lastModifiedByUser;
    this.lastModifiedTimestamp = lastModifiedTimestamp;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotDto visionBot = (VisionBotDto) o;

      result = Objects.equals(id, visionBot.id)
          && Objects.equals(name, visionBot.name)
          && Objects.equals(currentState, visionBot.currentState)
          && Objects.equals(running, visionBot.running)
          && Objects.equals(lockedUserId, visionBot.lockedUserId)
          && Objects.equals(lastModifiedByUser, visionBot.lastModifiedByUser)
          && Objects.equals(lastModifiedTimestamp, visionBot.lastModifiedTimestamp);

    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(currentState)
        ^ Objects.hashCode(running)
        ^ Objects.hashCode(lockedUserId)
        ^ Objects.hashCode(lastModifiedByUser)
        ^ Objects.hashCode(lastModifiedTimestamp);
  }
}

