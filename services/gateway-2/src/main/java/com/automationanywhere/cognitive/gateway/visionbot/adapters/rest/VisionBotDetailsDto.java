package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotDetailsDto {

  public final Id id;
  public final String name;
  public final Id organizationId;
  public final Id projectId;
  public final String projectName;
  public final Id categoryId;
  public final String categoryName;
  public final String environment;
  public final String status;
  public final boolean running;
  public final Id lockedUserId;
  public final VisionBotRunDetailsDto botRunDetails;
  public final VisionBotRunDetailsDto productionBotRunDetails;
  public final VisionBotRunDetailsDto stagingBotRunDetails;

  public VisionBotDetailsDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("organizationId") final Id organizationId,
      @JsonProperty("projectId") final Id projectId,
      @JsonProperty("projectName") final String projectName,
      @JsonProperty("categoryId") final Id categoryId,
      @JsonProperty("categoryName") final String categoryName,
      @JsonProperty("environment") final String environment,
      @JsonProperty("status") final String status,
      @JsonProperty("running") final boolean running,
      @JsonProperty("lockedUserId") final Id lockedUserId,
      @JsonProperty("botRunDetails") final VisionBotRunDetailsDto botRunDetails,
      @JsonProperty("productionBotRunDetails") final VisionBotRunDetailsDto productionBotRunDetails,
      @JsonProperty("stagingBotRunDetails") final VisionBotRunDetailsDto stagingBotRunDetails
  ) {
    this.id = id;
    this.name = name;
    this.organizationId = organizationId;
    this.projectId = projectId;
    this.projectName = projectName;
    this.categoryId = categoryId;
    this.categoryName = categoryName;
    this.environment = environment;
    this.status = status;
    this.running = running;
    this.lockedUserId = lockedUserId;
    this.botRunDetails = botRunDetails;
    this.productionBotRunDetails = productionBotRunDetails;
    this.stagingBotRunDetails = stagingBotRunDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotDetailsDto visionbotData = (VisionBotDetailsDto) o;

      result = Objects.equals(id, visionbotData.id)
          && Objects.equals(name, visionbotData.name)
          && Objects.equals(organizationId, visionbotData.organizationId)
          && Objects.equals(projectId, visionbotData.projectId)
          && Objects.equals(projectName, visionbotData.projectName)
          && Objects.equals(categoryId, visionbotData.categoryId)
          && Objects.equals(categoryName, visionbotData.categoryName)
          && Objects.equals(environment, visionbotData.environment)
          && Objects.equals(status, visionbotData.status)
          && Objects.equals(running, visionbotData.running)
          && Objects.equals(lockedUserId, visionbotData.lockedUserId)
          && Objects.equals(botRunDetails, visionbotData.botRunDetails)
          && Objects.equals(productionBotRunDetails, visionbotData.productionBotRunDetails)
          && Objects.equals(stagingBotRunDetails, visionbotData.stagingBotRunDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(organizationId)
        ^ Objects.hashCode(projectId)
        ^ Objects.hashCode(projectName)
        ^ Objects.hashCode(categoryId)
        ^ Objects.hashCode(categoryName)
        ^ Objects.hashCode(environment)
        ^ Objects.hashCode(status)
        ^ Objects.hashCode(running)
        ^ Objects.hashCode(lockedUserId)
        ^ Objects.hashCode(botRunDetails)
        ^ Objects.hashCode(productionBotRunDetails)
        ^ Objects.hashCode(stagingBotRunDetails);
  }
}

