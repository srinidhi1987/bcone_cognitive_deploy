package com.automationanywhere.cognitive.app.errors;

import com.automationanywhere.cognitive.auth.ForbiddenException;
import com.automationanywhere.cognitive.validation.JsonValidationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * Responsible to handle all exceptions thrown by controllers.
 */
@ControllerAdvice
public class ErrorHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

  private final ObjectMapper om;

  public ErrorHandler(final ObjectMapper om) {
    this.om = om;
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<?> handleTopLevelException(
      final Exception ex,
      final ServletWebRequest request
  ) throws Exception {
    Throwable e = extractException(ex);

    LOGGER.error("Unhandled error", e);

    Object error;
    MediaType contentType = MediaType.APPLICATION_JSON_UTF8;
    HttpStatus httpStatus;

    if (e instanceof HttpClientErrorException) {
      HttpClientErrorException ce = (HttpClientErrorException) e;

      contentType = Optional.ofNullable(ce.getResponseHeaders().getContentType())
          .orElse(MediaType.APPLICATION_OCTET_STREAM);

      httpStatus = ce.getStatusCode();
      error = ce.getResponseBodyAsByteArray();
    } else if (e instanceof HttpStatusCodeException) {
      HttpStatusCodeException statusCodeException = (HttpStatusCodeException) e;
      httpStatus = HttpStatus.valueOf(statusCodeException.getRawStatusCode());
      error = statusCodeException.getResponseBodyAsByteArray();
      if (error == null || ((byte[]) error).length == 0) {
        error = new DefaultErrorDto(httpStatus.value(), e, request);
      }
    } else if (e instanceof JsonValidationException) {
      httpStatus = ((JsonValidationException) e).httpStatus;
      error = new JsonValidationErrorDto((JsonValidationException) e, request);
    } else {
      httpStatus = translateException(ex);
      error = new DefaultErrorDto(httpStatus.value(), e, request);
    }

    if (!(error instanceof byte[])) {
      error = om.writeValueAsBytes(error);
    }

    return createResponseEntity(error, contentType, httpStatus);
  }

  private ResponseEntity<?> createResponseEntity(
      final Object error,
      final MediaType contentType,
      final HttpStatus httpStatus
  ) throws JsonProcessingException {
    HttpHeaders headers = new HttpHeaders();
    headers.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(contentType.toString()));
    return new ResponseEntity<>(error, headers, httpStatus);
  }

  private HttpStatus translateException(final Exception ex) {
    HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    if (ex instanceof HttpRequestMethodNotSupportedException) {
      httpStatus = HttpStatus.METHOD_NOT_ALLOWED;
    } else if (ex instanceof HttpMediaTypeNotSupportedException) {
      httpStatus = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
    } else if (ex instanceof HttpMediaTypeNotAcceptableException) {
      httpStatus = HttpStatus.NOT_ACCEPTABLE;
    } else if (ex instanceof NoHandlerFoundException) {
      httpStatus = HttpStatus.NOT_FOUND;
    } else if (ex instanceof AsyncRequestTimeoutException) {
      httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
    } else if (ex instanceof ForbiddenException) {
      httpStatus = HttpStatus.FORBIDDEN;
    } else {
      httpStatus = translateBadRequestExceptions(ex);
    }
    return httpStatus;
  }

  private HttpStatus translateBadRequestExceptions(final Exception ex) {
    HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    if (ex instanceof MissingServletRequestParameterException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else if (ex instanceof ServletRequestBindingException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else if (ex instanceof TypeMismatchException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else if (ex instanceof HttpMessageNotReadableException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else if (ex instanceof MethodArgumentNotValidException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else if (ex instanceof MissingServletRequestPartException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else if (ex instanceof BindException) {
      httpStatus = HttpStatus.BAD_REQUEST;
    }
    return httpStatus;
  }

  private Throwable extractException(final Exception ex) {
    Throwable e = ex;

    while (true) {
      if (e.getCause() == null
          || e.getCause() == e
          || !(e instanceof ExecutionException
          || e instanceof CompletionException
          || !(e instanceof RestClientException
          || !(e instanceof NestedRuntimeException)))) {
        break;
      }

      e = e.getCause();
    }

    return e;
  }
}

