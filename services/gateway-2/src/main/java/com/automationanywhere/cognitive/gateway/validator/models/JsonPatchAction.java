package com.automationanywhere.cognitive.gateway.validator.models;

public abstract class JsonPatchAction {

  public final String path;

  public JsonPatchAction(
      final String path
  ) {
    this.path = path;
  }
}
