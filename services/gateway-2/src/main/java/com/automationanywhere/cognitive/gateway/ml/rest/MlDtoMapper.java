package com.automationanywhere.cognitive.gateway.ml.rest;

import com.automationanywhere.cognitive.gateway.ml.model.Value;
import org.springframework.stereotype.Component;

@Component
public class MlDtoMapper {

  public Value mapValue(final ValueDto value) {
    return value == null ? null : new Value(
      value.fieldId, value.correctedValue, value.originalValue
    );
  }
}
