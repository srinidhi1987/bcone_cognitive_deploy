package com.automationanywhere.cognitive.app.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.util.NestedServletException;

/**
 * Captures all unhandled errors thrown during request handling
 * and sends them back to a client as HTTP errors.
 * This is our fallback, normally Spring should handle errors for us,
 * but if an error happens outside of Spring we have to handle them here,
 * otherwise they will be handled by a container in a container specific way,
 * and we want the same type of error handling disregarding containers.
 */
@Component
@Order(0)
public class ErrorFilter implements Filter {

  private static final Logger LOGGER = LogManager.getLogger(ErrorFilter.class);

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response,
      final FilterChain chain) throws IOException, ServletException {
    HttpServletResponse resp = (HttpServletResponse) response;
    try {
      try {
        chain.doFilter(request, response);
      } catch (final NestedServletException ex) {
        throw ex.getCause();
      }
    } catch (final RestClientResponseException ex) {
      LOGGER.error("Unhandled error", ex);
      resp.sendError(ex.getRawStatusCode(), ex.getClass().getName() + ": " + ex.getMessage());
    } catch (final Throwable ex) {
      LOGGER.error("Unhandled error", ex);
      resp.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
          ex.getClass().getName() + ": " + ex.getMessage());
    }
  }
}
