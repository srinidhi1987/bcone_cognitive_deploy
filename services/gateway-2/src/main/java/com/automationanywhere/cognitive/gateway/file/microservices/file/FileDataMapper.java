package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.Category;
import com.automationanywhere.cognitive.gateway.file.models.CategoryDetail;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.EnvironmentCountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FieldDetail;
import com.automationanywhere.cognitive.gateway.file.models.File;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileCount;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFieldDetailFromClasssifier;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.Summary;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.gateway.file.models.VisionBot;
import java.util.Collections;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class FileDataMapper {

  public Categories mapCategories(final CategoriesData data) {
    return data == null ? null : new Categories(
        data.offset, data.limit, data.sort,
        data.categories == null ? null
            : data.categories.stream().map(this::mapCategory).collect(Collectors.toList())
    );
  }

  private Category mapCategory(final CategoryData data) {
    return data == null ? null : new Category(
        data.id, data.name, mapCategoryDetail(data.categoryDetail),
        mapVisionBot(data.visionBot), data.fileCount,
        data.files == null ? null
            : data.files.stream().map(this::mapFile).collect(Collectors.toList()),
        mapFileCount(data.productionFileDetails), mapFileCount(data.stagingFileDetails)
    );
  }

  private FileCount mapFileCount(final FileCountData data) {
    return data == null ? null : new FileCount(
        data.totalCount, data.totalSTPCount, data.unprocessedCount
    );
  }

  private File mapFile(final FileData data) {
    return data == null ? null : new File(
        data.id, data.name, data.location, data.format, data.processed
    );
  }

  private VisionBot mapVisionBot(final VisionBotData data) {
    return data == null ? null : new VisionBot(
        data.id, data.name, data.currentState, data.running, data.lockedUserId,
        data.lastModifiedByUser, data.lastModifiedTimestamp
    );
  }

  private CategoryDetail mapCategoryDetail(final CategoryDetailData data) {
    return data == null ? null : new CategoryDetail(
        data.id, data.noOfDocuments, data.priority, data.index,
        data.allFieldDetails == null ? null
            : data.allFieldDetails.stream().map(this::mapFieldDetail).collect(Collectors.toList())
    );
  }

  private FieldDetail mapFieldDetail(final FieldDetailData data) {
    return data == null ? null : new FieldDetail(
        data.fieldId, data.foundInTotalDocs
    );
  }

  public FileDetails mapFileDetails(final FileDetailsData data) {
    return data == null ? null : new FileDetails(
        data.fileId, data.projectId, data.fileName, data.fileLocation, data.fileSize,
        data.fileHeight, data.fileWidth, data.format, data.processed, data.classificationId,
        data.uploadRequestId, data.layoutId, data.isProduction
    );
  }

  public CountStatistics mapCountStatistics(final CountStatisticsData data) {
    return data == null ? null : new CountStatistics(
        data.projectId, data.categoryId, data.fileCount,
        mapEnvironmentCountStatistics(data.stagingFileCount),
        mapEnvironmentCountStatistics(data.productionFileCount)
    );
  }

  private EnvironmentCountStatistics mapEnvironmentCountStatistics(
      final EnvironmentCountStatisticsData model
  ) {
    return model == null ? null : new EnvironmentCountStatistics(
        model.unprocessedCount, model.totalCount
    );
  }

  public FilePatchDetailData mapFilePatchDetails(final FilePatchDetail model) {
    return model == null ? null : new FilePatchDetailData(model.op, model.path, model.value);
  }

  public FileBlobs mapFileBlobs(final FileBlobsData data) {
    return data == null ? null : new FileBlobs(
        data.fileId, data.fileBlobs
    );
  }

  public ClassificationAnalysisReport mapClassificationAnalysisReport(
      final ClassificationAnalysisReportData data
  ) {
    return data == null ? null : new ClassificationAnalysisReport(
        data.projectId, data.totalDocuments, data.allCategoryDetails == null ? null
        : data.allCategoryDetails.stream()
            .map(this::mapCategoryDetail).collect(Collectors.toList())
    );
  }

  public UploadResponse mapUploadResponse(final UploadResponseData data) {
    return data == null ? null : new UploadResponse(data.fileUploadJobId);
  }

  public FileStatus mapFileStatus(final FileStatusData m) {
    return m == null ? null : new FileStatus(
        m.projectId, m.currentProcessingObject, m.totalFileCount, m.totalProcessedCount,
        m.estimatedTimeRemainingInMins, m.complete
    );
  }

  public LearningInstanceSummary mapLearningInstanceSummary(
      final LearningInstanceSummaryData data
  ) {
    return data == null ? null : new LearningInstanceSummary(
        mapSummary(data.stagingFileSummary),
        mapSummary(data.productionFileSummary)
    );
  }

  private Summary mapSummary(final SummaryData data) {
    return data == null ? null : new Summary(
        data.totalGroups, data.documentsUnclassified, data.totalFiles
    );
  }

  public RawFileReportFromClassifierData mapRawFileReportFromClassifierData(
      final RawFileReportFromClassifier model
  ) {
    return model == null ? null : new RawFileReportFromClassifierData(
        model.documentId, model.fieldDetails == null ? Collections.emptyList()
            : model.fieldDetails.stream().map(this::mapRawFieldDetailFromClasssifier)
                .collect(Collectors.toList())
    );
  }

  private RawFieldDetailFromClasssifierData mapRawFieldDetailFromClasssifier(
      final RawFieldDetailFromClasssifier model
  ) {
    return model == null ? null : new RawFieldDetailFromClasssifierData(
        model.fieldId, model.aliasName, model.found
    );
  }
}
