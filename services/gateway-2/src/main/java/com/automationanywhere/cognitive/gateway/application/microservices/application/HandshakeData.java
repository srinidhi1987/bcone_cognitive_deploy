package com.automationanywhere.cognitive.gateway.application.microservices.application;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HandshakeData {

  public final String appVersion;
  public final String publicKey;
  public final String routingName;

  public HandshakeData(
      @JsonProperty("appVersion") final String appVersion,
      @JsonProperty("publicKey") final String publicKey,
      @JsonProperty("routingName") final String routingName
  ) {
    this.appVersion = appVersion;
    this.publicKey = publicKey;
    this.routingName = routingName;
  }
}
