package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class FieldDetailsData {

  public final List<String> standard;
  public final List<CustomFieldDetailData> custom;

  public FieldDetailsData(
      @JsonProperty("standard") final List<String> standard,
      @JsonProperty("custom") final List<CustomFieldDetailData> custom
  ) {
    this.standard = standard;
    this.custom = custom;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FieldDetailsData fieldDetail = (FieldDetailsData) o;

      result = Objects.equals(standard, fieldDetail.standard)
          && Objects.equals(custom, fieldDetail.custom);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(standard)
        ^ Objects.hashCode(custom);
  }
}

