package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

public abstract class JsonPatchActionData {

  public final String path;

  public JsonPatchActionData(
      final String path
  ) {
    this.path = path;
  }
}
