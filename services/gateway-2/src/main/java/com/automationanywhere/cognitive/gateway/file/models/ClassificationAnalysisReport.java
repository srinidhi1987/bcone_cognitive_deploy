package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.Objects;

public class ClassificationAnalysisReport {

  public final Id projectId;
  public final String totalDocuments;
  public final List<CategoryDetail> allCategoryDetails;

  public ClassificationAnalysisReport(
      final Id projectId,
      final String totalDocuments,
      final List<CategoryDetail> allCategoryDetails
  ) {
    this.projectId = projectId;
    this.totalDocuments = totalDocuments;
    this.allCategoryDetails = allCategoryDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ClassificationAnalysisReport classificationAnalysisReport = (ClassificationAnalysisReport) o;

      result = Objects.equals(projectId, classificationAnalysisReport.projectId)
          && Objects.equals(totalDocuments, classificationAnalysisReport.totalDocuments)
          && Objects
          .equals(allCategoryDetails, classificationAnalysisReport.allCategoryDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
        ^ Objects.hashCode(totalDocuments)
        ^ Objects.hashCode(allCategoryDetails);
  }
}

