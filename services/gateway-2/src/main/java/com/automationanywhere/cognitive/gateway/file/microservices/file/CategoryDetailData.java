package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class CategoryDetailData {

  public final Id id;
  public final Integer noOfDocuments;
  public final Integer priority;
  public final Integer index;

  @JsonProperty("allFieldDetail")
  public final List<FieldDetailData> allFieldDetails;

  public CategoryDetailData(
      @JsonProperty("id") final Id id,
      @JsonProperty("noOfDocuments") final Integer noOfDocuments,
      @JsonProperty("priority") final Integer priority,
      @JsonProperty("index") final Integer index,
      @JsonProperty("allFieldDetail") final List<FieldDetailData> allFieldDetails
  ) {
    this.id = id;
    this.noOfDocuments = noOfDocuments;
    this.priority = priority;
    this.index = index;
    this.allFieldDetails = allFieldDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CategoryDetailData categoryDetail = (CategoryDetailData) o;

      result = Objects.equals(id, categoryDetail.id)
        && Objects.equals(noOfDocuments, categoryDetail.noOfDocuments)
        && Objects.equals(priority, categoryDetail.priority)
        && Objects.equals(index, categoryDetail.index)
        && Objects.equals(allFieldDetails, categoryDetail.allFieldDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(noOfDocuments)
      ^ Objects.hashCode(priority)
      ^ Objects.hashCode(index)
      ^ Objects.hashCode(allFieldDetails);
  }
}

