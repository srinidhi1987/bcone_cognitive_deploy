package com.automationanywhere.cognitive.gateway.report.adapters;

import java.util.List;

public class AccuracyDto {

  private List<FieldAccuracyDto> fields;

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  public List<FieldAccuracyDto> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the fields to be set
   */
  public void setFields(List<FieldAccuracyDto> fields) {
    this.fields = fields;
  }
}
