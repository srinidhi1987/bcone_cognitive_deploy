package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class FilePatchDetail {

  public final String op;
  public final String path;
  public final String value;

  public FilePatchDetail(
      final String op,
      final String path,
      final String value
  ) {
    this.op = op;
    this.path = path;
    this.value = value;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FilePatchDetail projectPatchDetail
          = (FilePatchDetail) o;

      result = Objects.equals(op, projectPatchDetail.op)
          && Objects.equals(path, projectPatchDetail.path)
          && Objects.equals(value, projectPatchDetail.value);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(op)
        ^ Objects.hashCode(path)
        ^ Objects.hashCode(value);
  }
}

