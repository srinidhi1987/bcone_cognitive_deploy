package com.automationanywhere.cognitive.health;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.common.healthcheck.commands.MicroserviceTextHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.SimpleHttpStatusCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Project Service health check command.
 */
@Component
@SuppressWarnings("unused")
public class ProjectHealth extends SimpleHttpStatusCheckCommand {

  @Autowired
  public ProjectHealth(
      final ApplicationConfiguration config,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory,
      final AsyncRestTemplate restTemplate
  ) {
    super(
        "Project",
        Builder.get(config.projectURL()).build().url,
        buildInfoFactory,
        appInfoFactory,
        restTemplate
    );
  }
}
