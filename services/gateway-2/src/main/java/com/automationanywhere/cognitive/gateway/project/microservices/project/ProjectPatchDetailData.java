package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProjectPatchDetailData {

  public final String op;
  public final String path;
  public final Object value;
  public final Integer versionId;

  public ProjectPatchDetailData(
      @JsonProperty("op") final String op,
      @JsonProperty("path") final String path,
      @JsonProperty("value") final Object value,
      @JsonProperty("versionId") final Integer versionId
  ) {
    this.op = op;
    this.path = path;
    this.value = value;
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectPatchDetailData projectPatchDetailData
          = (ProjectPatchDetailData) o;

      result = Objects.equals(op, projectPatchDetailData.op)
          && Objects.equals(path, projectPatchDetailData.path)
          && Objects.equals(value, projectPatchDetailData.value)
          && Objects.equals(versionId, projectPatchDetailData.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(op)
        ^ Objects.hashCode(path)
        ^ Objects.hashCode(value)
        ^ Objects.hashCode(versionId);
  }
}

