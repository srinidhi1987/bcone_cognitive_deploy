package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class LearningInstanceSummary {

  public final Summary stagingFileSummary;
  public final Summary productionFileSummary;

  public LearningInstanceSummary(
      final Summary stagingFileSummary,
      final Summary productionFileSummary
  ) {
    this.stagingFileSummary = stagingFileSummary;
    this.productionFileSummary = productionFileSummary;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      LearningInstanceSummary learningInstanceSummary = (LearningInstanceSummary) o;

      result = Objects.equals(stagingFileSummary, learningInstanceSummary.stagingFileSummary)
        && Objects.equals(productionFileSummary, learningInstanceSummary.productionFileSummary);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(stagingFileSummary)
      ^ Objects.hashCode(productionFileSummary);
  }
}

