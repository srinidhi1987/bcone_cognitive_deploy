package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotStateDto {

  public final String state;

  public VisionBotStateDto(
      @JsonProperty("state") final String state
  ) {
    this.state = state;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotStateDto visionBotState = (VisionBotStateDto) o;

      result = Objects.equals(state, visionBotState.state);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(state);
  }
}

