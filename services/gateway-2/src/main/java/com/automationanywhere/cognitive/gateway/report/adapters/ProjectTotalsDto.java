package com.automationanywhere.cognitive.gateway.report.model;

import com.automationanywhere.cognitive.gateway.report.adapters.AccuracyDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ClassificationDto;
import com.automationanywhere.cognitive.gateway.report.adapters.ValidationDto;

public class ProjectTotalsDto {

  private long totalFilesProcessed;
  private long totalSTP;
  private long totalAccuracy;
  private long totalFilesUploaded;
  private long totalFilesToValidation;
  private long totalFilesValidated;
  private long totalFilesUnprocessable;
  private ClassificationDto classification;
  private AccuracyDto accuracy;
  private ValidationDto validation;

  /**
   * Returns the totalFilesProcessed.
   *
   * @return the value of totalFilesProcessed
   */
  public long getTotalFilesProcessed() {
    return totalFilesProcessed;
  }

  /**
   * Sets the totalFilesProcessed.
   *
   * @param totalFilesProcessed the totalFilesProcessed to be set
   */
  public void setTotalFilesProcessed(long totalFilesProcessed) {
    this.totalFilesProcessed = totalFilesProcessed;
  }

  /**
   * Returns the totalSTP.
   *
   * @return the value of totalSTP
   */
  public long getTotalSTP() {
    return totalSTP;
  }

  /**
   * Sets the totalSTP.
   *
   * @param totalSTP the totalSTP to be set
   */
  public void setTotalSTP(long totalSTP) {
    this.totalSTP = totalSTP;
  }

  /**
   * Returns the totalAccuracy.
   *
   * @return the value of totalAccuracy
   */
  public long getTotalAccuracy() {
    return totalAccuracy;
  }

  /**
   * Sets the totalAccuracy.
   *
   * @param totalAccuracy the totalAccuracy to be set
   */
  public void setTotalAccuracy(long totalAccuracy) {
    this.totalAccuracy = totalAccuracy;
  }

  /**
   * Returns the totalFilesUploaded.
   *
   * @return the value of totalFilesUploaded
   */
  public long getTotalFilesUploaded() {
    return totalFilesUploaded;
  }

  /**
   * Sets the totalFilesUploaded.
   *
   * @param totalFilesUploaded the totalFilesUploaded to be set
   */
  public void setTotalFilesUploaded(long totalFilesUploaded) {
    this.totalFilesUploaded = totalFilesUploaded;
  }

  /**
   * Returns the totalFilesToValidation.
   *
   * @return the value of totalFilesToValidation
   */
  public long getTotalFilesToValidation() {
    return totalFilesToValidation;
  }

  /**
   * Sets the totalFilesToValidation.
   *
   * @param totalFilesToValidation the totalFilesToValidation to be set
   */
  public void setTotalFilesToValidation(long totalFilesToValidation) {
    this.totalFilesToValidation = totalFilesToValidation;
  }

  /**
   * Returns the totalFilesValidated.
   *
   * @return the value of totalFilesValidated
   */
  public long getTotalFilesValidated() {
    return totalFilesValidated;
  }

  /**
   * Sets the totalFilesValidated.
   *
   * @param totalFilesValidated the totalFilesValidated to be set
   */
  public void setTotalFilesValidated(long totalFilesValidated) {
    this.totalFilesValidated = totalFilesValidated;
  }

  /**
   * Returns the totalFilesUnprocessable.
   *
   * @return the value of totalFilesUnprocessable
   */
  public long getTotalFilesUnprocessable() {
    return totalFilesUnprocessable;
  }

  /**
   * Sets the totalFilesUnprocessable.
   *
   * @param totalFilesUnprocessable the totalFilesUnprocessable to be set
   */
  public void setTotalFilesUnprocessable(long totalFilesUnprocessable) {
    this.totalFilesUnprocessable = totalFilesUnprocessable;
  }

  /**
   * Returns the classification.
   *
   * @return the value of classification
   */
  public ClassificationDto getClassification() {
    return classification;
  }

  /**
   * Sets the classification.
   *
   * @param classification the classification to be set
   */
  public void setClassification(
      ClassificationDto classification) {
    this.classification = classification;
  }

  /**
   * Returns the accuracy.
   *
   * @return the value of accuracy
   */
  public AccuracyDto getAccuracy() {
    return accuracy;
  }

  /**
   * Sets the accuracy.
   *
   * @param accuracy the accuracy to be set
   */
  public void setAccuracy(AccuracyDto accuracy) {
    this.accuracy = accuracy;
  }

  /**
   * Returns the validation.
   *
   * @return the value of validation
   */
  public ValidationDto getValidation() {
    return validation;
  }

  /**
   * Sets the validation.
   *
   * @param validation the validation to be set
   */
  public void setValidation(
      ValidationDto validation) {
    this.validation = validation;
  }
}
