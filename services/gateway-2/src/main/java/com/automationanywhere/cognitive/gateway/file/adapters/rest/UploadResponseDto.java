package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class UploadResponseDto {

  public final String fileUploadJobId;

  public UploadResponseDto(
      @JsonProperty("fileUploadJobId") final String fileUploadJobId
  ) {
    this.fileUploadJobId = fileUploadJobId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      UploadResponseDto uploadResponse = (UploadResponseDto) o;

      result = Objects.equals(fileUploadJobId, uploadResponse.fileUploadJobId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileUploadJobId);
  }
}

