package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ValidatorReviewDetailsData {

  public final String totalFilesReviewed;
  public final String totalFilesMarkedInvalid;
  public final String totalFailedFiles;
  public final String averageReviewTime;

  public ValidatorReviewDetailsData(
      @JsonProperty("totalFilesReviewed") final String totalFilesReviewed,
      @JsonProperty("totalFilesMarkedInvalid") final String totalFilesMarkedInvalid,
      @JsonProperty("totalFailedFiles") final String totalFailedFiles,
      @JsonProperty("averageReviewTime") final String averageReviewTime
  ) {
    this.totalFilesReviewed = totalFilesReviewed;
    this.totalFilesMarkedInvalid = totalFilesMarkedInvalid;
    this.totalFailedFiles = totalFailedFiles;
    this.averageReviewTime = averageReviewTime;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ValidatorReviewDetailsData validatorReviewDetails = (ValidatorReviewDetailsData) o;

      result = Objects.equals(totalFilesReviewed, validatorReviewDetails.totalFilesReviewed)
        && Objects.equals(totalFilesMarkedInvalid, validatorReviewDetails.totalFilesMarkedInvalid)
        && Objects.equals(totalFailedFiles, validatorReviewDetails.totalFailedFiles)
        && Objects.equals(averageReviewTime, validatorReviewDetails.averageReviewTime);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalFilesReviewed)
      ^ Objects.hashCode(totalFilesMarkedInvalid)
      ^ Objects.hashCode(totalFailedFiles)
      ^ Objects.hashCode(averageReviewTime);
  }
}

