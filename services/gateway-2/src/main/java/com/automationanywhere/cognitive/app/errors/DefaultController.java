package com.automationanywhere.cognitive.app.errors;

import com.automationanywhere.cognitive.path.PathManager;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.NoHandlerFoundException;

@Controller
public class DefaultController {
  private final PathManager pathManager;

  public DefaultController(final PathManager pathManager) {
    this.pathManager = pathManager;
  }

  @RequestMapping("/**")
  public void handle(final HttpServletRequest req) throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.putAll(Collections.list(req.getHeaderNames()).stream().map(
        n -> new SimpleEntry<>(n, Collections.list(req.getHeaders(n)))
    ).collect(Collectors.toMap(
        SimpleEntry::getKey,
        SimpleEntry::getValue
    )));

    throw new NoHandlerFoundException(
        req.getMethod(),
        pathManager.create(req),
        headers
    );
  }
}
