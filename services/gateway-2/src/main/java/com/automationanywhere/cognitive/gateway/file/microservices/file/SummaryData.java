package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class SummaryData {

  public final Integer totalGroups;
  public final Integer documentsUnclassified;
  public final Integer totalFiles;

  public SummaryData(
      @JsonProperty("totalGroups") final Integer totalGroups,
      @JsonProperty("documentsUnclassified") final Integer documentsUnclassified,
      @JsonProperty("totalFiles") final Integer totalFiles
  ) {
    this.totalGroups = totalGroups;
    this.documentsUnclassified = documentsUnclassified;
    this.totalFiles = totalFiles;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      SummaryData summary = (SummaryData) o;

      result = Objects.equals(totalGroups, summary.totalGroups)
          && Objects.equals(documentsUnclassified, summary.documentsUnclassified)
          && Objects.equals(totalFiles, summary.totalFiles);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalGroups)
        ^ Objects.hashCode(documentsUnclassified)
        ^ Objects.hashCode(totalFiles);
  }
}

