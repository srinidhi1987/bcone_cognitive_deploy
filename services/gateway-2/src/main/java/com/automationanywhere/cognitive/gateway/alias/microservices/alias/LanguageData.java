package com.automationanywhere.cognitive.gateway.alias.microservices.alias;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LanguageData {

  public final Id id;
  public final String name;
  public final String code;

  public LanguageData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("code") final String code
  ) {
    this.id = id;
    this.name = name;
    this.code = code;
  }
}
