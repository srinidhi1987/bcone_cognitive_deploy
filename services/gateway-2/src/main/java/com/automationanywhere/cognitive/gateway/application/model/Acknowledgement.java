package com.automationanywhere.cognitive.gateway.application.model;

public class Acknowledgement {

  public final String appId;

  public Acknowledgement(final String appId) {
    this.appId = appId;
  }
}
