package com.automationanywhere.cognitive.app;

import com.automationanywhere.cognitive.common.configuration.ConfigurationProvider;
import java.util.Properties;

public class ApplicationConfiguration {

  private static final String DEFAULT_BUILD_RESOURCE_NAME = "build.version";

  private final ConfigurationProvider provider;
  private final Properties props;

  public ApplicationConfiguration(final ConfigurationProvider provider) {
    this(provider, DEFAULT_BUILD_RESOURCE_NAME);
  }

  public ApplicationConfiguration(
      final ConfigurationProvider provider,
      final String buildResourceName
  ) {
    this.provider = provider;

    props = createProperties(buildResourceName);
  }

  private Properties createProperties(final String buildResourceName) {
    Properties props = new Properties();
    try {
      props.load(getClass().getClassLoader().getResourceAsStream(buildResourceName));
    } catch (final Exception ex) {
      throw new RuntimeException("Fatal error, cannot load the build details", ex);
    }
    return props;
  }

  public String applicationName() {
    return props.getProperty("Application-name");
  }

  public String applicationVersion() {
    return props.getProperty("Version");
  }

  public long asyncExecutorCorePoolSize() {
    return provider.getValue("aa.cognitive.gateway.async-executor.core-pool-size");
  }

  public long asyncExecutorMaxPoolSize() {
    return provider.getValue("aa.cognitive.gateway.async-executor.max-pool-size");
  }

  public long asyncExecutorKeepAliveTimeout() {
    return provider.getValue("aa.cognitive.gateway.async-executor.keep-alive-timeout");
  }

  public long asyncExecutorShutdownTimeout() {
    return provider.getValue("aa.cognitive.gateway.async-executor.shutdown-timeout");
  }

  public long restTemplateMaxConnCount() {
    return provider.getValue("aa.cognitive.gateway.rest-template.max-conn-count");
  }

  public long restTemplateKeepAliveTimeout() {
    return provider.getValue("aa.cognitive.gateway.rest-template.keep-alive-timeout");
  }

  public long restTemplateConnectTimeout() {
    return provider.getValue("aa.cognitive.gateway.rest-template.connect-timeout");
  }

  public long restTemplateReadTimeout() {
    return provider.getValue("aa.cognitive.gateway.rest-template.read-timeout");
  }

  public long restTemplateMaxRetryCount() {
    return provider.getValue("aa.cognitive.gateway.rest-template.max-retry-count");
  }

  public long restTemplateRetryDelay() {
    return provider.getValue("aa.cognitive.gateway.rest-template.retry-delay");
  }

  public long asyncHTTPServletRequestTimeout() {
    return provider.getValue("aa.cognitive.gateway.async-http-servlet-request-timeout");
  }

  public String authURL() {
    return provider.getValue("aa.cognitive.micro-services.auth.url");
  }

  public String authHealthCheck() {
    return provider.getValue("aa.cognitive.micro-services.auth.health-check");
  }

  public String authenticate() {
    return provider.getValue("aa.cognitive.micro-services.auth.authenticate");
  }

  public String authorize() {
    return provider.getValue("aa.cognitive.micro-services.auth.authorize");
  }

  public String invalidateSession() {
    return provider.getValue("aa.cognitive.micro-services.auth.invalidate-session");
  }

  public String projectURL() {
    return provider.getValue("aa.cognitive.micro-services.project.url");
  }

  public String projectHealthCheck() {
    return provider.getValue("aa.cognitive.micro-services.project.health-check");
  }

  public String visionBotHealthCheck() {
    return provider.getValue("aa.cognitive.micro-services.vision-bot.health-check");
  }

  public String validatorHealthCheck() {
    return provider.getValue("aa.cognitive.micro-services.validator.health-check");
  }

  public String getOrganizationProjects() {
    return provider.getValue("aa.cognitive.micro-services.project.get-organization-projects");
  }

  public String getOrganizationProjectMetadataList() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.get-organization-project-metadata-list"
    );
  }

  public String getOrganizationProject() {
    return provider.getValue("aa.cognitive.micro-services.project.get-organization-project");
  }

  public String getOrganizationProjectMetadata() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.get-organization-project-metadata"
    );
  }

  public String createOrganizationProject() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.create-organization-project"
    );
  }

  public String updateOrganizationProject() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.update-organization-project"
    );
  }

  public String patchOrganizationProject() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.patch-organization-project"
    );
  }

  public String deleteOrganizationProject() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.delete-organization-project"
    );
  }

  public String getArchiveDetailList() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.get-archive-detail-list"
    );
  }

  public String importOrganizationProjects() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.import-organization-projects"
    );
  }

  public String exportOrganizationProjects() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.export-organization-projects"
    );
  }

  public String getOrganizationProjectTaskStatus() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.get-organization-project-task-status"
    );
  }

  public String getOrganizationProjectTasks() {
    return provider.getValue(
        "aa.cognitive.micro-services.project.get-organization-project-tasks"
    );
  }

  public String aliasServiceURL() {
    return provider.getValue("aa.cognitive.micro-services.alias.url");
  }

  public String getDomainsLanguagesUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-domains-languages");
  }

  public String getDomainsUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-domains");
  }

  public String importDomainsUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.import-domains");
  }

  public String getAllProjectTypeUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-all-project-types");
  }

  public String getProjectTypeUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-project-types");
  }

  public String getProjectFieldsUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-all-project-fields");
  }

  public String getFieldUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-field");
  }

  public String getAliasesUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-aliases");
  }

  public String getLanguagesUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-languages");
  }

  public String getLanguageUri() {
    return provider.getValue("aa.cognitive.micro-services.alias.get-language");
  }

  public String fileURL() {
    return provider.getValue("aa.cognitive.micro-services.file.url");
  }

  public String fileHealthCheck() {
    return provider.getValue("aa.cognitive.micro-services.file.health-check");
  }

  public String getOrganizationProjectCategories() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-categories"
    );
  }

  public String getOrganizationProjectCategoryFiles() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-category-files"
    );
  }

  public String getOrganizationFileCount() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-file-count"
    );
  }

  public String getOrganizationProjectFileCount() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-file-count"
    );
  }

  public String getOrganizationProjectFiles() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-files"
    );
  }

  public String getOrganizationProjectFile() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-file"
    );
  }

  public String patchOrganizationProjectFile() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.patch-organization-project-file"
    );
  }

  public String uploadOrganizationProjectFiles() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.upload-organization-project-files"
    );
  }

  public String uploadOrganizationProjectStagingFiles() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.upload-organization-project-staging-files"
    );
  }

  public String deleteOrganizationProjectFile() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.delete-organization-project-file"
    );
  }

  public String getOrganizationProjectFileBlobs() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-fileblobs"
    );
  }

  public String getOrganizationProjectReport() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-report"
    );
  }

  public String getOrganizationProjectFileStatus() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-filestatus"
    );
  }

  public String getOrganizationProjectStagingFiles() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-staging-files"
    );
  }

  public String createSegmentedDocument() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.create-segmenteddocument"
    );
  }

  public String getSegmentedDocument() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-segmenteddocument"
    );
  }

  public String getOrganizationProjectClassificationSummary() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.get-organization-project-classificationsummary"
    );
  }

  public String updateOrganizationProjectClassification() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.update-organization-project-classification"
    );
  }

  public String updateOrganizationProjectClassificationById() {
    return provider.getValue(
        "aa.cognitive.micro-services.file.update-organization-project-classification-by-id"
    );
  }

  public String visionBotURL() {
    return provider.getValue("aa.cognitive.micro-services.vision-bot.url");
  }

  public String unlockVisionBots() {
    return provider.getValue("aa.cognitive.micro-services.vision-bot.unlock-vision-bots");
  }

  public String getVisionBotForEdit() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-vision-bot-for-edit"
    );
  }

  public String getVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-vision-bot"
    );
  }

  public String getVisionBotMetadata() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-vision-bot-metadata"
    );
  }

  public String getVisionBotById() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-vision-bot-by-id"
    );
  }

  public String patchVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.patch-vision-bot"
    );
  }

  public String getVisionBotByLayout() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-vision-bot-by-layout"
    );
  }

  public String getVisionBotByLayoutAndPage() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-vision-bot-by-layout-and-page"
    );
  }

  public String updateVisionBotById() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.update-vision-bot-by-id"
    );
  }

  public String getProjectVisionBotMetadata() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-project-vision-bot-metadata"
    );
  }

  public String getOrganizationVisionBotMetadata() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-organization-vision-bot-metadata"
    );
  }

  public String getOrganizationVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-organization-vision-bot"
    );
  }

  public String getOrganizationVisionBotById() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-organization-vision-bot-by-id"
    );
  }

  public String getProjectVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-project-vision-bot"
    );
  }

  public String updateVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.update-vision-bot"
    );
  }

  public String unlock() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.unlock"
    );
  }

  public String getValueField() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-value-field"
    );
  }

  public String runVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.run-vision-bot"
    );
  }

  public String getValidationData() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-validation-data"
    );
  }

  public String getPageImage() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-page-image"
    );
  }

  public String getSummary() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-summary"
    );
  }

  public String getTestSet() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.get-test-set"
    );
  }

  public String updateTestSet() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.update-test-set"
    );
  }

  public String deleteTestSet() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.delete-test-set"
    );
  }

  public String setState() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.set-state"
    );
  }

  public String getMutipleState() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.set-multiple-state"
    );
  }

  public String exportToCSV() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.export-to-csv"
    );
  }

  public String updateValidationData() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.update-validation-data"
    );
  }

  public String updateAutoMappingData() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.update-auto-mapping-data"
    );
  }

  public String updateValidationDocument() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.update-validation-document"
    );
  }

  public String generateVisionBotData() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.generate-vision-bot-data"
    );
  }

  public String generateVisionBotDataPaginated() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.generate-vision-bot-data-paginated"
    );
  }

  public String keepAliveLockedVisionBot() {
    return provider.getValue(
        "aa.cognitive.micro-services.vision-bot.keep-alive-locked-vision-bot"
    );
  }

  public String validatorURL() {
    return provider.getValue("aa.cognitive.micro-services.validator.url");
  }

  public String validatorKeepAlive() {
    return provider.getValue("aa.cognitive.micro-services.validator.keep-alive");
  }

  public String unlockDocuments() {
    return provider.getValue("aa.cognitive.micro-services.validator.unlock-documents");
  }

  public String getReviewedFileCountDetails() {
    return provider
        .getValue("aa.cognitive.micro-services.validator.get-reviewed-file-count-details");
  }

  public String getFailedVisionBotCount() {
    return provider.getValue("aa.cognitive.micro-services.validator.get-failed-vision-bot-count");
  }

  public String unlockProject() {
    return provider.getValue("aa.cognitive.micro-services.validator.unlock-project");
  }

  public String getInvalidFileTypeList() {
    return provider.getValue("aa.cognitive.micro-services.validator.get-reasons");
  }

  public String getProductionFileSummary() {
    return provider.getValue("aa.cognitive.micro-services.validator.get-production-file-summary");
  }

  public String markFileInvalid() {
    return provider.getValue("aa.cognitive.micro-services.validator.mark-file-invalid");
  }

  public String getFailedVbotData() {
    return provider.getValue("aa.cognitive.micro-services.validator.get-failed-vbot-data");
  }

  public String getAccuracyProcessingDetails() {
    return provider
        .getValue("aa.cognitive.micro-services.validator.get-accuracy-processing-details");
  }

  public String getProjectTotalsAccuracyDetails() {
    return provider
        .getValue("aa.cognitive.micro-services.validator.get-project-totals-accuracy-details");
  }

  public String getSendCorrectDataValues() {
    return provider.getValue("aa.cognitive.micro-services.validator.send-correct-data-values");
  }

  public String getPatchCorrectDataValues() {
    return provider.getValue("aa.cognitive.micro-services.validator.patch-correct-data-values");
  }

  public String getUpdateValidationStatus() {
    return provider.getValue("aa.cognitive.micro-services.validator.get-update-validation-status");
  }

  public String reportServiceURL() {
    return provider.getValue("aa.cognitive.micro-services.report.url");
  }

  public String getDashboardReportTotals() {
    return provider.getValue("aa.cognitive.micro-services.report.get-dashboard-report-totals");
  }

  public String getReportTotals() {
    return provider.getValue("aa.cognitive.micro-services.report.get-report-totals");
  }

  public String getReports() {
    return provider.getValue("aa.cognitive.micro-services.report.get-reports");
  }

  public long resourceLockerSchedulerShutdownTimeout() {
    return provider.getValue("aa.cognitive.gateway.resource-locker.scheduler-shutdown-timeout");
  }

  public long resourceLockerPollPeriod() {
    return provider.getValue("aa.cognitive.gateway.resource-locker.poll-period");
  }

  public String resourceLockerImportResource() {
    return provider.getValue("aa.cognitive.gateway.resource-locker.resources.import");
  }

  public String resourceLockerExportResource() {
    return provider.getValue("aa.cognitive.gateway.resource-locker.resources.export");
  }

  public String mlServiceURL() {
    return provider.getValue("aa.cognitive.micro-services.ml.url");
  }

  public String predict() {
    return provider.getValue("aa.cognitive.micro-services.ml.predict");
  }

  public String value() {
    return provider.getValue("aa.cognitive.micro-services.ml.value");
  }

  public String applicationServiceURL() {
    return provider.getValue("aa.cognitive.micro-services.application.url");
  }

  public String handshake() {
    return provider.getValue("aa.cognitive.micro-services.application.handshake");
  }

  public String acknowledge() {
    return provider.getValue("aa.cognitive.micro-services.application.acknowledge");
  }

  public String configuration() {
    return provider.getValue("aa.cognitive.micro-services.application.configuration");
  }
}
