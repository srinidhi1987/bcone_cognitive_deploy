package com.automationanywhere.cognitive.gateway.alias.microservices.alias;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ProjectTypeData {

  public final Id id;
  public final String name;
  public final String description;
  public final List<ProjectFieldData> fields;
  public final List<LanguageData> languages;

  public ProjectTypeData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("description") final String description,
      @JsonProperty("fields") final List<ProjectFieldData> fields,
      @JsonProperty("languages") final List<LanguageData> languages
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.fields = fields;
    this.languages = languages;
  }
}
