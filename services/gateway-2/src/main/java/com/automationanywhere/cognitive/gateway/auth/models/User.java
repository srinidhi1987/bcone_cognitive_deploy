package com.automationanywhere.cognitive.gateway.auth.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;

public class User {

  public final Id id;
  public final String name;
  public final List<String> roles;

  public User(final Id id, final String name, final List<String> roles) {
    this.id = id;
    this.name = name;
    this.roles = roles;
  }
}
