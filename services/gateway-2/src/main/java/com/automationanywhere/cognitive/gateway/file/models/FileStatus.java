package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class FileStatus {

  public final Id projectId;
  public final String currentProcessingObject;
  public final Integer totalFileCount;
  public final Integer totalProcessedCount;
  public final Integer estimatedTimeRemainingInMins;
  public final Boolean complete;

  public FileStatus(
      final Id projectId,
      final String currentProcessingObject,
      final Integer totalFileCount,
      final Integer totalProcessedCount,
      final Integer estimatedTimeRemainingInMins,
      final Boolean complete
  ) {
    this.projectId = projectId;
    this.currentProcessingObject = currentProcessingObject;
    this.totalFileCount = totalFileCount;
    this.totalProcessedCount = totalProcessedCount;
    this.estimatedTimeRemainingInMins = estimatedTimeRemainingInMins;
    this.complete = complete;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileStatus fileStatus = (FileStatus) o;

      result = Objects.equals(projectId, fileStatus.projectId)
          && Objects.equals(currentProcessingObject, fileStatus.currentProcessingObject)
          && Objects.equals(totalFileCount, fileStatus.totalFileCount)
          && Objects.equals(totalProcessedCount, fileStatus.totalProcessedCount)
          && Objects.equals(estimatedTimeRemainingInMins, fileStatus.estimatedTimeRemainingInMins)
          && Objects.equals(complete, fileStatus.complete);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
        ^ Objects.hashCode(currentProcessingObject)
        ^ Objects.hashCode(totalFileCount)
        ^ Objects.hashCode(totalProcessedCount)
        ^ Objects.hashCode(estimatedTimeRemainingInMins)
        ^ Objects.hashCode(complete);
  }
}

