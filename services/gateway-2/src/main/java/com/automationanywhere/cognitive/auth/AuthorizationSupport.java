package com.automationanywhere.cognitive.auth;

import com.automationanywhere.cognitive.common.auth.AnonymousAccess;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.auth.AuthService;
import com.automationanywhere.cognitive.gateway.auth.models.Authorization;
import com.automationanywhere.cognitive.gateway.auth.models.JWTAuthorization;
import com.automationanywhere.cognitive.gateway.auth.models.KeyTokenAuthorization;
import com.automationanywhere.cognitive.path.PathManager;
import java.lang.reflect.Method;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationSupport {

  private final AuthService authService;
  private final PathManager pathManager;
  private final ExceptionHandlerFactory ehf;

  @Autowired
  public AuthorizationSupport(
      final AuthService authService,
      final PathManager pathManager,
      final ExceptionHandlerFactory ehf
  ) {
    this.authService = authService;
    this.pathManager = pathManager;
    this.ehf = ehf;
  }

  public void authorize(final Method method, final String clientKey, final String clientToken, final String jwt) {
    if (method.getAnnotation(AnonymousAccess.class) == null) {
      ehf.execute(() -> {
        Authorization authorization = null;
        if (Optional.ofNullable(clientKey).orElse("").isEmpty()
            && Optional.ofNullable(clientToken).orElse("").isEmpty()) {
          authorization = new JWTAuthorization(pathManager.create(pathManager.create(method)), jwt);
        } else {
          authorization = new KeyTokenAuthorization(clientKey, clientToken);
        }
        authService.authorize(authorization).get();
      });
    }
  }
}
