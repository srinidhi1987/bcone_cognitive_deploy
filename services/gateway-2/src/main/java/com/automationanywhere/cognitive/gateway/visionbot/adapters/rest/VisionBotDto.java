package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotDto {

  public final Id id;
  public final String visionBotData;
  public final String validationData;

  public VisionBotDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("visionBotData") final String visionBotData,
      @JsonProperty("validationData") final String validationData
  ) {
    this.id = id;
    this.visionBotData = visionBotData;
    this.validationData = validationData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      VisionBotDto that = (VisionBotDto) o;

      result = Objects.equals(visionBotData, that.visionBotData)
          && Objects.equals(validationData, that.validationData)
          && Objects.equals(id, that.id);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(visionBotData)
        ^ Objects.hashCode(validationData)
        ^ Objects.hashCode(id);
  }
}

