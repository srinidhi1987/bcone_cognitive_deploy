package com.automationanywhere.cognitive.gateway.report.microservice.report.model;

import java.util.List;

public class AccuracyData {

  private List<FieldAccuracyData> fields;

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  public List<FieldAccuracyData> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the fields to be set
   */
  public void setFields(List<FieldAccuracyData> fields) {
    this.fields = fields;
  }
}
