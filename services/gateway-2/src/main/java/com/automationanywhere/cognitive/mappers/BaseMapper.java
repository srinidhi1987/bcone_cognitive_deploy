package com.automationanywhere.cognitive.mappers;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.errors.Mapper;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Base class of mappers.
 *
 * Implements method common for all mappers.
 */
public abstract class BaseMapper {

  private final ExceptionHandlerFactory exceptionHandlerFactory = new ExceptionHandlerFactory();

  /**
   * Maps a list using a mapper.
   *
   * @param list - list to map.
   * @param mapper - mapper to apply
   * @return mapped list.
   */
  public <I, O> List<O> mapList(final List<I> list, final Mapper<I, O> mapper) {
    return list == null ? Collections.emptyList()
        : list.stream().map(e -> exceptionHandlerFactory.map(e, mapper))
            .collect(Collectors.toList());
  }
}
