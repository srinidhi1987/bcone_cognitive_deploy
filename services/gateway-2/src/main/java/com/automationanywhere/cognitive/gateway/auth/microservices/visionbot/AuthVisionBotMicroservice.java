package com.automationanywhere.cognitive.gateway.auth.microservices.visionbot;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AuthVisionBotMicroservice {

  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public AuthVisionBotMicroservice(
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.restClient = restClient;
    this.cfg = cfg;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<?> unlockVisionBots(String username) {
    return restClient.send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.unlockVisionBots())
            .setHeader("username", username)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    ).thenApply(d -> null); // Return null instead of what the REST client returns.
  }
}
