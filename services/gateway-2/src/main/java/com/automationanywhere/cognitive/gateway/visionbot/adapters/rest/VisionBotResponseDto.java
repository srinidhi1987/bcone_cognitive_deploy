package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;

public class VisionBotResponseDto implements Serializable {
  private static final long serialVersionUID = -4151021958940711924L;
  public final Boolean success;
  public final String data;
  public String error;

  public VisionBotResponseDto(
      @JsonProperty("success") final Boolean success,
      @JsonProperty("data") final String data,
      @JsonProperty("errors") final String errors) {
    this.success = success;
    this.data = data;
    this.error = error;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VisionBotResponseDto that = (VisionBotResponseDto) o;
    return Objects.equals(success, that.success) &&
        Objects.equals(data, that.data) &&
        Objects.equals(error, that.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, data, error);
  }
}
