package com.automationanywhere.cognitive.gateway.validator.models;

public class ClassificationFieldDetails {

  private final String fieldId;
  private final double representationPercent;

  public ClassificationFieldDetails(
      String fieldId,
      double representationPercent
  ) {
    this.fieldId = fieldId;
    this.representationPercent = representationPercent;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the representationPercent.
   *
   * @return the value of representationPercent
   */
  public double getRepresentationPercent() {
    return representationPercent;
  }
}
