package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class ValidationDetailsDto {

  @JsonProperty("ValidatorData")
  public final List<ValidatorDetailsDto> validatorData;

  public ValidationDetailsDto(
      @JsonProperty("ValidatorData") final List<ValidatorDetailsDto> validatorData
  ) {
    this.validatorData = validatorData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ValidationDetailsDto validationDetails = (ValidationDetailsDto) o;

      result = Objects.equals(validatorData, validationDetails.validatorData);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(validatorData);
  }
}

