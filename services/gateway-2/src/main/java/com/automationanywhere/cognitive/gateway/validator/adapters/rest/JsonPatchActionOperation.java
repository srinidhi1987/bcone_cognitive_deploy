package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

public enum JsonPatchActionOperation {

  ADD, REPLACE, REMOVE, TEST, COPY, MOVE;
}
