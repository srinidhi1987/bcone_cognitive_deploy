package com.automationanywhere.cognitive.resourcelock;

public class UnlockedException extends RuntimeException {

  private static final long serialVersionUID = -4686859426817696209L;
}
