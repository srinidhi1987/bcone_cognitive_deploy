package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class CredentialsDto {

  public final String username;
  public final String password;

  public CredentialsDto(
      @JsonProperty("username") final String username,
      @JsonProperty("password") final String password
  ) {
    this.username = username;
    this.password = password;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      CredentialsDto that = (CredentialsDto) o;

      result = Objects.equals(username, that.username)
          && Objects.equals(password, that.password);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(username)
        ^ Objects.hashCode(password);
  }
}
