package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class CategoriesDto {

  public final Integer offset;
  public final Integer limit;
  public final String sort;
  public final List<CategoryDto> categories;

  public CategoriesDto(
      @JsonProperty("offset") final Integer offset,
      @JsonProperty("limit") final Integer limit,
      @JsonProperty("sort") final String sort,
      @JsonProperty("categories") final List<CategoryDto> categories
  ) {
    this.offset = offset;
    this.limit = limit;
    this.sort = sort;
    this.categories = categories;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CategoriesDto categories = (CategoriesDto) o;

      result = Objects.equals(offset, categories.offset)
        && Objects.equals(limit, categories.limit)
        && Objects.equals(sort, categories.sort)
        && Objects.equals(this.categories, categories.categories);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(offset)
      ^ Objects.hashCode(limit)
      ^ Objects.hashCode(sort)
      ^ Objects.hashCode(categories);
  }
}

