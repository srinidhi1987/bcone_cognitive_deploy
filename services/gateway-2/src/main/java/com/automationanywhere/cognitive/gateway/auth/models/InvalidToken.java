package com.automationanywhere.cognitive.gateway.auth.models;

import java.util.Objects;

public class InvalidToken {

  public final String token;

  public InvalidToken(final String token) {
    this.token = token;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof InvalidToken)) {
      return false;
    }
    InvalidToken that = (InvalidToken) o;
    return Objects.equals(token, that.token);
  }

  @Override
  public int hashCode() {
    return Objects.hash(token);
  }
}
