package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class LearningInstanceSummaryData {

  public final SummaryData stagingFileSummary;
  public final SummaryData productionFileSummary;

  public LearningInstanceSummaryData(
      @JsonProperty("stagingFileSummary") final SummaryData stagingFileSummary,
      @JsonProperty("productionFileSummary") final SummaryData productionFileSummary
  ) {
    this.stagingFileSummary = stagingFileSummary;
    this.productionFileSummary = productionFileSummary;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      LearningInstanceSummaryData learningInstanceSummary = (LearningInstanceSummaryData) o;

      result = Objects.equals(stagingFileSummary, learningInstanceSummary.stagingFileSummary)
          && Objects.equals(productionFileSummary, learningInstanceSummary.productionFileSummary);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(stagingFileSummary)
        ^ Objects.hashCode(productionFileSummary);
  }
}

