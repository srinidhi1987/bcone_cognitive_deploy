package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GroupTimeSpentDetailsDto {

  private final String categoryId;
  private final double timeSpend;

  public GroupTimeSpentDetailsDto(
      @JsonProperty("categoryId") String categoryId,
      @JsonProperty("timeSpend") double timeSpend
  ) {
    this.categoryId = categoryId;
    this.timeSpend = timeSpend;
  }

  /**
   * Returns the categoryId.
   *
   * @return the value of categoryId
   */
  public String getCategoryId() {
    return categoryId;
  }

  /**
   * Returns the timeSpend.
   *
   * @return the value of timeSpend
   */
  public double getTimeSpend() {
    return timeSpend;
  }
}
