package com.automationanywhere.cognitive.app;

import com.automationanywhere.cognitive.app.accesslogger.tomcat.CustomTomcatEmbeddedServletContainerFactory;
import com.automationanywhere.cognitive.app.auth.AuthorizationInterceptor;
import com.automationanywhere.cognitive.app.id.IdModule;
import com.automationanywhere.cognitive.app.resourcelock.ResourceLockInterceptor;
import com.automationanywhere.cognitive.app.threadpool.ThreadPoolFactory;
import com.automationanywhere.cognitive.auth.AuthorizationSupport;
import com.automationanywhere.cognitive.common.configuration.ConfigurationProvider;
import com.automationanywhere.cognitive.common.configuration.FileConfigurationProvider;
import com.automationanywhere.cognitive.common.configuration.FileConfigurationProviderFactory;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.integer.LongIdTransformer;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.automationanywhere.cognitive.app.enums.EnumsModule;
import com.automationanywhere.cognitive.path.PathManager;
import com.automationanywhere.cognitive.resourcelock.ResourceFactory;
import com.automationanywhere.cognitive.resourcelock.ResourceLocker;
import com.automationanywhere.cognitive.restclient.resttemplate.ApacheAsyncHTTPClientRequestFactoryCreator;
import com.automationanywhere.cognitive.restclient.resttemplate.CustomThreadFactory;
import com.automationanywhere.cognitive.restclient.resttemplate.JacksonHttpMessageConverter;
import com.automationanywhere.cognitive.restclient.resttemplate.LoggingRestTemplateInterceptor;
import com.automationanywhere.cognitive.restclient.resttemplate.StandardHeaderRestTemplate;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.client.AsyncClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * API Gateway SpringBoot configuration.
 */
@EnableAsync
@ComponentScan(basePackages = {
    "com.automationanywhere.cognitive.app",
    "com.automationanywhere.cognitive.auth",
    "com.automationanywhere.cognitive.path",
    "com.automationanywhere.cognitive.errors",
    "com.automationanywhere.cognitive.health",
    "com.automationanywhere.cognitive.healthcheck",
    "com.automationanywhere.cognitive.resourcelock",
    "com.automationanywhere.cognitive.id",
    "com.automationanywhere.cognitive.logging",
    "com.automationanywhere.cognitive.restclient",
    "com.automationanywhere.cognitive.gateway",
    "com.automationanywhere.cognitive.validation",
    "com.automationanywhere.cognitive.common.healthcheck"
})
@EnableAutoConfiguration
public class SpringBootConfiguration extends WebMvcConfigurerAdapter {

  private final FileConfigurationProvider configurationProvider;
  private final ThreadPoolTaskExecutor taskExecutor;
  private final ThreadPoolTaskExecutor microserviceExecutor;
  private final ApplicationConfiguration applicationConfiguration;
  private final AsyncRestTemplate asyncRestTemplate;
  private final ClasspathSchemaValidator jsonValidator;
  private final CompositeIdTransformer compositeIdTransformer;

  private final CustomTomcatEmbeddedServletContainerFactory
      customTomcatEmbeddedServletContainerFactory;

  private AuthorizationInterceptor authorizationInterceptor;
  private ResourceLockInterceptor resourceLockInterceptor;

  @Autowired
  public SpringBootConfiguration(
      final ConfigurableApplicationContext applicationContext,
      final ObjectMapper objectMapper,
      final UUIDIdTransformer uuidIdTransformer,
      final StringIdTransformer stringIdTransformer,
      final LongIdTransformer longIdTransformer,
      final ExceptionHandlerFactory ehf,
      @Value("${aa.cognitive.gateway.config-file-path:}") final String configFilePath
  ) {
    applicationContext.registerShutdownHook();

    compositeIdTransformer = new CompositeIdTransformer(
        uuidIdTransformer, longIdTransformer, stringIdTransformer
    );

    objectMapper.registerModule(new EnumsModule());
    objectMapper.registerModule(new IdModule(
        compositeIdTransformer, uuidIdTransformer, longIdTransformer, stringIdTransformer
    ));
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

    jsonValidator = new ClasspathSchemaValidator(
        "/schemas", ".json", objectMapper, ehf
    );

    FileConfigurationProviderFactory configurationProviderFactory
        = new FileConfigurationProviderFactory();

    configurationProvider = configurationProviderFactory.create(
        new YAMLMapper(),
        configFilePath.trim().isEmpty()
            ? FileConfigurationProviderFactory.DEFAULT_CONFIGURATION_FILE_PATH
            : configFilePath,
        null
    );

    applicationConfiguration = new ApplicationConfiguration(configurationProvider);

    ThreadPoolFactory threadPoolFactory = new ThreadPoolFactory();

    taskExecutor = threadPoolFactory.create(
        (int) applicationConfiguration.asyncExecutorCorePoolSize(),
        (int) applicationConfiguration.asyncExecutorMaxPoolSize(),
        (int) applicationConfiguration.asyncExecutorKeepAliveTimeout(),
        (int) applicationConfiguration.asyncExecutorShutdownTimeout(),
        "GATEWAY_ASYNC_EXEC_"
    );

    microserviceExecutor = threadPoolFactory.create(
        (int) applicationConfiguration.asyncExecutorCorePoolSize(),
        (int) applicationConfiguration.asyncExecutorMaxPoolSize(),
        (int) applicationConfiguration.asyncExecutorKeepAliveTimeout(),
        (int) applicationConfiguration.asyncExecutorShutdownTimeout(),
        "MICROSERVICE_ASYNC_EXEC_"
    );

    configurationProvider.listen(taskExecutor);
    configurationProvider.refresh();

    ApacheAsyncHTTPClientRequestFactoryCreator asyncHTTPClientRequestFactoryCreator
        = new ApacheAsyncHTTPClientRequestFactoryCreator(
        new CustomThreadFactory("GATEWAY_REST_CLIENT_"), applicationConfiguration
    );

    AsyncClientHttpRequestFactory asyncClientHttpRequestFactory
        = asyncHTTPClientRequestFactoryCreator.create();

    List<HttpMessageConverter<?>> converters = new ArrayList<>();
    RestTemplate restTemplate = new StandardHeaderRestTemplate(new RestTemplate());
    for (final HttpMessageConverter<?> converter : restTemplate.getMessageConverters()) {
      if (converter instanceof MappingJackson2HttpMessageConverter) {
        boolean prettyPrint = objectMapper.getSerializationConfig()
            .hasSerializationFeatures(SerializationFeature.INDENT_OUTPUT.getMask());

        JacksonHttpMessageConverter newConverter = new JacksonHttpMessageConverter(objectMapper);
        newConverter.setPrettyPrint(prettyPrint);
        newConverter.setObjectMapper(objectMapper);

        converters.add(newConverter);
      } else {
        converters.add(converter);
      }
    }
    restTemplate.setMessageConverters(converters);

    asyncRestTemplate = new AsyncRestTemplate(asyncClientHttpRequestFactory, restTemplate);
    asyncRestTemplate.setInterceptors(
        Collections.singletonList(new LoggingRestTemplateInterceptor())
    );

    customTomcatEmbeddedServletContainerFactory
        = new CustomTomcatEmbeddedServletContainerFactory();
  }

  @Override
  public void addInterceptors(final InterceptorRegistry registry) {
    registry.addInterceptor(authorizationInterceptor);
    registry.addInterceptor(resourceLockInterceptor);
  }

  @Override
  public void configureAsyncSupport(final AsyncSupportConfigurer configurer) {
    configurer.setDefaultTimeout(applicationConfiguration.asyncHTTPServletRequestTimeout());
  }

  @Override
  public void addViewControllers(final ViewControllerRegistry registry) {
    registry.addViewController("/").setViewName("redirect:/info/api/index.html");
    registry.addViewController("/info/api").setViewName("redirect:/info/api/index.html");
  }

  @Bean(destroyMethod = "shutdown")
  public ConfigurationProvider configurationProvider() {
    return configurationProvider;
  }

  @Bean(destroyMethod = "shutdown")
  @Qualifier("taskExecutor")
  public Executor taskExecutor() {
    return taskExecutor;
  }

  @Bean(destroyMethod = "shutdown")
  @Qualifier("microserviceExecutor")
  public Executor microserviceExecutor() {
    return microserviceExecutor;
  }

  @Bean
  public ApplicationConfiguration applicationConfiguration() {
    return applicationConfiguration;
  }

  @Bean
  public AsyncRestTemplate asyncRestTemplate() {
    return asyncRestTemplate;
  }

  @Bean
  public EmbeddedServletContainerFactory servletContainer() {
    return customTomcatEmbeddedServletContainerFactory;
  }

  @Bean
  public ClasspathSchemaValidator jsonValidator() {
    return jsonValidator;
  }

  @Bean
  public CompositeIdTransformer compositeIdTransformer() {
    return compositeIdTransformer;
  }

  @Bean
  public AuthorizationInterceptor authorizationInterceptor(
      final AuthorizationSupport authorizationSupport
  ) {
    authorizationInterceptor = new AuthorizationInterceptor(authorizationSupport);
    return authorizationInterceptor;
  }

  @Bean
  public ResourceLockInterceptor resourceLockInterceptor(
      final ResourceLocker resourceLocker,
      final ResourceFactory resourceFactory,
      final PathManager pathManager,
      final ObjectMapper objectMapper
  ) {
    resourceLockInterceptor = new ResourceLockInterceptor(
        pathManager,
        resourceFactory,
        resourceLocker,
        objectMapper
    );
    return resourceLockInterceptor;
  }
}
