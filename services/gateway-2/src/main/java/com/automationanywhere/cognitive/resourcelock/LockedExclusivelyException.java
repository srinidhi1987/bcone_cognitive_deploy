package com.automationanywhere.cognitive.resourcelock;

public class LockedExclusivelyException extends LockedException {

  private static final long serialVersionUID = -3438975199070251946L;

  public LockedExclusivelyException(final String activityName) {
    super(activityName);
  }
}
