package com.automationanywhere.cognitive.app.auth;

import com.automationanywhere.cognitive.auth.AuthorizationSupport;
import com.automationanywhere.cognitive.auth.ForbiddenException;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

  private static final String BEARER_PREFIX = "Bearer ";

  private final AuthorizationSupport authorizationSupport;

  public AuthorizationInterceptor(
      final AuthorizationSupport authorizationSupport
  ) {
    this.authorizationSupport = authorizationSupport;
  }

  @Override
  public boolean preHandle(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final Object handler
  ) throws Exception {
//    if (request.getDispatcherType() == DispatcherType.REQUEST) {
//      HandlerMethod handlerMethod = (HandlerMethod) handler;
//      String clientKey = request.getHeader(CognitivePlatformHeaders.CLIENT_KEY.headerName);
//      String clientToken = request.getHeader(CognitivePlatformHeaders.CLIENT_TOKEN.headerName);
//      String jwt = getToken(request.getHeader(HttpHeaders.AUTHORIZATION));
//
//      authorizationSupport.authorize(handlerMethod.getMethod(), clientKey, clientToken, jwt);
//    }
    return true;
  }

  private String getToken(final String authorizationHeader) {
    String jwt = null;
    if (authorizationHeader != null
        && !authorizationHeader.trim().isEmpty()
    ) {
        if (!authorizationHeader.startsWith(BEARER_PREFIX)) {
          throw new ForbiddenException("Invalid authorization header: " + authorizationHeader);
        }
        jwt = authorizationHeader.substring(BEARER_PREFIX.length());
    }
    return jwt;
  }
}
