package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import java.util.Objects;

public class JsonPatchActionRemoveData extends JsonPatchActionData {

  public final String op;

  public JsonPatchActionRemoveData(
      final String path
  ) {
    super(path);
    this.op = "remove";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionRemoveData)) {
      return false;
    }
    JsonPatchActionRemoveData that = (JsonPatchActionRemoveData) o;
    return Objects.equals(path, that.path);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path);
  }
}
