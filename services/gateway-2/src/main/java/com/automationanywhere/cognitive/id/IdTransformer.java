package com.automationanywhere.cognitive.id;

import com.fasterxml.jackson.core.TreeNode;

/**
 * Converts objects to ids and vice versa.
 */
public interface IdTransformer {

  /**
   * Checks if an id type is supported by the transformer.
   *
   * @param type - type to test.
   * @return true if the type is supported.
   * @throws NullPointerException if type is null.
   */
  boolean supports(Class<? extends Id> type);

  /**
   * Creates an id of a mandatory type from a JSON tree node.
   *
   * @param node - JSON to convert.
   * @return new instance of the id type.
   * @throws UnsupportedOperationException - if an implementation doesn't support this type of
   * conversion.
   */
  Id fromNode(TreeNode node) throws UnsupportedOperationException;

  /**
   * Converts an id of a mandatory type to a JSON tree node.
   *
   * @param id - id to convert
   * @return JSON tree node
   * @throws UnsupportedOperationException - if an implementation doesn't support this type of
   * conversion.
   */
  TreeNode toNode(Id id) throws UnsupportedOperationException;

  /**
   * Creates an id of a mandatory type from a string representation of the id.
   *
   * @param id - string representation of the id.
   * @return new instance of the id type.
   * @throws UnsupportedOperationException - if an implementation doesn't support this type of
   * conversion.
   */
  Id fromString(String id) throws UnsupportedOperationException;

  /**
   * Converts an id of a mandatory type to string representation of the id.
   *
   * @param id - id to convert
   * @return string representation of the id.
   * @throws UnsupportedOperationException - if an implementation doesn't support this type of
   * conversion.
   */
  String toString(Id id) throws UnsupportedOperationException;
}
