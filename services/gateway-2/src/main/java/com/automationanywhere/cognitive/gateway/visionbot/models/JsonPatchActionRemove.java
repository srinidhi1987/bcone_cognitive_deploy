package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Objects;

public class JsonPatchActionRemove extends JsonPatchAction {

  public JsonPatchActionRemove(
      final String path
  ) {
    super(path);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionRemove)) {
      return false;
    }
    JsonPatchActionRemove that = (JsonPatchActionRemove) o;
    return Objects.equals(path, that.path);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path);
  }
}
