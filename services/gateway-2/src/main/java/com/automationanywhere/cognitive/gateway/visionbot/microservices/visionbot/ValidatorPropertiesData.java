package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class ValidatorPropertiesData {
  @JsonProperty("FilePath")
  public final String filePath;

  @JsonProperty("StaticListItems")
  public final List<String> staticListItems;

  public ValidatorPropertiesData(
      @JsonProperty("FilePath") final String filePath,
      @JsonProperty("StaticListItems") final List<String> staticListItems
  ) {
    this.filePath = filePath;
    this.staticListItems = staticListItems;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ValidatorPropertiesData that = (ValidatorPropertiesData) o;

      result = Objects.equals(filePath, that.filePath)
          && Objects.equals(staticListItems, that.staticListItems);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(filePath)
        ^ Objects.hashCode(staticListItems);
  }
}
