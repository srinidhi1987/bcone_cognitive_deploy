package com.automationanywhere.cognitive.gateway.alias.adapters.rest;

import com.automationanywhere.cognitive.gateway.alias.DomainLanguagesInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Immutable data transfer object implementing the Domain Languages Info
 * interface that is able to convert to and from JSON.
 */
public class DomainLanguagesDto implements DomainLanguagesInfo {

  /** The name of the JSON field holding the Domain name */
  private static final String JSON_NAME = "name";

  /**
   * The name of the JSON field holding the languages supported for the Domain
   */
  private static final String JSON_LANGUAGES = "languages";

  /** The domain identifier */
  private final String name;

  /** The identifiers for languages supported for the Domain */
  private final Set<String> languages;

  /**
   * Construct a Domain from its elements.
   *
   * @param name the domain identifier
   * @param languages the identifiers for languages supported for the Domain
   */
  @JsonCreator
  public DomainLanguagesDto(
          @JsonProperty(JSON_NAME) @Nonnull final String name,
          @JsonProperty(JSON_LANGUAGES) @Nonnull final Set<String> languages) {
    this.name = checkNotNull(name, "name must not be null");
    this.languages = Collections.unmodifiableSet(
            checkNotNull(languages, "languages must not be null"));
  }

  /**
   * Construct from domain languages information object.
   *
   * @param domainLanguagesInfo the domain languages information object used to
   *     construct this object
   */
  DomainLanguagesDto(final DomainLanguagesInfo domainLanguagesInfo) {
    this(domainLanguagesInfo.getName(), domainLanguagesInfo.getLanguages());
  }

  @Override
  public String toString() {
    return "DomainLanguagesDto{"
            + JSON_NAME + "='" + getName() + "', "
            + JSON_LANGUAGES + "=" + getLanguages()
            + '}';
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof DomainLanguagesInfo)) {
      return false;
    }
    final DomainLanguagesInfo that = (DomainLanguagesInfo) other;
    return Objects.equals(this.name, that.getName())
            && Objects.equals(this.languages, that.getLanguages());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName(), getLanguages());
  }

  @Override
  @Nonnull
  @JsonProperty(JSON_NAME)
  public String getName() {
    return name;
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_LANGUAGES)
  public Set<String> getLanguages() {
    return languages;
  }

}
