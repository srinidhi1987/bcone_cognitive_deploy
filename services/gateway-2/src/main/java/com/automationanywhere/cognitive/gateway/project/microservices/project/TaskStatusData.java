package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class TaskStatusData {

  public final Id taskId;
  public final String taskType;
  public final String status;
  public final String description;

  public TaskStatusData(
      @JsonProperty("taskId") final Id taskId,
      @JsonProperty("taskType") final String taskType,
      @JsonProperty("status") final String status,
      @JsonProperty("description") final String description
  ) {
    this.taskId = taskId;
    this.taskType = taskType;
    this.status = status;
    this.description = description;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      TaskStatusData taskStatus = (TaskStatusData) o;

      result = Objects.equals(taskId, taskStatus.taskId)
          && Objects.equals(taskType, taskStatus.taskType)
          && Objects.equals(status, taskStatus.status)
          && Objects.equals(description, taskStatus.description);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(taskId)
        ^ Objects.hashCode(taskType)
        ^ Objects.hashCode(status)
        ^ Objects.hashCode(description);
  }
}

