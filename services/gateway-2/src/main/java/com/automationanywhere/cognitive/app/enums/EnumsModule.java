package com.automationanywhere.cognitive.app.enums;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.IOException;

public class EnumsModule extends SimpleModule {

  private static final String NAME = EnumsModule.class.getSimpleName();
  private static final VersionUtil VERSION_UTIL = new VersionUtil() {
  };
  private static final long serialVersionUID = -5184762567757736402L;

  public EnumsModule() {
    super(NAME, VERSION_UTIL.version());

    setDeserializerModifier(new BeanDeserializerModifier() {
      @Override
      public JsonDeserializer<Enum> modifyEnumDeserializer(
          final DeserializationConfig config,
          final JavaType type,
          final BeanDescription beanDesc,
          final JsonDeserializer<?> deserializer
      ) {
        return new JsonDeserializer<Enum>() {

          @Override
          @SuppressWarnings("unchecked")
          public Enum deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            Class<? extends Enum> rawClass = (Class<? extends Enum>) type.getRawClass();
            return Enum.valueOf(rawClass, jp.getValueAsString().toUpperCase());
          }
        };
      }
    });
  }
}
