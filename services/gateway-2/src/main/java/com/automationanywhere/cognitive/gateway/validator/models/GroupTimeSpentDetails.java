package com.automationanywhere.cognitive.gateway.validator.models;

public class GroupTimeSpentDetails {

  private final String categoryId;
  private final double timeSpend;

  public GroupTimeSpentDetails(
      String categoryId,
      double timeSpend
  ) {
    this.categoryId = categoryId;
    this.timeSpend = timeSpend;
  }

  /**
   * Returns the categoryId.
   *
   * @return the value of categoryId
   */
  public String getCategoryId() {
    return categoryId;
  }

  /**
   * Returns the timeSpend.
   *
   * @return the value of timeSpend
   */
  public double getTimeSpend() {
    return timeSpend;
  }
}
