package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FieldValueDefinitionData {
    @JsonProperty("Id") public final Id id;
    @JsonProperty("FieldId") public final Id fieldId;
    @JsonProperty("Label") public final String label;
    @JsonProperty("StartsWith") public final String startsWith;
    @JsonProperty("EndsWith") public final String endsWith;
    @JsonProperty("FormatExpression") public final String formatExpression;
    @JsonProperty("DisplayValue") public final String displayValue;
    @JsonProperty("IsMultiline") public final Boolean isMultiline;
    @JsonProperty("Bounds") public final String bounds;
    @JsonProperty("ValueBounds") public final String valueBounds;
    @JsonProperty("FieldDirection") public final Integer fieldDirection;
    @JsonProperty("MergeRatio") public final Integer mergeRatio;
    @JsonProperty("SimilarityFactor") public final Double similarityFactor;
    @JsonProperty("Type") public final Integer type;
    @JsonProperty("XDistant") public final Integer xDistant;
    @JsonProperty("IsValueBoundAuto") public final Boolean isValueBoundAuto;
    @JsonProperty("ValueType") public final Integer valueType;
    @JsonProperty("IsDollarCurrency") public final String dollarCurrencyUsed;

  public FieldValueDefinitionData(
    @JsonProperty("Id") final Id id,
    @JsonProperty("FieldId") final Id fieldId,
    @JsonProperty("Label") final String label,
    @JsonProperty("StartsWith") final String startsWith,
    @JsonProperty("EndsWith") final String endsWith,
    @JsonProperty("FormatExpression") final String formatExpression,
    @JsonProperty("DisplayValue") final String displayValue,
    @JsonProperty("IsMultiline") final Boolean isMultiline,
    @JsonProperty("Bounds") final String bounds,
    @JsonProperty("ValueBounds") final String valueBounds,
    @JsonProperty("FieldDirection") final Integer fieldDirection,
    @JsonProperty("MergeRatio") final Integer mergeRatio,
    @JsonProperty("SimilarityFactor") final Double similarityFactor,
    @JsonProperty("Type") final Integer type,
    @JsonProperty("XDistant") final Integer xDistant,
    @JsonProperty("IsValueBoundAuto") final Boolean isValueBoundAuto,
    @JsonProperty("ValueType") final Integer valueType,
    @JsonProperty("IsDollarCurrency") final String dollarCurrencyUsed
  ) {
    this.id = id;
    this.fieldId = fieldId;
    this.label = label;
    this.startsWith = startsWith;
    this.endsWith = endsWith;
    this.formatExpression = formatExpression;
    this.displayValue = displayValue;
    this.isMultiline = isMultiline;
    this.bounds = bounds;
    this.valueBounds = valueBounds;
    this.fieldDirection = fieldDirection;
    this.mergeRatio = mergeRatio;
    this.similarityFactor = similarityFactor;
    this.type = type;
    this.xDistant = xDistant;
    this.isValueBoundAuto = isValueBoundAuto;
    this.valueType = valueType;
    this.dollarCurrencyUsed = dollarCurrencyUsed;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      FieldValueDefinitionData that = (FieldValueDefinitionData) o;

      result = Objects.equals(id, that.id)
          && Objects.equals(fieldId, that.fieldId)
          && Objects.equals(label, that.label)
          && Objects.equals(startsWith, that.startsWith)
          && Objects.equals(endsWith, that.endsWith)
          && Objects.equals(formatExpression, that.formatExpression)
          && Objects.equals(displayValue, that.displayValue)
          && Objects.equals(isMultiline, that.isMultiline)
          && Objects.equals(bounds, that.bounds)
          && Objects.equals(valueBounds, that.valueBounds)
          && Objects.equals(fieldDirection, that.fieldDirection)
          && Objects.equals(mergeRatio, that.mergeRatio)
          && Objects.equals(similarityFactor, that.similarityFactor)
          && Objects.equals(type, that.type)
          && Objects.equals(xDistant, that.xDistant)
          && Objects.equals(isValueBoundAuto, that.isValueBoundAuto)
          && Objects.equals(valueType, that.valueType)
          && Objects.equals(dollarCurrencyUsed, that.dollarCurrencyUsed);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(fieldId)
        ^ Objects.hashCode(label)
        ^ Objects.hashCode(startsWith)
        ^ Objects.hashCode(endsWith)
        ^ Objects.hashCode(formatExpression)
        ^ Objects.hashCode(displayValue)
        ^ Objects.hashCode(isMultiline)
        ^ Objects.hashCode(bounds)
        ^ Objects.hashCode(valueBounds)
        ^ Objects.hashCode(fieldDirection)
        ^ Objects.hashCode(mergeRatio)
        ^ Objects.hashCode(similarityFactor)
        ^ Objects.hashCode(type)
        ^ Objects.hashCode(xDistant)
        ^ Objects.hashCode(isValueBoundAuto)
        ^ Objects.hashCode(valueType)
        ^ Objects.hashCode(dollarCurrencyUsed);
  }
}

