package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class ValidationDetailsData {

  @JsonProperty("ValidatorData")
  public final List<ValidatorDetailsData> validatorData;

  public ValidationDetailsData(
      @JsonProperty("ValidatorData") final List<ValidatorDetailsData> validatorData
  ) {
    this.validatorData = validatorData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ValidationDetailsData validationDetails = (ValidationDetailsData) o;

      result = Objects.equals(validatorData, validationDetails.validatorData);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(validatorData);
  }
}

