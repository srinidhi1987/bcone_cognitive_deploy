package com.automationanywhere.cognitive.gateway.visionbot.models;

public abstract class JsonPatchAction {

  public final String path;

  public JsonPatchAction(
      final String path
  ) {
    this.path = path;
  }
}
