package com.automationanywhere.cognitive.gateway.report.microservice.report.model;

import java.util.List;

public class ClassificationData {

  private List<ClassificationFieldData> fieldRepresentation;

  /**
   * Returns the fieldRepresentation.
   *
   * @return the value of fieldRepresentation
   */
  public List<ClassificationFieldData> getFieldRepresentation() {
    return fieldRepresentation;
  }

  /**
   * Sets the fieldRepresentation.
   *
   * @param fieldRepresentation the fieldRepresentation to be set
   */
  public void setFieldRepresentation(List<ClassificationFieldData> fieldRepresentation) {
    this.fieldRepresentation = fieldRepresentation;
  }
}
