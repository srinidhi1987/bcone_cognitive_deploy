package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileCountData {

  public final Integer totalCount;
  public final Integer totalSTPCount;
  public final Integer unprocessedCount;

  public FileCountData(
      @JsonProperty("totalCount") final Integer totalCount,
      @JsonProperty("totalSTPCount") final Integer totalSTPCount,
      @JsonProperty("unprocessedCount") final Integer unprocessedCount
  ) {
    this.totalCount = totalCount;
    this.totalSTPCount = totalSTPCount;
    this.unprocessedCount = unprocessedCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileCountData fileCount = (FileCountData) o;

      result = Objects.equals(totalCount, fileCount.totalCount)
        && Objects.equals(totalSTPCount, fileCount.totalSTPCount)
        && Objects.equals(unprocessedCount, fileCount.unprocessedCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalCount)
      ^ Objects.hashCode(totalSTPCount)
      ^ Objects.hashCode(unprocessedCount);
  }
}

