package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.Category;
import com.automationanywhere.cognitive.gateway.file.models.CategoryDetail;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.EnvironmentCountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FieldDetail;
import com.automationanywhere.cognitive.gateway.file.models.File;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileCount;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFieldDetailFromClasssifier;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.Summary;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.gateway.file.models.VisionBot;
import java.util.Collections;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class FileDtoMapper {

  public CategoriesDto mapCategories(final Categories model) {
    return model == null ? null : new CategoriesDto(
      model.offset, model.limit, model.sort,
      model.categories == null ? null
          : model.categories.stream().map(this::mapCategory).collect(Collectors.toList())
    );
  }

  private CategoryDto mapCategory(final Category model) {
    return model == null ? null : new CategoryDto(
        model.id, model.name, mapCategoryDetail(model.categoryDetail),
        mapVisionBot(model.visionBot), model.fileCount,
        model.files == null ? null
            : model.files.stream().map(this::mapFile).collect(Collectors.toList()),
        mapFileCount(model.productionFileDetails), mapFileCount(model.stagingFileDetails)
    );
  }

  private FileCountDto mapFileCount(final FileCount model) {
    return model == null ? null : new FileCountDto(
        model.totalCount, model.totalSTPCount, model.unprocessedCount
    );
  }

  private FileDto mapFile(final File model) {
    return model == null ? null : new FileDto(
        model.id, model.name, model.location, model.format, model.processed
    );
  }

  private VisionBotDto mapVisionBot(final VisionBot model) {
    return model == null ? null : new VisionBotDto(
        model.id, model.name, model.currentState, model.running, model.lockedUserId,
        model.lastModifiedByUser, model.lastModifiedTimestamp
    );
  }

  private CategoryDetailDto mapCategoryDetail(final CategoryDetail model) {
    return model == null ? null : new CategoryDetailDto(
      model.id, model.noOfDocuments, model.priority, model.index,
      model.allFieldDetails == null ? null
          : model.allFieldDetails.stream().map(this::mapFieldDetail).collect(Collectors.toList())
    );
  }

  private FieldDetailDto mapFieldDetail(final FieldDetail model) {
    return model == null ? null : new FieldDetailDto(
        model.fieldId, model.foundInTotalDocs
    );
  }

  public FileDetailsDto mapFileDetails(final FileDetails model) {
    return model == null ? null : new FileDetailsDto(
        model.fileId, model.projectId, model.fileName, model.fileLocation, model.fileSize,
        model.fileHeight, model.fileWidth, model.format, model.processed, model.classificationId,
        model.uploadRequestId, model.layoutId, model.isProduction
    );
  }

  public CountStatisticsDto mapCountStatistics(final CountStatistics model) {
    return model == null ? null : new CountStatisticsDto(
        model.projectId, model.categoryId, model.fileCount,
        mapEnvironmentCountStatistics(model.stagingFileCount),
        mapEnvironmentCountStatistics(model.productionFileCount)
    );
  }

  private EnvironmentCountStatisticsDto mapEnvironmentCountStatistics(
      final EnvironmentCountStatistics model
  ) {
    return model == null ? null : new EnvironmentCountStatisticsDto(
      model.unprocessedCount, model.totalCount
    );
  }

  public FilePatchDetail mapFilePatchDetails(final FilePatchDetailDto dto) {
    return dto == null ? null : new FilePatchDetail(dto.op, dto.path, dto.value);
  }

  public FileBlobsDto mapFileBlobs(final FileBlobs model) {
    return model == null ? null : new FileBlobsDto(
      model.fileId, model.fileBlobs
    );
  }

  public ClassificationAnalysisReportDto mapClassificationAnalysisReport(
      final ClassificationAnalysisReport model
  ) {
    return model == null ? null : new ClassificationAnalysisReportDto(
      model.projectId, model.totalDocuments, model.allCategoryDetails == null ? null
        : model.allCategoryDetails.stream()
            .map(this::mapCategoryDetail).collect(Collectors.toList())
    );
  }

  public UploadResponseDto mapUploadResponse(final UploadResponse m) {
    return m == null ? null : new UploadResponseDto(m.fileUploadJobId);
  }

  public FileStatusDto mapFileStatus(final FileStatus m) {
    return m == null ? null : new FileStatusDto(
        m.projectId, m.currentProcessingObject, m.totalFileCount, m.totalProcessedCount,
        m.estimatedTimeRemainingInMins, m.complete
    );
  }

  public LearningInstanceSummaryDto mapLearningInstanceSummary(
      final LearningInstanceSummary m
  ) {
    return m == null ? null : new LearningInstanceSummaryDto(
      mapSummary(m.stagingFileSummary),
      mapSummary(m.productionFileSummary)
    );
  }

  private SummaryDto mapSummary(final Summary m) {
    return m == null ? null : new SummaryDto(
      m.totalGroups, m.documentsUnclassified, m.totalFiles
    );
  }

  public RawFileReportFromClassifier mapRawFileReportFromClassifierData(
      final RawFileReportFromClassifierDto dto
  ) {
    return dto == null ? null : new RawFileReportFromClassifier(
        dto.documentId, dto.fieldDetails == null ? Collections.emptyList()
            : dto.fieldDetails.stream().map(this::mapRawFieldDetailFromClasssifier)
                .collect(Collectors.toList())
    );
  }

  private RawFieldDetailFromClasssifier mapRawFieldDetailFromClasssifier(
      final RawFieldDetailFromClassifierDto model
  ) {
    return model == null ? null : new RawFieldDetailFromClasssifier(
        model.fieldId, model.aliasName, model.found
    );
  }
}
