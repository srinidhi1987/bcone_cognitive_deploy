package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.Objects;

public class CategoryDetail {

  public final Id id;
  public final Integer noOfDocuments;
  public final Integer priority;
  public final Integer index;
  public final List<FieldDetail> allFieldDetails;

  public CategoryDetail(
      final Id id,
      final Integer noOfDocuments,
      final Integer priority,
      final Integer index,
      final List<FieldDetail> allFieldDetails
  ) {
    this.id = id;
    this.noOfDocuments = noOfDocuments;
    this.priority = priority;
    this.index = index;
    this.allFieldDetails = allFieldDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CategoryDetail categoryDetail = (CategoryDetail) o;

      result = Objects.equals(id, categoryDetail.id)
        && Objects.equals(noOfDocuments, categoryDetail.noOfDocuments)
        && Objects.equals(priority, categoryDetail.priority)
        && Objects.equals(index, categoryDetail.index)
        && Objects.equals(allFieldDetails, categoryDetail.allFieldDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(noOfDocuments)
      ^ Objects.hashCode(priority)
      ^ Objects.hashCode(index)
      ^ Objects.hashCode(allFieldDetails);
  }
}

