package com.automationanywhere.cognitive.validation;

public interface Schemas {

  String V1 = "application/vnd.cognitive.v1+json";
  String V2 = "application/vnd.cognitive.v2+json";

  String PATCH = "application/json-patch+json";
  String PATCH_V1 = "application/vnd.cognitive.v1+json-patch+json";

  String STANDARD_RESPONSE = "StandardResponse";
  String UPLOAD_RESPONSE = "UploadResponse";

  String AUTHENTICATION = "Authentication";
  String CREDENTIALS = "Credentials";

  String PROJECT_DETAILS = "ProjectDetails";
  String NEW_PROJECT_DETAILS = "NewProjectDetails";
  String PROJECT_DETAIL_CHANGE_SUMMARY = "ProjectDetailChangeSummary";
  String PROJECT_DETAIL_CHANGES = "ProjectDetailChanges";
  String PROJECT_DETAILS_LIST = "ProjectDetailsList";
  String PROJECT_PATCH_DETAILS = "ProjectPatchDetails";
  String PROJECT_PATCH_RESPONSE = "ProjectPatchResponse";
  String ARCHIVE_DETAILS_LIST = "ArchiveDetailList";
  String PROJECT_IMPORT_REQUEST = "ProjectImportRequest";
  String PROJECT_EXPORT_REQUEST = "ProjectExportRequest";
  String TASK_STATUS = "TaskStatus";
  String TASK_STATUS_LIST = "TaskStatusList";
  String TASK_LIST = "TaskList";
  String PROJECT_CATEGORIES = "ProjectCategories";

  String FILE_DETAILS_LIST = "FileDetailsList";
  String FILE_BLOB_LIST = "FileBlobList";
  String COUNT_STATISTICS_LIST = "CountStatisticsList";
  String FILE_PATCH_DETAILS = "FilePatchDetails";
  String PROJECT_REPORT = "ProjectReport";
  String FILE_STATUS = "FileStatus";
  String LEARNING_INSTANCE_SUMMARY = "LearningInstanceSummary";
  String FILE_REPORT_FROM_CLASSIFIER = "FileReportFromClassifier";

  // Alias Service
  String LANGUAGES_FOR_DOMAINS = "LanguagesForDomains";
  String SHAREABLE_DOMAIN = "ShareableDomain";
  String PROJECT_TYPE_LIST_ALL = "ProjectTypeListAll";
  String PROJECT_TYPE = "ProjectType";
  String PROJECT_FIELD_LIST_ALL = "ProjectFieldListAll";
  String PROJECT_FIELD = "ProjectField";
  String PROJECT_ALIASES = "ProjectAliases";
  String LANGUAGE_LIST_ALL = "LanguageListAll";
  String LANGUAGE = "Language";

  String VISION_BOT = "VisionBot";
  String VISION_BOT_CHANGES = "VisionBotChanges";
  String VISION_BOT_DETAILS_LIST = "VisionBotDetailsList";
  String VISION_BOT_DATA_LIST = "VisionBotDataList";
  String FIELD_VALUE_DEFINITION = "FieldValueDefinition";
  String EXPORT_DETAILS = "ExportDetails";
  String SUMMARY = "Summary";
  String TEST_SET = "TestSet";
  String TEST_SET_RESULT = "TestSetResult";
  String VISION_BOT_STATE = "VisionBotState";
  String MULTIPLE_VISION_BOT_STATE = "MultipleVisionBotState";
  String VALIDATION_DETAILS = "ValidationDetails";
  String VALIDATION_DETAILS_RESULT = "ValidationDetailsResult";
  String VISION_BOT_LIST = "VisionBotList";
  String PROCESSED_DOCUMENT_DETAILS = "ProcessedDocumentDetails";
  String PROCESSED_DOCUMENT_DETAILS_RESULT = "ProcessedDocumentDetailsResult";
  String JSON_PATCH_DOC = "JsonPatchDocument";

  String REVIEWED_FILE_COUNT_DETAILS_LIST = "ReviewedFileCountDetailsList";
  String UNLOCK_RESULT = "UnlockResult";
  String INVALID_FILE = "InvalidFile";
  String INVALID_FILE_RESULT = "InvalidFileResult";
  String INVALID_FILE_TYPE_LIST = "InvalidFileTypeList";
  String PRODUCTION_FILE_SUMMARY = "ProductionFileSummary";

  String VALUE_LIST = "ValueList";

  String REGISTRATION = "Registration";
  String HANDSHAKE = "Handshake";
  String ACKNOWLEDGEMENT = "Acknowledgement";
  String CONFIGURATION = "Configuration";

}
