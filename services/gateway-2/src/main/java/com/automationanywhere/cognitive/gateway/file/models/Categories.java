package com.automationanywhere.cognitive.gateway.file.models;

import java.util.List;
import java.util.Objects;

public class Categories {

  public final Integer offset;
  public final Integer limit;
  public final String sort;
  public final List<Category> categories;

  public Categories(
      final Integer offset,
      final Integer limit,
      final String sort,
      final List<Category> categories
  ) {
    this.offset = offset;
    this.limit = limit;
    this.sort = sort;
    this.categories = categories;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      Categories categories = (Categories) o;

      result = Objects.equals(offset, categories.offset)
        && Objects.equals(limit, categories.limit)
        && Objects.equals(sort, categories.sort)
        && Objects.equals(this.categories, categories.categories);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(offset)
      ^ Objects.hashCode(limit)
      ^ Objects.hashCode(sort)
      ^ Objects.hashCode(categories);
  }
}

