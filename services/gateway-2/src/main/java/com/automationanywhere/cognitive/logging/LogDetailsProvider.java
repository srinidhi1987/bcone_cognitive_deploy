package com.automationanywhere.cognitive.logging;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Provides information to be used for request tracking.
 */
@Component
public class LogDetailsProvider {

  private static final Logger LOGGER = LogManager.getLogger(LogDetailsProvider.class);

  private final ApplicationConfiguration ApplicationConfiguration;
  private String localHost;

  @Autowired
  public LogDetailsProvider(
      final ApplicationConfiguration ApplicationConfiguration
  ) {
    this.ApplicationConfiguration = ApplicationConfiguration;
  }

  public String getCID() {
    return UUID.randomUUID().toString();
  }

  public String getUserId() {
    return "<not implemented>";
  }

  public String getApplicationName() {
    return ApplicationConfiguration.applicationName();
  }

  public String getApplicationVersion() {
    return ApplicationConfiguration.applicationVersion();
  }

  public String getClientIP(final HttpServletRequest req) {
    return req.getRemoteAddr();
  }

  public int getClientPort(final HttpServletRequest req) {
    return req.getRemotePort();
  }

  public String getServerIP(final HttpServletRequest req) {
    return req.getLocalAddr();
  }

  public int getServerPort(final HttpServletRequest req) {
    return req.getLocalPort();
  }

  public String getRequestMethod(final HttpServletRequest req) {
    return req.getMethod();
  }

  public String getRequestPath(final HttpServletRequest req) {
    String path = "";

    for (final String p : Arrays.asList(req.getContextPath(), req.getServletPath(), req.getPathInfo())) {
      path = concat(path, p);
    }

    return path;
  }

  public String getRequestDetails(final HttpServletRequest req) {
    return MessageFormat.format(
        "{0}:{1,number,#}->{2}:{3,number,#} {4} {5}",
        getClientIP(req), getClientPort(req),
        getServerIP(req), getServerPort(req),
        getRequestMethod(req), getRequestPath(req)
    );
  }

  public String getHostName() {
    String hostName = null;
    try {
      hostName = getLocalHost();
    } catch (final UnknownHostException ex) {
      LOGGER.error("Failed to get the host ip address", ex);

      hostName = "<unknown>";
    }
    return hostName;
  }

  public void addTrackingInfo(
      final HttpServletRequest req,
      final Map<String, String> ctx,
      final LogContextKeys key,
      final CognitivePlatformHeaders header,
      final Supplier<String> provider
  ) {
    String value = null;
    if (header != null) {
      value = req.getHeader(header.headerName);
    }
    if (value == null) {
      value = "";
    }
    value = value.trim();

    if (value.isEmpty()) {
      if (provider == null) {
        value = "<unspecified>";
      } else {
        value = provider.get();
      }
    }

    ctx.put(key.key, value);
  }

  String getLocalHost() throws UnknownHostException {
    if (localHost == null) {
      // We do not expect the host name change while the app is running
      // Let's cache it. Getting the host name may be time consuming.
      localHost = InetAddress.getLocalHost().getCanonicalHostName();
    }
    return localHost;
  }

  private String concat(final String path, final String segment) {
    String result = path;
    if (segment != null && !segment.trim().isEmpty()) {
      String pp = segment.trim();
      if (result.isEmpty()) {
        result = pp;
      } else {
        if (result.endsWith("/")) {
          if (pp.startsWith("/")) {
            result = result.substring(0, result.length() - 1) + pp;
          } else {
            result = result + pp;
          }
        } else {
          if (pp.startsWith("/")) {
            result = result + pp;
          } else {
            result = result + "/" + pp;
          }
        }
      }
    }
    return result;
  }
}
