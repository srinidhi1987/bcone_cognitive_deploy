package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import static com.automationanywhere.cognitive.validation.Schemas.COUNT_STATISTICS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.FILE_BLOB_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.FILE_DETAILS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.FILE_PATCH_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.FILE_REPORT_FROM_CLASSIFIER;
import static com.automationanywhere.cognitive.validation.Schemas.FILE_STATUS;
import static com.automationanywhere.cognitive.validation.Schemas.LEARNING_INSTANCE_SUMMARY;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_CATEGORIES;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_REPORT;
import static com.automationanywhere.cognitive.validation.Schemas.STANDARD_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.UPLOAD_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.V1;

import com.automationanywhere.cognitive.dataresource.DataContainer;
import com.automationanywhere.cognitive.gateway.file.FileService;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@RestController
@RequestMapping("/")
public class FileController {

  private final FileDtoMapper mapper;
  private final FileService fileService;

  @Autowired
  public FileController(
      final FileDtoMapper mapper,
      final FileService fileService
  ) {
    this.mapper = mapper;
    this.fileService = fileService;
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/categories",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PROJECT_CATEGORIES
  )
  public CompletableFuture<StandardResponse<CategoriesDto>> getProjectCategories(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @RequestParam(name = "offset", required = false) final Integer offset,
      @RequestParam(name = "limit", required = false) final Integer limit,
      @RequestParam(name = "sort", required = false) final String sort
  ) {
    return fileService.getProjectCategories(
        organizationId,
        projectId,
        offset, limit, sort
    ).thenApply(categories -> new StandardResponse<>(
        mapper.mapCategories(categories)
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/categories/{id}/files",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = FILE_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<FileDetailsDto>>> getProjectCategoryFiles(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id classificationId
  ) {
    return fileService.getProjectCategoryFiles(
        organizationId,
        projectId,
        classificationId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/files/counts",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = COUNT_STATISTICS_LIST
  )
  public CompletableFuture<StandardResponse<List<CountStatisticsDto>>> getFileCount(
      @PathVariable("orgid") final Id organizationId
  ) {
    return fileService.getFileCount(
        organizationId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapCountStatistics).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files/counts",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = COUNT_STATISTICS_LIST
  )
  public CompletableFuture<StandardResponse<List<CountStatisticsDto>>> getProjectFileCount(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return fileService.getProjectFileCount(
        organizationId, projectId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapCountStatistics).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = FILE_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<FileDetailsDto>>> getProjectFiles(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return fileService.getProjectFiles(
        organizationId, projectId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/categories/{id}/files/staging",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = FILE_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<FileDetailsDto>>> getProjectStagingFiles(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId
  ) {
    return fileService.getProjectStagingFiles(
        organizationId, projectId, categoryId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files/{fileid}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = FILE_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<FileDetailsDto>>> getProjectFile(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("fileid") final Id fileId
  ) {
    return fileService.getProjectFile(
        organizationId, projectId, fileId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapFileDetails).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files/{fileid}",
      method = RequestMethod.PATCH,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = FILE_PATCH_DETAILS,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Void>> patchProjectFile(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("fileid") final Id fileId,
      @RequestBody final List<FilePatchDetailDto> filePatchDetails
  ) {
    return fileService.patchProjectFile(
        organizationId, projectId, fileId,
        filePatchDetails.stream().map(mapper::mapFilePatchDetails).collect(Collectors.toList())
    ).thenApply(d -> new StandardResponse<>(null));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/categories/{id}/files/{fileid}",
      method = RequestMethod.DELETE,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Void>> deleteProjectFile(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("fileid") final Id fileId
  ) {
    return fileService.deleteProjectFile(
        organizationId, projectId, categoryId, fileId
    ).thenApply(d -> new StandardResponse<>(null));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files/upload/{isproduction}",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = UPLOAD_RESPONSE
  )
  public CompletableFuture<StandardResponse<UploadResponseDto>> uploadProjectFiles(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("isproduction") final String isProduction,
      @RequestParam(name = "requestid", required = false) final Id requestId,
      final MultipartHttpServletRequest request
  ) throws Exception {
    List<DataContainer> data = extractData(request);

    return fileService.uploadFiles(
        organizationId, projectId, isProduction, requestId, data
    ).thenApply(m -> new StandardResponse<>(mapper.mapUploadResponse(m)));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = UPLOAD_RESPONSE
  )
  public CompletableFuture<StandardResponse<UploadResponseDto>> uploadProjectStagingFiles(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @RequestParam(name = "requestid", required = false) final Id requestId,
      final MultipartHttpServletRequest request
  ) throws Exception {
    List<DataContainer> data = extractData(request);
    return fileService.uploadStagingFiles(
        organizationId, projectId, requestId, data
    ).thenApply(m -> new StandardResponse<>(mapper.mapUploadResponse(m)));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/fileblob/{fileid}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = FILE_BLOB_LIST
  )
  public CompletableFuture<StandardResponse<List<FileBlobsDto>>> getFileBlobs(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("fileid") final Id fileId
  ) {
    return fileService.getFileBlobs(
        organizationId, projectId, fileId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapFileBlobs).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/report",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PROJECT_REPORT
  )
  public CompletableFuture<StandardResponse<ClassificationAnalysisReportDto>> getProjectReport(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return fileService.getProjectReport(
        organizationId, projectId
    ).thenApply(m -> new StandardResponse<>(
        mapper.mapClassificationAnalysisReport(m)
    ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/filestatus",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = FILE_STATUS
  )
  public CompletableFuture<StandardResponse<FileStatusDto>> getProjectFileStatus(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @RequestParam(name="requestid", required = false) final Id requestId
  ) {
    return fileService.getProjectFileStatus(
        organizationId, projectId, requestId
    ).thenApply(m -> new StandardResponse<>(
        mapper.mapFileStatus(m)
    ));
  }

  @RequestMapping(
      path = "/segmenteddocument/{fileid}",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> createSegmentedDocument(
      @PathVariable("fileid") final Id fileId,
      @RequestBody final String fileContent
  ) {
    return fileService.createSegmentedDocument(fileId, fileContent)
        .thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/segmenteddocument/{fileid}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> getSegmentedDocument(
      @PathVariable("fileid") final Id fileId
  ) {
    return fileService.getSegmentedDocument(fileId)
        .thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/classificationsummary",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = LEARNING_INSTANCE_SUMMARY
  )
  public CompletableFuture<StandardResponse<LearningInstanceSummaryDto>>
      getProjectClassificationSummary(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return fileService.getProjectClassificationSummary(organizationId, projectId)
        .thenApply(m -> new StandardResponse<>(mapper.mapLearningInstanceSummary(m)));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files/{fileid}/categories/{layoutid}/{id}",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = FILE_REPORT_FROM_CLASSIFIER
  )
  public CompletableFuture<StandardResponse<Void>> updateProjectClassificationById(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("fileid") final Id fileId,
      @PathVariable("layoutid") final Id layoutId,
      @PathVariable("id") final Id id,
      @RequestBody final RawFileReportFromClassifierDto data
  ) {
    return fileService.updateProjectClassificationById(
        organizationId, projectId, fileId, layoutId, id,
        mapper.mapRawFileReportFromClassifierData(data)
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/files/{fileid}/categories/{layoutid}",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<StandardResponse<Void>> updateProjectClassification(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("fileid") final Id fileId,
      @PathVariable("layoutid") final Id layoutId
  ) {
    return fileService.updateProjectClassification(
        organizationId, projectId, fileId, layoutId
    ).thenApply(StandardResponse::new);
  }

  private List<DataContainer> extractData(final MultipartHttpServletRequest request)
      throws IOException {
    List<DataContainer> data = new ArrayList<>();

    Iterator<String> it = request.getFileNames();
    while (it.hasNext()) {
      for (final MultipartFile file : request.getFiles(it.next())) {
        data.add(new DataContainer(
            file.getInputStream(),
            file.getContentType(),
            file.getOriginalFilename(),
            file.getSize()
        ));
      }
    }
    return data;
  }
}
