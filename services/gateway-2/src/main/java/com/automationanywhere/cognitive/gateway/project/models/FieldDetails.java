package com.automationanywhere.cognitive.gateway.project.models;

import java.util.List;
import java.util.Objects;

public class FieldDetails {

  public final List<String> standard;
  public final List<CustomFieldDetail> custom;

  public FieldDetails(
      final List<String> standard,
      final List<CustomFieldDetail> custom
  ) {
    this.standard = standard;
    this.custom = custom;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FieldDetails fieldDetails = (FieldDetails) o;

      result = Objects.equals(standard, fieldDetails.standard)
          && Objects.equals(custom, fieldDetails.custom);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(standard)
        ^ Objects.hashCode(custom);
  }
}

