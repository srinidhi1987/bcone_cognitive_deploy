package com.automationanywhere.cognitive.gateway.alias.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ProjectTypeDto {

  public final Id id;
  public final String name;
  public final String description;
  public final List<ProjectFieldDto> fields;
  public final List<LanguageDto> languages;

  public ProjectTypeDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("description") final String description,
      @JsonProperty("fields") final List<ProjectFieldDto> fields,
      @JsonProperty("languages") final List<LanguageDto> languages
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.fields = fields;
    this.languages = languages;
  }
}
