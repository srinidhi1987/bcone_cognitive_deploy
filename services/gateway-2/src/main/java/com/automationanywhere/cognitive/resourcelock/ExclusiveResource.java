package com.automationanywhere.cognitive.resourcelock;

import com.automationanywhere.cognitive.id.Id;

public class ExclusiveResource extends Resource {
  public final Id orgId;

  public ExclusiveResource(final String activityName, final Id orgId, final String path) {
    super(activityName, path);

    if (orgId == null) {
      throw new IllegalArgumentException();
    }

    this.orgId = orgId;
  }
}
