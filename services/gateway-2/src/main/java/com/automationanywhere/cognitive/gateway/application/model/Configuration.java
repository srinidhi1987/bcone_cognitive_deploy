package com.automationanywhere.cognitive.gateway.application.model;

public class Configuration {

  public final String crUrl;
  public final String routingName;

  public Configuration(final String crUrl, final String routingName) {
    this.crUrl = crUrl;
    this.routingName = routingName;
  }
}
