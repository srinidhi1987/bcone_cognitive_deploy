package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchAction;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.StagingFilesSummary;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorProperties;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorReviewDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotCount;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotRunDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.mappers.BaseMapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class VisionBotDataMapper extends BaseMapper {

  public VisionBot mapVisionBot(final VisionBotData data) {
    return data == null ? null : new VisionBot(data.id, data.visionBotData, data.validationData);
  }

  public VisionBotData mapVisionBot(final VisionBot model) {
    return model == null ? null : new VisionBotData(
        model.id, model.visionBotData, model.validationData
    );
  }

  public VisionBotDetails mapVisionBotDetails(final VisionBotDetailsData data) {
    return data == null ? null : new VisionBotDetails(
        data.id, data.name, data.organizationId, data.projectId, data.projectName, data.categoryId,
        data.categoryName, data.environment, data.status, data.running, data.lockedUserId,
        mapVisionBotRunDetails(data.botRunDetails),
        mapVisionBotRunDetails(data.productionBotRunDetails),
        mapVisionBotRunDetails(data.stagingBotRunDetails)
    );
  }

  private VisionBotRunDetails mapVisionBotRunDetails(final VisionBotRunDetailsData data) {
    return data == null ? null : new VisionBotRunDetails(
        data.id, data.startTime, data.endTime, data.runningMode, data.totalFiles,
        data.processedDocumentCount, data.failedDocumentCount, data.passedDocumentCount,
        data.documentProcessingAccuracy, data.fieldAccuracy,
        mapValidatorReviewDetails(data.validatorReviewDetails)
    );
  }

  private ValidatorReviewDetails mapValidatorReviewDetails(final ValidatorReviewDetailsData data) {
    return data == null ? null : new ValidatorReviewDetails(
        data.totalFilesReviewed, data.totalFilesMarkedInvalid,
        data.totalFailedFiles, data.averageReviewTime
    );
  }

  public FieldValueDefinitionData mapFieldValueDefinition(final FieldValueDefinition model) {
    return model == null ? null : new FieldValueDefinitionData(
        model.id, model.fieldId, model.label, model.startsWith, model.endsWith,
        model.formatExpression, model.displayValue, model.isMultiline, model.bounds,
        model.valueBounds, model.fieldDirection, model.mergeRatio, model.similarityFactor,
        model.type, model.xDistant, model.isValueBoundAuto, model.valueType, model.dollarCurrencyUsed
    );
  }

  public ValidationDetailsData mapValidationDetails(final ValidationDetails model) {
    return model == null ? null : new ValidationDetailsData(
        model.validatorData == null
            ? Collections.emptyList()
            : model.validatorData.stream().map(
                this::mapValidatorDetails).collect(Collectors.toList()
            )
    );
  }

  private ValidatorDetailsData mapValidatorDetails(final ValidatorDetails model) {
    return model == null ? null : new ValidatorDetailsData(
        model.id, mapValidatorProperties(model.validatorProperties),
        model.typeOfData, model.serializedData
    );
  }

  private ValidatorPropertiesData mapValidatorProperties(
      final ValidatorProperties model
  ) {
    return model == null ? null : new ValidatorPropertiesData(
        model.filePath, model.staticListItems == null ? Collections.emptyList()
        : new ArrayList<>(model.staticListItems)
    );
  }

  public ValidationDetails mapValidationDetails(final ValidationDetailsData data) {
    return data == null ? null : new ValidationDetails(
        data.validatorData == null
            ? Collections.emptyList()
            : data.validatorData.stream().map(
                this::mapValidatorDetails).collect(Collectors.toList()
            )
    );
  }

  private ValidatorDetails mapValidatorDetails(final ValidatorDetailsData data) {
    return data == null ? null : new ValidatorDetails(
        data.id, mapValidatorProperties(data.validatorProperties),
        data.typeOfData, data.serializedData
    );
  }

  private ValidatorProperties mapValidatorProperties(
      final ValidatorPropertiesData data
  ) {
    return data == null ? null : new ValidatorProperties(
        data.filePath, data.staticListItems == null ? Collections.emptyList()
        : new ArrayList<>(data.staticListItems)
    );
  }

  public Summary mapSummary(final SummaryData data) {
    return data == null ? null : new Summary(
        mapStagingFilesSummary(data.stagingSummary),
        mapVisionBotCount(data.botDetails)
    );
  }

  private VisionBotCount mapVisionBotCount(final VisionBotCountData data) {
    return data == null ? null : new VisionBotCount(
        data.numberOfBotsCreated, data.numberOfStagingBots, data.numberOfProductionBots
    );
  }

  private StagingFilesSummary mapStagingFilesSummary(final StagingFilesSummaryData data) {
    return data == null ? null : new StagingFilesSummary(
        data.totalFilesCount, data.totalPassedFileCount, data.totalFailedFileCount,
        data.totalTestedFileCount, data.accuracy
    );
  }

  public TestSet mapTestSet(final TestSetData data) {
    return data == null ? null : new TestSet(
        data.id, data.name, data.docItems
    );
  }

  public TestSetData mapTestSet(final TestSet model) {
    return model == null ? null : new TestSetData(
        model.id, model.name, model.docItems
    );
  }

  public VisionBotStateData mapVisionBotState(final VisionBotState model) {
    return model == null ? null : new VisionBotStateData(model.state);
  }

  public VisionBotStateAndIdsData mapVisionBotElements(final VisionBotElements model) {
    return model == null ? null : new VisionBotStateAndIdsData(model.state, model.visionBotIds);
  }

  public VisionBotResponse mapVisionBotResponse(final VisionBotResponseData visionBotResponse) {
    return visionBotResponse == null ? null
        : new VisionBotResponse(visionBotResponse.success, visionBotResponse.data,
            visionBotResponse.error);
  }

  public ProcessedDocumentDetails mapProcessedDocumentDetails(
      final ProcessedDocumentDetailsData data
  ) {
    return data == null ? null : new ProcessedDocumentDetails(
        data.id, data.docId, data.processingStatus,
        data.totalFieldCount, data.failedFieldCount, data.passedFieldCount
    );
  }

  public ProcessedDocumentDetailsData mapProcessedDocumentDetails(
      final ProcessedDocumentDetails model
  ) {
    return model == null ? null : new ProcessedDocumentDetailsData(
        model.id, model.docId, model.processingStatus,
        model.totalFieldCount, model.failedFieldCount, model.passedFieldCount
    );
  }

  public List<JsonPatchActionData> mapJsonPatchDocument(JsonPatchDocument doc) {
    return doc.actions.stream().map(this::mapJsonPatchAction).collect(Collectors.toList());
  }

  private JsonPatchActionData mapJsonPatchAction(JsonPatchAction action) {
    JsonPatchActionData data = null;
    if (action instanceof JsonPatchActionAdd) {
      JsonPatchActionAdd actionAdd = (JsonPatchActionAdd) action;
      data = new JsonPatchActionAddData(actionAdd.path, actionAdd.value);

    } else if (action instanceof JsonPatchActionReplace) {
      JsonPatchActionReplace actionReplace = (JsonPatchActionReplace) action;
      data = new JsonPatchActionReplaceData(actionReplace.path, actionReplace.value);

    } else if (action instanceof JsonPatchActionRemove) {
      JsonPatchActionRemove actionRemove = (JsonPatchActionRemove) action;
      data = new JsonPatchActionRemoveData(actionRemove.path);

    } else if (action instanceof JsonPatchActionTest) {
      JsonPatchActionTest actionTest = (JsonPatchActionTest) action;
      data = new JsonPatchActionTestData(actionTest.path, actionTest.value);

    } else if (action instanceof JsonPatchActionCopy) {
      JsonPatchActionCopy actionCopy = (JsonPatchActionCopy) action;
      data = new JsonPatchActionCopyData(actionCopy.path, actionCopy.from);

    } else if (action instanceof JsonPatchActionMove) {
      JsonPatchActionMove actionMove = (JsonPatchActionMove) action;
      data = new JsonPatchActionMoveData(actionMove.path, actionMove.from);
    }
    return data;
  }
}
