package com.automationanywhere.cognitive.gateway.auth.microservices.auth;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.Audit;
import com.automationanywhere.cognitive.gateway.auth.models.Authentication;
import com.automationanywhere.cognitive.gateway.auth.models.Authorization;
import com.automationanywhere.cognitive.gateway.auth.models.Credentials;
import com.automationanywhere.cognitive.gateway.auth.models.InvalidToken;
import com.automationanywhere.cognitive.gateway.auth.models.User;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AuthMicroservice {

  private final RestClient restClient;
  private final ApplicationConfiguration cfg;
  private final Audit audit;

  @Autowired
  public AuthMicroservice(
      final RestClient restClient,
      final ApplicationConfiguration cfg,
      final Audit audit
  ) {
    this.restClient = restClient;
    this.cfg = cfg;
    this.audit = audit;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Authentication> authenticate(final Credentials credentials) {
    return restClient.send(
        RequestDetails.Builder.post(cfg.authURL())
            .request(cfg.authenticate())
            .content(credentials, Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<AuthenticationData>() {
        }
    ).thenApply(data -> new Authentication(
        new User(data.user.id, data.user.name, data.user.roles), data.token)
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> authorize(final Authorization authorization) {
    audit.audit(authorization);

    return restClient.send(
        RequestDetails.Builder.post(cfg.authURL())
            .request(cfg.authorize())
            .content(authorization, Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    ).thenApply(d -> null); // Return null instead of what the REST client returns.
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> invalidateSession(final InvalidToken invalidToken) {
    audit.audit(invalidToken);

    return restClient.send(
        RequestDetails.Builder.get(cfg.authURL())
            .request(cfg.invalidateSession())
            .setHeader(HttpHeaders.AUTHORIZATION, invalidToken.token)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    ).thenApply(d -> null); // Return null instead of what the REST client returns.
  }
}
