package com.automationanywhere.cognitive.gateway.report.model;

public class FieldValidation {

  private String fieldId;
  private long percentValidated;

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Sets the fieldId.
   *
   * @param fieldId the fieldId to be set
   */
  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  /**
   * Returns the percentage of field validated.
   *
   * @return the value of percentValidated
   */
  public long getPercentValidated() {
    return percentValidated;
  }

  /**
   * Sets the percentage of field validated.
   *
   * @param percentValidated the percentValidated to be set
   */
  public void setPercentValidated(long percentValidated) {
    this.percentValidated = percentValidated;
  }
}
