package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProjectImportRequestData {
  public final ArchiveDetailsData archive;
  public final Id userId;
  public final String option;

  public ProjectImportRequestData(
      @JsonProperty("archive") final ArchiveDetailsData archive,
      @JsonProperty("userId") final Id userId,
      @JsonProperty("option") final String option
  ) {
    this.archive = archive;
    this.userId = userId;
    this.option = option;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ProjectImportRequestData that = (ProjectImportRequestData) o;

      result = Objects.equals(archive, that.archive)
          && Objects.equals(userId, that.userId)
          && Objects.equals(option, that.option);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archive)
        ^ Objects.hashCode(userId)
        ^ Objects.hashCode(option);
  }
}
