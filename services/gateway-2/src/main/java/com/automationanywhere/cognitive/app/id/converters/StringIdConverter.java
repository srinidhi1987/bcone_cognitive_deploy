package com.automationanywhere.cognitive.app.id.converters;

import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StringIdConverter extends BaseIdConverter<StringId> {

  @Autowired
  public StringIdConverter(final StringIdTransformer idTransformer) {
    super(idTransformer);
  }
}
