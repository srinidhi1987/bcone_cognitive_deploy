package com.automationanywhere.cognitive.gateway.validator.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class ReviewedFileCountDetails {

  public final Id projectId;
  public final long processedCount;
  public final long failedFileCount;
  public final long reviewFileCount;
  public final long invalidFileCount;

  public ReviewedFileCountDetails(
      final Id projectId,
      final long processedCount,
      final long failedFileCount,
      final long reviewFileCount,
      final long invalidFileCount
  ) {
    this.projectId = projectId;
    this.processedCount = processedCount;
    this.failedFileCount = failedFileCount;
    this.reviewFileCount = reviewFileCount;
    this.invalidFileCount = invalidFileCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ReviewedFileCountDetails that = (ReviewedFileCountDetails) o;

      result = Objects.equals(processedCount, that.processedCount)
          && Objects.equals(failedFileCount, that.failedFileCount)
          && Objects.equals(reviewFileCount, that.reviewFileCount)
          && Objects.equals(invalidFileCount, that.invalidFileCount)
          && Objects.equals(projectId, that.projectId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
        ^ Objects.hashCode(processedCount)
        ^ Objects.hashCode(failedFileCount)
        ^ Objects.hashCode(reviewFileCount)
        ^ Objects.hashCode(invalidFileCount);
  }
}