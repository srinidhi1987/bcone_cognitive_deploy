package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class ClassificationAnalysisReportDto {

  public final Id projectId;
  public final String totalDocuments;
  @JsonProperty("allCategoryDetail")
  public final List<CategoryDetailDto> allCategoryDetails;

  public ClassificationAnalysisReportDto(
      @JsonProperty("projectId") final Id projectId,
      @JsonProperty("totalDocuments") final String totalDocuments,
      @JsonProperty("allCategoryDetail") final List<CategoryDetailDto> allCategoryDetails
  ) {
    this.projectId = projectId;
    this.totalDocuments = totalDocuments;
    this.allCategoryDetails = allCategoryDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ClassificationAnalysisReportDto classificationAnalysisReport
          = (ClassificationAnalysisReportDto) o;

      result = Objects.equals(projectId, classificationAnalysisReport.projectId)
          && Objects.equals(totalDocuments, classificationAnalysisReport.totalDocuments)
          && Objects.equals(allCategoryDetails, classificationAnalysisReport.allCategoryDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
        ^ Objects.hashCode(totalDocuments)
        ^ Objects.hashCode(allCategoryDetails);
  }
}

