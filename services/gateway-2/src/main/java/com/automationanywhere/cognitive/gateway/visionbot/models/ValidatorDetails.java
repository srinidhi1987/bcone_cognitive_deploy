package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class ValidatorDetails {
  public final Id id;
  public final ValidatorProperties validatorProperties;
  public final String typeOfData;
  public final String serializedData;

  public ValidatorDetails(
      final Id id,
      final ValidatorProperties validatorProperties,
      final String typeOfData,
      final String serializedData
  ) {
    this.id = id;
    this.validatorProperties = validatorProperties;
    this.typeOfData = typeOfData;
    this.serializedData = serializedData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ValidatorDetails that = (ValidatorDetails) o;

      result = Objects.equals(id, that.id)
          && Objects.equals(validatorProperties, that.validatorProperties)
          && Objects.equals(typeOfData, that.typeOfData)
          && Objects.equals(serializedData, that.serializedData);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(validatorProperties)
        ^ Objects.hashCode(typeOfData)
        ^ Objects.hashCode(serializedData);
  }
}
