package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class EnvironmentCountStatisticsData {

  public final Integer unprocessedCount;
  public final Integer totalCount;

  public EnvironmentCountStatisticsData(
      @JsonProperty("unprocessedCount") final Integer unprocessedCount,
      @JsonProperty("totalCount") final Integer totalCount
  ) {
    this.unprocessedCount = unprocessedCount;
    this.totalCount = totalCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      EnvironmentCountStatisticsData environmentCountStatistics
          = (EnvironmentCountStatisticsData) o;

      result = Objects.equals(unprocessedCount, environmentCountStatistics.unprocessedCount)
          && Objects.equals(totalCount, environmentCountStatistics.totalCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(unprocessedCount)
        ^ Objects.hashCode(totalCount);
  }
}

