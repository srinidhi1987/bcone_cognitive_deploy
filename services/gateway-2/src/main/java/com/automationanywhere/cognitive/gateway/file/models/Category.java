package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.Objects;

public class Category {

  public final Id id;
  public final String name;
  public final CategoryDetail categoryDetail;
  public final VisionBot visionBot;
  public final Integer fileCount;
  public final List<File> files;
  public final FileCount productionFileDetails;
  public final FileCount stagingFileDetails;

  public Category(
      final Id id,
      final String name,
      final CategoryDetail categoryDetail,
      final VisionBot visionBot,
      final Integer fileCount,
      final List<File> files,
      final FileCount productionFileDetails,
      final FileCount stagingFileDetails
  ) {
    this.id = id;
    this.name = name;
    this.categoryDetail = categoryDetail;
    this.visionBot = visionBot;
    this.fileCount = fileCount;
    this.files = files;
    this.productionFileDetails = productionFileDetails;
    this.stagingFileDetails = stagingFileDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      Category category = (Category) o;

      result = Objects.equals(id, category.id)
        && Objects.equals(name, category.name)
        && Objects.equals(categoryDetail, category.categoryDetail)
        && Objects.equals(visionBot, category.visionBot)
        && Objects.equals(fileCount, category.fileCount)
        && Objects.equals(files, category.files)
        && Objects.equals(productionFileDetails, category.productionFileDetails)
        && Objects.equals(stagingFileDetails, category.stagingFileDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(name)
      ^ Objects.hashCode(categoryDetail)
      ^ Objects.hashCode(visionBot)
      ^ Objects.hashCode(fileCount)
      ^ Objects.hashCode(files)
      ^ Objects.hashCode(productionFileDetails)
      ^ Objects.hashCode(stagingFileDetails);
  }
}

