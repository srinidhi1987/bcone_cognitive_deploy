package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class CategoryDto {

  public final Id id;
  public final String name;
  public final VisionBotDto visionBot;
  public final Integer fileCount;
  public final List<FileDto> files;
  public final FileCountDto productionFileDetails;
  public final FileCountDto stagingFileDetails;

  public CategoryDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("visionBot") final VisionBotDto visionBot,
      @JsonProperty("fileCount") final Integer fileCount,
      @JsonProperty("files") final List<FileDto> files,
      @JsonProperty("productionFileDetails") final FileCountDto productionFileDetails,
      @JsonProperty("stagingFileDetails") final FileCountDto stagingFileDetails
  ) {
    this.id = id;
    this.name = name;
    this.visionBot = visionBot;
    this.fileCount = fileCount;
    this.files = files;
    this.productionFileDetails = productionFileDetails;
    this.stagingFileDetails = stagingFileDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CategoryDto category = (CategoryDto) o;

      result = Objects.equals(id, category.id)
          && Objects.equals(name, category.name)
          && Objects.equals(visionBot, category.visionBot)
          && Objects.equals(fileCount, category.fileCount)
          && Objects.equals(files, category.files)
          && Objects.equals(productionFileDetails, category.productionFileDetails)
          && Objects.equals(stagingFileDetails, category.stagingFileDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(visionBot)
        ^ Objects.hashCode(fileCount)
        ^ Objects.hashCode(files)
        ^ Objects.hashCode(productionFileDetails)
        ^ Objects.hashCode(stagingFileDetails);
  }
}

