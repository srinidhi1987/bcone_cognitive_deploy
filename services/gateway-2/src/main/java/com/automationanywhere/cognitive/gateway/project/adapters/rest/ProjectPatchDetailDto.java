package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProjectPatchDetailDto {

  public final String op;
  public final String path;
  public final Object value;
  public final Integer versionId;

  public ProjectPatchDetailDto(
      @JsonProperty("op") final String op,
      @JsonProperty("path") final String path,
      @JsonProperty("value") final Object value,
      @JsonProperty("versionId") final Integer versionId
  ) {
    this.op = op;
    this.path = path;
    this.value = value;
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectPatchDetailDto projectPatchDetail
          = (ProjectPatchDetailDto) o;

      result = Objects.equals(op, projectPatchDetail.op)
          && Objects.equals(path, projectPatchDetail.path)
          && Objects.equals(value, projectPatchDetail.value)
          && Objects.equals(versionId, projectPatchDetail.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(op)
        ^ Objects.hashCode(path)
        ^ Objects.hashCode(value)
        ^ Objects.hashCode(versionId);
  }
}

