package com.automationanywhere.cognitive.gateway.application.microservices.application;

public class AcknowledgementData {

  public final String appId;

  public AcknowledgementData(final String appId) {
    this.appId = appId;
  }
}
