package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class SummaryData {

  public final StagingFilesSummaryData stagingSummary;
  public final VisionBotCountData botDetails;

  public SummaryData(
      @JsonProperty("stagingSummary") final StagingFilesSummaryData stagingSummary,
      @JsonProperty("botDetails") final VisionBotCountData botDetails
  ) {
    this.stagingSummary = stagingSummary;
    this.botDetails = botDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      SummaryData summary = (SummaryData) o;

      result = Objects.equals(stagingSummary, summary.stagingSummary)
        && Objects.equals(botDetails, summary.botDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(stagingSummary)
      ^ Objects.hashCode(botDetails);
  }
}

