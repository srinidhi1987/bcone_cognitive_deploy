package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class CustomFieldDetail {

  public final Id id;
  public final String name;
  public final String type;

  public CustomFieldDetail(
      final Id id,
      final String name,
      final String type
  ) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CustomFieldDetail customFieldDetail = (CustomFieldDetail) o;

      result = Objects.equals(id, customFieldDetail.id)
          && Objects.equals(name, customFieldDetail.name)
          && Objects.equals(type, customFieldDetail.type);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(type);
  }
}

