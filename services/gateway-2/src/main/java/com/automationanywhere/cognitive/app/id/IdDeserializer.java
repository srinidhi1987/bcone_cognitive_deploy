package com.automationanywhere.cognitive.app.id;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.NullNode;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Deserializes UUID represented as strings into UUIDId objects.
 */
public class IdDeserializer<T extends Id> extends StdDeserializer<T> {

  private static final long serialVersionUID = -5196530935625682791L;
  private final IdTransformer idTransformer;

  public IdDeserializer(final IdTransformer idTransformer, final Class<T> type) {
    super(type);

    this.idTransformer = idTransformer;
  }

  @Override
  public T deserialize(
      final JsonParser p,
      final DeserializationContext ctx
  ) throws IOException {
    T result = null;

    TreeNode treeNode = p.getCodec().readTree(p);
    if (!(treeNode instanceof NullNode)) {
      try {
        @SuppressWarnings("unchecked")
        T id = (T) idTransformer.fromNode(treeNode);
        result = id;
      } catch (final UnsupportedOperationException ex) {
        StringWriter w = new StringWriter();
        p.getCodec().getFactory().createGenerator(w).writeTree(treeNode);
        throw new JsonParseException(p, w + " is not supported by any known Id implementation");
      }
    }

    return result;
  }
}
