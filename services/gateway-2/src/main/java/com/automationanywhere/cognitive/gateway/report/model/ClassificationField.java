package com.automationanywhere.cognitive.gateway.report.model;

public class ClassificationField {

  private String name;
  private long representationPercent;

  /**
   * Returns the name of the field.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of the field.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Returns the percentage of fields found across documents.
   *
   * @return the value of representationPercent
   */
  public long getRepresentationPercent() {
    return representationPercent;
  }

  /**
   * Sets the percentage of fields found across documents.
   *
   * @param representationPercent the representationPercent to be set
   */
  public void setRepresentationPercent(long representationPercent) {
    this.representationPercent = representationPercent;
  }
}
