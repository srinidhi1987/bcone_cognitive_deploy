package com.automationanywhere.cognitive.app.id;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.NullNode;
import java.io.IOException;

public class IdSerializer<T extends Id> extends JsonSerializer<T> {

  private final IdTransformer idTransformer;

  public IdSerializer(final IdTransformer idTransformer) {
    this.idTransformer = idTransformer;
  }

  @Override
  public void serialize(
      final T value,
      final JsonGenerator gen,
      final SerializerProvider serializers
  ) throws IOException {
    TreeNode node = null;
    try {
      node = idTransformer.toNode(value);
    } catch (final UnsupportedOperationException ex) {
      throw new JsonGenerationException(value.getClass().getName() + " is not supported", gen);
    }
    if (node == NullNode.getInstance()) {
      gen.writeNull();
    } else {
      gen.writeTree(node);
    }
  }
}
