package com.automationanywhere.cognitive.gateway.ml.ml;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ValueData {
  public final Id fieldId;
  public final String correctedValue;
  public final String originalValue;

  public ValueData(
      @JsonProperty("fieldId") final Id fieldId,
      @JsonProperty("correctedValue") final String correctedValue,
      @JsonProperty("originalValue") final String originalValue
  ) {
    this.fieldId = fieldId;
    this.correctedValue = correctedValue;
    this.originalValue = originalValue;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ValueData value = (ValueData) o;

      result = Objects.equals(fieldId, value.fieldId)
          && Objects.equals(correctedValue, value.correctedValue)
          && Objects.equals(originalValue, value.originalValue);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fieldId)
        ^ Objects.hashCode(correctedValue)
        ^ Objects.hashCode(originalValue);
  }
}
