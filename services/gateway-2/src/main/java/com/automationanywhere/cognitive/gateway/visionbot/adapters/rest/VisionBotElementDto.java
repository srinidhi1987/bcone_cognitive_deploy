package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class VisionBotElementDto {

  public final String state;
  public final List<String> visionBotList;

  public VisionBotElementDto(
      @JsonProperty("state") final String state,
      @JsonProperty("visionBotIds") final List<String> visionBotList
  ) {
    this.state = state;
    this.visionBotList = visionBotList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VisionBotElementDto that = (VisionBotElementDto) o;
    return Objects.equals(state, that.state) &&
        Objects.equals(visionBotList, that.visionBotList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(state, visionBotList);
  }
}

