package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class SummaryDto {

  public final StagingFilesSummaryDto stagingSummary;
  public final VisionBotCountDto botDetails;

  public SummaryDto(
      @JsonProperty("stagingSummary") final StagingFilesSummaryDto stagingSummary,
      @JsonProperty("botDetails") final VisionBotCountDto botDetails
  ) {
    this.stagingSummary = stagingSummary;
    this.botDetails = botDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      SummaryDto summary = (SummaryDto) o;

      result = Objects.equals(stagingSummary, summary.stagingSummary)
        && Objects.equals(botDetails, summary.botDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(stagingSummary)
      ^ Objects.hashCode(botDetails);
  }
}

