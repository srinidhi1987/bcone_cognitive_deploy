package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Objects;

public class JsonPatchActionTestData extends JsonPatchActionData {

  public final String op;
  public final JsonNode value;

  public JsonPatchActionTestData(
      final String path,
      final JsonNode value
  ) {
    super(path);
    this.op = "test";
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionTestData)) {
      return false;
    }
    JsonPatchActionTestData that = (JsonPatchActionTestData) o;
    return Objects.equals(path, that.path) && Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path, value);
  }
}
