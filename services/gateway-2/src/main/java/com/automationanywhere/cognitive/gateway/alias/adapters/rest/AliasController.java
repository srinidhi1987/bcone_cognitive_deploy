package com.automationanywhere.cognitive.gateway.alias.adapters.rest;

import static com.automationanywhere.cognitive.validation.Schemas.LANGUAGE;
import static com.automationanywhere.cognitive.validation.Schemas.LANGUAGES_FOR_DOMAINS;
import static com.automationanywhere.cognitive.validation.Schemas.LANGUAGE_LIST_ALL;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_ALIASES;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_FIELD;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_FIELD_LIST_ALL;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_TYPE;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_TYPE_LIST_ALL;
import static com.automationanywhere.cognitive.validation.Schemas.SHAREABLE_DOMAIN;
import static com.automationanywhere.cognitive.validation.Schemas.V1;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.automationanywhere.cognitive.gateway.alias.AliasService;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AliasController {

  private final AliasService service;
  private final AliasDtoMapper mapper;

  @Autowired
  public AliasController(final AliasService service, final AliasDtoMapper mapper) {
    this.service = service;
    this.mapper = mapper;
  }

  @RequestMapping(
          path = "/domains/languages",
          method = GET,
          produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = LANGUAGES_FOR_DOMAINS)
  public CompletableFuture<List<DomainLanguagesDto>> getDomainsLanguages() {
    return service.getDomainLanguages()
            .thenApply(mapper::getDomainLanguagesDtoList);
  }

  @RequestMapping(
          path = "/domains/{domainId}/export",
          method = GET,
          produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = SHAREABLE_DOMAIN)
  public CompletableFuture<ShareableDomainDto> getShareableDomain(
          @PathVariable(name = "domainId") final String domainName
  ) {
    return service.getDomain(domainName)
            .thenApply(mapper::getShareableDomainDto);
  }

  @RequestMapping(
          path = "/domains/import",
          method = POST,
          consumes = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(input = SHAREABLE_DOMAIN)
  public CompletableFuture<Void> importShareableDomain(
          @RequestBody final ShareableDomainDto shareableDomain
  ) {
    return service.importDomain(shareableDomain);
  }

  @RequestMapping(
      path = "/projecttypes",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = PROJECT_TYPE_LIST_ALL)
  public CompletableFuture<StandardResponse<List<ProjectTypeDto>>> getAllProjectTypes(
      @RequestParam(name = "languageid", required = false) final String langId
  ) {
    return service.getAllProjectTypes(langId)
        .thenApply(projectTypes -> new StandardResponse<>(mapper.getProjectTypeDto(projectTypes)));
  }

  @RequestMapping(
      path = "/projecttypes/{projecttypeid}",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = PROJECT_TYPE)
  public CompletableFuture<StandardResponse<ProjectTypeDto>> getProjectType(
      @PathVariable("projecttypeid") final String projectTypeId,
      @RequestParam(name = "languageid", required = false) final String langId
  ) {
    return service.getProjectType(projectTypeId, langId)
        .thenApply(projectType -> new StandardResponse<>(mapper.getProjectTypeDto(projectType)));

  }

  @RequestMapping(
      path = "/projecttypes/{projecttypeid}/fields",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = PROJECT_FIELD_LIST_ALL)
  public CompletableFuture<StandardResponse<List<ProjectFieldDto>>> getAllFields(
      @PathVariable("projecttypeid") final String projectTypeId,
      @RequestParam(name = "languageid", required = false) final String langId
  ) {
    return service.getProjectFields(projectTypeId, langId)
        .thenApply(projectFields -> new StandardResponse<>(mapper.getProjectFieldDto(projectFields)));
  }

  @RequestMapping(
      path = "/fields/{fieldid}",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = PROJECT_FIELD)
  public CompletableFuture<StandardResponse<ProjectFieldDto>> getField(
      @PathVariable final String fieldId,
      @RequestParam(name = "languageid", required = false) final String langId
  ) {
    return service.getProjectField(fieldId, langId)
        .thenApply(projectField -> new StandardResponse<>(mapper.getProjectFieldDto(projectField)));
  }

  @RequestMapping(
      path = "/fields/{fieldid}/alias",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = PROJECT_ALIASES)
  public CompletableFuture<StandardResponse<List<String>>> getAliases(
      @PathVariable final String fieldId,
      @RequestParam(name = "languageid", required = false) final String langId
  ) {
    return service.getProjectAliases(fieldId, langId).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/languages",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = LANGUAGE_LIST_ALL)
  public CompletableFuture<StandardResponse<List<LanguageDto>>> getLanguages() {
    return service.getLanguages()
        .thenApply(languages -> new StandardResponse<>(mapper.getLanguages(languages)));
  }

  @RequestMapping(
      path = "/languages/{languageid}",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(output = LANGUAGE)
  public CompletableFuture<StandardResponse<LanguageDto>> getLanguage(
      @PathVariable("languageid") final String langId
  ) {
    return service.getLanguage(langId)
        .thenApply(language -> new StandardResponse<>(mapper.getLanguage(language)));
  }
}
