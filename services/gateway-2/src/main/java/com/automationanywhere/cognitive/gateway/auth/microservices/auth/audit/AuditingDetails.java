package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit;

/**
 * Details extracted from an event that an audit instance will be able to audit.
 */
public interface AuditingDetails {
}
