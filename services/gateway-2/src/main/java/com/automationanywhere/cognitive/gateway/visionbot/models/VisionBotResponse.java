package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class VisionBotResponse {
  public final Boolean success;
  public final String data;
  public String error;

  public VisionBotResponse(
      @JsonProperty("success") final Boolean success,
      @JsonProperty("data") final String data,
      @JsonProperty("errors") final String errors) {
    this.success = success;
    this.data = data;
    this.error = error;
  }

  public VisionBotResponse(final String data) {
    this.success = true;
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VisionBotResponse that = (VisionBotResponse) o;
    return Objects.equals(success, that.success) &&
        Objects.equals(data, that.data) &&
        Objects.equals(error, that.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, data, error);
  }

}
