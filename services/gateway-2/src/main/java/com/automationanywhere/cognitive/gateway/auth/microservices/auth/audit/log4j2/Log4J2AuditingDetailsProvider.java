package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.log4j2;

import com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.AuditingDetailsProvider;
import com.automationanywhere.cognitive.gateway.auth.models.JWTAuthorization;
import java.util.Collections;
import org.springframework.stereotype.Component;

@Component
public class Log4J2AuditingDetailsProvider implements AuditingDetailsProvider {

  @Override
  public Log4J2AuditingDetails extract(final Object event) {
    return event instanceof JWTAuthorization
        ? new Log4J2AuditingDetails(
          "Checking the rights to access {}",
          Collections.singletonList(() -> ((JWTAuthorization) event).action)
        ) : null;
  }
}
