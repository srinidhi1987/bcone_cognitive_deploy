package com.automationanywhere.cognitive.gateway.alias;

public class DomainCodecException extends RuntimeException {

  private static final long serialVersionUID = -3586482512454052194L;

  /**
   * Thrown when there is an error encoding or decoding a Domain.
   *
   * @param message the exception message
   */
  public DomainCodecException(final String message) {
    super(message);
  }

  /**
   * Thrown when there is an error encoding or decoding a Domain.
   *
   * @param message the exception message
   * @param cause the exception cause
   */
  public DomainCodecException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
