package com.automationanywhere.cognitive.gateway.auth.adapters.rest;

import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.USERNAME_HEADER_NAME;
import static com.automationanywhere.cognitive.validation.Schemas.AUTHENTICATION;
import static com.automationanywhere.cognitive.validation.Schemas.CREDENTIALS;
import static com.automationanywhere.cognitive.validation.Schemas.STANDARD_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.V1;

import com.automationanywhere.cognitive.common.auth.AnonymousAccess;
import com.automationanywhere.cognitive.gateway.auth.AuthService;
import com.automationanywhere.cognitive.gateway.auth.models.Credentials;
import com.automationanywhere.cognitive.gateway.auth.models.InvalidToken;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class AuthController {

  private final AuthService authService;

  @Autowired
  public AuthController(final AuthService authService) {
    this.authService = authService;
  }

  @RequestMapping(
      path = "/authentication",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = CREDENTIALS,
      output = AUTHENTICATION
  )
  @AnonymousAccess
  public CompletableFuture<StandardResponse<AuthenticationDto>> authenticate(
      @RequestBody final CredentialsDto credentials
  ) {
    return authService.authenticate(
        new Credentials(credentials.username, credentials.password)
    ).thenApply(data -> new StandardResponse<>(
        new AuthenticationDto(
            new UserDto(data.user.id, data.user.name, data.user.roles),
            data.token
        )
    ));
  }

  @RequestMapping(
      path = "/logout",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  @AnonymousAccess
  public CompletableFuture<StandardResponse<Void>> logout(
      @RequestHeader(name = USERNAME_HEADER_NAME) final String username,
      @RequestHeader(name = HttpHeaders.AUTHORIZATION) final String token
  ) {
    return authService.logout(
        new InvalidToken(token), username
    ).thenApply(
        data -> new StandardResponse<>(null)
    );
  }
}
