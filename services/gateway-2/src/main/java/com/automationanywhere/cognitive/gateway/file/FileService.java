package com.automationanywhere.cognitive.gateway.file;

import com.automationanywhere.cognitive.dataresource.DataContainer;
import com.automationanywhere.cognitive.gateway.file.microservices.file.FileMicroservice;
import com.automationanywhere.cognitive.gateway.file.models.Categories;
import com.automationanywhere.cognitive.gateway.file.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.gateway.file.models.CountStatistics;
import com.automationanywhere.cognitive.gateway.file.models.FileBlobs;
import com.automationanywhere.cognitive.gateway.file.models.FileDetails;
import com.automationanywhere.cognitive.gateway.file.models.FilePatchDetail;
import com.automationanywhere.cognitive.gateway.file.models.FileStatus;
import com.automationanywhere.cognitive.gateway.file.models.LearningInstanceSummary;
import com.automationanywhere.cognitive.gateway.file.models.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.gateway.file.models.UploadResponse;
import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class FileService {
  private final FileMicroservice fileMicroservice;

  @Autowired
  public FileService(final FileMicroservice fileMicroservice) {
    this.fileMicroservice = fileMicroservice;
  }

  @Async
  public CompletableFuture<Categories> getProjectCategories(
      final Id organizationId,
      final Id projectId,
      final Integer offset, final Integer limit, final String sort
  ) {
    return fileMicroservice.getOrganizationProjectCategories(
        organizationId, projectId,
        offset, limit, sort
    );
  }

  @Async
  public CompletableFuture<List<FileDetails>> getProjectCategoryFiles(
      final Id organizationId,
      final Id projectId,
      final Id classificationId
  ) {
    return fileMicroservice.getProjectCategoryFiles(
        organizationId, projectId, classificationId
    );
  }

  @Async
  public CompletableFuture<List<CountStatistics>> getFileCount(
      final Id organizationId
  ) {
    return fileMicroservice.getFileCount(organizationId);
  }

  @Async
  public CompletableFuture<List<CountStatistics>> getProjectFileCount(
      final Id organizationId, final Id projectId
  ) {
    return fileMicroservice.getProjectFileCount(organizationId, projectId);
  }

  @Async
  public CompletableFuture<List<FileDetails>> getProjectFiles(
      final Id organizationId, final Id projectId
  ) {
    return fileMicroservice.getProjectFiles(organizationId, projectId);
  }

  @Async
  public CompletableFuture<List<FileDetails>> getProjectStagingFiles(
      final Id organizationId, final Id projectId, final Id categoryId
  ) {
    return fileMicroservice.getProjectStagingFiles(organizationId, projectId, categoryId);
  }

  @Async
  public CompletableFuture<List<FileDetails>> getProjectFile(
      final Id organizationId, final Id projectId, final Id fileId
  ) {
    return fileMicroservice.getProjectFile(organizationId, projectId, fileId);
  }

  @Async
  public CompletableFuture<Void> patchProjectFile(
      final Id organizationId, final Id projectId, final Id fileId,
      final List<FilePatchDetail> filePatchDetails
  ) {
    return fileMicroservice.patchProjectFile(organizationId, projectId, fileId, filePatchDetails);
  }

  @Async
  public CompletableFuture<Void> deleteProjectFile(
      final Id organizationId, final Id projectId, final Id categoryId, final Id fileId
  ) {
    return fileMicroservice.deleteProjectFile(organizationId, projectId, categoryId, fileId);
  }

  @Async
  public CompletableFuture<List<FileBlobs>> getFileBlobs(
      final Id organizationId, final Id projectId, final Id fileId
  ) {
    return fileMicroservice.getFileBlobs(organizationId, projectId, fileId);
  }

  @Async
  public CompletableFuture<ClassificationAnalysisReport> getProjectReport(
      final Id organizationId, final Id projectId
  ) {
    return fileMicroservice.getProjectReport(organizationId, projectId);
  }

  @Async
  public CompletableFuture<FileStatus> getProjectFileStatus(
      final Id organizationId, final Id projectId, final Id requestId
  ) {
    return fileMicroservice.getProjectFileStatus(organizationId, projectId, requestId);
  }

  @Async
  public CompletableFuture<String> createSegmentedDocument(
      final Id fileId, final String fileContent
  ) {
    return fileMicroservice.createSegmentedDocument(fileId, fileContent);
  }

  @Async
  public CompletableFuture<String> getSegmentedDocument(final Id fileId) {
    return fileMicroservice.getSegmentedDocument(fileId);
  }

  @Async
  public CompletableFuture<UploadResponse> uploadFiles(
      final Id organizationId,
      final Id projectId,
      final String isProduction,
      final Id requestId,
      final List<DataContainer> data
  ) {
    return fileMicroservice.uploadFiles(
        organizationId, projectId, isProduction, requestId, data
    );
  }

  @Async
  public CompletableFuture<UploadResponse> uploadStagingFiles(
      final Id organizationId,
      final Id projectId,
      final Id requestId,
      final List<DataContainer> data
  ) {
    return fileMicroservice.uploadStagingFiles(
        organizationId, projectId, requestId, data
    );
  }

  @Async
  public CompletableFuture<LearningInstanceSummary> getProjectClassificationSummary(
      final Id organizationId,
      final Id projectId
  ) {
    return fileMicroservice.getProjectClassificationSummary(organizationId, projectId);
  }

  @Async
  public CompletableFuture<Void> updateProjectClassification(
      final Id organizationId, final Id projectId,
      final Id fileId, final Id layoutId
  ) {
    return fileMicroservice.updateProjectClassification(
        organizationId, projectId, fileId, layoutId
    );
  }

  @Async
  public CompletableFuture<Void> updateProjectClassificationById(
      final Id organizationId, final Id projectId,
      final Id fileId, final Id layoutId, final Id id,
      final RawFileReportFromClassifier rawFileReportFromClassifier
  ) {
    return fileMicroservice.updateProjectClassificationById(
        organizationId, projectId, fileId, layoutId, id, rawFileReportFromClassifier
    );
  }
}
