package com.automationanywhere.cognitive.gateway.report.model;

public class GroupTimeSpent {

  private String name;
  private long timeSpent;

  /**
   * Returns the category name.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the category name.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Returns the time spent to validate group in minutes.
   *
   * @return the value of timeSpent
   */
  public long getTimeSpent() {
    return timeSpent;
  }

  /**
   * Sets the time spent to validate group in minutes.
   *
   * @param timeSpent the timeSpent to be set
   */
  public void setTimeSpent(long timeSpent) {
    this.timeSpent = timeSpent;
  }
}
