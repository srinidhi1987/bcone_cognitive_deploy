package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class RawFileReportFromClassifierDto {

  public final Id documentId;
  @JsonProperty("fieldDetail")
  public final List<RawFieldDetailFromClassifierDto> fieldDetails;

  public RawFileReportFromClassifierDto(
      @JsonProperty("documentId") final Id documentId,
      @JsonProperty("fieldDetail") final List<RawFieldDetailFromClassifierDto> fieldDetails
  ) {
    this.documentId = documentId;
    this.fieldDetails = fieldDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      RawFileReportFromClassifierDto that = (RawFileReportFromClassifierDto) o;

      result = Objects.equals(documentId, that.documentId)
          && Objects.equals(fieldDetails, that.fieldDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(documentId)
        ^ Objects.hashCode(fieldDetails);
  }
}
