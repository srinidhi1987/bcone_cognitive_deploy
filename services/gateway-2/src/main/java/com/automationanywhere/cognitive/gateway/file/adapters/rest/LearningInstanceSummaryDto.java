package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class LearningInstanceSummaryDto {

  public final SummaryDto stagingFileSummary;
  public final SummaryDto productionFileSummary;

  public LearningInstanceSummaryDto(
      @JsonProperty("stagingFileSummary") final SummaryDto stagingFileSummary,
      @JsonProperty("productionFileSummary") final SummaryDto productionFileSummary
  ) {
    this.stagingFileSummary = stagingFileSummary;
    this.productionFileSummary = productionFileSummary;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      LearningInstanceSummaryDto learningInstanceSummary = (LearningInstanceSummaryDto) o;

      result = Objects.equals(stagingFileSummary, learningInstanceSummary.stagingFileSummary)
          && Objects.equals(productionFileSummary, learningInstanceSummary.productionFileSummary);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(stagingFileSummary)
        ^ Objects.hashCode(productionFileSummary);
  }
}

