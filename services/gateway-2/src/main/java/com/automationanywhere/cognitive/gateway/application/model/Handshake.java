package com.automationanywhere.cognitive.gateway.application.model;

public class Handshake {

  public final String appVersion;
  public final String publicKey;
  public final String routingName;

  public Handshake(final String appVersion, final String publicKey, final String routingName) {
    this.appVersion = appVersion;
    this.publicKey = publicKey;
    this.routingName = routingName;
  }
}
