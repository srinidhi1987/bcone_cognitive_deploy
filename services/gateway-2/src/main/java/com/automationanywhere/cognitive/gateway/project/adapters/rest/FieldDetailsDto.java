package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class FieldDetailsDto {

  public final List<String> standard;
  public final List<CustomFieldDetailDto> custom;

  public FieldDetailsDto(
      @JsonProperty("standard") final List<String> standard,
      @JsonProperty("custom") final List<CustomFieldDetailDto> custom
  ) {
    this.standard = standard;
    this.custom = custom;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FieldDetailsDto fieldDetails = (FieldDetailsDto) o;

      result = Objects.equals(standard, fieldDetails.standard)
          && Objects.equals(custom, fieldDetails.custom);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(standard)
        ^ Objects.hashCode(custom);
  }
}

