package com.automationanywhere.cognitive.resourcelock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.path.PathManager;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.method.HandlerMethod;

@Component
public class ResourceFactory {

  private static final StringId ORG_ID = new StringId("1");

  private final List<String> knownResources;

  private final ApplicationConfiguration cfg;
  private final PathManager pathManager;

  @Autowired
  public ResourceFactory(
      final ApplicationConfiguration cfg,
      final PathManager pathManager
  ) {
    this.cfg = cfg;
    this.pathManager = pathManager;
    knownResources = new ArrayList<>();
    knownResources.add(cfg.resourceLockerImportResource());
    knownResources.add(cfg.resourceLockerExportResource());
  }

  public Resource create(final String path, final HandlerMethod method) {
    String uri = pathManager.create(method.getMethod());

    return knownResources.stream()
        .filter(uri::equals)
        .findAny()
        .map(u -> (Resource) new ExclusiveResource(getActivityName(u), ORG_ID, path))
        .orElse(new Resource(path));
  }

  private String getActivityName(final String uri) {
    String result = null;
    if (uri.equals(cfg.resourceLockerImportResource())) {
      result = "import";
    } else if (uri.equals(cfg.resourceLockerExportResource())) {
      result = "export";
    } else {
      throw new IllegalStateException();
    }
    return result;
  }
}
