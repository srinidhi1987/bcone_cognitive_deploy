package com.automationanywhere.cognitive.restclient;

/**
 * Headers that front-ends or microservices may use to track requests.
 */
public enum CognitivePlatformHeaders {

  CID(CognitivePlatformHeaders.CID_HEADER_NAME),
  HOST_NAME(CognitivePlatformHeaders.HOST_NAME_HEADER_NAME),
  APPLICATION_NAME(CognitivePlatformHeaders.APP_NAME_HEADER_NAME),
  APPLICATION_VERSION(CognitivePlatformHeaders.APP_VERSION_HEADER_NAME),
  SESSION_ID(CognitivePlatformHeaders.SESSION_ID_HEADER_NAME),
  TENANT_ID(CognitivePlatformHeaders.TENANT_ID_HEADER_NAME),
  CLIENT_IP(CognitivePlatformHeaders.CLIENT_IP_HEADER_NAME),
  CLIENT_KEY(CognitivePlatformHeaders.CLIENT_KEY_HEADER_NAME),
  CLIENT_TOKEN(CognitivePlatformHeaders.CLIENT_TOKEN_HEADER_NAME),
  USERNAME(CognitivePlatformHeaders.USERNAME_HEADER_NAME);

  public static final String CID_HEADER_NAME = "cid";
  public static final String HOST_NAME_HEADER_NAME = "x-aa-cp-hostname";
  public static final String APP_NAME_HEADER_NAME = "x-aa-cp-appname";
  public static final String APP_VERSION_HEADER_NAME = "x-aa-cp-appversion";
  public static final String SESSION_ID_HEADER_NAME = "x-aa-cp-sid";
  public static final String TENANT_ID_HEADER_NAME = "x-aa-cp-tid";
  public static final String CLIENT_IP_HEADER_NAME = "x-forwarded-for";
  public static final String CLIENT_KEY_HEADER_NAME = "clientkey";
  public static final String CLIENT_TOKEN_HEADER_NAME = "clienttoken";
  public static final String USERNAME_HEADER_NAME = "username";

  public final String headerName;

  CognitivePlatformHeaders(final String headerName) {
    this.headerName = headerName;
  }
}
