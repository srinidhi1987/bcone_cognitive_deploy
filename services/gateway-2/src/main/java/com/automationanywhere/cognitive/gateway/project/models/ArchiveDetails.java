package com.automationanywhere.cognitive.gateway.project.models;

import java.util.Objects;

public class ArchiveDetails {
  public final String archiveName;

  public ArchiveDetails(final String archiveName) {
    this.archiveName = archiveName;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ArchiveDetails that = (ArchiveDetails) o;

      result = Objects.equals(archiveName, that.archiveName);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archiveName) ^ 32;
  }
}
