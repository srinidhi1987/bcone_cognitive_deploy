package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileStatusData {

  public final Id projectId;
  public final String currentProcessingObject;
  public final Integer totalFileCount;
  public final Integer totalProcessedCount;
  public final Integer estimatedTimeRemainingInMins;
  public final Boolean complete;

  public FileStatusData(
      @JsonProperty("projectId") final Id projectId,
      @JsonProperty("currentProcessingObject") final String currentProcessingObject,
      @JsonProperty("totalFileCount") final Integer totalFileCount,
      @JsonProperty("totalProcessedCount") final Integer totalProcessedCount,
      @JsonProperty("estimatedTimeRemainingInMins") final Integer estimatedTimeRemainingInMins,
      @JsonProperty("complete") final Boolean complete
  ) {
    this.projectId = projectId;
    this.currentProcessingObject = currentProcessingObject;
    this.totalFileCount = totalFileCount;
    this.totalProcessedCount = totalProcessedCount;
    this.estimatedTimeRemainingInMins = estimatedTimeRemainingInMins;
    this.complete = complete;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileStatusData fileWorker = (FileStatusData) o;

      result = Objects.equals(projectId, fileWorker.projectId)
          && Objects.equals(currentProcessingObject, fileWorker.currentProcessingObject)
          && Objects.equals(totalFileCount, fileWorker.totalFileCount)
          && Objects.equals(totalProcessedCount, fileWorker.totalProcessedCount)
          && Objects
          .equals(estimatedTimeRemainingInMins, fileWorker.estimatedTimeRemainingInMins)
          && Objects.equals(complete, fileWorker.complete);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
        ^ Objects.hashCode(currentProcessingObject)
        ^ Objects.hashCode(totalFileCount)
        ^ Objects.hashCode(totalProcessedCount)
        ^ Objects.hashCode(estimatedTimeRemainingInMins)
        ^ Objects.hashCode(complete);
  }
}

