package com.automationanywhere.cognitive.gateway.application.model;

public class Registration {

  public final String controlRoomUrl;
  public final String controlRoomVersion;
  public final String appId;

  public Registration(
      final String controlRoomUrl,
      final String controlRoomVersion,
      final String appId
  ) {
    this.controlRoomUrl = controlRoomUrl;
    this.controlRoomVersion = controlRoomVersion;
    this.appId = appId;
  }
}
