package com.automationanywhere.cognitive.app.threadpool;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.core.task.TaskDecorator;

/**
 * Propagates log context: sets up a log context before a runnable execution;
 * doesn't restore it to the previous state after the runnable termination
 * to keep the tracking details associated with the thread as long as possible.
 */
public class LogContextPropagatingTaskDecorator implements TaskDecorator {

  @Override
  public Runnable decorate(final Runnable runnable) {
    Map<String, String> context = new HashMap<>(ThreadContext.getContext());
    return () -> propagate(context, runnable);
  }

  void propagate(final Map<String, String> context, final Runnable runnable) {
    ThreadContext.putAll(context);

    runnable.run();
  }
}
