package com.automationanywhere.cognitive.gateway.auth.microservices.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthenticationData {

  public final UserData user;
  public final String token;

  public AuthenticationData(
      @JsonProperty("user") final UserData user,
      @JsonProperty("token") final String token
  ) {
    this.user = user;
    this.token = token;
  }
}
