package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class CountStatistics {

  public final Id projectId;
  public final Id categoryId;
  public final Integer fileCount;
  public final EnvironmentCountStatistics stagingFileCount;
  public final EnvironmentCountStatistics productionFileCount;

  public CountStatistics(
      final Id projectId,
      final Id categoryId,
      final Integer fileCount,
      final EnvironmentCountStatistics stagingFileCount,
      final EnvironmentCountStatistics productionFileCount
  ) {
    this.projectId = projectId;
    this.categoryId = categoryId;
    this.fileCount = fileCount;
    this.stagingFileCount = stagingFileCount;
    this.productionFileCount = productionFileCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CountStatistics countStatistics = (CountStatistics) o;

      result = Objects.equals(projectId, countStatistics.projectId)
        && Objects.equals(categoryId, countStatistics.categoryId)
        && Objects.equals(fileCount, countStatistics.fileCount)
        && Objects.equals(stagingFileCount, countStatistics.stagingFileCount)
        && Objects.equals(productionFileCount, countStatistics.productionFileCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
      ^ Objects.hashCode(categoryId)
      ^ Objects.hashCode(fileCount)
      ^ Objects.hashCode(stagingFileCount)
      ^ Objects.hashCode(productionFileCount);
  }
}

