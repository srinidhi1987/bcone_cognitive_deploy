package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GroupTimeSpentDetailsData {

  private final String categoryId;
  private final double timeSpend;

  public GroupTimeSpentDetailsData(
      @JsonProperty("categoryId") String categoryId,
      @JsonProperty("timeSpend") double timeSpend
  ) {
    this.categoryId = categoryId;
    this.timeSpend = timeSpend;
  }

  /**
   * Returns the categoryId.
   *
   * @return the value of categoryId
   */
  public String getCategoryId() {
    return categoryId;
  }

  /**
   * Returns the timeSpend.
   *
   * @return the value of timeSpend
   */
  public double getTimeSpend() {
    return timeSpend;
  }
}
