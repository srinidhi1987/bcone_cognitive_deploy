package com.automationanywhere.cognitive.gateway.alias.model;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;

public class ProjectField {

  public final Id id;
  public final String name;
  public final String description;
  public final String fieldType;
  public final List<String> aliases;
  public final Boolean isDefaultSelected;
  public final String dataType;

  public ProjectField(
      final Id id,
      final String name,
      final String description,
      final String fieldType,
      final List<String> aliases,
      final Boolean isDefaultSelected,
      final String dataType
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.fieldType = fieldType;
    this.aliases = aliases;
    this.isDefaultSelected = isDefaultSelected;
    this.dataType = dataType;
  }
}
