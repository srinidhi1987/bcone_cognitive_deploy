package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProjectPatchResponseData {
  public final Integer versionId;

  public ProjectPatchResponseData(
      @JsonProperty("versionId") final Integer versionId
  ) {
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectPatchResponseData projectPatchResponseData
          = (ProjectPatchResponseData) o;

      result = Objects.equals(versionId, projectPatchResponseData.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(versionId);
  }
}
