package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit;

/**
 * Extracts auditing details from an event,
 * A corresponding audit instance will use the details audit the event.
 */
public interface AuditingDetailsProvider {

  /**
   * Creates auditing details suitable for an audit instance
   * corresponding to the instance of the provider.
   *
   * @param event - event to use to extract auditing details.
   * @return auditing details.
   */
  AuditingDetails extract(Object event);
}
