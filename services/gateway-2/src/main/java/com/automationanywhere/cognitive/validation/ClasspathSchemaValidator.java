package com.automationanywhere.cognitive.validation;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

/**
 * Validates JSON objects against loaded schemas. Allows configuring where the schemes will be
 * loaded from. This implementation assumes the input object is a string respresentation of a JSON
 * before deserialization. The output object must be a POJO before serialization.
 */
public class ClasspathSchemaValidator implements SchemaValidator {

  private static final Logger LOGGER = LogManager.getLogger(ClasspathSchemaValidator.class);

  private static final String JAR_SCHEMA = "jar";
  private static final String JAR_EXT_PATH = "jar!";
  private static final String EMPTY = "";
  private static final String EXCLAMATION = "!";
  private static final char SLASH = '/';
  private final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

  private final String prefix;
  private final String postfix;
  private final ObjectMapper om;
  private final ExceptionHandlerFactory ehf;
  private final Map<String, JsonNode> keyToSchema = new HashMap<>();

  public ClasspathSchemaValidator(
      final String prefix, final String postfix, final ObjectMapper om,
      final ExceptionHandlerFactory ehf
  ) {
    this.prefix = prefix;
    this.postfix = postfix;
    this.om = om;
    this.ehf = ehf;
  }

  @PostConstruct
  public void loadJsonSchemas() throws URISyntaxException, IOException {
    URL resource = ClasspathSchemaValidator.class.getResource(prefix);
    if (resource == null) {
      throw new IOException("Resource not found with prefix: " + prefix);
    }

    URI uri = resource.toURI();
    if (JAR_SCHEMA.equals(uri.getScheme())) {
      String path = uri.toString();
      int i = path.indexOf(JAR_EXT_PATH);
      path = path.substring(i + JAR_EXT_PATH.length(), path.length()).replace(EXCLAMATION, EMPTY);
      FileSystem fileSystem = null;
      try {
        fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
        Path resourcePath = fileSystem.getPath(path);
        Files.walkFileTree(resourcePath, new SimpleFileVisitor<Path>() {
          @Override
          public FileVisitResult visitFile(Path schemaPath, BasicFileAttributes attrs) {
            return ehf.call(() -> {
              String fullName = schemaPath.toString();
              if (fullName.endsWith(postfix)) {
                int slash = fullName.lastIndexOf(SLASH);
                String fileName = fullName.substring(slash + 1, fullName.length());
                URL schemaResource = ClasspathSchemaValidator.class.getResource(fullName);
                String key = fileName.substring(0, fileName.length() - postfix.length());
                try {
                  JsonNode schema = JsonLoader.fromURL(schemaResource);
                  keyToSchema.put(key, schema);
                } catch (final IOException ex) {
                  throw new IOException("Failed to load schema from " + fullName, ex);
                }
              }
              return FileVisitResult.CONTINUE;
            });
          }
        });
      } finally {
        if (fileSystem != null) {
          fileSystem.close();
        }
      }
    } else {
      Files.walk(Paths.get(uri))
          .filter(path -> !path.toFile().isDirectory() && path.toString().endsWith(postfix))
          .forEach(schemaPath -> ehf.execute(() -> {
            String fullName = schemaPath.toString();
            int slash = fullName.lastIndexOf(File.separator);
            String fileName = fullName.substring(slash + 1, fullName.length());
            String key = fileName.substring(0, fileName.length() - postfix.length());
            try {
              JsonNode schema = JsonLoader.fromFile(schemaPath.toFile());
              keyToSchema.put(key, schema);
            } catch (final IOException ex) {
              throw new IOException("Failed to load schema from " + fullName, ex);
            }
          }));
    }
  }

  @Override
  public boolean checkKey(final String key) {
    if (keyToSchema.get(key) == null) {
      throw new IllegalStateException("Key " + key + " is unknown");
    }
    return true;
  }

  @Override
  public boolean checkInput(final Object json, final String key) {
    return new JsonSchemaValidator<Object>() {
      @Override
      protected JsonNode load() throws Exception {
        return JsonLoader.fromString((String) json);
      }
    }.check(key, HttpStatus.BAD_REQUEST);
  }

  @Override
  public boolean checkOutput(final Object object, final String key) throws ValidationException {
    return new JsonSchemaValidator<Object>() {
      @Override
      protected JsonNode load() throws Exception {
        return om.valueToTree(object);
      }
    }.check(key, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  abstract class JsonSchemaValidator<T> {

    protected abstract JsonNode load() throws Exception;

    public boolean check(final String key, final HttpStatus httpStatus) {
      try {
        JsonNode json = load();

        LOGGER.trace("Validating\n{}", json);

        checkJson(json, key, httpStatus);
      } catch (final RuntimeException ex) {
        throw ex;
      } catch (final Exception ex) {
        throw new ValidationException("Failed to check an object against " + key, ex);
      }
      return true;
    }

    private void checkJson(
        final JsonNode json, final String key, final HttpStatus httpStatus
    ) throws ProcessingException {
      JsonNode schema = keyToSchema.get(key);
      JsonSchema jsonSchema = factory.getJsonSchema(schema);

      ProcessingReport processingReport = jsonSchema.validate(json);
      if (!processingReport.isSuccess()) {
        throw new JsonValidationException(processingReport, httpStatus);
      }
    }
  }
}