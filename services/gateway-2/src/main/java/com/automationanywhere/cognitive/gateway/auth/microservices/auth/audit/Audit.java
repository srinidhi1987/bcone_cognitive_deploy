package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit;

/**
 * Audits an event.
 */
public interface Audit {

  /**
   * Audits an event.
   *
   * @param event - event to audit.
   */
  void audit(final Object event);
}
