package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileDetailsData {

  public final Id fileId;
  public final Id projectId;
  public final String fileName;
  public final String fileLocation;
  public final Integer fileSize;
  public final Integer fileHeight;
  public final Integer fileWidth;
  public final String format;
  public final Boolean processed;
  public final Id classificationId;
  @JsonProperty("uploadrequestId")
  public final Id uploadRequestId;
  public final Id layoutId;
  public final String isProduction;

  public FileDetailsData(
      @JsonProperty("fileId") final Id fileId,
      @JsonProperty("projectId") final Id projectId,
      @JsonProperty("fileName") final String fileName,
      @JsonProperty("fileLocation") final String fileLocation,
      @JsonProperty("fileSize") final Integer fileSize,
      @JsonProperty("fileHeight") final Integer fileHeight,
      @JsonProperty("fileWidth") final Integer fileWidth,
      @JsonProperty("format") final String format,
      @JsonProperty("processed") final Boolean processed,
      @JsonProperty("classificationId") final Id classificationId,
      @JsonProperty("uploadrequestId") final Id uploadRequestId,
      @JsonProperty("layoutId") final Id layoutId,
      @JsonProperty("isProduction") final String isProduction
  ) {
    this.fileId = fileId;
    this.projectId = projectId;
    this.fileName = fileName;
    this.fileLocation = fileLocation;
    this.fileSize = fileSize;
    this.fileHeight = fileHeight;
    this.fileWidth = fileWidth;
    this.format = format;
    this.processed = processed;
    this.classificationId = classificationId;
    this.uploadRequestId = uploadRequestId;
    this.layoutId = layoutId;
    this.isProduction = isProduction;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileDetailsData filedetails = (FileDetailsData) o;

      result = Objects.equals(fileId, filedetails.fileId)
        && Objects.equals(projectId, filedetails.projectId)
        && Objects.equals(fileName, filedetails.fileName)
        && Objects.equals(fileLocation, filedetails.fileLocation)
        && Objects.equals(fileSize, filedetails.fileSize)
        && Objects.equals(fileHeight, filedetails.fileHeight)
        && Objects.equals(fileWidth, filedetails.fileWidth)
        && Objects.equals(format, filedetails.format)
        && Objects.equals(processed, filedetails.processed)
        && Objects.equals(classificationId, filedetails.classificationId)
        && Objects.equals(uploadRequestId, filedetails.uploadRequestId)
        && Objects.equals(layoutId, filedetails.layoutId)
        && Objects.equals(isProduction, filedetails.isProduction);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
      ^ Objects.hashCode(projectId)
      ^ Objects.hashCode(fileName)
      ^ Objects.hashCode(fileLocation)
      ^ Objects.hashCode(fileSize)
      ^ Objects.hashCode(fileHeight)
      ^ Objects.hashCode(fileWidth)
      ^ Objects.hashCode(format)
      ^ Objects.hashCode(processed)
      ^ Objects.hashCode(classificationId)
      ^ Objects.hashCode(uploadRequestId)
      ^ Objects.hashCode(layoutId)
      ^ Objects.hashCode(isProduction);
  }
}

