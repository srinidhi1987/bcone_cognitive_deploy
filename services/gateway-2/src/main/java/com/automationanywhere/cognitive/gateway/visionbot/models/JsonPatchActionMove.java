package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Objects;

public class JsonPatchActionMove extends JsonPatchAction {

  public final String from;

  public JsonPatchActionMove(
      final String path,
      final String from
  ) {
    super(path);
    this.from = from;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionMove)) {
      return false;
    }
    JsonPatchActionMove that = (JsonPatchActionMove) o;
    return Objects.equals(path, that.path) && Objects.equals(from, that.from);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, from);
  }
}
