package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Objects;

public class FieldDetailData {

  public final String fieldId;
  public final BigDecimal foundInTotalDocs;

  public FieldDetailData(
      @JsonProperty("fieldId") final String fieldId,
      @JsonProperty("foundInTotalDocs") final BigDecimal foundInTotalDocs
  ) {
    this.fieldId = fieldId;
    this.foundInTotalDocs = foundInTotalDocs;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FieldDetailData fieldDetail = (FieldDetailData) o;

      result = Objects.equals(fieldId, fieldDetail.fieldId)
        && Objects.equals(foundInTotalDocs, fieldDetail.foundInTotalDocs);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fieldId)
      ^ Objects.hashCode(foundInTotalDocs);
  }
}

