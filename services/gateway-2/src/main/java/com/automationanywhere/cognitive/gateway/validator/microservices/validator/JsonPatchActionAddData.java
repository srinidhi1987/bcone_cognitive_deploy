package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Objects;

public class JsonPatchActionAddData extends JsonPatchActionData {

  public final String op;
  public final JsonNode value;

  public JsonPatchActionAddData(
      final String path,
      final JsonNode value
  ) {
    super(path);
    this.op = "add";
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionAddData)) {
      return false;
    }
    JsonPatchActionAddData that = (JsonPatchActionAddData) o;
    return Objects.equals(path, that.path) && Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path, value);
  }
}
