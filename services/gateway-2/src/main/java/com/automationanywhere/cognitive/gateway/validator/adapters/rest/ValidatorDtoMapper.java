package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ClassificationFieldDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotdata;
import com.automationanywhere.cognitive.gateway.validator.models.FieldAccuracyDashboardDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FieldValidation;
import com.automationanywhere.cognitive.gateway.validator.models.GroupTimeSpentDetails;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchAction;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.mappers.BaseMapper;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ValidatorDtoMapper extends BaseMapper {

  public ReviewedFileCountDetailsDto mapReviewedFileCountDetails(
      final ReviewedFileCountDetails model
  ) {
    return model == null ? null : new ReviewedFileCountDetailsDto(
        model.projectId, model.processedCount, model.failedFileCount,
        model.reviewFileCount, model.invalidFileCount
    );
  }

  public SuccessDto mapSuccess(
      final Success model
  ) {
    return model == null ? null : new SuccessDto(model.success);
  }

  public InvalidFileTypeDto mapInvalidFileType(
      final InvalidFileType model
  ) {
    return model == null ? null : new InvalidFileTypeDto(
        model.id, model.reason
    );
  }

  public ProductionFileSummaryDto mapProductionFileSummary(
      final ProductionFileSummary model
  ) {
    return model == null ? null : new ProductionFileSummaryDto(
        model.projectId, model.processedFiles, model.successFiles, model.reviewFiles,
        model.invalidFiles, model.pendingForReview, model.accuracy
    );
  }

  public List<InvalidFile> mapInvalidFiles(
      final List<InvalidFileDto> invalidFiles
  ) {
    return ofNullable(invalidFiles).orElse(emptyList()).stream()
        .map(this::mapInvalidFile)
        .collect(Collectors.toList());
  }

  public InvalidFile mapInvalidFile(
      final InvalidFileDto model
  ) {
    return model == null ? null : new InvalidFile(
        model.fileId, model.reasonId, model.reasonText
    );
  }

  public FailedVbotdataDto mapFailedVBotdata(FailedVbotdata data) {
    return data == null ? null : new FailedVbotdataDto(
        data.getFileId(),
        data.getOrgId(),
        data.getProjectId(),
        data.getvBotDocument(),
        data.getCorrectedData(),
        data.getValidationStatus(),
        data.getCreatedAt(),
        data.getUpdatedBy(),
        data.getUpdatedAt(),
        data.getVisionBotId(),
        data.getLockedUserId(),
        data.getFailedFieldCounts(),
        data.getPassFieldCounts(),
        data.getTotalFieldCounts(),
        data.getStartTime(),
        data.getEndTime(),
        data.getAverageTimeInSeconds(),
        data.getLastExportTime()
    );
  }

  public AccuracyProcessingDetailsDto mapAccuracyProcessingDetails(AccuracyProcessingDetails data) {
    return data == null ? null : new AccuracyProcessingDetailsDto(
        data.getId(),
        data.getFailedFieldCounts(),
        data.getPassFieldCounts(),
        data.getTotalFieldCounts(),
        data.getFieldAccuracy(),
        data.getReviewFileCount(),
        data.getInvalidFileCount(),
        data.getFailedDocumentCount(),
        data.getPassedDocumentCounts(),
        data.getProcessedDocumentCounts(),
        data.getDocumentAccuracy(),
        data.getAverageReviewTimeInSeconds()
    );
  }

  public ProjectTotalsAccuracyDetailsDto mapPrjTotAccuracyDetails(
      ProjectTotalsAccuracyDetails data) {
    return data == null ? null : new ProjectTotalsAccuracyDetailsDto(
        data.getTotalFilesProcessed(),
        data.getTotalSTP(),
        data.getTotalAccuracy(),
        data.getTotalFilesUploaded(),
        data.getTotalFilesToValidation(),
        data.getTotalFilesValidated(),
        data.getTotalFilesUnprocessable(),
        mapClassificationFieldDetails(data.getClassification()),
        mapFieldAccuracyDashboardDetails(data.getAccuracy()),
        mapFieldValidation(data.getValidation()),
        mapGroupTimeSpentDetails(data.getCategories())
    );
  }

  public FailedVbotStatusDto mapFailedVBotStatus(FailedVbotStatus data) {
    return data == null ? null : new FailedVbotStatusDto(
        data.isSuccess(),
        data.getCount(),
        data.getError()
    );
  }

  private ClassificationFieldDetailsDto[] mapClassificationFieldDetails(
      ClassificationFieldDetails[] data
  ) {
    if (data == null) {
      return null;
    }

    ClassificationFieldDetailsDto[] result = new ClassificationFieldDetailsDto[data.length];
    for (int i = 0; i < data.length; i++) {
      ClassificationFieldDetails item = data[i];
      result[i] = new ClassificationFieldDetailsDto(
          item.getFieldId(),
          item.getRepresentationPercent()
      );
    }
    return result;
  }

  private FieldAccuracyDashboardDetailsDto[] mapFieldAccuracyDashboardDetails(
      FieldAccuracyDashboardDetails[] data
  ) {
    if (data == null) {
      return null;
    }

    FieldAccuracyDashboardDetailsDto[] result = new FieldAccuracyDashboardDetailsDto[data.length];
    for (int i = 0; i < data.length; i++) {
      FieldAccuracyDashboardDetails item = data[i];
      result[i] = new FieldAccuracyDashboardDetailsDto(
          item.getFieldId(),
          item.getAccuracyPercent()
      );
    }
    return result;
  }

  private FieldValidationDto[] mapFieldValidation(
      FieldValidation[] data
  ) {
    if (data == null) {
      return null;
    }

    FieldValidationDto[] result = new FieldValidationDto[data.length];
    for (int i = 0; i < data.length; i++) {
      FieldValidation item = data[i];
      result[i] = new FieldValidationDto(
          item.getFieldId(),
          item.getPercentValidated()
      );
    }
    return result;
  }

  private GroupTimeSpentDetailsDto[] mapGroupTimeSpentDetails(
      GroupTimeSpentDetails[] data
  ) {
    if (data == null) {
      return null;
    }

    GroupTimeSpentDetailsDto[] result = new GroupTimeSpentDetailsDto[data.length];
    for (int i = 0; i < data.length; i++) {
      GroupTimeSpentDetails item = data[i];
      result[i] = new GroupTimeSpentDetailsDto(
          item.getCategoryId(),
          item.getTimeSpend()
      );
    }
    return result;
  }

  public JsonPatchDocument mapJsonPatchDocument(List<JsonPatchActionDto> actions) {
    return new JsonPatchDocument(
        ofNullable(actions).orElse(emptyList()).stream()
            .map(this::mapJsonPatchAction)
            .filter(Objects::nonNull)
            .collect(Collectors.toList())
    );
  }

  private JsonPatchAction mapJsonPatchAction(JsonPatchActionDto dto) {
    JsonPatchAction action = null;
    if (dto.op == JsonPatchActionOperation.ADD) {
      action = new JsonPatchActionAdd(dto.path, dto.value);

    } else if (dto.op == JsonPatchActionOperation.REPLACE) {
      action = new JsonPatchActionReplace(dto.path, dto.value);

    } else if (dto.op == JsonPatchActionOperation.REMOVE) {
      action = new JsonPatchActionRemove(dto.path);

    } else if (dto.op == JsonPatchActionOperation.TEST) {
      action = new JsonPatchActionTest(dto.path, dto.value);

    } else if (dto.op == JsonPatchActionOperation.COPY) {
      action = new JsonPatchActionCopy(dto.path, dto.from);

    } else if (dto.op == JsonPatchActionOperation.MOVE) {
      action = new JsonPatchActionMove(dto.path, dto.from);
    }
    return action;
  }
}
