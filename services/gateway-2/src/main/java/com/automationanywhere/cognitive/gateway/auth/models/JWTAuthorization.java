package com.automationanywhere.cognitive.gateway.auth.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class JWTAuthorization implements Authorization {
  public final String action;
  public final String jwt;

  public JWTAuthorization(
      @JsonProperty("action") final String action,
      @JsonProperty("jwt") final String jwt
  ) {
    this.action = action;
    this.jwt = jwt;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      JWTAuthorization that = (JWTAuthorization) o;

      result = Objects.equals(action, that.action)
          && Objects.equals(jwt, that.jwt);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(action)
        ^ Objects.hashCode(jwt);
  }
}
