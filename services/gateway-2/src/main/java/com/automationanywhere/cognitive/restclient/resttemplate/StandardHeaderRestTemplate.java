package com.automationanywhere.cognitive.restclient.resttemplate;

import com.automationanywhere.cognitive.logging.LogContextKeys;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.io.IOException;
import java.lang.reflect.Type;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.RestTemplate;

/**
 * Decorates a rest template with an ability to send standard headers.
 *
 * @see CognitivePlatformHeaders
 */
public class StandardHeaderRestTemplate extends DelegatingRestTemplate {

  public StandardHeaderRestTemplate(final RestTemplate restTemplate) {
    super(restTemplate);
  }

  @Override
  protected <T> RequestCallback acceptHeaderRequestCallback(final Class<T> responseType) {
    RequestCallback requestCallback = super.acceptHeaderRequestCallback(responseType);
    return new RequestCallback() {
      @Override
      public void doWithRequest(final ClientHttpRequest request) throws IOException {
        addHeaders(request.getHeaders());

        requestCallback.doWithRequest(request);
      }
    };
  }

  @Override
  protected <T> RequestCallback httpEntityCallback(final Object requestBody) {
    return httpEntityCallback(requestBody, null);
  }

  @Override
  protected <T> RequestCallback httpEntityCallback(final Object requestBody,
      final Type responseType) {
    HttpHeaders headers = createHttpHeaders();

    addHeaders(headers);

    HttpEntity<?> requestEntity = null;

    if (requestBody instanceof RequestEntity) {
      RequestEntity oldRequestEntity = (RequestEntity) requestBody;

      addHeaders(headers, oldRequestEntity.getHeaders());
      requestEntity = createRequestEntity(headers, oldRequestEntity);
    } else if (requestBody instanceof HttpEntity) {
      HttpEntity<?> oldRequestEntity = (HttpEntity<?>) requestBody;

      addHeaders(headers, oldRequestEntity.getHeaders());
      requestEntity = createHttpEntity(oldRequestEntity.getBody(), headers);
    } else if (requestBody != null) {
      requestEntity = createHttpEntity(requestBody, headers);
    } else {
      requestEntity = createHttpEntity(null, headers);
    }

    return createRequestCallback(requestEntity, responseType);
  }

  private void addHeaders(final HttpHeaders headers, final HttpHeaders oldHeaders) {
    for (final String name : oldHeaders.keySet()) {
      headers.put(name, oldHeaders.get(name));
    }
  }

  void addHeaders(final HttpHeaders headers) {
    headers.add(CognitivePlatformHeaders.CID.headerName, ThreadContext.get(LogContextKeys.CID.key));
    headers.add(CognitivePlatformHeaders.SESSION_ID.headerName,
        ThreadContext.get(LogContextKeys.SESSION_ID.key));
    headers.add(CognitivePlatformHeaders.TENANT_ID.headerName,
        ThreadContext.get(LogContextKeys.TENANT_ID.key));
    headers.add(CognitivePlatformHeaders.HOST_NAME.headerName,
        ThreadContext.get(LogContextKeys.HOST_NAME.key));
    headers.add(CognitivePlatformHeaders.APPLICATION_NAME.headerName,
        ThreadContext.get(LogContextKeys.APPLICATION_NAME.key));
    headers.add(CognitivePlatformHeaders.APPLICATION_VERSION.headerName,
        ThreadContext.get(LogContextKeys.APPLICATION_VERSION.key));
    headers.add(CognitivePlatformHeaders.CLIENT_IP.headerName,
        ThreadContext.get(LogContextKeys.CLIENT_IP.key));
  }

  RequestCallback createRequestCallback(final HttpEntity<?> requestEntity,
      final Type responseType) {
    return super.httpEntityCallback(requestEntity, responseType);
  }

  RequestEntity<?> createRequestEntity(final HttpHeaders headers,
      final RequestEntity oldRequestEntity) {
    return new RequestEntity<>(
        oldRequestEntity.getBody(), headers, oldRequestEntity.getMethod(), oldRequestEntity.getUrl()
    );
  }

  <T> HttpEntity<T> createHttpEntity(final T body, final HttpHeaders headers) {
    return new HttpEntity<>(body, headers);
  }

  HttpHeaders createHttpHeaders() {
    return new HttpHeaders();
  }

}
