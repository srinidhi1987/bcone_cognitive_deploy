package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FilePatchDetailData {

  public final String op;
  public final String path;
  public final String value;

  public FilePatchDetailData(
      @JsonProperty("op") final String op,
      @JsonProperty("path") final String path,
      @JsonProperty("value") final String value
  ) {
    this.op = op;
    this.path = path;
    this.value = value;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FilePatchDetailData projectPatchDetailData
          = (FilePatchDetailData) o;

      result = Objects.equals(op, projectPatchDetailData.op)
          && Objects.equals(path, projectPatchDetailData.path)
          && Objects.equals(value, projectPatchDetailData.value);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(op)
        ^ Objects.hashCode(path)
        ^ Objects.hashCode(value);
  }
}

