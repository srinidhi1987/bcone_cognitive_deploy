package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class NewProjectDetailsDto {

  public final String name;
  public final String description;
  public final Id organizationId;
  public final Id projectTypeId;
  public final String projectType;
  public final String primaryLanguage;
  public final FieldDetailsDto fields;
  public final String projectState;
  public final String environment;

  public NewProjectDetailsDto(
      @JsonProperty("name") final String name,
      @JsonProperty("description") final String description,
      @JsonProperty("organizationId") final Id organizationId,
      @JsonProperty("projectTypeId") final Id projectTypeId,
      @JsonProperty("projectType") final String projectType,
      @JsonProperty("primaryLanguage") final String primaryLanguage,
      @JsonProperty("fields") final FieldDetailsDto fields,
      @JsonProperty("projectState") final String projectState,
      @JsonProperty("environment") final String environment
  ) {
    this.name = name;
    this.description = description;
    this.organizationId = organizationId;
    this.projectTypeId = projectTypeId;
    this.projectType = projectType;
    this.primaryLanguage = primaryLanguage;
    this.fields = fields;
    this.projectState = projectState;
    this.environment = environment;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      NewProjectDetailsDto projectDetailForAddition = (NewProjectDetailsDto) o;

      result = Objects.equals(name, projectDetailForAddition.name)
        && Objects.equals(description, projectDetailForAddition.description)
        && Objects.equals(organizationId, projectDetailForAddition.organizationId)
        && Objects.equals(projectTypeId, projectDetailForAddition.projectTypeId)
        && Objects.equals(projectType, projectDetailForAddition.projectType)
        && Objects.equals(primaryLanguage, projectDetailForAddition.primaryLanguage)
        && Objects.equals(fields, projectDetailForAddition.fields)
        && Objects.equals(projectState, projectDetailForAddition.projectState)
        && Objects.equals(environment, projectDetailForAddition.environment);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name)
      ^ Objects.hashCode(description)
      ^ Objects.hashCode(organizationId)
      ^ Objects.hashCode(projectTypeId)
      ^ Objects.hashCode(projectType)
      ^ Objects.hashCode(primaryLanguage)
      ^ Objects.hashCode(fields)
      ^ Objects.hashCode(projectState)
      ^ Objects.hashCode(environment);
  }
}

