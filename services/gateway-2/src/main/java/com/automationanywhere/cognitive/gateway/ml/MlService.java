package com.automationanywhere.cognitive.gateway.ml;

import com.automationanywhere.cognitive.gateway.ml.ml.MlMicroservice;
import com.automationanywhere.cognitive.gateway.ml.model.Value;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MlService {

  private final MlMicroservice repository;

  @Autowired
  public MlService(final MlMicroservice repository) {
    this.repository = repository;
  }

  public CompletableFuture<byte[]> predict(
      final String fieldId, final String originalValue
  ) {
    return repository.predict(fieldId, originalValue);
  }

  public CompletableFuture<byte[]> setValue(final List<Value> value) {
    return repository.setValue(value);
  }
}
