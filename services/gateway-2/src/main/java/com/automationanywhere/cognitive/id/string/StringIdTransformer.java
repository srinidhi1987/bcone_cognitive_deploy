package com.automationanywhere.cognitive.id.string;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import org.springframework.stereotype.Component;

@Component
public class StringIdTransformer implements IdTransformer {
  @Override
  public StringId fromNode(final TreeNode node) throws IllegalArgumentException {
    StringId result = null;
    if (node != null) {
      if (!(node instanceof NullNode)) {
        if (!(node instanceof TextNode)) {
          throw new UnsupportedOperationException(node.getClass() + " is not supported");
        }
        result = fromString(((TextNode) node).asText());
      }
    }
    return result;
  }

  @Override
  public TreeNode toNode(final Id id) {
    return id == null ? NullNode.getInstance() : new TextNode(toString(id));
  }

  @Override
  public StringId fromString(final String id) {
    return id == null ? null : new StringId(id);
  }

  @Override
  public String toString(final Id id) {
    if (id != null && !supports(id.getClass())) {
      throw new UnsupportedOperationException(id.getClass() + " is not supported");
    }

    return id == null ? null : ((StringId) id).id;
  }

  @Override
  public boolean supports(final Class<? extends Id> type) {
    return type.equals(StringId.class);
  }
}
