package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class EnvironmentCountStatistics {

  public final Integer unprocessedCount;
  public final Integer totalCount;

  public EnvironmentCountStatistics(
      final Integer unprocessedCount,
      final Integer totalCount
  ) {
    this.unprocessedCount = unprocessedCount;
    this.totalCount = totalCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      EnvironmentCountStatistics environmentCountStatistics = (EnvironmentCountStatistics) o;

      result = Objects.equals(unprocessedCount, environmentCountStatistics.unprocessedCount)
        && Objects.equals(totalCount, environmentCountStatistics.totalCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(unprocessedCount)
      ^ Objects.hashCode(totalCount);
  }
}

