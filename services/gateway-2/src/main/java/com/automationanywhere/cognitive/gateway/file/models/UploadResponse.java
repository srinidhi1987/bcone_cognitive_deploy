package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class UploadResponse {

  public final String fileUploadJobId;

  public UploadResponse(
      final String fileUploadJobId
  ) {
    this.fileUploadJobId = fileUploadJobId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      UploadResponse uploadResponse = (UploadResponse) o;

      result = Objects.equals(fileUploadJobId, uploadResponse.fileUploadJobId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileUploadJobId);
  }
}

