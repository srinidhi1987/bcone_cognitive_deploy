package com.automationanywhere.cognitive.restclient.resttemplate;

import java.util.Map;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.util.concurrent.SettableListenableFuture;

/**
 * Callback for a listenable future that itself delegates to another listenable future
 * but before that sets the log context.
 */
public class LogContextPropagatingListenableFutureCallback implements
    ListenableFutureCallback<ClientHttpResponse> {

  private final Map<String, String> ctx;
  private final SettableListenableFuture<ClientHttpResponse> future;

  public LogContextPropagatingListenableFutureCallback(
      final SettableListenableFuture<ClientHttpResponse> future
  ) {
    ctx = ThreadContext.getContext();
    this.future = future;
  }

  @Override
  public void onFailure(final Throwable ex) {
    ThreadContext.putAll(ctx);

    future.setException(ex);
  }

  @Override
  public void onSuccess(final ClientHttpResponse result) {
    ThreadContext.putAll(ctx);

    future.set(result);
  }
}
