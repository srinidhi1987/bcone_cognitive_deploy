package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import java.util.Objects;

public class JsonPatchActionCopyData extends JsonPatchActionData {

  public final String op;
  public final String from;

  public JsonPatchActionCopyData(
      final String path,
      final String from
  ) {
    super(path);
    this.op = "copy";
    this.from = from;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionCopyData)) {
      return false;
    }
    JsonPatchActionCopyData that = (JsonPatchActionCopyData) o;
    return Objects.equals(path, that.path) && Objects.equals(from, that.from);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path, from);
  }
}
