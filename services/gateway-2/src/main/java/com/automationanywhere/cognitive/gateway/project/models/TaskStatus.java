package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class TaskStatus {

  public final Id taskId;
  public final String taskType;
  public final String status;
  public final String description;

  public TaskStatus(
      final Id taskId,
      final String taskType,
      final String status,
      final String description
  ) {
    this.taskId = taskId;
    this.taskType = taskType;
    this.status = status;
    this.description = description;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      TaskStatus taskStatus = (TaskStatus) o;

      result = Objects.equals(taskId, taskStatus.taskId)
          && Objects.equals(taskType, taskStatus.taskType)
          && Objects.equals(status, taskStatus.status)
          && Objects.equals(description, taskStatus.description);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(taskId)
        ^ Objects.hashCode(taskType)
        ^ Objects.hashCode(status)
        ^ Objects.hashCode(description);
  }
}

