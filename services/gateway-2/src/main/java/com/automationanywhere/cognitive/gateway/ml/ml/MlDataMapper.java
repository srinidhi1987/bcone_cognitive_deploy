package com.automationanywhere.cognitive.gateway.ml.ml;

import com.automationanywhere.cognitive.gateway.ml.model.Value;
import org.springframework.stereotype.Component;

@Component
public class MlDataMapper {

  public ValueData mapValue(final Value value) {
    return value == null ? null : new ValueData(
        value.fieldId, value.correctedValue, value.originalValue
    );
  }
}
