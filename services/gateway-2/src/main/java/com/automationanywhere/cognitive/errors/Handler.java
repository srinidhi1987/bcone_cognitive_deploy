package com.automationanywhere.cognitive.errors;

import java.util.function.Consumer;

/**
 * Handles an input data.
 *
 * Similar to {@link Consumer} but allows throwing exceptions.
 */
public interface Handler<I> {

  /**
   * Handles an input data.
   *
   * @throws Exception - thrown in case of an error which the handler is not able to handle itself.
   */
  void handle(I input) throws Exception;
}
