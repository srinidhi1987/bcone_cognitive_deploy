package com.automationanywhere.cognitive.resourcelock;

public class LockedException extends RuntimeException {

  private static final long serialVersionUID = -3831843550218665955L;
  public final String activityName;

  public LockedException(final String activityName) {
    this.activityName = activityName;
  }
}
