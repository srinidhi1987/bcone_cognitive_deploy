package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class InvalidFileData {

  public final Id fileId;
  public final String reasonId;
  public final String reasonText;

  public InvalidFileData(
      @JsonProperty("fileId") final Id fileId,
      @JsonProperty("reasonId") final String reasonId,
      @JsonProperty("reasonText") final String reasonText
  ) {
    this.fileId = fileId;
    this.reasonId = reasonId;
    this.reasonText = reasonText;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      InvalidFileData that = (InvalidFileData) o;

      result = Objects.equals(fileId, that.fileId)
          && Objects.equals(reasonId, that.reasonId)
          && Objects.equals(reasonText, that.reasonText);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
        ^ Objects.hashCode(reasonId)
        ^ Objects.hashCode(reasonText);
  }
}
