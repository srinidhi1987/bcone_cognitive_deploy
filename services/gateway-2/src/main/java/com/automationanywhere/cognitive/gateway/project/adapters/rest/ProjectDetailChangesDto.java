package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class ProjectDetailChangesDto {

  public final Id id;
  public final String name;
  public final String description;
  public final Id organizationId;
  public final Id projectTypeId;
  public final String projectType;
  public final String primaryLanguage;
  public final FieldDetailsDto fields;
  public final String projectState;
  public final String environment;
  public final List<OCREngineDetailDto> ocrEngineDetails;
  public final Integer versionId;

  public ProjectDetailChangesDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("description") final String description,
      @JsonProperty("organizationId") final Id organizationId,
      @JsonProperty("projectTypeId") final Id projectTypeId,
      @JsonProperty("projectType") final String projectType,
      @JsonProperty("primaryLanguage") final String primaryLanguage,
      @JsonProperty("fields") final FieldDetailsDto fields,
      @JsonProperty("projectState") final String projectState,
      @JsonProperty("environment") final String environment,
      @JsonProperty("ocrEngineDetails") final List<OCREngineDetailDto> ocrEngineDetails,
      @JsonProperty("versionId") final Integer versionId
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.organizationId = organizationId;
    this.projectTypeId = projectTypeId;
    this.projectType = projectType;
    this.primaryLanguage = primaryLanguage;
    this.fields = fields;
    this.projectState = projectState;
    this.environment = environment;
    this.ocrEngineDetails = ocrEngineDetails;
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectDetailChangesDto projectDetailForUpdation = (ProjectDetailChangesDto) o;

      result = Objects.equals(id, projectDetailForUpdation.id)
        && Objects.equals(name, projectDetailForUpdation.name)
        && Objects.equals(description, projectDetailForUpdation.description)
        && Objects.equals(organizationId, projectDetailForUpdation.organizationId)
        && Objects.equals(projectTypeId, projectDetailForUpdation.projectTypeId)
        && Objects.equals(projectType, projectDetailForUpdation.projectType)
        && Objects.equals(primaryLanguage, projectDetailForUpdation.primaryLanguage)
        && Objects.equals(fields, projectDetailForUpdation.fields)
        && Objects.equals(projectState, projectDetailForUpdation.projectState)
        && Objects.equals(environment, projectDetailForUpdation.environment)
        && Objects.equals(ocrEngineDetails, projectDetailForUpdation.ocrEngineDetails)
        && Objects.equals(versionId, projectDetailForUpdation.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(name)
      ^ Objects.hashCode(description)
      ^ Objects.hashCode(organizationId)
      ^ Objects.hashCode(projectTypeId)
      ^ Objects.hashCode(projectType)
      ^ Objects.hashCode(primaryLanguage)
      ^ Objects.hashCode(fields)
      ^ Objects.hashCode(projectState)
      ^ Objects.hashCode(environment)
      ^ Objects.hashCode(ocrEngineDetails)
      ^ Objects.hashCode(versionId);
  }
}

