package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

public abstract class JsonPatchActionData {

  public final String path;

  public JsonPatchActionData(
      final String path
  ) {
    this.path = path;
  }
}
