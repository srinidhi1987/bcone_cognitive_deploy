package com.automationanywhere.cognitive.gateway.report.microservice.report.model;

public class ReportingProjectData {

  private String id;
  private String organizationId;
  private String name;
  private String description;
  private String environment;
  private String primaryLanguage;
  private String projectType;
  private String projectState;
  private long totalFilesProcessed;
  private long totalSTP;
  private long numberOfFiles;
  private long totalAccuracy;
  private long currentTrainedPercentage;

  /**
   * Returns the project unique identifier.
   *
   * @return the value of id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the project unique identifier.
   *
   * @param id the id to be set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Returns the organization unique identifier.
   *
   * @return the value of organizationId
   */
  public String getOrganizationId() {
    return organizationId;
  }

  /**
   * Sets the organization unique identifier.
   *
   * @param organizationId the organizationId to be set
   */
  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  /**
   * Returns the name of the project.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of the project.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Returns the description of the project.
   *
   * @return the value of description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the description of the project.
   *
   * @param description the description to be set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Returns the current environment of the project.
   *
   * @return the value of environment
   */
  public String getEnvironment() {
    return environment;
  }

  /**
   * Sets the current environment of the project.
   *
   * @param environment the environment to be set
   */
  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  /**
   * Returns the primary language for the project.
   *
   * @return the value of primaryLanguage
   */
  public String getPrimaryLanguage() {
    return primaryLanguage;
  }

  /**
   * Sets the primary language for the project.
   *
   * @param primaryLanguage the primaryLanguage to be set
   */
  public void setPrimaryLanguage(String primaryLanguage) {
    this.primaryLanguage = primaryLanguage;
  }

  /**
   * Returns the domain of the project.
   *
   * @return the value of projectType
   */
  public String getProjectType() {
    return projectType;
  }

  /**
   * Sets the domain of the project.
   *
   * @param projectType the projectType to be set
   */
  public void setProjectType(String projectType) {
    this.projectType = projectType;
  }

  /**
   * Returns the state of the project.
   *
   * @return the value of projectState
   */
  public String getProjectState() {
    return projectState;
  }

  /**
   * Sets the state of the project.
   *
   * @param projectState the projectState to be set
   */
  public void setProjectState(String projectState) {
    this.projectState = projectState;
  }

  /**
   * Returns the total count of files processed.
   *
   * @return the value of totalFilesProcessed
   */
  public long getTotalFilesProcessed() {
    return totalFilesProcessed;
  }

  /**
   * Sets the total count of files processed.
   *
   * @param totalFilesProcessed the totalFilesProcessed to be set
   */
  public void setTotalFilesProcessed(long totalFilesProcessed) {
    this.totalFilesProcessed = totalFilesProcessed;
  }

  /**
   * Returns the total count for straight-through processing.
   *
   * @return the value of totalSTP
   */
  public long getTotalSTP() {
    return totalSTP;
  }

  /**
   * Sets the total count for straight-through processing.
   *
   * @param totalSTP the totalSTP to be set
   */
  public void setTotalSTP(long totalSTP) {
    this.totalSTP = totalSTP;
  }

  /**
   * Returns the total files uploaded.
   *
   * @return the value of numberOfFiles
   */
  public long getNumberOfFiles() {
    return numberOfFiles;
  }

  /**
   * Sets the total files uploaded.
   *
   * @param numberOfFiles the numberOfFiles to be set
   */
  public void setNumberOfFiles(long numberOfFiles) {
    this.numberOfFiles = numberOfFiles;
  }

  /**
   * Returns the total accuracy.
   *
   * @return the value of totalAccuracy
   */
  public long getTotalAccuracy() {
    return totalAccuracy;
  }

  /**
   * Sets the total accuracy.
   *
   * @param totalAccuracy the totalAccuracy to be set
   */
  public void setTotalAccuracy(long totalAccuracy) {
    this.totalAccuracy = totalAccuracy;
  }

  /**
   * Returns the current trained percentage of bots.
   *
   * @return the value of currentTrainedPercentage
   */
  public long getCurrentTrainedPercentage() {
    return currentTrainedPercentage;
  }

  /**
   * Sets the current trained percentage of bots.
   *
   * @param currentTrainedPercentage the currentTrainedPercentage to be set
   */
  public void setCurrentTrainedPercentage(long currentTrainedPercentage) {
    this.currentTrainedPercentage = currentTrainedPercentage;
  }
}
