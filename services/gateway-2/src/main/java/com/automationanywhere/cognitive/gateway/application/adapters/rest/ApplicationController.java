package com.automationanywhere.cognitive.gateway.application.adapters.rest;

import static com.automationanywhere.cognitive.validation.Schemas.ACKNOWLEDGEMENT;
import static com.automationanywhere.cognitive.validation.Schemas.CONFIGURATION;
import static com.automationanywhere.cognitive.validation.Schemas.HANDSHAKE;
import static com.automationanywhere.cognitive.validation.Schemas.REGISTRATION;
import static com.automationanywhere.cognitive.validation.Schemas.V1;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.automationanywhere.cognitive.common.auth.AnonymousAccess;
import com.automationanywhere.cognitive.gateway.application.ApplicationService;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

  private final ApplicationService service;
  private final ApplicationDtoMapper mapper;

  @Autowired
  public ApplicationController(
      final ApplicationService service,
      final ApplicationDtoMapper mapper
  ) {
    this.service = service;
    this.mapper = mapper;
  }

  @RequestMapping(
      path = "/v1/registration/handshake",
      method = POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @AnonymousAccess
  @SchemaValidation(input = REGISTRATION, output = HANDSHAKE)
  public CompletableFuture<HandshakeDto> doHandshake(
      @RequestBody final RegistrationDto registration
  ) {
    return service.register(mapper.mapRegistration(registration))
        .thenApply(mapper::mapHandshake);
  }

  @RequestMapping(
      path = "/v1/registration/acknowledge",
      method = POST,
      consumes = {MediaType.APPLICATION_JSON_VALUE}
  )
  @AnonymousAccess
  @SchemaValidation(input = ACKNOWLEDGEMENT)
  public CompletableFuture<Void> acknowledge(
      @RequestBody final AcknowledgementDto acknowledgement
  ) throws Exception {
    return service.acknowledge(mapper.mapAcknowledgement(acknowledgement));
  }

  @RequestMapping(
      path = "/configuration",
      method = GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @AnonymousAccess
  @SchemaValidation(output = CONFIGURATION)
  public CompletableFuture<ConfigurationDto> retrieveConfiguration() {
    return service.retrieveConfiguration()
        .thenApply(mapper::mapConfiguration);
  }
}
