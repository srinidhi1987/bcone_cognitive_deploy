package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.log4j2;

import com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.Audit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Log4J2Audit implements Audit {
  private static final Logger LOGGER = LogManager.getLogger(Log4J2Audit.class);

  private final Log4J2AuditingDetailsProvider auditingDetailsProvider;

  @Autowired
  public Log4J2Audit(final Log4J2AuditingDetailsProvider auditingDetailsProvider) {
    this.auditingDetailsProvider = auditingDetailsProvider;
  }

  public void audit(final Object event) {
    Log4J2AuditingDetails details = auditingDetailsProvider.extract(event);
    if (details != null) {
      LOGGER.debug(
          details.message,
          details.suppliers.toArray(new Supplier[details.suppliers.size()])
      );
    }
  }
}
