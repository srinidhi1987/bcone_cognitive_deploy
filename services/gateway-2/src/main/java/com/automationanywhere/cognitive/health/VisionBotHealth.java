package com.automationanywhere.cognitive.health;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.common.healthcheck.commands.MicroserviceTextHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.SimpleHttpStatusCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Vision Bot Service health check command.
 */
@Component
@SuppressWarnings("unused")
public class VisionBotHealth extends SimpleHttpStatusCheckCommand {

  @Autowired
  public VisionBotHealth(
      final ApplicationConfiguration config,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory,
      final AsyncRestTemplate restTemplate
  ) {
    super(
        "VisionBot",
        Builder.get(config.visionBotURL()).build().url,
        buildInfoFactory,
        appInfoFactory,
        restTemplate
    );
  }
}
