package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Objects;

public class JsonPatchActionReplaceData extends JsonPatchActionData {

  public final String op;
  public final JsonNode value;

  public JsonPatchActionReplaceData(
      final String path,
      final JsonNode value
  ) {
    super(path);
    this.op = "replace";
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionReplaceData)) {
      return false;
    }
    JsonPatchActionReplaceData that = (JsonPatchActionReplaceData) o;
    return Objects.equals(path, that.path) && Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path, value);
  }
}
