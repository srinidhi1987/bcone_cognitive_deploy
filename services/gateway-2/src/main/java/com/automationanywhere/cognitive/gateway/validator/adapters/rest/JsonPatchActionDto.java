package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.Objects;

public final class JsonPatchActionDto {

  public final JsonPatchActionOperation op;
  public final String path;
  public final JsonNode value;
  public final String from;

  public JsonPatchActionDto(
      @JsonProperty("op") final JsonPatchActionOperation op,
      @JsonProperty("path") final String path,
      @JsonProperty("value") final JsonNode value,
      @JsonProperty("from") final String from
  ) {
    this.op = op;
    this.path = path;
    this.value = value;
    this.from = from;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionDto)) {
      return false;
    }
    JsonPatchActionDto that = (JsonPatchActionDto) o;
    return Objects.equals(op, that.op) &&
        Objects.equals(path, that.path) &&
        Objects.equals(value, that.value) &&
        Objects.equals(from, that.from);
  }

  @Override
  public int hashCode() {
    return Objects.hash(op, path, value, from);
  }
}