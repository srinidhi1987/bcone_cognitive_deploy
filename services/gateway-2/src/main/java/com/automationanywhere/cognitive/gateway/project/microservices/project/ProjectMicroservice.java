package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ProjectMicroservice {

  private static final String USER_NAME_HEADER_NAME = "updatedBy";

  private final ProjectDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public ProjectMicroservice(
      final ProjectDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  RestClient getRestClient() {
    return restClient;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<ProjectDetails>> getOrganizationProjects(
      final Id organizationId, final List<String> excludeFields
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getOrganizationProjects())
            .param(organizationId)
            .param(excludeFields.stream().collect(Collectors.joining(",")))
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    ).thenApply(
        data -> data == null ?
            Collections.emptyList() :
            data.stream().map(mapper::mapProjectDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<ProjectDetails>> getOrganizationProjectMetadataList(
      final Id organizationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getOrganizationProjectMetadataList())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<ProjectDetailsData>>() {
        }
    ).thenApply(
        data -> data == null ?
            Collections.emptyList() :
            data.stream().map(mapper::mapProjectDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProjectDetails> getOrganizationProject(
      final Id organizationId, final Id projectId, final String excludeFields
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getOrganizationProject())
            .param(organizationId)
            .param(projectId)
            .param(excludeFields)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ProjectDetailsData>() {
        }
    ).thenApply(mapper::mapProjectDetails);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProjectDetailChanges> getOrganizationProjectMetadata(
      final Id organizationId, final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getOrganizationProjectMetadata())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ProjectDetailChangesData>() {
        }
    ).thenApply(mapper::mapProjectDetailChanges);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProjectDetailChanges> createOrganizationProject(
      final Id organizationId, final NewProjectDetails newProject
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.projectURL())
            .request(cfg.createOrganizationProject())
            .param(organizationId)
            .content(mapper.mapNewProjectDetails(newProject), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ProjectDetailChangesData>() {
        }
    ).thenApply(mapper::mapProjectDetailChanges);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> updateOrganizationProject(
      final Id organizationId, final Id projectId, final ProjectDetailChanges project
  ) {
    return getRestClient().send(
        RequestDetails.Builder.put(cfg.projectURL())
            .request(cfg.updateOrganizationProject())
            .param(organizationId)
            .param(projectId)
            .content(mapper.mapProjectDetailChanges(project), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProjectDetails> patchOrganizationProject(
      final Id organizationId,
      final Id projectId,
      final List<ProjectPatchDetail> projectPatchDetails
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.projectURL()) // That's correct, they expect POST
            .setHeader("X-HTTP-Method-Override", "PATCH")
            .request(cfg.patchOrganizationProject())
            .param(organizationId)
            .param(projectId)
            .content(
                projectPatchDetails.stream()
                    .map(mapper::mapProjectPatchDetail).collect(Collectors.toList()),
                Types.JSON
            )
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<ProjectDetailsData>>() {
        }
    ).thenApply(r -> mapper.mapProjectDetails(r.data));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> deleteOrganizationProject(
      final Id organizationId,
      final Id projectId,
      final String userName
  ) {
    return getRestClient().send(
        RequestDetails.Builder.delete(cfg.projectURL())
            .request(cfg.deleteOrganizationProject())
            .param(organizationId)
            .param(projectId)
            .setHeader(USER_NAME_HEADER_NAME, userName)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<ArchiveDetails>> getArchiveDetailList(final Id organizationId) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getArchiveDetailList())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<ArchiveDetailsData>>() {
        }
    ).thenApply(l -> l == null
        ? Collections.emptyList()
        : l.stream().map(mapper::mapArchiveDetails).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<TaskStatus> importOrganizationProject(
      final Id organizationId, final ProjectImportRequest projectImportRequest
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.projectURL())
            .request(cfg.importOrganizationProjects())
            .param(organizationId)
            .content(mapper.mapProjectImportRequest(projectImportRequest), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<TaskStatusData>() {
        }
    ).thenApply(mapper::mapTaskStatus);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<TaskStatus> exportOrganizationProjects(
      final Id organizationId, final ProjectExportRequest projectExportRequest
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.projectURL())
            .request(cfg.exportOrganizationProjects())
            .param(organizationId)
            .content(mapper.mapProjectExportRequest(projectExportRequest), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<TaskStatusData>() {
        }
    ).thenApply(mapper::mapTaskStatus);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<TaskStatus>> getOrganizationProjectTaskStatus(
      final Id organizationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getOrganizationProjectTaskStatus())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<TaskStatusData>>() {
        }
    ).thenApply(l -> l == null
        ? Collections.emptyList()
        : l.stream().map(mapper::mapTaskStatus).collect(Collectors.toList())
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<Task>> getOrganizationProjectTasks(
      final Id organizationId,
      final String sort,
      final String order
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.projectURL())
            .request(cfg.getOrganizationProjectTasks())
            .param(organizationId)
            .param(sort)
            .param(order)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<TaskData>>() {
        }
    ).thenApply(l -> l == null
        ? Collections.emptyList()
        : l.stream().map(mapper::mapTask).collect(Collectors.toList())
    );
  }
}
