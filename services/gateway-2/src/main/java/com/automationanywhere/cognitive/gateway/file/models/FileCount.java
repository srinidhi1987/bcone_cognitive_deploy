package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class FileCount {

  public final Integer totalCount;
  public final Integer totalSTPCount;
  public final Integer unprocessedCount;

  public FileCount(
      final Integer totalCount,
      final Integer totalSTPCount,
      final Integer unprocessedCount
  ) {
    this.totalCount = totalCount;
    this.totalSTPCount = totalSTPCount;
    this.unprocessedCount = unprocessedCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileCount fileCount = (FileCount) o;

      result = Objects.equals(totalCount, fileCount.totalCount)
        && Objects.equals(totalSTPCount, fileCount.totalSTPCount)
        && Objects.equals(unprocessedCount, fileCount.unprocessedCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalCount)
      ^ Objects.hashCode(totalSTPCount)
      ^ Objects.hashCode(unprocessedCount);
  }
}

