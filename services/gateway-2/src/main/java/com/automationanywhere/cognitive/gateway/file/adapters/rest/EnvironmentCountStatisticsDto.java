package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class EnvironmentCountStatisticsDto {

  public final Integer unprocessedCount;
  public final Integer totalCount;

  public EnvironmentCountStatisticsDto(
      @JsonProperty("unprocessedCount") final Integer unprocessedCount,
      @JsonProperty("totalCount") final Integer totalCount
  ) {
    this.unprocessedCount = unprocessedCount;
    this.totalCount = totalCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      EnvironmentCountStatisticsDto environmentCountStatistics = (EnvironmentCountStatisticsDto) o;

      result = Objects.equals(unprocessedCount, environmentCountStatistics.unprocessedCount)
        && Objects.equals(totalCount, environmentCountStatistics.totalCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(unprocessedCount)
      ^ Objects.hashCode(totalCount);
  }
}

