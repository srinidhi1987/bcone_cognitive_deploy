package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileDto {

  public final Id id;
  public final String name;
  public final String location;
  public final String format;
  public final Boolean processed;

  public FileDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("location") final String location,
      @JsonProperty("format") final String format,
      @JsonProperty("processed") final Boolean processed
  ) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.format = format;
    this.processed = processed;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      FileDto fileData = (FileDto) o;

      result = Objects.equals(id, fileData.id)
          && Objects.equals(name, fileData.name)
          && Objects.equals(location, fileData.location)
          && Objects.equals(format, fileData.format)
          && Objects.equals(processed, fileData.processed);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(location)
        ^ Objects.hashCode(format)
        ^ Objects.hashCode(processed);
  }
}
