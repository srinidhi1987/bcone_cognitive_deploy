package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class StagingFilesSummaryData {

  public final Integer totalFilesCount;
  public final Integer totalPassedFileCount;
  public final Integer totalFailedFileCount;
  public final Integer totalTestedFileCount;
  public final Integer accuracy;

  public StagingFilesSummaryData(
      @JsonProperty("totalFilesCount") final Integer totalFilesCount,
      @JsonProperty("totalPassedFileCount") final Integer totalPassedFileCount,
      @JsonProperty("totalFailedFileCount") final Integer totalFailedFileCount,
      @JsonProperty("totalTestedFileCount") final Integer totalTestedFileCount,
      @JsonProperty("accuracy") final Integer accuracy
  ) {
    this.totalFilesCount = totalFilesCount;
    this.totalPassedFileCount = totalPassedFileCount;
    this.totalFailedFileCount = totalFailedFileCount;
    this.totalTestedFileCount = totalTestedFileCount;
    this.accuracy = accuracy;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      StagingFilesSummaryData stagingFilesSummary = (StagingFilesSummaryData) o;

      result = Objects.equals(totalFilesCount, stagingFilesSummary.totalFilesCount)
        && Objects.equals(totalPassedFileCount, stagingFilesSummary.totalPassedFileCount)
        && Objects.equals(totalFailedFileCount, stagingFilesSummary.totalFailedFileCount)
        && Objects.equals(totalTestedFileCount, stagingFilesSummary.totalTestedFileCount)
        && Objects.equals(accuracy, stagingFilesSummary.accuracy);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalFilesCount)
      ^ Objects.hashCode(totalPassedFileCount)
      ^ Objects.hashCode(totalFailedFileCount)
      ^ Objects.hashCode(totalTestedFileCount)
      ^ Objects.hashCode(accuracy);
  }
}

