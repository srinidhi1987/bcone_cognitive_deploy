package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ValidatorDetailsDto {
  @JsonProperty("ID")
  public final Id id;

  @JsonProperty("ValidatorData")
  public final ValidatorPropertiesDto validatorProperties;

  @JsonProperty("TypeOfData")
  public final String typeOfData;

  @JsonProperty("SerializedData")
  public final String serializedData;

  public ValidatorDetailsDto(
      @JsonProperty("ID") final Id id,
      @JsonProperty("ValidatorData") final ValidatorPropertiesDto validatorProperties,
      @JsonProperty("TypeOfData") final String typeOfData,
      @JsonProperty("SerializedData") final String serializedData
  ) {
    this.id = id;
    this.validatorProperties = validatorProperties;
    this.typeOfData = typeOfData;
    this.serializedData = serializedData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ValidatorDetailsDto that = (ValidatorDetailsDto) o;

      result = Objects.equals(id, that.id)
          && Objects.equals(validatorProperties, that.validatorProperties)
          && Objects.equals(typeOfData, that.typeOfData)
          && Objects.equals(serializedData, that.serializedData);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(validatorProperties)
        ^ Objects.hashCode(typeOfData)
        ^ Objects.hashCode(serializedData);
  }
}
