package com.automationanywhere.cognitive.gateway.auth.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class KeyTokenAuthorization implements Authorization {
  @JsonProperty("clientkey")
  public final String clientKey;

  @JsonProperty("clienttoken")
  public final String clientToken;

  public KeyTokenAuthorization(
      @JsonProperty("clientKey") final String clientKey,
      @JsonProperty("clientToken") final String clientToken
  ) {
    this.clientKey = clientKey;
    this.clientToken = clientToken;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      KeyTokenAuthorization that = (KeyTokenAuthorization) o;

      result = Objects.equals(clientKey, that.clientKey)
          && Objects.equals(clientToken, that.clientToken);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(clientKey)
        ^ Objects.hashCode(clientToken);
  }
}
