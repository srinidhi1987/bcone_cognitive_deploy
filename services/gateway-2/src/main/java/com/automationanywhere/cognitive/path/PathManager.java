package com.automationanywhere.cognitive.path;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Creates paths from various input data.
 */
@Component
public class PathManager {

  /**
   * Creates a path from another path replacing path parameters of {param} type
   * with parameters of :param type.
   *
   * A path is normalized if it starts with slash and contains no empty path elements,
   * for example, el/el, /el//el, // are not normalized. Examples of normalized paths are
   * /, /el/, /el/el
   *
   * @param normalizedPath - an input path.
   * @return created path.
   */
  public String create(final String normalizedPath) {
    StringBuilder sb = new StringBuilder();

    for (final String p : normalizedPath.split("/", -1)) {
      if (!p.isEmpty()) {
        String inner = p;
        if (inner.startsWith("{") && inner.endsWith("}")) {
          inner = ":" + inner.substring(1, inner.length() - 1);
        }
        sb.append("/");
        sb.append(inner);
      }
    }

    String result = sb.toString();
    if (normalizedPath.endsWith("/")) {
      if (!result.endsWith("/")) {
        result += "/";
      }
    }
    return result;
  }

  /**
   * Creates a normalized path by concatenating path elements.
   *
   * @param elements - path elements to concat.
   * @return created path.
   */
  public String create(final String... elements) {
    StringBuilder sb = new StringBuilder();

    String lastElement = "";
    for (final String element : elements) {
      if (!element.isEmpty()) {
        lastElement = element;
      }
      for (final String p : element.split("/", -1)) {
        if (!p.isEmpty()) {
          sb.append("/");
          sb.append(p);
        }
      }
    }

    if (sb.length() == 0) {
      sb.append("/");
    }

    String result = sb.toString();
    if (lastElement.endsWith("/") && !result.endsWith("/")) {
      result += "/";
    }

    return result;
  }

  /**
   * Creates a path from a handler method annotated with RequestMapping annotation.
   *
   * @param method - annotated method.
   * @return created path.
   */
  public String create(final Method method) {
    String prefix = getPath(method.getDeclaringClass().getAnnotation(RequestMapping.class));
    String postfix = getPath(method.getAnnotation(RequestMapping.class));
    return create(prefix, postfix);
  }

  /**
   * Creates a path from a request.
   *
   * @param request - HTTP request which path will be extracted.
   * @return path of a request.
   */
  public String create(final HttpServletRequest request) {
    return create(
        request.getContextPath(),
        request.getServletPath(),
        Optional.ofNullable(request.getPathInfo()).orElse("")
    );
  }

  /**
   * Finds parameter names (in the form :paramName) of a path.
   *
   * @param path - path which param names to extract.
   * @return list of found param names.
   */
  public List<String> getParamNames(final String path) {
    List<String> paramNames = new ArrayList<>();

    String p = path;
    while(p != null && !p.isEmpty()) {
      int i = p.indexOf(":");
      if (i == -1) {
        break;
      }

      p = p.substring(i + 1);
      i = p.indexOf("/");
      if (i == -1) {
        paramNames.add(p);
        p = null;
      } else {
        paramNames.add(p.substring(0, i));
        p = p.substring(i + 1);
      }
    }

    return paramNames;
  }

  private String getPath(final RequestMapping classRequestMapping) {
    String path = "";

    if (classRequestMapping != null) {
      String[] value = classRequestMapping.value();
      if (value.length == 0) {
        value = classRequestMapping.path();
      }
      if (value.length > 1) {
        throw new IllegalStateException("Multiple paths are not supported");
      }
      if (value.length == 1) {
        path = Optional.ofNullable(value[0]).orElse("").trim();
      }
    }
    return path;
  }
}
