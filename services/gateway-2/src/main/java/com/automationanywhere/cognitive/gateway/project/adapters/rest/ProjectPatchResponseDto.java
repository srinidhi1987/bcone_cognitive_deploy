package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProjectPatchResponseDto {
  public final Integer versionId;

  public ProjectPatchResponseDto(
      @JsonProperty("versionId") final Integer versionId
  ) {
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectPatchResponseDto projectPatchResponseDto
          = (ProjectPatchResponseDto) o;

      result = Objects.equals(versionId, projectPatchResponseDto.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(versionId);
  }
}
