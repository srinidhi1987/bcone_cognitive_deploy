package com.automationanywhere.cognitive.gateway.application.microservices.application;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ApplicationMicroservice {

  private final ApplicationDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public ApplicationMicroservice(
      final ApplicationDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Handshake> register(final Registration registration) {
    Builder builder = Builder
        .post(cfg.applicationServiceURL())
        .request(cfg.handshake())
        .accept(Types.JSON)
        .content(mapper.mapRegistration(registration), Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<HandshakeData>() {
        }
    ).thenApply(mapper::mapHandshake);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> acknowledge(final Acknowledgement acknowledgement) {
    Builder builder = Builder
        .post(cfg.applicationServiceURL())
        .request(cfg.acknowledge())
        .content(mapper.mapAcknowledgement(acknowledgement), Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<String>() {
        }
    ).thenApply(v -> null);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Configuration> retrieveConfiguration() {
    Builder builder = Builder
        .get(cfg.applicationServiceURL())
        .accept(Types.JSON)
        .request(cfg.configuration());

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<ConfigurationData>() {
        }
    ).thenApply(mapper::mapConfiguration);
  }
}