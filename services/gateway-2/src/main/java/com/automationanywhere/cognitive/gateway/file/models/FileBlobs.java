package com.automationanywhere.cognitive.gateway.file.models;

import java.util.Objects;

public class FileBlobs {

  public final String fileId;
  public final String fileBlobs;

  public FileBlobs(
      final String fileId,
      final String fileBlobs
  ) {
    this.fileId = fileId;
    this.fileBlobs = fileBlobs;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileBlobs fileBlobs = (FileBlobs) o;

      result = Objects.equals(fileId, fileBlobs.fileId)
          && Objects.equals(this.fileBlobs, fileBlobs.fileBlobs);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
        ^ Objects.hashCode(fileBlobs);
  }
}

