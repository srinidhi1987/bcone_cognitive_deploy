package com.automationanywhere.cognitive.gateway.validator.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class InvalidFile {

  public final Id fileId;
  public final String reasonId;
  public final String reasonText;

  public InvalidFile(
      final Id fileId,
      final String reasonId,
      final String reasonText
  ) {
    this.fileId = fileId;
    this.reasonId = reasonId;
    this.reasonText = reasonText;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      InvalidFile that = (InvalidFile) o;

      result = Objects.equals(fileId, that.fileId)
          && Objects.equals(reasonId, that.reasonId)
          && Objects.equals(reasonText, that.reasonText);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
        ^ Objects.hashCode(reasonId)
        ^ Objects.hashCode(reasonText);
  }
}
