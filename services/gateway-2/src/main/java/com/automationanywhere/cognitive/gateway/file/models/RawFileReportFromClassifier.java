package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.Objects;

public class RawFileReportFromClassifier {

  public final Id documentId;
  public final List<RawFieldDetailFromClasssifier> fieldDetails;

  public RawFileReportFromClassifier(
      final Id documentId,
      final List<RawFieldDetailFromClasssifier> fieldDetails
  ) {
    this.documentId = documentId;
    this.fieldDetails = fieldDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      RawFileReportFromClassifier that = (RawFileReportFromClassifier) o;

      result = Objects.equals(documentId, that.documentId)
          && Objects.equals(fieldDetails, that.fieldDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(documentId)
        ^ Objects.hashCode(fieldDetails);
  }
}
