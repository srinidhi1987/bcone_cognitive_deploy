package com.automationanywhere.cognitive.gateway.report.microservice.report;

import static com.automationanywhere.cognitive.restclient.RequestDetails.Builder.get;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.DashboardProdProjectTotalsData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ProjectTotalsData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ReportingProjectData;
import com.automationanywhere.cognitive.gateway.report.model.DashboardProdProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.ReportingProject;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

@Repository
public class ReportMicroservice {

  private final ReportDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration appConfig;

  @Autowired
  public ReportMicroservice(
      ReportDataMapper mapper,
      RestClient restClient,
      ApplicationConfiguration appConfig
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.appConfig = appConfig;
  }

  public CompletableFuture<DashboardProdProjectTotals> getDashboardReportTotals(String orgId) {
    return restClient.send(
        get(appConfig.reportServiceURL())
            .request(appConfig.getDashboardReportTotals())
            .param(orgId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<DashboardProdProjectTotalsData>() {
        }
    ).thenApply(mapper::getDashboardProdProjectTotals);
  }

  public CompletableFuture<ProjectTotals> getReportTotals(String orgId, String projectId) {
    return restClient.send(
        get(appConfig.reportServiceURL())
            .request(appConfig.getReportTotals())
            .param(orgId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ProjectTotalsData>() {
        }
    ).thenApply(mapper::getProjectTotals);
  }

  public CompletableFuture<List<ReportingProject>> getReports(String orgId) {
    return restClient.send(
        get(appConfig.reportServiceURL())
            .request(appConfig.getReports())
            .param(orgId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<ReportingProjectData>>() {
        }
    ).thenApply(mapper::getReportingProject);
  }
}
