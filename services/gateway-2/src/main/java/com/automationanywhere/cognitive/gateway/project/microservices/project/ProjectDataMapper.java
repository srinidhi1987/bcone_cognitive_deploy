package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.Category;
import com.automationanywhere.cognitive.gateway.project.models.CustomFieldDetail;
import com.automationanywhere.cognitive.gateway.project.models.FieldDetails;
import com.automationanywhere.cognitive.gateway.project.models.File;
import com.automationanywhere.cognitive.gateway.project.models.FileCount;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.OCREngineDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.gateway.project.models.VisionBot;
import java.util.ArrayList;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ProjectDataMapper {

  public ProjectDetails mapProjectDetails(final ProjectDetailsData p) {
    return p == null ? null : new ProjectDetails(
        p.id, p.name, p.description, p.organizationId, p.projectTypeId, p.projectType,
        p.confidenceThreshold,
        p.numberOfFiles, p.numberOfCategories, p.unprocessedFileCount, p.primaryLanguage,
        p.accuracyPercentage, p.visionBotCount, p.currentTrainedPercentage,
        p.categories == null ? null
            : p.categories.stream().map(this::mapCategory).collect(Collectors.toList()),
        mapFieldDetails(p.fields), p.projectState, p.environment, p.updatedAt, p.createdAt,
        p.ocrEngineDetails == null ? null
            : p.ocrEngineDetails.stream().map(this::mapOCREngineDetail).collect(Collectors.toList()),
        p.versionId
    );
  }

  public NewProjectDetailsData mapNewProjectDetails(final NewProjectDetails newProject) {
    return newProject == null ? null : new NewProjectDetailsData(
        newProject.name, newProject.description, newProject.organizationId,
        newProject.projectTypeId,
        newProject.projectType, newProject.primaryLanguage,
        mapFieldDetails(newProject.fields), newProject.projectState, newProject.environment
    );
  }

  public ProjectDetailChanges mapProjectDetailChanges(
      final ProjectDetailChangesData changes
  ) {
    return changes == null ? null : new ProjectDetailChanges(
        changes.id, changes.name, changes.description, changes.organizationId,
        changes.projectTypeId,
        changes.projectType, changes.primaryLanguage,
        mapFieldDetails(changes.fields), changes.projectState, changes.environment,
        changes.ocrEngineDetails == null ? null
            : changes.ocrEngineDetails.stream().map(this::mapOCREngineDetail)
                .collect(Collectors.toList()),
        changes.versionId
    );
  }

  public ProjectDetailChangesData mapProjectDetailChanges(
      final ProjectDetailChanges changes
  ) {
    return changes == null ? null : new ProjectDetailChangesData(
        changes.id, changes.name, changes.description, changes.organizationId,
        changes.projectTypeId,
        changes.projectType, changes.primaryLanguage,
        mapFieldDetails(changes.fields), changes.projectState, changes.environment,
        changes.ocrEngineDetails == null ? null
            : changes.ocrEngineDetails.stream().map(this::mapOCREngineDetail)
                .collect(Collectors.toList()),
        changes.versionId
    );
  }

  public ProjectPatchResponse mapProjectPatchResponse(final ProjectPatchResponseData projectPatchResponseData) {
    return projectPatchResponseData == null ? null : new ProjectPatchResponse(
        projectPatchResponseData.versionId
    );
  }

  private FieldDetails mapFieldDetails(final FieldDetailsData fields) {
    return fields == null ? null : new FieldDetails(
        fields.standard,
        fields.custom == null ? null
            : fields.custom.stream().map(this::mapCustomFieldDetail).collect(Collectors.toList())
    );
  }

  private FieldDetailsData mapFieldDetails(final FieldDetails fields) {
    return fields == null ? null : new FieldDetailsData(
        fields.standard,
        fields.custom == null ? null
            : fields.custom.stream().map(this::mapCustomFieldDetail).collect(Collectors.toList())
    );
  }

  private CustomFieldDetail mapCustomFieldDetail(final CustomFieldDetailData customFieldDetail) {
    return customFieldDetail == null ? null : new CustomFieldDetail(
        customFieldDetail.id, customFieldDetail.name, customFieldDetail.type
    );
  }

  private CustomFieldDetailData mapCustomFieldDetail(final CustomFieldDetail customFieldDetail) {
    return customFieldDetail == null ? null : new CustomFieldDetailData(
        customFieldDetail.id, customFieldDetail.name, customFieldDetail.type
    );
  }

  private OCREngineDetail mapOCREngineDetail(final OCREngineDetailData ocrEngineDetail) {
    return ocrEngineDetail == null ? null
        : new OCREngineDetail(ocrEngineDetail.id, ocrEngineDetail.name, ocrEngineDetail.engineType);
  }

  private OCREngineDetailData mapOCREngineDetail(final OCREngineDetail ocrEngineDetail) {
    return ocrEngineDetail == null ? null
        : new OCREngineDetailData(ocrEngineDetail.id, ocrEngineDetail.name,
            ocrEngineDetail.engineType);
  }

  private Category mapCategory(final CategoryData category) {
    return category == null ? null : new Category(
        category.id, category.name,
        mapVisionBot(category.visionBot),
        category.fileCount,
        category.files == null ? null :
            category.files.stream().map(this::mapFile).collect(Collectors.toList()),
        mapFileCount(category.productionFileDetails),
        mapFileCount(category.stagingFileDetails)
    );
  }

  private VisionBot mapVisionBot(final VisionBotData visionBot) {
    return visionBot == null ? null : new VisionBot(
        visionBot.id, visionBot.name, visionBot.currentState
    );
  }

  private FileCount mapFileCount(final FileCountData fileCount) {
    return fileCount == null ? null : new FileCount(
        fileCount.totalCount, fileCount.unprocessedCount
    );
  }

  private File mapFile(final FileData file) {
    return file == null ? null : new File(
        file.id, file.name, file.location, file.format, file.processed
    );
  }

  public ArchiveDetails mapArchiveDetails(final ArchiveDetailsData archiveDetails) {
    return archiveDetails == null ? null : new ArchiveDetails(
        archiveDetails.archiveName
    );
  }

  public ArchiveDetailsData mapArchiveDetails(final ArchiveDetails archiveDetails) {
    return archiveDetails == null ? null : new ArchiveDetailsData(
        archiveDetails.archiveName
    );
  }

  public ProjectImportRequestData mapProjectImportRequest(
      final ProjectImportRequest projectImportRequest
  ) {
    return projectImportRequest == null ? null : new ProjectImportRequestData(
        mapArchiveDetails(projectImportRequest.archive),
        projectImportRequest.userId,
        projectImportRequest.option
    );
  }

  public TaskStatus mapTaskStatus(final TaskStatusData taskStatus) {
    return taskStatus == null ? null : new TaskStatus(
        taskStatus.taskId, taskStatus.taskType, taskStatus.status, taskStatus.description
    );
  }

  public Task mapTask(final TaskData taskData) {
    return taskData == null ? null : new Task(
        taskData.taskId, taskData.taskType,  taskData.status,
        taskData.description, taskData.taskOptions, taskData.userId,
        taskData.startTime, taskData.endTime
    );
  }

  public ProjectExportRequestData mapProjectExportRequest(
      final ProjectExportRequest projectExportRequest
  ) {
    return projectExportRequest == null ? null : new ProjectExportRequestData(
        projectExportRequest.archiveName,
        projectExportRequest.projectExportDetails == null
            ? null
            : new ArrayList<>(projectExportRequest.projectExportDetails),
        projectExportRequest.userId
    );
  }


  public ProjectPatchDetailData mapProjectPatchDetail(final ProjectPatchDetail projectPatchDetail) {
    return projectPatchDetail == null
        ? null
        : new ProjectPatchDetailData(
            projectPatchDetail.op,
            projectPatchDetail.path,
            projectPatchDetail.value,
            projectPatchDetail.versionId
        );
  }
}
