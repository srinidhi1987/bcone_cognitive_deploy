package com.automationanywhere.cognitive.gateway.auth.microservices.validator;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component("AuthValidatorMicroservice")
public class AuthValidatorMicroservice {

  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public AuthValidatorMicroservice(
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.restClient = restClient;
    this.cfg = cfg;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<?> unlockDocuments(String username) {
    return restClient.send(
        RequestDetails.Builder.post(cfg.validatorURL())
            .request(cfg.unlockDocuments())
            .setHeader("username", username)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    ).thenApply(d -> null); // Return null instead of what the REST client returns.
  }
}
