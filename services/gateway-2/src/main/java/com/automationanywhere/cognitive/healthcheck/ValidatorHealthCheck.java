package com.automationanywhere.cognitive.healthcheck;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.common.healthcheck.commands.MicroserviceTextHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Validator Service health check command.
 */
@Component
@SuppressWarnings("unused")
public class ValidatorHealthCheck extends MicroserviceTextHealthCheckCommand {

  @Autowired
  public ValidatorHealthCheck(
      final ApplicationConfiguration config,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory,
      final AsyncRestTemplate restTemplate
  ) {
    super(
        "Validator",
        Builder.get(config.validatorURL()).build().url,
        buildInfoFactory,
        appInfoFactory,
        restTemplate
    );
  }
}
