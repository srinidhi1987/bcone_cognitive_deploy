package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class VisionBotMicroservice {

  private static final String USERNAME_HEADER = "username";

  private final VisionBotDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public VisionBotMicroservice(
      final VisionBotDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  RestClient getRestClient() {
    return restClient;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<VisionBot> getVisionBotForEdit(
      final Id organizationId, final Id projectId, final Id id, final String userName
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getVisionBotForEdit())
            .param(organizationId)
            .param(projectId)
            .param(id)
            .setHeader(USERNAME_HEADER, userName)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<VisionBotData>() {
        }
    ).thenApply(mapper::mapVisionBot);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Id> unlock(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id userId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.unlock())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .setHeader(USERNAME_HEADER, userId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<Id>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<VisionBot> getVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getVisionBot())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<VisionBotData>() {
        }
    ).thenApply(mapper::mapVisionBot);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<VisionBot> getVisionBotById(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getVisionBotById())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<VisionBotData>() {
        }
    ).thenApply(mapper::mapVisionBot);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Boolean> patchVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id userId,
      final JsonPatchDocument jsonPatchDocument
  ) {
    return getRestClient().send(
        RequestDetails.Builder.patch(cfg.visionBotURL())
            .request(cfg.patchVisionBot())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .setHeader(USERNAME_HEADER, userId)
            .content(mapper.mapJsonPatchDocument(jsonPatchDocument), Types.JSON_PATCH)
            .accept(Types.JSON_PATCH)
            .build(),
        new ParameterizedTypeReference<StandardResponse<Boolean>>() {
        }
    ).thenApply(r -> r.success);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<VisionBotDetails>> getVisionBotMetadata(
      final Id organizationId,
      final Id projectId,
      final Id categoryId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getVisionBotMetadata())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    ).thenApply(l -> mapper.mapList(l, mapper::mapVisionBotDetails));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<VisionBotDetails>> getProjectVisionBotMetadata(
      final Id organizationId,
      final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getProjectVisionBotMetadata())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    ).thenApply(l -> mapper.mapList(l, mapper::mapVisionBotDetails));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<VisionBotDetails>> getProjectVisionBot(
      final Id organizationId,
      final Id projectId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getProjectVisionBot())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    ).thenApply(l -> mapper.mapList(l, mapper::mapVisionBotDetails));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<VisionBotDetails>> getOrganizationVisionBotMetadata(
      final Id organizationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getOrganizationVisionBotMetadata())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    ).thenApply(l -> mapper.mapList(l, mapper::mapVisionBotDetails));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<Map<String, Object>>> getOrganizationVisionBot(
      final Id organizationId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getOrganizationVisionBot())
            .param(organizationId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<Map<String, Object>>>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<VisionBotDetails>> getOrganizationVisionBotById(
      final Id organizationId, final Id visionBotId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getOrganizationVisionBotById())
            .param(organizationId)
            .param(visionBotId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<VisionBotDetailsData>>() {
        }
    ).thenApply(l -> mapper.mapList(l, mapper::mapVisionBotDetails));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<VisionBot> updateVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final VisionBot visionBot
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.updateVisionBot())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .content(mapper.mapVisionBot(visionBot), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<VisionBotData>() {
        }
    ).thenApply(mapper::mapVisionBot);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<VisionBot> updateVisionBotById(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Map<String, Object> visionBot
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.updateVisionBotById())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .content(visionBot, Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<VisionBotData>() {
        }
    ).thenApply(mapper::mapVisionBot);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<StandardResponse<String>> getValueField(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId,
      final Id fieldDefId,
      final Id documentId,
      final FieldValueDefinition fieldValueDefinition
  ) {
    return getRestClient().send(
        Builder.post(cfg.visionBotURL())
            .request(cfg.getValueField())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(layoutId)
            .param(fieldDefId)
            .param(documentId)
            .content(mapper.mapFieldValueDefinition(fieldValueDefinition), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    );
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> runVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final String environment
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.runVisionBot())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(environment)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> getValidationData(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getValidationData())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> getVisionBotByLayout(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getVisionBotByLayout())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(layoutId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> getVisionBotByLayoutAndPage(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId,
      final int pageIndex
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getVisionBotByLayoutAndPage())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(layoutId)
            .param(pageIndex)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> getPageIndex(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id documentId,
      final String index
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getPageImage())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(documentId)
            .param(index)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Summary> getSummary(final Id organizationId, final Id projectId) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getSummary())
            .param(organizationId)
            .param(projectId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<SummaryData>() {
        }
    ).thenApply(mapper::mapSummary);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<TestSet> getTestSet(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.getTestSet())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(layoutId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<TestSetData>() {
        }
    ).thenApply(mapper::mapTestSet);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<TestSet> updateTestSet(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId,
      final TestSet testSet
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.updateTestSet())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(layoutId)
            .content(mapper.mapTestSet(testSet), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<TestSetData>() {
        }
    ).thenApply(mapper::mapTestSet);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> deleteTestSet(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.delete(cfg.visionBotURL())
            .request(cfg.deleteTestSet())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(layoutId)
            .build(),
        new ParameterizedTypeReference<Void>() {
        }
    ).thenApply(r -> null);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> setState(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final VisionBotState state
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.setState())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .content(mapper.mapVisionBotState(state), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<VisionBotResponse> setVisionBotDetails(
      final Id organizationId,
      final Id projectId,
      final VisionBotElements visionBotElements
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.getMutipleState())
            .param(organizationId)
            .param(projectId)
            .content(mapper.mapVisionBotElements(visionBotElements), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<VisionBotResponseData>() {
        }
    ).thenApply(r -> mapper.mapVisionBotResponse(r));
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> exportToCSV(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Map<String, Object> vBotDetails
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.exportToCSV())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .content(vBotDetails, Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> updateValidationDetails(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final ValidationDetails validationDetails
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.updateValidationData())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .content(mapper.mapValidationDetails(validationDetails), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(r -> r.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> updateAutoMappingData(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final String data
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.updateAutoMappingData())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .content(data, Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(r -> r.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<ProcessedDocumentDetails> updateValidationDocument(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id visionBotDetailsId,
      final Id documentId,
      final ProcessedDocumentDetails processedDocumentDetails
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.updateValidationDocument())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(visionBotDetailsId)
            .param(documentId)
            .content(mapper.mapProcessedDocumentDetails(processedDocumentDetails), Types.JSON)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<ProcessedDocumentDetailsData>() {
        }
    ).thenApply(mapper::mapProcessedDocumentDetails);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> generateVisionBotData(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id documentId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.generateVisionBotData())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(documentId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(r -> r.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<String> generateVisionBotDataPaginated(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id documentId,
      final int pageIndex
  ) {
    return getRestClient().send(
        RequestDetails.Builder.get(cfg.visionBotURL())
            .request(cfg.generateVisionBotDataPaginated())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .param(documentId)
            .param(pageIndex)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<String>>() {
        }
    ).thenApply(sr -> sr.data);
  }

  @Async("microserviceExecutor")
  public CompletableFuture<Void> keepAliveLockedVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId
  ) {
    return getRestClient().send(
        RequestDetails.Builder.post(cfg.visionBotURL())
            .request(cfg.keepAliveLockedVisionBot())
            .param(organizationId)
            .param(projectId)
            .param(categoryId)
            .param(visionBotId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<StandardResponse<byte[]>>() {
        }
    ).thenApply(r -> null);
  }
}
