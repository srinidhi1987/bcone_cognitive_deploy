package com.automationanywhere.cognitive.gateway.alias.model;

import com.automationanywhere.cognitive.id.Id;

public class Language {

  public final Id id;
  public final String name;
  public final String code;

  public Language(
      final Id id,
      final String name,
      final String code
  ) {
    this.id = id;
    this.name = name;
    this.code = code;
  }
}
