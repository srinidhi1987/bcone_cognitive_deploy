package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ArchiveDetailsData {
  public final String archiveName;

  public ArchiveDetailsData(
      @JsonProperty("archiveName") final String archiveName
  ) {
    this.archiveName = archiveName;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ArchiveDetailsData that = (ArchiveDetailsData) o;

      result = Objects.equals(archiveName, that.archiveName);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archiveName) ^ 32;
  }
}
