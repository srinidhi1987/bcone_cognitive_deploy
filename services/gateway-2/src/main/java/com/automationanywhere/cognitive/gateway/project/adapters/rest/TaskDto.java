package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.OffsetDateTime;
import java.util.Objects;

public class TaskDto {

  public final Id taskId;
  public final String taskType;
  public final String status;
  public final String description;
  public final String userId;
  public final OffsetDateTime startTime;
  public final OffsetDateTime endTime;
  public final String taskOptions;

  public TaskDto(
      @JsonProperty("taskId") final Id taskId,
      @JsonProperty("taskType") final String taskType,
      @JsonProperty("status") final String status,
      @JsonProperty("description") final String description,
      @JsonProperty("taskOptions") final String taskOptions,
      @JsonProperty("userId") final String userId,
      @JsonProperty("startTime") final OffsetDateTime startTime,
      @JsonProperty("endTime") final OffsetDateTime endTime
  ) {
    this.taskId = taskId;
    this.taskType = taskType;
    this.status = status;
    this.description = description;
    this.taskOptions = taskOptions;
    this.userId = userId;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      TaskDto taskData = (TaskDto) o;

      result = Objects.equals(taskId, taskData.taskId)
          && Objects.equals(taskType, taskData.taskType)
          && Objects.equals(status, taskData.status)
          && Objects.equals(description, taskData.description)
          && Objects.equals(taskOptions, taskData.taskOptions)
          && Objects.equals(userId, taskData.userId)
          && Objects.equals(startTime, taskData.startTime)
          && Objects.equals(endTime, taskData.endTime);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(taskId)
        ^ Objects.hashCode(taskType)
        ^ Objects.hashCode(status)
        ^ Objects.hashCode(description)
        ^ Objects.hashCode(taskOptions)
        ^ Objects.hashCode(userId)
        ^ Objects.hashCode(startTime)
        ^ Objects.hashCode(endTime);
  }
}

