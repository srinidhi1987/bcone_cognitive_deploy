package com.automationanywhere.cognitive.gateway.report.model;

import java.util.List;

public class Validation {

  private List<FieldValidation> fields;
  private List<GroupTimeSpent> categories;

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  public List<FieldValidation> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the fields to be set
   */
  public void setFields(
      List<FieldValidation> fields) {
    this.fields = fields;
  }

  /**
   * Returns the categories.
   *
   * @return the value of categories
   */
  public List<GroupTimeSpent> getCategories() {
    return categories;
  }

  /**
   * Sets the categories.
   *
   * @param categories the categories to be set
   */
  public void setCategories(
      List<GroupTimeSpent> categories) {
    this.categories = categories;
  }
}
