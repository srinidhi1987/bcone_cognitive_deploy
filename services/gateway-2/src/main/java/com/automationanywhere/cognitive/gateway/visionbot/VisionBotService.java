package com.automationanywhere.cognitive.gateway.visionbot;

import com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot.VisionBotMicroservice;
import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class VisionBotService {
  private final VisionBotMicroservice visionBotMicroservice;

  @Autowired
  public VisionBotService(final VisionBotMicroservice visionBotMicroservice) {
    this.visionBotMicroservice = visionBotMicroservice;
  }

  @Async
  public CompletableFuture<VisionBot> getVisionBotForEdit(
      final Id organizationId,
      final Id projectId,
      final Id id,
      final String userName
  ) {
    return visionBotMicroservice.getVisionBotForEdit(organizationId, projectId, id, userName);
  }

  @Async
  public CompletableFuture<Id> unlock(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id userId
  ) {
    return visionBotMicroservice.unlock(organizationId, projectId, categoryId, visionBotId, userId);
  }

  @Async
  public CompletableFuture<VisionBot> getVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId
  ) {
    return visionBotMicroservice.getVisionBot(organizationId, projectId, categoryId);
  }

  @Async
  public CompletableFuture<VisionBot> getVisionBotById(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId
  ) {
    return visionBotMicroservice.getVisionBotById(organizationId, projectId, categoryId, visionBotId);
  }

  @Async
  public CompletableFuture<Boolean> patchVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id userId,
      final JsonPatchDocument jsonPatchDocument
  ) {
    return visionBotMicroservice.patchVisionBot(
        organizationId, projectId, categoryId, visionBotId,userId, jsonPatchDocument
    );
  }

  @Async
  public CompletableFuture<List<VisionBotDetails>> getVisionBotMetadata(
      final Id organizationId,
      final Id projectId,
      final Id categoryId
  ) {
    return visionBotMicroservice.getVisionBotMetadata(organizationId, projectId, categoryId);
  }

  @Async
  public CompletableFuture<List<VisionBotDetails>> getProjectVisionBot(
      final Id organizationId,
      final Id projectId
  ) {
    return visionBotMicroservice.getProjectVisionBot(organizationId, projectId);
  }

  @Async
  public CompletableFuture<List<VisionBotDetails>> getProjectVisionBotMetadata(
      final Id organizationId,
      final Id projectId
  ) {
    return visionBotMicroservice.getProjectVisionBotMetadata(organizationId, projectId);
  }

  @Async
  public CompletableFuture<List<Map<String, Object>>> getOrganizationVisionBot(
      final Id organizationId
  ) {
    return visionBotMicroservice.getOrganizationVisionBot(organizationId);
  }

  @Async
  public CompletableFuture<List<VisionBotDetails>> getOrganizationVisionBotById(
      final Id organizationId, final Id visionBotId
  ) {
    return visionBotMicroservice.getOrganizationVisionBotById(organizationId, visionBotId);
  }

  @Async
  public CompletableFuture<List<VisionBotDetails>> getOrganizationVisionBotMetadata(
      final Id organizationId
  ) {
    return visionBotMicroservice.getOrganizationVisionBotMetadata(organizationId);
  }

  @Async
  public CompletableFuture<VisionBot> updateVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final VisionBot visionBot
  ) {
    return visionBotMicroservice.updateVisionBot(organizationId, projectId, categoryId, visionBot);
  }

  @Async
  public CompletableFuture<VisionBot> updateVisionBotById(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Map<String, Object> visionBot
  ) {
    return visionBotMicroservice.updateVisionBotById(
        organizationId, projectId, categoryId, visionBotId, visionBot
    );
  }

  @Async
  public CompletableFuture<StandardResponse<String>> getValueField(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId,
      final Id fieldDefId,
      final Id documentId,
      final FieldValueDefinition fieldValueDefinition
  ) {
    return visionBotMicroservice.getValueField(
        organizationId, projectId, categoryId, visionBotId,
        layoutId, fieldDefId, documentId, fieldValueDefinition
    );
  }

  @Async
  public CompletableFuture<String> runVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final String environment) {
    return visionBotMicroservice.runVisionBot(
        organizationId, projectId, categoryId, visionBotId, environment
    );
  }

  @Async
  public CompletableFuture<String> getValidationData(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId
  ) {
    return visionBotMicroservice
        .getValidationData(organizationId, projectId, categoryId, visionBotId);
  }

  @Async
  public CompletableFuture<String> getVisionBotByLayout(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId
  ) {
    return visionBotMicroservice.getVisionBotByLayout(
        organizationId, projectId, categoryId, visionBotId, layoutId
    );
  }

  @Async
  public CompletableFuture<String> getVisionBotByLayoutAndPage(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId,
      final int pageIndex
  ) {
    return visionBotMicroservice.getVisionBotByLayoutAndPage(
        organizationId, projectId, categoryId, visionBotId, layoutId, pageIndex
    );
  }

  @Async
  public CompletableFuture<String> getPageIndex(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id documentId,
      final String index
  ) {
    return visionBotMicroservice.getPageIndex(
        organizationId, projectId, categoryId, documentId, index
    );
  }

  @Async
  public CompletableFuture<Summary> getSummary(final Id organizationId, final Id projectId) {
    return visionBotMicroservice.getSummary(organizationId, projectId);
  }

  @Async
  public CompletableFuture<TestSet> getTestSet(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId
  ) {
    return visionBotMicroservice.getTestSet(
        organizationId, projectId, categoryId, visionBotId, layoutId
    );
  }

  @Async
  public CompletableFuture<TestSet> updateTestSet(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId,
      final TestSet testSet
  ) {
    return visionBotMicroservice.updateTestSet(
        organizationId, projectId, categoryId, visionBotId, layoutId, testSet
    );
  }

  @Async
  public CompletableFuture<Void> deleteTestSet(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id layoutId
  ) {
    return visionBotMicroservice.deleteTestSet(
        organizationId, projectId, categoryId, visionBotId, layoutId
    );
  }

  @Async
  public CompletableFuture<String> setState(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final VisionBotState state
  ) {
    return visionBotMicroservice.setState(
        organizationId, projectId, categoryId, visionBotId, state
    );
  }

  @Async
  public CompletableFuture<VisionBotResponse> setVisionBotDetails(
      final Id organizationId,
      final Id projectId,
      final VisionBotElements visionBotElements
  ) {
    return visionBotMicroservice.setVisionBotDetails(
        organizationId, projectId, visionBotElements
    );
  }

  @Async
  public CompletableFuture<String> exportToCSV(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Map<String, Object> vBotDetails
  ) {
    return visionBotMicroservice.exportToCSV(
        organizationId, projectId, categoryId, vBotDetails
    );
  }

  @Async
  public CompletableFuture<String> updateValidationDetails(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final ValidationDetails validationDetails
  ) {
    return visionBotMicroservice.updateValidationDetails(
        organizationId, projectId, categoryId, visionBotId, validationDetails
    );
  }

  @Async
  public CompletableFuture<String> updateAutoMappingData(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final String data
  ) {
    return visionBotMicroservice.updateAutoMappingData(
        organizationId, projectId, categoryId, visionBotId, data
    );
  }

  @Async
  public CompletableFuture<ProcessedDocumentDetails> updateValidationDocument(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id visionBotDetailsId,
      final Id documentId,
      final ProcessedDocumentDetails processedDocumentDetails
  ) {
    return visionBotMicroservice.updateValidationDocument(
      organizationId, projectId, categoryId, visionBotId, visionBotDetailsId, documentId,
        processedDocumentDetails
    );
  }

  @Async
  public CompletableFuture<String> generateVisionBotData(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id documentId
  ) {
    return visionBotMicroservice.generateVisionBotData(
      organizationId, projectId, categoryId, visionBotId, documentId
    );
  }

  @Async
  public CompletableFuture<String> generateVisionBotDataPaginated(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId,
      final Id documentId,
      final int pageIndex
  ) {
    return visionBotMicroservice.generateVisionBotDataPaginated(
        organizationId, projectId, categoryId, visionBotId, documentId, pageIndex
    );
  }

  @Async
  public CompletableFuture<Void> keepAliveLockedVisionBot(
      final Id organizationId,
      final Id projectId,
      final Id categoryId,
      final Id visionBotId
  ) {
    return visionBotMicroservice.keepAliveLockedVisionBot(
        organizationId, projectId, categoryId, visionBotId
    );
  }
}
