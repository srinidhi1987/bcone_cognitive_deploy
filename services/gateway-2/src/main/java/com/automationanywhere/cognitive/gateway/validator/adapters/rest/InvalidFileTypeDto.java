package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class InvalidFileTypeDto {

  public final Id id;
  public final String reason;

  public InvalidFileTypeDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("reason") final String reason
  ) {
    this.id = id;
    this.reason = reason;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      InvalidFileTypeDto that = (InvalidFileTypeDto) o;

      result = Objects.equals(id, that.id)
          && Objects.equals(reason, that.reason);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(reason);
  }
}
