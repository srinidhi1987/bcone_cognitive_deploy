package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileCountDto {

  public final Integer totalCount;
  public final Integer unprocessedCount;

  public FileCountDto(
      @JsonProperty("totalCount") final Integer totalCount,
      @JsonProperty("unprocessedCount") final Integer unprocessedCount
  ) {
    this.totalCount = totalCount;
    this.unprocessedCount = unprocessedCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileCountDto fileCount = (FileCountDto) o;

      result = Objects.equals(totalCount, fileCount.totalCount)
          && Objects.equals(unprocessedCount, fileCount.unprocessedCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalCount)
        ^ Objects.hashCode(unprocessedCount);
  }
}

