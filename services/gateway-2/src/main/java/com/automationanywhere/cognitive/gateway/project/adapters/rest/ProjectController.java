package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import static com.automationanywhere.cognitive.validation.Schemas.ARCHIVE_DETAILS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.NEW_PROJECT_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_DETAILS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_DETAIL_CHANGES;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_DETAIL_CHANGE_SUMMARY;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_EXPORT_REQUEST;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_IMPORT_REQUEST;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_PATCH_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.PROJECT_PATCH_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.STANDARD_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.TASK_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.TASK_STATUS;
import static com.automationanywhere.cognitive.validation.Schemas.TASK_STATUS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.V1;

import com.automationanywhere.cognitive.gateway.project.ProjectService;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/organizations")
public class ProjectController {

  private final ProjectService projectService;
  private final ProjectDtoMapper mapper;

  @Autowired
  public ProjectController(
      final ProjectService projectService,
      final ProjectDtoMapper mapper
  ) {
    this.projectService = projectService;
    this.mapper = mapper;
  }

  @RequestMapping(
      path = "/{organizationId}/projects",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PROJECT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> getOrganizationProjects(
      @PathVariable("organizationId") final Id organizationId,
      @RequestParam(name = "excludeFields", required = false) final String excludeFields
  ) {
    return projectService.getOrganizationProjects(
        organizationId,
        extractExcludeFields(excludeFields)
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapProjectDetails).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/metadata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PROJECT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<ProjectDetailsDto>>> getProjectMetadataList(
      @PathVariable("organizationId") final Id organizationId
  ) {
    return projectService.getOrganizationProjectMetadataList(organizationId)
        .thenApply(list -> new StandardResponse<>(
            list.stream().map(mapper::mapProjectDetails).collect(Collectors.toList())
        ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/{projectId}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PROJECT_DETAILS
  )
  public CompletableFuture<StandardResponse<ProjectDetailsDto>> getOrganizationProject(
      @PathVariable("organizationId") final Id organizationId,
      @PathVariable("projectId") final Id projectId,
      @RequestParam(name = "excludeFields", required = false) final String excludeFields
  ) {
    return projectService.getOrganizationProject(
        organizationId, projectId, excludeFields
    ).thenApply(p -> new StandardResponse<>(mapper.mapProjectDetails(p)));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/{projectId}/metadata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PROJECT_DETAILS
  )
  public CompletableFuture<StandardResponse<ProjectDetailChangesDto>> getProjectMetadata(
      @PathVariable("organizationId") final Id organizationId,
      @PathVariable("projectId") final Id projectId
  ) {
    return projectService.getOrganizationProjectMetadata(
        organizationId, projectId
    ).thenApply(p -> new StandardResponse<>(
        mapper.mapProjectDetailChanges(p)
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = NEW_PROJECT_DETAILS,
      output = PROJECT_DETAIL_CHANGE_SUMMARY
  )
  public CompletableFuture<StandardResponse<ProjectDetailChangesDto>> createOrganizationProject(
      @PathVariable("organizationId") final Id organizationId,
      @RequestBody final NewProjectDetailsDto projectDetails
  ) {
    return projectService.createOrganizationProject(
        organizationId, mapper.mapNewProjectDetails(projectDetails)
    ).thenApply(p -> new StandardResponse<>(
        mapper.mapProjectDetailChanges(p)
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/{projectId}",
      method = RequestMethod.PUT,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = PROJECT_DETAIL_CHANGES,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Void>> updateOrganizationProject(
      @PathVariable("organizationId") final Id organizationId,
      @PathVariable("projectId") final Id projectId,
      @RequestBody final ProjectDetailChangesDto projectDetails
  ) {
    return projectService.updateOrganizationProject(
        organizationId, projectId, mapper.mapProjectDetailChanges(projectDetails)
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{organizationId}/projects/{projectId}",
      method = RequestMethod.PATCH,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE}
  )
  @SchemaValidation(
      input = PROJECT_PATCH_DETAILS,
      output = PROJECT_DETAILS
  )
  public CompletableFuture<StandardResponse<ProjectDetailsDto>> patchOrganizationProject(
      @PathVariable("organizationId") final Id organizationId,
      @PathVariable("projectId") final Id projectId,
      @RequestBody final List<ProjectPatchDetailDto> projectPatchDetails
  ) {
    return projectService.patchOrganizationProject(
        organizationId, projectId,
        projectPatchDetails.stream().map(mapper::mapProjectPatchDetail).collect(Collectors.toList())
    ).thenApply(p -> new StandardResponse<>(
        mapper.mapProjectDetails(p)
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/{projectId}",
      method = RequestMethod.DELETE,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Void>> deleteOrganizationProject(
      @PathVariable("organizationId") final Id organizationId,
      @PathVariable("projectId") final Id projectId,
      @RequestHeader(name = "updatedBy", required = false) final String userName
  ) {
    return projectService.deleteOrganizationProject(
        organizationId, projectId, userName
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{organizationId}/projects/import/archivedetails",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = ARCHIVE_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<ArchiveDetailsDto>>> getArchiveDetailList(
      @PathVariable("organizationId") final Id organizationId
  ) {
    return projectService.getArchiveDetailList(
        organizationId
    ).thenApply(list -> new StandardResponse<>(
        list.stream().map(mapper::mapArchiveDetails).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/import/start",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = PROJECT_IMPORT_REQUEST,
      output = TASK_STATUS
  )
  public CompletableFuture<StandardResponse<TaskStatusDto>> importOrganizationProject(
      @PathVariable("organizationId") final Id organizationId,
      @RequestBody final ProjectImportRequestDto projectImportRequest
  ) {
    return projectService.importOrganizationProject(
        organizationId, mapper.mapProjectImportRequest(projectImportRequest)
    ).thenApply(ts -> new StandardResponse<>(
        mapper.mapTaskStatus(ts)
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/export/start",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = PROJECT_EXPORT_REQUEST,
      output = TASK_STATUS
  )
  public CompletableFuture<StandardResponse<TaskStatusDto>> exportOrganizationProjects(
      @PathVariable("organizationId") final Id organizationId,
      @RequestBody final ProjectExportRequestDto projectExportRequest
  ) {
    return projectService.exportOrganizationProjects(
        organizationId, mapper.mapProjectExportRequest(projectExportRequest)
    ).thenApply(ts -> new StandardResponse<>(
        mapper.mapTaskStatus(ts)
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/tasks/status",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = TASK_STATUS_LIST
  )
  public CompletableFuture<StandardResponse<List<TaskStatusDto>>> getOrganizationProjectTaskStatus(
      @PathVariable("organizationId") final Id organizationId
  ) {
    return projectService.getOrganizationProjectTaskStatus(
        organizationId
    ).thenApply(l -> new StandardResponse<>(
        l.stream().map(mapper::mapTaskStatus).collect(Collectors.toList())
    ));
  }

  @RequestMapping(
      path = "/{organizationId}/projects/tasks",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = TASK_LIST
  )
  public CompletableFuture<StandardResponse<List<TaskDto>>> getOrganizationProjectTasks(
      @PathVariable("organizationId") final Id organizationId,
      @RequestParam(name = "sort", required = false) final String sort,
      @RequestParam(name = "order", required = false) final String order
  ) {
    return projectService.getOrganizationProjectTasks(
        organizationId, sort, order
    ).thenApply(l -> new StandardResponse<>(
        l.stream().map(mapper::mapTask).collect(Collectors.toList())
    ));
  }

  private List<String> extractExcludeFields(final String excludeFields) {
    return excludeFields == null ?
        Collections.emptyList() :
        Arrays.stream(excludeFields.split(","))
            .map(String::trim)
            .filter(e -> !e.isEmpty())
            .collect(Collectors.toList());
  }
}
