package com.automationanywhere.cognitive.gateway.application.microservices.application;

import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import org.springframework.stereotype.Component;

@Component
public class ApplicationDataMapper {

  public RegistrationData mapRegistration(final Registration model) {
    return model == null ? null : new RegistrationData(
        model.controlRoomUrl, model.controlRoomVersion, model.appId
    );
  }

  public Handshake mapHandshake(final HandshakeData data) {
    return data == null ? null : new Handshake(
        data.appVersion, data.publicKey, data.routingName
    );
  }

  public AcknowledgementData mapAcknowledgement(final Acknowledgement model) {
    return model == null ? null : new AcknowledgementData(model.appId);
  }

  public Configuration mapConfiguration(final ConfigurationData data) {
    return data == null ? null : new Configuration(
      data.crUrl, data.routingName
    );
  }
}
