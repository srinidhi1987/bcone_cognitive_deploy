package com.automationanywhere.cognitive.gateway.alias.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LanguageDto {

  public final Id id;
  public final String name;
  public final String code;

  public LanguageDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("code") final String code
  ) {
    this.id = id;
    this.name = name;
    this.code = code;
  }
}
