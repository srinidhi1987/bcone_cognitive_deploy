package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class ProjectExportRequestData {

  public final String archiveName;
  public final List<String> projectExportDetails;
  public final Id userId;

  public ProjectExportRequestData(
      @JsonProperty("archiveName") final String archiveName,
      @JsonProperty("projectExportDetails") final List<String> projectExportDetails,
      @JsonProperty("userId") final Id userId
  ) {
    this.archiveName = archiveName;
    this.projectExportDetails = projectExportDetails;
    this.userId = userId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectExportRequestData projectExportRequest = (ProjectExportRequestData) o;

      result = Objects.equals(archiveName, projectExportRequest.archiveName)
          && Objects.equals(projectExportDetails, projectExportRequest.projectExportDetails)
          && Objects.equals(userId, projectExportRequest.userId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archiveName)
        ^ Objects.hashCode(projectExportDetails)
        ^ Objects.hashCode(userId);
  }
}

