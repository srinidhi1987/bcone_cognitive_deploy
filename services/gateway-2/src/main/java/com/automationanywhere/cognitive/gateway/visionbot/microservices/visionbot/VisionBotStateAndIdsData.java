package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class VisionBotStateAndIdsData {

  public final String state;
  public final List<String> visionBotIds;

  public VisionBotStateAndIdsData(
      @JsonProperty("state") final String state,
      @JsonProperty("visionBotIds") final List<String> visionBotList
  ) {
    this.state = state;
    this.visionBotIds = visionBotList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VisionBotStateAndIdsData that = (VisionBotStateAndIdsData) o;
    return Objects.equals(state, that.state) &&
        Objects.equals(visionBotIds, that.visionBotIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(state, visionBotIds);
  }
}

