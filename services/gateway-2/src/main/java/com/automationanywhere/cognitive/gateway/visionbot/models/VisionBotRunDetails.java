package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class VisionBotRunDetails {

  public final Id id;
  public final String startTime;
  public final String endTime;
  public final String runningMode;
  public final String totalFiles;
  public final String processedDocumentCount;
  public final String failedDocumentCount;
  public final String passedDocumentCount;
  public final String documentProcessingAccuracy;
  public final String fieldAccuracy;
  public final ValidatorReviewDetails validatorReviewDetails;

  public VisionBotRunDetails(
      final Id id,
      final String startTime,
      final String endTime,
      final String runningMode,
      final String totalFiles,
      final String processedDocumentCount,
      final String failedDocumentCount,
      final String passedDocumentCount,
      final String documentProcessingAccuracy,
      final String fieldAccuracy,
      final ValidatorReviewDetails validatorReviewDetails
  ) {
    this.id = id;
    this.startTime = startTime;
    this.endTime = endTime;
    this.runningMode = runningMode;
    this.totalFiles = totalFiles;
    this.processedDocumentCount = processedDocumentCount;
    this.failedDocumentCount = failedDocumentCount;
    this.passedDocumentCount = passedDocumentCount;
    this.documentProcessingAccuracy = documentProcessingAccuracy;
    this.fieldAccuracy = fieldAccuracy;
    this.validatorReviewDetails = validatorReviewDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotRunDetails visionBotRunDetails = (VisionBotRunDetails) o;

      result = Objects.equals(id, visionBotRunDetails.id)
          && Objects.equals(startTime, visionBotRunDetails.startTime)
          && Objects.equals(endTime, visionBotRunDetails.endTime)
          && Objects.equals(runningMode, visionBotRunDetails.runningMode)
          && Objects.equals(totalFiles, visionBotRunDetails.totalFiles)
          && Objects.equals(processedDocumentCount, visionBotRunDetails.processedDocumentCount)
          && Objects.equals(failedDocumentCount, visionBotRunDetails.failedDocumentCount)
          && Objects.equals(passedDocumentCount, visionBotRunDetails.passedDocumentCount)
          && Objects
          .equals(documentProcessingAccuracy, visionBotRunDetails.documentProcessingAccuracy)
          && Objects.equals(fieldAccuracy, visionBotRunDetails.fieldAccuracy)
          && Objects
          .equals(validatorReviewDetails, visionBotRunDetails.validatorReviewDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(startTime)
        ^ Objects.hashCode(endTime)
        ^ Objects.hashCode(runningMode)
        ^ Objects.hashCode(totalFiles)
        ^ Objects.hashCode(processedDocumentCount)
        ^ Objects.hashCode(failedDocumentCount)
        ^ Objects.hashCode(passedDocumentCount)
        ^ Objects.hashCode(documentProcessingAccuracy)
        ^ Objects.hashCode(fieldAccuracy)
        ^ Objects.hashCode(validatorReviewDetails);
  }
}

