package com.automationanywhere.cognitive.restclient;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class StandardResponse<T> {
  public final Boolean success;
  public final T data;

  public StandardResponse(
      @JsonProperty("success") final Boolean success,
      @JsonProperty("data") final T data,
      @JsonProperty("errors") final List<String> errors
  ) {
    this.success = success;
    if (errors != null) {
      for (final String error : errors) {
        if (error != null && !error.trim().isEmpty()) {
          throw new IllegalArgumentException("Error elements should be blank or empty");
        }
      }
    }
    this.data = data;
  }

  public StandardResponse(final T data) {
    success = true;
    this.data = data;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      StandardResponse<?> that = (StandardResponse<?>) o;

      result = Objects.equals(data, that.data)
          && Objects.equals(success, that.success);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(data) ^ Objects.hashCode(success);
  }
}
