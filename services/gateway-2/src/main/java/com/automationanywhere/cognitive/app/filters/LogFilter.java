package com.automationanywhere.cognitive.app.filters;

import com.automationanywhere.cognitive.logging.LogContextKeys;
import com.automationanywhere.cognitive.logging.LogDetailsProvider;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-1)
public class LogFilter implements Filter {

  public static final String LOG_CONTEXT_REQ_ATTR_NAME = "LOG_CONTEXT_REQ_ATTR_NAME";

  private final LogDetailsProvider provider;

  @Autowired
  public LogFilter(final LogDetailsProvider provider) {
    this.provider = provider;
  }

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response,
      final FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;

    @SuppressWarnings("unchecked")
    Map<String, String> ctx = (Map<String, String>) req.getAttribute(LOG_CONTEXT_REQ_ATTR_NAME);
    if (ctx == null) {
      ctx = ThreadContext.getContext();

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.CID, CognitivePlatformHeaders.CID, provider::getCID
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.APPLICATION_NAME, null, provider::getApplicationName
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.APPLICATION_VERSION, null, provider::getApplicationVersion
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.CLIENT_IP, CognitivePlatformHeaders.CLIENT_IP, () -> provider.getClientIP(req)
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.HOST_NAME, null, provider::getHostName
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.SESSION_ID, CognitivePlatformHeaders.SESSION_ID, null
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.TENANT_ID, CognitivePlatformHeaders.TENANT_ID, null
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.USER_ID, null, provider::getUserId
      );

      provider.addTrackingInfo(
          req, ctx, LogContextKeys.REQUEST_DETAILS, null, () -> provider.getRequestDetails(req)
      );

      ctx = Collections.unmodifiableMap(ctx);
      req.setAttribute(LOG_CONTEXT_REQ_ATTR_NAME, ctx);
    }

    ThreadContext.putAll(ctx);

    //We do not want to restore the previous state of the log context
    //We want to keep our context alive as long as possible
    //to be able to send log events containing our tracking details.
    chain.doFilter(request, response);
  }
}
