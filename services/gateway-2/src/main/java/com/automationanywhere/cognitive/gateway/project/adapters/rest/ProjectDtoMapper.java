package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.gateway.project.models.ArchiveDetails;
import com.automationanywhere.cognitive.gateway.project.models.Category;
import com.automationanywhere.cognitive.gateway.project.models.CustomFieldDetail;
import com.automationanywhere.cognitive.gateway.project.models.FieldDetails;
import com.automationanywhere.cognitive.gateway.project.models.File;
import com.automationanywhere.cognitive.gateway.project.models.FileCount;
import com.automationanywhere.cognitive.gateway.project.models.NewProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.OCREngineDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetailChanges;
import com.automationanywhere.cognitive.gateway.project.models.ProjectDetails;
import com.automationanywhere.cognitive.gateway.project.models.ProjectExportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectImportRequest;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchDetail;
import com.automationanywhere.cognitive.gateway.project.models.ProjectPatchResponse;
import com.automationanywhere.cognitive.gateway.project.models.Task;
import com.automationanywhere.cognitive.gateway.project.models.TaskStatus;
import com.automationanywhere.cognitive.gateway.project.models.VisionBot;
import java.util.ArrayList;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ProjectDtoMapper {

  public ProjectDetailsDto mapProjectDetails(final ProjectDetails p) {
    return p == null ? null : new ProjectDetailsDto(
        p.id, p.name, p.description, p.organizationId, p.projectTypeId, p.projectType,
        p.confidenceThreshold,
        p.numberOfFiles, p.numberOfCategories, p.unprocessedFileCount, p.primaryLanguage,
        p.accuracyPercentage, p.visionBotCount, p.currentTrainedPercentage,
        p.categories == null ? null
            : p.categories.stream().map(this::mapCategory).collect(Collectors.toList()),
        mapFieldDetails(p.fields), p.projectState, p.environment, p.updatedAt, p.createdAt,
        p.ocrEngineDetails == null ? null
            : p.ocrEngineDetails.stream().map(this::mapOCREngineDetail).collect(Collectors.toList()),
        p.versionId
    );
  }

  public NewProjectDetails mapNewProjectDetails(final NewProjectDetailsDto p) {
    return p == null ? null : new NewProjectDetails(
        p.name, p.description, p.organizationId, p.projectTypeId, p.projectType, p.primaryLanguage,
        mapFieldDetails(p.fields), p.projectState, p.environment
    );
  }


  public ProjectDetailChangesDto mapProjectDetailChanges(final ProjectDetailChanges c) {
    return c == null ? null : new ProjectDetailChangesDto(
        c.id, c.name, c.description, c.organizationId, c.projectTypeId, c.projectType,
        c.primaryLanguage,
        mapFieldDetails(c.fields), c.projectState, c.environment,
        c.ocrEngineDetails == null ? null
            : c.ocrEngineDetails.stream().map(this::mapOCREngineDetail).collect(Collectors.toList()),
        c.versionId
    );
  }

  public ProjectDetailChanges mapProjectDetailChanges(final ProjectDetailChangesDto c) {
    return c == null ? null : new ProjectDetailChanges(
        c.id, c.name, c.description, c.organizationId, c.projectTypeId, c.projectType,
        c.primaryLanguage,
        mapFieldDetails(c.fields), c.projectState, c.environment,
        c.ocrEngineDetails == null ? null
            : c.ocrEngineDetails.stream().map(this::mapOCREngineDetail).collect(Collectors.toList()),
        c.versionId
    );
  }

  private FieldDetailsDto mapFieldDetails(final FieldDetails fields) {
    return fields == null ? null : new FieldDetailsDto(
        fields.standard,
        fields.custom == null ? null
            : fields.custom.stream().map(this::mapCustomFieldDetail).collect(Collectors.toList())
    );
  }

  private FieldDetails mapFieldDetails(final FieldDetailsDto fields) {
    return fields == null ? null : new FieldDetails(
        fields.standard,
        fields.custom == null ? null
            : fields.custom.stream().map(this::mapCustomFieldDetail).collect(Collectors.toList())
    );
  }

  private CustomFieldDetailDto mapCustomFieldDetail(final CustomFieldDetail customFieldDetail) {
    return customFieldDetail == null ? null : new CustomFieldDetailDto(
        customFieldDetail.id, customFieldDetail.name, customFieldDetail.type
    );
  }

  private CustomFieldDetail mapCustomFieldDetail(final CustomFieldDetailDto customFieldDetail) {
    return customFieldDetail == null ? null : new CustomFieldDetail(
        customFieldDetail.id, customFieldDetail.name, customFieldDetail.type
    );
  }

  private OCREngineDetailDto mapOCREngineDetail(final OCREngineDetail ocrEngineDetail) {
    return ocrEngineDetail == null ? null
        : new OCREngineDetailDto(ocrEngineDetail.id, ocrEngineDetail.name,
            ocrEngineDetail.engineType);
  }

  private OCREngineDetail mapOCREngineDetail(final OCREngineDetailDto ocrEngineDetail) {
    return ocrEngineDetail == null ? null
        : new OCREngineDetail(ocrEngineDetail.id, ocrEngineDetail.name, ocrEngineDetail.engineType);
  }

  private CategoryDto mapCategory(final Category category) {
    return category == null ? null : new CategoryDto(
        category.id, category.name,
        mapVisionBot(category.visionBot),
        category.fileCount,
        category.files == null ? null :
            category.files.stream().map(this::mapFile).collect(Collectors.toList()),
        mapFileCount(category.productionFileDetails),
        mapFileCount(category.stagingFileDetails)
    );
  }

  private VisionBotDto mapVisionBot(final VisionBot visionBot) {
    return visionBot == null ? null : new VisionBotDto(
        visionBot.id, visionBot.name, visionBot.currentState
    );
  }

  private FileCountDto mapFileCount(final FileCount fileCount) {
    return fileCount == null ? null : new FileCountDto(
        fileCount.totalCount, fileCount.unprocessedCount
    );
  }

  private FileDto mapFile(final File file) {
    return file == null ? null : new FileDto(
        file.id, file.name, file.location, file.format, file.processed
    );
  }

  public ArchiveDetailsDto mapArchiveDetails(final ArchiveDetails archiveDetails) {
    return archiveDetails == null ? null : new ArchiveDetailsDto(
        archiveDetails.archiveName
    );
  }

  public ArchiveDetails mapArchiveDetails(final ArchiveDetailsDto archiveDetails) {
    return archiveDetails == null ? null : new ArchiveDetails(
        archiveDetails.archiveName
    );
  }

  public TaskStatusDto mapTaskStatus(final TaskStatus taskStatus) {
    return taskStatus == null ? null : new TaskStatusDto(
        taskStatus.taskId, taskStatus.taskType, taskStatus.status, taskStatus.description
    );
  }

  public TaskDto mapTask(final Task task) {
    return task == null ? null : new TaskDto(
        task.taskId, task.taskType, task.status, task.description, task.taskOptions, task.userId,
        task.startTime, task.endTime
    );
  }

  public ProjectImportRequest mapProjectImportRequest(
      final ProjectImportRequestDto projectImportRequest
  ) {
    return projectImportRequest == null ? null : new ProjectImportRequest(
        mapArchiveDetails(projectImportRequest.archive),
        projectImportRequest.userId,
        projectImportRequest.option
    );
  }

  public ProjectExportRequest mapProjectExportRequest(
      final ProjectExportRequestDto projectExportRequest
  ) {
    return projectExportRequest == null ? null : new ProjectExportRequest(
        projectExportRequest.archiveName,
        projectExportRequest.projectExportDetails == null
            ? null
            : new ArrayList<>(projectExportRequest.projectExportDetails),
        projectExportRequest.userId
    );
  }

  public ProjectPatchDetail mapProjectPatchDetail(
      final ProjectPatchDetailDto projectPatchDetailDto) {
    return projectPatchDetailDto == null
        ? null
        : new ProjectPatchDetail(
            projectPatchDetailDto.op,
            projectPatchDetailDto.path,
            projectPatchDetailDto.value,
            projectPatchDetailDto.versionId
        );
  }

  public ProjectPatchResponseDto mapProjectPatchResponse(
      final ProjectPatchResponse projectPatchResponse) {
    return projectPatchResponse == null
        ? null
        : new ProjectPatchResponseDto(
            projectPatchResponse.versionId
        );
  }
}
