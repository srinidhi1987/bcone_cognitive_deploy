package com.automationanywhere.cognitive.gateway.alias;

/**
 * Specifies the information present in the "shareable" representation of the
 * domain metadata.
 *
 * @author Erik K. Worth
 */
public interface ShareableDomainInfo {

  /** Returns the domain name such as "Invoices" */
  String getName();

  /** Returns the encoded "shareable" representation of the domain metadata */
  String getDomain();

  /** Returns the version of the domain metadata structure */
  int getVersion();
}
