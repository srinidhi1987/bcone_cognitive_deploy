package com.automationanywhere.cognitive.gateway.report.adapters;

import java.util.List;

public class ValidationDto {

  private List<FieldValidationDto> fields;
  private List<GroupTimeSpentDto> categories;

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  public List<FieldValidationDto> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the fields to be set
   */
  public void setFields(List<FieldValidationDto> fields) {
    this.fields = fields;
  }

  /**
   * Returns the categories.
   *
   * @return the value of categories
   */
  public List<GroupTimeSpentDto> getCategories() {
    return categories;
  }

  /**
   * Sets the categories.
   *
   * @param categories the categories to be set
   */
  public void setCategories(List<GroupTimeSpentDto> categories) {
    this.categories = categories;
  }
}
