package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.List;
import java.util.Objects;

public class ValidatorProperties {
  public final String filePath;
  public final List<String> staticListItems;

  public ValidatorProperties(
      final String filePath,
      final List<String> staticListItems
  ) {
    this.filePath = filePath;
    this.staticListItems = staticListItems;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ValidatorProperties that = (ValidatorProperties) o;

      result = Objects.equals(filePath, that.filePath)
          && Objects.equals(staticListItems, that.staticListItems);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(filePath)
        ^ Objects.hashCode(staticListItems);
  }
}
