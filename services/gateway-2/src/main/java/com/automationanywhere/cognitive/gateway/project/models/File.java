package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class File {

  public final Id id;
  public final String name;
  public final String location;
  public final String format;
  public final Boolean processed;

  public File(
      final Id id,
      final String name,
      final String location,
      final String format,
      final Boolean processed
  ) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.format = format;
    this.processed = processed;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      File fileData = (File) o;

      result = Objects.equals(id, fileData.id)
          && Objects.equals(name, fileData.name)
          && Objects.equals(location, fileData.location)
          && Objects.equals(format, fileData.format)
          && Objects.equals(processed, fileData.processed);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(location)
        ^ Objects.hashCode(format)
        ^ Objects.hashCode(processed);
  }
}
