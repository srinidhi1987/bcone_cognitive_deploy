package com.automationanywhere.cognitive.resourcelock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.project.microservices.project.ProjectMicroservice;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

@Component
public class ResourceLocker {
  private static final Logger LOGGER = LogManager.getLogger(ResourceLocker.class);

  private final ProjectMicroservice projectMicroservice;
  private final ThreadPoolTaskScheduler taskScheduler;
  private final ApplicationConfiguration cfg;

  private final List<Resource> resources = new ArrayList<>();

  @Autowired
  public ResourceLocker(
      final ProjectMicroservice projectMicroservice,
      final ApplicationConfiguration cfg
  ) {
    this.projectMicroservice = projectMicroservice;
    this.cfg = cfg;

    taskScheduler = new ThreadPoolTaskScheduler();
    taskScheduler.setDaemon(true);
    taskScheduler.setPoolSize(1);
    taskScheduler.setRemoveOnCancelPolicy(true);
    taskScheduler.setWaitForTasksToCompleteOnShutdown(false);
    taskScheduler.setThreadNamePrefix("GATEWAY_TASK_STATUS_POLL_");
    taskScheduler.setAwaitTerminationSeconds((int) cfg.resourceLockerSchedulerShutdownTimeout());
  }

  @PostConstruct
  public void init() {
    taskScheduler.initialize();
  }

  @PreDestroy
  public void destroy() {
    taskScheduler.shutdown();
  }

  public boolean isLocked(final Resource resource) {
    boolean locked = false;
    synchronized (resources) {
      locked = resources.stream().anyMatch(r -> r == resource);
    }
    return locked;
  }

  public void lock(final Resource resource) {
    if (resource == null) {
      throw new IllegalArgumentException();
    }

    synchronized (resources) {
      checkCanLock(resource);

      resources.add(resource);
    }

    LOGGER.info("Lock acquired for " + resource.path);
  }

  public void start(final ExclusiveResource resource) {
    if (resource == null) {
      throw new IllegalArgumentException();
    }

    checkWasLocked(resource);

    Map<String, String> ctx = ThreadContext.getContext();

    CountDownLatch latch = new CountDownLatch(1);

    ScheduledFuture<?>[] f = {null};
    f[0] = taskScheduler.scheduleAtFixedRate(() -> {
      ThreadContext.putAll(ctx);

      try {
        latch.await();
        poll(resource, f[0]);
      } catch (final InterruptedException ex) {
        LOGGER.error("Poll was interrupted", ex);
      }
    }, cfg.resourceLockerPollPeriod());

    latch.countDown();
  }

  public void unlock(final Resource resource) {
    if (resource == null) {
      throw new IllegalArgumentException();
    }

    synchronized (this) {
      checkWasLocked(resource);

      resources.remove(resource);
    }

    LOGGER.info("Lock released for " + resource.path);
  }

  private void poll(
      final ExclusiveResource resource,
      final ScheduledFuture<?> future
  ) {
    LOGGER.debug("Getting task status for " + resource.path);

    projectMicroservice.getOrganizationProjectTaskStatus(resource.orgId).whenComplete((l, ex) -> {
      if (ex != null)  {
        LOGGER.error("Failed to get the status of " + resource.path);
      } else {
        if (l.isEmpty()) {
          future.cancel(true);

          unlock(resource);
        } else {
          LOGGER.debug("Task hasn't yet completed for " + resource.path);
        }
      }
    });
  }

  private void checkCanLock(final Resource resource) {
    synchronized (resources) {
      Optional<ExclusiveResource> found = resources.stream()
          .filter(r -> r instanceof ExclusiveResource)
          .map(r -> (ExclusiveResource) r)
          .findAny();
      if (found.isPresent()) {
        throw new LockedExclusivelyException(found.get().activityName);
      }
      if (resource instanceof ExclusiveResource) {
        if (!resources.isEmpty()) {
          throw new LockedException(Resource.DEFAULT_ACTIVITY_NAME);
        }
      }
    }
  }

  private void checkWasLocked(final Resource resource) {
    if (!isLocked(resource)) {
      throw new UnlockedException();
    }
  }
}
