package com.automationanywhere.cognitive.gateway.ml.rest;

import static com.automationanywhere.cognitive.validation.Schemas.V1;
import static com.automationanywhere.cognitive.validation.Schemas.VALUE_LIST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.automationanywhere.cognitive.gateway.ml.MlService;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MlController {

  private final MlService service;
  private final MlDtoMapper mapper;

  @Autowired
  public MlController(
      final MlService service,
      final MlDtoMapper mapper
  ) {
    this.service = service;
    this.mapper = mapper;
  }

  @RequestMapping(
      path = "/predict",
      method = GET
  )
  public CompletableFuture<byte[]> predict(
      @RequestParam(name = "fieldid") final String fieldId,
      @RequestParam(name = "originalvalue") final String originalValue
  ) {
    return service.predict(fieldId, originalValue);
  }

  @RequestMapping(
      path = "/value",
      method = POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = VALUE_LIST
  )
  public CompletableFuture<byte[]> setValue(
      @RequestBody final List<ValueDto> values
  ) {
    return service.setValue(
        values == null ? Collections.emptyList()
            : values.stream().map(mapper::mapValue).collect(Collectors.toList())
    );
  }
}
