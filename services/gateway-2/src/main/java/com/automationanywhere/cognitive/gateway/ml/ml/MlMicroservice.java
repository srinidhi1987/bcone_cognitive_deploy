package com.automationanywhere.cognitive.gateway.ml.ml;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.ml.model.Value;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

@Repository
public class MlMicroservice {

  private final MlDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public MlMicroservice(
      final MlDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  public CompletableFuture<byte[]> predict(
      final String fieldId,
      final String originalValue
  ) {
    Builder builder = Builder
        .get(cfg.mlServiceURL())
        .request(cfg.predict())
        .param(fieldId)
        .param(originalValue);

    return restClient.send(
        builder.build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    );
  }

  public CompletableFuture<byte[]> setValue(final List<Value> values) {
     Builder builder = Builder
        .post(cfg.mlServiceURL())
        .request(cfg.value())
        .content(values.stream().map(mapper::mapValue).collect(Collectors.toList()), Types.JSON)
        .accept(Types.JSON);

    return restClient.send(
        builder.build(),
        new ParameterizedTypeReference<byte[]>() {
        }
    );
 }
}