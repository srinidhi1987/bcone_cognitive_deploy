package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class CategoryData {

  public final Id id;
  public final String name;
  public final VisionBotData visionBot;
  public final Integer fileCount;
  public final List<FileData> files;
  public final FileCountData productionFileDetails;
  public final FileCountData stagingFileDetails;

  public CategoryData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("visionBot") final VisionBotData visionBot,
      @JsonProperty("fileCount") final Integer fileCount,
      @JsonProperty("files") final List<FileData> files,
      @JsonProperty("productionFileDetails") final FileCountData productionFileDetails,
      @JsonProperty("stagingFileDetails") final FileCountData stagingFileDetails
  ) {
    this.id = id;
    this.name = name;
    this.visionBot = visionBot;
    this.fileCount = fileCount;
    this.files = files;
    this.productionFileDetails = productionFileDetails;
    this.stagingFileDetails = stagingFileDetails;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CategoryData category = (CategoryData) o;

      result = Objects.equals(id, category.id)
          && Objects.equals(name, category.name)
          && Objects.equals(visionBot, category.visionBot)
          && Objects.equals(fileCount, category.fileCount)
          && Objects.equals(files, category.files)
          && Objects.equals(productionFileDetails, category.productionFileDetails)
          && Objects.equals(stagingFileDetails, category.stagingFileDetails);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(visionBot)
        ^ Objects.hashCode(fileCount)
        ^ Objects.hashCode(files)
        ^ Objects.hashCode(productionFileDetails)
        ^ Objects.hashCode(stagingFileDetails);
  }
}

