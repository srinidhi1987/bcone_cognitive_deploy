package com.automationanywhere.cognitive.gateway.report.adapters;

import java.util.List;

public class ClassificationDto {

  private List<ClassificationFieldDto> fieldRepresentation;

  /**
   * Returns the fieldRepresentation.
   *
   * @return the value of fieldRepresentation
   */
  public List<ClassificationFieldDto> getFieldRepresentation() {
    return fieldRepresentation;
  }

  /**
   * Sets the fieldRepresentation.
   *
   * @param fieldRepresentation the fieldRepresentation to be set
   */
  public void setFieldRepresentation(List<ClassificationFieldDto> fieldRepresentation) {
    this.fieldRepresentation = fieldRepresentation;
  }
}
