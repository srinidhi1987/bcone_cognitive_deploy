package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotCountData {

  public final Integer numberOfBotsCreated;
  public final Integer numberOfStagingBots;
  public final Integer numberOfProductionBots;

  public VisionBotCountData(
      @JsonProperty("numberOfBotsCreated") final Integer numberOfBotsCreated,
      @JsonProperty("numberOfStagingBots") final Integer numberOfStagingBots,
      @JsonProperty("numberOfProductionBots") final Integer numberOfProductionBots
  ) {
    this.numberOfBotsCreated = numberOfBotsCreated;
    this.numberOfStagingBots = numberOfStagingBots;
    this.numberOfProductionBots = numberOfProductionBots;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotCountData visionBotCount = (VisionBotCountData) o;

      result = Objects.equals(numberOfBotsCreated, visionBotCount.numberOfBotsCreated)
        && Objects.equals(numberOfStagingBots, visionBotCount.numberOfStagingBots)
        && Objects.equals(numberOfProductionBots, visionBotCount.numberOfProductionBots);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(numberOfBotsCreated)
      ^ Objects.hashCode(numberOfStagingBots)
      ^ Objects.hashCode(numberOfProductionBots);
  }
}

