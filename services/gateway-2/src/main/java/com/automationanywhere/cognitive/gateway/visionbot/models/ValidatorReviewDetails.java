package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Objects;

public class ValidatorReviewDetails {

  public final String totalFilesReviewed;
  public final String totalFilesMarkedInvalid;
  public final String totalFailedFiles;
  public final String averageReviewTime;

  public ValidatorReviewDetails(
      final String totalFilesReviewed,
      final String totalFilesMarkedInvalid,
      final String totalFailedFiles,
      final String averageReviewTime
  ) {
    this.totalFilesReviewed = totalFilesReviewed;
    this.totalFilesMarkedInvalid = totalFilesMarkedInvalid;
    this.totalFailedFiles = totalFailedFiles;
    this.averageReviewTime = averageReviewTime;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ValidatorReviewDetails validatorReviewDetails = (ValidatorReviewDetails) o;

      result = Objects.equals(totalFilesReviewed, validatorReviewDetails.totalFilesReviewed)
        && Objects.equals(totalFilesMarkedInvalid, validatorReviewDetails.totalFilesMarkedInvalid)
        && Objects.equals(totalFailedFiles, validatorReviewDetails.totalFailedFiles)
        && Objects.equals(averageReviewTime, validatorReviewDetails.averageReviewTime);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalFilesReviewed)
      ^ Objects.hashCode(totalFilesMarkedInvalid)
      ^ Objects.hashCode(totalFailedFiles)
      ^ Objects.hashCode(averageReviewTime);
  }
}

