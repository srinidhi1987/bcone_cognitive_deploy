package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.time.OffsetDateTime;
import java.util.Objects;

public class Task {

  public final Id taskId;
  public final String taskType;
  public final String taskOptions;
  public final String status;
  public final String description;
  public final String userId;
  public final OffsetDateTime startTime;
  public final OffsetDateTime endTime;

  public Task(
      final Id taskId,
      final String taskType,
      final String status,
      final String description,
      final String taskOptions,
      final String userId,
      final OffsetDateTime startTime,
      final OffsetDateTime endTime) {
    this.taskId = taskId;
    this.taskType = taskType;
    this.taskOptions = taskOptions;
    this.status = status;
    this.description = description;
    this.userId = userId;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      Task task = (Task) o;

      result = Objects.equals(taskId, task.taskId)
          && Objects.equals(taskType, task.taskType)
          && Objects.equals(status, task.status)
          && Objects.equals(description, task.description)
          && Objects.equals(taskOptions, task.taskOptions)
          && Objects.equals(userId, task.userId)
          && Objects.equals(startTime, task.startTime)
          && Objects.equals(endTime, task.endTime);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(taskId)
        ^ Objects.hashCode(taskType)
        ^ Objects.hashCode(status)
        ^ Objects.hashCode(description)
        ^ Objects.hashCode(taskOptions)
        ^ Objects.hashCode(userId)
        ^ Objects.hashCode(startTime)
        ^ Objects.hashCode(endTime);
  }
}

