package com.automationanywhere.cognitive.gateway.ml.model;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class Value {
  public final Id fieldId;
  public final String correctedValue;
  public final String originalValue;

  public Value(
      final Id fieldId,
      final String correctedValue,
      final String originalValue
  ) {
    this.fieldId = fieldId;
    this.correctedValue = correctedValue;
    this.originalValue = originalValue;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      Value value = (Value) o;

      result = Objects.equals(fieldId, value.fieldId)
          && Objects.equals(correctedValue, value.correctedValue)
          && Objects.equals(originalValue, value.originalValue);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fieldId)
        ^ Objects.hashCode(correctedValue)
        ^ Objects.hashCode(originalValue);
  }
}
