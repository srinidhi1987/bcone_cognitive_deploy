package com.automationanywhere.cognitive.gateway.report.microservice.report;

import com.automationanywhere.cognitive.gateway.report.microservice.report.model.AccuracyData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ClassificationData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ClassificationFieldData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.DashboardProdProjectTotalsData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.FieldAccuracyData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.FieldValidationData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.GroupTimeSpentData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ProjectTotalsData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ReportingProjectData;
import com.automationanywhere.cognitive.gateway.report.microservice.report.model.ValidationData;
import com.automationanywhere.cognitive.gateway.report.model.Accuracy;
import com.automationanywhere.cognitive.gateway.report.model.Classification;
import com.automationanywhere.cognitive.gateway.report.model.ClassificationField;
import com.automationanywhere.cognitive.gateway.report.model.DashboardProdProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.FieldAccuracy;
import com.automationanywhere.cognitive.gateway.report.model.FieldValidation;
import com.automationanywhere.cognitive.gateway.report.model.GroupTimeSpent;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.ReportingProject;
import com.automationanywhere.cognitive.gateway.report.model.Validation;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ReportDataMapper {

  public List<DashboardProdProjectTotals> getDashboardProdProjectTotals(
      List<DashboardProdProjectTotalsData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getDashboardProdProjectTotals)
        .collect(Collectors.toList());
  }

  public DashboardProdProjectTotals getDashboardProdProjectTotals(
      DashboardProdProjectTotalsData data) {
    DashboardProdProjectTotals entity = new DashboardProdProjectTotals();
    entity.setTotalFilesProcessed(data.getTotalFilesProcessed());
    entity.setTotalSTP(data.getTotalSTP());
    entity.setTotalAccuracy(data.getTotalAccuracy());
    entity.setTotalFilesCount(data.getTotalFilesCount());
    return entity;
  }

  public List<ProjectTotals> getProjectTotals(List<ProjectTotalsData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getProjectTotals)
        .collect(Collectors.toList());
  }

  public ProjectTotals getProjectTotals(ProjectTotalsData data) {
    ProjectTotals entity = new ProjectTotals();
    entity.setTotalFilesProcessed(data.getTotalFilesProcessed());
    entity.setTotalSTP(data.getTotalSTP());
    entity.setTotalAccuracy(data.getTotalAccuracy());
    entity.setTotalFilesUploaded(data.getTotalFilesUploaded());
    entity.setTotalFilesToValidation(data.getTotalFilesToValidation());
    entity.setTotalFilesValidated(data.getTotalFilesValidated());
    entity.setTotalFilesUnprocessable(data.getTotalFilesUnprocessable());
    entity.setClassification(getClassification(data.getClassification()));
    entity.setAccuracy(getAccuracy(data.getAccuracy()));
    entity.setValidation(getValidation(data.getValidation()));
    return entity;
  }

  public Classification getClassification(ClassificationData data) {
    Classification entity = new Classification();
    entity.setFieldRepresentation(getClassificationField(data.getFieldRepresentation()));
    return entity;
  }

  public List<ClassificationField> getClassificationField(List<ClassificationFieldData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getClassificationField)
        .collect(Collectors.toList());
  }

  public ClassificationField getClassificationField(ClassificationFieldData data) {
    ClassificationField entity = new ClassificationField();
    entity.setName(data.getName());
    entity.setRepresentationPercent(data.getRepresentationPercent());
    return entity;
  }

  public Accuracy getAccuracy(AccuracyData data) {
    Accuracy entity = new Accuracy();
    entity.setFields(getFieldAccuracy(data.getFields()));
    return entity;
  }

  public List<FieldAccuracy> getFieldAccuracy(List<FieldAccuracyData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getFieldAccuracy)
        .collect(Collectors.toList());
  }

  public FieldAccuracy getFieldAccuracy(FieldAccuracyData data) {
    FieldAccuracy entity = new FieldAccuracy();
    entity.setName(data.getName());
    entity.setAccuracyPercent(data.getAccuracyPercent());
    return entity;
  }

  public Validation getValidation(ValidationData data) {
    Validation entity = new Validation();
    entity.setFields(getFieldValidation(data.getFields()));
    entity.setCategories(getGroupTimeSpent(data.getCategories()));
    return entity;
  }

  private List<FieldValidation> getFieldValidation(List<FieldValidationData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getFieldValidation)
        .collect(Collectors.toList());
  }

  public FieldValidation getFieldValidation(FieldValidationData data) {
    FieldValidation entity = new FieldValidation();
    entity.setFieldId(data.getFieldId());
    entity.setPercentValidated(data.getPercentValidated());
    return entity;
  }

  private List<GroupTimeSpent> getGroupTimeSpent(List<GroupTimeSpentData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getGroupTimeSpent)
        .collect(Collectors.toList());
  }

  private GroupTimeSpent getGroupTimeSpent(GroupTimeSpentData data) {
    GroupTimeSpent entity = new GroupTimeSpent();
    entity.setName(data.getName());
    entity.setTimeSpent(data.getTimeSpent());
    return entity;
  }

  public List<ReportingProject> getReportingProject(List<ReportingProjectData> data) {
    return Optional.ofNullable(data).orElse(Collections.emptyList())
        .stream()
        .map(this::getReportingProject)
        .collect(Collectors.toList());
  }

  public ReportingProject getReportingProject(ReportingProjectData data) {
    ReportingProject entity = new ReportingProject();
    entity.setId(data.getId());
    entity.setOrganizationId(data.getOrganizationId());
    entity.setName(data.getName());
    entity.setDescription(data.getDescription());
    entity.setEnvironment(data.getEnvironment());
    entity.setPrimaryLanguage(data.getPrimaryLanguage());
    entity.setProjectType(data.getProjectType());
    entity.setProjectState(data.getProjectState());
    entity.setTotalFilesProcessed(data.getTotalFilesProcessed());
    entity.setTotalSTP(data.getTotalSTP());
    entity.setNumberOfFiles(data.getNumberOfFiles());
    entity.setTotalAccuracy(data.getTotalAccuracy());
    entity.setCurrentTrainedPercentage(data.getCurrentTrainedPercentage());
    return entity;
  }
}
