package com.automationanywhere.cognitive.restclient.resttemplate;

import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;

/**
 * This converter extracts the data field from a standard response object
 * making it transparent for REST client callers.
 *
 * If a response doesn't contain a standard response object,
 * it's deserialized as is.
 *
 * If a response contains a standard response object but the object describes an error
 * an exception is thrown.
 */
public class JacksonHttpMessageConverter extends MappingJackson2HttpMessageConverter {

  private static final String DATA_FIELD_NAME = "data";
  private static final String SUCCESS_FIELD_NAME = "success";
  private static final String ERRORS_FIELD_NAME = "errors";
  private static final String HTTP_STATUS_CODE_FIELD_NAME = "httpStatusCode";

  public JacksonHttpMessageConverter(final ObjectMapper objectMapper) {
    super(objectMapper);
  }

  @Override
  protected Object readInternal(
      final Class<?> clazz, final HttpInputMessage inputMessage
  ) throws IOException, HttpMessageNotReadableException {
    JavaType javaType = getJavaType(clazz, null);
    return readJavaType(javaType, inputMessage);
  }

  @Override
  public Object read(
      final Type type, final Class<?> contextClass, final HttpInputMessage inputMessage
  ) throws IOException, HttpMessageNotReadableException {
    JavaType javaType = getJavaType(type, contextClass);
    return readJavaType(javaType, inputMessage);
  }

  Object readJavaType(final JavaType javaType, final HttpInputMessage inputMessage) {
    Object obj = null;

    try {
      if (StandardResponse.class.isAssignableFrom(javaType.getRawClass())) {
        // They want to deal with the container object themselves
        obj = objectMapper.readValue(inputMessage.getBody(), javaType);
      } else {
        // They want the container object to be transparent.
        JsonNode jsonNode = objectMapper.readTree(inputMessage.getBody());

        JsonNode errors = getErrors(jsonNode);
        JsonNode data = getData(jsonNode);
        HttpStatus httpStatus = getHttpStatus(jsonNode);
        Boolean success = getSuccess(jsonNode);

        int n = errors == null ? 0 : 1;
        n+= data == null ? 0 : 1;
        n+= httpStatus == null ? 0 : 1;
        n+= success == null ? 0 : 1;

        if (n == jsonNode.size()) { // This is a standard response
          obj = extractData(javaType, jsonNode, success, data, httpStatus, errors);
        } else { // This was not a standard response, let's deserialize it as is
          obj = objectMapper.convertValue(jsonNode, javaType);
        }
      }
    } catch (final JsonProcessingException ex) {
      throw new HttpMessageNotReadableException(
          "JSON parse error: " + ex.getOriginalMessage(), ex
      );
    } catch (final IOException ex) {
      throw new HttpMessageNotReadableException(
          "I/O error while reading input message", ex
      );
    }

    return obj;
  }

  private Object extractData(
      final JavaType javaType,
      final JsonNode jsonNode,
      final Boolean success,
      final JsonNode data,
      final HttpStatus httpStatus,
      final JsonNode errors
  ) throws IOException {
    Object obj = null;

    boolean hasErrors = hasErrors(errors);

    if (!hasErrors) {
      if (!(data == null || data.isNull())) {
        // It's a success and there are data, let's extract it.
        obj = objectMapper.convertValue(data, javaType);
      } // It's a success but there's nothing to extract, let it be null
    } else {
      // It's an error, let's throw it.
      throw convertToException(jsonNode, httpStatus);
    }

    return obj;
  }

  private boolean hasErrors(final JsonNode errors) {
    boolean result = false;

    if (errors != null) {
      if (errors.isArray()) {
        for (int i = 0; i < errors.size(); i+= 1) {
          JsonNode error = errors.get(i);
          if (error.isTextual()) {
            if (!error.asText().trim().isEmpty()) {
              result = true;
              break;
            }
          }
        }
      } else if (errors.isTextual()){
        result = !errors.asText().trim().isEmpty();
      }
    }

    return result;
  }

  HttpClientErrorException convertToException(
      final JsonNode jsonNode, final HttpStatus extractedHttpStatus
  ) throws JsonProcessingException {
    HttpStatus httpStatus = extractedHttpStatus;
    if (httpStatus == null) {
      // This is not good, they return an error data structure
      // without an error HTTP status code
      httpStatus = HttpStatus.BAD_REQUEST;
    } else {
      if (!(httpStatus.is4xxClientError()
          || httpStatus.is5xxServerError())) {
        // This is very bad.
        // They set errors, but still insist it's not an error
        // by returning not an error http status code in the both
        // in the HTTP response and in the response JSON.
        // If you set an error message but do not set an error code
        // why to set the message? it will be ignored!
        // This code is to avoid the silent consumption of error messages.
        // Let errors be heard.
        httpStatus = HttpStatus.BAD_REQUEST;
      }
    }

    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.put(
        HttpHeaders.CONTENT_TYPE,
        Collections.singletonList(MediaType.APPLICATION_JSON_UTF8_VALUE)
    );

    return new HttpClientErrorException(
        httpStatus,
        httpStatus.getReasonPhrase(),
        httpHeaders,
        objectMapper.writeValueAsBytes(jsonNode),
        MediaType.APPLICATION_JSON_UTF8.getCharset()
    );
  }

  private Boolean getSuccess(final JsonNode jsonNode) {
    Boolean success = null;
    if (jsonNode.has(SUCCESS_FIELD_NAME)) {
      if (jsonNode.get(SUCCESS_FIELD_NAME).isNull()) {
        success = true;
      } else if (jsonNode.get(SUCCESS_FIELD_NAME).isBoolean()) {
        success = jsonNode.get(SUCCESS_FIELD_NAME).asBoolean();
      }
    }
    return success;
  }

  private HttpStatus getHttpStatus(final JsonNode jsonNode) {
    HttpStatus httpStatus = null;

    if (jsonNode.has(HTTP_STATUS_CODE_FIELD_NAME)) {
      if (jsonNode.get(HTTP_STATUS_CODE_FIELD_NAME).isNull()) {
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
      } else {
        if (jsonNode.get(HTTP_STATUS_CODE_FIELD_NAME).canConvertToInt()) {
          try {
            httpStatus = HttpStatus.valueOf(
                jsonNode.get(HTTP_STATUS_CODE_FIELD_NAME).intValue()
            );
          } catch (final IllegalArgumentException ex) {
            // The field has the expected name, but it contains something
            // that doesn't look like an HTTP status code
            // So, the JSON is not a standard response,
            // we will deserialize it as is and will pass that object to the caller.
            httpStatus = null;
          }
        }
      }
    }
    return httpStatus;
  }

  private JsonNode getData(final JsonNode jsonNode) {
    return jsonNode.has(DATA_FIELD_NAME) ?
        jsonNode.get(DATA_FIELD_NAME) :
        null;
  }

  private JsonNode getErrors(final JsonNode jsonNode) {
    JsonNode errors = null;

    if (jsonNode.has(ERRORS_FIELD_NAME)) {
      errors = jsonNode.get(ERRORS_FIELD_NAME);

      if (!errors.isNull()) {
        if (!jsonNode.get(ERRORS_FIELD_NAME).isTextual()) {
          if (jsonNode.get(ERRORS_FIELD_NAME).isArray()) {
            for (final JsonNode item : jsonNode.get(ERRORS_FIELD_NAME)) {
              if (!item.isNull()) {
                if (!item.isTextual()) {
                  errors = null;
                  break;
                }
              }
            }
          } else {
            errors = null;
          }
        }
      }
    }

    return errors;
  }
}
