package com.automationanywhere.cognitive.gateway.application.microservices.application;

public class RegistrationData {

  public final String controlRoomUrl;
  public final String controlRoomVersion;
  public final String appId;

  public RegistrationData(
      final String controlRoomUrl,
      final String controlRoomVersion,
      final String appId
  ) {
    this.controlRoomUrl = controlRoomUrl;
    this.controlRoomVersion = controlRoomVersion;
    this.appId = appId;
  }
}
