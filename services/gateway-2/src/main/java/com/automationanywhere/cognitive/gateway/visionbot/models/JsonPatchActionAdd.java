package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Objects;

public class JsonPatchActionAdd extends JsonPatchAction {

  public final JsonNode value;

  public JsonPatchActionAdd(
      final String path,
      final JsonNode value
  ) {
    super(path);
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof JsonPatchActionAdd)) {
      return false;
    }
    JsonPatchActionAdd that = (JsonPatchActionAdd) o;
    return Objects.equals(path, that.path) && Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, value);
  }
}
