package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class SummaryDto {

  public final Integer totalGroups;
  public final Integer documentsUnclassified;
  public final Integer totalFiles;

  public SummaryDto(
      @JsonProperty("totalGroups") final Integer totalGroups,
      @JsonProperty("documentsUnclassified") final Integer documentsUnclassified,
      @JsonProperty("totalFiles") final Integer totalFiles
  ) {
    this.totalGroups = totalGroups;
    this.documentsUnclassified = documentsUnclassified;
    this.totalFiles = totalFiles;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      SummaryDto summary = (SummaryDto) o;

      result = Objects.equals(totalGroups, summary.totalGroups)
          && Objects.equals(documentsUnclassified, summary.documentsUnclassified)
          && Objects.equals(totalFiles, summary.totalFiles);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(totalGroups)
        ^ Objects.hashCode(documentsUnclassified)
        ^ Objects.hashCode(totalFiles);
  }
}

