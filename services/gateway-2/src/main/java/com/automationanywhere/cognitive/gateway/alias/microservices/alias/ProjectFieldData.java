package com.automationanywhere.cognitive.gateway.alias.microservices.alias;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ProjectFieldData {

  public final Id id;
  public final String name;
  public final String description;
  public final String fieldType;
  public final List<String> aliases;
  public final Boolean isDefaultSelected;
  public final String dataType;

  public ProjectFieldData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("description") final String description,
      @JsonProperty("fieldType") final String fieldType,
      @JsonProperty("aliases") final List<String> aliases,
      @JsonProperty("isDefaultSelected") final Boolean isDefaultSelected,
      @JsonProperty("dataType") final String dataType
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.fieldType = fieldType;
    this.aliases = aliases;
    this.isDefaultSelected = isDefaultSelected;
    this.dataType = dataType;
  }
}