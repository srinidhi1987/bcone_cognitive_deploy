package com.automationanywhere.cognitive.gateway.project.models;

import java.util.Objects;

public class ProjectPatchResponse {
  public final Integer versionId;

  public ProjectPatchResponse(
      final Integer versionId
  ) {
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectPatchResponse projectPatchResponse
          = (ProjectPatchResponse) o;

      result = Objects.equals(versionId, projectPatchResponse.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(versionId);
  }
}
