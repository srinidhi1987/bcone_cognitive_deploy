package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Objects;

public class VisionBotState {

  public final String state;

  public VisionBotState(
      final String state
  ) {
    this.state = state;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotState visionBotState = (VisionBotState) o;

      result = Objects.equals(state, visionBotState.state);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(state);
  }
}

