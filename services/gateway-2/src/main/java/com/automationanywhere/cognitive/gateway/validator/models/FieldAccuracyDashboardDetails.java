package com.automationanywhere.cognitive.gateway.validator.models;

public class FieldAccuracyDashboardDetails {

  private final String fieldId;
  private final double accuracyPercent;

  public FieldAccuracyDashboardDetails(
      String fieldId,
      double accuracyPercent
  ) {
    this.fieldId = fieldId;
    this.accuracyPercent = accuracyPercent;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the accuracyPercent.
   *
   * @return the value of accuracyPercent
   */
  public double getAccuracyPercent() {
    return accuracyPercent;
  }
}
