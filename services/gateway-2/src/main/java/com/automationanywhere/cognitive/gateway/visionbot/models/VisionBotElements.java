package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.List;
import java.util.Objects;

public class VisionBotElements {

  public final String state;
  public final List<String> visionBotIds;

  public VisionBotElements(
      final String state,
      final List<String> visionBotList
  ) {
    this.state = state;
    this.visionBotIds = visionBotList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VisionBotElements that = (VisionBotElements) o;
    return Objects.equals(state, that.state) &&
        Objects.equals(visionBotIds, that.visionBotIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(state, visionBotIds);
  }
}

