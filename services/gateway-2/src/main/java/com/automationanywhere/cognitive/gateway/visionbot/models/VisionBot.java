package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class VisionBot {

  public final Id id;
  public final String visionBotData;
  public final String validationData;

  public VisionBot(
      final Id id, final String visionBotData, final String validationData
  ) {
    this.id = id;
    this.visionBotData = visionBotData;
    this.validationData = validationData;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      VisionBot visionBot = (VisionBot) o;

      result = Objects.equals(visionBotData, visionBot.visionBotData)
          && Objects.equals(validationData, visionBot.validationData)
          && Objects.equals(id, visionBot.id);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(visionBotData)
        ^ Objects.hashCode(validationData)
        ^ Objects.hashCode(id);
  }
}
