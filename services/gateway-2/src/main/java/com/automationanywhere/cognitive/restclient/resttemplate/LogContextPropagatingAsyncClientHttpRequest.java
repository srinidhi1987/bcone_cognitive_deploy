package com.automationanywhere.cognitive.restclient.resttemplate;

import java.io.IOException;
import org.springframework.http.client.AsyncClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.SettableListenableFuture;

/**
 * Decorates an {@link AsyncClientHttpRequest} with a facility to propagate a log context
 * to a thread where a response handling (of the request) will happen.
 */
public class LogContextPropagatingAsyncClientHttpRequest extends DelegatingAsyncClientHttpRequest {

  public LogContextPropagatingAsyncClientHttpRequest(final AsyncClientHttpRequest delegate) {
    super(delegate);
  }

  @Override
  public ListenableFuture<ClientHttpResponse> executeAsync() throws IOException {
    SettableListenableFuture<ClientHttpResponse> future = new SettableListenableFuture<>();

    super.executeAsync().addCallback(new LogContextPropagatingListenableFutureCallback(future));

    return future;
  }
}
