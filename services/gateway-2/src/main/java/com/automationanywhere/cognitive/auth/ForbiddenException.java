package com.automationanywhere.cognitive.auth;

public class ForbiddenException extends RuntimeException {

  private static final long serialVersionUID = -8580988746346369547L;

  public ForbiddenException(final String message) {
    super(message);
  }
}
