package com.automationanywhere.cognitive.gateway.alias.adapters.rest;

import com.automationanywhere.cognitive.gateway.alias.DomainLanguagesInfo;
import com.automationanywhere.cognitive.gateway.alias.ShareableDomainInfo;
import com.automationanywhere.cognitive.gateway.alias.model.Language;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectField;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectType;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class AliasDtoMapper {


  /**
   * Returns the list of supported Languages for each Domain in a list of
   * objects that can be externalized as JSON.
   *
   * @param domainLanguagesInfoList the service model representation of the Domain
   *     Languages information
   * @return the Domain Languages DTO
   */
  public List<DomainLanguagesDto> getDomainLanguagesDtoList(
          final List<DomainLanguagesInfo> domainLanguagesInfoList) {
    return domainLanguagesInfoList.stream()
            .map(DomainLanguagesDto::new)
            .collect(Collectors.toList());
  }

  /**
   * Returns the Shareable Domain DTO that may be externalized as JSON.
   *
   * @param shareableDomain the service model representation of the shareable
   *     domain
   * @return the Shareable Domain DTO
   */
  public ShareableDomainDto getShareableDomainDto(
          final ShareableDomainInfo shareableDomain) {
    return new ShareableDomainDto(shareableDomain);
  }

  public List<ProjectTypeDto> getProjectTypeDto(final List<ProjectType> projectTypeData) {
    return Optional.ofNullable(projectTypeData)
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getProjectTypeDto)
        .collect(Collectors.toList());
  }

  public ProjectTypeDto getProjectTypeDto(final ProjectType projectType) {
    return new ProjectTypeDto(
      projectType.id,
      projectType.name,
      projectType.description,
      getProjectFieldDto(projectType.fields),
      getLanguages(projectType.languages)
    );
  }

  public List<ProjectFieldDto> getProjectFieldDto(final List<ProjectField> projectTypeData) {
    return Optional.ofNullable(projectTypeData)
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getProjectFieldDto)
        .collect(Collectors.toList());
  }

  public ProjectFieldDto getProjectFieldDto(final ProjectField data) {
    return new ProjectFieldDto(
      data.id,
      data.name,
      data.description,
      data.fieldType,
      data.aliases,
      data.isDefaultSelected,
      data.dataType
    );
  }

  public List<LanguageDto> getLanguages(final List<Language> languages) {
    return Optional.ofNullable(languages)
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getLanguage)
        .collect(Collectors.toList());

  }

  public LanguageDto getLanguage(final Language language) {
    return new LanguageDto(
      language.id,
      language.code,
      language.name
    );
  }
}
