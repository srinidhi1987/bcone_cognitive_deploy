package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class VisionBotResponseData {
  public final Boolean success;
  public final String data;
  public String error;

  public VisionBotResponseData(
      @JsonProperty("success") final Boolean success,
      @JsonProperty("data") final String data,
      @JsonProperty("errors") final String errors) {
    this.success = success;
    this.data = data;
    this.error = error;
  }

  public VisionBotResponseData(final String data) {
    this.success = true;
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VisionBotResponseData that = (VisionBotResponseData) o;
    return Objects.equals(success, that.success) &&
        Objects.equals(data, that.data) &&
        Objects.equals(error, that.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, data, error);
  }

}
