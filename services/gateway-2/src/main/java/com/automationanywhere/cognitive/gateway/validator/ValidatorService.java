package com.automationanywhere.cognitive.gateway.validator;

import com.automationanywhere.cognitive.gateway.validator.microservices.validator.ValidatorMicroservice;
import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ValidatorService {

  private final ValidatorMicroservice validatorMicroservice;

  @Autowired
  public ValidatorService(final ValidatorMicroservice validatorMicroservice) {
    this.validatorMicroservice = validatorMicroservice;
  }

  @Async
  public CompletableFuture<List<ReviewedFileCountDetails>>
  getReviewedFileCountDetails(final Id organizationId) {
    return validatorMicroservice.getReviewedFileCountDetails(organizationId);
  }

  @Async
  public CompletableFuture<Long> getFailedVisionBotCount(
      final Id organizationId, final Id projectId
  ) {
    return validatorMicroservice.getFailedVisionBotCount(organizationId, projectId);
  }

  @Async
  public CompletableFuture<ProductionFileSummary> getProductionFileSummary(
      final Id organizationId, final Id projectId
  ) {
    return validatorMicroservice.getProductionFileSummary(organizationId, projectId);
  }

  @Async
  public CompletableFuture<Success> markFileInvalid(
      final Id organizationId,
      final Id projectId,
      final Id fileId,
      final String username,
      final List<InvalidFile> invalidFile
  ) {
    return validatorMicroservice.markFileInvalid(
        organizationId, projectId, fileId, username, invalidFile
    );
  }

  @Async
  public CompletableFuture<Success> unlockProject(
      final Id organizationId, final Id projectId, final String userName
  ) {
    return validatorMicroservice.unlockProject(organizationId, projectId, userName);
  }

  @Async
  public CompletableFuture<List<InvalidFileType>> getInvalidFileTypeList() {
    return validatorMicroservice.getInvalidFileTypeList();
  }

  @Async
  public CompletableFuture<Map<String, Object>> getFailedVbotdata(
      Id organizationId, Id projectId, final String userName
  ) {
    return validatorMicroservice.getFailedVbotData(organizationId, projectId, userName);
  }

  @Async
  public CompletableFuture<AccuracyProcessingDetails> getDocumentProcessingDetails(
      Id organizationId, Id typeId
  ) {
    return validatorMicroservice.getDocumentProcessingDetails(organizationId, typeId);
  }

  @Async
  public CompletableFuture<ProjectTotalsAccuracyDetails> getDashboardAccuracyDetails(
      Id organizationId, Id typeId
  ) {
    return validatorMicroservice.getDashboardAccuracyDetails(organizationId, typeId);
  }

  @Async
  public CompletableFuture<Success> sendCorrectDataValues(
      Id organizationId,
      Id projectId,
      Id fileId,
      String userName,
      String additionInfo
  ) {
    return validatorMicroservice
        .sendCorrectDataValues(organizationId, projectId, fileId, userName, additionInfo);
  }

  @Async
  public CompletableFuture<Success> patchCorrectDataValues(
      final Id organizationId,
      final Id projectId,
      final Id fileId,
      final String userName,
      final JsonPatchDocument jsonPatchDocument
  ) {
    return validatorMicroservice.patchCorrectDataValues(
        organizationId, projectId, fileId, userName, jsonPatchDocument
    );
  }

  @Async
  public CompletableFuture<FailedVbotStatus> updateValidationStatus(
      Id organizationId,
      Id projectId,
      Id visionBotId,
      Id fileId,
      Boolean validationStatus
  ) {
    return validatorMicroservice
        .updateValidationStatus(organizationId, projectId, visionBotId, fileId, validationStatus);
  }

  @Async
  public CompletableFuture<Void> keepAlive(
      final Id organizationId,
      final Id projectId,
      final Id fileId
  ) {
    return validatorMicroservice.keepAlive(organizationId, projectId, fileId);
  }
}
