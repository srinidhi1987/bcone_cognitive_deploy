package com.automationanywhere.cognitive.id.integer;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class LongId implements Id {

  public final long id;

  public LongId(final long id) {
    this.id = id;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      LongId stringId = (LongId) o;

      result = Objects.equals(id, stringId.id);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id) ^ 32;
  }
}
