package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

import com.automationanywhere.cognitive.gateway.visionbot.models.FieldValueDefinition;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchAction;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.visionbot.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.visionbot.models.ProcessedDocumentDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.StagingFilesSummary;
import com.automationanywhere.cognitive.gateway.visionbot.models.Summary;
import com.automationanywhere.cognitive.gateway.visionbot.models.TestSet;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidationDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorProperties;
import com.automationanywhere.cognitive.gateway.visionbot.models.ValidatorReviewDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBot;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotCount;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotElements;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotResponse;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotRunDetails;
import com.automationanywhere.cognitive.gateway.visionbot.models.VisionBotState;
import com.automationanywhere.cognitive.mappers.BaseMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class VisionBotDtoMapper extends BaseMapper {

  public VisionBotDto mapVisionBot(final VisionBot model) {
    return model == null ? null : new VisionBotDto(
        model.id, model.visionBotData, model.validationData
    );
  }

  public VisionBot mapVisionBot(final VisionBotDto dto) {
    return dto == null ? null : new VisionBot(
        dto.id, dto.visionBotData, dto.validationData
    );
  }

  public VisionBotDetailsDto mapVisionBotDetails(final VisionBotDetails model) {
    return model == null ? null : new VisionBotDetailsDto(
        model.id, model.name, model.organizationId, model.projectId, model.projectName,
        model.categoryId, model.categoryName, model.environment, model.status, model.running,
        model.lockedUserId, mapVisionBotRunDetails(model.botRunDetails),
        mapVisionBotRunDetails(model.productionBotRunDetails),
        mapVisionBotRunDetails(model.stagingBotRunDetails)
    );
  }

  private VisionBotRunDetailsDto mapVisionBotRunDetails(final VisionBotRunDetails model) {
    return model == null ? null : new VisionBotRunDetailsDto(
        model.id, model.startTime, model.endTime, model.runningMode, model.totalFiles,
        model.processedDocumentCount, model.failedDocumentCount, model.passedDocumentCount,
        model.documentProcessingAccuracy, model.fieldAccuracy,
        mapValidatorReviewDetails(model.validatorReviewDetails)
    );
  }

  private ValidatorReviewDetailsDto mapValidatorReviewDetails(final ValidatorReviewDetails model) {
    return model == null ? null : new ValidatorReviewDetailsDto(
        model.totalFilesReviewed, model.totalFilesMarkedInvalid,
        model.totalFailedFiles, model.averageReviewTime
    );
  }

  public FieldValueDefinition mapFieldValueDefinition(final FieldValueDefinitionDto data) {
    return data == null ? null : new FieldValueDefinition(
        data.id, data.fieldId, data.label, data.startsWith, data.endsWith,
        data.formatExpression, data.displayValue, data.isMultiline, data.bounds,
        data.valueBounds, data.fieldDirection, data.mergeRatio, data.similarityFactor,
        data.type, data.xDistant, data.isValueBoundAuto, data.valueType, data.dollarCurrencyUsed
    );
  }

  public ValidationDetailsDto mapValidationDetails(final ValidationDetails model) {
    return model == null ? null : new ValidationDetailsDto(
        model.validatorData == null
            ? emptyList()
            : model.validatorData.stream().map(this::mapValidatorDetails)
                .collect(Collectors.toList())
    );
  }

  private ValidatorDetailsDto mapValidatorDetails(final ValidatorDetails model) {
    return model == null ? null : new ValidatorDetailsDto(
        model.id, mapValidatorProperties(model.validatorProperties),
        model.typeOfData, model.serializedData
    );
  }

  private ValidatorPropertiesDto mapValidatorProperties(
      final ValidatorProperties model
  ) {
    return model == null ? null : new ValidatorPropertiesDto(
        model.filePath, model.staticListItems == null ? emptyList()
        : new ArrayList<>(model.staticListItems)
    );
  }

  public ValidationDetails mapValidationDetails(final ValidationDetailsDto data) {
    return data == null ? null : new ValidationDetails(
        data.validatorData == null
            ? emptyList()
            : data.validatorData.stream().map(this::mapValidatorDetails)
                .collect(Collectors.toList())
    );
  }

  private ValidatorDetails mapValidatorDetails(final ValidatorDetailsDto data) {
    return data == null ? null : new ValidatorDetails(
        data.id, mapValidatorProperties(data.validatorProperties),
        data.typeOfData, data.serializedData
    );
  }

  private ValidatorProperties mapValidatorProperties(
      final ValidatorPropertiesDto data
  ) {
    return data == null ? null : new ValidatorProperties(
        data.filePath, data.staticListItems == null ? emptyList()
        : new ArrayList<>(data.staticListItems)
    );
  }

  public SummaryDto mapSummary(final Summary model) {
    return model == null ? null : new SummaryDto(
        mapStagingFilesSummary(model.stagingSummary),
        mapVisionBotCount(model.botDetails)
    );
  }

  private VisionBotCountDto mapVisionBotCount(final VisionBotCount model) {
    return model == null ? null : new VisionBotCountDto(
        model.numberOfBotsCreated, model.numberOfStagingBots, model.numberOfProductionBots
    );
  }

  private StagingFilesSummaryDto mapStagingFilesSummary(final StagingFilesSummary model) {
    return model == null ? null : new StagingFilesSummaryDto(
        model.totalFilesCount, model.totalPassedFileCount, model.totalFailedFileCount,
        model.totalTestedFileCount, model.accuracy
    );
  }

  public TestSetDto mapTestSet(final TestSet model) {
    return model == null ? null : new TestSetDto(
        model.id, model.name, model.docItems
    );
  }

  public TestSet mapTestSet(final TestSetDto data) {
    return data == null ? null : new TestSet(
        data.id, data.name, data.docItems
    );
  }

  public VisionBotState mapVisionBotState(final VisionBotStateDto data) {
    return data == null ? null : new VisionBotState(data.state);
  }

  public VisionBotElements mapVisionBotStateDetails(final VisionBotElementDto data) {
    return data == null ? null : new VisionBotElements(data.state , data.visionBotList);
  }

  public ProcessedDocumentDetailsDto mapProcessedDocumentDetails(
      final ProcessedDocumentDetails model
  ) {
    return model == null ? null : new ProcessedDocumentDetailsDto(
        model.id, model.docId, model.processingStatus,
        model.totalFieldCount, model.failedFieldCount, model.passedFieldCount
    );
  }

  public VisionBotResponseDto mapVisionBotResponseDto(final VisionBotResponse visionBotResponse) {
    return visionBotResponse == null ? null
        : new VisionBotResponseDto(visionBotResponse.success, visionBotResponse.data,
            visionBotResponse.error);
  }

  public ProcessedDocumentDetails mapProcessedDocumentDetails(
      final ProcessedDocumentDetailsDto data
  ) {
    return data == null ? null : new ProcessedDocumentDetails(
        data.id, data.docId, data.processingStatus,
        data.totalFieldCount, data.failedFieldCount, data.passedFieldCount
    );
  }

  public JsonPatchDocument mapJsonPatchDocument(List<JsonPatchActionDto> actions) {
    return new JsonPatchDocument(
        ofNullable(actions).orElse(emptyList()).stream()
            .map(this::mapJsonPatchAction)
            .filter(Objects::nonNull)
            .collect(Collectors.toList())
    );
  }

  private JsonPatchAction mapJsonPatchAction(JsonPatchActionDto dto) {
    JsonPatchAction action = null;
    if (dto.op == JsonPatchActionOperation.ADD) {
      action = new JsonPatchActionAdd(dto.path, dto.value);

    } else if (dto.op == JsonPatchActionOperation.REPLACE) {
      action = new JsonPatchActionReplace(dto.path, dto.value);

    } else if (dto.op == JsonPatchActionOperation.REMOVE) {
      action = new JsonPatchActionRemove(dto.path);

    } else if (dto.op == JsonPatchActionOperation.TEST) {
      action = new JsonPatchActionTest(dto.path, dto.value);

    } else if (dto.op == JsonPatchActionOperation.COPY) {
      action = new JsonPatchActionCopy(dto.path, dto.from);

    } else if (dto.op == JsonPatchActionOperation.MOVE) {
      action = new JsonPatchActionMove(dto.path, dto.from);
    }
    return action;
  }
}
