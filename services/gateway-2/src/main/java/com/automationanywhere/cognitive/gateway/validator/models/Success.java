package com.automationanywhere.cognitive.gateway.validator.models;

import java.util.Objects;

public class Success {

  public final boolean success;

  public Success(
      final boolean success
  ) {
    this.success = success;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      Success that = (Success) o;

      result = Objects.equals(success, that.success);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(success) ^ 32;
  }
}
