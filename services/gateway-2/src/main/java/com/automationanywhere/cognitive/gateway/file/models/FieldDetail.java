package com.automationanywhere.cognitive.gateway.file.models;

import java.math.BigDecimal;
import java.util.Objects;

public class FieldDetail {

  public final String fieldId;
  public final BigDecimal foundInTotalDocs;

  public FieldDetail(
      final String fieldId,
      final BigDecimal foundInTotalDocs
  ) {
    this.fieldId = fieldId;
    this.foundInTotalDocs = foundInTotalDocs;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FieldDetail fieldDetail = (FieldDetail) o;

      result = Objects.equals(fieldId, fieldDetail.fieldId)
        && Objects.equals(foundInTotalDocs, fieldDetail.foundInTotalDocs);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fieldId)
      ^ Objects.hashCode(foundInTotalDocs);
  }
}

