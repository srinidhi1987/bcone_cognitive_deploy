package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class ProjectImportRequest {
  public final ArchiveDetails archive;
  public final Id userId;
  public final String option;

  public ProjectImportRequest(
      final ArchiveDetails archive, final Id userId,
      final String option
  ) {
    this.archive = archive;
    this.userId = userId;
    this.option = option;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ProjectImportRequest that = (ProjectImportRequest) o;

      result = Objects.equals(archive, that.archive)
          && Objects.equals(option, that.option)
          && Objects.equals(userId, that.userId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(archive)
        ^ Objects.hashCode(option)
        ^ Objects.hashCode(userId);
  }
}
