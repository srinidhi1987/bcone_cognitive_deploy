package com.automationanywhere.cognitive.gateway.project.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class CustomFieldDetailDto {

  public final Id id;
  public final String name;
  public final String type;

  public CustomFieldDetailDto(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("type") final String type
  ) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CustomFieldDetailDto customFieldDetail = (CustomFieldDetailDto) o;

      result = Objects.equals(id, customFieldDetail.id)
          && Objects.equals(name, customFieldDetail.name)
          && Objects.equals(type, customFieldDetail.type);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(type);
  }
}

