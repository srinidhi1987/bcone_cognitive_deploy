package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

public class ProjectDetails {

  public final Id id;
  public final String name;
  public final String description;
  public final Id organizationId;
  public final Id projectTypeId;
  public final String projectType;
  public final Integer confidenceThreshold;
  public final Integer numberOfFiles;
  public final Integer numberOfCategories;
  public final Integer unprocessedFileCount;
  public final String primaryLanguage;
  public final Integer accuracyPercentage;
  public final Integer visionBotCount;
  public final Integer currentTrainedPercentage;
  public final List<Category> categories;
  public final FieldDetails fields;
  public final String projectState;
  public final String environment;
  public final OffsetDateTime updatedAt;
  public final OffsetDateTime createdAt;
  public final List<OCREngineDetail> ocrEngineDetails;
  public final Integer versionId;

  public ProjectDetails(
      final Id id,
      final String name,
      final String description,
      final Id organizationId,
      final Id projectTypeId,
      final String projectType,
      final Integer confidenceThreshold,
      final Integer numberOfFiles,
      final Integer numberOfCategories,
      final Integer unprocessedFileCount,
      final String primaryLanguage,
      final Integer accuracyPercentage,
      final Integer visionBotCount,
      final Integer currentTrainedPercentage,
      final List<Category> categories,
      final FieldDetails fields,
      final String projectState,
      final String environment,
      final OffsetDateTime updatedAt,
      final OffsetDateTime createdAt,
      final List<OCREngineDetail> ocrEngineDetails,
      final Integer versionId
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.organizationId = organizationId;
    this.projectTypeId = projectTypeId;
    this.projectType = projectType;
    this.confidenceThreshold = confidenceThreshold;
    this.numberOfFiles = numberOfFiles;
    this.numberOfCategories = numberOfCategories;
    this.unprocessedFileCount = unprocessedFileCount;
    this.primaryLanguage = primaryLanguage;
    this.accuracyPercentage = accuracyPercentage;
    this.visionBotCount = visionBotCount;
    this.currentTrainedPercentage = currentTrainedPercentage;
    this.categories = categories;
    this.fields = fields;
    this.projectState = projectState;
    this.environment = environment;
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;
    this.ocrEngineDetails = ocrEngineDetails;
    this.versionId = versionId;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProjectDetails projectDetails = (ProjectDetails) o;

      result = Objects.equals(id, projectDetails.id)
          && Objects.equals(name, projectDetails.name)
          && Objects.equals(description, projectDetails.description)
          && Objects.equals(organizationId, projectDetails.organizationId)
          && Objects.equals(projectTypeId, projectDetails.projectTypeId)
          && Objects.equals(projectType, projectDetails.projectType)
          && Objects.equals(confidenceThreshold, projectDetails.confidenceThreshold)
          && Objects.equals(numberOfFiles, projectDetails.numberOfFiles)
          && Objects.equals(numberOfCategories, projectDetails.numberOfCategories)
          && Objects.equals(unprocessedFileCount, projectDetails.unprocessedFileCount)
          && Objects.equals(primaryLanguage, projectDetails.primaryLanguage)
          && Objects.equals(accuracyPercentage, projectDetails.accuracyPercentage)
          && Objects.equals(visionBotCount, projectDetails.visionBotCount)
          && Objects.equals(currentTrainedPercentage, projectDetails.currentTrainedPercentage)
          && Objects.equals(categories, projectDetails.categories)
          && Objects.equals(fields, projectDetails.fields)
          && Objects.equals(projectState, projectDetails.projectState)
          && Objects.equals(environment, projectDetails.environment)
          && Objects.equals(updatedAt, projectDetails.updatedAt)
          && Objects.equals(createdAt, projectDetails.createdAt)
          && Objects.equals(ocrEngineDetails, projectDetails.ocrEngineDetails)
          && Objects.equals(versionId, projectDetails.versionId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(organizationId)
        ^ Objects.hashCode(projectTypeId)
        ^ Objects.hashCode(projectType)
        ^ Objects.hashCode(confidenceThreshold)
        ^ Objects.hashCode(numberOfFiles)
        ^ Objects.hashCode(numberOfCategories)
        ^ Objects.hashCode(unprocessedFileCount)
        ^ Objects.hashCode(primaryLanguage)
        ^ Objects.hashCode(accuracyPercentage)
        ^ Objects.hashCode(visionBotCount)
        ^ Objects.hashCode(currentTrainedPercentage)
        ^ Objects.hashCode(categories)
        ^ Objects.hashCode(fields)
        ^ Objects.hashCode(projectState)
        ^ Objects.hashCode(environment)
        ^ Objects.hashCode(updatedAt)
        ^ Objects.hashCode(createdAt)
        ^ Objects.hashCode(ocrEngineDetails)
        ^ Objects.hashCode(versionId);
  }
}

