package com.automationanywhere.cognitive.gateway.file.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class FileDetails {

  public final Id fileId;
  public final Id projectId;
  public final String fileName;
  public final String fileLocation;
  public final Integer fileSize;
  public final Integer fileHeight;
  public final Integer fileWidth;
  public final String format;
  public final Boolean processed;
  public final Id classificationId;
  public final Id uploadRequestId;
  public final Id layoutId;
  public final String isProduction;

  public FileDetails(
      final Id fileId,
      final Id projectId,
      final String fileName,
      final String fileLocation,
      final Integer fileSize,
      final Integer fileHeight,
      final Integer fileWidth,
      final String format,
      final Boolean processed,
      final Id classificationId,
      final Id uploadRequestId,
      final Id layoutId,
      final String isProduction
  ) {
    this.fileId = fileId;
    this.projectId = projectId;
    this.fileName = fileName;
    this.fileLocation = fileLocation;
    this.fileSize = fileSize;
    this.fileHeight = fileHeight;
    this.fileWidth = fileWidth;
    this.format = format;
    this.processed = processed;
    this.classificationId = classificationId;
    this.uploadRequestId = uploadRequestId;
    this.layoutId = layoutId;
    this.isProduction = isProduction;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileDetails fileDetails = (FileDetails) o;

      result = Objects.equals(fileId, fileDetails.fileId)
        && Objects.equals(projectId, fileDetails.projectId)
        && Objects.equals(fileName, fileDetails.fileName)
        && Objects.equals(fileLocation, fileDetails.fileLocation)
        && Objects.equals(fileSize, fileDetails.fileSize)
        && Objects.equals(fileHeight, fileDetails.fileHeight)
        && Objects.equals(fileWidth, fileDetails.fileWidth)
        && Objects.equals(format, fileDetails.format)
        && Objects.equals(processed, fileDetails.processed)
        && Objects.equals(classificationId, fileDetails.classificationId)
        && Objects.equals(uploadRequestId, fileDetails.uploadRequestId)
        && Objects.equals(layoutId, fileDetails.layoutId)
        && Objects.equals(isProduction, fileDetails.isProduction);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
      ^ Objects.hashCode(projectId)
      ^ Objects.hashCode(fileName)
      ^ Objects.hashCode(fileLocation)
      ^ Objects.hashCode(fileSize)
      ^ Objects.hashCode(fileHeight)
      ^ Objects.hashCode(fileWidth)
      ^ Objects.hashCode(format)
      ^ Objects.hashCode(processed)
      ^ Objects.hashCode(classificationId)
      ^ Objects.hashCode(uploadRequestId)
      ^ Objects.hashCode(layoutId)
      ^ Objects.hashCode(isProduction);
  }
}

