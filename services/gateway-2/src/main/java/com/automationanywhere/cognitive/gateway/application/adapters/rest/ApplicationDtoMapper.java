package com.automationanywhere.cognitive.gateway.application.adapters.rest;

import com.automationanywhere.cognitive.gateway.application.model.Acknowledgement;
import com.automationanywhere.cognitive.gateway.application.model.Configuration;
import com.automationanywhere.cognitive.gateway.application.model.Handshake;
import com.automationanywhere.cognitive.gateway.application.model.Registration;
import org.springframework.stereotype.Component;

@Component
public class ApplicationDtoMapper {

  public Registration mapRegistration(final RegistrationDto data) {
    return data == null ? null : new Registration(
        data.controlRoomUrl, data.controlRoomVersion, data.appId
    );
  }

  public HandshakeDto mapHandshake(final Handshake model) {
    return model == null ? null : new HandshakeDto(
        model.appVersion, model.publicKey, model.routingName
    );
  }

  public Acknowledgement mapAcknowledgement(final AcknowledgementDto data) {
    return data == null ? null : new Acknowledgement(data.appId);
  }

  public ConfigurationDto mapConfiguration(final Configuration model) {
    return model == null ? null : new ConfigurationDto(
        model.crUrl, model.routingName
    );
  }
}
