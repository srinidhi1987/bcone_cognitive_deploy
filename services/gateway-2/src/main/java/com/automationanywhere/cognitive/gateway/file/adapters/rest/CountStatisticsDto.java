package com.automationanywhere.cognitive.gateway.file.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class CountStatisticsDto {

  public final Id projectId;
  public final Id categoryId;
  public final Integer fileCount;
  public final EnvironmentCountStatisticsDto stagingFileCount;
  public final EnvironmentCountStatisticsDto productionFileCount;

  public CountStatisticsDto(
      @JsonProperty("projectId") final Id projectId,
      @JsonProperty("categoryId") final Id categoryId,
      @JsonProperty("fileCount") final Integer fileCount,
      @JsonProperty("stagingFileCount") final EnvironmentCountStatisticsDto stagingFileCount,
      @JsonProperty("productionFileCount") final EnvironmentCountStatisticsDto productionFileCount
  ) {
    this.projectId = projectId;
    this.categoryId = categoryId;
    this.fileCount = fileCount;
    this.stagingFileCount = stagingFileCount;
    this.productionFileCount = productionFileCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      CountStatisticsDto countStatistics = (CountStatisticsDto) o;

      result = Objects.equals(projectId, countStatistics.projectId)
        && Objects.equals(categoryId, countStatistics.categoryId)
        && Objects.equals(fileCount, countStatistics.fileCount)
        && Objects.equals(stagingFileCount, countStatistics.stagingFileCount)
        && Objects.equals(productionFileCount, countStatistics.productionFileCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
      ^ Objects.hashCode(categoryId)
      ^ Objects.hashCode(fileCount)
      ^ Objects.hashCode(stagingFileCount)
      ^ Objects.hashCode(productionFileCount);
  }
}

