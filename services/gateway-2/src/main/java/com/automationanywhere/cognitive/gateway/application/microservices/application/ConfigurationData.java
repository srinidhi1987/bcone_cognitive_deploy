package com.automationanywhere.cognitive.gateway.application.microservices.application;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfigurationData {

  public final String crUrl;
  public final String routingName;

  public ConfigurationData(
      @JsonProperty("crUrl") final String crUrl,
      @JsonProperty("routingName") final String routingName
  ) {
    this.crUrl = crUrl;
    this.routingName = routingName;
  }
}
