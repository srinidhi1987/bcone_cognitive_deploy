package com.automationanywhere.cognitive.gateway.visionbot.models;

import java.util.Objects;

public class VisionBotCount {

  public final Integer numberOfBotsCreated;
  public final Integer numberOfStagingBots;
  public final Integer numberOfProductionBots;

  public VisionBotCount(
      final Integer numberOfBotsCreated,
      final Integer numberOfStagingBots,
      final Integer numberOfProductionBots
  ) {
    this.numberOfBotsCreated = numberOfBotsCreated;
    this.numberOfStagingBots = numberOfStagingBots;
    this.numberOfProductionBots = numberOfProductionBots;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBotCount visionBotCount = (VisionBotCount) o;

      result = Objects.equals(numberOfBotsCreated, visionBotCount.numberOfBotsCreated)
        && Objects.equals(numberOfStagingBots, visionBotCount.numberOfStagingBots)
        && Objects.equals(numberOfProductionBots, visionBotCount.numberOfProductionBots);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(numberOfBotsCreated)
      ^ Objects.hashCode(numberOfStagingBots)
      ^ Objects.hashCode(numberOfProductionBots);
  }
}

