package com.automationanywhere.cognitive.resourcelock;

public class Resource {
  public static final String DEFAULT_ACTIVITY_NAME = "activity";

  public final String activityName;
  public final String path;

  public Resource(final String path) {
    this(DEFAULT_ACTIVITY_NAME, path);
  }

  protected Resource(final String activityName, final String path) {
    if (activityName == null) {
      throw new IllegalArgumentException();
    }
    if (path == null) {
      throw new IllegalArgumentException();
    }
    this.activityName = activityName;
    this.path = path;
  }
}
