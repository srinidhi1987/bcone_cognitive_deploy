package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FailedVbotStatusDto {

  public final boolean success;
  public final int count;
  public final int error;

  public FailedVbotStatusDto(
      @JsonProperty("success") final boolean success,
      @JsonProperty("count") final int count,
      @JsonProperty("errors") final int error
  ) {
    this.success = success;
    this.count = count;
    this.error = error;
  }

  /**
   * Returns the success.
   *
   * @return the value of success
   */
  public boolean isSuccess() {
    return success;
  }

  /**
   * Returns the count.
   *
   * @return the value of count
   */
  public int getCount() {
    return count;
  }

  /**
   * Returns the error.
   *
   * @return the value of error
   */
  public int getError() {
    return error;
  }
}
