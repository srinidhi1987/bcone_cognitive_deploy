package com.automationanywhere.cognitive.gateway.report.microservice.report.model;

public class FieldAccuracyData {

  private String name;
  private long accuracyPercent;

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Returns the percentage of field accuracy.
   *
   * @return the value of accuracyPercent
   */
  public long getAccuracyPercent() {
    return accuracyPercent;
  }

  /**
   * Sets the percentage of field accuracy.
   *
   * @param accuracyPercent the accuracyPercent to be set
   */
  public void setAccuracyPercent(long accuracyPercent) {
    this.accuracyPercent = accuracyPercent;
  }
}
