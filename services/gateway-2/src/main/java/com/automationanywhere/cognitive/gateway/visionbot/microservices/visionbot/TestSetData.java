package com.automationanywhere.cognitive.gateway.visionbot.microservices.visionbot;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class TestSetData {

  public final Id id;
  public final String name;
  public final String docItems;

  public TestSetData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("docItems") final String docItems
  ) {
    this.id = id;
    this.name = name;
    this.docItems = docItems;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      TestSetData testSet = (TestSetData) o;

      result = Objects.equals(id, testSet.id)
        && Objects.equals(name, testSet.name)
        && Objects.equals(docItems, testSet.docItems);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(name)
      ^ Objects.hashCode(docItems);
  }
}

