package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FieldAccuracyDashboardDetailsDto {

  private final String fieldId;
  private final double accuracyPercent;

  public FieldAccuracyDashboardDetailsDto(
      @JsonProperty("fieldId") String fieldId,
      @JsonProperty("accuracyPercent") double accuracyPercent
  ) {
    this.fieldId = fieldId;
    this.accuracyPercent = accuracyPercent;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the accuracyPercent.
   *
   * @return the value of accuracyPercent
   */
  public double getAccuracyPercent() {
    return accuracyPercent;
  }
}
