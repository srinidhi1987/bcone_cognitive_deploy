package com.automationanywhere.cognitive.gateway.project.microservices.project;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class OCREngineDetailData {

  public final Id id;
  public final String name;
  public final String engineType;

  public OCREngineDetailData(
      @JsonProperty("id") final Id id,
      @JsonProperty("name") final String name,
      @JsonProperty("engineType") final String engineType
  ) {
    this.id = id;
    this.name = name;
    this.engineType = engineType;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      OCREngineDetailData oCREngineDetails = (OCREngineDetailData) o;

      result = Objects.equals(id, oCREngineDetails.id)
          && Objects.equals(engineType, oCREngineDetails.engineType)
          && Objects.equals(name, oCREngineDetails.name);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(engineType)
        ^ Objects.hashCode(name);
  }
}

