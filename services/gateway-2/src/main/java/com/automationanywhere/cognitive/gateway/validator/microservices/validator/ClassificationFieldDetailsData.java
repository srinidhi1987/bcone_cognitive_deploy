package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClassificationFieldDetailsData {

  private final String fieldId;
  private final double representationPercent;

  public ClassificationFieldDetailsData(
      @JsonProperty("fieldId") String fieldId,
      @JsonProperty("representationPercent") double representationPercent
  ) {
    this.fieldId = fieldId;
    this.representationPercent = representationPercent;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the representationPercent.
   *
   * @return the value of representationPercent
   */
  public double getRepresentationPercent() {
    return representationPercent;
  }
}
