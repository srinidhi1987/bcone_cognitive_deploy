package com.automationanywhere.cognitive.restclient.resttemplate;

import java.io.IOException;
import java.net.URI;
import org.apache.http.client.HttpClient;
import org.apache.http.nio.client.HttpAsyncClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.AsyncClientHttpRequest;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;

/**
 * The factory produces client http requests that are able to propagate log contexts
 * to the thread where a response handling will happen.
 */
public class LogContextPropagatingHttpComponentsAsyncClientHttpRequestFactory extends
    HttpComponentsAsyncClientHttpRequestFactory {

  public LogContextPropagatingHttpComponentsAsyncClientHttpRequestFactory(
      final HttpClient httpClient, final HttpAsyncClient asyncClient
  ) {
    super(httpClient, asyncClient);
  }

  @Override
  public AsyncClientHttpRequest createAsyncRequest(
      final URI uri, final HttpMethod httpMethod
  ) throws IOException {
    return new LogContextPropagatingAsyncClientHttpRequest(
        super.createAsyncRequest(uri, httpMethod)
    );
  }
}
