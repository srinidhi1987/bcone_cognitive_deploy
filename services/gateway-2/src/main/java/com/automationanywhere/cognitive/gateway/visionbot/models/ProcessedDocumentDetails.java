package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class ProcessedDocumentDetails {

  public final Id id;
  public final Id docId;
  public final String processingStatus;
  public final String totalFieldCount;
  public final String failedFieldCount;
  public final String passedFieldCount;

  public ProcessedDocumentDetails(
      final Id id,
      final Id docId,
      final String processingStatus,
      final String totalFieldCount,
      final String failedFieldCount,
      final String passedFieldCount
  ) {
    this.id = id;
    this.docId = docId;
    this.processingStatus = processingStatus;
    this.totalFieldCount = totalFieldCount;
    this.failedFieldCount = failedFieldCount;
    this.passedFieldCount = passedFieldCount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      ProcessedDocumentDetails processedDocumentDetails = (ProcessedDocumentDetails) o;

      result = Objects.equals(id, processedDocumentDetails.id)
        && Objects.equals(docId, processedDocumentDetails.docId)
        && Objects.equals(processingStatus, processedDocumentDetails.processingStatus)
        && Objects.equals(totalFieldCount, processedDocumentDetails.totalFieldCount)
        && Objects.equals(failedFieldCount, processedDocumentDetails.failedFieldCount)
        && Objects.equals(passedFieldCount, processedDocumentDetails.passedFieldCount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(docId)
      ^ Objects.hashCode(processingStatus)
      ^ Objects.hashCode(totalFieldCount)
      ^ Objects.hashCode(failedFieldCount)
      ^ Objects.hashCode(passedFieldCount);
  }
}

