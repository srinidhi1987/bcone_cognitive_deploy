package com.automationanywhere.cognitive.gateway.alias.microservices.alias;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.alias.DomainLanguagesInfo;
import com.automationanywhere.cognitive.gateway.alias.model.Language;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectField;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectType;
import com.automationanywhere.cognitive.gateway.alias.model.ShareableDomain;
import com.automationanywhere.cognitive.restclient.RequestDetails.Builder;
import com.automationanywhere.cognitive.restclient.RequestDetails.Types;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

@Repository
public class AliasMicroservice {

  private final AliasDataMapper mapper;
  private final RestClient restClient;
  private final ApplicationConfiguration cfg;

  @Autowired
  public AliasMicroservice(
      final AliasDataMapper mapper,
      final RestClient restClient,
      final ApplicationConfiguration cfg
  ) {
    this.mapper = mapper;
    this.restClient = restClient;
    this.cfg = cfg;
  }

  /**
   * Returns the supported languages for all Domains in the Future.
   *
   * @return the supported languages for all Domains in the Future
   */
  @Async("microserviceExecutor")
  public CompletableFuture<List<DomainLanguagesData>> getLanguagesForDomains() {
    Builder builder = Builder
            .get(cfg.aliasServiceURL())
            .request(cfg.getDomainsLanguagesUri())
            .accept(Types.JSON);

    return restClient.send(builder.build(),
            new ParameterizedTypeReference<List<DomainLanguagesData>>() {
            }
    );
  }

  /**
   * Returns the JSON representation of the Domain in the Future without
   * mapping it to a DTO.  This so the gateway can remain agnostic to changes
   * in the structure of the domain document.
   *
   * @param name the domain name such as "Invoices"
   * @return the JSON representation of the Domain without mapping it to a DTO
   */
  @Async("microserviceExecutor")
  public CompletableFuture<String> getDomains(final String name) {
    Builder builder = Builder
            .get(cfg.aliasServiceURL())
            .request(cfg.getDomainsUri())
            .param(name)
            .accept(Types.JSON);

    return restClient.send(builder.build(),
            new ParameterizedTypeReference<String>() {
            }
    );
  }

  /**
   * Sends the provided domain JSON payload to the remote alias service.
   *
   * @param domainJson the domain JSON
   * @return a void response
   */
  @Async("microserviceExecutor")
  public CompletableFuture<Void> importDomains(final String domainJson) {
    Builder builder = Builder
            .post(cfg.aliasServiceURL())
            .request(cfg.importDomainsUri())
            .content(domainJson, Types.JSON);

    return restClient.send(builder.build(),
            new ParameterizedTypeReference<Void>() {
            }
    );
  }

  public CompletableFuture<List<ProjectType>> getAllProjectTypes(final String langId) {
    Builder builder = Builder
        .get(cfg.aliasServiceURL())
        .request(cfg.getAllProjectTypeUri())
        .param(langId)
        .accept(Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<List<ProjectTypeData>>() {
        }
    ).thenApply(mapper::getProjectTypes);
  }

  public CompletableFuture<ProjectType> getProjectType(
      final String projectTypeId, final String langId
  ) {
    Builder builder = Builder.get(cfg.aliasServiceURL())
        .request(cfg.getProjectTypeUri())
        .param(projectTypeId)
        .param(langId)
        .accept(Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<ProjectTypeData>() {
        }
    ).thenApply(mapper::getProjectType);
  }


  public CompletableFuture<List<ProjectField>> getProjectFields(
      final String projectTypeId,
      final String langId
  ) {
    Builder builder = Builder.get(cfg.aliasServiceURL())
        .request(cfg.getProjectFieldsUri())
        .param(projectTypeId)
        .param(langId)
        .accept(Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<List<ProjectFieldData>>() {
        }
    ).thenApply(mapper::getProjectFields);
  }

  public CompletableFuture<ProjectField> getField(final String fieldId, final String langId) {
    Builder builder = Builder.get(cfg.aliasServiceURL())
        .request(cfg.getFieldUri())
        .param(fieldId)
        .param(langId)
        .accept(Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<ProjectFieldData>() {
        }
    ).thenApply(mapper::getProjectField);
  }

  public CompletableFuture<List<Language>> getLanguages() {
    return restClient.send(
        Builder.get(cfg.aliasServiceURL())
            .request(cfg.getLanguagesUri())
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<List<LanguageData>>() {
        }
    ).thenApply(mapper::getLanguages);
  }

  public CompletableFuture<Language> getLanguage(final String langId) {
    return restClient.send(
        Builder.get(cfg.aliasServiceURL())
            .request(cfg.getLanguageUri())
            .param(langId)
            .accept(Types.JSON)
            .build(),
        new ParameterizedTypeReference<LanguageData>() {
        }
    ).thenApply(mapper::getLanguage);
  }

  public CompletableFuture<List<String>> getAliases(final String fieldId, final String langId) {
    Builder builder = Builder.get(cfg.aliasServiceURL())
        .request(cfg.getAliasesUri())
        .param(fieldId)
        .param(langId)
        .accept(Types.JSON);

    return restClient.send(builder.build(),
        new ParameterizedTypeReference<List<String>>() {
        }
    );
  }
}