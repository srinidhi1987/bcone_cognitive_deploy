package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProductionFileSummaryData {

  public final Id projectId;
  public final long processedFiles;
  public final long successFiles;
  public final long reviewFiles;
  public final long invalidFiles;
  public final long pendingForReview;
  public final double accuracy;

  public ProductionFileSummaryData(
      @JsonProperty("projectId") final Id projectId,
      @JsonProperty("processedFiles") final long processedFiles,
      @JsonProperty("successFiles") final long successFiles,
      @JsonProperty("reviewFiles") final long reviewFiles,
      @JsonProperty("invalidFiles") final long invalidFiles,
      @JsonProperty("pendingForReview") final long pendingForReview,
      @JsonProperty("accuracy") final double accuracy
  ) {
    this.projectId = projectId;
    this.processedFiles = processedFiles;
    this.successFiles = successFiles;
    this.reviewFiles = reviewFiles;
    this.invalidFiles = invalidFiles;
    this.pendingForReview = pendingForReview;
    this.accuracy = accuracy;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      ProductionFileSummaryData that = (ProductionFileSummaryData) o;

      result = Objects.equals(processedFiles, that.processedFiles)
          && Objects.equals(successFiles, that.successFiles)
          && Objects.equals(reviewFiles, that.reviewFiles)
          && Objects.equals(invalidFiles, that.invalidFiles)
          && Objects.equals(pendingForReview, that.pendingForReview)
          && Objects.equals(accuracy, that.accuracy)
          && Objects.equals(projectId, that.projectId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(projectId)
        ^ Objects.hashCode(processedFiles)
        ^ Objects.hashCode(successFiles)
        ^ Objects.hashCode(reviewFiles)
        ^ Objects.hashCode(invalidFiles)
        ^ Objects.hashCode(pendingForReview)
        ^ Objects.hashCode(accuracy);
  }
}
