package com.automationanywhere.cognitive.gateway.report.microservice.report.model;

import java.util.List;

public class ValidationData {

  private List<FieldValidationData> fields;
  private List<GroupTimeSpentData> categories;

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  public List<FieldValidationData> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the fields to be set
   */
  public void setFields(List<FieldValidationData> fields) {
    this.fields = fields;
  }

  /**
   * Returns the categories.
   *
   * @return the value of categories
   */
  public List<GroupTimeSpentData> getCategories() {
    return categories;
  }

  /**
   * Sets the categories.
   *
   * @param categories the categories to be set
   */
  public void setCategories(List<GroupTimeSpentData> categories) {
    this.categories = categories;
  }
}
