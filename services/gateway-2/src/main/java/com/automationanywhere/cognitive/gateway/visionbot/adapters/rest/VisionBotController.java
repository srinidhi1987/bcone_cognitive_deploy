package com.automationanywhere.cognitive.gateway.visionbot.adapters.rest;

import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.USERNAME_HEADER_NAME;
import static com.automationanywhere.cognitive.validation.Schemas.EXPORT_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.FIELD_VALUE_DEFINITION;
import static com.automationanywhere.cognitive.validation.Schemas.JSON_PATCH_DOC;
import static com.automationanywhere.cognitive.validation.Schemas.MULTIPLE_VISION_BOT_STATE;
import static com.automationanywhere.cognitive.validation.Schemas.PATCH;
import static com.automationanywhere.cognitive.validation.Schemas.PATCH_V1;
import static com.automationanywhere.cognitive.validation.Schemas.PROCESSED_DOCUMENT_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.PROCESSED_DOCUMENT_DETAILS_RESULT;
import static com.automationanywhere.cognitive.validation.Schemas.STANDARD_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.SUMMARY;
import static com.automationanywhere.cognitive.validation.Schemas.TEST_SET;
import static com.automationanywhere.cognitive.validation.Schemas.TEST_SET_RESULT;
import static com.automationanywhere.cognitive.validation.Schemas.V1;
import static com.automationanywhere.cognitive.validation.Schemas.VALIDATION_DETAILS;
import static com.automationanywhere.cognitive.validation.Schemas.VISION_BOT;
import static com.automationanywhere.cognitive.validation.Schemas.VISION_BOT_CHANGES;
import static com.automationanywhere.cognitive.validation.Schemas.VISION_BOT_DATA_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.VISION_BOT_DETAILS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.VISION_BOT_STATE;

import com.automationanywhere.cognitive.gateway.visionbot.VisionBotService;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/organizations")
public class VisionBotController {

  private final VisionBotDtoMapper mapper;
  private final VisionBotService visionBotService;

  @Autowired
  public VisionBotController(
      final VisionBotDtoMapper mapper,
      final VisionBotService visionBotService
  ) {
    this.mapper = mapper;
    this.visionBotService = visionBotService;
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/Unlock",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Id>> unlock(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final Id userId
  ) {
    return visionBotService.unlock(organizationId, projectId, categoryId, visionId, userId)
        .thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/Edit",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT
  )
  public CompletableFuture<StandardResponse<VisionBotDto>> getVisionBotForEdit(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final String userName
  ) {
    return visionBotService.getVisionBotForEdit(
        organizationId,
        projectId,
        categoryId,
        userName
    ).thenApply(model -> new StandardResponse<>(
        mapper.mapVisionBot(model)
    ));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT
  )
  public CompletableFuture<StandardResponse<VisionBotDto>> getVisionBotById(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId
  ) {
    return visionBotService.getVisionBotById(organizationId, projectId, categoryId, visionBotId)
        .thenApply(m -> new StandardResponse<>(mapper.mapVisionBot(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}",
      method = RequestMethod.PATCH,
      consumes = {PATCH_V1, PATCH},
      produces = {PATCH, PATCH_V1}
  )
  @SchemaValidation(
      input = JSON_PATCH_DOC,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Boolean>> patchVisionBot(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final Id userId,
      @RequestBody final List<JsonPatchActionDto> actions
  ) {

    return visionBotService.patchVisionBot(
        organizationId, projectId, categoryId, visionBotId,userId, mapper.mapJsonPatchDocument(actions)
    ).thenApply(r -> new StandardResponse<Boolean>(r, null, null));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}"
          + "/layout/{layoutid}/vbotdocument",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> getVisionBotByLayout(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("layoutid") final Id layoutId
  ) {
    return visionBotService.getVisionBotByLayout(
        organizationId, projectId, categoryId, visionBotId, layoutId
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}"
          + "/layout/{layoutid}/pages/{pageindex}/vbotdocument",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> getVisionBotByLayoutAndPage(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("layoutid") final Id layoutId,
      @PathVariable("pageindex") final int pageIndex
  ) {
    return visionBotService.getVisionBotByLayoutAndPage(
        organizationId, projectId, categoryId, visionBotId, layoutId, pageIndex
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT
  )
  public CompletableFuture<StandardResponse<VisionBotDto>> getVisionBot(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId
  ) {
    return visionBotService.getVisionBot(organizationId, projectId, categoryId)
        .thenApply(m -> new StandardResponse<>(mapper.mapVisionBot(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbotmetadata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> getVisionBotMetadata(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId
  ) {
    return visionBotService.getVisionBotMetadata(organizationId, projectId, categoryId)
        .thenApply(l -> new StandardResponse<>(mapper.mapList(l, mapper::mapVisionBotDetails)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/visionbotdata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> getProjectVisionBot(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return visionBotService.getProjectVisionBot(organizationId, projectId)
        .thenApply(l -> new StandardResponse<>(mapper.mapList(l, mapper::mapVisionBotDetails)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/visionbotmetadata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>> getProjectVisionBotMetadata(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return visionBotService.getProjectVisionBotMetadata(organizationId, projectId)
        .thenApply(l -> new StandardResponse<>(mapper.mapList(l, mapper::mapVisionBotDetails)));
  }

  @RequestMapping(
      path = "/{orgid}/visionbotdata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT_DATA_LIST
  )
  public CompletableFuture<StandardResponse<List<Map<String, Object>>>>
  getOrganizationVisionBot(@PathVariable("orgid") final Id organizationId
  ) {
    return visionBotService.getOrganizationVisionBot(organizationId)
        .thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/visionbotdata/{vbotid}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>>
  getOrganizationVisionBotById(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("vbotid") final Id visionBotId
  ) {
    return visionBotService.getOrganizationVisionBotById(organizationId, visionBotId)
        .thenApply(l -> new StandardResponse<>(mapper.mapList(l, mapper::mapVisionBotDetails)));
  }

  @RequestMapping(
      path = "/{orgid}/visionbotmetadata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = VISION_BOT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<VisionBotDetailsDto>>>
  getOrganizationVisionBotMetadata(@PathVariable("orgid") final Id organizationId
  ) {
    return visionBotService.getOrganizationVisionBotMetadata(organizationId)
        .thenApply(l -> new StandardResponse<>(mapper.mapList(l, mapper::mapVisionBotDetails)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = VISION_BOT_CHANGES,
      output = VISION_BOT
  )
  public CompletableFuture<StandardResponse<VisionBotDto>> updateVisionBot(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @RequestBody final VisionBotDto visionBot
  ) {
    return visionBotService.updateVisionBot(
        organizationId, projectId, categoryId,
        mapper.mapVisionBot(visionBot)
    ).thenApply(m -> new StandardResponse<>(mapper.mapVisionBot(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = VISION_BOT_CHANGES,
      output = VISION_BOT
  )
  public CompletableFuture<StandardResponse<VisionBotDto>> updateVisionBotById(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @RequestBody final Map<String, Object> visionBot
  ) {
    return visionBotService.updateVisionBotById(
        organizationId, projectId, categoryId, visionBotId,
        visionBot
    ).thenApply(m -> new StandardResponse<>(mapper.mapVisionBot(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/layout/{layoutid}"
          + "/fielddef/{fielddefid}/documents/{documentid}/valuefield",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1},
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE}
  )
  @SchemaValidation(
      input = FIELD_VALUE_DEFINITION,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> getValueField(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("layoutid") final Id layoutId,
      @PathVariable("fielddefid") final Id fieldDefId,
      @PathVariable("documentid") final Id documentId,
      @RequestBody final FieldValueDefinitionDto fieldValue
  ) {
    return visionBotService.getValueField(
        organizationId, projectId, categoryId, visionBotId,
        layoutId, fieldDefId, documentId, mapper.mapFieldValueDefinition(fieldValue)
    );
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}"
          + "/mode/{environment}/run",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> runVisionBot(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("environment") final String environment
  ) {
    return visionBotService.runVisionBot(
        organizationId, projectId, categoryId, visionBotId, environment
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/validationdata",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> getValidationData(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId
  ) {
    return visionBotService.getValidationData(
        organizationId, projectId, categoryId, visionBotId
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/documents/{documentId}"
          + "/pages/{pageindex}/image",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> getPageImage(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("documentId") final Id documentId,
      @PathVariable("pageindex") final String index
  ) {
    return visionBotService.getPageIndex(
        organizationId, projectId, categoryId, documentId, index
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/summary",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = SUMMARY
  )
  public CompletableFuture<StandardResponse<SummaryDto>> getSummary(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return visionBotService.getSummary(
        organizationId, projectId
    ).thenApply(m -> new StandardResponse<>(mapper.mapSummary(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/layout/{layoutid}"
          + "/testset",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = TEST_SET_RESULT
  )
  public CompletableFuture<StandardResponse<TestSetDto>> getTestSet(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("layoutid") final Id layoutId
  ) {
    return visionBotService.getTestSet(
        organizationId, projectId, categoryId, visionBotId, layoutId
    ).thenApply(m -> new StandardResponse<>(mapper.mapTestSet(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/layout/{layoutid}"
          + "/testset",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = TEST_SET,
      output = TEST_SET_RESULT
  )
  public CompletableFuture<StandardResponse<TestSetDto>> updateTestSet(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("layoutid") final Id layoutId,
      @RequestBody final TestSetDto testSet
  ) {
    return visionBotService.updateTestSet(
        organizationId, projectId, categoryId, visionBotId, layoutId, mapper.mapTestSet(testSet)
    ).thenApply(m -> new StandardResponse<>(mapper.mapTestSet(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/layout/{layoutid}"
          + "/testset",
      method = RequestMethod.DELETE,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Void>> deleteTestSet(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("layoutid") final Id layoutId
  ) {
    return visionBotService.deleteTestSet(
        organizationId, projectId, categoryId, visionBotId, layoutId
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/visionbots/state",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = MULTIPLE_VISION_BOT_STATE,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<VisionBotResponseDto> updateMultipleBotState(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @RequestBody final VisionBotElementDto visionBotElementDto
  ) {
    return visionBotService.setVisionBotDetails(
        organizationId, projectId,
        mapper.mapVisionBotStateDetails(visionBotElementDto)
    ).thenApply(m -> mapper.mapVisionBotResponseDto(m));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/state",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = VISION_BOT_STATE,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> setState(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @RequestBody final VisionBotStateDto state
  ) {
    return visionBotService.setState(
        organizationId, projectId, categoryId, visionBotId,
        mapper.mapVisionBotState(state)
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/vbotdata",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = EXPORT_DETAILS,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> exportToCSV(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @RequestBody final Map<String, Object> vBotDetails
  ) {
    return visionBotService.exportToCSV(
        organizationId, projectId, categoryId, vBotDetails
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/validationdata",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = VALIDATION_DETAILS,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> updateValidationDetails(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @RequestBody final ValidationDetailsDto validationDetails
  ) {
    return visionBotService.updateValidationDetails(
        organizationId, projectId, categoryId, visionBotId,
        mapper.mapValidationDetails(validationDetails)
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/automappingdata",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> updateAutoMappingData(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @RequestBody final String data
  ) {
    return visionBotService.updateAutoMappingData(
        organizationId, projectId, categoryId, visionBotId, data
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/"
          + "visionbotrundetails/{visionbotrundetailsid}/documents/{documentid}",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = PROCESSED_DOCUMENT_DETAILS,
      output = PROCESSED_DOCUMENT_DETAILS_RESULT
  )
  public CompletableFuture<StandardResponse<ProcessedDocumentDetailsDto>> updateValidationDocument(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("visionbotrundetailsid") final Id visionBotDetailsId,
      @PathVariable("documentid") final Id documentId,
      @RequestBody final ProcessedDocumentDetailsDto processedDocumentDetails
  ) {
    return visionBotService.updateValidationDocument(
        organizationId, projectId, categoryId, visionBotId, visionBotDetailsId, documentId,
        mapper.mapProcessedDocumentDetails(processedDocumentDetails)
    ).thenApply(m -> new StandardResponse<>(mapper.mapProcessedDocumentDetails(m)));
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}"
          + "/documents/{documentid}/vbotdocument",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> generateVisionBotData(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("documentid") final Id documentId
  ) {
    return visionBotService.generateVisionBotData(
        organizationId, projectId, categoryId, visionBotId, documentId
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}"
          + "/documents/{documentid}/pages/{pageindex}/vbotdocument",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<String>> generateVisionBotDataPaginated(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionBotId,
      @PathVariable("documentid") final Id documentId,
      @PathVariable("pageindex") final int pageIndex
  ) {
    return visionBotService.generateVisionBotDataPaginated(
        organizationId, projectId, categoryId, visionBotId, documentId, pageIndex
    ).thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/{orgid}/projects/{prjid}/categories/{id}/visionbots/{visionbotid}/keepalive",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Void>> keepAliveLockedVisionBot(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("id") final Id categoryId,
      @PathVariable("visionbotid") final Id visionId
  ) {
    return visionBotService.keepAliveLockedVisionBot(
        organizationId, projectId, categoryId, visionId
    ).thenApply(StandardResponse::new);
  }
}
