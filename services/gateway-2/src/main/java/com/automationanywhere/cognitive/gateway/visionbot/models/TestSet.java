package com.automationanywhere.cognitive.gateway.visionbot.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class TestSet {

  public final Id id;
  public final String name;
  public final String docItems;

  public TestSet(
      final Id id,
      final String name,
      final String docItems
  ) {
    this.id = id;
    this.name = name;
    this.docItems = docItems;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      TestSet testSet = (TestSet) o;

      result = Objects.equals(id, testSet.id)
        && Objects.equals(name, testSet.name)
        && Objects.equals(docItems, testSet.docItems);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
      ^ Objects.hashCode(name)
      ^ Objects.hashCode(docItems);
  }
}

