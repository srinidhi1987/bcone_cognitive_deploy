package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectTotalsAccuracyDetailsDto {

  private final int totalFilesProcessed;
  private final int totalSTP;
  private final int totalAccuracy;
  private final int totalFilesUploaded;
  private final int totalFilesToValidation;
  private final int totalFilesValidated;
  private final int totalFilesUnprocessable;
  private final ClassificationFieldDetailsDto[] classification;
  private final FieldAccuracyDashboardDetailsDto[] accuracy;
  private final FieldValidationDto[] validation;
  private final GroupTimeSpentDetailsDto[] categories;

  public ProjectTotalsAccuracyDetailsDto(
      @JsonProperty("totalFilesProcessed") int totalFilesProcessed,
      @JsonProperty("totalSTP") int totalSTP,
      @JsonProperty("totalAccuracy") int totalAccuracy,
      @JsonProperty("totalFilesUploaded") int totalFilesUploaded,
      @JsonProperty("totalFilesToValidation") int totalFilesToValidation,
      @JsonProperty("totalFilesValidated") int totalFilesValidated,
      @JsonProperty("totalFilesUnprocessable") int totalFilesUnprocessable,
      @JsonProperty("classification") ClassificationFieldDetailsDto[] classification,
      @JsonProperty("accuracy") FieldAccuracyDashboardDetailsDto[] accuracy,
      @JsonProperty("validation") FieldValidationDto[] validation,
      @JsonProperty("categories") GroupTimeSpentDetailsDto[] categories
  ) {
    this.totalFilesProcessed = totalFilesProcessed;
    this.totalSTP = totalSTP;
    this.totalAccuracy = totalAccuracy;
    this.totalFilesUploaded = totalFilesUploaded;
    this.totalFilesToValidation = totalFilesToValidation;
    this.totalFilesValidated = totalFilesValidated;
    this.totalFilesUnprocessable = totalFilesUnprocessable;
    this.classification = classification;
    this.accuracy = accuracy;
    this.validation = validation;
    this.categories = categories;
  }

  /**
   * Returns the totalFilesProcessed.
   *
   * @return the value of totalFilesProcessed
   */
  public int getTotalFilesProcessed() {
    return totalFilesProcessed;
  }

  /**
   * Returns the totalSTP.
   *
   * @return the value of totalSTP
   */
  public int getTotalSTP() {
    return totalSTP;
  }

  /**
   * Returns the totalAccuracy.
   *
   * @return the value of totalAccuracy
   */
  public int getTotalAccuracy() {
    return totalAccuracy;
  }

  /**
   * Returns the totalFilesUploaded.
   *
   * @return the value of totalFilesUploaded
   */
  public int getTotalFilesUploaded() {
    return totalFilesUploaded;
  }

  /**
   * Returns the totalFilesToValidation.
   *
   * @return the value of totalFilesToValidation
   */
  public int getTotalFilesToValidation() {
    return totalFilesToValidation;
  }

  /**
   * Returns the totalFilesValidated.
   *
   * @return the value of totalFilesValidated
   */
  public int getTotalFilesValidated() {
    return totalFilesValidated;
  }

  /**
   * Returns the totalFilesUnprocessable.
   *
   * @return the value of totalFilesUnprocessable
   */
  public int getTotalFilesUnprocessable() {
    return totalFilesUnprocessable;
  }

  /**
   * Returns the classification.
   *
   * @return the value of classification
   */
  public ClassificationFieldDetailsDto[] getClassification() {
    return classification;
  }

  /**
   * Returns the accuracy.
   *
   * @return the value of accuracy
   */
  public FieldAccuracyDashboardDetailsDto[] getAccuracy() {
    return accuracy;
  }

  /**
   * Returns the validation.
   *
   * @return the value of validation
   */
  public FieldValidationDto[] getValidation() {
    return validation;
  }

  /**
   * Returns the categories.
   *
   * @return the value of categories
   */
  public GroupTimeSpentDetailsDto[] getCategories() {
    return categories;
  }
}
