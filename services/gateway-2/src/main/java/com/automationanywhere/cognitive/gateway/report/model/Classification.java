package com.automationanywhere.cognitive.gateway.report.model;

import java.util.List;

public class Classification {

  private List<ClassificationField> fieldRepresentation;

  /**
   * Returns the fieldRepresentation.
   *
   * @return the value of fieldRepresentation
   */
  public List<ClassificationField> getFieldRepresentation() {
    return fieldRepresentation;
  }

  /**
   * Sets the fieldRepresentation.
   *
   * @param fieldRepresentation the fieldRepresentation to be set
   */
  public void setFieldRepresentation(
      List<ClassificationField> fieldRepresentation) {
    this.fieldRepresentation = fieldRepresentation;
  }
}
