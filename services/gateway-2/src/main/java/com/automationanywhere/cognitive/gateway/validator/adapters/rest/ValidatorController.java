package com.automationanywhere.cognitive.gateway.validator.adapters.rest;

import static com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders.USERNAME_HEADER_NAME;
import static com.automationanywhere.cognitive.validation.Schemas.INVALID_FILE;
import static com.automationanywhere.cognitive.validation.Schemas.INVALID_FILE_RESULT;
import static com.automationanywhere.cognitive.validation.Schemas.INVALID_FILE_TYPE_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.JSON_PATCH_DOC;
import static com.automationanywhere.cognitive.validation.Schemas.PATCH;
import static com.automationanywhere.cognitive.validation.Schemas.PATCH_V1;
import static com.automationanywhere.cognitive.validation.Schemas.PRODUCTION_FILE_SUMMARY;
import static com.automationanywhere.cognitive.validation.Schemas.REVIEWED_FILE_COUNT_DETAILS_LIST;
import static com.automationanywhere.cognitive.validation.Schemas.STANDARD_RESPONSE;
import static com.automationanywhere.cognitive.validation.Schemas.UNLOCK_RESULT;
import static com.automationanywhere.cognitive.validation.Schemas.V1;

import com.automationanywhere.cognitive.gateway.validator.ValidatorService;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.StandardResponse;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class ValidatorController {

  private final ValidatorDtoMapper mapper;
  private final ValidatorService validatorService;

  @Autowired
  public ValidatorController(
      final ValidatorDtoMapper mapper,
      final ValidatorService visionBotService
  ) {
    this.mapper = mapper;
    validatorService = visionBotService;
  }

  @RequestMapping(
      path = "/organizations/{orgid}/validator/reviewedfilecount",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = REVIEWED_FILE_COUNT_DETAILS_LIST
  )
  public CompletableFuture<StandardResponse<List<ReviewedFileCountDetailsDto>>>
  getReviewedFileCountDetails(
      @PathVariable("orgid") final Id organizationId
  ) {
    return validatorService.getReviewedFileCountDetails(organizationId)
        .thenApply(l -> new StandardResponse<>(
            l.stream().map(mapper::mapReviewedFileCountDetails).collect(Collectors.toList())
        ));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/validator/projects/{prjid}",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<Long>> getFailedVisionBotCount(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return validatorService.getFailedVisionBotCount(organizationId, projectId)
        .thenApply(StandardResponse::new);
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{projid}/unlock/",
      method = RequestMethod.POST,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = UNLOCK_RESULT
  )
  public CompletableFuture<StandardResponse<SuccessDto>> unlockProject(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("projid") final Id projectId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final String userName
  ) {
    return validatorService.unlockProject(organizationId, projectId, userName)
        .thenApply(d -> new StandardResponse<>(mapper.mapSuccess(d)));
  }

  @RequestMapping(
      path = "/validator/invalidreasons/",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = INVALID_FILE_TYPE_LIST
  )
  public CompletableFuture<StandardResponse<List<InvalidFileTypeDto>>> getInvalidFileTypeList() {
    return validatorService.getInvalidFileTypeList().thenApply(l ->
        new StandardResponse<>(l.stream().map(mapper::mapInvalidFileType)
            .collect(Collectors.toList()))
    );
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/productionsummary",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      output = PRODUCTION_FILE_SUMMARY
  )
  public CompletableFuture<StandardResponse<ProductionFileSummaryDto>> getProductionFileSummary(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return validatorService.getProductionFileSummary(organizationId, projectId).thenApply(
        d -> new StandardResponse<>(mapper.mapProductionFileSummary(d))
    );
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{projid}/files/{fileid}/invalidate",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  @SchemaValidation(
      input = INVALID_FILE,
      output = INVALID_FILE_RESULT
  )
  public CompletableFuture<StandardResponse<SuccessDto>> markFileInvalid(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("projid") final Id projectId,
      @PathVariable("fileid") final Id fileId,
      @RequestHeader(USERNAME_HEADER_NAME) final String username,
      @RequestBody final List<InvalidFileDto> invalidFiles
  ) {
    return validatorService.markFileInvalid(
        organizationId, projectId, fileId, username, mapper.mapInvalidFiles(invalidFiles)
    ).thenApply(
        d -> new StandardResponse<>(mapper.mapSuccess(d))
    );
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/visionbot/",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<StandardResponse<Map<String, Object>>> getFailedVbotData(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final String userName
  ) {
    return validatorService.getFailedVbotdata(organizationId, projectId, userName).thenApply(
        StandardResponse::new
    );
  }

  @RequestMapping(
      path = "/organizations/{orgid}/validator/processingdetails/{idtype}",
      method = RequestMethod.GET,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<StandardResponse<AccuracyProcessingDetailsDto>> getDocumentProcessingDetails(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("idtype") final Id typeId
  ) {
    return validatorService.getDocumentProcessingDetails(organizationId, typeId).thenApply(
        d -> new StandardResponse<>(mapper.mapAccuracyProcessingDetails(d))
    );
  }

  @RequestMapping(
      path = "/organizations/{orgid}/validator/dashboard/project/{prjid}",
      method = RequestMethod.GET,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<StandardResponse<ProjectTotalsAccuracyDetailsDto>> getDashboardAccuracyDetails(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId
  ) {
    return validatorService.getDashboardAccuracyDetails(organizationId, projectId).thenApply(
        d -> new StandardResponse<>(mapper.mapPrjTotAccuracyDetails(d))
    );
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{projid}/visionbot/{fileid}",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<StandardResponse<SuccessDto>> sendCorrectDataValues(
      @PathVariable("orgid") Id organizationId,
      @PathVariable("projid") Id projectId,
      @PathVariable("fileid") Id fileId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final String userName,
      @RequestBody String additionInfo
  ) {
    return validatorService.sendCorrectDataValues(
        organizationId, projectId, fileId, userName, additionInfo
    ).thenApply(d -> new StandardResponse<>(new SuccessDto(d.success)));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{projid}/visionbot/{fileid}",
      method = RequestMethod.PATCH,
      consumes = {PATCH_V1, PATCH},
      produces = {PATCH, PATCH_V1}
  )
  @SchemaValidation(
      input = JSON_PATCH_DOC,
      output = STANDARD_RESPONSE
  )
  public CompletableFuture<StandardResponse<SuccessDto>> patchCorrectDataValues(
      @PathVariable("orgid") Id organizationId,
      @PathVariable("projid") Id projectId,
      @PathVariable("fileid") Id fileId,
      @RequestHeader(name = USERNAME_HEADER_NAME, required = false) final String userName,
      @RequestBody List<JsonPatchActionDto> actions
  ) {
    return validatorService.patchCorrectDataValues(
        organizationId, projectId, fileId, userName, mapper.mapJsonPatchDocument(actions)
    ).thenApply(d -> new StandardResponse<>(null));
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{prjid}/visionbot/{visionbotid}/files/{fileid}/validationstatus/{validationstatus}",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<FailedVbotStatusDto> updateValidationStatus(
      @PathVariable("orgid") final Id organizationId,
      @PathVariable("prjid") final Id projectId,
      @PathVariable("visionbotid") final Id visionbotId,
      @PathVariable("fileid") final Id fileId,
      @PathVariable("validationstatus") final Boolean validationStatus
  ) {
    return validatorService
        .updateValidationStatus(organizationId, projectId, visionbotId, fileId, validationStatus)
        .thenApply(mapper::mapFailedVBotStatus);
  }

  @RequestMapping(
      path = "/organizations/{orgid}/projects/{projid}/files/{fileid}/keepalive",
      method = RequestMethod.POST,
      consumes = {V1, MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, V1}
  )
  public CompletableFuture<StandardResponse<Void>> keepAlive(
      @PathVariable("orgid") Id organizationId,
      @PathVariable("projid") Id projectId,
      @PathVariable("fileid") Id fileId
  ) {
    return validatorService.keepAlive(organizationId, projectId, fileId)
        .thenApply(StandardResponse::new);
  }
}
