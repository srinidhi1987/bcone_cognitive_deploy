package com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.log4j2;

import com.automationanywhere.cognitive.gateway.auth.microservices.auth.audit.AuditingDetails;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.util.Supplier;

public class Log4J2AuditingDetails implements AuditingDetails {
  public final String message;
  public final List<Supplier<?>> suppliers;

  public Log4J2AuditingDetails(
      final String message,
      final List<Supplier<?>> suppliers
  ) {
    this.message = message;
    this.suppliers = Collections.unmodifiableList(new ArrayList<>(suppliers));
  }
}
