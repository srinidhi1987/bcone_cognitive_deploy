package com.automationanywhere.cognitive.gateway.validator.microservices.validator;

import com.automationanywhere.cognitive.gateway.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ClassificationFieldDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotStatus;
import com.automationanywhere.cognitive.gateway.validator.models.FailedVbotdata;
import com.automationanywhere.cognitive.gateway.validator.models.FieldAccuracyDashboardDetails;
import com.automationanywhere.cognitive.gateway.validator.models.FieldValidation;
import com.automationanywhere.cognitive.gateway.validator.models.GroupTimeSpentDetails;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFile;
import com.automationanywhere.cognitive.gateway.validator.models.InvalidFileType;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchAction;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionAdd;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionCopy;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionMove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionRemove;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionReplace;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchActionTest;
import com.automationanywhere.cognitive.gateway.validator.models.JsonPatchDocument;
import com.automationanywhere.cognitive.gateway.validator.models.ProductionFileSummary;
import com.automationanywhere.cognitive.gateway.validator.models.ProjectTotalsAccuracyDetails;
import com.automationanywhere.cognitive.gateway.validator.models.ReviewedFileCountDetails;
import com.automationanywhere.cognitive.gateway.validator.models.Success;
import com.automationanywhere.cognitive.mappers.BaseMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ValidatorDataMapper extends BaseMapper {

  public ReviewedFileCountDetails mapReviewedFileCountDetails(
      final ReviewedFileCountDetailsData data
  ) {
    return data == null ? null : new ReviewedFileCountDetails(
        data.projectId, data.processedCount, data.failedFileCount,
        data.reviewFileCount, data.invalidFileCount
    );
  }

  public Success mapSuccess(
      final SuccessData data
  ) {
    return data == null ? null : new Success(data.success);
  }

  public InvalidFileType mapInvalidFileType(
      final InvalidFileTypeData data
  ) {
    return data == null ? null : new InvalidFileType(
        data.id, data.reason
    );
  }

  public ProductionFileSummary mapProductionFileSummary(
      final ProductionFileSummaryData data
  ) {
    return data == null ? null : new ProductionFileSummary(
        data.projectId, data.processedFiles, data.successFiles, data.reviewFiles,
        data.invalidFiles, data.pendingForReview, data.accuracy
    );
  }

  public InvalidFileData mapInvalidFile(
      final InvalidFile data
  ) {
    return data == null ? null : new InvalidFileData(
        data.fileId, data.reasonId, data.reasonText
    );
  }

  public FailedVbotdata mapFailedVBotData(FailedVbotdataData data) {
    return data == null ? null : new FailedVbotdata(
        data.getFileId(),
        data.getOrgId(),
        data.getProjectId(),
        data.getvBotDocument(),
        data.getCorrectedData(),
        data.getValidationStatus(),
        data.getCreatedAt(),
        data.getUpdatedBy(),
        data.getUpdatedAt(),
        data.getVisionBotId(),
        data.getLockedUserId(),
        data.getFailedFieldCounts(),
        data.getPassFieldCounts(),
        data.getTotalFieldCounts(),
        data.getStartTime(),
        data.getEndTime(),
        data.getAverageTimeInSeconds(),
        data.getLastExportTime()
    );
  }

  public AccuracyProcessingDetails mapAccuracyProcessingDetails(
      AccuracyProcessingDetailsData data) {
    return data == null ? null : new AccuracyProcessingDetails(
        data.getId(),
        data.getFailedFieldCounts(),
        data.getPassFieldCounts(),
        data.getTotalFieldCounts(),
        data.getFieldAccuracy(),
        data.getReviewFileCount(),
        data.getInvalidFileCount(),
        data.getFailedDocumentCount(),
        data.getPassedDocumentCounts(),
        data.getProcessedDocumentCounts(),
        data.getDocumentAccuracy(),
        data.getAverageReviewTimeInSeconds()
    );
  }

  public ProjectTotalsAccuracyDetails mapPrjTotAccuracyDetails(
      ProjectTotalsAccuracyDetailsData data) {
    return data == null ? null : new ProjectTotalsAccuracyDetails(
        data.getTotalFilesProcessed(),
        data.getTotalSTP(),
        data.getTotalAccuracy(),
        data.getTotalFilesUploaded(),
        data.getTotalFilesToValidation(),
        data.getTotalFilesValidated(),
        data.getTotalFilesUnprocessable(),
        mapClassificationFieldDetails(data.getClassification()),
        mapFieldAccuracyDashboardDetails(data.getAccuracy()),
        mapFieldValidation(data.getValidation()),
        mapGroupTimeSpentDetails(data.getCategories())
    );
  }

  public FailedVbotStatus mapFailedVBotStatus(FailedVbotStatusData data) {
    return data == null ? null : new FailedVbotStatus(
        data.isSuccess(),
        data.getCount(),
        data.getError()
    );
  }

  private ClassificationFieldDetails[] mapClassificationFieldDetails(
      ClassificationFieldDetailsData[] data
  ) {
    if (data == null) {
      return null;
    }

    ClassificationFieldDetails[] result = new ClassificationFieldDetails[data.length];
    for (int i = 0; i < data.length; i++) {
      ClassificationFieldDetailsData item = data[i];
      result[i] = new ClassificationFieldDetails(
          item.getFieldId(),
          item.getRepresentationPercent()
      );
    }
    return result;
  }

  private FieldAccuracyDashboardDetails[] mapFieldAccuracyDashboardDetails(
      FieldAccuracyDashboardDetailsData[] data
  ) {
    if (data == null) {
      return null;
    }

    FieldAccuracyDashboardDetails[] result = new FieldAccuracyDashboardDetails[data.length];
    for (int i = 0; i < data.length; i++) {
      FieldAccuracyDashboardDetailsData item = data[i];
      result[i] = new FieldAccuracyDashboardDetails(
          item.getFieldId(),
          item.getAccuracyPercent()
      );
    }
    return result;
  }

  private FieldValidation[] mapFieldValidation(
      FieldValidationData[] data
  ) {
    if (data == null) {
      return null;
    }

    FieldValidation[] result = new FieldValidation[data.length];
    for (int i = 0; i < data.length; i++) {
      FieldValidationData item = data[i];
      result[i] = new FieldValidation(
          item.getFieldId(),
          item.getPercentValidated()
      );
    }
    return result;
  }

  private GroupTimeSpentDetails[] mapGroupTimeSpentDetails(
      GroupTimeSpentDetailsData[] data
  ) {
    if (data == null) {
      return null;
    }

    GroupTimeSpentDetails[] result = new GroupTimeSpentDetails[data.length];
    for (int i = 0; i < data.length; i++) {
      GroupTimeSpentDetailsData item = data[i];
      result[i] = new GroupTimeSpentDetails(
          item.getCategoryId(),
          item.getTimeSpend()
      );
    }
    return result;
  }

  public List<JsonPatchActionData> mapJsonPatchDocument(JsonPatchDocument doc) {
    return doc.actions.stream().map(this::mapJsonPatchAction).collect(Collectors.toList());
  }

  private JsonPatchActionData mapJsonPatchAction(JsonPatchAction action) {
    JsonPatchActionData data = null;
    if (action instanceof JsonPatchActionAdd) {
      JsonPatchActionAdd actionAdd = (JsonPatchActionAdd) action;
      data = new JsonPatchActionAddData(actionAdd.path, actionAdd.value);

    } else if (action instanceof JsonPatchActionReplace) {
      JsonPatchActionReplace actionReplace = (JsonPatchActionReplace) action;
      data = new JsonPatchActionReplaceData(actionReplace.path, actionReplace.value);

    } else if (action instanceof JsonPatchActionRemove) {
      JsonPatchActionRemove actionRemove = (JsonPatchActionRemove) action;
      data = new JsonPatchActionRemoveData(actionRemove.path);

    } else if (action instanceof JsonPatchActionTest) {
      JsonPatchActionTest actionTest = (JsonPatchActionTest) action;
      data = new JsonPatchActionTestData(actionTest.path, actionTest.value);

    } else if (action instanceof JsonPatchActionCopy) {
      JsonPatchActionCopy actionCopy = (JsonPatchActionCopy) action;
      data = new JsonPatchActionCopyData(actionCopy.path, actionCopy.from);

    } else if (action instanceof JsonPatchActionMove) {
      JsonPatchActionMove actionMove = (JsonPatchActionMove) action;
      data = new JsonPatchActionMoveData(actionMove.path, actionMove.from);
    }
    return data;
  }
}
