package com.automationanywhere.cognitive.gateway.file.microservices.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class FileBlobsData {

  public final String fileId;
  @JsonProperty("fileBlob")
  public final String fileBlobs;

  public FileBlobsData(
      @JsonProperty("fileId") final String fileId,
      @JsonProperty("fileBlob") final String fileBlobs
  ) {
    this.fileId = fileId;
    this.fileBlobs = fileBlobs;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      FileBlobsData fileBLOb = (FileBlobsData) o;

      result = Objects.equals(fileId, fileBLOb.fileId)
          && Objects.equals(fileBlobs, fileBLOb.fileBlobs);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fileId)
        ^ Objects.hashCode(fileBlobs);
  }
}

