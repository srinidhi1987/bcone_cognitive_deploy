package com.automationanywhere.cognitive.gateway.alias;

import com.automationanywhere.cognitive.gateway.alias.microservices.alias.AliasMicroservice;
import com.automationanywhere.cognitive.gateway.alias.model.Language;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectField;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectType;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.automationanywhere.cognitive.gateway.alias.model.ShareableDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.automationanywhere.cognitive.gateway.alias.DomainCodec.DOMAIN_ENCODING_V1;

@Service
public class AliasService {

  private final AliasMicroservice repository;

  @Autowired
  public AliasService(final AliasMicroservice repository) {
    this.repository = repository;
  }

  /**
   * Returns the languages supported for each domain.
   *
   * @return the languages supported for each domain
   */
  public CompletableFuture<List<DomainLanguagesInfo>> getDomainLanguages() {
    return repository.getLanguagesForDomains()
            .thenApply(Collections::unmodifiableList);
  }

  /**
   * Returns the "shareable" representation of the domain data pulled from the
   * Alias service in its raw form and then encoded.
   *
   * @param domainName the name of the desired domain
   * @return the "shareable" representation of the domain data
   */
  public CompletableFuture<ShareableDomain> getDomain(final String domainName) {
    return repository.getDomains(domainName).thenApply(json ->
            new ShareableDomain(
                    domainName,
                    encodeDomain(json),
                    DOMAIN_ENCODING_V1));
  }

  /**
   * Decodes the domain JSON from the provided shareable domain metadata and
   * sends it to the remote alias service for import.
   *
   * @param shareableDomain the "shareable" representation of the domain
   *     metadata
   * @return a void response
   */
  public CompletableFuture<Void> importDomain(
          final ShareableDomainInfo shareableDomain) {
    final String domainJson = decodeDomain(shareableDomain.getDomain());
    return repository.importDomains(domainJson);
  }

  public CompletableFuture<List<ProjectType>> getAllProjectTypes(final String langId) {
    return repository.getAllProjectTypes(langId);
  }

  public CompletableFuture<ProjectType> getProjectType(
      final String projectTypeId, final String langId
  ) {
    return repository.getProjectType(projectTypeId, langId);
  }

  public CompletableFuture<List<ProjectField>> getProjectFields(
      final String projectTypeId,
      final String langId
  ) {
    return repository.getProjectFields(projectTypeId, langId);
  }

  public CompletableFuture<ProjectField> getProjectField(
      final String fieldId, final String langId
  ) {
    return repository.getField(fieldId, langId);
  }

  public CompletableFuture<List<Language>> getLanguages() {
    return repository.getLanguages();
  }

  public CompletableFuture<Language> getLanguage(final String langId) {
    return repository.getLanguage(langId);
  }

  public CompletableFuture<List<String>> getProjectAliases(
      final String fieldId, final String langId
  ) {
    return repository.getAliases(fieldId, langId);
  }

  /**
   * Returns the encoded representation of the domain metadata that may be
   * shared among partners and customers.
   *
   * @param domainJson the domain JSON
   * @return the encoded representation of the domain metadata
   */
  private static String encodeDomain(final String domainJson) {
    return DomainCodec.encodeDomain(domainJson);
  }

  /**
   * Returns the domain JSON from the encoded "shareable" representation of the
   * domain metadata.
   *
   * @param base64Domain the base64 encoded representation of the domain
   * @return the domain JSON
   */
  private static String decodeDomain(final String base64Domain) {
    return DomainCodec.decodeDomain(base64Domain);
  }

}
