package com.automationanywhere.cognitive.gateway.report.adapters;

import com.automationanywhere.cognitive.gateway.report.model.Accuracy;
import com.automationanywhere.cognitive.gateway.report.model.Classification;
import com.automationanywhere.cognitive.gateway.report.model.ClassificationField;
import com.automationanywhere.cognitive.gateway.report.model.DashboardProdProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.FieldAccuracy;
import com.automationanywhere.cognitive.gateway.report.model.FieldValidation;
import com.automationanywhere.cognitive.gateway.report.model.GroupTimeSpent;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotals;
import com.automationanywhere.cognitive.gateway.report.model.ProjectTotalsDto;
import com.automationanywhere.cognitive.gateway.report.model.ReportingProject;
import com.automationanywhere.cognitive.gateway.report.model.Validation;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ReportDtoMapper {

  public List<DashboardProdProjectTotalsDto> getDashboardTotalsDto(
      List<DashboardProdProjectTotals> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getDashboardTotalsDto)
        .collect(Collectors.toList());
  }

  public DashboardProdProjectTotalsDto getDashboardTotalsDto(
      DashboardProdProjectTotals entity) {
    DashboardProdProjectTotalsDto dto = new DashboardProdProjectTotalsDto();
    dto.setTotalFilesProcessed(entity.getTotalFilesProcessed());
    dto.setTotalSTP(entity.getTotalSTP());
    dto.setTotalAccuracy(entity.getTotalAccuracy());
    dto.setTotalFilesCount(entity.getTotalFilesCount());
    return dto;
  }

  public List<ProjectTotalsDto> getProjectTotalsDto(List<ProjectTotals> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getProjectTotalsDto)
        .collect(Collectors.toList());
  }

  public ProjectTotalsDto getProjectTotalsDto(ProjectTotals entity) {
    ProjectTotalsDto dto = new ProjectTotalsDto();
    dto.setTotalFilesProcessed(entity.getTotalFilesProcessed());
    dto.setTotalSTP(entity.getTotalSTP());
    dto.setTotalAccuracy(entity.getTotalAccuracy());
    dto.setTotalFilesUploaded(entity.getTotalFilesUploaded());
    dto.setTotalFilesToValidation(entity.getTotalFilesToValidation());
    dto.setTotalFilesValidated(entity.getTotalFilesValidated());
    dto.setTotalFilesUnprocessable(entity.getTotalFilesUnprocessable());
    dto.setClassification(getClassificationDto(entity.getClassification()));
    dto.setAccuracy(getAccuracyDto(entity.getAccuracy()));
    dto.setValidation(getValidationDto(entity.getValidation()));
    return dto;
  }

  public ClassificationDto getClassificationDto(Classification entity) {
    ClassificationDto dto = new ClassificationDto();
    dto.setFieldRepresentation(getClassificationField(entity.getFieldRepresentation()));
    return dto;
  }

  public List<ClassificationFieldDto> getClassificationField(List<ClassificationField> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getClassificationField)
        .collect(Collectors.toList());
  }

  public ClassificationFieldDto getClassificationField(ClassificationField entity) {
    ClassificationFieldDto dto = new ClassificationFieldDto();
    dto.setName(entity.getName());
    dto.setRepresentationPercent(entity.getRepresentationPercent());
    return dto;
  }

  public AccuracyDto getAccuracyDto(Accuracy entity) {
    AccuracyDto dto = new AccuracyDto();
    dto.setFields(getFieldAccuracy(entity.getFields()));
    return dto;
  }

  public List<FieldAccuracyDto> getFieldAccuracy(List<FieldAccuracy> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getFieldAccuracy)
        .collect(Collectors.toList());
  }

  public FieldAccuracyDto getFieldAccuracy(FieldAccuracy entity) {
    FieldAccuracyDto dto = new FieldAccuracyDto();
    dto.setName(entity.getName());
    dto.setAccuracyPercent(entity.getAccuracyPercent());
    return dto;
  }

  public ValidationDto getValidationDto(Validation entity) {
    ValidationDto dto = new ValidationDto();
    dto.setFields(getFieldValidation(entity.getFields()));
    dto.setCategories(getGroupTimeSpent(entity.getCategories()));
    return dto;
  }

  private List<FieldValidationDto> getFieldValidation(List<FieldValidation> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getFieldValidation)
        .collect(Collectors.toList());
  }

  public FieldValidationDto getFieldValidation(FieldValidation entity) {
    FieldValidationDto dto = new FieldValidationDto();
    dto.setFieldId(entity.getFieldId());
    dto.setPercentValidated(entity.getPercentValidated());
    return dto;
  }

  private List<GroupTimeSpentDto> getGroupTimeSpent(List<GroupTimeSpent> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getGroupTimeSpent)
        .collect(Collectors.toList());
  }

  private GroupTimeSpentDto getGroupTimeSpent(GroupTimeSpent entity) {
    GroupTimeSpentDto dto = new GroupTimeSpentDto();
    dto.setName(entity.getName());
    dto.setTimeSpent(entity.getTimeSpent());
    return dto;
  }

  public List<ReportingProjectDto> getReportingProject(List<ReportingProject> entities) {
    return Optional.ofNullable(entities).orElse(Collections.emptyList())
        .stream()
        .map(this::getReportingProject)
        .collect(Collectors.toList());
  }

  public ReportingProjectDto getReportingProject(ReportingProject entity) {
    ReportingProjectDto dto = new ReportingProjectDto();
    dto.setId(entity.getId());
    dto.setOrganizationId(entity.getOrganizationId());
    dto.setName(entity.getName());
    dto.setDescription(entity.getDescription());
    dto.setEnvironment(entity.getEnvironment());
    dto.setPrimaryLanguage(entity.getPrimaryLanguage());
    dto.setProjectType(entity.getProjectType());
    dto.setProjectState(entity.getProjectState());
    dto.setTotalFilesProcessed(entity.getTotalFilesProcessed());
    dto.setTotalSTP(entity.getTotalSTP());
    dto.setNumberOfFiles(entity.getNumberOfFiles());
    dto.setTotalAccuracy(entity.getTotalAccuracy());
    dto.setCurrentTrainedPercentage(entity.getCurrentTrainedPercentage());
    return dto;
  }
}
