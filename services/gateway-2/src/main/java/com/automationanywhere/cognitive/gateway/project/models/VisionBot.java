package com.automationanywhere.cognitive.gateway.project.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class VisionBot {

  public final Id id;
  public final String name;
  public final String currentState;

  public VisionBot(
      final Id id,
      final String name,
      final String currentState
  ) {
    this.id = id;
    this.name = name;
    this.currentState = currentState;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && getClass() == o.getClass()) {
      VisionBot visionBot = (VisionBot) o;

      result = Objects.equals(id, visionBot.id)
          && Objects.equals(name, visionBot.name)
          && Objects.equals(currentState, visionBot.currentState);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(currentState);
  }
}

