package com.automationanywhere.cognitive.gateway.report.model;

public class DashboardProdProjectTotals {

  private long totalFilesProcessed;
  private long totalSTP;
  private long totalAccuracy;
  private long totalFilesCount;

  /**
   * Returns the total files processed across all production instances.
   *
   * @return the value of totalFilesProcessed
   */
  public long getTotalFilesProcessed() {
    return totalFilesProcessed;
  }

  /**
   * Sets the totalFilesProcessed.
   *
   * @param totalFilesProcessed the totalFilesProcessed to be set
   */
  public void setTotalFilesProcessed(long totalFilesProcessed) {
    this.totalFilesProcessed = totalFilesProcessed;
  }

  /**
   * Returns total straight-through processing count across all production instances.
   *
   * @return the value of totalSTP
   */
  public long getTotalSTP() {
    return totalSTP;
  }

  /**
   * Sets the totalSTP.
   *
   * @param totalSTP the totalSTP to be set
   */
  public void setTotalSTP(long totalSTP) {
    this.totalSTP = totalSTP;
  }

  /**
   * Returns the total document accuracy across all production instances.
   *
   * @return the value of totalAccuracy
   */
  public long getTotalAccuracy() {
    return totalAccuracy;
  }

  /**
   * Sets the totalAccuracy.
   *
   * @param totalAccuracy the totalAccuracy to be set
   */
  public void setTotalAccuracy(long totalAccuracy) {
    this.totalAccuracy = totalAccuracy;
  }

  /**
   * Returns the total files uploaded across all production instances.
   *
   * @return the value of totalFilesCount
   */
  public long getTotalFilesCount() {
    return totalFilesCount;
  }

  /**
   * Sets the totalFilesCount.
   *
   * @param totalFilesCount the totalFilesCount to be set
   */
  public void setTotalFilesCount(long totalFilesCount) {
    this.totalFilesCount = totalFilesCount;
  }
}
