package com.automationanywhere.cognitive.gateway.application.adapters.rest;

public class ConfigurationDto {

  public final String crUrl;
  public final String routingName;

  public ConfigurationDto(final String crUrl, final String routingName) {
    this.crUrl = crUrl;
    this.routingName = routingName;
  }
}
