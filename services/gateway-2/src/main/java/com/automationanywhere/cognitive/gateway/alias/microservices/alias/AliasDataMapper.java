package com.automationanywhere.cognitive.gateway.alias.microservices.alias;

import com.automationanywhere.cognitive.gateway.alias.model.Language;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectField;
import com.automationanywhere.cognitive.gateway.alias.model.ProjectType;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class AliasDataMapper {

  public List<ProjectType> getProjectTypes(final List<ProjectTypeData> projectTypeData) {
    return Optional.ofNullable(projectTypeData)
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getProjectType)
        .collect(Collectors.toList());
  }

  public ProjectType getProjectType(final ProjectTypeData data) {
    return new ProjectType(
      data.id,
      data.name,
      data.description,
      getProjectFields(data.fields),
      getLanguages(data.languages)
    );
  }

  public List<ProjectField> getProjectFields(final List<ProjectFieldData> projectTypeData) {
    return Optional.ofNullable(projectTypeData)
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getProjectField)
        .collect(Collectors.toList());
  }

  public ProjectField getProjectField(final ProjectFieldData data) {
    return new ProjectField(
        data.id,
        data.name,
        data.description,
        data.fieldType,
        data.aliases,
        data.isDefaultSelected,
        data.dataType
    );
  }

  public List<Language> getLanguages(final List<LanguageData> languages) {
    return Optional.ofNullable(languages)
        .orElse(Collections.emptyList())
        .stream()
        .map(this::getLanguage)
        .collect(Collectors.toList());
  }

  public Language getLanguage(final LanguageData entity) {
    return new Language(
      entity.id,
      entity.code,
      entity.name
    );
  }
}
