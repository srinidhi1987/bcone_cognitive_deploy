﻿### **How to develop an API in Gateway**
In Gateway we have 3 layers: controllers, services and repositories. All 3 layers **MUST** support asynchronous calls. It's strongly recommended to use immutable models. Controllers and repositories **MAY** use service models as their own input and output models, services **MUST NOT** use controller or repository models.

#### **Asynchronous calls**

We use **CompletableFuture** objects:

```java
public CompletableFuture<List<OrderModel>> getCustomerOrders(
  @PathVariable("customerId") final String customerId
) {
...
}
```
Remember to use the @Async annotation in services and repositories

```java
@Async
public CompletableFuture<Customer> getCustomer(final String id)
```


If  calls should go into a sequence one after another, because, for example, the second call depends on an output of the first call, the subsequent calls may be executed as call cascades:

```java
return this.customerRepository.getCustomer(customerId).thenCompose(customer -> {
  return this.orderRepository.getCustomerOrders(customerId).thenApply(this.exceptionHandlerFactory.map(orders -> {
    return this.orderAggregator.aggregate(customer, orders);
  }));
});
```

#### **A general note on models**

It's strongly recommended to use immutable models. In case if a model has to be modified standard approaches like using custom constructors/factories/builders can be used, for example:

```java
public class Model {
  public final long id;
  public final String name;
  
  public Model(final long id, final String name) {
    this.id = id;
    this.name = name;
  }
  
  public Model(final Model model, final long id) {
    this.id = id;
    this.name = model.name;
  }
}
...
Model oldModel = new Model(1, "name");
Model model = new Model(oldModel, 2);
```

#### **Controllers**

Depending on a client application type this may be a REST interface, a SOAP interface, a message queue or something else. We call them controllers and put under 
**com.automationanywhere.cognitive.gateway.controllers** so the rest controllers go under **com.automationanywhere.cognitive.gateway.controllers.rest**
The responsibilities of controllers are to accept client requests for further processing and to send responses back to clients. The most of controller types would work with input and output models, but we do not keep them separate, so the package for both input and output models of rest controllers would be **com.automationanywhere.cognitive.gateway.controllers.rest.models**

A typical controller behaviour consists of 3 steps:

* to transform input controller models into input service models
* to call a service
* to transform output service models into output controller models

To implement REST controllers we use **@RequestMapping** and related annotations:

```java
@RequestMapping(
  path = "/customers/{customerId}/orders",
  method = RequestMethod.GET,
  ...
)
public CompletableFuture<List<OrderModel>> getCustomerOrders(
  @PathVariable("customerId") final String customerId
) {
...
}
```

Controllers **MUST NOT** pass controller models to services while it's still allowed to use service models in client interfaces if no transformations are needed and client applications can cope with service models. Please notice that it may be not possible to use service models as input controller models because usually JSON deserializers  need metadata info:

```java
public class CustomerData {
  public final Id customerId;
  public final String name;

  public CustomerData(
    @JsonProperty("customerId") final Id customerId,
    @JsonProperty("name") final String name
  ) {
    this.customerId = customerId;
    this.name = name;
  }
}
```

Please notice the @JsonProperty which **MAY** be used in controller models but **MUST NOT** be used in service models because service models know nothing about the nature of clients. Using @JsonProperty annotation would make services dependent on a particular type of client application which is not acceptable solution.

#### **Services**

The responsibility of a service is to call all necessary repositories to get needed data and then to aggregate (to combine together) the data. We put services under **com.automationanywhere.cognitive.gateway.services**, since service models are common and are shared between all the three layers we put them at the very top **com.automationanywhere.cognitive.gateway.models**

Services call repositories either asynchronously or in synchronisation cascades and may combine data themselves if the aggregation is split into several steps. Or, if the aggregation may be done in one step, services may delegate the aggregation to dedicated aggregators. We put them in **com.automationanywhere.cognitive.gateway.services.aggregators**

Please notice, since services by definition do not depend on the nature of client applications or underlying data storages or sources there is no such thing as a REST service or a JPA service.

#### **Repositories**
The responsibility of repositories is to retrieve or modify data and to return the data or response details to services. Depending on the underlying storage type this may be an RDBMS, a NoSQL data storage, a message queue or, for example, another RESTful application.  We put repositories under **com.automationanywhere.cognitive.gateway.repositories** Repositories accept and return service models but may use their internal models. We put them under **com.automationanywhere.cognitive.gateway.repositories.models** Those model cannot be used by services or by controllers. Depending on the nature of the underlying data storage/source/provider internal models may be entities (in case of JPA or JDBC based repositories) or JSON models or something else.

#### **Mappers**

Both  controllers and repositories may need to convert models to/from service models. Repositories need this because services **DO NOT** know nothing about internal repository models. Controllers may need this because service models may be not compatible with models that client application can work with. And only a particular controller may know how to reconcile clients and services. We put REST controller mappers under **com.automationanywhere.cognitive.gateway.controllers.rest.mappers** and repository mappers under **com.automationanywhere.cognitive.gateway.repositories.mappers**

#### **API Versions**

We support API versions. It means that different client applications may call different versions of APIs at the same time. A client **MAY** specify an API version explicitly. If an API version is not specified Gateway will route a request to a controller which implements the latest API version.

For REST client applications that send requests without a body an API version **MAY BE** set in the **accept** header

**Accept**: application/vnd.cognitive.v2+json

For REST client applications that send not empty bodies in their requests an API version **MAY BE** set in the content-type header:

**Content-Type**: application/vnd.cognitive.v2+json; charset=UTF-8

REST controllers **MUST** use **headers**, **consumes** and **produces** parameters:

```java
@RequestMapping(
  path = "/customers/{customerId}/orders/schema",
  method = RequestMethod.GET,
  headers = {
    "accept=application/vnd.cognitive.v1+json"
  },
  produces = {
    "application/vnd.cognitive.v1+json"
  }
)
```

Please notice that a controller that implements the latest API version **SHOULD** also include the default mime-types like so:

```java
headers = {
  "accept=application/vnd.cognitive.v2+json",
  "accept=application/json",
  "accept=*/*"
},
```

Controllers, repositories, services and other components that implement old API versions **MUST BE** annotated withe **@Deprecated** annotation.

To avoid class name collisions and to avoid class name pollution we put different class versions in appropriate subpackages. For example, v1 and v2 services go into **com.automationanywhere.cognitive.gateway.services.v1** and **com.automationanywhere.cognitive.gateway.services.v2**; repository models go into **com.automationanywhere.cognitive.gateway.repositories.models.v1** and **com.automationanywhere.cognitive.gateway.repositories.models.v2**
Since API gateway is a Spring Boot application you will need to specify component names explicitly to avoid component name collisions, for example,

**@RestController("orderControllerV1")** and **@RestController("orderControllerV2")** or 

**@Component("customerRepositoryV1")** and **@Component("customerRepositoryV2")**

#### **Examples**

A controller example:

```java
public CompletableFuture<List<OrderModel>> getCustomerOrders(
  @PathVariable("customerId") final String customerId
) {
  return this.orderService.getCustomerOrders(customerId).thenApply(this.exceptionHandlerFactory.map(this.mapper));
}
```
**this.exceptionHandlerFactory** is an instance of a helper class **FunctionWrapperFactory** which produces wrappers of mappers into **Function** objects acceptable by **CompletableFuture** classes.

A service example:

```java
CompletableFuture<List<Order>> customerOrdersFuture = this.orderRepository.getCustomerOrders(customerId);
CompletableFuture<Customer> customerFuture = this.customerRepository.getCustomer(customerId);

return CompletableFuture.allOf(
  customerOrdersFuture,
  customerFuture
).thenApply(this.exceptionHandlerFactory.call(() -> {
  List<Order> orders = customerOrdersFuture.join();
  Customer customer = customerFuture.join();

  return this.orderAggregator.aggregate(customer, orders);
}));
```

A service example with call cascades:

```java
return this.customerRepository.getCustomer(customerId).thenCompose(customer -> {
  return this.orderRepository.getCustomerOrders(customerId).thenApply(this.exceptionHandlerFactory.map(orders -> {
    return this.orderAggregator.aggregate(customer, orders);
  }));
});
```

A repository example:

```java
return this.client.send(
  RequestDetails.Builder.get(this.cfg.customerURL())
    .request(this.cfg.getCustomers())
    .param(id)
    .build(),
  new ParameterizedTypeReference<CustomerData>() {}
).thenApply(this.exceptionHandlerFactory.map(this.mapper));
```

Here we use an instance of our helper HTTP rest client: **this.client**;
**RequestDetails.Builder** to construct an HTTP request details