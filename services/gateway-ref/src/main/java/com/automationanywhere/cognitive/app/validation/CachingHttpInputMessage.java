package com.automationanywhere.cognitive.app.validation;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.Scanner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.util.MimeType;

/**
 * Reads contents of an HttpInputMessage and caches it.
 * This allows the {@link #getBody()} method to return new instances of the {@link InputStream}
 * every time the method id called.
 */
public class CachingHttpInputMessage implements HttpInputMessage {

  private final ExceptionHandlerFactory ehf;
  private final HttpInputMessage inputMessage;

  private final String contents;
  private final Charset charset;

  public CachingHttpInputMessage(final HttpInputMessage inputMessage,
      final ExceptionHandlerFactory ehf) {
    this.inputMessage = inputMessage;
    this.ehf = ehf;

    this.charset = Optional.ofNullable(this.inputMessage.getHeaders().getContentType())
        .map(MimeType::getCharset)
        .orElse(StandardCharsets.UTF_8);

    this.contents = this.getContents(this.inputMessage, this.charset);
  }

  public String getContents() {
    return this.contents;
  }

  private String getContents(final HttpInputMessage inputMessage, final Charset charset) {
    String contents = null;

    Scanner s = new Scanner(this.ehf.map(inputMessage, HttpInputMessage::getBody), charset.name());
    try {
      contents = s.useDelimiter("\\A").next();
    } finally {
      s.close();
    }

    return contents;
  }

  @Override
  public InputStream getBody() throws IOException {
    return new ByteArrayInputStream(this.contents.getBytes(this.charset));
  }

  @Override
  public HttpHeaders getHeaders() {
    return this.inputMessage.getHeaders();
  }
}
