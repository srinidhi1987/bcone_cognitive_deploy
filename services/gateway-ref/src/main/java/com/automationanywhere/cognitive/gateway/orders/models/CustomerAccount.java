package com.automationanywhere.cognitive.gateway.orders.models;

import com.automationanywhere.cognitive.id.Id;

public class CustomerAccount {

  public final Id id;
  public final String firstName;
  public final String lastName;

  public CustomerAccount(final Id id, final String firstName, final String lastName) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
