package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class ProductDTO {

  public final String name;
  public final Long amount;

  public ProductDTO(
      @JsonProperty("name") final String name,
      @JsonProperty("amount") final Long amount
  ) {
    this.name = name;
    this.amount = amount;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      ProductDTO that = (ProductDTO) o;

      result = Objects.equals(this.name, that.name)
          && Objects.equals(this.amount, that.amount);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.name)
        ^ Objects.hashCode(this.amount);
  }
}
