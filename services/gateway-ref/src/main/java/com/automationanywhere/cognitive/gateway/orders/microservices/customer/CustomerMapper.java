package com.automationanywhere.cognitive.gateway.orders.microservices.customer;

import com.automationanywhere.cognitive.gateway.orders.models.Customer;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccount;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

  public CustomerAccount mapCustomerAccount(final CustomerData input) {
    String firstName = input.name;
    String lastName = "";

    String[] names = firstName.split(" +");
    if (names.length == 2) {
      firstName = names[0];
      lastName = names[1];
    }

    return new CustomerAccount(input.customerId, firstName, lastName);
  }

  @Deprecated
  public Customer mapCustomer(final CustomerData input) {
    return new Customer(input.customerId, input.name);
  }
}
