package com.automationanywhere.cognitive.errors;

import java.util.function.Function;

/**
 * Converts data from one type into another.
 * Similar to {@link Function} but allows throwing exceptions.
 */
public interface Mapper<I, O> {

  /**
   * Converts data from one type into another.
   *
   * @param input - data to convert.
   * @return converted data.
   * @throws Exception - thrown in case of an error which the mapper is not able to handle itself.
   */
  O map(I input) throws Exception;
}
