package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Deprecated
public class CustomerOrderDTO {

  public final Id orderId;
  public final CustomerDTO customer;
  public final List<ProductDTO> products;

  public CustomerOrderDTO(
      @JsonProperty("orderId") final Id orderId,
      @JsonProperty("customer") final CustomerDTO customer,
      @JsonProperty("products") final List<ProductDTO> products
  ) {
    this.orderId = orderId;
    this.customer = customer;
    this.products =
        products == null ? null : Collections.unmodifiableList(new ArrayList<>(products));
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      CustomerOrderDTO that = (CustomerOrderDTO) o;

      result = Objects.equals(this.orderId, that.orderId)
          && Objects.equals(this.customer, that.customer)
          && Objects.equals(this.products, that.products);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.orderId)
        ^ Objects.hashCode(this.customer)
        ^ Objects.hashCode(this.products);
  }
}
