package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import java.util.ArrayList;
import java.util.List;
import org.apache.catalina.Valve;
import org.apache.catalina.valves.AccessLogValve;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;

/**
 * Replaces access log valve with a custom access log valve.
 */
public class CustomTomcatEmbeddedServletContainerFactory extends
    TomcatEmbeddedServletContainerFactory {

  private static final String DEFAULT_FORMAT =
      "%t %a:%{remote}p->%v:%{local}p \"%m %U%q %H\" %s %b %D-%Fms";

  private final String pattern;

  public CustomTomcatEmbeddedServletContainerFactory(final String pattern) {
    this.pattern = pattern;
  }

  public CustomTomcatEmbeddedServletContainerFactory() {
    this(DEFAULT_FORMAT);
  }

  @Override
  public void addEngineValves(final Valve... engineValves) {
    List<Valve> valves = this.createValvesList();
    for (final Valve v : engineValves) {
      if (v instanceof AccessLogValve) {
        AccessLogValve prevValve = (AccessLogValve) v;

        CustomAccessLogValve valve = createCustomAccessLogValve();
        valve.setPattern(this.pattern);
        valve.setRequestAttributesEnabled(prevValve.getRequestAttributesEnabled());

        valves.add(valve);
      } else {
        valves.add(v);
      }
    }

    super.addEngineValves(valves.toArray(new Valve[valves.size()]));
  }

  CustomAccessLogValve createCustomAccessLogValve() {
    return new CustomAccessLogValve();
  }

  List<Valve> createValvesList() {
    return new ArrayList<>();
  }
}
