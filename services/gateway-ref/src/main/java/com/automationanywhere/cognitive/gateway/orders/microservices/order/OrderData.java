package com.automationanywhere.cognitive.gateway.orders.microservices.order;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

@Deprecated
public class OrderData {

  public final Id orderId;
  public final Id customerId;
  public final List<Map<String, Long>> products;

  public OrderData(
      @JsonProperty("orderId") final Id orderId,
      @JsonProperty("customerId") final Id customerId,
      @JsonProperty("products") final List<Map<String, Long>> products
  ) {
    this.orderId = orderId;
    this.customerId = customerId;
    this.products = products;
  }
}
