package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerJson {

  public final Id id;

  public CustomerJson(
      @JsonProperty("id") final Id id
  ) {
    this.id = id;
  }
}
