package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccountOrder;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerOrder;
import com.automationanywhere.cognitive.gateway.orders.models.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CustomerOrderListMapper {

  public List<CustomerAccountOrderDTO> mapCustomerAccountOrderList(
      final List<CustomerAccountOrder> orders) {
    List<CustomerAccountOrderDTO> orderDTOs = new ArrayList<>();

    for (final CustomerAccountOrder order : orders) {
      List<ProductDTO> products = this.mapProducts(order.products);

      orderDTOs.add(new CustomerAccountOrderDTO(
          order.id, order.createdAt,
          new CustomerAccountDTO(order.customer.id, order.customer.firstName,
              order.customer.lastName),
          products
      ));
    }

    return orderDTOs;
  }

  @Deprecated
  public List<CustomerOrderDTO> mapOrderList(final List<CustomerOrder> orders) {
    List<CustomerOrderDTO> result = new ArrayList<>();

    for (final CustomerOrder order : orders) {
      List<ProductDTO> products = this.mapProducts(order.products);

      result.add(new CustomerOrderDTO(
          order.id, new CustomerDTO(order.customer.id, order.customer.name), products
      ));
    }

    return result;
  }

  private List<ProductDTO> mapProducts(final List<Product> products) {
    List<ProductDTO> result = new ArrayList<>();

    for (final Product p : products) {
      result.add(new ProductDTO(p.name, p.quantity));
    }
    return result;
  }
}
