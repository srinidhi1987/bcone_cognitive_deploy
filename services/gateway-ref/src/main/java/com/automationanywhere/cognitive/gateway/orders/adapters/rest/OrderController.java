package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static com.automationanywhere.cognitive.validation.Schemas.In.CUSTOMER;
import static com.automationanywhere.cognitive.validation.Schemas.Out.CUSTOMER_ACCOUNT_ORDERS;
import static com.automationanywhere.cognitive.validation.Schemas.Out.CUSTOMER_ORDERS;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.orders.OrderService;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OrderController {

  private final OrderService orderService;
  private final CustomerOrderListMapper mapper;
  private final ExceptionHandlerFactory exceptionHandlerFactory;

  @Autowired
  public OrderController(
      final OrderService orderService,
      final CustomerOrderListMapper mapper,
      final ExceptionHandlerFactory exceptionHandlerFactory
  ) {
    this.orderService = orderService;
    this.mapper = mapper;
    this.exceptionHandlerFactory = exceptionHandlerFactory;
  }

  @RequestMapping(
      path = "/customers/{customerId}/orders",
      method = RequestMethod.GET,
      produces = {
          "application/vnd.cognitive.v1+json"
      }
  )
  @Deprecated
  @SchemaValidation(output = CUSTOMER_ORDERS)
  public CompletableFuture<List<CustomerOrderDTO>> getCustomerOrders(
      @PathVariable("customerId") final Id customerId
  ) throws Exception {
    return this.orderService.getCustomerOrders(customerId).thenApply(
        this.exceptionHandlerFactory.fromMapper(this.mapper::mapOrderList)
    );
  }

  @RequestMapping(
      path = "/customers/{customerId}/orders",
      method = RequestMethod.GET,
      produces = {
          "application/json",
          "application/vnd.cognitive.v2+json"
      }
  )
  @SchemaValidation(output = CUSTOMER_ACCOUNT_ORDERS)
  public CompletableFuture<List<CustomerAccountOrderDTO>> getCustomerAccountOrders(
      @PathVariable("customerId") final UUIDId customerId
  ) {
    return this.orderService.getCustomerAccountOrders(customerId).thenApply(
        this.exceptionHandlerFactory.fromMapper(this.mapper::mapCustomerAccountOrderList)
    );
  }

  @RequestMapping(
      path = "/customers/orders",
      method = RequestMethod.POST,
      consumes = {
          "application/json",
          "application/vnd.cognitive.v2+json"
      },
      produces = {
          "application/json",
          "application/vnd.cognitive.v2+json"
      }
  )
  @SchemaValidation(
      input = CUSTOMER,
      output = CUSTOMER_ACCOUNT_ORDERS
  )
  public CompletableFuture<List<CustomerAccountOrderDTO>> loadCustomerAccountOrders(
      @RequestBody final CustomerJson customer
  ) {
    return this.orderService.getCustomerAccountOrders(customer.id).thenApply(
        this.exceptionHandlerFactory.fromMapper(this.mapper::mapCustomerAccountOrderList)
    );
  }
}
