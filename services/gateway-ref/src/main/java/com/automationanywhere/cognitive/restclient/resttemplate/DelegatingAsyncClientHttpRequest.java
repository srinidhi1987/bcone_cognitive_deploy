package com.automationanywhere.cognitive.restclient.resttemplate;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.AsyncClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.concurrent.ListenableFuture;

/**
 * Delegates all method calls to an underlying delegate object.
 * Useful for decorator implementations.
 */
public class DelegatingAsyncClientHttpRequest implements AsyncClientHttpRequest {

  private final AsyncClientHttpRequest delegate;

  public DelegatingAsyncClientHttpRequest(final AsyncClientHttpRequest delegate) {
    this.delegate = delegate;
  }

  @Override
  public ListenableFuture<ClientHttpResponse> executeAsync() throws IOException {
    return this.delegate.executeAsync();
  }

  @Override
  public HttpMethod getMethod() {
    return this.delegate.getMethod();
  }

  @Override
  public URI getURI() {
    return this.delegate.getURI();
  }

  @Override
  public HttpHeaders getHeaders() {
    return this.delegate.getHeaders();
  }

  @Override
  public OutputStream getBody() throws IOException {
    return this.delegate.getBody();
  }
}
