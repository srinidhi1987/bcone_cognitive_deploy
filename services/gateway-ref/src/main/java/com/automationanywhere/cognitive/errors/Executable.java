package com.automationanywhere.cognitive.errors;

/**
 * Executes a code.
 * Similar to {@link Runnable} but allows throwing of exceptions.
 */
public interface Executable {

  /**
   * Executes something.
   *
   * @throws Exception - thrown in case if the executable is not able to handle an error itself.
   */
  void execute() throws Exception;
}
