package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import java.io.CharArrayWriter;
import java.io.StringWriter;
import java.util.Date;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.AbstractAccessLogValve;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Sends all access logs to the standard logger.
 */
public class CustomAccessLogValve extends AbstractAccessLogValve {

  private static final Logger LOGGER = LogManager
      .getLogger("com.automationanywhere.cognitive.gateway.components.AccessLogger");

  protected void log(final CharArrayWriter message) {
    Logger accessLogger = this.getAccessLogger();
    try {
      StringWriter w = new StringWriter();
      message.writeTo(w);
      message.flush();

      accessLogger.trace(w.toString());
    } catch (final Exception ex) {
      accessLogger.error("Failed to write a log message", ex);
    }
  }

  Logger getAccessLogger() {
    return LOGGER;
  }

  @Override
  protected AbstractAccessLogValve.AccessLogElement createAccessLogElement(final char pattern) {
    AbstractAccessLogValve.AccessLogElement ale = null;
    switch (pattern) {
      case 't':
        ale = new DateTimeAccessLogElement();
        break;
      case 'a':
        ale = new RemoteAddrElementDecorator(
            new AccessLogElementWrapper(super.createAccessLogElement(pattern)));
        break;
      default:
        ale = super.createAccessLogElement(pattern);
    }
    return ale;
  }

  public interface CustomAccessLogElement extends AbstractAccessLogValve.AccessLogElement {

  }

  public class AccessLogElementWrapper implements CustomAccessLogValve.CustomAccessLogElement {

    private final AbstractAccessLogValve.AccessLogElement element;

    public AccessLogElementWrapper(final AbstractAccessLogValve.AccessLogElement element) {
      this.element = element;
    }

    @Override
    public void addElement(final CharArrayWriter buf, final Date date, final Request request,
        final Response response, final long time) {
      this.element.addElement(buf, date, request, response, time);
    }
  }
}
