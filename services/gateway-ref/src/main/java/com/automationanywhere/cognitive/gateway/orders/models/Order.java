package com.automationanywhere.cognitive.gateway.orders.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Deprecated
public class Order {

  public final Id id;
  public final Id customerId;
  public final List<Product> products;

  public Order(final Id id, final Id customerId, final List<Product> products) {
    this.id = id;
    this.customerId = customerId;
    this.products = Collections.unmodifiableList(new ArrayList<>(products));
  }
}
