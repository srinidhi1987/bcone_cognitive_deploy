package com.automationanywhere.cognitive.gateway.orders.models;

import com.automationanywhere.cognitive.id.Id;

@Deprecated
public class Customer {

  public final Id id;
  public final String name;

  public Customer(final Id id, final String name) {
    this.id = id;
    this.name = name;
  }
}
