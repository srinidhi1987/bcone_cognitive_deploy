package com.automationanywhere.cognitive.app;

import com.automationanywhere.cognitive.common.configuration.ConfigurationProvider;
import java.util.Properties;

public class ApplicationConfiguration {

  private static final String DEFAULT_BUILD_RESOURCE_NAME = "build.version";

  private final ConfigurationProvider provider;
  private final Properties props;

  public ApplicationConfiguration(final ConfigurationProvider provider) {
    this(provider, DEFAULT_BUILD_RESOURCE_NAME);
  }

  public ApplicationConfiguration(final ConfigurationProvider provider,
      final String buildResourceName) {
    this.provider = provider;

    this.props = this.createProperties(buildResourceName);
  }

  private Properties createProperties(final String buildResourceName) {
    Properties props = new Properties();
    try {
      props.load(this.getClass().getClassLoader().getResourceAsStream(buildResourceName));
    } catch (final Exception ex) {
      throw new RuntimeException("Fatal error, cannot load the build details", ex);
    }
    return props;
  }

  public String applicationName() {
    return this.props.getProperty("Application-name");
  }

  public String applicationVersion() {
    return this.props.getProperty("Version");
  }

  public String orderURL() {
    return this.provider.getValue("aa.cognitive.micro-services.order.url");
  }

  public String getCustomerOrders() {
    return this.provider.getValue("aa.cognitive.micro-services.order.get-customer-orders");
  }

  public String customerURL() {
    return this.provider.getValue("aa.cognitive.micro-services.customer.url");
  }

  public String getCustomers() {
    return this.provider.getValue("aa.cognitive.micro-services.customer.get-customer");
  }

  public String orderHealthCheck() {
    return this.provider.getValue("aa.cognitive.micro-services.order.health-check");
  }

  public String customerHealthCheck() {
    return this.provider.getValue("aa.cognitive.micro-services.customer.health-check");
  }

  public long asyncExecutorCorePoolSize() {
    return this.provider.getValue("aa.cognitive.gateway.async-executor.core-pool-size");
  }

  public long asyncExecutorMaxPoolSize() {
    return this.provider.getValue("aa.cognitive.gateway.async-executor.max-pool-size");
  }

  public long asyncExecutorKeepAliveTimeout() {
    return this.provider.getValue("aa.cognitive.gateway.async-executor.keep-alive-timeout");
  }

  public long restTemplateMaxConnCount() {
    return this.provider.getValue("aa.cognitive.gateway.rest-template.max-conn-count");
  }

  public long restTemplateKeepAliveTimeout() {
    return this.provider.getValue("aa.cognitive.gateway.rest-template.keep-alive-timeout");
  }

  public long restTemplateConnectTimeout() {
    return this.provider.getValue("aa.cognitive.gateway.rest-template.connect-timeout");
  }

  public long restTemplateReadTimeout() {
    return this.provider.getValue("aa.cognitive.gateway.rest-template.read-timeout");
  }

  public long restTemplateMaxRetryCount() {
    return this.provider.getValue("aa.cognitive.gateway.rest-template.max-retry-count");
  }

  public long restTemplateRetryDelay() {
    return this.provider.getValue("aa.cognitive.gateway.rest-template.retry-delay");
  }

  public long asyncHTTPServletRequestTimeout() {
    return this.provider.getValue("aa.cognitive.gateway.async-http-servlet-request-timeout");
  }
}
