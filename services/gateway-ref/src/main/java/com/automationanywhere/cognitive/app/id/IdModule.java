package com.automationanywhere.cognitive.app.id;

import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.integer.LongId;
import com.automationanywhere.cognitive.id.integer.LongIdTransformer;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * Jackson module supporting UUIDId classes.
 * It allows to use {@link Id} in models
 * and UUIDs represented as strings will be converted to UUIDId objects
 * or vice versa.
 */
public class IdModule extends SimpleModule {

  private static final String NAME = IdModule.class.getSimpleName();

  private static final VersionUtil VERSION_UTIL = new VersionUtil() {
  };

  public IdModule(
      final CompositeIdTransformer idTransformer,
      final UUIDIdTransformer uuidIdTransformer,
      final LongIdTransformer longIdTransformer,
      final StringIdTransformer stringIdTransformer
  ) {
    super(NAME, VERSION_UTIL.version());

    addSerializer(Id.class, new IdSerializer<>(idTransformer));
    addSerializer(UUIDId.class, new IdSerializer<>(uuidIdTransformer));
    addSerializer(LongId.class, new IdSerializer<>(longIdTransformer));
    addSerializer(StringId.class, new IdSerializer<>(stringIdTransformer));

    addDeserializer(Id.class, new IdDeserializer<>(idTransformer, Id.class));
    addDeserializer(UUIDId.class, new IdDeserializer<>(uuidIdTransformer, UUIDId.class));
    addDeserializer(LongId.class, new IdDeserializer<>(longIdTransformer, LongId.class));
    addDeserializer(StringId.class, new IdDeserializer<>(stringIdTransformer, StringId.class));
  }
}
