package com.automationanywhere.cognitive.gateway.orders.microservices.order;

import static com.automationanywhere.cognitive.restclient.RequestDetails.Types.JSON;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.orders.models.Order;
import com.automationanywhere.cognitive.gateway.orders.models.OrderV2;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class OrderMicroservice {

  private final RestClient client;
  private final OrderListMapper mapper;
  private final ApplicationConfiguration cfg;
  private final ExceptionHandlerFactory exceptionHandlerFactory;

  @Autowired
  public OrderMicroservice(
      final RestClient client,
      final OrderListMapper mapper,
      final ApplicationConfiguration cfg,
      final ExceptionHandlerFactory exceptionHandlerFactory
  ) {
    this.client = client;
    this.mapper = mapper;
    this.cfg = cfg;
    this.exceptionHandlerFactory = exceptionHandlerFactory;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<List<OrderV2>> getOrdersV2(final Id customerId) {
    return this.client.send(
        RequestDetails.Builder.get(this.cfg.orderURL())
            .request(this.cfg.getCustomerOrders())
            .param(customerId)
            .accept(JSON, 2)
            .build(),
        new ParameterizedTypeReference<List<OrderDataV2>>() {
        }
    ).thenApply(this.exceptionHandlerFactory.fromMapper(this.mapper::mapCreationTrackingOrder));
  }

  @Async("microserviceExecutor")
  @Deprecated
  public CompletableFuture<List<Order>> getOrders(final Id customerId) {
    return this.client.send(
        RequestDetails.Builder.get(this.cfg.orderURL())
            .request(this.cfg.getCustomerOrders())
            .param(customerId)
            .accept(JSON, 1)
            .build(),
        new ParameterizedTypeReference<List<OrderData>>() {
        }
    ).thenApply(this.exceptionHandlerFactory.fromMapper(this.mapper::mapOrder));
  }
}
