package com.automationanywhere.cognitive.gateway.orders.microservices.order;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class OrderDataV2 {

  public final Id orderId;
  public final Date createdAt;
  public final Id customerId;
  public final List<Map<String, Long>> products;

  public OrderDataV2(
      @JsonProperty("orderId") final Id orderId,
      @JsonProperty("createdAt") final Date createdAt,
      @JsonProperty("customerId") final Id customerId,
      @JsonProperty("products") final List<Map<String, Long>> products
  ) {
    this.orderId = orderId;
    this.createdAt = createdAt;
    this.customerId = customerId;
    this.products = products;
  }
}
