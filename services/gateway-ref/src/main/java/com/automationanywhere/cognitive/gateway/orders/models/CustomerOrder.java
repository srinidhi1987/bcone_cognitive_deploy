package com.automationanywhere.cognitive.gateway.orders.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Deprecated
public class CustomerOrder {

  public final Id id;
  public final Customer customer;
  public final List<Product> products;

  public CustomerOrder(final Id id, final Customer customer, final List<Product> products) {
    this.id = id;
    this.customer = customer;
    this.products = Collections.unmodifiableList(new ArrayList<>(products));
  }
}
