package com.automationanywhere.cognitive.gateway.orders.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class OrderV2 {

  public final Id id;
  public final Date createdAt;
  public final Id customerId;
  public final List<Product> products;

  public OrderV2(final Id id, final Date createdAt, final Id customerId,
      final List<Product> products) {
    this.id = id;
    this.createdAt = createdAt;
    this.customerId = customerId;
    this.products = Collections.unmodifiableList(new ArrayList<>(products));
  }
}
