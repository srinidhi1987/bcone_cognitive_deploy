package com.automationanywhere.cognitive.app.id.converters;

import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IdConverter extends BaseIdConverter<Id> {

  @Autowired
  public IdConverter(final CompositeIdTransformer idTransformer) {
    super(idTransformer);
  }
}
