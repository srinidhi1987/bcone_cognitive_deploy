package com.automationanywhere.cognitive.id.integer;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.NumericNode;
import org.springframework.stereotype.Component;

@Component
public class LongIdTransformer implements IdTransformer {
  @Override
  public LongId fromNode(final TreeNode node) throws IllegalArgumentException {
    LongId result = null;
    if (node != null) {
      if (!(node instanceof NullNode)) {
        if (!(node instanceof NumericNode)) {
          throw new UnsupportedOperationException(node.getClass() + " is not supported");
        }
        NumericNode numericNode = (NumericNode) node;
        if (!numericNode.isIntegralNumber()) {
          throw new UnsupportedOperationException(node.getClass() + " is not supported");
        }
        if (!numericNode.canConvertToLong()) {
          throw new UnsupportedOperationException(numericNode.asText() + " is not supported");
        }
        result = new LongId(numericNode.asLong());
      }
    }
    return result;
  }

  @Override
  public TreeNode toNode(final Id id) {
    TreeNode node = null;

    if (id == null) {
      node = NullNode.getInstance();
    } else {
      if (!supports(id.getClass())) {
        throw new UnsupportedOperationException(id.getClass() + " is not supported");
      }
      node = new LongNode(((LongId) id).id);
    }
    return node;
  }

  @Override
  public LongId fromString(final String id) {
    LongId result = null;
    if (id != null) {
      try {
        result = new LongId(Long.parseLong(id));
      } catch (final NumberFormatException ex) {
        throw new UnsupportedOperationException(id + " is not supported");
      }
    }
    return result;
  }

  @Override
  public String toString(final Id id) {
    if (id != null && !supports(id.getClass())) {
      throw new UnsupportedOperationException(id.getClass() + " is not supported");
    }

    return id == null ? null : ((Long) ((LongId) id).id).toString();
  }

  @Override
  public boolean supports(final Class<? extends Id> type) {
    return type.equals(LongId.class);
  }
}
