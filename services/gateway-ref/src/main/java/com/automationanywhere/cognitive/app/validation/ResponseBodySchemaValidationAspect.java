package com.automationanywhere.cognitive.app.validation;

import com.automationanywhere.cognitive.errors.ErrorDTO;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class ResponseBodySchemaValidationAspect implements ResponseBodyAdvice<Object> {

  private final ClasspathSchemaValidator validator;

  @Autowired
  public ResponseBodySchemaValidationAspect(final ClasspathSchemaValidator validator) {
    this.validator = validator;
  }

  @Override
  public boolean supports(
      final MethodParameter returnType,
      final Class<? extends HttpMessageConverter<?>> converterType
  ) {
    return true;
  }

  @Override
  public Object beforeBodyWrite(
      final Object body,
      final MethodParameter returnType,
      final MediaType selectedContentType,
      final Class<? extends HttpMessageConverter<?>> selectedConverterType,
      final ServerHttpRequest request,
      final ServerHttpResponse response
  ) {
    if (!(body instanceof ErrorDTO)) {
      SchemaValidation annotation = returnType.getMethod().getAnnotation(SchemaValidation.class);
      if (!(annotation == null || annotation.output().isEmpty())) {
        this.validator.checkOutput(body, annotation.output());
      }
    }
    return body;
  }
}
