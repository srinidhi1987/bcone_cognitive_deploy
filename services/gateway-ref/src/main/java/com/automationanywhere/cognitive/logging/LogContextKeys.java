package com.automationanywhere.cognitive.logging;

/**
 * Keys used to reference values stored in the log thread context.
 */
public enum LogContextKeys {
  CID("cid"),
  HOST_NAME("hostname"),
  CLIENT_IP("clientip"),
  APPLICATION_NAME("appname"),
  APPLICATION_VERSION("version"),
  SESSION_ID("sid"),
  USER_ID("uid"),
  TENANT_ID("tid");

  public final String key;

  private LogContextKeys(final String key) {
    this.key = key;
  }
}
