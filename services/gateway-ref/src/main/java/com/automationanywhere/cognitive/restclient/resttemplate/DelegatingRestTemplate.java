package com.automationanywhere.cognitive.restclient.resttemplate;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplateHandler;

/**
 * Wraps another rest template object and delegates all own method execution to the wrapped rest
 * template. Useful for the implementation of decorators.
 */
public class DelegatingRestTemplate extends RestTemplate {

  private final RestTemplate delegate;

  public DelegatingRestTemplate(final RestTemplate delegate) {
    this.delegate = delegate;
  }

  public List<HttpMessageConverter<?>> getMessageConverters() {
    return this.delegate.getMessageConverters();
  }

  public void setMessageConverters(final List<HttpMessageConverter<?>> messageConverters) {
    this.delegate.setMessageConverters(messageConverters);
  }

  public ResponseErrorHandler getErrorHandler() {
    return this.delegate.getErrorHandler();
  }

  public void setErrorHandler(final ResponseErrorHandler errorHandler) {
    this.delegate.setErrorHandler(errorHandler);
  }

  public void setDefaultUriVariables(final Map<String, ?> defaultUriVariables) {
    this.delegate.setDefaultUriVariables(defaultUriVariables);
  }

  public UriTemplateHandler getUriTemplateHandler() {
    return this.delegate.getUriTemplateHandler();
  }

  public void setUriTemplateHandler(final UriTemplateHandler handler) {
    this.delegate.setUriTemplateHandler(handler);
  }

  public <T> T getForObject(final String url, final Class<T> responseType,
      final Object... uriVariables) throws RestClientException {
    return this.delegate.getForObject(url, responseType, uriVariables);
  }

  public <T> T getForObject(final String url, final Class<T> responseType,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.getForObject(url, responseType, uriVariables);
  }

  public <T> T getForObject(final URI url, final Class<T> responseType) throws RestClientException {
    return this.delegate.getForObject(url, responseType);
  }

  public <T> ResponseEntity<T> getForEntity(final String url, final Class<T> responseType,
      final Object... uriVariables) throws RestClientException {
    return this.delegate.getForEntity(url, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> getForEntity(final String url, final Class<T> responseType,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.getForEntity(url, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> getForEntity(final URI url, final Class<T> responseType)
      throws RestClientException {
    return this.delegate.getForEntity(url, responseType);
  }

  public HttpHeaders headForHeaders(final String url, final Object... uriVariables)
      throws RestClientException {
    return this.delegate.headForHeaders(url, uriVariables);
  }

  public HttpHeaders headForHeaders(final String url, final Map<String, ?> uriVariables)
      throws RestClientException {
    return this.delegate.headForHeaders(url, uriVariables);
  }

  public HttpHeaders headForHeaders(final URI url) throws RestClientException {
    return this.delegate.headForHeaders(url);
  }

  public URI postForLocation(final String url, final Object request, final Object... uriVariables)
      throws RestClientException {
    return this.delegate.postForLocation(url, request, uriVariables);
  }

  public URI postForLocation(final String url, final Object request,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.postForLocation(url, request, uriVariables);
  }

  public URI postForLocation(final URI url, final Object request) throws RestClientException {
    return this.delegate.postForLocation(url, request);
  }

  public <T> T postForObject(final String url, final Object request, final Class<T> responseType,
      final Object... uriVariables) throws RestClientException {
    return this.delegate.postForObject(url, request, responseType, uriVariables);
  }

  public <T> T postForObject(final String url, final Object request, final Class<T> responseType,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.postForObject(url, request, responseType, uriVariables);
  }

  public <T> T postForObject(final URI url, final Object request, final Class<T> responseType)
      throws RestClientException {
    return this.delegate.postForObject(url, request, responseType);
  }

  public <T> ResponseEntity<T> postForEntity(final String url, final Object request,
      final Class<T> responseType, final Object... uriVariables) throws RestClientException {
    return this.delegate.postForEntity(url, request, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> postForEntity(final String url, final Object request,
      final Class<T> responseType, final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.postForEntity(url, request, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> postForEntity(final URI url, final Object request,
      final Class<T> responseType) throws RestClientException {
    return this.delegate.postForEntity(url, request, responseType);
  }

  public void put(final String url, final Object request, final Object... uriVariables)
      throws RestClientException {
    this.delegate.put(url, request, uriVariables);
  }

  public void put(final String url, final Object request, final Map<String, ?> uriVariables)
      throws RestClientException {
    this.delegate.put(url, request, uriVariables);
  }

  public void put(final URI url, final Object request) throws RestClientException {
    this.delegate.put(url, request);
  }

  public <T> T patchForObject(final String url, final Object request, final Class<T> responseType,
      final Object... uriVariables) throws RestClientException {
    return this.delegate.patchForObject(url, request, responseType, uriVariables);
  }

  public <T> T patchForObject(final String url, final Object request, final Class<T> responseType,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.patchForObject(url, request, responseType, uriVariables);
  }

  public <T> T patchForObject(final URI url, final Object request, final Class<T> responseType)
      throws RestClientException {
    return this.delegate.patchForObject(url, request, responseType);
  }

  public void delete(final String url, final Object... uriVariables) throws RestClientException {
    this.delegate.delete(url, uriVariables);
  }

  public void delete(final String url, final Map<String, ?> uriVariables)
      throws RestClientException {
    this.delegate.delete(url, uriVariables);
  }

  public void delete(final URI url) throws RestClientException {
    this.delegate.delete(url);
  }

  public Set<HttpMethod> optionsForAllow(final String url, final Object... uriVariables)
      throws RestClientException {
    return this.delegate.optionsForAllow(url, uriVariables);
  }

  public Set<HttpMethod> optionsForAllow(final String url, final Map<String, ?> uriVariables)
      throws RestClientException {
    return this.delegate.optionsForAllow(url, uriVariables);
  }

  public Set<HttpMethod> optionsForAllow(final URI url) throws RestClientException {
    return this.delegate.optionsForAllow(url);
  }

  public <T> ResponseEntity<T> exchange(final String url, final HttpMethod method,
      final HttpEntity<?> requestEntity, final Class<T> responseType, final Object... uriVariables)
      throws RestClientException {
    return this.delegate.exchange(url, method, requestEntity, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> exchange(final String url, final HttpMethod method,
      final HttpEntity<?> requestEntity, final Class<T> responseType,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.exchange(url, method, requestEntity, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> exchange(final URI url, final HttpMethod method,
      final HttpEntity<?> requestEntity, final Class<T> responseType) throws RestClientException {
    return this.delegate.exchange(url, method, requestEntity, responseType);
  }

  public <T> ResponseEntity<T> exchange(final String url, final HttpMethod method,
      final HttpEntity<?> requestEntity, final ParameterizedTypeReference<T> responseType,
      final Object... uriVariables) throws RestClientException {
    return this.delegate.exchange(url, method, requestEntity, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> exchange(final String url, final HttpMethod method,
      final HttpEntity<?> requestEntity, final ParameterizedTypeReference<T> responseType,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.exchange(url, method, requestEntity, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> exchange(final URI url, final HttpMethod method,
      final HttpEntity<?> requestEntity, final ParameterizedTypeReference<T> responseType)
      throws RestClientException {
    return this.delegate.exchange(url, method, requestEntity, responseType);
  }

  public <T> ResponseEntity<T> exchange(final RequestEntity<?> requestEntity,
      final Class<T> responseType) throws RestClientException {
    return this.delegate.exchange(requestEntity, responseType);
  }

  public <T> ResponseEntity<T> exchange(final RequestEntity<?> requestEntity,
      final ParameterizedTypeReference<T> responseType) throws RestClientException {
    return this.delegate.exchange(requestEntity, responseType);
  }

  public <T> T execute(final String url, final HttpMethod method,
      final RequestCallback requestCallback, final ResponseExtractor<T> responseExtractor,
      final Object... uriVariables) throws RestClientException {
    return this.delegate.execute(url, method, requestCallback, responseExtractor, uriVariables);
  }

  public <T> T execute(final String url, final HttpMethod method,
      final RequestCallback requestCallback, final ResponseExtractor<T> responseExtractor,
      final Map<String, ?> uriVariables) throws RestClientException {
    return this.delegate.execute(url, method, requestCallback, responseExtractor, uriVariables);
  }

  public <T> T execute(final URI url, final HttpMethod method,
      final RequestCallback requestCallback, final ResponseExtractor<T> responseExtractor)
      throws RestClientException {
    return this.delegate.execute(url, method, requestCallback, responseExtractor);
  }

  public List<ClientHttpRequestInterceptor> getInterceptors() {
    return this.delegate.getInterceptors();
  }

  public void setInterceptors(final List<ClientHttpRequestInterceptor> interceptors) {
    this.delegate.setInterceptors(interceptors);
  }

  public ClientHttpRequestFactory getRequestFactory() {
    return this.delegate.getRequestFactory();
  }

  public void setRequestFactory(final ClientHttpRequestFactory requestFactory) {
    this.delegate.setRequestFactory(requestFactory);
  }
}
