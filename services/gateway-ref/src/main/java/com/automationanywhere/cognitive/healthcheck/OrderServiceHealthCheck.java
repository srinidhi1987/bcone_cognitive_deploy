package com.automationanywhere.cognitive.healthcheck;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.common.healthcheck.command.MicroserviceHealthCheckCommand;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Order Service health check command.
 */
@Component
public class OrderServiceHealthCheck extends MicroserviceHealthCheckCommand {

  /**
   * Constructs an health check command for Order Service.
   *
   * @param config the instance of ApplicationConfiguration
   */
  @Autowired
  public OrderServiceHealthCheck(
      final ApplicationConfiguration config,
      final AsyncRestTemplate asyncRestTemplate
  ) {
    super(
        "Order Service",
        RequestDetails.Builder.get(config.orderURL()).request(config.orderHealthCheck()).build().url
    );
  }
}
