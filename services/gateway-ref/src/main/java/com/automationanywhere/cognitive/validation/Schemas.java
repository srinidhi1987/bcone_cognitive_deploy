package com.automationanywhere.cognitive.validation;

public interface Schemas {

  interface In {

    String CUSTOMER = "Customer";
  }

  interface Out {

    String CUSTOMER_ORDERS = "CustomerOrders";
    String CUSTOMER_ACCOUNT_ORDERS = "CustomerAccountOrders";
  }
}
