package com.automationanywhere.cognitive.logging;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Provides information to be used for request tracking.
 */
@Component
public class LogDetailsProvider {

  private static final Logger LOGGER = LogManager.getLogger(LogDetailsProvider.class);

  private final ApplicationConfiguration ApplicationConfiguration;

  @Autowired
  public LogDetailsProvider(final ApplicationConfiguration ApplicationConfiguration) {
    this.ApplicationConfiguration = ApplicationConfiguration;
  }

  public String getCID() {
    return UUID.randomUUID().toString();
  }

  public String getUserId() {
    return "<not implemented>";
  }

  public String getApplicationName() {
    return this.ApplicationConfiguration.applicationName();
  }

  public String getApplicationVersion() {
    return this.ApplicationConfiguration.applicationVersion();
  }

  public String getClientIP(final HttpServletRequest req) {
    return req.getRemoteAddr();
  }

  public String getHostName() {
    String hostName = null;
    try {
      hostName = this.getLocalHost().getCanonicalHostName();
    } catch (final UnknownHostException ex) {
      LOGGER.error("Failed to get the host ip address", ex);

      hostName = "<unknown>";
    }
    return hostName;
  }

  public void addTrackingInfo(
      final HttpServletRequest req,
      final Map<String, String> ctx,
      final LogContextKeys key,
      final CognitivePlatformHeaders header,
      final Supplier<String> provider
  ) {
    String oldValue = this.getValue(key);
    if (oldValue == null) {
      String value = null;
      if (header != null) {
        value = req.getHeader(header.headerName);
      }
      if (value == null) {
        value = "";
      }
      value = value.trim();

      if (value.isEmpty()) {
        if (provider == null) {
          value = "<unspecified>";
        } else {
          value = provider.get();
        }
      }

      ctx.put(key.key, value);
    }
  }

  String getValue(final LogContextKeys key) {
    return ThreadContext.get(key.key);
  }

  InetAddress getLocalHost() throws UnknownHostException {
    return InetAddress.getLocalHost();
  }
}
