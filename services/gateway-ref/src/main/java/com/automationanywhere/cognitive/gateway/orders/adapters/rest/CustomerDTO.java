package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

@Deprecated
public class CustomerDTO {

  public final Id customerId;
  public final String name;

  public CustomerDTO(
      @JsonProperty("customerId") final Id customerId,
      @JsonProperty("name") final String name
  ) {
    this.customerId = customerId;
    this.name = name;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      CustomerDTO that = (CustomerDTO) o;

      result = Objects.equals(this.customerId, that.customerId)
          && Objects.equals(this.name, that.name);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.customerId)
        ^ Objects.hashCode(this.name);
  }
}
