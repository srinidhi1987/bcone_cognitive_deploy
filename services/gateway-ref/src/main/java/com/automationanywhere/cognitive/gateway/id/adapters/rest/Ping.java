package com.automationanywhere.cognitive.gateway.id.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class Ping {
  public final Id id;

  public Ping(@JsonProperty("id") final Id id) {
    this.id = id;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      Ping ping = (Ping) o;

      result = Objects.equals(this.id, ping.id);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.id) ^ 32;
  }
}
