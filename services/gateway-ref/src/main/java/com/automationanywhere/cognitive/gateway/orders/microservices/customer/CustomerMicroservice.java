package com.automationanywhere.cognitive.gateway.orders.microservices.customer;

import static com.automationanywhere.cognitive.restclient.RequestDetails.Types.JSON;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.orders.models.Customer;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccount;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.restclient.RequestDetails;
import com.automationanywhere.cognitive.restclient.RestClient;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class CustomerMicroservice {

  private final RestClient client;
  private final CustomerMapper mapper;
  private final ApplicationConfiguration cfg;
  private final ExceptionHandlerFactory exceptionHandlerFactory;

  @Autowired
  public CustomerMicroservice(
      final RestClient client,
      final CustomerMapper mapper,
      final ApplicationConfiguration cfg,
      final ExceptionHandlerFactory exceptionHandlerFactory
  ) {
    this.client = client;
    this.mapper = mapper;
    this.cfg = cfg;
    this.exceptionHandlerFactory = exceptionHandlerFactory;
  }

  @Async("microserviceExecutor")
  public CompletableFuture<CustomerAccount> getCustomerAccount(final Id customerId) {
    return this.client.send(
        RequestDetails.Builder.get(this.cfg.customerURL())
            .request(this.cfg.getCustomers())
            .param(customerId)
            .accept(JSON, 2)
            .build(),
        new ParameterizedTypeReference<CustomerData>() {
        }
    ).thenApply(this.exceptionHandlerFactory.fromMapper(this.mapper::mapCustomerAccount));
  }

  @Async("microserviceExecutor")
  @Deprecated
  public CompletableFuture<Customer> getCustomer(final Id customerId) {
    return this.client.send(
        RequestDetails.Builder.get(this.cfg.customerURL())
            .request(this.cfg.getCustomers())
            .param(customerId)
            .accept(JSON, 1)
            .build(),
        new ParameterizedTypeReference<CustomerData>() {
        }
    ).thenApply(this.exceptionHandlerFactory.fromMapper(this.mapper::mapCustomer));
  }
}
