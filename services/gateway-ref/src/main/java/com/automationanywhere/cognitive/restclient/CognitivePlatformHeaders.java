package com.automationanywhere.cognitive.restclient;

/**
 * Headers that front-ends or microservices may use to track requests.
 */
public enum CognitivePlatformHeaders {
  CID("x-aa-cp-cid"),
  HOST_NAME("x-aa-cp-hostname"),
  APPLICATION_NAME("x-aa-cp-appname"),
  APPLICATION_VERSION("x-aa-cp-appversion"),
  SESSION_ID("x-aa-cp-sid"),
  TENANT_ID("x-aa-cp-tid"),
  CLIENT_IP("x-forwarded-for");

  public final String headerName;

  private CognitivePlatformHeaders(final String headerName) {
    this.headerName = headerName;
  }
}
