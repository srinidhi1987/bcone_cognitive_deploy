package com.automationanywhere.cognitive.id.string;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;

public class StringId implements Id {

  public final String id;

  public StringId(final String id) {
    if (id == null) {
      throw new IllegalArgumentException();
    }
    this.id = id;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      StringId stringId = (StringId) o;

      result = Objects.equals(this.id, stringId.id);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.id) ^ 32;
  }
}
