package com.automationanywhere.cognitive.restclient.resttemplate;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.AsyncClientHttpRequestExecution;
import org.springframework.http.client.AsyncClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.concurrent.ListenableFuture;

/**
 * Logs requests before sending them; logs responses upon receiving them.
 */
public class LoggingRestTemplateInterceptor implements AsyncClientHttpRequestInterceptor {

  private static final Logger LOGGER = LogManager.getLogger(LoggingRestTemplateInterceptor.class);

  @Override
  public ListenableFuture<ClientHttpResponse> intercept(final HttpRequest request,
      final byte[] body, final AsyncClientHttpRequestExecution execution) throws IOException {
    LOGGER.trace("Request {} {}", request.getMethod(), request.getURI());

    ListenableFuture<ClientHttpResponse> responseListenableFuture = execution
        .executeAsync(request, body);
    responseListenableFuture.addCallback(new LoggingResponseListenableFuture(request));

    return responseListenableFuture;
  }
}
