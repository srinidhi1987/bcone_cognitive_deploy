package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CustomerAccountOrderDTO {

  public final Id orderId;
  public final Date createdAt;
  public final CustomerAccountDTO customer;
  public final List<ProductDTO> products;

  public CustomerAccountOrderDTO(
      @JsonProperty("orderId") final Id orderId,
      @JsonProperty("createdAt") final Date createdAt,
      @JsonProperty("customer") final CustomerAccountDTO customer,
      @JsonProperty("products") final List<ProductDTO> products
  ) {
    this.orderId = orderId;
    this.createdAt = createdAt;
    this.customer = customer;
    this.products =
        products == null ? null : Collections.unmodifiableList(new ArrayList<>(products));
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      CustomerAccountOrderDTO that = (CustomerAccountOrderDTO) o;

      result = Objects.equals(this.orderId, that.orderId)
          && Objects.equals(this.createdAt, that.createdAt)
          && Objects.equals(this.customer, that.customer)
          && Objects.equals(this.products, that.products);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.orderId)
        ^ Objects.hashCode(this.createdAt)
        ^ Objects.hashCode(this.customer)
        ^ Objects.hashCode(this.products);
  }
}
