package com.automationanywhere.cognitive.app.errors;

import com.automationanywhere.cognitive.validation.JsonValidationException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import java.util.List;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * Adds validation report to the standard error fields.
 */
public class JsonValidationErrorDTO extends DefaultErrorDTO {

  public final List<ProcessingMessage> report;

  public JsonValidationErrorDTO(final JsonValidationException ex, final ServletWebRequest request) {
    super("Json validation failed", ex.httpStatus.value(), ex, request);

    this.report = ex.report;
  }
}
