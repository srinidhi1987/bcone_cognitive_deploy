package com.automationanywhere.cognitive.restclient.resttemplate;

import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.protocol.HttpContext;

public class DefaultConnectionKeepAliveStrategy implements ConnectionKeepAliveStrategy {

  private final long duration;

  public DefaultConnectionKeepAliveStrategy(final long duration) {
    this.duration = duration;
  }

  @Override
  public long getKeepAliveDuration(final HttpResponse httpResponse, final HttpContext httpContext) {
    return this.duration;
  }
}
