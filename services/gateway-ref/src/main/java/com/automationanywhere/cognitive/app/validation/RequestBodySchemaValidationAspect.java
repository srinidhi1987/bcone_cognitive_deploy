package com.automationanywhere.cognitive.app.validation;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.io.IOException;
import java.lang.reflect.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

@ControllerAdvice
public class RequestBodySchemaValidationAspect implements RequestBodyAdvice {

  private final ClasspathSchemaValidator validator;
  private final ExceptionHandlerFactory ehf;

  @Autowired
  public RequestBodySchemaValidationAspect(
      final ClasspathSchemaValidator validator,
      final ExceptionHandlerFactory ehf
  ) {
    this.validator = validator;
    this.ehf = ehf;
  }

  @Override
  public boolean supports(
      final MethodParameter methodParameter,
      final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType
  ) {
    return true;
  }

  @Override
  public Object handleEmptyBody(
      final Object body,
      final HttpInputMessage inputMessage,
      final MethodParameter parameter,
      final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType
  ) {
    return body;
  }

  @Override
  public HttpInputMessage beforeBodyRead(
      final HttpInputMessage inputMessage,
      final MethodParameter parameter,
      final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType
  ) throws IOException {
    HttpInputMessage result = inputMessage;

    SchemaValidation annotation = parameter.getMethod().getAnnotation(SchemaValidation.class);
    if (!(annotation == null || annotation.input().isEmpty())) {
      CachingHttpInputMessage cachingHttpInputMessage = new CachingHttpInputMessage(inputMessage,
          this.ehf);

      this.validator.checkInput(cachingHttpInputMessage.getContents(), annotation.input());

      result = cachingHttpInputMessage;
    }

    return result;
  }

  @Override
  public Object afterBodyRead(
      final Object body,
      final HttpInputMessage inputMessage,
      final MethodParameter parameter,
      final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType
  ) {
    return body;
  }
}
