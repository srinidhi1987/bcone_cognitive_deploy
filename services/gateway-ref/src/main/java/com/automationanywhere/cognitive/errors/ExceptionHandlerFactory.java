package com.automationanywhere.cognitive.errors;

import java.util.concurrent.Callable;
import java.util.function.Function;
import org.springframework.stereotype.Component;

/**
 * Wraps {@link Mapper}, {@link Handler}, {@link Callable} and {@link Executable} into {@link
 * Function} objects.
 */
@Component
public class ExceptionHandlerFactory {

  public <I, O> Function<I, O> fromMapper(final Mapper<I, O> mapper) {
    return input -> ExceptionHandlerFactory.this.execute(() -> {
      return mapper.map(input);
    });
  }

  public <I, O> O map(final I input, final Mapper<I, O> mapper) {
    return this.fromMapper(mapper).apply(input);
  }

  public <I> Function<I, Void> fromHandler(final Handler<I> handler) {
    return input -> ExceptionHandlerFactory.this.execute(() -> {
      handler.handle(input);
      return null;
    });
  }

  public <I> void handle(final I input, final Handler<I> handler) {
    this.fromHandler(handler).apply(input);
  }

  public <I> Function<Object, Void> fromExecutable(final Executable executable) {
    return input -> ExceptionHandlerFactory.this.execute(() -> {
      executable.execute();
      return null;
    });
  }

  public void execute(final Executable executable) {
    this.fromExecutable(executable).apply(null);
  }

  public <O> Function<Object, O> fromCallable(final Callable<O> callable) {
    return input -> ExceptionHandlerFactory.this.execute(callable);
  }

  public <O> O call(final Callable<O> callable) {
    return this.fromCallable(callable).apply(null);
  }

  private <I, O, E extends Exception> O execute(final Callable<O> callable) throws E {
    O result = null;
    try {
      result = callable.call();
    } catch (final RuntimeException ex) {
      throw ex;
    } catch (final Exception th) {
      @SuppressWarnings("unchecked")
      E e = (E) th;
      throw e;
    }
    return result;
  }
}
