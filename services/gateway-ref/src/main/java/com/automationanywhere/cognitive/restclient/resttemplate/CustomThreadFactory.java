package com.automationanywhere.cognitive.restclient.resttemplate;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A thread factory that delegates the thread creation to the default thread factory.
 * But after a thread creation allows customizing the new thread
 * by setting its name using an index and a configurable prefix.
 */
public class CustomThreadFactory implements ThreadFactory {

  private final ThreadFactory tf = Executors.defaultThreadFactory();
  private final AtomicInteger index = new AtomicInteger(0);

  private final String prefix;

  public CustomThreadFactory(final String prefix) {
    this.prefix = prefix;
  }

  @Override
  public Thread newThread(final Runnable r) {
    Thread thread = this.tf.newThread(r);
    thread.setName(this.prefix + this.index.incrementAndGet());
    return thread;
  }
}
