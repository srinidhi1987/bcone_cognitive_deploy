package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.io.CharArrayWriter;
import java.util.Date;
import java.util.Optional;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;

/**
 * Decorates the standard remote addr element by adding the value of the x-forwarded-for header if
 * present.
 */
public class RemoteAddrElementDecorator implements CustomAccessLogValve.CustomAccessLogElement {

  private final CustomAccessLogValve.CustomAccessLogElement accessLogElement;

  public RemoteAddrElementDecorator(
      final CustomAccessLogValve.CustomAccessLogElement accessLogElement) {
    this.accessLogElement = accessLogElement;
  }

  @Override
  public void addElement(final CharArrayWriter buf, final Date date, final Request request,
      final Response response, final long time) {
    this.accessLogElement.addElement(buf, date, request, response, time);

    String header = Optional
        .ofNullable(request.getHeader(CognitivePlatformHeaders.CLIENT_IP.headerName)).orElse("")
        .trim();
    if (!header.isEmpty()) {
      buf.append("/");
      buf.append(header);
    }
  }
}
