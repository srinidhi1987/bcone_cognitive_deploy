package com.automationanywhere.cognitive.restclient.resttemplate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * Listens to HTTP response receive events and logs the responses upon receiving them.
 */
public class LoggingResponseListenableFuture implements
    ListenableFutureCallback<ClientHttpResponse> {

  private static final Logger LOGGER = LogManager.getLogger(LoggingResponseListenableFuture.class);

  private final HttpRequest request;

  public LoggingResponseListenableFuture(final HttpRequest request) {
    this.request = request;
  }

  @Override
  public void onFailure(final Throwable ex) {
    this.log(ex);
  }

  @Override
  public void onSuccess(final ClientHttpResponse result) {
    Integer rawStatusCode = null;
    try {
      rawStatusCode = result.getRawStatusCode();
    } catch (final Exception ex) {
      this.log(ex);
    }
    if (rawStatusCode != null) {
      LOGGER.trace("Response {} {}: {}", this.request.getMethod(), this.request.getURI(),
          rawStatusCode);
    }
  }

  void log(final Throwable ex) {
    LOGGER.error("Response {} {}", this.request.getMethod(), this.request.getURI(), ex);
  }
}
