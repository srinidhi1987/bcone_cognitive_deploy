package com.automationanywhere.cognitive.restclient.resttemplate;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import java.util.concurrent.ThreadFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.client.HttpAsyncClient;
import org.springframework.http.client.AsyncClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;

/**
 * Creates {@link HttpComponentsAsyncClientHttpRequestFactory} which uses Apache HTTP component
 * facilities to execute HTTP requests.
 */
public class ApacheAsyncHTTPClientRequestFactoryCreator {

  private final ThreadFactory threadFactory;
  private final ApplicationConfiguration cfg;

  public ApacheAsyncHTTPClientRequestFactoryCreator(
      final ThreadFactory threadFactory,
      final ApplicationConfiguration cfg
  ) {
    this.threadFactory = threadFactory;
    this.cfg = cfg;
  }

  public AsyncClientHttpRequestFactory create() {
    HttpAsyncClient httpAsyncClient = HttpAsyncClients.custom()
        .setMaxConnTotal((int) this.cfg.restTemplateMaxConnCount())
        .setKeepAliveStrategy(
            new DefaultConnectionKeepAliveStrategy(this.cfg.restTemplateKeepAliveTimeout()))
        .setThreadFactory(this.threadFactory)
        .build();

    HttpClient httpClient = HttpClients.custom()
        .setMaxConnTotal((int) this.cfg.restTemplateMaxConnCount())
        .setKeepAliveStrategy(
            new DefaultConnectionKeepAliveStrategy(this.cfg.restTemplateKeepAliveTimeout()))
        .build();

    HttpComponentsAsyncClientHttpRequestFactory factory
        = new LogContextPropagatingHttpComponentsAsyncClientHttpRequestFactory(
        httpClient, httpAsyncClient
    );
    factory.setConnectTimeout((int) this.cfg.restTemplateConnectTimeout());
    factory.setReadTimeout((int) this.cfg.restTemplateReadTimeout());

    return factory;
  }

}
