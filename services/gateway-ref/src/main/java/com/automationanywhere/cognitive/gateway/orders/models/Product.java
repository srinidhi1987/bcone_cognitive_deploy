package com.automationanywhere.cognitive.gateway.orders.models;

public class Product {

  public final String name;
  public final long quantity;

  public Product(final String name, final long quantity) {
    this.name = name;
    this.quantity = quantity;
  }
}
