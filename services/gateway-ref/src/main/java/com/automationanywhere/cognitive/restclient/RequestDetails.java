package com.automationanywhere.cognitive.restclient;

import com.automationanywhere.cognitive.dataresource.DataContainer;
import com.automationanywhere.cognitive.dataresource.DataResource;
import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import com.automationanywhere.cognitive.id.integer.LongIdTransformer;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;

/**
 * A holder object that contains details of a request.
 */
public class RequestDetails {

  private static final String FILE_PART_NAME = "file";

  public final String url;
  public final Object body;
  public final HttpMethod method;
  public final HttpHeaders headers;

  public RequestDetails(
      final String url, final Object body, final HttpMethod method, final HttpHeaders headers
  ) {
    this.url = url;
    this.body = body;
    this.method = method;
    this.headers = headers;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      RequestDetails that = (RequestDetails) o;

      result = Objects.equals(url, that.url)
          && Objects.equals(body, that.body)
          && Objects.equals(method, that.method)
          && Objects.equals(headers, that.headers);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(url)
        ^ Objects.hashCode(body)
        ^ Objects.hashCode(method)
        ^ Objects.hashCode(headers);
  }

  public enum Types {
    JSON(MediaType.APPLICATION_JSON_UTF8),
    TEXT(MediaType.TEXT_PLAIN),
    ATTACHMENT(MediaType.MULTIPART_FORM_DATA);

    public final MediaType mediaType;

    Types(final MediaType mediaType) {
      this.mediaType = mediaType;
    }
  }

  /**
   * Builds valid URLs from URL parts: prefix + request with parameters.
   * Request may be formatted according to the {@link MessageFormat} rules.
   * In this case the placeholders will be replaced with the provided parameters.
   * Prefix is required, request and params parts are optional.
   */
  public static class Builder {

    private static final List<String> RESTRICTED_HEADERS = Arrays.asList(
        HttpHeaders.ACCEPT.toLowerCase(),
        HttpHeaders.CONTENT_TYPE.toLowerCase()
    );

    private final String prefix;
    private final String request;
    private final List<Object> params;
    private final HttpMethod method;
    private final Object body;
    private final Map<String, Set<String>> headers;

    private final IdTransformer idTransformer = new CompositeIdTransformer(
        new UUIDIdTransformer(), new LongIdTransformer(), new StringIdTransformer()
    );

    private Builder(
        final String prefix,
        final String request,
        final List<Object> params,
        final HttpMethod method,
        final Object body,
        final Map<String, Set<String>> headers
    ) {
      this.prefix = prefix;
      this.request = request;
      this.params = params == null ? Collections.emptyList() : Collections.unmodifiableList(params);
      this.method = method;
      this.body = body;
      this.headers =
          headers == null ? Collections.emptyMap() : Collections.unmodifiableMap(headers);
    }

    public static Builder get(final String prefix) {
      return new Builder(prefix, null, null, HttpMethod.GET, null, null);
    }

    public static Builder post(final String prefix) {
      return new Builder(prefix, null, null, HttpMethod.POST, null, null);
    }

    public static Builder put(final String prefix) {
      return new Builder(prefix, null, null, HttpMethod.PUT, null, null);
    }

    public static Builder patch(final String prefix) {
      return new Builder(prefix, null, null, HttpMethod.PATCH, null, null);
    }

    public static Builder delete(final String prefix) {
      return new Builder(prefix, null, null, HttpMethod.DELETE, null, null);
    }

    public Builder request(final String request) {
      return new Builder(prefix, request, params, method, body, headers);
    }

    /**
     * Sets a header value,
     * if the header already has values they will be replaced
     * with the given one.
     *
     * @param header - name of the header
     * @param value - a header value
     * @return a new instance of the modified builder
     */
    public Builder setHeader(final String header, final Object value) {
      String normalizedHeader = normalizeHeader(header);

      Map<String, Set<String>> headers = new HashMap<>(this.headers);
      headers.put(normalizedHeader, new HashSet<>(Collections.singletonList(toString(value))));

      return new Builder(
          prefix, request, params, method, body, headers
      );
    }

    /**
     * Adds a header value to the list of header values,
     * or creates a new list and adds the value to it
     * if the header hasn't exited yet.
     *
     * @param header - name of the header
     * @param value - a header value
     * @return a new instance of the modified builder
     */
    public Builder addHeader(final String header, final Object value) {
      String normalizedHeader = normalizeHeader(header);

      Map<String, Set<String>> headers = new HashMap<>(this.headers);
      Set<String> values = this.headers.getOrDefault(normalizedHeader, new HashSet<>());
      values.add(toString(value));

      return new Builder(
          prefix, request, params, method, body, headers
      );
    }

    String normalizeHeader(final String header) {
      if (header == null) {
        throw new IllegalArgumentException("Header name cannot be null");
      }
      String trimmedHeader = header.trim().toLowerCase();
      if (trimmedHeader.isEmpty()) {
        throw new IllegalArgumentException("Header name cannot be blank");
      }
      if (RESTRICTED_HEADERS.stream().anyMatch(h -> h.equals(trimmedHeader))) {
        throw new IllegalArgumentException(
            "Please use appropriate method to set the " + header + " header"
        );
      }
      return trimmedHeader;
    }

    public Builder accept(final Types type) {
      return accept(type, null);
    }

    public Builder accept(final Types type, final Integer version) {
      HashMap<String, Set<String>> headers = new HashMap<>(this.headers);

      headers.put(
          HttpHeaders.ACCEPT,
          createMediaTypeHeader(type.mediaType, version)
      );

      return new Builder(
          prefix, request, params, method, body, headers
      );
    }

    Set<String> createMediaTypeHeader(final MediaType mediaType, final Integer version) {
      String result = version == null
          ? mediaType.getType() + "/" + mediaType.getSubtype()
          : MessageFormat.format(
              "{0}/vnd.cognitive.v{2,number,0}+{1}",
              mediaType.getType(), mediaType.getSubtype(), version
          );

      Charset charset = mediaType.getCharset();
      if (charset != null) {
        result += ";charset=" + charset.name();
      }

      return new HashSet<>(Collections.singletonList(result));
    }

    public Builder content(final DataContainer data) {
      return content(Collections.singletonList(data));
    }

    public Builder content(final List<DataContainer> data) {
      return content(data, Types.ATTACHMENT, null);
    }

    public Builder content(final Object body, final Types type) {
      return content(body, type, null);
    }

    public Builder content(final Object object, final Types type, final Integer version) {
      Object body = object;

      if (type == Types.ATTACHMENT) {
        if (version != null) {
          throw new IllegalArgumentException("Multipart form data doesn't support versioning");
        }

        @SuppressWarnings("unchecked")
        List<DataContainer> dataContainers = (List<DataContainer>) object;
        body = createPartBody(dataContainers);
      }

      HashMap<String, Set<String>> headers = new HashMap<>(this.headers);

      headers.put(
          HttpHeaders.CONTENT_TYPE,
          createMediaTypeHeader(type.mediaType, version)
      );

      return new Builder(
          prefix, request, params, method, body, headers
      );
    }

    private Object createPartBody(final List<DataContainer> dataContainers) {
      LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

      for (final DataContainer dataContainer : dataContainers) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(dataContainer.mimeType));

        map.add(FILE_PART_NAME, new HttpEntity<InputStreamSource>(
            new DataResource(dataContainer), headers
        ));
      }

      return map;
    }

    /**
     * Adds a parameter to the list of parameters.
     *
     * @param param - param to add.
     *
     * Params will be converted to strings using {@link Object#toString()}.
     * Id implementations will be converted to strings using {@link IdTransformer#toString(Id)}
     *
     * Placeholders (elements like {index}, for example {0} and {1} are placeholders in
     * http://www.{0}.com/path/{1}) in the URL except the query part of the URL
     * will be replaced with the obtained string value;
     *
     * RequestDetails.Builder
     *     .get("http://www.{0}.com")
     *     .request("/path/{1}")
     *     .param("example")
     *     .param(new UUIDId(UUID.fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb")))
     *     .build()
     *     .url
     *
     * gives http://www.example.com/path/2c076351-774b-49e4-8e55-ce0ab4251ceb
     *
     * nulls and missing params will keep a placeholder unmodified
     *
     * RequestDetails.Builder
     *     .get("http://www.{0}.com")
     *     .request("/path/{1}")
     *     .param("example")
     *     .param(null)
     *     .build()
     *     .url
     *
     * gives http://www.example.com/path/{1}
     *
     * RequestDetails.Builder
     *     .get("http://www.{0}.com")
     *     .request("/path/{1}")
     *     .param("example")
     *     .build()
     *     .url
     *
     * gives http://www.example.com/path/{1}
     *
     * In the query part of a URL placeholders are also replaced with strings according to the
     * same rules but null params and missing params will make a query param be removed
     * from a query string.
     *
     * RequestDetails.Builder
     *     .get("http://www.{0}.com")
     *     .request("?path={1}")
     *     .param("example")
     *     .param(new UUIDId(UUID.fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb")))
     *     .build()
     *     .url
     *
     * gives http://www.example.com?path=2c076351-774b-49e4-8e55-ce0ab4251ceb
     *
     * RequestDetails.Builder
     *     .get("http://www.{0}.com")
     *     .request("?path={1}")
     *     .param("example")
     *     .param(null)
     *     .build()
     *     .url
     *
     * gives http://www.example.com
     *
     * RequestDetails.Builder
     *     .get("http://www.{0}.com")
     *     .request("?path={1}")
     *     .param("example")
     *     .build()
     *     .url
     *
     * gives http://www.example.com
     *
     * @return the same builder instance.
     */
    public Builder param(final Object param) {
      List<Object> params = new ArrayList<>(this.params);
      params.add(param);

      return new Builder(
          prefix, request, params, method, body, headers
      );
    }

    /**
     * Builds a url from its parts.
     *
     * @return a url.
     */
    public RequestDetails build() {
      String spec = prefix;

      if (request != null) {
        spec += request;

        List<String> params = this.params.stream().map(this::toString).collect(Collectors.toList());
        spec = MessageFormat.format(spec, params.toArray(new Object[params.size()]));
        spec = normalizeQuery(spec);
      }

      return new RequestDetails(spec, body, method, createHttpHeaders());
    }

    private HttpHeaders createHttpHeaders() {
      HttpHeaders headers = new HttpHeaders();
      for (final Entry<String, Set<String>> e : this.headers.entrySet()) {
        headers.put(e.getKey(), new ArrayList<>(e.getValue()));
      }
      return headers;
    }

    private String normalizeQuery(final String spec) {
      String result = spec;

      int i = result.indexOf("?");
      if (i >= 0) {
        StringBuilder query = new StringBuilder();
        String[] parts = result.substring(i + 1).split("&", -1);
        for (final String part : parts) {
          String p = "";
          if (!part.trim().isEmpty()) {
            String[] keyValue = part.split("=", 2);
            if (keyValue.length > 1) {
              if (!(keyValue[1].trim().isEmpty() || keyValue[1].matches(".*\\{.*}.*"))) {
                p = part;
              }
            }
          }
          if (!p.isEmpty()) {
            if (query.length() > 0) {
              query.append("&");
            }
            query.append(p);
          }
        }

        result = result.substring(0, i);
        if (query.length() > 0) {
          result+= "?" + query;
        }
      }

      return result;
    }

    private String toString(final Object obj) {
      String result = "";
      if (obj instanceof Id) {
        result = idTransformer.toString((Id) obj);
      } else if (obj != null) {
        result = obj.toString();
      }
      return result;
    }
  }
}
