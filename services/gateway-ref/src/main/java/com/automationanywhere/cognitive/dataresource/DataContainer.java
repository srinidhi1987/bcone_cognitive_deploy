package com.automationanywhere.cognitive.dataresource;

import java.io.InputStream;
import java.util.Objects;

/**
 * Holds data and provides information about the data it holds.
 */
public class DataContainer {

  public final InputStream data;
  public final String mimeType;
  public final String name;
  public final Long contentLength;

  public DataContainer(
      final InputStream data,
      final String mimeType,
      final String name,
      final Long contentLength
  ) {
    this.data = data;
    this.mimeType = mimeType;
    this.name = name;
    this.contentLength = contentLength;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      DataContainer that = (DataContainer) o;

      result = Objects.equals(data, that.data)
          && Objects.equals(mimeType, that.mimeType)
          && Objects.equals(name, that.name)
          && Objects.equals(contentLength, that.contentLength);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(data)
        ^ Objects.hashCode(mimeType)
        ^ Objects.hashCode(name)
        ^ Objects.hashCode(contentLength);
  }
}
