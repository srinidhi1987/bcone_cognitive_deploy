package com.automationanywhere.cognitive.id;

import com.fasterxml.jackson.core.TreeNode;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Converts objects to ids and vice versa.
 * Supports many implementations of {@link Id}
 */
public class CompositeIdTransformer implements IdTransformer {

  private final List<IdTransformer> transformers;

  public CompositeIdTransformer(
      final IdTransformer... transformers
  ) {
    this.transformers = Arrays.asList(transformers);
  }

  @Override
  public boolean supports(final Class<? extends Id> type) {
    if (type == null) {
      throw new NullPointerException();
    }
    return this.findIdTransformer(type) != null;
  }

  private <T> T transform(final Function<IdTransformer, T> func) {
    if (this.transformers.isEmpty()) {
      throw new UnsupportedOperationException("No transformers found");
    }

    Optional<T> result = null;
    for (final IdTransformer transformer : this.transformers) {
      try {
        result = Optional.ofNullable(func.apply(transformer));
        break;
      } catch (final UnsupportedOperationException ex) {
        result = null;
      }
    }
    if (result == null) {
      throw new UnsupportedOperationException("No suitable transformers found");
    }

    return result.orElse(null);
  }

  @Override
  public TreeNode toNode(final Id id) {
    return this.transform(transformer -> transformer.toNode(id));
  }

  @Override
  public Id fromNode(final TreeNode node) {
    return this.transform(transformer -> transformer.fromNode(node));
  }

  @Override
  public String toString(final Id id) {
    return this.transform(transformer -> transformer.toString(id));
  }

  @Override
  public Id fromString(final String id) {
    return this.transform(transformer -> transformer.fromString(id));
  }

  /**
   * Converts an id of one type to the id of another type.
   *
   * @param id - id to convert
   * @param type - type of the new id.
   * @return new instance of the id of the specified type.
   * @throws UnsupportedOperationException - if an implementation doesn't support this type of
   * conversion.
   */
  public Id toId(
      final Id id, final Class<? extends Id> type
  ) throws UnsupportedOperationException {
    IdTransformer idTransformer = this.findIdTransformer(type);

    if (idTransformer == null) {
      throw new UnsupportedOperationException();
    }

    TreeNode node = this.toNode(id);
    return idTransformer.fromNode(node);
  }

  private IdTransformer findIdTransformer(final Class<? extends Id> type) {
    return this.transformers.stream().filter(
        t -> t.supports(type)
    ).findAny().orElse(null);
  }
}
