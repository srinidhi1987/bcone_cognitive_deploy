package com.automationanywhere.cognitive.gateway.orders.microservices.order;

import com.automationanywhere.cognitive.gateway.orders.models.Order;
import com.automationanywhere.cognitive.gateway.orders.models.OrderV2;
import com.automationanywhere.cognitive.gateway.orders.models.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class OrderListMapper {

  public List<OrderV2> mapCreationTrackingOrder(final List<OrderDataV2> list) {
    List<OrderV2> orders = new ArrayList<>();
    for (final OrderDataV2 order : list) {
      List<Product> products = new ArrayList<>();
      for (final Map<String, Long> m : order.products) {
        Map.Entry<String, Long> e = m.entrySet().iterator().next();
        products.add(new Product(e.getKey(), e.getValue()));
      }
      orders.add(new OrderV2(
          order.orderId, order.createdAt, order.customerId, products
      ));
    }
    return orders;
  }

  @Deprecated
  public List<Order> mapOrder(final List<OrderData> list) {
    List<Order> orders = new ArrayList<>();
    for (final OrderData orderData : list) {
      List<Product> products = new ArrayList<>();
      for (final Map<String, Long> m : orderData.products) {
        Map.Entry<String, Long> e = m.entrySet().iterator().next();
        products.add(new Product(e.getKey(), e.getValue()));
      }
      orders.add(new Order(
          orderData.orderId, orderData.customerId, products
      ));
    }
    return orders;
  }
}
