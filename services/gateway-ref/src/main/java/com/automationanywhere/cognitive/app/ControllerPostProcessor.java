package com.automationanywhere.cognitive.app;

import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.lang.reflect.Method;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
public class ControllerPostProcessor implements BeanPostProcessor {

  private final ClasspathSchemaValidator jsonValidator;

  @Autowired
  public ControllerPostProcessor(final ClasspathSchemaValidator jsonValidator) {
    this.jsonValidator = jsonValidator;
  }

  @Override
  public Object postProcessBeforeInitialization(final Object bean, final String beanName)
      throws BeansException {
    if (bean.getClass().getAnnotation(RestController.class) != null) {
      for (final Method m : bean.getClass().getMethods()) {
        SchemaValidation annotation = m.getAnnotation(SchemaValidation.class);
        if (annotation != null) {
          if (annotation.input().isEmpty() && annotation.output().isEmpty()) {
            throw new IllegalStateException("Either input or output is required: " + m);
          }
          if (!annotation.input().isEmpty()) {
            this.jsonValidator.checkKey(annotation.input());
          }
          if (!annotation.output().isEmpty()) {
            this.jsonValidator.checkKey(annotation.output());
          }
        }
      }
    }
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(final Object bean, final String beanName)
      throws BeansException {
    return bean;
  }
}
