package com.automationanywhere.cognitive.id.uuid;

import com.automationanywhere.cognitive.id.Id;
import java.util.Objects;
import java.util.UUID;

public class UUIDId implements Id {

  public final UUID uuid;

  public UUIDId(final UUID uuid) {
    if (uuid == null) {
      throw new IllegalArgumentException();
    }
    this.uuid = uuid;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      UUIDId uuidId = (UUIDId) o;

      result = Objects.equals(this.uuid, uuidId.uuid);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.uuid) ^ 32;
  }
}
