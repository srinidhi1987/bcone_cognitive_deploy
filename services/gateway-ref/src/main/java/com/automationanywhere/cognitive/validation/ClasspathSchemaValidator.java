package com.automationanywhere.cognitive.validation;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.http.HttpStatus;

/**
 * Validates JSON objects against loaded schemas. Allows configuring where the schemes will be
 * loaded from. This implementation assumes the input object is a string respresentation of a JSON
 * before deserialization. The output object must be a POJO before serialization.
 */
public class ClasspathSchemaValidator implements SchemaValidator {

  private final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

  private final String prefix;
  private final String postfix;
  private final ObjectMapper om;
  private final ExceptionHandlerFactory ehf;
  private final Map<String, JsonNode> keyToSchema = new HashMap<>();

  public ClasspathSchemaValidator(
      final String prefix, final String postfix, final ObjectMapper om,
      final ExceptionHandlerFactory ehf
  ) {
    this.prefix = prefix;
    this.postfix = postfix;
    this.om = om;
    this.ehf = ehf;
  }

  @PostConstruct
  public void loadJsonSchemas() {
    File schemasFolder = new File(
        this.getClass().getClassLoader().getResource(this.prefix).getFile()
    );
    this.keyToSchema.putAll(
        Arrays.stream(schemasFolder.listFiles((dir, name) -> name.endsWith(this.postfix)))
            .map(file -> this.ehf.call(() -> {
              SimpleEntry<String, JsonNode> entry = null;
              try {
                JsonNode schema = JsonLoader.fromFile(file);
                String name = file.getName();
                String key = name.substring(0, name.length() - this.postfix.length());
                entry = new SimpleEntry<>(key, schema);
              } catch (final IOException ex) {
                throw new IOException("Failed to load schema from " + file, ex);
              }
              return entry;
            }))
            .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue))
    );
  }

  @Override
  public boolean checkKey(final String key) {
    if (this.keyToSchema.get(key) == null) {
      throw new IllegalStateException("Key " + key + " is unknown");
    }
    return true;
  }

  @Override
  public boolean checkInput(final Object json, final String key) {
    return new JsonSchemaValidator<Object>() {
      @Override
      protected JsonNode load() throws Exception {
        return JsonLoader.fromString((String) json);
      }
    }.check(key, HttpStatus.BAD_REQUEST);
  }

  @Override
  public boolean checkOutput(final Object object, final String key) throws ValidationException {
    return new JsonSchemaValidator<Object>() {
      @Override
      protected JsonNode load() throws Exception {
        return ClasspathSchemaValidator.this.om.valueToTree(object);
      }
    }.check(key, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  abstract class JsonSchemaValidator<T> {

    protected abstract JsonNode load() throws Exception;

    public boolean check(final String key, final HttpStatus httpStatus) {
      try {
        this.checkJson(this.load(), key, httpStatus);
      } catch (final RuntimeException ex) {
        throw ex;
      } catch (final Exception ex) {
        throw new ValidationException("Failed to check an object against " + key, ex);
      }
      return true;
    }

    private void checkJson(
        final JsonNode json, final String key, final HttpStatus httpStatus
    ) throws ProcessingException {
      JsonNode schema = ClasspathSchemaValidator.this.keyToSchema.get(key);
      JsonSchema jsonSchema = ClasspathSchemaValidator.this.factory.getJsonSchema(schema);

      ProcessingReport processingReport = jsonSchema.validate(json);
      if (!processingReport.isSuccess()) {
        throw new JsonValidationException(processingReport, httpStatus);
      }
    }
  }
}