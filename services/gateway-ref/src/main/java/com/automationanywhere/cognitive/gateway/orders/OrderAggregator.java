package com.automationanywhere.cognitive.gateway.orders;

import com.automationanywhere.cognitive.gateway.orders.models.Customer;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccount;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccountOrder;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerOrder;
import com.automationanywhere.cognitive.gateway.orders.models.Order;
import com.automationanywhere.cognitive.gateway.orders.models.OrderV2;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OrderAggregator {

  public List<CustomerAccountOrder> aggregateCustomerAccountOrders(final CustomerAccount customer,
      final List<OrderV2> orders) {
    List<CustomerAccountOrder> customerOrders = new ArrayList<>();
    for (final OrderV2 order : orders) {
      customerOrders.add(new CustomerAccountOrder(
          order.id, order.createdAt, customer, order.products
      ));
    }
    return customerOrders;
  }

  @Deprecated
  public List<CustomerOrder> aggregateCustomerOrders(final Customer customer,
      final List<Order> orders) {
    List<CustomerOrder> customerOrders = new ArrayList<>();
    for (final Order order : orders) {
      customerOrders.add(new CustomerOrder(
          order.id, customer, order.products
      ));
    }
    return customerOrders;
  }
}
