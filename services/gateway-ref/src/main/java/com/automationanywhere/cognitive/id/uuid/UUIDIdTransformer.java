package com.automationanywhere.cognitive.id.uuid;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class UUIDIdTransformer implements IdTransformer {

  @Override
  public UUIDId fromNode(final TreeNode node) {
    UUIDId result = null;
    if (node != null) {
      if (!(node instanceof NullNode)) {
        if (!(node instanceof TextNode)) {
          throw new UnsupportedOperationException(node.getClass() + " is not supported");
        }
        result = this.fromString(((TextNode) node).asText());
      }
    }
    return result;
  }

  @Override
  public TreeNode toNode(final Id id) {
    return id == null ? NullNode.getInstance() : new TextNode(this.toString(id));
  }

  @Override
  public UUIDId fromString(final String id) {
    UUIDId result = null;

    if (id != null) {
      try {
        result = new UUIDId(UUID.fromString(id));
      } catch (final IllegalArgumentException ex) {
        throw new UnsupportedOperationException(id + " is not supported");
      }
    }

    return result;
  }

  @Override
  public String toString(final Id id) {
    if (id != null && !this.supports(id.getClass())) {
      throw new UnsupportedOperationException(id.getClass() + " is not supported");
    }

    return id == null ? null : ((UUIDId) id).uuid.toString();
  }

  @Override
  public boolean supports(final Class<? extends Id> type) {
    return type.equals(UUIDId.class);
  }
}
