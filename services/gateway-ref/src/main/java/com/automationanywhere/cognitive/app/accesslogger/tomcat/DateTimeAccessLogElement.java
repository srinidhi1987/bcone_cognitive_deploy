package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Formats a date using a customizable format.
 */
public class DateTimeAccessLogElement implements CustomAccessLogValve.CustomAccessLogElement {

  private static final Logger LOGGER = LogManager.getLogger(DateTimeAccessLogElement.class);

  private static final Locale DEFAULT_LOCALE = Locale.US;
  private static final TimeZone DEFAULT_TZ = TimeZone.getTimeZone("UTC");
  private static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

  private final SimpleDateFormat formatter;

  public DateTimeAccessLogElement() {
    this(DEFAULT_TZ, DEFAULT_LOCALE, DEFAULT_FORMAT);
  }

  public DateTimeAccessLogElement(final TimeZone tz, final Locale locale, final String format) {
    this.formatter = new SimpleDateFormat(format, locale);
    this.formatter.setTimeZone(tz);
    this.formatter.setCalendar(Calendar.getInstance(tz, locale));
  }

  public void addElement(final CharArrayWriter buf, final Date date, final Request request,
      final Response response, final long time) {
    try {
      buf.write(this.formatter.format(date));
    } catch (final IOException ex) {
      LOGGER.error("Failed to format the date", ex);
    }
  }
}
