package com.automationanywhere.cognitive.gateway.orders;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.orders.microservices.customer.CustomerMicroservice;
import com.automationanywhere.cognitive.gateway.orders.microservices.order.OrderMicroservice;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccount;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerAccountOrder;
import com.automationanywhere.cognitive.gateway.orders.models.CustomerOrder;
import com.automationanywhere.cognitive.gateway.orders.models.OrderV2;
import com.automationanywhere.cognitive.id.Id;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

  private final OrderMicroservice orderMicroservice;
  private final CustomerMicroservice customerMicroservice;
  private final OrderAggregator orderAggregator;
  private final ExceptionHandlerFactory exceptionHandlerFactory;

  public OrderService(
      final OrderMicroservice orderMicroservice,
      final CustomerMicroservice customerMicroservice,
      final OrderAggregator orderAggregator,
      final ExceptionHandlerFactory exceptionHandlerFactory
  ) {
    this.orderMicroservice = orderMicroservice;
    this.customerMicroservice = customerMicroservice;
    this.orderAggregator = orderAggregator;
    this.exceptionHandlerFactory = exceptionHandlerFactory;
  }

  @Async
  public CompletableFuture<List<CustomerAccountOrder>> getCustomerAccountOrders(
      final Id customerId) {
    CompletableFuture<List<OrderV2>> customerOrdersFuture = this.orderMicroservice
        .getOrdersV2(customerId);
    CompletableFuture<CustomerAccount> customerFuture = this.customerMicroservice
        .getCustomerAccount(customerId);

    return CompletableFuture.allOf(
        customerOrdersFuture,
        customerFuture
    ).thenApply(this.exceptionHandlerFactory.fromCallable(() -> {
      List<OrderV2> orders = customerOrdersFuture.join();
      CustomerAccount customer = customerFuture.join();

      return this.orderAggregator.aggregateCustomerAccountOrders(customer, orders);
    }));
  }

  @Async
  @Deprecated
  public CompletableFuture<List<CustomerOrder>> getCustomerOrders(final Id customerId) {
    return this.customerMicroservice.getCustomer(customerId).thenCompose(customer -> {
      return this.orderMicroservice.getOrders(customerId)
          .thenApply(this.exceptionHandlerFactory.fromMapper(orders -> {
            return this.orderAggregator.aggregateCustomerOrders(customer, orders);
          }));
    });
  }
}
