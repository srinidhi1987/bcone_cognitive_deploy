package com.automationanywhere.cognitive.app.id.converters;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.IdTransformer;
import org.springframework.core.convert.converter.Converter;

public abstract class BaseIdConverter<T extends Id> implements Converter<String, T> {

  private final IdTransformer idTransformer;

  public BaseIdConverter(final IdTransformer idTransformer) {
    this.idTransformer = idTransformer;
  }

  @Override
  public T convert(final String source) {
    @SuppressWarnings("unchecked")
    T result = (T) this.idTransformer.fromString(source);
    return result;
  }
}
