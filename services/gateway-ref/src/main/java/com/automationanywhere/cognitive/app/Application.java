package com.automationanywhere.cognitive.app;

import com.automationanywhere.cognitive.logging.LogContextKeys;
import com.automationanywhere.cognitive.logging.LogDetailsProvider;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.boot.SpringApplication;

/**
 * Main application class.
 */
public final class Application {

  /**
   * Private constructor restricts instantiation.
   */
  private Application() {
  }

  /**
   * Entry point.
   *
   * @param args - app args
   */
  public static void main(final String[] args) {
    System.setProperty("log4j2.isThreadContextMapInheritable", "true");
    System.setProperty("server.tomcat.accesslog.enabled", "true");

    LogDetailsProvider p = new LogDetailsProvider(new ApplicationConfiguration(null));

    ThreadContext.put(LogContextKeys.APPLICATION_NAME.key, p.getApplicationName());
    ThreadContext.put(LogContextKeys.APPLICATION_VERSION.key, p.getApplicationVersion());
    ThreadContext.put(LogContextKeys.HOST_NAME.key, p.getHostName());

    SpringApplication.run(SpringBootConfiguration.class, args);
  }
}
