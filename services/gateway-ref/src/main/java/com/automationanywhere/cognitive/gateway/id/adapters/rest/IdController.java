package com.automationanywhere.cognitive.gateway.id.adapters.rest;

import static com.automationanywhere.cognitive.restclient.RequestDetails.Builder.get;
import static com.automationanywhere.cognitive.restclient.RequestDetails.Builder.post;

import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Demonstrates the following cases:
 *
 * GET requests:
 *
 * 1) An input parameter type is unknown and
 * A) may be transparently used
 * b) or may be converted to a concrete type due to requirements of a microservice
 *
 * POST requests:
 *
 * 2) Gateway internally uses abstract ids
 * A) and sends ids without modifications as they are
 * b) converts ids to concrete types before sending them to a microservice
 * due to requirements of a microservice.
 *
 */
@RestController
@RequestMapping("/api")
public class IdController {
  private final CompositeIdTransformer idTransformer;

  @Autowired
  public IdController(final CompositeIdTransformer idTransformer) {
    this.idTransformer = idTransformer;
  }

  /*
     * Case 1a:
     * The input parameter type is unknown and is used transparently.
     */
  @RequestMapping(
      path = "/ping/{id}",
      method = RequestMethod.GET,
      produces = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      }
  )
  public CompletableFuture<Ping> getPong(@PathVariable("id") final Id id) {
    return CompletableFuture.completedFuture(new Ping(id));
  }

  /*
   * Case 1b uuid:
   * The input parameter type is unknown
   * and is converted to a UUID due to requirements of a microservice.
   * If the sender provides an id incompatible with the UUID Id an error will be returned.
   */
  @RequestMapping(
      path = "/path/{id}/uuid",
      method = RequestMethod.GET,
      produces = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      }
  )
  public CompletableFuture<URL> getURLWithUUID(@PathVariable("id") final Id id) {
    return CompletableFuture.supplyAsync(() -> {
      Id uuidId = this.idTransformer.toId(id, UUIDId.class);
      return new URL(get("http://www.example.com").request("/{0}").param(uuidId).build().url);
    });
  }

  /*
   * Case 1b string:
   * The input parameter type is unknown
   * and is converted to a string due to requirements of a microservice.
   */
  @RequestMapping(
      path = "/path/{id}/string",
      method = RequestMethod.GET,
      produces = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      }
  )
  public CompletableFuture<URL> getURLWithString(@PathVariable("id") final Id id) {
    return CompletableFuture.supplyAsync(() -> {
      Id stringId = this.idTransformer.toId(id, StringId.class);
      return new URL(get("http://www.example.com").request("/{0}").param(stringId).build().url);
    });
  }

  /*
   * Case 2a:
   * The id is abstract and is used transparently.
   */
  @RequestMapping(
      path = "/ping",
      method = RequestMethod.POST,
      consumes = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      },
      produces = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      }
  )
  public CompletableFuture<Ping> postPing(@RequestBody final Ping ping) {
    return CompletableFuture.completedFuture(ping);
  }

  /*
   * Case 2b uuid:
   * The id is abstract
   * and is converted to a UUID due to requirements of a microservice.
   * If the sender provides an id incompatible with the UUID Id an error will be returned.
   */
  @RequestMapping(
      path = "/path/uuid",
      method = RequestMethod.POST,
      consumes = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      },
      produces = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      }
  )
  public CompletableFuture<URL> retrieveURLWithUUID(@RequestBody final Ping ping) {
    return CompletableFuture.supplyAsync(() -> {
      Id uuidId = this.idTransformer.toId(ping.id, UUIDId.class);
      return new URL(post("http://www.example.com").request("/{0}").param(uuidId).build().url);
    });
  }

  /*
   * Case 2b string:
   * The id is abstract
   * and is converted to a string due to requirements of a microservice.
   */
  @RequestMapping(
      path = "/path/string",
      method = RequestMethod.POST,
      consumes = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      },
      produces = {
          "application/json",
          "application/vnd.cognitive.v1+json"
      }
  )
  public CompletableFuture<URL> retrieveURLWithString(@RequestBody final Ping ping) {
    return CompletableFuture.supplyAsync(() -> {
      Id stringId = this.idTransformer.toId(ping.id, StringId.class);
      return new URL(post("http://www.example.com").request("/{0}").param(stringId).build().url);
    });
  }
}
