package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class CustomerAccountDTO {

  public final Id customerId;
  public final String firstName;
  public final String lastName;

  public CustomerAccountDTO(
      @JsonProperty("customerId") final Id customerId,
      @JsonProperty("firstName") final String firstName,
      @JsonProperty("lastName") final String lastName
  ) {
    this.customerId = customerId;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      CustomerAccountDTO that = (CustomerAccountDTO) o;

      result = Objects.equals(this.customerId, that.customerId)
          && Objects.equals(this.firstName, that.firstName)
          && Objects.equals(this.lastName, that.lastName);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.customerId)
        ^ Objects.hashCode(this.firstName)
        ^ Objects.hashCode(this.lastName);
  }
}
