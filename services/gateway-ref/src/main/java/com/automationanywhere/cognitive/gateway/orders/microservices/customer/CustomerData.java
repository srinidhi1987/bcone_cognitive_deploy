package com.automationanywhere.cognitive.gateway.orders.microservices.customer;

import com.automationanywhere.cognitive.id.Id;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerData {

  public final Id customerId;
  public final String name;

  public CustomerData(
      @JsonProperty("customerId") final Id customerId,
      @JsonProperty("name") final String name
  ) {
    this.customerId = customerId;
    this.name = name;
  }
}
