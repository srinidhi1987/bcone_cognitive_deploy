package com.automationanywhere.cognitive.healthcheck;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.common.healthcheck.command.RabbitMQHealthCheckCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Message queue health check.
 */
@Component
public class MessageQueueHealthCheck extends RabbitMQHealthCheckCommand {

  /**
   * Constructs an health check command for Message Queue.
   *
   * @param config the instance of ApplicationConfiguration
   */
  @Autowired
  public MessageQueueHealthCheck(final ApplicationConfiguration config) {
    super("Message Queue");
  }
}
