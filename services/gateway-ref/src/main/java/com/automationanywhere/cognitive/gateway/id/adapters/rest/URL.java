package com.automationanywhere.cognitive.gateway.id.adapters.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class URL {
  public final String url;

  public URL(@JsonProperty("url") final String url) {
    this.url = url;
  }
}
