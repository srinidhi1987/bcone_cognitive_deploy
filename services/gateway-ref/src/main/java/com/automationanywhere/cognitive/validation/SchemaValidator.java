package com.automationanywhere.cognitive.validation;

/**
 * Validates objects against schemas.
 */
public interface SchemaValidator {

  /**
   * Checks if a key of a schema is known to the validator.
   *
   * @param key - schema key to check.
   * @return always returns true.
   * @throws ValidationException - if validation fails for whatever reason.
   */
  boolean checkKey(String key) throws ValidationException;

  /**
   * Validates an input object.
   *
   * @param object - input object to validate.
   * @param key - key of the schema of the object.
   * @return always returns true
   * @throws ValidationException - if validation fails for whatever reason.
   */
  boolean checkInput(Object object, String key) throws ValidationException;

  /**
   * Validates an output object.
   *
   * @param object - output object to validate.
   * @param key - key of the schema of the object.
   * @return always returns true
   * @throws ValidationException - if validation fails for whatever reason.
   */
  boolean checkOutput(Object object, String key) throws ValidationException;
}
