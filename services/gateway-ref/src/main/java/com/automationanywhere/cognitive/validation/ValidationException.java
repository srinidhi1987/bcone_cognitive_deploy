package com.automationanywhere.cognitive.validation;

/**
 * Thrown by {@link ClasspathSchemaValidator}  if validation fails.
 */
public class ValidationException extends RuntimeException {

  public ValidationException(final String message) {
    super(message);
  }

  public ValidationException(final String message, final Exception cause) {
    super(message, cause);
  }
}
