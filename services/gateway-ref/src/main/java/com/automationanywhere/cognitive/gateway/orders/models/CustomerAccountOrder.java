package com.automationanywhere.cognitive.gateway.orders.models;

import com.automationanywhere.cognitive.id.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class CustomerAccountOrder {

  public final Id id;
  public final Date createdAt;
  public final CustomerAccount customer;
  public final List<Product> products;

  public CustomerAccountOrder(final Id id, final Date createdAt, final CustomerAccount customer,
      final List<Product> products) {
    this.id = id;
    this.createdAt = createdAt;
    this.customer = customer;
    this.products = Collections.unmodifiableList(new ArrayList<>(products));
  }
}
