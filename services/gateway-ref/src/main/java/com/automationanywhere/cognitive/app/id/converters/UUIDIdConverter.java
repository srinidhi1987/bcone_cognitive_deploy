package com.automationanywhere.cognitive.app.id.converters;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UUIDIdConverter extends BaseIdConverter<UUIDId> {

  @Autowired
  public UUIDIdConverter(final UUIDIdTransformer idTransformer) {
    super(idTransformer);
  }
}
