package com.automationanywhere.cognitive.validation;

import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.http.HttpStatus;

public class JsonValidationException extends ValidationException {

  public final List<ProcessingMessage> report;
  public final HttpStatus httpStatus;

  public JsonValidationException(final ProcessingReport processingReport,
      final HttpStatus httpStatus) {
    super("Json validation failed");

    this.httpStatus = httpStatus;

    List<ProcessingMessage> report = new ArrayList<>();
    for (final ProcessingMessage processingMessage : processingReport) {
      report.add(processingMessage);
    }
    this.report = Collections.unmodifiableList(report);
  }

  @Override
  public String getMessage() {
    return super.getMessage() + "\n:" + this.report;
  }
}
