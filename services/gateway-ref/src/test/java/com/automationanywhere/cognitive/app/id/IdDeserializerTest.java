package com.automationanywhere.cognitive.app.id;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class IdDeserializerTest {

  private final ObjectMapper om = new ObjectMapper();
  private final UUIDIdTransformer uuidIdTransformer = new UUIDIdTransformer();
  private final StringIdTransformer stringIdTransformer = new StringIdTransformer();
  private final CompositeIdTransformer idTransformer
      = new CompositeIdTransformer(this.uuidIdTransformer, this.stringIdTransformer);

  @BeforeMethod
  public void setUp() throws Exception {
  }

  @Test
  public void testNull() throws Exception {
    assertThat(new IdDeserializer<>(this.idTransformer, Id.class).deserialize(
        this.om.getFactory().createParser("null"), null
    )).isNull();
  }

  @Test
  public void testUUID() throws Exception {
    UUID uuid = UUID.randomUUID();
    assertThat(new IdDeserializer<>(this.idTransformer, Id.class).deserialize(
        this.om.getFactory().createParser("\"" + uuid.toString() + "\""), null
    )).isEqualTo(new UUIDId(uuid));
  }

  @Test
  public void testNotText() throws Exception {
    try {
      new IdDeserializer<>(this.idTransformer, Id.class).deserialize(
          this.om.getFactory().createParser("1"), null
      );
      Assert.fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).startsWith("1 is not supported by any known Id implementation");
    }
  }
}
