package com.automationanywhere.cognitive.app.errors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.testng.annotations.Test;

public class DefaultErrorDTOTest {

  @Test
  public void testCreate() {
    assertThat(new DefaultErrorDTO(200, new Exception(), null).path).isEqualTo("");
    assertThat(new DefaultErrorDTO("msg", 200, new Exception(), null).path).isEqualTo("");

    ServletWebRequest request = mock(ServletWebRequest.class);
    HttpServletRequest req = mock(HttpServletRequest.class);

    doReturn(req).when(request).getNativeRequest(HttpServletRequest.class);
    doReturn("localhost").when(req).getHeader("Host");
    doReturn("").when(req).getServletPath();
    doReturn("a=b").when(req).getQueryString();

    assertThat(new DefaultErrorDTO("msg", 200, new Exception(), request).path)
        .isEqualTo("localhost?a=b");

    doReturn("/").when(req).getPathInfo();
    doReturn(null).when(req).getQueryString();
    assertThat(new DefaultErrorDTO("msg", 200, new Exception(), request).path)
        .isEqualTo("localhost/");
  }
}
