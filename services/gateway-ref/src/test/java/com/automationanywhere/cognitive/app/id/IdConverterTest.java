package com.automationanywhere.cognitive.app.id;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.app.id.converters.IdConverter;
import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import java.util.UUID;
import org.testng.annotations.Test;

public class IdConverterTest {

  @Test
  public void testConvert() {
    IdConverter c = new IdConverter(new CompositeIdTransformer(
        new UUIDIdTransformer(), new StringIdTransformer()
    ));

    UUID uuid = UUID.randomUUID();
    assertThat(c.convert(uuid.toString())).isEqualTo(new UUIDId(uuid));
    assertThat(c.convert("id1")).isEqualTo(new StringId("id1"));
    assertThat(c.convert(null)).isNull();
  }
}
