package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static com.xebialabs.restito.builder.stub.StubHttp.whenHttp;
import static com.xebialabs.restito.semantics.Action.contentType;
import static com.xebialabs.restito.semantics.Action.ok;
import static com.xebialabs.restito.semantics.Action.status;
import static com.xebialabs.restito.semantics.Action.stringContent;
import static com.xebialabs.restito.semantics.Condition.get;
import static com.xebialabs.restito.semantics.Condition.parameter;
import static com.xebialabs.restito.semantics.Condition.withHeader;
import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.orders.microservices.customer.CustomerData;
import com.automationanywhere.cognitive.gateway.orders.microservices.order.OrderDataV2;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebialabs.restito.server.StubServer;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class OrderControllerV2Test extends AbstractTestNGSpringContextTests {

  @Autowired
  private TestRestTemplate rtpl;

  @Autowired
  private ApplicationConfiguration cfg;

  @Autowired
  private ObjectMapper om;

  private StubServer cServer;
  private StubServer oServer;

  @BeforeMethod
  public void setUp() throws Exception {
    this.cServer = new StubServer(new URI(this.cfg.customerURL()).getPort()).run();
    this.oServer = new StubServer(new URI(this.cfg.orderURL()).getPort()).run();
  }

  @AfterMethod
  public void tearDown() throws Exception {
    this.cServer.stop();
    this.oServer.stop();
  }

  @Test
  public void testGetOrders() throws Exception {
    Date createdAt = new Date();

    UUIDId oId1 = new UUIDId(UUID.randomUUID());
    UUIDId oId2 = new UUIDId(UUID.randomUUID());
    UUIDId cId = new UUIDId(UUID.randomUUID());
    UUIDId cId2 = new UUIDId(UUID.randomUUID());

    Map<String, Long> p1 = new HashMap<>();
    p1.put("p1", 1L);

    Map<String, Long> p2 = new HashMap<>();
    p2.put("p2", 10L);

    List<Map<String, Long>> products1 = Arrays.asList(p1, p2);
    List<ProductDTO> productsA = Arrays.asList(
        new ProductDTO("p1", 1L),
        new ProductDTO("p2", 10L)
    );

    Map<String, Long> a1 = new HashMap<>();
    a1.put("a1", 11L);

    HashMap<String, Long> a2 = new HashMap<>();
    a2.put("a2", 110L);

    List<Map<String, Long>> products2 = Arrays.asList(a1, a2);

    List<ProductDTO> productsB = Arrays.asList(
        new ProductDTO("a1", 11L),
        new ProductDTO("a2", 110L)
    );

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(new CustomerData(cId, "n 1")))
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(Arrays.asList(
            new OrderDataV2(oId1, createdAt, cId, products1),
            new OrderDataV2(oId2, createdAt, cId, products2)
        )))
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("x-forwarded-for", Collections.singletonList("127.0.0.1"));
    headers.put("accept", Collections.singletonList("application/json"));

    ResponseEntity<List<CustomerAccountOrderDTO>> result = this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<List<CustomerAccountOrderDTO>>() {
        }
    );
    assertThat(result.getBody()).isEqualTo(Arrays.asList(
        new CustomerAccountOrderDTO(oId1, createdAt, new CustomerAccountDTO(cId, "n", "1"),
            productsA),
        new CustomerAccountOrderDTO(oId2, createdAt, new CustomerAccountDTO(cId, "n", "1"),
            productsB)
    ));

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId2.uuid),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(new CustomerData(cId2, "n1")))
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId2.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(Arrays.asList(
            new OrderDataV2(oId1, createdAt, cId2, products1),
            new OrderDataV2(oId2, createdAt, cId2, products2)
        )))
    );

    headers = new HttpHeaders();
    headers.put("x-forwarded-for", Collections.singletonList("127.0.0.1"));
    headers.put("accept", Collections.singletonList("application/json"));

    result = this.rtpl.exchange(
        "/api/customers/" + cId2.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<List<CustomerAccountOrderDTO>>() {
        }
    );
    assertThat(result.getBody()).isEqualTo(Arrays.asList(
        new CustomerAccountOrderDTO(oId1, createdAt, new CustomerAccountDTO(cId2, "n1", ""),
            productsA),
        new CustomerAccountOrderDTO(oId2, createdAt, new CustomerAccountDTO(cId2, "n1", ""),
            productsB)
    ));
  }

  @Test
  public void testOrderServiceFailure() throws Exception {
    UUIDId cId = new UUIDId(UUID.randomUUID());

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        stringContent(this.om.writeValueAsString(new CustomerData(cId, "n1")))
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        status(HttpStatus.BAD_REQUEST_400),
        stringContent("Bad user data")
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));

    assertThat(this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCode().value()).isEqualTo(400);
  }

  @Test
  public void testCustomerServiceFailure() throws Exception {
    UUIDId oId1 = new UUIDId(UUID.randomUUID());
    UUIDId oId2 = new UUIDId(UUID.randomUUID());
    UUIDId cId = new UUIDId(UUID.randomUUID());

    Map<String, Long> p1 = new HashMap<>();
    p1.put("p1", 1L);

    Map<String, Long> p2 = new HashMap<>();
    p2.put("p2", 10L);

    List<Map<String, Long>> products1 = Arrays.asList(p1, p2);

    Map<String, Long> a1 = new HashMap<>();
    a1.put("a1", 11L);

    HashMap<String, Long> a2 = new HashMap<>();
    a2.put("a2", 110L);

    List<Map<String, Long>> products2 = Arrays.asList(a1, a2);

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        status(HttpStatus.BAD_REQUEST_400),
        stringContent("Bad user data")
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        stringContent(this.om.writeValueAsString(Arrays.asList(
            new OrderDataV2(oId1, new Date(), cId, products1),
            new OrderDataV2(oId2, new Date(), cId, products2)
        )))
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));

    assertThat(this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCode().value()).isEqualTo(500);
  }

  @Test
  public void testNotFound() {
    assertThat(this.rtpl.getForEntity(
        "/?a=b", String.class
    ).getStatusCode().value()).isEqualTo(404);
  }

  @Test
  public void testPost() throws Exception {
    Date createdAt = new Date();

    UUIDId oId1 = new UUIDId(UUID.randomUUID());
    UUIDId oId2 = new UUIDId(UUID.randomUUID());
    UUIDId cId = new UUIDId(UUID.randomUUID());

    Map<String, Long> p1 = new HashMap<>();
    p1.put("p1", 1L);

    Map<String, Long> p2 = new HashMap<>();
    p2.put("p2", 10L);

    List<Map<String, Long>> products1 = Arrays.asList(p1, p2);

    List<ProductDTO> productsA = Arrays.asList(
        new ProductDTO("p1", 1L),
        new ProductDTO("p2", 10L)
    );

    Map<String, Long> a1 = new HashMap<>();
    a1.put("a1", 11L);

    HashMap<String, Long> a2 = new HashMap<>();
    a2.put("a2", 110L);

    List<Map<String, Long>> products2 = Arrays.asList(a1, a2);

    List<ProductDTO> productsB = Arrays.asList(
        new ProductDTO("a1", 11L),
        new ProductDTO("a2", 110L)
    );

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(new CustomerData(cId, "n 1")))
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v2+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(Arrays.asList(
            new OrderDataV2(oId1, createdAt, cId, products1),
            new OrderDataV2(oId2, createdAt, cId, products2)
        )))
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("x-forwarded-for", Collections.singletonList("127.0.0.1"));
    headers.put("accept", Collections.singletonList("application/json"));

    ResponseEntity<List<CustomerAccountOrderDTO>> result = this.rtpl.exchange(
        "/api/customers/orders", HttpMethod.POST, new HttpEntity<>(new CustomerJson(cId), headers),
        new ParameterizedTypeReference<List<CustomerAccountOrderDTO>>() {
        }
    );

    assertThat(result.getBody()).isEqualTo(Arrays.asList(
        new CustomerAccountOrderDTO(oId1, createdAt, new CustomerAccountDTO(cId, "n", "1"),
            productsA),
        new CustomerAccountOrderDTO(oId2, createdAt, new CustomerAccountDTO(cId, "n", "1"),
            productsB)
    ));
  }

  @Test
  public void testInvalidCustomerJson() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));

    Map<String, String> e = new HashMap<>();
    e.put("id", "\"1\"");

    ResponseEntity<String> result = this.rtpl.exchange(
        "/api/customers/orders", HttpMethod.POST,
        new HttpEntity<>(e, headers),
        new ParameterizedTypeReference<String>() {
        }
    );
    assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.BAD_REQUEST_400.getStatusCode());
  }
}
