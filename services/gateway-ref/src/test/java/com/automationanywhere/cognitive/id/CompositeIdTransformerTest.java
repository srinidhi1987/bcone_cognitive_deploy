package com.automationanywhere.cognitive.id;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.UUID;
import org.testng.annotations.Test;

public class CompositeIdTransformerTest {

  @Test
  public void testTransform() {
    CompositeIdTransformer t = new CompositeIdTransformer(new StringIdTransformer());

    assertThat(t.toNode(null)).isSameAs(NullNode.getInstance());
    assertThat(((TextNode) t.toNode(new StringId("id1"))).asText()).isEqualTo("id1");
    assertThat(t.toString(new StringId("id1"))).isEqualTo("id1");
    assertThat(t.fromNode(new TextNode("id2"))).isEqualTo(new StringId("id2"));
    assertThat(t.fromString("id2")).isEqualTo(new StringId("id2"));

    try {
      t.fromNode(new IntNode(1));
      fail();
    } catch (final UnsupportedOperationException ex) {
      assertThat(ex.getMessage()).isEqualTo("No suitable transformers found");
    }
  }

  @Test
  public void testEmptyTransformer() throws Exception {
    CompositeIdTransformer t = new CompositeIdTransformer();

    try {
      t.fromString("id");
      fail();
    } catch (final UnsupportedOperationException ex) {
      assertThat(ex.getMessage()).isEqualTo("No transformers found");
    }
  }

  @Test
  public void testSupports() {
    CompositeIdTransformer t = new CompositeIdTransformer();

    try {
      t.supports(null);
      fail();
    } catch (final NullPointerException ex) {
      //As expected
    }

    assertThat(t.supports(Id.class)).isFalse();
    assertThat(t.supports(StringId.class)).isFalse();
    assertThat(t.supports(UUIDId.class)).isFalse();

    t = new CompositeIdTransformer(
        new UUIDIdTransformer(), new StringIdTransformer()
    );

    try {
      t.supports(null);
      fail();
    } catch (final NullPointerException ex) {
      //As expected
    }

    assertThat(t.supports(Id.class)).isFalse();
    assertThat(t.supports(StringId.class)).isTrue();
    assertThat(t.supports(UUIDId.class)).isTrue();
  }

  @Test
  public void testToId() {
    CompositeIdTransformer t = new CompositeIdTransformer(
        new UUIDIdTransformer(), new StringIdTransformer()
    );

    try {
      t.toId(new StringId("id1"), UUIDId.class);

      fail();
    } catch (final UnsupportedOperationException ex) {
      //As expected
    }

    try {
      t.toId(new StringId("id1"), BadId.class);

      fail();
    } catch (final UnsupportedOperationException ex) {
      //As expected
    }

    try {
      t.toId(new UUIDId(UUID.randomUUID()), BadId.class);

      fail();
    } catch (final UnsupportedOperationException ex) {
      //As expected
    }

    UUID uuid = UUID.randomUUID();
    assertThat(t.toId(new StringId(uuid.toString()), UUIDId.class))
        .isEqualTo(new UUIDId(uuid));

    assertThat(t.toId(new UUIDId(uuid), StringId.class))
        .isEqualTo(new StringId(uuid.toString()));
  }
}

class BadId implements Id {
}