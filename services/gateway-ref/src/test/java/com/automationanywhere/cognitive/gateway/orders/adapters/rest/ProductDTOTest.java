package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class ProductDTOTest {

  @Test
  public void testEquals() {
    ProductDTO c1 = new ProductDTO("n1", 1L);
    ProductDTO c2 = new ProductDTO("n1", 1L);
    ProductDTO c3 = new ProductDTO("n1", 2L);
    ProductDTO c4 = new ProductDTO("n2", 1L);

    assertThat(c1.equals(c1)).isTrue();
    assertThat(c1.equals(c2)).isTrue();
    assertThat(c1.equals(c3)).isFalse();
    assertThat(c1.equals(c4)).isFalse();
    assertThat(c1.equals(null)).isFalse();
    assertThat(c1.equals(new Object())).isFalse();
  }
}
