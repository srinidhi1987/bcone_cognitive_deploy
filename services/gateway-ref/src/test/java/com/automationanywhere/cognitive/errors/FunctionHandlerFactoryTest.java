package com.automationanywhere.cognitive.errors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.fail;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FunctionHandlerFactoryTest {

  private ExceptionHandlerFactory exceptionHandlerFactory;

  @BeforeMethod
  public void setUp() throws Exception {
    this.exceptionHandlerFactory = new ExceptionHandlerFactory();
  }

  @Test
  private void testFromMapper() {
    assertThat(this.exceptionHandlerFactory.fromMapper((a) -> {
      return "b" + a;
    }).apply("a")).isEqualTo("ba");

    Exception e = new Exception();

    try {
      this.exceptionHandlerFactory.fromMapper((a) -> {
        throw e;
      }).apply("a");
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testMap() {
    assertThat(this.exceptionHandlerFactory.<String, String>map("a", (a) -> {
      return "b" + a;
    })).isEqualTo("ba");

    Exception e = new Exception();

    try {
      this.exceptionHandlerFactory.map("a", (a) -> {
        throw e;
      });
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testFromHandler() {
    assertThat(this.exceptionHandlerFactory.fromHandler((a) -> {
    }).apply("a")).isNull();

    RuntimeException e = new RuntimeException();

    try {
      this.exceptionHandlerFactory.fromHandler((a) -> {
        throw e;
      }).apply("a");
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testHandle() throws Exception {
    Handler<String> handler = spy(new Handler<String>() {
      @Override
      public void handle(final String input) throws Exception {
      }
    });
    this.exceptionHandlerFactory.handle("a", handler);

    verify(handler).handle("a");

    RuntimeException e = new RuntimeException();

    try {
      this.exceptionHandlerFactory.handle("a", (a) -> {
        throw e;
      });
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testFromExecutable() {
    assertThat(this.exceptionHandlerFactory.fromExecutable(() -> {
    }).apply("a")).isNull();

    Exception e = new Exception();

    try {
      this.exceptionHandlerFactory.fromExecutable(() -> {
        throw e;
      }).apply("a");
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testExecute() throws Exception {
    Executable r = spy(new Executable() {
      @Override
      public void execute() throws Exception {
      }
    });
    this.exceptionHandlerFactory.execute(r);

    verify(r).execute();

    Exception e = new Exception();

    try {
      this.exceptionHandlerFactory.fromExecutable(() -> {
        throw e;
      }).apply("a");
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testFromCallable() {
    assertThat(this.exceptionHandlerFactory.fromCallable(() -> {
      return "b";
    }).apply("a")).isEqualTo("b");

    RuntimeException e = new RuntimeException();

    try {
      this.exceptionHandlerFactory.fromCallable(() -> {
        throw e;
      }).apply("a");
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }

  @Test
  private void testCall() {
    assertThat(this.exceptionHandlerFactory.<String>call(() -> {
      return "b";
    })).isEqualTo("b");

    RuntimeException e = new RuntimeException();

    try {
      this.exceptionHandlerFactory.fromCallable(() -> {
        throw e;
      }).apply("a");
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }
}
