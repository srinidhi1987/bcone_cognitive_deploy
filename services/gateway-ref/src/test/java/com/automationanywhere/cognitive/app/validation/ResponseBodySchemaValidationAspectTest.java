package com.automationanywhere.cognitive.app.validation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.automationanywhere.cognitive.app.errors.DefaultErrorDTO;
import com.automationanywhere.cognitive.errors.ErrorDTO;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.lang.reflect.Method;
import org.springframework.core.MethodParameter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ResponseBodySchemaValidationAspectTest {

  private ResponseBodySchemaValidationAspect a;
  private ClasspathSchemaValidator v;

  @BeforeMethod
  public void setUp() throws Exception {
    this.v = mock(ClasspathSchemaValidator.class);
    this.a = new ResponseBodySchemaValidationAspect(this.v);
  }

  @Test
  public void testBeforeBodyWriteWithNullAnnotation() throws Exception {
    Object b = new Object();

    Method m = NoAnnOutputClass.class.getMethod("m", Object.class);

    assertThat(this.a.beforeBodyWrite(b, new MethodParameter(m, 0), null, null, null, null))
        .isSameAs(b);

    verify(this.v, never()).checkOutput(any(), any());
  }

  @Test
  public void testBeforeBodyReadWithEmptyOutput() throws Exception {
    Object b = new Object();

    Method m = EmptyOutputClass.class.getMethod("m", Object.class);

    assertThat(this.a.beforeBodyWrite(b, new MethodParameter(m, 0), null, null, null, null))
        .isSameAs(b);

    verify(this.v, never()).checkOutput(any(), any());
  }

  @Test
  public void testBeforeBodyReadWithErrorDTO() throws Exception {
    ErrorDTO b = new DefaultErrorDTO(400, new Exception(), null);

    Method m = EmptyOutputClass.class.getMethod("m", Object.class);

    assertThat(this.a.beforeBodyWrite(b, new MethodParameter(m, 0), null, null, null, null))
        .isSameAs(b);

    verify(this.v, never()).checkOutput(any(), any());
  }
}

class NoAnnOutputClass {

  public void m(final Object i) {
  }
}

class EmptyOutputClass {

  @SchemaValidation
  public void m(final Object i) {
  }
}
