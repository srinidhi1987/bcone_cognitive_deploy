package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import org.apache.catalina.Valve;
import org.apache.catalina.valves.AccessLogValve;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomTomcatEmbeddedServletContainerFactoryTest {

  private CustomTomcatEmbeddedServletContainerFactory f;

  @BeforeMethod
  public void setUp() throws Exception {
    this.f = spy(new CustomTomcatEmbeddedServletContainerFactory());
  }

  @Test
  public void testAddEngineValves() {
    List<Valve> l = new ArrayList<>();

    Valve v1 = mock(Valve.class);
    AccessLogValve v2 = mock(AccessLogValve.class);

    doReturn(true).when(v2).getRequestAttributesEnabled();

    doReturn(l).when(this.f).createValvesList();

    this.f.addEngineValves(v1, v2);

    assertThat(l.size()).isEqualTo(2);
    assertThat(l.get(0)).isSameAs(v1);
    assertThat(((CustomAccessLogValve) l.get(1)).getRequestAttributesEnabled()).isTrue();
    assertThat(((CustomAccessLogValve) l.get(1)).getPattern())
        .isEqualTo("%t %a:%{remote}p->%v:%{local}p \"%m %U%q %H\" %s %b %D-%Fms");
  }

  @Test
  public void testEmptyValves() {
    Valve v1 = mock(Valve.class);
    Valve v2 = mock(Valve.class);

    this.f.addEngineValves(v1, v2);

    verify(this.f, never()).createCustomAccessLogValve();
  }
}
