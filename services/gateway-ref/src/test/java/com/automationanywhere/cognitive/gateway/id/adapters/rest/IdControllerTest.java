package com.automationanywhere.cognitive.gateway.id.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.string.StringId;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.Collections;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@Test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class IdControllerTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private TestRestTemplate rtpl;

  @Test
  public void testGet() throws Exception {
    String id = "id1";
    UUID uuid = UUID.randomUUID();

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept",
        Collections.singletonList("application/vnd.cognitive.v1+json;charset=UTF-8"));

    ResponseEntity<Ping> ping = this.rtpl.exchange(
        "/api/ping/" + id, HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<Ping>() {
        }
    );
    assertThat(ping.getBody()).isEqualTo(new Ping(new StringId(id)));

    ping = this.rtpl.exchange(
        "/api/ping/" + uuid, HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<Ping>() {
        }
    );
    assertThat(ping.getBody()).isEqualTo(new Ping(new UUIDId(uuid)));

    ResponseEntity<URL> stringURL = this.rtpl.exchange(
        "/api/path/" + id + "/string", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<URL>() {
        }
    );
    assertThat(stringURL.getBody().url).isEqualTo("http://www.example.com/" + id);

    ResponseEntity<URL> uuidURL = this.rtpl.exchange(
        "/api/path/" + uuid + "/uuid", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<URL>() {
        }
    );
    assertThat(uuidURL.getBody().url).isEqualTo("http://www.example.com/" + uuid);

    assertThat(this.rtpl.exchange(
        "/api/path/" + id + "/uuid", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCodeValue()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
  }

  @Test
  public void testPost() throws Exception {
    String id = "id1";
    UUID uuid = UUID.randomUUID();

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept",
        Collections.singletonList("application/vnd.cognitive.v1+json;charset=UTF-8"));

    ResponseEntity<Ping> ping = this.rtpl.exchange(
        "/api/ping", HttpMethod.POST, new HttpEntity<>(new Ping(new StringId(id)), headers),
        new ParameterizedTypeReference<Ping>() {
        }
    );
    assertThat(ping.getBody()).isEqualTo(new Ping(new StringId(id)));

    ping = this.rtpl.exchange(
        "/api/ping", HttpMethod.POST, new HttpEntity<>(new Ping(new UUIDId(uuid)), headers),
        new ParameterizedTypeReference<Ping>() {
        }
    );
    assertThat(ping.getBody()).isEqualTo(new Ping(new UUIDId(uuid)));

    ResponseEntity<URL> stringURL = this.rtpl.exchange(
        "/api/path/string", HttpMethod.POST, new HttpEntity<>(new Ping(new StringId(id)), headers),
        new ParameterizedTypeReference<URL>() {
        }
    );
    assertThat(stringURL.getBody().url).isEqualTo("http://www.example.com/" + id);

    ResponseEntity<URL> uuidURL = this.rtpl.exchange(
        "/api/path/uuid", HttpMethod.POST, new HttpEntity<>(new Ping(new UUIDId(uuid)), headers),
        new ParameterizedTypeReference<URL>() {
        }
    );
    assertThat(uuidURL.getBody().url).isEqualTo("http://www.example.com/" + uuid);

    assertThat(this.rtpl.exchange(
        "/api/path/uuid", HttpMethod.POST, new HttpEntity<>(new Ping(new StringId(id)), headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCodeValue()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
  }
}
