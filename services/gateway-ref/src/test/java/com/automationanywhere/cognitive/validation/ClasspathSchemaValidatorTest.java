package com.automationanywhere.cognitive.validation;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.app.id.IdModule;
import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.gateway.orders.adapters.rest.CustomerJson;
import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.integer.LongIdTransformer;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator.JsonSchemaValidator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ClasspathSchemaValidatorTest {

  private ObjectMapper om;

  @BeforeMethod
  public void setUp() throws Exception {
    om = new ObjectMapper();
    UUIDIdTransformer uuidIdTransformer = new UUIDIdTransformer();
    StringIdTransformer stringIdTransformer = new StringIdTransformer();
    LongIdTransformer longIdTransformer = new LongIdTransformer();
    om.registerModule(new IdModule(new CompositeIdTransformer(
        uuidIdTransformer, longIdTransformer, stringIdTransformer
    ), uuidIdTransformer, longIdTransformer, stringIdTransformer));
  }

  @Test
  public void testChecks() throws Exception {
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "schemas", ".json", om, new ExceptionHandlerFactory()
    );
    v.loadJsonSchemas();

    assertThat(v.checkKey("Customer")).isTrue();

    assertThat(v.checkInput(om.writeValueAsString(
        new CustomerJson(new UUIDId(UUID.randomUUID()))
    ), "Customer")).isTrue();

    assertThat(v.checkOutput(
        new CustomerJson(new UUIDId(UUID.randomUUID())),
        "Customer"
    )).isTrue();
  }

  @Test
  public void testErrors() {
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "com/automationanywhere/cognitive/app/validation", ".class", om,
        new ExceptionHandlerFactory());

    try {
      v.loadJsonSchemas();
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).startsWith("Failed to load schema from");
      assertThat(ex.getMessage()).endsWith(
          "com/automationanywhere/cognitive/app/validation/"
              + "RequestBodySchemaValidationAspectTest.class");
    }

    try {
      v.checkKey("key");
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo("Key key is unknown");
    }

    Exception e = new Exception("Test");

    JsonSchemaValidator vv = v.new JsonSchemaValidator() {
      @Override
      protected JsonNode load() throws Exception {
        throw e;
      }
    };

    try {
      vv.check("k", HttpStatus.BAD_REQUEST);
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getCause()).isSameAs(e);
    }
  }
}
