package com.automationanywhere.cognitive.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.common.configuration.FileConfigurationProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.testng.annotations.Test;

public class ApplicationConfigurationTest {

  @Test
  public void testBuildProperties() {
    ApplicationConfiguration ac = new ApplicationConfiguration(new FileConfigurationProvider(
        new ObjectMapper(), "application.yaml"
    ), "build.version");
    assertThat(ac.applicationVersion()).isNotBlank();
    assertThat(ac.applicationName()).isEqualTo("gateway-ref");
  }

  @Test
  public void testFatalError() {
    try {
      new ApplicationConfiguration(null, UUID.randomUUID().toString());
      fail();
    } catch (final Exception e) {
      assertThat(e.getCause()).isInstanceOf(NullPointerException.class);
    }
  }
}
