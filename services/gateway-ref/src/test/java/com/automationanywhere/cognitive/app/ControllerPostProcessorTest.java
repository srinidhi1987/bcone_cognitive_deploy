package com.automationanywhere.cognitive.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.BadControllerPostProcessorTestBean;
import com.automationanywhere.cognitive.InputControllerPostProcessorTestBean;
import com.automationanywhere.cognitive.InputOutputControllerPostProcessorTestBean;
import com.automationanywhere.cognitive.OutputControllerPostProcessorTestBean;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ControllerPostProcessorTest {

  private ControllerPostProcessor p;
  private ClasspathSchemaValidator v;

  @BeforeMethod
  public void setUp() throws Exception {
    this.v = mock(ClasspathSchemaValidator.class);
    this.p = spy(new ControllerPostProcessor(this.v));
  }

  @Test
  public void testOutput() throws Exception {
    OutputControllerPostProcessorTestBean b = new OutputControllerPostProcessorTestBean();

    this.p.postProcessBeforeInitialization(b, "bean");

    verify(this.v).checkKey("out");
    verify(this.v, times(1)).checkKey(any());
  }

  @Test
  public void testInput() throws Exception {
    InputControllerPostProcessorTestBean b = new InputControllerPostProcessorTestBean();

    this.p.postProcessBeforeInitialization(b, "bean");

    verify(this.v).checkKey("in");
    verify(this.v, times(1)).checkKey(any());
  }

  @Test
  public void testInputOutput() throws Exception {
    InputOutputControllerPostProcessorTestBean b = new InputOutputControllerPostProcessorTestBean();

    this.p.postProcessBeforeInitialization(b, "bean");

    verify(this.v).checkKey("in");
    verify(this.v).checkKey("out");
    verify(this.v, times(2)).checkKey(any());
  }

  @Test
  public void testBadBean() throws Exception {
    BadControllerPostProcessorTestBean b = new BadControllerPostProcessorTestBean();

    try {
      this.p.postProcessBeforeInitialization(b, "bean");
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo(
          "Either input or output is required: "
              + "public void com.automationanywhere.cognitive.BadControllerPostProcessorTestBean"
              + ".pub(java.lang.Object)");
    }
  }
}

