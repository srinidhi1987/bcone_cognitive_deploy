package com.automationanywhere.cognitive.app.id;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.UUID;
import org.testng.annotations.Test;

public class UUIDIdTest {

  @Test
  public void testEquals() {
    UUID uuid = UUID.randomUUID();
    UUIDId u1 = new UUIDId(uuid);
    UUIDId u2 = new UUIDId(uuid);
    UUIDId u3 = new UUIDId(UUID.randomUUID());

    assertThat(u1.equals(u1)).isTrue();
    assertThat(u1.equals(u2)).isTrue();
    assertThat(u1.equals(u3)).isFalse();
    assertThat(u1.equals(null)).isFalse();
    assertThat(u1.equals(new Object())).isFalse();
  }
}
