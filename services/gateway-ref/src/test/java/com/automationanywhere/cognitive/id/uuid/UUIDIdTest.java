package com.automationanywhere.cognitive.id.uuid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import java.util.UUID;
import org.testng.annotations.Test;

public class UUIDIdTest {
  @Test
  public void testCreate() throws Exception {
    UUID uuid = UUID.randomUUID();
    assertThat(new UUIDId(uuid).uuid).isEqualTo(uuid);

    try {
      new UUIDId(null);
      fail();
    } catch (final IllegalArgumentException ex) {
      //As expected
    }
  }


  @Test
  public void testEquals() {
    UUID uuid = UUID.randomUUID();
    UUIDId u1 = new UUIDId(uuid);
    UUIDId u2 = new UUIDId(uuid);
    UUIDId u3 = new UUIDId(UUID.randomUUID());

    assertThat(u1.equals(u1)).isTrue();
    assertThat(u1.equals(u2)).isTrue();
    assertThat(u1.equals(u3)).isFalse();
    assertThat(u1.equals(null)).isFalse();
    assertThat(u1.equals(new Object())).isFalse();
  }

  @Test
  public void testHashCode() {
    UUID uuid1 = UUID.randomUUID();
    UUIDId id1 = new UUIDId(uuid1);
    assertThat(id1.hashCode()).isNotEqualTo(uuid1.hashCode());
    assertThat(id1.hashCode()).isEqualTo(new UUIDId(uuid1).hashCode());
    assertThat(id1.hashCode()).isNotEqualTo(new UUIDId(UUID.randomUUID()).hashCode());
  }
}
