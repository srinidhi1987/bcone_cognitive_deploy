package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplateHandler;
import org.testng.annotations.Test;

public class DelegatingRestTemplateTest {

  @Test
  public void testDelegating1() throws Exception {
    RestTemplate d = mock(RestTemplate.class);
    List<HttpMessageConverter<?>> messageConverters = mock(List.class);
    ResponseErrorHandler errorHandler = mock(ResponseErrorHandler.class);
    UriTemplateHandler handler = mock(UriTemplateHandler.class);
    Map<String, ?> defaultUriVariables = new HashMap<>();
    Map<String, ?> uriVariables = mock(Map.class);
    URI u1 = new URI("file:///");
    URI uri = new URI("file:///a");
    URI uri2 = new URI("file:///b");
    URI uri3 = new URI("file:///b");
    Integer o = -1;
    String o2 = "-2";
    Long o3 = -3L;
    ResponseEntity re = mock(ResponseEntity.class);
    HttpHeaders h = mock(HttpHeaders.class);
    Set<HttpMethod> s = new HashSet<>();
    Set<HttpMethod> s2 = new HashSet<>();
    DelegatingRestTemplate r = new DelegatingRestTemplate(d);

    r.setMessageConverters(messageConverters);
    verify(d).setMessageConverters(messageConverters);

    doReturn(messageConverters).when(d).getMessageConverters();
    assertThat(r.getMessageConverters()).isSameAs(messageConverters);

    r.setErrorHandler(errorHandler);
    verify(d).setErrorHandler(errorHandler);

    doReturn(errorHandler).when(d).getErrorHandler();
    assertThat(r.getErrorHandler()).isSameAs(errorHandler);

    r.setDefaultUriVariables(defaultUriVariables);
    verify(d).setDefaultUriVariables(defaultUriVariables);

    r.setUriTemplateHandler(handler);
    verify(d).setUriTemplateHandler(handler);

    doReturn(handler).when(d).getUriTemplateHandler();
    assertThat(r.getUriTemplateHandler()).isSameAs(handler);

    doReturn(o).when(d).getForObject("u", Integer.class);
    assertThat(r.getForObject("u", Integer.class)).isSameAs(o);

    doReturn(o2).when(d).getForObject("u2", String.class, uriVariables);
    assertThat(r.getForObject("u2", String.class, uriVariables)).isSameAs(o2);

    doReturn(o3).when(d).getForObject(u1, Long.class);
    assertThat(r.getForObject(u1, Long.class)).isSameAs(o3);

    doReturn(re).when(d).getForEntity("u", Integer.class, "v1");
    assertThat(r.getForEntity("u", Integer.class, "v1")).isSameAs(re);

    doReturn(re).when(d).getForEntity("u2", String.class, uriVariables);
    assertThat(r.getForEntity("u2", String.class, uriVariables)).isSameAs(re);

    doReturn(re).when(d).getForEntity(u1, Long.class);
    assertThat(r.getForEntity(u1, Long.class)).isSameAs(re);

    doReturn(h).when(d).headForHeaders("u", "v1");
    assertThat(r.headForHeaders("u", "v1")).isSameAs(h);

    doReturn(h).when(d).headForHeaders("u2", uriVariables);
    assertThat(r.headForHeaders("u2", uriVariables)).isSameAs(h);

    doReturn(h).when(d).headForHeaders(u1);
    assertThat(r.headForHeaders(u1)).isSameAs(h);

    doReturn(uri).when(d).postForLocation("u", o, "v1");
    assertThat(r.postForLocation("u", o, "v1")).isSameAs(uri);

    doReturn(uri2).when(d).postForLocation("u2", o2, uriVariables);
    assertThat(r.postForLocation("u2", o2, uriVariables)).isSameAs(uri2);

    doReturn(uri3).when(d).postForLocation(u1, o3);
    assertThat(r.postForLocation(u1, o3)).isSameAs(uri3);

    doReturn(o).when(d).postForObject("u", 1L, Integer.class, "v");
    assertThat(r.postForObject("u", 1L, Integer.class, "v")).isSameAs(o);

    doReturn(o2).when(d).postForObject("u2", 2, String.class, uriVariables);
    assertThat(r.postForObject("u2", 2, String.class, uriVariables)).isSameAs(o2);

    doReturn(o3).when(d).postForObject(u1, "3", Long.class);
    assertThat(r.postForObject(u1, "3", Long.class)).isSameAs(o3);

    doReturn(re).when(d).postForEntity("u", 1L, Integer.class, "v");
    assertThat(r.postForEntity("u", 1L, Integer.class, "v")).isSameAs(re);

    doReturn(re).when(d).postForEntity("u2", 2, String.class, uriVariables);
    assertThat(r.postForEntity("u2", 2, String.class, uriVariables)).isSameAs(re);

    doReturn(re).when(d).postForEntity(u1, "3", Long.class);
    assertThat(r.postForEntity(u1, "3", Long.class)).isSameAs(re);

    r.put("u", 1L);
    verify(d).put("u", 1L);

    r.put("u2", 2, uriVariables);
    verify(d).put("u2", 2, uriVariables);

    r.put(u1, "3");
    verify(d).put(u1, "3");

    doReturn(o).when(d).patchForObject("u", 1L, Integer.class, "v");
    assertThat(r.patchForObject("u", 1L, Integer.class, "v")).isSameAs(o);

    doReturn(o2).when(d).patchForObject("u2", 2, String.class, uriVariables);
    assertThat(r.patchForObject("u2", 2, String.class, uriVariables)).isSameAs(o2);

    doReturn(o3).when(d).patchForObject(u1, "3", Long.class);
    assertThat(r.patchForObject(u1, "3", Long.class)).isSameAs(o3);

    r.delete("u");
    verify(d).delete("u");

    r.delete("u2", uriVariables);
    verify(d).delete("u2", uriVariables);

    r.delete(u1);
    verify(d).delete(u1);

    doReturn(s).when(d).optionsForAllow("u");
    assertThat(r.optionsForAllow("u")).isSameAs(s);

    doReturn(s2).when(d).optionsForAllow("u2", uriVariables);
    assertThat(r.optionsForAllow("u2", uriVariables)).isSameAs(s2);
  }

  @Test
  public void testDelegating2() throws Exception {
    RestTemplate d = mock(RestTemplate.class);
    Map<String, ?> uriVariables = mock(Map.class);
    URI u1 = new URI("file:///");
    ResponseEntity re = mock(ResponseEntity.class);
    DelegatingRestTemplate r = new DelegatingRestTemplate(d);
    RequestEntity<Integer> rei = mock(RequestEntity.class);
    Set<HttpMethod> s3 = new HashSet<>();
    HttpEntity<?> reqe = mock(HttpEntity.class);
    HttpEntity<?> reqe2 = mock(HttpEntity.class);
    HttpEntity<?> reqe3 = mock(HttpEntity.class);

    ParameterizedTypeReference<Integer> i = new ParameterizedTypeReference<Integer>() {
    };
    ParameterizedTypeReference<Long> l = new ParameterizedTypeReference<Long>() {
    };
    ParameterizedTypeReference<String> str = new ParameterizedTypeReference<String>() {
    };

    ClientHttpRequestFactory rf = mock(ClientHttpRequestFactory.class);
    ClientHttpRequestFactory rf2 = mock(ClientHttpRequestFactory.class);

    List<ClientHttpRequestInterceptor> is = mock(List.class);
    List<ClientHttpRequestInterceptor> is2 = mock(List.class);

    doReturn(s3).when(d).optionsForAllow(u1);
    assertThat(r.optionsForAllow(u1)).isSameAs(s3);

    doReturn(re).when(d).exchange("u", HttpMethod.HEAD, reqe, Integer.class, "v1");
    assertThat(r.exchange("u", HttpMethod.HEAD, reqe, Integer.class, "v1")).isSameAs(re);

    doReturn(re).when(d).exchange("u2", HttpMethod.HEAD, reqe2, Long.class, uriVariables);
    assertThat(r.exchange("u2", HttpMethod.HEAD, reqe2, Long.class, uriVariables)).isSameAs(re);

    doReturn(re).when(d).exchange(u1, HttpMethod.HEAD, reqe3, String.class);
    assertThat(r.exchange(u1, HttpMethod.HEAD, reqe3, String.class)).isEqualTo(re);

    doReturn(re).when(d).exchange("u", HttpMethod.HEAD, reqe, Integer.class, "v1");
    assertThat(r.exchange("u", HttpMethod.HEAD, reqe, Integer.class, "v1")).isSameAs(re);

    doReturn(re).when(d).exchange("u2", HttpMethod.HEAD, reqe2, Integer.class, uriVariables);
    assertThat(r.exchange("u2", HttpMethod.HEAD, reqe2, Integer.class, uriVariables)).isSameAs(re);

    doReturn(re).when(d).exchange(u1, HttpMethod.HEAD, reqe3, Integer.class);
    assertThat(r.exchange(u1, HttpMethod.HEAD, reqe3, Integer.class)).isSameAs(re);

    doReturn(re).when(d).exchange("u", HttpMethod.HEAD, reqe, i, "v1");
    assertThat(r.exchange("u", HttpMethod.HEAD, reqe, i, "v1")).isSameAs(re);

    doReturn(re).when(d).exchange("u2", HttpMethod.HEAD, reqe2, l, uriVariables);
    assertThat(r.exchange("u2", HttpMethod.HEAD, reqe2, l, uriVariables)).isSameAs(re);

    doReturn(re).when(d).exchange(u1, HttpMethod.HEAD, reqe3, str);
    assertThat(r.exchange(u1, HttpMethod.HEAD, reqe3, str)).isSameAs(re);

    doReturn(re).when(d).exchange(rei, Integer.class);
    assertThat(r.exchange(rei, Integer.class)).isSameAs(re);

    doReturn(re).when(d).exchange(rei, i);
    assertThat(r.exchange(rei, i)).isSameAs(re);

    RequestCallback cb = mock(RequestCallback.class);
    ResponseExtractor<Integer> exti = mock(ResponseExtractor.class);
    ResponseExtractor<Long> extl = mock(ResponseExtractor.class);
    ResponseExtractor<String> exts = mock(ResponseExtractor.class);

    doReturn(1).when(d).execute("u", HttpMethod.HEAD, cb, exti, "v1");
    assertThat(r.execute("u", HttpMethod.HEAD, cb, exti, "v1")).isEqualTo(1);

    doReturn(2L).when(d).execute("u2", HttpMethod.HEAD, cb, extl, uriVariables);
    assertThat(r.execute("u2", HttpMethod.HEAD, cb, extl, uriVariables)).isEqualTo(2L);

    doReturn("str").when(d).execute(u1, HttpMethod.HEAD, cb, exts);
    assertThat(r.execute(u1, HttpMethod.HEAD, cb, exts)).isEqualTo("str");

    r.setInterceptors(is);
    verify(d).setInterceptors(is);

    doReturn(is2).when(d).getInterceptors();
    assertThat(r.getInterceptors()).isSameAs(is2);

    r.setRequestFactory(rf);
    verify(d).setRequestFactory(rf);

    doReturn(rf2).when(d).getRequestFactory();
    assertThat(r.getRequestFactory()).isSameAs(rf2);
  }
}
