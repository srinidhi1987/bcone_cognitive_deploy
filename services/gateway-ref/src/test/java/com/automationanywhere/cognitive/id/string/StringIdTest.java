package com.automationanywhere.cognitive.id.string;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import org.testng.annotations.Test;

public class StringIdTest {

  @Test
  public void testCreate() throws Exception {
    assertThat(new StringId("id1").id).isEqualTo("id1");

    try {
      new StringId(null);
      fail();
    } catch (final IllegalArgumentException ex) {
      //As expected
    }
  }

  @Test
  public void testEquals() {
    String id = "id1";
    StringId u1 = new StringId(id);
    StringId u2 = new StringId(id);
    StringId u3 = new StringId("id2");

    assertThat(u1.equals(u1)).isTrue();
    assertThat(u1.equals(u2)).isTrue();
    assertThat(u1.equals(u3)).isFalse();
    assertThat(u1.equals(null)).isFalse();
    assertThat(u1.equals(new Object())).isFalse();
  }

  @Test
  public void testHashCode() {
    String id1 = "id1";
    StringId sid1 = new StringId(id1);
    assertThat(sid1.hashCode()).isNotEqualTo(id1.hashCode());
    assertThat(sid1.hashCode()).isEqualTo(new StringId(id1).hashCode());
    assertThat(sid1.hashCode()).isNotEqualTo(new StringId("id2").hashCode());
  }
}
