package com.automationanywhere.cognitive;

import com.automationanywhere.cognitive.app.Application;
import com.automationanywhere.cognitive.app.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Import;

@TestConfiguration
@SpringBootApplication(scanBasePackageClasses = Application.class)
@Import(SpringBootConfiguration.class)
public class TestApplication {

}
