package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.Collections;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@Test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class OrderControllerTimeoutTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private TestRestTemplate rtpl;

  @Test
  public void testOrderServiceFailure() throws Exception {
    UUIDId cId = new UUIDId(UUID.randomUUID());

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept", Collections.singletonList("application/json"));

    assertThat(this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCode().value()).isEqualTo(500);
  }
}
