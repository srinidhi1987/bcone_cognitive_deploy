package com.automationanywhere.cognitive.id.string;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.UUID;
import org.testng.annotations.Test;

public class StringIdTransformerTest {

  @Test
  public void testCreate() throws Exception {
    UUID uuid = UUID.randomUUID();
    assertThat(new UUIDId(uuid).uuid).isEqualTo(uuid);

    try {
      new UUIDId(null);
      fail();
    } catch (final IllegalArgumentException ex) {
      //As expected
    }
  }

  @Test
  public void testTransformations() {
    StringIdTransformer t = new StringIdTransformer();
    String id = "id1";

    assertThat(t.fromNode(NullNode.getInstance())).isNull();
    assertThat(t.fromNode(null)).isNull();
    assertThat(t.fromString(null)).isNull();
    assertThat(t.toString(null)).isNull();
    assertThat(t.toNode(null)).isSameAs(NullNode.getInstance());

    assertThat(t.fromString(id)).isEqualTo(new StringId(id));
    assertThat(t.fromNode(new TextNode(id))).isEqualTo(new StringId(id));

    assertThat(t.toNode(new StringId(id))).isEqualTo(new TextNode(id));
    assertThat(t.toString(new StringId(id))).isEqualTo(id);

    try {
      t.fromNode(new IntNode(2));
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo(
          "class com.fasterxml.jackson.databind.node.IntNode is not supported"
      );
    }
  }

  @Test
  public void testSupports() {
    StringIdTransformer t = new StringIdTransformer();
    assertThat(t.supports(Id.class)).isFalse();
    assertThat(t.supports(StringId.class)).isTrue();
    assertThat(t.supports(UUIDId.class)).isFalse();

    try {
      t.supports(null);
      fail();
    } catch (final NullPointerException ex) {
      //As expected
    }
  }
}
