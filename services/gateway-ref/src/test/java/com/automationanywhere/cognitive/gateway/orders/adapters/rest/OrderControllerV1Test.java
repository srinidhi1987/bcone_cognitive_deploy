package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static com.xebialabs.restito.builder.stub.StubHttp.whenHttp;
import static com.xebialabs.restito.semantics.Action.contentType;
import static com.xebialabs.restito.semantics.Action.ok;
import static com.xebialabs.restito.semantics.Action.status;
import static com.xebialabs.restito.semantics.Action.stringContent;
import static com.xebialabs.restito.semantics.Condition.get;
import static com.xebialabs.restito.semantics.Condition.parameter;
import static com.xebialabs.restito.semantics.Condition.withHeader;
import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.gateway.orders.microservices.customer.CustomerData;
import com.automationanywhere.cognitive.gateway.orders.microservices.order.OrderData;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebialabs.restito.server.StubServer;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public final class OrderControllerV1Test extends AbstractTestNGSpringContextTests {

  @Autowired
  private TestRestTemplate rtpl;

  @Autowired
  private ApplicationConfiguration cfg;

  @Autowired
  private ObjectMapper om;

  private StubServer cServer;
  private StubServer oServer;

  @BeforeMethod
  public void setUp() throws Exception {
    this.cServer = new StubServer(new URI(this.cfg.customerURL()).getPort()).run();
    this.oServer = new StubServer(new URI(this.cfg.orderURL()).getPort()).run();
  }

  @AfterMethod
  public void tearDown() throws Exception {
    this.cServer.stop();
    this.oServer.stop();
  }

  @Test
  public void testGetOrders() throws Exception {
    UUIDId oId1 = new UUIDId(UUID.randomUUID());
    UUIDId oId2 = new UUIDId(UUID.randomUUID());
    UUIDId cId = new UUIDId(UUID.randomUUID());

    List<Map<String, Long>> products1 = Arrays.asList(
        new HashMap<String, Long>() {{
          put("p1", 1L);
        }},
        new HashMap<String, Long>() {{
          put("p2", 10L);
        }}
    );

    List<Map<String, Long>> products2 = Arrays.asList(
        new HashMap<String, Long>() {{
          put("a1", 11L);
        }},
        new HashMap<String, Long>() {{
          put("a2", 110L);
        }}
    );

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v1+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(new CustomerData(cId, "n 1")))
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v1+json;charset=UTF-8")
    ).then(
        ok(),
        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE),
        stringContent(this.om.writeValueAsString(Arrays.asList(
            new OrderData(oId1, cId, products1),
            new OrderData(oId2, cId, products2)
        )))
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("x-forwarded-for", Collections.singletonList("127.0.0.1"));
    headers.put("accept",
        Collections.singletonList("application/vnd.cognitive.v1+json;charset=UTF-8"));

    ResponseEntity<List<CustomerOrderDTO>> result = this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<List<CustomerOrderDTO>>() {
        }
    );
    assertThat(result.getBody()).isEqualTo(Arrays.asList(
        new CustomerOrderDTO(oId1, new CustomerDTO(cId, "n 1"), Arrays.asList(
            new ProductDTO("p1", 1L),
            new ProductDTO("p2", 10L)
        )),
        new CustomerOrderDTO(oId2, new CustomerDTO(cId, "n 1"), Arrays.asList(
            new ProductDTO("a1", 11L),
            new ProductDTO("a2", 110L)
        ))
    ));
  }

  @Test
  public void testOrderServiceFailure() throws Exception {
    UUIDId cId = new UUIDId(UUID.randomUUID());

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v1+json;charset=UTF-8")
    ).then(
        ok(),
        stringContent(this.om.writeValueAsString(new CustomerData(cId, "n1")))
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v1+json;charset=UTF-8")
    ).then(
        status(HttpStatus.BAD_REQUEST_400),
        stringContent("Bad user data")
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept",
        Collections.singletonList("application/vnd.cognitive.v1+json;charset=UTF-8"));

    assertThat(this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCode().value()).isEqualTo(500);
  }

  @Test
  public void testCustomerServiceFailure() throws Exception {
    UUIDId oId1 = new UUIDId(UUID.randomUUID());
    UUIDId oId2 = new UUIDId(UUID.randomUUID());
    UUIDId cId = new UUIDId(UUID.randomUUID());

    List<Map<String, Long>> products1 = Arrays.asList(
        new HashMap<String, Long>() {{
          put("p1", 1L);
        }},
        new HashMap<String, Long>() {{
          put("p2", 10L);
        }}
    );

    List<Map<String, Long>> products2 = Arrays.asList(
        new HashMap<String, Long>() {{
          put("a1", 11L);
        }},
        new HashMap<String, Long>() {{
          put("a2", 110L);
        }}
    );

    whenHttp(this.cServer).match(
        get("/api/customers/" + cId.uuid),
        withHeader("accept", "application/vnd.cognitive.v1+json;charset=UTF-8")
    ).then(
        status(HttpStatus.BAD_REQUEST_400),
        stringContent("Bad user data")
    );

    whenHttp(this.oServer).match(
        get("/api/orders"), parameter("customerId", cId.uuid.toString()),
        withHeader("accept", "application/vnd.cognitive.v1+json;charset=UTF-8")
    ).then(
        ok(),
        stringContent(this.om.writeValueAsString(Arrays.asList(
            new OrderData(oId1, cId, products1),
            new OrderData(oId2, cId, products2)
        )))
    );

    HttpHeaders headers = new HttpHeaders();
    headers.put("accept",
        Collections.singletonList("application/vnd.cognitive.v1+json;charset=UTF-8"));

    assertThat(this.rtpl.exchange(
        "/api/customers/" + cId.uuid + "/orders", HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<String>() {
        }
    ).getStatusCode().value()).isEqualTo(400);
  }

  @Test
  public void testNotFound() {
    assertThat(this.rtpl.getForEntity(
        "/?a=b", String.class
    ).getStatusCode().value()).isEqualTo(404);
  }
}
