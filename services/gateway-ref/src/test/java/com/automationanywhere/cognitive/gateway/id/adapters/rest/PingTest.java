package com.automationanywhere.cognitive.gateway.id.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.string.StringId;
import org.testng.annotations.Test;

public class PingTest {
  @Test
  public void testEquals() {
    StringId id1 = new StringId("id");
    StringId id2 = new StringId("id");
    StringId id3 = new StringId("di");

    Ping p1 = new Ping(id1);
    Ping p2 = new Ping(id2);
    Ping p3 = new Ping(id3);

    assertThat(p1.equals(p1)).isTrue();
    assertThat(p1.equals(p2)).isTrue();
    assertThat(p1.equals(p3)).isFalse();
    assertThat(p1.equals(null)).isFalse();
    assertThat(p1.equals(new Object())).isFalse();
  }
}
