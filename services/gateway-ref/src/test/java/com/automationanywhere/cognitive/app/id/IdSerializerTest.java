package com.automationanywhere.cognitive.app.id;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.CompositeIdTransformer;
import com.automationanywhere.cognitive.id.Id;
import com.automationanywhere.cognitive.id.string.StringIdTransformer;
import com.automationanywhere.cognitive.id.uuid.UUIDId;
import com.automationanywhere.cognitive.id.uuid.UUIDIdTransformer;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.UUID;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IdSerializerTest {

  private final IdSerializer s = new IdSerializer(
      new CompositeIdTransformer(new UUIDIdTransformer(), new StringIdTransformer())
  );

  @Test
  public void testWriteUUID() throws Exception {
    JsonGenerator jg = Mockito.mock(JsonGenerator.class);

    UUID uuid = UUID.randomUUID();
    this.s.serialize(new UUIDId(uuid), jg, null);

    Mockito.verify(jg).writeTree(new TextNode(uuid.toString()));
  }

  @Test
  public void testWriteNull() throws Exception {
    JsonGenerator jg = Mockito.mock(JsonGenerator.class);

    this.s.serialize(null, jg, null);

    Mockito.verify(jg).writeNull();
  }

  @Test
  public void testUnsupportedType() throws Exception {
    JsonGenerator jg = Mockito.mock(JsonGenerator.class);
    Id id = Mockito.mock(Id.class);

    try {
      this.s.serialize(id, jg, null);
      Assert.fail();
    } catch (final JsonGenerationException ex) {
      assertThat(ex.getMessage()).isEqualTo(id.getClass().getName() + " is not supported");
    }
  }
}
