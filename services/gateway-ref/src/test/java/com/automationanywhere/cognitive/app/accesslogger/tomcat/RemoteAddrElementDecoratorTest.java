package com.automationanywhere.cognitive.app.accesslogger.tomcat;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.io.CharArrayWriter;
import java.util.Date;
import org.apache.catalina.connector.Request;
import org.testng.annotations.Test;

public class RemoteAddrElementDecoratorTest {

  @Test
  public void testAddElement() {
    Date d = new Date();
    CharArrayWriter buf = new CharArrayWriter();

    CustomAccessLogValve.CustomAccessLogElement e = mock(
        CustomAccessLogValve.CustomAccessLogElement.class);

    Request req = mock(Request.class);

    doReturn(null).when(req).getHeader("x-forwarded-for");

    doAnswer((c) -> {
      ((CharArrayWriter) c.getArgument(0)).write("TesT");
      return null;
    }).when(e).addElement(buf, d, req, null, -2);

    RemoteAddrElementDecorator dec = new RemoteAddrElementDecorator(e);
    dec.addElement(buf, d, req, null, -2);

    assertThat(buf.toString()).isEqualTo("TesT");
  }

  @Test
  public void testAddElementAndHeader() {
    Date d = new Date();
    CharArrayWriter buf = new CharArrayWriter();

    CustomAccessLogValve.CustomAccessLogElement e = mock(
        CustomAccessLogValve.CustomAccessLogElement.class);

    Request req = mock(Request.class);

    doReturn("hdr").when(req).getHeader("x-forwarded-for");

    doAnswer((c) -> {
      ((CharArrayWriter) c.getArgument(0)).write("TesT");
      return null;
    }).when(e).addElement(buf, d, req, null, -2);

    RemoteAddrElementDecorator dec = new RemoteAddrElementDecorator(e);
    dec.addElement(buf, d, req, null, -2);

    assertThat(buf.toString()).isEqualTo("TesT/hdr");
  }
}
