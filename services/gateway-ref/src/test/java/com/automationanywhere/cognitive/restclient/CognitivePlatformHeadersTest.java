package com.automationanywhere.cognitive.restclient;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class CognitivePlatformHeadersTest {

  @Test
  public void testValues() {
    assertThat(CognitivePlatformHeaders.values()).containsExactlyInAnyOrder(
        CognitivePlatformHeaders.APPLICATION_NAME,
        CognitivePlatformHeaders.APPLICATION_VERSION,
        CognitivePlatformHeaders.CID,
        CognitivePlatformHeaders.CLIENT_IP,
        CognitivePlatformHeaders.HOST_NAME,
        CognitivePlatformHeaders.SESSION_ID,
        CognitivePlatformHeaders.TENANT_ID
    );
  }

  @Test
  public void testValueOf() {
    assertThat(CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.CID.name()))
        .isEqualTo(CognitivePlatformHeaders.CID);
    assertThat(CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.HOST_NAME.name()))
        .isEqualTo(CognitivePlatformHeaders.HOST_NAME);
    assertThat(CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.CLIENT_IP.name()))
        .isEqualTo(CognitivePlatformHeaders.CLIENT_IP);
    assertThat(CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.APPLICATION_NAME.name()))
        .isEqualTo(CognitivePlatformHeaders.APPLICATION_NAME);
    assertThat(
        CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.APPLICATION_VERSION.name()))
        .isEqualTo(CognitivePlatformHeaders.APPLICATION_VERSION);
    assertThat(CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.SESSION_ID.name()))
        .isEqualTo(CognitivePlatformHeaders.SESSION_ID);
    assertThat(CognitivePlatformHeaders.valueOf(CognitivePlatformHeaders.TENANT_ID.name()))
        .isEqualTo(CognitivePlatformHeaders.TENANT_ID);
  }
}
