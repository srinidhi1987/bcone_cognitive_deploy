package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.UUID;
import org.testng.annotations.Test;

public class CustomerAccountDTOTest {

  @Test
  public void testEquals() {
    UUID uuid1 = UUID.randomUUID();
    UUIDId id1 = new UUIDId(uuid1);
    UUIDId id2 = new UUIDId(uuid1);
    UUIDId id3 = new UUIDId(UUID.randomUUID());

    CustomerAccountDTO c1 = new CustomerAccountDTO(id1, "n", "1");
    CustomerAccountDTO c2 = new CustomerAccountDTO(id2, "n", "1");
    CustomerAccountDTO c3 = new CustomerAccountDTO(id3, "n", "1");
    CustomerAccountDTO c4 = new CustomerAccountDTO(id2, "m", "1");
    CustomerAccountDTO c5 = new CustomerAccountDTO(id2, "n", "2");

    assertThat(c1.equals(c1)).isTrue();
    assertThat(c1.equals(c2)).isTrue();
    assertThat(c1.equals(c3)).isFalse();
    assertThat(c1.equals(c4)).isFalse();
    assertThat(c1.equals(c5)).isFalse();
    assertThat(c1.equals(null)).isFalse();
    assertThat(c1.equals(new Object())).isFalse();
  }
}
