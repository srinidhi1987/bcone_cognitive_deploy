package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.mockito.Mockito;
import org.testng.annotations.Test;

public class CustomerOrderDTOTest {

  @Test
  public void testEquals() {
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();
    UUIDId id1 = new UUIDId(uuid1);
    UUIDId id2 = new UUIDId(uuid1);
    UUIDId id3 = new UUIDId(UUID.randomUUID());

    UUIDId id21 = new UUIDId(uuid2);
    UUIDId id22 = new UUIDId(uuid2);
    UUIDId id23 = new UUIDId(UUID.randomUUID());
    UUIDId id24 = new UUIDId(UUID.randomUUID());

    List<ProductDTO> p1 = Arrays.asList(
        new ProductDTO("a", 1L),
        new ProductDTO("b", 2L)
    );
    List<ProductDTO> p2 = Arrays.asList(
        new ProductDTO("a", 1L),
        new ProductDTO("b", 2L)
    );
    List<ProductDTO> p3 = Arrays.asList(
        new ProductDTO("aa", 1L),
        new ProductDTO("bb", 2L)
    );
    List<ProductDTO> p5 = Arrays.asList(
        new ProductDTO("kk", 11L),
        new ProductDTO("ll", 22L)
    );
    CustomerDTO c1 = new CustomerDTO(id21, "nm1");
    CustomerDTO c2 = new CustomerDTO(id22, "nm1");
    CustomerDTO c3 = new CustomerDTO(id23, "nm1");
    CustomerDTO c4 = new CustomerDTO(id24, "nm2");

    CustomerOrderDTO o1 = new CustomerOrderDTO(id1, c1, p1);
    CustomerOrderDTO o2 = new CustomerOrderDTO(id2, c2, p2);
    CustomerOrderDTO o3 = new CustomerOrderDTO(id3, c3, p3);
    CustomerOrderDTO o4 = new CustomerOrderDTO(id2, c4, p3);
    CustomerOrderDTO o5 = new CustomerOrderDTO(id2, c2, p5);

    assertThat(o1.equals(o1)).isTrue();
    assertThat(o1.equals(o2)).isTrue();
    assertThat(o1.equals(o3)).isFalse();
    assertThat(o1.equals(o4)).isFalse();
    assertThat(o1.equals(o5)).isFalse();
    assertThat(o1.equals(null)).isFalse();
    assertThat(o1.equals(new Object())).isFalse();
  }

  @Test
  public void testCreate() {
    UUIDId id = new UUIDId(UUID.randomUUID());
    CustomerDTO cm = Mockito.mock(CustomerDTO.class);
    List<ProductDTO> p = Collections.emptyList();

    assertThat(new CustomerOrderDTO(id, cm, null).products).isNull();
    assertThat(new CustomerOrderDTO(id, cm, p).products).isNotSameAs(p);
    assertThat(new CustomerOrderDTO(id, cm, p).products).isEqualTo(p);
  }
}
