package com.automationanywhere.cognitive.dataresource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.io.InputStream;
import org.testng.annotations.Test;

public class DataContainerTest {
  @Test
  public void testEquals() {
    InputStream is1 = mock(InputStream.class);
    InputStream is2 = mock(InputStream.class);

    DataContainer dc1 = new DataContainer(is1, "mt", "nm", 1L);
    DataContainer dc2 = new DataContainer(is1, "mt", "nm", 1L);
    DataContainer dc3 = new DataContainer(is2, "mt", "nm", 1L);
    DataContainer dc4 = new DataContainer(is1, "mt0", "nm", 1L);
    DataContainer dc5 = new DataContainer(is1, "mt", "nm0", 1L);
    DataContainer dc6 = new DataContainer(is1, "mt", "nm", 2L);

    assertThat(dc1.equals(null)).isFalse();
    assertThat(dc1.equals(new Object())).isFalse();
    assertThat(dc1.equals(dc1)).isTrue();
    assertThat(dc1.equals(dc2)).isTrue();
    assertThat(dc1.equals(dc3)).isFalse();
    assertThat(dc1.equals(dc4)).isFalse();
    assertThat(dc1.equals(dc5)).isFalse();
    assertThat(dc1.equals(dc6)).isFalse();
  }

  @Test
  public void testHashCode() {
    InputStream is1 = mock(InputStream.class);
    InputStream is2 = mock(InputStream.class);

    DataContainer dc1 = new DataContainer(is1, "mt", "nm", 1L);
    DataContainer dc2 = new DataContainer(is1, "mt", "nm", 1L);
    DataContainer dc3 = new DataContainer(is2, "mt", "nm", 1L);

    assertThat(dc1.hashCode()).isEqualTo(dc2.hashCode());
    assertThat(dc1.hashCode()).isNotEqualTo(dc3.hashCode());
    assertThat(dc1.hashCode()).isNotEqualTo(new Object().hashCode());
  }
}
