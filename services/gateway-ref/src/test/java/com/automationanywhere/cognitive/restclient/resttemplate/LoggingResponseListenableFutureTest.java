package com.automationanywhere.cognitive.restclient.resttemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URI;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoggingResponseListenableFutureTest {

  private HttpRequest req;
  private LoggingResponseListenableFuture f;

  @BeforeMethod
  public void setUp() throws Exception {
    this.req = mock(HttpRequest.class);
    this.f = Mockito.spy(new LoggingResponseListenableFuture(this.req));
  }

  @Test
  public void testOnFailure() {
    Throwable ex = new Exception("test");

    this.f.onFailure(ex);

    Mockito.verify(this.f).log(ex);
  }

  @Test
  public void testOnSuccess() throws Exception {
    IOException ex = new IOException("test");

    ClientHttpResponse res = mock(ClientHttpResponse.class);

    doThrow(ex).when(res).getRawStatusCode();

    this.f.onSuccess(res);

    Mockito.verify(this.f).log(ex);
    Mockito.verify(this.f).log(any());
  }

  @Test
  public void testLogError() throws Exception {
    Throwable ex = new Exception("test");
    HttpMethod m = HttpMethod.GET;
    URI u = new URI("file:///");

    doReturn(m).when(this.req).getMethod();
    doReturn(u).when(this.req).getURI();

    this.f.log(ex);

    verify(this.req).getMethod();
    verify(this.req).getURI();
  }
}
