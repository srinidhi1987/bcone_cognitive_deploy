package com.automationanywhere.cognitive.app.filters;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.util.NestedServletException;
import org.testng.annotations.Test;

public class ErrorFilterTest {

  @Test
  public void testFilter() throws Exception {
    ServletRequest req = mock(HttpServletRequest.class);
    ServletResponse resp = mock(HttpServletResponse.class);
    FilterChain c = mock(FilterChain.class);

    ErrorFilter f = new ErrorFilter();
    f.doFilter(req, resp, c);

    verify(c).doFilter(req, resp);
  }

  @Test
  public void testFilterFailure() throws Exception {
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    FilterChain c = mock(FilterChain.class);

    IOException e = new IOException("Test");
    doThrow(e).when(c).doFilter(req, resp);

    ErrorFilter f = new ErrorFilter();
    f.doFilter(req, resp, c);

    verify(resp).sendError(500, e.getClass().getName() + ": Test");
  }

  @Test
  public void testFilterCustomFailure() throws Exception {
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    FilterChain c = mock(FilterChain.class);

    RestClientResponseException e = new RestClientResponseException("errortest", 412, "st", null,
        new byte[]{}, StandardCharsets.UTF_8);
    doThrow(e).when(c).doFilter(req, resp);

    ErrorFilter f = new ErrorFilter();
    f.doFilter(req, resp, c);

    verify(resp).sendError(412, e.getClass().getName() + ": errortest");
  }

  @Test
  public void testFilterNestedFailure() throws Exception {
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    FilterChain c = mock(FilterChain.class);

    Exception e0 = new Exception("testerror");
    NestedServletException e = new NestedServletException("errortest", e0);
    doThrow(e).when(c).doFilter(req, resp);

    ErrorFilter f = new ErrorFilter();
    f.doFilter(req, resp, c);

    verify(resp).sendError(500, e0.getClass().getName() + ": testerror");
  }
}
