package com.automationanywhere.cognitive;

import com.automationanywhere.cognitive.validation.SchemaValidation;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BadControllerPostProcessorTestBean {

  @SchemaValidation
  public void pub(final Object p1) {
  }
}
