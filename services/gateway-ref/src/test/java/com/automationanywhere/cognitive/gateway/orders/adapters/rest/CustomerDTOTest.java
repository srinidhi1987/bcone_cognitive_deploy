package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.UUID;
import org.testng.annotations.Test;

public class CustomerDTOTest {

  @Test
  public void testEquals() {
    UUID uuid1 = UUID.randomUUID();
    UUIDId id1 = new UUIDId(uuid1);
    UUIDId id2 = new UUIDId(uuid1);
    UUIDId id3 = new UUIDId(UUID.randomUUID());

    CustomerDTO c1 = new CustomerDTO(id1, "n1");
    CustomerDTO c2 = new CustomerDTO(id2, "n1");
    CustomerDTO c3 = new CustomerDTO(id3, "n1");
    CustomerDTO c4 = new CustomerDTO(id2, "n2");
    CustomerDTO c5 = new CustomerDTO(id3, "n1");

    assertThat(c1.equals(c1)).isTrue();
    assertThat(c1.equals(c2)).isTrue();
    assertThat(c1.equals(c3)).isFalse();
    assertThat(c1.equals(c4)).isFalse();
    assertThat(c1.equals(c5)).isFalse();
    assertThat(c1.equals(null)).isFalse();
    assertThat(c1.equals(new Object())).isFalse();
  }
}
