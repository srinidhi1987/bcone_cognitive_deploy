package com.automationanywhere.cognitive.gateway.orders.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.id.uuid.UUIDId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.mockito.Mockito;
import org.testng.annotations.Test;

public class CustomerAccountOrderDTOTest {

  @Test
  public void testEquals() throws InterruptedException {
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();
    UUIDId id1 = new UUIDId(uuid1);
    UUIDId id2 = new UUIDId(uuid1);
    UUIDId id3 = new UUIDId(UUID.randomUUID());

    UUIDId id21 = new UUIDId(uuid2);
    UUIDId id22 = new UUIDId(uuid2);
    UUIDId id23 = new UUIDId(UUID.randomUUID());
    UUIDId id24 = new UUIDId(UUID.randomUUID());

    List<ProductDTO> p1 = Arrays.asList(
        new ProductDTO("a", 1L),
        new ProductDTO("b", 2L)
    );
    List<ProductDTO> p2 = Arrays.asList(
        new ProductDTO("a", 1L),
        new ProductDTO("b", 2L)
    );
    List<ProductDTO> p3 = Arrays.asList(
        new ProductDTO("aa", 1L),
        new ProductDTO("bb", 2L)
    );
    List<ProductDTO> p5 = Arrays.asList(
        new ProductDTO("kk", 11L),
        new ProductDTO("ll", 22L)
    );
    CustomerAccountDTO c1 = new CustomerAccountDTO(id21, "nm", "1");
    CustomerAccountDTO c2 = new CustomerAccountDTO(id22, "nm", "1");
    CustomerAccountDTO c3 = new CustomerAccountDTO(id23, "nm", "1");
    CustomerAccountDTO c4 = new CustomerAccountDTO(id24, "nm", "2");

    Date d = new Date();
    Thread.sleep(1);
    Date d2 = new Date();

    CustomerAccountOrderDTO o1 = new CustomerAccountOrderDTO(id1, d, c1, p1);
    CustomerAccountOrderDTO o2 = new CustomerAccountOrderDTO(id2, d, c2, p2);
    CustomerAccountOrderDTO o3 = new CustomerAccountOrderDTO(id3, d, c3, p3);
    CustomerAccountOrderDTO o4 = new CustomerAccountOrderDTO(id2, d, c4, p3);
    CustomerAccountOrderDTO o5 = new CustomerAccountOrderDTO(id2, d, c2, p5);
    CustomerAccountOrderDTO o6 = new CustomerAccountOrderDTO(id2, d2, c2, p2);

    assertThat(o1.equals(o1)).isTrue();
    assertThat(o1.equals(o2)).isTrue();
    assertThat(o1.equals(o3)).isFalse();
    assertThat(o1.equals(o4)).isFalse();
    assertThat(o1.equals(o5)).isFalse();
    assertThat(o1.equals(o6)).isFalse();
    assertThat(o1.equals(null)).isFalse();
    assertThat(o1.equals(new Object())).isFalse();
  }

  @Test
  public void testCreate() {
    UUIDId id = new UUIDId(UUID.randomUUID());
    CustomerAccountDTO cm = Mockito.mock(CustomerAccountDTO.class);
    List<ProductDTO> p = Collections.emptyList();

    Date d = new Date();
    assertThat(new CustomerAccountOrderDTO(id, d, cm, null).products).isNull();
    assertThat(new CustomerAccountOrderDTO(id, d, cm, p).products).isNotSameAs(p);
    assertThat(new CustomerAccountOrderDTO(id, d, cm, p).products).isEqualTo(p);
  }
}
