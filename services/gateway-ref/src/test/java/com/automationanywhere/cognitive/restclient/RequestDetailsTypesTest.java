package com.automationanywhere.cognitive.restclient;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class RequestDetailsTypesTest {

  @Test
  public void testValues() {
    assertThat(RequestDetails.Types.values()).containsExactlyInAnyOrder(
        RequestDetails.Types.JSON, RequestDetails.Types.TEXT, RequestDetails.Types.ATTACHMENT
    );
  }

  @Test
  public void testValueOf() {
    assertThat(RequestDetails.Types.valueOf(RequestDetails.Types.JSON.name()))
        .isEqualTo(RequestDetails.Types.JSON);

    assertThat(RequestDetails.Types.valueOf(RequestDetails.Types.TEXT.name()))
        .isEqualTo(RequestDetails.Types.TEXT);

    assertThat(RequestDetails.Types.valueOf(RequestDetails.Types.ATTACHMENT.name()))
        .isEqualTo(RequestDetails.Types.ATTACHMENT);
  }
}
