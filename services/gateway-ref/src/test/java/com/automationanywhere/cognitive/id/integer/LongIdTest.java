package com.automationanywhere.cognitive.id.integer;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Objects;
import org.testng.annotations.Test;

public class LongIdTest {

  @Test
  public void testCreate() throws Exception {
    assertThat(new LongId(1).id).isEqualTo(1);
  }

  @Test
  public void testEquals() {
    long id = 1;
    LongId u1 = new LongId(id);
    LongId u2 = new LongId(id);
    LongId u3 = new LongId(2);

    assertThat(u1.equals(u1)).isTrue();
    assertThat(u1.equals(u2)).isTrue();
    assertThat(u1.equals(u3)).isFalse();
    assertThat(u1.equals(null)).isFalse();
    assertThat(u1.equals(new Object())).isFalse();
  }

  @Test
  public void testHashCode() {
    long id1 = 1;
    LongId sid1 = new LongId(id1);
    assertThat(sid1.hashCode()).isNotEqualTo(Objects.hashCode(id1));
    assertThat(sid1.hashCode()).isEqualTo(new LongId(id1).hashCode());
    assertThat(sid1.hashCode()).isNotEqualTo(new LongId(2).hashCode());
  }
}
