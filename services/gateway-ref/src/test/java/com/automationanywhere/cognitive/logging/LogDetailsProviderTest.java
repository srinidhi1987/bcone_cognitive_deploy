package com.automationanywhere.cognitive.logging;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.app.ApplicationConfiguration;
import com.automationanywhere.cognitive.restclient.CognitivePlatformHeaders;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogDetailsProviderTest {

  private ApplicationConfiguration ac;
  private LogDetailsProvider p;

  @BeforeMethod
  public void setUp() throws Exception {
    this.ac = mock(ApplicationConfiguration.class);
    this.p = Mockito.spy(new LogDetailsProvider(this.ac));
  }

  @Test
  public void testGetHostNameError() throws Exception {
    UnknownHostException e = new UnknownHostException();

    doThrow(e).when(this.p).getLocalHost();

    assertThat(this.p.getHostName()).isEqualTo("<unknown>");
  }

  @Test
  public void testExistingValue() {
    Map<String, String> ctx = new HashMap<>();

    doReturn("a").when(this.p).getValue(LogContextKeys.USER_ID);

    this.p.addTrackingInfo(null, ctx, LogContextKeys.USER_ID, CognitivePlatformHeaders.CLIENT_IP,
        null);

    assertThat(ctx.isEmpty()).isTrue();
  }

  @Test
  public void testRequestValue() {
    Map<String, String> ctx = new HashMap<>();

    HttpServletRequest req = mock(HttpServletRequest.class);

    doReturn("b").when(req).getHeader("x-forwarded-for");

    this.p.addTrackingInfo(req, ctx, LogContextKeys.USER_ID, CognitivePlatformHeaders.CLIENT_IP,
        null);

    Map<String, String> map = new HashMap<>();
    map.put("uid", "b");
    assertThat(ctx).isEqualTo(map);
  }
}
