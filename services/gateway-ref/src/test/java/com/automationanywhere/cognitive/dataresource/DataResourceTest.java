package com.automationanywhere.cognitive.dataresource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.io.InputStream;
import org.testng.annotations.Test;

public class DataResourceTest {
  @Test
  public void testDataSource() throws IOException {
    // given
    InputStream is = mock(InputStream.class);
    DataContainer dc = new DataContainer(is, "mt", "nm", 67L);
    DataResource dr = new DataResource(dc);

    InputStream is2 = mock(InputStream.class);
    DataContainer dc2 = new DataContainer(is2, "mt", "nm2", null);
    DataResource dr2 = new DataResource(dc2);

    // when
    assertThat(dr.getInputStream()).isSameAs(is);
    assertThat(dr.getFilename()).isEqualTo("nm");
    assertThat(dr.contentLength()).isEqualTo(67L);

    assertThat(dr2.getInputStream()).isSameAs(is2);
    assertThat(dr2.getFilename()).isEqualTo("nm2");
    assertThat(dr2.contentLength()).isEqualTo(-1);
  }
}
