package com.automationanywhere.cognitive.app.validation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import com.automationanywhere.cognitive.validation.ClasspathSchemaValidator;
import com.automationanywhere.cognitive.validation.SchemaValidation;
import java.lang.reflect.Method;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestBodySchemaValidationAspectTest {

  private RequestBodySchemaValidationAspect a;
  private ClasspathSchemaValidator v;

  @BeforeMethod
  public void setUp() throws Exception {
    this.v = mock(ClasspathSchemaValidator.class);
    this.a = new RequestBodySchemaValidationAspect(v, new ExceptionHandlerFactory());
  }

  @Test
  public void testHandleEmptyBody() {
    Object b = new Object();
    assertThat(this.a.handleEmptyBody(b, null, null, null, null)).isSameAs(b);
  }

  @Test
  public void testBeforeBodyReadWithNullAnnotation() throws Exception {
    HttpInputMessage im = mock(HttpInputMessage.class);
    MethodParameter p = mock(MethodParameter.class);

    Method m = NoAnnInputClass.class.getMethod("m", Object.class);

    doReturn(m).when(p).getMethod();

    assertThat(this.a.beforeBodyRead(im, p, null, null)).isSameAs(im);

    verify(this.v, never()).checkInput(any(), any());
  }

  @Test
  public void testBeforeBodyReadWithEmptyInput() throws Exception {
    HttpInputMessage im = mock(HttpInputMessage.class);
    MethodParameter p = mock(MethodParameter.class);

    Method m = EmptyInputClass.class.getMethod("m", Object.class);

    doReturn(m).when(p).getMethod();

    assertThat(this.a.beforeBodyRead(im, p, null, null)).isSameAs(im);

    verify(this.v, never()).checkInput(any(), any());
  }
}

class NoAnnInputClass {

  public void m(final Object i) {
  }

  ;
}

class EmptyInputClass {

  @SchemaValidation
  public void m(final Object i) {
  }

  ;
}
