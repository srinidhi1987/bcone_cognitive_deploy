package com.automationanywhere.cognitive.app.threadpool;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.fail;

import java.util.HashMap;
import org.testng.annotations.Test;

public class LogContextPropagatingTaskDecoratorTest {

  @Test
  public void testPropagate() {
    Runnable r = mock(Runnable.class);
    LogContextPropagatingTaskDecorator d = new LogContextPropagatingTaskDecorator();

    d.propagate(new HashMap<>(), r);

    verify(r).run();
  }

  @Test
  public void testPropagateFailure() {
    RuntimeException e = new RuntimeException();

    Runnable r = () -> {
      throw e;
    };
    LogContextPropagatingTaskDecorator d = new LogContextPropagatingTaskDecorator();

    try {
      d.propagate(new HashMap<>(), r);
      fail();
    } catch (final Exception ex) {
      assertThat(ex).isSameAs(e);
    }
  }
}
