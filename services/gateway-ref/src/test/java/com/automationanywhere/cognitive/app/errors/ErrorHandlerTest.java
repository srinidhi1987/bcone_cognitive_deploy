package com.automationanywhere.cognitive.app.errors;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.app.Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.ApplicationContextException;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;
import org.testng.annotations.Test;

public class ErrorHandlerTest {

  private final ObjectMapper om = new ObjectMapper();

  @Test
  public void testErrorHandler() throws Exception {
    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new Exception(), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(Exception.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new UnsupportedOperationException(new Exception()), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(UnsupportedOperationException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new SelfException(), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(SelfException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(
            new ApplicationContextException("e", new IllegalArgumentException()), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(IllegalArgumentException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new ExecutionException(new IllegalStateException()), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(IllegalStateException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new RestClientException("e", new RuntimeException()), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(RestClientException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new HttpServerErrorException(HttpStatus.FORBIDDEN), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(HttpServerErrorException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new NoSuchRequestHandlingMethodException("m", Object.class), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(NoSuchRequestHandlingMethodException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new HttpRequestMethodNotSupportedException("m"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(HttpRequestMethodNotSupportedException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new HttpMediaTypeNotSupportedException("m"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(HttpMediaTypeNotSupportedException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new HttpMediaTypeNotAcceptableException("m"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(HttpMediaTypeNotAcceptableException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new NoHandlerFoundException("m", "u", new HttpHeaders()), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(NoHandlerFoundException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new AsyncRequestTimeoutException(), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(AsyncRequestTimeoutException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new MissingServletRequestParameterException("p", "t"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(MissingServletRequestParameterException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new ServletRequestBindingException("m"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(ServletRequestBindingException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new TypeMismatchException("m", Object.class), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(TypeMismatchException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new HttpMessageNotReadableException("m"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(HttpMessageNotReadableException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new MissingServletRequestPartException("m"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(MissingServletRequestPartException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new BindException(new Object(), "n"), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(BindException.class.getName());

    assertThat(this.om.readValue((byte[]) (new ErrorHandler(this.om)
        .handleTopLevelException(new MethodArgumentNotValidException(new MethodParameter(
            Application.class.getMethod("main", String[].class), 0),
            new BindException(
              new Object(), "n"
            )
        ), null)
        .getBody()), DefaultErrorDTO.class).exception)
        .isEqualTo(MethodArgumentNotValidException.class.getName());
  }
}

class SelfException extends Exception {

  public SelfException() {
  }

  @Override
  public synchronized Throwable getCause() {
    return this;
  }
}
