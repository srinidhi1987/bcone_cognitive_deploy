package com.automationanywhere.cognitive.app.validation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.errors.ExceptionHandlerFactory;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CachingHttpInputMessageTest {

  private CachingHttpInputMessage m;
  private HttpInputMessage im;
  private HttpHeaders h;
  private ByteArrayInputStream is;

  @BeforeMethod
  public void setUp() throws Exception {
    this.h = new HttpHeaders();
    this.is = new ByteArrayInputStream("test".getBytes(StandardCharsets.UTF_8));
    this.im = mock(HttpInputMessage.class);

    doReturn(this.h).when(this.im).getHeaders();
    doReturn(this.is).when(this.im).getBody();

    this.m = new CachingHttpInputMessage(this.im, new ExceptionHandlerFactory());
  }

  @Test
  public void testGetHeaders() throws Exception {
    assertThat(this.m.getHeaders()).isSameAs(this.h);
  }

  @Test
  public void testGetBody() throws Exception {
    String c = "";
    try (
        Scanner s = new Scanner(this.m.getBody());
    ) {
      while (s.hasNext()) {
        c += s.next();
      }
    }

    assertThat(c).isEqualTo("test");
  }

  @Test
  public void testGetContents() throws Exception {
    assertThat(this.m.getContents()).isEqualTo("test");
  }
}

