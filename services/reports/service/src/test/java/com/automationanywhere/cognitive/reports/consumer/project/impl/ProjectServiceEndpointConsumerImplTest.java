package com.automationanywhere.cognitive.reports.consumer.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DependentServiceConnectionFailureException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProjectServiceEndpointConsumerImplTest {
    @Mock
    RestTemplateWrapper restTemplateWrapper;
    @Mock
    private RestTemplate restTemplate;
    
    @InjectMocks
    ProjectServiceEndpointConsumerImpl projectServiceConsumer;
    
    @BeforeTest
    public void setUp(){
        this.restTemplate = new RestTemplate();
        this.restTemplateWrapper = new RestTemplateWrapper(restTemplate);
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(new ObjectMapper());
        messageConverters.add(jsonMessageConverter);
        StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
        messageConverters.add(stringHttpMessageConerters);
        restTemplate.setMessageConverters(messageConverters);
        projectServiceConsumer = new ProjectServiceEndpointConsumerImpl("rootUrl");
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfTestConnectionWhenRestCallToAliasServiceReturnsOK(){
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        projectServiceConsumer.testConnection();
        
        //then
        //no exception
    }
    
    @Test(expectedExceptions={DependentServiceConnectionFailureException.class}, expectedExceptionsMessageRegExp="Error while connecting to Project service")
    public void testOfTestConnectionWhenRestCallToAliasServiceReturnsNotOK(){
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        projectServiceConsumer.testConnection();
        
        //then
        //exception thrown
    }
}
