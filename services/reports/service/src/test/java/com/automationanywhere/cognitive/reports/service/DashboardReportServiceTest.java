package com.automationanywhere.cognitive.reports.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.reports.consumer.project.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.reports.consumer.validator.contract.ValidatorConsumer;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.models.ProjectDetail;
import com.automationanywhere.cognitive.reports.service.contract.DashboardReportService;
import com.automationanywhere.cognitive.reports.service.impl.DashboardReportServiceImpl;
import java.util.ArrayList;
import java.util.HashMap;
import org.testng.annotations.Test;

public class DashboardReportServiceTest {

  @Test
  public void getDashboardAllProjectTotalsReturnsOneRecord() throws DataAccessLayerException {

    // GIVEN
    String orgId = "1";
    String prjId = "1";
    ProjectDetail projectDetail = new ProjectDetail();
    projectDetail.setId("1234");
    projectDetail.setName("Prj_detail_1234");

    ProjectServiceEndpointConsumer pseConsumer = mock(ProjectServiceEndpointConsumer.class);
    ValidatorConsumer validatorConsumer = mock(ValidatorConsumer.class);
    DashboardReportService service = new DashboardReportServiceImpl(
        null,
        null,
        pseConsumer,
        validatorConsumer,
        null
    );

    when(pseConsumer.getAllProjectDetails(orgId)).thenReturn(new ProjectDetail[]{projectDetail});

    // WHEN
    ArrayList<HashMap<String, Object>> reportData = service.getReportData(orgId, prjId);

    // THEN
    assertThat(reportData).isNotEmpty().hasSize(1);
    assertThat(reportData.get(0)).containsOnly(
        entry("projectType", null),
        entry("description", null),
        entry("projectState", null),
        entry("organizationId", "1"),
        entry("numberOfFiles", 0),
        entry("environment", null),
        entry("currentTrainedPercentage", 0.0f),
        entry("name", "Prj_detail_1234"),
        entry("totalFilesProcessed", 0),
        entry("totalAccuracy", 0.0f),
        entry("id", "1234"),
        entry("totalSTP", 0.0),
        entry("primaryLanguage", null)
    );
  }
}
