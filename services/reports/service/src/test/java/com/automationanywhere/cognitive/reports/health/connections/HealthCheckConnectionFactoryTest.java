package com.automationanywhere.cognitive.reports.health.connections;

import static org.junit.Assert.assertThat;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.InstanceOf;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author mitul.maheshwari 
 * 	This class will validate the type of healthcheck
 *  connection return by HealthCheckConnectionFactory based on parameter
 *  type.
 *
 */
public class HealthCheckConnectionFactoryTest {

	@InjectMocks
	HealthCheckConnectionFactory healthCheckConnectionFactory;

	@Mock
	private DBCheckConnection dbCheckConnection;

	@Mock
	private ServiceCheckConnection svcCheckConnection;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetConnectionDBType() {

		HealthCheckConnection expectedConType = healthCheckConnectionFactory.getConnection(ConnectionType.DB);
		assertThat(expectedConType, new InstanceOf(DBCheckConnection.class));
	}

	@Test
	public void testGetConnectionSVCType() {

		HealthCheckConnection expectedConType = healthCheckConnectionFactory.getConnection(ConnectionType.SVC);
		assertThat(expectedConType, new InstanceOf(ServiceCheckConnection.class));
	}
}
