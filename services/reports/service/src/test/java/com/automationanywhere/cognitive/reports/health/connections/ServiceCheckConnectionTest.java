package com.automationanywhere.cognitive.reports.health.connections;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.reports.consumer.alias.contract.AliasServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.filemanger.contract.FileServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.project.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.reports.consumer.validator.contract.ValidatorConsumer;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DependentServiceConnectionFailureException;

public class ServiceCheckConnectionTest {

    @Mock
    AliasServiceConsumer aliasServiceConsumer;
    @Mock
    FileServiceConsumer fileServiceConsumer;
    @Mock
    ProjectServiceEndpointConsumer projectServiceEndpointConsumer;
    @Mock
    ValidatorConsumer validatorConsumer;
    
    @BeforeMethod
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfCheckConnectionForServiceWhenAllServiceConnectionSuccess(){
        
        //given
        List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity aliasConn = new ServiceConnectivity("Alias", HTTPStatusCode.OK);
        ServiceConnectivity projConn = new ServiceConnectivity("Project", HTTPStatusCode.OK);
        ServiceConnectivity fileConn = new ServiceConnectivity("Filemanager", HTTPStatusCode.OK);
        ServiceConnectivity validatorConn = new ServiceConnectivity("Validator", HTTPStatusCode.OK);
        servicesListExpected.add(aliasConn);
        servicesListExpected.add(fileConn);
        servicesListExpected.add(projConn);
        servicesListExpected.add(validatorConn);
        ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(aliasServiceConsumer, fileServiceConsumer, projectServiceEndpointConsumer, validatorConsumer);
        Mockito.doNothing().when(aliasServiceConsumer).testConnection();
        Mockito.doNothing().when(projectServiceEndpointConsumer).testConnection();
        Mockito.doNothing().when(fileServiceConsumer).testConnection();
        Mockito.doNothing().when(validatorConsumer).testConnection();
        
        //when
        List<ServiceConnectivity> servicesList =  serviceCheckConnection.checkConnectionForService();
        
        int index = 0;
        // then
        for(ServiceConnectivity serviceConnectivity:servicesList){
            ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
            assertThat(serviceConnectivity.getHTTPStatus()).isEqualTo(expectedServiceConnectivity.getHTTPStatus());
            assertThat(serviceConnectivity.getServiceName()).isEqualTo(expectedServiceConnectivity.getServiceName());
            index++;
        }
        
    }
    
    @Test
    public void testOfCheckConnectionForServiceWhenAllServiceConnectionFails(){
        
        //given
        List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity aliasConn = new ServiceConnectivity("Alias", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        ServiceConnectivity projConn = new ServiceConnectivity("Project", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        ServiceConnectivity fileConn = new ServiceConnectivity("Filemanager", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        ServiceConnectivity validatorConn = new ServiceConnectivity("Validator", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        servicesListExpected.add(aliasConn);
        servicesListExpected.add(fileConn);
        servicesListExpected.add(projConn);
        servicesListExpected.add(validatorConn);
        ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(aliasServiceConsumer, fileServiceConsumer, projectServiceEndpointConsumer, validatorConsumer);
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(aliasServiceConsumer).testConnection();
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(projectServiceEndpointConsumer).testConnection();
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(fileServiceConsumer).testConnection();
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(validatorConsumer).testConnection();
        
        //when
        List<ServiceConnectivity> servicesList =  serviceCheckConnection.checkConnectionForService();
        
        int index = 0;
        // then
        for(ServiceConnectivity serviceConnectivity:servicesList){
            ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
            assertThat(serviceConnectivity.getHTTPStatus()).isEqualTo(expectedServiceConnectivity.getHTTPStatus());
            assertThat(serviceConnectivity.getServiceName()).isEqualTo(expectedServiceConnectivity.getServiceName());
            index++;
        }
        
    }
}
