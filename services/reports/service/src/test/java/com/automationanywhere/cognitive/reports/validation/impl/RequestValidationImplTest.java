package com.automationanywhere.cognitive.reports.validation.impl;

import com.automationanywhere.cognitive.reports.models.ProjectDetail;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RequestValidationImplTest {
    private RequestValidationImpl impl;

    @Test
    public void testOfValidationShouldCreateAnEmptyProjectDetailObject() throws Exception {
        // given
        impl = new RequestValidationImpl();
        String orgId = "org1";
        String projectId = "pr1";

        // when
        ProjectDetail projectDetail = impl.validateProjectId(orgId, projectId);

        // then
        assertThat(projectDetail).isNotNull();
    }
}