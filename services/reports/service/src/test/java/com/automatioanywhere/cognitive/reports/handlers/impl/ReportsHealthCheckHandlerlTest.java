package com.automatioanywhere.cognitive.reports.handlers.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.reports.health.connections.ConnectionType;
import com.automationanywhere.cognitive.reports.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.reports.health.connections.HealthCheckConnectionFactory;
import com.automationanywhere.cognitive.reports.health.handlers.impl.ReportsHealthCheckHandler;

public class ReportsHealthCheckHandlerlTest {

    @Mock
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    @InjectMocks
    ReportsHealthCheckHandler reportsHealthCheckHandler;

    @BeforeMethod
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOfCheckHealthWhenDBConnectionFails() {
        //given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class))).thenReturn(healthCheckConnection);
        Mockito.doThrow(DBConnectionFailureException.class).when(healthCheckConnection).checkConnection();
        
        //when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        //then
        assertThat(healthcheck).contains("Database Connectivity: FAILURE");
    }

    @Test
    public void testOfCheckHealthWhenDBConnectionSuccess() {
        //given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class))).thenReturn(healthCheckConnection);
        Mockito.doNothing().when(healthCheckConnection).checkConnection();
        
        //when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        //then
        assertThat(healthcheck).contains("Database Connectivity: OK");
  
    }

    @Test
    public void testOfCheckHealthWhenAliasServiceConnectionFails() {
        
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("ALIAS", HTTPStatusCode.OK);
        dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("ALIAS: INTERNAL_SERVER_ERROR");
    }

    @Test
    public void testOfCheckHealthWhenAliasServiceConnectionSuccess() {
        
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("ALIAS", HTTPStatusCode.OK);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("ALIAS: OK");
    }

    @Test
    public void testOfCheckHealthWhenProjectServiceConnectionSuccess() {
     // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("PROJECT", HTTPStatusCode.OK);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("PROJECT: OK");
    }

    @Test
    public void testOfCheckHealthWhenProjectServiceConnectionFails() {
       // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("PROJECT", HTTPStatusCode.OK);
        dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("PROJECT: INTERNAL_SERVER_ERROR");
    }

    @Test
    public void testOfCheckHealthWhenFileManagerServiceConnectionFails() {
     // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("FILEMANAGER", HTTPStatusCode.OK);
        dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
        
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("FILEMANAGER: INTERNAL_SERVER_ERROR");
    }

    @Test
    public void testOfCheckHealthWhenFileManagerServiceConnectionSuccess() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("FILEMANAGER", HTTPStatusCode.OK);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("FILEMANAGER: OK");
    }

    @Test
    public void testOfCheckHealthWhenValidatorConnectionSuccess() {
       // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("VALIDATOR", HTTPStatusCode.OK);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("VALIDATOR: OK");
    }

    @Test
    public void testOfCheckHealthWhenValidatorConnectionFails() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("VALIDATOR", HTTPStatusCode.OK);
        dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = reportsHealthCheckHandler.checkHealth();
        
        // then
        assertThat(healthcheck).contains("VALIDATOR: INTERNAL_SERVER_ERROR");
    }
}
