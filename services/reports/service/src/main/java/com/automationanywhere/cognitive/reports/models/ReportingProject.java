package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class ReportingProject {
    String id;
    String organizationId;
    String name;
    String description;
    String environmnet;
    String primaryLanguage;
    String projectType;
    String projectState;
    long totalFieldsProcessed;
    long totalSTP;
    long numberOfFiles;
    long totalAccuracy;
    long currentTrainedPercentage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnvironmnet() {
        return environmnet;
    }

    public void setEnvironmnet(String environmnet) {
        this.environmnet = environmnet;
    }

    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectState() {
        return projectState;
    }

    public void setProjectState(String projectState) {
        this.projectState = projectState;
    }

    public long getTotalFieldsProcessed() {
        return totalFieldsProcessed;
    }

    public void setTotalFieldsProcessed(long totalFieldsProcessed) {
        this.totalFieldsProcessed = totalFieldsProcessed;
    }

    public long getTotalSTP() {
        return totalSTP;
    }

    public void setTotalSTP(long totalSTP) {
        this.totalSTP = totalSTP;
    }

    public long getNumberOfFiles() {
        return numberOfFiles;
    }

    public void setNumberOfFiles(long numberOfFiles) {
        this.numberOfFiles = numberOfFiles;
    }

    public long getTotalAccuracy() {
        return totalAccuracy;
    }

    public void setTotalAccuracy(long totalAccuracy) {
        this.totalAccuracy = totalAccuracy;
    }

    public long getCurrentTrainedPercentage() {
        return currentTrainedPercentage;
    }

    public void setCurrentTrainedPercentage(long currentTrainedPercentage) {
        this.currentTrainedPercentage = currentTrainedPercentage;
    }
}
