package com.automationanywhere.cognitive.reports.consumer.project.contract;

import com.automationanywhere.cognitive.reports.consumer.project.model.ProjectDetailMetaData;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.models.ProjectDetail;

/**
 * Created by Mayur.Panchal on 20-02-2017.
 */
public interface ProjectServiceEndpointConsumer {
    ProjectDetailMetaData getProjectDetails(String orgId, String prjId) throws DataAccessLayerException;
    void testConnection();
    ProjectDetail[] getAllProjectDetails(String orgId) throws DataAccessLayerException;
}
