package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class ClassificationField {
    private String name;
    private long number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }
}
