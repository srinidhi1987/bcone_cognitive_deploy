package com.automationanywhere.cognitive.reports.handlers.impl;

import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.reports.handlers.contract.AbstractHandler;
import com.automationanywhere.cognitive.reports.handlers.contract.ReportHandler;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.reports.models.CustomResponse;
import com.automationanywhere.cognitive.reports.service.contract.DashboardReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class ReportHandlerImpl extends AbstractHandler implements ReportHandler {

    private AALogger log = AALogger.create(getClass());

    @Autowired
    @Qualifier("dashboardService")
    private DashboardReportService dashboardService;

    @Override
    public String GetDashboardProduction(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
    	String orgId = request.params(":orgid");
    	log.entry();
        HashMap<String,Object> dashboardAllProjectTotals = dashboardService.getDashboardAllProjectTotals(orgId);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setData(dashboardAllProjectTotals);
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String GetDashboardProjectWise(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        String orgId = request.params(":orgid"),projectId = request.params(":projectId");
        log.debug(" orgid: " + orgId + "projectId :" + projectId);
        HashMap<String,Object> projectTotalsWithAccuracyDetails = dashboardService.getProjectTotal(orgId,projectId);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setData(projectTotalsWithAccuracyDetails);
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String GetDashboardAllProjects(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        CustomResponse customResponse = new CustomResponse();
        String orgId = request.params(":orgid"),prjId = request.params(":prjid");
        log.debug(" orgid: " + orgId + "projectId :" + prjId);
        ArrayList<HashMap<String, Object>> reportData = dashboardService.getReportData(orgId,prjId);
        customResponse.setData(reportData);
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }
}
