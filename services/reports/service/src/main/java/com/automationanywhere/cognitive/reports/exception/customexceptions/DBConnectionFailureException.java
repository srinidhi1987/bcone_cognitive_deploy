package com.automationanywhere.cognitive.reports.exception.customexceptions;

public class DBConnectionFailureException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4331028232272202788L;

	public DBConnectionFailureException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
