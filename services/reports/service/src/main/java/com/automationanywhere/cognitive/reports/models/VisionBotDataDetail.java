package com.automationanywhere.cognitive.reports.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.automationanywhere.cognitive.reports.models.entitymodel.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 07-02-2017.
 */
@Entity
@Table(name="VisionBotDocuments")
public class VisionBotDataDetail extends EntityModelBase {

    @Id
    @Column(name = "fileId")
    @JsonProperty(value = "fileId")
    private String fileId;
    @JsonProperty(value = "orgId")
    @Column(name = "orgId")
    private String orgId;
    @JsonProperty(value = "projectId")
    @Column(name = "projectId")
    private String projectId;
    @JsonProperty(value = "vBotDocument")
    @Column(name = "vBotDocument")
    private String vBotDocument;
    @JsonProperty(value = "validationStatus")
    @Column(name = "ValidationStatus")
    private String validationStatus;
    @JsonProperty(value = "correctedData")
    @Column(name = "CorrectedData")
    private String correctedData;
    @JsonProperty(value = "createdAt")
    @Column(name = "CreatedAt")
    private Date createdAt;
    @JsonProperty(value = "updatedBy")
    @Column(name = "UpdatedBy")
    private String updatedBy;
    @JsonProperty(value = "updatedAt")
    @Column(name = "UpdatedAt")
    private Date updatedAt;
    /*@JsonProperty(value = "vbotInUse")
    @Column(name = "VBotInUse")
    private boolean vbotInUse;*/
    @JsonProperty(value = "visionBotId")
    @Column(name = "VisionBotId")
    private String visionBotId;
    @JsonProperty(value = "lockedUserId")
    @Column(name = "LockedUserid")
    private String lockedUserId;
    @JsonProperty(value = "failedFieldCounts")
    @Column(name = "FailedFieldCounts")
    private float FailedFieldCounts=0;
    @JsonProperty(value = "passFieldCounts")
    @Column(name = "PassFieldCounts")
    private float PassFieldCounts=0;
    @JsonProperty(value = "totalFieldCounts")
    @Column(name = "TotalFieldCounts")
    private float TotalFieldCounts =0 ;
    @JsonProperty(value = "startTime")
    @Column(name = "StartTime")
    private Date StartTime;
    @JsonProperty(value = "endTime")
    @Column(name = "EndTime")
    private Date EndTime;
    @JsonProperty(value = "averageTimeInSeconds")
    @Column(name = "AverageTimeInSeconds")
    private long AverageTimeInSeconds=0;
    @Column(name = "lastExportTime")
    @JsonProperty(value = "lastExportTime")
    private Date lastExportTime;


    public String getOrgId()
    {
        return this.orgId;
    }
    public void setOrgId(String orgid)
    {
        this.orgId = orgid;
    }

    public long getAverageTimeInSeconds() {
        return AverageTimeInSeconds;
    }

    public void setAverageTimeInSeconds(long averageTimeInSeconds) {
        AverageTimeInSeconds = averageTimeInSeconds;
    }

    public void setVBotData(String json)
    {
        this.vBotDocument = json;
    }
    public String getVBotData()
    {
        return this.vBotDocument;
    }

    public String getFileId()
    {
        return this.fileId;
    }
    public void setFileId(String id)
    {
        this.fileId = id;
    }

    public String getVisionBotId()
    {
        return this.visionBotId;
    }
    public void setVisionBotId(String visionbotid)
    {
        this.visionBotId = visionbotid;
    }

    public String getProjectId() { return this.projectId; }

    public Date getStartTime() {
        return StartTime;
    }

    public void setStartTime(Date startTime) {
        StartTime = startTime;
    }

    public Date getEndTime() {
        return EndTime;
    }

    public void setEndTime(Date endTime) {
        EndTime = endTime;
    }

    public void setProjectId(String projectId) { this.projectId = projectId; }

    public String getValidationStatus() { return this.validationStatus;}
    public void setValidationStatus(String status) { this.validationStatus = status; }

    public String getCorrectedData() { return this.correctedData; }
    public void setCorrectedData(String correctedData) { this.correctedData = correctedData; }

    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }
    public Date getCreatedAt()
    {
        return this.createdAt;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    public String getUpdatedBy()
    {
        return this.updatedBy;
    }

    public void setUpdatedAt(Date updatedAt)
    {
        this.updatedAt = updatedAt;
    }
    public Date getUpdatedAt()
    {
        return this.updatedAt;
    }

 /*   public void setVBotInUse(boolean vbotInUse)
    {
        this.vbotInUse = vbotInUse;
    }
    public boolean getVBotInUse()
    {
        return this.vbotInUse;
    }*/

    public void setLockedUserId(String lockeduserid)
    {
        this.lockedUserId = lockeduserid;
    }
    public String getLockedUserId()
    {
        return this.lockedUserId;
    }

    public float getFailedFieldCounts()
    {
        return this.FailedFieldCounts;
    }
    public void setFailedFieldCounts(float failedFieldCounts)
    {
        this.FailedFieldCounts = failedFieldCounts;
    }

    public float getPassFieldCounts() { return this.PassFieldCounts; }
    public void setPassFieldCounts(float passFieldCounts) { this.PassFieldCounts = passFieldCounts; }

    public float getTotalFieldCounts() { return this.TotalFieldCounts; }
    public void setTotalFieldCounts(float totalFieldCounts) { this.TotalFieldCounts = totalFieldCounts; }

    public Date getLastExportTime() {
        return lastExportTime;
    }

    public void setLastExportTime(Date lastExportTime) {
        this.lastExportTime = lastExportTime;
    }
}
