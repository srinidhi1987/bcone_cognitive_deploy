package com.automationanywhere.cognitive.reports.models;

import java.sql.Timestamp;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class GroupTimeSpent {
    private String name;
    Timestamp number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getNumber() {
        return number;
    }

    public void setNumber(Timestamp number) {
        this.number = number;
    }
}
