package com.automationanywhere.cognitive.reports;

import com.automationanywhere.cognitive.common.inetaddress.wrapper.InetAddressWrapper;
import com.automationanywhere.cognitive.common.logger.AALogger;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static spark.Spark.secure;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class Main {

    private static String jdbcUrl;
    private static String username;
    private static String jdbcString = "jdbc:sqlserver://";
    private static String servicePort;
    private static String fileManagerRootUrl;
    private static String projectServiceRootUrl;
    private static String validatorServiceRootUrl;
    private static String aliasServiceRootUrl;
    private static boolean windowsAuth;

    private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations\\CognitivePlatform.jks";
    private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations\\Cognitive.properties";
    private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
    private static final String PROPERTIES_NOTFOUND = "NotFound";
    private static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final int DATASOURCE_INITIAL_SIZE = 3;
    private static final String HTTP_SCHEME = "http";
    private static final String HTTPS_SCHEME = "https";
    private static final String FILENAME_COGNITIVE_SETTINGS = "configurations\\Settings.txt";
    private static final String PROPERTIES_SQL_WINAUTH = "SQLWindowsAuthentication";
    private static String settingsFilePath;

    private static AALogger logger = AALogger.create(Main.class);

    public static void main(String[] args) throws IOException {
        if (args.length > 8) {

            jdbcUrl = jdbcString + args[0] + ":" + args[1];
            jdbcUrl += ";databaseName=" + args[2];
            username = args[3];
            servicePort = args[4];
            loadSettings();
            if (windowsAuth) {
                jdbcUrl += ";integratedSecurity=true";
            }

            String fullyQualifiedName = InetAddressWrapper.getFullyQualifiedName();

            if (isSSLCertificateConfigured()) {
                projectServiceRootUrl = String.format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, args[5]);
                fileManagerRootUrl = String.format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, args[6]);
                validatorServiceRootUrl = String.format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, args[7]);
                aliasServiceRootUrl = String.format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, args[8]);
            } else {
                projectServiceRootUrl = String.format("%s://%s:%s", HTTP_SCHEME, fullyQualifiedName, args[5]);
                fileManagerRootUrl = String.format("%s://%s:%s", HTTP_SCHEME, fullyQualifiedName, args[6]);
                validatorServiceRootUrl = String.format("%s://%s:%s", HTTP_SCHEME, fullyQualifiedName, args[7]);
                aliasServiceRootUrl = String.format("%s://%s:%s", HTTP_SCHEME, fullyQualifiedName, args[8]);
            }

        } else {
            System.out.println(
                    "9 arguments necessary to run jar file <ipaddress> <databasename> <username> <servicePort> "
                            + "<project_servicePort> <filemanager_servicePort> <validator_servicePort> <alias_servicePort>");
            return;
        }
        setServiceSecurity();
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
    }

    public static String getServicePort() {
        return servicePort;
    }

    public static String getFileManagerRootUrl() {
        return fileManagerRootUrl;
    }

    public static String getProjectServiceRootUrl() {
        return projectServiceRootUrl;
    }

    public static String getValidatorServiceRootUrl() {
        return validatorServiceRootUrl;
    }

    public static String getAliasServiceRootUrl() {
        return aliasServiceRootUrl;
    }

    private static void setServiceSecurity() {
        if (isSSLCertificateConfigured()) {
            try {
                String certificateKey = loadSSLCertificatePassword();
                if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
                	Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
                    String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
                    secure(cognitiveCertificateFilePath, certificateKey, null, null);

//                    applyHttpsSchemeToExternalServiceUrl();
                }

            } catch (final IOException ex) {
                logger.error("Failed to set service security", ex);
            }
        }
    }

    private static String loadSSLCertificatePassword() throws IOException {
    	Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitivePropertiesFilePath = propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
        if (fileExists(cognitivePropertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitivePropertiesFilePath));
            String certificateKey = properties.getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
            return certificateKey;
        } else {
        	return PROPERTIES_NOTFOUND;
        }
    }

    private static boolean isSSLCertificateConfigured() {
        boolean hasCertificate = false;
        Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
        if (fileExists(cognitiveCertificateFilePath)) {
            hasCertificate = true;
        }
        return hasCertificate;
    }

	private static boolean fileExists(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}

    public static BasicDataSource createDataSource(String password) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(SQL_SERVER_DRIVER);
        dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
        dataSource.setUrl(jdbcUrl);
        if (!windowsAuth) {
            dataSource.setUsername(username);
            dataSource.setPassword(password);
        }
        return dataSource;
    }

    private static void loadSettings() throws IOException {
        Path configurationDirectoryPath  = Paths.get(System.getProperty("user.dir")).getParent();
        settingsFilePath = configurationDirectoryPath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
        Properties properties = loadProperties(settingsFilePath);
        if(properties.containsKey(PROPERTIES_SQL_WINAUTH))
        {
            windowsAuth = Boolean.parseBoolean(properties.getProperty(PROPERTIES_SQL_WINAUTH));
        }
    }

    private static Properties loadProperties(String filePath) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filePath));
        return properties;
    }
}
