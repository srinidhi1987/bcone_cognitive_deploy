package com.automationanywhere.cognitive.reports.consumer.project.model;

import java.util.Date;
import java.util.List;

import com.automationanywhere.cognitive.reports.models.CategoryDetail;
import com.automationanywhere.cognitive.reports.models.Environment;
import com.automationanywhere.cognitive.reports.models.ProjectState;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Mayur.Panchal on 20-02-2017.
 */
public class ProjectDetailMetaData
{
    private String id;

    private String name;

    private String description;

    private String organizationId;

    private String projectType;

    private String projectTypeId;

    private String primaryLanguage;

    private ProjectState projectState;

    private Environment environment;

    @JsonIgnore private String fileUploadToken;

    @JsonIgnore private boolean fileUploadTokenAlive;

    @JsonIgnore private String createdBy;

    @JsonIgnore private String lastUpdatedBy;

    @JsonIgnore private Date createdAt;

    @JsonIgnore private Date updatedAt;

    private Fields fields;

    private int numberOfFiles;
    private int numberOfCategories;
    private int unprocessedFileCount;

    private float accuracyPercentage;

    private int visionBotCount;

    private float currentTrainedPercentage;

    private List<CategoryDetail> categories;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectType() {
        return this.projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectTypeId() {
        return projectTypeId;
    }

    public void setProjectTypeId(String projectTypeId) {
        this.projectTypeId = projectTypeId;
    }

    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public ProjectState getProjectState() {
        return projectState;
    }

    public void setProjectState(ProjectState state) {
        this.projectState = state;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getFileUploadToken() {
        return fileUploadToken;
    }

    public void setFileUploadToken(String fileUploadToken) {
        this.fileUploadToken = fileUploadToken;
    }

    public boolean isFileUploadTokenAlive() {
        return fileUploadTokenAlive;
    }

    public void setFileUploadTokenAlive(boolean fileUploadTokenAlive) {
        this.fileUploadTokenAlive = fileUploadTokenAlive;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fieldDetailList) {
        this.fields = fieldDetailList;
    }
    public void setNumberOfFiles(int numberOfFiles){ this.numberOfFiles = numberOfFiles;}
    public int getNumberOfFiles() { return this.numberOfFiles; }

    public void setNumberOfCategories(int numberOfCategories){ this.numberOfCategories = numberOfCategories;}
    public int getNumberOfCategories() { return this.numberOfCategories; }


    public void setAccuracyPercentage(float accuracyPercentage){ this.accuracyPercentage = accuracyPercentage;}
    public float getAccuracyPercentage() { return this.accuracyPercentage; }

    public void setVisionBotCount(int visionBotCount){ this.visionBotCount = visionBotCount;}
    public int getVisionBotCount() { return this.visionBotCount; }

    public void setCurrentTrainedPercentage(float currentTrainedPercentage){ this.currentTrainedPercentage = currentTrainedPercentage;}
    public float getCurrentTrainedPercentage() { return this.currentTrainedPercentage; }

    public void setUnprocessedFileCount(int unprocessedFileCount){ this.unprocessedFileCount = unprocessedFileCount;}
    public int getUnprocessedFileCount() { return this.unprocessedFileCount; }

    public List<CategoryDetail> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDetail> categories) {
        this.categories = categories;
    }
}