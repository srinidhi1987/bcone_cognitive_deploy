package com.automationanywhere.cognitive.reports.exception.customexceptions;

public class DependentServiceConnectionFailureException extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8214396996269673489L;

	public DependentServiceConnectionFailureException() {
        super();
    }

    public DependentServiceConnectionFailureException(String message) {
        super(message);
    }

}
