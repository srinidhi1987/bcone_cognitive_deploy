package com.automationanywhere.cognitive.reports.consumer.project.impl;

import static com.automationanywhere.cognitive.reports.constants.HeaderConstants.CORRELATION_ID;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.reports.consumer.project.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.reports.consumer.project.model.ProjectDetailMetaData;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.reports.models.CustomResponse;
import com.automationanywhere.cognitive.reports.models.ProjectDetail;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Mayur.Panchal on 20-02-2017.
 */
public class ProjectServiceEndpointConsumerImpl implements ProjectServiceEndpointConsumer {
    private String rootUrl;
    private RestTemplate restTemplate;
    AALogger aaLogger = AALogger.create(this.getClass());
    @Autowired
    private RestTemplateWrapper restTemplateWrapper;
    private String HEARTBEAT_URL = "/heartbeat";
    public ProjectServiceEndpointConsumerImpl(String rooturl)
    {
        this.restTemplate = new RestTemplate();
        this.rootUrl = rooturl;
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(new ObjectMapper());
        messageConverters.add(jsonMessageConverter);
        StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
        messageConverters.add(stringHttpMessageConerters);
        restTemplate.setMessageConverters(messageConverters);
    }

    @Override
    public ProjectDetailMetaData getProjectDetails(String orgId, String prjId) throws DataAccessLayerException {
    	aaLogger.traceEntry(" orgId :" + orgId + " prjId : " + prjId);
        String relativeUrl = "organizations/%s/projects/%s/metadata";
        String url = rootUrl +String.format(relativeUrl,orgId,prjId);
        ProjectDetailMetaData projectDetail = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpHeaders headers = new HttpHeaders();
		headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<CustomResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, CustomResponse.class);
		projectDetail = objectMapper.convertValue(standardResponse.getBody().getData(), ProjectDetailMetaData.class);
        aaLogger.exit();
        return projectDetail;
    }
    @Override
    public ProjectDetail[] getAllProjectDetails(String orgId) throws DataAccessLayerException {
    	aaLogger.traceEntry(" orgId :" + orgId );
        String relativeUrl = "organizations/%s/projects";
        String url = rootUrl +String.format(relativeUrl,orgId);
        ProjectDetail[] projectDetail = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpHeaders headers = new HttpHeaders();
		headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<CustomResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, CustomResponse.class);
		projectDetail = objectMapper.convertValue(standardResponse.getBody().getData(), ProjectDetail[].class);
        aaLogger.exit();
        return projectDetail;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        aaLogger.entry();
        String heartBeatURL = rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse = null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL,
                String.class);
        if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
           throw new DependentServiceConnectionFailureException(
                    "Error while connecting to Project service");
        }
        aaLogger.exit();
    }
}
