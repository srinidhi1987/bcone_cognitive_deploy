package com.automationanywhere.cognitive.reports.models;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class ProjectTotals {
    long totalFilesProcessed;
    long totalSTP;
    long totalAccuracy;
    int totalFilesUploaded;
    int totalFilesToValidate;
    int tatalFilesValidated;
    int totalFilesInprocessable;




    public long getTotalFilesProcessed() {
        return totalFilesProcessed;
    }

    public void setTotalFilesProcessed(long totalFilesProcessed) {
        this.totalFilesProcessed = totalFilesProcessed;
    }

    public long getTotalSTP() {
        return totalSTP;
    }

    public void setTotalSTP(long totalSTP) {
        this.totalSTP = totalSTP;
    }

    public long getTotalAccuracy() {
        return totalAccuracy;
    }

    public void setTotalAccuracy(long totalAccuracy) {
        this.totalAccuracy = totalAccuracy;
    }

    public int getTotalFilesUploaded() {
        return totalFilesUploaded;
    }

    public void setTotalFilesUploaded(int totalFilesUploaded) {
        this.totalFilesUploaded = totalFilesUploaded;
    }

    public int getTotalFilesToValidate() {
        return totalFilesToValidate;
    }

    public void setTotalFilesToValidate(int totalFilesToValidate) {
        this.totalFilesToValidate = totalFilesToValidate;
    }

    public int getTatalFilesValidated() {
        return tatalFilesValidated;
    }

    public void setTatalFilesValidated(int tatalFilesValidated) {
        this.tatalFilesValidated = tatalFilesValidated;
    }

    public int getTotalFilesInprocessable() {
        return totalFilesInprocessable;
    }

    public void setTotalFilesInprocessable(int totalFilesInprocessable) {
        this.totalFilesInprocessable = totalFilesInprocessable;
    }

    HashMap<String, HashMap<String,List<ClassificationField>>> classification;
    public HashMap<String, HashMap<String,List<ClassificationField>>> getClassificationField() {
        return classification;
    }

    public void setClassificationField(HashMap<String, HashMap<String,List<ClassificationField>>> classification) {
        this.classification = classification;
    }
    HashMap<String,HashMap<String,Object>>  accuracy;
    public HashMap<String,HashMap<String,Object>> getFieldAccuracy() {
        return accuracy;
    }

    public void setFieldAccuracy(HashMap<String,HashMap<String,Object>> accuracy) {
        this.accuracy = accuracy;
    }
    HashMap<String, HashMap<String,Object>> validation;
    public HashMap<String, HashMap<String,Object>> getFieldValidation() {
        return validation;
    }

    public void setFieldValidation(HashMap<String, HashMap<String,Object>> validation) {
        this.validation = validation;
    }

}
