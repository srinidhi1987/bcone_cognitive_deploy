package com.automationanywhere.cognitive.reports.models.entitymodel;

import com.automationanywhere.cognitive.common.security.sqlinjection.SqlInjectionChecker;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashMap;
import java.util.Map;
import org.springframework.util.CollectionUtils;

/**
 * Created by Mayur.Panchal on 11-12-2016.
 */
public abstract class EntityModelBase {

    @JsonIgnore
    public Map<String, MapValueParameter> WhereParam;
    @JsonIgnore
    public Map<String, Object> SetParam;

    @JsonIgnore
    public EntityModelBase() {
        WhereParam = new HashMap<>();
        SetParam = new HashMap<>();
    }

    public Map<String, Object> getUpdateQuery(StringBuilder query) {
        query.setLength(0);
        Map<String, Object> inputParam = null;

        // Adds SET clause
        if (!CollectionUtils.isEmpty(SetParam)) {
            inputParam = new HashMap<>();
            query.append("UPDATE ").append(this.getClass().getSimpleName()).append(" SET ");
            for (Map.Entry<String, Object> entry : SetParam.entrySet()) {
                String key = entry.getKey();
                SqlInjectionChecker.checkColumnName(key);
                query.append(" ").append(key).append("= :").append(key).append(", ");
                inputParam.put(key, entry.getValue());
            }
            query.setLength(query.length() - 2);

            // Adds WHERE clause
            if (!CollectionUtils.isEmpty(WhereParam)) {
                appendWhereClause(query, inputParam);
            }
        }
        return inputParam;
    }

    public Map<String, Object> getSelectQuery(StringBuilder query) {
        query.setLength(0);
        Map<String, Object> params = new HashMap<>();
        query.append(" FROM  ").append(this.getClass().getSimpleName());
        if (!CollectionUtils.isEmpty(WhereParam)) {
            appendWhereClause(query, params);
        }
        return params;
    }

    public Map<String, Object> getDeleteQuery(StringBuilder query) {
        query.setLength(0);
        Map<String, Object> params = null;
        if (!CollectionUtils.isEmpty(WhereParam)) {
            params = new HashMap<>();
            query.append("DELETE FROM  ").append(this.getClass().getSimpleName());
            appendWhereClause(query, params);
        }
        return params;
    }

    private void appendWhereClause(StringBuilder query, Map<String, Object> params) {
        query.append(" WHERE ");
        for (Map.Entry<String, MapValueParameter> entry : WhereParam.entrySet()) {
            String key = entry.getKey();
            SqlInjectionChecker.checkColumnName(key);
            query.append(" ").append(key).append(entry.getValue().getOperator());
            query.append(" :").append(key).append(" AND ");
            params.put(key, entry.getValue().getValue());
        }
        query.setLength(query.length() - 4);
    }
}
