package com.automationanywhere.cognitive.reports.validation.contract;

import com.automationanywhere.cognitive.reports.models.ProjectDetail;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface RequestValidation {
    ProjectDetail validateProjectId(String orgId, String projectId);
    boolean validateRequest(Request request, Response response);
}
