package com.automationanywhere.cognitive.reports.exception.customexceptions;

public class UnSupportedConnectionTypeException extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = -7637664933196822948L;

	public UnSupportedConnectionTypeException(String message) {
        super(message);
    }

}
