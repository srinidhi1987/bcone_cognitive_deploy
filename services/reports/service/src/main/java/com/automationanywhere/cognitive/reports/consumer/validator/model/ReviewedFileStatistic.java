package com.automationanywhere.cognitive.reports.consumer.validator.model;

/**
 * Created by Mayur.Panchal on 22-03-2017.
 */
public class ReviewedFileStatistic {
     String projectId;
    long processedCount;
    long failedFileCount;
    long reviewFileCount;
    long invalidFileCount;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public long getProcessedCount() {
        return processedCount;
    }

    public void setProcessedCount(long processedCount) {
        this.processedCount = processedCount;
    }

    public long getFailedFileCount() {
        return failedFileCount;
    }

    public void setFailedFileCount(long failedFileCount) {
        this.failedFileCount = failedFileCount;
    }

    public long getReviewFileCount() {
        return reviewFileCount;
    }

    public void setReviewFileCount(long reviewFileCount) {
        this.reviewFileCount = reviewFileCount;
    }

    public long getInvalidFileCount() {
        return invalidFileCount;
    }

    public void setInvalidFileCount(long invalidFileCount) {
        this.invalidFileCount = invalidFileCount;
    }
}
