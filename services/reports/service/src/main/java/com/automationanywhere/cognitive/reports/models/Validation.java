package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class Validation {
    FieldValidation[] fieldValidations;
    GroupTimeSpent groupTimeSpent;

    public FieldValidation[] getFieldValidations() {
        return fieldValidations;
    }

    public void setFieldValidations(FieldValidation[] fieldValidations) {
        this.fieldValidations = fieldValidations;
    }

    public GroupTimeSpent getGroupTimeSpent() {
        return groupTimeSpent;
    }

    public void setGroupTimeSpent(GroupTimeSpent groupTimeSpent) {
        this.groupTimeSpent = groupTimeSpent;
    }
}
