package com.automationanywhere.cognitive.reports.consumer.alias.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by Purnil.Soni on 05-12-2016.
 */
public class StorageFieldItem {
    public final String id;
    public final String name;
    public final String description;
    public final String fieldType;
    public final List<String> aliases;
    public final boolean isDefaultSelected;
    public final String dataType;

    public StorageFieldItem(
        @JsonProperty("id") final String id,
        @JsonProperty("name") final String name,
        @JsonProperty("description") final String description,
        @JsonProperty("fieldType") final String fieldType,
        @JsonProperty("aliases") final List<String> aliases,
        @JsonProperty("isDefaultSelected") final boolean isDefaultSelected,
        @JsonProperty("dataType") final String dataType
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.fieldType = fieldType;
        this.aliases = aliases;
        this.isDefaultSelected = isDefaultSelected;
        this.dataType = dataType;
    }
}