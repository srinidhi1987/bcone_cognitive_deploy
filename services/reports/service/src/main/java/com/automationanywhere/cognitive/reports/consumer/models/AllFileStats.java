package com.automationanywhere.cognitive.reports.consumer.models;

/**
 * Created by Mayur.Panchal on 22-03-2017.
 */
public class AllFileStats {
    long totalFilesProcessed;
    long totalFilesCount;

    public long getTotalFilesProcessed() {
        return totalFilesProcessed;
    }

    public void setTotalFilesProcessed(long totalFilesProcessed) {
        this.totalFilesProcessed = totalFilesProcessed;
    }

    public long getTotalFilesCount() {
        return totalFilesCount;
    }

    public void setTotalFilesCount(long totalFilesCount) {
        this.totalFilesCount = totalFilesCount;
    }
}
