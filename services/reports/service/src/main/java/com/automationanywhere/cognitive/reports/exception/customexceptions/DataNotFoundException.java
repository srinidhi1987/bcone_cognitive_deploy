package com.automationanywhere.cognitive.reports.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class DataNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3591390947060954860L;
}
