package com.automationanywhere.cognitive.reports.validation.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
//import com.automationanywhere.cognitive.reports.logging.Constants;
import com.automationanywhere.cognitive.reports.models.ProjectDetail;
import com.automationanywhere.cognitive.reports.validation.contract.RequestValidation;
        import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class RequestValidationImpl implements RequestValidation
{
    AALogger log = AALogger.create(getClass());

    /*@Autowired
    protected ProjectServiceEndpointConsumer projectService;*/
    @Override
    public boolean validateRequest(Request request, Response response)
    {
        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        log.traceEntry( " orgId : " + orgId + "prjid: " + projectID);
        validateProjectId(orgId, projectID);
        // We do not check the result since the current implemention always returns a not null object
        // That is caught by SonarQube, so if we add a check here
        // if (validateProjectId(orgId, projectID) != null)
        // SonarQube will complain.
        String fileId = request.params(":fileid");
        /*if (fileId != null)
        {
            List<FileDetails> list = fileService.getSpecificFile(fileId);
            if(list == null || list.isEmpty())
            {
                halt(404,"Data request.");
            }
        }*/
        String categoryId = request.params(":id");
        String layoutId = request.params(":layoutid");
        log.exit();
        return true;

    }

    // Todo: This is horrible...
    // After implementing the actual validation
    // remember to check the result after calling the method in validateRequest
    @Override
    public ProjectDetail validateProjectId(String orgId, String projectId) {
        log.traceEntry( " orgId : " + orgId + "prjid: " + projectId);
        ProjectDetail projectDetail = new ProjectDetail();
        log.exit();
        return projectDetail;
    }
}
