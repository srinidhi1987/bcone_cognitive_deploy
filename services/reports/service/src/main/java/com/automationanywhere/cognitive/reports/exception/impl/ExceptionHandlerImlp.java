package com.automationanywhere.cognitive.reports.exception.impl;

import com.automationanywhere.cognitive.reports.exception.contract.ExceptionHandler;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.reports.models.CustomResponse;
import com.automationanywhere.cognitive.reports.parser.contract.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class ExceptionHandlerImlp implements ExceptionHandler
{
    AALogger log = AALogger.create(getClass());

    @Autowired
    JsonParser jsonParser;
    @Override
    public void handleDataAccessLayerException(Exception dataAccessLayerException, Request request, Response response)
    {
        log.error(dataAccessLayerException.getMessage(), dataAccessLayerException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        customResponse.setErrors("Internal Server Error.");
        response.status(500);
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleDataNotFoundException(Exception dataNotFoundException, Request request, Response response)
    {
        log.error(dataNotFoundException.getMessage(), dataNotFoundException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(404);
        customResponse.setErrors("Data is not available");
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleException(Exception exception, Request request, Response response)
    {
        log.error(exception.getMessage(), exception);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(500);
        customResponse.setErrors("Internal Server Error.");
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }
}
