package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Jemin.Shah on 29-11-2016.
 */
public enum ProjectState
{
    training,
    classified,
    inDesign,
    complete
}
