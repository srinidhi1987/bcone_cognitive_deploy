package com.automationanywhere.cognitive.reports.handlers.contract;

import com.automationanywhere.cognitive.reports.parser.contract.JsonParser;
import com.automationanywhere.cognitive.reports.validation.contract.RequestValidation;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Mayur.Panchal on 09-12-2016.
 */
public abstract class AbstractHandler {
    @Autowired
    protected RequestValidation requestValidation;
    @Autowired
    protected JsonParser jsonParser;


}
