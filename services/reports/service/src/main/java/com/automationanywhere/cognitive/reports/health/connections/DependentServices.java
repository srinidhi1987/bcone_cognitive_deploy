package com.automationanywhere.cognitive.reports.health.connections;

/**
 * @author shweta.thakur
 * This class will hold Dependent micro-services list
 */
public enum DependentServices {
    Alias, Filemanager, Project, Validator
}
