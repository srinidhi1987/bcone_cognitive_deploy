package com.automationanywhere.cognitive.reports.parser.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.reports.models.CustomResponse;
import com.automationanywhere.cognitive.reports.parser.contract.JsonParser;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class JsonParserImpl implements JsonParser
{
    private AALogger logger = AALogger.create(getClass());

    protected ObjectMapper jsonObjectMapper = new ObjectMapper();
    
	@Override
    public String getResponse(CustomResponse customResponse)
    {
        logger.entry();
        try
        {
            jsonObjectMapper.setVisibility(jsonObjectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                    .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
            jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            jsonObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            jsonObjectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);
        }
        catch (JsonProcessingException e)
        {
            logger.error(e.getMessage(), e);
            return "";
        }
        finally
        {
           logger.exit();
        }
    }
}
