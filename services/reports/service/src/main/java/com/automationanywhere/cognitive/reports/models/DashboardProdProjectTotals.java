package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class DashboardProdProjectTotals {
    private long totalFilesProcessed;
    private long totalSTP;
    private long totalAccuracy;
    private long totalFilesCount;

    public long getTotalFilesProcessed() {
        return totalFilesProcessed;
    }

    public void setTotalFilesProcessed(long totalFilesProcessed) {
        this.totalFilesProcessed = totalFilesProcessed;
    }

    public long getTotalSTP() {
        return totalSTP;
    }

    public void setTotalSTP(long totalSTP) {
        this.totalSTP = totalSTP;
    }

    public long getTotalAccuracy() {
        return totalAccuracy;
    }

    public void setTotalAccuracy(long totalAccuracy) {
        this.totalAccuracy = totalAccuracy;
    }

    public long getTotalFilesCount() {
        return totalFilesCount;
    }

    public void setTotalFilesCount(long totalFilesCount) {
        this.totalFilesCount = totalFilesCount;
    }
}
