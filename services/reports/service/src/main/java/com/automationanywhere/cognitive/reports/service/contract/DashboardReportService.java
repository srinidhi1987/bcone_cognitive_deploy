package com.automationanywhere.cognitive.reports.service.contract;

import java.util.ArrayList;
import java.util.HashMap;

import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public interface DashboardReportService {
    HashMap<String,Object> getDashboardAllProjectTotals(String orgId) throws DataAccessLayerException;
    HashMap<String,Object> getProjectTotal(String orgId, String projectId) throws DataAccessLayerException;
    ArrayList<HashMap<String, Object>> getReportData(String orgId, String projectId) throws DataAccessLayerException;
}

