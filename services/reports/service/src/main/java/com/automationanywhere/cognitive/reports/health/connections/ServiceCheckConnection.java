package com.automationanywhere.cognitive.reports.health.connections;

import java.util.ArrayList;
import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.reports.consumer.alias.contract.AliasServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.filemanger.contract.FileServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.project.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.reports.consumer.validator.contract.ValidatorConsumer;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DBConnectionFailureException;

public class ServiceCheckConnection implements HealthCheckConnection{

    AliasServiceConsumer aliasServiceConsumer;
    FileServiceConsumer fileServiceConsumer;
    ProjectServiceEndpointConsumer projectServiceEndpointConsumer;
    ValidatorConsumer validatorConsumer;
    AALogger aaLogger = AALogger.create(this.getClass());
    public ServiceCheckConnection(AliasServiceConsumer aliasServiceConsumer, FileServiceConsumer fileServiceConsumer,
                                  ProjectServiceEndpointConsumer projectServiceEndpointConsumer,
                                  ValidatorConsumer validatorConsumer) {
        super();
        this.aliasServiceConsumer = aliasServiceConsumer;
        this.fileServiceConsumer = fileServiceConsumer; 
        this.projectServiceEndpointConsumer = projectServiceEndpointConsumer;
        this.validatorConsumer = validatorConsumer;
    }

    @Override
    public void checkConnection() throws DBConnectionFailureException {
        
    }

    /* 
     * This method will check the connection to dependent microservices by checking the 
     * Heartbeat api response
     * @return list of services and their connection status
     */
    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        aaLogger.entry();
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = null;
        for (DependentServices de : DependentServices.values()) {
            try {
                dependentService = new ServiceConnectivity(de.toString(), HTTPStatusCode.OK);
                switch (de) {
                    case Alias: aliasServiceConsumer.testConnection();
                        break;
                    case Project: projectServiceEndpointConsumer.testConnection();
                        break;
                    case Filemanager: fileServiceConsumer.testConnection();
                        break;
                    case Validator: validatorConsumer.testConnection();
                        break;
                }
            } catch (Exception ex) {
                prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.INTERNAL_SERVER_ERROR);
            }
            if (!dependentServices.contains(dependentService)) {
                prepareDependentServiceList(dependentServices, dependentService, de,HTTPStatusCode.OK);
            }
        }
        return dependentServices;
    }
    
    private void prepareDependentServiceList(
            List<ServiceConnectivity> dependentServices,
            ServiceConnectivity dependentService, DependentServices de, HTTPStatusCode mode) {
        dependentService.setHTTPStatus(mode);
        dependentServices.add(dependentService);
    }
}
