package com.automationanywhere.cognitive.reports.consumer.validator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class FieldValidation {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty(value = "fieldId")
    private String id;

    @JsonProperty(value = "percentValidated")
    private double percentValidated;



    public double getPercentValidated() {
        return percentValidated;
    }

    public void setPercentValidated(double percentValidated) {
        this.percentValidated = percentValidated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty(value = "name")
    private String name;
}