package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class FieldAccuracy {
    private String name;
    private long accuracyPercentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAccuracyPercentage() {
        return accuracyPercentage;
    }

    public void setAccuracyPercentage(long accuracyPercentage) {
        this.accuracyPercentage = accuracyPercentage;
    }
}
