package com.automationanywhere.cognitive.reports.datalayer.contract;

import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;

import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 11-12-2016.
 */
public interface CommonDataAccessLayer<T,C> {
    boolean InsertRow(T entityModel) throws DataAccessLayerException;
    boolean UpdateRow(T entityModel) throws DataAccessLayerException;
    List<Object> SelectRows(T queryDetail) throws DataAccessLayerException;
    boolean DeleteRows(T queryDetail) throws DataAccessLayerException;
    List<Object> ExecuteCustomEntityQuery(C queryDetail) throws DataAccessLayerException;
    List<Object> CustomQueryExecute(StringBuilder query, Map<String, Object> inputParam)throws DataAccessLayerException;
}
