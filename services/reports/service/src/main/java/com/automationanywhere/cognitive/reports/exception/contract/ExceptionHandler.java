package com.automationanywhere.cognitive.reports.exception.contract;

import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface ExceptionHandler {
    void handleDataAccessLayerException(Exception dataAccessLayerException, Request request, Response response);
    void handleDataNotFoundException(Exception dataNotFoundException, Request request, Response response);
    void handleException(Exception dataNotFoundException, Request request, Response response);
}
