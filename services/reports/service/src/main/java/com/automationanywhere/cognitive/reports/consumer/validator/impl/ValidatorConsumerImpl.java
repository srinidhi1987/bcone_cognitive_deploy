package com.automationanywhere.cognitive.reports.consumer.validator.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.reports.consumer.validator.contract.ValidatorConsumer;
import com.automationanywhere.cognitive.reports.consumer.validator.model.ProjectTotalsWithAccuracyDetails;
import com.automationanywhere.cognitive.reports.consumer.validator.model.ReviewedFileStatistic;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.reports.models.CustomResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.cognitive.reports.constants.HeaderConstants.CORRELATION_ID;

/**
 * Created by Mayur.Panchal on 21-03-2017.
 */
public class ValidatorConsumerImpl implements ValidatorConsumer {
    private String rootUrl;
    private RestTemplate restTemplate;
    MultiValueMap<String, String> headerparams;
    AALogger aaLogger = AALogger.create(this.getClass());
    @Autowired
    private RestTemplateWrapper restTemplateWrapper;
    private String HEARTBEAT_URL = "/heartbeat";
    public ValidatorConsumerImpl(String rootUrl) {
        this.restTemplate = new RestTemplate();
        headerparams = new LinkedMultiValueMap<String, String>();
        headerparams.add("username", "reportingservice");
        this.rootUrl = rootUrl;
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(new ObjectMapper());
        messageConverters.add(jsonMessageConverter);
        StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
        messageConverters.add(stringHttpMessageConerters);
        restTemplate.setMessageConverters(messageConverters);
    }

    @Override
    public ReviewedFileStatistic[] getReviewedfilecount(String orgId) throws DataAccessLayerException {
    	aaLogger.traceEntry(" orgId: " + orgId);
        String relativeUrl = "organizations/%s/validator/reviewedfilecount";
        String url = rootUrl +String.format(relativeUrl,orgId);
        ReviewedFileStatistic[] reviewedFileStatistics = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpHeaders headers = new HttpHeaders();
		headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<CustomResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, CustomResponse.class);
		reviewedFileStatistics = objectMapper.convertValue(standardResponse.getBody().getData(), ReviewedFileStatistic[].class);
        aaLogger.exit();
        return reviewedFileStatistics;
    }

    @Override
    public ProjectTotalsWithAccuracyDetails getPerformanceSummary(String orgId, String projectId) throws DataAccessLayerException {
    	aaLogger.traceEntry(" orgId: " + orgId + " projectId: " + projectId);
        String relativeUrl = "organizations/%s/validator/dashboard/project/%s";
        String url = rootUrl +String.format(relativeUrl,orgId,projectId);
        ProjectTotalsWithAccuracyDetails projectTotalsWithAccuracyDetails = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpHeaders headers = new HttpHeaders();
		headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<CustomResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, CustomResponse.class);
		projectTotalsWithAccuracyDetails = objectMapper.convertValue(standardResponse.getBody().getData(), ProjectTotalsWithAccuracyDetails.class);
        aaLogger.exit();
        return projectTotalsWithAccuracyDetails;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        aaLogger.entry();
        String heartBeatURL = rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse = null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL,
                String.class);
        if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
            throw new DependentServiceConnectionFailureException(
                    "Error while connecting to Validator service");
        }
        aaLogger.exit();
    }
}
