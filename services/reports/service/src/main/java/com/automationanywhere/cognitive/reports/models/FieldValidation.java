package com.automationanywhere.cognitive.reports.models;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class FieldValidation {
    private String name;
    private long percentValidated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPercentValidated() {
        return percentValidated;
    }

    public void setPercentValidated(long percentValidated) {
        this.percentValidated = percentValidated;
    }
}
