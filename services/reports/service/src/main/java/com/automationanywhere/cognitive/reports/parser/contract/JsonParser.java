package com.automationanywhere.cognitive.reports.parser.contract;

import com.automationanywhere.cognitive.reports.models.CustomResponse;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface JsonParser {
    String getResponse(CustomResponse customResponse);
}
