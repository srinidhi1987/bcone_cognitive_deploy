package com.automationanywhere.cognitive.reports.health.connections;

import org.springframework.beans.factory.annotation.Autowired;
import com.automationanywhere.cognitive.reports.exception.customexceptions.UnSupportedConnectionTypeException;

public class HealthCheckConnectionFactory {
    @Autowired
    private DBCheckConnection dbCheckConnection;
    @Autowired
    private ServiceCheckConnection svcCheckConnection;
    
	public HealthCheckConnection getConnection(ConnectionType connType){
		switch(connType){
			case DB : return dbCheckConnection;
			case SVC: return svcCheckConnection;
			default: throw new UnSupportedConnectionTypeException(connType  +  " is not supported");
		}
	}
}
