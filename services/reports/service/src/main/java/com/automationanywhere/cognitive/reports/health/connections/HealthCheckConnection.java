package com.automationanywhere.cognitive.reports.health.connections;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DBConnectionFailureException;

import java.util.List;;

public interface HealthCheckConnection {
	void checkConnection() throws DBConnectionFailureException;
	List<ServiceConnectivity> checkConnectionForService() ;
}
