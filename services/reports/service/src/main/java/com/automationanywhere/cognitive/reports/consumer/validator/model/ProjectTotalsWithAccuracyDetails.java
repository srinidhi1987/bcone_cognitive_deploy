package com.automationanywhere.cognitive.reports.consumer.validator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class ProjectTotalsWithAccuracyDetails {

    @JsonProperty(value = "totalFilesProcessed")
    private int totalFilesProcessed;

    @JsonProperty(value = "totalSTP")
    private double totalSTP;

    @JsonProperty(value = "totalAccuracy")
    private double totalAccuracy;

    @JsonProperty(value = "totalFilesUploaded")
    private int totalFilesUploaded;

    @JsonProperty(value = "totalFilesToValidation")
    private int totalFilesToValidation;

    @JsonProperty(value = "totalFilesValidated")
    private int totalFilesValidated;

    @JsonProperty(value = "totalFilesUnprocessable")
    private int totalFilesUnprocessable;

    @JsonProperty(value = "accuracy")
    private List<FieldAccuracyDashboardDetails> fieldAccuracy;

    @JsonProperty(value = "validation")
    private List<FieldValidation> fieldValidation;

    @JsonProperty(value = "categories")
    private List<GroupTimeSpentDetails> groupTimeSpentDetails;
    @JsonProperty(value = "classification")
    private List<ClassificationFieldDetails> classificationFieldDetails = new ArrayList<>();

    public int getTotalFilesProcessed() {
        return totalFilesProcessed;
    }

    public void setTotalFilesProcessed(int totalFilesProcessed) {
        this.totalFilesProcessed = totalFilesProcessed;
    }

    public double getTotalSTP() {
        return totalSTP;
    }

    public void setTotalSTP(double totalSTP) {
        this.totalSTP = totalSTP;
    }

    public double getTotalAccuracy() {
        return totalAccuracy;
    }

    public void setTotalAccuracy(double totalAccuracy) {
        this.totalAccuracy = totalAccuracy;
    }

    public int getTotalFilesUploaded() {
        return totalFilesUploaded;
    }

    public void setTotalFilesUploaded(int totalFilesUploaded) {
        this.totalFilesUploaded = totalFilesUploaded;
    }

    public int getTotalFilesToValidation() {
        return totalFilesToValidation;
    }

    public void setTotalFilesToValidation(int totalFilesToValidation) {
        this.totalFilesToValidation = totalFilesToValidation;
    }

    public int getTotalFilesValidated() {
        return totalFilesValidated;
    }

    public void setTotalFilesValidated(int totalFilesValidated) {
        this.totalFilesValidated = totalFilesValidated;
    }

    public int getTotalFilesUnprocessable() {
        return totalFilesUnprocessable;
    }

    public void setTotalFilesUnprocessable(int totalFilesUnprocessable) {
        this.totalFilesUnprocessable = totalFilesUnprocessable;
    }

    public List<FieldAccuracyDashboardDetails> getFieldAccuracy() {
        return fieldAccuracy;
    }

    public void setFieldAccuracy(List<FieldAccuracyDashboardDetails> fieldAccuracy) {
        this.fieldAccuracy = fieldAccuracy;
    }

    public List<FieldValidation> getFieldValidation() {
        return fieldValidation;
    }

    public void setFieldValidation(List<FieldValidation> fieldValidation) {
        this.fieldValidation = fieldValidation;
    }

    public List<GroupTimeSpentDetails> getGroupTimeSpentDetails() {
        return groupTimeSpentDetails;
    }

    public void setGroupTimeSpentDetails(List<GroupTimeSpentDetails> groupTimeSpentDetails) {
        this.groupTimeSpentDetails = groupTimeSpentDetails;
    }

    public List<ClassificationFieldDetails> getClassificationFieldDetails() {
        return classificationFieldDetails;
    }

    public void setClassificationFieldDetails(List<ClassificationFieldDetails> classificationFieldDetails) {
        this.classificationFieldDetails = classificationFieldDetails;
    }
}