package com.automationanywhere.cognitive.reports.consumer.validator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class FieldAccuracyDashboardDetails {

    @JsonProperty(value = "fieldId")
    private String fieldId;

    @JsonProperty(value = "accuracyPercent")
    private double percentFieldAccuracy;

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public double getPercentFieldAccuracy() {
        return percentFieldAccuracy;
    }

    public void setPercentFieldAccuracy(double percentFieldAccuracy) {
        this.percentFieldAccuracy = percentFieldAccuracy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "name")
    private String name;
}