package com.automationanywhere.cognitive.reports.health.handlers.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.health.handlers.AbstractHealthHandler;
import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.reports.health.connections.ConnectionType;
import com.automationanywhere.cognitive.reports.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.reports.health.connections.HealthCheckConnectionFactory;


public class ReportsHealthCheckHandler implements AbstractHealthHandler {

    @Autowired
    HealthCheckConnectionFactory healthCheckConnectionFactory;

    @Override
    public String checkHealth(){
        HealthCheckConnection healthCheckConnection;
        Connectivity dbConnectionType = Connectivity.OK;
        List<ServiceConnectivity> serviceConnectivity = null;
        for (ConnectionType connectionType : ConnectionType.values()) {
            healthCheckConnection = healthCheckConnectionFactory.getConnection(connectionType);
            try {
                healthCheckConnection.checkConnection();
            } catch (DBConnectionFailureException dcfe) {
                dbConnectionType = Connectivity.FAILURE;
            }
            if(connectionType.equals(ConnectionType.SVC)){
                serviceConnectivity = healthCheckConnection.checkConnectionForService();
            }
        }
        return HealthApiResponseBuilder.prepareSubSystem(HTTPStatusCode.OK,
                SystemStatus.ONLINE, dbConnectionType, Connectivity.NOT_APPLICABLE,
                Connectivity.NOT_APPLICABLE,
                serviceConnectivity);
    }

}
