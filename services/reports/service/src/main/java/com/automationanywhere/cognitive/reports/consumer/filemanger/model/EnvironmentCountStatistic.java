package com.automationanywhere.cognitive.reports.consumer.filemanger.model;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class EnvironmentCountStatistic
{
    private int unprocessedCount;
    private int totalCount;

    public int getUnprocessedCount() {
        return unprocessedCount;
    }

    public void setUnprocessedCount(int unprocessedCount) {
        this.unprocessedCount = unprocessedCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
