package com.automationanywhere.cognitive.reports.consumer.filemanger.contract;

import com.automationanywhere.cognitive.reports.consumer.filemanger.model.CountStatistic;

/**
 * Created by Mayur.Panchal on 21-03-2017.
 */
public interface FileServiceConsumer {
    CountStatistic[] getAllFileStats(String orgId);
    void testConnection();
}
