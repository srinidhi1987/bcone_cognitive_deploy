package com.automationanywhere.cognitive.reports.consumer.validator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class AccuracyProcessingDetails {

    @JsonProperty(value = "id")
    private String id;

    @JsonProperty(value = "failedFieldCount")
    private double failedFieldCounts;

    @JsonProperty(value = "passedFieldCount")
    private double passFieldCounts;

    @JsonProperty(value = "totalFieldCount")
    private double totalFieldCounts;

    @JsonProperty(value = "fieldAccuracy")
    private double fieldAccuracy;

    @JsonProperty(value = "reviewFileCount")
    private long reviewFileCount;

    @JsonProperty(value = "invalidFileCount")
    private long invalidFileCount;

    @JsonProperty(value = "failedDocumentCount")
    private long failedDocumentCount = 0;

    @JsonProperty(value = "passedDocumentCount")
    private long passedDocumentCounts;

    @JsonProperty(value = "processedDocumentCount")
    private long processedDocumentCounts;

    @JsonProperty(value = "documentAccuracy")
    private long documentAccuracy;

    @JsonProperty(value = "averageReviewTimeInSeconds")
    private long averageReviewTimeInSeconds = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getFailedFieldCounts() {
        return failedFieldCounts;
    }

    public void setFailedFieldCounts(double failedFieldCounts) {
        this.failedFieldCounts = failedFieldCounts;
    }

    public double getPassFieldCounts() {
        return passFieldCounts;
    }

    public long getFailedDocumentCount() {
        return failedDocumentCount;
    }

    public void setFailedDocumentCount(long failedDocumentCount) {
        this.failedDocumentCount = failedDocumentCount;
    }

    public void setPassFieldCounts(double passFieldCounts) {
        this.passFieldCounts = passFieldCounts;
    }

    public double getTotalFieldCounts() {
        return totalFieldCounts;
    }

    public void setTotalFieldCounts(double totalFieldCounts) {
        this.totalFieldCounts = totalFieldCounts;
    }

    public double getFieldAccuracy() {
        return fieldAccuracy;
    }

    public void setFieldAccuracy(double fieldAccuracy) {
        this.fieldAccuracy = fieldAccuracy;
    }

    public long getReviewFileCount() {
        return reviewFileCount;
    }

    public void setReviewFileCount(long reviewFileCount) {
        this.reviewFileCount = reviewFileCount;
    }

    public long getAverageReviewTime() {
        return averageReviewTimeInSeconds;
    }

    public void setAverageReviewTime(long averageReviewTime) {
        this.averageReviewTimeInSeconds = averageReviewTime;
    }

    public long getInvalidFileCount() {
        return invalidFileCount;
    }

    public void setInvalidFileCount(long invalidFileCount) {
        this.invalidFileCount = invalidFileCount;
    }

    public long getPassedDocumentCounts() {
        return passedDocumentCounts;
    }

    public void setPassedDocumentCounts(long passedDocumentCounts) {
        this.passedDocumentCounts = passedDocumentCounts;
    }

    public long getProcessedDocumentCounts() {
        return processedDocumentCounts;
    }

    public void setProcessedDocumentCounts(long processedDocumentCounts) {
        this.processedDocumentCounts = processedDocumentCounts;
    }

    public long getDocumentAccuracy() {
        return documentAccuracy;
    }

    public void setDocumentAccuracy(long documentAccuracy) {
        this.documentAccuracy = documentAccuracy;
    }
}