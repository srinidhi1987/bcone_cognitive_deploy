package com.automationanywhere.cognitive.reports.consumer.validator.contract;

import com.automationanywhere.cognitive.reports.consumer.validator.model.ProjectTotalsWithAccuracyDetails;
import com.automationanywhere.cognitive.reports.consumer.validator.model.ReviewedFileStatistic;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;

/**
 * Created by Mayur.Panchal on 21-03-2017.
 */
public interface ValidatorConsumer {
    ReviewedFileStatistic[] getReviewedfilecount(String orgId) throws DataAccessLayerException;
    ProjectTotalsWithAccuracyDetails getPerformanceSummary(String orgId, String projectId)throws DataAccessLayerException;
    void testConnection();
}
