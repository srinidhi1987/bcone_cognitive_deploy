package com.automationanywhere.cognitive.reports.exception.customexceptions;

/**
 * Created by keval.sheth on 29-11-2016.
 */
public class ProjectNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8926150059787041632L;

}
