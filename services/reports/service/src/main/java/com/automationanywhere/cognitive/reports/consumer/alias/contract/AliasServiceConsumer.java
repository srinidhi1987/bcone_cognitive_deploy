package com.automationanywhere.cognitive.reports.consumer.alias.contract;

import com.automationanywhere.cognitive.reports.consumer.alias.model.StorageFieldItem;

/**
 * Created by Mayur.Panchal on 23-03-2017.
 */
public interface AliasServiceConsumer {
    StorageFieldItem[] getAllFieldsByProjectType(String projectType);
    void testConnection();
}
