package com.automationanywhere.cognitive.reports.models.customentitymodel;

/**
 * Created by Mayur.Panchal on 04-05-2017.
 */
public class ProjectWiseFieldCount {
    public final String projectId;
    public final double totalFailFieldCount;
    public final double totalPassFieldCount;
    public final double totalFieldCount;

    public ProjectWiseFieldCount(
        final String projectId,
        final double totalFailFieldCount,
        final double totalPassFieldCount,
        final double totalFieldCount
    ) {
        this.projectId = projectId;
        this.totalFailFieldCount = totalFailFieldCount;
        this.totalPassFieldCount = totalPassFieldCount;
        this.totalFieldCount = totalFieldCount;
    }
}
