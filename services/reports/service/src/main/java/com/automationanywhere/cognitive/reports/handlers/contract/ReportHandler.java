package com.automationanywhere.cognitive.reports.handlers.contract;

import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataNotFoundException;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public interface ReportHandler {
    String GetDashboardProduction(Request request, Response response) throws DataNotFoundException,DataAccessLayerException;
    String GetDashboardProjectWise(Request request, Response response) throws DataNotFoundException,DataAccessLayerException;
    String GetDashboardAllProjects(Request request, Response response) throws DataNotFoundException,DataAccessLayerException;
}
