package com.automationanywhere.cognitive.reports.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class InvalidProjectStatusException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9082578568189780726L;
}
