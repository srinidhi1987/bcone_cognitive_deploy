package com.automationanywhere.cognitive.reports.consumer.validator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mayur.Panchal on 23-03-2017.
 */
public class ClassificationFieldDetails {

    @JsonProperty(value = "fieldId")
    private String fieldid;

    @JsonProperty(value = "representationPercent")
    private double representationPercent;

    public String getFieldid() {
        return fieldid;
    }

    public void setFieldid(String fieldid) {
        this.fieldid = fieldid;
    }

    public double getRepresentationPercent() {
        return representationPercent;
    }

    public void setRepresentationPercent(double representationPercent) {
        this.representationPercent = representationPercent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "name")
    private String name;
}