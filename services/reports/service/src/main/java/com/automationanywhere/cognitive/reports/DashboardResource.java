package com.automationanywhere.cognitive.reports;

import static com.automationanywhere.cognitive.reports.constants.HeaderConstants.CORRELATION_ID;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.port;

import java.util.UUID;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.reports.exception.contract.ExceptionHandler;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.reports.handlers.contract.ReportHandler;
import com.automationanywhere.cognitive.reports.health.handlers.impl.ReportsHealthCheckHandler;

import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class DashboardResource {

    private static AALogger logger = AALogger.create(DashboardResource.class);

	@Autowired
	private ReportHandler reportHandler;

	@Autowired
	private ExceptionHandler exceptionHandler;

	@Autowired
	private ReportsHealthCheckHandler healthApiHandler;

    //@PostConstruct
    /**
     * @Constructor
     * @param port
     */
	public DashboardResource(final String port) {
        port(Integer.parseInt(port));//9992
        before((request, response) -> {
            preRequestProcessor(request);
        });

        exception(DataNotFoundException.class, (ex, req, res) -> exceptionHandler.handleDataNotFoundException(ex, req, res));
        exception(DataAccessLayerException.class, (ex, req, res) -> exceptionHandler.handleDataAccessLayerException(ex, req, res));
        exception(Exception.class, (ex, req, res) -> exceptionHandler.handleException(ex, req, res));
        after(((request, response) -> postRequestProcessor(request, response)));
        //FileDetail Handler
        get("organizations/:orgId/reporting/projects/totals", (req, res) -> reportHandler.GetDashboardProduction(req, res));
        get("organizations/:orgId/reporting/projects/:projectId/totals", (req, res) -> reportHandler.GetDashboardProjectWise(req, res));
        get("organizations/:orgId/reporting/projects", (req, res) -> reportHandler.GetDashboardAllProjects(req, res));
        get("/health", ((request, response) -> {
            logger.entry();
            response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
            response.body("OK");
            logger.exit();
            return response;
        }));
        get("/heartbeat", ((request, response) -> {
          	 return getHeartBeatInfo(response);
        }));
        get("/healthcheck", ((request, response) -> {
            return healthApiHandler.checkHealth();
       }));
    }

    private void preRequestProcessor(Request request) {
        addTransactionIdentifier(request);
        logger.debug("Request URI.." + request.uri());
    }

    private void postRequestProcessor(Request request, Response response) {
        addHeader(request, response);
    }

    private void addTransactionIdentifier(Request request) {
        String correlationId = request.headers(CORRELATION_ID);
        ThreadContext.put(CORRELATION_ID, correlationId != null 
        		? correlationId : UUID.randomUUID().toString());
    }

	private void addHeader(Request request, Response response) {
		response.type("application/json");
	}
    
    /**
     * @param response
     * @return Object
     */
	private Object getHeartBeatInfo(Response response) {
		String heartBeat;
    	//currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization 
		response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
		heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
		response.body(heartBeat);
		return response;
	}
}
