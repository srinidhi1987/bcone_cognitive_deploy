package com.automationanywhere.cognitive.reports.health.connections;
/**
 * @author shweta.thakur
 *
 * This class represents Connection Type available for a subsystem 
 * DB:  If subsystem connects to DB
 * SVC: If subsystem connects to other microservices
 */
public enum ConnectionType {
	DB, SVC
}
