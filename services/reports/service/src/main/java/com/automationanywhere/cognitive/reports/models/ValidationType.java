package com.automationanywhere.cognitive.reports.models;

/**
 * Created by keval.sheth on 29-11-2016.
 */
public enum ValidationType {
    Pass,
    Fail,
    Invalid
}
