package com.automationanywhere.cognitive.reports.consumer.validator.model;

import javax.persistence.Column;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */
public class FieldAccuracyDetails{
    @Id
    @Column(name = "fileid")
    private String FileId;
    @JsonProperty(value = "fieldId")
    @Column(name = "fieldid")
    private String FieldId;
    @JsonProperty(value = "oldValue")
    @Column(name = "oldvalue")
    private String OldValue;
    @JsonProperty(value = "newValue")
    @Column(name = "newvalue")
    private String NewValue;
    @JsonProperty(value = "passAccuracyValue")
    @Column(name = "passaccuracyvalue")
    private float PassAccuracyValue;


    public String getFileId() {
        return FileId;
    }

    public void setFileId(String fileId) {
        FileId = fileId;
    }

    public String getFieldId() {
        return FieldId;
    }

    public void setFieldId(String fieldId) {
        FieldId = fieldId;
    }

    public String getOldValue() {
        return OldValue;
    }

    public void setOldValue(String oldValue) {
        OldValue = oldValue;
    }

    public String getNewValue() {
        return NewValue;
    }

    public void setNewValue(String newValue) {
        NewValue = newValue;
    }

    public float getPassAccuracyValue() {
        return PassAccuracyValue;
    }

    public void setPassAccuracyValue(float passAccuracyValue) {
        PassAccuracyValue = passAccuracyValue;
    }

}