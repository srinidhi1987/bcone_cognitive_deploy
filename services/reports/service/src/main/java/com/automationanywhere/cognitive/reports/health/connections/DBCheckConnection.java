package com.automationanywhere.cognitive.reports.health.connections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.reports.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DBConnectionFailureException;

/**
 * @author shweta.thakur
 *
 *         This class checks the database connection health
 */
public class DBCheckConnection implements HealthCheckConnection {
    private CommonDataAccessLayer<?, ?> commonDataAccessLayer;

    public DBCheckConnection(CommonDataAccessLayer<?, ?> commonDataAccessLayer) {
        this.commonDataAccessLayer = commonDataAccessLayer;
    }

    @Override
    public void checkConnection() {
        try {
            commonDataAccessLayer.CustomQueryExecute(new StringBuilder("SELECT 1 FROM FileDetails"),
                    new HashMap<String, Object>());
        } catch (Exception ex) {
            throw new DBConnectionFailureException("Error connecting database ", ex);
        }
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        //return empty list
        return new ArrayList<ServiceConnectivity>();
    }
}
