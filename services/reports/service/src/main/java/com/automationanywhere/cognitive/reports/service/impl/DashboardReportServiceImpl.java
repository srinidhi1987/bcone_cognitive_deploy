package com.automationanywhere.cognitive.reports.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.reports.consumer.alias.contract.AliasServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.alias.model.StorageFieldItem;
import com.automationanywhere.cognitive.reports.consumer.filemanger.contract.FileServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.project.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.reports.consumer.project.model.CustomField;
import com.automationanywhere.cognitive.reports.consumer.project.model.ProjectDetailMetaData;
import com.automationanywhere.cognitive.reports.consumer.validator.contract.ValidatorConsumer;
import com.automationanywhere.cognitive.reports.consumer.validator.model.ProjectTotalsWithAccuracyDetails;
import com.automationanywhere.cognitive.reports.consumer.validator.model.ReviewedFileStatistic;
import com.automationanywhere.cognitive.reports.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.reports.models.Environment;
import com.automationanywhere.cognitive.reports.models.ProjectDetail;
import com.automationanywhere.cognitive.reports.models.customentitymodel.ProjectWiseFieldCount;
import com.automationanywhere.cognitive.reports.models.customentitymodel.ProjectWiseFieldCountProvider;
import com.automationanywhere.cognitive.reports.service.contract.DashboardReportService;

/**
 * Created by Mayur.Panchal on 17-03-2017.
 */
public class DashboardReportServiceImpl implements DashboardReportService {

    private AALogger aaLogger = AALogger.create(this.getClass());
    private CommonDataAccessLayer commonDataAccessLayer;
    private FileServiceConsumer fileServiceConsumer;
    private ProjectServiceEndpointConsumer projectServiceEndpointConsumer;
    private ValidatorConsumer validatorConsumer;
    private AliasServiceConsumer aliasService;

    @Autowired
    public DashboardReportServiceImpl(
        CommonDataAccessLayer commonDataAccessLayer,
        FileServiceConsumer fileServiceConsumer,
        ProjectServiceEndpointConsumer projectServiceEndpointConsumer,
        ValidatorConsumer validatorConsumer,
        AliasServiceConsumer aliasService
    ) {
        this.commonDataAccessLayer = commonDataAccessLayer;
        this.fileServiceConsumer = fileServiceConsumer;
        this.projectServiceEndpointConsumer = projectServiceEndpointConsumer;
        this.validatorConsumer = validatorConsumer;
        this.aliasService = aliasService;
    }

    @SuppressWarnings("unchecked")
	@Override
    public HashMap<String, Object> getDashboardAllProjectTotals(String orgId) throws DataAccessLayerException {
        HashMap<String,Object> dashboardAllProjectTotals = new HashMap<>();
		aaLogger.traceEntry(" orgId: " + orgId);

        ProjectDetail[] projectDetails = projectServiceEndpointConsumer.getAllProjectDetails(orgId);
        ReviewedFileStatistic[] rfs = validatorConsumer.getReviewedfilecount(orgId);
        List<ReviewedFileStatistic> reviewdFileStatistic = new ArrayList<>();
        if(rfs !=null)
            reviewdFileStatistic = Arrays.asList(rfs);

        long totalFilesProcessed = 0;
        long totalFilesCount = 0;
        long totalAccuracy = 0;
        long totalFailedCount =0;
        int productionprojectcount =0;
        HashMap<String, String> projectState = new HashMap<>();
        aaLogger.trace(" Calculating totalFilesProcessed, totalFilesCount ,totalFailedCount and totalAccuracy ");
        if(projectDetails != null && rfs !=null) {
            for (int i = 0; i < projectDetails.length; i++) {
                ProjectDetail projectDetail = projectDetails[i];
                projectState.put(projectDetail.getId(), projectDetail.getEnvironment().toString());
                if (projectDetail.getEnvironment() == Environment.staging)
                    continue;
                ReviewedFileStatistic reviewFileStats = reviewdFileStatistic.stream().filter(p -> p.getProjectId().equals(projectDetail.getId())).findAny().orElse(null);
                if (reviewFileStats == null)
                    continue;
                totalFilesProcessed += (projectDetail.getNumberOfFiles() - projectDetail.getUnprocessedFileCount());
                totalFilesCount += projectDetail.getNumberOfFiles();
                totalFailedCount += reviewFileStats.getFailedFileCount();
                productionprojectcount++;
            }
        }
        ProjectWiseFieldCountProvider projectWiseFieldCountProvider = new ProjectWiseFieldCountProvider();
        List<ProjectWiseFieldCount> projectWiseFieldCounts = commonDataAccessLayer.ExecuteCustomEntityQuery(projectWiseFieldCountProvider);
        double totalPassFieldCount=0, totalFieldCount=0;
        for(int x=0;x<projectWiseFieldCounts.size();x++)
        {

            ProjectWiseFieldCount projectWiseFieldCount = projectWiseFieldCounts.get(x);
            String environment =projectState.get(projectWiseFieldCount.projectId);
            if(environment != null && environment != Environment.staging.toString()) {
                totalPassFieldCount += projectWiseFieldCount.totalPassFieldCount;
                totalFieldCount += projectWiseFieldCount.totalFieldCount;
            }

        }
        long stp = 0;
        if(totalFieldCount>0)
        {
            totalAccuracy = ((long) (totalPassFieldCount * 100 / totalFieldCount));
        }
        aaLogger.trace("Done Calculating totalFilesProcessed, totalFilesCount ,totalFailedCount and totalAccuracy ");

        if(totalFilesCount != 0)
            stp = (totalFilesProcessed-totalFailedCount)*100/totalFilesCount;
        dashboardAllProjectTotals.put("totalFilesProcessed", totalFilesProcessed);
        dashboardAllProjectTotals.put("totalFilesCount", totalFilesCount);
        dashboardAllProjectTotals.put("totalSTP",stp);
        dashboardAllProjectTotals.put("totalAccuracy",totalAccuracy);
        aaLogger.exit();
        return dashboardAllProjectTotals;
    }

    @Override
    public HashMap<String,Object> getProjectTotal(String orgId, String projectId) throws DataAccessLayerException {
    	aaLogger.traceEntry(  " orgId: " + orgId + " projectId: " + projectId);

        ProjectDetailMetaData projectDetailMeta = projectServiceEndpointConsumer.getProjectDetails(orgId,projectId);

        StorageFieldItem[] fieldsItem =  aliasService.getAllFieldsByProjectType(projectDetailMeta.getProjectTypeId());
        HashMap<String, String> map = new HashMap<>();
        for(int i=0; i < fieldsItem.length ; i++)
        {
            map.put(fieldsItem[i].id, fieldsItem[i].name);
        }

        List<CustomField> customFields=  projectDetailMeta.getFields().getCustom();
        customFields.forEach( customField -> {
            map.put(customField.getId(),customField.getName());
        });
        ProjectTotalsWithAccuracyDetails projectTotals = validatorConsumer.getPerformanceSummary(orgId,projectId);
        aaLogger.trace(" Preparing projectTotalsResponse ..");
        HashMap<String,Object> projectTotalsResponse = new HashMap<>();
        projectTotalsResponse.put("totalFilesProcessed",projectTotals.getTotalFilesProcessed());
        projectTotalsResponse.put("totalSTP",projectTotals.getTotalSTP());
        projectTotalsResponse.put("totalAccuracy",projectTotals.getTotalAccuracy());
        projectTotalsResponse.put("totalFilesUploaded",projectTotals.getTotalFilesUploaded());
        projectTotalsResponse.put("totalFilesToValidation",projectTotals.getTotalFilesToValidation());
        projectTotalsResponse.put("totalFilesValidated",projectTotals.getTotalFilesValidated());
        projectTotalsResponse.put("totalFilesUnprocessable",projectTotals.getTotalFilesUnprocessable());
        aaLogger.trace(" Add field details to projectTotalsResponse ..");
        HashMap<String,Object> fieldRepresentation =  new HashMap<>();
        projectTotals.getClassificationFieldDetails().forEach(item -> {
            item.setName(map.get(item.getFieldid()));
        });
        fieldRepresentation.put("fieldRepresentation",projectTotals.getClassificationFieldDetails());

        projectTotalsResponse.put("classification",fieldRepresentation);
        aaLogger.trace(" Add accuracy to projectTotalsResponse ..");
        HashMap<String,Object> fields =  new HashMap<>();
        projectTotals.getFieldAccuracy().forEach(item -> {
            item.setName(map.get(item.getFieldId()));
        });
        fields.put("fields",projectTotals.getFieldAccuracy());

        projectTotalsResponse.put("accuracy",fields);
        aaLogger.trace(" Add validation details to projectTotalsResponse ..");

        HashMap<String,Object> fieldsv =  new HashMap<>();
        projectTotals.getFieldValidation().forEach(item -> {
            item.setName(map.get(item.getId()));
        });
        fieldsv.put("fields",projectTotals.getFieldValidation());
        projectTotals.getGroupTimeSpentDetails().forEach(item -> {
            item.setName("Group_"+item.getCategoryId());
        });
        fieldsv.put("categories",projectTotals.getGroupTimeSpentDetails());
        projectTotalsResponse.put("validation",fieldsv);
        aaLogger.exit();
        return projectTotalsResponse;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getReportData(String orgId, String projectId) throws DataAccessLayerException {
        ArrayList<HashMap<String,Object>> reportData = new ArrayList<>();
        aaLogger.traceEntry();

        ProjectDetail[] projectDetails = projectServiceEndpointConsumer.getAllProjectDetails(orgId);
        ReviewedFileStatistic[] rfs = validatorConsumer.getReviewedfilecount(orgId);
        List<ReviewedFileStatistic> reviewdFileStatistic = new ArrayList<>();
        if(rfs !=null)
            reviewdFileStatistic = Arrays.asList(rfs);
        aaLogger.trace(" Preparing report data from project details..") ; 
        if(projectDetails != null) {
            for (int i = 0; i < projectDetails.length; i++) {
                ProjectDetail projectDetail = projectDetails[i];
                HashMap<String, Object> element = new HashMap<>();
                element.put("id", projectDetail.getId());
                element.put("organizationId", orgId);
                element.put("name", projectDetail.getName());
                element.put("description", projectDetail.getDescription());
                element.put("environment", projectDetail.getEnvironment());
                element.put("primaryLanguage", projectDetail.getPrimaryLanguage());
                element.put("projectType", projectDetail.getProjectType());
                element.put("projectState", projectDetail.getProjectState());
                element.put("totalFilesProcessed", projectDetail.getNumberOfFiles() - projectDetail.getUnprocessedFileCount());
                long stp = 0;
                ReviewedFileStatistic reviewFileStats = reviewdFileStatistic.stream().filter(p -> p.getProjectId().equals(projectDetail.getId())).findAny().orElse(null);
                if (reviewFileStats != null) {
                    if (projectDetail.getNumberOfFiles() > 0)
                        stp = ((reviewFileStats.getProcessedCount() - reviewFileStats.getFailedFileCount()) * 100 / projectDetail.getNumberOfFiles());
                }
                element.put("totalSTP", (double) stp);
                element.put("numberOfFiles", projectDetail.getNumberOfFiles());
                element.put("totalAccuracy", projectDetail.getAccuracyPercentage());
                element.put("currentTrainedPercentage", projectDetail.getCurrentTrainedPercentage());
                reportData.add(element);
            }
        }
        aaLogger.exit();
        return reportData;
    }
}
