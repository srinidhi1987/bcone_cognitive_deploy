package com.automationanywhere.cognitive.reports.consumer.project.model;

import com.automationanywhere.cognitive.reports.consumer.project.model.FieldType;

/**
 * Created by Jemin.Shah on 08-12-2016.
 */
public class CustomField
{
    private FieldType type;
    private String name;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
