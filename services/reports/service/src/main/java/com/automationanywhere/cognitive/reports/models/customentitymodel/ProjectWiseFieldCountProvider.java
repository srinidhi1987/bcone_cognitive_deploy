package com.automationanywhere.cognitive.reports.models.customentitymodel;

import com.automationanywhere.cognitive.reports.models.ValidationType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 04-05-2017.
 */
public class ProjectWiseFieldCountProvider extends CustomEntityModelBase<ProjectWiseFieldCount> {
    public ProjectWiseFieldCountProvider() {
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {
        queryString.setLength(0);
        String query ="";
        Map<String,Object> inputParam = new HashMap<String,Object>();
        query="select projectId, sum(FailedFieldCounts) as totalFailedFieldCount, sum(PassFieldCounts) as totalPassFieldCount,sum(TotalFieldCounts) as totalFieldCount from VisionBotDataDetail where validationStatus != :invalidValidationStatus group by projectId";
        inputParam.put("invalidValidationStatus", ValidationType.Invalid.toString());
        queryString.append(query);
        return inputParam;
    }

    @Override
    public List<ProjectWiseFieldCount> objectToClass(List<Object> result) {
        List<ProjectWiseFieldCount> list = new ArrayList<>();
        for (int j = 0; j < result.size(); j++) {
            Object[] obj1 = (Object[]) result.get(j);
            ProjectWiseFieldCount cc= new ProjectWiseFieldCount(
                obj1[0].toString(),
                (double)obj1[1],
                (double)obj1[2],
                (double)obj1[3]
            );
            list.add(cc);
        }
        return list;
    }
}
