package com.automationanywhere.cognitive.reports.consumer.alias.impl;

import com.automationanywhere.cognitive.reports.consumer.alias.contract.AliasServiceConsumer;
import com.automationanywhere.cognitive.reports.consumer.alias.model.StorageFieldItem;
import com.automationanywhere.cognitive.reports.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.reports.models.CustomResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.cognitive.reports.constants.HeaderConstants.CORRELATION_ID;

/**
 * Created by Mayur.Panchal on 23-03-2017.
 */
public class AliasServiceConsumerImpl implements AliasServiceConsumer {

    private String rootUrl;
    AALogger aaLogger = AALogger.create(this.getClass());
    //TODO use the spring beans injected rest template
    private RestTemplate restTemplate;
    @Autowired
    private RestTemplateWrapper restTemplateWrapper;
    private String HEARTBEAT_URL = "/heartbeat";

    public AliasServiceConsumerImpl(String rootUrl) {
        aaLogger.traceEntry(rootUrl);
        this.restTemplate = new RestTemplate();
        this.rootUrl = rootUrl;
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(new ObjectMapper());
        messageConverters.add(jsonMessageConverter);
        StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
        messageConverters.add(stringHttpMessageConerters);
        restTemplate.setMessageConverters(messageConverters);
        aaLogger.exit();
    }

    @Override
    public StorageFieldItem[] getAllFieldsByProjectType(String projectType) {
        aaLogger.traceEntry("projectType: " + projectType);
        String relativeUrl = "projecttypes/%s/fields";
        String url = rootUrl + String.format(relativeUrl, projectType);
        StorageFieldItem[] countStatistics = new StorageFieldItem[0];
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<CustomResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET,
                    entity, CustomResponse.class);
            countStatistics = objectMapper.convertValue(standardResponse.getBody().getData(),
                    StorageFieldItem[].class);
        } catch (Exception ex) {
            aaLogger.error(ex.getMessage(), ex);
            return countStatistics;
        }
        aaLogger.exit();
        return countStatistics;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        aaLogger.entry();
        String heartBeatURL = rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse = null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL,
                String.class);
        if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
            throw new DependentServiceConnectionFailureException(
                    "Error while connecting to Alias service");
        }
        aaLogger.exit();
    }

}
