package com.automationanywhere.cognitive.common.domain.service.security;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.AsymmetricKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.SymmetricKey;
import com.automationanywhere.cognitive.common.infrastructure.security.database.exceptions.DatabaseKeyProviderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@ContextConfiguration(locations = {"classpath:spring/test-common-context.xml"})
public class KeyManagerTest extends AbstractTestNGSpringContextTests {

  private KeyManager keyManager;

  @Autowired
  private KeyProvider databaseKeyProvider;

  @BeforeMethod
  public void setUp() {
    keyManager = new KeyManager(databaseKeyProvider);
  }

  @Test(groups = {"createandretrieve"})
  public void testCreateAndRetrieveKey() throws Exception {

    // given
    KeyName keyName = KeyName.SYSTEM_KEY;
    KeyType keyType = KeyType.SYMMETRIC;

    // when
    keyManager.createKey(keyName);

    // when
    CryptographyKey keySavedInDatabase = keyManager.retrieveKey(keyName);

    // then
    assertThat(keySavedInDatabase).isNotNull();
    assertThat(keySavedInDatabase instanceof SymmetricKey);
    SymmetricKey symmetricKey = (SymmetricKey) keySavedInDatabase;
    assertThat(symmetricKey.key()).isNotNull();
  }

  @Test(groups = {"createandretrieve"})
  public void testCreateAndRetrieveAysmmetricKey() throws Exception {

    // given
    KeyName keyName = KeyName.CR_COMMUNICATION_KEY;
    KeyType keyType = KeyType.ASYMMETRIC;

    // when
    keyManager.createKey(keyName);

    // when
    CryptographyKey keySavedInDatabase = keyManager.retrieveKey(keyName);

    // then
    assertThat(keySavedInDatabase).isNotNull();
    assertThat(keySavedInDatabase instanceof AsymmetricKey);
    AsymmetricKey asymmetricKey = (AsymmetricKey) keySavedInDatabase;
    assertThat(asymmetricKey.publicKey()).isNotNull();
    assertThat(asymmetricKey.privateKey()).isNotNull();
  }

  @Test(expectedExceptions = DatabaseKeyProviderException.class, dependsOnGroups={"createandretrieve"})
  public void testRemoveKey() throws Exception {

    // given
    KeyName keyName = KeyName.SYSTEM_KEY;

    // when
    keyManager.removeKey(keyName);

    // then
    CryptographyKey keySavedInDatabase = keyManager.retrieveKey(keyName);

    // then
    // exception is thrown
  }

}