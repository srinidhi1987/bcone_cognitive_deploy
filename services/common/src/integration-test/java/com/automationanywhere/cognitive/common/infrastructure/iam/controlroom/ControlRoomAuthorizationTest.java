package com.automationanywhere.cognitive.common.infrastructure.iam.controlroom;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.iam.exceptions.AuthorizationException;
import com.automationanywhere.cognitive.common.domain.service.security.KeyManager;
import com.automationanywhere.cognitive.common.domain.service.security.KeyProvider;
import com.automationanywhere.cognitive.common.infrastructure.controlroom.ControlRoomSdkWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@ContextConfiguration(locations = {"classpath:spring/test-common-context.xml"})
public class ControlRoomAuthorizationTest extends AbstractTestNGSpringContextTests {
  @Autowired
  AppConfigurationRepository defaultAppConfigurationRepository;
  private ControlRoomSdkWrapper sdk;
  @Autowired
  private KeyProvider databaseKeyProvider;

  private ControlRoomAuthorization controlRoomAuthorization;

  @BeforeMethod
  public void setUp() {
    KeyManager keyManager = new KeyManager(databaseKeyProvider);
    sdk = new ControlRoomSdkWrapper(defaultAppConfigurationRepository, keyManager);
    controlRoomAuthorization = new ControlRoomAuthorization(sdk);
  }

  @Test
  private void authorize_Should_Authorize_Token() throws Exception {
    //given
    String token = sdk.getAppUserToken();
    //when
    //then
    controlRoomAuthorization.authorize(token);
  }

  @Test(expectedExceptions = AuthorizationException.class)
  private void authorize_Should_Throw_AuthorizationException() throws Exception {
    //given
    String token = "invalidToken";
    //when

    //then
    controlRoomAuthorization.authorize(token);
  }
}
