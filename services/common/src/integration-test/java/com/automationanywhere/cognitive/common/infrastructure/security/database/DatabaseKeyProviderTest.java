package com.automationanywhere.cognitive.common.infrastructure.security.database;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyGeneratorFactory;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.SymmetricKey;
import com.automationanywhere.cognitive.common.infrastructure.security.database.exceptions.DatabaseKeyProviderException;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(locations = {"classpath:spring/test-common-context.xml"})
public class DatabaseKeyProviderTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private SecurityKeyRepository hibernateSecurityKeyRepository;
  private DatabaseKeyProvider keyProvider;

  @BeforeMethod
  public void setUp() {
    keyProvider = new DatabaseKeyProvider(hibernateSecurityKeyRepository);
  }

  @Test(groups = "saveandget")
  public void testSaveAndGetKey() throws Exception {

    // given
    CryptographyKey symmetricKey = KeyGeneratorFactory.keyGenerator(KeyType.SYMMETRIC)
        .generateKey();

    // when
    keyProvider.saveKey(KeyName.SYSTEM_KEY, symmetricKey);

    // when
    CryptographyKey securityKey = keyProvider.getKey(KeyName.SYSTEM_KEY);

    // then
    assertThat(securityKey).isNotNull();
    assertThat(securityKey instanceof  SymmetricKey);
    assertThat(((SymmetricKey)securityKey).key()).isEqualTo(((SymmetricKey)symmetricKey).key());

  }

  @Test(groups = "saveandget", expectedExceptions = {DatabaseKeyProviderException.class})
  public void testSaveKeyWithSameName() throws Exception {

    // given
    CryptographyKey symmetricKey = KeyGeneratorFactory.keyGenerator(KeyType.SYMMETRIC)
        .generateKey();

    // when
    keyProvider.saveKey(KeyName.SYSTEM_KEY, symmetricKey);

    // then
    // exception is thrown
  }

  @Test(expectedExceptions = {DatabaseKeyProviderException.class}, dependsOnGroups = {"saveandget"})
  public void testRemoveKey() {
    // given
    KeyName systemKey = KeyName.SYSTEM_KEY;

    // when
    keyProvider.removeKey(systemKey);

    // then
    CryptographyKey securityKey = keyProvider.getKey(systemKey);
    //exception is thrown
  }

}