package com.automationanywhere.cognitive.common.aspect;

import com.automationanywhere.cognitive.common.healthapi.responsebuilders.BuildConfigurationReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;

import java.io.IOException;

@ContextConfiguration(locations = {"classpath:spring/test-common-context.xml"})
public class PerformanceMonitorInterceptorTest {

    @Test(expectedExceptions = IOException.class)
    public void testOfPerformanceMonitorInterceptorShouldGenerateObjectFromSpringProxy() throws IOException {
        //given
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/test-common-context.xml");
        BuildConfigurationReader buildConfigurationReader = context.getBean("buildConfigurationReader",BuildConfigurationReader.class);

        //when
        buildConfigurationReader.readBuildVersionFile() ;

        //then
        //Assertion through expectedException annotator.
    }

}
