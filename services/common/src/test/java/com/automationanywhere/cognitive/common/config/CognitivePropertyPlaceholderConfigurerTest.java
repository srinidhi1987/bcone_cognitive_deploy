package com.automationanywhere.cognitive.common.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import com.automationanywhere.cognitive.common.security.CipherService;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Properties;
import java.util.Set;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CognitivePropertyPlaceholderConfigurerTest {

  @Test
  private void modifyPropertiesShouldDecryptAndUpdateProperties() throws Exception {
    // GIVEN
    CipherService cipherService = mock(CipherService.class);
    DataTransformer dataTransformer = mock(DataTransformer.class);

    Mockito.when(dataTransformer.transform(Mockito.any(byte[].class)))
        .thenReturn(new ByteArrayInputStream("encryptedVal1".getBytes()));

    Mockito.when(cipherService.decrypt(Mockito.any()))
        .thenReturn(new ByteArrayInputStream("encryptedVal1".getBytes()));

    Mockito.when(dataTransformer.transform(Mockito.any(InputStream.class)))
        .thenReturn(getByteArrayOutputStream(new ByteArrayInputStream("encryptedVal1".getBytes())));

    Set<String> encryptedKeys = Sets.newSet("test");

    CognitivePropertyPlaceholderConfigurer props = new CognitivePropertyPlaceholderConfigurer();

    props.setCipherService(cipherService);
    props.setDataTransformer(dataTransformer);
    props.setEncryptedKeys(encryptedKeys);

    // WHEN
    String value = props.convertProperty("test", "1234");

    // THEN
    assertThat(value).isEqualTo("encryptedVal1");
  }
  @Test
  private void modifyPropertiesShouldNotDecryptAndUpdateProperties() throws Exception {
    // GIVEN
    Set<String> encryptedKeys = Sets.newSet("test");

    CognitivePropertyPlaceholderConfigurer props = new CognitivePropertyPlaceholderConfigurer();
    props.setEncryptedKeys(encryptedKeys);

    // WHEN
    String value = props.convertProperty("test2", "1234");

    // THEN
    assertThat(value).isEqualTo("1234");
  }

  private ByteArrayOutputStream getByteArrayOutputStream(InputStream inputStream)
      throws IOException {
    ByteArrayOutputStream byteArrStream = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    try {
      while ((length = inputStream.read(buffer)) != -1) {
        byteArrStream.write(buffer, 0, length);
      }
    } catch (IOException ex) {
      throw ex;
    } finally {
      inputStream.close();
    }
    return byteArrStream;
  }
}