package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

public class AsymmetricKeyGeneratorTest {

  @Test
  public void testGenerateKey() throws Exception {

    // given
    AsymmetricKeyGenerator asymmetricKeyGenerator = new AsymmetricKeyGenerator();

    // when
    CryptographyKey cryptographyKey = asymmetricKeyGenerator.generateKey();

    // then
    assertThat(cryptographyKey).isNotNull();
    assertTrue(cryptographyKey instanceof AsymmetricKey);
    AsymmetricKey asymmetricKey = (AsymmetricKey) cryptographyKey;
    assertThat(asymmetricKey.privateKey()).isNotNull();
    assertThat(asymmetricKey.publicKey()).isNotNull();
  }
}