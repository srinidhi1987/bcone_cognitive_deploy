package com.automationanywhere.cognitive.common.healthcheck.heartbeat;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Heartbeat;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.testng.annotations.Test;

public class DefaultHeartbeatComponentTest {

  @Test
  public void testPrepareHeartBeat() throws ExecutionException, InterruptedException {
    // GIVEN
    String appName = "name";
    String type = "";
    Status status = Status.OK;
    String failReason = "";
    BuildInfo buildInfo = new BuildInfo(
        appName,
        "1.0",
        "1234",
        "12345678",
        "2018-03-20"
    );
    BuildInfoFactory buildInfoFactory = mock(BuildInfoFactory.class);
    when(buildInfoFactory.build()).thenReturn(buildInfo);

    AppInfo appInfo = new AppInfo(
        appName,
        type,
        status,
        failReason,
        "2018-03-20 11:20:32.234"
    );
    AppInfoFactory appInfoFactory = mock(AppInfoFactory.class);
    when(appInfoFactory.build(appName, type, status, failReason)).thenReturn(appInfo);

    Heartbeat heartbeat = new Heartbeat(appInfo, buildInfo);

    DefaultHeartbeatComponent component = new DefaultHeartbeatComponent(
        buildInfoFactory,
        appInfoFactory
    );

    // WHEN
    CompletableFuture<Heartbeat> future = component.prepareHeartbeat();

    //THEN
    assertThat(future.get()).isEqualToComparingFieldByFieldRecursively(heartbeat);
  }

  @Test
  public void testHeartbeatConvert() throws ExecutionException, InterruptedException {
    // GIVEN
    Heartbeat heartbeat = new Heartbeat(new AppInfo(
        "name",
        "",
        Status.OK,
        "",
        "2018-03-20 11:20:32.234"
    ), new BuildInfo(
        "name",
        "1.0",
        "1234",
        "12345678",
        "2018-03-20"
    ));
    String expected = "Application: name\n"
        + "Status: OK\n"
        + "Reason: \n"
        + "Version: 1.0\n"
        + "Branch: 1234\n"
        + "GIT #: 12345678\n"
        + "Build Time: 2018-03-20\n"
        + "Application uptime: 2018-03-20 11:20:32.234\n";

    DefaultHeartbeatComponent component = new DefaultHeartbeatComponent(null, null);

    //WHEN
    String result = component.convert(heartbeat);

    // THEN
    assertThat(result).isEqualTo(expected);

  }
}
