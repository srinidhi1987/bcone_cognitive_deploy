package com.automationanywhere.cognitive.common.configuration;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;

public class FileChangeListenerTest {
  private FileChangeListener l;
  private Runnable cb;

  @BeforeMethod
  public void setUp() throws Exception {
    this.l = spy(new FileChangeListener());
    this.cb = mock(Runnable.class);
  }

  @Test
  public void testListenConsumesExceptions() throws Exception {
    FileConfigurationProvider p = mock(FileConfigurationProvider.class);

    doThrow(new IOException()).when(this.l).createWatcher("./file", this.cb);

    this.l.listen(p, "./file", this.cb);

    Mockito.verify(this.l).createWatcher("./file", this.cb);
  }

  @Test
  public void testInterrupt() throws Exception {
    FileConfigurationProvider p = mock(FileConfigurationProvider.class);

    CountDownLatch c = new CountDownLatch(1);
    Thread t = new Thread(() -> {
      c.countDown();

      this.l.listen(p, "./file", this.cb);
    });
    t.setDaemon(true);
    t.start();

    c.await(1000, TimeUnit.MILLISECONDS);

    t.interrupt();

    Thread.sleep(500);

    assertThat(t.isAlive()).isEqualTo(false);
  }

  @Test
  public void testRefresh() {
    FileConfigurationProvider p = mock(FileConfigurationProvider.class);

    WatchEvent<?> e = mock(WatchEvent.class);
    Mockito.doReturn(StandardWatchEventKinds.OVERFLOW)
           .doReturn(StandardWatchEventKinds.ENTRY_DELETE)
           .doReturn(StandardWatchEventKinds.ENTRY_CREATE)
           .doReturn(StandardWatchEventKinds.ENTRY_MODIFY).when(e).kind();

    this.l.refresh(p, e);
    Mockito.verify(p, never()).refresh();

    this.l.refresh(p, e);
    Mockito.verify(p, never()).refresh();

    this.l.refresh(p, e);
    Mockito.verify(p, never()).refresh();

    this.l.refresh(p, e);
    Mockito.verify(p).refresh();
  }
}
