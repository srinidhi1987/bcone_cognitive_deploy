package com.automationanywhere.cognitive.common.healthcheck.info;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.automationanywhere.cognitive.common.healthcheck.exception.BuildInfoFileNotFoundException;
import org.testng.annotations.Test;

public class AppInfoFactoryTest {

  @Test
  public void testBuildFileNotFound() {
    // GIVEN
    BuildInfoFactory factory = new BuildInfoFactory();

    // WHEN
    Throwable throwable = catchThrowable(factory::build);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(BuildInfoFileNotFoundException.class);
  }
}
