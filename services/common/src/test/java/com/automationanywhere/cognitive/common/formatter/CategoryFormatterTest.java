package com.automationanywhere.cognitive.common.formatter;

import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by msundell on 5/16/17
 */
public class CategoryFormatterTest {

  @Test
  public void testOfFormattingACategoryIdShouldGenerateACorrectCategoryName() throws Exception {
    // given
    String categoryId = "100";

    // when
    String categoryName = CategoryFormatter.format(categoryId);

    // then
    assertThat(categoryName).isNotNull();
    assertThat(categoryName).isEqualTo("Group_" + categoryId);
  }

  @Test
  public void testOfConvertingAnEmptyStringShouldResultInYetToBeClassified() throws Exception {
    // given
    String categoryId = "";

    // when
    String categoryName = CategoryFormatter.format(categoryId);

    // then
    assertThat(categoryName).isNotNull();
    assertThat(categoryName).isEqualTo("Yet to be classified");
  }

  @Test
  public void testOfConvertingNegativeOneShouldResultInUnclassified() throws Exception {
    // given
    String categoryId = "-1";

    // when
    String categoryName = CategoryFormatter.format(categoryId);

    // then
    assertThat(categoryName).isNotNull();
    assertThat(categoryName).isEqualTo("Unclassified");
  }
}
