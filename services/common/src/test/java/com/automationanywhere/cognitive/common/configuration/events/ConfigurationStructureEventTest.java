package com.automationanywhere.cognitive.common.configuration.events;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigurationStructureEventTest {
  @Test
  public void testEquals() {
    ConfigurationStructureEvent e = new ConfigurationAddEvent("k", "v");
    ConfigurationStructureEvent e2 = new ConfigurationAddEvent("k", "v");
    ConfigurationStructureEvent e3 = new ConfigurationAddEvent("kz", "v");
    ConfigurationStructureEvent e4 = new ConfigurationAddEvent("k", "va");

    assertThat(e.equals(e)).isTrue();
    assertThat(e.equals(e2)).isTrue();
    assertThat(e.equals(null)).isFalse();
    assertThat(e.equals(new Object())).isFalse();
    assertThat(e.equals(e3)).isFalse();
    assertThat(e.equals(e4)).isFalse();

    assertThat(e.hashCode()).isEqualTo(e2.hashCode());
  }
}
