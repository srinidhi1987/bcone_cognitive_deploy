package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.bind.DatatypeConverter;
import org.testng.annotations.Test;

public class SymmetricKeyAlgorithmTest {

  @Test
  public void testEncryptAndDecrypt() throws Exception {

    // given
    String secretData = "secret data";
    CryptographyKey cryptographyKey = new SymmetricKeyGenerator().generateKey();
    SymmetricKeyAlgorithm symmetricKeyAlgorithm = new SymmetricKeyAlgorithm();
    InputStream data = new ByteArrayInputStream(secretData.getBytes());

    // when
    InputStream actualEncryptedStream = symmetricKeyAlgorithm.encrypt(cryptographyKey, data);

    // then
    assertThat(actualEncryptedStream).isNotNull();

    byte[] enc = null;
    enc = streamToByteArray(actualEncryptedStream);
    String actualEncryptedData = DatatypeConverter.printBase64Binary(enc);
    assertThat(actualEncryptedData).isNotEqualTo(secretData);

    // given
    InputStream encryptedDataStream = new ByteArrayInputStream(
        DatatypeConverter.parseBase64Binary(actualEncryptedData));

    // when
    InputStream actualDecryptedStream = symmetricKeyAlgorithm.decrypt(cryptographyKey, encryptedDataStream);

    // then
    assertThat(actualDecryptedStream).isNotNull();
    String actualDecryptedData = streamToString(actualDecryptedStream);
    assertThat(actualDecryptedData).isEqualTo(secretData);

  }

  /**
   * @return String converted from inputStream
   */
  private String streamToString(final InputStream inputStream) {
    StringBuilder stringBuilder = new StringBuilder();
    try (BufferedReader bufferedStreamReader = new BufferedReader(
        new InputStreamReader(inputStream))) {
      String line;

      while ((line = bufferedStreamReader.readLine()) != null) {
        stringBuilder.append(line);
      }
    } catch (IOException ioe) {
      throw new RuntimeException("Error while converting InputStream to String", ioe);
    }
    return stringBuilder.toString();
  }

  /**
   * @return byte array converted from inputStream
   */
  public byte[] streamToByteArray(final InputStream inputStream) {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    final int BYTE_BUFFER_SIZE = 1024;
    byte[] byteArray = null;
    try {
      int nRead;
      byte[] data = new byte[BYTE_BUFFER_SIZE];
      while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
        buffer.write(data, 0, nRead);
      }

      buffer.flush();
      byteArray = buffer.toByteArray();
    } catch (IOException ioe) {
      throw new RuntimeException(
          "Error while converting the input stream into byte array.", ioe);
    }
    return byteArray;
  }

}