package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertTrue;

public class KeyBasedAlgorithmFactoryTest {

  @Test
  public void testKeyBasedAlgorithmWhenTypeIsSymmetric() throws Exception {

    // given
    KeyType keyType = KeyType.SYMMETRIC;

    // when
    KeyBasedAlgorithms keyBasedAlgorithms = KeyBasedAlgorithmFactory.keyBasedAlgorithm(keyType);

    // then
    assertThat(keyBasedAlgorithms).isNotNull();
    assertTrue(keyBasedAlgorithms instanceof SymmetricKeyAlgorithm);
  }

  @Test
  public void testKeyBasedAlgorithmWhenTypeIsAsymmetric() throws Exception {

    // given
    KeyType keyType = KeyType.ASYMMETRIC;

    // when
    KeyBasedAlgorithms keyBasedAlgorithms = KeyBasedAlgorithmFactory.keyBasedAlgorithm(keyType);

    // then
    assertThat(keyBasedAlgorithms).isNotNull();
    assertTrue(keyBasedAlgorithms instanceof AsymmetricKeyAlgorithm);
  }
}