package com.automationanywhere.cognitive.common.security;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SSLContextProviderTest {

    @InjectMocks
    SSLContextProvider sslContextProvider;

    @BeforeTest
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOfgetSelfSignedCertificateValidationContextShouldProvideValidContext() throws KeyManagementException, NoSuchAlgorithmException {
        //given

        //when
        sslContextProvider.setSelfSignedContectFactory();

        //then
        //there should not be any exception
    }

    @Test
    public void testOfgetRequestFactoryShouldReturnValidRequestFactory() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        //given

        //when
        sslContextProvider.getRequestFactory();

        //then
        //there should not be any exception
    }

}
