package com.automationanywhere.cognitive.common.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DataTransformerTest {

  DataTransformer dataTransformer;

  @BeforeClass
  public void setUp() {
    dataTransformer = new DataTransformer();
  }

  @Test
  public void transformShouldReturnByteArrayInputStream() throws Exception {
    //given
    byte[] byteArr = {0x7a, 0x30};

    //when
    ByteArrayInputStream byteArrayInputStream = dataTransformer.transform(byteArr);

    //then
    assertThat(byteArrayInputStream).isNotNull();
    assertThat(byteArrayInputStream).hasSameContentAs(new ByteArrayInputStream(byteArr));

  }

  @Test
  public void transformShouldReturnByteArrayOutputStream() throws Exception {
    //given
    byte[] byteArr = {0x7a, 0x30};

    InputStream inputStream = new ByteArrayInputStream(byteArr);

    //when
    ByteArrayOutputStream actualByteArrOutStream = dataTransformer.transform(inputStream);

    ByteArrayOutputStream expectedByteArrOutStream = new ByteArrayOutputStream(byteArr.length);
    expectedByteArrOutStream.write(byteArr, 0, byteArr.length);

    //then
    assertThat(actualByteArrOutStream).isNotNull();
    assertThat(actualByteArrOutStream.toByteArray())
        .isEqualTo(expectedByteArrOutStream.toByteArray());
  }

  //@Test(expectedExceptions = IOException.class)
  public void transformShouldThrowIOException() throws IOException {
    //given
    byte[] byteArr = {0x7a, 0x30};

    InputStream inputStream = new ByteArrayInputStream(byteArr);
    inputStream.close();
    //when
    ByteArrayOutputStream actualByteArrOutStream = dataTransformer.transform(inputStream);


  }
  @Test
  public void transformToListOfStringArrayShouldReturnListOfStringArray() throws Exception {
    //given
    Object[] objectArray = {null, "", (Boolean) true, "Abc"};
    List<Object[]> lstOfObjectArr = new ArrayList<>();
    lstOfObjectArr.add(objectArray);

    String[] strData = {"", " ", "1", "Abc"};
    List<String[]> expectedLstOfStringArr = new ArrayList<>();
    expectedLstOfStringArr.add(strData);
    //when
    List<String[]> lstOfStringArr = dataTransformer.transformToListOfStringArray(lstOfObjectArr);

    //then
    assertThat(lstOfStringArr).containsExactlyElementsOf(expectedLstOfStringArr);

  }

  @Test
  public void transformToListOfStringShouldReturnListOfString() throws Exception {
    //given
    Object[] objectArray = {null, "", (Boolean) true, "Abc"};
    List<Object> lstOfObjects = Arrays.asList(objectArray);


    String[] strData = {"", " ", "1", "Abc"};
    List<String> expectedLstOfString = Arrays.asList(strData);

    //when
    List<String> lstOfString = dataTransformer.transformToListOfString(lstOfObjects);

    //then
    assertThat(lstOfString).containsExactlyElementsOf(expectedLstOfString);

  }


}
