package com.automationanywhere.cognitive.common.healthapi.responsebuilders;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Properties;

import org.mockito.Mockito;
import org.testng.annotations.Test;


/**
 * @author shweta.thakur
 *
 */
public class BuildConfigurationReaderTest {

	@Test(expectedExceptions={IOException.class})
	public void testOfReadBuildVersionFileWithInvalidFile() throws IOException {
		// when
		BuildConfigurationReader buildConfigurationReader = new BuildConfigurationReader();
		buildConfigurationReader.readBuildVersionFile() ;	
	}
	
	@Test
	public void testOfReadBuildVersionFileWithValidFile() throws IOException {
		//given
		Properties prop = new Properties();
		prop.put("Application-name", "filemanager");
		prop.put("Version", "1.0.0");
		prop.put("Branch", "develop");
		prop.put("GitHash", "121212");
		prop.put("Buildtime", "2017-08-07T11:14:31.059Z");
		
		// when
		BuildConfigurationReader buildConfigurationReader = Mockito.mock(BuildConfigurationReader.class);
		Mockito.when (buildConfigurationReader.readBuildVersionFile()).thenReturn(prop); 	
		
		//Then
		assertThat(prop.get("Application-name")).isEqualTo("filemanager");
		assertThat(prop.get("Version")).isEqualTo("1.0.0");
		assertThat(prop.get("Branch")).isEqualTo("develop");
		assertThat(prop.get("Buildtime")).isEqualTo("2017-08-07T11:14:31.059Z");
		assertThat(prop.get("GitHash")).isEqualTo("121212");
	}
	
	@Test
	public void testOfReadBuildVersionFileWithEmptyFile() throws IOException {
		//given
		Properties prop = new Properties();

		// when
		BuildConfigurationReader buildConfigurationReader = Mockito.mock(BuildConfigurationReader.class);
		Mockito.when (buildConfigurationReader.readBuildVersionFile()).thenReturn(prop); 
		
		//Then
		assertThat(prop.size()).isEqualTo(0);
	}
	
}
