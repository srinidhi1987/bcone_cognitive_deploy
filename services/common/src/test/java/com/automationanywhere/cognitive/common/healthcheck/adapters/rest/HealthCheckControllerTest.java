package com.automationanywhere.cognitive.common.healthcheck.adapters.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.components.DetailedHealthCheckComponent;
import com.automationanywhere.cognitive.common.healthcheck.components.SimpleHealthComponent;
import com.automationanywhere.cognitive.common.healthcheck.heartbeat.DefaultHeartbeatComponent;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Heartbeat;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class HealthCheckControllerTest {

  private static DefaultHeartbeatComponent heartbeatComponent;
  private static DetailedHealthCheckComponent healthCheckComponent;
  private static SimpleHealthComponent simpleHealthComponent;
  private static BuildInfoFactory buildInfoFactory;
  private static AppInfoFactory appInfoFactory;
  private static HealthCheckController controller;

  @BeforeClass
  public static void setUp() {
    heartbeatComponent = spy(new DefaultHeartbeatComponent(null, null));
    healthCheckComponent = spy(new DetailedHealthCheckComponent(null, null));
    simpleHealthComponent = spy(new SimpleHealthComponent(null, null));
    buildInfoFactory = new BuildInfoFactory();
    appInfoFactory = new AppInfoFactory();
    controller = new HealthCheckController(
        Optional.of(heartbeatComponent),
        Optional.of(healthCheckComponent),
        Optional.of(simpleHealthComponent),
        Optional.empty(),
        buildInfoFactory,
        appInfoFactory
    );
  }

  @Test
  public void executeHealth() throws ExecutionException, InterruptedException {
    // GIVEN
    Application application = new Application();
    application.setAppInfo(appInfoFactory.build(
        "test",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        ""
    ));
    Application subsystem = new Application();
    subsystem.setAppInfo(appInfoFactory.build(
        "subsystem",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        ""
    ));
    application.setSubsystems(Collections.singletonList(subsystem));
    doReturn(CompletableFuture.completedFuture(application))
        .when(simpleHealthComponent).executeHealthCheck(new ArrayList<>());

    // WHEN
    CompletableFuture<String> health = controller.health();

    // THEN
    assertThat(health.get()).isEqualTo(
        "{\"success\":true,\"data\":{\"serviceStatuses\":[{\"name\":\"subsystem\",\"statusCode\":200}]},\"errors\":\"\"}");
  }

  @Test
  public void executeHealthJson() throws ExecutionException, InterruptedException {
    // GIVEN
    Application application = new Application();
    application.setAppInfo(appInfoFactory.build(
        "test",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        ""
    ));
    Application subsystem = new Application();
    subsystem.setAppInfo(appInfoFactory.build(
        "subsystem",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        ""
    ));
    application.setSubsystems(Collections.singletonList(subsystem));
    doReturn(CompletableFuture.completedFuture(application))
        .when(simpleHealthComponent).executeHealthCheck(new ArrayList<>());

    // WHEN
    CompletableFuture health = controller.healthJson();

    // THEN
    assertThat(health.get()).isEqualTo(application);
  }

  @Test
  public void executeHealthCheck() throws ExecutionException, InterruptedException {
    // GIVEN
    Application application = new Application();
    application.setAppInfo(appInfoFactory.build(
        "test",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        "1234"
    ));
    application.setBuildInfo(buildInfoFactory.build(
        "test",
        "1",
        "2",
        "3",
        "4"
    ));
    Application subsystem = new Application();
    subsystem.setAppInfo(appInfoFactory.build(
        "subsystem",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        "4567"
    ));
    subsystem.setBuildInfo(buildInfoFactory.build(
        "subsystem",
        "5",
        "6",
        "7",
        "8"
    ));
    application.setSubsystems(Collections.singletonList(subsystem));
    doReturn(CompletableFuture.completedFuture(application))
        .when(healthCheckComponent).executeHealthCheck(new ArrayList<>());

    // WHEN
    CompletableFuture<String> health = controller.healthCheck();

    // THEN
    assertThat(health.get()).isEqualTo(
        "Application: test\n"
            + "Status: OK\n"
            + "Reason: \n"
            + "Version: 1\n"
            + "Branch: 2\n"
            + "GIT #: 3\n"
            + "Build Time: 4\n"
            + "Application uptime: 1234\n"
            + "\n"
            + "Subsystems: \n"
            + "\n"
            + "  Subsystem: \n"
            + "    Application: subsystem\n"
            + "    Status: OK\n"
            + "    Reason: \n"
            + "    Version: 5\n"
            + "    Branch: 6\n"
            + "    GIT #: 7\n"
            + "    Build Time: 8\n"
            + "    Application uptime: 4567\n");
  }

  @Test
  public void executeHealthCheckJson() throws ExecutionException, InterruptedException {
    // GIVEN
    Application application = new Application();
    application.setAppInfo(appInfoFactory.build(
        "test",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        "1234"
    ));
    application.setBuildInfo(buildInfoFactory.build(
        "test",
        "1",
        "2",
        "3",
        "4"
    ));
    Application subsystem = new Application();
    subsystem.setAppInfo(appInfoFactory.build(
        "subsystem",
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        "4567"
    ));
    subsystem.setBuildInfo(buildInfoFactory.build(
        "subsystem",
        "5",
        "6",
        "7",
        "8"
    ));
    application.setSubsystems(Collections.singletonList(subsystem));
    doReturn(CompletableFuture.completedFuture(application))
        .when(healthCheckComponent).executeHealthCheck(new ArrayList<>());

    // WHEN
    CompletableFuture health = controller.healthCheckJson();

    // THEN
    assertThat(health.get()).isEqualToComparingFieldByFieldRecursively(application);
  }

  @Test
  public void executeHeartBeat() throws ExecutionException, InterruptedException {
    // GIVEN
    Heartbeat heartbeat = new Heartbeat(
        appInfoFactory.build(
            "test",
            CommandType.APPLICATION.name(),
            Status.OK,
            "",
            "1234"
        ),
        buildInfoFactory.build(
            "test",
            "1",
            "2",
            "3",
            "4"
        )
    );
    doReturn(CompletableFuture.completedFuture(heartbeat))
        .when(heartbeatComponent).prepareHeartbeat();

    // WHEN
    CompletableFuture<String> health = controller.heartbeatText();

    // THEN
    assertThat(health.get()).isEqualTo(
        "Application: test\n"
            + "Status: OK\n"
            + "Reason: \n"
            + "Version: 1\n"
            + "Branch: 2\n"
            + "GIT #: 3\n"
            + "Build Time: 4\n"
            + "Application uptime: 1234\n");
  }

  @Test
  public void executeHeartBeatJson() throws ExecutionException, InterruptedException {
    // GIVEN
    Heartbeat heartbeat = new Heartbeat(
        appInfoFactory.build(
            "test",
            CommandType.APPLICATION.name(),
            Status.OK,
            "",
            "1234"
        ),
        buildInfoFactory.build(
            "test",
            "1",
            "2",
            "3",
            "4"
        )
    );
    doReturn(CompletableFuture.completedFuture(heartbeat))
        .when(heartbeatComponent).prepareHeartbeat();

    // WHEN
    CompletableFuture health = controller.heartbeatJson();

    // THEN
    assertThat(health.get()).isEqualTo(heartbeat);
  }
}
