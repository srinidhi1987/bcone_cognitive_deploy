package com.automationanywhere.cognitive.common.healthcheck.info;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import org.testng.annotations.Test;

public class BuildInfoFactoryTest {

  @Test
  public void test() {
    // GIVEN
    String name = "name";
    String type = "type";
    String fail = "fail";
    AppInfoFactory factory = new AppInfoFactory();

    // WHEN
    AppInfo appInfo = factory.build(name, type, Status.OK, fail);

    // THEN
    assertThat(appInfo).isNotNull();
    assertThat(appInfo.getName()).isEqualTo(name);
    assertThat(appInfo.getType()).isEqualTo(type);
    assertThat(appInfo.getStatus()).isEqualTo(Status.OK);
    assertThat(appInfo.getFailReason()).isEqualTo(fail);
  }
}
