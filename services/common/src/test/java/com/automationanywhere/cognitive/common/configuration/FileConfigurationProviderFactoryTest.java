package com.automationanywhere.cognitive.common.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class FileConfigurationProviderFactoryTest {
  @Test
  public void testCreate() {
    FileConfigurationProviderFactory f = spy(new FileConfigurationProviderFactory());

    FileConfigurationProvider p = mock(FileConfigurationProvider.class);

    doReturn(p).when(f).createFileConfigurationProvider(notNull(), eq("./application.yaml"));

    f.create();

    verify(p).refresh();
    verify(p, never()).listen(any());
  }

  @Test
  public void testCustomCreate() {
    ObjectMapper om = mock(ObjectMapper.class);
    FileConfigurationProviderFactory f = spy(new FileConfigurationProviderFactory());

    FileConfigurationProvider p = mock(FileConfigurationProvider.class);

    doReturn(p).when(f).createFileConfigurationProvider(om, "./application.yaml");

    f.create(om, "./application.yaml", null);

    verify(p).refresh();
    verify(p, never()).listen(any());
  }
}