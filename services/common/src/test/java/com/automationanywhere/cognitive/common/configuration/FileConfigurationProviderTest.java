package com.automationanywhere.cognitive.common.configuration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.offset;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.fail;

import com.automationanywhere.cognitive.common.configuration.events.ConfigurationAddEvent;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationChangeEvent;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationChangeListener;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationEvent;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationRemoveEvent;
import com.automationanywhere.cognitive.common.configuration.exceptions.NoSuchKeyException;
import com.automationanywhere.cognitive.common.configuration.exceptions.NotContainerException;
import com.automationanywhere.cognitive.common.configuration.exceptions.NotValueException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.DecimalNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.FloatNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ShortNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.support.TaskExecutorAdapter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FileConfigurationProviderTest {

  private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
  private FileConfigurationProviderFactory factory = new FileConfigurationProviderFactory();

  private AsyncTaskExecutor te;
  private ExecutorService es;

  @BeforeMethod
  public void setUp() {
    this.es = Executors.newSingleThreadExecutor();
    this.te = new TaskExecutorAdapter(this.es);
  }

  @AfterMethod
  public void tearDown() throws Exception {
    this.es.shutdownNow();
    if (!this.es.awaitTermination(1000, TimeUnit.MILLISECONDS)) {
      throw new RuntimeException("Shutdown Timeout");
    }
  }

  @Test
  public void testGetValue() throws Exception {
    Path path = Files.createTempFile("test-", ".properties");
    File file = path.toFile();

    try {
      saveProps(file, "x: x");

      FileConfigurationProvider p = this.factory
          .create(this.mapper, file.getCanonicalPath(), this.te);

      Throwable throwable = catchThrowable(() -> p.getValue("a"));
      assertThat(throwable).isNotNull().isInstanceOf(NoSuchKeyException.class);

      saveProps(file, "a: value\nb:\n c: 10");
      p.refresh();
      assertThat((String) p.getValue("a")).isEqualTo("value");
      assertThat((Long) p.getValue("b.c")).isEqualTo(10L);

      saveProps(file, "a: value\nb:\n c: 11");
      p.refresh();
      assertThat((Long) p.getValue("b.c")).isEqualTo(11L);

      throwable = catchThrowable(() -> p.getValue("b"));
      assertThat(throwable).isNotNull().isInstanceOf(NotValueException.class);

      throwable = catchThrowable(() -> p.getValue("c.b"));
      assertThat(throwable).isNotNull().isInstanceOf(NoSuchKeyException.class);

      throwable = catchThrowable(() -> p.getValue("a.x"));
      assertThat(throwable).isNotNull().isInstanceOf(NotContainerException.class);

      saveProps(file, "a: value\nb: 12");
      p.refresh();
      assertThat((Long) p.getValue("b")).isEqualTo(12L);

      saveProps(file, "a: value\nb: data");
      p.refresh();
      assertThat((String) p.getValue("b")).isEqualTo("data");

    } finally {
      file.delete();
    }
  }

  @Test
  public void testEvents() throws Exception {
    Path path = Files.createTempFile("test-", ".properties");
    File file = path.toFile();
    List<AtomicInteger> errors = new ArrayList<>();

    try {
      this.saveProps(file, "a:\n b: 1");

      ConfigurationProvider p = this.factory.create(this.mapper, path.toFile().getCanonicalPath(), this.te);

      this.testEvent(
        file, p, errors, "a",
        "a:\n b: 2",
        new ConfigurationChangeEvent("a.b", 1L, 2L)
      );

      this.testEvent(
        file, p, errors, "a.b",
        "a:\n b: 1",
        new ConfigurationChangeEvent("a.b", 2L, 1L)
      );

      this.testEvent(
        file, p, errors, "a",
        "a:\n b: 1\n c: 8",
        new ConfigurationAddEvent("a.c", 8L)
      );

      this.testEvent(
        file, p, errors, "a.c",
        "a:\n b: 1",
        new ConfigurationRemoveEvent("a.c", 8L)
      );

      this.testEvent(
        file, p, errors, "a.b",
        "a:\n b:\n  c: 11\n  d: 12",
        new ConfigurationRemoveEvent("a.b", 1L),
        new ConfigurationAddEvent("a.b.c", 11L),
        new ConfigurationAddEvent("a.b.d", 12L)
      );

      this.testEvent(
        file, p, errors, "a.b",
        "a:\n b: 100",
        new ConfigurationRemoveEvent("a.b.c", 11L),
        new ConfigurationRemoveEvent("a.b.d", 12L),
        new ConfigurationAddEvent("a.b", 100L)
      );

      for (final AtomicInteger error : errors) {
        assertThat(error.get()).isEqualTo(0);
      }
    } finally {
      file.delete();
    }
  }

  @Test
  public void testGetValueFromDefaultConfig() throws Exception {
    File file = new File("./application.yaml");

    try {
      this.saveProps(file, "x: x");

      assertThat((String) this.factory.create(this.te).getValue("x")).isEqualTo("x");
    } finally {
      file.delete();
    }
  }

  @Test
  public void testRefreshFailsOnMissingFile() {
    FileConfigurationProvider p = new FileConfigurationProvider(new ObjectMapper(), UUID.randomUUID().toString());

    try {
      p.refresh();
    } catch (final Exception ex) {
      assertThat(ex.getCause()).isInstanceOf(IOException.class);
    }
  }

  @Test
  public void testSubscribe() {
    ConfigurationChangeListener l = mock(ConfigurationChangeListener.class);

    FileConfigurationProvider p = new FileConfigurationProvider(new ObjectMapper(), UUID.randomUUID().toString());
    assertThat(p.subscribe("key", l)).isSameAs(l);
  }

  @Test
  public void testUnsubscribe() {
    ConfigurationChangeListener l = mock(ConfigurationChangeListener.class);

    FileConfigurationProvider p = new FileConfigurationProvider(new ObjectMapper(), UUID.randomUUID().toString());
    assertThat(p.subscribe("key", l)).isSameAs(l);
    assertThat(p.subscribe("key2", l)).isSameAs(l);

    assertThat(p.unsubscribe("key", l)).isSameAs(l);
    assertThat(p.unsubscribe("key", l)).isNull();
    assertThat(p.unsubscribe("key2", l)).isSameAs(l);
    assertThat(p.unsubscribe("key2", l)).isNull();
  }

  @Test
  public void testNodeToValue() {
    FileConfigurationProvider p = new FileConfigurationProvider(new ObjectMapper(), UUID.randomUUID().toString());

    assertThat(p.getValue(NullNode.getInstance())).isNull();
    assertThat((Long) p.getValue(new BigIntegerNode(BigInteger.ONE))).isEqualTo(1L);
    assertThat((Long) p.getValue(new LongNode(2L))).isEqualTo(2L);
    assertThat((Long) p.getValue(new ShortNode((short) 3))).isEqualTo(3L);
    assertThat((Double) p.getValue(new DecimalNode(BigDecimal.TEN))).isCloseTo(10, offset(0.0001));
    assertThat((Double) p.getValue(new DoubleNode(11))).isCloseTo(11, offset(0.0001));
    assertThat((Double) p.getValue(new FloatNode(12))).isCloseTo(12, offset(0.0001));
    assertThat((Boolean) p.getValue(BooleanNode.getTrue())).isEqualTo(true);
  }

  private void saveProps(final File file, final String props) throws Exception {
    try (
      FileOutputStream os = new FileOutputStream(file);
    ) {
      os.write(props.getBytes(StandardCharsets.UTF_8));
    }

    Thread.sleep(500);
  }

  private void testEvent(final File file, final ConfigurationProvider p, final List<AtomicInteger> errors, final String key, final String text, final ConfigurationEvent... events) throws Exception {
    List<ConfigurationEvent> list = new ArrayList<>(Arrays.asList(events));
    CountDownLatch latch = new CountDownLatch(events.length);
    AtomicInteger error = new AtomicInteger(0);
    ConfigurationChangeListener[] l = {null};

    errors.add(error);

    l[0] = p.subscribe(key, (e) -> {
      if (latch.getCount() == 0) {
        error.incrementAndGet();
      }

      if (latch.getCount() == 1) {
        p.unsubscribe(key, l[0]);
      }

      assertThat(list.remove(e)).isTrue();

      latch.countDown();
    });

    this.saveProps(file, text);

    latch.await(10000, TimeUnit.MILLISECONDS);
    assertThat(latch.getCount()).isEqualTo(0);
  }

  @Test
  public void testListenerDetails() {
    ConfigurationChangeListener l = mock(ConfigurationChangeListener.class);
    ConfigurationChangeListener l2 = mock(ConfigurationChangeListener.class);

    FileConfigurationProvider.ListenerDetails ld = new FileConfigurationProvider.ListenerDetails("k", l);
    FileConfigurationProvider.ListenerDetails ld2 = new FileConfigurationProvider.ListenerDetails("k", l);
    FileConfigurationProvider.ListenerDetails ld3 = new FileConfigurationProvider.ListenerDetails("k", l2);
    FileConfigurationProvider.ListenerDetails ld4 = new FileConfigurationProvider.ListenerDetails("k1", l);

    assertThat(ld.equals(ld2)).isTrue();
    assertThat(ld.equals(ld3)).isFalse();
    assertThat(ld.equals(ld4)).isFalse();
    assertThat(ld.equals(new Object())).isFalse();
    assertThat(ld.equals(ld)).isTrue();
    assertThat(ld.equals(null)).isFalse();
  }

  @Test
  public void testPublishEvent() {
    FileConfigurationProvider p = new FileConfigurationProvider(null, null);
    ConfigurationChangeListener l = mock(ConfigurationChangeListener.class);
    ConfigurationAddEvent e = new ConfigurationAddEvent("k", "a");

    p.publishEvent(e, new FileConfigurationProvider.ListenerDetails("l", l));

    verify(l, never()).onEvent(e);

    p.publishEvent(e, new FileConfigurationProvider.ListenerDetails("k", l));

    verify(l).onEvent(e);
  }

  @Test
  public void testListenShouldFailOnTimeout() {
    FileConfigurationProvider p = spy(new FileConfigurationProvider(null, "fp"));

    FileChangeListener l = spy(FileChangeListener.class);

    doReturn(l).when(p).createFileChangeListener();

    doNothing().when(l).listen(eq(p), eq("fp"), any());

    try {
      p.listen(this.te);
      fail();
    } catch (final Exception ex) {
      assertThat(ex.getMessage()).isEqualTo("Timeout while waiting the listener to start");
    }

    verify(l).listen(any(), any(), any());
  }

  @Test
  public void testListenShouldFailOnInterruption() throws Throwable {
    FileConfigurationProvider p = spy(new FileConfigurationProvider(null, "fp"));

    FileChangeListener l = spy(FileChangeListener.class);

    doReturn(l).when(p).createFileChangeListener();

    doNothing().when(l).listen(eq(p), eq("fp"), any());

    CountDownLatch l1 = new CountDownLatch(1);
    CountDownLatch l2 = new CountDownLatch(1);
    Throwable[] e = {null};
    Future<?> f = this.te.submit(() -> {
      l1.countDown();

      try {
        try {
          p.listen(this.te);
          fail();
        } catch (final Exception ex) {
          assertThat(ex.getMessage()).isEqualTo("Interrupted while waiting the listener to start");
          l2.countDown();
        }
      } catch (final Throwable ex) {
        e[0] = ex;
        throw ex;
      }
    });

    if (!l1.await(1000, TimeUnit.MILLISECONDS)) {
      fail();
    }

    f.cancel(true);

    if (!l2.await(1000, TimeUnit.MILLISECONDS)) {
      if (e[0] != null) {
        throw e[0];
      }
      fail();
    }
  }

  @Test
  public void testShutdown() throws Throwable {
    FileConfigurationProvider p = spy(new FileConfigurationProvider(null, "fp"));

    p.shutdown();

    FileChangeListener l = spy(FileChangeListener.class);

    doReturn(l).when(p).createFileChangeListener();

    CountDownLatch l0 = new CountDownLatch(1);

    doAnswer(ctx -> {
      ((Runnable) ctx.getArguments()[2]).run();

      synchronized (this) {
        try {
          this.wait(1000);
        } catch (final InterruptedException ex) {
          l0.countDown();
        }
      }

      return null;
    }).when(l).listen(eq(p), eq("fp"), any());

    p.listen(this.te);

    p.shutdown();

    assertThat(l0.await(1000, TimeUnit.MILLISECONDS)).isTrue();
  }

  @Test
  public void testListenShouldNotInitializeTwice() {
    FileConfigurationProvider p = spy(new FileConfigurationProvider(null, "fp"));

    FileChangeListener l = spy(FileChangeListener.class);

    doReturn(l).when(p).createFileChangeListener();

    doAnswer(ctx -> {
      ((Runnable) ctx.getArguments()[2]).run();
      return null;
    }).when(l).listen(eq(p), eq("fp"), any());

    p.listen(this.te);

    verify(l).listen(any(), any(), any());
    verify(l).listen(eq(p), eq("fp"), any());

    p.listen(this.te);

    verify(l).listen(any(), any(), any());
  }
}
