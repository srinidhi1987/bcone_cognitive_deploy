package com.automationanywhere.cognitive.common.domain.service.security;

import static org.assertj.core.api.Assertions.assertThat;
import com.automationanywhere.cognitive.common.domain.service.security.exceptions.BadSignatureException;
import com.automationanywhere.cognitive.common.domain.service.security.exceptions.CipherException;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.SymmetricKey;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.bind.DatatypeConverter;
import org.mockito.Mockito;
import org.testng.annotations.Test;

public class SecurityTest {

  @Test
  public void testEncryptAndDecrypt() throws Exception {

    // given
    KeyProvider keyProvider = Mockito.mock(KeyProvider.class);
    CryptographyKey cryptographyKey = new SymmetricKey("ZDiRkDuC77xxFnoA867dd/WHe4ABH5BOnsZtS49eqy0=");
    Mockito.when(keyProvider.getKey(Mockito.any())).thenReturn(cryptographyKey);
    Security security = new Security(keyProvider);
    String secretData = "Secret Data";

    // when
    InputStream actualEncryptedStream = security.encrypt(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, new ByteArrayInputStream(secretData.getBytes()));

    // then
    assertThat(actualEncryptedStream).isNotNull();

    byte[] enc = null;
    enc = streamToByteArray(actualEncryptedStream);
    String actualEncryptedData = DatatypeConverter.printBase64Binary(enc);
    assertThat(actualEncryptedData).isNotEqualTo(secretData);

    // given
    InputStream encryptedDataStream = new ByteArrayInputStream(
        DatatypeConverter.parseBase64Binary(actualEncryptedData));

    // when
    InputStream actualDecryptedStream = security.decrypt(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, encryptedDataStream);

    // then
    assertThat(actualDecryptedStream).isNotNull();
    String actualDecryptedData = streamToString(actualDecryptedStream);
    assertThat(actualDecryptedData).isEqualTo(secretData);
  }

  @Test(expectedExceptions = CipherException.class)
  public void testEncryptAndDecryptWhenIncorrectKey() {
    // given
    KeyProvider keyProvider = Mockito.mock(KeyProvider.class);
    CryptographyKey cryptographyKey = new SymmetricKey("ZDiRkDuC77xxFnoA867dd/WHe4ABH5BOnsZtS49eqy0=121212");
    Mockito.when(keyProvider.getKey(Mockito.any())).thenReturn(cryptographyKey);
    Security security = new Security(keyProvider);
    String secretData = "Secret Data";

    // when
    InputStream actualEncryptedStream = security.encrypt(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, new ByteArrayInputStream(secretData.getBytes()));

    // then
    assertThat(actualEncryptedStream).isNotNull();

    byte[] enc = null;
    enc = streamToByteArray(actualEncryptedStream);
    String actualEncryptedData = DatatypeConverter.printBase64Binary(enc);
    assertThat(actualEncryptedData).isNotEqualTo(secretData);

    // given
    InputStream encryptedDataStream = new ByteArrayInputStream(
        DatatypeConverter.parseBase64Binary(actualEncryptedData));

    // when
    InputStream actualDecryptedStream = security.decrypt(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, encryptedDataStream);

    // then
    assertThat(actualDecryptedStream).isNotNull();
    String actualDecryptedData = streamToString(actualDecryptedStream);
    assertThat(actualDecryptedData).isEqualTo(secretData);
  }

  @Test(expectedExceptions = BadSignatureException.class)
  public void testEncryptAndDecryptWhenIncorrectEncryptedStream() {
    // given
    KeyProvider keyProvider = Mockito.mock(KeyProvider.class);
    CryptographyKey cryptographyKey = new SymmetricKey("ZDiRkDuC77xxFnoA867dd/WHe4ABH5BOnsZtS49eqy0=");
    Mockito.when(keyProvider.getKey(Mockito.any())).thenReturn(cryptographyKey);
    Security security = new Security(keyProvider);
    String secretData = "Secret Data";
    String corruptedEncryptedStream = "12132pZFEUgiXGxrIiwxDAE7KTqlbkY3OdPt3RSre8x7oB/jl/9ff7OWvlbPAHn+GTyuz1212345345345";

    // given
    InputStream encryptedDataStream = new ByteArrayInputStream(
        DatatypeConverter.parseBase64Binary(corruptedEncryptedStream));

    // when
    InputStream actualDecryptedStream = security.decrypt(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, encryptedDataStream);

    // then
    // exception is thrown
  }

  @Test(expectedExceptions = CipherException.class)
  public void testEncryptAndDecryptWhenIncorrectEncryptedStreamWithWrongIV() {
    // given
    KeyProvider keyProvider = Mockito.mock(KeyProvider.class);
    CryptographyKey cryptographyKey = new SymmetricKey("ZDiRkDuC77xxFnoA867dd/WHe4ABH5BOnsZtS49eqy0=");
    Mockito.when(keyProvider.getKey(Mockito.any())).thenReturn(cryptographyKey);
    Security security = new Security(keyProvider);
    String secretData = "Secret Data";
    String streamWithWrongIV = "8123768174681746";

    // given
    InputStream encryptedDataStream = new ByteArrayInputStream(
        DatatypeConverter.parseBase64Binary(streamWithWrongIV));

    // when
    InputStream actualDecryptedStream = security.decrypt(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, encryptedDataStream);

    // then
    // exception is thrown
  }

  /**
   * @return String converted from inputStream
   */
  private String streamToString(final InputStream inputStream) {
    StringBuilder stringBuilder = new StringBuilder();
    try (BufferedReader bufferedStreamReader = new BufferedReader(
        new InputStreamReader(inputStream))) {
      String line;

      while ((line = bufferedStreamReader.readLine()) != null) {
        stringBuilder.append(line);
      }
    } catch (IOException ioe) {
      throw new RuntimeException("Error while converting InputStream to String", ioe);
    }
    return stringBuilder.toString();
  }

  /**
   * @return byte array converted from inputStream
   */
  public byte[] streamToByteArray(final InputStream inputStream) {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    final int BYTE_BUFFER_SIZE = 1024;
    byte[] byteArray = null;
    try {
      int nRead;
      byte[] data = new byte[BYTE_BUFFER_SIZE];
      while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
        buffer.write(data, 0, nRead);
      }

      buffer.flush();
      byteArray = buffer.toByteArray();
    } catch (IOException ioe) {
      throw new RuntimeException(
          "Error while converting the input stream into byte array.", ioe);
    }
    return byteArray;
  }
}