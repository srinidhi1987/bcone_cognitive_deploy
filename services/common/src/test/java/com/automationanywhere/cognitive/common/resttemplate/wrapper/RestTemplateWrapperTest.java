package com.automationanywhere.cognitive.common.resttemplate.wrapper;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RestTemplateWrapperTest {
    @Mock
    RestTemplate restTemplate;
    @InjectMocks
    RestTemplateWrapper restTemplateWrapper;
    
    @BeforeTest
    public void setUp(){
        this.restTemplate = new RestTemplate();
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfExchangeGetWhenRestTemplateExchangeReturnsOK(){
        //given
        ResponseEntity<String> standardResponseResponseEntity = new ResponseEntity<String>(HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(),Mockito.any(),Mockito.any(),Mockito.<Class<String>> any())).thenReturn(standardResponseResponseEntity);
        
        //when
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntityActual = (ResponseEntity<String>) restTemplateWrapper.exchangeGet("", Mockito.<Class<String>> any());
        
        //then
        assertThat(standardResponseResponseEntity).isEqualTo(standardResponseResponseEntityActual);
        
    }
    
    @Test(expectedExceptions = {Exception.class})
    public void testOfExchangeGetWhenRestTemplateExchangeThrowsException(){
        //given
        ResponseEntity<String> standardResponseResponseEntity = new ResponseEntity<String>(HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(),Mockito.any(),Mockito.any(),Mockito.<Class<String>> any())).thenThrow(Exception.class);
        
        //when
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntityActual = (ResponseEntity<String>) restTemplateWrapper.exchangeGet("", Mockito.<Class<String>> any());
        
        //then
        //throw Exception
        
    }
}
