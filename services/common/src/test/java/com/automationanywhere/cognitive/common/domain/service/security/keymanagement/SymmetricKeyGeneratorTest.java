package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

public class SymmetricKeyGeneratorTest {

  @Test
  public void testGenerateKey() throws Exception {

    // given
    SymmetricKeyGenerator symmetricKeyGenerator = new SymmetricKeyGenerator();

    // when
    CryptographyKey cryptographyKey = symmetricKeyGenerator.generateKey();

    // then
    assertThat(cryptographyKey).isNotNull();
    assertTrue(cryptographyKey instanceof SymmetricKey);
    SymmetricKey symmetricKey = (SymmetricKey) cryptographyKey;
    assertThat(symmetricKey.key()).isNotNull();
  }

}