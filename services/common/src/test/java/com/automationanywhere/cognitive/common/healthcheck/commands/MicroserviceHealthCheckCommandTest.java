package com.automationanywhere.cognitive.common.healthcheck.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.net.ConnectException;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFutureTask;
import org.springframework.web.client.AsyncRestTemplate;
import org.testng.annotations.Test;

public class MicroserviceHealthCheckCommandTest {

  @Test
  public void executeCheckSuccessful() throws ExecutionException, InterruptedException {
    // GIVEN
    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String name = "name";
    String host = "URL";
    String upTime = "123";
    String version = "123";
    String branch = "123";
    String git = "123";
    String buildTime = "123";

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        version,
        branch,
        git,
        buildTime
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        upTime
    ));
    application.setDependencies(Collections.singletonList(new Dependency("database", Status.OK)));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    String url = host + "/healthcheck";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<Application> entity = new HttpEntity<>(headers);

    ListenableFutureTask<ResponseEntity<Application>> futureTask = new ListenableFutureTask<>(
        () -> new ResponseEntity<>(application, HttpStatus.OK));
    doReturn(futureTask).when(restTemplate).exchange(url, HttpMethod.GET, entity, Application.class);

    MicroserviceHealthCheckCommand command = new MicroserviceHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }

  @Test
  public void executeCheckFailure() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/healthcheck";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<Application> entity = new HttpEntity<>(headers);

    MicroserviceHealthCheckCommand command = new MicroserviceHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    ListenableFutureTask<ResponseEntity<Application>> futureTask = spy(new ListenableFutureTask<>(
        () -> new ResponseEntity<>(new Application(), HttpStatus.INTERNAL_SERVER_ERROR)));

    doReturn(futureTask).when(restTemplate)
        .exchange(url, HttpMethod.GET, entity, Application.class);
    ExecutionException ex = new ExecutionException("Error to execute",
        new ConnectException("Connection Exception"));
    doThrow(ex).when(futureTask).get();

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        "",
        "",
        "",
        ""
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        command.getType().name(),
        Status.NOT_OK,
        "Connection Exception",
        ""
    ));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }
}
