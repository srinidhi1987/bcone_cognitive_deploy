package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.ByteArrayInputStream;
import java.util.Random;
import org.testng.annotations.Test;

public class InputStreamWithHeaderTest {

  @Test
  public void readShouldThrowExceptionOnNegativeOffset() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] b = new byte[h.length];
    InputStreamWithHeader is = new InputStreamWithHeader(h, null);

    // when
    Throwable ex = catchThrowable(() -> is.read(b, -1, 1));

    // then
    assertThat(ex).isInstanceOf(IndexOutOfBoundsException.class);
    assertThat(ex.getMessage()).isEqualTo(
        "the offset should be greater or equal to zero, but it's -1"
    );
  }

  @Test
  public void readShouldThrowExceptionOnNegativeLength() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] b = new byte[h.length];
    InputStreamWithHeader is = new InputStreamWithHeader(h, null);

    // when
    Throwable ex = catchThrowable(() -> is.read(b, 0, -1));

    // then
    assertThat(ex).isInstanceOf(IndexOutOfBoundsException.class);
    assertThat(ex.getMessage()).isEqualTo(
        "the length should be greater or equal to zero, but it's -1"
    );
  }

  @Test
  public void readShouldThrowExceptionOnIndexesBeyondTheEndOfBuffer() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] b = new byte[h.length];
    InputStreamWithHeader is = new InputStreamWithHeader(h, null);

    // when
    Throwable ex = catchThrowable(() -> is.read(b, 1, h.length));

    // then
    assertThat(ex).isInstanceOf(IndexOutOfBoundsException.class);
    assertThat(ex.getMessage()).isEqualTo(
        "the last byte should be less or equal to 15 but it's 1 + 16 - 1 = 16"
    );
  }

  @Test
  public void readShouldDoNothingOnZeroLengthReads() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] b = new byte[h.length];
    byte[] c = new byte[h.length];

    new Random().nextBytes(h);
    for (int i = 0; i < h.length; i += 1) {
      c[i] = b[i] = (byte) ~h[i];
    }

    InputStreamWithHeader is = new InputStreamWithHeader(h, null);

    // when
    int n = is.read(b, 0, 0);

    // then
    assertThat(n).isEqualTo(0);
    assertThat(b).isEqualTo(c);
  }

  @Test
  public void readShouldNotReadDataIfTheBufferSizeIsEqualToTheHeaderSize() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] b = new byte[h.length + 1];

    Random random = new Random();
    random.nextBytes(h);
    for (int i = 0; i < h.length; i += 1) {
      b[i] = (byte) ~h[i];
    }
    byte last = (byte) (random.nextInt(256) - 128);
    b[b.length - 1] = last;

    InputStreamWithHeader is = new InputStreamWithHeader(h, null);

    // when
    int n = is.read(b, 0, h.length);

    // then
    assertThat(n).isEqualTo(h.length);
    assertThat(b[b.length - 1]).isEqualTo(last);

    for (int i = 0; i < h.length; i += 1) {
      assertThat(b[i]).isEqualTo(h[i]);
    }
  }

  @Test
  public void readShouldDoPartialReadsOfHeaderAndData() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] d = new byte[16];
    byte[] b = new byte[h.length + d.length];

    Random random = new Random();
    random.nextBytes(h);
    random.nextBytes(d);
    for (int i = 0; i < h.length; i += 1) {
      b[i] = (byte) ~h[i];
    }
    for (int i = h.length; i < h.length + d.length; i += 1) {
      b[i] = (byte) ~d[i - h.length];
    }

    InputStreamWithHeader is = new InputStreamWithHeader(h, new ByteArrayInputStream(d));

    // when
    int n = is.read(b, 0, (h.length + d.length) / 3);
    n += is.read(b, (h.length + d.length) / 3, (h.length + d.length) / 3);
    n += is.read(b, 2 * ((h.length + d.length) / 3),
        h.length + d.length - 2 * ((h.length + d.length) / 3));

    // then
    assertThat(n).isEqualTo(h.length + d.length);

    for (int i = 0; i < h.length; i += 1) {
      assertThat(b[i]).isEqualTo(h[i]);
    }

    for (int i = 0; i < d.length; i += 1) {
      assertThat(b[i + h.length]).isEqualTo(d[i]);
    }
  }

  @Test
  public void readShouldSupportEmptyStreams() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] b = new byte[h.length + 1];

    Random random = new Random();
    random.nextBytes(h);
    for (int i = 0; i < h.length; i += 1) {
      b[i] = (byte) ~h[i];
    }
    byte last = (byte) (random.nextInt(256) - 128);
    b[b.length - 1] = last;

    InputStreamWithHeader is = new InputStreamWithHeader(h, new ByteArrayInputStream(new byte[0]));

    // when
    int n = is.read(b, 0, b.length);

    // then
    assertThat(n).isEqualTo(h.length);
    assertThat(b[b.length - 1]).isEqualTo(last);

    for (int i = 0; i < h.length; i += 1) {
      assertThat(b[i]).isEqualTo(h[i]);
    }
  }

  @Test
  public void availableShouldIncludeAllTheHeaderLength() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] d = new byte[16];

    InputStreamWithHeader is = new InputStreamWithHeader(h, new ByteArrayInputStream(d));

    // when
    int r = is.available();

    // then
    assertThat(r).isEqualTo(h.length + d.length);
  }

  @Test
  public void availableShouldIncludePartOfTheHeaderLength() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] d = new byte[16];

    InputStreamWithHeader is = new InputStreamWithHeader(h, new ByteArrayInputStream(d));

    // when

    is.read();
    int r = is.available();

    // then
    assertThat(r).isEqualTo((h.length - 1) + d.length);
  }

  @Test
  public void availableShouldIncludeOnlyTheDataLength() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] d = new byte[16];

    InputStreamWithHeader is = new InputStreamWithHeader(h, new ByteArrayInputStream(d));

    // when
    for (int i = 0; i < h.length; i += 1) {
      is.read();
    }
    int r = is.available();

    // then
    assertThat(r).isEqualTo(d.length);
  }

  @Test
  public void availableShouldIncludeOnlyThePartOfTheDataLength() throws Exception {
    // given
    byte[] h = new byte[16];
    byte[] d = new byte[16];

    InputStreamWithHeader is = new InputStreamWithHeader(h, new ByteArrayInputStream(d));

    // when
    for (int i = 0; i < h.length; i += 1) {
      is.read();
    }
    is.read();

    int r = is.available();

    // then
    assertThat(r).isEqualTo(d.length - 1);
  }

  @Test
  public void readShouldNotReturnEOS() throws Exception {
    // given
    byte[] h = new byte[]{(byte) -1};

    InputStreamWithHeader is = new InputStreamWithHeader(h, null);

    // when
    int i = is.read();

    // then
    assertThat(i).isNotEqualTo(-1);
    assertThat((byte) i).isEqualTo((byte) 0x0000_00FF);
  }
}
