package com.automationanywhere.cognitive.common.healthcheck.health;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.common.healthcheck.commands.SimpleHttpStatusCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.components.SimpleHealthComponent;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import org.testng.annotations.Test;

public class SimpleHealthComponentTest {

  @Test
  public void executeHealthCheckWithoutCommands() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.emptyList());

    BuildInfoFactory buildInfoFactory = mock(BuildInfoFactory.class);
    when(buildInfoFactory.build()).thenReturn(application.getBuildInfo());

    AppInfoFactory appInfoFactory = mock(AppInfoFactory.class);
    when(appInfoFactory.build(
        application.getBuildInfo().getAppName(),
        "",
        Status.OK,
        null
    )).thenReturn(application.getAppInfo());

    SimpleHealthComponent component = new SimpleHealthComponent(
        buildInfoFactory,
        appInfoFactory
    );

    // WHEN
    CompletableFuture<Application> future = component.executeHealthCheck(Collections.emptyList());

    //THEN
    assertThat(future.get()).isEqualToComparingFieldByFieldRecursively(application);
  }

  @Test
  public void executeHealthCheckWithOneSubsystemCommand() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.singletonList(buildSubsystem()));
    AppInfo appInfo = application.getAppInfo();

    BuildInfoFactory buildInfoFactory = mock(BuildInfoFactory.class);
    when(buildInfoFactory.build()).thenReturn(application.getBuildInfo());

    AppInfoFactory appInfoFactory = mock(AppInfoFactory.class);
    when(appInfoFactory.build(
        appInfo.getName(),
        "",
        appInfo.getStatus(),
        null
    )).thenReturn(appInfo);

    SimpleHttpStatusCheckCommand subsystemCommand = mock(SimpleHttpStatusCheckCommand.class);
    when(subsystemCommand.executeCheck()).thenReturn(
        CompletableFuture.completedFuture(new ApplicationHealthCheckStatus(buildSubsystem()))
    );

    SimpleHealthComponent component = new SimpleHealthComponent(
        buildInfoFactory,
        appInfoFactory
    );

    // WHEN
    CompletableFuture<Application> future = component.executeHealthCheck(
        Collections.singletonList(subsystemCommand)
    );

    //THEN
    assertThat(future.get()).isEqualToComparingFieldByFieldRecursively(application);
  }

  @Test
  public void testHealthCheckConvertWithoutSubsystemsDependencies() throws Exception {
    // GIVEN
    Application application = buildApplication();
    String expected = "{\"success\":false,\"data\":{\"serviceStatuses\":[]},\"errors\":\"\"}";

    SimpleHealthComponent component = new SimpleHealthComponent(null, null);

    //WHEN
    String result = component.convert(application);

    // THEN
    assertThat(result).isEqualTo(expected);
  }

  @Test
  public void testHealthCheckConvertWithoutDependenciesWithOneSubsystem() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.singletonList(buildSubsystem()));
    application.setDependencies(Collections.emptyList());

    String expected = "{\"success\":false,\"data\":{\"serviceStatuses\":[{\"name\":\"subsystem\",\"statusCode\":500}]},\"errors\":\"\"}";

    SimpleHealthComponent component = new SimpleHealthComponent(null, null);

    //WHEN
    String result = component.convert(application);

    // THEN
    assertThat(result).isEqualTo(expected);
  }

  private Application buildApplication() {
    Application application = new Application();
    application.setAppInfo(new AppInfo(
        "name",
        "",
        Status.NOT_OK,
        "",
        "2018-03-20 11:20:32.234"
    ));
    application.setBuildInfo(new BuildInfo(
        "name",
        "1.0",
        "1234",
        "12345678",
        "2018-03-20"
    ));
    return application;
  }

  private Application buildSubsystem() {
    Application subsystem = new Application();
    subsystem.setAppInfo(new AppInfo(
        "subsystem",
        "APPLICATION",
        Status.NOT_OK,
        "Error",
        "2017-01-12 07:13:11.456"
    ));
    subsystem.setBuildInfo(new BuildInfo(
        "subsystem",
        "1.1",
        "12345",
        "123456789",
        "2017-01-12"
    ));
    return subsystem;
  }
}
