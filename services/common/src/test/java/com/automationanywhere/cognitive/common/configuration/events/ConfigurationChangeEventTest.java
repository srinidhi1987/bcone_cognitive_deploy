package com.automationanywhere.cognitive.common.configuration.events;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigurationChangeEventTest {
  @Test
  public void testEquals() {
    ConfigurationChangeEvent e = new ConfigurationChangeEvent("k", "v1", "v2");
    ConfigurationChangeEvent e2 = new ConfigurationChangeEvent("k", "v1", "v2");
    ConfigurationChangeEvent e3 = new ConfigurationChangeEvent("k", "va", "v2");
    ConfigurationChangeEvent e4 = new ConfigurationChangeEvent("k", "v1", "vb");
    ConfigurationChangeEvent e5 = new ConfigurationChangeEvent("z", "v1", "v2");

    assertThat(e.equals(e)).isTrue();
    assertThat(e.equals(e2)).isTrue();
    assertThat(e.equals(null)).isFalse();
    assertThat(e.equals(new Object())).isFalse();
    assertThat(e.equals(e3)).isFalse();
    assertThat(e.equals(e4)).isFalse();
    assertThat(e.equals(e5)).isFalse();

    assertThat(e.hashCode()).isEqualTo(e2.hashCode());
  }
}
