package com.automationanywhere.cognitive.common.security;

import static com.automationanywhere.cognitive.common.security.StreamKeyProvider.MAX_KEY_SIZE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class StreamKeyProviderTest {

  private InputStream is = null;

  @AfterMethod
  public void tearDown() throws Exception {
    // We have to release resources acquired in tests
    if (is != null) {
      is.close();
    }
  }

  @Test
  public void streamKeyProviderShouldThrowExceptionIfAKeyIsTooLong() throws Exception {
    // given
    is = new ByteArrayInputStream(new byte[MAX_KEY_SIZE + 1]);

    // when
    Throwable ex = catchThrowable(() -> new StreamKeyProvider("AES", is));

    // then
    assertThat(ex).isInstanceOf(CipherException.class);
    assertThat(ex.getMessage()).isEqualTo(
        "Key is too long, the supported max key size is " + MAX_KEY_SIZE
    );
  }

  @Test
  public void streamKeyProviderShouldThrowExceptionIfAStreamIsBroken() throws IOException {
    // given
    InputStream is = mock(InputStream.class);
    IOException e = new IOException();

    doThrow(e).when(is).read(any(), anyInt(), anyInt());

    // when
    Throwable ex = catchThrowable(() -> new StreamKeyProvider("AES", is));

    // then
    assertThat(ex.getCause()).isSameAs(e);
  }
}
