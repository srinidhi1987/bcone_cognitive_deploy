package com.automationanywhere.cognitive.common.healthapi.json.models;
import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
public class HeartBeatTest {

    @Test
    public void testOfToStringWithBuildInfoNull(){
        
        //given
        BuildInfo buildInfo = null;
        HeartBeat heartBeat = new HeartBeat();
        heartBeat.setBuildInfo(buildInfo);
        heartBeat.setHttpStatusCode(HTTPStatusCode.OK);
        
        //when
        String heartbeat = heartBeat.toString();
        
        //then
        assertThat(heartbeat).contains("BuildInfo=Error reading build.version file");
    }
    
    @Test
    public void testOfToStringWithBuildInfoIsNotNull(){
        
        //given
        BuildInfo buildInfo = new BuildInfo();
        buildInfo.setApplication("AppName");
        HeartBeat heartBeat = new HeartBeat();
        heartBeat.setBuildInfo(buildInfo);
        heartBeat.setHttpStatusCode(HTTPStatusCode.OK);
        
        //when
        String heartbeat = heartBeat.toString();
        
        //then
        assertThat(heartbeat).doesNotContain("BuildInfo=Error reading build.version file");
        assertThat(heartbeat).contains("Application: AppName");
    }
}
