package com.automationanywhere.cognitive.common.healthapi.responsebuilders;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.testng.annotations.Test;

import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.json.models.BuildInfo;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;

public class HealthApiResponseBuilderTest {

    @Test
    public void testOfPrepareHeartBeat() {
        // given when
        String heartBeat = HealthApiResponseBuilder.prepareHeartBeat(
                HTTPStatusCode.OK, SystemStatus.ONLINE);

        // then
        assertThat(heartBeat)
                .contains(
                        "Application: null\nStatus: INTERNAL_SERVER_ERROR\nReason: Internal server error\nBuildInfo=Error reading build.version file\nApplication uptime: ");
    }
/*		//when
		BuildInfo buildInfo = HealthApiResponseBuilder.getBuildInfo();
    }*/

    @Test
    public void testOfPrepareBuildInfo() {
        // given
        Properties prop = new Properties();
        prop.put("Application-name", "filemanager");
        prop.put("Version", "1.0.0");
        prop.put("Branch", "develop");
        prop.put("GitHash", "121212");
        prop.put("Buildtime", "2017-08-07T11:14:31.059Z");

        // when
        BuildInfo buildInfo = HealthApiResponseBuilder.prepareBuildInfo(prop);
        assertThat(buildInfo).isNotNull();

        // then
        assertThat(buildInfo.toString()).isEqualTo("Version: 1.0.0\nBranch: develop\nGIT #: 121212\nBuild Time: 2017-08-07T11:14:31.059Z");

    }
    
    @Test
    public void testOfPrepareSubsystemWhenOnlyDBDependencyApplicable() {
        // given
        HTTPStatusCode httpStatusCode = HTTPStatusCode.OK;
        SystemStatus systemStatus = SystemStatus.ONLINE;
        Connectivity dbConnectivity = Connectivity.OK;
        
        // when
        String subSystem = HealthApiResponseBuilder.prepareSubSystem
                (httpStatusCode, systemStatus, dbConnectivity, Connectivity.NOT_APPLICABLE,Connectivity.NOT_APPLICABLE, 
                        new ArrayList<ServiceConnectivity>());

        // then
        assertThat(subSystem).contains("\nDependencies:\n  Database Connectivity: OK   ");

    }
    
    @Test
    public void testOfPrepareSubsystemWhenBothDBAndMQDependencyApplicable() {
        // given
        HTTPStatusCode httpStatusCode = HTTPStatusCode.OK;
        SystemStatus systemStatus = SystemStatus.ONLINE;
        Connectivity dbConnectivity = Connectivity.OK;
        Connectivity mqConnectivity = Connectivity.FAILURE;
        
        // when
        String subSystem = HealthApiResponseBuilder.prepareSubSystem
                (httpStatusCode, systemStatus, dbConnectivity, mqConnectivity, Connectivity.NOT_APPLICABLE, 
                        new ArrayList<ServiceConnectivity>());

        // then
        assertThat(subSystem).contains("Dependencies:\n  Database Connectivity: OK \n  MessageQueue Connectivity: FAILURE  ");

    }
    
    @Test
    public void testOfPrepareSubsystemWhenAllDBAndMQAndOtherServiceDependencyApplicable() {
        // given
        HTTPStatusCode httpStatusCode = HTTPStatusCode.OK;
        SystemStatus systemStatus = SystemStatus.ONLINE;
        Connectivity dbConnectivity = Connectivity.OK;
        Connectivity mqConnectivity = Connectivity.FAILURE;
        List<ServiceConnectivity> svcConnectivityList = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity projectService = new ServiceConnectivity("Project", HTTPStatusCode.OK);
        ServiceConnectivity fileService = new ServiceConnectivity("FileManager", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        svcConnectivityList.add(projectService);
        svcConnectivityList.add(fileService);
        // when
        String subSystem = HealthApiResponseBuilder.prepareSubSystem
                (httpStatusCode, systemStatus, dbConnectivity, mqConnectivity, Connectivity.NOT_APPLICABLE, 
                        svcConnectivityList);

        // then
        assertThat(subSystem).contains("Dependencies:\n  Database Connectivity: OK \n  MessageQueue Connectivity: FAILURE  \n  Project: OK\n  FileManager: INTERNAL_SERVER_ERROR");

    }
    
    @Test
    public void testOfPrepareSubsystemWhenCRDependencyApplicableAndOK() {
        // given
        HTTPStatusCode httpStatusCode = HTTPStatusCode.OK;
        SystemStatus systemStatus = SystemStatus.ONLINE;
        Connectivity dbConnectivity = Connectivity.NOT_APPLICABLE;
        Connectivity mqConnectivity = Connectivity.NOT_APPLICABLE;
        Connectivity crConnectivity = Connectivity.OK;
        // when
        String subSystem = HealthApiResponseBuilder.prepareSubSystem
                (httpStatusCode, systemStatus, dbConnectivity, mqConnectivity, crConnectivity, 
                        null);

        // then
        assertThat(subSystem).contains("Control Room Connectivity: OK ");

    }
    
    @Test
    public void testOfPrepareSubsystemWhenCRDependencyApplicableAndNotOK() {
        // given
        HTTPStatusCode httpStatusCode = HTTPStatusCode.OK;
        SystemStatus systemStatus = SystemStatus.ONLINE;
        Connectivity dbConnectivity = Connectivity.NOT_APPLICABLE;
        Connectivity mqConnectivity = Connectivity.NOT_APPLICABLE;
        Connectivity crConnectivity = Connectivity.FAILURE;
        // when
        String subSystem = HealthApiResponseBuilder.prepareSubSystem
                (httpStatusCode, systemStatus, dbConnectivity, mqConnectivity, crConnectivity, 
                        null);

        // then
        assertThat(subSystem).contains("Control Room Connectivity: FAILURE ");

    }
}
