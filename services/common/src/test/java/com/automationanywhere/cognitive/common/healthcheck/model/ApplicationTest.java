package com.automationanywhere.cognitive.common.healthcheck.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class ApplicationTest {

  @Test
  public void addSubsystemShouldAddAnElement() {
    // given
    Application sub = new Application();
    Application app = new Application();

    // when
    app.addSubsystem(sub);

    // then
    assertThat(app.getSubsystems()).containsExactlyInAnyOrder(sub);
  }

  @Test
  public void addDependencyShouldAddAnElement() {
    // given
    Dependency dep = new Dependency("d", Status.OK);
    Application app = new Application();

    // when
    app.addDependency(dep);

    // then
    assertThat(app.getDependencies()).containsExactlyInAnyOrder(dep);
  }
}
