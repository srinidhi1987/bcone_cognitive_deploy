package com.automationanywhere.cognitive.common.healthcheck.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFutureTask;
import org.springframework.web.client.AsyncRestTemplate;
import org.testng.annotations.Test;

public class MicroserviceTextHealthCheckCommandTest {

  @Test
  public void executeCheckSuccessful() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";
    String upTime = "123";
    String version = "123";
    String branch = "123";
    String git = "123";
    String buildTime = "123";
    String microserviceResp = "Subsystem \n"
        + "Application: " + name + " \n"
        + "Status: OK \n"
        + "Application uptime: " + upTime + " \n"
        + "Version: " + version + " \n"
        + "Branch: " + branch + " \n"
        + "GIT #: " + git + " \n"
        + "Build Time: " + buildTime + " \n"
        + "Dependencies \n"
        + "database: OK";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/healthcheck";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<>(headers);

    ListenableFutureTask<ResponseEntity<String>> futureTask = new ListenableFutureTask<>(
        () -> new ResponseEntity<>(microserviceResp, HttpStatus.OK));
    doReturn(futureTask).when(restTemplate).exchange(url, HttpMethod.GET, entity, String.class);

    MicroserviceTextHealthCheckCommand command = new MicroserviceTextHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        version,
        branch,
        git,
        buildTime
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        upTime
    ));
    application.setDependencies(Collections.singletonList(new Dependency("database", Status.OK)));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }

  @Test
  public void executeCheckShouldSetNotOKIfStatusIsFailure() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";
    String upTime = "123";
    String version = "123";
    String branch = "123";
    String git = "123";
    String buildTime = "123";
    String microserviceResp = "Subsystem \n"
        + "Application: " + name + " \n"
        + "Status: FAILURE \n"
        + "Application uptime: " + upTime + " \n"
        + "Version: " + version + " \n"
        + "Branch: " + branch + " \n"
        + "GIT #: " + git + " \n"
        + "Build Time: " + buildTime + " \n"
        + "Dependencies \n"
        + "database: FAILURE";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/healthcheck";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<>(headers);

    ListenableFutureTask<ResponseEntity<String>> futureTask = new ListenableFutureTask<>(
        () -> new ResponseEntity<>(microserviceResp, HttpStatus.OK));
    doReturn(futureTask).when(restTemplate).exchange(url, HttpMethod.GET, entity, String.class);

    MicroserviceTextHealthCheckCommand command = new MicroserviceTextHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        version,
        branch,
        git,
        buildTime
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        CommandType.APPLICATION.name(),
        Status.NOT_OK,
        "",
        upTime
    ));
    application.setDependencies(Collections.singletonList(new Dependency("database", Status.NOT_OK)));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }

  @Test
  public void executeCheckShouldReturnEmptyAppIfBodyIsEmpty() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/healthcheck";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<>(headers);

    ListenableFutureTask<ResponseEntity<String>> futureTask = new ListenableFutureTask<>(
        () -> new ResponseEntity<>("", HttpStatus.OK));
    doReturn(futureTask).when(restTemplate).exchange(url, HttpMethod.GET, entity, String.class);

    MicroserviceTextHealthCheckCommand command = new MicroserviceTextHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    Application application = new Application(null, null, null, new ArrayList<>());
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }

  @Test
  public void executeCheckFailure() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/healthcheck";

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<>(headers);

    MicroserviceTextHealthCheckCommand command = new MicroserviceTextHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    ListenableFutureTask<ResponseEntity<String>> futureTask = spy(new ListenableFutureTask<>(
        () -> new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR)));

    doReturn(futureTask).when(restTemplate).exchange(url, HttpMethod.GET, entity, String.class);
    ExecutionException ex = new ExecutionException("Error to execute", new ConnectException("Connection Exception"));
    doThrow(ex).when(futureTask).get();

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        "",
        "",
        "",
        ""
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        command.getType().name(),
        Status.NOT_OK,
        "Connection Exception",
        ""
    ));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }

  @Test
  public void onSuccessShouldCompleteExceptionIfInternalErrorHappens() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";
    String url = host + "/healthcheck";
    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<Application> entity = new HttpEntity<>(headers);
    @SuppressWarnings("unchecked")
    ResponseEntity<String> e = mock(ResponseEntity.class);
    ListenableFutureTask<ResponseEntity<String>> futureTask = spy(new ListenableFutureTask<>(() -> e));
    RuntimeException ex = new RuntimeException();

    doReturn(futureTask).when(restTemplate).exchange(url, HttpMethod.GET, entity, String.class);
    doThrow(ex).when(e).getBody();

    MicroserviceTextHealthCheckCommand command = new MicroserviceTextHealthCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    futureTask.run();

    // WHEN
    Throwable th = catchThrowable(() -> command.executeCheck().getNow(null));

    //THEN
    assertThat(th).isInstanceOf(CompletionException.class);
    assertThat(th).hasCause(ex);
  }
}
