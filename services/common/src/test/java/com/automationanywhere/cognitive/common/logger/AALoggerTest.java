package com.automationanywhere.cognitive.common.logger;

import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Copyright (c) 2017 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
public class AALoggerTest extends AbstractTestNGSpringContextTests {
//    @Test
    public void testOfDebugMessageShouldCall() throws Exception {
        // Todo: Implement a unit test for AALogger using TestNG and compatible with JaCoCo
        // Given how the AALogger creates new instances we'll need Powermockito to test
        // Powermockito extends Powermock
        // Powermock does not work with JaCoCo: https://github.com/powermock/powermock/issues/727
        // Once Powermock has resolved this issue we'll complete testing of this class

        // given
        // AALogger log = AALogger.create(this.getClass());

        // when

        // then
    }
}
