package com.automationanywhere.cognitive.common.healthcheck.health;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.common.healthcheck.commands.DetailedHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.MicroserviceHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.RabbitMQHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.components.DetailedHealthCheckComponent;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.testng.annotations.Test;

public class DetailedHealthCheckComponentTest {

  @Test
  public void executeHealthCheckWithoutCommands() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.emptyList());
    application.setDependencies(Collections.emptyList());
    BuildInfoFactory buildInfoFactory = mock(BuildInfoFactory.class);
    when(buildInfoFactory.build()).thenReturn(application.getBuildInfo());
    AppInfoFactory appInfoFactory = mock(AppInfoFactory.class);
    when(appInfoFactory.build(
        application.getBuildInfo().getAppName(),
        application.getAppInfo().getType(),
        application.getAppInfo().getStatus(),
        application.getAppInfo().getFailReason()
    )).thenReturn(application.getAppInfo());

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(
        buildInfoFactory,
        appInfoFactory
    );

    // WHEN
    CompletableFuture<Application> future = component.executeHealthCheck(Collections.emptyList());

    //THEN
    assertThat(future.get()).isEqualToComparingFieldByFieldRecursively(application);
  }

  @Test
  public void executeHealthCheckWithOneSubsystemCommand() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setDependencies(Collections.emptyList());
    application.setSubsystems(Collections.singletonList(buildSubsystem()));
    AppInfo appInfo = application.getAppInfo();

    BuildInfoFactory buildInfoFactory = mock(BuildInfoFactory.class);
    when(buildInfoFactory.build()).thenReturn(application.getBuildInfo());

    AppInfoFactory appInfoFactory = mock(AppInfoFactory.class);
    when(appInfoFactory.build(
        appInfo.getName(),
        "",
        Status.NOT_OK,
        "Found 1 error in subsystem or dependency"
    )).thenReturn(appInfo);

    MicroserviceHealthCheckCommand subsystemCommand = mock(MicroserviceHealthCheckCommand.class);
    when(subsystemCommand.executeCheck()).thenReturn(
        CompletableFuture.completedFuture(new ApplicationHealthCheckStatus(buildSubsystem()))
    );

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(
        buildInfoFactory,
        appInfoFactory
    );

    // WHEN
    CompletableFuture<Application> future = component.executeHealthCheck(
        Collections.singletonList(subsystemCommand)
    );

    //THEN
    assertThat(future.get()).isEqualToComparingFieldByFieldRecursively(application);
  }

  @Test
  public void executeHealthCheckWithOneSubsystemAndDependencyCommand() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.singletonList(buildSubsystem()));
    application.setDependencies(Collections.singletonList(buildDependency()));
    AppInfo appInfo = application.getAppInfo();

    BuildInfoFactory buildInfoFactory = mock(BuildInfoFactory.class);
    when(buildInfoFactory.build()).thenReturn(application.getBuildInfo());

    AppInfoFactory appInfoFactory = mock(AppInfoFactory.class);
    when(appInfoFactory.build(
        appInfo.getName(),
        "",
        Status.NOT_OK,
        "Found 1 error in subsystem or dependency"
    )).thenReturn(appInfo);

    MicroserviceHealthCheckCommand subsystemCommand = mock(MicroserviceHealthCheckCommand.class);
    when(subsystemCommand.executeCheck()).thenReturn(
        CompletableFuture.completedFuture(new ApplicationHealthCheckStatus(buildSubsystem()))
    );

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(
        buildInfoFactory,
        appInfoFactory
    );

    ArrayList<DetailedHealthCheckCommand> commands = new ArrayList<>();
    commands.add(subsystemCommand);
    commands.add(new RabbitMQHealthCheckCommand("message_queue"));

    // WHEN
    CompletableFuture<Application> future = component.executeHealthCheck(commands);

    //THEN
    assertThat(future.get()).isEqualToComparingFieldByFieldRecursively(application);
  }

  @Test
  public void testHealthCheckConvertWithoutSubsystemsDependencies() throws Exception {
    // GIVEN
    Application application = buildApplication();
    String expected = "Application: name\n"
        + "Status: OK\n"
        + "Reason: \n"
        + "Version: 1.0\n"
        + "Branch: 1234\n"
        + "GIT #: 12345678\n"
        + "Build Time: 2018-03-20\n"
        + "Application uptime: 2018-03-20 11:20:32.234\n";

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(null, null);

    //WHEN
    String result = component.convert(application);

    // THEN
    assertThat(result).isEqualTo(expected);
  }

  @Test
  public void testHealthCheckConvertWithoutDependenciesWithOneSubsystem() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.singletonList(buildSubsystem()));
    application.setDependencies(Collections.emptyList());

    String expected = "Application: name\n"
        + "Status: OK\n"
        + "Reason: \n"
        + "Version: 1.0\n"
        + "Branch: 1234\n"
        + "GIT #: 12345678\n"
        + "Build Time: 2018-03-20\n"
        + "Application uptime: 2018-03-20 11:20:32.234\n"
        + "\n"
        + "Subsystems: \n"
        + "\n"
        + "  Subsystem: \n"
        + "    Application: subsystem\n"
        + "    Status: NOT_OK\n"
        + "    Reason: Error\n"
        + "    Version: 1.1\n"
        + "    Branch: 12345\n"
        + "    GIT #: 123456789\n"
        + "    Build Time: 2017-01-12\n"
        + "    Application uptime: 2017-01-12 07:13:11.456\n";

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(null, null);

    //WHEN
    String result = component.convert(application);

    // THEN
    assertThat(result).isEqualTo(expected);
  }

  @Test
  public void testHealthCheckConvertWithoutSubsystemsWithOneDependency() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.emptyList());
    application.setDependencies(Collections.singletonList(buildDependency()));

    String expected = "Application: name\n"
        + "Status: OK\n"
        + "Reason: \n"
        + "Version: 1.0\n"
        + "Branch: 1234\n"
        + "GIT #: 12345678\n"
        + "Build Time: 2018-03-20\n"
        + "Application uptime: 2018-03-20 11:20:32.234\n"
        + "\n"
        + "Dependencies: \n"
        + "  message_queue: OK\n";

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(null, null);

    //WHEN
    String result = component.convert(application);

    // THEN
    assertThat(result).isEqualTo(expected);
  }

  @Test
  public void testHealthCheckConvertWithOneSubsystemAndDependency() throws Exception {
    // GIVEN
    Application application = buildApplication();
    application.setSubsystems(Collections.singletonList(buildSubsystem()));
    application.setDependencies(Collections.singletonList(buildDependency()));

    String expected = "Application: name\n"
        + "Status: OK\n"
        + "Reason: \n"
        + "Version: 1.0\n"
        + "Branch: 1234\n"
        + "GIT #: 12345678\n"
        + "Build Time: 2018-03-20\n"
        + "Application uptime: 2018-03-20 11:20:32.234\n"
        + "\n"
        + "Subsystems: \n"
        + "\n"
        + "  Subsystem: \n"
        + "    Application: subsystem\n"
        + "    Status: NOT_OK\n"
        + "    Reason: Error\n"
        + "    Version: 1.1\n"
        + "    Branch: 12345\n"
        + "    GIT #: 123456789\n"
        + "    Build Time: 2017-01-12\n"
        + "    Application uptime: 2017-01-12 07:13:11.456\n"
        + "\n"
        + "Dependencies: \n"
        + "  message_queue: OK\n";

    DetailedHealthCheckComponent component = new DetailedHealthCheckComponent(null, null);

    //WHEN
    String result = component.convert(application);

    // THEN
    assertThat(result).isEqualTo(expected);
  }

  private Application buildApplication() {
    Application application = new Application();
    application.setAppInfo(new AppInfo(
        "name",
        "",
        Status.OK,
        "",
        "2018-03-20 11:20:32.234"
    ));
    application.setBuildInfo(new BuildInfo(
        "name",
        "1.0",
        "1234",
        "12345678",
        "2018-03-20"
    ));
    return application;
  }

  private Application buildSubsystem() {
    Application subsystem = new Application();
    subsystem.setAppInfo(new AppInfo(
        "subsystem",
        "APPLICATION",
        Status.NOT_OK,
        "Error",
        "2017-01-12 07:13:11.456"
    ));
    subsystem.setBuildInfo(new BuildInfo(
        "subsystem",
        "1.1",
        "12345",
        "123456789",
        "2017-01-12"
    ));
    return subsystem;
  }

  private Dependency buildDependency() {
    return new Dependency(
        "message_queue", Status.OK
    );
  }
}
