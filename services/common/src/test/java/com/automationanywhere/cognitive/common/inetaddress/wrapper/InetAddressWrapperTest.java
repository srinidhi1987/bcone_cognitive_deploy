package com.automationanywhere.cognitive.common.inetaddress.wrapper;


import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class InetAddressWrapperTest {

    @Test
    public void testOfGetFullyQualifiedNameWhenSuccess(){
        //given
        
        //when
        String fqdn = InetAddressWrapper.getFullyQualifiedName();

        //then
        assertThat(fqdn).isNotNull();
    }
}
