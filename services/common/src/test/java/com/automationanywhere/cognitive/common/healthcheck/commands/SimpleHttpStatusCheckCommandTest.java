package com.automationanywhere.cognitive.common.healthcheck.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.net.ConnectException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFutureTask;
import org.springframework.web.client.AsyncRestTemplate;
import org.testng.annotations.Test;

public class SimpleHttpStatusCheckCommandTest {

  @Test
  public void executeCheckSuccessful() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/health";

    ListenableFutureTask<ResponseEntity<String>> futureTask = new ListenableFutureTask<>(
        () -> new ResponseEntity<>("OK", HttpStatus.OK));
    doReturn(futureTask).when(restTemplate).getForEntity(url, String.class);

    SimpleHttpStatusCheckCommand command = new SimpleHttpStatusCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        "",
        "",
        "",
        ""
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        CommandType.APPLICATION.name(),
        Status.OK,
        "",
        ""
    ));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }

  @Test
  public void executeCheckFailure() throws ExecutionException, InterruptedException {
    // GIVEN
    String name = "name";
    String host = "URL";

    BuildInfoFactory buildInfoFactory = new BuildInfoFactory();
    AppInfoFactory appInfoFactory = new AppInfoFactory();
    AsyncRestTemplate restTemplate = mock(AsyncRestTemplate.class);

    String url = host + "/health";

    SimpleHttpStatusCheckCommand command = new SimpleHttpStatusCheckCommand(
        name, host, buildInfoFactory, appInfoFactory, restTemplate
    );

    ListenableFutureTask<ResponseEntity<String>> futureTask = spy(new ListenableFutureTask<>(
        () -> new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR)));

    doReturn(futureTask).when(restTemplate).getForEntity(url, String.class);
    ExecutionException ex = new ExecutionException("Error to execute", new ConnectException("Connection Exception"));
    doThrow(ex).when(futureTask).get();

    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(
        name,
        "",
        "",
        "",
        ""
    ));
    application.setAppInfo(appInfoFactory.build(
        name,
        command.getType().name(),
        Status.NOT_OK,
        "Connection Exception",
        ""
    ));
    HealthCheckStatus status = new ApplicationHealthCheckStatus(application);

    // WHEN
    CompletableFuture<HealthCheckStatus> result = command.executeCheck();
    futureTask.run();

    // THEN
    assertThat(result.get()).isEqualToComparingFieldByFieldRecursively(status);
  }
}
