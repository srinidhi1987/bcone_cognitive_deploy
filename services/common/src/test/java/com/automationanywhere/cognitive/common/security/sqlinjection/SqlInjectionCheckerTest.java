package com.automationanywhere.cognitive.common.security.sqlinjection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

import org.testng.annotations.Test;

public class SqlInjectionCheckerTest {

  @Test
  public void testListOfValidNames() {
    // GIVEN
    String[] columnNames = {"insertedAt", "column_1", "deletedAt", "NAME"};

    for (String columnName : columnNames) {
      // WHEN
      Throwable throwable = catchThrowable(() -> SqlInjectionChecker.checkColumnName(columnName));

      // THEN
      assertThat(throwable).doesNotThrowAnyException();
    }
  }

  @Test
  public void testListOfInvalidNames() {

    // GIVEN
    String[] columnNames = {
        "column_1;InSeRt InTo",
        "insertedAt GO DROP TABLE ",
        "deletedAt ; select ",
        "NAME UNION SELECT 1",
        "column_2;UPDATE User SET password='111'",
        "column_3;deletE from A;",
        "column_4;ALTER TABLE"
    };

    for (String columnName : columnNames) {
      // WHEN
      Throwable throwable = catchThrowable(() -> SqlInjectionChecker.checkColumnName(columnName));

      // THEN
      assertThat(throwable)
          .isInstanceOf(SqlInjectionException.class)
          .hasMessageContaining(columnName);
    }
  }
}
