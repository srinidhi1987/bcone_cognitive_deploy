package com.automationanywhere.cognitive.common.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DefaultCipherServiceTest {

  private DefaultCipherService c;
  private Random r;

  @BeforeMethod
  public void setUp() throws Exception {
    KeyGenerator kg = KeyGenerator.getInstance("AES");
    kg.init(256);
    SecretKey key = kg.generateKey();

    try (
        ByteArrayInputStream is = new ByteArrayInputStream(key.getEncoded());
    ) {
      c = new DefaultCipherService(new StreamKeyProvider("AES", is));
    }

    r = new Random();
  }

  @Test
  public void testEncryptDecrypt() throws IOException {
    // given
    int to = 10 * 1024 * 1024;
    int from = 1 * 1024 * 1024;
    int len = from + r.nextInt(to - from + 1);
    byte[] bytes = new byte[len];
    byte[] buffer = new byte[bytes.length];

    r.nextBytes(bytes);

    // when
    InputStream en = c.encrypt(new ByteArrayInputStream(bytes));

    InputStream de = c.decrypt(c.encrypt(new ByteArrayInputStream(bytes)));

    // then
    int b = 0;
    int read = 0;
    while ((b = en.read()) != -1 && read < bytes.length) {
      if ((byte) b != bytes[read]) {
        break;
      }
      read += 1;
    }

    assertThat(read).isNotEqualTo(bytes.length);

    int n = 0;
    int o = 0;
    int l = buffer.length;
    while ((n = de.read(buffer, o, l)) != -1) {
      o += n;
      l -= n;
    }

    assertThat(n).isEqualTo(-1);
    assertThat(o).isEqualTo(buffer.length);

    for (int i = 0; i < buffer.length; i += 1) {
      assertThat(buffer[i]).isEqualTo(bytes[i]);
    }
  }

  @Test
  public void createInputStreamShouldRethrowRuntimeExceptions() {
    // given
    RuntimeException e = new RuntimeException();

    // when
    Throwable ex = catchThrowable(() -> c.createInputStream(() -> {
      throw e;
    }));

    // then
    assertThat(ex).isSameAs(e);
  }

  @Test
  public void createInputStreamShouldWrapNotRuntimeExceptions() {
    // given
    IOException e = new IOException();

    // when
    Throwable ex = catchThrowable(() -> c.createInputStream(() -> {
      throw e;
    }));

    // then
    assertThat(ex.getCause()).isSameAs(e);
  }

  @Test
  public void validateSignatureShouldThrowIfASignatureIsTooShort() throws IOException {
    // given
    byte[] b = {
        0x7a, (byte) 0xc6, (byte) 0xf4, 0x30, 0x52,
        (byte) 0xd9, 0x14, (byte) 0xfb, (byte) 0xb9
    };

    // when
    Throwable ex = catchThrowable(() -> c.validateSignature(new ByteArrayInputStream(b)));

    // then
    assertThat(ex).isInstanceOf(BadSignatureException.class);
    assertThat(ex.getMessage()).isEqualTo("Wrong signature size");
  }

  @Test
  public void validateSignatureShouldThrowIfASignatureIsNotValid() throws IOException {
    // given
    byte[] b = {
        0x7a, (byte) 0xc6, (byte) 0xf4, 0x30, 0x52,
        (byte) 0xd9, 0x14, (byte) 0xfb, (byte) 0xb9, (byte) 0xFF
    };

    // when
    Throwable ex = catchThrowable(() -> c.validateSignature(new ByteArrayInputStream(b)));

    // then
    assertThat(ex).isInstanceOf(BadSignatureException.class);
    assertThat(ex.getMessage()).isEqualTo("Not a valid signature");
  }

  @Test
  public void getIVShouldThrowAnExceptionIfAStreamIsTooShort() {
    byte[] buffer = new byte[9];
    r.nextBytes(buffer);

    Throwable th = catchThrowable(() -> c.decrypt(new ByteArrayInputStream(buffer)));

    assertThat(th).isInstanceOf(CipherException.class);
    assertThat(th.getMessage()).isEqualTo("IV size is wrong");
  }
}
