package com.automationanywhere.cognitive.common.domain.service.security.exceptions;

import com.automationanywhere.cognitive.common.security.CipherException;
import com.automationanywhere.cognitive.common.security.CipherService;

/**
 * The exception is thrown by the {@link CipherService} if the message signature validation fails.
 */
public class BadSignatureException extends CipherException {
  
  private static final long serialVersionUID = 3960528743490392924L;

  public BadSignatureException(final String message) {
    super(message);
  }
}
