package com.automationanywhere.cognitive.common.config;

import static java.util.Collections.unmodifiableSet;

import com.automationanywhere.cognitive.common.security.CipherService;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Set;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class CognitivePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

  private CipherService cipherService;
  private DataTransformer dataTransformer;
  private Set<String> encryptedKeys;

  public void setEncryptedKeys(Set<String> encryptedKeys) {
    this.encryptedKeys = unmodifiableSet(encryptedKeys);
  }

  public void setCipherService(CipherService cipherService) {
    this.cipherService = cipherService;
  }

  public void setDataTransformer(DataTransformer dataTransformer) {
    this.dataTransformer = dataTransformer;
  }

  @Override
  protected String convertProperty(String propertyName, String propertyValue) {
    String value;
    if (encryptedKeys.contains(propertyName)) {
      try {
        value = decrypt(Base64.getDecoder().decode(propertyValue.getBytes()));
      } catch (IOException e) {
        throw new RuntimeException("Error to decrypt configuration property " + propertyName, e);
      }
    } else {
      value = propertyValue;
    }
    return value;
  }

  private String decrypt(byte[] value) throws IOException {
    InputStream inputStream = dataTransformer.transform(value);
    InputStream decryptedStream = cipherService.decrypt(inputStream);
    return dataTransformer.transform(decryptedStream).toString("UTF-8");
  }
}

