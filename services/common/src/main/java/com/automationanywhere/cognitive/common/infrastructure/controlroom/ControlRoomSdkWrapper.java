package com.automationanywhere.cognitive.common.infrastructure.controlroom;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.appconfiguration.Configuration;
import com.automationanywhere.cognitive.common.domain.service.security.KeyManager;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.AsymmetricKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.token.bootstrap.ControlRoomAdapter;
import com.automationanywhere.um.api.dto.RoleBasic;
import io.jsonwebtoken.Claims;
import java.util.List;
import java.util.Properties;

/**
 * Before Control Room can be used it should be initialized first.
 * This class wraps CR SDK methods making sure that the SDK init
 * is called before any actual SDK method is executed.
 */
public class ControlRoomSdkWrapper {

  private final AppConfigurationRepository appConfigurationRepository;
  private final KeyManager keyManager;

  private Configuration configuration = null;
  private ControlRoomAdapter sdk;

  public ControlRoomSdkWrapper(
          final AppConfigurationRepository appConfigurationRepository,
          final KeyManager keyManager
  ) {
    this.appConfigurationRepository = appConfigurationRepository;
    this.keyManager = keyManager;
  }

  public List<RoleBasic> getRoles(final String token) throws Exception {
    init();
    return sdk.getAppRegistrationAdapter().getRoles(token);
  }

  public Claims getClaims(final String token)throws Exception{
    init();
    return sdk.getJwtValidationService().getClaims(token);
  }

  public void validateToken(final String token,final Claims claims) throws Exception{
    init();
    sdk.getJwtValidationService().validateToken(token,claims);
  }

  public String getAppUserToken() throws Exception{
    init();
    return sdk.getAppRegistrationAdapter().getAppUserToken();
  }

  private void init() {
    try {
      synchronized (this) {
        if (configuration == null) {
          configuration = appConfigurationRepository.load();
          if (configuration == null) {
            throw new ControlRoomSdkWrapperException("The app hasn't yet been registered");
          }
        }
        if (sdk == null) {
          Properties props = createSDKProps(configuration.controlRoomUrl, configuration.appId);
          sdk = ControlRoomAdapter.init(props, null);
        }
      }
    } catch (final ControlRoomSdkWrapperException ex) {
      throw ex;
    } catch (final Exception ex) {
      throw new ControlRoomSdkWrapperException(ex);
    }
  }

  private Properties createSDKProps(final String controlRoomUrl, final String appId) {
    Properties props = new Properties();

    AsymmetricKey key = (AsymmetricKey) keyManager.retrieveKey(KeyName.CR_COMMUNICATION_KEY);
    props.setProperty(ControlRoomAdapter.APP_PRIVATE_KEY, key.privateKey());
    props.setProperty(ControlRoomAdapter.APP_URL, controlRoomUrl);
    props.setProperty(ControlRoomAdapter.APP_USER, appId);

    return props;
  }
}
