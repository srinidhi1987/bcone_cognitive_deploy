package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

public enum KeyName {
  SYSTEM_KEY(KeyType.SYMMETRIC), CR_COMMUNICATION_KEY(KeyType.ASYMMETRIC), PRODUCT_KEY(KeyType.SYMMETRIC), CR_COMMUNICATION_KEY_PRIVATE(KeyType.ASYMMETRIC), CR_COMMUNICATION_KEY_PUBLIC(KeyType.ASYMMETRIC);

  private final KeyType keyType;

  KeyName(KeyType keyType) {
    this.keyType = keyType;
  }

  public KeyType getKeyType() {
    return keyType;
  }

}
