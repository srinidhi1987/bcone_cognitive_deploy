package com.automationanywhere.cognitive.common.domain.service.security.exceptions;

public class KeyGenerationException extends RuntimeException{

  private static final long serialVersionUID = -7351400936357935262L;

  public KeyGenerationException(String message, Throwable cause) {
    super(message, cause);
  }

}
