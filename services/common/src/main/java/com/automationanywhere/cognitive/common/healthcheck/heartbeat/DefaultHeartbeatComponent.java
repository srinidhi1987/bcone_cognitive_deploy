package com.automationanywhere.cognitive.common.healthcheck.heartbeat;

import static com.automationanywhere.cognitive.common.healthcheck.model.Status.OK;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Heartbeat;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Default implementation of base heartbeat component. An instance of this class will be used if no subclass of
 * <code>HeartbeatComponent</code> was found in the classpath.
 */
public class DefaultHeartbeatComponent implements HeartbeatComponent<Heartbeat> {

  private static final char NEW_LINE = '\n';

  private BuildInfoFactory buildInfoFactory;
  private AppInfoFactory appInfoFactory;

  @Autowired
  public DefaultHeartbeatComponent(
      BuildInfoFactory buildInfoFactory,
      AppInfoFactory appInfoFactory
  ) {
    this.buildInfoFactory = buildInfoFactory;
    this.appInfoFactory = appInfoFactory;
  }

  @Override
  public CompletableFuture<Heartbeat> prepareHeartbeat() {
    BuildInfo buildInfo = buildInfoFactory.build();
    AppInfo appInfo = appInfoFactory.build(buildInfo.getAppName(), "", OK, "");
    return CompletableFuture.completedFuture(new Heartbeat(appInfo, buildInfo));
  }

  @Override
  public String convert(Heartbeat heartbeat) {
    AppInfo appInfo = heartbeat.getAppInfo();
    BuildInfo buildInfo = heartbeat.getBuildInfo();
    StringBuilder sb = new StringBuilder();
    sb.append("Application: ").append(buildInfo.getAppName()).append(NEW_LINE);
    sb.append("Status: ").append(appInfo.getStatus()).append(NEW_LINE);
    sb.append("Reason: ").append(appInfo.getFailReason()).append(NEW_LINE);
    sb.append("Version: ").append(buildInfo.getVersion()).append(NEW_LINE);
    sb.append("Branch: ").append(buildInfo.getBranch()).append(NEW_LINE);
    sb.append("GIT #: ").append(buildInfo.getGit()).append(NEW_LINE);
    sb.append("Build Time: ").append(buildInfo.getBuildTime()).append(NEW_LINE);
    sb.append("Application uptime: ").append(appInfo.getUptime()).append(NEW_LINE);
    return sb.toString();
  }
}
