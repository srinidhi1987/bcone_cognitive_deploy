package com.automationanywhere.cognitive.common.healthcheck.adapters.rest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import com.automationanywhere.cognitive.common.auth.AnonymousAccess;
import com.automationanywhere.cognitive.common.healthcheck.commands.DetailedHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.HealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.SimpleHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.components.DetailedHealthCheckComponent;
import com.automationanywhere.cognitive.common.healthcheck.components.HealthCheckComponent;
import com.automationanywhere.cognitive.common.healthcheck.components.SimpleHealthComponent;
import com.automationanywhere.cognitive.common.healthcheck.heartbeat.DefaultHeartbeatComponent;
import com.automationanywhere.cognitive.common.healthcheck.heartbeat.HeartbeatComponent;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Default controller endpoint for health check.
 */
@RestController
@SuppressWarnings("unused")
public class HealthCheckController {

  private static final String TEXT_PLAIN = "text/plain";
  private static final String APP_JSON = "application/json";

  private final HeartbeatComponent<Object> heartbeatComponent;
  private final HealthCheckComponent<Object, DetailedHealthCheckCommand> healthCheckComponent;
  private final HealthCheckComponent<Object, SimpleHealthCheckCommand> healthComponent;
  private final List<DetailedHealthCheckCommand> detailedHealthCheckCommands;
  private final List<SimpleHealthCheckCommand> simpleHealthCommands;

  @Autowired(required = false)
  @SuppressWarnings({"unchecked", "OptionalUsedAsFieldOrParameterType"})
  public HealthCheckController(
      Optional<HeartbeatComponent> heartbeatComponent,
      Optional<HealthCheckComponent> healthCheckComponent,
      Optional<HealthCheckComponent> healthComponent,
      Optional<List<HealthCheckCommand>> healthCheckCommands,
      BuildInfoFactory buildInfoFactory,
      AppInfoFactory appInfoFactory
  ) {
    this.heartbeatComponent = heartbeatComponent
        .orElse(new DefaultHeartbeatComponent(buildInfoFactory, appInfoFactory));
    this.healthCheckComponent = healthCheckComponent
        .orElse(new DetailedHealthCheckComponent(buildInfoFactory, appInfoFactory));
    this.healthComponent = healthComponent
        .orElse(new SimpleHealthComponent(buildInfoFactory, appInfoFactory));

    detailedHealthCheckCommands = healthCheckCommands
        .orElse(new ArrayList<>())
        .stream()
        .filter(c -> c instanceof DetailedHealthCheckCommand)
        .map(c -> (DetailedHealthCheckCommand) c)
        .collect(Collectors.toList());

    simpleHealthCommands = healthCheckCommands
        .orElse(new ArrayList<>())
        .stream()
        .filter(c -> c instanceof SimpleHealthCheckCommand)
        .map(c -> (SimpleHealthCheckCommand) c)
        .collect(Collectors.toList());
  }

  /**
   * Endpoint for heartbeat as text.
   *
   * @return the heartbeat information
   */
  @AnonymousAccess
  @RequestMapping(
      value = "/heartbeat",
      method = GET,
      produces = {TEXT_PLAIN}
  )
  @SuppressWarnings("unchecked")
  public CompletableFuture<String> heartbeatText() {
    CompletableFuture future = heartbeatComponent.prepareHeartbeat();
    return future.thenApply(heartbeatComponent::convert);
  }

  /**
   * Endpoint for heartbeat as Json.
   *
   * @return the heartbeat information
   */
  @AnonymousAccess
  @RequestMapping(
      value = "/heartbeat",
      method = GET,
      consumes = {APP_JSON},
      produces = {APP_JSON}
  )
  public CompletableFuture<?> heartbeatJson() {
    return heartbeatComponent.prepareHeartbeat();
  }

  /**
   * Endpoint for heath check.
   *
   * @return the health check information
   */
  @AnonymousAccess
  @RequestMapping(
      value = "/healthcheck",
      method = GET,
      produces = {TEXT_PLAIN}
  )
  @SuppressWarnings("unchecked")
  public CompletableFuture<String> healthCheck() {
    CompletableFuture future = healthCheckComponent.executeHealthCheck(detailedHealthCheckCommands);
    return future.thenApply(healthCheckComponent::convert);
  }

  /**
   * Endpoint for heath check.
   *
   * @return the health check information
   */
  @AnonymousAccess
  @RequestMapping(
      value = "/healthcheck",
      method = GET,
      consumes = {APP_JSON},
      produces = {APP_JSON}
  )
  public CompletableFuture<?> healthCheckJson() {
    return healthCheckComponent.executeHealthCheck(detailedHealthCheckCommands);
  }

  /**
   * Endpoint for heath check.
   *
   * @return the health check information
   */
  @AnonymousAccess
  @RequestMapping(
      value = "/health",
      method = GET,
      produces = {TEXT_PLAIN}
  )
  @SuppressWarnings("unchecked")
  public CompletableFuture<String> health() {
    CompletableFuture future = healthComponent.executeHealthCheck(simpleHealthCommands);
    return future.thenApply(healthComponent::convert);
  }

  /**
   * Endpoint for heath check.
   *
   * @return the health check information
   */
  @AnonymousAccess
  @RequestMapping(
      value = "/health",
      method = GET,
      consumes = {APP_JSON},
      produces = {APP_JSON}
  )
  public CompletableFuture<?> healthJson() {
    return healthComponent.executeHealthCheck(simpleHealthCommands);
  }
}
