package com.automationanywhere.cognitive.common.infrastructure.security.database;

import com.automationanywhere.cognitive.common.domain.service.security.KeyProvider;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.AsymmetricKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.SymmetricKey;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKey;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKeyRepository;
import com.automationanywhere.cognitive.common.infrastructure.security.database.exceptions.DatabaseKeyProviderException;
import com.automationanywhere.cognitive.common.infrastructure.security.database.mappers.CryptographyKeyMapper;
import org.jetbrains.annotations.NotNull;

public class DatabaseKeyProvider implements KeyProvider {

  private final SecurityKeyRepository securityKeyRepository;
  private final String SYSTEM = "SYSTEM";

  public DatabaseKeyProvider(
      SecurityKeyRepository securityKeyRepository) {
    this.securityKeyRepository = securityKeyRepository;
  }

  @Override
  public CryptographyKey getKey(KeyName name) throws DatabaseKeyProviderException{
    try {
      if (name.equals(KeyName.CR_COMMUNICATION_KEY)) {
        SecurityKey privateKey = securityKeyRepository
            .securityKeyByName(KeyName.CR_COMMUNICATION_KEY_PRIVATE);
        SecurityKey publicKey = securityKeyRepository
            .securityKeyByName(KeyName.CR_COMMUNICATION_KEY_PUBLIC);
        if (privateKey == null || publicKey == null) {
          return null;
        }
        return CryptographyKeyMapper.cryptographyAsymmetricKey(privateKey, publicKey);
      } else {
        SecurityKey securityKey = securityKeyRepository.securityKeyByName(name);
        if (securityKey == null) {
          return null;
        }
        return CryptographyKeyMapper.cryptographySymmetricKey(securityKey);
      }
    } catch (Exception ex) {
      throw new DatabaseKeyProviderException(
          String.format("Error while fetching the security key %s from database!", name), ex);
    }
  }

  @Override
  public void saveKey(KeyName keyName, CryptographyKey keyValue) throws DatabaseKeyProviderException{
    try {
      KeyType keyType = keyName.getKeyType();
      if (keyType.equals(KeyType.SYMMETRIC)) {
        SymmetricKey symmetricKey = (SymmetricKey) keyValue;
        SecurityKey securityKey = createSecurityKey(keyName,
            symmetricKey.key(),
            keyType);
        securityKeyRepository.add(securityKey);
      } else {
        AsymmetricKey asymmetricKey = (AsymmetricKey) keyValue;
        SecurityKey privateKey = createSecurityKey(KeyName.CR_COMMUNICATION_KEY_PRIVATE,
            asymmetricKey.privateKey(), keyType);
        securityKeyRepository.add(privateKey);
        SecurityKey publicKey = createSecurityKey(KeyName.CR_COMMUNICATION_KEY_PUBLIC,
            asymmetricKey.publicKey(), keyType);
        securityKeyRepository.add(publicKey);
      }
    } catch (Exception ex) {
      throw new DatabaseKeyProviderException("Error while saving the key in database!", ex);
    }
  }

  @NotNull
  private SecurityKey createSecurityKey(KeyName keyName, String keyValue, KeyType keyType) {
    SecurityKey securityKey = new SecurityKey();
    securityKey.setKeyName(keyName);
    securityKey.setKeyType(keyType);
    securityKey.setCreatedBy(SYSTEM);
    securityKey.setKeyValue(keyValue);
    return securityKey;
  }

  @Override
  public void removeKey(KeyName name) throws DatabaseKeyProviderException{
    try {
      SecurityKey securityKey = securityKeyRepository.securityKeyByName(name);
      securityKeyRepository.remove(securityKey);
    } catch (Exception ex) {
      throw new DatabaseKeyProviderException("Error while removing the key from database!", ex);
    }
  }
}
