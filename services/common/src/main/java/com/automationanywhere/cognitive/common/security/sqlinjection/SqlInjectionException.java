package com.automationanywhere.cognitive.common.security.sqlinjection;

public class SqlInjectionException extends RuntimeException {
    
  private static final long serialVersionUID = 705394199036115208L;

  public SqlInjectionException(String message) {
    super(message);
  }
}
