package com.automationanywhere.cognitive.common.infrastructure.audit;

import com.automationanywhere.cognitive.common.domain.service.audit.Auditing;
import com.automationanywhere.cognitive.common.domain.service.audit.AuditingEvent;
import com.automationanywhere.cognitive.common.domain.service.audit.AuditingException;
import com.automationanywhere.cognitive.common.infrastructure.controlroom.ControlRoomSdkWrapper;

/**
 * This implementation send auditing events to Control Room.
 */
public class ControlRoomAuditing implements Auditing {
  //TODO: If you do not need it to send auditing events, remove it.
  private final ControlRoomSdkWrapper sdk;

  public ControlRoomAuditing(final ControlRoomSdkWrapper sdk) {
    this.sdk = sdk;
  }

  @Override
  public void audit(final AuditingEvent auditingEvent) throws AuditingException {
    try {
      //TODO: This is and example, it should be replaced with real CR usage.
      /*
      String activityType = null;
      switch (auditingEvent.activityType()) {
        case CREATE_LEARNING_INSTANCE:
          activityType = ControlRoomActivityType.CREATE_LI.name();
        default:
          throw new UnsupportedOperationException();
      }
      sdk.audit(new ControlRoomSdk.AuditingEvent(
          auditingEvent.eventDescription(),
          activityType,
          AuditingContext.AUDITING_CONTEXT.get().environmentName(),
          AuditingContext.AUDITING_CONTEXT.get().hostName(),
          AuditingContext.AUDITING_CONTEXT.get().userName(),
          auditingEvent.status().name,
          auditingEvent.verbosityLevel().name,
          AuditingContext.AUDITING_CONTEXT.get().source(),
          AuditingContext.AUDITING_CONTEXT.get().entityVersion(),
          auditingEvent.objectName(),
          auditingEvent.detail()
      ));*/
    } catch (final Exception ex) {
      throw new AuditingException(ex);
    }
  }
}
