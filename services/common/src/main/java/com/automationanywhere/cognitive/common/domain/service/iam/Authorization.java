package com.automationanywhere.cognitive.common.domain.service.iam;

import com.automationanywhere.cognitive.common.domain.service.iam.exceptions.AuthorizationException;

/**
 * Authorizes token
 */
public interface Authorization {

  /**
   * sends a token for authorization.
   *
   * @param token token that needs to be authorized.
   * @throws AuthorizationException if authorization get's failed due to any cause.
   */
  public void authorize(String token) throws AuthorizationException;
}
