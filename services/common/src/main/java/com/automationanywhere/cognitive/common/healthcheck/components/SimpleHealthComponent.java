package com.automationanywhere.cognitive.common.healthcheck.components;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.commands.SimpleHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Default implementation of base health check component. An instance of this class will be used if no subclass of
 * <code>HealthCheckComponent</code> was found in the classpath.
 */
public class SimpleHealthComponent extends BaseHealthCheckCommand<SimpleHealthCheckCommand> {

  private BuildInfoFactory buildInfoFactory;
  private AppInfoFactory appInfoFactory;

  @Autowired
  public SimpleHealthComponent(
      BuildInfoFactory buildInfoFactory,
      AppInfoFactory appInfoFactory
  ) {
    this.buildInfoFactory = buildInfoFactory;
    this.appInfoFactory = appInfoFactory;
  }

  @Override
  public CompletableFuture<Application> executeHealthCheck(List<SimpleHealthCheckCommand> commands) {

    CompletableFuture[] commandFutures = getCompletableFutures(commands);

    return CompletableFuture.allOf(commandFutures)
        .thenApply(ignoredVoid -> {

          List<HealthCheckStatus> healthCheckStatuses = getHealthCheckStatuses(commandFutures);

          Application app = new Application();
          app.setSubsystems(healthCheckStatuses.stream().filter(Objects::nonNull)
              .filter(cs -> cs.getCommandType() == CommandType.APPLICATION)
              .map(ct -> ((ApplicationHealthCheckStatus) ct).getApplication())
              .collect(Collectors.toList()));

          long foundError = healthCheckStatuses.stream().filter(ct ->
              ct == null || ct.getStatus() == Status.NOT_OK
          ).count();

          BuildInfo buildInfo = buildInfoFactory.build();
          app.setBuildInfo(buildInfo);
          app.setAppInfo(appInfoFactory.build(
              buildInfo.getAppName(),
              "",
              foundError == 0 ? Status.OK : Status.NOT_OK,
              null
          ));
          return app;
        });
  }

  @Override
  public String convert(Application app) {
    StringBuilder sb = new StringBuilder();
    sb.append("{\"success\":").append(app.getAppInfo().getStatus() == Status.OK).append(',');
    sb.append("\"data\":{\"serviceStatuses\":[");
    List<Application> subsystems = app.getSubsystems();
    if (subsystems != null && !subsystems.isEmpty()) {
      for (Application subApp : subsystems) {
        AppInfo appInfo = subApp.getAppInfo();
        sb.append("{\"name\":\"").append(appInfo.getName()).append("\"");
        sb.append(",\"statusCode\":").append(appInfo.getStatus() == Status.OK ? "200" : "500")
            .append("}");
      }
    }

    sb.append("]},\"errors\":\"").append(app.getAppInfo().getFailReason());
    sb.append("\"}");
    return sb.toString();
  }
}
