package com.automationanywhere.cognitive.common.healthapi.constants;
/**
 * @author shweta.thakur
 *
 * This class represents the Http Status Code
 * 
 */
public enum HTTPStatusCode {
	INTERNAL_SERVER_ERROR(500, "Internal server error"), SERVICE_UNAVAILABLE(
			503, "Service unavailable"), OK(200, "OK"), UNDER_MAINTENANCE(599,
			"Under Maintenance"), RESOURCE_NOT_FOUND(404, "Resources Not found"), REQUEST_TIMEOUT(
			408, "Request Timeout");

	private int code;
	private String message;

	HTTPStatusCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
