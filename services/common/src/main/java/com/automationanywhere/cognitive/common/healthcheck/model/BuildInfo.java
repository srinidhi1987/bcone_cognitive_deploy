package com.automationanywhere.cognitive.common.healthcheck.model;

/**
 * Responsible for keep the data of build.
 */
public class BuildInfo {

  private final String appName;
  private final String version;
  private final String branch;
  private final String git;
  private final String buildTime;

  /**
   * Full constructor.
   */
  public BuildInfo(
      String appName,
      String version,
      String branch,
      String git,
      String buildTime
  ) {
    this.appName = appName;
    this.version = version;
    this.branch = branch;
    this.git = git;
    this.buildTime = buildTime;
  }

  /**
   * Returns the appName.
   *
   * @return the value of appName
   */
  public String getAppName() {
    return appName;
  }

  /**
   * Returns the version.
   *
   * @return the value of version
   */
  public String getVersion() {
    return version;
  }

  /**
   * Returns the branch.
   *
   * @return the value of branch
   */
  public String getBranch() {
    return branch;
  }

  /**
   * Returns the git.
   *
   * @return the value of git
   */
  public String getGit() {
    return git;
  }

  /**
   * Returns the buildTime.
   *
   * @return the value of buildTime
   */
  public String getBuildTime() {
    return buildTime;
  }
}
