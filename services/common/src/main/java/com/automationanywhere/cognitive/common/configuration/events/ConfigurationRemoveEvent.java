package com.automationanywhere.cognitive.common.configuration.events;

/**
 * The event is sent whenever configuration provider removes a configuration parameter.
 */
public class ConfigurationRemoveEvent extends ConfigurationStructureEvent {
  public ConfigurationRemoveEvent(final String key, final Object value) {
    super(key, value);
  }
}
