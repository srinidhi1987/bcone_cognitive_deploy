package com.automationanywhere.cognitive.common.healthcheck.model;

/**
 * Possible status.
 */
public enum Status {
    OK, NOT_OK
}
