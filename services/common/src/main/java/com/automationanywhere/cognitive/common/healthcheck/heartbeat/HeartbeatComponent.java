package com.automationanywhere.cognitive.common.healthcheck.heartbeat;

import java.util.concurrent.CompletableFuture;

/**
 * All components to handle heartbeats should implements this class.
 *
 * @param <T> the class used by this component as <code>Heartbeat</code> class to keeps the heartbeat information.
 */
public interface HeartbeatComponent<T> {

    /**
     * Prepares the heartbeat with related info as build information.
     *
     * @return An instance of status object with the heartbeat information.
     */
    CompletableFuture<T> prepareHeartbeat();

    /**
     * Converts the status object in string.
     *
     * @param obj the status object.
     * @return the string converted from the status object.
     */
    String convert(T obj);
}
