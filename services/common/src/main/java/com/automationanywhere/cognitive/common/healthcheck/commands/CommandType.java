package com.automationanywhere.cognitive.common.healthcheck.commands;

/**
 * The types that commands can be
 */
public enum CommandType {

    APPLICATION, DEPENDENCY
}
