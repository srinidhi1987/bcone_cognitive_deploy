package com.automationanywhere.cognitive.common.infrastructure.iam.controlroom;

import com.automationanywhere.cognitive.common.domain.service.iam.Authorization;
import com.automationanywhere.cognitive.common.domain.service.iam.exceptions.AuthorizationException;
import com.automationanywhere.cognitive.common.infrastructure.controlroom.ControlRoomSdkWrapper;
import io.jsonwebtoken.Claims;

/**
 * Implementation to send authorization token to Control Room.
 */
public class ControlRoomAuthorization implements Authorization {

  private final ControlRoomSdkWrapper sdk;

  public ControlRoomAuthorization(final ControlRoomSdkWrapper sdk) {
    this.sdk = sdk;
  }

  @Override
  public void authorize(String token) throws AuthorizationException {
    try {
      Claims claims = sdk.getClaims(token);
      sdk.validateToken(token, claims);
    } catch (final Exception ex) {
      throw new AuthorizationException(ex);
    }
  }
}
