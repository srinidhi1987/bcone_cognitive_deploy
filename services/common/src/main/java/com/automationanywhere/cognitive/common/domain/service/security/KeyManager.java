package com.automationanywhere.cognitive.common.domain.service.security;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyGeneratorFactory;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;

public class KeyManager {

  private final KeyProvider keyProvider;

  public KeyManager(KeyProvider keyProvider) {
    this.keyProvider = keyProvider;
  }

  public void createKey(KeyName name) {
    CryptographyKey key = KeyGeneratorFactory.keyGenerator(name.getKeyType()).generateKey();
    keyProvider.saveKey(name, key);
  }

  public void migrateKey(KeyName keyName, KeyProvider destination) {
    CryptographyKey key = keyProvider.getKey(keyName);
    destination.saveKey(keyName, key);
  }

  public void removeKey(KeyName name) {
    keyProvider.removeKey(name);
  }

  public CryptographyKey retrieveKey(KeyName name) {
    return keyProvider.getKey(name);
  }
}
