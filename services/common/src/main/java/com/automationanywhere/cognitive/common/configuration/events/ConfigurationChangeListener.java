package com.automationanywhere.cognitive.common.configuration.events;

/**
 * A listener will be notified whenever a configuration changes
 * and the listener is registered to listen to changes of the configuration.
 */
public interface ConfigurationChangeListener {
  /**
   * Triggered by a configuration provider when it detects a configuration parameter change.
   *
   * @param event - details of the configuration parameter change.
   */
  void onEvent(ConfigurationEvent event);
}
