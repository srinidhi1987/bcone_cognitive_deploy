package com.automationanywhere.cognitive.common.infrastructure.security.database.mappers;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.AsymmetricKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.SymmetricKey;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKey;

public class CryptographyKeyMapper {

  public static CryptographyKey cryptographySymmetricKey(final SecurityKey securityKey) {
    return new SymmetricKey(securityKey.getKeyValue());
  }

  public static CryptographyKey cryptographyAsymmetricKey(final SecurityKey privateKey,
      final SecurityKey publicKey) {
    return new AsymmetricKey(privateKey.getKeyValue(), publicKey.getKeyValue());
  }
}
