package com.automationanywhere.cognitive.common.configuration.exceptions;

/**
 * Thrown when a key is not a primitive value.
 */
public class NotValueException extends RuntimeException {
  public static final long serialVersionUID = 1;

  public NotValueException(final String message) {
    super(message);
  }
}
