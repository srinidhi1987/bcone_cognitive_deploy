package com.automationanywhere.cognitive.common.futures;

import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class CompletableFutureConverter {

    public static <T, U> CompletableFuture<T> convert(ListenableFuture<U> future, Function<U, T> function) {

        CompletableFuture<T> completableFuture = new CompletableFuture<>();
        future.addCallback(new ListenableFutureCallback<U>() {
            @Override
            public void onFailure(Throwable ex) {
                completableFuture.completeExceptionally(ex);
            }

            @Override
            public void onSuccess(U result) {
                completableFuture.complete(function.apply(result));
            }
        });
        return completableFuture;
    }
}
