package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

public class SymmetricKey implements CryptographyKey {

  private final String symmetricKey;

  public SymmetricKey(String symmetricKey) {
    this.symmetricKey = symmetricKey;
  }

  public String key() {
    return symmetricKey;
  }
}
