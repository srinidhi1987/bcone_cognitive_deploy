package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import com.automationanywhere.cognitive.common.domain.service.security.exceptions.KeyGenerationException;

public interface SecurityKeyGenerator {
   public CryptographyKey generateKey() throws KeyGenerationException;
}
