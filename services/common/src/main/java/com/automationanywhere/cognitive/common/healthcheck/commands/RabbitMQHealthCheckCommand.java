package com.automationanywhere.cognitive.common.healthcheck.commands;

import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.DependencyHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;

import java.util.concurrent.CompletableFuture;

/**
 * Default health check command to check the connection of RabbitMQ server.
 */
public class RabbitMQHealthCheckCommand implements DetailedHealthCheckCommand {

    private String name;

    /**
     * Constructs with the name of command.
     *
     * @param name the command name
     */
    public RabbitMQHealthCheckCommand(String name) {
        this.name = name;
    }

    @Override
    public CommandType getType() {
        return CommandType.DEPENDENCY;
    }

    @Override
    public CompletableFuture<HealthCheckStatus> executeCheck() {
        return CompletableFuture.completedFuture(new DependencyHealthCheckStatus(new Dependency(name, Status.OK)));
    }
}
