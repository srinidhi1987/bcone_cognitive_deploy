package com.automationanywhere.cognitive.common.healthapi.constants;
/**
 * @author shweta.thakur
 *
 * This class represents the Mode of health check /Heart beat
 * 
 */
public enum SystemStatus {
	ONLINE, OFFLINE
}
