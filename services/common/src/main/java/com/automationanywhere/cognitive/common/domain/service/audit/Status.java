package com.automationanywhere.cognitive.common.domain.service.audit;

public enum Status {
  SUCCESS("Success"),
  FAILURE("Failure");

  public final String name;

  Status(final String name) {
    this.name = name;
  }
}
