package com.automationanywhere.cognitive.common.security;

/**
 * Base exception for unclassified errors thrown by the {@link CipherService}
 */
public class CipherException extends RuntimeException {
    
  private static final long serialVersionUID = -2195117392496586631L;

  public CipherException(final String message) {
    super(message);
  }

  public CipherException(final Exception ex) {
    super(ex);
  }
}
