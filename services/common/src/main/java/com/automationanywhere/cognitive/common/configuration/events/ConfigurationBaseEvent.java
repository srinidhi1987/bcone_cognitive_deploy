package com.automationanywhere.cognitive.common.configuration.events;

/**
 * Default implementation of the configuration event.
 */
public abstract class ConfigurationBaseEvent implements ConfigurationEvent {
  public final String key;

  protected ConfigurationBaseEvent(final String key) {
    this.key = key;
  }

  @Override
  public String getKey() {
    return key;
  }
}
