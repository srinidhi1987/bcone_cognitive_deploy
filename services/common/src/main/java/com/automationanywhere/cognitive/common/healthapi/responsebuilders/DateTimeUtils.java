package com.automationanywhere.cognitive.common.healthapi.responsebuilders;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public class DateTimeUtils {

    /**
     * This function will return the application up time 
     * @return
     */
    public static String generateTimeInDaysHoursMinSecs() {
        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
        long timeInMs = rb.getUptime();
        long milliseconds;
        milliseconds = timeInMs;
        long seconds = milliseconds / 1000;
        long minutes = seconds / 60;
        seconds %= 60;
        long hours = minutes / 60;
        minutes %= 60;
        long days = hours / 24;
        hours %= 24;
        return days + "d " + hours + "h " + minutes + "m " + seconds + "s";
    }
}
