package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import com.automationanywhere.cognitive.common.domain.service.security.exceptions.BadSignatureException;
import com.automationanywhere.cognitive.common.domain.service.security.exceptions.CipherException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.concurrent.Callable;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Default implementation uses a symmetric secret key to encrypt and decrypt messages. The key
 * algorithm should be compatible with the CBC mode of the operation, the ISO10126 padding method
 * and a random 16 byte initialization vector.
 *
 * Before the encryption the message will be appended with 10 bytes of a signature:
 *
 * 0xB7B9FB14D95230F4C67A
 *
 * After that the concatenated value will be encrypted using a random initialization vector, which
 * later will be appended to the encrypted message:
 *
 * result = IV + encrypt(signature + message)
 */
public class SymmetricKeyAlgorithm implements KeyBasedAlgorithms {

  private static final String CIPHER_SPEC = "/CBC/PKCS5Padding";
  private static final int MAX_KEY_SIZE = 1024;

  // CBC IV length for AES is constant and is 16 bytes long.
  private static final int IV_LENGTH = 16;

  private static final byte[] SIGNATURE = {
      0x7a, (byte) 0xc6, (byte) 0xf4, 0x30, 0x52,
      (byte) 0xd9, 0x14, (byte) 0xfb, (byte) 0xb9, (byte) 0xb7
  };

  private final SecureRandom rng = new SecureRandom();

  @Override
  public InputStream encrypt(CryptographyKey key, InputStream data) throws CipherException {
    return createInputStream(() -> {
      byte[] iv = createIV();
      Cipher cipher = createCipher(true, iv, key);
      return new InputStreamWithHeader(
          iv, new CipherInputStream(new InputStreamWithHeader(SIGNATURE, data), cipher)
      );
    });
  }

  @Override
  public InputStream decrypt(CryptographyKey key, InputStream data) throws CipherException {
    return createInputStream(() -> {
      Cipher cipher = createCipher(false, getIV(data), key);
      CipherInputStream is = new CipherInputStream(data, cipher);
      validateSignature(is);
      return is;
    });
  }

  InputStream createInputStream(final Callable<InputStream> callable) {
    InputStream result = null;
    try {
      result = callable.call();
    } catch (final RuntimeException ex) {
      throw ex;
    } catch (final Exception ex) {
      throw new CipherException(ex);
    }
    return result;
  }

  void validateSignature(final InputStream result) throws IOException {
    for (int index = 0; index < SIGNATURE.length; index += 1) {
      int value = result.read();
      if (value == -1) {
        throw new BadSignatureException("Wrong signature size");
      }
      if ((byte) value != SIGNATURE[index]) {
        throw new BadSignatureException("Not a valid signature");
      }
    }
  }

  private Cipher createCipher(final boolean encrypt, final byte[] iv, final CryptographyKey cryptographyKey)
      throws Exception {
    String symmetricKey = ((SymmetricKey) cryptographyKey).key();
    Key key = streamToSecretKey(symmetricKey, "AES");
    Cipher cipher = Cipher.getInstance(
        key.getAlgorithm() + CIPHER_SPEC
    );
    cipher.init(
        encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE,
        key,
        new IvParameterSpec(iv)
    );
    return cipher;
  }

  private byte[] getIV(final InputStream input) throws IOException {
    byte[] iv = new byte[IV_LENGTH];
    for (int index = 0; index < IV_LENGTH; index += 1) {
      int value = input.read();
      if (value == -1) {
        throw new CipherException("IV size is wrong");
      }
      iv[index] = (byte) value;
    }
    return iv;
  }

  private byte[] createIV() {
    byte[] iv = new byte[IV_LENGTH];
    rng.nextBytes(iv);
    return iv;
  }

  private Key streamToSecretKey(final String symmetricKey, final String type) throws IOException {
    Key secretKey = null;
    try {
      // Decodes a Base64 encoded String into a byte array
      byte[] decodedKey = Base64.getDecoder().decode(symmetricKey);

      // Constructs a secret key from the given byte array
      secretKey = new SecretKeySpec(symmetricKey.getBytes(), 0,
          decodedKey.length, type);
    } catch (Exception ex) {
      throw new CipherException("Invalid Key!", ex);
    }

    return secretKey;
  }
}
