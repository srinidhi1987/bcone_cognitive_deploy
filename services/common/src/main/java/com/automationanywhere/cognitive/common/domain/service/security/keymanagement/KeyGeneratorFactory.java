package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

public class KeyGeneratorFactory {

  public static SecurityKeyGenerator keyGenerator(KeyType keyType){
    SecurityKeyGenerator keyGenerator = null;
    switch(keyType){
      case SYMMETRIC: keyGenerator = new SymmetricKeyGenerator();
      break;
      case ASYMMETRIC: keyGenerator = new AsymmetricKeyGenerator();
    }
    return keyGenerator;
  }
}
