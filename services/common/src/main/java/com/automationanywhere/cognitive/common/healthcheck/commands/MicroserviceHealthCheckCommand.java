package com.automationanywhere.cognitive.common.healthcheck.commands;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.concurrent.CompletableFuture;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Default health check command for all Microservices.
 */
public class MicroserviceHealthCheckCommand implements DetailedHealthCheckCommand {

  private String name;
  private String host;
  private BuildInfoFactory buildInfoFactory;
  private AppInfoFactory appInfoFactory;
  private AsyncRestTemplate restTemplate;

  /**
   * Constructs a health check command for Microservice.
   *
   * @param host the host of the microservice (url + port), shouldn't have the /healthcheck in the
   * URL, it will be added by the system before call the microservice.
   */
  @SuppressWarnings("unused")
  public MicroserviceHealthCheckCommand(
      final String name,
      final String host,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory,
      final AsyncRestTemplate restTemplate
  ) {
    this.name = name;
    this.host = host;
    this.buildInfoFactory = buildInfoFactory;
    this.appInfoFactory = appInfoFactory;
    this.restTemplate = restTemplate;
  }

  @Async
  @Override
  public CompletableFuture<HealthCheckStatus> executeCheck() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<Application> entity = new HttpEntity<>(headers);
    String url = host + "/healthcheck";
    ListenableFuture<ResponseEntity<Application>> responseEntity =
        restTemplate.exchange(url, HttpMethod.GET, entity, Application.class);

    CompletableFuture<HealthCheckStatus> future = new CompletableFuture<>();
    responseEntity.addCallback(new ListenableFutureCallback<ResponseEntity<Application>>() {
      @Override
      public void onFailure(Throwable ex) {
        future.complete(parseError(ex));
      }

      @Override
      public void onSuccess(ResponseEntity<Application> result) {
        future.complete(new ApplicationHealthCheckStatus(result.getBody()));
      }
    });
    return future;
  }

  @Override
  public CommandType getType() {
    return CommandType.APPLICATION;
  }

  /**
   * Creates an error status with the message error when the check call throws an exception or was not possible.
   *
   * @param e the exception thrown trying to execute the check health of the microservice.
   * @return the status instance
   */
  private HealthCheckStatus parseError(Throwable e) {
    Application application = new Application();
    application.setBuildInfo(buildInfoFactory.build(this.name));
    application.setAppInfo(appInfoFactory.build(
        this.name,
        getType().name(),
        Status.NOT_OK,
        e.getMessage(),
        ""
    ));
    return new ApplicationHealthCheckStatus(application);
  }
}
