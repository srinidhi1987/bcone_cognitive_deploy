package com.automationanywhere.cognitive.common.healthcheck.components;

import com.automationanywhere.cognitive.common.healthcheck.commands.DetailedHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.commands.HealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BaseHealthCheckCommand<C extends HealthCheckCommand> implements HealthCheckComponent<Application, C> {
  private static final Logger LOGGER = LogManager.getLogger(DetailedHealthCheckComponent.class);

  protected CompletableFuture[] getCompletableFutures(
      final List<C> commands
  ) {
    return commands.stream()
        .map(command -> command.executeCheck().handleAsync((checkStatus, th) -> {
          if (th != null) {
            LOGGER.error("Unhandled error", th);
          }
          return checkStatus;
        })).toArray(CompletableFuture[]::new);
  }

  protected List<HealthCheckStatus> getHealthCheckStatuses(final CompletableFuture[] commandFutures) {
    return Arrays.stream(commandFutures)
        .map(cf -> {
          HealthCheckStatus healthCheckStatus = null;
          try {
            healthCheckStatus = (HealthCheckStatus) cf.get();
          } catch (final Exception ex) {
            LOGGER.error("Unexpected error", ex);
          }
          return healthCheckStatus;
        }).collect(Collectors.toList());
  }
}
