package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import com.automationanywhere.cognitive.common.domain.service.security.exceptions.KeyGenerationException;
import java.io.IOException;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import javax.xml.bind.DatatypeConverter;
import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;
import org.bouncycastle.util.io.pem.PemGenerationException;
import org.bouncycastle.util.io.pem.PemObject;

public class AsymmetricKeyGenerator implements SecurityKeyGenerator {

  private static int KEY_PAIR_KEY_SIZE = 2048;
  private static final String RSA = "RSA";

  public CryptographyKey generateKey() throws KeyGenerationException{
    try {
      Security.addProvider(new BouncyCastleFipsProvider());
      KeyPairGenerator keyGen = KeyPairGenerator.getInstance(RSA);
      keyGen.initialize(KEY_PAIR_KEY_SIZE, SecureRandom.getInstanceStrong());
      KeyPair keyPair = keyGen.generateKeyPair();
      String privateKey = privateKeyToPKCS8(keyPair.getPrivate());
      String publicKey = publicKeyToPKCS8(keyPair.getPublic());
      return new AsymmetricKey(privateKey, publicKey);
    } catch (Exception e) {
      throw new KeyGenerationException("Error while generating the asymmetric key pair!", e);
    }
  }

  private  String privateKeyToPKCS8(PrivateKey privateKey) {
    StringWriter privateKeyPKCS8 = new StringWriter();
    try (final JcaPEMWriter pw = new JcaPEMWriter(privateKeyPKCS8)) {
      JcaPKCS8Generator jcsPKCS8Generator = new JcaPKCS8Generator(privateKey, null);
      PemObject pemObject = jcsPKCS8Generator.generate();
      pw.writeObject(pemObject);
    }
    catch(IOException ioe){
      throw new KeyGenerationException("Error while converting private key to PKCS8!", ioe);
    }
    return privateKeyPKCS8.toString();
  }

  private  String publicKeyToPKCS8(PublicKey publicKey) {
    final StringWriter publicKeyPKCS8 = new StringWriter();
    try (final JcaPEMWriter pemWriter2 = new JcaPEMWriter(publicKeyPKCS8)) {
      pemWriter2.writeObject(publicKey);
    }
    catch (IOException ioe){
      throw new KeyGenerationException("Error while converting publickey to PKCS8!", ioe);
    }
    return publicKeyPKCS8.toString();
  }
}
