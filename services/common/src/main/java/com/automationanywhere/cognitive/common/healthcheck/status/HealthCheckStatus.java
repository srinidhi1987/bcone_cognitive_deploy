package com.automationanywhere.cognitive.common.healthcheck.status;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;

/**
 * Responsible for keep the status of health check.
 */
public interface HealthCheckStatus {

    /**
     * Returns the type of the health check command.
     *
     * @return the type of command
     */
    CommandType getCommandType();

    /**
     * Returns the status of application.
     *
     * @return the status.
     */
    Status getStatus();
}
