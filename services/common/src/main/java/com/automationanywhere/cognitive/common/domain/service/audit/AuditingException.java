package com.automationanywhere.cognitive.common.domain.service.audit;

public class AuditingException extends RuntimeException {

  private static final long serialVersionUID = -4968642626420324546L;

  public AuditingException(final Throwable cause) {
    super(cause);
  }
}
