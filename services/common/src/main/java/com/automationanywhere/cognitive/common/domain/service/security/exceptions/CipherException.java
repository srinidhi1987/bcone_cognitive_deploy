package com.automationanywhere.cognitive.common.domain.service.security.exceptions;

import com.automationanywhere.cognitive.common.security.CipherService;

/**
 * Base exception for unclassified errors thrown by the {@link CipherService}
 */
public class CipherException extends RuntimeException {

  private static final long serialVersionUID = -7733499085717882488L;

  public CipherException(final String message) {
    super(message);
  }

  public CipherException(final Exception ex) {
    super(ex);
  }

  public CipherException(final String message, final Exception ex) {
    super(message, ex);
  }
}
