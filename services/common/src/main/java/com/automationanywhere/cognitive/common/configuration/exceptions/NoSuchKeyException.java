package com.automationanywhere.cognitive.common.configuration.exceptions;

/**
 * Thrown when a key is unknown to the configuration provider.
 */
public class NoSuchKeyException extends RuntimeException {
  public static final long serialVersionUID = 1;

  public NoSuchKeyException(final String message) {
    super(message);
  }
}
