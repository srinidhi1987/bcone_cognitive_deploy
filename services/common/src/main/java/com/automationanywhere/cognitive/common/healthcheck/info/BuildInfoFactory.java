package com.automationanywhere.cognitive.common.healthcheck.info;

import com.automationanywhere.cognitive.common.healthcheck.exception.BuildInfoFileNotFoundException;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import java.io.InputStream;
import java.util.Properties;
import org.springframework.stereotype.Component;

@Component
public class BuildInfoFactory {

  private static final String BUILD_VERSION_FILE = "/build.version";

  public BuildInfo build(String name) {
    return new BuildInfo(name, "", "", "", "");
  }

  public BuildInfo build(String name, String version, String branch, String git, String buildTime) {
    return new BuildInfo(name, version, branch, git, buildTime);
  }

  public BuildInfo build() {
    Properties prop = new Properties();
    try (final InputStream stream = BuildInfo.class.getResourceAsStream(BUILD_VERSION_FILE)) {
      prop.load(stream);

    } catch (Exception e) {
      throw new BuildInfoFileNotFoundException(e);
    }

    return new BuildInfo(
        prop.getProperty("Application-name"),
        prop.getProperty("Version"),
        prop.getProperty("Branch"),
        prop.getProperty("GitHash"),
        prop.getProperty("Buildtime")
    );
  }
}
