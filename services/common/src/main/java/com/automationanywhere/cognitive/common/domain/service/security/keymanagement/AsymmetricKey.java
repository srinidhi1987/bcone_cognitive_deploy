package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

public class AsymmetricKey implements CryptographyKey {

  private final String privateKey;
  private final String publicKey;

  public AsymmetricKey(String privateKey, String publicKey) {
    this.privateKey = privateKey;
    this.publicKey = publicKey;
  }

   public String privateKey() {
    return privateKey;
  }

  public String publicKey() {
    return publicKey;
  }
}
