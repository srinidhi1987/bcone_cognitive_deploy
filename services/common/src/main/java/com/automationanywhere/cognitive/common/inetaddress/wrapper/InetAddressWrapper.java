package com.automationanywhere.cognitive.common.inetaddress.wrapper;

import java.net.InetAddress;
import java.net.UnknownHostException;
import com.automationanywhere.cognitive.common.logger.AALogger;

public class InetAddressWrapper {

    private static AALogger aaLogger = AALogger.create(InetAddressWrapper.class);

    public static String getFullyQualifiedName() {
        aaLogger.entry();
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            aaLogger.exit();
            return inetAddress.getCanonicalHostName();
        } catch (UnknownHostException e) {
            aaLogger.error(e.getMessage(), e);
            return "localhost";
        }
    }
}
