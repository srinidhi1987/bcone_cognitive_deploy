package com.automationanywhere.cognitive.common.security;

import java.io.InputStream;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;

/**
 * The default implementation reads a symmetric secret key of a given type from a stream.
 */
public class StreamKeyProvider implements KeyProvider {

  public static final int MAX_KEY_SIZE = 1024;

  private final Key key;

  public StreamKeyProvider(final String type, final InputStream is) {
    try {
      byte buffer[] = new byte[MAX_KEY_SIZE / 8];
      int n = 0;
      int offset = 0;
      int len = buffer.length;
      while (len > 0 && (n = is.read(buffer, offset, len)) != -1) {
        offset += n;
        len -= n;
      }

      if (n != -1) {
        throw new CipherException("Key is too long, the supported max key size is " + MAX_KEY_SIZE);
      }

      key = new SecretKeySpec(buffer, 0, offset, type);
    } catch (final RuntimeException ex) {
      throw ex;
    } catch (final Exception ex) {
      throw new CipherException(ex);
    }
  }

  @Override
  public Key getKey() {
    return key;
  }
}
