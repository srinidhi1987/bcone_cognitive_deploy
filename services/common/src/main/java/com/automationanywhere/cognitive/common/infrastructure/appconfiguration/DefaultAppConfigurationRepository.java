package com.automationanywhere.cognitive.common.infrastructure.appconfiguration;

import static com.automationanywhere.cognitive.common.infrastructure.appconfiguration.QueryBuilder.APP_ID_KEY_NAME;
import static com.automationanywhere.cognitive.common.infrastructure.appconfiguration.QueryBuilder.APP_REGISTERED_KEY_NAME;
import static com.automationanywhere.cognitive.common.infrastructure.appconfiguration.QueryBuilder.CR_URL_KEY_NAME;
import static com.automationanywhere.cognitive.common.infrastructure.appconfiguration.QueryBuilder.CR_VERSION_KEY_NAME;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.appconfiguration.Configuration;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

public class DefaultAppConfigurationRepository implements AppConfigurationRepository {

  public static final String CONFIGURATIONS_TRANSACTION_MANAGER_NAME = "configurationsTransactionManager";

  private final SessionFactory sessionFactory;

  public DefaultAppConfigurationRepository(final SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  @Transactional(transactionManager = CONFIGURATIONS_TRANSACTION_MANAGER_NAME)
  public void save(final Configuration configuration) {
    try {
      Session session = getSession();

      int count = (Integer) session.createNativeQuery(
          "SELECT COUNT(c.[Key]) FROM Configurations c WHERE c.[Key]=:key")
          .setParameter("key", APP_ID_KEY_NAME)
          .getSingleResult();

      QueryBuilder queryBuilder = null;
      if (count == 0) {
        queryBuilder = InsertQueryBuilder.create(session);
      } else if (count == 1) {
        queryBuilder = UpdateQueryBuilder.create(session);
      } else {
        throw new DatabaseException(
            "A configuration record about only one Control Room was expected.");
      }
      queryBuilder.put(APP_ID_KEY_NAME, configuration.appId)
          .put(CR_URL_KEY_NAME, configuration.controlRoomUrl)
          .put(CR_VERSION_KEY_NAME, configuration.controlRoomVersion)
          .put(APP_REGISTERED_KEY_NAME, configuration.registered.toString())
          .build().forEach(Query::executeUpdate);
    } catch (final Exception ex) {
      throw new DatabaseException(ex);
    }
  }

  @Override
  @Transactional(transactionManager = CONFIGURATIONS_TRANSACTION_MANAGER_NAME)
  public Configuration load() {
    List<?> records = null;
    try {
      Session session = getSession();
      records = (List<?>) session.createNativeQuery(
          "SELECT c.[Key], c.[Value] FROM Configurations c WHERE c.[Key] IN (:keys)"
      ).setParameter("keys", QueryBuilder.KEY_NAMES).getResultList();
    } catch (final Exception ex) {
      throw new DatabaseException(ex);
    }
    return records.isEmpty() ? null : new Configuration(
        find(records, CR_URL_KEY_NAME),
        find(records, CR_VERSION_KEY_NAME),
        find(records, APP_ID_KEY_NAME),
        Boolean.parseBoolean(find(records, APP_REGISTERED_KEY_NAME))
    );
  }

  private String find(final List<?> records, final String columnName) {
    Object[] objects = records.stream().map(record -> (Object[]) record)
        .filter(record -> columnName.equals(record[0]))
        .findAny()
        .get();// records are not empty and all keys must present, so #get cannot throw here.

    return (String) objects[1];
  }

  private Session getSession() {
    Session session = null;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (final HibernateException ex) {
      session = sessionFactory.openSession();
    }
    return session;
  }
}
