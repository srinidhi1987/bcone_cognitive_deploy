package com.automationanywhere.cognitive.common.healthapi.responsebuilders;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import com.automationanywhere.cognitive.common.exceptions.BuildFileNotFoundException;
import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.json.models.BuildInfo;
import com.automationanywhere.cognitive.common.healthapi.json.models.HeartBeat;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.healthapi.json.models.Subsytem;
import com.automationanywhere.cognitive.common.logger.AALogger;
/**
 * @author shweta.thakur
 *
 * This is a class to handle all the response creation for Healthcheck/Heartbeat
 * 
 */
public class HealthApiResponseBuilder {
	private static AALogger aaLogger = AALogger.create(HealthApiResponseBuilder.class);	
	/**
	 * Create a build info object based on the data from build.version file
	 * @return
	 */
	public static BuildInfo getBuildInfo(){
		BuildInfo buildInfo = null;
		BuildConfigurationReader buildConfigurationReader = new BuildConfigurationReader();
		Properties prop = null;
		try {
			prop = buildConfigurationReader.readBuildVersionFile();
			buildInfo = prepareBuildInfo(prop);
			return buildInfo;
		} catch (IOException e) {
			aaLogger.error("Error reading build.version file", e.getMessage());
			throw new BuildFileNotFoundException("Error reading build.version file", e);
		}
	}

	/**
	 * @param prop
	 * @return
	 */
	public static BuildInfo prepareBuildInfo(Properties prop) {
		BuildInfo buildInfo;
		buildInfo = new BuildInfo(prop.getProperty("Application-name"),
				prop.getProperty("Version"), prop.getProperty("Branch"),
				prop.getProperty("GitHash"), prop.getProperty("Buildtime"));
		return buildInfo;
	}

	/**
	 * @param httpCode
	 * @param mode
	 * @return
	 */
	public static String prepareHeartBeat(HTTPStatusCode httpCode,SystemStatus mode) {
		HeartBeat heartBeat = new HeartBeat();
		try {
			BuildInfo buildInfo = getBuildInfo();
			heartBeat.setBuildInfo(buildInfo);
			heartBeat.setHttpStatusCode(httpCode);
			heartBeat.setMode(mode);
		} catch (BuildFileNotFoundException e) {
			heartBeat.setHttpStatusCode(HTTPStatusCode.INTERNAL_SERVER_ERROR);
			heartBeat.setMode(SystemStatus.OFFLINE);
		}
		return heartBeat.toString();
	}
	
    /**
     * @param httpStatusCode
     * @param systemStatus
     * @param applicationName
     * @param dbConnectivity
     * @param mqConnectivity
     * @param serviceConnectivities
     * @return
     */
    public static String prepareSubSystem(HTTPStatusCode httpStatusCode,
            SystemStatus systemStatus, Connectivity dbConnectivity,
            Connectivity mqConnectivity,Connectivity crConnectivity,
            List<ServiceConnectivity> serviceConnectivities) {
        Subsytem subsytem = null;
        BuildInfo buildInfo = null;
        try {
            buildInfo = getBuildInfo();
            subsytem = new Subsytem.SubsystemBuilder(httpStatusCode,
                    systemStatus, buildInfo).setDBConnectivity(dbConnectivity)
                    .setMQConnectivity(mqConnectivity).setCRConnectivity(crConnectivity)
                    .setServiceConnectivity(serviceConnectivities).build();
        } catch (BuildFileNotFoundException buildFileNotFound) {
            httpStatusCode = HTTPStatusCode.INTERNAL_SERVER_ERROR;
            systemStatus = SystemStatus.OFFLINE;
            subsytem = new Subsytem.SubsystemBuilder(httpStatusCode,
                    systemStatus, buildInfo).setDBConnectivity(dbConnectivity)
                    .setMQConnectivity(mqConnectivity).setCRConnectivity(crConnectivity)
                    .setServiceConnectivity(serviceConnectivities).build();
        }
        return subsytem.toString();
    }

}
