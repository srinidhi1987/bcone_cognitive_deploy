package com.automationanywhere.cognitive.common.exceptions;

public class BuildFileNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1156190698088860555L;
    
	public BuildFileNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BuildFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}