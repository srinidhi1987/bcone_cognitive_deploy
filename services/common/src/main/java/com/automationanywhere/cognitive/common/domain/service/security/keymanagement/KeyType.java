package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

public enum KeyType {
  ASYMMETRIC, SYMMETRIC
}
