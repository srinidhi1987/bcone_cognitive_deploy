package com.automationanywhere.cognitive.common.infrastructure.security.database.persistence;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKey;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKeyRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class HibernateSecurityKeyRepository implements SecurityKeyRepository {

  private final SessionFactory sessionFactory;

  public HibernateSecurityKeyRepository(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public void add(final SecurityKey securityKey) {
    Session session = this.session();
    Transaction transaction = null;
    try {
      transaction = session.beginTransaction();
      session.save(securityKey);
      transaction.commit();
    } catch (HibernateException he) {
      if (transaction != null) {
        transaction.rollback();
      }
      throw he;
    } finally {
      session.close();
    }
  }

  @Override
  public void remove(final SecurityKey securityKey) {
    Session session = this.session();
    Transaction transaction = null;
    try {
      transaction = session.beginTransaction();
      session.delete(securityKey);
      transaction.commit();
    } catch (HibernateException he) {
      if (transaction != null) {
        transaction.rollback();
      }
      throw he;
    } finally {
      session.close();
    }

  }

  @Override
  public SecurityKey securityKeyByName(final KeyName name) {
    Session session = this.session();
    Transaction transaction = null;
    SecurityKey securityKey = null;
    try {
      transaction = session.beginTransaction();
      securityKey = (SecurityKey) session
          .createNativeQuery("SELECT * FROM SecurityKeys where KeyName=?")
          .setParameter(1, name.toString()).addEntity(SecurityKey.class).uniqueResult();
      transaction.commit();
    } catch (HibernateException he) {
      if (transaction != null) {
        transaction.rollback();
      }
      throw he;
    } finally {
      session.close();
    }
    return securityKey;
  }

  private Session session() {
    Session session = null;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (HibernateException ex) {
      session = sessionFactory.openSession();
    }
    return session;
  }
}
