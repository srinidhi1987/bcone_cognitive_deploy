package com.automationanywhere.cognitive.common.formatter;

/**
 * Created by msundell on 5/16/17
 */
@SuppressWarnings("WeakerAccess")
public class CategoryFormatter {

  /**
   * This method converts a category ID to a category name.
   * Todo: This implementation does not support non-English languages since the category name is
   * used in the user interface
   * @param categoryId category ID
   * @return <code>String</code> category name
   */
  @SuppressWarnings("WeakerAccess")
  public static String format(String categoryId) {
    String categoryName;

    switch (categoryId) {
      case "":
        categoryName = "Yet to be classified";
        break;
      case "-1":
        // Todo: -1 is a code magic number, this may need to be refactored
        // http://wiki.c2.com/?MagicNumber
        categoryName = "Unclassified";
        break;
      default:
        categoryName = "Group_" + categoryId;
        break;
    }
    return categoryName;
  }
}
