package com.automationanywhere.cognitive.common.infrastructure.security.database.exceptions;

public class DatabaseKeyProviderException extends RuntimeException{

  private static final long serialVersionUID = -2696890683426433185L;

  public DatabaseKeyProviderException(String message, Throwable cause) {
    super(message, cause);
  }

  public DatabaseKeyProviderException(String message) {
    super(message);
  }
}
