package com.automationanywhere.cognitive.common.domain.service.iam.exceptions;

public class AuthorizationException extends RuntimeException{

  private static final long serialVersionUID = -2744429938340057826L;

  public AuthorizationException(final Throwable cause){super(cause);}
}
