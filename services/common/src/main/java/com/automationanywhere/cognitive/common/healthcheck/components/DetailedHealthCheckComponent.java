package com.automationanywhere.cognitive.common.healthcheck.components;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.commands.DetailedHealthCheckCommand;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.DependencyHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Default implementation of base health check component. An instance of this class will be used if no subclass of
 * <code>HealthCheckComponent</code> was found in the classpath.
 */
public class DetailedHealthCheckComponent extends BaseHealthCheckCommand<DetailedHealthCheckCommand> {
  private static final char NEW_LINE = '\n';

  private BuildInfoFactory buildInfoFactory;
  private AppInfoFactory appInfoFactory;

  @Autowired
  public DetailedHealthCheckComponent(
      BuildInfoFactory buildInfoFactory,
      AppInfoFactory appInfoFactory
  ) {
    this.buildInfoFactory = buildInfoFactory;
    this.appInfoFactory = appInfoFactory;
  }

  @Override
  public CompletableFuture<Application> executeHealthCheck(List<DetailedHealthCheckCommand> commands) {
    CompletableFuture[] commandFutures = getCompletableFutures(commands);

    return CompletableFuture.allOf(commandFutures)
        .thenApply(ignoredVoid -> {
          List<HealthCheckStatus> healthCheckStatuses = getHealthCheckStatuses(commandFutures);

          Application app = new Application();
          app.setSubsystems(healthCheckStatuses.stream().filter(Objects::nonNull)
              .filter(cs -> cs.getCommandType() == CommandType.APPLICATION)
              .map(ct -> ((ApplicationHealthCheckStatus) ct).getApplication())
              .collect(Collectors.toList()));
          app.setDependencies(healthCheckStatuses.stream().filter(Objects::nonNull)
              .filter(cs -> cs.getCommandType() == CommandType.DEPENDENCY)
              .map(ct -> ((DependencyHealthCheckStatus) ct).getDependency())
              .collect(Collectors.toList()));

          long foundError = healthCheckStatuses.stream().filter(ct ->
              ct == null || ct.getStatus() == Status.NOT_OK
          ).count();

          String failReason;
          Status status;
          if (foundError == 0) {
            status = Status.OK;
            failReason = "";
          } else {
            status = Status.NOT_OK;
            if (foundError == 1) {
              failReason = "Found 1 error in subsystem or dependency";
            } else {
              failReason = "Found " + foundError + " errors in subsystems or dependencies";
            }
          }

          BuildInfo buildInfo = buildInfoFactory.build();
          app.setBuildInfo(buildInfo);
          app.setAppInfo(appInfoFactory.build(buildInfo.getAppName(), "", status, failReason));
          return app;
        });
  }

  @Override
  public String convert(Application app) {
    StringBuilder sb = new StringBuilder();
    convertApp(sb, app, "");
    return sb.toString();
  }

  private void convertApp(StringBuilder sb, Application app, String indentation) {
    AppInfo appInfo = app.getAppInfo();
    BuildInfo buildInfo = app.getBuildInfo();

    sb.append(indentation).append("Application: ").append(appInfo.getName()).append(NEW_LINE);
    sb.append(indentation).append("Status: ").append(appInfo.getStatus()).append(NEW_LINE);
    sb.append(indentation).append("Reason: ").append(appInfo.getFailReason()).append(NEW_LINE);
    sb.append(indentation).append("Version: ").append(buildInfo.getVersion()).append(NEW_LINE);
    sb.append(indentation).append("Branch: ").append(buildInfo.getBranch()).append(NEW_LINE);
    sb.append(indentation).append("GIT #: ").append(buildInfo.getGit()).append(NEW_LINE);
    sb.append(indentation).append("Build Time: ").append(buildInfo.getBuildTime()).append(NEW_LINE);
    sb.append(indentation).append("Application uptime: ").append(appInfo.getUptime())
        .append(NEW_LINE);

    List<Application> subsystems = app.getSubsystems();
    if (subsystems != null && !subsystems.isEmpty()) {
      sb.append(NEW_LINE).append(indentation).append("Subsystems: ").append(NEW_LINE);
      for (Application subApp : subsystems) {
        sb.append(NEW_LINE).append(indentation).append("  Subsystem: ").append(NEW_LINE);
        convertApp(sb, subApp, indentation + "    ");
      }
    }
    List<Dependency> dependencies = app.getDependencies();
    if (dependencies != null && !dependencies.isEmpty()) {
      sb.append(NEW_LINE).append(indentation).append("Dependencies: ").append(NEW_LINE);
      indentation += "  ";
      for (Dependency dep : dependencies) {
        sb.append(indentation).append(dep.getName()).append(": ").append(dep.getStatus())
            .append(NEW_LINE);
      }
    }
  }
}
