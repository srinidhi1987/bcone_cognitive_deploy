package com.automationanywhere.cognitive.common.healthapi.json.models;
/**
 * @author shweta.thakur
 *
 * This class represents the HeartBeat information
 * 
 */
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.DateTimeUtils;

public class HeartBeat {
	
	HTTPStatusCode httpStatusCode;
	SystemStatus mode;
	BuildInfo buildInfo;
	
	public HTTPStatusCode getHttpStatusCode() {
		return httpStatusCode;
	}
	
	public void setHttpStatusCode(HTTPStatusCode httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	
	public SystemStatus getMode() {
		return mode;
	}
	
	public void setMode(SystemStatus mode) {
		this.mode = mode;
	}
	
	/**
	 * @return the buildInfo
	 */
	public BuildInfo getBuildInfo() {
		return buildInfo;
	}
	/**
	 * @param buildInfo the buildInfo to set
	 */
	public void setBuildInfo(BuildInfo buildInfo) {
		this.buildInfo = buildInfo;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String buildInfoDetails;
		String applicationName = null;
		if (buildInfo == null )
		{
			buildInfoDetails = "BuildInfo=Error reading build.version file";
		}
		else
		{
			buildInfoDetails = buildInfo.toString();
			applicationName = buildInfo.getApplication();
		}
		return String.format("Application: %s" + "\nStatus: %s" +  "\nReason: %s" + "\n%s" + "\nApplication uptime: %s", applicationName, httpStatusCode,httpStatusCode.getMessage(),buildInfoDetails, DateTimeUtils.generateTimeInDaysHoursMinSecs());
	}
}
