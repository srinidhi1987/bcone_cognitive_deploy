package com.automationanywhere.cognitive.common.security;

import java.security.Key;

/**
 * Provides a symmetric secret key that can be used to encrypt and decrypt messages.
 */
public interface KeyProvider {

  /**
   * Returns a symmetric key.
   *
   * @return symmetric key
   */
  Key getKey();
}
