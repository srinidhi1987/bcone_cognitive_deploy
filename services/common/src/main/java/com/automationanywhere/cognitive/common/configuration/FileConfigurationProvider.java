package com.automationanywhere.cognitive.common.configuration;

import com.automationanywhere.cognitive.common.configuration.events.ConfigurationAddEvent;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationChangeEvent;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationChangeListener;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationEvent;
import com.automationanywhere.cognitive.common.configuration.events.ConfigurationRemoveEvent;
import com.automationanywhere.cognitive.common.configuration.exceptions.NoSuchKeyException;
import com.automationanywhere.cognitive.common.configuration.exceptions.NotContainerException;
import com.automationanywhere.cognitive.common.configuration.exceptions.NotValueException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.task.AsyncTaskExecutor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Default implementation that reads configuration from a file
 * that can be mapped to a JSON object by a given object mapper.
 *
 * Notifies registered listener if a configuration parameter changes, gets added or removed.
 */
public class FileConfigurationProvider implements ConfigurationProvider {
  private static final Logger LOGGER = LogManager.getLogger(FileConfigurationProvider.class);

  private final String filePath;
  private final ObjectMapper mapper;
  private final Set<FileConfigurationProvider.ListenerDetails> listeners = new HashSet<>();

  private AtomicReference<JsonNode> tree = new AtomicReference<>(NullNode.getInstance());
  private Future future;

  public FileConfigurationProvider(final ObjectMapper mapper, final String filePath) {
    this.mapper = mapper;
    this.filePath = filePath;
  }

  public void shutdown() {
    synchronized (this) {
      if (this.future != null) {
        this.future.cancel(true);
      }
    }
  }

  @Override
  public <T> T getValue(final String key) throws NoSuchKeyException, NotValueException, NotContainerException {
    return this.getValue(this.tree.get(), null, key);
  }

  private <T> T getValue(final JsonNode node, final String prefix, final String postfix) throws NoSuchKeyException, NotValueException, NotContainerException {
    T value = null;

    int index = postfix.indexOf(".");
    if (index == -1) {
      if (!node.has(postfix)) {
        throw new NoSuchKeyException(postfix + " not found");
      }

      JsonNode field = node.get(postfix);
      if (field.isContainerNode()) {
        throw new NotValueException(postfix + " is not a primitive value");
      }

      @SuppressWarnings("unchecked")
      T v = (T) this.getValue(field);
      value = v;
    } else {
      String name = postfix.substring(0, index);
      if (!node.has(name)) {
        throw new NoSuchKeyException(name + " not found");
      }

      JsonNode child = node.get(name);

      String newPrefix = this.createNewPrefix(prefix, name);

      if (!child.isContainerNode()) {
        throw new NotContainerException(newPrefix + " is a primitive");
      }

      value = this.getValue(child, newPrefix, postfix.substring(index + 1));
    }

    return value;
  }

  @Override
  public ConfigurationChangeListener subscribe(final String key, final ConfigurationChangeListener listener) {
    synchronized (this.listeners) {
      this.listeners.add(new FileConfigurationProvider.ListenerDetails(key, listener));
    }
    return listener;
  }

  @Override
  public ConfigurationChangeListener unsubscribe(final String key, final ConfigurationChangeListener listener) {
    ConfigurationChangeListener result = null;

    synchronized (this.listeners) {
      if (this.listeners.remove(new FileConfigurationProvider.ListenerDetails(key, listener))) {
        result = listener;
      }
    }

    return result;
  }

  public void refresh() {
    try {
      JsonNode newTree = this.mapper.readTree(new File(this.filePath));
      if (null == newTree) {
        return;
      }
      JsonNode oldTree = this.tree.getAndSet(newTree);

      this.compareAndNotify(null, oldTree, this.tree.get());
    } catch (final IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  public void listen(final AsyncTaskExecutor executor) {
    synchronized (this) {
      if (this.future == null) {
        CountDownLatch latch = new CountDownLatch(1);

        LOGGER.debug("Starting a file change listener");
        this.future = executor.submit(() -> {
          this.createFileChangeListener().listen(this, this.filePath, latch::countDown);
        });

        try {
          if (!latch.await(1000, TimeUnit.MILLISECONDS)) {
            throw new RuntimeException("Timeout while waiting the listener to start");
          }
        } catch (final InterruptedException ex) {
          throw new RuntimeException("Interrupted while waiting the listener to start", ex);
        }
      }
    }
  }

  private void compareAndNotify(final String prefix, final JsonNode oldNode, final JsonNode newNode) {
    if (oldNode.getNodeType() == newNode.getNodeType()) {
      if (oldNode.isContainerNode()) {
        this.compareAndNotifyRemoved(prefix, oldNode, newNode);
        this.compareAndNotifyAdded(prefix, oldNode, newNode);
      } else {
        Object ov = this.getValue(oldNode);
        Object nv = this.getValue(newNode);
        if (!Objects.equals(ov, nv)) {
          this.publishChangeEvent(prefix, ov, nv);
        }
      }
    } else {
      if (oldNode.isContainerNode()) {
        this.compareAndNotifyRemoved(prefix, oldNode, new ObjectNode(JsonNodeFactory.instance));
        this.publishAddEvent(prefix, this.getValue(newNode));
      } else {
        if (newNode.isContainerNode()) {
          this.publishRemoveEvent(prefix, this.getValue(oldNode));
          this.compareAndNotifyAdded(prefix, new ObjectNode(JsonNodeFactory.instance), newNode);
        } else {
          this.publishChangeEvent(prefix, this.getValue(oldNode), this.getValue(newNode));
        }
      }
    }
  }

  private void compareAndNotifyRemoved(final String prefix, final JsonNode oldNode, final JsonNode newNode) {
    Iterator<Map.Entry<String, JsonNode>> oldIt = oldNode.fields();
    while (oldIt.hasNext()) {
      Map.Entry<String, JsonNode> o = oldIt.next();
      String oldKey = o.getKey();

      JsonNode found = null;
      Iterator<Map.Entry<String, JsonNode>> newIt = newNode.fields();
      while (newIt.hasNext()) {
        Map.Entry<String, JsonNode> n = newIt.next();
        String newKey = n.getKey();

        if (newKey.equals(oldKey)) {
          found = n.getValue();
          break;
        }
      }

      String newPrefix = this.createNewPrefix(prefix, oldKey);

      if (found == null) {
        this.notifyListeners(newPrefix, o.getValue(), false);
      } else {
        this.compareAndNotify(newPrefix, o.getValue(), found);
      }
    }
  }

  private void compareAndNotifyAdded(final String prefix, final JsonNode oldNode, final JsonNode newNode) {
    Iterator<Map.Entry<String, JsonNode>> newIt = newNode.fields();
    while (newIt.hasNext()) {
      Map.Entry<String, JsonNode> n = newIt.next();
      String newKey = n.getKey();

      boolean found = false;
      Iterator<Map.Entry<String, JsonNode>> oldIt = oldNode.fields();
      while (oldIt.hasNext()) {
        Map.Entry<String, JsonNode> o = oldIt.next();
        String oldKey = o.getKey();

        if (newKey.equals(oldKey)) {
          found = true;
          break;
        }
      }

      if (!found) {
        String newPrefix = this.createNewPrefix(prefix, newKey);
        this.notifyListeners(newPrefix, n.getValue(), true);
      }
    }
  }

  private void notifyListeners(final String prefix, final JsonNode value, final boolean added) {
    if (value.isContainerNode()) {
      Iterator<Map.Entry<String, JsonNode>> it = value.fields();
      while (it.hasNext()) {
        Map.Entry<String, JsonNode> e = it.next();

        this.notifyListeners(this.createNewPrefix(prefix, e.getKey()), e.getValue(), added);
      }
    } else {
      Object v = this.getValue(value);

      if (added) {
        this.publishAddEvent(prefix, v);
      } else {
        this.publishRemoveEvent(prefix, v);
      }
    }
  }

  private void publishChangeEvent(final String key, final Object oldValue, final Object newValue) {
    this.publishEvent(new ConfigurationChangeEvent(key, oldValue, newValue));
  }

  private void publishAddEvent(final String key, final Object newValue) {
    this.publishEvent(new ConfigurationAddEvent(key, newValue));
  }

  private void publishRemoveEvent(final String key, final Object oldValue) {
    this.publishEvent(new ConfigurationRemoveEvent(key, oldValue));
  }

  private void publishEvent(final ConfigurationEvent event) {
    List<FileConfigurationProvider.ListenerDetails> listenerDetails = new ArrayList<>(this.listeners);
    for (final FileConfigurationProvider.ListenerDetails l : listenerDetails) {
      this.publishEvent(event, l);
    }
  }

  void publishEvent(final ConfigurationEvent event, final FileConfigurationProvider.ListenerDetails ld) {
    if (event.getKey().startsWith(ld.key)) {
      ld.listener.onEvent(event);
    }
  }

  Object getValue(final JsonNode value) {
    Object v = null;
    if (!value.isNull()) {
      if (value.isBigInteger()) {
        v = value.asLong();
      } else if (value.isLong()) {
        v = value.asLong();
      } else if (value.isInt()) {
        v = value.asLong();
      } else if (value.isShort()) {
        v = value.asLong();
      } else if (value.isBigDecimal()) {
        v = value.asDouble();
      } else if (value.isDouble()) {
        v = value.asDouble();
      } else if (value.isFloat()) {
        v = value.asDouble();
      } else if (value.isBoolean()) {
        v = value.asBoolean();
      } else {
        v = value.asText();
      }
    }
    return v;
  }

  private String createNewPrefix(final String prefix, final String name) {
    return prefix == null ? name : prefix + "." + name;
  }

  FileChangeListener createFileChangeListener() {
    return new FileChangeListener();
  }

  static class ListenerDetails {
    private final String key;
    private final ConfigurationChangeListener listener;

    ListenerDetails(final String key, final ConfigurationChangeListener listener) {
      this.key = key;
      this.listener = listener;
    }

    @Override
    public boolean equals(final Object o) {
      boolean result = o == this;

      if (!result && o != null && o.getClass() == this.getClass()) {
        FileConfigurationProvider.ListenerDetails that = (FileConfigurationProvider.ListenerDetails) o;

        result = Objects.equals(this.key, that.key)
              && Objects.equals(this.listener, that.listener);
      }

      return result;
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(this.key)
           ^ Objects.hashCode(this.listener);
    }
  }
}
