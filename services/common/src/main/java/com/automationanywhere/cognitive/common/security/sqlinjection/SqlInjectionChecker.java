package com.automationanywhere.cognitive.common.security.sqlinjection;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * SQL injection checker to be used to check dynamic columns names
 */
public class SqlInjectionChecker {

  private static final String REGEX = "(?i).*(?:SELECT |UPDATE |DELETE |ALTER |INSERT |DROP |DECLARE |SET |UNION |=|\\(|\\))+.*";
  private static final Pattern SQL_INJECTION_PATTERN = Pattern.compile(REGEX);

  /**
   * Thread safe method to check the column name
   *
   * @param column the column name to be checked
   * @throws SqlInjectionException only if the column name have a SQL injection string
   */
  public static void checkColumnName(String column) throws SqlInjectionException {
    Matcher matcher = SQL_INJECTION_PATTERN.matcher(column);
    if (matcher.matches()) {
      throw new SqlInjectionException("Found SQL injection pattern in: " + column);
    }
  }
}
