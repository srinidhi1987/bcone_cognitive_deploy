package com.automationanywhere.cognitive.common.healthcheck.model;

/**
 * Responsible for keep the data of heartbeat.
 */
public class Heartbeat {

    private AppInfo appInfo;
    private BuildInfo buildInfo;

    /**
     * Default constructor.
     *
     * @param appInfo   the AppInfo instance.
     * @param buildInfo the BuildInfo instance.
     */
    public Heartbeat(AppInfo appInfo, BuildInfo buildInfo) {
        this.appInfo = appInfo;
        this.buildInfo = buildInfo;
    }

    /**
     * Returns the appInfo.
     *
     * @return the value of appInfo
     */
    public AppInfo getAppInfo() {
        return appInfo;
    }

    /**
     * Sets the appInfo.
     *
     * @param appInfo the appInfo to be set
     */
    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    /**
     * Returns the buildInfo.
     *
     * @return the value of buildInfo
     */
    public BuildInfo getBuildInfo() {
        return buildInfo;
    }

    /**
     * Sets the buildInfo.
     *
     * @param buildInfo the buildInfo to be set
     */
    public void setBuildInfo(BuildInfo buildInfo) {
        this.buildInfo = buildInfo;
    }
}
