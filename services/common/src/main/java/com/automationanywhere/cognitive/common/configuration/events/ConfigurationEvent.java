package com.automationanywhere.cognitive.common.configuration.events;

/**
 * A configuration event signal that a configuration parameter somehow was changed.
 */
public interface ConfigurationEvent {
  /**
   * Key associated with the changed configuration parameter.
   *
   * @return configuration parameter key.
   */
  String getKey();
}
