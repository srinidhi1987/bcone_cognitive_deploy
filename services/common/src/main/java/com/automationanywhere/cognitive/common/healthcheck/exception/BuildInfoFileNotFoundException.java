package com.automationanywhere.cognitive.common.healthcheck.exception;

/**
 * Exception thrown when the file build.version was not found.
 */
public class BuildInfoFileNotFoundException extends RuntimeException {
    public static final long serialVersionUID = 1;

    /**
     * Default Constructor.
     *
     * @param e the exception related.
     */
    public BuildInfoFileNotFoundException(Exception e) {
        super(e);
    }
}
