package com.automationanywhere.cognitive.common.security;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This stream implementation wraps another stream.
 * The data this stream implementation provides is composed of a fixed header
 * followed by the data provided by the wrapped stream.
 *
 * The implementation is not thread safe.
 */
class InputStreamWithHeader extends FilterInputStream {

  private final byte[] header;

  private int read = 0;

  public InputStreamWithHeader(final byte[] header, final InputStream in) {
    super(in);

    this.header = new byte[header.length];
    System.arraycopy(header, 0, this.header, 0, header.length);
  }

  @Override
  public int read() throws IOException {
    int b;

    if (read < header.length) {
      // This is to cast byte to int with bit-to-bit precision.
      // (int) this.header[this.read] may be not a bit-to-bit operation.
      // (int) ((byte) 0x0000_00FF) is transformed to 0xFFFF_FFFF which is -1 and not 255
      b = header[read] & 0x0000_00FF;
      read += 1;
    } else {
      b = in.read();
    }

    return b;
  }

  @Override
  public int read(final byte[] b, final int off, final int len) throws IOException {
    checkIndexes(b, off, len);

    int n = 0;

    if (len > 0) { // if len == 0, they actually do not want to read anything.
      int o = off;
      int l = len;
      int left = header.length - read;

      if (left > 0) { // There is a header left to read
        int min = Math.min(l, left);

        System.arraycopy(header, read, b, off, min);

        n += min;
        o += min;
        l -= min;

        read += min;
      }

      if (n == 0) { // There was no header to read, let's read the data
        n = in.read(b, o, l);
      } else {
        if (l > 0) { // Header was read, and there is still space in the buffer to fill.
          int m = in.read(b, o, l); // Let's read some data
          if (m != -1) { // It's not the end of the stream
            n += m; // let's update the amount of actually read bytes
          } // if m == -1 then we've reached the end of the stream, but we cannot return -1,
          // because we've read the header,
          // they will know about the end of the stream on the next read operation.
        }
      }
    }

    return n;
  }

  private void checkIndexes(final byte[] buffer, final int offset, final int length) {
    if (offset < 0) {
      throw new IndexOutOfBoundsException(
          "the offset should be greater or equal to zero, but it's " + offset);
    }

    if (length < 0) {
      throw new IndexOutOfBoundsException(
          "the length should be greater or equal to zero, but it's " + length);
    }

    if (offset + length > buffer.length) {
      throw new IndexOutOfBoundsException(
          "the last byte should be less or equal to " + (buffer.length - 1)
              + " but it's " + offset + " + " + length + " - 1 = " + (offset + length - 1));
    }
  }

  @Override
  public int available() throws IOException {
    return header.length - read + in.available();
  }
}
