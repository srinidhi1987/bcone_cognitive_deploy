package com.automationanywhere.cognitive.common.healthcheck.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Responsible for keep the data of application.
 */
public class Application {

    private AppInfo appInfo;
    private BuildInfo buildInfo;
    private List<Application> subsystems;
    private List<Dependency> dependencies;

    public Application() {
    }

    public Application(
        final AppInfo appInfo,
        final BuildInfo buildInfo,
        final List<Application> subsystems,
        final List<Dependency> dependencies
    ) {
        this.appInfo = appInfo;
        this.buildInfo = buildInfo;
        this.subsystems = subsystems;
        this.dependencies = dependencies;
    }

    /**
     * Returns the appInfo.
     *
     * @return the value of appInfo
     */
    public AppInfo getAppInfo() {
        return appInfo;
    }

    /**
     * Sets the appInfo.
     *
     * @param appInfo the appInfo to be set
     */
    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    /**
     * Returns the buildInfo.
     *
     * @return the value of buildInfo
     */
    public BuildInfo getBuildInfo() {
        return buildInfo;
    }

    /**
     * Sets the buildInfo.
     *
     * @param buildInfo the buildInfo to be set
     */
    public void setBuildInfo(BuildInfo buildInfo) {
        this.buildInfo = buildInfo;
    }

    /**
     * Returns the subsystems.
     *
     * @return the value of subsystems
     */
    public List<Application> getSubsystems() {
        return subsystems;
    }

    /**
     * Sets the subsystems.
     *
     * @param subsystems the subsystems to be set
     */
    public void setSubsystems(List<Application> subsystems) {
        this.subsystems = subsystems;
    }

    public void addSubsystem(Application subsystem) {
        if (this.subsystems == null) {
            this.subsystems = new ArrayList<>();
        }
        this.subsystems.add(subsystem);
    }

    /**
     * Returns the dependencies.
     *
     * @return the value of dependencies
     */
    public List<Dependency> getDependencies() {
        return dependencies;
    }

    /**
     * Sets the dependencies.
     *
     * @param dependencies the dependencies to be set
     */
    public void setDependencies(List<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

    public void addDependency(Dependency dependency) {
        if (this.dependencies == null) {
            this.dependencies = new ArrayList<>();
        }
        this.dependencies.add(dependency);
    }
}
