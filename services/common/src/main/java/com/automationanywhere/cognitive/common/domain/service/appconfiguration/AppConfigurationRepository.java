package com.automationanywhere.cognitive.common.domain.service.appconfiguration;

/**
 * Gives the access to the app configuration details,
 * including the details updating.
 */
public interface AppConfigurationRepository {

  /**
   * Persists a configuration.
   *
   * @param configuration configuration to save.
   */
  void save(Configuration configuration) throws AppConfigurationException;

  /**
   * Returns existing configuration or null.
   *
   * @return existing configuration or null.
   */
  Configuration load() throws AppConfigurationException;
}
