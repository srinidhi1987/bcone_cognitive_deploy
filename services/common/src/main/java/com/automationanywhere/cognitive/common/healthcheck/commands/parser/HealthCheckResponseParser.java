package com.automationanywhere.cognitive.common.healthcheck.commands.parser;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parses text health check responses received from microservices.
 */
public class HealthCheckResponseParser {

  private final String name;
  private final CommandType type;
  private final BuildInfoFactory buildInfoFactory;
  private final AppInfoFactory appInfoFactory;

  public HealthCheckResponseParser(
      final String name,
      final CommandType type,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory
  ) {
    this.name = name;
    this.type = type;
    this.buildInfoFactory = buildInfoFactory;
    this.appInfoFactory = appInfoFactory;
  }

  /**
   * Parses a text response from a microservice.
   * The response should be health check details in text format.
   *
   * @param body - text response.
   * @return health check details of the microservices.
   */
  public Application parse(final String body) {
    BuildInfo buildInfo = null;
    AppInfo appInfo = null;
    List<Dependency> dependencies = new ArrayList<>();

    HealthCheckResponse resp = new HealthCheckResponse(body);
    if (resp.isValid()) {
      String appName = resp.getSectionValue(HealthCheckResponse.APP_NAME);

      buildInfo = buildInfoFactory.build(
          appName,
          resp.getSectionValue(HealthCheckResponse.VERSION),
          resp.getSectionValue(HealthCheckResponse.BRANCH),
          resp.getSectionValue(HealthCheckResponse.GIT),
          resp.getSectionValue(HealthCheckResponse.BUILD_TIME)
      );

      appInfo = appInfoFactory.build(
          appName,
          CommandType.APPLICATION.name(),
          resp.getStatusValue().contains(Status.OK.name()) ? Status.OK : Status.NOT_OK,
          "",
          resp.getSectionValue(HealthCheckResponse.UP_TIME)
      );

      dependencies = resp.getDependencies().stream().map(kv -> new Dependency(
          kv.getKey(),
          kv.getValue().contains(Status.OK.name()) ? Status.OK : Status.NOT_OK
      )).collect(Collectors.toList());
    }
    return new Application(
        appInfo, buildInfo, null, dependencies
    );
  }

  /**
   * Creates an error status with the message error when the check call throws an exception or was not possible.
   *
   * @param th the exception thrown trying to execute the check health of the microservice.
   * @return the status instance
   */
  public Application parseError(final Throwable th) {
    return new Application(
        appInfoFactory.build(
            name,
            type.name(),
            Status.NOT_OK,
            th.getMessage(),
            ""
        ),
        buildInfoFactory.build(name),
        null,
        null
    );
  }
}
