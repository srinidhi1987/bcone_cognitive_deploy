package com.automationanywhere.cognitive.common.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.springframework.core.task.AsyncTaskExecutor;

/**
 * Default configuration provider implementation that reads configuration parameters
 * from a file that can be mapped a JSON tree.
 *
 */
public class FileConfigurationProviderFactory {
  public static final String DEFAULT_CONFIGURATION_FILE_PATH = "./application.yaml";

  /**
   * The default factory method creates a provider that is able to read YAML files named "application.yaml"
   * located in the application working directory.
   *
   * @return new provider instance.
   */
  public FileConfigurationProvider create() {
    return this.create(new ObjectMapper(new YAMLFactory()), DEFAULT_CONFIGURATION_FILE_PATH, null);
  }

  /**
   * The default factory method creates a provider that is able to read YAML files named "application.yaml"
   * located in the application working directory.
   *
   * @return new provider instance.
   * @param asyncTaskExecutor - executor used to create a thread where the provider will listen to file changes. (optional)
   */
  public FileConfigurationProvider create(final AsyncTaskExecutor asyncTaskExecutor) {
    return this.create(new ObjectMapper(new YAMLFactory()), DEFAULT_CONFIGURATION_FILE_PATH, asyncTaskExecutor);
  }

  /**
   * Creates a file configuration provider.
   *
   * @param mapper - mapper capable of transforming file contents into a JSON object.
   * @param filePath - file to read.
   * @param asyncTaskExecutor - executor used to create a thread where the provider will listen to file changes. (optional)
   * @return new provider instance.
   */
  public FileConfigurationProvider create(final ObjectMapper mapper, final String filePath, final AsyncTaskExecutor asyncTaskExecutor) {
    FileConfigurationProvider provider = this.createFileConfigurationProvider(mapper, filePath);
    provider.refresh();

    if (asyncTaskExecutor != null) {
      provider.listen(asyncTaskExecutor);
    }

    return provider;
  }

  FileConfigurationProvider createFileConfigurationProvider(final ObjectMapper mapper, final String filePath) {
    return new FileConfigurationProvider(mapper, filePath);
  }
}
