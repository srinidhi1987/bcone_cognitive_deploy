package com.automationanywhere.cognitive.common.healthapi.constants;
/**
	This class represents the Connectivity to a specific dependency system 
	OK: dependency system is up and running
	NOT_APPLICABLE: dependency system is not applicable for this microservice
	FAILURE: connection to dependency system is failing , details will be provided in message
**/
public enum Connectivity {
	OK ("OK"), NOT_APPLICABLE ("Not Applicable"), FAILURE ("Failure");

	public final String message;
	
	Connectivity(final String message){
		this.message = message;
	}
}
