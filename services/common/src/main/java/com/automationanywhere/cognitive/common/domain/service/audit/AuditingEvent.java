package com.automationanywhere.cognitive.common.domain.service.audit;

public class AuditingEvent {

  private final ActivityType activityType;
  private final Status status;
  private final VerbosityLevel verbosityLevel;
  private final String objectName;
  private final String eventDescription;
  private final String detail;

  public AuditingEvent(
      final ActivityType activityType,
      final Status status,
      final VerbosityLevel verbosityLevel,
      final String objectName,
      final String eventDescription,
      final String detail
  ) {
    if (activityType == null) {
      throw new IllegalArgumentException();
    }
    if (status == null) {
      throw new IllegalArgumentException();
    }
    if (verbosityLevel == null) {
      throw new IllegalArgumentException();
    }
    this.activityType = activityType;
    this.status = status;
    this.verbosityLevel = verbosityLevel;
    this.objectName = objectName;
    this.eventDescription = eventDescription;
    this.detail = detail;
  }

  public ActivityType activityType() {
    return activityType;
  }

  public Status status() {
    return status;
  }

  public VerbosityLevel verbosityLevel() {
    return verbosityLevel;
  }

  public String objectName() {
    return objectName;
  }

  public String eventDescription() {
    return eventDescription;
  }

  public String detail() {
    return detail;
  }
}
