package com.automationanywhere.cognitive.common.infrastructure.controlroom;

public class ControlRoomSdkWrapperException extends RuntimeException {

  private static final long serialVersionUID = 2279378150571288841L;

  public ControlRoomSdkWrapperException(final String message) {
    super(message);
  }

  public ControlRoomSdkWrapperException(final Throwable cause) {
    super(cause);
  }
}
