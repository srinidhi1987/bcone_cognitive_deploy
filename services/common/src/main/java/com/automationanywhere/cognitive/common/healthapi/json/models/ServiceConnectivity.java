package com.automationanywhere.cognitive.common.healthapi.json.models;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;

public class ServiceConnectivity {
    
    public ServiceConnectivity(String serviceName, HTTPStatusCode httpStatus) {
        super();
        this.serviceName = serviceName;
        this.httpStatus = httpStatus;
    }
    String serviceName;
    HTTPStatusCode httpStatus;
    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }
    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    /**
     * @return the systemStatus
     */
    public HTTPStatusCode getHTTPStatus() {
        return httpStatus;
    }
    /**
     * @param systemStatus the systemStatus to set
     */
    public void setHTTPStatus(HTTPStatusCode systemStatus) {
        this.httpStatus = systemStatus;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return serviceName + ":" + httpStatus.getCode();
    }
    
}
