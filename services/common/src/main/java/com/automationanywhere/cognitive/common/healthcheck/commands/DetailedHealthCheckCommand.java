package com.automationanywhere.cognitive.common.healthcheck.commands;

/**
 * The command gets detailed information about a component status.
 */
public interface DetailedHealthCheckCommand extends HealthCheckCommand {
}
