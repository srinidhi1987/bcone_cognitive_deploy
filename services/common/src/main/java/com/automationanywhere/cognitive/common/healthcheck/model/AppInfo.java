package com.automationanywhere.cognitive.common.healthcheck.model;

/**
 * Responsible for keep the data of application.
 */
public class AppInfo {

  private final String name;
  private final String type;
  private final Status status;
  private final String failReason;
  private final String uptime;

  public AppInfo(
      String name,
      String type,
      Status status,
      String failReason,
      String uptime
  ) {
    this.name = name;
    this.type = type;
    this.status = status;
    this.failReason = failReason;
    this.uptime = uptime;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the type.
   *
   * @return the value of type
   */
  public String getType() {
    return type;
  }

  /**
   * Returns the status.
   *
   * @return the value of status
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Returns the failReason.
   *
   * @return the value of failReason
   */
  public String getFailReason() {
    return failReason;
  }

  /**
   * Returns the uptime.
   *
   * @return the value of uptime
   */
  public String getUptime() {
    return uptime;
  }
}
