package com.automationanywhere.cognitive.common.healthcheck.commands;

import com.automationanywhere.cognitive.common.healthcheck.commands.parser.HealthCheckResponseParser;
import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.concurrent.CompletableFuture;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Default health check command for all Microservices.
 */
public class MicroserviceTextHealthCheckCommand implements DetailedHealthCheckCommand {

  private final String host;
  private final AsyncRestTemplate restTemplate;
  private final HealthCheckResponseParser parser;

  /**
   * Constructs a health check command for Microservice.
   *
   * @param host the host of the microservice (url + port), shouldn't have the /healthcheck in the
   * URL, it will be added by the system before call the microservice.
   */
  @SuppressWarnings("unused")
  public MicroserviceTextHealthCheckCommand(
      final String name,
      final String host,
      final BuildInfoFactory buildInfoFactory,
      final AppInfoFactory appInfoFactory,
      final AsyncRestTemplate restTemplate
  ) {
    this.host = host;
    this.restTemplate = restTemplate;

    parser = new HealthCheckResponseParser(name, getType(), buildInfoFactory, appInfoFactory);
  }

  @Async
  @Override
  public CompletableFuture<HealthCheckStatus> executeCheck() {
    CompletableFuture<HealthCheckStatus> future = new CompletableFuture<>();

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    ListenableFuture<ResponseEntity<String>> responseEntity = restTemplate.exchange(
        host + "/healthcheck", HttpMethod.GET, new HttpEntity<Application>(headers), String.class
    );

    responseEntity.addCallback(new ListenableFutureCallback<ResponseEntity<String>>() {
      @Override
      public void onSuccess(final ResponseEntity<String> result) {
        try {
          future.complete(new ApplicationHealthCheckStatus(parser.parse(result.getBody())));
        } catch (final Exception ex) {
          future.completeExceptionally(ex);
        }
      }

      @Override
      public void onFailure(final Throwable ex) {
        future.complete(new ApplicationHealthCheckStatus(parser.parseError(ex)));
      }
    });

    return future;
  }

  @Override
  public CommandType getType() {
    return CommandType.APPLICATION;
  }
}