package com.automationanywhere.cognitive.common.healthcheck.components;

import com.automationanywhere.cognitive.common.healthcheck.commands.HealthCheckCommand;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * All components to handle health check should implements this class.
 *
 * @param <T> the class used by this component as <code>Application</code> class to keeps the status information.
 */
public interface HealthCheckComponent<T, C extends HealthCheckCommand> {

    /**
     * Executes the health check of this application calling all commands retrieved from classpath.
     *
     * @param commands the list of commands to check.
     * @return An instance of status object with the result of check.
     */
    CompletableFuture<T> executeHealthCheck(List<C> commands);

    /**
     * Converts the status object in string.
     *
     * @param obj the status object.
     * @return the string converted from the status object.
     */
    String convert(T obj);
}
