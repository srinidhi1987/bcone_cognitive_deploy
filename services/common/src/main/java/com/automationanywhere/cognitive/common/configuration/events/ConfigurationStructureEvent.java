package com.automationanywhere.cognitive.common.configuration.events;

import java.util.Objects;

/**
 * Base configuration event that describes a change in relationships
 * (for example, parent-child ones) between parameters.
 */
public abstract class ConfigurationStructureEvent extends ConfigurationBaseEvent {
  /**
   * Value of the changed configuration parameter.
   */
  public final Object value;

  protected ConfigurationStructureEvent(final String key, final Object value) {
    super(key);

    this.value = value;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      ConfigurationStructureEvent that = (ConfigurationStructureEvent) o;

      result = Objects.equals(this.key, that.key)
            && Objects.equals(this.value, that.value);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.key)
         ^ Objects.hashCode(this.value);
  }
}
