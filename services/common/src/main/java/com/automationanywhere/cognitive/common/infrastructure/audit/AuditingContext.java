package com.automationanywhere.cognitive.common.infrastructure.audit;

public class AuditingContext {

  public static final ThreadLocal<AuditingContext> AUDITING_CONTEXT = new ThreadLocal<AuditingContext>() {
    @Override
    protected AuditingContext initialValue() {
      return new AuditingContext();
    }

    @Override
    public void set(final AuditingContext value) {
      throw new UnsupportedOperationException();
    }
  };

  private static final long DEFAULT_ENTITY_VERSION = 0;
  private static final String DEFAULT_ENVIRONMENT_VARIABLE = "IQBOT";

  private String hostName;
  private String userName;
  private String source;

  private AuditingContext() {
  }

  public long entityVersion() {
    return DEFAULT_ENTITY_VERSION;
  }

  public String environmentName() {
    return DEFAULT_ENVIRONMENT_VARIABLE;
  }

  public String source() {
    return source;
  }

  public void source(final String source) {
    this.source = source;
  }

  public String hostName() {
    return hostName;
  }

  public void hostName(final String hostName) {
    this.hostName = hostName;
  }

  public String userName() {
    return userName;
  }

  public void userName(final String userName) {
    this.userName = userName;
  }
}
