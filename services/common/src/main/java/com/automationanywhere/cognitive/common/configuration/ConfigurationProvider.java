package com.automationanywhere.cognitive.common.configuration;

import com.automationanywhere.cognitive.common.configuration.events.ConfigurationChangeListener;
import com.automationanywhere.cognitive.common.configuration.exceptions.NoSuchKeyException;
import com.automationanywhere.cognitive.common.configuration.exceptions.NotContainerException;
import com.automationanywhere.cognitive.common.configuration.exceptions.NotValueException;

/**
 * Common interfaces to get system wide configuration.
 */
public interface ConfigurationProvider {
  /**
   * Returns a configuration value by its key.
   *
   * @param key - key of the configuration value to return.
   * @return configuration value, may be null, if the configuration value exists and it's set to null.
   *
   * @throws NoSuchKeyException - if the configuration parameter is unknown to the provider.
   * @throws NotValueException - configuration providers that support parameter hierarchies
   *                             may be able to return only primitive values and may throw
   *                             the exception if a whole node/value container is associated with the key.
   * @throws NotContainerException - configuration providers that support parameter hierarchies
   *                                 may throw an exception of one of the top key parts actually
   *                                 is a primitive value but not a node/value container.
   */
  <T> T getValue(String key) throws NoSuchKeyException, NotValueException, NotContainerException;

  /**
   * Registers a listener to be notified on configuration parameter changes.
   * The same listener instances may be registered with different keys.
   * Registering the same listener instance with the same key takes no effect.
   *
   * @param key - key of the configuration parameter whose changes to listen to.
   * @param listener - listener of the changes.
   * @return the same listener instance.
   */
  ConfigurationChangeListener subscribe(String key, ConfigurationChangeListener listener);

  /**
   * Unregistered a listener.
   * Unregistering a listener which is not registered takes no effect.
   *
   * @param key - key of the configuration parameter whose changes the listener listens to.
   * @param listener - listener to unregister.
   * @return the same listener instance or null if the listener hasn't been registered before.
   */
  ConfigurationChangeListener unsubscribe(String key, ConfigurationChangeListener listener);
}
