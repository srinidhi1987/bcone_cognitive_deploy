package com.automationanywhere.cognitive.common.util;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DataTransformer {

  /**
   * Transforms byte array to {@link ByteArrayInputStream} object.
   */
  public ByteArrayInputStream transform(byte[] i) {

    return new ByteArrayInputStream(i);
  }

  /**
   * Transforms {@link InputStream} to {@link ByteArrayInputStream} object.
   */
  public ByteArrayOutputStream transform(InputStream inputStream) throws IOException {
    ByteArrayOutputStream byteArrStream = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;

    try {
      while ((length = inputStream.read(buffer)) != -1) {
        byteArrStream.write(buffer, 0, length);
      }
    } catch (IOException ex) {
      throw ex;
    } finally {
      inputStream.close();
    }
    return byteArrStream;
  }

  /**
   * Transforms list of array of objects to list of array of string.
   */
  public List<String[]> transformToListOfStringArray(List<Object[]> lstOfObjectArr) {
    List<String[]> lstOfStringArr = null;

    if (lstOfObjectArr != null && !lstOfObjectArr.isEmpty()) {
      lstOfStringArr = lstOfObjectArr.stream().map(objectArr ->
      {
        return Arrays.stream(objectArr).map(this::getString).collect(
            Collectors.toList()).toArray(new String[objectArr.length]);
      }).collect(Collectors.toList());

    }
    return lstOfStringArr;
  }

  /**
   * Transforms list of objects to list of string.
   */
  public List<String> transformToListOfString(List<Object> lstOfObject) {
    List<String> lstOfString = null;
    if (lstOfObject != null && !lstOfObject.isEmpty()) {
      lstOfString = lstOfObject.stream().map(this::getString).collect(Collectors.toList());
    }
    return lstOfString;
  }

  private String getString(Object obj) {
    if (obj == null) {
      return "";
    }
    if (obj instanceof String && String.valueOf(obj).equals("")) {
      return " ";
    }
    if (obj instanceof Boolean) {
      return (Boolean) obj ? "1" : "0";
    } else {
      return String.valueOf(obj);
    }
  }
}
