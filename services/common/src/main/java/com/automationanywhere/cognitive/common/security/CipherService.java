package com.automationanywhere.cognitive.common.security;

import java.io.InputStream;

/**
 * The service is responsible for encrypting and decrypting data
 * using a mandatory encryption/decryption cipher defined by
 * implementations.
 */
public interface CipherService {

  /**
   * Encrypts a message using a cipher.
   *
   * @param input - message to encrypt.
   * @return a new stream of the encrypted data.
   * @throws CipherException - if the encryption fails.
   */
  InputStream encrypt(InputStream input) throws CipherException;

  /**
   * Decrypts a message using a cipher.
   *
   * @param input - message to decrypt.
   * @return a new stream of the decrypted data.
   * @throws CipherException - if the decryption fails.
   */
  InputStream decrypt(InputStream input) throws CipherException;
}
