package com.automationanywhere.cognitive.common.domain.service.appconfiguration;

import java.util.Objects;

public class Configuration {

  public final String controlRoomUrl;
  public final String controlRoomVersion;
  public final String appId;
  public final Boolean registered;

  public Configuration(
      final String controlRoomUrl,
      final String controlRoomVersion,
      final String appId,
      final Boolean registered
  ) {
    this.controlRoomUrl = controlRoomUrl;
    this.controlRoomVersion = controlRoomVersion;
    this.appId = appId;
    this.registered = registered;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == getClass()) {
      Configuration that = (Configuration) o;

      result = Objects.equals(controlRoomUrl, that.controlRoomUrl)
          && Objects.equals(controlRoomVersion, that.controlRoomVersion)
          && Objects.equals(registered, that.registered)
          && Objects.equals(appId, that.appId);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(controlRoomUrl)
        ^ Objects.hashCode(controlRoomVersion)
        ^ Objects.hashCode(registered)
        ^ Objects.hashCode(appId);
  }
}
