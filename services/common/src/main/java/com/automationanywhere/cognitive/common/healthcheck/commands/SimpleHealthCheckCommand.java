package com.automationanywhere.cognitive.common.healthcheck.commands;

/**
 * The command returns if the a component is running or not.
 */
public interface SimpleHealthCheckCommand extends HealthCheckCommand {
}
