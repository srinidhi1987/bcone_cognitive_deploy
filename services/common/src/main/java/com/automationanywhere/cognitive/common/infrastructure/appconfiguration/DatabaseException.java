package com.automationanywhere.cognitive.common.infrastructure.appconfiguration;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationException;

public class DatabaseException extends AppConfigurationException {

  private static final long serialVersionUID = -2058622593396649147L;

  public DatabaseException(final String message) {
    super(message);
  }

  public DatabaseException(final Throwable cause) {
    super(cause);
  }
}
