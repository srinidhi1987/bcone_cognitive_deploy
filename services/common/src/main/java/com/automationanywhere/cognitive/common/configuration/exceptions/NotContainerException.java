package com.automationanywhere.cognitive.common.configuration.exceptions;

/**
 * Thrown when a key is not a value container.
 */
public class NotContainerException extends RuntimeException {
  public static final long serialVersionUID = 1;

  public NotContainerException(final String message) {
    super(message);
  }
}
