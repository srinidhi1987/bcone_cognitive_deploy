package com.automationanywhere.cognitive.common.infrastructure.appconfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

public abstract class QueryBuilder {

  public static final String APP_ID_KEY_NAME = "appId";
  public static final String CR_URL_KEY_NAME = "controlRoomUrl";
  public static final String CR_VERSION_KEY_NAME = "controlRoomVersion";
  public static final String APP_REGISTERED_KEY_NAME = "appRegistered";
  public static final List<String> KEY_NAMES = Arrays.asList(
      APP_ID_KEY_NAME, CR_URL_KEY_NAME, CR_VERSION_KEY_NAME, APP_REGISTERED_KEY_NAME
  );

  // They should be accessible by our classes only (that is, classes defined in this package).
  final Map<String, String> map;
  final Session session;

  QueryBuilder(final Session session, final Map<String, String> map) {
    this.session = session;
    this.map = map;
  }

  abstract QueryBuilder create(final Map<String, String> map);

  public QueryBuilder put(final String key, final String value) {
    Map<String, String> map = new HashMap<>(this.map);
    map.put(key, value);

    return create(map);
  }

  public abstract List<NativeQuery> build();
}

class InsertQueryBuilder extends QueryBuilder {

  private static final String USER_NAME = "SYSTEM";

  public static QueryBuilder create(final Session session) {
    return new InsertQueryBuilder(session, new HashMap<>());
  }

  private InsertQueryBuilder(final Session session, final Map<String, String> map) {
    super(session, map);
  }

  @Override
  QueryBuilder create(final Map<String, String> map) {
    return new InsertQueryBuilder(session, map);
  }

  @Override
  public List<NativeQuery> build() {
    StringBuilder qb = new StringBuilder("INSERT INTO Configurations([Key], [Value], [CreatedBy]) VALUES(");

    int index = 0;
    for (; index < map.size(); index+= 1) {
      if (index > 0) {
        qb.append("),(");
      }
      qb.append(":key").append(index)
          .append(",:value").append(index)
          .append(",:createdBy").append(index);
    }
    qb.append(")");

    NativeQuery query = session.createNativeQuery(qb.toString());

    index = 0;
    for (final Entry<String, String> entry : map.entrySet()) {
      query.setParameter("key" + index, entry.getKey());
      query.setParameter("value" + index, entry.getValue());
      query.setParameter("createdBy" + index, USER_NAME);
      index+= 1;
    }

    return Collections.singletonList(query);
  }
}

class UpdateQueryBuilder extends QueryBuilder {
  public static QueryBuilder create(final Session session) {
    return new UpdateQueryBuilder(session, new HashMap<>());
  }

  private UpdateQueryBuilder(final Session session, final Map<String, String> map) {
    super(session, map);
  }

  @Override
  QueryBuilder create(final Map<String, String> map) {
    return new UpdateQueryBuilder(session, map);
  }

  @Override
  public List<NativeQuery> build() {
    List<NativeQuery> queries = new ArrayList<>();

    for (final Entry<String, String> entry : map.entrySet()) {
      NativeQuery query = session.createNativeQuery(
          "UPDATE Configurations SET [Value]=:value WHERE [Key]=:key"
      );
      query.setParameter("key", entry.getKey());
      query.setParameter("value", entry.getValue());

      queries.add(query);
    }

    return queries;
  }
}