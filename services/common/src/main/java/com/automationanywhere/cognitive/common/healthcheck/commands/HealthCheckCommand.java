package com.automationanywhere.cognitive.common.healthcheck.commands;

import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.concurrent.CompletableFuture;

/**
 * All commands to check the health must implements this interface.
 */
public interface HealthCheckCommand {

    /**
     * Returns the type of the health check command.
     *
     * @return the type of command
     */
    CommandType getType();

    /**
     * Executes the command to check the status. This method should execute asynchronous.
     *
     * @return the status of system checked
     */
    CompletableFuture<HealthCheckStatus> executeCheck();
}

