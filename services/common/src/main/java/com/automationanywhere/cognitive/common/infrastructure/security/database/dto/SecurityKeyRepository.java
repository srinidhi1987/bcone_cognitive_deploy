package com.automationanywhere.cognitive.common.infrastructure.security.database.dto;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;

public interface SecurityKeyRepository {
  public void add(SecurityKey securityKey);
  public void remove(SecurityKey securityKey);
  public SecurityKey securityKeyByName(KeyName name);
}
