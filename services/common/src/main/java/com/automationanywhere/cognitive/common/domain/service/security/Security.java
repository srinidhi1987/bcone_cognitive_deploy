package com.automationanywhere.cognitive.common.domain.service.security;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyBasedAlgorithmFactory;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyBasedAlgorithms;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import java.io.InputStream;

public class Security {

  private final KeyProvider keyProvider;

  public Security(KeyProvider keyProvider) {
    this.keyProvider = keyProvider;
  }

  public InputStream encrypt(KeyName keyName, KeyType keyType, InputStream data) {
    CryptographyKey secretKey = keyProvider.getKey(keyName);
    KeyBasedAlgorithms keyBasedAlgorithms = KeyBasedAlgorithmFactory.keyBasedAlgorithm(keyType);
    return keyBasedAlgorithms.encrypt(secretKey, data);
  }

  public InputStream decrypt(KeyName keyName, KeyType keyType, InputStream data) {
    CryptographyKey secretKey = keyProvider.getKey(keyName);
    KeyBasedAlgorithms keyBasedAlgorithms = KeyBasedAlgorithmFactory.keyBasedAlgorithm(keyType);
    return keyBasedAlgorithms.decrypt(secretKey, data);
  }
}