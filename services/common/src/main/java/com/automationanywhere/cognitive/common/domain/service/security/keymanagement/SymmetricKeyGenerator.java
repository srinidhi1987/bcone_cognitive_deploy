package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import com.automationanywhere.cognitive.common.domain.service.security.exceptions.KeyGenerationException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

public class SymmetricKeyGenerator implements SecurityKeyGenerator {

  public static final int KEY_SIZE = 256;

  @Override
  public CryptographyKey generateKey() throws KeyGenerationException{
    String symmetricKey = null;
    try {
      KeyGenerator keygen = KeyGenerator.getInstance("AES");
      keygen.init(KEY_SIZE);
      SecretKey secretKey = keygen.generateKey();
      byte[] key = keygen.generateKey().getEncoded();
      symmetricKey = DatatypeConverter.printBase64Binary(key);
    } catch (NoSuchAlgorithmException e) {
      throw new KeyGenerationException("Error while generating the Symmetric Key!", e);
    }
    return new SymmetricKey(symmetricKey);
  }
}
