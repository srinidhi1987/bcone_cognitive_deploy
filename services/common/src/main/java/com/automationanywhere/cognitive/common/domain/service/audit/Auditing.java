package com.automationanywhere.cognitive.common.domain.service.audit;

/**
 * Sends auditing events to auditing event storage.
 */
public interface Auditing {

  /**
   * Sends an auditing event.
   *
   * @param auditingEvent - event to send.
   * @throws AuditingException if sending an event fails for whatever reason.
   */
  void audit(AuditingEvent auditingEvent) throws AuditingException;
}
