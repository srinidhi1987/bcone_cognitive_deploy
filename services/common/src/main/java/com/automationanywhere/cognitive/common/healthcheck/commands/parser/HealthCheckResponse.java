package com.automationanywhere.cognitive.common.healthcheck.commands.parser;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Represents a text health check response received from a microservice.
 */
public class HealthCheckResponse {
  public static final String APP_NAME = "Application";
  public static final String UP_TIME = "Application uptime";
  public static final String VERSION = "Version";
  public static final String BRANCH = "Branch";
  public static final String GIT = "GIT #";
  public static final String BUILD_TIME = "Build Time";

  private static final String SUBSYSTEM = "Subsystem";
  private static final String SUBSYSTEM_BAD_NAME = "Subsytem";
  private static final String DEPENDENCIES = "Dependencies";

  private static final int SUB_SYSTEM_SECTION_INDEX = 0;
  private static final int STATUS_SECTION_INDEX = 2;
  private static final int MIN_LAST_SECTION_INDEX = STATUS_SECTION_INDEX;
  private static final String DEP_KEY_VALUE_SEP = ": ";

  private final List<String> SECTION_NAMES = Arrays.asList(
      APP_NAME,
      UP_TIME,
      VERSION,
      BRANCH,
      GIT,
      BUILD_TIME
  );

  private final List<String> lines;
  private final Map<String, String> sectionMap;
  private final boolean valid;
  private final List<SimpleEntry<String, String>> dependencies;

  public HealthCheckResponse(final String body) {
    lines = Arrays.asList(body.split("\n"));

    valid = lines.size() > MIN_LAST_SECTION_INDEX
        && (lines.get(SUB_SYSTEM_SECTION_INDEX).startsWith(SUBSYSTEM)
            || lines.get(SUB_SYSTEM_SECTION_INDEX).startsWith(SUBSYSTEM_BAD_NAME)
        );

    sectionMap = valid ? createSectionMap() : new HashMap<>();
    dependencies = Collections.unmodifiableList(valid ? createDependencies() : new ArrayList<>());
  }

  private List<SimpleEntry<String, String>> createDependencies() {
    // Let's first find the index where dependency lines start
    return Optional.of(IntStream.range(0, lines.size())
          .filter(i -> lines.get(i).startsWith(DEPENDENCIES)).findAny()
      ).filter(OptionalInt::isPresent)
      .map(OptionalInt::getAsInt)
      // And then let's collect all the remaining dependency lines
      .map(depIndex -> IntStream.range(depIndex + 1, lines.size())
          // converting each line into a key-value pair
          .mapToObj(i -> lines.get(i).split(DEP_KEY_VALUE_SEP))
          .map(kv -> new SimpleEntry<>(kv[0], kv[1]))
          .collect(Collectors.toList())
      // so that we get a list of key-value pairs
      // FIXME: Can we create a map instead of a list? If dependency "keys" may repeat, then a map won't work for us.
      ).orElseGet(ArrayList::new);
  }

  private Map<String, String> createSectionMap() {
    // Let's find what section name corresponds to what section value
    // and create a map for them to remember those mappings.
    return SECTION_NAMES.stream().map(sectionName -> {
        String prefix = sectionName + ": ";
        return lines.stream()
            .filter(line -> line.startsWith(prefix)).findAny()
            .map(line -> line.substring(prefix.length(), line.length()).trim())
            .map(value -> new SimpleEntry<>(sectionName, value));
      }).filter(Optional::isPresent)
      .map(Optional::get)
      .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
  }

  /**
   * Returns a section value.
   *
   * @param name - name of the section.
   * @return a section value.
   */
  public String getSectionValue(final String name) {
    return sectionMap.get(name);
  }

  /**
   * Returns a status value.
   *
   * @return a status value.
   */
  public String getStatusValue() {
    return lines.get(STATUS_SECTION_INDEX);
  }

  public List<SimpleEntry<String, String>> getDependencies() {
    return dependencies;
  }

  public boolean isValid() {
    return valid;
  }
}
