package com.automationanywhere.cognitive.common.infrastructure.security;


import com.automationanywhere.cognitive.common.domain.service.security.KeyProvider;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;

public class ControlRoomKeyProvider implements KeyProvider {

  private final String keyName;

  public ControlRoomKeyProvider(String keyName) {
    this.keyName = keyName;
  }

  @Override
  public CryptographyKey getKey(KeyName name) {
    return null;
  }

  @Override
  public void saveKey(KeyName keyName, CryptographyKey keyValue) {

  }

  @Override
  public void removeKey(KeyName name) {

  }
}
