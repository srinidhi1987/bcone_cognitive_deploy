package com.automationanywhere.cognitive.common.healthcheck.commands;

import com.automationanywhere.cognitive.common.healthcheck.info.AppInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import com.automationanywhere.cognitive.common.healthcheck.status.ApplicationHealthCheckStatus;
import com.automationanywhere.cognitive.common.healthcheck.status.HealthCheckStatus;
import java.util.concurrent.CompletableFuture;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

public class SimpleHttpStatusCheckCommand implements SimpleHealthCheckCommand {

  private String name;
  private String host;
  private BuildInfoFactory buildInfoFactory;
  private AppInfoFactory appInfoFactory;
  private AsyncRestTemplate restTemplate;

  public SimpleHttpStatusCheckCommand(
      String name,
      String host,
      BuildInfoFactory buildInfoFactory,
      AppInfoFactory appInfoFactory,
      AsyncRestTemplate restTemplate
  ) {
    this.name = name;
    this.host = host;
    this.buildInfoFactory = buildInfoFactory;
    this.appInfoFactory = appInfoFactory;
    this.restTemplate = restTemplate;
  }

  @Override
  public CommandType getType() {
    return CommandType.APPLICATION;
  }

  @Override
  public CompletableFuture<HealthCheckStatus> executeCheck() {

    String url = host + "/health";
    ListenableFuture<ResponseEntity<String>> responseEntity =
        restTemplate.getForEntity(url, String.class);

    CompletableFuture<HealthCheckStatus> future = new CompletableFuture<>();
    responseEntity.addCallback(new ListenableFutureCallback<ResponseEntity<String>>() {
      @Override
      public void onFailure(Throwable ex) {
        Application application = new Application();
        application.setBuildInfo(buildInfoFactory.build(name));
        application.setAppInfo(appInfoFactory.build(
            name,
            CommandType.APPLICATION.name(),
            Status.NOT_OK,
            ex.getMessage(),
            ""
        ));
        future.complete(new ApplicationHealthCheckStatus(application));
      }

      @Override
      public void onSuccess(ResponseEntity<String> result) {
        Application application = new Application();
        application.setBuildInfo(buildInfoFactory.build(name));
        application.setAppInfo(appInfoFactory.build(
            name,
            CommandType.APPLICATION.name(),
            result.getStatusCode() == HttpStatus.OK ? Status.OK : Status.NOT_OK,
            "",
            ""
        ));
        future.complete(new ApplicationHealthCheckStatus(application));
      }
    });
    return future;
  }
}
