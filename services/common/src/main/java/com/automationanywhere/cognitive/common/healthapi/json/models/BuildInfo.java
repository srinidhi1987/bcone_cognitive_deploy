package com.automationanywhere.cognitive.common.healthapi.json.models;
/**
 * @author shweta.thakur
 *
 * This class represents the Build related information
 * 
 */
public class BuildInfo {
	private String application;
	private String version;
	private String branch;
	private String git;
	private String buildTime;

	public BuildInfo(String application, String version, String branch,
			String git, String buildTime) {
		super();
		this.application = application;
		this.version = version;
		this.branch = branch;
		this.git = git;
		this.buildTime = buildTime;
	}

	public BuildInfo() {
	}
	
	@Override
	public String toString() {
		return String.format("Version: %s" + "\nBranch: %s" + "\nGIT #: %s" + "\nBuild Time: %s", version, branch, git, buildTime);
/*		return "Application:" + application + "\nVersion:" + version
				+ "\nBranch:" + branch + "\nGIT #=" + git + "\nBuild Time:"
				+ buildTime;
*/	}

    /**
     * @return the application
     */
    public String getApplication() {
        return application;
    }

    /**
     * @param application the application to set
     */
    public void setApplication(String application) {
        this.application = application;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * @return the git
     */
    public String getGit() {
        return git;
    }

    /**
     * @param git the git to set
     */
    public void setGit(String git) {
        this.git = git;
    }

    /**
     * @return the buildTime
     */
    public String getBuildTime() {
        return buildTime;
    }

    /**
     * @param buildTime the buildTime to set
     */
    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }
}