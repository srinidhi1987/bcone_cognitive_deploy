package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

import com.automationanywhere.cognitive.common.domain.service.security.exceptions.CipherException;
import java.io.InputStream;

public interface KeyBasedAlgorithms {

  InputStream encrypt(CryptographyKey key, InputStream data) throws CipherException;

  InputStream decrypt(CryptographyKey key, InputStream data)  throws CipherException;
}
