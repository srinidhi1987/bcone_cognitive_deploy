package com.automationanywhere.cognitive.common.healthcheck.status;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.model.Dependency;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;

/**
 * Keeps the status information for system dependency.
 */
public class DependencyHealthCheckStatus implements HealthCheckStatus {

    private Dependency dependency;

    /**
     * Constructs the status with all attributes.
     *
     * @param dependency the dependency.
     */
    public DependencyHealthCheckStatus(Dependency dependency) {
        this.dependency = dependency;
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.DEPENDENCY;
    }

    @Override
    public Status getStatus() {
        return dependency.getStatus();
    }

    /**
     * Returns the dependency model.
     *
     * @return the dependency model
     */
    public Dependency getDependency() {
        return dependency;
    }
}
