package com.automationanywhere.cognitive.common.domain.service.security;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.CryptographyKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;

/**
 * Provides a symmetric secret key that can be used to encrypt and decrypt messages.
 */
public interface KeyProvider {

  /**
   * Returns a symmetric key based on name
   *
   * @return symmetric/asymmetric key
   */
  CryptographyKey getKey(KeyName keyName);
  
  /**
   *
   * sets a symmetric/asymmetric key.
   */
  void saveKey(KeyName keyName, CryptographyKey keyValue);

  /**
   * removes a /asymmetric key.
   */
  void removeKey(KeyName name);
}
