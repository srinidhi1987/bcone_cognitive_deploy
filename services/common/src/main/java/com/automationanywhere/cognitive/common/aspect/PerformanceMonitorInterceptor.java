package com.automationanywhere.cognitive.common.aspect;

import com.automationanywhere.cognitive.common.logger.AALogger;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.StopWatch;

public class PerformanceMonitorInterceptor implements MethodInterceptor {
    AALogger logger = AALogger.create(PerformanceMonitorInterceptor.class);

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        StopWatch sw = new StopWatch(getClass().getSimpleName());
        try{
            sw.start(invocation.getMethod().getName());
            logger.trace("%s method started" + invocation.getMethod().getName());
            return invocation.proceed();
        }
        finally {
            sw.stop();
            logger.trace(String.format("%s method completed: ",
                    invocation.getMethod().getName()));
            logger.debug(String.format("Method %s, ResponseTime %s ms",invocation.getMethod().getName(), sw.getTotalTimeMillis()));
        }
    }
}
