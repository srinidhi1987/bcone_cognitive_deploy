package com.automationanywhere.cognitive.common.infrastructure.security.database.dto;

import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyType;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Table(name="SecurityKeys")
@Entity
public class SecurityKey {

  @GeneratedValue(generator = "UUID")
  @Type(type = "uuid-char")
  @GenericGenerator(
      name = "UUID",
      strategy = "org.hibernate.id.UUIDGenerator"
  )
  @Id
  private UUID id;

  /*
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column
  @Id
  private int id;*/

  @Column(name="keyname")
  @Enumerated(EnumType.STRING)
  private KeyName keyName;

  @Column
  private String keyValue;

  @Column
  @Enumerated(EnumType.STRING)
  private KeyType keyType;

  @Column
  @Transient
  private Date createdOn;

  @Column
  private String createdBy;

  public KeyName getKeyName() {
    return keyName;
  }

  public void setKeyName(
      KeyName keyName) {
    this.keyName = keyName;
  }

  public KeyType getKeyType() {
    return keyType;
  }

  public void setKeyType(
      KeyType keyType) {
    this.keyType = keyType;
  }

  public String getKeyValue() {
    return keyValue;
  }

  public void setKeyValue(String keyValue) {
    this.keyValue = keyValue;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public UUID getId() {
    return id;
  }
  public void setId(UUID id) {
    this.id = id;
  }
}
