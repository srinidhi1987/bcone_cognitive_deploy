package com.automationanywhere.cognitive.common.healthapi.json.models;

import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.DateTimeUtils;

/**
 * @author shweta.thakur
 * This class represents the subsystem section of the healthcheck response
 * Subsystem represents the different microservices/applications running
 * and their dependent system status like Database connection, Message Queue 
 * connection, other microservices
 */
public class Subsytem {
    HTTPStatusCode httpStatusCode;
    SystemStatus systemStatus;
    BuildInfo buildInfo;
    Connectivity dbConnectivity;
    Connectivity mqConnectivity;
    Connectivity crConnectivity;
    List<ServiceConnectivity> serviceConnectivityList;
    
    private Subsytem(SubsystemBuilder subsystemBuilder){
        this.httpStatusCode = subsystemBuilder.httpStatusCode;
        this.systemStatus = subsystemBuilder.systemStatus;
        this.buildInfo = subsystemBuilder.buildInfo;
        this.dbConnectivity = subsystemBuilder.dbConnectivity;
        this.mqConnectivity = subsystemBuilder.mqConnectivity;
        this.crConnectivity = subsystemBuilder.crConnectivity;
        this.serviceConnectivityList= subsystemBuilder.serviceConnectivityList;
    }
    
    public static class SubsystemBuilder {

        // required parameters
        private HTTPStatusCode httpStatusCode;
        private SystemStatus systemStatus;
        private BuildInfo buildInfo;

        // optional parameters, will be dependent on service implementation
        Connectivity dbConnectivity;
        Connectivity mqConnectivity;
        Connectivity crConnectivity;
        List<ServiceConnectivity> serviceConnectivityList;

        public SubsystemBuilder(HTTPStatusCode httpStatusCode,
                SystemStatus systemStatus, BuildInfo buildInfo) {
            this.httpStatusCode = httpStatusCode;
            this.systemStatus = systemStatus;
            this.buildInfo = buildInfo;
        }

        public SubsystemBuilder setDBConnectivity(Connectivity dbConnectivity) {
            this.dbConnectivity = dbConnectivity;
            return this;
        }

        public SubsystemBuilder setMQConnectivity(Connectivity mqConnectivity) {
            this.mqConnectivity = mqConnectivity;
            return this;
        }
        
        public SubsystemBuilder setCRConnectivity(Connectivity crConnectivity) {
            this.crConnectivity = crConnectivity;
            return this;
        }

        public SubsystemBuilder setServiceConnectivity(
                List<ServiceConnectivity> serviceConnectivityList) {
            this.serviceConnectivityList = serviceConnectivityList;
            return this;
        }

        public Subsytem build() {
            return new Subsytem(this);
        }
    }
   
    /**
     * If the MQ Connectivity , DB Connectivity and dependent services are not applicable 
     * then do not display them in the output response
     */
    @Override
    public String toString() {
        String applicationName = buildInfo!=null ? buildInfo.getApplication():"";
        String dbConnectionDependency = dbConnectivity != Connectivity.NOT_APPLICABLE ? "\n  Database Connectivity: " + dbConnectivity : "";
        String mqConnectionDependency = mqConnectivity != Connectivity.NOT_APPLICABLE ? "\n  MessageQueue Connectivity: " + mqConnectivity : "";
        String crConnectionDependency = crConnectivity != Connectivity.NOT_APPLICABLE ? "\n  Control Room Connectivity: " + crConnectivity : "";
        String servicesConnectionDependency = (serviceConnectivityList!=null && serviceConnectivityList.size() > 0) ? prepareServiceConnectionString(serviceConnectivityList) : "";
        return String.format("Subsytem \nApplication: %s \nStatus: %s \nApplication uptime: %s \n%s \nDependencies:%s %s %s %s" , 
                applicationName, httpStatusCode.name(), DateTimeUtils.generateTimeInDaysHoursMinSecs(), buildInfo, dbConnectionDependency,
                mqConnectionDependency,crConnectionDependency,servicesConnectionDependency);
     }

    public String prepareServiceConnectionString(List<ServiceConnectivity> serviceConnectivityList){
        StringBuffer serviceConnectivityString = new StringBuffer();
        for (ServiceConnectivity sc : serviceConnectivityList) {
            serviceConnectivityString.append("\n  ").append(sc.getServiceName()).append(": ").append(sc.getHTTPStatus());
        }
        return serviceConnectivityString.toString();
    }
    /**
     * @return the httpStatusCode
     */
    public HTTPStatusCode getHttpStatusCode() {
        return httpStatusCode;
    }

    /**
     * @param httpStatusCode the httpStatusCode to set
     */
    public void setHttpStatusCode(HTTPStatusCode httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    /**
     * @return the systemStatus
     */
    public SystemStatus getSystemStatus() {
        return systemStatus;
    }

    /**
     * @param systemStatus the systemStatus to set
     */
    public void setSystemStatus(SystemStatus systemStatus) {
        this.systemStatus = systemStatus;
    }

    /**
     * @return the buildInfo
     */
    public BuildInfo getBuildInfo() {
        return buildInfo;
    }

    /**
     * @param buildInfo the buildInfo to set
     */
    public void setBuildInfo(BuildInfo buildInfo) {
        this.buildInfo = buildInfo;
    }

    /**
     * @return the dbConnectivity
     */
    public Connectivity getDbConnectivity() {
        return dbConnectivity;
    }

    /**
     * @param dbConnectivity the dbConnectivity to set
     */
    public void setDbConnectivity(Connectivity dbConnectivity) {
        this.dbConnectivity = dbConnectivity;
    }

    /**
     * @return the mqConnectivity
     */
    public Connectivity getMqConnectivity() {
        return mqConnectivity;
    }

    /**
     * @param mqConnectivity the mqConnectivity to set
     */
    public void setMqConnectivity(Connectivity mqConnectivity) {
        this.mqConnectivity = mqConnectivity;
    }

    /**
     * @return the serviceConnectivityList
     */
    public List<ServiceConnectivity> getServiceConnectivityList() {
        return serviceConnectivityList;
    }

    /**
     * @param serviceConnectivityList the serviceConnectivityList to set
     */
    public void setServiceConnectivityList(List<ServiceConnectivity> serviceConnectivityList) {
        this.serviceConnectivityList = serviceConnectivityList;
    }

    /**
     * @return the crConnectivity
     */
    public Connectivity getCrConnectivity() {
        return crConnectivity;
    }

    /**
     * @param crConnectivity the crConnectivity to set
     */
    public void setCrConnectivity(Connectivity crConnectivity) {
        this.crConnectivity = crConnectivity;
    }
}