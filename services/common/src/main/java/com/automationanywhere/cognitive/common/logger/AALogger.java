package com.automationanywhere.cognitive.common.logger;

import org.apache.logging.log4j.util.Supplier;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.spi.AbstractLogger;
import org.apache.logging.log4j.spi.ExtendedLoggerWrapper;

/**
 * Copyright (c) 2017 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
public class AALogger extends ExtendedLoggerWrapper {
    
    private static final long serialVersionUID = 4360643331922995965L;
    private final ExtendedLoggerWrapper logger;

    private static final String FQCN = AALogger.class.getName();
    private static final Level FATAL = Level.forName("FATAL", 350);
    private static final Level ERROR = Level.forName("ERROR", 450);
    private static final Level INFO = Level.forName("INFO", 550);
    private static final Level DEBUG = Level.forName("DEBUG", 650);
    private static final Level TRACE = Level.forName("TRACE", 750);

    private AALogger(final Logger logger) {
        super((AbstractLogger) logger, logger.getName(), logger.getMessageFactory());
        this.logger = this;
    }

    public static AALogger create(Class<?> loggerName) {
        final Logger wrapped = LogManager.getLogger(loggerName);
        return new AALogger(wrapped);
    }

    public void fatal(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, FATAL, null, msgSupplier, null);
    }

    public void error(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, ERROR, null, msgSupplier, null);
    }

    public void info(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, INFO, null, msgSupplier, null);
    }

    public void debug(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, DEBUG, null, msgSupplier, null);
    }

    public void trace(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, TRACE, null, msgSupplier, null);
    }

    public void fatal(final String message) {
        logger.logIfEnabled(FQCN, FATAL, null, message);
    }

    public void error(final String message) {
        logger.logIfEnabled(FQCN, ERROR, null, message);
    }

    public void info(final String message) {
        logger.logIfEnabled(FQCN, INFO, null, message);
    }

    public void debug(final String message) {
        logger.logIfEnabled(FQCN, DEBUG, null, message);
    }

    public void trace(final String message) {
        logger.logIfEnabled(FQCN, TRACE, null, message);
    }
    
    public void auditlog(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, INFO, null, msgSupplier, null);
    }
}
