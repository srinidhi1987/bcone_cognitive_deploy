package com.automationanywhere.cognitive.common.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 * The class listens to the file changes and updates a configuration provider in that case.
 */
public class FileChangeListener {
  private static final Logger LOGGER = LogManager.getLogger(FileChangeListener.class);

  /**
   * Listens to the file changes and updates a configuration provider in that case.
   * The method should be used as a runnable of a thread.
   * For that reason it consumes any errors by logging them,
   * thus the thread run method doesn't need to log them itself.
   *  @param provider - configuration provider to update.
   * @param fileName - name of the file whose modification to listen to.
   * @param callback - will be called when the listener is ready to listen to file changes.
   */
  public void listen(
    final FileConfigurationProvider provider,
    final String fileName,
    final Runnable callback
  ) {
    try {
      WatchService watcher = this.createWatcher(fileName, callback);

      while (!Thread.interrupted()) {
        this.refresh(provider, watcher);
      }
    } catch (final Exception ex) {
      LOGGER.error("Error while waiting for a file system change", ex);
    }
  }

  void refresh(final FileConfigurationProvider provider, final WatchService watcher) throws InterruptedException {
    WatchKey key = watcher.take();

    for (final WatchEvent<?> e : key.pollEvents()) {
      this.refresh(provider, e);
    }

    key.reset();
  }

  void refresh(final FileConfigurationProvider provider, final WatchEvent<?> event) {
    if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
      provider.refresh();
    }
  }

  WatchService createWatcher(final String fileName, final Runnable callback) throws IOException {
    FileSystem fileSystem = FileSystems.getDefault();
    WatchService watcher = fileSystem.newWatchService();

    Path path = fileSystem.getPath(fileName).getParent();
    path.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);

    callback.run();

    return watcher;
  }
}
