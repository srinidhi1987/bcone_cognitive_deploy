package com.automationanywhere.cognitive.common.infrastructure.controlroom;

import java.util.Properties;

/**
 * TODO: Remove it when the real SDK is ready.
 */
public class ControlRoomSdk {

  public static final String CR_URL_PROP_KEY = "crUrl";
  public static final String APP_ID_PROP_KEY = "appId";
  public static final String PRIVATE_KEY_PROP_KEY = "privateKey";

  public void audit(final AuditingEvent event) {
  }

  public void init(final Properties properties, final KeyService keyService) {
  }

  public enum ControlRoomActivityType {
    ACTIVATE_AUTOMATION("Activate automation"),
    ADD_CREDENTIAL("Add Credential"),
    ADD_LOCKER("Add Locker"),
    ADD_ROLE("Create Role"),
    ALLOCATE_LICENSE("Allocate License"),
    AUDIT_ERROR("Audit Error"),
    AUDIT_UNSET_ACTIVITY_TYPE("Unset Activity Type"),
    BI_ADD_USER_DASHBOARD("BI Add User Dashboard"),
    BI_COPY_DASHBOARD("BI Copy Dashboard"),
    BI_DELETE_DASHBOARD("BI Delete Dashboard"),
    BI_DELETE_USER_DASHBOARD("BI Delete User Dashboard"),
    BI_END_TASK("BI End Task"),
    BI_GET_TASK_DATA("BI Get Task Data"),
    BI_GET_USER_DASHBOARDS("BI Get User Dashboards"),
    BI_OPERATIONS("BI Operations"),
    BI_REGISTER_DASHBOARD("BI Register Dashboard"),
    BI_RUN_DATA("Bi Run Data"),
    BI_SAVE_DASHBOARD("BI Save Dashboard"),
    BI_SEARCH_DASHBOARD("BI Search Dashboard"),
    BI_START_TASK("BI Start Task"),
    BI_UPDATE_DASHBOARD("BI Update Dashboard"),
    CHECKOUT("Checkout"),
    CLIENT_LOGIN("Client log in"),
    CONFIGURE_CREDENTIAL_VAULT("Configure Credential Vault"),
    CONNECT_CREDENTIAL_VAULT("Connect Credential Vault"),
    CR_CONFIGURATIONS("General configuration settings"),
    CREATE_AUTOMATION("Create Automation"),
    CREATE_USER("Create User"),
    CREATE_FOLDER("Create Folder"),
    CV_SETTING("Credential Vault Settings"),
    DEACTIVATE_AUTOMATION("Deactivate automation"),
    DELETE_AUTOMATION("Delete Automation"),
    DELETE_BOT("Delete bot"),
    DELETE_CREDENTIAL("Delete Credential"),
    DELETE_FILE("Delete file"),
    DELETE_FOLDER("Delete folder"),
    DELETE_LOCKER("Delete Locker"),
    DELETE_ROLE("Delete Role"),
    DELETE_USER("Delete User"),
    DEPLOYMENT_AUTOMATION("Run bot Deployed"),
    DEVICE_SETTING("Client application configuration settings"),
    DISABLE_USER("Disable User"),
    DOWNLOAD_BOT("Download bot"),
    DOWNLOAD_FILE("Download file"),
    EDIT_BOT("Edit bot"),
    EDIT_SETTINGS("Update Settings"),
    EMAIL_NOTIFICATION("Email Notification"),
    EMAIL_SETTING("Email Notification Settings"),
    ENABLE_USER("Enable User"),
    EXECUTE_AUTOMATION("Execute Automation"),
    EXPORT_BOT_FILES_API("Export bots"),
    FIRST_ADMIN_SETTINGS("General configuration settings"),
    FORCE_UNLOCK_BOT("Unlock bot"),
    FORCE_UNLOCK_FILE("Unlock file"),
    IMPORT_BOT_FILES_API("Import bots"),
    INSTALL_LICENSE("Install License"),
    LOGIN("User log in"),
    LOGOUT("User log out"),
    MOVE_TO_HISTORY("Move Automation To History"),
    MULTI_LOGIN("User multiple log in"),
    POOL_CREATE_FAILED("Device pool creation failed"),
    POOL_CREATED_SUCCESSFULLY("Device pool created"),
    POOL_DELETE_FAILED("Device pool deletion failed"),
    POOL_DELETED_SUCCESSFULLY("Device pool deleted"),
    POOL_UPDATE_FAILED("Device update failed"),
    POOL_UPDATED_SUCCESSFULLY("Device pool updated"),
    QUEUE_CREATE_FAILED("Create queue failed"),
    QUEUE_CREATED("Create Queue"),
    QUEUE_CREATED_DRAFT("Create draft of queue"),
    QUEUE_DELETE_FAILED("Delete queue failed"),
    QUEUE_DELETED("Delete Queue"),
    QUEUE_UPDATE_FAILED("Update queue failed"),
    QUEUE_UPDATED("Update Queue"),
    QUEUE_EXPORT("Export Queues"),
    QUEUE_IMPORT("Import Queues"),
    READ_CREDENTIAL("Read Credential"),
    READ_LOCKER("Read Locker"),
    RELEASE_LICENSE("Release License"),
    RENAME_FOLDER("Rename Folder"),
    ROLLBACK("Rollback"),
    RUN_AUTOMATION("Run Automation"),
    RUN_BOT_DISCONNECTED("Run bot disconnected"),
    RUN_BOT_ENDED("Run bot finished"),
    RUN_BOT_PAUSED("Run bot paused"),
    RUN_BOT_RECONNECTED("Run bot reconnected"),
    RUN_BOT_RESUMED("Run bot resumed"),
    RUN_BOT_STOPPED("Run bot stopped"),
    RUN_BOT_LOCAL_CLIENT_FINISHED("Run Bot (Local Client) finished"),
    RUN_BOT_LOCAL_CLIENT_PAUSED("Run Bot (Local Client) paused"),
    RUN_BOT_LOCAL_CLIENT_RESUMED("Run Bot (Local Client) resumed"),
    RUN_BOT_LOCAL_CLIENT_STARTED("Run Bot (Local Client) started"),
    RUN_BOT_LOCAL_CLIENT_STOPPED("Run Bot (Local Client) stopped"),
    RUN_BOT_LOCAL_CLIENT_SUSPENDED("Run Bot (Local Client) suspended"),
    RUN_BOT_LOCAL_HOTKEY_FINISHED("Run Bot (Local Hotkey) finished"),
    RUN_BOT_LOCAL_HOTKEY_PAUSED("Run Bot (Local Hotkey) paused"),
    RUN_BOT_LOCAL_HOTKEY_RESUMED("Run Bot (Local Hotkey) resumed"),
    RUN_BOT_LOCAL_HOTKEY_STARTED("Run Bot (Local Hotkey) started"),
    RUN_BOT_LOCAL_HOTKEY_STOPPED("Run Bot (Local Hotkey) stopped"),
    RUN_BOT_LOCAL_HOTKEY_SUSPENDED("Run Bot (Local Hotkey) suspended"),
    RUN_BOT_LOCAL_SCHEDULE_FINISHED("Run Bot (Local Schedule) finished"),
    RUN_BOT_LOCAL_SCHEDULE_PAUSED("Run Bot (Local Schedule) paused"),
    RUN_BOT_LOCAL_SCHEDULE_RESUMED("Run Bot (Local Schedule) resumed"),
    RUN_BOT_LOCAL_SCHEDULE_STARTED("Run Bot (Local Schedule) started"),
    RUN_BOT_LOCAL_SCHEDULE_STOPPED("Run Bot (Local Schedule) stopped"),
    RUN_BOT_LOCAL_SCHEDULE_SUSPENDED("Run Bot (Local Schedule) suspended"),
    RUN_BOT_LOCAL_TRIGGER_FINISHED("Run Bot (Local Trigger) finished"),
    RUN_BOT_LOCAL_TRIGGER_PAUSED("Run Bot (Local Trigger) paused"),
    RUN_BOT_LOCAL_TRIGGER_RESUMED("Run Bot (Local Trigger) resumed"),
    RUN_BOT_LOCAL_TRIGGER_STARTED("Run Bot (Local Trigger) started"),
    RUN_BOT_LOCAL_TRIGGER_STOPPED("Run Bot (Local Trigger) stopped"),
    RUN_BOT_LOCAL_TRIGGER_SUSPENDED("Run Bot (Local Trigger) suspended"),
    RUN_LOGIC_LOCAL_CLIENT_FINISHED("Run Logic (Local Client) finished"),
    RUN_LOGIC_LOCAL_CLIENT_PAUSED("Run Logic (Local Client) paused"),
    RUN_LOGIC_LOCAL_CLIENT_RESUMED("Run Logic (Local Client) resumed"),
    RUN_LOGIC_LOCAL_CLIENT_STARTED("Run Logic (Local Client) started"),
    RUN_LOGIC_LOCAL_CLIENT_STOPPED("Run Logic (Local Client) stopped"),
    RUN_LOGIC_LOCAL_CLIENT_SUSPENDED("Run Logic (Local Client) suspended"),
    RUN_WORKFLOW_LOCAL_CLIENT_FINISHED("Run Workflow (Local Client) finished"),
    RUN_WORKFLOW_LOCAL_CLIENT_STARTED("Run Workflow (Local Client) started"),
    RUN_WORKFLOW_LOCAL_CLIENT_STOPPED("Run Workflow (Local Client) stopped"),
    RUN_WORKFLOW_LOCAL_SCHEDULE_FINISHED("Run Workflow (Local Schedule) finished"),
    RUN_WORKFLOW_LOCAL_SCHEDULE_STARTED("Run Workflow (Local Schedule) started"),
    RUN_WORKFLOW_LOCAL_SCHEDULE_STOPPED("Run Workflow (Local Schedule) stopped"),
    RUN_WORKFLOW_LOCAL_TRIGGER_FINISHED("Run Workflow (Local Trigger) finished"),
    RUN_WORKFLOW_LOCAL_TRIGGER_STARTED("Run Workflow (Local Trigger) started"),
    RUN_WORKFLOW_LOCAL_TRIGGER_STOPPED("Run Workflow (Local Trigger) stopped"),
    SCHEDULE_BOT_DISCONNECTED("Schedule bot disconnected"),
    SCHEDULE_BOT_ENDED("Schedule bot ended"),
    SCHEDULE_BOT_PAUSED("Schedule bot paused"),
    SCHEDULE_BOT_RECONNECTED("Schedule bot reconnected"),
    SCHEDULE_BOT_RESUMED("Schedule bot resumed"),
    SCHEDULE_BOT_STOPPED("Schedule bot stopped"),
    SEND_EMAIL("Send Email"),
    UNDO_CHECKOUT("Undo checkout"),
    UNLOCK_CREDENTIAL_VAULT("Unlock Credential Vault"),
    UPDATE_AUTOMATION("Update Automation"),
    UPDATE_CREDENTIAL("Update Credential"),
    UPDATE_CREDENTIAL_OWNERSHIP("Transfer Credential Ownership"),
    UPDATE_LOCKER("Update Locker"),
    UPDATE_ROLE("Edit Role"),
    UPDATE_USER("Edit User"),
    UPLOAD_BOT("Upload bot"),
    UPLOAD_FILE("Upload file"),
    VCS_SETTING("Version Control Settings"),
    WLM_FILE_UPLOADED("Upload work items file"),
    WLM_LOAD_WORKITEMS("Load work items from file"),
    WORKITEM_CREATED("Create WorkItem"),
    WORKITEM_CREATED_FAILED("Create workItem failed"),
    WORKITEM_DELETED("Delete WorkItem"),
    WORKITEM_DELETED_FAILED("Delete workItem failed"),
    WORKITEM_UPDATED("Update WorkItem"),
    WORKITEM_UPDATED_FAILED("Update workItem failed"),
    MIGRATION_START("Migration started"),
    MIGRATION_FINISH("Migration finished"),
    BOT_RUNNER_SESSION("Bot Runner Session"),
    CREATE_LI("Create LI"),
    AUTOMATION_MISFIRED("Schedule misfired");

    public final String name;

    ControlRoomActivityType(final String name) {
      this.name = name;
    }
  }

  public static class KeyService {
  }

  public static class AuditingEvent {

    public final String eventDescription;
    public final String activityType;
    public final String environmentName;
    public final String hostName;
    public final String userName;
    public final String status;
    public final String verbosityLevel;
    public final String source;
    public final Long entityVersion;
    public final String objectName;
    public final String detail;

    public AuditingEvent(
        final String eventDescription,
        final String activityType,
        final String environmentName,
        final String hostName,
        final String userName,
        final String status,
        final String verbosityLevel,
        final String source,
        final Long entityVersion,
        final String objectName,
        final String detail
    ) {
      this.eventDescription = eventDescription;
      this.activityType = activityType;
      this.environmentName = environmentName;
      this.hostName = hostName;
      this.userName = userName;
      this.status = status;
      this.verbosityLevel = verbosityLevel;
      this.source = source;
      this.entityVersion = entityVersion;
      this.objectName = objectName;
      this.detail = detail;
    }
  }
}
