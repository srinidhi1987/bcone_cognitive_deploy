package com.automationanywhere.cognitive.common.resttemplate.wrapper;


import com.automationanywhere.cognitive.common.header.constants.HeaderConstants;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
/**
	This class is a wrapper over standard Http Rest Template,currently it just implements get
**/
public class RestTemplateWrapper {
    private RestTemplate restTemplate;
    private AALogger aaLogger = AALogger.create(this.getClass());

    public RestTemplateWrapper(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private void configureRestTemplate() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(new ObjectMapper());
        StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
        messageConverters.add(stringHttpMessageConerters);
        messageConverters.add(jsonMessageConverter);
        restTemplate.setMessageConverters(messageConverters);
    }

    public ResponseEntity<? extends Object> exchangeGet(String url, Class<? extends Object> responseEntityType) {
        aaLogger.entry();
        configureRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HeaderConstants.CORRELATION_ID, ThreadContext.get(HeaderConstants.CORRELATION_ID));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        aaLogger.exit();
        return restTemplate.exchange(url, HttpMethod.GET, entity, responseEntityType);
    }


}
