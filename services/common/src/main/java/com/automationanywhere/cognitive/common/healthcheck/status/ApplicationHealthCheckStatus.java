package com.automationanywhere.cognitive.common.healthcheck.status;

import com.automationanywhere.cognitive.common.healthcheck.commands.CommandType;
import com.automationanywhere.cognitive.common.healthcheck.model.Application;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;

/**
 * Default class to keeps the status information of the application subsystems.
 */
public class ApplicationHealthCheckStatus implements HealthCheckStatus {

    private Application application;

    /**
     * Default constructor.
     *
     * @param application the application model object with all information about subsystem.
     */
    public ApplicationHealthCheckStatus(Application application) {
        this.application = application;
    }

    @Override
    public CommandType getCommandType() {
        return CommandType.APPLICATION;
    }

    @Override
    public Status getStatus() {
        return application.getAppInfo().getStatus();
    }

    /**
     * Returns the application model.
     *
     * @return the application model
     */
    public Application getApplication() {
        return application;
    }
}
