package com.automationanywhere.cognitive.common.configuration.events;

/**
 * The event is sent whenever configuration provider adds a configuration parameter.
 */
public class ConfigurationAddEvent extends ConfigurationStructureEvent {
  public ConfigurationAddEvent(final String key, final Object value) {
    super(key, value);
  }
}
