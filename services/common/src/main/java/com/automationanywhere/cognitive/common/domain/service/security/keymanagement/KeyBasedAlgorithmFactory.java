package com.automationanywhere.cognitive.common.domain.service.security.keymanagement;

public class KeyBasedAlgorithmFactory {
  public static KeyBasedAlgorithms keyBasedAlgorithm(KeyType keyType){
    KeyBasedAlgorithms keyBasedAlgorithms = null;
    switch(keyType){
      case SYMMETRIC: keyBasedAlgorithms = new SymmetricKeyAlgorithm();
        break;
      case ASYMMETRIC: keyBasedAlgorithms = new AsymmetricKeyAlgorithm();
    }
    return keyBasedAlgorithms;
  }
}
