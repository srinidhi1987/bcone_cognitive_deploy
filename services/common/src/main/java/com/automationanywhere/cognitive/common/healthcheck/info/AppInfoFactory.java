package com.automationanywhere.cognitive.common.healthcheck.info;

import com.automationanywhere.cognitive.common.healthcheck.model.AppInfo;
import com.automationanywhere.cognitive.common.healthcheck.model.Status;
import java.lang.management.ManagementFactory;
import org.springframework.stereotype.Component;

@Component
public class AppInfoFactory {

  public AppInfo build(
      String appName,
      String type,
      Status status,
      String failReason,
      String uptime
  ) {
    return new AppInfo(
        appName,
        type,
        status,
        failReason,
        uptime
    );
  }

  public AppInfo build(
      String appName,
      String type,
      Status status,
      String failReason
  ) {
    return new AppInfo(
        appName,
        type,
        status,
        failReason,
        generateUptime()
    );
  }

  /**
   * Generates the uptime string.
   *
   * @return the uptime in string.
   */
  private String generateUptime() {
    long millisec = ManagementFactory.getRuntimeMXBean().getUptime();
    long days = millisec / 86_400_000;
    millisec %= 86_400_000;
    long hours = millisec / 3_600_000;
    millisec %= 3_600_000;
    long min = millisec / 60_000;
    millisec %= 60_000;
    long sec = millisec / 1_000;
    millisec %= 1_000;
    return String.format("%dd %dh %dm %ds.%d", days, hours, min, sec, millisec);
  }
}
