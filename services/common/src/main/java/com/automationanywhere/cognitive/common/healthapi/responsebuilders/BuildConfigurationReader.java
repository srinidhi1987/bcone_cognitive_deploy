package com.automationanywhere.cognitive.common.healthapi.responsebuilders;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import com.automationanywhere.cognitive.common.logger.AALogger;

/**
 * @author shweta.thakur
 * 
 * This class is used to read the build file
 *
 */
public class BuildConfigurationReader {
	AALogger aaLogger = AALogger.create(this.getClass());

	/**
	 * This function will return a properties map based on the build.version file read
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public Properties readBuildVersionFile() throws IOException {
		// Read from the build.version file
		Properties prop = new Properties();
		InputStream inputStream = BuildConfigurationReader.class.getResourceAsStream("/build.version");
		if(inputStream==null){
			throw new IOException("build.version file not found");
		}
		prop.load(inputStream);
		return prop;
	}

}
