package com.automationanywhere.cognitive.common.domain.service.appconfiguration;

public class AppConfigurationException extends RuntimeException {

  private static final long serialVersionUID = -2058622593396649147L;

  public AppConfigurationException(final String message) {
    super(message);
  }

  public AppConfigurationException(final Throwable cause) {
    super(cause);
  }
}
