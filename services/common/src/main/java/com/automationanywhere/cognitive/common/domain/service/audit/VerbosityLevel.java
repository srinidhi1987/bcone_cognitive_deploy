package com.automationanywhere.cognitive.common.domain.service.audit;

public enum VerbosityLevel {
  GENERAL("General");

  public final String name;

  VerbosityLevel(final String name) {
    this.name = name;
  }
}
