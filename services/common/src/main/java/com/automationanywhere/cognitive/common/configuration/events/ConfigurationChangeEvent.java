package com.automationanywhere.cognitive.common.configuration.events;

import java.util.Objects;

/**
 * The event is sent whenever configuration provider detects changes of configuration parameters.
 */
public class ConfigurationChangeEvent extends ConfigurationBaseEvent {
  public final Object oldValue;
  public final Object newValue;

  public ConfigurationChangeEvent(
    final String key,
    final Object oldValue,
    final Object newValue
  ) {
    super(key);

    this.oldValue = oldValue;
    this.newValue = newValue;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      ConfigurationChangeEvent that = (ConfigurationChangeEvent) o;

      result = Objects.equals(this.key, that.key)
            && Objects.equals(this.oldValue, that.oldValue)
            && Objects.equals(this.newValue, that.newValue);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.key)
         ^ Objects.hashCode(this.oldValue)
         ^ Objects.hashCode(this.newValue);
  }
}
