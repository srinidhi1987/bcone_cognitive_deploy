package com.automationanywhere.cognitive.common.healthcheck.model;

/**
 * Responsible for keep the data of dependency.
 */
public class Dependency {

  private final String name;
  private final Status status;

  /**
   * Default constructor.
   *
   * @param name the name of the dependency.
   * @param status the status of the dependency.
   */
  public Dependency(String name, Status status) {
    this.name = name;
    this.status = status;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the status.
   *
   * @return the value of status
   */
  public Status getStatus() {
    return status;
  }
}
