# Example YAML to get you started quickly.
# Be aware that YAML has indentation based scoping.
# Code completion support is available so start typing for available options.
swagger: '2.0'

# This is your document metadata
info:
  version: "1.0.0"
  title: Authentication Service

# Describe your paths here
paths:
  # This is a path endpoint. Change it.
 
  /authentication:
    post:
      # Describe this verb here. Note: you can use markdown
      description: |
        Endpoint to authenticate a user. A app token will be spent back after successful authentication. That token will be attached to the header. - **Authorization: Bearer <token>**
      # This is array of GET operation parameters:
      parameters:
        # An example parameter that is in query and is required
        - name: authenticate user
          in: body
          description: user credentials
          required: true
          schema:
            $ref: '#/definitions/UserCredentials'

        # Expected responses for this operation:
      responses:
        # Response code
        200:
          description: Gets aggreigated totals across all organization projects.
          schema:
            type: array
            items:
              $ref: '#/definitions/AuthenticationResponse'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'

  /authentication/logout:
    post:
      # Describe this verb here. Note: you can use markdown
      description: |
        Endpoint to expire a user and session
      # This is array of GET operation parameters:
      parameters:
        # An example parameter that is in query and is required
        - name: logout
          in: body
          description: Logout a current user session
          required: true
          schema:
            $ref: '#/definitions/UserLogout'
          
        # Expected responses for this operation:
      responses:
        # Response code
        200:
          description: Gets aggreigated totals across all organization projects.
          schema:
            type: array
            items:
              $ref: '#/definitions/StandardResponse'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'

definitions:
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string

  StandardResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Was successful
      data:
        type: object
      errors:
        type: array
        items:
          type: string

  AuthenticationResponse:
    type: object
    properties:
      success:
        type: boolean
        description: successful login
      data:
        type: object
        properties:
          user:
            type: object
            properties:
              id:
                type: string
                description: Unique identifier for the user object
              name:
                type: string
                description: Name of the current user
              role:
                type: string
                description: Role assigned to the user
                enum: ['Admin', 'Validator']
          token:
            type: string
            description: App token based on current user session
      errors:
        type: array
        items:
          type: string
        
  UserCredentials:
    type: object
    properties:
      username:
        type: string
        description: Username / Email of user
      password:
        type: string
        description: User password
        
  UserLogout:
    type: object
    properties:
      token:
        type: string
        description: Current app token which will be expired.
        
      