swagger: '2.0'

# This is your document metadata
info:
  version: "1.4.0"
  title: Authentication Service

paths:
  /authentication:
    post:
      description: |
        Endpoint to authenticate a user. A app token will be spent back after successful authentication. That token will be attached to the header. - **Authorization: Bearer <token>**
      parameters:
        - name: authenticate user
          in: body
          description: user credentials
          required: true
          schema:
            $ref: '#/definitions/UserCredentials'
      responses:
        200:
          description: Gets aggregated totals across all organization projects.
          schema:
            type: array
            items:
              $ref: '#/definitions/AuthenticationResponse'
        403:
          description: User/Password not valid
          schema:
            $ref: '#/definitions/StandardResponse'

  /invalidate-session:
    get:
      description: Invalidates the current user's session
      parameters:
        - name: Authorization
          in: header
          description: Bearer token
          required: true
          type: string
      responses:
        200:
          description: The session was invalidated.
          schema:
            $ref: '#/definitions/StandardResponse'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'

  /authorization:
    post:
      description: |
        Endpoint to authorize a user
      parameters:
        - name: token
          in: body
          required: true
          schema:
            allOf:
              - $ref: '#/definitions/KeyTokenAuthorization'
              - $ref: '#/definitions/TokenAuthorization'
      responses:
        200:
          description: The user is authorized.
        403:
          description: The user is unautorized
          schema:
            type: string
            description: the unauthorized alert message

definitions:
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string

  StandardResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Was successful
      data:
        type: object
      errors:
        type: array
        items:
          type: string

  AuthenticationResponse:
    type: object
    properties:
      success:
        type: boolean
        description: successful login
      data:
        type: object
        properties:
          user:
            type: object
            properties:
              id:
                type: string
                description: Unique identifier for the user object
              name:
                type: string
                description: Name of the current user
              roles:
                type: array
                description: All roles assigned to the user
                items:
                  type: string
          token:
            type: string
            description: App token based on current user session
      errors:
        type: array
        items:
          type: string

  UserCredentials:
    type: object
    properties:
      username:
        type: string
        description: Username / Email of user
      password:
        type: string
        description: User password

  KeyTokenAuthorization:
    type: object
    properties:
      clientkey:
        type: string
      clienttoken:
        type: string

  TokenAuthorization:
    type: object
    properties:
      jwt:
        type: string
      action:
        type: string