package com.automationanywhere.cognitive.auth.custom.exceptions;

public class CRConnectionFailureException extends RuntimeException{
    
    private static final long serialVersionUID = -6734758924714852221L;

    public CRConnectionFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public CRConnectionFailureException(String message) {
        super(message);
    }

}
