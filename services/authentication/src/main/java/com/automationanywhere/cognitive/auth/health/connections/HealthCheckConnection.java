package com.automationanywhere.cognitive.auth.health.connections;

public interface HealthCheckConnection {
	void checkConnectionToCR(String crHostName, int crPort);
}
