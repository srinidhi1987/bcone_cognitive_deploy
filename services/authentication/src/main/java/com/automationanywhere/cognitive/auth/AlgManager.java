package com.automationanywhere.cognitive.auth;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * This is a centralized way to manage everything related to cryptographic operations
 * of the SAME algorithm. This means that callers can be sure that different components
 * (like MACs or keys) provided by his manager are compatible with each other.
 */
public class AlgManager {
  private static final String JWT_ALG_NAME = "HS256";
  private static final String MAC_ALG_NAME = "HmacSHA256";

  public Mac createMac() throws NoSuchAlgorithmException {
    return Mac.getInstance(MAC_ALG_NAME);
  }

  public SecretKeySpec createKey(final String password) {
    return new SecretKeySpec(password.getBytes(StandardCharsets.UTF_8), MAC_ALG_NAME);
  }

  public String getJwtAlgName() {
    return JWT_ALG_NAME;
  }
}
