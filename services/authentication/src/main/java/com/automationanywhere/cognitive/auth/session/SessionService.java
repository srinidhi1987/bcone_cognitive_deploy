package com.automationanywhere.cognitive.auth.session;

import java.util.Date;
import java.util.UUID;
import java.util.function.Function;

/**
 * Session service is responsible for maintaining the session lifecycle.
 * It's up to the implementation to select what strategy to use to
 * clean-up a data storage: invalid sessions may be purged whenever
 * a session storage is accessed; or it may be cleaned up on a schedule.
 */
public class SessionService {
  private final SessionStorage sessionStorage = new SimpleSessionStorage();
  private final Long maxSessionTime;
  private final Long maxSessionIdleTime;

  public SessionService(final Long maxSessionTime, final Long maxSessionIdleTime) {
    this.maxSessionTime = maxSessionTime;
    this.maxSessionIdleTime = maxSessionIdleTime;
  }

  /**
   * Starts a session.
   *
   * @return - session object.
   */
  public Session createSession(final String entityId) {
    long now = getNow();
    Session session = new Session(
        UUID.randomUUID().toString(), entityId, now, now
    );

    synchronized (sessionStorage) {
      sessionStorage.save(session);
    }

    return session;
  }

  /**
   * Finds a session by its id.
   * Never returns invalid or expired session objects.
   *
   * @param sessionId - id of the session to find.
   * @return found session, never returns null.
   *
   * @throws SessionNotFoundException - if a session with the given id doesn't exit.
   * @throws SessionExpiredException - if a session expired.
   */
  public Session findSession(final String sessionId)
      throws SessionNotFoundException, SessionExpiredException {
    Session session = null;
    synchronized (sessionStorage) {
      session = sessionStorage.find(sessionId);
      if (session == null) {
        throw new SessionNotFoundException();
      }
      if (!validateSession(session)) {
        sessionStorage.remove(session.id);
        throw new SessionExpiredException();
      }
    }
    return session;
  }

  /**
   * Resets the idle timeout of a session.
   *
   * @param id - id of the session to reset.
   *
   * @throws SessionNotFoundException - if a session with the given id doesn't exit.
   * @throws SessionExpiredException - if a session expired.
   */
  public void resetSession(final String id)
      throws SessionNotFoundException, SessionExpiredException {
    synchronized (sessionStorage) {
      Session session = findSession(id);

      if (maxSessionIdleTime != null) {
        sessionStorage.save(new Session(session, getNow()));
      }
    }
  }

  /**
   * This implementation allows one entity to be associated with many sessions.
   * In some cases there may be restrictions on how many sessions are allowed per entity.
   * This method is used to terminate sessions, what sessions are terminated
   * is defined by a caller. (This means that a caller may terminate sessions using any criteria.)
   *
   * @param id - id of the current session.
   * @param filter - removing criteria, the function receives
   * an object with the found current session and another session known to the session service;
   * if the function returns true then the other session will be removed.
   *
   * @throws SessionNotFoundException - if a session with the given id doesn't exit.
   * @throws SessionExpiredException - if a session expired.
   */
  public void invalidateSessions(final String id, final Function<FilterDetails, Boolean> filter) {
    synchronized (sessionStorage) {
      Session session = findSession(id);

      sessionStorage.remove(s -> filter.apply(new FilterDetails(session, s)));
    }
  }

  private boolean validateSession(final Session session) {
    long now = getNow();
    return (maxSessionTime == null || now < session.startTime + maxSessionTime)
        && (maxSessionIdleTime == null || now < session.lastAccessedTime + maxSessionIdleTime);
  }

  long getNow() {
    return new Date().getTime();
  }

  public static class FilterDetails {
    public final Session currentSession;
    public final Session session;

    public FilterDetails(final Session currentSession, final Session session) {
      this.currentSession = currentSession;
      this.session = session;
    }
  }
}
