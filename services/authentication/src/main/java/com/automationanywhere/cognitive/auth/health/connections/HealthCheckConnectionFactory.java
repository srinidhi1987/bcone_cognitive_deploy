package com.automationanywhere.cognitive.auth.health.connections;

import org.springframework.web.client.RestTemplate;
import com.automationanywhere.cognitive.auth.custom.exceptions.UnSupportedConnectionTypeException;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;

public class HealthCheckConnectionFactory {
	public HealthCheckConnection getConnection(ConnectionType connType){
		switch(connType){
		    case CR: return new CRCheckConnection(new RestTemplateWrapper(new RestTemplate())); 
		    default: throw new UnSupportedConnectionTypeException();
		}
	}
}
