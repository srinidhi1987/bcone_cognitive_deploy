package com.automationanywhere.cognitive.auth.exception.impl;

public class AuthenticationException extends RuntimeException {

    private static final long serialVersionUID = 4164074038345862525L;
    private String message;
    int statusCode;

    public AuthenticationException(int statusCode, String message) {
        this.message = message;
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
