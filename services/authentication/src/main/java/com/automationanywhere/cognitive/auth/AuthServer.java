package com.automationanywhere.cognitive.auth;

import static com.automationanywhere.cognitive.auth.constants.HeaderConstants.CORRELATION_ID;
import static com.automationanywhere.cognitive.auth.constants.MessageConstants.MESSAGE_LICENSE_EXPIRED;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.secure;

import com.automationanywhere.cognitive.auth.exception.impl.AuthenticationException;
import com.automationanywhere.cognitive.auth.exception.impl.ExceptionFilter;
import com.automationanywhere.cognitive.auth.exception.impl.HttpStatusCode;
import com.automationanywhere.cognitive.auth.health.connections.HealthCheckConnectionFactory;
import com.automationanywhere.cognitive.auth.health.handlers.impl.AuthenticationHealthCheckHandler;
import com.automationanywhere.cognitive.auth.model.AuthenticationRequest;
import com.automationanywhere.cognitive.auth.model.ControlRoomAuthorizationResponse;
import com.automationanywhere.cognitive.auth.model.UserControlRoomAuthorizationResponse;
import com.automationanywhere.cognitive.auth.service.ControlRoomAuthenticationService;
import com.automationanywhere.cognitive.auth.session.Session;
import com.automationanywhere.cognitive.auth.session.SessionExpiredException;
import com.automationanywhere.cognitive.auth.session.SessionNotFoundException;
import com.automationanywhere.cognitive.auth.session.SessionService;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.function.Function;
import org.apache.logging.log4j.ThreadContext;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

public class AuthServer {

    private static final int FIRST_OPTIONAL_PARAM_INDEX = 5;
    private static final String MAX_SESSION_TIME_PARAM_PREFIX = "max-session-time=";
    private static final String MAX_SESSION_IDLE_TIME_PARAM_PREFIX = "max-session-idle-time=";
    private static final String[] PREFIXES = {
        MAX_SESSION_TIME_PARAM_PREFIX,
        MAX_SESSION_IDLE_TIME_PARAM_PREFIX
    };
    private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations" + File.separator + "CognitivePlatform.jks";
    private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations" + File.separator + "Cognitive.properties";
    private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
    private static final String PROPERTIES_NOTFOUND = "NotFound";
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String AUTH_HEADER_PREFIX = "Bearer ";
    private static final String ROLES_ATTR_NAME = "roles";
    private static final boolean DEFAULT_ENABLE_CONTROL_ROOM_SSL = false;
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final JWTDetailsParser PARSER = new JWTDetailsParser();

    private static String urlTokenAuth = "<urlscheme>://<hostname>:<port>/webcrsvc/SecurityV2/ValidateToken/";
    private static String strPassword;
    private static String strControlRoomHost;
    private static boolean crSSLConfigured = DEFAULT_ENABLE_CONTROL_ROOM_SSL;
    private static int iControlRoomPort;
    private static Set<String> permissionsSet;
    private static SessionService sessionService;
    private static ControlRoomAuthenticationService authenticationService;
    private static AALogger logger = AALogger.create(AuthServer.class);

    private enum HTTP_METHODS {GET, POST, PUT, PATCH, DELETE}

    private static CRResponse sendHttpRequest(HTTP_METHODS httpMethod, String strUrl,
                                              Map<String, String> headerMap,
                                              byte[] body) {
        logger.entry();
        logger.debug(() -> strUrl);
        StringBuilder response = new StringBuilder();
        HttpURLConnection conn = null;
        try {
            URL url = new URL(strUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(httpMethod.toString());

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            for (String header : headerMap.keySet())
                conn.setRequestProperty(header, headerMap.get(header));
            // TODO Content-Length header should match body.length - check?

            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                wr.write(body, 0, body.length);
                wr.close();
            }

            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            boolean isNotFirstTime = false;
            while ((line = rd.readLine()) != null) {
                if (isNotFirstTime) {
                    response.append('\r');
                } else {
                    isNotFirstTime = true;
                }
                response.append(line);
            }
            rd.close();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        CRResponse crResponse = new CRResponse(
            response.toString(), conn == null ? Collections.emptyMap() : conn.getHeaderFields()
        );
        logger.exit();

        return crResponse;
    }

    private static String authenticateTokenWithCR(String token, String crHostName, int iCrPort) {
        logger.entry();
        logger.debug(() -> String.format("CRHostName:%s, CRPort: %s", crHostName, iCrPort));
        urlTokenAuth = urlTokenAuth.replace("<hostname>", crHostName);
        urlTokenAuth = urlTokenAuth.replace("<port>", "" + iCrPort);
        urlTokenAuth = urlTokenAuth.replace("<urlscheme>", crSSLConfigured ? "https" : "http");
        HashMap<String, String> headers = new HashMap<>();
        headers.put("X-Authorization", token);
        String formBody = "";
        String result = sendHttpRequest(
                HTTP_METHODS.POST, urlTokenAuth, headers, formBody.getBytes()).getResponseBody();
        logger.exit();
        return result;
    }

    public static void main(String[] args) throws IOException {
        // If the index of the last param is less than the index of the last not optional param
        // then there are too few parameters passed in the command line.
        if (0 + args.length - 1 < FIRST_OPTIONAL_PARAM_INDEX - 1) {
            System.out.println("The service is responsible for authentication and authorization of request callers.\n"
                + "It excepts the folowing parameters in the folowing order:\n"
                + "password local-port control-room-host control-room-port action-roles-file control-room-isSSL max-session-time=seconds\n\n"
                + "password: HMAC password used to sign tokens or validate token signatures\n"
                + "local-port: port on the host where the service is running which others sould use to connect to the service\n"
                + "control-room-host: FQN or IP of the host where a Control Room instance is running\n"
                + "control-room-port: port which the Control Room instance is avilable on\n"
                + "action-roles-file: file containing the ACL matrix, requests and user roles will be checked against the matrix in that file to authorize request callers\n"
                + "control-room-isSSL: if 1 is set then SSL will be enabled, if the parameter is not specified the SSL is not enabled by defautlt\n"
                + "max-session-time=seconds: specifies the maximum amount of time in seconds that a session may last, if not specified sessions are infinite.\n"
                + "max-session-idle-time=seconds: specifies the maximum amount of time in seconds that a session may be inactive, if not specified session inactivty time is not considered for the session invalidtion.\n"
            );
            logger.error("Invalid arguments passed");
            return;
        }

        int iLocalPort;
        Long maxSessionTime = null;
        Long maxSessionIdleTime = null;

        try {
            strPassword = args[0];
            iLocalPort = Integer.parseInt(args[1]);
            strControlRoomHost = args[2];
            iControlRoomPort = Integer.parseInt(args[3]);
            permissionsSet = new HashSet<>();
            String strPermissions = String.join("\n",
                    Files.readAllLines(Paths.get(args[4])));

            int firstNamedParamIndex = FIRST_OPTIONAL_PARAM_INDEX;
            if (args.length > FIRST_OPTIONAL_PARAM_INDEX) {
                try {
                    crSSLConfigured = Integer.parseUnsignedInt(args[FIRST_OPTIONAL_PARAM_INDEX]) == 1;
                    firstNamedParamIndex = FIRST_OPTIONAL_PARAM_INDEX + 1;
                    logger.info("CR SSL Configuration found: " + crSSLConfigured);
                } catch (final NumberFormatException ex) {
                    // The 5th parameter is specified but it's not a number,
                    // then it is another parameter like max-session-time
                    // which will be parsed in the getMaxSessionTime call below

                    // It means that no value was provided by a user,
                    // setting a default value explicitly to avoid a NOOP.
                    crSSLConfigured = DEFAULT_ENABLE_CONTROL_ROOM_SSL;
                }
            }

            maxSessionTime = getMaxSessionTime(args, firstNamedParamIndex);
            maxSessionIdleTime = getMaxSessionIdleTime(args, firstNamedParamIndex);

            for (int i = firstNamedParamIndex; i < args.length; i+= 1) {
                int index = i;
                if (Arrays.stream(PREFIXES).noneMatch(args[index]::startsWith)) {
                    throw new RuntimeException("Unknown parameter " + args[index]);
                }
            }

            List<?> permissions = null;
            try {
                permissions = (List<?>) MAPPER.readValue(strPermissions, List.class);
            } catch (IOException e) {
                logger.error("bad permissions JSON");
                return;
            }

            for (final Object permission : permissions) {
                if (!(permission instanceof String)) {
                    logger.error("permission should be " +
                            "<action>@<role1>,<role2>...");
                    return;
                }
                // sort roles just in case...
                String strPermission = (String) permission;
                String[] strParts = strPermission.split("@");
                if (strParts.length != 2) {
                    logger.error("wrong permission format " + permission);
                    return;
                }
                String[] strRoles = strParts[1].split(",");
                List<String> roles = Arrays.asList(strRoles);
                Collections.sort(Arrays.asList(strRoles));
                if (roles != null && roles.size() > 0) {
                    for (String role : roles) {
                        String sortedRoles = strParts[0] + "@" + role;
                        permissionsSet.add(sortedRoles);
                    }
                }
            }
        } catch (IOException e) {
            logger.error("can't load permissions", e);
            return;
        } catch (NumberFormatException e) {
            logger.error("can't convert port to integer", e);
            return;
        }

        setSessionService(new SessionService(maxSessionTime, maxSessionIdleTime));
        setAuthenticationService();
        setServiceSecurity();
        setExceptionFilter();
        port(iLocalPort);

        setUpRoutes();
    }

    private static void setExceptionFilter() {
        ExceptionFilter exceptionFilter = new ExceptionFilter();
        exceptionFilter.init();
        exceptionFilter.setMapper(MAPPER);
    }

    private static void setUpRoutes() {
        after(((request, response) -> postRequestProcessor(request, response)));
        before((request, response) -> preRequestProcessor(request));

        get("/health", getHealthHandler());
        get("/heartbeat", getHeartbeatHandler());
        get("/healthcheck", getHealthCheckHandler());
        post("/authentication", getAuthenticationHandler());
        get("/invalidate-session", getInvalidateSessionHandler());
        post("/authorization", getAuthorizationHandler());
    }

    private static Object handle(final Response res, final Callable<Object> callable) throws Exception {
        Object result = null;

        logger.entry();

        try {
            result = callable.call();
            res.status(HttpStatus.Code.OK.getCode());
        } catch (final JWTDetailsParserException ex) {
            logger.error("Invalid JWT.", ex);
            result = initResult(res, HttpStatus.Code.BAD_REQUEST, ex.getMessage());
        } catch (final InvalidDataException ex) {
            logger.error("Invalid request payload.", ex);
            result = initResult(res, HttpStatus.Code.BAD_REQUEST, ex.getMessage());
        } catch (final SessionNotFoundException ex) {
            logger.error("Session has already expired.", ex);
            result = initResult(res, HttpStatus.Code.FORBIDDEN, "Session not found.");
        } catch (final SessionExpiredException ex) {
            logger.error("Session has already expired.", ex);
            result = initResult(res, HttpStatus.Code.FORBIDDEN, "Session has already expired.");
        } catch (final AuthorizationException ex) {
            logger.error("Authorization error.", ex);
            result = initResult(res, HttpStatus.Code.FORBIDDEN, "You are not allowed to access this Resource.");
        } catch (final AuthenticationException ex) {
            //Please noitice we use the same HTTP status code for all authentication/authorization related errors.
            logger.error("Authentication error", ex);

            result = initResult(
                res, HttpStatus.Code.FORBIDDEN,
                ex.getStatusCode() == HttpStatusCode.PAYMENTGATEWAYREQUIRED.getCode()
                    ? MESSAGE_LICENSE_EXPIRED
                    : ex.getMessage()
            );
        } catch (final Exception ex) {
            logger.error("Unhandled error", ex);
            result = initResult(res, HttpStatus.Code.INTERNAL_SERVER_ERROR, ex.getMessage());
        } finally {
            logger.exit();
        }

        return result;
    }

    private static Route getAuthorizationHandler() {
        return (req, res) -> handle(res, () -> {
            JsonNode body = getBody(req);

            //VisionBotLiteCommand Auth Logic
            if (body.has("clienttoken")) {
                authorizeVisionBotLight(body);
            } else {
                authorizeUser(body);
            }

            return true;
        });
    }

    private static JsonNode getBody(final Request req) {
        JsonNode body = null;
        try {
            body = MAPPER.readTree(req.body());
        } catch (final IOException ex) {
            throw new InvalidDataException("Invalid request payload", ex);
        }
        return body;
    }

    private static void authorizeUser(final JsonNode body) throws Exception {
        TokenAuthorization auth = getTokenAuthorization(body);
        JWTDetails jwtDetails = PARSER.parse(auth.jwt, strPassword);
        Session session = sessionService.findSession(jwtDetails.sessionId);
        authorizeUser(auth, session.get(ROLES_ATTR_NAME));
        sessionService.resetSession(session.id);
    }

    private static void authorizeUser(final TokenAuthorization auth, final List<String> roles) {
        boolean isAllowed = false;
        if (auth.action != null && roles != null) {
            for (String role : roles) {
                String key = auth.action + "@" + role;
                if (permissionsSet.contains(key)) {
                    isAllowed = true;
                    break;
                }
            }
        }
        if (!isAllowed) {
            throw new AuthorizationException();
        }
    }

    private static TokenAuthorization getTokenAuthorization(final JsonNode body) {
        TokenAuthorization auth = null;
        try {
            auth = MAPPER.treeToValue(body, TokenAuthorization.class);
        } catch (final IOException ex) {
            throw new InvalidDataException("Bad authorization token in the payload", ex);
        }
        return auth;
    }

    private static void authorizeVisionBotLight(final JsonNode body) throws IOException {
        KeyTokenAuthorization auth = MAPPER.treeToValue(body, KeyTokenAuthorization.class);
        String authResponse = authenticateTokenWithCR(auth.clientToken, strControlRoomHost, iControlRoomPort);
        CRAuthResponse crAuthResponse = MAPPER.readValue(authResponse, CRAuthResponse.class);
        String crAuthResponseClientKey = crAuthResponse.clientKey;
        if (!auth.clientKey.toLowerCase().equals(crAuthResponseClientKey.toLowerCase())) {
            throw new AuthorizationException();
        }
    }

    private static Route getInvalidateSessionHandler() {
        return (req, res) -> handle(res, () -> {
            String token = getToken(req);

            JWTDetails jwtDetails = PARSER.parse(token, strPassword);

            sessionService.invalidateSessions(
                jwtDetails.sessionId,
                fd -> fd.currentSession.id.equals(fd.session.id)
            );

            return buildStandardResponse(true, null, null).toJsonString();
        });
    }

    private static String getToken(final Request req) {
        String authHeader = req.headers(AUTHORIZATION_HEADER_NAME);
        if (authHeader == null || !authHeader.startsWith(AUTH_HEADER_PREFIX)) {
            throw new InvalidDataException("Invalid authorization header");
        }
        return authHeader.substring(AUTH_HEADER_PREFIX.length()).trim();
    }

    private static Route getAuthenticationHandler() {
        return (req, res) -> handle(res, () -> {
            AuthenticationRequest authenticationRequest = getAuthenticationRequest(req);
            UserControlRoomAuthorizationResponse authenticationResponse = (UserControlRoomAuthorizationResponse) authenticationService.authenticate(authenticationRequest);
            Session session = createSession(authenticationResponse);
            session.put(ROLES_ATTR_NAME, authenticationResponse.getRoles());
            return buildAuthenticationResponse(authenticationRequest.getUsername(), authenticationResponse, session.id).toJsonString();
        });
    }

    private static AuthenticationRequest getAuthenticationRequest(final Request req)
        throws IOException {
        String requestBody = req.body();
        requestBody = requestBody.replace("\\", "\\\\");
        return MAPPER.readValue(requestBody, AuthenticationRequest.class);
    }

    private static Route getHealthCheckHandler() {
        return (request, response) -> {
            HealthCheckConnectionFactory healthCheckConnectionFactory = new HealthCheckConnectionFactory();
            AuthenticationHealthCheckHandler healthCheckHandler = new AuthenticationHealthCheckHandler(healthCheckConnectionFactory);
            return healthCheckHandler.checkCRHealth(strControlRoomHost, iControlRoomPort);
        };
    }

    private static Route getHeartbeatHandler() {
        return (request, response) -> {
            return getHeartBeatInfo(response);
        };
    }

    private static Route getHealthHandler() {
        return (request, response) -> {
            logger.entry();
            response.status(HttpStatusCode.OK.getCode());
            response.body("OK");
            logger.exit();
            return response;
        };
    }

    private static Object initResult(
        final Response res, final HttpStatus.Code httpStatus, final String message
    ) {
        AuthenticationResponse errorResponse = createErrorMessage(
            httpStatus, message
        );
        res.status(httpStatus.getCode());
        res.body(errorResponse.toString());
        return errorResponse.toJsonString();
    }

    static void setSessionService(final SessionService ss) {
        sessionService = ss;
    }

    private static void setAuthenticationService() {
        authenticationService = new ControlRoomAuthenticationService(
            strControlRoomHost, String.valueOf(iControlRoomPort), crSSLConfigured
        );
    }

    static Session createSession(
        final UserControlRoomAuthorizationResponse authenticationResponse
    ) {
        Session session = sessionService.createSession(authenticationResponse.userId);
        // A sync on the session is not needed here
        // because at this moment, this is the only place where we modify it.
        sessionService.invalidateSessions( // Invalidate all sessions
            session.id,
            fd -> fd.session.entityId.equals(fd.currentSession.entityId) // ... of the current user
                && !fd.currentSession.id.equals(fd.session.id) // ... except the current session itsef.
        );
        return session;
    }

    private static Long getMaxSessionTime(final String[] args, int start) {
        return AuthServer.getParam(
            args,
            start,
            MAX_SESSION_TIME_PARAM_PREFIX,
            v -> 1000 * Long.parseUnsignedLong(v)
        );
    }

    private static Long getMaxSessionIdleTime(final String[] args, int start) {
        return AuthServer.getParam(
            args,
            start,
            MAX_SESSION_IDLE_TIME_PARAM_PREFIX,
            v -> 1000 * Long.parseUnsignedLong(v)
        );
    }

    private static <T> T getParam(
        final String[] args,
        final int start,
        final String prefix,
        final Function<String, T> parser
    ) {
        T param = null;
        for (int i = start; i < args.length; i+= 1) {
            if (args[i].startsWith(prefix) ) {
                param = parser.apply(args[i].substring(prefix.length()));
                break;
            }
        }
        return param;
    }

    private static void postRequestProcessor(Request request, Response response) {
        addHeader(request, response);
        captureStopTimeAttribute(request);
    }

    private static void addHeader(Request request, Response response) {
        response.type("application/json");
    }

    private static void captureStartTimeAttribute(Request request) {
        long startTime = System.currentTimeMillis();
        request.attribute("startTime", startTime);
    }

    private static void captureStopTimeAttribute(Request request) {
        long startTime = (Long) request.attribute("startTime");
        logger.debug("Request URL: " + request.uri().toString()
                + ", Response Time: " + (System.currentTimeMillis() - startTime));
    }

    private static AuthenticationResponse createErrorMessage(HttpStatus.Code statusCode, String errorMessage) {
        logger.entry();
        AuthenticationResponse errorResponse = new AuthenticationResponse();
        errorResponse.setData(null);
        errorResponse.setSuccess(false);
        ArrayList<String> errors = new ArrayList<>();
        logger.error(errorMessage);
        errors.add(errorMessage);
        errorResponse.setErrors(errors);
        logger.exit();
        return errorResponse;
    }

    private static void setServiceSecurity() {
        logger.entry();
        logger.trace("Setting security for service");
        if (isSSLCertificateConfigured()) {
            try {
                String certificateKey = loadSSLCertificatePassword();
                if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
                    Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
                    String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
                    secure(cognitiveCertificateFilePath, certificateKey, null, null);
                } else {
                    logger.trace("Certificate not found.");
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        logger.exit();
    }

    private static String loadSSLCertificatePassword() throws IOException {
        logger.entry();
        Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitivePropertiesFilePath = propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
        logger.debug("Configuration filepath: " + cognitivePropertiesFilePath);
        if (fileExists(cognitivePropertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitivePropertiesFilePath));
            String certificateKey = properties.getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
            logger.exit();
            return certificateKey;
        } else {
            logger.traceEntry("Cognitive properties file not found.");
            logger.exit();
            return PROPERTIES_NOTFOUND;
        }
    }

    private static Object getHeartBeatInfo(Response response) {
        logger.entry();
        String heartBeat;
        //currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization
        response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
        heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
        response.body(heartBeat);
        logger.exit();
        return response;
    }

    private static boolean isSSLCertificateConfigured() {
        logger.exit();
        boolean hasCertificate = false;
        Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
        logger.debug("Certification file path: " + cognitiveCertificateFilePath);
        if (fileExists(cognitiveCertificateFilePath)) {
            hasCertificate = true;
        } else {
            logger.trace("certificate file not found");
        }
        logger.exit();
        return hasCertificate;
    }

    private static boolean fileExists(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }

    private static void preRequestProcessor(Request request) {
        captureStartTimeAttribute(request);
        addTransactionIdentifier(request);
        logger.debug("Request URI.." + request.uri());
    }

    private static void addTransactionIdentifier(Request request) {
        ThreadContext.put(CORRELATION_ID, request.headers(CORRELATION_ID) != null ?
                request.headers(CORRELATION_ID) : UUID.randomUUID().toString());
    }

    private static StandardResponse buildAuthenticationResponse(final String username, final ControlRoomAuthorizationResponse authorizationResponse, final String sessionId) throws UnsupportedEncodingException {
        String roles = String.join(",", authorizationResponse.getRoles());
        String header = new String(Base64.getUrlEncoder().encode(
                "{\"alg\":\"HS256\",\"typ\":\"JWT\"}".getBytes("UTF-8")));
        String payload = new String(Base64.getUrlEncoder().encode(
                ("{\"roles\":\"" + roles + "\", \"" + JWTDetailsParser.SESSION_ID_PROP_NAME + "\":\"" + sessionId + "\"}").getBytes("UTF-8")));
        String toSign = header + "." + payload;
        HmacGenerator hmacGenerator = new HmacGenerator();
        byte[] sign = hmacGenerator.create(strPassword, toSign);
        byte[] sign64 = Base64.getUrlEncoder().encode(sign);
        String signature = new String(sign64, "UTF-8");
        String token = header + "." + payload + "." + signature;
        AuthenticationResponseDataUser userData = new AuthenticationResponseDataUser();
        userData.setName(username.toString());
        userData.setRole(authorizationResponse.getRoles());

        AuthenticationResponseData data = new AuthenticationResponseData();
        data.setToken(token);
        data.setUser(userData);
        StandardResponse response = buildStandardResponse(true, null, data);
        logger.exit();
        return response;
    }

    private static StandardResponse buildStandardResponse(boolean isSuccess, String errorMessage, Object data) {
        StandardResponse authResponse = new StandardResponse();
        authResponse.setSuccess(isSuccess);
        List<String> errors = new ArrayList<>();
        errors.add(errorMessage);
        authResponse.setErrors(errors);
        authResponse.data(data);
        return authResponse;
    }
}
