package com.automationanywhere.cognitive.auth.model;


import java.util.ArrayList;
import java.util.List;

public class ControlRoomAuthorizationResponse implements AuthenticationResponse {
    private List<AAKeyValuePair> roles;
    private List<String> licenseFeatures;

    public List<String> getRoles() {
        List<String> rolesList = new ArrayList<>();
        roles.forEach(role -> {
            rolesList.add(role.getValue());
        });
        return rolesList;
    }

    public void setRoles(List<AAKeyValuePair> roles) {
        this.roles = roles;
    }

    public List<String> getLicenseFeatures() {
        return licenseFeatures;
    }

    public void setLicenseFeatures(List<String> licesneFeatures) {
        this.licenseFeatures = licesneFeatures;
    }
}
