package com.automationanywhere.cognitive.auth;

public class HmacFactoryException extends RuntimeException {

  private static final long serialVersionUID = -7298458922792300749L;
	
  public HmacFactoryException() {
    super();
  }

  public HmacFactoryException(final String message) {
    super(message);
  }

  public HmacFactoryException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public HmacFactoryException(final Throwable cause) {
    super(cause);
  }
}
