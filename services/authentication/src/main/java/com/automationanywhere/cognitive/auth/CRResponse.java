package com.automationanywhere.cognitive.auth;

import java.util.List;
import java.util.Map;

public class CRResponse {

    private String responseBody;
    private Map<String,List<String>> responseHeaders;

    public CRResponse(String responseBody, Map<String,List<String>> responseHeaders){
        this.responseBody = responseBody;
        this.responseHeaders = responseHeaders;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }
}
