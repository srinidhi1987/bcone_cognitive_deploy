package com.automationanywhere.cognitive.auth.health.connections;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.automationanywhere.cognitive.auth.custom.exceptions.CRConnectionFailureException;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CRCheckConnection implements HealthCheckConnection{

    private RestTemplateWrapper restTemplateWrapper;
    private String crGetStatusURL = "http://<crhost>:<crport>/webcrsvc/config/getstatus/";
    
    AALogger aaLogger = AALogger.create(this.getClass());
    
    public CRCheckConnection(RestTemplateWrapper restTemplateWrapper) {
        this.restTemplateWrapper  = restTemplateWrapper;
    }
    
    @Override
    public void checkConnectionToCR(String crUrl, int crPort) {

        aaLogger.entry();
        crGetStatusURL = crGetStatusURL.replace("<crhost>", crUrl);
        crGetStatusURL = crGetStatusURL.replace("<crport>", "" + crPort);
        try {
            // make the getStatus RestCall to Control Room
            ResponseEntity<?> crResponseEntity = restTemplateWrapper.exchangeGet(crGetStatusURL, String.class);

            if (crResponseEntity == null || !crResponseEntity.getStatusCode().equals(HttpStatus.OK))
                throw new CRConnectionFailureException("Control room response returns null or Not OK  ");
            else {
                // parse response
                CRGetStatusResponse crResponse2 = parseCRResponseBody((String) crResponseEntity.getBody());
                if(!crResponse2.isSuccess()){
                    throw new CRConnectionFailureException("Control room response returns isSuccess=false  " + crResponseEntity.getBody());
                }
            }
        } catch (Exception e) {
            throw new CRConnectionFailureException("Exception occurred while connecting to Control Room ", e);
        }
        aaLogger.exit();
    }
    
    private CRGetStatusResponse parseCRResponseBody(String crResponse) throws IOException{
        aaLogger.entry();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        CRGetStatusResponse crGetStatusResponse = null;
        try {
            crGetStatusResponse = objectMapper.readValue(crResponse, CRGetStatusResponse.class);
        } catch (IOException e) {
            throw e;
        }
        aaLogger.exit();
        return crGetStatusResponse;
    }

}
