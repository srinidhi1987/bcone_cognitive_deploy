package com.automationanywhere.cognitive.auth.model;

import java.util.List;

public class UserControlRoomAuthorizationResponse extends ControlRoomAuthorizationResponse {
  public final String userId;

  public UserControlRoomAuthorizationResponse(
      final String userId,
      final List<String> licenseFeatures,
      final List<AAKeyValuePair> roles
  ) {
    this.userId = userId;

    setLicenseFeatures(licenseFeatures);
    setRoles(roles);
  }
}
