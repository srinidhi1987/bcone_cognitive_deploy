package com.automationanywhere.cognitive.auth.session;

import java.util.HashMap;
import java.util.Map;

/**
 * Session object contains session parameters like its id and session end-time;
 * also may contain data shared between requests within the same session
 * (this is, so named, session context).
 *
 * The class is mutable, it contains a session context which can modified.
 * (But data stored in the session context may be immutable)
 *
 * The class is not thread safe, if a session context can be modified from different threads,
 * callers are responsible for synchronizing the data modification.
 */
public class Session {
  public final String id;

  // Id of the entity who started the session.
  // As for now we do not support id abstraction and assume id is always a string
  // which is true in case with Control Room.
  public final String entityId;

  // startTime is when the session started.
  public final long startTime;

  // lastAccessTime is when the session was accessed the last time.
  public final long lastAccessedTime;

  // Session context is a data shared between requests executed within the same session.
  private final Map<String, Object> ctx = new HashMap<>();

  public Session(
      final String id,
      final String entityId,
      final long startTime,
      final long lastAccessedTime
  ) {
    if (id == null) {
      throw new IllegalArgumentException("Session should have an id");
    }

    this.id = id;
    this.entityId = entityId;
    this.startTime = startTime;
    this.lastAccessedTime = lastAccessedTime;
  }

  public Session(
      final Session session,
      final long lastAccessedTime
  ) {
    this.id = session.id;
    this.entityId = session.entityId;
    this.startTime = session.startTime;
    this.ctx.putAll(session.ctx);
    this.lastAccessedTime = lastAccessedTime;
  }

  @SuppressWarnings("unchecked")
  public <T> T get(final String key) {
    return (T) ctx.get(key);
  }

  public <T> void put(final String key, final T value) {
    ctx.put(key, value);
  }

  public void remove(final String key) {
    ctx.remove(key);
  }
}
