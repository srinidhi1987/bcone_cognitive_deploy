/**
 * Copyright (c) 2016 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.auth.exception.impl;

import static spark.Spark.exception;

import com.automationanywhere.cognitive.auth.CustomResponse;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import spark.Response;

/**
 * This filter handles exceptions that has been thrown from resource/service layer.
 * It maps java exceptions to the corresponding HTTP status/response.
 */
public class ExceptionFilter  {
    private ObjectMapper mapper;	
    private AALogger logger = AALogger.create(this.getClass());


    public void init() {
        exception(Exception.class, (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;

    }

    private void handleException(Exception e, Response res, HttpStatusCode statusCode) {
        logger.entry();
        String errorMessage = Optional.ofNullable(e.getMessage()).orElse("").trim().isEmpty()
            ? statusCode.getMessage() : e.getMessage();
        logger.error(e.getMessage());
        res.status(statusCode.getCode());
        CustomResponse customResponse=new CustomResponse();
        customResponse.setSuccess(false);
        String customResonseInFormOfJson="";
        try {
            customResponse.setError(errorMessage);
            customResonseInFormOfJson=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);
            logger.trace("Exception Handler Response :" + customResonseInFormOfJson, e);
        }
        catch (Exception ex)
        {
            logger.error("Exception Occurred :" + errorMessage, ex);
        }
        res.body(customResonseInFormOfJson);
        logger.exit();
    }
}
