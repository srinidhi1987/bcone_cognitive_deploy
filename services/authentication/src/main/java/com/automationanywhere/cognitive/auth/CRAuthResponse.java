package com.automationanywhere.cognitive.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Nakuldev.Patel on 19-04-2017.
 */
public class CRAuthResponse {
    @JsonProperty("$id")
    public final String id;
    @JsonProperty("m_Item1")
    public final String status;
    @JsonProperty("m_Item2")
    public final String clientKey;

    public CRAuthResponse() {
        id = null;
        status = null;
        clientKey = null;
    }

    public CRAuthResponse(
        @JsonProperty("$id") final String id,
        @JsonProperty("m_Item1") final String status,
        @JsonProperty("m_Item2") final String clientKey
    ) {
        this.id = id;
        this.status = status;
        this.clientKey = clientKey;
    }
}
