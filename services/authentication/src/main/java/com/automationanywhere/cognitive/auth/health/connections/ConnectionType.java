package com.automationanywhere.cognitive.auth.health.connections;
/**
 * @author shweta.thakur
 *
 * This class represents Connection Type available for a subsystem 
 * DB:  If subsystem connects to DB
 * SVC: If subsystem connects to other microservices
 * MQ: If subsystem connects to rabbit MQ
 */
public enum ConnectionType {
	CR
}
