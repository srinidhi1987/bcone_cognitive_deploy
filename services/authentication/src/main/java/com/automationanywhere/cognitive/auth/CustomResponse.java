/**
 * Created by Nakuldev.Patel on 14-04-2017.
 */
package com.automationanywhere.cognitive.auth;

import java.util.HashMap;
import java.util.Map;

public class CustomResponse {

    private boolean success;
    private Object data;
    private String errors;
    private int httpStatusCode;

    public int HttpStatusCode(){
        return httpStatusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getErrors() {
        return errors;
    }

    public void setError(String error) {
        this.errors = error;
    }

    public void setHttpStatusCode(int httpStatusCode){
        this.httpStatusCode = httpStatusCode;
    }

    public Map<String, Comparable> toMap() {
        Map<String, Comparable> hashMap = new HashMap<String, Comparable>();

        hashMap.put("success",this.success);

        if(!this.success) {
            hashMap.put("error", this.errors);
            hashMap.put("data", null);
        } else {
            hashMap.put("data", this.data.toString());
            hashMap.put("error", null);
        }
        return hashMap;
    }

}
