package com.automationanywhere.cognitive.auth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains details extracted from a JW token.
 */
class JWTDetails {
    public final String sessionId;
    public final List<String> roles;

    public JWTDetails(final String sessionId, final List<String> roles) {
        this.sessionId = sessionId;
        this.roles = roles == null
            ? Collections.emptyList()
            : Collections.unmodifiableList(new ArrayList<>(roles));
    }
}
