package com.automationanywhere.cognitive.auth.service;

import com.automationanywhere.cognitive.auth.exception.impl.AuthenticationException;
import com.automationanywhere.cognitive.auth.exception.impl.HttpStatusCode;
import com.automationanywhere.cognitive.auth.exception.impl.InvalidArgumentException;
import com.automationanywhere.cognitive.auth.model.AAKeyValuePair;
import com.automationanywhere.cognitive.auth.model.AuthenticationRequest;
import com.automationanywhere.cognitive.auth.model.AuthenticationResponse;
import com.automationanywhere.cognitive.auth.model.ControlRoomAuthenticationResponse;
import com.automationanywhere.cognitive.auth.model.ControlRoomAuthorizationResponse;
import com.automationanywhere.cognitive.auth.model.UserControlRoomAuthorizationResponse;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;

import static com.automationanywhere.cognitive.auth.constants.MessageConstants.*;

public class ControlRoomAuthenticationService implements AuthenticationService {

    private static final int PAYLOAD_INDEX = 1;
    private static final int SIGNATURE_INDEX = 2;
    private static final int LAST_INDEX = SIGNATURE_INDEX;

    private static AALogger logger = AALogger.create(AuthenticationService.class);
    private ObjectMapper objectMapper = new ObjectMapper();
    private static String baseURL;
    private static boolean sslConfigured = false;
    private static String routeAuthentication = "controlroomapi/v1/authenticate/";
    private static String routeAuthorization = "controlroomapi/v1/user/rolesandlicensetype";
    private static final String USER_PROP_NAME = "user";

    private enum HTTPMETHOD {GET, POST, PUT, PATCH, DELETE}

    private static String[] whitelistRoles = new String[]{"IQBotValidator", "IQBotServices", "Admin"};
    private static String[] validLicenseFeatures = new String[]{"Development", "IQBotRuntime"};


    public ControlRoomAuthenticationService(String hostname, String port, boolean isSSL) {
        sslConfigured = isSSL;
        baseURL = buildControlRoomURL(hostname, port, isSSL);
    }

    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) throws AuthenticationException, IOException {
        return authenticateAndGetUserMetadata(authenticationRequest);
    }

    private UserControlRoomAuthorizationResponse authenticateAndGetUserMetadata(AuthenticationRequest authenticationRequest) throws InvalidArgumentException, AuthenticationException, IOException {

        validateAuthenticationRequest(authenticationRequest);
        ControlRoomAuthenticationResponse authenticationResponse = authenticateWithControlRoom(authenticationRequest);
        ControlRoomAuthorizationResponse authorizationResponse = authorizeWithControlRoom(authenticationResponse);
        validateRolesAndLicense(authorizationResponse);
        return createUserControlRoomAuthorizationResponse(
            authorizationResponse.getLicenseFeatures(),
            authorizationResponse.getRoles(),
            authenticationResponse.getToken()
        );
    }

    UserControlRoomAuthorizationResponse createUserControlRoomAuthorizationResponse(
        final List<String> licenseFeatures, final List<String> roles, final String token
    ) throws IOException {
        String[] sections = token.split("\\.");
        if (0 + sections.length - 1 != LAST_INDEX) {
            throw new IllegalArgumentException("Invalid format of the token");
        }

        byte[] payload = Base64.getDecoder().decode(sections[PAYLOAD_INDEX]);
        JsonNode node = objectMapper.readTree(payload).get(USER_PROP_NAME);
        if (node == null) {
            throw new IllegalArgumentException("No user id found in the token");
        }
        if (!node.isTextual()) { // CR returns a string.
            throw new IllegalArgumentException("No valid user id found in the token");
        }

        return new UserControlRoomAuthorizationResponse(
            node.asText(),
            licenseFeatures,
            roles.stream().map(r -> {
                AAKeyValuePair kv = new AAKeyValuePair();
                // No need to set a key here, it's not used and is not exposed by ControlRoomAuthorizationResponse
                kv.setValue(r);
                return kv;
            }).collect(Collectors.toList())
        );
    }

    private String buildControlRoomURL(String hostname, String port, boolean isSSL) {
        String crBaseUrl = "<ssl>://<hostname>:<port>/";
        if (isSSL)
            crBaseUrl = crBaseUrl.replace("<ssl>", "https");
        else
            crBaseUrl = crBaseUrl.replace("<ssl>", "http");

        crBaseUrl = crBaseUrl.replace("<hostname>", hostname);
        crBaseUrl = crBaseUrl.replace("<port>", port);
        return crBaseUrl;
    }

    private void validateAuthenticationRequest(AuthenticationRequest authenticationRequest) throws InvalidArgumentException {
        if (authenticationRequest == null ||
                authenticationRequest.getUsername().isEmpty() ||
                authenticationRequest.getPassword().isEmpty())
            throw new InvalidArgumentException(MESSAGE_INVALID_AUTHARGUMENTS);
    }

    private ControlRoomAuthenticationResponse authenticateWithControlRoom(AuthenticationRequest authenticationRequest) throws AuthenticationException, IOException {
        ControlRoomAuthenticationResponse crResponse;
        String response = "";
        try {
            String jsonRequest = objectMapper.writeValueAsString(authenticationRequest);
            String authURL = String.format("%s%s", baseURL, routeAuthentication);
            logger.debug(String.format("authentication url: %s",authURL ));
            response = restClient(authURL, jsonRequest, HTTPMETHOD.POST.toString(), null);
            crResponse = objectMapper.readValue(response.toString(), ControlRoomAuthenticationResponse.class);
        } catch (AuthenticationException aex) {
            if (aex.getStatusCode() == HttpStatusCode.UNAUTHORIZED.getCode())
                aex.setMessage(MESSAGE_INVALID_CREDENTIALS);
            else if (aex.getStatusCode() == HttpStatusCode.FORBIDDEN.getCode())
                aex.setMessage(MESSAGE_USERACCOUNT_DEACTIVATED);
            throw aex;
        }
        return crResponse;
    }

    private ControlRoomAuthorizationResponse authorizeWithControlRoom(ControlRoomAuthenticationResponse authenticationRequest) throws IOException {
        String response = "";
        String authzURL = String.format("%s%s", baseURL, routeAuthorization);
        logger.debug(String.format("authorization url: %s",authzURL ));
        response = restClient(authzURL, null, HTTPMETHOD.GET.toString(), authenticationRequest.getToken());
        return objectMapper.readValue(response.toString(), ControlRoomAuthorizationResponse.class);

    }

    private void validateRolesAndLicense(ControlRoomAuthorizationResponse authorizationResponse) throws AuthenticationException {
        if (Collections.disjoint(Arrays.asList(whitelistRoles), authorizationResponse.getRoles())) {
            throw new AuthenticationException(HttpStatusCode.FORBIDDEN.getCode(), MESSAGE_INVALID_ROLE);
        } else if (Collections.disjoint(Arrays.asList(validLicenseFeatures), authorizationResponse.getLicenseFeatures())) {
            throw new AuthenticationException(HttpStatusCode.FORBIDDEN.getCode(), MESSAGE_INVALID_LICESNE);
        }
    }

    private String restClient(String url, String params, String httpMethod, String token) throws AuthenticationException, IOException {

        HttpURLConnection conn = null;
        if (sslConfigured)
            conn = (HttpsURLConnection) new URL(url).openConnection();
        else
            conn = (HttpURLConnection) new URL(url).openConnection();
        try {
            conn.setDoOutput(true);
            conn.setRequestMethod(httpMethod.toString());
            conn.setRequestProperty("Content-Type", "application/json");
            if (params != null) {
                OutputStream os = conn.getOutputStream();
                os.write(params.getBytes());
                os.flush();
            }
            if (token != null) {
                conn.setRequestProperty("X-Authorization", token);
            }

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new AuthenticationException(conn.getResponseCode(), getStringFromStream(conn.getErrorStream()));
            }

            return getStringFromStream(conn.getInputStream());
        } catch (AuthenticationException cex) {
            throw cex;
        } catch (Exception ex) {
            throw new AuthenticationException(HttpStatusCode.INTERNAL_SERVER_ERROR.getCode(), ex.getMessage());
        } finally {
            if (conn != null)
                conn.disconnect();
        }
    }

    private String getStringFromStream(InputStream stream) throws IOException {
        StringBuilder content = new StringBuilder();

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String output;
            while ((output = bufferedReader.readLine()) != null) {
                content.append(output);
            }
            bufferedReader.close();
        } catch (Exception ex) {
            logger.error("Error reading response from controlroom: " + ex.getStackTrace());
        }
        return content.toString();
    }
}
