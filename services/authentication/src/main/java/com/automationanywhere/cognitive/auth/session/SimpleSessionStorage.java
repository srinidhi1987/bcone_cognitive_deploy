package com.automationanywhere.cognitive.auth.session;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * The storage is not thread safe.
 */
public class SimpleSessionStorage implements SessionStorage {
  private final Map<String, Session> sessions = new HashMap<>();

  public void save(final Session session) {
    sessions.put(session.id, session);
  }

  public void remove(final String sessionId) {
    sessions.remove(sessionId);
  }

  public void remove(final Function<Session, Boolean> filter) {
    sessions.entrySet().removeIf(e -> filter.apply(e.getValue()));
  }

  public Session find(final String id) {
    return sessions.get(id);
  }
}
