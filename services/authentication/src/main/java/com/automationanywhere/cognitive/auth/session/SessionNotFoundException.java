package com.automationanywhere.cognitive.auth.session;

/**
 * The exception is thrown when a session cannot be found.
 */
public class SessionNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -1871353134586738966L;

  public SessionNotFoundException() {
  }

  public SessionNotFoundException(final String message) {
    super(message);
  }

  public SessionNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public SessionNotFoundException(final Throwable cause) {
    super(cause);
  }
}
