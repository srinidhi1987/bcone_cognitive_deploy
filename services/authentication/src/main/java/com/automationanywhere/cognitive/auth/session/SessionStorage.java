package com.automationanywhere.cognitive.auth.session;

import java.util.function.Function;

/**
 * Session storage is responsible for storing session objects.
 * It's up to the implementation to support or not
 * the persistence of sessions or to make them durable between restarts or not.
 */
public interface SessionStorage {
  /**
   * Stores a session in the storage.
   *
   * @param session - session to store.
   */
  void save(final Session session);

  /**
   * Removes a session from the storage.
   *
   * @param sessionId - id of the session to remove
   */
  void remove(final String sessionId);

  /**
   * Removes sessions from the storage.
   *
   * @param filter - criteria of sessions to remove.
   */
  void remove(final Function<Session, Boolean> filter);

  /**
   * Finds a session by its id.
   *
   * @param id - id of the session to find.
   * @return found session or null.
   */
  Session find(final String id);
}
