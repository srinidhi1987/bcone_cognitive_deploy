package com.automationanywhere.cognitive.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Str {
    private static final Map<Character, Character> ESCAPED = new HashMap<>();

    static {
        ESCAPED.put('/', '/');
        ESCAPED.put('\\', '\\');
        ESCAPED.put('b', '\b');
        ESCAPED.put('f', '\f');
        ESCAPED.put('n', '\n');
        ESCAPED.put('r', '\r');
        ESCAPED.put('t', '\t');
    }

    String str = "";
    private Set<Character> quoteSet = new HashSet<>();
    int shift = 0;
    private boolean isTerminatorEaten = false;

    Character first() {
        if(shift == str.length())
            return null;
        return str.charAt(shift);
    }

    Character get(int i) {
        return (str.length() > shift + i) ? str.charAt(shift + i) : null;
    }

    void skip(int n) {
        shift += n;
    }

    void skipOne() {
        shift++;
    }

    String head(int i) {
        return str.substring(shift, shift + i);
    }

    private static boolean isWhitespace(Character c) {
        return c == ' ' || c == '\t' || c == '\n' || c == '\r';
    }

    void skipWhitespaces() {
        for (Character f = first(); shift != str.length() && isWhitespace(f);
             f = first())
            ++shift;
    }

    boolean startsWith(String st) {
        return (st.length() <= str.length() - shift) &&
                (str.substring(shift, shift + st.length()).equals(st));
    }

    Str(String _str, String quoteStr, boolean _isTerminatorEaten) {
        isTerminatorEaten = _isTerminatorEaten;
        str = _str;
        for(Character c : quoteStr.toCharArray())
            quoteSet.add(c);
    }

    Str(Str inStr, String quoteStr, boolean _isTerminatorEaten) {
        isTerminatorEaten = _isTerminatorEaten;
        for(Character c : quoteStr.toCharArray())
            quoteSet.add(c);
        inStr.skipOne();
        outerLoop:
        while (true) {
            Character c = inStr.first();
            if(c == null)
                break;
            if (quoteSet.contains(c)) {
                if(isTerminatorEaten)
                    inStr.skipOne();
                return;
            }
            inStr.skipOne();
            if(c == '\\') {
                c = inStr.first();
                if (c == null)
                    break;
                inStr.skipOne();
                if (quoteSet.contains(c))
                    str += c;
                else if (ESCAPED.keySet().contains(c))
                    str += ESCAPED.get(c);
                else if (c == 'u') {
                    int n = 0;
                    for (int i = 0; i < 4; i++) {
                        c = inStr.get(i);
                        if (c == null)
                            break outerLoop;
                        if (c >= '0' && c <= '9')
                            n = n << 4 + (c - '0');
                        else if (c >= 'a' && c <= 'f')
                            n = n << 4 + (c - 'a');
                        else if (c >= 'A' && c <= 'F')
                            n = n << 4 + (c - 'A');
                        else
                            break outerLoop;
                    }
                    str += (new String(Character.toChars(n)));
                    inStr.skip(4);
                } else
                    break;
            } else
                str += c;
        }
        str = null;
    }
}
