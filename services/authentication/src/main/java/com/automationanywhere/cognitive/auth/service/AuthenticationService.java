package com.automationanywhere.cognitive.auth.service;

import com.automationanywhere.cognitive.auth.exception.impl.AuthenticationException;
import com.automationanywhere.cognitive.auth.model.AuthenticationRequest;
import com.automationanywhere.cognitive.auth.model.AuthenticationResponse;

import java.io.IOException;

public interface AuthenticationService {

    AuthenticationResponse authenticate (AuthenticationRequest authenticationRequest) throws AuthenticationException, IOException;
}
