package com.automationanywhere.cognitive.auth.model;

public class ControlRoomAuthenticationResponse implements AuthenticationResponse{
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
