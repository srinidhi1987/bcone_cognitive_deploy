package com.automationanywhere.cognitive.auth.health.handlers.impl;

import com.automationanywhere.cognitive.auth.custom.exceptions.CRConnectionFailureException;
import com.automationanywhere.cognitive.auth.health.connections.ConnectionType;
import com.automationanywhere.cognitive.auth.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.auth.health.connections.HealthCheckConnectionFactory;
import com.automationanywhere.cognitive.common.health.handlers.AbstractHealthHandler;
import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;

public class AuthenticationHealthCheckHandler implements AbstractHealthHandler {
    
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    AALogger aaLogger = AALogger.create(this.getClass());
    
    public AuthenticationHealthCheckHandler(HealthCheckConnectionFactory healthCheckConnectionFactory) {
        this.healthCheckConnectionFactory = healthCheckConnectionFactory;
    }
    
    @Override
    public String checkHealth() {
        return "";
    }
    
    //New implementation for CR as Authentication service does not use Spring
    public String checkCRHealth(String crHostName, int crPort) {
        aaLogger.entry();
        HealthCheckConnection healthCheckConnection;
        Connectivity crConnectivity = Connectivity.OK;
        for (ConnectionType connectionType : ConnectionType.values()) {
            healthCheckConnection = healthCheckConnectionFactory.getConnection(connectionType);
            if (connectionType.equals(ConnectionType.CR)) {
                try {
                    healthCheckConnection.checkConnectionToCR(crHostName, crPort);
                } catch (CRConnectionFailureException cfe) {
                    aaLogger.error("Error connecting to control room" ,cfe);
                    crConnectivity = Connectivity.FAILURE;
                }
            }
        }
        return HealthApiResponseBuilder.prepareSubSystem(HTTPStatusCode.OK,
                SystemStatus.ONLINE, Connectivity.NOT_APPLICABLE, Connectivity.NOT_APPLICABLE,crConnectivity,
                null);
    }
}
    


