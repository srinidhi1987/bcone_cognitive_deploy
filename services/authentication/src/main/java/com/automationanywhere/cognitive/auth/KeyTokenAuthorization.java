package com.automationanywhere.cognitive.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KeyTokenAuthorization {

  @JsonProperty("clientkey")
  public final String clientKey;

  @JsonProperty("clienttoken")
  public final String clientToken;

  public KeyTokenAuthorization(
      @JsonProperty("clientkey") final String clientKey,
      @JsonProperty("clienttoken") final String clientToken
  ) {
    this.clientKey = clientKey;
    this.clientToken = clientToken;
  }
}
