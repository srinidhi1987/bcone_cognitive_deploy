package com.automationanywhere.cognitive.auth.health.connections;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CRGetStatusResponse {
    @JsonProperty("$id")
    private String id;
    @JsonProperty("IsSuccess")
    private boolean isSuccess;
    @JsonProperty("ResultCode")
    private int resultCode;
    @JsonProperty("ResultMessage")
    private String resultMessage;
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the isSuccess
     */
    public boolean isSuccess() {
        return isSuccess;
    }
    /**
     * @param isSuccess the isSuccess to set
     */
    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
    /**
     * @return the resultCode
     */
    public int getResultCode() {
        return resultCode;
    }
    /**
     * @param resultCode the resultCode to set
     */
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
    /**
     * @return the resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }
    /**
     * @param resultMessage the resultMessage to set
     */
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

}
