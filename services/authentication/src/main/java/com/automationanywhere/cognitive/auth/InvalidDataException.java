package com.automationanywhere.cognitive.auth;

public class InvalidDataException extends RuntimeException {

  private static final long serialVersionUID = 3878464557926432150L;
  
  public InvalidDataException(final String message) {
    super(message);
  }

  public InvalidDataException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
