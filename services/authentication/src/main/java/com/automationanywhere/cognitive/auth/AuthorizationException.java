package com.automationanywhere.cognitive.auth;

/**
 * The exception is thrown when the access to a resource is not granted.
 */
public class AuthorizationException extends RuntimeException {

  private static final long serialVersionUID = 5415641204469237072L;	
  
  public AuthorizationException() {
  }

  public AuthorizationException(final String message) {
    super(message);
  }

  public AuthorizationException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public AuthorizationException(final Throwable cause) {
    super(cause);
  }
}
