package com.automationanywhere.cognitive.auth;

public class JWTDetailsParserException extends RuntimeException {

  private static final long serialVersionUID = -2692509938650668691L;

  public JWTDetailsParserException(final String message) {
    super(message);
  }

  public JWTDetailsParserException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
