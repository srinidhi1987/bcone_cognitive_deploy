package com.automationanywhere.cognitive.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenAuthorization {
  public final String jwt;
  public final String action;

  public TokenAuthorization(
      @JsonProperty("jwt") final String jwt,
      @JsonProperty("action") final String action
  ) {
    this.jwt = jwt;
    this.action = action;
  }
}
