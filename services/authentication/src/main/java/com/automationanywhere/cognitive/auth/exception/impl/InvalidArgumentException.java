package com.automationanywhere.cognitive.auth.exception.impl;

public class InvalidArgumentException extends RuntimeException {

    private static final long serialVersionUID = 7461139382524724366L;
    private String message;

    public InvalidArgumentException(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
