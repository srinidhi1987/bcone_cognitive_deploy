package com.automationanywhere.cognitive.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.automationanywhere.cognitive.common.logger.AALogger;

public class Json {
	private static AALogger aaLogger = AALogger.create(Json.class);
    public static String toString(Object obj) {
    	aaLogger.traceEntry();
        StringBuilder sb = new StringBuilder();
        if(obj == null)
            sb.append("null");
        else if (obj instanceof Map) {
            sb.append('{');
            Map o = (Map) obj;
            boolean isNotFirst = false;
            for (Object k : o.keySet()) {
                if (isNotFirst)
                    sb.append(',');
                isNotFirst = true;
                sb.append(toString(k)).append(':').append(toString(o.get(k)));
            }
            sb.append('}');
        } else if (obj instanceof List) {
            sb.append('[');
            List list = (List) obj;
            boolean isNotFirst = false;
            for (Object o : list) {
                if (isNotFirst)
                    sb.append(',');
                isNotFirst = true;
                sb.append(toString(o));
            }
            sb.append(']');
        } else if (obj instanceof String) {
            String s = (String)obj;
            sb.append('"').append(s.replaceAll("\\\\", "\\\\\\\\")
                    .replaceAll("\"", "\\\"")).append('"');
        } else if (obj instanceof Number) {
            sb.append(obj);
        } else if (obj instanceof Boolean) {
            sb.append(obj);
        }
        aaLogger.exit();
        return sb.toString();
    }

    public static Pair parse(String input) {
    	aaLogger.traceEntry();
        if (input == null)
            return null;
        Str str = new Str(input, "\"", true);
        str.skipWhitespaces();
        Pair result = new Pair();
        if(str.shift == str.str.length())
            result = new Pair(false, result);
        else if (str.first() == '{') {
            JsonObj jsonObj = new JsonObj(str);
            str.skipWhitespaces();
            result = new Pair(!(jsonObj.hash == null) &&
                    (str.shift == str.str.length()), result);
            if((boolean)result.left)
                result = new Pair(result, jsonObj.hash);
        } else if (str.first() == '[') {
            JsonArr jsonArr = new JsonArr(str);
            str.skipWhitespaces();
            result = new Pair(!(jsonArr.list == null) &&
                    (str.shift == str.str.length()), result);
            if((boolean)result.left)
                result = new Pair(result, jsonArr.list);
        } else {
            result = parseJson(str);
            result = new Pair((boolean)result.left &&
                    (str.shift == str.str.length()), result);
        }
        aaLogger.exit();
        return result;
    }

    private static class JsonObj {
        Map<String, Object> hash = new HashMap<>();

        JsonObj(Str str) {
            boolean bFirst = true;
            str.skipOne();
            do {
                if(str.first() == null)
                    break;
                str.skipWhitespaces();
                if(str.first() == null)
                    break;
                if (str.first() == '}') {
                    if(!bFirst)
                        break;
                    str.skipOne();
                    return;
                }
                if (str.first() != '"')
                    break;
                Str key = new Str(str, "\"", true);
                if(key.str == null)
                    break;
                str.skipWhitespaces();
                if(str.first() == null)
                    break;
                if (str.first() != ':')
                    break;
                str.skipOne();
                str.skipWhitespaces();
                if(str.first() == null)
                    break;
                Pair pair = parseJson(str);
                if (!(boolean)pair.left)
                    break;
                hash.put(key.str, pair.right);
                str.skipWhitespaces();
                if(str.first() == null)
                    break;
                if (str.first() == ',') {
                    bFirst = false;
                    str.skipOne();
                }
                else if (str.first() != '}')
                    break;
                else {
                    str.skipOne();
                    return;
                }
            } while (true);
            hash = null; // to allow deletion of created content
        }
    }

    private static class JsonArr {
        List<Object> list = new ArrayList<>();

        void add(Object val) {
            list.add(val);
        }

        JsonArr(Str str) {
            boolean bFirst = true;
            str.skipOne();
            do {
                str.skipWhitespaces();
                if(str.first() == null)
                    break;
                if (str.first() == ']') {
                    if(!bFirst)
                        break;
                    str.skipOne();
                    return;
                }
                Pair pair = parseJson(str);
                if (!(boolean)pair.left)
                    break;
                add(pair.right);
                str.skipWhitespaces();
                if(str.first() == null)
                    break;
                if (str.first() == ',') {
                    bFirst = false;
                    str.skipOne();
                }
                else if (str.first() != ']')
                    break;
                else {
                    str.skipOne();
                    return;
                }
            } while (true);
            list = null;
        }
    }

    private static Pair parseJson(Str s) {
    	aaLogger.entry();
		Object obj = null;
        Pair pair = new Pair(false, obj);
        if (s.first() == '{') {
            JsonObj jsonObj = new JsonObj(s);
            pair = new Pair(jsonObj.hash != null, pair);
            if ((boolean) pair.left)
                pair = new Pair(pair, jsonObj.hash);
        } else if (s.first() == '[') {
            JsonArr jsonArr = new JsonArr(s);
            pair = new Pair(jsonArr.list != null, pair);
            if ((boolean) pair.left)
                pair = new Pair(pair, jsonArr.list);
        } else if (s.first() == '"') {
            Str str = new Str(s, "\"", true);
            pair = new Pair(str.str != null, pair);
            if ((boolean) pair.left)
                pair = new Pair(pair, str.str);
        } else if (s.first() == '-' ||
                (s.first() >= '0' && s.first() <= '9')) {
            Number n = parseNumber(s);
            pair = new Pair(
                n != null, n
            );
        } else if (s.startsWith("true")) {
            s.skip(4);
            pair = new Pair(true, true);
        } else if (s.startsWith("false")) {
            s.skip(5);
            pair = new Pair(true, false);
        } else if (s.startsWith("null")) {
            s.skip(4);
            pair = new Pair(true, null);
        }
        aaLogger.exit();
        return pair;
    }

    private static Number parseNumber(Str s) {
    	aaLogger.traceEntry();
        Number num;
        int i = 0;
        if(s.get(i) == null)
            return null;
        if (s.get(i) == '-')
            i++;
        if(s.get(i) == null)
            return null;
        if (s.get(i) == '0') {
            i++;
            if (s.get(i) == null)
                return 0;
        }
        else if (s.get(i) >= '1' && s.get(i) <= '9') {
            i++;
            while (s.get(i) != null && s.get(i) >= '0' && s.get(i) <= '9')
                i++;
            if(s.get(i) == null) {
                num = Integer.parseInt(s.head(i));
                s.skip(i);
                return num;
            }
        } else
            return null;
        boolean isFloatingPoint = false;
        if (s.get(i) == '.') {
            isFloatingPoint = true;
            i++;
            if (s.get(i) >= '0' && s.get(i) <= '9') {
                while (s.get(i) != null && s.get(i) >= '0' && s.get(i) <= '9')
                    i++;
            }
            else
                return null;
            if(s.get(i) == null) {
                num = Double.parseDouble(s.head(i));
                s.skip(i);
                return num;
            }
        }
        if (s.get(i) == 'e' || s.get(i) == 'E') {
            isFloatingPoint = true;
            i++;
            if(s.get(i) == null)
                return null;
            if (s.get(i) == '+' || s.get(i) == '-')
                i++;
            if(s.get(i) == null)
                return null;
            if (s.get(i) < '0' || s.get(i) > '9')
                return null;
            while (s.get(i) != null && s.get(i) >= '0' && s.get(i) <= '9')
                i++;
            if(s.get(i) == null)
                return Double.parseDouble(s.head(i));
        }
        if (isFloatingPoint)
            num = Double.parseDouble(s.head(i));
        else
            num = Integer.parseInt(s.head(i));
        s.skip(i);
        aaLogger.exit();
        return num;
    }
}
