package com.automationanywhere.cognitive.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 * Extracts user roles and a session id from a token.
 */
public class JWTDetailsParser {
  public static final String SESSION_ID_PROP_NAME = "sessionId";

  private static final String ALG_PROP_NAME = "alg";
  private static final String ROLES_PROP_NAME = "roles";

  // This is the index of the metadata part of the JW token: header.payload.signature
  private static final int HEADER_INDEX = 0;
  // This is the index of the JW token part which contains token details/token body: header.payload.signature
  private static final int PAYLOAD_INDEX = 1;
  // This is the index of the JW token part which contains the signature of the token itself: header.payload.signature
  private static final int SIGNATURE_INDEX = 2;
  private static final int LAST_INDEX = SIGNATURE_INDEX;

  private final HmacGenerator hmacGenerator = new HmacGenerator();
  private final AlgManager algManager = new AlgManager();
  private final ObjectMapper om = new ObjectMapper();

  public JWTDetails parse(final String jwt, final String password) throws Exception {
    if (jwt == null) {
      throw new JWTDetailsParserException("JWT not found");
    }

    String[] jwtParts = jwt.split("\\.");
    if (0 + jwtParts.length - 1 != LAST_INDEX) {
      throw new JWTDetailsParserException("JWT is corrupted");
    }

    Map<String, Object> header = getMap(jwtParts[HEADER_INDEX]);

    validateAlgorithm(header);
    validateSignature(password, jwtParts);

    Map<String, Object> payload = getMap(jwtParts[PAYLOAD_INDEX]);

    Object roles = validateRoles(payload);
    Object sessionId = validateSessionId(payload);

    return new JWTDetails(
        (String) sessionId, createRoles((String) roles)
    );
  }

  private void validateAlgorithm(final Map<String, Object> header) {
    if (!algManager.getJwtAlgName().equals(header.get(ALG_PROP_NAME))) {
      throw new JWTDetailsParserException("only " + algManager.getJwtAlgName() + " is supported for now");
    }
  }

  private void validateSignature(final String password, final String[] jwtParts) {
    byte[] signature = hmacGenerator.create(password, jwtParts[HEADER_INDEX] + "." + jwtParts[PAYLOAD_INDEX]);
    byte[] receivedSignature = decode(jwtParts[SIGNATURE_INDEX]);
    if (!Arrays.equals(signature, receivedSignature)) {
      throw new JWTDetailsParserException("permission check failed");
    }
  }

  private Object validateRoles(final Map<String, Object> payload) {
    Object roles = payload.get(ROLES_PROP_NAME);
    if (!(roles instanceof String)) {
      throw new JWTDetailsParserException("JWT payload roles should be a string");
    }
    return roles;
  }

  private Object validateSessionId(final Map<String, Object> payload) throws Exception {
    Object sessionId = payload.get(SESSION_ID_PROP_NAME);
    if (sessionId != null) {
      if (!(sessionId instanceof String)) {
        throw new JWTDetailsParserException("JWT payload sessionId should be a string");
      }
    }
    return sessionId;
  }

  private List<String> createRoles(final String roles) {
    List<String> multiRoles = new ArrayList<>();
    if (roles.contains(",")) {
      String[] arrayRoles = roles.split(",");
      multiRoles.addAll(Arrays.asList(arrayRoles));
    } else {
      multiRoles.add(roles);
    }
    return multiRoles;
  }

  @SuppressWarnings("unchecked")
  private Map<String, Object> getMap(final String part) {
    try {
      return om.readValue(new String(decode(part)), Map.class);
    } catch (final IOException ex) {
      throw new JWTDetailsParserException("JWT section is broken", ex);
    }
  }

  private byte[] decode(final String text) {
    byte[] bytes = null;

    try {
      bytes = Base64.getUrlDecoder().decode(
          text.getBytes(StandardCharsets.UTF_8)
      );
    } catch (final IllegalArgumentException ex) {
      throw new JWTDetailsParserException("String is not Base64 encoded", ex);
    }
    return bytes;
  }
}
