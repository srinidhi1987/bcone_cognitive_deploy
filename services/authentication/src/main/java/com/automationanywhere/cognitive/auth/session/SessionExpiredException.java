package com.automationanywhere.cognitive.auth.session;

/**
 * The exception is thrown when an expired session is detected.
 */
public class SessionExpiredException extends RuntimeException {

  private static final long serialVersionUID = -6141431561921980979L;

  public SessionExpiredException() {
  }

  public SessionExpiredException(final String message) {
    super(message);
  }

  public SessionExpiredException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public SessionExpiredException(final Throwable cause) {
    super(cause);
  }
}
