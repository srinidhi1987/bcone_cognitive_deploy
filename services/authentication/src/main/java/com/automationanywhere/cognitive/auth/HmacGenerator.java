package com.automationanywhere.cognitive.auth;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;

/**
 * Produces hashes of data using an arbitrary MAC algorithm.
 */
public class HmacGenerator {
  private final AlgManager algManager = new AlgManager();

  /**
   * Produces hashes of data using an arbitrary MAC algorithm.
   *
   * @param password - a secrete password needed for the algorithm to generate a hash.
   * @param data - data to hash.
   * @return a hash of the data.
   */
  public byte[] create(final String password, final String data) {
    byte hash[] = null;
    try {
      Mac mac = algManager.createMac();
      mac.init(algManager.createKey(password));
      hash = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
    } catch (final NoSuchAlgorithmException | InvalidKeyException ex) {
      throw new HmacFactoryException("Signing failed", ex);
    }
    return hash;
  }
}
