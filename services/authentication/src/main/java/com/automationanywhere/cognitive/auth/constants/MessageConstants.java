package com.automationanywhere.cognitive.auth.constants;

public class MessageConstants {

    public static final String MESSAGE_LICENSE_EXPIRED = "No License allocated or license is expired. Please contact Administrator.";
    public static final String MESSAGE_INVALID_LICESNE = "You do not have valid IQ Bot License. Please contact Administrator.";
    public static final String MESSAGE_INVALID_ROLE = "You do not have a valid Role. Please contact Administrator.";
    public static final String MESSAGE_INVALID_CREDENTIALS = "Invalid Username or Password.";
    public static final String MESSAGE_INVALID_AUTHARGUMENTS = "Username or Password cannot be empty.";
    public static final String MESSAGE_USERACCOUNT_DEACTIVATED = "Your account is not activated. Please contact the Administrator.";
}
