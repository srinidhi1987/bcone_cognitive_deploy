package com.automationanywhere.cognitive.auth.session;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.util.UUID;
import org.testng.annotations.Test;

public class SessionServiceTest {
  @Test
  public void openSessionShouldCreateInfiniteSessions() {
    // given
    SessionService ss = spy(new SessionService(null, null));

    doReturn(1L).when(ss).getNow();

    // when
    Session s = ss.createSession(null);

    // then
    assertThat(s.startTime).isEqualTo(1L);
    assertThat(s.lastAccessedTime).isEqualTo(1L);
  }

  @Test
  public void openSessionShouldCreateFiniteSessions() {
    // given
    SessionService ss = spy(new SessionService(1000L, 500L));

    doReturn(1L).when(ss).getNow();

    // when
    Session s = ss.createSession(null);

    // then
    assertThat(s.startTime).isEqualTo(1L);
    assertThat(s.lastAccessedTime).isGreaterThanOrEqualTo(1L);
  }

  @Test
  public void findShouldFindActualSessions() {
    // given
    SessionService ss = spy(new SessionService(1000L, 1000L));

    doReturn(1L).when(ss).getNow();

    Session s = ss.createSession(null);

    // when
    Session r = ss.findSession(s.id);

    // then
    assertThat(r).isSameAs(r);
  }

  @Test
  public void findShouldNotFindNotExistingSessions() {
    // given
    SessionService ss = spy(new SessionService(null, null));

    doReturn(0L).when(ss).getNow();

    // when
    Throwable th = catchThrowable(() -> ss.findSession(UUID.randomUUID().toString()));

    // then
    assertThat(th).isInstanceOf(SessionNotFoundException.class);
  }

  @Test
  public void findShouldNotFindExpiredSessions() throws Exception {
    // given
    SessionService ss = spy(new SessionService(1L, null));
    doReturn(0L).doReturn(1L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    Throwable th = catchThrowable(() -> ss.findSession(s.id));

    // then
    assertThat(th).isInstanceOf(SessionExpiredException.class);
  }

  @Test
  public void findShouldNotFindInactiveSessions() throws Exception {
    // given
    SessionService ss = spy(new SessionService(null, 1L));
    doReturn(0L).doReturn(1L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    Throwable th = catchThrowable(() -> ss.findSession(s.id));

    // then
    assertThat(th).isInstanceOf(SessionExpiredException.class);
  }

  @Test
  public void resetSessionShouldNotFindNotExistingSessions() {
    // given
    SessionService ss = spy(new SessionService(null, null));

    doReturn(1L).when(ss).getNow();

    // when
    Throwable th = catchThrowable(() -> ss.resetSession(UUID.randomUUID().toString()));

    // then
    assertThat(th).isInstanceOf(SessionNotFoundException.class);
  }

  @Test
  public void resetSessionShouldNotFindExpiredSessions() throws Exception {
    // given
    SessionService ss = spy(new SessionService(1L, null));
    doReturn(0L).doReturn(1L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    Throwable th = catchThrowable(() -> ss.resetSession(s.id));

    // then
    assertThat(th).isInstanceOf(SessionExpiredException.class);
  }

  @Test
  public void resetSessionShouldNotFindInactiveSessions() throws Exception {
    // given
    SessionService ss = spy(new SessionService(null, 1L));
    doReturn(0L).doReturn(1L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    Throwable th = catchThrowable(() -> ss.resetSession(s.id));

    // then
    assertThat(th).isInstanceOf(SessionExpiredException.class);
  }

  @Test
  public void resetSessionShouldResetSession() throws Exception {
    // given
    SessionService ss = spy(new SessionService(null, 500L));
    doReturn(0L).doReturn(1L).doReturn(2L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    ss.resetSession(s.id);
    Session r = ss.findSession(s.id);

    // then
    assertThat(r.lastAccessedTime).isEqualTo(2L);
  }

  @Test
  public void destroySessionShouldNotFindNotExistingSessions() {
    // given
    SessionService ss = spy(new SessionService(null, null));

    doReturn(1L).when(ss).getNow();

    // when
    Throwable th = catchThrowable(() -> ss.invalidateSessions(UUID.randomUUID().toString(), null));

    // then
    assertThat(th).isInstanceOf(SessionNotFoundException.class);
  }

  @Test
  public void destroySessionShouldNotFindExpiredSessions() throws Exception {
    // given
    SessionService ss = spy(new SessionService(1L, null));
    doReturn(0L).doReturn(1L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    Throwable th = catchThrowable(() -> ss.invalidateSessions(s.id, null));

    // then
    assertThat(th).isInstanceOf(SessionExpiredException.class);
  }

  @Test
  public void destroySessionShouldNotFindInactiveSessions() throws Exception {
    // given
    SessionService ss = spy(new SessionService(null, 1L));
    doReturn(0L).doReturn(1L).when(ss).getNow();
    Session s = ss.createSession(null);

    // when
    Throwable th = catchThrowable(() -> ss.invalidateSessions(s.id, null));

    // then
    assertThat(th).isInstanceOf(SessionExpiredException.class);
  }

  @Test
  public void destroySessionShouldDestroyOtherSessions() throws Exception {
    // given
    String uIdA = "a";
    String uIdB = "b";

    SessionService ss = spy(new SessionService(1000L, 500L));

    doReturn(1L).when(ss).getNow();

    Session s1 = ss.createSession(uIdA);
    Session s2 = ss.createSession(uIdA);
    Session s3 = ss.createSession(uIdA);
    Session s4 = ss.createSession(uIdB);

    // when
    ss.invalidateSessions(
        s2.id,
        ud -> !ud.session.id.equals(ud.currentSession.id)
            && ud.session.entityId.equals(uIdA)
    );

    // then
    assertThat(ss.findSession(s2.id)).isSameAs(s2);
    assertThat(ss.findSession(s4.id)).isSameAs(s4);

    assertThat(catchThrowable(() -> ss.findSession(s1.id))).isInstanceOf(SessionNotFoundException.class);
    assertThat(catchThrowable(() -> ss.findSession(s3.id))).isInstanceOf(SessionNotFoundException.class);
  }
}
