package com.automationanywhere.cognitive.auth;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.automationanywhere.cognitive.auth.model.AAKeyValuePair;
import com.automationanywhere.cognitive.auth.model.UserControlRoomAuthorizationResponse;
import com.automationanywhere.cognitive.auth.session.Session;
import com.automationanywhere.cognitive.auth.session.SessionNotFoundException;
import com.automationanywhere.cognitive.auth.session.SessionService;
import java.util.Arrays;
import java.util.Collections;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AuthServerTest {

  private SessionService ss;

  @BeforeMethod
  public void setUp() throws Exception {
    ss = new SessionService(null, null);
    AuthServer.setSessionService(ss);
  }

  @Test
  public void testCreateSession() {
    // given
    AAKeyValuePair r = new AAKeyValuePair();
    r.setValue("role");

    UserControlRoomAuthorizationResponse resp = new UserControlRoomAuthorizationResponse(
        "1",
        Collections.singletonList("feature"),
        Collections.singletonList(r)
    );

    Session s1 = ss.createSession("1");
    Session s2 = ss.createSession("1");
    Session s3 = ss.createSession("2");

    // when
    Session s = AuthServer.createSession(resp);

    // then
    assertThat(Arrays.asList(s1.id, s2.id, s3.id)).doesNotContain(s.id);
    assertThat(ss.findSession(s3.id)).isSameAs(s3);
    assertThat(ss.findSession(s.id)).isSameAs(s);

    assertThatThrownBy(() -> ss.findSession(s1.id)).isInstanceOf(SessionNotFoundException.class);
    assertThatThrownBy(() -> ss.findSession(s2.id)).isInstanceOf(SessionNotFoundException.class);
  }
}
