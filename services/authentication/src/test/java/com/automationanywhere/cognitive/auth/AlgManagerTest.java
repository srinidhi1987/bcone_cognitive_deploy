package com.automationanywhere.cognitive.auth;

import static org.assertj.core.api.Assertions.assertThat;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AlgManagerTest {
  private AlgManager m;

  @BeforeMethod
  public void setUp() throws Exception {
    m = new AlgManager();
  }

  @Test
  public void getJwtAlgNameShouldReturnAValidAlg() {
    // given

    // when
    String algName = m.getJwtAlgName();

    // then
    assertThat(algName).isEqualTo("HS256");
  }

  @Test
  public void createKeyShouldCreateAnEmptyKeyWithAValidAlg() {
    // given

    // when
    SecretKeySpec k = m.createKey("pass");

    // then
    assertThat(k.getAlgorithm()).isEqualTo("HmacSHA256");
  }

  @Test
  public void createMacShouldCreateAValidAlg() throws Exception {
    // given

    // when
    Mac mac = m.createMac();

    // then
    assertThat(mac.getAlgorithm()).isEqualTo("HmacSHA256");
  }
}
