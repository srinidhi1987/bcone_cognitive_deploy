package com.automationanywhere.cognitive.auth.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.automationanywhere.cognitive.auth.model.UserControlRoomAuthorizationResponse;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ControlRoomAuthenticationServiceTest {
  private ControlRoomAuthenticationService s;

  @BeforeMethod
  public void setUp() throws Exception {
    s = new ControlRoomAuthenticationService("localhost", "8080", false);
  }

  @Test
  public void createUserControlRoomAuthorizationResponseShouldCreateAResponse() throws Exception {
    // given
    String token = "a." + Base64.getUrlEncoder().encodeToString("{\"user\": \"1\"}".getBytes("UTF-8")) + ".c";
    List<String> roles = Collections.singletonList("role");
    List<String> features = Collections.singletonList("feature");

    // when
    UserControlRoomAuthorizationResponse resp = s.createUserControlRoomAuthorizationResponse(
        features,
        roles,
        token
    );

    // then
    assertThat(resp.userId).isEqualTo("1");
    assertThat(resp.getLicenseFeatures()).containsExactlyInAnyOrder("feature");
    assertThat(resp.getRoles()).containsExactlyInAnyOrder("role");
  }

  @Test
  public void createUserControlRoomAuthorizationResponseShouldThrowAnErrorIfThereIsNoId() throws Exception {
    // given
    String token = "a." + Base64.getUrlEncoder().encodeToString("{}".getBytes("UTF-8")) + ".c";
    List<String> roles = Collections.singletonList("role");
    List<String> features = Collections.singletonList("feature");

    // when
    Throwable th = catchThrowable(() -> s.createUserControlRoomAuthorizationResponse(
        features,
        roles,
        token
    ));

    // then
    assertThat(th).isInstanceOf(IllegalArgumentException.class).hasMessage("No user id found in the token");
  }

  @Test
  public void createUserControlRoomAuthorizationResponseShouldThrowAnErrorIfIdIsNotString() throws Exception {
    // given
    String token = "a." + Base64.getUrlEncoder().encodeToString("{\"user\": 1}".getBytes("UTF-8")) + ".c";
    List<String> roles = Collections.singletonList("role");
    List<String> features = Collections.singletonList("feature");

    // when
    Throwable th = catchThrowable(() -> s.createUserControlRoomAuthorizationResponse(
        features,
        roles,
        token
    ));

    // then
    assertThat(th).isInstanceOf(IllegalArgumentException.class).hasMessage("No valid user id found in the token");
  }

  @Test
  public void createUserControlRoomAuthorizationResponseShouldThrowAnErrorIfTokenHasInvalidFormat() throws Exception {
    // given
    String token = "a.c";
    List<String> roles = Collections.singletonList("role");
    List<String> features = Collections.singletonList("feature");

    // when
    Throwable th = catchThrowable(() -> s.createUserControlRoomAuthorizationResponse(
        features,
        roles,
        token
    ));

    // then
    assertThat(th).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid format of the token");
  }
}
