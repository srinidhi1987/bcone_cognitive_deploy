package com.automationanywhere.cognitive.auth.health.connections;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.auth.custom.exceptions.CRConnectionFailureException;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;

public class CRCheckConnectionTest {

    @Mock
    RestTemplateWrapper restTemplateWrapper;
    @Mock
    RestTemplate restTemplate;;
    @InjectMocks
    CRCheckConnection crCheckConnection;
    
  
    @BeforeTest
    public void setUp(){
        this.restTemplate = new RestTemplate();
        this.restTemplateWrapper = new RestTemplateWrapper(restTemplate);
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfcheckConnectionToCRWhenGetStatusReturnsSuccessTrue(){
        
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.when(standardResponseResponseEntity.getBody()).thenReturn("{ \"$id\": \"1\", \"IsSuccess\": true, \"ResultCode\": 0,\"ResultMessage\": \"\"}");
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        CRCheckConnection crCheckConnection = new CRCheckConnection(restTemplateWrapper);
        crCheckConnection.checkConnectionToCR(Mockito.anyString(), Mockito.anyInt());
        
        //then
        //no exception
    }
    
    @Test(expectedExceptions={CRConnectionFailureException.class},expectedExceptionsMessageRegExp="Exception occurred while connecting to Control Room ")
    public void testOfcheckConnectionToCRWhenGetStatusReturnsSuccessFalse(){
        
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.when(standardResponseResponseEntity.getBody()).thenReturn("{ \"$id\": \"1\", \"IsSuccess\": false, \"ResultCode\": 0,\"ResultMessage\": \"\"}");
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        CRCheckConnection crCheckConnection = new CRCheckConnection(restTemplateWrapper);
        crCheckConnection.checkConnectionToCR(Mockito.anyString(), Mockito.anyInt());
        
        //then
        //throw exception
    }
    
    @Test(expectedExceptions={CRConnectionFailureException.class}, expectedExceptionsMessageRegExp="Exception occurred while connecting to Control Room ")
    public void testOfcheckConnectionToCRWhenGetStatusRequestFails(){
        
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        CRCheckConnection crCheckConnection = new CRCheckConnection(restTemplateWrapper);
        crCheckConnection.checkConnectionToCR(Mockito.anyString(), Mockito.anyInt());
        
        //then
        //throw exception
    }
    
    @Test(expectedExceptions={CRConnectionFailureException.class}, expectedExceptionsMessageRegExp="Exception occurred while connecting to Control Room ")
    public void testOfcheckConnectionToCRWhenGetStatusRequestReturnsNull(){
        
        //given
        Mockito.doReturn(null).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        CRCheckConnection crCheckConnection = new CRCheckConnection(restTemplateWrapper);
        crCheckConnection.checkConnectionToCR(Mockito.anyString(), Mockito.anyInt());
        
        //then
        //throw exception
    }
}
