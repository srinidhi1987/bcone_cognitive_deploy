package com.automationanywhere.cognitive.auth;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HmacGeneratorTest {
  private HmacGenerator f;

  @BeforeMethod
  public void setUp() throws Exception {
    f = new HmacGenerator();
  }

  @Test
  public void hmacFactoryShouldCreateTheSameHashes() {
    // given

    // when
    byte[] h1 = f.create("password", "data");
    byte[] h2 = f.create("password", "data");

    // then
    assertThat(Arrays.equals(h1, h2)).isTrue();
  }

  @Test
  public void hmacFactoryShouldCreateDifferentHashes() {
    // given

    // when
    byte[] h1 = f.create("password", "data");
    byte[] h2 = f.create("drowssap", "data");

    // then
    assertThat(Arrays.equals(h1, h2)).isFalse();
  }
}
