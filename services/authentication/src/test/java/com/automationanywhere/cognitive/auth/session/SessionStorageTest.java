package com.automationanywhere.cognitive.auth.session;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SessionStorageTest {
  private SessionStorage ss;

  @BeforeMethod
  public void setUp() throws Exception {
    ss = new SimpleSessionStorage();
  }

  @Test
  public void storeShouldStoreSessions() {
    // given
    Session s = new Session("1", null, 1L, 0L);

    // when
    ss.save(s);

    // then
    assertThat(ss.find("1")).isSameAs(s);
    assertThat(ss.find("2")).isNull();
  }

  @Test
  public void removeShouldRemoveASession() {
    // given
    Session s1 = new Session("1", null, 1L, 0L);
    Session s2 = new Session("2", null, 1L, 0L);
    ss.save(s1);
    ss.save(s2);

    // when
    ss.remove("1");

    // then
    assertThat(ss.find("1")).isNull();
    assertThat(ss.find("2")).isSameAs(s2);
  }

  @Test
  public void removeShouldRemoveSessions() {
    // given
    Session s1 = new Session("1", null, 1L, 0L);
    Session s2 = new Session("2", null, 1L, 0L);
    Session s3 = new Session("3", null, 1L, 0L);
    ss.save(s1);
    ss.save(s2);
    ss.save(s3);

    // when
    ss.remove(s -> !s.id.equals("2"));

    // then
    assertThat(ss.find("1")).isNull();
    assertThat(ss.find("3")).isNull();
    assertThat(ss.find("2")).isSameAs(s2);
  }
}
