package com.automationanywhere.cognitive.auth;

import org.testng.annotations.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomResponseTest {

    @Test
    public void testOfSuccessCustomResponseShouldYieldProperMap() throws Exception {
        // given
        CustomResponse response = new CustomResponse();
        int statusCode = 200;
        boolean success = true;
        String errorMessage = "no error";
        String data = "some message";

        // when
        response.setHttpStatusCode(statusCode);
        response.setSuccess(success);
        response.setError(errorMessage);
        response.setData(data);
        Map<String, Comparable> map = response.toMap();

        // then
        assertThat(response).isNotNull();
        assertThat(response.HttpStatusCode()).isEqualTo(statusCode);
        assertThat(response.isSuccess()).isEqualTo(success);
        assertThat(response.getErrors()).isEqualTo(errorMessage);
        assertThat(response.getData()).isEqualTo(data);

        assertThat(map).isNotNull();
        assertThat(map).containsKeys("success", "error", "data");
        boolean actualValue = (boolean) map.get("success");
        assertThat(actualValue).isEqualTo(success);
        String actualError = (String) map.get("error");
        assertThat(actualError).isNull();
        Object actualData = map.get("data");
        assertThat(actualData).isEqualTo(data);
    }

    @Test
    public void testOfErrorCustomResponseShouldYieldProperMap() throws Exception {
        // given
        CustomResponse response = new CustomResponse();
        int statusCode = 200;
        boolean success = false;
        String errorMessage = "some error";
        String data = "no message";

        // when
        response.setHttpStatusCode(statusCode);
        response.setSuccess(success);
        response.setError(errorMessage);
        response.setData(data);
        Map<String, Comparable> map = response.toMap();

        // then
        assertThat(response).isNotNull();
        assertThat(response.HttpStatusCode()).isEqualTo(statusCode);
        assertThat(response.isSuccess()).isEqualTo(success);
        assertThat(response.getErrors()).isEqualTo(errorMessage);
        assertThat(response.getData()).isEqualTo(data);

        assertThat(map).isNotNull();
        assertThat(map).containsKeys("success", "error", "data");
        assertThat((boolean) map.get("success")).isEqualTo(success);
        assertThat((String) map.get("error")).isEqualTo(errorMessage);
        assertThat((String) map.get("data")).isNull();
    }
}
