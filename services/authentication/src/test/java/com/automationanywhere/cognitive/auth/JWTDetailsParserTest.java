package com.automationanywhere.cognitive.auth;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Base64;
import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class JWTDetailsParserTest {
  private JWTDetailsParser p;

  @BeforeMethod
  public void setUp() throws Exception {
     p = new JWTDetailsParser();
  }

  @Test
  public void parseShouldParseAToken() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"roles\":\"role\",\"sessionId\":\"0\"}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    JWTDetails tk = p.parse(token, "password");

    // then
    assertThat(tk.roles).containsExactlyInAnyOrder("role");
    assertThat(tk.sessionId).isEqualTo("0");
  }

  @Test
  public void parseShouldParseATokenWithoutSessionId() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"roles\":\"role\"}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    JWTDetails tk = p.parse(token, "password");

    // then
    assertThat(tk.roles).containsExactlyInAnyOrder("role");
    assertThat(tk.sessionId).isNull();
  }

  @Test
  public void parseShouldParseATokenWithEmptyRoles() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"roles\":\"\"}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    JWTDetails tk = p.parse(token, "password");

    // then
    assertThat(tk.roles).containsExactly("");
    assertThat(tk.sessionId).isNull();
  }

  @Test
  public void parseShouldParseATokenWithManyRoles() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"roles\":\"a,b\"}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    JWTDetails tk = p.parse(token, "password");

    // then
    assertThat(tk.roles).containsExactlyInAnyOrder("b", "a");
    assertThat(tk.sessionId).isNull();
  }

  @Test
  public void parseShouldThrowIfTokenIsNull() {
    // given
    String token = null;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT not found");
  }

  @Test
  public void parseShouldThrowIfTokenHasNot3Sections() {
    // given
    String token = "abc";

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT is corrupted");
  }

  @Test
  public void parseShouldThrowIfAlgorithmIsInvalid() throws Exception {
    // given
    String token = Base64.getEncoder().encodeToString("{\"alg\":\"a\"}".getBytes("UTF-8")) + ".b.c";

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("only HS256 is supported for now");
  }

  @Test
  public void parseShouldThrowIfHeaderIsEmpty() throws Exception {
    // given
    String token = Base64.getEncoder().encodeToString("".getBytes("UTF-8")) + ".b.c";

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT section is broken");
  }

  @Test
  public void parseShouldThrowIfHeaderIsNotAContainerNode() throws Exception {
    // given
    String token = Base64.getEncoder().encodeToString("123".getBytes("UTF-8")) + ".b.c";

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT section is broken");
  }

  @Test
  public void parseShouldThrowIfPayloadIsEmpty() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT section is broken");
  }

  @Test
  public void parseShouldThrowIfPayloadIsNotAContainerNode() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("123".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT section is broken");
  }

  @Test
  public void parseShouldThrowIfSignatureIsInvalid() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("123".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "drowssap"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("permission check failed");
  }

  @Test
  public void parseShouldThrowIfRolesIsNotAString() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"roles\":1,\"sessionId\":\"0\"}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT payload roles should be a string");
  }

  @Test
  public void parseShouldThrowIfRolesAreMissing() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"sessionId\":\"role\"}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT payload roles should be a string");
  }

  @Test
  public void parseShouldThrowIfSessionIdIsNotAString() throws Exception {
    // given
    String header = Base64.getUrlEncoder().encodeToString("{\"alg\":\"HS256\"}".getBytes("UTF-8"));
    String body = Base64.getUrlEncoder().encodeToString("{\"roles\":\"\",\"sessionId\":1}".getBytes("UTF-8"));
    String sign = Base64.getUrlEncoder().encodeToString(new HmacGenerator().create("password", header + "." + body));
    String token = header + "." + body + "." + sign;

    // when
    Throwable t = Assertions.catchThrowable(() -> p.parse(token, "password"));

    // then
    assertThat(t).isInstanceOf(JWTDetailsParserException.class).hasMessage("JWT payload sessionId should be a string");
  }
}
