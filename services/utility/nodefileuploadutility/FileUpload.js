const fs = require('fs');
const exec = require('child_process').exec;

module.exports = {
curlUpload(command,cb) {
	const child = exec(command, (error, stdout, stderr) => {
	  if (error) {
		  console.log(error);
		throw error;
	  }
	  console.log(stdout);
	  if(cb)
	  cb(null, files);
	});
},
upload(orgId, host,dirpath,projectId, files, cb) {
	if (Array.isArray(files)){
		files.map((file) => {
			const command = 'curl -v '+ 
			'-F data=@"'+ dirpath + ""+ file +'" '
			+`http://${host}:9996/organizations/1/projects/${projectId}/files/upload/1`;
			this.curlUpload(command,null);
		});
	}
	else{
		const command = 'curl -v '+ 
			'-F data=@"'+ files.file +'" '
			+`http://${host}:9996/organizations/1/projects/${projectid}/files/upload/1`;
			this.curlUpload(command,null);
	}
	if(cb)
	cb(null, files);
},
}