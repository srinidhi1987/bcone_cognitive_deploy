@echo off
SETLOCAL
cd /d %~dp0
if NOT DEFINED JAVA_HOME goto error
set RUN_JAVA=%JAVA_HOME%\bin\java
set mypath=%cd%
pushd %mypath%

nssm.exe install "Automation Anywhere Cognitive Visionbot Manager" "%RUN_JAVA%" "-Dfile.encoding=UTF-8 -jar visionbotmanager-1.0-SNAPSHOT.jar 192.168.2.59 1433 Filemanager_Demo sa Automation@123 7998"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Visionbot Manager"

@rem java -Dfile.encoding=UTF-8 -jar visionbotmanager-1.0-SNAPSHOT.jar 192.168.2.59 1433 Filemanager_Demo sa Automation@123 7998

nssm.exe install "Automation Anywhere Cognitive Projects" "%RUN_JAVA%" "-jar project-1.0-SNAPSHOT.jar 192.168.2.59 1433 Project_Demo sa Automation@123 7999"
nssm.exe set "Automation Anywhere Cognitive Projects" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Projects" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Projects" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Projects"


@rem java -jar project-1.0-SNAPSHOT.jar 192.168.2.59 1433 Project_Demo sa Automation@123 7999

nssm.exe install "Automation Anywhere Cognitive File Manager" "%RUN_JAVA%" "-jar filemanager-1.0-SNAPSHOT.jar 192.168.2.59 1433 FileManager_Demo sa Automation@123 localhost 7999 7996"
nssm.exe set "Automation Anywhere Cognitive File Manager" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive File Manager" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive File Manager" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive File Manager"

@rem java -jar filemanager-1.0-SNAPSHOT.jar 192.168.2.59 1433 FileManager_Demo sa Automation@123 localhost 7999 7996
nssm.exe install "Automation Anywhere Cognitive Alias" "%RUN_JAVA%" "-jar alias-1.0-SNAPSHOT.jar 192.168.2.59 1433 AliasData sa Automation@123 7997"
nssm.exe set "Automation Anywhere Cognitive Alias" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Alias" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Alias" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Alias"

@rem java -jar alias-1.0-SNAPSHOT.jar 192.168.2.59 1433 AliasData sa Automation@123 7997
nssm.exe install "Automation Anywhere Cognitive Validator" "%RUN_JAVA%" "-jar validator-1.0.jar 192.168.2.59 1433 FileManager_Demo sa Automation@123 7995"
nssm.exe set "Automation Anywhere Cognitive Validator" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Validator" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Validator" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Validator"

@rem java -Dfile.encoding=UTF-8 -jar validator-1.0.jar 192.168.2.59 1433 FileManager_Demo sa Automation@123 7995

nssm.exe install "Automation Anywhere Cognitive Packer" "%RUN_JAVA%" "-jar packer-1.0-SNAPSHOT.jar packer-config.properties"
nssm.exe set "Automation Anywhere Cognitive Packer" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Packer" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Packer" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Packer"

@rem java -jar packer-1.0-SNAPSHOT.jar packer-config.properties

nssm.exe install "Automation Anywhere Cognitive Report" "%RUN_JAVA%" "-jar reports-1.0-SNAPSHOT.jar 192.168.2.59 1433 FileManager_Demo sa Automation@123 7992"
nssm.exe set "Automation Anywhere Cognitive Report" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Report" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Report" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Report"

@rem java -jar reports-1.0-SNAPSHOT.jar 192.168.2.59 1433 FileManager_Demo sa Automation@123 7992

nssm.exe install "Automation Anywhere Cognitive Gateway" "%RUN_JAVA%" "-Dfile.encoding=UTF-8 -jar  gateway-1.0-SNAPSHOT.jar 7100 localhost 7200 7100 aliasservice.json localhost 7997 7100 fileservice.json localhost 7996 7100 projectservice.json localhost 7999 7100 visionbotservice.json localhost 7998 7100 validatorservice.json localhost 7995 7100 reportservice.json localhost 7992 7100 packerservice.json localhost 7993"
nssm.exe set "Automation Anywhere Cognitive Gateway" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Gateway" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Gateway" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Gateway"

@rem java -Dfile.encoding=UTF-8 -jar  gateway-1.0-SNAPSHOT.jar 7100 localhost 7200 7100 aliasservice.json localhost 7997 7100 fileservice.json localhost 7996 7100 projectservice.json localhost 7999 7100 visionbotservice.json localhost 7998 7100 validatorservice.json localhost 7995 7100 reportservice.json localhost 7992 7100 packerservice.json localhost 7993

nssm.exe install "Automation Anywhere Cognitive Authentication" "%RUN_JAVA%" "-Dfile.encoding=UTF-8 -jar authentication-1.0-SNAPSHOT.jar password 7200 192.168.2.59 8081 action_roles.json"
nssm.exe set "Automation Anywhere Cognitive Authentication" AppDirectory "%mypath%"
nssm.exe set "Automation Anywhere Cognitive Authentication" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Authentication" ImagePath """%mypath%\nssm.exe"""
timeout 2
nssm start "Automation Anywhere Cognitive Authentication"

@rem java -Dfile.encoding=UTF-8 -jar authentication-1.0-SNAPSHOT.jar password 7200 192.168.2.59 8081 action_roles.json

