﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace Automation.Cognitive.SSLUtility
{

    class SSLOptions
    {
        [Option('i', "installationpath", Required = true, HelpText = "Path where IQbot is installed.")]
        public string IQBotInstallationPath { get; set; }

        [Option('c', "certificatepath", Required = true, HelpText = @"Path where pfx located on disk. e.g.: C:\SSLUtil\Cert.crt")]
        public string CertificatePath { get; set; }

        [Option('k', "certificatekey", Required = true, HelpText = @"Path where SSL certificate key located on disk. e.g.: C:\SSLUtil\Decrypted-Key.key")]
        public string CertificateKeyPath { get; set; }

        [Option('a', "cacertificatepath", Required = true, HelpText = @"Path where CA certificate located on disk. e.g.: C:\SSLUtil\ca.crt")]
        public string CACertificatePath { get; set; }

        [Option('p', "keystorepassword", Required = true, HelpText = @"Password of java keystore.")]
        public string Password { get; set; }

        [Option('j', "javakeystorepath", Required = true, HelpText = @"Java Key Store. e.g.: C:\SSLUtil\CognitiveConfiguration.jks")]
        public string JavaKeyStorePath { get; set; }
    }
}
