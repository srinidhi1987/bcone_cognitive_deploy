﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.SSLUtility
{
    public class ServiceConfiguration
    {
        public string Name { get; set; }
        public string ServiceUri { get; set; }
        public int MaximumBufferSize { get; set; }
        public int DefaultConnectionLimit { get; set; }
    }

    public class RabbitMQSettings
    {
        public string VirtualHost { get; set; }
        public string Uri { get; set; }
        public string UserName { get; set; }
    }

    public class ClassifierMQSettings
    {
        public string FileUploadedMQName { get; set; }
    }

    public class ClassifierDBSettings
    {
        public string DbConnectionString { get; set; }
    }

    public class MLDBSettings
    {
        public string DbConnectionString { get; set; }
    }

    public class MLWebService
    {
        public string port { get; set; }
    }

    public class LoggerSettings
    {
        public string level { get; set; }
    }

    public class SSLSettings
    {
        public bool RunWithSSL { get; set; }
        public string SSL_CERT_FilePath { get; set; }
        public string SSL_CERT_Key { get; set; }
    }

    public class MLConfig
    {
        public List<ServiceConfiguration> ServiceConfiguration { get; set; }
        public RabbitMQSettings RabbitMQSettings { get; set; }
        public ClassifierMQSettings ClassifierMQSettings { get; set; }
        public ClassifierDBSettings ClassifierDBSettings { get; set; }
        public MLDBSettings MLDBSettings { get; set; }
        public MLWebService MLWebService { get; set; }
        public LoggerSettings LoggerSettings { get; set; }
        public SSLSettings SSLSettings { get; set; }
    }

    public class CognitiveConfiguration
    {
        public List<ServiceConfiguration> ServiceConfiguration { get; set; }
        public RabbitMQSettings RabbitMQSettings { get; set; }
        public ClassifierMQSettings ClassifierMQSettings { get; set; }
        public ClassifierDBSettings ClassifierDBSettings { get; set; }
        public MLDBSettings MLDBSettings { get; set; }
        public MLWebService MLWebService { get; set; }
        public LoggerSettings LoggerSettings { get; set; }
    }
}
