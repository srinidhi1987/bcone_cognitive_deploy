﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using Automation.CR.Web.Services.Client;
using Automation.Generic.Serialization;
using CommandLine.Text;
using System.Net.NetworkInformation;

namespace Automation.Cognitive.SSLUtility
{
    class Program
    {
        public static string IQBOT_INSTALL_PATH = @"";
        public static string CERTIFICATE_PATH = @"";
        public static string CERTIFICATE_KEY_PATH = @"";
        public static string CA_CERTIFICATE_KEY_PATH = @"";
        public static string JAVA_KEY_STORE_PATH = @"";
        public static string CERTIFICATE_PASSWORD = @"";

        public static string FRONT_END_CERTIFICATE_KEY_PATH = @"Portal\keys\";
        public static string ML_WEBSERVICE_CONFIG_PATH = @"ML\WebService\";
        public static string ML_TRANSLATIONSVC_CONFIG_PATH = @"ML\translationsvc\";
        public static string CLASSIFIER_CONFIG_PATH = @"Workers\Classifier\";
        public static string ENGINE_WORKER_CONFIG_PATH = @"Workers\VisionBotEngine\";
        public static string CONFIG_PATH = @"Configurations\";

        static void Main(string[] args)
        {
            if (!Validate(args))
            {
                Console.Read();
                return;
            }
            var options = new SSLOptions();
            CommandLine.Parser.Default.ParseArguments(args, options);
            Program.IQBOT_INSTALL_PATH = options.IQBotInstallationPath;
            Program.CERTIFICATE_PATH = options.CertificatePath;
            Program.CERTIFICATE_KEY_PATH = options.CertificateKeyPath;
            Program.CA_CERTIFICATE_KEY_PATH = options.CACertificatePath;
            Program.JAVA_KEY_STORE_PATH = options.JavaKeyStorePath;
            Program.CERTIFICATE_PASSWORD = options.Password;
            //Assuming Keys & SSL Certificate are generated.
            Console.WriteLine(string.Format("IQBot Install Path: {0}", Program.IQBOT_INSTALL_PATH));
            Console.WriteLine(string.Format("Certificate Path: {0}", Program.CERTIFICATE_PATH));
            Console.WriteLine(string.Format("Certificate Key Path: {0}", Program.CERTIFICATE_KEY_PATH));
            Console.WriteLine(string.Format("CA Certificate Path: {0}", Program.CA_CERTIFICATE_KEY_PATH));
            Console.WriteLine(string.Format("Java Key Store Path: {0}", Program.JAVA_KEY_STORE_PATH));
            Console.WriteLine(string.Format("Certificate Password: {0}",MaskPassword(Program.CERTIFICATE_PASSWORD)));
            Console.WriteLine("TLS/SSL Configuration starting...");
            try
            {
                Console.WriteLine("Copying files...");
                SSLUtil.CopyFilesToRespectiveFolders();
                Console.WriteLine("Files copied...");

                Console.WriteLine("Changing Config Settings...");
                SSLUtil.ChangeConfigSettings();
                Console.WriteLine("Changing Config Settings Complete.");

                Console.WriteLine("TLS/SSL Configuration Completed.");
                Console.WriteLine("Restart IQBot Services. Refer technical documents for restarting IQBot Services.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("SSL Configuration Failed!");
            }
            Console.ReadKey();
        }

        private static string MaskPassword(string input)
        {
            char maskBy = '*';
            int count = input.Length <= 2 ? 0 : input.Length - 4;
            return new string(input.Select((c, i) => i < count ? maskBy : c).ToArray());
        }

        private static bool Validate(string[] args)
        {
            var options = new SSLOptions();
            var isvalid = CommandLine.Parser.Default.ParseArguments(args, options);
            if(!isvalid)
                Console.WriteLine(HelpText.AutoBuild(options).ToString());
            return isvalid;
        }
    }

    public static class SSLUtil
    {
        public static bool IsControlRoomSecured()
        {
            string applicationPath = Automation.Util.CommonMethods.GetAAApplicationPath();
            var provider = new RestExecutorConfigurationProvider(applicationPath, null);
            var restConfiguration = provider.Get();
            var controlRoomUrl = new Uri(restConfiguration.ApiUrl);
            return controlRoomUrl.Scheme.ToUpper().Equals("HTTPS");
        }

        public static void CopyFilesToRespectiveFolders()
        {
            var frontendPath = Path.Combine(Program.IQBOT_INSTALL_PATH, Program.FRONT_END_CERTIFICATE_KEY_PATH);
            var configPath = Path.Combine(Program.IQBOT_INSTALL_PATH, Program.CONFIG_PATH);
            //check if cert file path & key file path exists?
            var certfileInfo = new FileInfo(Program.CERTIFICATE_PATH);
            if (!certfileInfo.Exists)
                throw new FileNotFoundException("Certificate File Not Found", Program.CERTIFICATE_PATH);

            var keyfileInfo = new FileInfo(Program.CERTIFICATE_KEY_PATH);
            if (!keyfileInfo.Exists)
                throw new FileNotFoundException("Certificate Key File Not Found", Program.CERTIFICATE_KEY_PATH);

            var cacertfileInfo = new FileInfo(Program.CA_CERTIFICATE_KEY_PATH);
            if (!cacertfileInfo.Exists)
                throw new FileNotFoundException("CA Certificate File Not Found", Program.CA_CERTIFICATE_KEY_PATH);

            var jksInfo = new FileInfo(Program.JAVA_KEY_STORE_PATH);
            if (!jksInfo.Exists)
                throw new FileNotFoundException("JKS File Not Found", Program.JAVA_KEY_STORE_PATH);

            //Is frontend directory accessible.
            if (!IsWriteable(new DirectoryInfo(frontendPath)) && 
                !IsWriteable(new DirectoryInfo(configPath)))
                throw new SecurityException(string.Format("Not enough permissions! \n{0} \n{1} \nRe-run as Admin.",frontendPath,configPath));
            //Copy file to Front end
            if (Directory.Exists(frontendPath))
            {
                File.Copy(Program.CERTIFICATE_PATH, Path.Combine(frontendPath,certfileInfo.Name),true);
                File.Copy(Program.CERTIFICATE_KEY_PATH, Path.Combine(frontendPath,keyfileInfo.Name), true);
                File.Copy(Program.CA_CERTIFICATE_KEY_PATH, Path.Combine(frontendPath, cacertfileInfo.Name), true);
            }
            
            //Copy file to Config folder.
            if (Directory.Exists(configPath))
            {
                File.Copy(Program.CERTIFICATE_PATH, Path.Combine(configPath,certfileInfo.Name), true);
                File.Copy(Program.CERTIFICATE_KEY_PATH, Path.Combine(configPath,keyfileInfo.Name), true);
                File.Copy(Program.JAVA_KEY_STORE_PATH, Path.Combine(configPath, "CognitivePlatform.jks"), true);
            }
        }

        public static void ChangeConfigSettings()
        {
            //change npmstart for front end
            var configpath = Path.Combine(Program.IQBOT_INSTALL_PATH,Program.CONFIG_PATH);
            var mlconfigPath = Path.Combine(Program.IQBOT_INSTALL_PATH, Program.ML_WEBSERVICE_CONFIG_PATH);
            var trnsconfigPath = Path.Combine(Program.IQBOT_INSTALL_PATH, Program.ML_TRANSLATIONSVC_CONFIG_PATH);
            var classifierConfigPath = Path.Combine(Program.IQBOT_INSTALL_PATH, Program.CLASSIFIER_CONFIG_PATH);
            var engineWorkerConfigPath = Path.Combine(Program.IQBOT_INSTALL_PATH, Program.ENGINE_WORKER_CONFIG_PATH);

            if (!IsWriteable(new DirectoryInfo(configpath)))
                throw new SecurityException(
                    string.Format("Not Enough Permissions. \n{0} \nRe-run with Admin Permissions.", configpath));

            //if (!IsWriteable(new DirectoryInfo(mlconfigPath)))
            //    throw new SecurityException(
            //        string.Format("Not Enough Permissions. \n{0} \nRe-run with Admin Permissions.", mlconfigPath));

            //if (!IsWriteable(new DirectoryInfo(configpath)))
            //    throw new SecurityException(
            //        string.Format("Not Enough Permissions. \n{0} \nRe-run with Admin Permissions.", trnsconfigPath));

            if (Directory.Exists(configpath))
            {
                FileInfo keyinfo = new FileInfo(Program.CERTIFICATE_KEY_PATH);
                var frontendkeyfilename = "./keys/" + keyinfo.Name;

                FileInfo certinfo = new FileInfo(Program.CERTIFICATE_PATH);
                var frontendcertname = "./keys/" + certinfo.Name;

                FileInfo cacertinfo = new FileInfo(Program.CA_CERTIFICATE_KEY_PATH);
                var frontendcacertname = "./keys/" + cacertinfo.Name;

                //--env "RUN_WITH_SSL=true" --env "SSL_CERT_KEY=./keys/key.key" --env "SSL_CERT_FILE=./keys/cert.crt
                //front end npm start changes.
                var npmenv = string.Format("--env \"RUN_WITH_SSL=true\" --env \"SSL_CERT_KEY={0}\" --env \"SSL_CERT_FILE={1}\" --env \"SSL_CA_CERT_FILE={2}\"", frontendkeyfilename,frontendcertname,frontendcacertname);
                
                var npmString = File.ReadAllText(@".\filestoreplace\npmstart.txt");
                npmString = npmString.Replace("REPLACE_THIS", npmenv);

                File.WriteAllText(Path.Combine(configpath, "npmstart.bat"), npmString);

                string cognitiveproperties = $@"CertificateKey={Program.CERTIFICATE_PASSWORD}";
                if (File.ReadAllText(Path.Combine(configpath, "cognitive.properties")).Contains("CertificateKey="))
                {
                    var replacetext = File.ReadAllText(Path.Combine(configpath, "cognitive.properties"));
                    var certificateValue = string.Empty;
                    string[] properties = replacetext.Split(Environment.NewLine.ToCharArray());
                    foreach (string s in properties)
                    {
                        string[] keyvalues = s.Split('=');
                        if (keyvalues != null 
                            && keyvalues.Length > 1 
                            && keyvalues[0].Equals("CertificateKey"))
                        {
                            certificateValue = keyvalues[1];
                            break;
                        }
                    }
                    string cognitiveproperitestobereplaced 
                        = replacetext.Replace(string.Format("CertificateKey={0}", certificateValue), cognitiveproperties);

                    File.WriteAllText(Path.Combine(configpath, "cognitive.properties"), cognitiveproperitestobereplaced);
                }
                else
                {
                    //change cognitive.properties for backend
                    File.AppendAllLines(Path.Combine(configpath, "cognitive.properties"),
                        new List<string>() { cognitiveproperties });
                }
            }

            //change ML configuration file
            var mlconfigfile = Path.Combine(mlconfigPath,"CognitiveServiceConfiguration.json");
            if (Directory.Exists(mlconfigPath) &&
                File.Exists(mlconfigfile) &&
                IsWriteable(new DirectoryInfo(mlconfigPath)))
            {
                JSONSerialization jsonSerialization = new JSONSerialization();
                File.WriteAllText(mlconfigfile, jsonSerialization.SerializeToString(ReadMLConfig(mlconfigfile)));
            }

            var transaltionscvconfigfile = Path.Combine(trnsconfigPath, "CognitiveServiceConfiguration.json");
            if (Directory.Exists(trnsconfigPath) &&
                File.Exists(transaltionscvconfigfile) &&
                IsWriteable(new DirectoryInfo(trnsconfigPath)))
            {
                JSONSerialization jsonSerialization = new JSONSerialization();
                File.WriteAllText(transaltionscvconfigfile, jsonSerialization.SerializeToString(ReadMLConfig(transaltionscvconfigfile)));
            }

            //change Classifier configuration file
            var classfierConfigfile = Path.Combine(classifierConfigPath, "CognitiveServiceConfiguration.json");
            ModifyCognitiveConfigurationFile(classfierConfigfile);

            //change engine worker configuration file
            var engineWorkerConfigfile = Path.Combine(engineWorkerConfigPath, "CognitiveServiceConfiguration.json");
            ModifyCognitiveConfigurationFile(engineWorkerConfigfile);

            //change gateway bat file
            if (IsWriteable(new DirectoryInfo(configpath)))
            {
                var gatewayText = File.ReadAllText(@".\filestoreplace\gateway.txt");
                gatewayText = gatewayText.Replace("REPLACE_THIS", GetFullyQualifiedDomainName());
                File.WriteAllText(Path.Combine(configpath, "gateway.bat"), gatewayText);
            }

            //turn on the SSL parameter of Auth Service if Control Room is on Secure Mode.
            if (IsControlRoomSecured() && IsWriteable(new DirectoryInfo(configpath)))
                File.WriteAllBytes(Path.Combine(configpath,"authentication.bat"), File.ReadAllBytes(@".\filestoreplace\authentication-bat.txt"));
        }

        private static void ModifyCognitiveConfigurationFile(string configFilePath)
        {
            if (File.Exists(configFilePath) &&
                IsWriteable(new DirectoryInfo(configFilePath)))
            {
                JSONSerialization jsonSerialization = new JSONSerialization();
                File.WriteAllText(configFilePath, jsonSerialization.SerializeToString(ReadCognitiveConfig(configFilePath)));
            }
        }

        private static MLConfig ReadMLConfig(string filePath)
        {
            JSONSerialization jsonSerialization = new JSONSerialization();
            var mlConfig = jsonSerialization.DeserializeFromStream<MLConfig>(File.OpenRead(filePath));
            (from serviceconfig in mlConfig.ServiceConfiguration
            select serviceconfig).ToList().ForEach(s=>s.ServiceUri = UpdateUrl(s.ServiceUri));
            var certificatefilename = new FileInfo(Program.CERTIFICATE_PATH).Name;
            var certificatekeyname = new FileInfo(Program.CERTIFICATE_KEY_PATH).Name;
            mlConfig.SSLSettings = new SSLSettings()
            {
                RunWithSSL = true,
                SSL_CERT_FilePath = Path.Combine(Program.IQBOT_INSTALL_PATH,Program.CONFIG_PATH,certificatefilename),
                SSL_CERT_Key = Path.Combine(Program.IQBOT_INSTALL_PATH,Program.CONFIG_PATH,certificatekeyname)
            };
            return mlConfig;
        }

        private static CognitiveConfiguration ReadCognitiveConfig(string filePath)
        {
            JSONSerialization jsonSerialization = new JSONSerialization();
            var cognitiveConfiguration = jsonSerialization.DeserializeFromStream<CognitiveConfiguration>(File.OpenRead(filePath));
            (from serviceconfig in cognitiveConfiguration.ServiceConfiguration
             select serviceconfig).ToList().ForEach(s => s.ServiceUri = UpdateUrl(s.ServiceUri));
            
            return cognitiveConfiguration;
        }

        private static string UpdateUrl(string serviceUri)
        {
            UriBuilder uriBuilder = new UriBuilder(new Uri(serviceUri));
            uriBuilder.Scheme = "https";
            uriBuilder.Host = GetFullyQualifiedDomainName();
            return uriBuilder.Uri.ToString();
        }

        public static string GetFullyQualifiedDomainName()
        {
            var ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            return string.IsNullOrWhiteSpace(ipProperties.DomainName) ? ipProperties.HostName : string.Format("{0}.{1}", ipProperties.HostName, ipProperties.DomainName);
        }

        public static bool IsReadable(this DirectoryInfo di)
        {
            AuthorizationRuleCollection rules = null;
            WindowsIdentity identity = null;
            try
            {
                rules = di.GetAccessControl().GetAccessRules(true, true, typeof (SecurityIdentifier));
                identity = WindowsIdentity.GetCurrent();
            }
            catch (UnauthorizedAccessException uae)
            {
                Debug.WriteLine(uae.ToString());
                return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            bool isAllow = false;
            string userSID = identity.User.Value;

            foreach (FileSystemAccessRule rule in rules)
            {
                if (rule.IdentityReference.ToString() == userSID || identity.Groups.Contains(rule.IdentityReference))
                {
                    if ((rule.FileSystemRights.HasFlag(FileSystemRights.Read) ||
                        rule.FileSystemRights.HasFlag(FileSystemRights.ReadAttributes) ||
                        rule.FileSystemRights.HasFlag(FileSystemRights.ReadData)) && rule.AccessControlType == AccessControlType.Deny)
                        return false;
                    else if ((rule.FileSystemRights.HasFlag(FileSystemRights.Read) &&
                        rule.FileSystemRights.HasFlag(FileSystemRights.ReadAttributes) &&
                        rule.FileSystemRights.HasFlag(FileSystemRights.ReadData)) && rule.AccessControlType == AccessControlType.Allow)
                        isAllow = true;

                }
            }
            return isAllow;
        }

        public static bool IsWriteable(this DirectoryInfo me)
        {
            AuthorizationRuleCollection rules;
            WindowsIdentity identity;
            try
            {
                rules = me.GetAccessControl().GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));
                identity = WindowsIdentity.GetCurrent();
            }
            catch (UnauthorizedAccessException uae)
            {
                Debug.WriteLine(uae.ToString());
                return false;
            }

            bool isAllow = false;
            string userSID = identity.User.Value;

            foreach (FileSystemAccessRule rule in rules)
            {
                if (rule.IdentityReference.ToString() == userSID || identity.Groups.Contains(rule.IdentityReference))
                {
                    if ((rule.FileSystemRights.HasFlag(FileSystemRights.Write) ||
                        rule.FileSystemRights.HasFlag(FileSystemRights.WriteAttributes) ||
                        rule.FileSystemRights.HasFlag(FileSystemRights.WriteData) ||
                        rule.FileSystemRights.HasFlag(FileSystemRights.CreateDirectories) ||
                        rule.FileSystemRights.HasFlag(FileSystemRights.CreateFiles)) && rule.AccessControlType == AccessControlType.Deny)
                        return false;
                    else if ((rule.FileSystemRights.HasFlag(FileSystemRights.Write) &&
                        rule.FileSystemRights.HasFlag(FileSystemRights.WriteAttributes) &&
                        rule.FileSystemRights.HasFlag(FileSystemRights.WriteData) &&
                        rule.FileSystemRights.HasFlag(FileSystemRights.CreateDirectories) &&
                        rule.FileSystemRights.HasFlag(FileSystemRights.CreateFiles)) && rule.AccessControlType == AccessControlType.Allow)
                        isAllow = true;

                }
            }
            return isAllow;
        }

    }

}
