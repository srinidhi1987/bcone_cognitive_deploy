﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace taxonomy_sql_generator
{
    using OfficeOpenXml;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OleDb;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class Program
    {
        public static void Main(string[] args)
        {
            var fileName = @"D:\Taxonomy.xlsx";
            var outputFileName = @"D:\Taxonomy.sql";

            var sbPre = new StringBuilder();
            var sbPost = new StringBuilder();
            var sbDocTypeMaster = new StringBuilder();
            var sbFields = new StringBuilder();
            var sbAliases = new StringBuilder();

            var sb = new StringBuilder();

            using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName)))
            {
                var myWorksheet = xlPackage.Workbook.Worksheets.First();
                var totalRows = myWorksheet.Dimension.End.Row;
                var totalColumns = myWorksheet.Dimension.End.Column;

                var sqlDocTypeMaster = "insert into [dbo].[DocTypeMaster] ([Id], [Name], [LanguageId]) values ({0}, '{1}', 1);";
                var sqlFields = "insert into [dbo].[Fields] ([Id], [Name], [TypeId], [DocTypeId], [IsDefaultSelected], [DataTypeId]) values ({0}, '{1}', {2}, {3}, {4}, {5});";
                var sqlAliases = "insert into [dbo].[Aliases] ([Id], [FieldId], [Alias], [LanguageId]) values ({0}, {1}, '{2}', {3});";

                var sqlDelete = "delete from {0};";
                var sqlIdentity = "Set Identity_Insert [{0}] {1};";
                var identityTables = new List<string> {
                    "DocTypeMaster",
                    "Fields",
                    "Aliases"
                };

                for (var i = identityTables.Count - 1; i >= 0; i--)
                {
                    var tableName = identityTables[i];
                    sbPre.AppendLine(string.Format($"[dbo].[{sqlDelete}]", tableName));
                }

                var docTypeId = 0;
                var fieldId = 0;
                var aliasId = 0;

                for (int rowNum = 2; rowNum <= totalRows; rowNum++)
                {
                    var docTypeName = myWorksheet.Cells[rowNum, 1].Text;
                    if (!string.IsNullOrWhiteSpace(docTypeName))
                    {
                        sbDocTypeMaster.AppendLine(string.Format(sqlDocTypeMaster, ++docTypeId, Sanitize(docTypeName)));
                    }

                    if (string.IsNullOrWhiteSpace(myWorksheet.Cells[rowNum, 2].Text))
                    {
                        break;
                    }

                    var dataTypeName = myWorksheet.Cells[rowNum, 3].Text;

                    if (string.IsNullOrWhiteSpace(dataTypeName))
                    {
                        break;
                    }

                    var fieldTypeNames = myWorksheet.Cells[rowNum, 4].Text.Split('/');
                    var isDefaultName = myWorksheet.Cells[rowNum, 5].Text;

                    var fieldNames = fieldTypeNames.Count() == 1 ? new string[] { myWorksheet.Cells[rowNum, 2].Text } : myWorksheet.Cells[rowNum, 2].Text.Split('/');

                    for (int i = 0; i < fieldNames.Count(); i++)
                    {
                        try
                        {
                            var fieldName = fieldNames[i];
                            var fieldTypeName = fieldTypeNames[i];

                            var fieldTypeId = GetFieldTypeId(fieldTypeName);
                            var isDefaultSelected = (string.IsNullOrEmpty(isDefaultName) ? false : isDefaultName.ToLower() == "y") ? 1 : 0;
                            var dataTypeId = GetDataTypeId(dataTypeName);

                            sbFields.AppendLine(string.Format(sqlFields, ++fieldId, Sanitize(fieldName), fieldTypeId, docTypeId, isDefaultSelected, dataTypeId));

                            var englishAliases = myWorksheet.Cells[rowNum, 6].Text.Split(',');
                            foreach (var aliasName in englishAliases)
                            {
                                if (string.IsNullOrWhiteSpace(aliasName)) continue;
                                sbAliases.AppendLine(string.Format(sqlAliases, ++aliasId, fieldId, Sanitize(aliasName), 1));
                            }

                            var germanAliases = myWorksheet.Cells[rowNum, 7].Text.Split(',');
                            foreach (var aliasName in germanAliases)
                            {
                                if (string.IsNullOrWhiteSpace(aliasName)) continue;
                                sbAliases.AppendLine(string.Format(sqlAliases, ++aliasId, fieldId, Sanitize(aliasName), 2));
                            }

                            var frenchAliases = myWorksheet.Cells[rowNum, 8].Text.Split(',');
                            foreach (var aliasName in frenchAliases)
                            {
                                if (string.IsNullOrWhiteSpace(aliasName)) continue;
                                sbAliases.AppendLine(string.Format(sqlAliases, ++aliasId, fieldId, Sanitize(aliasName), 3));
                            }

                            var spanishAliases = myWorksheet.Cells[rowNum, 9].Text.Split(',');
                            foreach (var aliasName in spanishAliases)
                            {
                                if (string.IsNullOrWhiteSpace(aliasName)) continue;
                                sbAliases.AppendLine(string.Format(sqlAliases, ++aliasId, fieldId, Sanitize(aliasName), 4));
                            }

                            var italianAliases = myWorksheet.Cells[rowNum, 10].Text.Split(',');
                            foreach (var aliasName in italianAliases)
                            {
                                if (string.IsNullOrWhiteSpace(aliasName)) continue;
                                sbAliases.AppendLine(string.Format(sqlAliases, ++aliasId, fieldId, Sanitize(aliasName), 5));
                            }
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            continue;
                        }
                    }
                }

                sb.Append(sbPre);

                sb.AppendLine(string.Format(sqlIdentity, "DocTypeMaster", "On"));
                sb.Append(sbDocTypeMaster);
                sb.AppendLine(string.Format(sqlIdentity, "DocTypeMaster", "Off"));

                sb.AppendLine(string.Format(sqlIdentity, "Fields", "On"));
                sb.Append(sbFields);
                sb.AppendLine(string.Format(sqlIdentity, "Fields", "Off"));

                sb.AppendLine(string.Format(sqlIdentity, "Aliases", "On"));
                sb.Append(sbAliases);
                sb.AppendLine(string.Format(sqlIdentity, "Aliases", "Off"));

                sb.Append(sbPost);

                File.WriteAllText(outputFileName, sb.ToString());
            }
        }

        private static int GetFieldTypeId(string fieldTypeName)
        {
            fieldTypeName = fieldTypeName.ToLower();
            switch (fieldTypeName)
            {
                case "f": return 1;
                case "tc": return 2;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        private static int GetDataTypeId(string typeName)
        {
            typeName = typeName.ToLower();
            switch (typeName)
            {
                case "text": return 1;
                case "number": return 3;
                case "date": return 2;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        private static string Sanitize(string rawValue)
        {
            if (string.IsNullOrWhiteSpace(rawValue)) return string.Empty;

            return rawValue.Trim().Replace("\'", "\'\'");
        }
    }
}