﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;
using Newtonsoft.Json;

namespace Automation.Cognitive.VisionBotEngine.Model.V2.Project
{
    public class CustomFieldV2
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "type")]
        public ProjectFieldType Type { get; set; }

        public Automation.VisionBotEngine.FieldValueType FieldValueType { get; set; }

        [JsonProperty(PropertyName = "isAddedLater")]
        public bool IsAddedLater { get; set; }
    }
}
