﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Newtonsoft.Json;
using System.Collections.Generic;

namespace Automation.Cognitive.VisionBotEngine.Model.V2.Project
{
    public class ProjectFieldsV2
    {
        [JsonProperty(PropertyName = "standard")]
        public List<StandardFieldV2> Standard { get; set; }

        [JsonProperty(PropertyName = "custom")]
        public List<CustomFieldV2> Custom { get; set; }

        public ProjectFieldsV2()
        {
            Standard = new List<StandardFieldV2>();
            Custom = new List<CustomFieldV2>();
        }
    }
}
