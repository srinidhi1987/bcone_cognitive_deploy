﻿using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using Cognitive.VisionBotEngine;
    using Cognitive.VisionBotEngine.Common;
    using ResizeRescan.Interface;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Automation.Cognitive.VisionBotEngine.Common;
    using Services.Client;
    using System.Threading;
    using System.IO;
    using Cognitive.VisionBotEngine.ExtractionEngine.TableEngine;
    using System.Globalization;

    class TableValueExtractor
    {
        private IFieldOperations _fieldOperations;
        private IRectangleOperations _rectOperations;
        private IRescan _rescan;
        public string path;
        public TableValueExtractor(IFieldOperations fieldOperations, IRectangleOperations rectangleOperations)
        {
            this._fieldOperations = fieldOperations;
            this._rectOperations = rectangleOperations;
        }
        //public TableValueExtractor(IRescan rescan) : this(new RectangleOperations(), new FieldOperations())
        //{
        //    _rescan = rescan;
        //}
        PageProperties currentPage;
        internal TableValue Extract(TableInfo tableInfo, List<PageProperties> pages)
        {
            TableValue result = new TableValue();
            result.Rows = new List<DataRecord>();
            fillTableHeaders(result, tableInfo);
            VBotLogger.Trace(() => string.Format("[fillTableRows] Entered."));
            if (!IsPrimaryColumnDefined(tableInfo.Layout.PrimaryColumn))
            {
                VBotLogger.Trace(() => $"[{nameof(fillTableRows)}] Primary column is not defined in table \"{tableInfo.Layout.TableId}\"");
                return result;
            }

            TableInfo clonedTableInfo = tableInfo.Clone();
            List<Field> allPagesField = new List<Field>(clonedTableInfo.Fields);
            List<Field> allPagesRegions = new List<Field>(clonedTableInfo.Regions);
            List<Line> allPagesLine = new List<Line>(clonedTableInfo.Lines);
            int currentPageTopOffSet = 0;
            bool isTableStartFound = false;
            int primaryColumnIndex = clonedTableInfo.Layout.Columns.FindIndex(x => x.Id == clonedTableInfo.Layout.PrimaryColumn.Id);
            List<FieldLayout> lastPageTableColumns = new List<FieldLayout>();
            for (int pageNo = 0; pageNo < pages.Count; pageNo++)
            {
                currentPage = pages[pageNo];
                string imgFilePath = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".png"); ;
                SaveCurrentPageImage(imgFilePath, path, pageNo);
                ReAssignTableColumnLabels(tableInfo, clonedTableInfo);
                FieldLayout primaryColumnLayout = clonedTableInfo.Layout.PrimaryColumn;

                Rectangle currentPageRect = new Rectangle(0, currentPageTopOffSet, pages[pageNo].Width, pages[pageNo].Height);

                clonedTableInfo.Fields = GetCurrentPageFieldExceptPageFooter(currentPageRect, allPagesField);

                clonedTableInfo.Lines = getCurrentPageLines(currentPageRect, allPagesLine);

                clonedTableInfo.Regions = GetCurrentPageFieldExceptPageFooter(currentPageRect, allPagesRegions);



                if (clonedTableInfo.Fields?.Count > 0)
                {
                    bool isThisHeaderLessTable = false;

                    Point centerPointOfDocument = _rectOperations.GetCenterPoint(currentPageRect);

                    rotateFieldsAsPerTheirAngle(clonedTableInfo.Fields, centerPointOfDocument);

                    //Primary Column Headers Finds
                    // tableInfo.Layout.PrimaryColumn.
                    //find Other Header Respect to given Header
                    List<Field> primaryColumnKeys = _fieldOperations.GetAllBestMatchingFields(clonedTableInfo.Fields, primaryColumnLayout.Label, .9);
                    if (primaryColumnKeys.Count <= 0)
                    {
                        currentPageTopOffSet += pages[pageNo].Height;
                        continue;
                    }

                    //Find Y Offset for every page
                    List<FieldLayout> sortedFiledByY = clonedTableInfo.Layout.Columns.OrderBy(X => X.Bounds.Y).ToList();
                    int primaryKeyValueBoundY = clonedTableInfo.Layout.PrimaryColumn.Bounds.Y + clonedTableInfo.Layout.PrimaryColumn.ValueBounds.Y;
                    int firstFieldValueBundY = sortedFiledByY[0].Bounds.Y + sortedFiledByY[0].ValueBounds.Y;
                    int yOffset = primaryKeyValueBoundY - firstFieldValueBundY;
                    int curentPageYOffset = 0;// primaryColumnKeys[0].Bounds.Y - yOffset - primaryKeyValueBoundY;

                    List<FieldTraininngInfo> mappedValueFields = new List<FieldTraininngInfo>();
                    foreach (FieldLayout fieldLayout in clonedTableInfo.Layout.Columns)
                    {
                        mappedValueFields.Add(new FieldTraininngInfo(tableInfo.ColumnDefs.Columns.FirstOrDefault(x => x.Id == fieldLayout.FieldId && !x.IsDeleted), fieldLayout, new Rectangle(fieldLayout.Bounds.X + fieldLayout.ValueBounds.X,
                            fieldLayout.Bounds.Y + fieldLayout.ValueBounds.Y + curentPageYOffset,
                            fieldLayout.ValueBounds.Width,
                            fieldLayout.ValueBounds.Height), Rectangle.Empty)
                        {
                            valueDataType = GetDataTypesFromFieldLayoutValueBound(fieldLayout, allPagesField)
                        });

                    }

                    int tableBottom = getTableBottomFromFooter(tableInfo
                                    , curentPageYOffset + 1);

                    for (int k = 0; k < 1; k++)
                    {
                        Field primaryKey = primaryColumnKeys[k];
                        Field nextKey = k < primaryColumnKeys.Count - 1 ? primaryColumnKeys[k + 1] : null;

                        var valueRect = new RectangleOperations().GetBoundsFromRects(mappedValueFields.Select(x => x.AbsoluteValueRect).ToList());
                        // valueRect= new RectangleOperations().AddOffset(valueRect, -valueRect.X,- valueRect.Y);

                        var valueRects = mappedValueFields.Select(x => x.AbsoluteValueRect).ToList();
                        var normalizedAllValueBoundRect = Rectangle.Empty;
                        if (valueRects.Count > 0)
                        {
                            var valueRectsHeightWise = valueRects.OrderBy(x => x.Height).ToList();
                            var smallestValueRect = valueRectsHeightWise[0];

                            var normalizedValuesRect = mappedValueFields.Select(x =>
                              {
                                  if (x.AbsoluteValueRect.Bottom > smallestValueRect.Top)
                                  {
                                      return new Rectangle(x.AbsoluteValueRect.X, x.AbsoluteValueRect.Y, x.AbsoluteValueRect.Width, smallestValueRect.Height);
                                  }
                                  else
                                  {
                                      return new Rectangle(x.AbsoluteValueRect.Bottom - smallestValueRect.Height, x.AbsoluteValueRect.Y, x.AbsoluteValueRect.Width, smallestValueRect.Height);
                                  }
                              }).ToList();

                            normalizedAllValueBoundRect = new RectangleOperations().GetBoundsFromRects(normalizedValuesRect);
                        }

                        int jumpNextValueOffset = Math.Max(clonedTableInfo.Layout.Columns[primaryColumnIndex].ValueBounds.Height, (int)(normalizedAllValueBoundRect.Height * .60));

                        //  List<Rectangle> newMappedValueFields = new List<Rectangle>();
                        foreach (var rect in mappedValueFields)
                        {
                            rect.RespectToTableValueRect = new RectangleOperations().AddOffset(rect.AbsoluteValueRect, -valueRect.X, -valueRect.Y);
                        }

                        List<Field> rowsValue = new List<Field>();


                        int templateStartY = primaryKey.Bounds.Y - mappedValueFields[primaryColumnIndex].RespectToTableValueRect.Y;// valueRect.Top;// primaryKey.Bounds.Top;
                        bool isNeedToMove = true;
                        List<FieldLayout> sortedFiledByX = clonedTableInfo.Layout.Columns.OrderBy(X => X.Bounds.X).ToList();
                        //int xStart = valueRect.X;// sortedFiledByX[0].Bounds.X + sortedFiledByX[0].ValueBounds.X;
                        int xStart = primaryKey.Bounds.X - mappedValueFields[primaryColumnIndex].RespectToTableValueRect.X + clonedTableInfo.Layout.Columns[primaryColumnIndex].ValueBounds.X;
                        int xEnd = xStart + valueRect.Width + 100;
                        //Point templatePoint = Point.Empty;
                        int currentRow = 0;

                        do
                        {
                            START_AGAIN:
                            int nextRowCount = currentRow + 1;
                            Point templatePoint = getTemplateMatchingPoint(clonedTableInfo
                               , currentPageRect
                               , templateStartY
                               , tableBottom
                               , mappedValueFields
                               , tableInfo.Layout
                               , imgFilePath
                               , currentPageTopOffSet
                               , isNeedToMove
                               , xStart
                               , xEnd
                               , primaryColumnIndex
                               );
                            if (templatePoint.IsEmpty)// || (nextKey != null && (templatePoint.Y + valueRect.Height) > nextKey.Bounds.Y))
                            {
                                if (currentRow >= primaryColumnKeys.Count)
                                    break;
                                else
                                {

                                    if (nextRowCount < primaryColumnKeys.Count && Math.Abs(primaryColumnKeys[currentRow].Bounds.Y - primaryColumnKeys[nextRowCount].Bounds.Y) < valueRect.Height)
                                    {
                                        xStart = xStart + (primaryColumnKeys[nextRowCount].Bounds.X - primaryColumnKeys[currentRow].Bounds.X);
                                        templateStartY = valueRect.Top;
                                        currentRow++;
                                        goto START_AGAIN;
                                    }
                                    else
                                        break;
                                }
                            }
                            xStart = templatePoint.X;
                            xEnd = xStart + valueRect.Width + 100;
                            templateStartY = getApproximateNextTemplateTop(primaryColumnKeys
                                , yOffset
                                , valueRect
                                , currentRow, nextRowCount
                                , templatePoint
                                , jumpNextValueOffset);
                            bool nextPointFound;
                            Rectangle currentRectangle = getCurrentRowRectangle(tableInfo
                                , clonedTableInfo
                                , currentPageTopOffSet
                                , imgFilePath, currentPageRect
                                , tableBottom, valueRect
                                , mappedValueFields, ref templateStartY
                                , xStart, xEnd
                                , templatePoint, out nextPointFound, primaryColumnIndex);

                            List<Field> currentRowFields = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(clonedTableInfo.Fields, currentRectangle);

                            isNeedToMove = true;
                            //xStart = templatePoint.X;
                            DataRecord row = extractRow(tableInfo, result, mappedValueFields, templatePoint, currentRowFields);
                            result.Rows.Add(row);


                            if (primaryColumnKeys.Count > nextRowCount)
                            {
                                if (arePrimaryColumnsKeysVerticallyAlligned(primaryColumnKeys[currentRow]
                                    , primaryColumnKeys[nextRowCount]
                                    , valueRect))
                                {
                                    //TODO: it will jump to next primary column but skip if there is more row in between two primary columns.need to think
                                    templateStartY = primaryColumnKeys[nextRowCount].Bounds.Y - yOffset - jumpNextValueOffset;
                                }
                                else
                                {
                                    currentRow -= 1;
                                }
                            }
                            else if (nextPointFound == false)
                                break;
                            currentRow++;
                        } while (true);
                    }

                    lastPageTableColumns = clonedTableInfo.Layout.Columns.Select(column => column.Clone()).ToList();
                }

                currentPageTopOffSet += pages[pageNo].Height;

                //if (File.Exists(imgFilePath))
                //    File.Delete(imgFilePath);
            }


            return result;
        }

        private DataTypes GetDataTypesFromFieldLayoutValueBound(FieldLayout fieldLayout, List<Field> fields)
        {
            var valueBoundRect = new Rectangle(fieldLayout.Bounds.X + fieldLayout.ValueBounds.X,
                               fieldLayout.Bounds.Y + fieldLayout.ValueBounds.Y,
                               fieldLayout.ValueBounds.Width,
                               fieldLayout.ValueBounds.Height);
            var fieldValue = _fieldOperations.GetAllSystemFieldsWithinRect(fields, valueBoundRect);

            var field = _fieldOperations.GetSingleFieldFromFields(fieldValue);
            if (field != null && !string.IsNullOrWhiteSpace(field.Text))
            {
                return GetDataTypes(field.Text);
            }
            return DataTypes.String;
        }

        private Rectangle getCurrentRowRectangle(TableInfo tableInfo
            , TableInfo clonedTableInfo, int currentPageTopOffSet
            , string imgFilePath, Rectangle currentPageRect
            , int tableBottom, Rectangle valueRect
            , List<FieldTraininngInfo> newMappedValueFields
            , ref int templateStartY
            , int xStart, int xEnd, Point templatePoint, out bool nextPointFound, int primaryColumnIndex = -1)
        {
            Rectangle currentRectangle = new Rectangle(templatePoint.X
                , templatePoint.Y
                , valueRect.Width, valueRect.Height);
            nextPointFound = false;
            if (!templatePoint.IsEmpty)
            {
                Point templatePointNextRow = getTemplateMatchingPoint(clonedTableInfo
                    , currentPageRect
                    , templateStartY
                    , tableBottom
                    , newMappedValueFields
                    , tableInfo.Layout
                    , imgFilePath
                    , currentPageTopOffSet
                    , false
                    , xStart
                    , xEnd
                    , primaryColumnIndex
                    );
                if (!templatePointNextRow.IsEmpty)
                {
                    nextPointFound = true;
                    templateStartY = templatePointNextRow.Y;
                    currentRectangle = new Rectangle(templatePoint.X
                   , templatePoint.Y
                   , valueRect.Width, templatePointNextRow.Y - templatePoint.Y);
                }
            }

            return currentRectangle;
        }

        private static int getApproximateNextTemplateTop(List<Field> primaryColumnKeys
            , int yOffset
            , Rectangle valueRect
            , int currentRow
            , int nextRowCount
            , Point templatePoint
            , int nextTemplateJump)
        {
            int templateStartY = templatePoint.Y + nextTemplateJump;

            //if (primaryColumnKeys.Count > nextRowCount)
            //{
            //    //if (primaryColumnKeys.Count <= nextRowCountInside)
            //    //    nextRowCountInside = currentRow;
            //    if (arePrimaryColumnsKeysVerticallyAlligned(primaryColumnKeys[currentRow]
            //        , primaryColumnKeys[nextRowCount]
            //        , valueRect))
            //        templateStartY = primaryColumnKeys[nextRowCount].Bounds.Y - yOffset;
            //}

            return templateStartY;
        }

        private static bool arePrimaryColumnsKeysVerticallyAlligned(Field currentRowPrimaryColumn
            , Field nextRowPrimaryColumn
            , Rectangle valueRect)
        {
            return Math.Abs(nextRowPrimaryColumn.Bounds.Y - currentRowPrimaryColumn.Bounds.Y) > valueRect.Height;
        }

        private DataRecord extractRow(TableInfo tableInfo
            , TableValue result
            , List<FieldTraininngInfo> newMappedValueFields
            , Point templatePoint
            , List<Field> currentRowFields)
        {
            var row = new DataRecord();
            row.Fields = new List<FieldData>();
            for (int i = 0; i < result.Headers.Count; i++)
            {
                FieldLayout colLayout = GetColumnLayout(tableInfo, result.Headers[i].Id);
                int index = tableInfo.Layout.Columns.FindIndex(x => x == colLayout);
                if (index > -1)
                {
                    row.Fields.Add(GetFieldDataForCell(currentRowFields, result.Headers[i]
                        , new RectangleOperations()
                        .AddOffset(newMappedValueFields[index].RespectToTableValueRect, templatePoint.X, templatePoint.Y)));
                    if (row.Fields[i].Value.Field.Text.IsNullOrEmpty())
                    {
                        row.Fields[i].Value.Field = getValue(colLayout, row.Fields[i].Field, currentRowFields);
                        (row.Fields[i].Value as TextValue).Value = row.Fields[i].Value.Field.Text;
                    }
                }
                else
                {
                    row.Fields.Add(GetFieldDataForCell(currentRowFields, result.Headers[i]
                       , Rectangle.Empty));
                }
            }

            return row;
        }

        void SaveCurrentPageImage(string pathToSaveCurrPage, string docPath, int pageIndex)
        {
            DocumentProperties docProps = new DocumentProperties();
            docProps.Path = docPath;

            IDocumentPageImageProvider processingDocumentPageImageProvider =
                new DocumentPageImageProvider(docProps, new ImageProcessingConfig(), new VBotEngineSettings(), false);
            Image img = processingDocumentPageImageProvider.GetPage(pageIndex);
            if (img != null)
            {

                img.Save(pathToSaveCurrPage);

            }
        }
        private Field getValue(FieldLayout fieldLayout1, FieldDef fieldDef, List<Field> interestedFields)
        {
            SIRFields sirFields = new SIRFields();
            sirFields.Fields = interestedFields;
            //FieldLayout fieldLayout1 = tableLayout.Columns.FirstOrDefault(col => col.FieldId == header.Id);
            VBotDocument tempVBotDoc = new VBotDocument()
            {
                SirFields = sirFields
            };
            if (fieldLayout1 != null)
            {
                FieldLayout fieldLayout = fieldLayout1.Clone();
                // fieldLayout.IsValueBoundAuto = true;
                string tempLabel = fieldLayout.Label;
                // var keyFields = fieldOperation.GetAllBestMatchingFields(sirFields.Fields, fieldLayout.Label, .8);
                fieldLayout = new VBotDocumentGenerator(null, null)
                    .UpdateFieldLayout(tempVBotDoc, fieldLayout);
                // if (keyFields != null && keyFields.Count == 0)
                if (fieldLayout.Label.IsNullOrEmpty())
                {
                    //VBotLogger.Trace(() => $"[fillTableValueBasedOnAlgorithm2] layout label key not found.");
                    //VBotLogger.Trace(() => $"[fillTableValueBasedOnAlgorithm2] try to find key based on aliases.");
                    //try
                    //{
                    //    AliasDetail aliasDetail = new AliasServiceEndPointConsumer().GetAliasDetailBasedOnFieldId(fieldLayout1.FieldId);
                    //    if (aliasDetail != null)
                    //    {
                    //        foreach (string label in aliasDetail.aliases)
                    //        {
                    //            VBotLogger.Trace(() => $"[fillTableValueBasedOnAlgorithm2] try to find key based on aliase :{label}.");
                    //            fieldLayout = fieldLayout1.Clone();
                    //            fieldLayout.IsValueBoundAuto = true;
                    //            fieldLayout.Label = label;
                    //            fieldLayout = UpdateFieldLayout(tempVBotDoc, fieldLayout);
                    //            // keyFields = fieldOperation.GetAllBestMatchingFields(sirFields.Fields, label, .8);
                    //            //if (keyFields != null && keyFields.Count > 0)
                    //            if (!fieldLayout.Label.IsNullOrEmpty())
                    //            {
                    //                VBotLogger.Trace(() => $"[fillTableValueBasedOnAlgorithm2] key found based on aliase :{label}.");
                    //                tempLabel = label;
                    //                break;
                    //            }
                    //            VBotLogger.Trace(() => $"[fillTableValueBasedOnAlgorithm2] key not found based on aliase :{label}.");
                    //        }
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    ex.Log(() => $"[fillTableValueBasedOnAlgorithm2]");
                    //}
                }

                Field value = new Field();
                //if (keyFields != null && keyFields.Count > 0)
                if (!fieldLayout.Label.IsNullOrEmpty())
                {
                    VBotLogger.Trace(() => $"[fillTableValueBasedOnAlgorithm2] try to find value  based on label :{fieldLayout.Label}.");
                    // fieldLayout.Bounds = keyFields[0].Bounds;
                    //fieldLayout.Label = tempLabel;
                    FieldValueExtraction fieldValueExtractor = new FieldValueExtraction();
                    fieldValueExtractor.LayoutFieldKeys = new List<FieldKey>();
                    value = fieldValueExtractor.GetValueField(fieldLayout, sirFields, fieldDef);
                }

                return value;
            }
            return new Field();
        }
        bool drawEveryStep = false;
        bool drawMatchedSeps = false;

        private Point getTemplateMatchingPoint(TableInfo clonedTableInfo
            , Rectangle currentPageRect
            , int startY
            , int endY
            , List<FieldTraininngInfo> newMappedValueFields
            , TableLayout tableLayout
            , string currPageImagePath
            , int currentPageTopOffSet
            , bool isNeedToMoveInX, int x = 0, int xEnd = 0, int primaryColumnIndex = -1)
        {
            int minXStart = x;
            int increamentFactorX = 5;
            int increamentFactorY = 5;
            int y = startY;
            Point previousPoint = new Point();
            double prevNoOfFieldsFound = 0;
            bool findingMoreFields = false;
            int noOfTryToFindMoreFields = 0;
            bool foundFirstPosition = false;

            do
            {

                List<Rectangle> ValueFields = newMappedValueFields
                    .Select(rect => new RectangleOperations().AddOffset(rect.RespectToTableValueRect, x, y))
                    .ToList();
                var valueRect = new RectangleOperations().GetBoundsFromRects(ValueFields);
                double matchingValueBound = 0;
                int index = 0;
                foreach (Rectangle rect in ValueFields)
                {
                    var matchingFields = _fieldOperations.GetFieldsWithinBoundFieldByOverlapping(clonedTableInfo.Fields, rect, 0.6f);// GetFieldsWithinBoundField(clonedTableInfo.Fields, rect);

                    if (index == primaryColumnIndex && primaryColumnIndex > -1)
                    {
                        if (matchingFields == null || (matchingFields != null && matchingFields.Count == 0))
                        {
                            matchingValueBound = 0;
                            break;
                        }
                    }
                    if (matchingFields.Count > 0)
                    {
                        if (!tableLayout.Columns.Any(ele => ele.Label.Contains(matchingFields[0].Text)))
                        {
                            matchingValueBound = matchingValueBound + 1; //++;

                            if (!(newMappedValueFields[index].valueDataType == DataTypes.String || newMappedValueFields[index].valueDataType == DataTypes.AlphaNumeric))
                            {
                                var field = _fieldOperations.GetSingleFieldFromFields(matchingFields);
                                if (field != null && !string.IsNullOrWhiteSpace(field.Text))
                                {
                                    if (GetDataTypes(field.Text) == newMappedValueFields[index].valueDataType)
                                    {
                                        matchingValueBound = matchingValueBound + .5;
                                    }
                                }
                            }
                        }

                    }
                    index++;
                }

                if (VBotLogger.GetLogLevel() < (int)LogTypes.ERROR)
                {
                    using (Bitmap bitmap1 = new Bitmap(Image.FromFile(currPageImagePath)))
                    {
                        using (Graphics g = Graphics.FromImage(bitmap1))
                        {
                            g.DrawRectangles(new Pen(Color.Red, 1), ValueFields.ToArray());
                            g.Save();
                        }
                        bitmap1.Save("C:\\Test5778.tiff");
                    };


                }
                if (matchingValueBound >= Math.Round(ValueFields.Count * 0.7, MidpointRounding.AwayFromZero))
                {
                    if (foundFirstPosition == false)
                    {
                        foundFirstPosition = true;
                        //y -= increamentFactorY/2;
                        //x -= increamentFactorX/2;
                        increamentFactorX = 2;
                        increamentFactorY = 2;
                    }
                    if (VBotLogger.GetLogLevel() < (int)LogTypes.ERROR)
                    {
                        using (Bitmap bitmap = new Bitmap(Image.FromFile(currPageImagePath)))
                        {
                            List<Rectangle> drawRects = new List<Rectangle>();// ValueFields.ConvertAll(rect => rect.Clone()).ToList();
                            foreach (var rect in ValueFields)
                            {
                                drawRects.Add(new RectangleOperations().AddOffset(rect, 0, -currentPageTopOffSet));
                            }
                            using (Graphics g = Graphics.FromImage(bitmap))
                            {
                                g.DrawRectangles(new Pen(Color.Red, 2), drawRects.ToArray());
                                g.Save();
                            }
                            bitmap.Save("C:\\RowMatched.tiff");
                            Thread.Sleep(200);
                        };// new Bitmap(Image.FromFile("D:\\20160517E001111.000.tiff"));

                    }

                    if (matchingValueBound >= Math.Round(ValueFields.Count * 0.95, MidpointRounding.AwayFromZero))
                    {
                        return new Point(x, y);

                    }
                    else if (prevNoOfFieldsFound < matchingValueBound)
                    {
                        findingMoreFields = true;
                        previousPoint.X = x;
                        previousPoint.Y = y;
                        prevNoOfFieldsFound = matchingValueBound;
                    }
                    else if (matchingValueBound < prevNoOfFieldsFound || (findingMoreFields == true && noOfTryToFindMoreFields >= 5))
                        return previousPoint;
                }

                if (x > currentPageRect.Width / 4 || valueRect.Right > xEnd)
                {
                    if (findingMoreFields == true)
                        noOfTryToFindMoreFields++;
                    y += increamentFactorY;
                    x = minXStart;
                }

                if (isNeedToMoveInX)
                {
                    x += increamentFactorX;
                }
                else
                {
                    if (findingMoreFields == true)
                        noOfTryToFindMoreFields++;
                    y += increamentFactorY;
                }


            } while ((endY <= 0 && y < (currentPageRect.Y + currentPageRect.Height)) ||
                    (endY > 0 && y <= endY));

            return Point.Empty;
        }
        public enum DataTypes
        {
            String,
            Number,
            AlphaNumeric,
            Decimal,
            DateTime
        }
        private static DataTypes GetDataTypes(string text)
        {
            string numberRegex = @"^([0-9]+)$";
            string AlphaNumericRegex = @"^([ A-Z/-]+[0-9]+|[0-9]+[ A-Z/-]+)[ A-Z0-9/-]*$";
            string decimalRegex = @"^([0-9]+[.,]{1,0})$";
            DateTime DateTime;
            decimal decimalNumber;
            if (Decimal.TryParse(text, out decimalNumber))
            {
                return DataTypes.Decimal;
            }
            if (DateTime.TryParse(text, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime))
            {
                return DataTypes.DateTime;
            }
            if (Regex.IsMatch(text, numberRegex))
            {
                return DataTypes.Number;
            }

            else if (Regex.IsMatch(text, AlphaNumericRegex))
            {
                return DataTypes.AlphaNumeric;
            }

            return DataTypes.String;
        }
        public TableValue GetTableValue(TableInfo tableInfo, List<PageProperties> pageProperties)
        {
            TableValue result = new TableValue();
            fillTableHeaders(result, tableInfo);
            fillTableRows(result, tableInfo, pageProperties);
            return result;
        }
        private void fillTableRows(TableValue tableValue, TableInfo tableInfo, List<PageProperties> pages)
        {
            VBotLogger.Trace(() => string.Format("[fillTableRows] Entered."));
            if (!IsPrimaryColumnDefined(tableInfo.Layout.PrimaryColumn))
            {
                VBotLogger.Trace(() => $"[{nameof(fillTableRows)}] Primary column is not defined in table \"{tableInfo.Layout.TableId}\"");
                return;
            }

            TableInfo clonedTableInfo = tableInfo.Clone();
            List<Field> allPagesField = new List<Field>(clonedTableInfo.Fields);
            List<Field> allPagesRegions = new List<Field>(clonedTableInfo.Regions);
            List<Line> allPagesLine = new List<Line>(clonedTableInfo.Lines);
            int currentPageTopOffSet = 0;
            bool isTableStartFound = false;

            List<FieldLayout> lastPageTableColumns = new List<FieldLayout>();
            for (int pageNo = 0; pageNo < pages.Count; pageNo++)
            {
                ReAssignTableColumnLabels(tableInfo, clonedTableInfo);
                FieldLayout primaryColumnLayout = clonedTableInfo.Layout.PrimaryColumn;

                Rectangle currentPageRect = new Rectangle(0, currentPageTopOffSet, pages[pageNo].Width, pages[pageNo].Height);

                clonedTableInfo.Fields = GetCurrentPageFieldExceptPageFooter(currentPageRect, allPagesField);

                clonedTableInfo.Lines = getCurrentPageLines(currentPageRect, allPagesLine);

                clonedTableInfo.Regions = GetCurrentPageFieldExceptPageFooter(currentPageRect, allPagesRegions);

                currentPageTopOffSet += pages[pageNo].Height;

                if (clonedTableInfo.Fields?.Count > 0)
                {
                    bool isThisHeaderLessTable = false;

                    Point centerPointOfDocument = _rectOperations.GetCenterPoint(currentPageRect);

                    rotateFieldsAsPerTheirAngle(clonedTableInfo.Fields, centerPointOfDocument);

                    Field primaryColField = GetPrimaryColumnField(primaryColumnLayout, clonedTableInfo);

                    if (primaryColField == null || string.IsNullOrWhiteSpace(primaryColField.Text))
                    {
                        VBotLogger.Trace(() => $"[{nameof(fillTableRows)}] Primary column is not identified with label '{primaryColumnLayout.Label}' in Page Number '{pageNo}'");

                        if (isTableStartFound)
                        {
                            int top = GetContinuedHeaderlessTableStartPosition(clonedTableInfo);
                            if (top < 0)
                            {
                                break;
                            }

                            isThisHeaderLessTable = true;

                            foreach (Field header in clonedTableInfo.Headers)
                            {
                                header.Bounds = new Rectangle(header.Bounds.Left
                                    , top - header.Bounds.Height
                                    , header.Bounds.Width
                                    , header.Bounds.Height);
                            }

                            clonedTableInfo.Layout.Columns = lastPageTableColumns;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {

                        //if (!primaryColumnLayout.ValueBounds.IsEmpty)
                        //{
                        //    primaryColField.Bounds = new Rectangle(primaryColField.Bounds.Left
                        //        + primaryColumnLayout.ValueBounds.Left
                        //        , primaryColField.Bounds.Top + primaryColumnLayout.ValueBounds.Top
                        //        , primaryColumnLayout.ValueBounds.Width
                        //        , primaryColumnLayout.ValueBounds.Height);
                        //}

                        isTableStartFound = true;
                        clonedTableInfo.Headers = GetTableHeaders(clonedTableInfo, primaryColField);
                    }


                    //appendTableValueForGivenTableInfo(primaryColumnLayout
                    //    , clonedTableInfo
                    //    , tableValue
                    //    , isThisHeaderLessTable);

                    if (hasColumns(clonedTableInfo))
                    {
                        lastPageTableColumns = clonedTableInfo.Layout.Columns.Select(column => column.Clone()).ToList();
                    }
                }
            }
        }

        public FieldValue GetCellValue(List<Field> fields, Rectangle cellRect)
        {
            FieldValue fieldValue = new TextValue();
            fieldValue.Field.Bounds = cellRect;
            if (!_rectOperations.IsRectangleEmpty(cellRect))
            {
                List<Field> cellfields = fields.FindAll(field => cellRect.Contains(field.Bounds.X + (field.Bounds.Width / 2), field.Bounds.Y + (field.Bounds.Height / 2)));
                cellfields = cellfields.OrderBy(field => field.Bounds.Top).ToList();

                fieldValue.Field.Text = _fieldOperations.GetTextFromFields(cellfields);


                if (cellfields.Count > 0)
                {
                    fieldValue.Field.Confidence = new ConfidenceHelper().GetMergedFieldConfidence(cellfields);
                    fieldValue.Field.Bounds = _fieldOperations.GetBoundsFromFields(cellfields);
                }
            }

            return fieldValue;
        }

        public Rectangle GetTableRect(TableInfo tableInfo
            , FieldLayout primaryColumnLayout
            , Field bottomTableRegion
            , List<Field> priamryColumnValues = null)
        {
            Rectangle rect = new Rectangle();

            Field leftMostColumn = tableInfo.Headers[0];
            Field rightMostColumn = tableInfo.Headers[tableInfo.Headers.Count - 1];

            rect.X = GetTableLeft(leftMostColumn, tableInfo.Lines);
            rect.Y = primaryColumnLayout.Bounds.Top;

            // TODO : Header with the lowest top should be the table top
            if (Math.Abs(leftMostColumn.Bounds.Y - rect.Y) > 2 * leftMostColumn.Bounds.Height)
            {
                rect.Y = leftMostColumn.Bounds.Top;
            }

            int tableRight = GetTableRight(rightMostColumn, tableInfo.Lines);

            rect.Width = tableRight - rect.X;

            int tableBottom = getTableBottomFromFooter(tableInfo
                , leftMostColumn.Bounds.Bottom + 1);

            if (tableBottom == 0)
            {
                tableBottom = getTableBottomFromPrimaryColumnValues(tableInfo
                , bottomTableRegion
                , priamryColumnValues
                , leftMostColumn.Bounds.X
                , rightMostColumn.Bounds.X);
            }

            if (tableBottom == 0)
            {
                tableBottom = bottomTableRegion.Bounds.Bottom + (bottomTableRegion.Bounds.Height * 5);

                int lastRowTop = bottomTableRegion.Bounds.Top;

                foreach (Field header in tableInfo.Headers)
                {
                    Rectangle cell = new Rectangle(header.Bounds.X, lastRowTop, header.Bounds.Width, bottomTableRegion.Bounds.Height * 2);
                    var foundFields = _fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, cell);
                    if (foundFields != null && foundFields.Count > 0)
                    {
                        Field parent = tableInfo.Regions.FirstOrDefault(element => element.Id == foundFields[0].ParentId);
                        if (parent != null && parent.Bounds.Bottom > tableBottom)
                        {
                            tableBottom = parent.Bounds.Bottom;
                        }
                    }
                }
            }

            rect.Height = tableBottom - rect.Top;

            return rect;
        }

        private int getTableBottomFromPrimaryColumnValues(TableInfo tableInfo, Field bottomTableRegion, List<Field> priamryColumnValues, int tableLeft, int tableRight)
        {
            int tableBottom = 0;
            if (priamryColumnValues != null
                   && priamryColumnValues.Count > 1)
            {
                Rectangle secondLastRowRect = new Rectangle(tableLeft
                        , priamryColumnValues[priamryColumnValues.Count - 2].Bounds.Top
                        , tableRight - tableLeft
                        , priamryColumnValues[priamryColumnValues.Count - 1].Bounds.Top - priamryColumnValues[priamryColumnValues.Count - 2].Bounds.Top);


                List<Field> secondLastRowField = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(tableInfo.Fields, secondLastRowRect);

                Field bottomFieldOfSecondLastRow = null;
                if (secondLastRowField != null && secondLastRowField.Count > 0)
                {
                    bottomFieldOfSecondLastRow = secondLastRowField.OrderByDescending(x => x.Bounds.Bottom).ToList()[0];
                }

                if (bottomFieldOfSecondLastRow == null)
                {
                    return tableBottom;
                }

                int maxBottomOfSecondLastRow = bottomFieldOfSecondLastRow.Bounds.Bottom + 10;
                int predictedRowBottomOfSecondLastRow = getRowBottomFromVerticalLines(tableInfo
                , priamryColumnValues[priamryColumnValues.Count - 2]
                , tableLeft
                , tableRight
                , maxBottomOfSecondLastRow);

                int actualBottomOfTheSecondLastRow = bottomFieldOfSecondLastRow.Bounds.Bottom;
                bool isVericalLinesAreValidToFindRowBottom = (predictedRowBottomOfSecondLastRow - actualBottomOfTheSecondLastRow + 3) > 0;

                if (isVericalLinesAreValidToFindRowBottom)
                {
                    int maxRowBottomofLastRow = bottomTableRegion.Bounds.Bottom + (bottomTableRegion.Bounds.Height * 5);

                    tableBottom = getRowBottomFromVerticalLines(tableInfo
                        , priamryColumnValues[priamryColumnValues.Count - 1]
                        , tableLeft
                        , tableRight
                        , maxRowBottomofLastRow);
                }
            }

            return tableBottom;
        }

        private int getRowBottomFromVerticalLines(TableInfo tableInfo
            , Field priamryColumnCell
            , int tableLeft, int tableRight
            , int maxRowBottom)
        {
            int offset = 3;
            int rowBottom = 0;
            bool isLoopInitalized = false;
            List<Line> possibleLines = new List<Line>();

            do
            {
                Rectangle leftRectAtLastRow = new Rectangle(tableLeft
                    , priamryColumnCell.Bounds.Top + offset
                    , priamryColumnCell.Bounds.Width
                    , priamryColumnCell.Bounds.Height);

                Rectangle rightRectAtLastRow = new Rectangle(tableRight
                   , priamryColumnCell.Bounds.Top + offset
                   , priamryColumnCell.Bounds.Width
                   , priamryColumnCell.Bounds.Height);

                List<Line> lines = _rectOperations.FindAllVerticalLinesInBetweenRects(leftRectAtLastRow
                     , rightRectAtLastRow
                     , tableInfo.Lines);


                bool isLinesMatched = possibleLines.TrueForAll(
                    x => lines.FindIndex(line => Math.Abs(x.X1 - line.X1) > 5) > -1);

                if (isLoopInitalized
                    && (!isLinesMatched
                    || (leftRectAtLastRow.Bottom > (maxRowBottom + leftRectAtLastRow.Height))))
                {
                    break;
                }

                if (!isLoopInitalized)
                {
                    if (lines.Count == 0)
                    {
                        break;
                    }

                    isLoopInitalized = true;
                    possibleLines = lines;
                }
                if (leftRectAtLastRow.Top > priamryColumnCell.Bounds.Bottom)
                {
                    rowBottom = leftRectAtLastRow.Top;
                }

                offset += 3;
            }
            while (true);

            return rowBottom;
        }

        public Line GetRowLine(int tableLeft, int tableRight, int rowTop)
        {
            Line rowLine = new Line();

            rowLine.X1 = tableLeft;
            rowLine.X2 = tableRight;

            rowLine.Y1 = rowLine.Y2 = rowTop;

            return rowLine;
        }

        public int GetTableRight(Field rightColumnRegion, List<Line> lines)
        {
            return rightColumnRegion.Bounds.X + rightColumnRegion.Bounds.Width;
        }

        public int GetTableLeft(Field leftColumnRegion, List<Line> lines)
        {
            return leftColumnRegion.Bounds.X;
        }

        public List<Field> GetPrimaryColumnValues(FieldLayout primaryColumnLayout
            , TableInfo tableInfo
            , TableValue tableValue
            , bool isThisHeaderLessTable)
        {
            List<Field> masterColumnRows = new List<Field>();

            int masterColumnIndex = GetColumnIndex(tableInfo.Headers, primaryColumnLayout);

            if (masterColumnIndex < 0)
            {
                return masterColumnRows;
            }

            tableInfo.Layout.PrimaryColumn.Bounds = tableInfo.Headers[masterColumnIndex].Bounds;

            int colValueStartHeight = tableInfo.Layout.PrimaryColumn.Bounds.Bottom + 1;

            int tableBottom = getTableBottomFromFooter(tableInfo, colValueStartHeight);

            if (tableBottom == 0)
            {
                tableBottom = getTableBottomFromFields(tableInfo
                    , masterColumnIndex
                    , tableValue
                    , isThisHeaderLessTable);
            }

            int colHeight = tableBottom - colValueStartHeight + tableInfo.Layout.PrimaryColumn.Bounds.Height;

            Rectangle colRect = Rectangle.Empty;
            if (!isThisHeaderLessTable)
            {
                colRect = GetColumnValueRect(tableInfo, primaryColumnLayout, colHeight);
            }
            else
            {
                colRect = tableInfo.Headers[masterColumnIndex].Bounds.Clone();
            }

            colRect.Y = colValueStartHeight;
            colRect.Height = tableBottom - colRect.Y;

            //  masterColumnRows = tableInfo.Fields.FindAll(field => field.Type == FieldType.SystemField && colRect.Contains(field.Bounds) || colRect.IntersectsWith(field.Bounds));
            masterColumnRows = tableInfo.Fields.FindAll(field => field.Type == FieldType.SystemField && colRect.Contains(field.Bounds));
            masterColumnRows = masterColumnRows.OrderBy(field => field.Bounds.Top).ToList();

            bool areColumnsAtSameHeightExists = false;
            int minRowHeight = 10;

            do
            {
                areColumnsAtSameHeightExists = false;

                for (int i = 1; i < masterColumnRows.Count; i++)
                {
                    if ((Math.Abs(masterColumnRows[i - 1].Bounds.Top - masterColumnRows[i].Bounds.Top) < 10) ||
                        masterColumnRows[i - 1].Bounds.Height < minRowHeight || masterColumnRows[i].Bounds.Height < minRowHeight)
                    {
                        areColumnsAtSameHeightExists = true;
                        masterColumnRows.RemoveAt(i);
                        break;
                    }
                }
            }
            while (areColumnsAtSameHeightExists);

            return masterColumnRows;
        }

        public List<Field> GetCombinedFieldsWithGivenFieldWhichMatchWithGivenText(Field field, List<Field> tempFields, string text, double similarity)
        {
            Rectangle colRect = new Rectangle(field.Bounds.Left, field.Bounds.Top, field.Bounds.Width + 50, field.Bounds.Height);
            List<Field> horizontallyfieldsStorage = _fieldOperations.GetFieldsIntersectingWithRect(tempFields, colRect);
            horizontallyfieldsStorage = horizontallyfieldsStorage.FindAll(element => element.Text.StartsWith(text));
            List<Field> verticallyfieldsStorage = new List<Field>();
            List<Field> mergedFieldStorage = new List<Field>();
            foreach (Field elemnt in horizontallyfieldsStorage)
            {
                tempFields.Remove(elemnt);
            }

            for (int j = 2; j < 5; j++)
            {
                colRect = new Rectangle(field.Bounds.Left, field.Bounds.Top, field.Bounds.Width, field.Bounds.Height * j);
                List<Field> verticallyfields = _fieldOperations.GetFieldsIntersectingWithRect(tempFields, colRect);
                verticallyfields = verticallyfields.FindAll(element => element.Text.StartsWith(text));
                foreach (Field elemnt in verticallyfields)
                {
                    tempFields.Remove(elemnt);
                }
                verticallyfieldsStorage.AddRange(verticallyfields);
            }

            foreach (Field elemnt in horizontallyfieldsStorage)
            {
                mergedFieldStorage.Add(_fieldOperations.GetSingleFieldFromFields(new List<Field>() { field, elemnt }));
            }

            foreach (Field elemnt in verticallyfieldsStorage)
            {
                mergedFieldStorage.Add(_fieldOperations.GetSingleFieldFromFields(new List<Field>() { field, elemnt }));
            }
            return mergedFieldStorage;
        }

        private static Field getPreviousColumnHeader(TableInfo tableInfo, int columnHeaderIndex)
        {
            Field columnHeader = tableInfo.Headers[columnHeaderIndex];

            Field prviousColumnHeader = tableInfo.Headers.LastOrDefault(field => field.Bounds.Right <= columnHeader.Bounds.Left);

            return prviousColumnHeader;
        }

        private static Field getNextColumnHeader(TableInfo tableInfo, int columnHeaderIndex)
        {
            Field columnHeader = tableInfo.Headers[columnHeaderIndex];

            Field nextColumnHeader = tableInfo.Headers.FirstOrDefault(field => field.Bounds.Left >= columnHeader.Bounds.Right);

            return nextColumnHeader;
        }

        private static bool isValidColumnRect(Field nextColumnHeader, Field prevColumnHeader, int minLeft, int maxRight, Rectangle tempColRect)
        {
            if (prevColumnHeader != null && tempColRect.Left < minLeft)
            {
                return false;
            }

            if (nextColumnHeader != null && tempColRect.Right > maxRight)
            {
                return false;
            }

            return true;
        }

        private static bool hasColumns(TableInfo tableInfo)
        {
            return (tableInfo?.Layout?.Columns?.Count ?? 0) > 0;
        }

        private void fillTableHeaders(TableValue tableValue, TableInfo tableInfo)
        {
            VBotLogger.Trace(() => string.Format("[fillTableHeaders] Entered."));
            tableValue.Headers = tableInfo.ColumnDefs.Columns.FindAll(c => c.IsDeleted == false);
        }



        private static List<Line> getCurrentPageLines(Rectangle currentPageRect, List<Line> lines)
        {
            List<Line> result = new List<Line>();

            if (lines == null || lines.Count == 0)
            { return result; }

            foreach (Line line in lines)
            {
                if (currentPageRect.Contains(line))
                {
                    result.Add(line);
                }
            }

            return result;
        }

        private void rotateFieldsAsPerTheirAngle(List<Field> fields, Point referencePoint)
        {
            foreach (Field field in fields)
            {
                if (field.Angle == decimal.Zero)
                {
                    continue;
                }

                field.Bounds = new Rectangle(rotatePoint(field.Bounds.Location, referencePoint, -(double)field.Angle), field.Bounds.Size);
            }
        }

        private void ReAssignTableColumnLabels(TableInfo source, TableInfo destination)
        {
            if (source == null || destination == null || source.Layout == null || destination.Layout == null)
            {
                return;
            }

            var sourceColumns = source.Layout.Columns.ToDictionary(x => x.Id);

            foreach (FieldLayout column in destination.Layout.Columns)
            {
                if (!sourceColumns.ContainsKey(column.Id))
                {
                    continue;
                }

                column.Label = sourceColumns[column.Id].Label;
            }
        }

        private List<Field> GetCurrentPageFieldExceptPageFooter(Rectangle pageBoundry, List<Field> allPagesField)
        {
            List<Field> pageFields = _fieldOperations.GetAllSystemFieldsWithinRect(allPagesField, pageBoundry);

            int possibleFooterHeight = 200;
            Rectangle possiblePagefooterBoarder = new Rectangle(
                                                         0,
                                                         pageBoundry.Bottom - possibleFooterHeight,
                                                         pageBoundry.Width,
                                                         possibleFooterHeight);

            List<Field> possibleFooterFields = _fieldOperations.GetAllSystemFieldsWithinRect(pageFields, possiblePagefooterBoarder);

            List<Field> actualFooterFields = GetActualFooterFields(possibleFooterFields, possiblePagefooterBoarder);

            foreach (Field field in actualFooterFields)
            {
                pageFields.Remove(field);
            }

            return pageFields;
        }

        private List<Field> GetActualFooterFields(List<Field> possibleFooterField, Rectangle pagefooterRect)
        {
            List<string> footerRegexes = new List<string>()
                                            {
                                                @"(page)[ ]+\d*[ ]+of[ ]+\d*",
                                                @"(page)[ ]*\d*[ ]*/[ ]*\d*",
                                                @"(page)[ ]*\d*"
                                            };

            List<Field> actualFooterFields = new List<Field>();
            foreach (Field field in possibleFooterField)
            {
                foreach (string pattern in footerRegexes)
                {
                    if (Regex.IsMatch(field.Text.ToLower(), pattern))
                    {
                        actualFooterFields.Add(field);
                        break;
                    }
                }
            }

            Rectangle actualFooterBoarder = _fieldOperations.GetBoundsFromFields(actualFooterFields);
            List<Field> fieldsToBeRemoved = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(possibleFooterField, new Rectangle(0, actualFooterBoarder.Top, pagefooterRect.Width, actualFooterBoarder.Height));

            return fieldsToBeRemoved;
        }

        private int GetContinuedHeaderlessTableStartPosition(TableInfo pageSpecificTableInfo)
        {
            int top = pageSpecificTableInfo.Fields.Min(field => field.Bounds.Top);

            int bottom = pageSpecificTableInfo.Fields.Max(field => field.Bounds.Bottom);
            bool isNotIntersected = true;

            if (pageSpecificTableInfo.Headers.Count == 0)
            {
                return -1;
            }

            int minimumNumberOfColumnNeededToDecidePossibiltyOfTable = (int)((pageSpecificTableInfo.Headers.Count / 2) + 1);
            for (int i = 0; i < bottom; i++)
            {
                isNotIntersected = true;
                Rectangle rectLeft = new Rectangle();
                Rectangle rectRight = new Rectangle();

                foreach (Field header in pageSpecificTableInfo.Headers)
                {
                    rectLeft = new Rectangle(header.Bounds.X - 1, top - 2 + i, 1, header.Bounds.Height);
                    rectRight = new Rectangle(header.Bounds.X + header.Bounds.Width + 1, top - 2 + i, 1, header.Bounds.Height);
                    if (_fieldOperations.GetFieldsIntersectingWithRect(pageSpecificTableInfo.Fields, rectLeft).Count > 0 || _fieldOperations.GetFieldsIntersectingWithRect(pageSpecificTableInfo.Fields, rectRight).Count > 0)
                    {
                        isNotIntersected = false;
                        break;
                    }
                }
                if (isNotIntersected)
                {
                    bool isNotEmptySpace = false;
                    int numberOfCellContainData = 0;
                    foreach (Field header in pageSpecificTableInfo.Headers)
                    {
                        Rectangle rect = new Rectangle(header.Bounds.X, top + i, header.Bounds.Width, header.Bounds.Height);
                        if (_fieldOperations.GetFieldsWithinBoundField(pageSpecificTableInfo.Fields, rect).Count > 0)
                        {
                            numberOfCellContainData++;
                            if (numberOfCellContainData >= minimumNumberOfColumnNeededToDecidePossibiltyOfTable)
                            {
                                isNotEmptySpace = true;
                                break;
                            }
                        }
                    }
                    if (isNotEmptySpace)
                    {
                        top = rectLeft.Top;
                        return top;
                    }
                }
            }

            return -1;
        }

        private void appendTableValueForGivenTableInfo(FieldLayout primaryColumnLayout
            , TableInfo tableInfo
            , TableValue tableValue
            , bool isThisHeaderLessTable)
        {
            if (tableInfo.Headers.Count > 0)
            {
                List<Field> primaryColumnRowValues = GetPrimaryColumnValues(primaryColumnLayout
                    , tableInfo
                    , tableValue
                    , isThisHeaderLessTable);

                if (primaryColumnRowValues.Count > 0)
                {
                    var lastCellValueOfPrimaryColumn = primaryColumnRowValues[primaryColumnRowValues.Count - 1];

                    Rectangle tableRect = GetTableRect(tableInfo
                        , primaryColumnLayout
                        , lastCellValueOfPrimaryColumn
                        , primaryColumnRowValues);

                    List<int> rowTop = GetRowTops(primaryColumnRowValues
                        , primaryColumnLayout
                        , tableRect
                        , tableInfo.Lines);

                    List<Line> headerLeftLines = new List<Line>();
                    List<Line> headerRightLines = new List<Line>();
                    List<Rectangle> headerBounds = new List<Rectangle>();

                    for (int i = 0; i < tableValue.Headers.Count; i++)
                    {
                        FieldLayout colLayout = GetColumnLayout(tableInfo, tableValue.Headers[i].Id);

                        if (!isThisHeaderLessTable)
                        {
                            updateHeaderBoundForGivenFieldLayout(tableInfo
                              , colLayout
                              , tableRect.Height);
                        }

                        Line leftLine = new Line();
                        Line rightLine = new Line();
                        Rectangle headerBound = new Rectangle();
                        int columnIndex = GetColumnIndex(tableInfo.Headers, colLayout);

                        if (columnIndex > -1)
                        {
                            headerBound = tableInfo.Headers[columnIndex].Bounds.Clone();
                            leftLine = new Line(headerBound.Left, headerBound.Left
                                , headerBound.Top, headerBound.Bottom);
                            rightLine = new Line(headerBound.Right, headerBound.Right
                                , headerBound.Top, headerBound.Bottom);
                        }

                        headerLeftLines.Add(leftLine);
                        headerRightLines.Add(rightLine);
                        headerBounds.Add(headerBound);
                    }
                    int noOfRowInTableValue = tableValue.Rows.Count;
                    for (int j = 0; j < rowTop.Count; j++)
                    {
                        tableValue.Rows.Add(new DataRecord());
                        tableValue.Rows[noOfRowInTableValue + j].Fields = new List<FieldData>(tableInfo.Headers.Count - 1);

                        for (int i = 0; i < tableValue.Headers.Count; i++)
                        {
                            int offset = 1;

                            int cellTop = rowTop[j] - offset;

                            if (j == 0 && !headerBounds[i].IsEmpty)
                            {
                                cellTop = headerBounds[i].Bottom + offset;
                            }

                            int cellBottom = j < rowTop.Count - 1 ? rowTop[j + 1] - offset : tableRect.Bottom;

                            Rectangle cellRect = new Rectangle(headerLeftLines[i].X1, cellTop, headerRightLines[i].X1 - headerLeftLines[i].X1, cellBottom - cellTop);

                            FieldData fieldData = GetFieldDataForCell(tableInfo.Fields, tableValue.Headers[i], cellRect);

                            if (string.IsNullOrWhiteSpace(fieldData.Value.Field.Text))
                            {
                                if (fieldData.Field.IsRequired)
                                {
                                    VBotLogger.Trace(() => $"[ExtractFieldValues] Unable to extract column {fieldData?.Field?.Name} cell R{j}C{i}.");
                                }
                                else
                                {
                                    fieldData.Value.Field.Text = fieldData.Field.DefaultValue;
                                }
                            }

                            tableValue.Rows[noOfRowInTableValue + j].Fields.Add(fieldData);
                        }
                    }
                }
                else
                {
                    VBotLogger.Trace(() => "[ExtractFieldValues] Unable to identify primary row values.");
                }
            }
            else
            {
                VBotLogger.Trace(() => "[ExtractFieldValues] Unable to identify table headers.");
            }
        }

        private void updateHeaderBoundForGivenFieldLayout(TableInfo tableInfo, FieldLayout colLayout, int tableRectHeight)
        {
            int columnIndex = GetColumnIndex(tableInfo.Headers, colLayout);

            if (columnIndex > -1)
            {
                Field header = tableInfo.Headers[columnIndex];

                if (colLayout.ValueBounds == Rectangle.Empty)
                {
                    Rectangle colRect = GetColumnValueRect(tableInfo, colLayout, tableRectHeight);

                    int mostLeft = Math.Min(colRect.Left, header.Bounds.Left);
                    int mostRight = Math.Max(colRect.Right, header.Bounds.Right);

                    header.Bounds = new Rectangle(mostLeft
                        , header.Bounds.Top
                        , mostRight - mostLeft
                        , header.Bounds.Height);
                }
            }
        }

        private void updateHeaderBoundOfGivenTable(TableInfo tableInfo, TableValue tableValue, Field bottomIdentifiedField, bool isThisHeaderLessTable)
        {
            Rectangle tableRect = GetTableRect(tableInfo, tableInfo.Layout.PrimaryColumn, bottomIdentifiedField);

            foreach (var header in tableValue.Headers)
            {
                FieldLayout colLayout = GetColumnLayout(tableInfo, header.Id);

                if (!isThisHeaderLessTable)
                {
                    updateHeaderBoundForGivenFieldLayout(tableInfo, colLayout, tableRect.Height);
                }
            }
        }

        private FieldData GetFieldDataForCell(List<Field> fields, FieldDef headerFieldDef, Rectangle cellRect)
        {
            StatisticsLogger.Log($"Extracting cell start : {headerFieldDef.Name}");
            FieldData fieldData = new FieldData();

            fieldData.Field = new FieldDef();
            fieldData.Field = headerFieldDef;

            StatisticsLogger.Log($"Extracting cell start ~ First Try");
            fieldData.Value = GetCellValue(fields, cellRect);
            StatisticsLogger.Log($"Extracting cell end ~ First Try");

            if (_rescan != null)
            {
                Field valueField = _rescan.GetRescanValue(fieldData.Value.Field, fieldData.Field);
                if (valueField != null && !string.IsNullOrEmpty(valueField.Text))
                {
                    fieldData.Value.Field = valueField;
                }
            }

            StatisticsLogger.Log($"Extracting cell end : {headerFieldDef.Name}");

            return fieldData;
        }

        private static FieldLayout GetColumnLayout(TableInfo tableInfo, string colId)
        {
            FieldLayout colLayout = tableInfo.Layout.Columns.FirstOrDefault(col => col.FieldId == colId);

            if (colLayout == null)
            {
                colLayout = new FieldLayout();
            }

            return colLayout;
        }

        private static bool IsPrimaryColumnDefined(FieldLayout primaryColumnLayout)
        {
            return primaryColumnLayout != null && !string.IsNullOrWhiteSpace(primaryColumnLayout.Label);
        }

        private Field GetPrimaryColumnField(FieldLayout primaryColumnLayout, TableInfo tableInfo)
        {
            Field masterColField = null;

            List<Field> masterColFields = _fieldOperations.GetAllBestMatchingFields(tableInfo.Fields
                 , primaryColumnLayout.Label
                 , primaryColumnLayout.SimilarityFactor);

            if (masterColFields.Count > 0)
            {
                masterColField = masterColFields.OrderBy(
                   field =>
                       _rectOperations.GetCenterDistanceBetweenRects(field.Bounds
                           , primaryColumnLayout.Bounds))
                   .ToList()[0];
            }

            if (masterColField == null)
            {
                masterColField = _fieldOperations.FindBestMatchingFieldFromCombiningMultipleSIR(primaryColumnLayout.Label
                , primaryColumnLayout.SimilarityFactor
                , tableInfo.Fields
                , null);
            }

            if (masterColField != null)
            {
                if (!string.IsNullOrEmpty(masterColField.Text)
                    && !string.IsNullOrEmpty(primaryColumnLayout.Label)
                    && primaryColumnLayout.Label.Contains("|"))
                {
                    primaryColumnLayout.Label = masterColField.Text;
                }
                return masterColField;
            }

            return null;
        }

        private List<int> GetRowTops(List<Field> masterColumnRowValues
            , FieldLayout primaryColumnLayout
            , Rectangle tableRect
            , List<Line> lines)
        {
            List<int> rowTop = new List<int>();

            for (int i = 0; i < masterColumnRowValues.Count; i++)
            {

                var currentRowBounds = masterColumnRowValues[i].Bounds;
                int rowTopValue = currentRowBounds.Top;
                Rectangle previousRowRect = Rectangle.Empty;
                if (i == 0)
                {
                    previousRowRect = new Rectangle(currentRowBounds.Left
                        , tableRect.Top
                        , currentRowBounds.Width, 1);
                }
                else
                {
                    previousRowRect = masterColumnRowValues[i - 1].Bounds;
                }

                List<Line> foundLines = _rectOperations.FindAllHorizontalLinesInBetweenRects(previousRowRect
                      , currentRowBounds
                      , lines);

                if (foundLines.Count > 0)
                {
                    rowTopValue = foundLines.Max(line => line.Y1);
                }

                Line rowLine = GetRowLine(tableRect.Left
                    , tableRect.Right
                    , rowTopValue - 1);

                rowTop.Add(rowLine.Y1);
            }

            return rowTop;
        }

        private Rectangle GetColumnValueRect(TableInfo tableInfo, FieldLayout columnLayout, int columnHeight)
        {
            int columnIndex = GetColumnIndex(tableInfo.Headers, columnLayout);

            if (columnIndex < 0)
            {
                return new Rectangle();
            }

            Field columnHeader = tableInfo.Headers[columnIndex];
            Field nextColumnHeader = getNextColumnHeader(tableInfo, columnIndex);
            Field prevColumnHeader = getPreviousColumnHeader(tableInfo, columnIndex);

            Rectangle colRect = columnHeader.Bounds;

            colRect.Y = colRect.Top + colRect.Height + 5;
            colRect.Height = columnHeight - colRect.Height - 5;

            List<Field> colFields = new List<Field>();

            if (prevColumnHeader == null)
            {
                List<Field> allLeftFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - columnHeader.Bounds.Top) < columnHeader.Bounds.Height && fi.Bounds.Right < (columnHeader.Bounds.Left));

                //decide any field or line between header field and left side field and assign thier right value
                if (allLeftFields != null && allLeftFields.Count > 0)
                {
                    allLeftFields = allLeftFields.OrderByDescending(fi => fi.Bounds.Right).ToList();

                    prevColumnHeader = allLeftFields[0];
                }
            }

            int minLeft = getMinLeftForGivenHeaderUsingPreviousHeaderField(tableInfo, columnHeader, prevColumnHeader);

            if (nextColumnHeader == null)
            {
                //get right side fields

                List<Field> allRightFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - columnHeader.Bounds.Top) < columnHeader.Bounds.Height && fi.Bounds.Left > (columnHeader.Bounds.Left + columnHeader.Bounds.Width - 5));

                allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();


                if (allRightFields != null && allRightFields.Count > 0)
                {
                    allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();

                    nextColumnHeader = allRightFields[0];
                }
            }

            int maxRight = getMaxRightForGivenHeaderUsingNextHeaderField(tableInfo, columnHeader, nextColumnHeader);

            Rectangle tempColRect = colRect;

            do
            {
                tempColRect.Inflate(1, 0);

                colFields = _fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, tempColRect);
                List<Field> colFields_regions = new List<Field>();
                colFields_regions = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(tableInfo.Regions, tempColRect);
                if (colFields_regions.Count > 0)
                {
                    colFields.AddRange(colFields_regions);
                }

                if (colFields.Count > 0)
                {
                    tempColRect = getColumnRectFromIntersectedFields(tableInfo, colFields, tempColRect);

                    if (isValidColumnRect(nextColumnHeader, prevColumnHeader, minLeft, maxRight, tempColRect))
                    {
                        return tempColRect;
                    }
                }
            }
            while (tempColRect.Left > minLeft && tempColRect.Right < maxRight);

            return colRect;
        }

        private int getMaxRightForGivenHeaderUsingNextHeaderField(TableInfo tableInfo, Field columnHeader, Field nextColumnHeader)
        {
            int maxRight = 3 * columnHeader.Bounds.Right;



            if (nextColumnHeader != null)
            {
                int top = _rectOperations.GetMostTopOfTwoRectangle(nextColumnHeader.Bounds, columnHeader.Bounds);
                int bottom = _rectOperations.GetMostBottomOfTwoRectangle(nextColumnHeader.Bounds, columnHeader.Bounds);

                Rectangle emptySpaceRectBetweenHeader = new Rectangle(columnHeader.Bounds.Right + 5, top, nextColumnHeader.Bounds.Left - columnHeader.Bounds.Right - 5, bottom - top);

                List<Field> extraHeader = _fieldOperations.GetFieldsWithinBoundField(tableInfo.Fields, emptySpaceRectBetweenHeader);
                extraHeader.RemoveAll(field => field.Text.Length <= 1);

                if (extraHeader.Count > 0)
                {
                    Field mostRightField = extraHeader.OrderByDescending(field => field.Bounds.Left).ToList()[0];
                    maxRight = mostRightField.Bounds.Left;
                }
                else
                {
                    maxRight = nextColumnHeader.Bounds.Left;
                }
            }

            return maxRight;
        }

        private int getMinLeftForGivenHeaderUsingPreviousHeaderField(TableInfo tableInfo, Field columnHeader, Field prevColumnHeader)
        {
            int minLeft = 0;



            if (prevColumnHeader != null)
            {
                int top = _rectOperations.GetMostTopOfTwoRectangle(prevColumnHeader.Bounds, columnHeader.Bounds);
                int bottom = _rectOperations.GetMostBottomOfTwoRectangle(prevColumnHeader.Bounds, columnHeader.Bounds);

                Rectangle emptySpaceRectBetweenHeader = new Rectangle(prevColumnHeader.Bounds.Right + 5, top, columnHeader.Bounds.Left - prevColumnHeader.Bounds.Right - 5, bottom - top);

                List<Field> extraHeader = _fieldOperations.GetFieldsWithinBoundField(tableInfo.Fields, emptySpaceRectBetweenHeader);
                extraHeader.RemoveAll(field => field.Text.Length <= 1);

                if (extraHeader.Count > 0)
                {
                    Field mostRightField = extraHeader.OrderByDescending(field => field.Bounds.Right).ToList()[0];
                    minLeft = mostRightField.Bounds.Right;
                }
                else
                {
                    minLeft = prevColumnHeader.Bounds.Right;
                }
            }

            return minLeft;
        }

        private Rectangle getColumnRectFromIntersectedFields(TableInfo tableInfo, List<Field> colFields, Rectangle originalColRect)
        {
            Rectangle colRect = _fieldOperations.GetBoundsFromFields(colFields);

            colRect = new Rectangle(colRect.Left, originalColRect.Top, colRect.Width, originalColRect.Height);

            int nextCount = colFields.Count;
            int prevCount = 0;

            do
            {
                prevCount = nextCount;

                colFields = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(tableInfo.Fields, colRect);

                Rectangle tempRect = _fieldOperations.GetBoundsFromFields(colFields);
                Rectangle mergedRect = new Rectangle(tempRect.Left, originalColRect.Top, tempRect.Width, originalColRect.Height);

                colRect = mergedRect;
                nextCount = colFields.Count;
            }
            while (nextCount > prevCount);

            return colRect;
        }

        private int getTableBottomFromFields(TableInfo tableInfo, int masterColumnIndex, TableValue tableValue, bool isThisHeaderLessTable)
        {
            int maxBottom = tableInfo.Fields.Max(x => x.Bounds.Bottom);

            int tableBottom = tableInfo.Headers[masterColumnIndex].Bounds.Bottom;

            Field prevRowField = tableInfo.Headers[masterColumnIndex].Clone();

            int maxLeft = prevRowField.Bounds.Left;
            int maxRight = prevRowField.Bounds.Right;
            //get left side line
            List<Field> allLeftFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - prevRowField.Bounds.Top) < prevRowField.Bounds.Height && fi.Bounds.Right < (prevRowField.Bounds.Left));

            //decide any field or line between header field and left side field and assign thier right value
            if (allLeftFields != null && allLeftFields.Count > 0)
            {
                allLeftFields = allLeftFields.OrderByDescending(fi => fi.Bounds.Right).ToList();

                var leftSideField = allLeftFields[0];
                List<Line> foundLines = _rectOperations.FindAllVerticalLinesInBetweenRects(prevRowField.Bounds
                        , leftSideField.Bounds
                        , tableInfo.Lines);

                //decide min left
                if (foundLines.Count > 0)
                {
                    maxLeft = foundLines.OrderByDescending(x => x.X2).ToList()[0].X2;
                }
                else
                {
                    maxLeft = leftSideField.Bounds.Right;
                }
            }
            //get right side line

            List<Field> allRightFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - prevRowField.Bounds.Top) < prevRowField.Bounds.Height && fi.Bounds.Left > (prevRowField.Bounds.Left + prevRowField.Bounds.Width - 5));

            allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();

            //decide any field or line between header field and right side field and assign thier left value
            if (allRightFields != null && allRightFields.Count > 0)
            {
                allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();

                var rightSideField = allRightFields[0];
                List<Line> foundLinesRight = _rectOperations.FindAllVerticalLinesInBetweenRects(prevRowField.Bounds
                        , rightSideField.Bounds
                        , tableInfo.Lines);

                //decide max right
                if (foundLinesRight.Count > 0)
                {
                    maxRight = foundLinesRight.OrderBy(x => x.X1).ToList()[0].X1;
                }
                else
                {
                    maxRight = rightSideField.Bounds.Left;
                }
            }
            prevRowField.Bounds = new Rectangle(maxLeft, prevRowField.Bounds.Top, maxRight - maxLeft, prevRowField.Bounds.Height);

            Field nextRowField = _fieldOperations.GetNearestBottomField(prevRowField, tableInfo.Fields, 5);

            while (nextRowField != null)
            {
                updateHeaderBoundOfGivenTable(tableInfo, tableValue, nextRowField, isThisHeaderLessTable);

                tableBottom = nextRowField.Bounds.Bottom;

                prevRowField = nextRowField;
                nextRowField = _fieldOperations.GetNearestBottomField(prevRowField, tableInfo.Fields, 10);

                if (nextRowField == null)
                {
                    const int stepToMoveDown = 10;
                    int currentIncrementedStep = 0;
                    do
                    {
                        currentIncrementedStep += stepToMoveDown;
                        nextRowField = _fieldOperations.GetNearestBottomField(prevRowField, tableInfo.Fields, currentIncrementedStep);
                    } while (nextRowField == null && (prevRowField.Bounds.Bottom + currentIncrementedStep) < maxBottom);

                    if (nextRowField != null)
                    {
                        int top = nextRowField.Bounds.Top;

                        /**
                         * Below Line is For number of Column value need to qualify possibility of row/table present So  it is based on all column.
                         * Currently Code does not care whether header (table column) is  optional or not.
                         * TODO: It is left to be decided what modification need to be made for optional column.
                         */
                        int minimumNumberOfColumnNeededToDecidePossibiltyOfTable = (int)((tableInfo.Headers.Count / 2) + 1);
                        int numberOfCellNotIntersected = 0;
                        foreach (Field header in tableInfo.Headers)
                        {
                            var rectLeft = new Rectangle(header.Bounds.X - 1, top - 2, 1, header.Bounds.Height);
                            var rectRight = new Rectangle(header.Bounds.X + header.Bounds.Width + 1, top - 2, 1, header.Bounds.Height);
                            if (!(_fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, rectLeft).Count > 0 || _fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, rectRight).Count > 0))
                            {
                                numberOfCellNotIntersected++;
                            }
                        }
                        if (numberOfCellNotIntersected > minimumNumberOfColumnNeededToDecidePossibiltyOfTable)
                        {
                            bool isNotEmptySpace = false;
                            int numberOfCellContainData = 0;
                            foreach (Field header in tableInfo.Headers)
                            {
                                Rectangle rect = new Rectangle(header.Bounds.X, top, header.Bounds.Width, header.Bounds.Height);
                                if (_fieldOperations.GetFieldsWithinBoundField(tableInfo.Fields, rect).Count > 0)
                                {
                                    numberOfCellContainData++;
                                    if (numberOfCellContainData >= minimumNumberOfColumnNeededToDecidePossibiltyOfTable)
                                    {
                                        isNotEmptySpace = true;
                                        break;
                                    }
                                }
                            }
                            if (!isNotEmptySpace)
                            {
                                nextRowField = null;
                            }
                        }
                        else
                        {
                            nextRowField = null;
                        }
                    }
                }
            }

            return tableBottom;
        }

        private int getTableBottomFromFooter(TableInfo tableInfo, int tableRowStartPosition)
        {
            int tableBottom = 0;

            if (string.IsNullOrWhiteSpace(tableInfo.Layout.Footer.Label))
            {
                return tableBottom;
            }

            List<Field> footerFields = _fieldOperations.GetAllBestMatchingFields(tableInfo.Fields
                , tableInfo.Layout.Footer.Label
                , tableInfo.Layout.Footer.SimilarityFactor);

            footerFields.RemoveAll(field => field.Bounds.Top <= tableRowStartPosition);

            Field bottomField = null;

            if (footerFields.Count > 0)
            {
                footerFields = footerFields.OrderBy(field => field.Bounds.Top).ToList();
                bottomField = footerFields[0];
            }

            if (bottomField != null)
            {
                tableBottom = bottomField.Bounds.Top - 10;
            }
            else
            {
                bottomField = tableInfo.Fields.FirstOrDefault(field => field.Bounds.Top > tableRowStartPosition &&
                                                            field.Text.ToLower().StartsWith(tableInfo.Layout.Footer.Label.ToLower()));

                if (bottomField != null)
                {
                    tableBottom = bottomField.Bounds.Top - 10;
                }
            }

            return tableBottom;
        }

        public int GetColumnIndex(List<Field> headers, FieldLayout columnLayout)
        {
            if (string.IsNullOrEmpty(columnLayout.Label))
            {
                return -1;
            }

            List<Field> headerFields = headers.FindAll(field => field.Text.PartiallyEquals(columnLayout.Label, columnLayout.SimilarityFactor));
            headerFields = headerFields.OrderByDescending(field => field.Text.GetSimilarity(columnLayout.Label)).ToList();

            if (headerFields.Count > 0)
            {
                return headers.FindIndex(field => field.Text.Equals(headerFields[0].Text));
            }
            else
            {
                return -1;
            }
        }

        public List<Field> GetTableHeaders(TableInfo tableInfo, Field primaryColField)
        {
            List<Field> tableHeaders = new List<Field>();

            int headerBottom = 0;
            int allowedDistance = GetAllowedDistanceBetweenHeaderAndPrimaryColumn(tableInfo);
            foreach (FieldLayout fieldLayout in tableInfo.Layout.Columns)
            {
                Field columnField = getColumnFieldFromLayout(fieldLayout, primaryColField, tableInfo.Fields, allowedDistance);

                if (columnField.Text.Length > 1)
                {
                    if (columnField.Bounds.Bottom > headerBottom)
                    {
                        headerBottom = columnField.Bounds.Bottom;
                    }

                    fieldLayout.Bounds = columnField.Bounds;
                    if (fieldLayout.ValueBounds != Rectangle.Empty
                    && fieldLayout.Id != tableInfo.Layout.PrimaryColumn.Id)
                    {
                        //columnField.Bounds = _fieldOperations.FindRelativeBound(fieldLayout);
                    }

                    tableHeaders.Add(columnField);
                }
            }

            for (int i = 0; i < tableHeaders.Count; i++)
            {
                Rectangle headerRect = tableHeaders[i].Bounds;
                headerRect.Height = headerBottom - headerRect.Top;

                tableHeaders[i].Bounds = headerRect;
            }

            tableHeaders = tableHeaders.OrderBy(field => field.Bounds.Left).ToList();

            return tableHeaders;
        }
        private int GetAllowedDistanceBetweenHeaderAndPrimaryColumn(TableInfo tableInfo)
        {
            List<FieldLayout> columns = tableInfo.Layout.Columns.ConvertAll(x => x.Clone());
            columns = columns.OrderBy(x => x.Bounds.Y).ToList();
            int maxYIndex = columns.Count - 1;
            if (maxYIndex < 0)
                return 0;
            int allowedDist = columns[maxYIndex].Bounds.Y - tableInfo.Layout.PrimaryColumn.Bounds.Y;
            return allowedDist;
        }
        private Field getColumnFieldFromLayout(FieldLayout columnLayout, Field primaryColField, List<Field> fields, int allowedDistance)
        {
            List<Field> similarColFields = _fieldOperations.GetAllBestMatchingFields(fields, columnLayout.Label, columnLayout.SimilarityFactor);
            Field columnField = new Field();

            if (similarColFields.Count > 0)
            {
                int maxAllowedDistance = primaryColField.Bounds.Height;
                Field colFieldAtSameHeight = similarColFields.FirstOrDefault(field => _rectOperations.GetYDistanceBetweenRects(field.Bounds, primaryColField.Bounds) < maxAllowedDistance);
                if (colFieldAtSameHeight == null)
                {
                    maxAllowedDistance = allowedDistance + primaryColField.Bounds.Height;
                    colFieldAtSameHeight = similarColFields.FirstOrDefault(field => _rectOperations.GetYDistanceBetweenRects(field.Bounds, primaryColField.Bounds) < maxAllowedDistance);
                }

                if (colFieldAtSameHeight != null)
                {
                    columnField = colFieldAtSameHeight;
                }
            }

            if (string.IsNullOrEmpty(columnField.Text) || columnField.Text.Length <= 1)
            {
                columnField = _fieldOperations
                    .FindBestMatchingFieldFromCombiningMultipleSIR(columnLayout.Label
                        , columnLayout.SimilarityFactor
                        , fields, primaryColField);
            }

            if (!string.IsNullOrEmpty(columnField.Text) && !string.IsNullOrEmpty(columnLayout.Label) && columnLayout.Label.Contains("|"))
            {
                columnLayout.Label = columnField.Text;
            }

            return columnField;
        }

        private Point rotatePoint(Point pointToRotate, Point centerPoint, double angleInDegrees)
        {
            double angleInRadians = Math.Atan(angleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new Point
            {
                X =
                    (int)
                    (cosTheta * (pointToRotate.X - centerPoint.X) -
                    sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y =
                    (int)
                    (sinTheta * (pointToRotate.X - centerPoint.X) +
                    cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
        }


    }
}
