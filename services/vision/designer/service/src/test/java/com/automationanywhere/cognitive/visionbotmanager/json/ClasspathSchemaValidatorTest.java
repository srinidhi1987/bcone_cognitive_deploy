package com.automationanywhere.cognitive.visionbotmanager.json;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.automationanywhere.cognitive.visionbotmanager.exception.SchemaValidationException;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.FileSystems;
import java.util.Collections;
import java.util.Objects;
import org.testng.annotations.Test;

public class ClasspathSchemaValidatorTest {

  @Test
  public void testChecks() {
    // GIVEN
    ObjectMapper om = new ObjectMapper();
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator",
        ".json"
    );
    v.loadJsonSchemas();

    // WHEN
    Throwable checkKeyException = catchThrowable(() -> v.checkKey("Credentials"));
    Throwable checkJsonException = catchThrowable(() -> v.checkJson(
        om.writeValueAsString(new CredentialsDto("un", "pw")),
        "Credentials")
    );

    // THEN
    assertThat(checkKeyException).isNull();
    assertThat(checkJsonException).isNull();
  }

  @Test
  public void testErrors() {
    // GIVEN
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/com/automationanywhere/cognitive/visionbotmanager",
        ".class"
    );

    // WHEN
    Throwable throwable = catchThrowable(v::loadJsonSchemas);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessageStartingWith("Failed to load schema from");

    // WHEN
    throwable = catchThrowable(() -> v.checkKey("key"));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessage("Key key is unknown");
  }

  @Test
  public void testChecksFromJar() {
    // GIVEN
    ObjectMapper om = new ObjectMapper();

    ClassLoader classLoader = getClass().getClassLoader();
    URL jar = classLoader.getResource("classpath-schema-validator-test.jar");
    addJar(jar);

    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator-jar",
        ".json"
    );
    v.loadJsonSchemas();

    // WHEN
    Throwable checkKeyException = catchThrowable(() -> v.checkKey("Credentials"));
    Throwable checkJsonException = catchThrowable(() -> v.checkJson(
        om.writeValueAsString(new CredentialsDto("un", "pw")),
        "Credentials")
    );

    // THEN
    assertThat(checkKeyException).isNull();
    assertThat(checkJsonException).isNull();
  }

  @Test
  public void testErrorsFromJar() {
    // GIVEN
    ClassLoader classLoader = getClass().getClassLoader();
    URL jar = classLoader.getResource("classpath-schema-validator-test.jar");
    addJar(jar);

    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/com/automationanywhere/cognitive/visionbotmanager",
        ".class"
    );

    // WHEN
    Throwable throwable = catchThrowable(v::loadJsonSchemas);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessageStartingWith("Failed to load schema from");

    // WHEN
    throwable = catchThrowable(() -> v.checkKey("key"));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessage("Key key is unknown");
  }

  @Test
  public void testResourceNotFoundThrowException() {
    // GIVEN
    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/not-found-prefix", ".json"
    );

    // WHEN
    Throwable throwable = catchThrowable(v::loadJsonSchemas);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(SchemaValidationException.class)
        .hasMessage("Resource not found with prefix: /not-found-prefix");
  }

  @Test
  public void testFileSystemAlreadyExistsException() throws Exception {
    // GIVEN
    ClassLoader classLoader = getClass().getClassLoader();
    URL jar = classLoader.getResource("classpath-schema-validator-test.jar");
    addJar(jar);

    ClasspathSchemaValidator v = new ClasspathSchemaValidator(
        "/classpath-schema-validator-jar", ".class"
    );

    URL resource = ClasspathSchemaValidator.class.getResource("/classpath-schema-validator-jar");

    try (
        @SuppressWarnings("unused")
        FileSystem fs = FileSystems.newFileSystem(resource.toURI(), Collections.emptyMap())
    ) {

      // WHEN
      Throwable throwable = catchThrowable(v::loadJsonSchemas);

      // THEN
      assertThat(throwable).isNotNull().isInstanceOf(FileSystemAlreadyExistsException.class);
    }
  }

  private void addJar(URL jarFile) {
    try {
      URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
      Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
      method.setAccessible(true);
      method.invoke(sysloader, jarFile);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public class CredentialsDto {

    public final String username;
    public final String password;

    public CredentialsDto(
        @JsonProperty("username") final String username,
        @JsonProperty("password") final String password
    ) {
      this.username = username;
      this.password = password;
    }

    @Override
    public boolean equals(final Object o) {
      boolean result = o == this;

      if (!result && o != null && o.getClass() == getClass()) {
        CredentialsDto that = (CredentialsDto) o;

        result = Objects.equals(username, that.username)
            && Objects.equals(password, that.password);
      }

      return result;
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(username) ^ Objects.hashCode(password);
    }
  }
}
