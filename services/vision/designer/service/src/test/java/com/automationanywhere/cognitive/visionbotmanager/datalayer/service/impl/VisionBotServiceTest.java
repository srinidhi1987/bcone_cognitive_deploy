package com.automationanywhere.cognitive.visionbotmanager.datalayer.service.impl;

import com.automationanywhere.cognitive.visionbotmanager.datalayer.INewVisionBotServiceDataAccessLayer;
import com.automationanywhere.cognitive.visionbotmanager.datalayer.VisionBotDetailProvider;
import com.automationanywhere.cognitive.visionbotmanager.exception.DatabaseException;
import com.automationanywhere.cognitive.visionbotmanager.impl.VisionbotService;
import com.automationanywhere.cognitive.visionbotmanager.models.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

public class VisionBotServiceTest {

    @Mock
    private INewVisionBotServiceDataAccessLayer newVisionBotServiceDataAccessLayer;

    @Mock
    VisionBotDetailProvider detailProvider;

    @InjectMocks
    private VisionbotService visionbotService;

    @BeforeTest
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    private void getSummaryDetails_WithStagingDataPresent() throws ParseException
    {
        String orgId ="1";
        String prjId = "941c4f56-b858-4fd2-84b8-37ae51e07c06";
        String visionbotId = "4669436f-c904-4f69-abcd-6447312c32fa";
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        List<VisionBotRunDetailsDto> runDetailsForStaging = new ArrayList<>();

        VisionBotRunDetailsDto dto1 = new VisionBotRunDetailsDto();
        dto1.setId("613a031d-3175-4697-887a-0f16160f3c4d");
        String createdDate = "2017-10-12 11:49:52.917";// new Date(2017,10,12,11,49,52);

        dto1.setStartTime(date.parse(createdDate));
        dto1.setTotalFiles(2);
        dto1.setPassedDocumentCount(1);
        dto1.setFailedDocumentCount(1);
        dto1.setProcessedDocumentCount(2);
        dto1.setTotalFieldCount(45.0);
        dto1.setPassedFieldCount(30.0);
        dto1.setVisionbotId("4669436f-c904-4f69-abcd-6447312c32fa");

        VisionBotRunDetailsDto dto2 = new VisionBotRunDetailsDto();
        dto2.setId("776469c8-aafd-486c-9820-ec6e78ce98a9");
        String createdDate2 = "2017-10-05 10:57:38.983";//  new Date(2017,10,05,10,57,38);
        dto2.setStartTime(date.parse(createdDate2));
        dto2.setTotalFiles(2);
        dto2.setPassedDocumentCount(1);
        dto2.setFailedDocumentCount(1);
        dto2.setProcessedDocumentCount(2);
        dto2.setTotalFieldCount(45.0);
        dto2.setPassedFieldCount(30.0);
        dto2.setVisionbotId(visionbotId);

        VisionBotRunDetailsDto dto3 = new VisionBotRunDetailsDto();
        dto3.setId("2942aea0-d565-43b4-85af-181b72ce0f61");
        String createdDate3 =  "2017-10-05 10:57:41.160"; //new Date(2017,10,05,10,57,41);
        dto3.setStartTime(date.parse(createdDate3));
        dto3.setTotalFiles(2);
        dto3.setPassedDocumentCount(0);
        dto3.setFailedDocumentCount(2);
        dto3.setProcessedDocumentCount(2);
        dto3.setTotalFieldCount(12.0);
        dto3.setPassedFieldCount(3.0);
        dto3.setVisionbotId("7c128cb7-e546-4ba6-9fe5-bd9bec6a131c");

        runDetailsForStaging.add(dto1);
        runDetailsForStaging.add(dto2);
        runDetailsForStaging.add(dto3);

        StagingFilesSummary expectedResult = new StagingFilesSummary();

        expectedResult.setTotalFilesCount(4);
        expectedResult.setTotalPassedFileCount(1);
        expectedResult.setTotalFailedFileCount(3);
        expectedResult.setTotalTestedFileCount(4);
        expectedResult.setAccuracy(57.00);

        when(detailProvider.getOrganizationWiseRunDetailsDtoForStaging(orgId,prjId)).thenReturn(runDetailsForStaging);
        StagingFilesSummary actualResult = visionbotService.getSummaryDetails(orgId,prjId);

        assertThat(actualResult).isNotNull();
        assertThat(actualResult.getTotalPassedFileCount()).isEqualTo(expectedResult.getTotalPassedFileCount());
        assertThat(actualResult.getTotalFilesCount()).isEqualTo(expectedResult.getTotalFilesCount());
        assertThat(actualResult.getTotalFailedFileCount()).isEqualTo(expectedResult.getTotalFailedFileCount());
        assertThat(actualResult.getTotalTestedFileCount()).isEqualTo(expectedResult.getTotalTestedFileCount());
        assertThat(actualResult.getAccuracy()).isEqualTo(expectedResult.getAccuracy());
    }

    @Test
    private void getSummaryDetails_StagingDataNotPresent()
    {
        String orgId ="1";
        String prjId = "941c4f56-b858-4fd2-84b8-37ae51e07c06";
        List<VisionBotRunDetailsDto> runDetailsForStaging = new ArrayList<>();
        StagingFilesSummary expectedResult = new StagingFilesSummary();

        expectedResult.setTotalFilesCount(0);
        expectedResult.setTotalPassedFileCount(0);
        expectedResult.setTotalFailedFileCount(0);
        expectedResult.setTotalTestedFileCount(0);
        expectedResult.setAccuracy(00.00);

        when(detailProvider.getOrganizationWiseRunDetailsDtoForStaging(orgId,prjId)).thenReturn(runDetailsForStaging);
        StagingFilesSummary actualResult = visionbotService.getSummaryDetails(orgId,prjId);

        assertThat(actualResult).isNotNull();
        assertThat(actualResult.getTotalPassedFileCount()).isEqualTo(expectedResult.getTotalPassedFileCount());
        assertThat(actualResult.getTotalFilesCount()).isEqualTo(expectedResult.getTotalFilesCount());
        assertThat(actualResult.getTotalFailedFileCount()).isEqualTo(expectedResult.getTotalFailedFileCount());
        assertThat(actualResult.getTotalTestedFileCount()).isEqualTo(expectedResult.getTotalTestedFileCount());
        assertThat(actualResult.getAccuracy()).isEqualTo(expectedResult.getAccuracy());
    }

    @Test(expectedExceptions=DatabaseException.class,expectedExceptionsMessageRegExp="Unable to retrieve staging data from database")
    private void getSummaryDetails_DatabaseConectionException()
    {
        String orgId ="1";
        String prjId = "941c4f56-b858-4fd2-84b8-37ae51e07c06";
        List<VisionBotRunDetailsDto> runDetailsForStaging = null;
        StagingFilesSummary expectedResult = new StagingFilesSummary();

        expectedResult.setTotalFilesCount(0);
        expectedResult.setTotalPassedFileCount(0);
        expectedResult.setTotalFailedFileCount(0);
        expectedResult.setTotalTestedFileCount(0);
        expectedResult.setAccuracy(00.00);

        when(detailProvider.getOrganizationWiseRunDetailsDtoForStaging(orgId,prjId)).thenReturn(runDetailsForStaging);
        StagingFilesSummary actualResult = visionbotService.getSummaryDetails(orgId,prjId);

        assertThat(actualResult).isNull();

    }

    @Test
    private void getVisionBotCounts_VisionbotsPresent()
    {
        String orgId ="1";
        String prjId = "941c4f56-b858-4fd2-84b8-37ae51e07c06";
        int totalBotsCreated = 4;
        int stagingBots = 3;
        int productionBots = 1;

        when(detailProvider.getVisionBotCount(orgId,prjId)).thenReturn(totalBotsCreated);
        when(detailProvider.getVisionBotCountStateWise(orgId,prjId, Environment.staging)).thenReturn(stagingBots);
        when(detailProvider.getVisionBotCountStateWise(orgId,prjId, Environment.production)).thenReturn(productionBots);

        VisionBotCount expectedResult = new VisionBotCount();
        expectedResult.setNumberOfBotsCreated(totalBotsCreated);
        expectedResult.setNumberOfStagingBots(stagingBots);
        expectedResult.setNumberOfProductionBots(productionBots);

        VisionBotCount actualResult = visionbotService.getVisionBotCounts(orgId,prjId);

        assertThat(actualResult).isNotNull();
        assertThat(actualResult.getNumberOfBotsCreated()).isEqualTo(expectedResult.getNumberOfBotsCreated());
        assertThat(actualResult.getNumberOfStagingBots()).isEqualTo(expectedResult.getNumberOfStagingBots());
        assertThat(actualResult.getNumberOfProductionBots()).isEqualTo(expectedResult.getNumberOfProductionBots());
    }
}
