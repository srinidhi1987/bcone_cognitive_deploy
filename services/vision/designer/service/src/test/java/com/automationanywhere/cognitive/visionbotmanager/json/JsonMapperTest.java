package com.automationanywhere.cognitive.visionbotmanager.json;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.automationanywhere.cognitive.visionbotmanager.exception.InvalidJsonData;
import com.automationanywhere.cognitive.visionbotmanager.exception.InvalidJsonPatch;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.ByteArrayInputStream;
import org.testng.annotations.Test;

public class JsonMapperTest {

  @Test
  public void buildJsonPatchWithEmptyDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream("".getBytes());

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("is not allowed empty or null document");
  }

  @Test
  public void buildJsonPatchWithBlankDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream("    ".getBytes());

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("is not allowed empty or null document");
  }

  @Test
  public void buildJsonPatchWithNullDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(null));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("is not allowed empty or null document");
  }

  @Test
  public void buildJsonPatchWithNotArrayDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "{\"op\": \"remove\", \"path\": \"/Id\"}".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("not an array of operations");
  }

  @Test
  public void buildJsonPatchWithNoOperationType() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"path\": \"Id\", \"value\":\"/ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'op' attribute not found");
  }

  @Test
  public void buildJsonPatchWithInvalidOperationType() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"insert\", \"path\": \"/Id\", \"value\":\"ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("invalid operation 'insert'");
  }

  @Test
  public void buildJsonPatchWithNoPathAttribute() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"add\", \"value\":\"ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'path' attribute not found");
  }

  @Test
  public void buildJsonPatchWithEmptyPathAttribute() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"add\", \"path\": \" \", \"value\":\"ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'path' is invalid");
  }

  @Test
  public void buildJsonPatchWithNullPathAttribute() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"add\", \"path\": null, \"value\":\"ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'path' is invalid");
  }

  @Test
  public void buildJsonPatchWithPathAttributeNotStartedWithSlash() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"add\", \"path\": \"ABC\", \"value\":\"ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'path' is invalid");
  }

  @Test
  public void buildJsonPatchWithNoFromAttribute() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"move\", \"path\":\"/\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'from' attribute not found");
  }

  @Test
  public void buildJsonPatchWithEmptyFromAttribute() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"copy\", \"from\": \" \", \"path\":\"/ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'from' is invalid");
  }

  @Test
  public void buildJsonPatchWithNullFromAttribute() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"move\", \"from\": null, \"path\":\"/\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'from' is invalid");
  }

  @Test
  public void buildJsonPatchWithFromAttributeNotStartedWithSlash() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"move\", \"from\": \"ABC\", \"path\":\"/\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'from' is invalid");
  }

  @Test
  public void buildJsonPatchWithNoValueAttributeToAdd() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"add\", \"path\": \"/ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonPatch.class)
        .hasMessage("'value' attribute not found");
  }

  @Test
  public void buildJsonPatchWithNoValueAttributeToRemove() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(
        "[{\"op\": \"remove\", \"path\": \"/ABC\"}]".getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void buildJsonPatchWithValidDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    ByteArrayInputStream is = new ByteArrayInputStream(("["
        + "{\"op\": \"test\", \"path\": \"/ABC\", \"value\":\"123\"},"
        + "{\"op\": \"replace\", \"path\": \"/ABC\", \"value\":\"123\"},"
        + "{\"op\": \"add\", \"path\": \"/ABC\", \"value\":\"123\"},"
        + "{\"op\": \"remove\", \"path\": \"/ABC\"},"
        + "{\"op\": \"move\", \"from\": \"/ABC\", \"path\": \"/DEF\"},"
        + "{\"op\": \"copy\", \"from\": \"/ABC\", \"path\": \"/DEF\"}"
        + "]").getBytes()
    );

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonPatch(is));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void buildJsonNodeWithValidDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    String data = "["
        + "{\"op\": \"test\", \"path\": \"/ABC\", \"value\":\"123\"},"
        + "{\"op\": \"replace\", \"path\": \"/ABC\", \"value\":\"123\"}"
        + "]";

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonNode(data));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void buildJsonNodeWithInvalidDocument() {
    // GIVEN
    JsonMapper mapper = new JsonMapper();

    String data = "["
        + "{\"op\": \"test\", \"path\": \"/ABC\", \"value\":\"123\"},"
        + "{\"op\": \"replace\", \"path\": \"/ABC\", \"value\":\"123\""
        + "]";

    // WHEN
    Throwable throwable = catchThrowable(() -> mapper.buildJsonNode(data));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidJsonData.class)
        .hasMessageStartingWith("Invalid JSON data [Unexpected close marker ']': expected '}'");
  }

  @Test
  public void removeKeyFromJsonNodeRecursively() {
    // GIVEN
    String data = "{"
        + "\"$type\": \"123\", \"abc\": \"/ABC\", \"object\": {"
        + "\"$type\": \"456\", \"cde\": \"/CDE\", \"items\":["
        + "{\"$type\": \"789\", \"fgh\": \"/FGH\"}"
        + "]}}";

    JsonMapper mapper = new JsonMapper();
    JsonNode jsonNode = mapper.buildJsonNode(data);

    // WHEN
    mapper.removeKeyRecursively(jsonNode, "$type");

    // THEN
    assertThat(mapper.toString(jsonNode)).isEqualToIgnoringWhitespace(
        "{\"abc\":\"/ABC\",\"object\":{\"cde\":\"/CDE\",\"items\":[{\"fgh\": \"/FGH\"}]}}"
    );
  }

  @Test
  public void removeParentFromArrayRecursively() {
    // GIVEN
    String data = "{\"object\": {\"$values\":"
        + "  [{\"abc\": \"/ABC\"},{\"def\": {\"$values\":"
        + "    [{\"ghi\": \"/GHI\"},{\"jkl\": \"/JKL\"}]}"
        + "  }]"
        + "}}";

    JsonMapper mapper = new JsonMapper();
    JsonNode jsonNode = mapper.buildJsonNode(data);

    // WHEN
    mapper.removeParentFromArrayRecursively(jsonNode, "$values");

    // THEN
    assertThat(mapper.toString(jsonNode)).isEqualToIgnoringWhitespace(
        "{\"object\":[{\"abc\":\"/ABC\"},{\"def\":[{\"ghi\":\"/GHI\"},{\"jkl\":\"/JKL\"}]}]}"
    );
  }
}
