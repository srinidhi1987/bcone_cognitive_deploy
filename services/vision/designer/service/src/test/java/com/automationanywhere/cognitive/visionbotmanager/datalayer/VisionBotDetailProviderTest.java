package com.automationanywhere.cognitive.visionbotmanager.datalayer;

import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotDetail;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by msundell on 8/14/17
 */
public class VisionBotDetailProviderTest {
    @Test
    public void testTest() throws Exception {
        // given
        String id = "123";

        DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
        detachedcriteria.add(Restrictions.eq("id", id));

        VisionBotDetail visionBotDetail = new VisionBotDetail();
        visionBotDetail.setId(id);

        List<VisionBotDetail> visionBotDetailsData = new ArrayList<>();
        visionBotDetailsData.add(visionBotDetail);

        INewVisionBotServiceDataAccessLayer datalayer = mock(INewVisionBotServiceDataAccessLayer.class);
        when(datalayer.Select(eq(VisionBotDetail.class), any(DetachedCriteria.class))).thenReturn(visionBotDetailsData);

        VisionBotDetailProvider provider = new VisionBotDetailProvider(datalayer);

        // when
        VisionBotDetail visionBotDetailsResult = provider.GetVisionBotDetail(id);

        // then
        assertThat(visionBotDetailsResult).isNotNull();
        assertThat(visionBotDetailsResult.getId()).isEqualTo(id);
    }
}