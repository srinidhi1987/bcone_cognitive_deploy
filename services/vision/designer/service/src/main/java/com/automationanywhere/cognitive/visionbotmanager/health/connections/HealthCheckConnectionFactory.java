package com.automationanywhere.cognitive.visionbotmanager.health.connections;

import com.automationanywhere.cognitive.visionbotmanager.exception.UnSupportedConnectionTypeException;
import org.springframework.beans.factory.annotation.Autowired;

public class HealthCheckConnectionFactory {

  @Autowired
  private DBCheckConnection dbCheckConnection;
  @Autowired
  private ServiceCheckConnection svcCheckConnection;
  @Autowired
  private MQCheckConnection mqCheckConnection;

  public HealthCheckConnection getConnection(ConnectionType connType) {
    switch (connType) {
      case DB:
        return dbCheckConnection;
      case SVC:
        return svcCheckConnection;
      case MQ:
        return mqCheckConnection;
      default:
        throw new UnSupportedConnectionTypeException(connType + " is not supported");
    }
  }
}
