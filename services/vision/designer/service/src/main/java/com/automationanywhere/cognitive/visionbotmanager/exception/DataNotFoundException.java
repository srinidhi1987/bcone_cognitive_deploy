package com.automationanywhere.cognitive.visionbotmanager.exception;

/**
 * Created by Mukesh.Methaniya on 08-02-2017.
 */
public class DataNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -7046718225943201357L;

  public DataNotFoundException(String message) {
    super(message);
  }
}
