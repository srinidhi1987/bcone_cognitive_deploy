package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class SettingsDto {

  private final boolean enableAutoRotation;
  private final boolean enablePageOrientation;
  private final boolean enableBorderFilter;
  private final boolean enableBinarization;
  private final boolean enableGrayscaleConversion;
  private final boolean enableNoiseFilter;
  private final boolean enableRemoveLines;
  private final boolean enableContrastStretch;
  private final int binarizationType;
  private final int noiseThreshold;
  private final int binarizationThreshold;
  private final boolean enableBackgroundRemoval;
  private final boolean preserveOriginalDpi;
  private final boolean useVectorInfoInPdf;
  private final boolean tesseract4Engine;

  public SettingsDto(
      @JsonProperty("EnableAutoRotation") final boolean enableAutoRotation,
      @JsonProperty("EnablePageOrientation") final boolean enablePageOrientation,
      @JsonProperty("EnableBorderFilter") final boolean enableBorderFilter,
      @JsonProperty("EnableBinarization") final boolean enableBinarization,
      @JsonProperty("EnableGrayscaleConversion") final boolean enableGrayscaleConversion,
      @JsonProperty("EnableNoiseFilter") final boolean enableNoiseFilter,
      @JsonProperty("EnableRemoveLines") final boolean enableRemoveLines,
      @JsonProperty("EnableContrastStretch") final boolean enableContrastStretch,
      @JsonProperty("BinarizationType") final int binarizationType,
      @JsonProperty("NoiseThreshold") final int noiseThreshold,
      @JsonProperty("BinarizationThreshold") final int binarizationThreshold,
      @JsonProperty("EnbleBackgroundRemoval") final boolean enbleBackgroundRemoval,
      @JsonProperty("PreserveOriginalDpi") final boolean preserveOriginalDpi,
      @JsonProperty("UseVectorInfoInPdf") final boolean useVectorInfoInPdf,
      @JsonProperty("IsTesseract4Engine") final boolean tesseract4Engine
  ) {
    this.enableAutoRotation = enableAutoRotation;
    this.enablePageOrientation = enablePageOrientation;
    this.enableBorderFilter = enableBorderFilter;
    this.enableBinarization = enableBinarization;
    this.enableGrayscaleConversion = enableGrayscaleConversion;
    this.enableNoiseFilter = enableNoiseFilter;
    this.enableRemoveLines = enableRemoveLines;
    this.enableContrastStretch = enableContrastStretch;
    this.binarizationType = binarizationType;
    this.noiseThreshold = noiseThreshold;
    this.binarizationThreshold = binarizationThreshold;
    this.enableBackgroundRemoval = enbleBackgroundRemoval;
    this.preserveOriginalDpi = preserveOriginalDpi;
    this.useVectorInfoInPdf = useVectorInfoInPdf;
    this.tesseract4Engine = tesseract4Engine;
  }

  /**
   * Returns the enableAutoRotation.
   *
   * @return the value of enableAutoRotation
   */
  @JsonProperty("EnableAutoRotation")
  public boolean isEnableAutoRotation() {
    return enableAutoRotation;
  }

  /**
   * Returns the enablePageOrientation.
   *
   * @return the value of enablePageOrientation
   */
  @JsonProperty("EnablePageOrientation")
  public boolean isEnablePageOrientation() {
    return enablePageOrientation;
  }

  /**
   * Returns the enableBorderFilter.
   *
   * @return the value of enableBorderFilter
   */
  @JsonProperty("EnableBorderFilter")
  public boolean isEnableBorderFilter() {
    return enableBorderFilter;
  }

  /**
   * Returns the enableBinarization.
   *
   * @return the value of enableBinarization
   */
  @JsonProperty("EnableBinarization")
  public boolean isEnableBinarization() {
    return enableBinarization;
  }

  /**
   * Returns the enableGrayscaleConversion.
   *
   * @return the value of enableGrayscaleConversion
   */
  @JsonProperty("EnableGrayscaleConversion")
  public boolean isEnableGrayscaleConversion() {
    return enableGrayscaleConversion;
  }

  /**
   * Returns the enableNoiseFilter.
   *
   * @return the value of enableNoiseFilter
   */
  @JsonProperty("EnableNoiseFilter")
  public boolean isEnableNoiseFilter() {
    return enableNoiseFilter;
  }

  /**
   * Returns the enableRemoveLines.
   *
   * @return the value of enableRemoveLines
   */
  @JsonProperty("EnableRemoveLines")
  public boolean isEnableRemoveLines() {
    return enableRemoveLines;
  }

  /**
   * Returns the enableContrastStretch.
   *
   * @return the value of enableContrastStretch
   */
  @JsonProperty("EnableContrastStretch")
  public boolean isEnableContrastStretch() {
    return enableContrastStretch;
  }

  /**
   * Returns the binarizationType.
   *
   * @return the value of binarizationType
   */
  @JsonProperty("BinarizationType")
  public int getBinarizationType() {
    return binarizationType;
  }

  /**
   * Returns the noiseThreshold.
   *
   * @return the value of noiseThreshold
   */
  @JsonProperty("NoiseThreshold")
  public int getNoiseThreshold() {
    return noiseThreshold;
  }

  /**
   * Returns the binarizationThreshold.
   *
   * @return the value of binarizationThreshold
   */
  @JsonProperty("BinarizationThreshold")
  public int getBinarizationThreshold() {
    return binarizationThreshold;
  }

  /**
   * Returns the enableBackgroundRemoval.
   *
   * @return the value of enableBackgroundRemoval
   */
  @JsonProperty("EnbleBackgroundRemoval")
  public boolean isEnableBackgroundRemoval() {
    return enableBackgroundRemoval;
  }

  /**
   * Returns the preserveOriginalDpi.
   *
   * @return the value of preserveOriginalDpi
   */
  @JsonProperty("PreserveOriginalDpi")
  public boolean isPreserveOriginalDpi() {
    return preserveOriginalDpi;
  }

  /**
   * Returns the useVectorInfoInPdf.
   *
   * @return the value of useVectorInfoInPdf
   */
  @JsonProperty("UseVectorInfoInPdf")
  public boolean isUseVectorInfoInPdf() {
    return useVectorInfoInPdf;
  }

  /**
   * Returns the tesseract4Engine.
   *
   * @return the value of tesseract4Engine
   */
  public boolean isTesseract4Engine() {
    return tesseract4Engine;
  }
}
