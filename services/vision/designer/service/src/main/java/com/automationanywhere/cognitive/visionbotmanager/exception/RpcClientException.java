package com.automationanywhere.cognitive.visionbotmanager.exception;

public class RpcClientException extends RuntimeException {

  private static final long serialVersionUID = 8103825883100047174L;

  public RpcClientException(String message) {
    super(message);
  }

  public RpcClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
