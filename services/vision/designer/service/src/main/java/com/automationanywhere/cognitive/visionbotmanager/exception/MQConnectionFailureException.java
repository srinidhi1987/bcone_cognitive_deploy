package com.automationanywhere.cognitive.visionbotmanager.exception;

public class MQConnectionFailureException extends RuntimeException {

  private static final long serialVersionUID = 8459399781620418452L;

  public MQConnectionFailureException(String message, Throwable cause) {
    super(message, cause);
  }

}
