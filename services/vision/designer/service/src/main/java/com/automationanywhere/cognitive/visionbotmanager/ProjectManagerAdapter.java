package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.visionbotmanager.models.dao.ProjectDetail;
import java.util.List;

/**
 * Created by pooja.wani on 07-03-2017.
 */
public interface ProjectManagerAdapter {

  ProjectDetail getProjectMetaData(String organizationId, String projectId);

  List<ProjectDetail> getProjectMetaDataList(String organizationId);

  void testConnection();
}
