package com.automationanywhere.cognitive.visionbotmanager.models;

public class VisionBotCount extends EntityModelBase {

  private int numberOfBotsCreated;

  private int numberOfStagingBots;

  private int numberOfProductionBots;

  public int getNumberOfStagingBots() {
    return numberOfStagingBots;
  }

  public void setNumberOfStagingBots(int numberOfStagingBots) {
    this.numberOfStagingBots = numberOfStagingBots;
  }

  public int getNumberOfBotsCreated() {
    return numberOfBotsCreated;
  }

  public void setNumberOfBotsCreated(int numberOfBotsCreated) {
    this.numberOfBotsCreated = numberOfBotsCreated;
  }

  public int getNumberOfProductionBots() {
    return numberOfProductionBots;
  }

  public void setNumberOfProductionBots(int numberOfProductionBots) {
    this.numberOfProductionBots = numberOfProductionBots;
  }


}
