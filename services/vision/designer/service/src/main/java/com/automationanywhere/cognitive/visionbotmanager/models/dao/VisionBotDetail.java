package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.automationanywhere.cognitive.visionbotmanager.models.Environment;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by Mukesh.Methaniya on 30-01-2017.
 */
@Entity
@Table(name = "VisionBotDetail")
public class VisionBotDetail extends EntityModelBase {

  @Id
  @Column(name = "id")
  @JsonProperty("id")
  private String id;

  @Column(name = "organizationid")
  @JsonProperty("organizationId")
  private String organizationId;


  @Column(name = "projectid")
  @JsonProperty("projectId")
  private String projectId;


  @Column(name = "classificationid")
  @JsonProperty("categoryId")
  private String classificationId;


  @Column(name = "Environment")
  @Enumerated(EnumType.STRING)
  @JsonProperty("environment")
  private Environment environment;


  @Column(name = "LockedUserid")
  @JsonProperty("lockedUserId")
  private String lockedUserId;

  @Column(name = "LockedTimestamp")
  @JsonProperty("lockedTimestamp")
  private Date lockedTimestamp;

  @Column(name = "LastModifiedByUser")
  @JsonProperty("LastModifiedByUser")
  private String lastModifiedByUser;

  @Column(name = "LastModifiedTimestamp")
  @JsonProperty("LastModifiedTimestamp")
  private Date lastModifiedTimestamp;

  @OneToOne(fetch = FetchType.LAZY, mappedBy = "visionBotDetail", cascade = CascadeType.ALL)
  @JsonProperty("visionBot")
  private VisionBot visionBot;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  public String getLockedUserId() {
    return lockedUserId;
  }

  public void setLockedUserId(String lockedUserId) {
    this.lockedUserId = lockedUserId;
  }

  public Date getLockedTimestamp() {
    return lockedTimestamp;
  }

  public void setLockedTimestamp(Date lockedTimestamp) {
    this.lockedTimestamp = lockedTimestamp;
  }

  public VisionBot getVisionBot() {
    return visionBot;
  }

  public void setVisionBot(VisionBot visionBot) {
    this.visionBot = visionBot;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  public Date getLastModifiedTimestamp() {
    return lastModifiedTimestamp;
  }

  public void setLastModifiedTimestamp(Date lastModifiedTimestamp) {
    this.lastModifiedTimestamp = lastModifiedTimestamp;
  }

  public String getLastModifiedByUser() {
    return lastModifiedByUser;
  }

  public void setLastModifiedByUser(String lastModifiedByUser) {
    this.lastModifiedByUser = lastModifiedByUser;
  }
}
