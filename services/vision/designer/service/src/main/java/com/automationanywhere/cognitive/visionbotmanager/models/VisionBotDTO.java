package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class VisionBotDTO {

  @JsonProperty("state")
  private String state;

  @JsonProperty("visionBotIds")
  private List<String> visionBotIds;

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public List<String> getVisionBotIds() {
    return visionBotIds;
  }

  public void setVisionBotIds(List<String> visionBotIds) {
    this.visionBotIds = visionBotIds;
  }
}
