package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.automationanywhere.cognitive.visionbotmanager.models.Environment;
import com.automationanywhere.cognitive.visionbotmanager.models.RunState;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by Mukesh.Methaniya on 28-02-2017.
 */
@Entity
@Table(name = "VisionBotRunDetails")
public class VisionBotRunDetails extends EntityModelBase {

  @Id
  @Column(name = "id")
  //  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @JsonProperty("id")
  private String id;


/*    @Column(name = "projectid")
    @JsonProperty("projectId")
    private String  projectId;

    @Column(name = "projectid")
    @JsonProperty("projectId")
    private String categoryId;*/

  @Column(name = "visionbotid")
  @JsonProperty("visionbotId")
  private String visionbotId;

  @Column(name = "createdat")
  @JsonProperty("createdAt")
  private Date createdAt;

  @Column(name = "totalFiles")
  @JsonProperty("totalFiles")
  private int totalFiles = 0;


  @Column(name = "environment")
  @Enumerated(EnumType.STRING)
  @JsonProperty("environment")
  private Environment environment;

  @Column(name = "runstateid")
  @Enumerated(EnumType.ORDINAL)
  @JsonProperty("runState")
  private RunState runState = RunState.Running;


  @OneToMany(mappedBy = "visionBotRunDetails", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  //@JoinColumn(name = "visionbotrundetailsid")
  @JsonProperty("documentsDetails")
  private
  List<DocumentProccessedDatails> documentsDetails;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getVisionbotId() {
    return visionbotId;
  }

  public void setVisionbotId(String visionbotId) {
    this.visionbotId = visionbotId;
  }

  public List<DocumentProccessedDatails> getDocumentsDetails() {
    return documentsDetails;
  }

  public void setDocumentsDetails(final List<DocumentProccessedDatails> documentsDetails) {
    this.documentsDetails = documentsDetails;
  }

  public int getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(int totalFiles) {
    this.totalFiles = totalFiles;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  public void addDocumentsDetails(DocumentProccessedDatails document) {
    if (documentsDetails == null) {
      documentsDetails = new ArrayList<DocumentProccessedDatails>();
    }

    documentsDetails.add(document);
  }

  public RunState getRunState() {
    return runState;
  }

  public void setRunState(RunState runState) {
    this.runState = runState;
  }
}
