package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Summary {

  @JsonProperty("stagingSummary")
  private StagingFilesSummary stagingSummary;
  @JsonProperty("botDetails")
  private VisionBotCount botDetails;

  public StagingFilesSummary getStagingSummary() {
    return stagingSummary;
  }

  public void setStagingSummary(StagingFilesSummary stagingSummary) {
    this.stagingSummary = stagingSummary;
  }

  public VisionBotCount getBotDetails() {
    return botDetails;
  }

  public void setBotDetails(VisionBotCount botDetails) {
    this.botDetails = botDetails;
  }


}
