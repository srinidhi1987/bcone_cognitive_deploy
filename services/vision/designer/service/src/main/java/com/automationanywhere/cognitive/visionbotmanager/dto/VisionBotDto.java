package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class VisionBotDto {

  private final String id;
  private final String name;
  private final double version;
  private final List<LayoutDto> layouts;
  private final DataModelDto dataModel;
  private final DataModelIdStructureDto dataModelIdStructure;
  private final SettingsDto settings;
  private final String language;

  public VisionBotDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("Name") final String name,
      @JsonProperty("Version") final double version,
      @JsonProperty("Layouts") final List<LayoutDto> layouts,
      @JsonProperty("DataModel") final DataModelDto dataModel,
      @JsonProperty("DataModelIdStructure") final DataModelIdStructureDto dataModelIdStructure,
      @JsonProperty("Settings") final SettingsDto settings,
      @JsonProperty("Language") final String language
  ) {
    this.id = id;
    this.name = name;
    this.version = version;
    this.layouts = layouts;
    this.dataModel = dataModel;
    this.dataModelIdStructure = dataModelIdStructure;
    this.settings = settings;
    this.language = language;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  @JsonProperty("Name")
  public String getName() {
    return name;
  }

  /**
   * Returns the version.
   *
   * @return the value of version
   */
  @JsonProperty("Version")
  public double getVersion() {
    return version;
  }

  /**
   * Returns the layouts.
   *
   * @return the value of layouts
   */
  @JsonProperty("Layouts")
  public List<LayoutDto> getLayouts() {
    return layouts;
  }

  /**
   * Returns the dataModel.
   *
   * @return the value of dataModel
   */
  @JsonProperty("DataModel")
  public DataModelDto getDataModel() {
    return dataModel;
  }

  /**
   * Returns the dataModelIdStructure.
   *
   * @return the value of dataModelIdStructure
   */
  @JsonProperty("DataModelIdStructure")
  public DataModelIdStructureDto getDataModelIdStructure() {
    return dataModelIdStructure;
  }

  /**
   * Returns the settings.
   *
   * @return the value of settings
   */
  @JsonProperty("Settings")
  public SettingsDto getSettings() {
    return settings;
  }

  /**
   * Returns the language.
   *
   * @return the value of language
   */
  @JsonProperty("Language")
  public String getLanguage() {
    return language;
  }
}
