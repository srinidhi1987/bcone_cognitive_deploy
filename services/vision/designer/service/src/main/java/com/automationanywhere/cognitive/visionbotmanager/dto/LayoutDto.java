package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class LayoutDto {

  private final SettingsDto settings;
  private final String id;
  private final String name;
  private final DocPropertiesDto docProperties;
  private final String vBotDocSetId;
  private final String VBotTestDocSetId;
  private final SirFieldsDto sirFields;
  private final List<LayoutFieldDto> fields;
  private final List<LayoutTableDto> tables;
  private final List<IdentifierDto> identifiers;

  public LayoutDto(
      @JsonProperty("Settings") final SettingsDto settings,
      @JsonProperty("Id") final String id,
      @JsonProperty("Name") final String name,
      @JsonProperty("DocProperties") final DocPropertiesDto docProperties,
      @JsonProperty("VBotDocSetId") final String vBotDocSetId,
      @JsonProperty("VBotTestDocSetId") final String VBotTestDocSetId,
      @JsonProperty("SirFields") final SirFieldsDto sirFields,
      @JsonProperty("Fields") final List<LayoutFieldDto> fields,
      @JsonProperty("Tables") final List<LayoutTableDto> tables,
      @JsonProperty("Identifiers") final List<IdentifierDto> identifiers
  ) {
    this.settings = settings;
    this.id = id;
    this.name = name;
    this.docProperties = docProperties;
    this.vBotDocSetId = vBotDocSetId;
    this.VBotTestDocSetId = VBotTestDocSetId;
    this.sirFields = sirFields;
    this.fields = fields;
    this.tables = tables;
    this.identifiers = identifiers;
  }

  /**
   * Returns the settings.
   *
   * @return the value of settings
   */
  @JsonProperty("Settings")
  public SettingsDto getSettings() {
    return settings;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  @JsonProperty("Name")
  public String getName() {
    return name;
  }

  /**
   * Returns the docProperties.
   *
   * @return the value of docProperties
   */
  @JsonProperty("DocProperties")
  public DocPropertiesDto getDocProperties() {
    return docProperties;
  }

  /**
   * Returns the vBotDocSetId.
   *
   * @return the value of vBotDocSetId
   */
  @JsonProperty("VBotDocSetId")
  public String getVBotDocSetId() {
    return vBotDocSetId;
  }

  /**
   * Returns the VBotTestDocSetId.
   *
   * @return the value of VBotTestDocSetId
   */
  @JsonProperty("VBotTestDocSetId")
  public String getVBotTestDocSetId() {
    return VBotTestDocSetId;
  }

  /**
   * Returns the sirFields.
   *
   * @return the value of sirFields
   */
  @JsonProperty("SirFields")
  public SirFieldsDto getSirFields() {
    return sirFields;
  }

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  @JsonProperty("Fields")
  public List<LayoutFieldDto> getFields() {
    return fields;
  }

  /**
   * Returns the tables.
   *
   * @return the value of tables
   */
  @JsonProperty("Tables")
  public List<LayoutTableDto> getTables() {
    return tables;
  }

  /**
   * Returns the identifiers.
   *
   * @return the value of identifiers
   */
  @JsonProperty("Identifiers")
  public List<IdentifierDto> getIdentifiers() {
    return identifiers;
  }
}
