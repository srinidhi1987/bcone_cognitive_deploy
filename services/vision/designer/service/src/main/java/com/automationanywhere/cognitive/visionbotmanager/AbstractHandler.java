package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.models.CustomResponse;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * Created by Mayur.Panchal on 09-12-2016.
 */
public abstract class AbstractHandler {

  protected ObjectMapper jsonObjectMapper;
  @Autowired
  MessageSource messageSource;
  private AALogger aaLogger = AALogger.create(this.getClass());

  protected String getResponse(CustomResponse customResponse) {
    try {
      jsonObjectMapper.setVisibility(
          jsonObjectMapper.getSerializationConfig().getDefaultVisibilityChecker()
              .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
              .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
              .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
              .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
      jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
      jsonObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
      jsonObjectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
      return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);
    } catch (JsonProcessingException e) {
      aaLogger.error(
          messageSource.getMessage("JSON Processing exception occured", null, Locale.getDefault()),
          e);
      return "";
    }
  }

}
