package com.automationanywhere.cognitive.visionbotmanager.health.connections;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.FileManagerAdapter;
import com.automationanywhere.cognitive.visionbotmanager.ProjectManagerAdapter;
import com.automationanywhere.cognitive.visionbotmanager.ValidatorAdapter;
import java.util.ArrayList;
import java.util.List;

public class ServiceCheckConnection implements HealthCheckConnection {

  AALogger aaLogger = AALogger.create(this.getClass());
  FileManagerAdapter fileManagerAdapter;
  ValidatorAdapter validatorAdapter;
  ProjectManagerAdapter projectManagerAdapter;

  public ServiceCheckConnection(FileManagerAdapter fileManagerAdapter,
      ValidatorAdapter validatorAdapter, ProjectManagerAdapter projectManagerAdapter) {
    super();
    this.fileManagerAdapter = fileManagerAdapter;
    this.validatorAdapter = validatorAdapter;
    this.projectManagerAdapter = projectManagerAdapter;
  }

  @Override
  public void checkConnection() {
    // TODO Auto-generated method stub

  }

  @Override
  public List<ServiceConnectivity> checkConnectionForService() {
    aaLogger.entry();
    List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity dependentService = null;
    for (DependentServices de : DependentServices.values()) {
      try {
        dependentService = new ServiceConnectivity(de.toString(), HTTPStatusCode.OK);
        switch (de) {
          case FileManager:
            fileManagerAdapter.testConnection();
            break;
          case Validator:
            validatorAdapter.testConnection();
            break;
          case Project:
            projectManagerAdapter.testConnection();
            break;
        }
      } catch (Exception ex) {
        prepareDependentServiceList(dependentServices, dependentService, de,
            HTTPStatusCode.INTERNAL_SERVER_ERROR);
      }
      if (!dependentServices.contains(dependentService)) {
        prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.OK);
      }
    }
    return dependentServices;
  }

  private void prepareDependentServiceList(
      List<ServiceConnectivity> dependentServices,
      ServiceConnectivity dependentService, DependentServices de, HTTPStatusCode mode) {
    dependentService.setHTTPStatus(mode);
    dependentServices.add(dependentService);
  }
}
