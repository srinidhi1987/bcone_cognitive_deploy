package com.automationanywhere.cognitive.visionbotmanager.impl;

import static com.automationanywhere.cognitive.common.formatter.CategoryFormatter.format;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.FileManagerAdapter;
import com.automationanywhere.cognitive.visionbotmanager.IVisionBotService;
import com.automationanywhere.cognitive.visionbotmanager.ProjectManagerAdapter;
import com.automationanywhere.cognitive.visionbotmanager.RpcClient;
import com.automationanywhere.cognitive.visionbotmanager.ValidatorAdapter;
import com.automationanywhere.cognitive.visionbotmanager.datalayer.INewVisionBotServiceDataAccessLayer;
import com.automationanywhere.cognitive.visionbotmanager.datalayer.IVisionbotServiceDataAccessLayer;
import com.automationanywhere.cognitive.visionbotmanager.datalayer.VisionBotDetailProvider;
import com.automationanywhere.cognitive.visionbotmanager.exception.DataIsInUseException;
import com.automationanywhere.cognitive.visionbotmanager.exception.DataIsLockedException;
import com.automationanywhere.cognitive.visionbotmanager.exception.DataNotFoundException;
import com.automationanywhere.cognitive.visionbotmanager.exception.DatabaseException;
import com.automationanywhere.cognitive.visionbotmanager.exception.ProjectDeletedException;
import com.automationanywhere.cognitive.visionbotmanager.json.JsonValidator;
import com.automationanywhere.cognitive.visionbotmanager.models.DocumentProcessingStatus;
import com.automationanywhere.cognitive.visionbotmanager.models.Environment;
import com.automationanywhere.cognitive.visionbotmanager.models.FileCount;
import com.automationanywhere.cognitive.visionbotmanager.models.ProductionDocumentsDetails;
import com.automationanywhere.cognitive.visionbotmanager.models.RunState;
import com.automationanywhere.cognitive.visionbotmanager.models.StagingFilesSummary;
import com.automationanywhere.cognitive.visionbotmanager.models.Status;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotCount;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotData;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotMetadata;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotRunDetailsDto;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.DocumentProccessedDatails;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.FileDetail;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.ProjectDetail;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.TestSet;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.TestSetInfo;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.ValidatorReviewDetails;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotDetail;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotRunDetails;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;

/**
 * Created by Mukesh.Methaniya on 15-12-2016.
 */
public class VisionbotService implements IVisionBotService {

  private static AALogger logger = AALogger.create(VisionbotService.class);
  private IVisionbotServiceDataAccessLayer visionbotServiceDataAccessLayer;
  private INewVisionBotServiceDataAccessLayer newVisionBotServiceDataAcessLayer;
  private ConnectionFactory rpcConnectionFactory;
  private FileManagerAdapter filemanageradapter;
  private ProjectManagerAdapter projectManagerAdapter;
  private ValidatorAdapter validatorAdapter;
  private VisionBotDetailProvider detailProvider;
  private JsonValidator validator;

  public static boolean isNullOrEmpty(String s) {
    return s == null || s.length() == 0;
  }

  public static boolean isNullOrWhitespace(String s) {
    return s == null || isWhitespace(s);

  }

  private static boolean isWhitespace(String s) {
    int length = s.length();
    if (length > 0) {
      for (int i = 0; i < length; i++) {
        if (!Character.isWhitespace(s.charAt(i))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public void Init(ApplicationContext context) {
    filemanageradapter = (FileManagerAdapter) context.getBean("fileManagerAdapter");
    projectManagerAdapter = (ProjectManagerAdapter) context.getBean("projectManagerAdapter");
    validatorAdapter = (ValidatorAdapter) context.getBean("validatorAdapter");
    rpcConnectionFactory = (ConnectionFactory) context.getBean("rpcConnectionfactory");
    visionbotServiceDataAccessLayer = (IVisionbotServiceDataAccessLayer) context
        .getBean("visionbotServiceDataAccessLayer");
    newVisionBotServiceDataAcessLayer = (INewVisionBotServiceDataAccessLayer) context
        .getBean("NewVisionBotServiceDataAcessLayer");
    validator = (JsonValidator) context.getBean("jsonValidator");
    detailProvider = new VisionBotDetailProvider(newVisionBotServiceDataAcessLayer);

     /*   RPCCallBack rpcCallBack=new RPCCallBack() {
            @Override
            public String actionToPerformed(String input) {

                System.out.println(" [x] actionToPerformed '" + input + "'");
                return "iam actionToPerformed and called";
            }
        };

        RabbitMQSetting rabbitMQSetting=new RabbitMQSetting("test",
                "192.168.2.59",
                "messagequeue",
                "passmessage"
                );

        RpcMsgQ rpcMsgQ=new RpcMsgQ("QUEUE_Visionbot_storeBotRunDataInDatabase",rabbitMQSetting);
        rpcMsgQ.Start(rpcCallBack);*/
  }
  
  @Override
  public VisionBotDetail SaveVisionBot(String organizationId, String projectID, String CategoryID, String data) {
      logger.entry();
      logger.debug(() -> " Input Params :" + " organizationId " + organizationId + " projectID " + projectID + " CategoryID " + CategoryID );
      VisionBotDetail visionBotDetail = new VisionBotDetail();
      String visionbotID = UUID.randomUUID().toString();
      visionBotDetail.setId(visionbotID);
      visionBotDetail.setOrganizationId(organizationId);
      visionBotDetail.setProjectId(projectID);
      visionBotDetail.setClassificationId(CategoryID);
      visionBotDetail.setEnvironment(Environment.staging);

      VisionBot visionBot = new VisionBot();
      visionBot.setId(visionbotID);
      visionBot.setDatablob(data);
      visionBotDetail.setVisionBot(visionBot);

      newVisionBotServiceDataAcessLayer.Insert(visionBotDetail);
      newVisionBotServiceDataAcessLayer.Insert(visionBot);
      logger.trace("Successfully save the visionbot ");
      logger.exit();
      return visionBotDetail;
  }

  @Override
  public VisionBot UpdateVisionBot(String visionBotID, String data) {
      logger.entry();
      logger.debug(() -> " Input Params :" + " visionBotID " + visionBotID );
      VisionBot visionBot = new VisionBot();
      String visionbotID = visionBotID;
      visionBot.setId(visionbotID);

      VisionBot result = detailProvider.GetVisionBot("", "", visionBotID);

      if (result == null) {
          logger.trace("VisionBot is not found on server");
          throw new DataNotFoundException("VisionBot(" + visionBotID + ") is not present on server.");
      }
      result.setDatablob(data);

      newVisionBotServiceDataAcessLayer.Update(result);
      logger.exit();
      return visionBot;
  }

  @Override
  public VisionBot GetValidationData(String visionBotID) {
    logger.entry();
    logger.debug(() -> " Input Params :" + " visionBotID " + visionBotID);
    VisionBot visionBot = new VisionBot();
    String visionbotID = visionBotID;
    visionBot.setId(visionbotID);

    VisionBot result = detailProvider.GetVisionBot("", "", visionBotID);

    if (result == null) {
      logger.info("VisionBot is not found on server");
      throw new DataNotFoundException("Visionbot (" + visionBotID + ") is not present on server.");
    }
    logger.exit();
    return result;
  }

   /* private void throwExceptionIfVisionBotCreationProcessIsAlreadyRunningForSameVisionbot(boolean isItVisionBotCreationMode, VisionBotDetail tempVisionBotDetails, String previousBotUser) {
        if ((!isItVisionBotCreationMode && previousBotUser.equals(tempVisionBotDetails.getLockedUserId()))
                || !isNullOrEmpty(previousBotUser)) {
            throw new DataIsInUseException("VisionBot creation is in processing mode. please try after some time.");
        }
    }*/

  @Override
  public VisionBot UpdateValidationData(String visionBotID, String data) {
    logger.entry();
    //logger.debug(() -> " Input Params :" + " visionBotID " + visionBotID + " data " + data);
    VisionBot visionBot = new VisionBot();
    String visionbotID = visionBotID;
    visionBot.setId(visionbotID);

    VisionBot result = detailProvider.GetVisionBot("", "", visionBotID);

    if (result == null) {
      logger.info("VisionBot is not found on server");
      throw new DataNotFoundException("Visionbot (" + visionBotID + ") is not present on server.");
    }

    result.setValidationDatablob(data);

    newVisionBotServiceDataAcessLayer.Update(result);
    logger.exit();
    return result;
  }

  @Override
  public VisionBot GetVisionBot(String visionBotID) {
    logger.entry();
    logger.debug(() -> " Input Params :" + " visionBotID " + visionBotID);
    VisionBot visionBot = new VisionBot();
    String visionbotID = visionBotID;
    visionBot.setId(visionbotID);

    VisionBot result = detailProvider.GetVisionBot("", "", visionBotID);

    if (result == null || result.getDatablob() == null) {
      logger.info("VisionBot is not found on server");
      throw new DataNotFoundException("Visionbot (" + visionBotID + ") is not present on server.");
    }

    return result;
  }

  public VisionBotDetail GetCategoryVisionBotForEdit(String organizationId, String ProjectId, String CategoryId, String user) {
      logger.entry();
      logger.debug(() -> " Input Params :" + " organizationId " + organizationId + " ProjectId " + ProjectId + " CategoryId " + CategoryId + " user " + user);
      List<VisionBotDetail> result = detailProvider.GetVisionBotDetails(organizationId, ProjectId, CategoryId);

      // boolean isItVisionBotCreationMode = false;
      if (result == null || result.size() == 0) {
          //   isItVisionBotCreationMode = true;
          VisionBotDetail visionBotDetail = CreateNewVisionBotDetails(organizationId, ProjectId, CategoryId);

          result = detailProvider.GetVisionBotDetails(organizationId, ProjectId, CategoryId);
      }

      if (result != null && result.size() > 0) {
          VisionBotDetail tempVisionBotDetails = result.get(0);

          String botUserBeforeLockAquire = tempVisionBotDetails.getLockedUserId();

          throwExceptionIfVisionBotIsInProductionMode(tempVisionBotDetails);

          throwExceptionIfStagingDocumentForThisVisionBotIsRunning(tempVisionBotDetails.getId());

          tryToAquireLockOfVisionbotDetailElseThrowException(user, tempVisionBotDetails);

          String rpcResponse = "";
          try {
              VisionBot visionBot = tempVisionBotDetails.getVisionBot();
              if (visionBot == null || visionBot.getDatablob() == null) {

                  throwExceptionIfVisionBotCreationProcessIsAlreadyRunningForSameVisionbot(tempVisionBotDetails, botUserBeforeLockAquire);

                  rpcResponse = CreateVisionBot(organizationId, ProjectId, CategoryId,
                      tempVisionBotDetails.getId());
                  result = detailProvider.GetVisionBotDetails(organizationId, ProjectId, CategoryId);
                  tempVisionBotDetails = result.get(0);
                  visionBot = tempVisionBotDetails.getVisionBot();
              }

              if (visionBot != null && visionBot.getDatablob() != null) {
                  return tempVisionBotDetails;
              }
              else if (!rpcResponse.isEmpty()) {
                  throw new DataNotFoundException(rpcResponse);
              }
              else {
                  throw new DataNotFoundException("Unable to create Visionbot with category id '" + CategoryId + "'.");
              }
          } catch (Exception ex) {
              UnLockVisionBot(ProjectId, CategoryId, tempVisionBotDetails.getId(), user);
              logger.error(ex.getMessage());
              throw ex;
          }
      } else {
          logger.error("Unable to create VisionBot with category id '" + CategoryId + "'.");
          throw new DataNotFoundException("Unable to create VisionBot with category id '" + CategoryId + "'.");
      }
  }

   /* private void throwExceptionIfVisionBotCreationProcessIsAlreadyRunningForSameVisionbot(boolean isItVisionBotCreationMode, VisionBotDetail tempVisionBotDetails, String previousBotUser) {
        if ((!isItVisionBotCreationMode && previousBotUser.equals(tempVisionBotDetails.getLockedUserId()))
                || !isNullOrEmpty(previousBotUser)) {
            throw new DataIsInUseException("VisionBot creation is in processing mode. please try after some time.");
        }
    }*/

    private void throwExceptionIfVisionBotCreationProcessIsAlreadyRunningForSameVisionbot(VisionBotDetail tempVisionBotDetails, String botUserBeforeLockAquire) {
        if ((botUserBeforeLockAquire != null && botUserBeforeLockAquire.equals(tempVisionBotDetails.getLockedUserId()))
                || !isNullOrEmpty(botUserBeforeLockAquire)) {
            throw new DataIsInUseException("The IQ Bot platform is currently busy and cannot process this request.Please try again after some time.");
        }
    }

  private void throwExceptionIfVisionBotIsInProductionMode(VisionBotDetail tempVisionBot) {
    if (tempVisionBot.getEnvironment() == Environment.production) {
      logger.error("Exception occured: visionbot is in the production mode");
      throw new IllegalStateException("Visionbot is in production mode.");
    }
  }

  private void throwExceptionIfStagingDocumentForThisVisionBotIsRunning(String visionBotDetailId) {

    VisionBotRunDetails runDetails = detailProvider.GetLetestVisionBotRunDetails(visionBotDetailId);

    if (runDetails != null && runDetails.getRunState() == RunState.Running) {
      logger
          .error("Staging document for this visionbot is in progress. Please try after some time.");
      throw new DataIsInUseException(
          "Staging document for this visionbot is in progress. Please try after some time.");
    }
  }

  private void tryToAquireLockOfVisionbotDetailElseThrowException(String user,
      VisionBotDetail tempVisionBot) {
    if (tempVisionBot.getLockedUserId() == null
        || tempVisionBot.getLockedUserId().equals("")
        || tempVisionBot.getLockedUserId().equals(user)) {
      tempVisionBot.setLockedUserId(user);
      tempVisionBot.setLockedTimestamp(new Date());
      newVisionBotServiceDataAcessLayer.Update(tempVisionBot);
      //return tempVisionBot;
    } else {
      throw new DataIsInUseException("This VisionBot is currently being trained by another user '"
          + tempVisionBot.getLockedUserId() + "'. Please try again later.");
    }
  }

  private VisionBotDetail CreateNewVisionBotDetails(String organizationId, String projectId,
      String categoryId) {
    logger.entry();
    logger.debug(() -> "Parameters : organizationId " + organizationId + " projectId " + projectId
        + " categoryId " + categoryId);
    VisionBotDetail visionBotDetail = new VisionBotDetail();
    String visionbotID = UUID.randomUUID().toString();
    visionBotDetail.setId(visionbotID);
    visionBotDetail.setOrganizationId(organizationId);
    visionBotDetail.setProjectId(projectId);
    visionBotDetail.setClassificationId(categoryId);
    visionBotDetail.setEnvironment(Environment.staging);

    VisionBot visionBot = new VisionBot();
    visionBot.setId(visionbotID);
    visionBotDetail.setVisionBot(visionBot);

    try {
      newVisionBotServiceDataAcessLayer.Insert(visionBotDetail);
    } catch (DataIntegrityViolationException dve) {
      logger.error(dve.getMessage());
      throw new ProjectDeletedException("Learning Instance has been deleted.");
    }
    logger.trace("Successfully created new vision bot");
    logger.exit();
    return visionBotDetail;
  }

	private String CreateVisionBot(String organizationId, String projectId, String categoryId,
			String visionBotDetailId) {
		logger.entry();
		logger.debug(() -> "Parameters : organizationId " + organizationId + " projectId " + projectId + " categoryId "
				+ categoryId + " visionBotDetailId " + visionBotDetailId);
		try {

			Map<String, Object> dictionary = new HashMap<String, Object>();
			dictionary.put("organizationId", organizationId);
			dictionary.put("projectId", projectId);
			dictionary.put("categoryId", categoryId);
			dictionary.put("visionBotDetailId", visionBotDetailId);

			ObjectMapper jsonObjectMapper = new ObjectMapper();

			String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(dictionary);

			RpcClient client = new RpcClient("QUEUE_CreateVisionBot", rpcConnectionFactory);
			logger.debug("Requesting CreateVisionBot(" + organizationId + projectId + categoryId + ")");
			String res = client.call(requestData);
			logger.debug("Response: '" + res + "'");
			client.close();
			logger.exit();
			return res;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new DataNotFoundException("Error in Creating VisionBot.");
		}
	}

  private void throwExceptionIfStagingDocumentIsNotAvailableForCategory(String organizationId,
      String projectId, String categoryId) {
    List<FileDetail> filesInCategory = filemanageradapter
        .getSpecificFilesFromVisionBot(organizationId, categoryId, projectId);

    if (filesInCategory != null && filesInCategory.size() > 0) {
      boolean anyStagingFilePresentInCategory = filesInCategory.stream()
          .anyMatch(x -> x.getProduction() == false);
      if (!anyStagingFilePresentInCategory) {
        String availableFilesInCategory = "";
        boolean isFirstTimeInLoop = true;
        int i = 0;
        for (FileDetail fileDetail : filesInCategory) {
          if (!isFirstTimeInLoop) {
            availableFilesInCategory = availableFilesInCategory + ", ";
          }
          availableFilesInCategory = availableFilesInCategory + fileDetail.getFilename();
          isFirstTimeInLoop = false;
          i++;
          if (i > 1) {
            break;
          }
        }
        logger.error("Staging documents are not available for this category.");
        throw new DataNotFoundException("Staging documents are not available for this category."
            + System.lineSeparator()
            + "Please upload following documents in staging."
            + System.lineSeparator()
            + System.lineSeparator()
            + "Documents: " + availableFilesInCategory);
      }
    } else {
      logger.error("Unable to find any file in this category");
      throw new DataNotFoundException("Unable to find any file in this category");
    }
  }

  public String UnLockVisionBot(String ProjectId, String CategoryId, String visionbotid,
      String username) {
    logger.entry();
    List<VisionBotDetail> result = detailProvider
        .GetVisionBotDetails("", ProjectId, CategoryId, username);
    if (result != null && result.size() > 0) {
      VisionBotDetail tempVisionBot = result.get(0);
      if (tempVisionBot.getLockedUserId().equals(username)) {
        tempVisionBot.setLockedUserId("");
        tempVisionBot.setLockedTimestamp(null);
        newVisionBotServiceDataAcessLayer.Update(tempVisionBot);
      }
      return tempVisionBot.getId();
    } else {
      logger.error("Visionbot with category id '" + CategoryId + "' is not present on server.");
      throw new DataNotFoundException(
          "Visionbot with category id '" + CategoryId + "' is not present on server.");
    }
  }

  public VisionBotDetail GetCategoryVisionBot(String ProjectId, String CategoryId) {
    logger.entry();
    List<VisionBotDetail> result = detailProvider.GetVisionBotDetails("", ProjectId, CategoryId);
    if (result != null && result.size() > 0) {
      return result.get(0);
    }
    {
      logger.error("Visionbot with category id '" + CategoryId + "' is not present on server.");
      throw new DataNotFoundException(
          "Visionbot with category id '" + CategoryId + "' is not present on server.");
    }
  }

  public VisionBotDetail GetVisionBotDetails(String visionId) {
    logger.entry();
    if (isNullOrWhitespace(visionId)) {
      logger.error("Parameter Is Not Valid");
      throw new IllegalArgumentException("Parameter Is Not Valid");
    }

    VisionBotDetail result = detailProvider.GetVisionBotData(visionId);

    if (result == null) {
      logger.error("Visionbot (" + visionId + ") is not present on server.");
      throw new DataNotFoundException("Visionbot (" + visionId + ") is not present on server.");
    }
    return result;
  }

  public List<VisionBotMetadata> GetOrganizationVisionBotsMetadata(String organizationId) {
    logger.entry();
    if (isNullOrWhitespace(organizationId)) {
      logger.error("Parameter Is Not Valid");
      throw new IllegalArgumentException("Parameter Is Not Valid");
    }
    List<VisionBotDetail> result = detailProvider.GetVisionBotDetails(organizationId, "", "");
    removeEmptyVisionbotDataEntriesFromList(result);
    if (result != null && result.size() > 0) {
      return ConvertVisonBotDetailsToVisionBotsMetaData(result);
    } else {
      logger.error("Resource  is not present on server.");
      throw new DataNotFoundException("Resource  is not present on server.");
    }
  }

  private void removeEmptyVisionbotDataEntriesFromList(List<VisionBotDetail> result) {
    logger.entry();
    if (result != null && result.size() > 0) {
      result.removeIf(visionBotDetail -> isVisionBotDataNotAvailable(visionBotDetail));
    }
    logger.exit();
  }

  private boolean isVisionBotDataNotAvailable(VisionBotDetail visionBotDetail) {
    return visionBotDetail.getVisionBot() == null
        || visionBotDetail.getVisionBot().getDatablob() == null;
  }

  public List<VisionBotMetadata> GetProjectVisionBotsMetadata(String organizationId,
      String ProjectId) {
    logger.entry();
    if (isNullOrWhitespace(organizationId) ||
        isNullOrWhitespace(ProjectId)) {
      logger.error("Parameter Is Not Valid");
      throw new IllegalArgumentException("Parameter Is Not Valid");
    }

    List<VisionBotDetail> result = detailProvider
        .GetVisionBotDetails(organizationId, ProjectId, "");
    removeEmptyVisionbotDataEntriesFromList(result);

    if (result != null && result.size() > 0) {
      return ConvertVisonBotDetailsToVisionBotsMetaData(result);
    } else {
      logger.error("Resource  is not present on server.");
      throw new DataNotFoundException("Resource  is not present on server.");
    }
  }

  public List<VisionBotMetadata> GetCategoryVisionBotsMetadata(String organizationId,
      String ProjectId, String CategoryId) {
    logger.entry();
    if (isNullOrWhitespace(organizationId) ||
        isNullOrWhitespace(ProjectId) ||
        isNullOrWhitespace(CategoryId)) {
      logger.error("Parameter Is Not Valid");
      throw new IllegalArgumentException("Parameter Is Not Valid");
    }

    List<VisionBotDetail> result = detailProvider
        .GetVisionBotDetails(organizationId, ProjectId, CategoryId);
    removeEmptyVisionbotDataEntriesFromList(result);
    if (result != null && result.size() > 0) {
      return ConvertVisonBotDetailsToVisionBotsMetaData(result);
    } else {
      logger.error("Resource  is not present on server.");
      throw new DataNotFoundException("Resource  is not present on server.");
    }
  }

  public List<VisionBotData> GetOrganizationVisionBotData(String organizationId,
      ArrayList<String> ids) {
    logger.entry();
    if (isNullOrWhitespace(organizationId)) {
      logger.error("Parameter Is Not Valid");
      throw new IllegalArgumentException("Parameter Is Not Valid");
    }

    List<VisionBotData> visionBotData = CreateVisionBotRunDetailsData(organizationId, ids);
    logger.exit();
    return visionBotData;
  }

  @Override
  public List<VisionBotData> GetOrganizationVisionBotData(String organizationId, String projectId) {
    logger.entry();
    if (isNullOrWhitespace(organizationId) || isNullOrWhitespace(projectId)) {
      logger.error("Parameter Is Not Valid");
      throw new IllegalArgumentException("Parameter Is Not Valid");
    }

    List<VisionBotData> visionBotData = CreateVisionBotRunDetailsData(organizationId, projectId);
    logger.exit();
    return visionBotData;
  }

  @Override
  public TestSet GetTestSet(String visionBotID, String layoutID) {
    logger.entry();
    TestSet result = detailProvider.GetTestSet(visionBotID, layoutID);
    if (result != null) {
      logger.exit();
      return result;
    } else {
      logger.error("TestSet for layout id '" + layoutID + "' is not present on server.");
      throw new DataNotFoundException(
          "TestSet for layout id '" + layoutID + "' is not present on server.");
    }
  }

  @Override
  public TestSet SetTestSet(String visionBotID, String layoutID, TestSet testSet) {
    logger.entry();
    throwNotFoundExceptionIfBotNotPresent(visionBotID);

    TestSetInfo testSetInfo = new TestSetInfo();
    testSetInfo.setVisionBotId(visionBotID);
    testSetInfo.setLayoutId(layoutID);
    testSet.setTestSetInfo(testSetInfo);
    TestSet result = detailProvider.GetTestSet(visionBotID, layoutID);

    if (result == null) {
      logger.trace("Data not found.");
      testSetInfo.setId(UUID.randomUUID().toString());
      newVisionBotServiceDataAcessLayer.Insert(testSet);
    } else {
      result.setName(testSet.getName());
      result.setTesItems(testSet.getTesItems());
      newVisionBotServiceDataAcessLayer.Update(result);
    }
    logger.exit();
    return testSet;
  }

  @Override
  public void DeleteTestSet(String visionBotID, String layoutID) {
    logger.entry();
    throwNotFoundExceptionIfBotNotPresent(visionBotID);

    TestSetInfo result = detailProvider.GetTestSetInfo(visionBotID, layoutID);
    if (result != null) {

      newVisionBotServiceDataAcessLayer.Delete(result);
    } else {
      logger.error("TestSet for layout id '" + layoutID + "' is not present on server.");
      throw new DataNotFoundException(
          "TestSet for layout id '" + layoutID + "' is not present on server.");
    }
  }

  public VisionBotDetail SetVisionBotState(String visionbotId, String data) {
    if (data.equalsIgnoreCase(Environment.production.toString())) {
      VisionBotRunDetails runDetails = detailProvider.GetLetestVisionBotRunDetails(visionbotId);
      if (runDetails != null && runDetails.getRunState() == RunState.Running) {
        logger.error("Visionbot is in running state.");
        throw new DataIsLockedException("Visionbot is in running state.");
      }
    }
    VisionBotDetail result = detailProvider.GetVisionBotDetail(visionbotId);

    if (result == null) {
      logger.error("Visionbot (" + visionbotId + ") is not present on server.");
      throw new DataNotFoundException("Visionbot (" + visionbotId + ") is not present on server.");
    }

    if (data.equalsIgnoreCase(Environment.staging.toString())) {
      result.setEnvironment(Environment.staging);
    } else if (data.equalsIgnoreCase(Environment.production.toString())) {
      result.setEnvironment(Environment.production);
    }
    newVisionBotServiceDataAcessLayer.Update(result);

    if (result.getEnvironment() == Environment.production) {
      Map<String, Object> dictionary = new HashMap<String, Object>();
      dictionary.put("organizationId", result.getOrganizationId());
      dictionary.put("projectId", result.getProjectId());
      dictionary.put("categoryId", result.getClassificationId());

      ObjectMapper jsonObjectMapper = new ObjectMapper();
      jsonObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      try {
        String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
            .writeValueAsString(dictionary);

        RpcClient client = new RpcClient("QUEUE_Visionbot_Update", rpcConnectionFactory);
        logger.trace("Requesting QUEUE_Visionbot_Update(" + result.getClassificationId() + ")");
        try {
          client.putMessage(requestData);
          logger.trace("Message dropped sucessfully.");
        } finally {
          client.close();
        }

      } catch (Exception ex) {
        throw new RuntimeException("Unable to start processing of production documents");
      }
    }
    return result;
  }

  @Override
  public void RunAndExtractVBotStaging(String visionbotId) throws Exception {

    logger.entry();
    logger.debug(() -> (" visionbotId :" + visionbotId));
    VisionBotDetail visionBotDetail = detailProvider.GetVisionBotDetail(visionbotId);
    if (visionBotDetail == null) {
      logger.error("Learning Instance has been deleted");
      throw new ProjectDeletedException("Learning Instance has been deleted");
    }

    throwExceptionIfVisionBotIsNotAvailableForStagingRun(visionBotDetail);

    Map<String, Object> dictionary = new HashMap<String, Object>();

    dictionary.put("visionBotId", visionbotId);
    ObjectMapper jsonObjectMapper = new ObjectMapper();
    jsonObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    VisionBotDetail result = GetVisionBotDetails(visionbotId);
    dictionary.put("organizationId", result.getOrganizationId());
    List<FileDetail> fileDetails = filemanageradapter
        .getSpecificFilesFromVisionBotForStaging(result.getOrganizationId(),
            result.getClassificationId(), result.getProjectId());
    dictionary.put("projectId", result.getProjectId());
    dictionary.put("categoryId", result.getClassificationId());
    String categoryId = result.getClassificationId();
    String projectId = result.getProjectId();

    VisionBotRunDetails runDetails = new VisionBotRunDetails();
    runDetails.setVisionbotId(visionbotId);
    runDetails.setCreatedAt(new Date());
    runDetails.setEnvironment(Environment.staging);
    runDetails.setId(UUID.randomUUID().toString());
    runDetails.setRunState(RunState.Running);

    for (FileDetail fileDetail : fileDetails) {

      DocumentProccessedDatails docProcessingDetails = new DocumentProccessedDatails();
      docProcessingDetails.setDocId(fileDetail.getFileid());
      docProcessingDetails.setId(UUID.randomUUID().toString());
      docProcessingDetails.setVisionBotRunDetails(runDetails);
      docProcessingDetails.setProcessingStatus(DocumentProcessingStatus.UnProcessed);
      runDetails.addDocumentsDetails(docProcessingDetails);
    }

    runDetails.setTotalFiles(fileDetails.size());
    newVisionBotServiceDataAcessLayer.Insert(runDetails);

    dictionary.put("visionBotRunDetailsId", runDetails.getId());
    try {

      String response = "";

      for (DocumentProccessedDatails docDetails : runDetails.getDocumentsDetails()) {
        dictionary.put("documentId", docDetails.getDocId());
        dictionary.put("documentProcessedDetailsId", docDetails.getId());

        String documentId = docDetails.getDocId();
        String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
            .writeValueAsString(dictionary);

        logger.debug(
            "Requesting tp process staging document(" + "1" + projectId + "," + categoryId + ","
                + visionbotId + "," + runDetails.getId() + "," + documentId + ")");
        RpcClient client = new RpcClient("QUEUE_Visionbot_ProcessStagingDocument",
            rpcConnectionFactory);
        logger.debug("Response: " + response);

        client.putMessage(requestData);
        client.close();
      }
      //newVisionBotServiceDataAcessLayer.Update(runDetails);
    } catch (Exception ex) {
      logger.error(ex.getMessage());
    }
    logger.exit();
  }

  private void throwExceptionIfVisionBotIsNotAvailableForStagingRun(VisionBotDetail visionBotDetail)
      throws Exception {

    if (visionBotDetail == null ||
        visionBotDetail.getVisionBot() == null) {
      logger.error("Visionbot is not created.");
      throw new DataNotFoundException("Visionbot is not created.");
    }

    if (visionBotDetail.getEnvironment() == Environment.production) {
      logger.error("Visionbot is in production mode.");
      throw new IllegalStateException("Visionbot is in production mode.");
    }

    String lockedBy = visionBotDetail.getLockedUserId();
    if (lockedBy != null && !lockedBy.isEmpty()) {
      logger
          .error("Visionbot is already in use by user '" + visionBotDetail.getLockedUserId() + "'");
      throw new DataIsInUseException(
          "Visionbot is already in use by user '" + visionBotDetail.getLockedUserId() + "'");
    }

    VisionBotRunDetails runDetails = detailProvider
        .GetLetestVisionBotRunDetails(visionBotDetail.getId());
    if (runDetails != null) {
      if (runDetails.getRunState() == RunState.Running) {
        logger.error("Staging document for this visionbot is already in running state.");
        throw new DataIsInUseException(
            "Staging document for this visionbot is already in running state.");
      }
    }
  }

  private void throwExceptionIfProjectStateIsInProductionForTheVisionBot(
      ProjectDetail projectDetail) {

    if (projectDetail == null
        || !projectDetail.getEnvironment().equals(Environment.staging.toString())) {
      logger.error("Project is not in staging mode.");
      throw new IllegalStateException("Project is not in staging mode.");
    }
  }

  public DocumentProccessedDatails updateVisionBotRunDetailsProcessedDocument(
      String visionBotRunDetailsId
      , String documentId
      , DocumentProccessedDatails proceessedDocument) {
    logger.entry();
    DocumentProccessedDatails result = detailProvider
        .GetDocumentProcessedDatails(proceessedDocument.getId(), documentId);
    result.setProcessingStatus(proceessedDocument.getProcessingStatus());
    result.setTotalFieldCount(proceessedDocument.getTotalFieldCount());
    result.setPassedFieldCount(proceessedDocument.getPassedFieldCount());
    result.setFailedFieldCount(proceessedDocument.getFailedFieldCount());

    newVisionBotServiceDataAcessLayer.Update(result);

    int unProcessedDocumentCount = detailProvider
        .GetUnProcessedDocumentDetailsCount(visionBotRunDetailsId);
    if (unProcessedDocumentCount == 0) {
      VisionBotRunDetails runDetails = detailProvider.GetVisionBotRunDetails(visionBotRunDetailsId);
      runDetails.setRunState(RunState.Completed);
      newVisionBotServiceDataAcessLayer.Update(runDetails);
    }
    result.setVisionBotRunDetails(detailProvider.GetVisionBotRunDetails(visionBotRunDetailsId));
    logger.exit();

    return result;
  }

  private List<VisionBotMetadata> ConvertVisonBotDetailsToVisionBotsMetaData(
      List<VisionBotDetail> visionBotDetails) {
    logger.entry();
    List<VisionBotMetadata> visionbotsMetadata = new ArrayList<VisionBotMetadata>();

    if (visionBotDetails != null && visionBotDetails.size() > 0) {
      for (VisionBotDetail visionBotDetail :
          visionBotDetails) {

        visionbotsMetadata.add(ConvertVisonBotDetailToVisionBotMetaData(visionBotDetail));
      }
    }
    logger.exit();
    return visionbotsMetadata;
  }

  private VisionBotMetadata ConvertVisonBotDetailToVisionBotMetaData(
      VisionBotDetail visionBotDetail) {
    logger.entry();
    VisionBotMetadata metadata = new VisionBotMetadata();
    if (visionBotDetail != null) {
      metadata.setId(visionBotDetail.getId());
      metadata.setOrganizationId(visionBotDetail.getOrganizationId());
      metadata.setProjectId(visionBotDetail.getProjectId());
      metadata.setClassificationId(visionBotDetail.getClassificationId());
      metadata.setEnvironment(visionBotDetail.getEnvironment());
      metadata.setLockeduserid(visionBotDetail.getLockedUserId());
    }
    logger.exit();
    return metadata;
  }

  private List<VisionBotData> CreateVisionBotRunDetailsData(String organizationId,
      ArrayList<String> ids) {
    logger.entry();
    List<VisionBotData> visionBotData = new ArrayList<>();
    List<VisionBotDetail> visionBotDetails = null;
    if (ids.size() > 0) {
      visionBotDetails = detailProvider.GetVisionBotDetails(organizationId, ids);
    } else {
      visionBotDetails = detailProvider.GetVisionBotDetails(organizationId);
    }

    removeEmptyVisionbotDataEntriesFromList(visionBotDetails);

    Map<String, ValidatorReviewDetails> validatorReviewDetails = null;// GetValidatorReviewDetails(organizationId);

    Map<String, ProductionDocumentsDetails> productionDocumentsDetailsMap = validatorAdapter
        .getProductionDocumentDetails(organizationId);

    FileCount[] organizationsFileCountCategoryWise = filemanageradapter
        .getFileCount(organizationId);

    Map<String, FileCount> organizationsFileCountCategoryWiseMap = new HashMap<String, FileCount>();

    if (organizationsFileCountCategoryWise != null
        && organizationsFileCountCategoryWise.length > 0) {
      for (FileCount fileCount : organizationsFileCountCategoryWise) {
        organizationsFileCountCategoryWiseMap.put(
            getKeyCombinationGivenId(fileCount.getProjectId(), fileCount.getClassificationId())
            , fileCount);
      }
    }

    List<VisionBotRunDetailsDto> runDetailsForStaging = detailProvider
        .getOrganizationWiseRunDetailsDtoForStaging(organizationId);
    List<ProjectDetail> projectDetailList = projectManagerAdapter
        .getProjectMetaDataList(organizationId);

    for (VisionBotDetail visionBotDetail : visionBotDetails) {

      VisionBotData data = getVisionBotData(visionBotDetail
          , validatorReviewDetails
          , productionDocumentsDetailsMap
          , organizationsFileCountCategoryWiseMap
          , runDetailsForStaging
          , projectDetailList);

      visionBotData.add(data);
    }
    logger.exit();
    return visionBotData;
  }

  private List<VisionBotData> CreateVisionBotRunDetailsData(String organizationId,
      String projectId) {
    logger.entry();
    List<VisionBotData> visionBotData = new ArrayList<>();

    List<VisionBotDetail> visionBotDetails = detailProvider
        .GetVisionBotDetails(organizationId, projectId);
    if (visionBotDetails == null || visionBotDetails.size() == 0) {
      return null;
    }

    removeEmptyVisionbotDataEntriesFromList(visionBotDetails);

    Map<String, ValidatorReviewDetails> validatorReviewDetails = null;

    Map<String, ProductionDocumentsDetails> productionDocumentsDetailsMap = validatorAdapter
        .getProductionDocumentDetails(organizationId);

    FileCount[] organizationsFileCountCategoryWise = filemanageradapter
        .getFileCount(organizationId, projectId);

    Map<String, FileCount> organizationsFileCountCategoryWiseMap = new HashMap<String, FileCount>();

    if (organizationsFileCountCategoryWise != null
        && organizationsFileCountCategoryWise.length > 0) {
      for (FileCount fileCount : organizationsFileCountCategoryWise) {
        organizationsFileCountCategoryWiseMap.put(
            getKeyCombinationGivenId(fileCount.getProjectId(), fileCount.getClassificationId())
            , fileCount);
      }
    }

    List<VisionBotRunDetailsDto> runDetailsForStaging = detailProvider
        .getOrganizationWiseRunDetailsDtoForStaging(organizationId, projectId);
    ProjectDetail projectDetail = projectManagerAdapter
        .getProjectMetaData(organizationId, projectId);
    List<ProjectDetail> projectDetailList = new ArrayList<ProjectDetail>();
    projectDetailList.add(projectDetail);

    for (VisionBotDetail visionBotDetail : visionBotDetails) {
      VisionBotData data = getVisionBotData(visionBotDetail
          , validatorReviewDetails
          , productionDocumentsDetailsMap
          , organizationsFileCountCategoryWiseMap
          , runDetailsForStaging
          , projectDetailList);

      visionBotData.add(data);
    }
    logger.exit();
    return visionBotData;
  }

  private String getKeyCombinationGivenId(String id_1, String id_2) {
    return id_1 + "_" + id_2;
  }

  private VisionBotData getVisionBotData(VisionBotDetail visionBotDetail
      , Map<String, ValidatorReviewDetails> validatorReviewDetails
      , Map<String, ProductionDocumentsDetails> productionDocumentsDetailsMap
      , Map<String, FileCount> organizationsFileCountCategoryWiseMap
      , List<VisionBotRunDetailsDto> runDetailsForStaging
      , List<ProjectDetail> projectDetailList) {
    logger.entry();
    VisionBotData data = new VisionBotData();

    data.setId(visionBotDetail.getId());
    data.setName(visionBotDetail.getId());
    data.setOrganizationId(visionBotDetail.getOrganizationId());
    data.setProjectId(visionBotDetail.getProjectId());
    data.setProjectName(
        findProjectNameByProjectId(visionBotDetail.getProjectId(), projectDetailList));
    data.setClassificationId(visionBotDetail.getClassificationId());
    data.setCategoryName(format(data.getClassificationId()));
    data.setEnvironment(visionBotDetail.getEnvironment());

    data.setStatus(BotStatus(visionBotDetail, projectDetailList));

    data.setLockedUserId(visionBotDetail.getLockedUserId());

    VisionBotRunDetails runDetails = detailProvider.GetLetestVisionBotRunDetails(data.getId());
    data.setRunning(runDetails != null && runDetails.getRunState() == RunState.Running);
    data.setLastModifiedByUser(visionBotDetail.getLastModifiedByUser());
    data.setLastModifiedTimestamp(visionBotDetail.getLastModifiedTimestamp() != null ? visionBotDetail.getLastModifiedTimestamp().toString() : "");

    productionDocumentsDetailsMap.get(visionBotDetail.getId());

    VisionBotRunDetailsDto visionBotRunDetailsProduction = getVisionBotRunDetailsForProductionMode(
        visionBotDetail
        , productionDocumentsDetailsMap
        , organizationsFileCountCategoryWiseMap);

    VisionBotRunDetailsDto visionBotRunDetailsStaging = null;

    if (runDetailsForStaging != null) {
      visionBotRunDetailsStaging = runDetailsForStaging.stream()
          .filter(v -> v.getVisionbotId().equals(visionBotDetail.getId()))
          .findFirst()
          .orElse(null);
    }

    if (visionBotRunDetailsStaging == null) {
      visionBotRunDetailsStaging = new VisionBotRunDetailsDto();
    }

    FileCount fileCount = organizationsFileCountCategoryWiseMap.get(
        getKeyCombinationGivenId(visionBotDetail.getProjectId(),
            visionBotDetail.getClassificationId()));

    if (fileCount != null) {
      visionBotRunDetailsStaging
          .setTotalFiles((int) fileCount.getStagingFileCount().getTotalCount());
      visionBotRunDetailsProduction
          .setTotalFiles((int) fileCount.getProductionFileCount().getTotalCount());
    }

    if (visionBotDetail.getEnvironment() == Environment.production) {
      data.setVisionBotRunDetailsDto(visionBotRunDetailsProduction);
    } else {
      data.setVisionBotRunDetailsDto(visionBotRunDetailsStaging);
    }

    data.setProductionBotRunDetails(visionBotRunDetailsProduction);
    data.setStagingBotRunDetails(visionBotRunDetailsStaging);
    logger.exit();
    return data;
  }

  private String findProjectNameByProjectId(String projectId,
      List<ProjectDetail> projectDetailList) {
    logger.entry();
    String result = projectDetailList.stream()
        .filter(p -> p.getId().equals(projectId))
        .findFirst()
        .map(ProjectDetail::getName)
        .orElse(null);
    logger.exit();
    return result;
  }

  private VisionBotRunDetailsDto getVisionBotRunDetailsForProductionMode(
      VisionBotDetail visionBotDetail
      , Map<String, ProductionDocumentsDetails> productionDocumentsDetailsMap
      , Map<String, FileCount> organizationsFileCountCategoryWiseMap
  ) {
    logger.entry();
    VisionBotRunDetailsDto visionBotRunDetailsProduction = new VisionBotRunDetailsDto();
    ProductionDocumentsDetails productionDocumentsDetails = productionDocumentsDetailsMap
        .get(visionBotDetail.getId());

    ValidatorReviewDetails validatorReviewDetails_1 = new ValidatorReviewDetails();
    if (productionDocumentsDetails != null) {
      visionBotRunDetailsProduction.setId(visionBotDetail.getId());
      visionBotRunDetailsProduction
          .setTotalFiles((int) productionDocumentsDetails.getProcessedDocumentCount());
      visionBotRunDetailsProduction
          .setProcessedDocumentCount((int) productionDocumentsDetails.getProcessedDocumentCount());
      visionBotRunDetailsProduction
          .setPassedDocumentCount((int) productionDocumentsDetails.getPassedDocumentCount());
      visionBotRunDetailsProduction
          .setFailedDocumentCount((int) productionDocumentsDetails.getFailedFileCount());
      visionBotRunDetailsProduction.setFieldAccuracy(productionDocumentsDetails.getFieldAccuracy());
      visionBotRunDetailsProduction
          .setDocumentProcessingAccuracy(productionDocumentsDetails.getDocumentAccuracy());
      visionBotRunDetailsProduction
          .setTotalFieldCount(productionDocumentsDetails.getTotalFieldCount());
      visionBotRunDetailsProduction
          .setFailedFieldCount(productionDocumentsDetails.getFailedFieldCount());
      visionBotRunDetailsProduction
          .setPassedFieldCount(productionDocumentsDetails.getPassFieldCount());

      validatorReviewDetails_1
          .setFailedFileCount((int) productionDocumentsDetails.getFailedFileCount());
      validatorReviewDetails_1
          .setInvalidFileCount((int) productionDocumentsDetails.getInvalidFileCount());
      validatorReviewDetails_1
          .setReviewFileCount((int) productionDocumentsDetails.getReviewFileCount());
      validatorReviewDetails_1
          .setProcessedCount((int) productionDocumentsDetails.getProcessedDocumentCount());
      validatorReviewDetails_1.setVisionBotId(visionBotDetail.getId());
      validatorReviewDetails_1
          .setAverageReviewTime(productionDocumentsDetails.getAverageReviewTime());
      visionBotRunDetailsProduction.setValidatorReviewDetails(validatorReviewDetails_1);
    }
    logger.exit();
    return visionBotRunDetailsProduction;
  }

  private Status BotStatus(VisionBotDetail visionBotDetail, List<ProjectDetail> projectDetailList) {
    logger.entry();
    Status botStatus = Status.training;
    ProjectDetail projectDetail = projectDetailList.stream()
        .filter(p -> p.getId().equals(visionBotDetail.getProjectId()))
        .findFirst()
        .orElse(null);

    if (projectDetail == null) {
      return Status.training;
    }

    Environment projectStatus;
    if (projectDetail.getEnvironment().equals("production")) {
      projectStatus = Environment.production;
    } else {
      projectStatus = Environment.staging;
    }
    Environment visionBotStatus = visionBotDetail.getEnvironment();

    if ((projectStatus == Environment.staging || projectStatus == Environment.production)
        && visionBotStatus == Environment.staging) {
      botStatus = Status.training;
    } else if (projectStatus == Environment.staging && visionBotStatus == Environment.production) {
      botStatus = Status.ready;
    } else if (projectStatus == Environment.production
        && visionBotStatus == Environment.production) {
      botStatus = Status.active;
    }
    logger.exit();
    return botStatus;
  }

  public Map<String, ValidatorReviewDetails> GetValidatorReviewDetails(
      String organizationId) //,String visionBotId)
  {
    Map<String, ValidatorReviewDetails> validatorReviewDetails = validatorAdapter
        .getValidationDetails(organizationId);//,visionBotId);

    return validatorReviewDetails;
  }

  public void throwNotFoundExceptionIfBotNotPresent(String visionBotID) {
    VisionBot visionBot = new VisionBot();
    String visionbotID = visionBotID;
    visionBot.setId(visionbotID);

    VisionBot result = detailProvider.GetVisionBot("", "", visionBotID);

    if (result == null) {
      logger.error("Visionbot (" + visionBotID + ") is not present on server.");
      throw new DataNotFoundException("Visionbot (" + visionBotID + ") is not present on server.");
    }
  }

  @Override
  public void throwExceptionIfVisionBotDataDoesNotExistForProject(String projectID,
      String categoryId, String orgId) {
    List<VisionBotDetail> result = detailProvider.GetVisionBotDetails(orgId, projectID, categoryId);
    if (result == null || result.size() == 0) {
      logger.error("Visionbot does not exists");
      throw new DataNotFoundException(" Visionbot does not exists");
    }
  }

  public StagingFilesSummary getSummaryDetails(String orgId, String projectId) {
    logger.entry();
    List<VisionBotRunDetailsDto> outputVisionBotRunDetailsDto = new ArrayList<>();

    List<VisionBotRunDetailsDto> runDetailsForStaging = detailProvider
        .getOrganizationWiseRunDetailsDtoForStaging(orgId, projectId);
    if (runDetailsForStaging == null) {
      throw new DatabaseException("Unable to retrieve staging data from database");
    }
    if (runDetailsForStaging.size() <= 0) {
      return new StagingFilesSummary();
    }

    HashMap<String, Date> latestVisionBotRunDetails = new HashMap<>();
    HashMap<String, VisionBotRunDetailsDto> visionBotRunDetailsDtoHashMap = new HashMap<>();

    runDetailsForStaging.forEach((item) -> {
      String visionBotId = item.getVisionbotId();
      Date currentVisionBotDate = item.getStartTime();
      String key = visionBotId + currentVisionBotDate;
      visionBotRunDetailsDtoHashMap.put(key, item);
      if (latestVisionBotRunDetails.containsKey(visionBotId)) {
        Date existingVisionBotDate = latestVisionBotRunDetails.get(visionBotId);
        if (currentVisionBotDate.after(existingVisionBotDate)) {
          latestVisionBotRunDetails.put(visionBotId, currentVisionBotDate);
          visionBotRunDetailsDtoHashMap.put(key, item);
        }
      } else {
        latestVisionBotRunDetails.put(visionBotId, item.getStartTime());
      }
    });

    for (Map.Entry<String, Date> entry : latestVisionBotRunDetails.entrySet()) {
      String key = entry.getKey() + entry.getValue();
      outputVisionBotRunDetailsDto.add(visionBotRunDetailsDtoHashMap.get(key));
    }
    logger.exit();
    return convertVisionBotRunDetailsToSummaryDetails(outputVisionBotRunDetailsDto);
  }

  private StagingFilesSummary convertVisionBotRunDetailsToSummaryDetails(
      List<VisionBotRunDetailsDto> runDetails) {
    logger.entry();
    StagingFilesSummary stagingSummary = new StagingFilesSummary();
    if (runDetails.size() > 0) {
      int totalFilesCount = 0, totalPassedFileCount = 0, totalFailedFileCount = 0, totalTestdFileCount = 0;
      int totalPassedFieldCount = 0, totalFieldCount = 0;

      for (int i = 0; i < runDetails.size(); i++) {
        VisionBotRunDetailsDto visionBotRundetails = runDetails.get(i);
        totalFilesCount += visionBotRundetails.getTotalFiles();
        totalPassedFileCount += visionBotRundetails.getPassedDocumentCount();
        totalFailedFileCount += visionBotRundetails.getFailedDocumentCount();
        totalTestdFileCount += visionBotRundetails.getProcessedDocumentCount();
        totalFieldCount += visionBotRundetails.getTotalFieldCount();
        totalPassedFieldCount += visionBotRundetails.getPassedFieldCount();
      }

      stagingSummary.setTotalFilesCount(totalFilesCount);
      stagingSummary.setTotalPassedFileCount(totalPassedFileCount);
      stagingSummary.setTotalFailedFileCount(totalFailedFileCount);
      stagingSummary.setTotalTestedFileCount(totalTestdFileCount);
      double accuracy = 0.0;
      if (totalFieldCount > 0) {
        accuracy = ((double) totalPassedFieldCount / totalFieldCount) * 100;
      }
      stagingSummary.setAccuracy(Math.floor(accuracy));
    }
    logger.exit();
    return stagingSummary;
  }

  public VisionBotCount getVisionBotCounts(String organizationId, String projectId) {
    logger.entry();
    VisionBotCount botCount = new VisionBotCount();
    botCount.setNumberOfStagingBots(
        detailProvider.getVisionBotCountStateWise(organizationId, projectId, Environment.staging));
    botCount.setNumberOfProductionBots(detailProvider
        .getVisionBotCountStateWise(organizationId, projectId, Environment.production));
    botCount.setNumberOfBotsCreated(detailProvider.getVisionBotCount(organizationId, projectId));
    logger.exit();
    return botCount;
  }

}
