package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mukesh.Methaniya on 16-03-2017.
 */
public class ProductionDocumentsDetails {

  @JsonProperty(value = "id")
  private String id;

  @JsonProperty(value = "failedFieldCount")
  private double failedFieldCount = 0.0;

  @JsonProperty(value = "passedFieldCount")
  private double passFieldCount = 0.0;

  @JsonProperty(value = "totalFieldCount")
  private double totalFieldCount = 0.0;

  @JsonProperty(value = "fieldAccuracy")
  private double fieldAccuracy = 0.0;

  @JsonProperty(value = "reviewFileCount")
  private long reviewFileCount = 0;

  @JsonProperty(value = "invalidFileCount")
  private long invalidFileCount = 0;

  @JsonProperty(value = "passedDocumentCount")
  private long passedDocumentCount = 0;

  @JsonProperty(value = "processedDocumentCount")
  private long processedDocumentCount = 0;

  @JsonProperty(value = "documentAccuracy")
  private long documentAccuracy = 0;

  @JsonProperty(value = "averageReviewTimeInSeconds")
  private long averageReviewTime = 0;

  @JsonProperty(value = "failedDocumentCount")
  private long failedFileCount = 0;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public double getFailedFieldCount() {
    return failedFieldCount;
  }

  public void setFailedFieldCount(double failedFieldCount) {
    this.failedFieldCount = failedFieldCount;
  }

  public double getPassFieldCount() {
    return passFieldCount;
  }

  public void setPassFieldCount(double passFieldCount) {
    this.passFieldCount = passFieldCount;
  }

  public double getTotalFieldCount() {
    return totalFieldCount;
  }

  public void setTotalFieldCount(double totalFieldCount) {
    this.totalFieldCount = totalFieldCount;
  }

  public double getFieldAccuracy() {
    return fieldAccuracy;
  }

  public void setFieldAccuracy(double fieldAccuracy) {
    this.fieldAccuracy = fieldAccuracy;
  }

  public long getReviewFileCount() {
    return reviewFileCount;
  }

  public void setReviewFileCount(long reviewFileCount) {
    this.reviewFileCount = reviewFileCount;
  }

  public long getInvalidFileCount() {
    return invalidFileCount;
  }

  public void setInvalidFileCount(long invalidFileCount) {
    this.invalidFileCount = invalidFileCount;
  }

  public long getPassedDocumentCount() {
    return passedDocumentCount;
  }

  public void setPassedDocumentCount(long passedDocumentCount) {
    this.passedDocumentCount = passedDocumentCount;
  }

  public long getProcessedDocumentCount() {
    return processedDocumentCount;
  }

  public void setProcessedDocumentCount(long processedDocumentCount) {
    this.processedDocumentCount = processedDocumentCount;
  }

  public long getDocumentAccuracy() {
    return documentAccuracy;
  }

  public void setDocumentAccuracy(long documentAccuracy) {
    this.documentAccuracy = documentAccuracy;
  }

  public long getAverageReviewTime() {
    return averageReviewTime;
  }

  public void setAverageReviewTime(long averageReviewTime) {
    this.averageReviewTime = averageReviewTime;
  }

  public long getFailedFileCount() {
    return failedFileCount;
  }

  public void setFailedFileCount(long failedFileCount) {
    this.failedFileCount = failedFileCount;
  }
}
