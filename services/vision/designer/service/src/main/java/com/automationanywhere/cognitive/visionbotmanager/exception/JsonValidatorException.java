package com.automationanywhere.cognitive.visionbotmanager.exception;

import java.util.List;

public class JsonValidatorException extends RuntimeException {

  private static final long serialVersionUID = 7859290529982935446L;
  private final List<JsonValidatorError> errors;

  public JsonValidatorException(
      final List<JsonValidatorError> errors
  ) {
    this.errors = errors;
  }

  public JsonValidatorException(
      final List<JsonValidatorError> errors,
      final Throwable cause
  ) {
    super(cause);
    this.errors = errors;
  }

  /**
   * Returns the errors.
   *
   * @return the value of errors
   */
  public List<JsonValidatorError> getErrors() {
    return errors;
  }
}
