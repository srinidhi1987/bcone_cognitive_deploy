package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class LineDto {

  private final int x1;
  private final int y1;
  private final int x2;
  private final int y2;

  public LineDto(
      @JsonProperty("X1") final int x1,
      @JsonProperty("Y1") final int y1,
      @JsonProperty("X2") final int x2,
      @JsonProperty("Y2") final int y2
  ) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  /**
   * Returns the x1.
   *
   * @return the value of x1
   */
  @JsonProperty("X1")
  public int getX1() {
    return x1;
  }

  /**
   * Returns the y1.
   *
   * @return the value of y1
   */
  @JsonProperty("Y1")
  public int getY1() {
    return y1;
  }

  /**
   * Returns the x2.
   *
   * @return the value of x2
   */
  @JsonProperty("X2")
  public int getX2() {
    return x2;
  }

  /**
   * Returns the y2.
   *
   * @return the value of y2
   */
  @JsonProperty("Y2")
  public int getY2() {
    return y2;
  }
}
