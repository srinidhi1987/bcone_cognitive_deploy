package com.automationanywhere.cognitive.visionbotmanager.service;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.RpcClient;
import com.automationanywhere.cognitive.visionbotmanager.exception.JsonPatchException;
import com.automationanywhere.cognitive.visionbotmanager.exception.RpcClientException;
import com.automationanywhere.cognitive.visionbotmanager.json.JsonMapper;
import com.automationanywhere.cognitive.visionbotmanager.json.JsonValidator;
import com.automationanywhere.cognitive.visionbotmanager.repositories.VisionBotRepository;
import com.automationanywhere.cognitive.visionbotmanager.service.model.DocumentId;
import com.automationanywhere.cognitive.visionbotmanager.service.model.LayoutId;
import com.automationanywhere.cognitive.visionbotmanager.service.model.PageBoundary;
import com.automationanywhere.cognitive.visionbotmanager.service.model.VisionBotEntity;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.rabbitmq.client.ConnectionFactory;
import java.io.InputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class VisionBotService {

  private static final AALogger LOGGER = AALogger.create(VisionBotService.class);
  private static final String QUEUE_GENERATE_AND_EXTRACT_BOT_DATA = "QUEUE_Visionbot_GenerateAndExtractVBotData";
  private static final String QUEUE_EXTRACT_VISION_BOT_DATA = "QUEUE_Visionbot_ExtractVBotData";
  private static final String TYPE_KEY = "$type";
  private static final String LAYOUT_KEY = "Layout";
  private static final String SIR_FIELDS_KEY = "SirFields";
  private static final String LINES_KEY = "Lines";
  private static final String DOC_PROPERTIES_KEY = "DocProperties";
  private static final String FIELDS_KEY = "Fields";
  private static final String FIELD_KEY = "Field";
  private static final String BOUNDS_KEY = "Bounds";
  private static final String VALUE_BOUNDS_KEY = "ValueBounds";
  private static final String VALUES_KEY = "$values";
  private static final String Y1_KEY = "Y1";
  private static final String REGIONS_KEY = "Regions";
  private static final String PAGE_PROPERTIES_KEY = "PageProperties";
  private static final String HEIGHT_KEY = "Height";
  private static final String SEGMENTED_DOC_KEY = "SegmentedDoc";
  private static final String DATA_KEY = "Data";
  private static final String FIELD_DATA_RECORD_KEY = "FieldDataRecord";
  private static final String TABLE_DATA_RECORD_KEY = "TableDataRecord";
  private static final String ROWS_KEY = "Rows";
  private static final String VALUE_KEY = "Value";

  private final VisionBotRepository visionBotRepository;
  private final JsonMapper parser;
  private final JsonValidator validator;
  private final ConnectionFactory connectionFactory;
  private final int visionBotLockedTimeout;

  @Autowired
  public VisionBotService(
      final VisionBotRepository visionBotRepository,
      final JsonMapper jsonMapper,
      final JsonValidator jsonValidator,
      @Qualifier("rpcConnectionfactory") final ConnectionFactory connectionFactory,
      @Value("${VisionBotLockedTimeout:15}") int visionBotLockedTimeout
  ) {
    this.visionBotRepository = visionBotRepository;
    this.parser = jsonMapper;
    this.validator = jsonValidator;
    this.connectionFactory = connectionFactory;
    this.visionBotLockedTimeout = visionBotLockedTimeout;
  }

  /**
   * List all orders for a specific customer.
   *
   * @param visionBotId the VisionBot ID
   */
  @Transactional
  public void patchVisionBot(
      final String visionBotId,
      final InputStream jsonPatchInputStream,
      final String username
  ) {

    LOGGER.debug("Patch VisionBot {}", visionBotId);
    JsonPatch jsonPatch = parser.buildJsonPatch(jsonPatchInputStream);

    try {
      // Retrieves VisionBot data
      VisionBotEntity visionBot = visionBotRepository.getVisionBot(visionBotId);
      LOGGER.debug("Retrieved VisionBot: {}", visionBot);
      String data = visionBot.getData();
      String validationData = visionBot.getValidationData();

      // Parses string to JsonNode and applies the patch
      LOGGER.debug("VisionBot data after replace chars: {}", data);
      JsonNode jsonNode = parser.buildJsonNode(data);
      JsonNode patched = jsonPatch.apply(jsonNode);

      // Writes the JsonNode as string
      data = parser.toString(patched);
      LOGGER.debug("VisionBot data after patch applied: {}", data);

      validator.validateVisionBot(data);

      // Saves the data to repository
      visionBotRepository.save(new VisionBotEntity(visionBot.getId(), data, validationData));
      LOGGER.debug("Saved VisionBot: {}", visionBot);

      // update the user details and timestamp
      if (!StringUtils.isEmpty(username)) {
        visionBotRepository.updateVisionBotDetail(visionBot.getId(), username);
        LOGGER.debug("Saved VisionBot: {}", visionBot);
      }

    } catch (com.github.fge.jsonpatch.JsonPatchException e) {
      throw new JsonPatchException(e.getMessage(), e);
    }
  }

  @Transactional
  public void unlockVisionBots(String username) {
    visionBotRepository.unlockVisionBots(username);
  }

  @Transactional
  public void keepAliveLockedVisionBot(String visionBotId) {
    visionBotRepository.keepAliveLockedVisionBot(visionBotId);
  }

  @Transactional
  public boolean unlockedExpiredVisionBots() {
    boolean result = false;
    try {
      visionBotRepository.unlockedExpiredVisionBots(visionBotLockedTimeout);
      result = true;

    } catch (Exception e) {
      LOGGER.error("Error to unlocked expired Vision Bots", e);
    }
    return result;
  }

  @Transactional
  public void updateAutoMappingData(String visionBotId, String data) {
    visionBotRepository.updateAutoMappingData(visionBotId, data);
  }

  @Transactional
  public String extractPaginatedVBotData(final DocumentId documentId, final Integer page) {
    String requestData = parser.toString(documentId);
    return getPaginatedVBotData(QUEUE_GENERATE_AND_EXTRACT_BOT_DATA, requestData, page);
  }

  @Transactional
  public String extractPaginatedVBotData(final LayoutId layoutId, final Integer page) {
    String requestData = parser.toString(layoutId);
    return getPaginatedVBotData(QUEUE_EXTRACT_VISION_BOT_DATA, requestData, page);
  }

  private String getPaginatedVBotData(String queue, String requestData, final Integer page) {

    String extractedVisionBotData;
    try {
      RpcClient client = new RpcClient(queue, connectionFactory);
      extractedVisionBotData = client.call(requestData);

    } catch (Exception e) {
      throw new RpcClientException("Error sending a message to queue: " + queue, e);
    }

    if (extractedVisionBotData == null || extractedVisionBotData.trim().isEmpty()) {
      throw new RpcClientException("Empty response from queue: " + queue);
    }

    JsonNode extractedVisionBotNode = parser.buildJsonNode(extractedVisionBotData);
    parser.removeKeyRecursively(extractedVisionBotNode, TYPE_KEY);

    PageBoundary pageBoundary = retrievePageBoundary(extractedVisionBotNode, page);
    filterSirFields(extractedVisionBotNode, pageBoundary);
    filterFields(extractedVisionBotNode, pageBoundary);
    filterSegmentedDocRegions(extractedVisionBotNode, pageBoundary);
    filterFieldDataRecord(extractedVisionBotNode, pageBoundary);
    filterTableDataRecord(extractedVisionBotNode, pageBoundary);

    return parser.toString(extractedVisionBotNode);
  }

  private PageBoundary retrievePageBoundary(JsonNode node, int page) {
    ArrayNode pageProperties = (ArrayNode) node
        .path(LAYOUT_KEY)
        .path(DOC_PROPERTIES_KEY)
        .path(PAGE_PROPERTIES_KEY)
        .path(VALUES_KEY);

    if (pageProperties.size() <= page) {
      throw new RuntimeException("Invalid page number " + page + " from " + pageProperties.size());
    }

    long lower = 0;
    for (int i = 0; i < page; i++) {
      lower += pageProperties.get(i).path(HEIGHT_KEY).asLong();
    }
    return new PageBoundary(
        page, lower, lower + pageProperties.get(page).path(HEIGHT_KEY).asLong()
    );
  }

  private void filterSirFields(JsonNode node, PageBoundary pageBoundary) {

    ObjectNode sirFieldsRoot = (ObjectNode) node.path(LAYOUT_KEY).path(SIR_FIELDS_KEY);

    ObjectNode sirFieldsValuesRoot = (ObjectNode) sirFieldsRoot.path(LINES_KEY);
    ArrayNode lines = (ArrayNode) sirFieldsValuesRoot.path(VALUES_KEY);
    ArrayNode filteredLines = parser.createArrayNode();
    for (JsonNode line : lines) {
      long y = line.path(Y1_KEY).asLong();
      if (pageBoundary.within(y)) {
        filteredLines.add(line);
      }
    }
    sirFieldsValuesRoot.replace(VALUES_KEY, filteredLines);

    ObjectNode fieldsValuesRoot = (ObjectNode) sirFieldsRoot.path(FIELDS_KEY);
    ArrayNode fields = (ArrayNode) fieldsValuesRoot.path(VALUES_KEY);
    ArrayNode filteredFields = parser.createArrayNode();
    for (JsonNode field : fields) {
      String boundsText = field.path(BOUNDS_KEY).asText();
      if (pageBoundary.within(boundsText)) {
        filteredFields.add(field);
      }
    }
    fieldsValuesRoot.replace(VALUES_KEY, filteredFields);

    ObjectNode regionsValuesRoot = (ObjectNode) sirFieldsRoot.path(REGIONS_KEY);
    ArrayNode regions = (ArrayNode) regionsValuesRoot.path(VALUES_KEY);
    ArrayNode filteredRegions = parser.createArrayNode();
    for (JsonNode region : regions) {
      String boundsText = region.path(BOUNDS_KEY).asText();
      if (pageBoundary.within(boundsText)) {
        filteredRegions.add(region);
      }
    }
    regionsValuesRoot.replace(VALUES_KEY, filteredRegions);

    ((ObjectNode) node).replace(SIR_FIELDS_KEY, sirFieldsRoot);
  }

  private void filterFields(JsonNode node, PageBoundary pageBoundary) {

    ObjectNode layoutNode = (ObjectNode) node.path(LAYOUT_KEY).path(FIELDS_KEY);
    ArrayNode fields = (ArrayNode) layoutNode.path(VALUES_KEY);

    ArrayNode filteredFields = parser.createArrayNode();
    for (JsonNode field : fields) {
      String boundsText = field.path(BOUNDS_KEY).asText();
      String valueBoundsText = field.path(VALUE_BOUNDS_KEY).asText();
      if (pageBoundary.within(boundsText) || pageBoundary.within(valueBoundsText)) {
        filteredFields.add(field);
      }
    }
    layoutNode.replace(VALUES_KEY, filteredFields);
  }

  private void filterSegmentedDocRegions(JsonNode node, PageBoundary pageBoundary) {

    ObjectNode layoutNode = (ObjectNode) node.path(SEGMENTED_DOC_KEY).path(REGIONS_KEY);
    ArrayNode regions = (ArrayNode) layoutNode.path(VALUES_KEY);

    ArrayNode filteredRegions = parser.createArrayNode();
    for (JsonNode region : regions) {
      String boundsText = region.path(BOUNDS_KEY).asText();
      if (pageBoundary.within(boundsText)) {
        filteredRegions.add(region);
      }
    }
    layoutNode.replace(VALUES_KEY, filteredRegions);
  }

  private void filterFieldDataRecord(JsonNode node, PageBoundary pageBoundary) {

    ObjectNode fieldsNode = (ObjectNode) node.path(DATA_KEY)
        .path(FIELD_DATA_RECORD_KEY).path(FIELDS_KEY);

    ArrayNode fields = (ArrayNode) fieldsNode.path(VALUES_KEY);

    ArrayNode filteredFields = parser.createArrayNode();
    for (JsonNode field : fields) {
      String boundsText = field.path(VALUE_KEY).path(FIELD_KEY).path(BOUNDS_KEY).asText();
      if (pageBoundary.within(boundsText)) {
        filteredFields.add(field);
      }
    }
    fieldsNode.replace(VALUES_KEY, filteredFields);
  }

  private void filterTableDataRecord(JsonNode node, PageBoundary pageBoundary) {

    ArrayNode tables = (ArrayNode) node.path(DATA_KEY).path(TABLE_DATA_RECORD_KEY).path(VALUES_KEY);

    for (JsonNode table : tables) {
      ObjectNode rowsNode = (ObjectNode) table.path(ROWS_KEY);
      ArrayNode rows = (ArrayNode) rowsNode.path(VALUES_KEY);
      ArrayNode filteredRows = parser.createArrayNode();
      for (JsonNode row : rows) {
        boolean hasFields = false;

        ObjectNode fieldNode = (ObjectNode) row.path(FIELDS_KEY);
        ArrayNode fields = (ArrayNode) fieldNode.path(VALUES_KEY);

        ArrayNode filteredFields = parser.createArrayNode();
        for (JsonNode field : fields) {
          String boundsText = field.path(VALUE_KEY).path(FIELD_KEY).path(BOUNDS_KEY).asText();
          if (pageBoundary.within(boundsText)) {
            filteredFields.add(field);
            hasFields = true;
          }
        }
        fieldNode.replace(VALUES_KEY, filteredFields);

        if (hasFields) {
          filteredRows.add(row);
        }
      }
      rowsNode.replace(VALUES_KEY, filteredRows);
    }
  }
}
