package com.automationanywhere.cognitive.visionbotmanager.exception;

public class MessageQueueConnectionCloseException extends RuntimeException {

  private static final long serialVersionUID = -8725407964310825855L;

  public MessageQueueConnectionCloseException(String message, Throwable cause) {
    super(message, cause);
  }

}
