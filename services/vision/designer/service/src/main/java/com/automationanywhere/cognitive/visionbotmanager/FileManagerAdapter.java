package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.visionbotmanager.models.FileCount;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.FileDetail;
import java.util.List;
import org.springframework.context.ApplicationContext;

/**
 * Created by Jemin.Shah on 11-01-2017.
 */
public interface FileManagerAdapter {

  ApplicationContext context = null;

  void setContext(ApplicationContext context);

  List<FileDetail> getSpecificFilesFromVisionBotForStaging(String organizationId,
      String classificationId, String projectId);

  List<FileDetail> getSpecificFilesFromVisionBot(String organizationId, String classificationId,
      String projectId);

  FileCount[] getFileCount(String organizationId);

  FileCount[] getFileCount(String organizationId, String projectId);

  void testConnection();
}
