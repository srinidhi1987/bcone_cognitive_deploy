package com.automationanywhere.cognitive.visionbotmanager;

/**
 * Created by Mukesh.Methaniya on 05-03-2017.
 */
public interface RPCCallBack {

  String actionToPerformed(String input);
}
