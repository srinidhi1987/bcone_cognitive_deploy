package com.automationanywhere.cognitive.visionbotmanager.health.connections;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.visionbotmanager.RpcClient;
import com.rabbitmq.client.ConnectionFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shweta.thakur
 * This class checks the MQ connection health
 */
public class MQCheckConnection implements HealthCheckConnection {

  private ConnectionFactory rpcConnectionfactory;

  public MQCheckConnection(ConnectionFactory rpcConnectionfactory) {
    super();
    this.rpcConnectionfactory = rpcConnectionfactory;
  }

  @Override
  public void checkConnection() {
    RpcClient rpcClient = new RpcClient();
    rpcClient.checkConnection(rpcConnectionfactory);
  }

  @Override
  public List<ServiceConnectivity> checkConnectionForService() {
    // return empty list
    return new ArrayList<ServiceConnectivity>();
  }

}
