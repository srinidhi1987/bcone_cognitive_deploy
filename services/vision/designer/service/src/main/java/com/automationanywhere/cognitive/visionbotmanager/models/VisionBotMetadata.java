package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Created by pooja.wani on 23-01-2017.
 */

public class VisionBotMetadata extends EntityModelBase {

  @JsonProperty("id")
  private String id;

  @JsonProperty("organizationId")
  private String organizationId;

  @JsonProperty("projectId")
  private String projectId;

  @JsonProperty("categoryId")
  private String classificationId;


  @Enumerated(EnumType.STRING)
  @JsonProperty("environment")
  private Environment environment;

  @JsonProperty("lockedUserId")
  private String lockedUserId;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getProjectId() {
    return this.projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getClassificationId() {
    return this.classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  public String getLockeduserid() {
    return this.lockedUserId;
  }

  public void setLockeduserid(String lockeduserid) {
    this.lockedUserId = lockeduserid;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

}
