package com.automationanywhere.cognitive.visionbotmanager.health.connections;

/**
 * @author shweta.thakur
 * This class will hold Dependent micro-services list
 */
public enum DependentServices {
  FileManager, Validator, Project
}
