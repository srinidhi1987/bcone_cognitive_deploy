package com.automationanywhere.cognitive.visionbotmanager.constants;

public class CustomErrorMessageConstant {

  public static final String FAILED_VISION_BOT_STATE_CHANGE = "Encountered error while changing state of one or more bots from your selection. The status of these bots was not changed.";
  public static final String ERROR_PARSING_REQUEST_PARAMETER = "Encountered error while parsing Parameter.";
  public static final String REQUIRED_PARAMETER_MISSING = "Required parameter state or visionbot is missing in request body";
  public static final String VISION_BOT_NOT_FOUND = "VisionBot not found with ID %s";
  public static final String INVALID_BODY = "Error to retrieve data from body";
}
