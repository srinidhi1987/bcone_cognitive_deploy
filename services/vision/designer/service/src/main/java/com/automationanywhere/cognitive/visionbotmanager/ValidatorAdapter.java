package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.visionbotmanager.models.ProductionDocumentsDetails;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.ValidatorReviewDetails;
import java.util.Map;

/**
 * Created by pooja.wani on 08-03-2017.
 */
public interface ValidatorAdapter {

  public Map<String, ValidatorReviewDetails> getValidationDetails(
      String organizationId); //, String visionbotId);

  Map<String, ProductionDocumentsDetails> getProductionDocumentDetails(String organizationId);

  void testConnection();
}
