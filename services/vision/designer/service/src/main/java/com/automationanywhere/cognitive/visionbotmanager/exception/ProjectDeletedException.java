package com.automationanywhere.cognitive.visionbotmanager.exception;

/**
 * Created by Jemin.Shah on 6/29/2017.
 */
public class ProjectDeletedException extends RuntimeException {

  private static final long serialVersionUID = 8409178592362446444L;

  public ProjectDeletedException(String message) {
    super(message);
  }
}
