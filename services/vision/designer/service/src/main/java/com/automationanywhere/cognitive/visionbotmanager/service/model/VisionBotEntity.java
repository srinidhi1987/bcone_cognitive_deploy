package com.automationanywhere.cognitive.visionbotmanager.service.model;

public final class VisionBotEntity {

  private final String id;
  private final String data;
  private final String validationData;

  public VisionBotEntity(
      final String id,
      final String data,
      final String validationData
  ) {
    this.id = id;
    this.data = data;
    this.validationData = validationData;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  public String getId() {
    return id;
  }

  /**
   * Returns the data.
   *
   * @return the value of data
   */
  public String getData() {
    return data;
  }

  /**
   * Returns the validationData.
   *
   * @return the value of validationData
   */
  public String getValidationData() {
    return validationData;
  }

  @Override
  public String toString() {
    return "VisionBotEntity{" +
        "id='" + id + '\'' +
        ", data='" + data + '\'' +
        ", validationData='" + validationData + '\'' +
        '}';
  }
}
