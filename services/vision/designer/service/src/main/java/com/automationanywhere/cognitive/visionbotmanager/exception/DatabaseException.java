package com.automationanywhere.cognitive.visionbotmanager.exception;

/**
 * Created by Mukesh.Methaniya on 22-02-2017.
 */
public class DatabaseException extends RuntimeException {
    
  private static final long serialVersionUID = 2260810023025135080L;

  public DatabaseException(String message) {
    super(message);
  }

  public DatabaseException(String message, Throwable cause) {
    super(message, cause);
  }
}


