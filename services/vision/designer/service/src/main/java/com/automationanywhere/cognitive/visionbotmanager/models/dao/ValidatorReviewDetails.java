package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pooja.wani on 04-03-2017.
 */
public class ValidatorReviewDetails extends EntityModelBase {

  @JsonProperty("visionBotId")
  private String visionBotId;

  @JsonProperty("processedCount")
  private int processedCount = 0;

  @JsonProperty("failedFileCount")
  private int failedFileCount = 0;

  @JsonProperty("reviewFileCount")
  private int reviewFileCount = 0;

  @JsonProperty("invalidFileCount")
  private int invalidFileCount = 0;

  @JsonProperty(value = "averageReviewTime")
  private double averageReviewTime = 0;

  public String getVisionBotId() {
    return visionBotId;
  }

  public void setVisionBotId(String visionBotId) {
    this.visionBotId = visionBotId;
  }

  public int getProcessedCount() {
    return processedCount;
  }

  public void setProcessedCount(int processedCount) {
    this.processedCount = processedCount;
  }

  public int getFailedFileCount() {
    return failedFileCount;
  }

  public void setFailedFileCount(int failedFileCount) {
    this.failedFileCount = failedFileCount;
  }

  public int getReviewFileCount() {
    return reviewFileCount;
  }

  public void setReviewFileCount(int reviewFileCount) {
    this.reviewFileCount = reviewFileCount;
  }

  public int getInvalidFileCount() {
    return invalidFileCount;
  }

  public void setInvalidFileCount(int invalidFileCount) {
    this.invalidFileCount = invalidFileCount;
  }

  public double getAverageReviewTime() {
    return averageReviewTime;
  }

  public void setAverageReviewTime(double averageReviewTime) {
    this.averageReviewTime = averageReviewTime;
  }

}
