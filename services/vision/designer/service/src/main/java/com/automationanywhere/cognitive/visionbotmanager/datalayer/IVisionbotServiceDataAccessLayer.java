package com.automationanywhere.cognitive.visionbotmanager.datalayer;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.automationanywhere.cognitive.visionbotmanager.models.QueryType;
import java.util.List;

/**
 * Created by Mukesh.Methaniya on 15-12-2016.
 */
public interface IVisionbotServiceDataAccessLayer<T> {

  boolean InsertRow(T entityModel);

  boolean UpdateRow(T entityModel, QueryType queryType);

  List<EntityModelBase> SelectRows(T queryDetail, QueryType queryType);

  boolean DeleteRows(T queryDetail, QueryType queryType);

}
