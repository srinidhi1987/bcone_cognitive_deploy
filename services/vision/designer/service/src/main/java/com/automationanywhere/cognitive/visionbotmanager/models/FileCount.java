package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mukesh.Methaniya on 20-03-2017.
 */
public class FileCount {

  @JsonProperty("projectId")
  private String projectId;

  @JsonProperty("categoryId")
  private String classificationId;

  @JsonProperty("stagingFileCount")
  private StagingFileCount stagingFileCount;

  @JsonProperty("productionFileCount")
  private ProductionFileCount productionFileCount;

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public StagingFileCount getStagingFileCount() {
    return stagingFileCount;
  }

  public void setStagingFileCount(StagingFileCount stagingFileCount) {
    this.stagingFileCount = stagingFileCount;
  }

  public ProductionFileCount getProductionFileCount() {
    return productionFileCount;
  }

  public void setProductionFileCount(ProductionFileCount productionFileCount) {
    this.productionFileCount = productionFileCount;
  }
}







