package com.automationanywhere.cognitive.visionbotmanager.impl;

import static com.automationanywhere.cognitive.visionbotmanager.constants.HeaderConstants.CORRELATION_ID;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.visionbotmanager.ValidatorAdapter;
import com.automationanywhere.cognitive.visionbotmanager.exception.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.visionbotmanager.models.ProductionDocumentsDetails;
import com.automationanywhere.cognitive.visionbotmanager.models.StandardResponse;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.ValidatorReviewDetails;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Created by pooja.wani on 08-03-2017.
 */
public class ValidatorAdapterImpl implements ValidatorAdapter {

  private static AALogger logger = AALogger.create(ValidatorAdapter.class);
  private RestTemplate restTemplate;
  private String rootUrl = "http://localhost:9995";
  private String relativeUrl = "/organizations/%s/validator/validationdetails";
  private ApplicationContext context;
  @Autowired
  private RestTemplateWrapper restTemplateWrapper;
  private String HEARTBEAT_URL = "/heartbeat";

  public ValidatorAdapterImpl(String rooturl) {
    this.restTemplate = new RestTemplate();
    this.rootUrl = rooturl;
  }

  public Map<String, ValidatorReviewDetails> getValidationDetails(
      String organizationId) //,String visionbotid)
  {
    logger.traceEntry();
    String url = rootUrl + String.format(relativeUrl, organizationId);
    ValidatorReviewDetails[] validatorDetails = null;
    List<ValidatorReviewDetails> validatorReviewList = new ArrayList<>();
    Map<String, ValidatorReviewDetails> validationData = new HashMap<String, ValidatorReviewDetails>();

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    HttpHeaders headers = new HttpHeaders();
    headers.add("username", "admin");
    headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
    HttpEntity<String> request = new HttpEntity<String>(headers);
    try {
      ResponseEntity<StandardResponse> response = restTemplate
          .exchange(url, HttpMethod.GET, request, StandardResponse.class);
      StandardResponse standardResponse = response.getBody();
      // StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class,e);
      validatorDetails = objectMapper
          .convertValue(standardResponse.getData(), ValidatorReviewDetails[].class);

      for (ValidatorReviewDetails d : validatorDetails) {
        validationData.put(d.getVisionBotId(), d);
      }
      return validationData;
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      // TODO: should we throw it? need to check complete flow to make sure it isnt bubble up to UI Layer.
    }
    logger.exit();
    return validationData;

  }

  public Map<String, ProductionDocumentsDetails> getProductionDocumentDetails(
      String organizationId) //,String visionbotid)
  {
    logger.traceEntry();
    String relativeUrl = "/organizations/%s/validator/processingdetails/visionbotid";
    String url = rootUrl + String.format(relativeUrl, organizationId); //, visionbotid);
    ProductionDocumentsDetails[] validatorDetails = null;
    List<ValidatorReviewDetails> validatorReviewList = new ArrayList<>();
    Map<String, ProductionDocumentsDetails> validationData = new HashMap<String, ProductionDocumentsDetails>();

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    HttpHeaders headers = new HttpHeaders();
    headers.add("username", "admin");
    headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
    HttpEntity<String> request = new HttpEntity<String>(headers);
    try {
      ResponseEntity<StandardResponse> response = restTemplate
          .exchange(url, HttpMethod.GET, request, StandardResponse.class);
      StandardResponse standardResponse = response.getBody();
      // StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class,e);
      validatorDetails = objectMapper
          .convertValue(standardResponse.getData(), ProductionDocumentsDetails[].class);

      for (ProductionDocumentsDetails d : validatorDetails) {
        validationData.put(d.getId(), d);
      }
      return validationData;
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      // TODO: should we throw it? need to check complete flow to make sure it isnt bubble up to UI Layer.
    }
    logger.exit();
    return validationData;

  }

  @SuppressWarnings("unchecked")
  @Override
  public void testConnection() {
    logger.entry();
    String heartBeatURL = rootUrl + HEARTBEAT_URL;
    ResponseEntity<String> standardResponse = null;
    standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL,
        String.class);
    if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
      throw new DependentServiceConnectionFailureException(
          "Error while connecting to Validator service");
    }
    logger.exit();
  }
}
