package com.automationanywhere.cognitive.visionbotmanager;

import static com.automationanywhere.cognitive.visionbotmanager.constants.CustomErrorMessageConstant.INVALID_BODY;
import static com.automationanywhere.cognitive.visionbotmanager.constants.HeaderConstants.CORRELATION_ID;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.patch;
import static spark.Spark.port;
import static spark.Spark.post;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.constants.CustomErrorMessageConstant;
import com.automationanywhere.cognitive.visionbotmanager.constants.HeaderConstants;
import com.automationanywhere.cognitive.visionbotmanager.exception.DataNotFoundException;
import com.automationanywhere.cognitive.visionbotmanager.exception.ExceptionFilter;
import com.automationanywhere.cognitive.visionbotmanager.exception.InvalidJsonPatch;
import com.automationanywhere.cognitive.visionbotmanager.exception.JsonPatchException;
import com.automationanywhere.cognitive.visionbotmanager.exception.JsonValidatorException;
import com.automationanywhere.cognitive.visionbotmanager.exception.ProjectDeletedException;
import com.automationanywhere.cognitive.visionbotmanager.exception.RpcClientException;
import com.automationanywhere.cognitive.visionbotmanager.health.impl.VisionBotManagerHealthCheckHandler;
import com.automationanywhere.cognitive.visionbotmanager.models.CustomResponse;
import com.automationanywhere.cognitive.visionbotmanager.models.StagingFilesSummary;
import com.automationanywhere.cognitive.visionbotmanager.models.Summary;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotCount;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotDTO;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotData;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotMetadata;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.DocumentProccessedDatails;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.TestSet;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotDetail;
import com.automationanywhere.cognitive.visionbotmanager.service.VisionBotService;
import com.automationanywhere.cognitive.visionbotmanager.service.model.DocumentId;
import com.automationanywhere.cognitive.visionbotmanager.service.model.LayoutId;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StringUtils;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;


/**
 * Created by Mukesh.Methaniya on 14-12-2016.
 */
public class VisionbotManagerHandlers extends AbstractHandler {

  private static final String CONTENT_TYPE_JSON_PATCH = "application/json-patch+json";
  private static final String HEADER_ACCEPT_PATCH = "Accept-Patch";
  private static final String HEADER_USERNAME_KEY = "username";
  private static final String HEADER_ORG_ID_KEY = "orgid";
  private static final String HEADER_PRJ_ID_KEY = "prjid";
  private static final String HEADER_CAT_ID_KEY = "id";
  private static final String HEADER_BOT_ID_KEY = "visionbotid";
  private static final String HEADER_LAYOUT_ID_KEY = "layoutid";
  private static final String HEADER_PAGE_INDEX_KEY = "pageindex";
  private static final String HEADER_DOCUMENT_ID_KEY = "documentid";

  private IVisionBotService visionBotService;
  private FileManagerAdapter filemanageradapter;
  private ExceptionFilter exceptionFilter;
  private ConnectionFactory rpcConnectionFactory;
  private VisionBotService vbService;
  private VisionBotManagerHealthCheckHandler visionBotHealthCheckHandler;

  private AALogger logger = AALogger.create(this.getClass());

  private static void addHeader(Request request, Response response) {
    response.type("application/json");
  }

  public void init(ApplicationContext context, String[] args) {

    rpcConnectionFactory = (ConnectionFactory) context.getBean("rpcConnectionfactory");

    visionBotService = (IVisionBotService) context.getBean("visionbotService");
    visionBotHealthCheckHandler = (VisionBotManagerHealthCheckHandler) context
        .getBean("visionBotHealthCheckHandler");
    visionBotService.Init(context);
    jsonObjectMapper = (ObjectMapper) context.getBean("objectMapper");
    jsonObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    exceptionFilter = new ExceptionFilter();
    exceptionFilter.setMapper(jsonObjectMapper);
    exceptionFilter.init();
    filemanageradapter = (FileManagerAdapter) context.getBean("fileManagerAdapter");
    vbService = (VisionBotService) context.getBean("visionBotService");
    // filemanageradapter.Init(context);

    port(Integer.parseInt(args[4]));
    before((request, response) -> {
      preRequestProcessor(request);
    });
    after(((request, response) -> postRequestProcessor(request, response)));
    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/Edit",
        (req, res) -> GetCategoryVisionBotForEdit(req, res));
    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/Unlock",
        (req, res) -> UnLockVisionBot(req, res));
    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots",
        (req, res) -> GetCategoryVisionBot(req, res));
    get("organizations/:orgid/projects/:prjid/categories/:id/visionbotmetadata",
        (req, res) -> GetCategorytVisionbotsMetadata(req, res));
    get("organizations/:orgid/visionbotmetadata",
        (req, res) -> GetOrganizationVisionbotsMetadata(req, res));
    get("organizations/:orgid/projects/:prjid/categories/visionbotmetadata",
        (req, res) -> GetProjectVisionbotsMetadata(req, res));

    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid",
        (req, res) -> GetVisionBot(req, res));
    post("organizations/:orgid/projects/:prjid/categories/:id/visionbots",
        (req, res) -> SaveVisionBot(req, res));
    post("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid",
        (req, res) -> UpdateVisionBot(req, res));
    patch("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid",
        this::patchVisionBot);

    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/validationdata",
        (req, res) -> GetValidationData(req, res));
    post(
        "organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/validationdata",
        (req, res) -> UpdateValidationData(req, res));

    post(
        "/organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/automappingdata",
        this::updateAutoMappingData);

    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/layout/:layoutid/testset",
        (req, res) -> GetTestSet(req, res));
    post(
        "organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/layout/:layoutid/testset",
        (req, res) -> SetTestSet(req, res));
    delete(
        "organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/layout/:layoutid/testset",
        (req, res) -> DeleteTestSet(req, res));

    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/mode/:environment/run",
        (req, res) -> RunAndExtractVBotStaging(req, res));

    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/documents/:documentid/vbotdocument",
        (req, res) -> GenerateAndExtractVBotData(req, res));
    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/layout/:layoutid/vbotdocument",
        (req, res) -> ExtractVBotData(req, res));

    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/documents/:documentid/pages/:pageindex/vbotdocument",
        this::extractPaginatedVBotDataByDocument);
    get("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/layout/:layoutid/pages/:pageindex/vbotdocument",
        this::extractPaginatedVBotDataByLayout);

    post(
        "organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/layout/:layoutid/fielddef/:fielddefid/documents/:documentid/valuefield",
        (req, res) -> GetValueField(req, res));
    get("organizations/:orgid/projects/:prjid/categories/:id/documents/:documentId/pages/:pageindex/image",
        (req, res) -> GetImage(req, res));

    post("organizations/:orgid/projects/:prjid/categories/:id/vbotdata",
        (req, res) -> ExportData(req, res));

    post("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/state",
        (req, res) -> SetVisionBotState(req, res));
    post("organizations/:orgid/projects/:prjid/visionbots/state",
        (req, res) -> UpdateStateOfMultipleBots(req, res));

    get("organizations/:orgid/visionbotdata", (req, res) -> GetOrganizationVisionBotData(req, res));
    get("organizations/:orgid/visionbotdata/:vbotid",
        (req, res) -> GetOrganizationVisionBotDataForSingleVisionbot(req, res));

    get("organizations/:orgid/projects/:projectId/visionbotdata",
        (req, res) -> GetProjectWiseVisionBotData(req, res));

    post(
        "organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/visionbotrundetails/:visionbotrundetailsid/documents/:documentid",
        (req, res) -> setVisionbotRunDetailsData(req, res));
    get("organizations/:orgid/projects/:prjid/summary", (req, res) -> getSummaryDetails(req, res));
    post("/visionbots/unlock", this::unlockVisionBots);
    post("organizations/:orgid/projects/:prjid/categories/:id/visionbots/:visionbotid/keepalive",
        this::keepAliveLockedVisionBot);
    get("/health", ((request, response) -> {
      response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
      response.body("OK");
      return response;
    }));

    get("/heartbeat", ((request, response) -> {
      return getHeartBeatInfo(response);
    }));

    get("/healthcheck", ((request, response) -> {
      return visionBotHealthCheckHandler.checkHealth();
    }));
  }

  private void postRequestProcessor(Request request, Response response) {
    addHeader(request, response);
  }

  private void preRequestProcessor(Request request) {
    addTransactionIdentifier(request);
    logger.debug("Request URI.." + request.uri());
  }

  private void addTransactionIdentifier(Request request) {
    String transactionId = request.headers(CORRELATION_ID);
    ThreadContext.put(CORRELATION_ID, transactionId != null ?
        transactionId : UUID.randomUUID().toString());
  }

  //region VisionBot functionality

  private String setVisionbotRunDetailsData(Request request, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();

    String visionbotrundetailsId = request.params(":visionbotrundetailsid");
    String docId = request.params(":documentid");
    logger.debug(() -> "visionbotrundetailsId: " + visionbotrundetailsId + " docId: " + docId);
    logger.trace("Preparing DocumentProccessedDatails from request.. ");
    String data = "";
    DocumentProccessedDatails documentProccessedDatails = null;
    try {
      data = IOUtils.toString(request.raw().getInputStream());
      documentProccessedDatails = jsonObjectMapper.readValue(data, DocumentProccessedDatails.class);

    } catch (Exception ex) {
      logger.error("Exception in DocumentProccessedDatails from request: " + ex);
      throw new IllegalArgumentException("Parameter is Invalid.");
    }
    documentProccessedDatails = visionBotService
        .updateVisionBotRunDetailsProcessedDocument(visionbotrundetailsId, docId,
            documentProccessedDatails);
    if (documentProccessedDatails != null) {
      logger.trace(
          "Successful updated visionbot run details: " + Thread.currentThread().getStackTrace());
      customResponse.setSuccess(true);
      customResponse.setData(documentProccessedDatails);
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetCategoryVisionBotForEdit(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = request.params(":orgid");
    String projectId = request.params(":prjid");
    String categoryId = request.params(":id");
    String username = request.headers("username");
    logger.debug(
        () -> "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: "
            + categoryId + " username: " + username);
    VisionBotDetail result = visionBotService
        .GetCategoryVisionBotForEdit(organizationId, projectId, categoryId, username);

    if (result != null) {
      logger.trace("Successful fetched the visionbot for specific category");
      customResponse.setData(result.getVisionBot());
      customResponse.setSuccess(true);
    } else {
      logger.trace("No visionbot available for categoryId: " + categoryId);
      customResponse.setData(null);
      customResponse.setSuccess(false);
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetCategoryVisionBot(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();

    String organizationId = request.params(":orgid");
    String projectId = request.params(":prjid");
    String categoryId = request.params(":id");
    logger.debug(
        () -> "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: "
            + categoryId);
    VisionBotDetail result = visionBotService.GetCategoryVisionBot(projectId, categoryId);

    if (result != null) {
      logger.trace("Successful fetched the visionbot.");
      customResponse.setData(result.getVisionBot());
      customResponse.setSuccess(true);
    } else {
      logger.trace("Visionbot details not found.");
      customResponse.setData(null);
      customResponse.setSuccess(false);
    }

    logger.exit();
    return getResponse(customResponse);
  }

  private String GetVisionBot(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotid = request.params(":visionbotid");
    logger.debug(() -> "visionbotid: " + visionbotid);
    VisionBot result = visionBotService.GetVisionBot(visionbotid);
    if (result != null) {
      logger.trace("Successful fetched the visionbot");
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String SaveVisionBot(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = request.params(":orgid");
    String projectID = request.params(":prjid");
    String categoryID = request.params(":id");
    logger.debug(
        () -> "organizationId: " + organizationId + " projectId: " + projectID + " categoryId: "
            + categoryID);
    String data = "";
    try {
      data = IOUtils.toString(request.raw().getInputStream());
    } catch (Exception ex) {
      logger.error("Parameter is Invalid.");
      throw new IllegalArgumentException("Parameter is Invalid.");
    }

    VisionBotDetail result = visionBotService
        .SaveVisionBot(organizationId, projectID, categoryID, data);
    if (result != null && result.getId() != null) {
      logger.trace("Successful saved the visionbot.");
      customResponse.setData(result.getVisionBot());
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String UpdateVisionBot(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotID = "";
    String data = "";
    try {
      visionbotID = request.params(":visionbotid");
      logger.debug("visionbotid: " + visionbotID);
      data = IOUtils.toString(request.raw().getInputStream());
    } catch (Exception ex) {
      logger.error("Parameter is Invalid.");
      throw new IllegalArgumentException("Parameter is Invalid.");
    }

    VisionBot result = visionBotService.UpdateVisionBot(visionbotID, data);
    if (result != null && result.getId() != null) {
      logger.trace("Successful updated the visionbot.");
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String patchVisionBot(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();

    // Validate Header
    String contentType = request.headers(HttpHeaders.CONTENT_TYPE);
    if (!CONTENT_TYPE_JSON_PATCH.equalsIgnoreCase(contentType)) {
      response.status(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE);
      response.header(HEADER_ACCEPT_PATCH, CONTENT_TYPE_JSON_PATCH);
      customResponse.setSuccess(false);
      customResponse.setErrors("[\"Unsupported media type\"]");

    } else {

      String visionBotID = null;
      try {
        visionBotID = request.params(":visionbotid");
        String username = request.headers(HeaderConstants.USERNAME);
        vbService.patchVisionBot(visionBotID, request.raw().getInputStream(), username);

        response.status(HttpStatus.SC_OK);
        customResponse.setSuccess(true);

      } catch (DataNotFoundException e) {
        response.status(HttpStatus.SC_NOT_FOUND);
        customResponse.setSuccess(false);
        customResponse.setErrors("[\"VisionBot not found\"]");
        logger.error("VisionBot not found with ID: " + visionBotID, e);

      } catch (InvalidJsonPatch e) {
        response.status(HttpStatus.SC_BAD_REQUEST);
        customResponse.setSuccess(false);
        customResponse.setErrors("[\"Invalid JSON Patch Document [" + e.getMessage() + "]\"]");
        logger.error("Invalid JSON Patch document", e);

      } catch (JsonValidatorException e) {
        throw e;

      } catch (JsonPatchException e) {
        response.status(HttpStatus.SC_UNPROCESSABLE_ENTITY);
        customResponse.setSuccess(false);
        customResponse.setErrors("[\"Error to apply patch: " + e.getMessage() + "\"]");
        logger.error("Error to apply patch", e);

      } catch (Exception e) {
        response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        customResponse.setSuccess(false);
        customResponse.setErrors("[\"Unexpected error to apply PATCH: " + e.getMessage() + "\"]");
        logger.error("Unexpected error to apply PATCH", e);
      }
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetValidationData(Request request, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String projectID = "";
    String visionbotid = "";
    try {
      projectID = request.params(":prjid");
      visionbotid = request.params(":visionbotid");
      logger.debug("projectID: " + projectID + " visionbotid: " + visionbotid);
    } catch (Exception ex) {
      logger.error("Parameter is Invalid.");
      throw new IllegalArgumentException("Parameter is Invalid.");
    }

    VisionBot result = visionBotService.GetValidationData(visionbotid);
    if (result != null) {
      logger.trace("Successful fetched the data of visionbot");
      customResponse.setData(result.getValidationDatablob());
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }

    logger.exit();
    return getResponse(customResponse);
  }

  private String UpdateValidationData(Request request, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotID = "";
    String data = "";
    try {
      visionbotID = request.params(":visionbotid");
      logger.debug("visionbotid: " + visionbotID);
      data = IOUtils.toString(request.raw().getInputStream());
    } catch (Exception ex) {
      throw new IllegalArgumentException("Parameter is Invalid.");
    }

    VisionBot result = visionBotService.UpdateValidationData(visionbotID, data);

    if (result != null && result.getId() != null) {
      logger.trace("Successful updated validation data of visionbot.");
      customResponse.setData(result.getValidationDatablob());
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private Object updateAutoMappingData(Request request, Response response) {
    logger.entry();
    try {
      CustomResponse customResponse = new CustomResponse();
      String visionBotId = request.params(HEADER_BOT_ID_KEY);
      String data = IOUtils.toString(request.raw().getInputStream());

      vbService.updateAutoMappingData(visionBotId, data);

      logger.trace("Successful updated auto mapping data of VisionBot.");
      customResponse.setSuccess(true);
      return getResponse(customResponse);

    } catch (IOException e) {
      throw new IllegalArgumentException(INVALID_BODY, e);

    } finally {
      logger.exit();
    }
  }

  private String UnLockVisionBot(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = request.params(":orgid");
    String projectId = request.params(":prjid");
    String categoryId = request.params(":id");
    String username = request.headers("username");
    String visionbotid = request.params("visionbotid");
    logger
        .debug("organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
            categoryId + " username: " + username + " visionbotid: " + visionbotid);
    String result = visionBotService.UnLockVisionBot(projectId, categoryId, visionbotid, username);

    if (result != null) {
      logger.trace("Successfully unlock the visionbot.");
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  //update multiple bot state
  private String UpdateStateOfMultipleBots(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    List<String> visionBotDetailList = new ArrayList<>();
    List<String> failedVisionBots = new ArrayList<>();
    VisionBotDTO visionBotDTO = null;

    try {
      String requestData = IOUtils.toString(request.raw().getInputStream());
      visionBotDTO = jsonObjectMapper.readValue(requestData, VisionBotDTO.class);
      logger.debug("visionbotid received : " + visionBotDTO.getVisionBotIds().toArray().toString());
    } catch (Exception ex) {
      logger.error("Error encountered while parsing request parameters");
      throw new IllegalArgumentException(
          CustomErrorMessageConstant.ERROR_PARSING_REQUEST_PARAMETER);
    }

    if (StringUtils.isEmpty(visionBotDTO.getState()) || visionBotDTO.getVisionBotIds().isEmpty()) {
      logger.error("Required parameter state or visionbot is missing in request body");
      throw new IllegalArgumentException(CustomErrorMessageConstant.REQUIRED_PARAMETER_MISSING);
    }
    for (String visionbotID : visionBotDTO.getVisionBotIds()) {
      try {
        VisionBotDetail result = visionBotService
            .SetVisionBotState(visionbotID, visionBotDTO.getState());
        visionBotDetailList.add(visionbotID);
        logger.trace(String
            .format(" Succesfully moved state of vision bot  %s To %s ", visionbotID,
                visionBotDTO.getState()));
      } catch (Exception e) {
        logger.trace("Failed to move state of visionbot ID :" + visionbotID);
        failedVisionBots.add(visionbotID);
      }
    }

    visionBotDTO.setVisionBotIds(visionBotDetailList);
    if (failedVisionBots.isEmpty()) {
      customResponse.setData(visionBotDetailList);
      customResponse.setSuccess(true);
    } else if (visionBotDetailList.isEmpty()) {
      response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      customResponse.setErrors(CustomErrorMessageConstant.FAILED_VISION_BOT_STATE_CHANGE);
      customResponse.setSuccess(false);
    } else {
      response.status(HttpStatus.SC_MULTI_STATUS);
      customResponse.setData(visionBotDetailList);
      customResponse.setSuccess(true);
      customResponse.setErrors(CustomErrorMessageConstant.FAILED_VISION_BOT_STATE_CHANGE);
    }
    logger.exit();
    return getResponse(customResponse);
  }

  //region IQTest functionality
  @SuppressWarnings("unchecked")
  private String SetVisionBotState(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotID = "";
    String data = "";
    String state = "Staging";
    Map<String, Object> dictionary = new HashMap<String, Object>();
    try {
      visionbotID = request.params(":visionbotid");
      logger.debug("visionbotid: " + visionbotID);
      data = IOUtils.toString(request.raw().getInputStream());
      dictionary = jsonObjectMapper.readValue(data, HashMap.class);
      state = dictionary.get("state").toString();

    } catch (Exception ex) {
      logger.error("Parameter is Invalid.");
      throw new IllegalArgumentException("Parameter is Invalid.");
    }

    VisionBotDetail result = visionBotService.SetVisionBotState(visionbotID, state);
    if (result != null && result.getId() != null) {
      customResponse.setData(result.getEnvironment());
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetTestSet(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotid = req.params(":visionbotid");
    String layoutid = req.params(":layoutid");
    logger.debug("visionbotid: " + visionbotid + " layoutid: " + layoutid);
    TestSet result = visionBotService.GetTestSet(visionbotid, layoutid);
    if (result != null) {
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String SetTestSet(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotid = req.params(":visionbotid");
    String layoutid = req.params(":layoutid");
    logger.debug("visionbotid: " + visionbotid + " layoutid: " + layoutid);
    TestSet testset;
    try {
      String data = IOUtils.toString(req.raw().getInputStream());
      testset = jsonObjectMapper.readValue(data, TestSet.class);
    } catch (Exception ex) {
      throw new IllegalArgumentException("Unable to parse TestSet.");
    }

    TestSet result = visionBotService.SetTestSet(visionbotid, layoutid, testset);
    if (result != null && result.getId() != null) {
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  //region VisionBot MetaData functionality

  private String DeleteTestSet(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String visionbotid = req.params(":visionbotid");
    String layoutid = req.params(":layoutid");
    logger.debug("visionbotid: " + visionbotid + " layoutid: " + layoutid);
    visionBotService.DeleteTestSet(visionbotid, layoutid);
    customResponse.setSuccess(true);
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetCategorytVisionbotsMetadata(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = req.params(":orgid");
    String projectId = req.params(":prjid");
    String categoryId = req.params(":id");
    logger
        .debug("organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
            categoryId);
    List<VisionBotMetadata> result = visionBotService
        .GetCategoryVisionBotsMetadata(organizationId, projectId, categoryId);

    if (result != null) {
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
      customResponse.setData(null);
      customResponse.setSuccess(false);
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetProjectVisionbotsMetadata(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = req.params(":orgid");
    String projectId = req.params(":prjid");
    logger.debug("organizationId: " + organizationId + " projectId: " + projectId);
    List<VisionBotMetadata> result = visionBotService
        .GetProjectVisionBotsMetadata(organizationId, projectId);
    if (result != null) {
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetOrganizationVisionbotsMetadata(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = req.params(":orgid");
    logger.debug("organizationId: " + organizationId);
    List<VisionBotMetadata> result = visionBotService
        .GetOrganizationVisionBotsMetadata(organizationId);
    if (result != null) {
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetOrganizationVisionBotData(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = req.params(":orgid");
    logger.debug("organizationId: " + organizationId);
    ArrayList<String> id = new ArrayList<>();
    List<VisionBotData> result = visionBotService.GetOrganizationVisionBotData(organizationId, id);

    if (result != null) {
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
      customResponse.setData(new ArrayList<VisionBotData>());
    }
    res.type("application/json");

    logger.exit();
    return getResponse(customResponse);
  }

  private String GetOrganizationVisionBotDataForSingleVisionbot(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = req.params(":orgid");
    logger.debug("organizationId: " + organizationId + " visiobotid: " + req.params(":vbotid"));
    ArrayList<String> id = new ArrayList<>();
    id.add(req.params(":vbotid"));

    List<VisionBotData> result = visionBotService.GetOrganizationVisionBotData(organizationId, id);

    if (result != null) {
      logger.trace("Successfully fetch the data for single visionbot ");
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    res.type("application/json");

    logger.exit();
    return getResponse(customResponse);
  }

  //region Engine functionality Based on RabbitMQ

  private String GetProjectWiseVisionBotData(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    String organizationId = req.params(":orgid");
    String projectId = req.params(":projectId");
    String categoryId = req.params(":categoryId");
    logger
        .debug("organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
            categoryId);
    List<VisionBotData> result = visionBotService
        .GetOrganizationVisionBotData(organizationId, projectId);
    if (result != null) {
      logger.trace("Successfully fetch the data for single visionbot ");
      customResponse.setData(result);
      customResponse.setSuccess(true);
    } else {
      logger.trace("Data not found.");
    }
    res.type("application/json");
    logger.exit();
    return getResponse(customResponse);
  }

  private String extractPaginatedVBotDataByDocument(Request req, Response resp) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    try {
      String orgId = req.params(HEADER_ORG_ID_KEY);
      String prjId = req.params(HEADER_PRJ_ID_KEY);
      String catId = req.params(HEADER_CAT_ID_KEY);
      String vBotId = req.params(HEADER_BOT_ID_KEY);
      String docId = req.params(HEADER_DOCUMENT_ID_KEY);
      String page = req.params(HEADER_PAGE_INDEX_KEY);

      String data = vbService.extractPaginatedVBotData(
          new DocumentId(orgId, prjId, catId, vBotId, docId), Integer.parseInt(page)
      );

      customResponse.setSuccess(true);
      customResponse.setData(data);

    } catch (RpcClientException e) {
      throw e;

    } catch (Exception e) {
      logger.error("Error to extract VisionBot data", e);

      resp.status(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      customResponse.setSuccess(false);
      customResponse.setErrors("[\"Error to extract VisionBot data: " + e.getMessage() + "\"]");

    } finally {
      logger.exit();
    }
    return getResponse(customResponse);
  }

  private String extractPaginatedVBotDataByLayout(Request req, Response resp) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    try {
      String orgId = req.params(HEADER_ORG_ID_KEY);
      String prjId = req.params(HEADER_PRJ_ID_KEY);
      String catId = req.params(HEADER_CAT_ID_KEY);
      String vBotId = req.params(HEADER_BOT_ID_KEY);
      String layoutId = req.params(HEADER_LAYOUT_ID_KEY);
      String page = req.params(HEADER_PAGE_INDEX_KEY);

      String data = vbService.extractPaginatedVBotData(
          new LayoutId(orgId, prjId, catId, vBotId, layoutId), Integer.parseInt(page)
      );

      customResponse.setSuccess(true);
      customResponse.setData(data);

    } catch (RpcClientException e) {
      throw e;

    }  catch (Exception e) {
      logger.error("Error to extract VisionBot data", e);

      resp.status(HttpStatus.SC_INTERNAL_SERVER_ERROR);
      customResponse.setSuccess(false);
      customResponse.setErrors("[\"Error to extract VisionBot data: " + e.getMessage() + "\"]");

    } finally {
      logger.exit();
    }
    return getResponse(customResponse);
  }

  private String ExtractVBotData(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    logger.traceEntry("Request URI.." + req.uri());
    try {
      String organizationId = req.params(":orgid");
      String projectId = req.params(":prjid");
      String categoryId = req.params(":id");
      String visionbotId = req.params(":visionbotid");
      String layoutId = req.params(":layoutid");
      logger.debug(
          "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
              categoryId + " visionbotId: " + visionbotId + " layoutId: " + layoutId);
      visionBotService.throwExceptionIfVisionBotDataDoesNotExistForProject(projectId, categoryId,
          organizationId);

      Map<String, Object> dictionary = new HashMap<String, Object>();
      dictionary.put("organizationId", organizationId);
      dictionary.put("projectId", projectId);
      dictionary.put("categoryId", categoryId);
      dictionary.put("visionbotId", visionbotId);
      dictionary.put("layoutId", layoutId);

      String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(dictionary);

      RpcClient client = new RpcClient("QUEUE_Visionbot_ExtractVBotData", rpcConnectionFactory);
      logger.info(
          " [x] Requesting ExtractVBotData(" + organizationId + projectId + categoryId + visionbotId
              + layoutId + ")");
      String response = client.call(requestData);
      logger.info(" [.] Got '" + response + "'");
      customResponse.setSuccess(true);
      customResponse.setData(response);
      client.close();
    } catch (ProjectDeletedException pde) {
      logger.error(pde.getMessage());
      throw pde;
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      customResponse.setSuccess(false);
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String RunAndExtractVBotStaging(Request req, Response res) throws Exception {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();

    String organizationId = req.params(":orgid");
    String visionbotId = req.params(":visionbotid");

    visionBotService.RunAndExtractVBotStaging(visionbotId);
    customResponse.setSuccess(true);

    return getResponse(customResponse);
  }

  private String GenerateAndExtractVBotData(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    try {
      String organizationId = req.params(":orgid");
      String projectId = req.params(":prjid");
      String categoryId = req.params(":id");
      String visionbotId = req.params(":visionbotid");
      String documentId = req.params(":documentid");
      logger.debug(
          "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
              categoryId + " visionbotId: " + visionbotId + " documentId: " + documentId);
      visionBotService.throwExceptionIfVisionBotDataDoesNotExistForProject(projectId, categoryId,
          organizationId);

      Map<String, Object> dictionary = new HashMap<String, Object>();
      dictionary.put("organizationId", organizationId);
      dictionary.put("projectId", projectId);
      dictionary.put("categoryId", categoryId);
      dictionary.put("visionbotId", visionbotId);
      dictionary.put("documentId", documentId);

      String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(dictionary);

      RpcClient client = new RpcClient("QUEUE_Visionbot_GenerateAndExtractVBotData",
          rpcConnectionFactory);
      String response = client.call(requestData);
      logger.debug("Response: " + response);

      customResponse.setData(response);
      client.close();
    } catch (DataNotFoundException pde) {
      logger.error(pde.getMessage());
      throw pde;
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      customResponse.setSuccess(false);

    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetValueField(Request req, Response res) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    try {
      String organizationId = req.params(":orgid");
      String projectId = req.params(":prjid");
      String categoryId = req.params(":id");
      String visionbotId = req.params(":visionbotid");
      String layoutId = req.params(":layoutid");
      String fieldDefId = req.params(":fielddefid");
      String documentId = req.params(":documentid");
      logger.debug(
          "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
              categoryId + " visionbotId: " + visionbotId + " documentId: " + documentId
              + " layoutId: " +
              layoutId + " fieldDefId: " + fieldDefId);
      visionBotService.throwExceptionIfVisionBotDataDoesNotExistForProject(projectId, categoryId,
          organizationId);
      String fieldLayout = IOUtils.toString(req.raw().getInputStream());

      Map<String, Object> dictionary = new HashMap<String, Object>();

      dictionary.put("organizationId", organizationId);
      dictionary.put("projectId", projectId);
      dictionary.put("categoryId", categoryId);
      dictionary.put("visionbotId", visionbotId);
      dictionary.put("layoutId", layoutId);
      dictionary.put("fieldLayout", fieldLayout);
      dictionary.put("fieldDefId", fieldDefId);
      dictionary.put("documentId", documentId);

      String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(dictionary);

      RpcClient client = new RpcClient("QUEUE_Visionbot_GetValueField", rpcConnectionFactory);
      String response = client.call(requestData);
      logger.debug("Response: " + response);
      customResponse.setData(response);
      client.close();
    } catch (DataNotFoundException dnfe) {
      logger.error(dnfe.getMessage());
      throw dnfe;
    } catch (Exception ex) {
      customResponse.setSuccess(false);
      logger.error(ex.getMessage());
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String GetImage(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    try {
      String organizationId = request.params(":orgid");
      String projectId = request.params(":prjid");
      String categoryId = request.params(":id");
      String documentId = request.params(":documentid");
      String pageIndex = request.params(":pageindex");
      logger.debug(
          "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
              categoryId + " documentId: " + documentId + " pageIndex: " +
              pageIndex);
      Map<String, Object> dictionary = new HashMap<String, Object>();

      dictionary.put("organizationId", organizationId);
      dictionary.put("projectId", projectId);
      dictionary.put("categoryId", categoryId);
      dictionary.put("documentId", documentId);
      dictionary.put("pageIndex", pageIndex);

      String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(dictionary);

      RpcClient client = new RpcClient("QUEUE_GetImage", rpcConnectionFactory);
      String res = client.call(requestData);
      logger.debug("Response: " + response);
      customResponse.setData(res);
      client.close();
    } catch (ProjectDeletedException pde) {
      logger.error(pde.getMessage());
      throw pde;
    } catch (Exception ex) {
      customResponse.setSuccess(false);
      logger.error(ex.getMessage());
    }
    logger.exit();
    return getResponse(customResponse);
  }

  private String ExportData(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    logger.traceEntry("Request URI.." + request.uri());
    try {

      String organizationId = request.params(":orgid");
      String projectId = request.params(":prjid");
      String categoryId = request.params(":id");
      String vbotData = IOUtils.toString(request.raw().getInputStream());
      logger.debug(
          "organizationId: " + organizationId + " projectId: " + projectId + " categoryId: " +
              categoryId);
      visionBotService.throwExceptionIfVisionBotDataDoesNotExistForProject(projectId, categoryId,
          organizationId);

      Map<String, Object> dictionary = new HashMap<String, Object>();
      dictionary.put("organizationId", organizationId);
      dictionary.put("projectId", projectId);
      dictionary.put("vBotData", vbotData);

      String requestData = jsonObjectMapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(dictionary);

      RpcClient client = new RpcClient("QUEUE_ExportData", rpcConnectionFactory);
      String res = client.call(requestData);
      logger.debug("Response: " + response);
      customResponse.setSuccess(true);
      customResponse.setData(res);
      client.close();
    } catch (ProjectDeletedException pde) {
      logger.error(pde.getMessage());
      throw pde;
    } catch (Exception ex) {
      customResponse.setSuccess(false);
      logger.error(ex.getMessage());
    }
    logger.exit();
    return getResponse(customResponse);
  }

  //region ProjectWise visionbot runnning detail Summary
  private String getSummaryDetails(Request request, Response response) {
    logger.entry();
    CustomResponse customResponse = new CustomResponse();
    logger.traceEntry("Request URI.." + request.uri());

    String organizationId = request.params(":orgid");
    String projectId = request.params(":prjid");

    StagingFilesSummary stagingFileDetails = visionBotService
        .getSummaryDetails(organizationId, projectId);
    VisionBotCount botCounts = visionBotService.getVisionBotCounts(organizationId, projectId);

    Summary summary = new Summary();
    summary.setStagingSummary(stagingFileDetails);
    summary.setBotDetails(botCounts);

    customResponse.setData(summary);
    customResponse.setSuccess(true);
    response.type("application/json");

    logger.exit();
    return getResponse(customResponse);
  }

  private String unlockVisionBots(Request req, Response res) {
    try {
      logger.entry();

      CustomResponse customResponse = new CustomResponse();

      String username = req.headers(HEADER_USERNAME_KEY);
      vbService.unlockVisionBots(username);

      customResponse.setSuccess(true);
      res.type(ContentType.APPLICATION_JSON.getMimeType());

      return getResponse(customResponse);

    } finally {
      logger.exit();
    }
  }

  private String keepAliveLockedVisionBot(Request request, Response res) {
    try {
      logger.entry();

      CustomResponse customResponse = new CustomResponse();
      String visionBotId = request.params("visionbotid");
      logger.debug(String.format("Keep alive locked VisionBot %s", visionBotId));

      vbService.keepAliveLockedVisionBot(visionBotId);

      customResponse.setSuccess(true);
      res.type(ContentType.APPLICATION_JSON.getMimeType());

      return getResponse(customResponse);

    } finally {
      logger.exit();
    }
  }

  /**
   * Get the heartbeat of the service
   */
  private Object getHeartBeatInfo(Response response) {
    logger.entry();
    String heartBeat;
    //currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization
    response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
    heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
    response.body(heartBeat);
    logger.exit();
    return response;
  }
}
