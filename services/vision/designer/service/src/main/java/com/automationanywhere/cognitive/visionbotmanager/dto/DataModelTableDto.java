package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class DataModelTableDto {

  private final String id;
  private final String name;
  private final List<DataModelFieldDto> columns;
  private final boolean deleted;

  public DataModelTableDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("Name") final String name,
      @JsonProperty("Columns") final List<DataModelFieldDto> columns,
      @JsonProperty("IsDeleted") final boolean deleted
  ) {
    this.id = id;
    this.name = name;
    this.columns = columns;
    this.deleted = deleted;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  @JsonProperty("Name")
  public String getName() {
    return name;
  }

  /**
   * Returns the columns.
   *
   * @return the value of columns
   */
  @JsonProperty("Columns")
  public List<DataModelFieldDto> getColumns() {
    return columns;
  }

  /**
   * Returns the deleted.
   *
   * @return the value of deleted
   */
  @JsonProperty("IsDeleted")
  public boolean isDeleted() {
    return deleted;
  }
}
