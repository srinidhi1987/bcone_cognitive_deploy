package com.automationanywhere.cognitive.visionbotmanager.models;

/**
 * Created by Mukesh.Methaniya on 15-12-2016.
 */
public enum QueryType {
  byProjectId,
  byCategoryId,
  byProjectIdCategoryId,
  byProjectIdCategoryIdbyEntityId,
  byEntityId,
  ByVisionbotIdLayoutId,
  byCategoryIdAndUnLock,
  Lock

}
