package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.models.RabbitMQSetting;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;

/**
 * Created by Mukesh.Methaniya on 04-03-2017.
 */
public class RpcMsgQ {

  private static AALogger logger = AALogger.create(RpcMsgQ.class);
  public final String hostName;
  public final String virtualHostName;
  public final String queueName;
  public final String userName;
  public final String password;
  public int port;

  public RpcMsgQ(final String queueName, final String hostName) {
    this.hostName = hostName;
    this.queueName = queueName;
    this.virtualHostName = null;
    this.userName = null;
    this.password = null;
  }

  public RpcMsgQ(final String queueName, final RabbitMQSetting setting) {
    this.hostName = setting.getUri();
    this.virtualHostName = setting.getVirtualHost();
    this.userName = setting.getUserName();
    this.password = setting.getPassword();
    this.queueName = queueName;
    this.port = setting.getPort();
  }

  public void Start(RPCCallBack functionality) {
    logger.entry();
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(this.hostName);
    factory.setVirtualHost(this.virtualHostName);
    factory.setUsername(this.userName);
    factory.setPassword(this.password);
    factory.setPort(port);

    try (
        final Connection connection = factory.newConnection();
    ) {
      Channel channel = connection.createChannel();
      channel.queueDeclare(this.queueName,
          true,
          false,
          false,
          null);

      channel.basicQos(0, 1, false);

      Consumer consumer = new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope,
            AMQP.BasicProperties properties, byte[] body)
            throws IOException {
          String message = new String(body, "UTF-8");
          System.out.println(" [x] Received '" + message + "'");
          String response = "";
          AMQP.BasicProperties replyProps = new AMQP.BasicProperties
              .Builder()
              .correlationId(properties.getCorrelationId())
              .deliveryMode(2)
              .build();
          try {
            System.out.println(" [.] functionality(" + message + ")");
            response = functionality.actionToPerformed(message);
          } catch (Exception e) {

            System.out.println(" [.] " + e.getMessage());
            response = "";

          } finally {
            if (!(properties.getReplyTo() == null && properties.getReplyTo().equals(""))) {
              if (response == null) {
                response = "";
              }
              byte[] responseBytes = response.getBytes("UTF-8");
              channel.basicPublish("", properties.getReplyTo(),
                  replyProps, responseBytes);

              channel.basicAck(envelope.getDeliveryTag(),
                  false);
              System.out.println(" [x] Done");
            }

          }
          //-------------

        }
      };
      channel.basicConsume(this.queueName,
          false,
          consumer);

      System.out.println(" [x] Awaiting RPC requests");

      System.out.println(" Press [enter] to exit.");
      System.console().readLine();
      logger.exit();
    } catch (final Exception ex) {
      logger.error("Unhandled error", ex);
    }
  }
}
