package com.automationanywhere.cognitive.visionbotmanager.exception;

public class DependentServiceConnectionFailureException extends RuntimeException {

  private static final long serialVersionUID = 7745918032775486575L;

  public DependentServiceConnectionFailureException() {
    super();
  }

  public DependentServiceConnectionFailureException(String message) {
    super(message);
  }

}
