package com.automationanywhere.cognitive.visionbotmanager.exception;

/**
 * Created by Mukesh.Methaniya on 22-02-2017.
 */
public class DataIsInUseException extends RuntimeException {
	
  private static final long serialVersionUID = 3695784790167576370L;

  public DataIsInUseException(String message) {
    super(message);
  }
}