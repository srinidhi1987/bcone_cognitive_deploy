package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class SirFieldDto {

  private final String id;
  private final String parentId;
  private final String text;
  private final int confidence;
  private final int segmentationType;
  private final int type;
  private final String bounds;
  private final double angle;

  public SirFieldDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("ParentId") final String parentId,
      @JsonProperty("Text") final String text,
      @JsonProperty("Confidence") final int confidence,
      @JsonProperty("SegmentationType") final int segmentationType,
      @JsonProperty("Type") final int type,
      @JsonProperty("Bounds") final String bounds,
      @JsonProperty("Angle") final double angle
  ) {
    this.id = id;
    this.parentId = parentId;
    this.text = text;
    this.confidence = confidence;
    this.segmentationType = segmentationType;
    this.type = type;
    this.bounds = bounds;
    this.angle = angle;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the parentId.
   *
   * @return the value of parentId
   */
  @JsonProperty("ParentId")
  public String getParentId() {
    return parentId;
  }

  /**
   * Returns the text.
   *
   * @return the value of text
   */
  @JsonProperty("Text")
  public String getText() {
    return text;
  }

  /**
   * Returns the confidence.
   *
   * @return the value of confidence
   */
  @JsonProperty("Confidence")
  public int getConfidence() {
    return confidence;
  }

  /**
   * Returns the segmentationType.
   *
   * @return the value of segmentationType
   */
  @JsonProperty("SegmentationType")
  public int getSegmentationType() {
    return segmentationType;
  }

  /**
   * Returns the type.
   *
   * @return the value of type
   */
  @JsonProperty("Type")
  public int getType() {
    return type;
  }

  /**
   * Returns the bounds.
   *
   * @return the value of bounds
   */
  @JsonProperty("Bounds")
  public String getBounds() {
    return bounds;
  }

  /**
   * Returns the angle.
   *
   * @return the value of angle
   */
  @JsonProperty("Angle")
  public double getAngle() {
    return angle;
  }
}
