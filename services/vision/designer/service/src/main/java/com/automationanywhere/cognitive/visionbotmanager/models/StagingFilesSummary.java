package com.automationanywhere.cognitive.visionbotmanager.models;

public class StagingFilesSummary {

  private int totalFilesCount;

  private int totalPassedFileCount;

  private int totalFailedFileCount;

  private int totalTestedFileCount;

  private double accuracy = 0;

  public int getTotalFilesCount() {
    return totalFilesCount;
  }

  public void setTotalFilesCount(int totalFilesCount) {
    this.totalFilesCount = totalFilesCount;
  }

  public int getTotalPassedFileCount() {
    return totalPassedFileCount;
  }

  public void setTotalPassedFileCount(int totalPassedFileCount) {
    this.totalPassedFileCount = totalPassedFileCount;
  }

  public int getTotalFailedFileCount() {
    return totalFailedFileCount;
  }

  public void setTotalFailedFileCount(int totalFailedFileCount) {
    this.totalFailedFileCount = totalFailedFileCount;
  }

  public int getTotalTestedFileCount() {
    return totalTestedFileCount;
  }

  public void setTotalTestedFileCount(int totalTestedFileCount) {
    this.totalTestedFileCount = totalTestedFileCount;
  }

  public double getAccuracy() {
    return accuracy;
  }

  public void setAccuracy(double accuracy) {
    this.accuracy = accuracy;
  }
}
