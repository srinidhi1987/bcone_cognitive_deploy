package com.automationanywhere.cognitive.visionbotmanager.repositories.data;

import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot;
import com.automationanywhere.cognitive.visionbotmanager.service.model.VisionBotEntity;
import org.springframework.stereotype.Component;

@Component
public class VisionBotDataMapper {

  public VisionBotEntity get(final VisionBot data) {
    return new VisionBotEntity(
        data.getId(),
        data.getDatablob(),
        data.getValidationDatablob()
    );
  }
}
