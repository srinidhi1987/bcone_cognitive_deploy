package com.automationanywhere.cognitive.visionbotmanager.constants;

public class HeaderConstants {

  public static final String CORRELATION_ID = "cid";
  public static final String USERNAME = "username";
}
