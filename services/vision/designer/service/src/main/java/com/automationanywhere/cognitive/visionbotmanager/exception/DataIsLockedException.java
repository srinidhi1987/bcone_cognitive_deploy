package com.automationanywhere.cognitive.visionbotmanager.exception;

/**
 * Created by Mayur.Panchal on 22-04-2017.
 */
public class DataIsLockedException extends RuntimeException {

  private static final long serialVersionUID = 258721060173122784L;

  public DataIsLockedException(String message) {
    super(message);
  }
}
