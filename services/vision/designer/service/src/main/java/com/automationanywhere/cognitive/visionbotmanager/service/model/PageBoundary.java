package com.automationanywhere.cognitive.visionbotmanager.service.model;

public class PageBoundary {

  private final int page;
  private final long lower;
  private final long upper;

  public PageBoundary(
      final int page,
      final long lower,
      final long upper
  ) {
    this.page = page;
    this.lower = lower;
    this.upper = upper;
  }

  public boolean within(final long y) {
    return lower < y && upper > y;
  }

  public boolean within(final String bounds) {
    if (bounds.matches("( *0 *,? *){4}")) {
      return true;
    }

    String[] values = bounds.split(",");
    if (values.length != 4) {
      throw new RuntimeException("Invalid boundary: " + bounds);
    }
    final long y = Long.parseLong(values[1].trim());
    return lower < y && upper > y;
  }

  @Override
  public String toString() {
    return "PageBoundary{" +
        "page=" + page +
        ", lower=" + lower +
        ", upper=" + upper +
        '}';
  }
}
