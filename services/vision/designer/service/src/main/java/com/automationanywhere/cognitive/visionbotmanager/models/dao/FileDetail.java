package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jemin.Shah on 11-01-2017.
 */
public class FileDetail {

  @JsonProperty("fileId")
  String fileid;

  @JsonProperty("projectId")
  String projectid;

  @JsonProperty("fileName")
  String filename;

  @JsonProperty("fileLocation")
  String filelocation;

  @JsonProperty("fileSize")
  double filesize;

  @JsonProperty("fileHeight")
  int fileheight;

  @JsonProperty("fileWidth")
  int filewidth;

  @JsonProperty("format")
  String format;

  @JsonProperty("processed")
  boolean processed;

  @JsonProperty("classificationId")
  String classificationid;

  @JsonProperty("uploadrequestId")
  String uploadrequestid;

  @JsonProperty("layoutId")
  String layoutid;

  @JsonProperty("isProduction")
  private Boolean isProduction;

  public String getFileid() {
    return fileid;
  }

  public void setFileid(String fileid) {
    this.fileid = fileid;
  }

  public String getProjectid() {
    return projectid;
  }

  public void setProjectid(String projectId) {
    this.projectid = projectId;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getFilelocation() {
    return filelocation;
  }

  public void setFilelocation(String filelocation) {
    this.filelocation = filelocation;
  }

  public double getFilesize() {
    return filesize;
  }

  public void setFilesize(double filesize) {
    this.filesize = filesize;
  }

  public int getFileheight() {
    return fileheight;
  }

  public void setFileheight(int fileheight) {
    this.fileheight = fileheight;
  }

  public int getFilewidth() {
    return filewidth;
  }

  public void setFilewidth(int filewidth) {
    this.filewidth = filewidth;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public boolean isProcessed() {
    return processed;
  }

  public void setProcessed(boolean processed) {
    this.processed = processed;
  }

  public String getClassificationid() {
    return classificationid;
  }

  public void setClassificationid(String classificationid) {
    this.classificationid = classificationid;
  }

  public String getUploadrequestid() {
    return uploadrequestid;
  }

  public void setUploadrequestid(String uploadrequestid) {
    this.uploadrequestid = uploadrequestid;
  }

  public String getLayoutid() {
    return layoutid;
  }

  public void setLayoutid(String layoutid) {
    this.layoutid = layoutid;
  }

  public Boolean getProduction() {
    return isProduction;
  }

  public void setProduction(Boolean production) {
    isProduction = production;
  }
}
