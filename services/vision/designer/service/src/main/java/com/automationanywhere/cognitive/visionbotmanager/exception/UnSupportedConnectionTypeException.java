package com.automationanywhere.cognitive.visionbotmanager.exception;

public class UnSupportedConnectionTypeException extends RuntimeException {

  private static final long serialVersionUID = 5387235211688393416L;

  public UnSupportedConnectionTypeException(String message) {
    super(message);
  }

}
