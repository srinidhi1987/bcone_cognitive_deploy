/**
 * Copyright (c) 2016 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.visionbotmanager.exception;

//import com.automationanywhere.constants.mapper.ObjectMapper;
//import com.automationanywhere.constants.util.http.HttpStatusCode;
//import com.automationanywhere.constants.validation.ValidationException;

import static spark.Spark.exception;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.models.CustomResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import org.apache.http.HttpStatus;
import org.springframework.dao.DuplicateKeyException;
import spark.Request;
import spark.Response;

//import static com.automationanywhere.constants.util.http.HttpStatusCode.*;
//import static com.automationanywhere.constants.util.http.MediaType.APPLICATION_JSON;

/**
 * This filter handles exceptions that has been thrown from resource/service layer.
 * It maps java exceptions to the corresponding HTTP status/response.
 */
public class ExceptionFilter {
  //  private static final Logger log = LoggerFactory.getLogger(ExceptionFilter.class);

  private ObjectMapper mapper;
  private AALogger logger = AALogger.create(this.getClass());


  public void init() {
    exception(NoSuchElementException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.NOT_FOUND));
    exception(IllegalArgumentException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.BAD_REQUEST));
    exception(IllegalStateException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.BAD_REQUEST));
    //    exception(ValidationException.class, (e, req, res) -> handleException(e, res, HttpStatusCode.BAD_REQUEST));
    exception(DuplicateKeyException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.CONFLICT));
    exception(ProjectDeletedException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.DELETED));
    exception(Exception.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
    exception(Exception.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
    exception(DataNotFoundException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.NOT_FOUND));
    exception(NotFoundException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.NOT_FOUND));
    exception(Exception.class, (e, req, res) -> handleException(e, res, HttpStatusCode.NOT_FOUND));
    exception(DatabaseException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
    exception(DataIsInUseException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.CONFLICT));
    exception(org.springframework.transaction.TransactionException.class,
        (e, req, res) -> handleException(
            new DatabaseException("Internal Server Communication Error encountered."), res,
            HttpStatusCode.INTERNAL_SERVER_ERROR));
    exception(DataIsLockedException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.RESOURCELOCKED));
    exception(RpcClientException.class,
        (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));

    exception(JsonValidatorException.class, this::handleJsonValidatorException);
    // log.debug("ExceptionFilter filter mapping initialized");
  }

  public void setMapper(ObjectMapper mapper) {
    this.mapper = mapper;

  }

  private void handleJsonValidatorException(JsonValidatorException e, Request req, Response resp) {
    try {
      logger.entry();

      List<String> errors = e.getErrors().stream()
          .map(error -> "Validation error [" + error.getPath() + "] " + error.getErrorMessage())
          .collect(Collectors.toList());

      logger.error("JSON validation error:\n  " + String.join("\n  ", errors), e);

      CustomResponse customResponse = new CustomResponse();
      customResponse.setSuccess(false);
      customResponse.setErrors(errors);

      resp.status(HttpStatus.SC_UNPROCESSABLE_ENTITY);
      resp.body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse));

    } catch (Exception ex) {
      logger.error("Exception to handle JsonValidatorException", e);

    } finally {
      logger.exit();
    }
  }

  private void handleException(Exception e, Response res, HttpStatusCode statusCode) {
    logger.entry();
    logger.error(e.getMessage());
    String errorMessage = e.getMessage() == null ? statusCode.getMessage() : e.getMessage();
    res.status(statusCode.getCode());
    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    String customResonseInFormOfJson = "";
    try {
      Error error = new Error(Integer.toString(statusCode.getCode()), errorMessage);
      if (statusCode.getCode() == 423) {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error.getMessage());
        customResponse.setErrors(errorList);
      } else {
        customResponse.setErrors(error);
      }
      customResonseInFormOfJson = mapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(customResponse);
      logger.trace("Exception Handler Response :" + customResonseInFormOfJson);
    } catch (Exception ex) {
      logger.error("Exception Occurred :" + errorMessage);
    }
    res.body(customResonseInFormOfJson);
    logger.exit();
  }

  private class Error {

    private String code;
    private String message;

    public Error(String code, String message) {
      this.code = code;
      this.message = message;
    }

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
