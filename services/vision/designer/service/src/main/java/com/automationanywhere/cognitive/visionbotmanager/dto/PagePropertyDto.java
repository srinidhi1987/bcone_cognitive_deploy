package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class PagePropertyDto {

  private final int pageIndex;
  private final int width;
  private final int height;
  private final String borderRect;

  public PagePropertyDto(
      @JsonProperty("PageIndex") final int pageIndex,
      @JsonProperty("Width") final int width,
      @JsonProperty("Height") final int height,
      @JsonProperty("BorderRect") final String borderRect
  ) {
    this.pageIndex = pageIndex;
    this.width = width;
    this.height = height;
    this.borderRect = borderRect;
  }

  /**
   * Returns the pageIndex.
   *
   * @return the value of pageIndex
   */
  @JsonProperty("PageIndex")
  public int getPageIndex() {
    return pageIndex;
  }

  /**
   * Returns the width.
   *
   * @return the value of width
   */
  @JsonProperty("Width")
  public int getWidth() {
    return width;
  }

  /**
   * Returns the height.
   *
   * @return the value of height
   */
  @JsonProperty("Height")
  public int getHeight() {
    return height;
  }

  /**
   * Returns the borderRect.
   *
   * @return the value of borderRect
   */
  @JsonProperty("BorderRect")
  public String getBorderRect() {
    return borderRect;
  }
}
