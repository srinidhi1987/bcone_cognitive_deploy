package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Mukesh.Methaniya on 09-02-2017.
 */

@XmlRootElement
public class ErrorMessage {

  @JsonProperty("errorMessage")
  private String errorMessage;

  @JsonProperty("errorCode")
  private int errorCode;

  @JsonProperty("documentationLink")
  private String documentationLink;

  public ErrorMessage(String message, int code, String documentationlink) {
    errorMessage = message;
    errorCode = code;
    documentationLink = documentationlink;
  }


  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public int getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(int errorCode) {
    this.errorCode = errorCode;
  }

  public String getDocumentationLink() {
    return documentationLink;
  }

  public void setDocumentationLink(String documentationLink) {
    this.documentationLink = documentationLink;
  }
}
