package com.automationanywhere.cognitive.visionbotmanager.datalayer;

import com.automationanywhere.cognitive.visionbotmanager.exception.DataNotFoundException;
import com.automationanywhere.cognitive.visionbotmanager.models.DocumentProcessingStatus;
import com.automationanywhere.cognitive.visionbotmanager.models.Environment;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotRunDetailsDto;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.DocumentProccessedDatails;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.TestSet;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.TestSetInfo;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotDetail;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotRunDetails;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Mukesh.Methaniya on 31-01-2017.
 */
public class VisionBotDetailProvider {

  private INewVisionBotServiceDataAccessLayer dataLayerProvider;

  public VisionBotDetailProvider(INewVisionBotServiceDataAccessLayer datalayer) {
    dataLayerProvider = datalayer;
  }

  public VisionBotDetail GetVisionBotDetail(String id) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    detachedcriteria.add(Restrictions.eq("id", id));

    List<VisionBotDetail> result = dataLayerProvider
        .Select(VisionBotDetail.class, detachedcriteria);

    if (result != null && result.size() > 0) {
      return result.get(0);
    }

    return null;
  }

  public VisionBotDetail GetVisionBotSpecificDetails(String organizationId, String id) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    detachedcriteria.add(Restrictions.eq("organizationId", organizationId));
    detachedcriteria.add(Restrictions.eq("id", id));

    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result.get(0);
  }

  public List<VisionBotDetail> GetVisionBotDetails(String organizationId) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    detachedcriteria.add(Restrictions.eq("organizationId", organizationId));

    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result;
  }

  public List<VisionBotDetail> GetVisionBotDetails(String organizationId,
      ArrayList<String> visionBotIds) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    detachedcriteria.add(Restrictions.eq("organizationId", organizationId));

    if (visionBotIds != null && visionBotIds.size() > 0) {

      for (String visonBotId : visionBotIds) {
        detachedcriteria.add(Restrictions.eq("id", visonBotId));
      }

    }
    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result;
  }

  public List<VisionBotDetail> GetVisionBotDetails(String organizationId, String projectId) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    if (!organizationId.equals("")) {
      detachedcriteria.add(Restrictions.eq("organizationId", organizationId));
    }
    if (!projectId.equals("")) {
      detachedcriteria.add(Restrictions.eq("projectId", projectId));
    }

    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result;
  }

  public List<VisionBotDetail> GetVisionBotDetails(String organizationId, String projectId,
      String categoryId) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    if (!organizationId.equals("")) {
      detachedcriteria.add(Restrictions.eq("organizationId", organizationId));
    }
    if (!projectId.equals("")) {
      detachedcriteria.add(Restrictions.eq("projectId", projectId));
    }
    if (!categoryId.equals("")) {
      detachedcriteria.add(Restrictions.eq("classificationId", categoryId));
    }

    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result;
  }

  public VisionBotDetail GetVisionBotData(String visionId) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);

    if (!visionId.equals("")) {
      detachedcriteria.add(Restrictions.eq("id", visionId));
    }
    detachedcriteria.add(Restrictions.eq("environment", Environment.staging));

    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result.get(0);
  }

  public DocumentProccessedDatails GetDocumentProcessedDatails(String docProcessedDetailsId,
      String documentId) {
    //  VisionBotRunDetails runDetails=new VisionBotRunDetails();
    //  runDetails.setId(visiobotRunDetailsId);

    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(DocumentProccessedDatails.class);
    detachedcriteria.add(Restrictions.eq("id", docProcessedDetailsId));
    // detachedcriteria.add(Restrictions.eq("docId",documentId));

    List<DocumentProccessedDatails> result = dataLayerProvider.Select(null, detachedcriteria);

    if (result != null && result.size() > 0) {
      return result.get(0);
    }

    throw new DataNotFoundException("Document is not present in record.");
  }

  public List<VisionBotDetail> GetVisionBotDetails(String organizationId, String projectId,
      String categoryId, String userName) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDetail.class);
    detachedcriteria.add(Restrictions.eq("projectId", projectId));
    detachedcriteria.add(Restrictions.eq("classificationId", categoryId));
    detachedcriteria.add(Restrictions.eq("lockedUserId", userName));

    List<VisionBotDetail> result = dataLayerProvider.Select(null, detachedcriteria);

    return result;
  }

  public VisionBot GetVisionBot(String organizationId, String projectId, String id) {
    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBot.class);
    detachedcriteria.add(Restrictions.eq("id", id));

    List<VisionBot> result = dataLayerProvider.Select(VisionBot.class, detachedcriteria);

    if (result != null && result.size() > 0) {
      return result.get(0);
    }

    return null;
  }


  public TestSet GetTestSet(String visionBotID, String layoutID) {
    TestSetInfo testSetInfo = GetTestSetInfo(visionBotID, layoutID);
    if (testSetInfo != null) {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(TestSet.class);
      detachedcriteria.add(Restrictions.eq("testSetInfo", testSetInfo));
      List<TestSet> result2 = dataLayerProvider.Select(TestSet.class, detachedcriteria);
      if (result2 != null) {
        return result2.get(0);
      }
    }
    return null;
  }

  public TestSetInfo GetTestSetInfo(String visionBotID, String layoutID) {
    TestSetInfo testSetInfo = new TestSetInfo();
    testSetInfo.setVisionBotId(visionBotID);
    testSetInfo.setLayoutId(layoutID);

    DetachedCriteria detachedcriteria = DetachedCriteria.forClass(TestSetInfo.class);
    detachedcriteria.add(Restrictions.eq("layoutid", layoutID));
    detachedcriteria.add(Restrictions.eq("visionbotid", visionBotID));

    List<TestSetInfo> result = dataLayerProvider.Select(TestSetInfo.class, detachedcriteria);

    if (result != null && result.size() > 0) {
      return result.get(0);
    }

    return null;
  }

  public VisionBotRunDetails GetLetestVisionBotRunDetails(String visionBotId) {
    DetachedCriteria detachedCriteria = DetachedCriteria.forClass(VisionBotRunDetails.class);
    detachedCriteria.add(Restrictions.eq("visionbotId", visionBotId));
    detachedCriteria.addOrder(Order.desc("createdAt"));
    detachedCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    List<VisionBotRunDetails> result = dataLayerProvider
        .Select(VisionBotRunDetails.class, detachedCriteria);

    if (result != null && result.size() > 0) {
      return result.get(0);
    }
    return null;
  }

  public VisionBotRunDetails GetVisionBotRunDetails(String visionBotRunDetailsId) {
    DetachedCriteria detachedCriteria = DetachedCriteria.forClass(VisionBotRunDetails.class);
    detachedCriteria.add(Restrictions.eq("id", visionBotRunDetailsId));
    //  detachedCriteria.addOrder(Order.desc("createdAt"));
    detachedCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    List<VisionBotRunDetails> result = dataLayerProvider
        .Select(VisionBotRunDetails.class, detachedCriteria);

    if (result != null && result.size() > 0) {
      return result.get(0);
    }
    return null;
  }

  public List<VisionBotRunDetailsDto> getOrganizationWiseRunDetailsDtoForStaging(
      String organizationsId) {
    String query = "select r.id  as runId, r.visionbotId, r.createdAt, " +
        "count(d.docId) as totalFiles, " +
        "sum(case when d.processingStatus = 'Pass' then 1 else 0 end) as passedFiles, " +
        "sum(case when d.processingStatus = 'Fail' then 1 else 0 end) as FailedFiles, " +
        "sum(case when d.processingStatus = 'UnProcessed' then 1 else 0 end) as UnProcessedFiles, "
        +
        "sum(d.TotalFieldCount) as totalFields, " +
        "sum(d.PassedFieldCount)as passedFields, " +
        "sum(d.FailedFieldCount) as failedFields " +
        "from DocumentProccessedDatails d " +
        "inner join VisionBotRunDetails r " +
        "with d.visionBotRunDetails.id = r.id " +
        "group by r.id, r.visionbotId, r.createdAt order by r.createdAt desc";
    List<Object> runDetailList = dataLayerProvider.ExecuteCustomQuery(query, null);
    List<VisionBotRunDetailsDto> rundetails = new ArrayList<VisionBotRunDetailsDto>();
    if (runDetailList != null) {
      for (Object row : runDetailList) {
        VisionBotRunDetailsDto runDetailsDto = convertVisionBotRunDetailsDaoToDto(row);
        rundetails.add(runDetailsDto);
      }
      return rundetails;
    }

    return null;
  }

  public List<VisionBotRunDetailsDto> getOrganizationWiseRunDetailsDtoForStaging(
      String organizationsId, String projectId) {
    String query = "select d.visionBotRunDetails.id  as runId, r.visionbotId, r.createdAt, " +
        "count(d.docId) as totalFiles, " +
        "sum(case when d.processingStatus = 'Pass' then 1 else 0 end) as passedFiles, " +
        "sum(case when d.processingStatus = 'Fail' then 1 else 0 end) as FailedFiles, " +
        "sum(case when d.processingStatus = 'UnProcessed' then 1 else 0 end) as UnProcessedFiles, "
        +
        "sum(d.TotalFieldCount) as totalFields, " +
        "sum(d.PassedFieldCount)as passedFields, " +
        "sum(d.FailedFieldCount) as failedFields " +
        "from VisionBotRunDetails r " +
        "inner join VisionBotDetail v " +
        "with v.id = r.visionbotId " +
        "and v.projectId = :projectId " +
        "inner join DocumentProccessedDatails d " +
        "with d.visionBotRunDetails.id = r.id " +
        "group by d.visionBotRunDetails.id, r.visionbotId, r.createdAt order by r.createdAt desc";

    Map<String, Object> queryMap = new HashMap<>();
    queryMap.put("projectId", projectId);

    List<Object> runDetailList = dataLayerProvider.ExecuteCustomQuery(query, queryMap);
    List<VisionBotRunDetailsDto> rundetails = new ArrayList<VisionBotRunDetailsDto>();
    if (runDetailList != null) {
      for (Object row : runDetailList) {
        VisionBotRunDetailsDto runDetailsDto = convertVisionBotRunDetailsDaoToDto(row);
        rundetails.add(runDetailsDto);
      }
      return rundetails;
    }

    return null;
  }

  private VisionBotRunDetailsDto convertVisionBotRunDetailsDaoToDto(Object databaseRow) {
    Object[] rowCells = (Object[]) databaseRow;
    VisionBotRunDetailsDto dto = new VisionBotRunDetailsDto();

    dto.setId(rowCells[0].toString());
    dto.setVisionbotId(rowCells[1].toString());
    dto.setStartTime((Date) rowCells[2]);

    dto.setTotalFiles(((Long) rowCells[3]).intValue());
    dto.setPassedDocumentCount(((Long) rowCells[4]).intValue());
    dto.setFailedDocumentCount(((Long) rowCells[5]).intValue());

    dto.setProcessedDocumentCount(dto.getPassedDocumentCount() + dto.getFailedDocumentCount());
    if (dto.getPassedDocumentCount() != 0 || dto.getFailedDocumentCount() != 0) {

      double processedDocumentAccuracy =
          (((double) dto.getPassedDocumentCount()) / (dto.getTotalFiles() - ((Long) rowCells[6])
              .intValue())) * 100;
      dto.setDocumentProcessingAccuracy((processedDocumentAccuracy));

      dto.setTotalFieldCount(((Long) rowCells[7]).intValue());
      dto.setPassedFieldCount(((Long) rowCells[8]).intValue());
      dto.setFailedFieldCount(((Long) rowCells[9]).intValue());

      double fieldAccuracyAverage = 0;
      if (dto.getTotalFieldCount() != 0) {
        fieldAccuracyAverage =
            (((double) dto.getPassedFieldCount()) / dto.getTotalFieldCount()) * 100;
      }

      dto.setFieldAccuracy(fieldAccuracyAverage);
    }
    return dto;
  }

  public int GetUnProcessedDocumentDetailsCount(String visionbotRunDetailsId) {
    DetachedCriteria unProcessedFileCountCriteria = DetachedCriteria
        .forClass(DocumentProccessedDatails.class, "processedDocument")
        .createAlias("processedDocument.visionBotRunDetails", "runDetails");
    unProcessedFileCountCriteria.add(Restrictions.eq("runDetails.id", visionbotRunDetailsId));
    unProcessedFileCountCriteria
        .add(Restrictions.eq("processingStatus", DocumentProcessingStatus.UnProcessed));
    unProcessedFileCountCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    unProcessedFileCountCriteria.setProjection(Projections.rowCount());

    int unProcessedDocumentCount = dataLayerProvider.Count(unProcessedFileCountCriteria);

    return unProcessedDocumentCount;

  }

    public int getVisionBotCountStateWise(String organizationsId, String projectId, Environment environment)
    {
        DetachedCriteria numberOfBots = DetachedCriteria.forClass(VisionBotDetail.class);
        numberOfBots.createAlias("visionBot", "visionBot");
        numberOfBots.add(Restrictions.eq("projectId", projectId));
        numberOfBots.add(Restrictions.eq("environment", environment));
        numberOfBots.add(Restrictions.isNotNull("visionBot.datablob"));
        numberOfBots.setProjection(Projections.rowCount());

    int count = dataLayerProvider.Count(numberOfBots);
    return count;
  }

    public int getVisionBotCount(String organizationsId, String projectId)
    {
        DetachedCriteria numberOfBots = DetachedCriteria.forClass(VisionBotDetail.class);
        numberOfBots.createAlias("visionBot", "visionBot");
        numberOfBots.add(Restrictions.eq("projectId", projectId));
        numberOfBots.setProjection(Projections.rowCount());
        numberOfBots.add(Restrictions.isNotNull("visionBot.datablob"));
    int count = dataLayerProvider.Count(numberOfBots);
    return count;


  }

}
