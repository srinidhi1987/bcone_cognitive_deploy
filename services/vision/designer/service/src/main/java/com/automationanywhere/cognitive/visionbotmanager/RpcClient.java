package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.exception.MQConnectionFailureException;
import com.automationanywhere.cognitive.visionbotmanager.exception.MessageQueueConnectionCloseException;
import com.rabbitmq.client.*;
import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Purnil.Soni on 04-01-2017.
 */
public class RpcClient {

  private static AALogger logger = AALogger.create(RpcClient.class);
  private Connection connection;
  private Channel channel;
  private String requestQueueName;
  private String replyQueueName;
  @SuppressWarnings("deprecation")
  private QueueingConsumer consumer;
  private String hostName;

  public RpcClient() {

  }

//    public RpcClient(String queueName) throws Exception {
//        this(queueName, "192.168.2.71");
//    }

//    public RpcClient(String queueName, String hostName) throws Exception {
//        this.requestQueueName = queueName;
//
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost(hostName);
//        factory.setVirtualHost("test");
//        factory.setUsername("messagequeue");
//        factory.setPassword("passmessage");
//        connection = factory.newConnection();
//        channel = connection.createChannel();
//
//        replyQueueName = channel.queueDeclare().getQueue();
//        consumer = new QueueingConsumer(channel);
//        channel.basicConsume(replyQueueName, true, consumer);
//    }
  @SuppressWarnings("deprecation")
  public RpcClient(String queueName, ConnectionFactory factory) throws Exception {
    logger.entry();
    this.requestQueueName = queueName;
    factory.setAutomaticRecoveryEnabled(true);
    boolean connectionSuccess = false;
    while (!connectionSuccess) {
      try {
        connection = factory.newConnection();
        connectionSuccess = true;
      } catch (ConnectException ex) {
        logger.error(ex.getMessage());
        Thread.sleep(1000);
      }
    }
    channel = connection.createChannel();
    replyQueueName = channel.queueDeclare().getQueue();
    consumer = new QueueingConsumer(channel);
    channel.basicConsume(replyQueueName, true, consumer);
    logger.exit();
  }

   /* public RpcClient(String queueName, ConnectionFactory factory, boolean ack) throws Exception {
        this.requestQueueName = queueName;

        connection = factory.newConnection();
        channel = connection.createChannel();

        replyQueueName = channel.queueDeclare().getQueue();
        consumer = new QueueingConsumer(channel);
        channel.basicConsume(replyQueueName, ack, consumer);
    }*/

  @SuppressWarnings("deprecation")
  public String call(String message) throws Exception {
    logger.entry();
    String response = null;
    String corrId = java.util.UUID.randomUUID().toString();

    AMQP.BasicProperties props = new AMQP.BasicProperties
        .Builder()
        .correlationId(corrId)
        .deliveryMode(2)
        .replyTo(replyQueueName)
        .build();

    channel.basicPublish("", requestQueueName, props, message.getBytes());

    while (true) {
      QueueingConsumer.Delivery delivery = consumer.nextDelivery();
      if (delivery.getProperties().getCorrelationId().equals(corrId)) {
        response = new String(delivery.getBody());
        break;
      }
    }
    logger.exit();
    return response;
  }

  public void putMessage(String message) throws Exception {
    logger.entry();
    String response = null;
    String corrId = java.util.UUID.randomUUID().toString();

    AMQP.BasicProperties props = new AMQP.BasicProperties
        .Builder()
        .correlationId(corrId)
        .deliveryMode(2)
        //.replyTo(replyQueueName)
        .build();

    channel.basicPublish("", requestQueueName, props, message.getBytes());
    logger.exit();

       /* while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response = new String(delivery.getBody());
                break;
            }
        }

        return response;*/
  }

  public void close() throws Exception {
    connection.close();
  }

  public void checkConnection(final ConnectionFactory rpcConnectionFactory) {
    Connection conn = null;
    try {
      conn = rpcConnectionFactory.newConnection();
    } catch (final IOException | TimeoutException ex) {
      throw new MQConnectionFailureException("Error while connecting to rabbit MQ", ex);
    } finally {
      closeConnection(conn);
    }
  }

  private void closeConnection(final Connection conn) {
    try {
      if (conn != null && conn.isOpen()) {
        conn.close();
      }
    } catch (final IOException ex) {
      throw new MessageQueueConnectionCloseException(
          "Error while closing mq connection ", ex
      );
    }
  }
}
