package com.automationanywhere.cognitive.visionbotmanager.exception;

public class InputStreamConversionException extends RuntimeException {

  private static final long serialVersionUID = 5707359378937525498L;

  public InputStreamConversionException(String message, Throwable exception) {
    super(message, exception);
  }
}
