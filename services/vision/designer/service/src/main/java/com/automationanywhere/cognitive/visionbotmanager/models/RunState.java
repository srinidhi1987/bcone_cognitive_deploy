package com.automationanywhere.cognitive.visionbotmanager.models;

/**
 * Created by Mukesh.Methaniya on 07-03-2017.
 */
public enum RunState {
  Running(1),
  Completed(2);

  private int value;

  private RunState(int value) {
    this.value = value;
  }
}
