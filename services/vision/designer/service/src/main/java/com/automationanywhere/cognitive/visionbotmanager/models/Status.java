package com.automationanywhere.cognitive.visionbotmanager.models;

/**
 * Created by pooja.wani on 07-03-2017.
 */
public enum Status {
  training,
  ready,
  active
}
