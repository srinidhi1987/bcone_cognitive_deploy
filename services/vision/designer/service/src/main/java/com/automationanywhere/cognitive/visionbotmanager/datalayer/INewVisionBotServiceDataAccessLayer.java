package com.automationanywhere.cognitive.visionbotmanager.datalayer;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import java.util.List;
import java.util.Map;
import org.hibernate.criterion.DetachedCriteria;

/**
 * Created by Mukesh.Methaniya on 30-01-2017.
 */
public interface INewVisionBotServiceDataAccessLayer {

  boolean Insert(EntityModelBase entity);

  boolean Update(EntityModelBase entity);

  boolean Delete(EntityModelBase entity);

  <T> List<T> Select(Class<T> type, DetachedCriteria detachedCriteria);

  List<Object> ExecuteCustomQuery(String plainQuery, Map<String, Object> queryMap);

  int Count(DetachedCriteria detachedCriteria);
}
