package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mukesh.Methaniya on 20-03-2017.
 */
public class ProductionFileCount {

  @JsonProperty("unprocessedCount")
  private long unprocessedCount = 0;

  @JsonProperty("totalCount")
  private long totalCount = 0;

  public long getUnprocessedCount() {
    return unprocessedCount;
  }

  public void setUnprocessedCount(long unprocessedCount) {
    this.unprocessedCount = unprocessedCount;
  }

  public long getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(long totalCount) {
    this.totalCount = totalCount;
  }
}
