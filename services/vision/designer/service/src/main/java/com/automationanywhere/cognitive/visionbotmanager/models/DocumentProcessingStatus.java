package com.automationanywhere.cognitive.visionbotmanager.models;

/**
 * Created by Mukesh.Methaniya on 28-02-2017.
 */
public enum DocumentProcessingStatus {
  UnProcessed,
  Pass,
  Fail

}
