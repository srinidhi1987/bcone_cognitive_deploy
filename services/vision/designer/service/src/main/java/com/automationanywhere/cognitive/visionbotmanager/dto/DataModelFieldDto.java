package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class DataModelFieldDto {

  private final String id;
  private final String name;
  private final int valueType;
  private final String formula;
  private final String validationId;
  private final boolean required;
  private final String defaultValue;
  private final String startsWith;
  private final String endsWith;
  private final String formatExpression;
  private final boolean deleted;

  public DataModelFieldDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("Name") final String name,
      @JsonProperty("ValueType") final int valueType,
      @JsonProperty("Formula") final String formula,
      @JsonProperty("ValidationID") final String validationId,
      @JsonProperty("IsRequired") final boolean required,
      @JsonProperty("DefaultValue") final String defaultValue,
      @JsonProperty("StartsWith") final String startsWith,
      @JsonProperty("EndsWith") final String endsWith,
      @JsonProperty("FormatExpression") final String formatExpression,
      @JsonProperty("IsDeleted") final boolean deleted
  ) {
    this.id = id;
    this.name = name;
    this.valueType = valueType;
    this.formula = formula;
    this.validationId = validationId;
    this.required = required;
    this.defaultValue = defaultValue;
    this.startsWith = startsWith;
    this.endsWith = endsWith;
    this.formatExpression = formatExpression;
    this.deleted = deleted;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  @JsonProperty("Name")
  public String getName() {
    return name;
  }

  /**
   * Returns the valueType.
   *
   * @return the value of valueType
   */
  @JsonProperty("ValueType")
  public int getValueType() {
    return valueType;
  }

  /**
   * Returns the formula.
   *
   * @return the value of formula
   */
  @JsonProperty("Formula")
  public String getFormula() {
    return formula;
  }

  /**
   * Returns the validationId.
   *
   * @return the value of validationId
   */
  @JsonProperty("ValidationID")
  public String getValidationId() {
    return validationId;
  }

  /**
   * Returns the required.
   *
   * @return the value of required
   */
  @JsonProperty("IsRequired")
  public boolean isRequired() {
    return required;
  }

  /**
   * Returns the defaultValue.
   *
   * @return the value of defaultValue
   */
  @JsonProperty("DefaultValue")
  public String getDefaultValue() {
    return defaultValue;
  }

  /**
   * Returns the startsWith.
   *
   * @return the value of startsWith
   */
  @JsonProperty("StartsWith")
  public String getStartsWith() {
    return startsWith;
  }

  /**
   * Returns the endsWith.
   *
   * @return the value of endsWith
   */
  @JsonProperty("EndsWith")
  public String getEndsWith() {
    return endsWith;
  }

  /**
   * Returns the formatExpression.
   *
   * @return the value of formatExpression
   */
  @JsonProperty("FormatExpression")
  public String getFormatExpression() {
    return formatExpression;
  }

  /**
   * Returns the deleted.
   *
   * @return the value of deleted
   */
  @JsonProperty("IsDeleted")
  public boolean isDeleted() {
    return deleted;
  }
}
