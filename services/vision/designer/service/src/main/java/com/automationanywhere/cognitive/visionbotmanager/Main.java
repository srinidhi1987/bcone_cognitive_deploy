package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.common.inetaddress.wrapper.InetAddressWrapper;
import com.automationanywhere.cognitive.common.logger.AALogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spark.Spark;

/**
 * Created by Mukesh.Methaniya on 14-12-2016.
 */
public class Main {

  private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations\\CognitivePlatform.jks";
  private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations\\Cognitive.properties";
  private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
  private static final String PROPERTIES_NOTFOUND = "NotFound";
  private static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
  private static final int DATASOURCE_INITIAL_SIZE = 3;
  private static final String HTTP_SHCEME = "http";
  private static final String HTTPS_SHCEME = "https";
  private static final String fullyQualifiedName = InetAddressWrapper.getFullyQualifiedName();
  private static VisionbotManagerHandlers visionbotManagerHandlers;
  private static String jdbcUrl;
  private static int sqlport;
  private static String databasename;
  private static String username;
  private static String jdbcString = "jdbc:sqlserver://";
  private static boolean windowsAuth;
  private static AALogger logger = AALogger.create(Main.class);
  private static String fileManagerPort;
  private static String projectServicePort;
  private static String validatorServicePort;
  private static String fileManagerUrl;
  private static String projectManagerUrl;
  private static String validatorManagerUrl;
  private static final String FILENAME_COGNITIVE_SETTINGS = "configurations\\Settings.txt";
  private static final String PROPERTIES_SQL_WINAUTH = "SQLWindowsAuthentication";
  private static String settingsFilePath;

  public static void main(String args[]) throws IOException {
    String argsParams = "";
    if (args.length > 7) {

      jdbcUrl = jdbcString + args[0] + ":" + args[1];
      jdbcUrl += ";databaseName=" + args[2];
      username = args[3];
      fileManagerPort = args[5];
      projectServicePort = args[6];
      validatorServicePort = args[7];
      loadSettings();
      if (windowsAuth) {
        jdbcUrl += ";integratedSecurity=true";
      }

      fileManagerUrl = String
          .format("%s://%s:%s", HTTP_SHCEME, fullyQualifiedName, fileManagerPort);
      projectManagerUrl = String
          .format("%s://%s:%s", HTTP_SHCEME, fullyQualifiedName, projectServicePort);
      validatorManagerUrl = String
          .format("%s://%s:%s", HTTP_SHCEME, fullyQualifiedName, validatorServicePort);

      for (int i = 0; i < args.length; i++) {
        argsParams += args[i] + " ";
      }
      logger.info(args.length + " arguments were observed in following order: " + argsParams);

    } else {

      for (int i = 0; i < args.length; i++) {
        argsParams += args[i] + " ";
      }

      logger.error(
          "8 arguments necessary to run jar file <ipaddress> <DatabasePort> <databasename> <username> <ServicePort> "
              + "<fileManagerPort> <projectServicePort> <validatorServicePort> but there were "
              + args.length + " arguments: " + argsParams);
      System.out.println(
          "8 arguments necessary to run jar file <ipaddress> <DatabasePort> <databasename> <username> <ServicePort>"
              + " <fileManagerPort> <projectServicePort> <validatorServicePort>");
      return;
    }
    setServiceSecurity();
    ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
    visionbotManagerHandlers = new VisionbotManagerHandlers();
    visionbotManagerHandlers.init(context, args);
  }

  public static String getUrl() {
    return jdbcUrl;
  }

  public static String getFileManagerAddress() {
    return fileManagerUrl;
  }

  public static String getProjectManagerAddress() {
    return projectManagerUrl;
  }

  public static String getValidatorManagerAddress() {
    return validatorManagerUrl;
  }

  public static String getName() {
    return username;
  }

  private static void setServiceSecurity() {
    if (isSSLCertificateConfigured()) {
      try {
        String certificateKey = loadSSLCertificatePassword();
        if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
          Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
          String cognitiveCertificateFilePath =
              certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
          Spark.secure(cognitiveCertificateFilePath, certificateKey, null, null);

          applyHttpsSchemeToExternalServiceUrl();
        }

      } catch (final IOException e) {
        logger.error("Failed to set service security", e);
      }
    }
  }

  private static void applyHttpsSchemeToExternalServiceUrl() {
    fileManagerUrl = String.format("%s://%s:%s", HTTPS_SHCEME, fullyQualifiedName, fileManagerPort);
    projectManagerUrl = String
        .format("%s://%s:%s", HTTPS_SHCEME, fullyQualifiedName, projectServicePort);
    validatorManagerUrl = String
        .format("%s://%s:%s", HTTPS_SHCEME, fullyQualifiedName, validatorServicePort);
  }

  private static String loadSSLCertificatePassword() throws IOException {
    Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
    String cognitivePropertiesFilePath =
        propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
    if (fileExists(cognitivePropertiesFilePath)) {
      Properties properties = new Properties();
      properties.load(new FileInputStream(cognitivePropertiesFilePath));
      String certificateKey = properties
          .getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
      return certificateKey;
    } else {
      return PROPERTIES_NOTFOUND;
    }
  }

  private static boolean isSSLCertificateConfigured() {
    boolean hasCertificate = false;
    Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
    String cognitiveCertificateFilePath =
        certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
    if (fileExists(cognitiveCertificateFilePath)) {
      hasCertificate = true;
    }
    return hasCertificate;
  }

  private static boolean fileExists(String filePath) {
    File file = new File(filePath);
    if (file.exists()) {
      return true;
    } else {
      return false;
    }
  }

  public static BasicDataSource createDataSource(String password) {
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(SQL_SERVER_DRIVER);
    dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
    dataSource.setUrl(jdbcUrl);
    if (!windowsAuth) {
      dataSource.setUsername(username);
      dataSource.setPassword(password);
    }
    return dataSource;
  }

  private static void loadSettings() throws IOException {
    Path configurationDirectoryPath  = Paths.get(System.getProperty("user.dir")).getParent();
    settingsFilePath = configurationDirectoryPath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
    Properties properties = loadProperties(settingsFilePath);
    if(properties.containsKey(PROPERTIES_SQL_WINAUTH))
    {
      windowsAuth = Boolean.parseBoolean(properties.getProperty(PROPERTIES_SQL_WINAUTH));
    }
  }

  private static Properties loadProperties(String filePath) throws IOException {
    Properties properties = new Properties();
    properties.load(new FileInputStream(filePath));
    return properties;
  }
}
