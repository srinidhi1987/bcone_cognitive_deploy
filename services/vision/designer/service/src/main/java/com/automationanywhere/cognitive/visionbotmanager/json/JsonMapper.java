package com.automationanywhere.cognitive.visionbotmanager.json;

import com.automationanywhere.cognitive.visionbotmanager.exception.InvalidJsonData;
import com.automationanywhere.cognitive.visionbotmanager.exception.InvalidJsonPatch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import org.springframework.stereotype.Component;

@Component
public class JsonMapper {

  private ObjectMapper mapper;

  public JsonMapper() {
    this.mapper = new ObjectMapper();
    this.mapper.disable(SerializationFeature.INDENT_OUTPUT);
  }

  public JsonPatch buildJsonPatch(InputStream inputStream) throws InvalidJsonPatch {
    try {
      if (inputStream == null || inputStream.available() <= 0) {
        throw new InvalidJsonPatch("is not allowed empty or null document");
      }

      JsonNode patchJsonNode = mapper.readTree(inputStream);
      if (patchJsonNode == null) {
        throw new InvalidJsonPatch("is not allowed empty or null document");
      }

      if (!patchJsonNode.isArray()) {
        throw new InvalidJsonPatch("not an array of operations");
      }

      patchJsonNode.forEach(this::validatePatchOperation);
      return JsonPatch.fromJson(patchJsonNode);

    } catch (IOException e) {
      throw new InvalidJsonPatch(e.getMessage(), e);
    }
  }

  public JsonNode buildJsonNode(String data) throws InvalidJsonData {
    try {
      return mapper.readTree(data);

    } catch (IOException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  public String toString(JsonNode json) throws InvalidJsonData {
    try {
      return mapper.writeValueAsString(json);

    } catch (JsonProcessingException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  public String toString(Object data) throws InvalidJsonData {
    try {
      return mapper.writeValueAsString(data);

    } catch (JsonProcessingException e) {
      throw new InvalidJsonData("Invalid data object [" + e.getMessage() + "]", e);
    }
  }

  public <T> T build(String data, Class<T> clazz) {
    try {
      return mapper.readValue(data, clazz);

    } catch (IOException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  public void removeKeyRecursively(JsonNode jsonNode, String key) {

    if (jsonNode.isObject()) {
      ((ObjectNode) jsonNode).remove(key);
    }

    for (JsonNode node : jsonNode) {
      removeKeyRecursively(node, key);
    }
  }

  public ArrayNode createArrayNode() {
    return mapper.createArrayNode();
  }

  public void removeParentFromArrayRecursively(
      final JsonNode jsonNode,
      final String key
  ) {
    removeParentFromArrayRecursively(null, null, jsonNode, key);
  }

  private void removeParentFromArrayRecursively(
      final JsonNode jsonParent,
      final String name,
      final JsonNode jsonNode,
      final String key
  ) {

    JsonNode current = jsonNode;
    if (current.isObject()) {
      JsonNode child = current.get(key);
      if (child != null && child.isArray()) {
        ((ObjectNode) jsonParent).replace(name, child);
        current = child;
      }
    }
    if (current.isArray()) {
      for (JsonNode node : current) {
        removeParentFromArrayRecursively(node, key);
      }
    } else {
      Iterator<String> fieldNames = current.fieldNames();
      while (fieldNames.hasNext()) {
        String childName = fieldNames.next();
        removeParentFromArrayRecursively(current, childName, current.get(childName), key);
      }
    }
  }

  private void validatePatchOperation(JsonNode operation) {
    JsonNode opNode = operation.get("op");
    checkOp(opNode);

    JsonNode pathNode = operation.get("path");
    checkPath(pathNode);

    String op = opNode.textValue();
    if (op.equalsIgnoreCase("move") || op.equalsIgnoreCase("copy")) {
      checkFrom(operation.get("from"));

    } else if (!op.equalsIgnoreCase("remove")) {
      checkValue(operation.get("value"));
    }
  }

  private void checkOp(JsonNode opNode) {
    if (opNode == null) {
      throw new InvalidJsonPatch("'op' attribute not found");
    }
    String op = opNode.textValue();
    if (!op.equalsIgnoreCase("add") &&
        !op.equalsIgnoreCase("remove") &&
        !op.equalsIgnoreCase("replace") &&
        !op.equalsIgnoreCase("test") &&
        !op.equalsIgnoreCase("move") &&
        !op.equalsIgnoreCase("copy")) {
      throw new InvalidJsonPatch("invalid operation '" + op + "'");
    }
  }

  private void checkPath(JsonNode pathNode) {
    if (pathNode == null) {
      throw new InvalidJsonPatch("'path' attribute not found");
    }
    String path = pathNode.textValue();
    if (path == null || path.trim().isEmpty() || !path.startsWith("/")) {
      throw new InvalidJsonPatch("'path' is invalid");
    }
  }

  private void checkFrom(JsonNode fromNode) {
    if (fromNode == null) {
      throw new InvalidJsonPatch("'from' attribute not found");
    }
    String from = fromNode.textValue();
    if (from == null || from.trim().isEmpty() || !from.startsWith("/")) {
      throw new InvalidJsonPatch("'from' is invalid");
    }
  }

  private void checkValue(JsonNode valueNode) {
    if (valueNode == null) {
      throw new InvalidJsonPatch("'value' attribute not found");
    }
  }
}