package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.automationanywhere.cognitive.visionbotmanager.models.DocumentProcessingStatus;
import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by Mukesh.Methaniya on 28-02-2017.
 */
@Entity
@Table(name = "DocumentProccessedDatails")
public class DocumentProccessedDatails extends EntityModelBase {

  @Id
  //  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column(name = "id")
  @JsonProperty("id")
  private String id;

  @Column(name = "docid")
  @JsonProperty("docId")
  private String docId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "visionbotrundetailsid")
  private VisionBotRunDetails visionBotRunDetails;

  @Column(name = "processingstatus")
  @JsonProperty("processingStatus")
  @Enumerated(EnumType.STRING)
  private DocumentProcessingStatus processingStatus;

  @Column(name = "totalfieldcount")
  @JsonProperty("totalFieldCount")
  private int TotalFieldCount;

  @Column(name = "passedfieldcount")
  @JsonProperty("passedFieldCount")
  private int PassedFieldCount;

  @Column(name = "failedfieldcount")
  @JsonProperty("failedFieldCount")
  private int FailedFieldCount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDocId() {
    return docId;
  }

  public void setDocId(String docId) {
    this.docId = docId;
  }

  public DocumentProcessingStatus getProcessingStatus() {
    return processingStatus;
  }

  public void setProcessingStatus(DocumentProcessingStatus processingStatus) {
    this.processingStatus = processingStatus;
  }

  public int getTotalFieldCount() {
    return TotalFieldCount;
  }

  public void setTotalFieldCount(int totalFieldCount) {
    TotalFieldCount = totalFieldCount;
  }

  public int getPassedFieldCount() {
    return PassedFieldCount;
  }

  public void setPassedFieldCount(int passedFieldCount) {
    PassedFieldCount = passedFieldCount;
  }

  public int getFailedFieldCount() {
    return FailedFieldCount;
  }

  public void setFailedFieldCount(int failedFieldCount) {
    FailedFieldCount = failedFieldCount;
  }

  public VisionBotRunDetails getVisionBotRunDetails() {
    return visionBotRunDetails;
  }

  public void setVisionBotRunDetails(VisionBotRunDetails visionBotRunDetails) {
    this.visionBotRunDetails = visionBotRunDetails;
  }
}
