package com.automationanywhere.cognitive.visionbotmanager.impl;

import static com.automationanywhere.cognitive.visionbotmanager.constants.HeaderConstants.CORRELATION_ID;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.visionbotmanager.ProjectManagerAdapter;
import com.automationanywhere.cognitive.visionbotmanager.exception.DataNotFoundException;
import com.automationanywhere.cognitive.visionbotmanager.exception.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.visionbotmanager.models.StandardResponse;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.ProjectDetail;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Created by pooja.wani on 07-03-2017.
 */
public class ProjectManagerAdapterImpl implements ProjectManagerAdapter {

  private final String RelativeUrlFetchAllProjectMetadata = "/organizations/%s/projects/metadata";
  private RestTemplate restTemplate;
  private String rootUrl = "http://localhost:9999";
  private String relativeUrl = "/organizations/%s/projects/%s";
  private ApplicationContext context;
  private AALogger logger = AALogger.create(this.getClass());
  @Autowired
  private RestTemplateWrapper restTemplateWrapper;
  private String HEARTBEAT_URL = "/heartbeat";

  public ProjectManagerAdapterImpl(String rooturl) {
    this.restTemplate = new RestTemplate();
    this.rootUrl = rooturl;
  }

  public ProjectDetail getProjectMetaData(String organizationId, String projectId) {
    logger.entry();
    String url = rootUrl + String.format(relativeUrl + "/metadata", organizationId, projectId);

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    HttpHeaders headers = new HttpHeaders();
    headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    ResponseEntity<StandardResponse> standardResponse = restTemplate
        .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
    //StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class);
    ProjectDetail projectDetails = objectMapper
        .convertValue(standardResponse.getBody().getData(), ProjectDetail.class);

    if (projectDetails == null) {
      throw new DataNotFoundException("Project is not available on server.");
    }

    return projectDetails;

  }

  public List<ProjectDetail> getProjectMetaDataList(String organizationId) {
    logger.entry();
    String url = rootUrl + String.format(RelativeUrlFetchAllProjectMetadata, organizationId);

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    HttpHeaders headers = new HttpHeaders();
    headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    ResponseEntity<StandardResponse> standardResponse = restTemplate
        .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
    //StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class);
    ProjectDetail[] projectDetails = objectMapper
        .convertValue(standardResponse.getBody().getData(), ProjectDetail[].class);

    if (projectDetails == null) {
      logger.error("ProjectDetails not found.");
      throw new DataNotFoundException("Project is not available on server.");
    }
    logger.exit();
    return Arrays.asList(projectDetails);

  }

  @SuppressWarnings("unchecked")
  @Override
  public void testConnection() {
    logger.entry();
    String heartBeatURL = rootUrl + HEARTBEAT_URL;
    ResponseEntity<String> standardResponse = null;
    standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL,
        String.class);
    if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
      throw new DependentServiceConnectionFailureException(
          "Error while connecting to Project service");
    }
    logger.exit();
  }


}
