package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mukesh.Methaniya on 03-01-2017.
 */
@Entity
@Table(name = "TestSetInfo")
public class TestSetInfo extends EntityModelBase {

  @Id
  @Column(name = "id")
  @JsonProperty("id")
  private String id;

  @Column(name = "visionbotid")
  @JsonProperty("visionbotId")
  private String visionbotid;


  @Column(name = "layoutid")
  @JsonProperty("layoutId")
  private String layoutid;

/*    @OneToOne(optional = false, fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "testsetid")
    @JsonProperty("testSet")
    private TestSet testSet;
*/
  //  public String getId()
  //  {
  //     return this.id;
  // }
  //  public void setId(String Id)
  //  {
  //     this.id = Id;
  // }

  public String getVisionBotId() {
    return this.visionbotid;
  }

  public void setVisionBotId(String id) {
    this.visionbotid = id;
  }

  public String getLayoutId() {
    return this.layoutid;
  }

  public void setLayoutId(String id) {
    this.layoutid = id;
  }

/*    public TestSet getTestSet() {
        return testSet;
    }

    public void setTestSet(TestSet testSet) {
        this.testSet = testSet;
    }*/

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }



   /* @Override
    public Map<String, Object> getUpdateQuery(StringBuilder queryString,QueryType queryType) {
        return null;
    }

    @Override
    public Map<String, Object> getSelectQuery(StringBuilder queryString, QueryType queryType) {

        queryString.setLength(0);
        Map<String, Object> inputParam = new HashMap<String, Object>();
        String querys = " FROM TestSetInfo ";
        String whereQuery = "";
        switch (queryType) {
            case ByVisionbotIdLayoutId:
                whereQuery += " where visionbotid= :visionbotid";
                inputParam.put("visionbotid", this.getVisionBotId());
                whereQuery += " and layoutid= :layoutid";
                inputParam.put("layoutid", this.getLayoutId());
                break;
        }
        if (whereQuery != "") {
            querys += whereQuery;
            queryString.append(querys);
        }

        return inputParam;
    }

    @Override
    public Map<String, Object> getDeleteQuery(StringBuilder queryString, QueryType queryType) {
        queryString.setLength(0);
        Map<String, Object> inputParam = new HashMap<String, Object>();
        String querys = "Delete TestSetInfo ";
        String whereQuery = "";
        switch (queryType) {
            case ByVisionbotIdLayoutId:
                whereQuery += " where visionbotid= :visionbotid";
                inputParam.put("visionbotid", this.getVisionBotId());
                whereQuery += " and layoutid= :layoutid";
                inputParam.put("layoutid", this.getLayoutId());
                break;
        }
        if (whereQuery != "") {
            querys += whereQuery;
            queryString.append(querys);
        }

        return  inputParam;
    }*/
}
