package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pooja.wani on 04-03-2017.
 */
public class VisionBotData extends EntityModelBase {

  @JsonProperty("botRunDetails")
  VisionBotRunDetailsDto visionBotRunDetailsDto;
  @JsonProperty("id")
  private String id;
  @JsonProperty("name")
  private String name;
  @JsonProperty("organizationId")
  private String organizationId;
  @JsonProperty("projectId")
  private String projectId;
  @JsonProperty("projectName")
  private String projectName;
  @JsonProperty("categoryId")
  private String classificationId;
  @JsonProperty("categoryName")
  private String categoryName;
  @JsonProperty("environment")
  private Environment environment;
  @JsonProperty("status")
  private Status status;
  @JsonProperty("running")
  private boolean running;
  @JsonProperty("lockedUserId")
  private String lockedUserId;

  @JsonProperty("lastModifiedByUser")
  private String lastModifiedByUser;

  @JsonProperty("lastModifiedTimestamp")
  private String lastModifiedTimestamp;

  @JsonProperty("productionBotRunDetails")
  private
  VisionBotRunDetailsDto productionBotRunDetails;

  @JsonProperty("stagingBotRunDetails")
  private
  VisionBotRunDetailsDto stagingBotRunDetails;

  /* @JsonProperty("validatorReviewDetails")
   ValidatorReviewDetails validatorReviewDetails;
*/
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  public String getLockedUserId() {
    return lockedUserId;
  }

  public void setLockedUserId(String lockedUserId) {
    this.lockedUserId = lockedUserId;
  }

  public VisionBotRunDetailsDto getVisionBotRunDetailsDto() {
    return visionBotRunDetailsDto;
  }

  public void setVisionBotRunDetailsDto(VisionBotRunDetailsDto visionBotRunDetailsDto) {
    this.visionBotRunDetailsDto = visionBotRunDetailsDto;
  }

  public VisionBotRunDetailsDto getProductionBotRunDetails() {
    return productionBotRunDetails;
  }

  public void setProductionBotRunDetails(VisionBotRunDetailsDto productionBotRunDetails) {
    this.productionBotRunDetails = productionBotRunDetails;
  }

  public VisionBotRunDetailsDto getStagingBotRunDetails() {
    return stagingBotRunDetails;
  }

  public void setStagingBotRunDetails(VisionBotRunDetailsDto stagingBotRunDetails) {
    this.stagingBotRunDetails = stagingBotRunDetails;
  }

  public String getLastModifiedByUser() {
    return lastModifiedByUser;
  }

  public String getLastModifiedTimestamp() {
    return lastModifiedTimestamp;
  }

  public void setLastModifiedTimestamp(String lastModifiedTimestamp) {
    this.lastModifiedTimestamp = lastModifiedTimestamp;
  }

  public void setLastModifiedByUser(String lastModifiedByUser) {
    this.lastModifiedByUser = lastModifiedByUser;
  }
    /* public ValidatorReviewDetails getValidatorReviewDetails() {
        return validatorReviewDetails;
    }
   public void setValidatorReviewDetails(ValidatorReviewDetails validatorReviewDetails) {
        this.validatorReviewDetails = validatorReviewDetails;
    }*/


}
