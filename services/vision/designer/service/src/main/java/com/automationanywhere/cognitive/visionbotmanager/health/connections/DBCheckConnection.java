package com.automationanywhere.cognitive.visionbotmanager.health.connections;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.visionbotmanager.datalayer.INewVisionBotServiceDataAccessLayer;
import com.automationanywhere.cognitive.visionbotmanager.exception.DBConnectionFailureException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shweta.thakur
 *
 * This class checks the database connection health
 */
public class DBCheckConnection implements HealthCheckConnection {

  private INewVisionBotServiceDataAccessLayer newVisionBotServiceDataAccessLayer;

  public DBCheckConnection(INewVisionBotServiceDataAccessLayer newVisionBotServiceDataAccessLayer) {
    this.newVisionBotServiceDataAccessLayer = newVisionBotServiceDataAccessLayer;
  }

  @Override
  public void checkConnection() {
    try {
      newVisionBotServiceDataAccessLayer.ExecuteCustomQuery("SELECT 1 FROM VisionBot", null);
    } catch (Exception ex) {
      throw new DBConnectionFailureException("Error connecting database ", ex);
    }
  }

  @Override
  public List<ServiceConnectivity> checkConnectionForService() {
    // return empty arraylist
    return new ArrayList<ServiceConnectivity>();
  }
}
