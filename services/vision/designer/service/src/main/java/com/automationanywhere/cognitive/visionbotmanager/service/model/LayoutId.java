package com.automationanywhere.cognitive.visionbotmanager.service.model;

public class LayoutId {

  public final String organizationId;
  public final String projectId;
  public final String categoryId;
  public final String visionbotId;
  public final String layoutId;

  public LayoutId(
      final String organizationId,
      final String projectId,
      final String categoryId,
      final String visionbotId,
      final String layoutId
  ) {
    this.organizationId = organizationId;
    this.projectId = projectId;
    this.categoryId = categoryId;
    this.visionbotId = visionbotId;
    this.layoutId = layoutId;
  }
}