package com.automationanywhere.cognitive.visionbotmanager.service.model;

public class DocumentId {

  public final String organizationId;
  public final String projectId;
  public final String categoryId;
  public final String visionbotId;
  public final String documentId;

  public DocumentId(
      final String organizationId,
      final String projectId,
      final String categoryId,
      final String visionbotId,
      final String documentId
  ) {
    this.organizationId = organizationId;
    this.projectId = projectId;
    this.categoryId = categoryId;
    this.visionbotId = visionbotId;
    this.documentId = documentId;
  }
}