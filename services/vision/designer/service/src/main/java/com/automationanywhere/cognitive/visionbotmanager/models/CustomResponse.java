package com.automationanywhere.cognitive.visionbotmanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mukesh.Methaniya on 15-12-2016.
 */
public class CustomResponse {

  @JsonProperty("success")
  private boolean success;

  @JsonProperty("data")
  private Object data;

  @JsonProperty("errors")
  private Object errors;

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public Object getErrors() {
    return errors;
  }

  public void setErrors(Object errors) {
    this.errors = errors;
  }
}
