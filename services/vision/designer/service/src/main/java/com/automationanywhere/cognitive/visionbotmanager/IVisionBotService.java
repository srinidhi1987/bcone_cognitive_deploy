package com.automationanywhere.cognitive.visionbotmanager;

import com.automationanywhere.cognitive.visionbotmanager.models.StagingFilesSummary;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotCount;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotData;
import com.automationanywhere.cognitive.visionbotmanager.models.VisionBotMetadata;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.DocumentProccessedDatails;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.TestSet;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBotDetail;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;

/**
 * Created by Mukesh.Methaniya on 13-12-2016.
 */
public interface IVisionBotService {

  void Init(ApplicationContext context);

  VisionBotDetail SaveVisionBot(String organizationId, String projectID, String categoryID,
      String data);

  VisionBot UpdateVisionBot(String visionBotID, String data);

  VisionBot GetValidationData(String visionBotID);

  VisionBot UpdateValidationData(String visionBotID, String data);

  VisionBot GetVisionBot(String VisionBotID);

  // List<VisionBot> GetVisionBotList(String projectID);
  VisionBotDetail GetCategoryVisionBotForEdit(String organizationId, String ProjectId,
      String CategoryId, String user);

  String UnLockVisionBot(String ProjectId, String CategoryId, String visionbotid, String username);

  VisionBotDetail GetCategoryVisionBot(String projectID, String VisionBotID);

  List<VisionBotMetadata> GetOrganizationVisionBotsMetadata(String organizationId);

  List<VisionBotMetadata> GetProjectVisionBotsMetadata(String organizationId, String ProjectId);

  List<VisionBotMetadata> GetCategoryVisionBotsMetadata(String organizationId, String ProjectId,
      String CategoryId);

  TestSet GetTestSet(String visionBotID, String testSetId);

  TestSet SetTestSet(String visionBotID, String layoutID, TestSet testSet);

  void DeleteTestSet(String visionBotID, String layoutID);

  VisionBotDetail GetVisionBotDetails(String visionId);

  VisionBotDetail SetVisionBotState(String visionbotId, String environment);

  void RunAndExtractVBotStaging(String visionbotId) throws Exception;

  //List<VisionBotData> GetOrganizationVisionBotData(String organizationId);
  List<VisionBotData> GetOrganizationVisionBotData(String organizationId, ArrayList<String> ids);

  List<VisionBotData> GetOrganizationVisionBotData(String organizationId, String projectId);

  DocumentProccessedDatails updateVisionBotRunDetailsProcessedDocument(String visionBotRunDetailsId
      , String documentId
      , DocumentProccessedDatails proceessedDocument);

  StagingFilesSummary getSummaryDetails(String organizationId, String projectId);

  VisionBotCount getVisionBotCounts(String organizationId, String projectId);

  public void throwExceptionIfVisionBotDataDoesNotExistForProject(String projectID,
      String categoryId, String orgId);
}
