package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.automationanywhere.cognitive.visionbotmanager.hibernate.attributeconverters.SecureStringAttributeConverter;
import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Nationalized;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by Mukesh.Methaniya on 02-01-2017.
 */
@Entity
@Table(name = "TestSet")
public class TestSet extends EntityModelBase {

  @Id
  @Column(name = "id")
  @JsonProperty("id")
  private String id;

  @Column(name = "name")
  @JsonProperty("name")
  private String name;


    @Nationalized
    @Column(name = "docitems")
    @JsonProperty("docItems")
    private String docItems;

  @OneToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "testsetinfoid")
  @JsonProperty("testSetInfo")
  //@JsonIgnore
  private TestSetInfo testSetInfo;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTesItems() {
    return this.docItems;
  }

  public void setTesItems(String docItems) {
    this.docItems = docItems;
  }

  public TestSetInfo getTestSetInfo() {
    return testSetInfo;
  }

  public void setTestSetInfo(TestSetInfo testSetInfo) {
    this.testSetInfo = testSetInfo;
  }


}
