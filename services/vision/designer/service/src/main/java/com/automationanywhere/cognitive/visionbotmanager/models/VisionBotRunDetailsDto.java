package com.automationanywhere.cognitive.visionbotmanager.models;

import com.automationanywhere.cognitive.visionbotmanager.models.dao.ValidatorReviewDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created by pooja.wani on 04-03-2017.
 */

public class VisionBotRunDetailsDto {


  ValidatorReviewDetails validatorReviewDetails;
  @JsonProperty("id")
  private String id;
  @JsonProperty("startTime")
  private Date startTime;
  @JsonProperty("endTime")
  private Date endTime;
  @JsonProperty("runningMode")
  private String runningMode;
  @JsonProperty("totalFiles")
  private int totalFiles = 0;
  @JsonProperty("processedDocumentCount")
  private int processedDocumentCount = 0;
  @JsonProperty("failedDocumentCount")
  private int failedDocumentCount = 0;
  @JsonProperty("passedDocumentCount")
  private int passedDocumentCount = 0;
  @JsonProperty("documentProcessingAccuracy")
  private double documentProcessingAccuracy = 0;
  @JsonProperty("fieldAccuracy")
  private double fieldAccuracy = 0;
  @JsonProperty("totalFieldCount")
  private double TotalFieldCount = 0.0;
  @JsonProperty("passedFieldCount")
  private double PassedFieldCount = 0.0;
  @JsonProperty("failedFieldCount")
  private double FailedFieldCount = 0.0;
  @JsonIgnore
  private String visionbotId;

  public static double round(double value) {

    try {
      DecimalFormat df = new DecimalFormat("#.##");
      return Double.valueOf(df.format(value));
    } catch (Exception ex) {
      return value;
    }

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getRunningMode() {
    return runningMode;
  }

  public void setRunningMode(String runningMode) {
    this.runningMode = runningMode;
  }

  public int getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(int totalFiles) {
    this.totalFiles = totalFiles;
  }

  public int getProcessedDocumentCount() {
    return processedDocumentCount;
  }

  public void setProcessedDocumentCount(int processedDocumentCount) {
    this.processedDocumentCount = processedDocumentCount;
  }

  public int getFailedDocumentCount() {
    return failedDocumentCount;
  }

  public void setFailedDocumentCount(int failedDocumentCount) {
    this.failedDocumentCount = failedDocumentCount;
  }

  public int getPassedDocumentCount() {
    return passedDocumentCount;
  }

  public void setPassedDocumentCount(int passedDocumentCount) {
    this.passedDocumentCount = passedDocumentCount;
  }

  public double getDocumentProcessingAccuracy() {
    return documentProcessingAccuracy;
  }

  public void setDocumentProcessingAccuracy(double documentProcessingAccuracy) {
    this.documentProcessingAccuracy = round(documentProcessingAccuracy);
  }

  public double getFieldAccuracy() {
    return fieldAccuracy;
  }

  public void setFieldAccuracy(double fieldAccuracy) {
    this.fieldAccuracy = round(fieldAccuracy);
  }

  public ValidatorReviewDetails getValidatorReviewDetails() {
    return validatorReviewDetails;
  }

  public void setValidatorReviewDetails(ValidatorReviewDetails validatorReviewDetails) {
    this.validatorReviewDetails = validatorReviewDetails;
  }

  public double getTotalFieldCount() {
    return TotalFieldCount;
  }

  public void setTotalFieldCount(double totalFieldCount) {
    TotalFieldCount = totalFieldCount;
  }

  public double getPassedFieldCount() {
    return PassedFieldCount;
  }

  public void setPassedFieldCount(double passedFieldCount) {
    PassedFieldCount = passedFieldCount;
  }

  public double getFailedFieldCount() {
    return FailedFieldCount;
  }

  public void setFailedFieldCount(double failedFieldCount) {
    FailedFieldCount = failedFieldCount;
  }

  public String getVisionbotId() {
    return visionbotId;
  }

  public void setVisionbotId(String visionbotId) {
    this.visionbotId = visionbotId;
  }
}
