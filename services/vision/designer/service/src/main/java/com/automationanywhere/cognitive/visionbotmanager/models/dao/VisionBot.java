package com.automationanywhere.cognitive.visionbotmanager.models.dao;


import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Nationalized;

import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mukesh.Methaniya on 30-01-2017.
 */
@Entity
@Table(name = "VisionBot")
public class VisionBot extends EntityModelBase {

  @Id
  @Column(name = "id")
  @JsonProperty("id")
  private String id;

    @Nationalized
    @Column(name = "datablob")
    @JsonProperty("visionBotData")
    private String datablob;

  @Column(name = "validationdatablob")
  @JsonProperty("validationData")
  private String validationDatablob;

  @OneToOne(fetch = FetchType.LAZY)
  @PrimaryKeyJoinColumn
  @JsonIgnore
  private VisionBotDetail visionBotDetail;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDatablob() {
    return datablob;
  }

  public void setDatablob(String datablob) {
    this.datablob = datablob;
  }

  public String getValidationDatablob() {
    return validationDatablob;
  }

  public void setValidationDatablob(String validationDatablob) {
    this.validationDatablob = validationDatablob;
  }

  public VisionBotDetail getVisionBotDetail() {
    return visionBotDetail;
  }

  public void setVisionBotDetail(VisionBotDetail visionBotDetail) {
    this.visionBotDetail = visionBotDetail;
  }
}
