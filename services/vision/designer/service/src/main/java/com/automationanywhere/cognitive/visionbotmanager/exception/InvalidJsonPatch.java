package com.automationanywhere.cognitive.visionbotmanager.exception;

public class InvalidJsonPatch extends RuntimeException {

  private static final long serialVersionUID = -7318886519656900013L;

  public InvalidJsonPatch(String message) {
    super(message);
  }

  public InvalidJsonPatch(String message, Throwable cause) {
    super(message, cause);
  }
}
