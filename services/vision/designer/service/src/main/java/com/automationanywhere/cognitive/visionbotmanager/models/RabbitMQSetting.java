package com.automationanywhere.cognitive.visionbotmanager.models;

/**
 * Created by Mukesh.Methaniya on 04-03-2017.
 */
public class RabbitMQSetting {

  private String VirtualHost;
  private String Uri;
  private String Password;
  private String UserName;
  private int Port;

  public RabbitMQSetting(String virtualHost, String uri, String userName, String password, int port) {
    VirtualHost = virtualHost;
    Uri = uri;
    UserName = userName;
    Password = password;
    Port = port;
  }

  public String getVirtualHost() {
    return this.VirtualHost;
  }

  public String getUri() {
    return this.Uri;
  }

  public String getPassword() {
    return this.Password;
  }

  public String getUserName() {
    return this.UserName;
  }

  public int getPort() {
    return Port;
  }

  public void setPort(int port) {
    Port = port;
  }
}
