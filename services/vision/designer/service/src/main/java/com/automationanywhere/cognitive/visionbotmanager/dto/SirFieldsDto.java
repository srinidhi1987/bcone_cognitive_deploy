package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class SirFieldsDto {

  private final List<LineDto> lines;
  private final List<SirFieldDto> fields;
  private final List<SirFieldDto> regions;

  public SirFieldsDto(
      @JsonProperty("Lines") final List<LineDto> lines,
      @JsonProperty("Fields") final List<SirFieldDto> fields,
      @JsonProperty("Regions") final List<SirFieldDto> regions
  ) {
    this.lines = lines;
    this.fields = fields;
    this.regions = regions;
  }

  /**
   * Returns the lines.
   *
   * @return the value of lines
   */
  @JsonProperty("Lines")
  public List<LineDto> getLines() {
    return lines;
  }

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  @JsonProperty("Fields")
  public List<SirFieldDto> getFields() {
    return fields;
  }

  /**
   * Returns the regions.
   *
   * @return the value of regions
   */
  @JsonProperty("Regions")
  public List<SirFieldDto> getRegions() {
    return regions;
  }
}
