package com.automationanywhere.cognitive.visionbotmanager.exception;

public class InvalidJsonData extends RuntimeException {

  private static final long serialVersionUID = 5144142429084059755L;

  public InvalidJsonData(String message, Throwable cause) {
    super(message, cause);
  }
}
