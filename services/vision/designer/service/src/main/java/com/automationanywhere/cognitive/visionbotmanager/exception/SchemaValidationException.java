package com.automationanywhere.cognitive.visionbotmanager.exception;

public class SchemaValidationException extends RuntimeException {

  private static final long serialVersionUID = -6976291338408059320L;

  public SchemaValidationException(String message) {
    super(message);
  }

  public SchemaValidationException(String message, Throwable cause) {
    super(message, cause);
  }
}
