package com.automationanywhere.cognitive.visionbotmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class DocPropertiesDto {

  private final String id;
  private final String name;
  private final String path;
  private final int type;
  private final int width;
  private final int height;
  private final int renderDpi;
  private final String languageCode;
  private final String borderRect;
  private final List<PagePropertyDto> pageProperties;
  private final double version;

  public DocPropertiesDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("Name") final String name,
      @JsonProperty("Path") final String path,
      @JsonProperty("Type") final int type,
      @JsonProperty("Width") final int width,
      @JsonProperty("Height") final int height,
      @JsonProperty("RenderDPI") final int renderDpi,
      @JsonProperty("LanguageCode") final String languageCode,
      @JsonProperty("BorderRect") final String borderRect,
      @JsonProperty("PageProperties") final List<PagePropertyDto> pageProperties,
      @JsonProperty("Version") final double version
  ) {
    this.id = id;
    this.name = name;
    this.path = path;
    this.type = type;
    this.width = width;
    this.height = height;
    this.renderDpi = renderDpi;
    this.languageCode = languageCode;
    this.borderRect = borderRect;
    this.pageProperties = pageProperties;
    this.version = version;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  @JsonProperty("Name")
  public String getName() {
    return name;
  }

  /**
   * Returns the path.
   *
   * @return the value of path
   */
  @JsonProperty("Path")
  public String getPath() {
    return path;
  }

  /**
   * Returns the type.
   *
   * @return the value of type
   */
  @JsonProperty("Type")
  public int getType() {
    return type;
  }

  /**
   * Returns the width.
   *
   * @return the value of width
   */
  @JsonProperty("Width")
  public int getWidth() {
    return width;
  }

  /**
   * Returns the height.
   *
   * @return the value of height
   */
  @JsonProperty("Height")
  public int getHeight() {
    return height;
  }

  /**
   * Returns the renderDpi.
   *
   * @return the value of renderDpi
   */
  @JsonProperty("RenderDPI")
  public int getRenderDpi() {
    return renderDpi;
  }

  /**
   * Returns the languageCode.
   *
   * @return the value of languageCode
   */
  @JsonProperty("LanguageCode")
  public String getLanguageCode() {
    return languageCode;
  }

  /**
   * Returns the borderRect.
   *
   * @return the value of borderRect
   */
  @JsonProperty("BorderRect")
  public String getBorderRect() {
    return borderRect;
  }

  /**
   * Returns the pageProperties.
   *
   * @return the value of pageProperties
   */
  @JsonProperty("PageProperties")
  public List<PagePropertyDto> getPageProperties() {
    return pageProperties;
  }

  /**
   * Returns the version.
   *
   * @return the value of version
   */
  @JsonProperty("Version")
  public double getVersion() {
    return version;
  }
}
