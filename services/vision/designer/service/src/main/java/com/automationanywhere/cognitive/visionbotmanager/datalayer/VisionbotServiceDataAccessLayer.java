package com.automationanywhere.cognitive.visionbotmanager.datalayer;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import com.automationanywhere.cognitive.visionbotmanager.models.QueryType;
import java.util.List;
import java.util.Map;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Mukesh.Methaniya on 15-12-2016.
 */
public class VisionbotServiceDataAccessLayer implements
    IVisionbotServiceDataAccessLayer<EntityModelBase> {

  private static AALogger logger = AALogger.create(VisionbotServiceDataAccessLayer.class);
  private SessionFactory sessionFactory;

  public VisionbotServiceDataAccessLayer(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Transactional
  @Override
  public boolean InsertRow(EntityModelBase entityModel) {
    try {
      sessionFactory.getCurrentSession().save(entityModel);
      return true;
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      return false;
    }
  }

  @Transactional
  @Override
  public boolean UpdateRow(EntityModelBase entityModel, QueryType queryType) {
      /*  try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam = entityModel.getUpdateQuery(queryString,queryType);
            if(!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                query.executeUpdate();
                return true;
            }
            else
                return false;
        }
        catch(Exception ex) {
            return  false;
        }*/
    return false;
  }

  @Transactional
  @Override
  public List<EntityModelBase> SelectRows(EntityModelBase queryDetail, QueryType queryType) {


      /*  Query query1 = sessionFactory.getCurrentSession().createQuery("FROM VisionBotDetail");
        // fillQueryParameter(inputParam,query);

        Collection<VisionBotDetail> resultset1 = query1.list();
        //  sessionFactory.getCurrentSession().close();
        com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot data= resultset1.iterator().next().getVisionBot();
        System.out.print("Test Started");
        System.out.print(data.getDatablob());
        System.out.print("Test Ended");


        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam  = queryDetail.getSelectQuery(queryString,queryType);
            if (!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                List<EntityModelBase> resultset = query.list();
                return resultset;
            }
            else
                return null;
        }
        catch(Exception ex) {
            return  null;
        }*/
    return null;
  }

  @Transactional
  @Override
  public boolean DeleteRows(EntityModelBase queryDetail, QueryType queryType) {
        /*try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam = queryDetail.getDeleteQuery(queryString,queryType);
            if (!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                query.executeUpdate();
                return true;
            }
            else
                return false;
        }
        catch(Exception ex) {
            return  false;
        }*/

    return false;
  }

  private void fillQueryParameter(Map<String, Object> inputParam, Query query) {
    for (Map.Entry<String, Object> entry : inputParam.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
  }
}
