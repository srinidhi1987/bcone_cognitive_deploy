package com.automationanywhere.cognitive.visionbotmanager.json;

import static java.util.Collections.singletonList;

import com.automationanywhere.cognitive.visionbotmanager.exception.JsonValidatorError;
import com.automationanywhere.cognitive.visionbotmanager.exception.JsonValidatorException;
import com.automationanywhere.cognitive.visionbotmanager.exception.SchemaValidationException;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;

/**
 * Validates JSON objects against loaded schemas. Allows configuring where the schemes will be
 * loaded from. This implementation assumes the input object is a string representation of a JSON
 * before deserialization.
 */
public class ClasspathSchemaValidator {

  private static final String ERROR_PATH_KEY = "instance";
  private static final String ERROR_POINTER_KEY = "pointer";
  private static final String JAR_SCHEMA = "jar";
  private static final String JAR_EXT_PATH = "jar!";
  private static final String EMPTY = "";
  private static final String EXCLAMATION = "!";
  private static final String SLASH = "/";
  private final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

  private final String prefix;
  private final String postfix;
  private final Map<String, JsonNode> keyToSchema = new HashMap<>();

  public ClasspathSchemaValidator(
      final String prefix,
      final String postfix
  ) {
    this.prefix = prefix;
    this.postfix = postfix;
  }

  @PostConstruct
  public void loadJsonSchemas() {
    URL resource = ClasspathSchemaValidator.class.getResource(prefix);
    if (resource == null) {
      throw new SchemaValidationException("Resource not found with prefix: " + prefix);
    }

    try {
      URI uri = resource.toURI();
      if (JAR_SCHEMA.equals(uri.getScheme())) {
        String path = uri.toString();
        int i = path.indexOf(JAR_EXT_PATH);
        path = path.substring(i + JAR_EXT_PATH.length(), path.length()).replace(EXCLAMATION, EMPTY);
        FileSystem fileSystem = null;
        try {
          fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
          Path resourcePath = fileSystem.getPath(path);
          Files.walkFileTree(resourcePath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(
                final Path schemaPath,
                final BasicFileAttributes attrs
            ) {
              processSchemaFiles(schemaPath, SLASH, true);
              return FileVisitResult.CONTINUE;
            }
          });
        } finally {
          if (fileSystem != null) {
            fileSystem.close();
          }
        }
      } else {
        Files.walk(Paths.get(uri))
            .filter(path -> !path.toFile().isDirectory() && path.toString().endsWith(postfix))
            .forEach(schemaPath -> processSchemaFiles(schemaPath, File.separator, false));
      }
    } catch (URISyntaxException | IOException e) {
      throw new SchemaValidationException("Error to initialize Classpath Validation Exception", e);
    }
  }

  private void processSchemaFiles(
      final Path schemaPath,
      final String separator,
      final boolean fromClasspath
  ) {
    String fullName = null;
    try {
      fullName = schemaPath.toString();
      if (fullName.endsWith(postfix)) {
        int slash = fullName.lastIndexOf(separator);
        String fileName = fullName.substring(slash + 1, fullName.length());
        String key = fileName.substring(0, fileName.length() - postfix.length());

        JsonNode schema;
        if (fromClasspath) {
          URL schemaResource = ClasspathSchemaValidator.class.getResource(fullName);
          schema = JsonLoader.fromURL(schemaResource);
        } else {
          schema = JsonLoader.fromFile(schemaPath.toFile());
        }
        keyToSchema.put(key, schema);
      }
    } catch (Exception e) {
      throw new SchemaValidationException("Failed to load schema from " + fullName, e);
    }
  }

  public void checkKey(final String key) {
    if (keyToSchema.get(key) == null) {
      throw new IllegalStateException("Key " + key + " is unknown");
    }
  }

  public void checkJson(final String jsonData, final String key) throws JsonValidatorException {
    ProcessingReport processingReport;
    try {
      JsonNode schema = keyToSchema.get(key);
      JsonSchema jsonSchema = factory.getJsonSchema(schema);

      JsonNode json = JsonLoader.fromString(jsonData);
      processingReport = jsonSchema.validate(json);

    } catch (Exception e) {
      throw new JsonValidatorException(
          singletonList(new JsonValidatorError("/", e.getMessage())), e
      );
    }

    if (processingReport != null && !processingReport.isSuccess()) {
      ArrayList<JsonValidatorError> errors = new ArrayList<>();
      processingReport.forEach(processingMessage -> {
        LogLevel logLevel = processingMessage.getLogLevel();
        if (logLevel == LogLevel.ERROR || logLevel == LogLevel.FATAL) {
          JsonNode jsonNode = processingMessage.asJson();
          String path = "";
          JsonNode pathNode = jsonNode.get(ERROR_PATH_KEY);
          if (pathNode != null) {
            JsonNode pointerNode = pathNode.get(ERROR_POINTER_KEY);
            if (pointerNode != null) {
              path = pointerNode.asText();
            }
          }
          errors.add(new JsonValidatorError(path, processingMessage.getMessage()));
        }
      });
      throw new JsonValidatorException(errors);
    }
  }
}



