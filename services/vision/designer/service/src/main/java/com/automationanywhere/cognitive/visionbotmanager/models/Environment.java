package com.automationanywhere.cognitive.visionbotmanager.models;

/**
 * Created by pooja.wani on 17-01-2017.
 */
public enum Environment {
  staging,
  production
}
