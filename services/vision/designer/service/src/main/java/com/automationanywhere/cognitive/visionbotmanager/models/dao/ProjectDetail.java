package com.automationanywhere.cognitive.visionbotmanager.models.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pooja.wani on 07-03-2017.
 */
public class ProjectDetail {

  @JsonProperty("id")
  private String id;

  @JsonProperty("name")
  private String name;

  @JsonProperty("environment")
  private String environment;

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
