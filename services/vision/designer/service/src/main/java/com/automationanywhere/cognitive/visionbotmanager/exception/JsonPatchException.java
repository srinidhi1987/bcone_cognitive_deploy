package com.automationanywhere.cognitive.visionbotmanager.exception;

public class JsonPatchException extends RuntimeException {

  private static final long serialVersionUID = 3152600598010524774L;

  public JsonPatchException(String message, Throwable cause) {
    super(message, cause);
  }
}
