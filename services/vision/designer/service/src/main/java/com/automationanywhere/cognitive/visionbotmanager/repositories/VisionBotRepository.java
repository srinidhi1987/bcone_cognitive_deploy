package com.automationanywhere.cognitive.visionbotmanager.repositories;

import static com.automationanywhere.cognitive.visionbotmanager.constants.CustomErrorMessageConstant.VISION_BOT_NOT_FOUND;
import static java.lang.String.format;

import com.automationanywhere.cognitive.visionbotmanager.exception.DataNotFoundException;
import com.automationanywhere.cognitive.visionbotmanager.exception.DatabaseException;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.VisionBot;
import com.automationanywhere.cognitive.visionbotmanager.repositories.data.VisionBotDataMapper;
import com.automationanywhere.cognitive.visionbotmanager.service.model.VisionBotEntity;
import java.util.Date;
import java.util.Optional;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class VisionBotRepository {

  private static final Logger LOGGER = LoggerFactory.getLogger(VisionBotRepository.class);
  private static final String UNLOCKED_USER = "";

  private final SessionFactory sessionFactory;
  private final VisionBotDataMapper mapper;

  @Autowired
  public VisionBotRepository(
      final SessionFactory sessionFactory,
      final VisionBotDataMapper mapper
  ) {
    this.sessionFactory = sessionFactory;
    this.mapper = mapper;
  }

  @Transactional
  public VisionBotEntity getVisionBot(String visionBotId) {
    LOGGER.debug("Retrieve VisionBot data from DB with ID: {}", visionBotId);

    Optional<VisionBot> data;
    try {
      Session session = getSession();

      data = Optional.ofNullable((VisionBot) session
          .createNativeQuery("SELECT {vb.*} FROM VisionBot vb WHERE vb.id = ?")
          .setParameter(1, visionBotId)
          .addEntity("vb", VisionBot.class)
          .uniqueResult());

    } catch (Exception e) {
      throw new DatabaseException("Error to retrieve VisionBot with ID " + visionBotId, e);
    }

    return mapper.get(data.orElseThrow(
        () -> new DataNotFoundException("VisionBot not found with ID: " + visionBotId))
    );
  }

  @Transactional
  public void save(VisionBotEntity visionBot) {
    LOGGER.debug("Saving VisionBot data to DB with ID: {}", visionBot.getId());

    try {
      Session session = getSession();

      int result = session.createNativeQuery(
          "UPDATE VisionBot SET datablob = ?, validationdatablob = ? WHERE id = ?")
          .setParameter(1, visionBot.getData())
          .setParameter(2, visionBot.getValidationData())
          .setParameter(3, visionBot.getId())
          .executeUpdate();

      if (result == 0) {
        throw new DataNotFoundException("VisionBot not found with ID: " + visionBot.getId());
      }

    } catch (Exception e) {
      throw new DatabaseException("Error to update VisionBot with ID " + visionBot.getId(), e);
    }
  }

  @Transactional
  public void unlockVisionBots(String username) {
    LOGGER.debug("Unlock all VisionBots of the user: {}", username);

    try {
      Session session = getSession();

      int result = session.createNativeQuery(
          "UPDATE VisionBotDetail SET LockedUserid = :unlockedUser, LockedTimestamp = NULL "
              + " WHERE LockedUserid = :user")
          .setParameter("unlockedUser", UNLOCKED_USER)
          .setParameter("user", username)
          .executeUpdate();

      LOGGER.debug("Unlock {} VisionBots of user {}", result, username);


    } catch (Exception e) {
      throw new DatabaseException("Error to unlock VisionBots with user " + username, e);
    }
  }

  @Transactional
  public void keepAliveLockedVisionBot(String visionBotId) {
    LOGGER.debug("Keep VisionBot {} locked", visionBotId);

    Session session = getSession();
    int result = session.createNativeQuery(
        "UPDATE VisionBotDetail SET LockedTimestamp = ? WHERE id = ?")
        .setParameter(1, new Date(), TemporalType.TIMESTAMP)
        .setParameter(2, visionBotId)
        .executeUpdate();

    if (result == 0) {
      throw new DataNotFoundException("VisionBotDetail not found with ID " + visionBotId);
    }
  }

  @Transactional
  public void unlockedExpiredVisionBots(int timeoutInSec) {

    LOGGER.debug("Unlocked VisionBots with timeout {} sec", timeoutInSec);

    Session session = getSession();

    int result = session.createNativeQuery(
        "UPDATE VisionBotDetail SET LockedUserid = '', LockedTimestamp = NULL"
            + " WHERE DATEDIFF(second, LockedTimestamp, SYSDATETIME()) > ? OR LockedTimestamp IS NULL")
        .setParameter(1, timeoutInSec)
        .executeUpdate();

    LOGGER.debug("Unlocked {} VisionBots", result);
  }

  @Transactional
  public void updateVisionBotDetail(final String visionBotId, final String username) {

    Query query = getSession().createNativeQuery(
        "UPDATE VisionBotDetail SET LastModifiedTimestamp = :lastModifiedTimestamp , "
            + "LastModifiedByUser = :lastModifiedByUser WHERE id = :visionBotId");
    query.setParameter("lastModifiedTimestamp", new Date());
    query.setParameter("lastModifiedByUser", username);
    query.setParameter("visionBotId", visionBotId);
    query.executeUpdate();
    LOGGER.debug("Update the user id & timestamp in visionBotDetail");
  }

  private Session getSession() {
    Session session;
    try {
      session = sessionFactory.getCurrentSession();

    } catch (HibernateException e) {
      session = sessionFactory.openSession();
    }
    return session;
  }

  @Transactional
  public void updateAutoMappingData(String visionBotId, String data) {

    LOGGER.debug("Update auto mapping data of VisionBot {}", visionBotId);

    Session session = getSession();

    int result = session.createNativeQuery(
        "UPDATE VisionBot SET automappeddatablob = :dataBlob WHERE id = :visionBotId ")
        .setParameter("dataBlob", data)
        .setParameter("visionBotId", visionBotId)
        .executeUpdate();

    if (result == 0) {
      throw new DataNotFoundException(format(VISION_BOT_NOT_FOUND, visionBotId));
    }

    LOGGER.debug("Update auto mapping data of VisionBot {} ", result);
  }
}
