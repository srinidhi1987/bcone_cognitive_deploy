package com.automationanywhere.cognitive.visionbotmanager.datalayer;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.visionbotmanager.exception.DatabaseException;
import com.automationanywhere.cognitive.visionbotmanager.models.EntityModelBase;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Mukesh.Methaniya on 31-01-2017.
 */
public class NewVisionBotServiceDataAcessLayer implements INewVisionBotServiceDataAccessLayer {

  private static AALogger logger = AALogger.create(NewVisionBotServiceDataAcessLayer.class);
  private SessionFactory sessionFactory;

  public NewVisionBotServiceDataAcessLayer(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }


  @Transactional
  @Override
  public boolean Insert(EntityModelBase entity) {
    try {
      sessionFactory.getCurrentSession().save(entity);
      return true;
    } catch (HibernateException ex) {
      throw new DatabaseException("Unable to insert data in database.");
    }
  }

  @Transactional
  @Override
  public boolean Update(EntityModelBase entity) {
    try {
      sessionFactory.getCurrentSession().update(entity);
      return true;
    } catch (HibernateException ex) {
      throw new DatabaseException("Unable to update data in database.");
    }
  }

  @Transactional
  @Override
  public boolean Delete(EntityModelBase entity) {
    try {
      sessionFactory.getCurrentSession().delete(entity);
      return true;
    } catch (HibernateException ex) {
      throw new DatabaseException("Unable to delete data in database.");
    }
  }

  @Transactional
  @Override
  public List<Object> ExecuteCustomQuery(String plainQuery, Map<String, Object> queryMap) {
    try {

      Query query = sessionFactory.getCurrentSession().createQuery(plainQuery);

      if (queryMap != null && queryMap.size() > 0) {
        for (Map.Entry<String, Object> parameter : queryMap.entrySet()) {
          query.setParameter(parameter.getKey(), parameter.getValue());
        }
      }

      @SuppressWarnings("unchecked")
      List<Object> resultset = query.list();
      return resultset;

    } catch (Exception ex) {
      logger.error(ex.getMessage());
      return null;
    }
  }

  @Transactional
  @Override
  public <T> List<T> Select(Class<T> type, DetachedCriteria detachedCriteria) {

    try {
    	@SuppressWarnings("unchecked")
    	List<T> result = detachedCriteria.getExecutableCriteria(sessionFactory.getCurrentSession())
          .list();
      return result;
    } catch (Exception ex) {
      throw new DatabaseException("Unable to retrieve data from database.");
    }
  }

  @Transactional
  @Override
  public int Count(DetachedCriteria detachedCriteria) {

    try {
      int result = ((Long) detachedCriteria
          .getExecutableCriteria(sessionFactory.getCurrentSession()).uniqueResult()).intValue();
      return result;
    } catch (Exception ex) {
      throw new DatabaseException("Unable to retrieve data from database.");
    }
  }
}
