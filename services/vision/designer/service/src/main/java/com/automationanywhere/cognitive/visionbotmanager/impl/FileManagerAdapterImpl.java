package com.automationanywhere.cognitive.visionbotmanager.impl;

import static com.automationanywhere.cognitive.visionbotmanager.constants.HeaderConstants.CORRELATION_ID;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.visionbotmanager.FileManagerAdapter;
import com.automationanywhere.cognitive.visionbotmanager.exception.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.visionbotmanager.models.FileCount;
import com.automationanywhere.cognitive.visionbotmanager.models.StandardResponse;
import com.automationanywhere.cognitive.visionbotmanager.models.dao.FileDetail;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


public class FileManagerAdapterImpl implements FileManagerAdapter {

  private RestTemplate restTemplate;
  private String rootUrl = "http://localhost:9996";
  private String relativeUrl = "/organizations/%s/projects/%s/categories/%s/files";
  private ApplicationContext context;
  private AALogger logger = AALogger.create(this.getClass());
  @Autowired
  private RestTemplateWrapper restTemplateWrapper;
  private String HEARTBEAT_URL = "/heartbeat";

  public FileManagerAdapterImpl(String rooturl) {
    logger.entry();
    this.rootUrl = rooturl;
    this.restTemplate = new RestTemplate();
    List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
    MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
    jsonMessageConverter.setObjectMapper(new ObjectMapper());
    StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
    messageConverters.add(stringHttpMessageConerters);
    messageConverters.add(jsonMessageConverter);
    restTemplate.setMessageConverters(messageConverters);
    logger.exit();
  }

  @Override
  public void setContext(ApplicationContext context) {
    this.context = context;
  }

  @Override
  public List<FileDetail> getSpecificFilesFromVisionBotForStaging(String organizationId,
      String classificationId, String projectId) {
    logger.entry();
    String url = rootUrl + String
        .format(relativeUrl + "?environment=staging", organizationId, projectId, classificationId);

    FileDetail[] fileDetails = null;//new ArrayList<>();
    List<FileDetail> fileDetailList = new ArrayList<>();
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    try {
      HttpHeaders headers = new HttpHeaders();
      headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
      HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      fileDetails = objectMapper
          .convertValue(standardResponse.getBody().getData(), FileDetail[].class);
      for (FileDetail file : fileDetails) {
        fileDetailList.add(file);
      }
      logger.exit();
      return fileDetailList;
    } catch (Exception ex) {
      logger.error("Exception occured to get specific files from visionbot " + ex);
      logger.exit();
      return null;
    }
  }

  @Override
  public List<FileDetail> getSpecificFilesFromVisionBot(String organizationId,
      String classificationId, String projectId) {
    logger.entry();
    String url = rootUrl + String.format(relativeUrl, organizationId, projectId, classificationId);

    FileDetail[] fileDetails = null;//new ArrayList<>();
    List<FileDetail> fileDetailList = new ArrayList<>();
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    try {
      HttpHeaders headers = new HttpHeaders();
      headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
      HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      fileDetails = objectMapper
          .convertValue(standardResponse.getBody().getData(), FileDetail[].class);
      for (FileDetail file : fileDetails) {
        fileDetailList.add(file);
      }
      logger.exit();
      return fileDetailList;
    } catch (Exception ex) {
      logger.error("Exception occured to get specific files from visionbot " + ex);
      logger.exit();
      return null;
    }
  }

  public FileCount[] getFileCount(String organizationId) {
    logger.entry();
    String relativeUrl = "/organizations/%s/files/counts";
    String url = rootUrl + String.format(relativeUrl, organizationId);

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    HttpHeaders headers = new HttpHeaders();
    headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    ResponseEntity<StandardResponse> standardResponse = restTemplate
        .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
    //StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class);
    FileCount[] fileCountListCategoryWise = objectMapper
        .convertValue(standardResponse.getBody().getData(), FileCount[].class);
    logger.exit();
    return fileCountListCategoryWise;

  }

  public FileCount[] getFileCount(String organizationId, String projectId) {
    logger.entry();
    String relativeUrl = "/organizations/%s/projects/%s/files/counts";
    String url = rootUrl + String.format(relativeUrl, organizationId, projectId);

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    HttpHeaders headers = new HttpHeaders();
    headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    ResponseEntity<StandardResponse> standardResponse = restTemplate
        .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
    //StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class);
    FileCount[] fileCountListCategoryWise = objectMapper
        .convertValue(standardResponse.getBody().getData(), FileCount[].class);
    logger.exit();
    return fileCountListCategoryWise;

  }

  @SuppressWarnings("unchecked")
  @Override
  public void testConnection() {
    logger.entry();
    String heartBeatURL = rootUrl + HEARTBEAT_URL;
    ResponseEntity<String> standardResponse = null;
    standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL,
        String.class);
    if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
      throw new DependentServiceConnectionFailureException(
          "Error while connecting to File service");
    }
    logger.exit();
  }

}
