package com.automationanywhere.cognitive.visionbotmanager.exception;

public class DBConnectionFailureException extends RuntimeException {
    
  private static final long serialVersionUID = -9123304555898639020L;

  public DBConnectionFailureException(String message, Throwable cause) {
    super(message, cause);
  }

}
