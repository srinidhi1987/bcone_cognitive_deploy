CREATE UNIQUE CLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_PROJECTID] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[ProjectId] ASC,
	[FileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_FIELDLEVELACCURACY_FIELDID_ACCURACYLEVEL_FILEID] ON [FileManager_Demo].[dbo].[FieldLevelAccuracy]
(
	[fieldid] ASC,
	[passaccuracyvalue] ASC
)
INCLUDE ( 	[fileid]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_PROJECTID_STARTTIME_ENDTIME] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[ProjectId] ASC,
	[StartTime] ASC,
	[EndTime] ASC
)
INCLUDE ( 	[AverageTimeInSeconds]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_PROJECTID_VALIDATIONSTATUS] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[ProjectId] ASC,
	[ValidationStatus] ASC
)
INCLUDE ( 	[PassFieldCounts],
	[TotalFieldCounts]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
CREATE NONCLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_VALIDATIONSTATUS] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[ValidationStatus] ASC
)
INCLUDE ( 	[ProjectId],
	[FileId],
	[PassFieldCounts],
	[TotalFieldCounts],
	[VBotDocument]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_VALIDATIONSTATUS_PK] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[IsVBotDocumentColumnEmpty] ASC,
	[ValidationStatus] ASC,
	[ProjectId] ASC
)
INCLUDE ( 	[FileId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_VALIDATIONSTATUS_PROJECTID_ALLFIELDCOUNTS] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[ValidationStatus] ASC
)
INCLUDE ( 	[ProjectId],
	[FailedFieldCounts],
	[PassFieldCounts],
	[TotalFieldCounts]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [IX_VISIONBOTDOCUMENTS_VALIDATIONSTATUS_VBOTDOCUMENT] ON [FileManager_Demo].[dbo].[VisionBotDocuments]
(
	[IsVBotDocumentNotEmptyAndValidationStatusPass] ASC
)
INCLUDE ( 	[ProjectId],
	[FileId],
	[PassFieldCounts],
	[TotalFieldCounts]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments] ADD  CONSTRAINT [DF_VisionBotDocuments_FailedFieldCounts]  DEFAULT ((0)) FOR [FailedFieldCounts]
GO
ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments] ADD  CONSTRAINT [DF_VisionBotDocuments_PassFieldCounts]  DEFAULT ((0)) FOR [PassFieldCounts]
GO
ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments] ADD  CONSTRAINT [DF_VisionBotDocuments_TotalFieldCounts]  DEFAULT ((0)) FOR [TotalFieldCounts]
GO
ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments] ADD  CONSTRAINT [DF_VisionBotDocuments_StartTime]  DEFAULT (((1)/(1))/(1990)) FOR [StartTime]
GO
ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments] ADD  CONSTRAINT [DF_VisionBotDocuments_EndTime]  DEFAULT (((1)/(1))/(1990)) FOR [EndTime]
GO
ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments] ADD  CONSTRAINT [DF_VisionBotDocuments_AverageTimeInSeconds]  DEFAULT ((0)) FOR [AverageTimeInSeconds]
GO
ALTER TABLE [FileManager_Demo].[dbo].[FieldLevelAccuracy]  WITH CHECK ADD  CONSTRAINT [FK_FieldLevelAccuracy_VisionBotDocuments] FOREIGN KEY([fileid])
REFERENCES [FileManager_Demo].[dbo].[VisionBotDocuments] ([FileId])
GO
ALTER TABLE [FileManager_Demo].[dbo].[FieldLevelAccuracy] CHECK CONSTRAINT [FK_FieldLevelAccuracy_VisionBotDocuments]
ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments]
ADD IsVBotDocumentColumnEmpty AS (case when isnull([VBotDocument],'')='' then (0) else (1) end)

ALTER TABLE [FileManager_Demo].[dbo].[VisionBotDocuments]
ADD IsVBotDocumentNotEmptyAndValidationStatusPass AS (case when NOT isnull([VBotDocument],'')='' then case when [validationStatus]='Pass' then (1) else (0) end else (0) end)