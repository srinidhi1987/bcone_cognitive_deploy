use FileManager
ALTER TABLE [VisionBotDocuments]
add [IsVBotDocumentColumnEmpty] 
 AS (case when isnull([VBotDocument],'')='' 
			then (0) 
		else (1) end) PERSISTED NOT NULL,
[IsVBotDocumentNotEmptyAndValidationStatusPass]  AS (case when NOT isnull([VBotDocument],'')='' then case when [validationStatus]='Pass' then (1) else (0) end else (0) end) PERSISTED NOT NULL
