package com.automationanywhere.cognitive.validator.json;

import com.automationanywhere.cognitive.validator.dto.FieldLevelAccuracyDto;
import com.automationanywhere.cognitive.validator.exception.InvalidJsonData;
import com.automationanywhere.cognitive.validator.exception.InvalidJsonPatch;
import com.automationanywhere.cognitive.validator.service.model.FieldLevelAccuracy;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JsonMapper {

  private static final String KEY_OP = "op";
  private static final String KEY_PATH = "path";
  private static final String KEY_FROM = "from";
  private static final String KEY_VALUE = "value";
  private static final String KEY_MOVE = "move";
  private static final String KEY_COPY = "copy";
  private static final String KEY_REMOVE = "remove";
  private static final String OP_ADD = "add";
  private static final String OP_REMOVE = "remove";
  private static final String OP_REPLACE = "replace";
  private static final String OP_TEST = "test";
  private static final String OP_MOVE = "move";
  private static final String OP_COPY = "copy";
  private static final String PATH_FIELD_ACCURACY_LIST = "/fieldAccuracyList";

  private ObjectMapper mapper;

  @Autowired
  public JsonMapper(
      final ObjectMapper mapper
  ) {
    this.mapper = mapper;
  }

  public JsonPatch buildJsonPatch(JsonNode jsonNode) throws InvalidJsonPatch {
    try {
      return JsonPatch.fromJson(jsonNode);

    } catch (IOException e) {
      throw new InvalidJsonPatch(e.getMessage(), e);
    }
  }

  public JsonNode buildJsonPatch(InputStream inputStream) throws InvalidJsonPatch {
    try {
      if (inputStream == null || inputStream.available() <= 0) {
        throw new InvalidJsonPatch("is not allowed empty or null document");
      }

      JsonNode patchJsonNode = mapper.readTree(inputStream);
      if (patchJsonNode == null) {
        throw new InvalidJsonPatch("is not allowed empty or null document");
      }

      if (!patchJsonNode.isArray()) {
        throw new InvalidJsonPatch("not an array of operations");
      }

      patchJsonNode.forEach(this::validatePatchOperation);
      return patchJsonNode;

    } catch (IOException e) {
      throw new InvalidJsonPatch("Failed to parse JSON data [" + e.getMessage() + "]", e);
    }
  }

  public JsonNode buildJsonNode(String data) throws InvalidJsonData {
    try {
      return mapper.readTree(data);

    } catch (IOException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  public String toString(JsonNode json) throws InvalidJsonData {
    try {
      return mapper.writeValueAsString(json);

    } catch (JsonProcessingException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  public <T> T build(String data, Class<T> clazz) {
    try {
      return mapper.readValue(data, clazz);

    } catch (IOException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  public <T> T build(JsonNode data, Class<T> clazz) {
    try {
      return mapper.treeToValue(data, clazz);

    } catch (IOException e) {
      throw new InvalidJsonData("Invalid JSON data [" + e.getMessage() + "]", e);
    }
  }

  private void validatePatchOperation(JsonNode operation) {
    JsonNode opNode = operation.get(KEY_OP);
    checkOp(opNode);

    JsonNode pathNode = operation.get(KEY_PATH);
    checkPath(pathNode);

    String op = opNode.textValue();
    if (op.equalsIgnoreCase(KEY_MOVE) || op.equalsIgnoreCase(KEY_COPY)) {
      checkFrom(operation.get(KEY_FROM));

    } else if (!op.equalsIgnoreCase(KEY_REMOVE)) {
      checkValue(operation.get(KEY_VALUE));
    }
  }

  private void checkOp(JsonNode opNode) {
    if (opNode == null) {
      throw new InvalidJsonPatch("'op' attribute not found");
    }
    String op = opNode.textValue();
    if (!(op.equalsIgnoreCase(OP_ADD) ||
        op.equalsIgnoreCase(OP_REMOVE) ||
        op.equalsIgnoreCase(OP_REPLACE) ||
        op.equalsIgnoreCase(OP_TEST) ||
        op.equalsIgnoreCase(OP_MOVE) ||
        op.equalsIgnoreCase(OP_COPY))) {
      throw new InvalidJsonPatch("invalid operation '" + op + "'");
    }
  }

  private void checkPath(JsonNode pathNode) {
    if (pathNode == null) {
      throw new InvalidJsonPatch("'path' attribute not found");
    }
    String path = pathNode.textValue();
    if (path == null || path.trim().isEmpty() || !path.startsWith("/")) {
      throw new InvalidJsonPatch("'path' is invalid");
    }
  }

  private void checkFrom(JsonNode fromNode) {
    if (fromNode == null) {
      throw new InvalidJsonPatch("'from' attribute not found");
    }
    String from = fromNode.textValue();
    if (from == null || from.trim().isEmpty() || !from.startsWith("/")) {
      throw new InvalidJsonPatch("'from' is invalid");
    }
  }

  private void checkValue(JsonNode valueNode) {
    if (valueNode == null) {
      throw new InvalidJsonPatch("'value' attribute not found");
    }
  }

  public JsonNode filter(JsonNode jsonNode, String prefix) {
    ArrayNode arrayNode = mapper.createArrayNode();
    jsonNode.forEach(node -> {
      String path = node.get(KEY_PATH).textValue();
      if (path.startsWith(prefix)) {
        ObjectNode newNode = node.deepCopy();
        newNode.put(KEY_PATH, path.substring(prefix.length(), path.length()));
        arrayNode.add(newNode);
      }
    });
    return arrayNode;
  }

  public List<FieldLevelAccuracy> buildFieldLevelAccuracy(JsonNode jsonPatch) {
    List<FieldLevelAccuracy> result = new ArrayList<>();
    jsonPatch.forEach(jsonNode -> {
      try {
        String path = jsonNode.get(KEY_PATH).textValue();
        String op = jsonNode.get(KEY_OP).textValue();
        if (path.equalsIgnoreCase(PATH_FIELD_ACCURACY_LIST) && op.equalsIgnoreCase("replace")) {
          FieldLevelAccuracyDto[] array =
              mapper.treeToValue(jsonNode.get(KEY_VALUE), FieldLevelAccuracyDto[].class);

          for (FieldLevelAccuracyDto dto : array) {
            result.add(new FieldLevelAccuracy(
                dto.getFileId(),
                dto.getFieldId(),
                dto.getOldValue(),
                dto.getNewValue(),
                dto.getPassAccuracyValue()
            ));
          }
        }
      } catch (JsonProcessingException e) {
        throw new InvalidJsonPatch("Error to convert fieldAccuracyList data to entity", e);
      }
    });
    return result;
  }
}
