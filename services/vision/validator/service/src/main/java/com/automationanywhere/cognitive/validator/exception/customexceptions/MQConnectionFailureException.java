package com.automationanywhere.cognitive.validator.exception.customexceptions;

public class MQConnectionFailureException extends RuntimeException {

  private static final long serialVersionUID = -3013965294334043470L;

  public MQConnectionFailureException(String message, Throwable cause) {
    super(message, cause);
  }

}