package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class DataModelDto {

  private final List<DataModelFieldDto> fields;
  private final List<DataModelTableDto> tables;

  public DataModelDto(
      @JsonProperty("Fields") final List<DataModelFieldDto> fields,
      @JsonProperty("Tables") final List<DataModelTableDto> tables
  ) {
    this.fields = fields;
    this.tables = tables;
  }

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  @JsonProperty("Fields")
  public List<DataModelFieldDto> getFields() {
    return fields;
  }

  /**
   * Returns the tables.
   *
   * @return the value of tables
   */
  @JsonProperty("Tables")
  public List<DataModelTableDto> getTables() {
    return tables;
  }
}
