package com.automationanywhere.cognitive.validator.consumer.filemanger.contract;

import com.automationanywhere.cognitive.validator.models.ClassificationAnalysisReport;

/**
 * Created by Mayur.Panchal on 21-03-2017.
 */
public interface FileServiceConsumer {

  ClassificationAnalysisReport getAllFileStats(String orgId);
}
