/**
 * Copyright (c) 2016 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.validator.exception.impl;

import static com.automationanywhere.cognitive.validator.exception.impl.HttpStatusCode.INTERNAL_SERVER_ERROR;
import static com.automationanywhere.cognitive.validator.exception.impl.HttpStatusCode.NOT_FOUND;
import static spark.Spark.exception;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.impl.DatabaseConnectionException;
import com.automationanywhere.cognitive.validator.models.CustomResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spark.Response;

/**
 * This filter handles exceptions that has been thrown from resource/service layer.
 * It maps java exceptions to the corresponding HTTP status/response.
 */
@Component
public class ExceptionFilter {

  private static final AALogger logger = AALogger.create(ExceptionFilter.class);

  private final ObjectMapper mapper;

  @Autowired
  public ExceptionFilter(
      final ObjectMapper mapper
  ) {
    this.mapper = mapper;

    exception(DataNotFoundException.class, (e, req, res) -> handleException(e, res, NOT_FOUND));
    exception(Exception.class, (e, req, res) -> handleException(e, res, INTERNAL_SERVER_ERROR));
    exception(DatabaseConnectionException.class,
        (e, req, res) -> handleException(e, res, INTERNAL_SERVER_ERROR));
  }

  private void handleException(Exception e, Response res, HttpStatusCode statusCode) {
    String errorMessage = e.getMessage() == null ? statusCode.getMessage() : e.getMessage();
    res.status(statusCode.getCode());
    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    String customResonseInFormOfJson = "";
    try {
      customResponse.setErrors(errorMessage);
      customResonseInFormOfJson = mapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(customResponse);
      logger.trace("Exception Handler Response :" + customResonseInFormOfJson);

    } catch (Exception ex) {
      logger.error("Exception Occurred :" + errorMessage, ex);
    }
    res.body(customResonseInFormOfJson);
  }
}
