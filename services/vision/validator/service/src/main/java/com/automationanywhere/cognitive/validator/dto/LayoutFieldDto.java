package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class LayoutFieldDto {

  private final String id;
  private final String fieldId;
  private final String label;
  private final String startsWith;
  private final String endsWith;
  private final String formatExpression;
  private final String displayValue;
  private final boolean multiline;
  private final String bounds;
  private final String valueBounds;
  private final int fieldDirection;
  private final double mergeRatio;
  private final double similarityFactor;
  private final int type;
  private final int xDistant;
  private final boolean valueBoundAuto;
  private final int valueType;
  private final String dollarCurrencyUsed;

  public LayoutFieldDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("FieldId") final String fieldId,
      @JsonProperty("Label") final String label,
      @JsonProperty("StartsWith") final String startsWith,
      @JsonProperty("EndsWith") final String endsWith,
      @JsonProperty("FormatExpression") final String formatExpression,
      @JsonProperty("DisplayValue") final String displayValue,
      @JsonProperty("IsMultiline") final boolean multiline,
      @JsonProperty("Bounds") final String bounds,
      @JsonProperty("ValueBounds") final String valueBounds,
      @JsonProperty("FieldDirection") final int fieldDirection,
      @JsonProperty("MergeRatio") final double mergeRatio,
      @JsonProperty("SimilarityFactor") final double similarityFactor,
      @JsonProperty("Type") final int type,
      @JsonProperty("XDistant") final int xDistant,
      @JsonProperty("IsValueBoundAuto") final boolean valueBoundAuto,
      @JsonProperty("ValueType") final int valueType,
      @JsonProperty("IsDollarCurrency") final String dollarCurrencyUsed
  ) {
    this.id = id;
    this.fieldId = fieldId;
    this.label = label;
    this.startsWith = startsWith;
    this.endsWith = endsWith;
    this.formatExpression = formatExpression;
    this.displayValue = displayValue;
    this.multiline = multiline;
    this.bounds = bounds;
    this.valueBounds = valueBounds;
    this.fieldDirection = fieldDirection;
    this.mergeRatio = mergeRatio;
    this.similarityFactor = similarityFactor;
    this.type = type;
    this.xDistant = xDistant;
    this.valueBoundAuto = valueBoundAuto;
    this.valueType = valueType;
    this.dollarCurrencyUsed = dollarCurrencyUsed;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  @JsonProperty("FieldId")
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the label.
   *
   * @return the value of label
   */
  @JsonProperty("Label")
  public String getLabel() {
    return label;
  }

  /**
   * Returns the startsWith.
   *
   * @return the value of startsWith
   */
  @JsonProperty("StartsWith")
  public String getStartsWith() {
    return startsWith;
  }

  /**
   * Returns the endsWith.
   *
   * @return the value of endsWith
   */
  @JsonProperty("EndsWith")
  public String getEndsWith() {
    return endsWith;
  }

  /**
   * Returns the formatExpression.
   *
   * @return the value of formatExpression
   */
  @JsonProperty("FormatExpression")
  public String getFormatExpression() {
    return formatExpression;
  }

  /**
   * Returns the displayValue.
   *
   * @return the value of displayValue
   */
  @JsonProperty("DisplayValue")
  public String getDisplayValue() {
    return displayValue;
  }

  /**
   * Returns the multiline.
   *
   * @return the value of multiline
   */
  @JsonProperty("IsMultiline")
  public boolean isMultiline() {
    return multiline;
  }

  /**
   * Returns the bounds.
   *
   * @return the value of bounds
   */
  @JsonProperty("Bounds")
  public String getBounds() {
    return bounds;
  }

  /**
   * Returns the valueBounds.
   *
   * @return the value of valueBounds
   */
  @JsonProperty("ValueBounds")
  public String getValueBounds() {
    return valueBounds;
  }

  /**
   * Returns the fieldDirection.
   *
   * @return the value of fieldDirection
   */
  @JsonProperty("FieldDirection")
  public int getFieldDirection() {
    return fieldDirection;
  }

  /**
   * Returns the mergeRatio.
   *
   * @return the value of mergeRatio
   */
  @JsonProperty("MergeRatio")
  public double getMergeRatio() {
    return mergeRatio;
  }

  /**
   * Returns the similarityFactor.
   *
   * @return the value of similarityFactor
   */
  @JsonProperty("SimilarityFactor")
  public double getSimilarityFactor() {
    return similarityFactor;
  }

  /**
   * Returns the type.
   *
   * @return the value of type
   */
  @JsonProperty("Type")
  public int getType() {
    return type;
  }

  /**
   * Returns the xDistant.
   *
   * @return the value of xDistant
   */
  @JsonProperty("XDistant")
  public int getXDistant() {
    return xDistant;
  }

  /**
   * Returns the valueBoundAuto.
   *
   * @return the value of valueBoundAuto
   */
  @JsonProperty("IsValueBoundAuto")
  public boolean isValueBoundAuto() {
    return valueBoundAuto;
  }

  /**
   * Returns the valueType.
   *
   * @return the value of valueType
   */
  @JsonProperty("ValueType")
  public int getValueType() {
    return valueType;
  }

  /**
   * Returns the dollarCurrencyUsed.
   *
   * @return the value of dollarCurrencyUsed
   */
  @JsonProperty("IsDollarCurrency")
  public String getDollarCurrencyUsed() {
    return dollarCurrencyUsed;
  }
}
