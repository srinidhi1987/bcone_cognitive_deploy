package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh.Savalia on 27-01-2017.
 */
public class FieldDetail {

  @JsonProperty("fieldId")
  public String fieldId;
  @JsonIgnore
  public String fieldName;
  @JsonIgnore
  public String aliasName;
  @JsonProperty("foundInTotalDocs")
  public long foundInTotalDocs;
  @JsonIgnore
  public float foundinPercentageOfDocs;
}
