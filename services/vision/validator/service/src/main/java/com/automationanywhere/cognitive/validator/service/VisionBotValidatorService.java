package com.automationanywhere.cognitive.validator.service;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static com.automationanywhere.cognitive.constants.Constants.PURGE_DATA;
import static com.automationanywhere.cognitive.logging.Constants.DOCUMENTS_LOCKED_BY_ANOTHER_USER;
import static com.automationanywhere.cognitive.logging.Constants.PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE;
import static com.automationanywhere.cognitive.logging.Constants.VALIDATOR_LOG_DETAIL;
import static java.util.Optional.ofNullable;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.exporter.impl.FileSystemBasedInvalidExporterImpl;
import com.automationanywhere.cognitive.exporter.impl.FileSystemBasedSuccessExporterImpl;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.ValidationType;
import com.automationanywhere.cognitive.validator.exception.DatabaseException;
import com.automationanywhere.cognitive.validator.exception.InvalidJsonData;
import com.automationanywhere.cognitive.validator.exception.JsonPatchException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DocumentLockedException;
import com.automationanywhere.cognitive.validator.json.JsonMapper;
import com.automationanywhere.cognitive.validator.json.JsonValidator;
import com.automationanywhere.cognitive.validator.models.ValidatorOperationTypeEnum;
import com.automationanywhere.cognitive.validator.repositories.MessageQueueRepository;
import com.automationanywhere.cognitive.validator.repositories.ValidatorRepository;
import com.automationanywhere.cognitive.validator.service.model.FieldLevelAccuracy;
import com.automationanywhere.cognitive.validator.service.model.InvalidFile;
import com.automationanywhere.cognitive.validator.service.model.VisionBotDocument;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonpatch.JsonPatch;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VisionBotValidatorService {

  private static final AALogger LOGGER = AALogger.create(VisionBotValidatorService.class);
  private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
  private static final String PATH_VB_DOCUMENT = "/vBotDocument";

  private final ValidatorRepository validatorRepository;
  private final MessageQueueRepository queueRepository;
  private final FileSystemBasedSuccessExporterImpl successExporter;
  private final FileSystemBasedInvalidExporterImpl invalidExporter;
  private final MessageSource msgSource;
  private final JsonMapper jsonMapper;
  private final JsonValidator jsonValidator;
  private final int documentLockedTimeout;

  @Autowired
  public VisionBotValidatorService(
      final ValidatorRepository validatorRepository,
      final MessageQueueRepository queueRepository,
      final FileSystemBasedSuccessExporterImpl successExporter,
      final FileSystemBasedInvalidExporterImpl invalidExporter,
      @Qualifier(MESSAGE_SOURCE) final MessageSource msgSource,
      final JsonMapper jsonMapper,
      final JsonValidator jsonValidator,
      @Value("${DocumentLockedTimeout:900}") int documentLockedTimeout
  ) {
    this.validatorRepository = validatorRepository;
    this.queueRepository = queueRepository;
    this.successExporter = successExporter;
    this.invalidExporter = invalidExporter;
    this.msgSource = msgSource;
    this.jsonMapper = jsonMapper;
    this.jsonValidator = jsonValidator;
    this.documentLockedTimeout = documentLockedTimeout;
  }

  /**
   * Patch
   *
   * @param fileId the VisionBot ID
   * @param jsonPatchInputStream the input stream with JSON Patch Document
   */
  @Transactional
  public void patchVisionBot(
      final String fileId,
      final String username,
      final InputStream jsonPatchInputStream
  ) {

    LOGGER.debug("Patch VisionBot data with file ID {}", fileId);
    JsonNode jsonPatch = jsonMapper.buildJsonPatch(jsonPatchInputStream);
    JsonPatch vBotPatch = jsonMapper.buildJsonPatch(jsonMapper.filter(jsonPatch, PATH_VB_DOCUMENT));

    VisionBotDocument vBotDocument = validatorRepository.getVisionBotDocument(fileId);

    if (vBotDocument == null) {
      throw new DataNotFoundException(
          msgSource.getMessage(PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE, null, Locale.getDefault())
      );
    }

    String lockedUser = ofNullable(vBotDocument.getLockedUserId()).orElse("");
    String currentUser = ofNullable(username).orElse("");
    if (!lockedUser.equalsIgnoreCase(currentUser)) {
      throw new DocumentLockedException(
          msgSource.getMessage(DOCUMENTS_LOCKED_BY_ANOTHER_USER, null, Locale.getDefault())
      );
    }

    Date endTime = new Date();
    try {
      // Updates all fields
      List<FieldLevelAccuracy> fields = jsonMapper.buildFieldLevelAccuracy(jsonPatch);
      LOGGER.debug("Retrieved {} FieldLevelAccuracy entities from patch", fields.size());

      float accuracyValueTotal = (float) fields.stream()
          .mapToDouble(FieldLevelAccuracy::getPassAccuracyValue)
          .sum();

      validatorRepository.save(fileId, fields);

      // Updates VisionBotDocument
      String data = vBotDocument.getVBotDocument();
      LOGGER.debug("VisionBot data after apply patch: {}", data);

      JsonNode jsonNode = jsonMapper.buildJsonNode(data);
      JsonNode patched = vBotPatch.apply(jsonNode);

      // Writes the JsonNode as string
      data = jsonMapper.toString(patched);
      LOGGER.debug("VisionBot data after patch applied: {}", data);

      jsonValidator.validateVisionBot(data);

      // Saves the data to repository
      long avgTimeInSeconds = 0L;
      if (vBotDocument.getStartTime() != null) {
        avgTimeInSeconds = (endTime.getTime() - vBotDocument.getStartTime().getTime()) / 1000;
      }
      validatorRepository.updateVBotDocument(fileId, new VisionBotDocument(
          vBotDocument.getFileId(),
          vBotDocument.getOrgId(),
          vBotDocument.getProjectId(),
          PURGE_DATA,
          ValidationType.Pass.toString(),
          data,
          vBotDocument.getCreatedAt(),
          username,
          endTime,
          vBotDocument.getVisionBotId(),
          "",
          fields.size() - accuracyValueTotal,
          accuracyValueTotal,
          fields.size(),
          vBotDocument.getStartTime(),
          endTime,
          avgTimeInSeconds,
          vBotDocument.getLastExportTime(),
          vBotDocument.getLockedTimestamp()
      ));

      LOGGER.info(() -> String.format("%s\t %s\t %s\t %s Success: Document validated successfully.",
          VALIDATOR_LOG_DETAIL, username, getHostName(), DATE_FORMAT.format(endTime)));

    } catch (DataNotFoundException | DatabaseException | InvalidJsonData e) {
      throw new JsonPatchException(e);

    } catch (JsonPatchException e) {
      throw e;

    } catch (Exception e) {
      throw new JsonPatchException(e);
    }
  }

  /**
   * Unlocks all documents locked by this user, than gets and locks the next unlocked document
   * ordered by the creation date, if there is no locked document or the locked document by this
   * user was the last one, the first unlocked document will be returned.
   *
   * @param orgId the organization ID.
   * @param projectId the project ID.
   * @param username the user requesting the document.
   * @return The next document or null if there is no available documents.
   */
  @Transactional
  public VisionBotDocument getAndLockFailedVBot(
      final String orgId,
      final String projectId,
      final String username
  ) {
    List<VisionBotDocument> docs = validatorRepository.searchAllFailedLockedVBotDocuments(
        orgId, projectId, username
    );

    validatorRepository.unlock(docs);

    VisionBotDocument nextDocLocked = null;
    VisionBotDocument currentDocLocked = docs.isEmpty() ? null : docs.get(docs.size() - 1);
    if (currentDocLocked != null) {
      nextDocLocked = validatorRepository.getNextFailedVBotDocument(
          orgId, projectId, currentDocLocked.getFileId(), currentDocLocked.getCreatedAt()
      );
    }

    if (currentDocLocked == null || nextDocLocked == null) {
      nextDocLocked = validatorRepository.getFirstFailedVBotDocument(orgId, projectId);
    }

    if (nextDocLocked != null) {
      validatorRepository.lock(nextDocLocked, username);
    }

    return nextDocLocked;
  }

  public void exportSuccessFile(final String fileId) {
    successExporter.exportSuccessFile(fileId);
    queueRepository.broadcastMessageToPurgeData(fileId);
  }

  public void unlockDocuments(String username) {
    validatorRepository.unlockDocuments(username);
  }

  public void keepValidatorDocumentAlive(String fileId) {
    validatorRepository.keepValidatorDocumentAlive(fileId);
  }

  @Transactional
  public void markInvalidFiles(
      final String username,
      final String fileId,
      final List<InvalidFile> invalidFiles
  ) {

    VisionBotDocument oldDoc = ofNullable(validatorRepository.getVisionBotDocument(fileId))
        .orElseThrow(() -> new DataNotFoundException(msgSource.getMessage(
            Constants.PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE,
            null,
            Locale.getDefault()
        )));

    List<InvalidFile> files;
    if (invalidFiles == null || invalidFiles.isEmpty()) {
      files = Collections.singletonList(new InvalidFile(fileId, "", ""));

    } else {
      files = invalidFiles.stream()
          .map(invalidFile -> new InvalidFile(
              fileId,
              ofNullable(invalidFile.reasonId).orElse(""),
              ofNullable(invalidFile.reasonText).orElse("")
          ))
          .distinct()
          .collect(Collectors.toList());
    }
    validatorRepository.insertInvalidFiles(files);

    Date currentDate = new Date();
    Date startTime = oldDoc.getStartTime();
    long diffDateInSeconds = 0;
    if (startTime != null) {
      diffDateInSeconds = (currentDate.getTime() - startTime.getTime()) / 1000;
    }

    validatorRepository.updateVBotDocument(fileId, new VisionBotDocument(
        fileId,
        oldDoc.getOrgId(),
        oldDoc.getProjectId(),
        oldDoc.getVBotDocument(),
        ValidatorOperationTypeEnum.Invalid.toString(),
        oldDoc.getCorrectedData(),
        oldDoc.getCreatedAt(),
        username,
        currentDate,
        oldDoc.getVisionBotId(),
        "",
        oldDoc.getFailedFieldCounts(),
        oldDoc.getPassFieldCounts(),
        oldDoc.getTotalFieldCounts(),
        oldDoc.getStartTime(),
        currentDate,
        diffDateInSeconds,
        oldDoc.getLastExportTime(),
        oldDoc.getLockedTimestamp()
    ));

    LOGGER.info(() -> String.format("%s\t %s\t %s\t %s Success: Document marked as invalid.",
        VALIDATOR_LOG_DETAIL, username, getHostName(), DATE_FORMAT.format(currentDate)));
  }

  public void exportInvalidFile(final String fileId) {
    invalidExporter.exportInvalidFile(fileId);
  }

  @Transactional
  public boolean unlockedExpiredDocuments() {
    boolean result = false;
    try {
      validatorRepository.unlockedExpiredDocuments(documentLockedTimeout);
      result = true;

    } catch (Exception e) {
      LOGGER.error("Error to unlocked expired documents", e);
    }
    return result;
  }

  private String getHostName() {
    String hostName = null;
    try {
      InetAddress address = InetAddress.getLocalHost();
      hostName = address.getHostName();

    } catch (UnknownHostException e) {
      LOGGER.error("Error to retrieve host name", e);
    }
    return hostName;
  }
}