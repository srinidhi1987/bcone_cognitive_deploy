package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class ClassificationDetails {

  @JsonProperty(value = "classificationId")
  private String classificationid;

  @JsonProperty(value = "classificationFieldDetails")
  private List<ClassificationFieldDetails> classificationDetails;

  public String getClassificationid() {
    return classificationid;
  }

  public void setFieldid(String fieldid) {
    this.classificationid = fieldid;
  }

  public List<ClassificationFieldDetails> getClassificationDetails() {
    return classificationDetails;
  }

  public void setClassificationDetails(List<ClassificationFieldDetails> classificationDetails) {
    this.classificationDetails = classificationDetails;
  }
}