package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class SegmentedDocDto {

  private final int processingMode;
  private final List<SegmentedDocRegionDto> regions;

  public SegmentedDocDto(
      @JsonProperty("processingMode") final int processingMode,
      @JsonProperty("Regions") final List<SegmentedDocRegionDto> regions
  ) {
    this.processingMode = processingMode;
    this.regions = regions;
  }

  /**
   * Returns the processingMode.
   *
   * @return the value of processingMode
   */
  @JsonProperty("processingMode")
  public int getProcessingMode() {
    return processingMode;
  }

  /**
   * Returns the regions.
   *
   * @return the value of regions
   */
  @JsonProperty("Regions")
  public List<SegmentedDocRegionDto> getRegions() {
    return regions;
  }
}
