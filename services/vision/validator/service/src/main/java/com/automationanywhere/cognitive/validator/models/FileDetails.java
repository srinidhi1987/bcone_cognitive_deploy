package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mayur.Panchal on 07-12-2016.
 */

@Entity
@Table(name = "FileDetails")
public class FileDetails {

  @Id
  @Column(name = "fileid")
  @JsonProperty("fileId")
  private String fileId;
  @JsonProperty("projectId")
  @Column(name = "projectid")
  private String projectId;
  @JsonProperty("fileName")
  @Column(name = "filename")
  private String fileName;
  @JsonProperty("fileLocation")
  @Column(name = "filelocation")
  private String fileLocation;
  @JsonProperty("fileSize")
  @Column(name = "filesize")
  private Long fileSize;
  @JsonProperty("fileHeight")
  @Column(name = "fileheight")
  private int fileHeight;
  @JsonProperty("fileWidth")
  @Column(name = "filewidth")
  private int fileWidth;
  @JsonProperty
  @Column(name = "format")
  private String format;
  @JsonProperty
  @Column(name = "processed")
  private boolean processed;
  @JsonProperty("classificationId")
  @Column(name = "classificationid")
  private String classificationId;
  @JsonProperty("uploadrequestId")
  @Column(name = "uploadrequestid")
  private String uploadrequestId;
  @JsonProperty("layoutId")
  @Column(name = "layoutid")
  private String layoutId;
  @JsonProperty("isProduction")
  @Column(name = "isproduction")
  private boolean isProduction;

  public FileDetails() {
    super();
  }

  ;

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String name) {
    this.fileName = name;
  }

  public String getFileId() {
    return this.fileId;
  }

  public void setFileId(String id) {
    this.fileId = id;
  }

  public String getProjectId() {
    return this.projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getClassificationId() {
    return this.classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public String getuploadrequestid() {
    return this.uploadrequestId;
  }

  public void setuploadrequestid(String uploadRequestid) {
    this.uploadrequestId = uploadRequestid;
  }

  public String getLocation() {
    return this.fileLocation;
  }

  public void setLocation(String location) {
    this.fileLocation = location;
  }

  public Long getSize() {
    return this.fileSize;
  }

  public void setSize(Long size) {
    this.fileSize = size;
  }

  public String getFormat() {
    return this.format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public int getHeight() {
    return this.fileHeight;
  }

  public void setHeight(int height) {
    this.fileHeight = height;
  }

  public int getWidth() {
    return this.fileWidth;
  }

  public void setWidth(int width) {
    this.fileWidth = width;
  }

  public int getIsProcessed() {
    return this.processed == false ? 0 : 1;
  }

  public void setIsProcessed(boolean isprocessed) {
    this.processed = isprocessed;
  }

  public String getLayoutId() {
    return this.layoutId;
  }

  public void setLayoutId(String layoutid) {
    this.layoutId = layoutid;
  }

  public boolean getIsProduction() {
    return this.isProduction;// == false ? 0 :1;
  }

  public void setIsProduction(boolean isProduction) {
    this.isProduction = isProduction;// == 0 ? false:true;
  }

  public static enum Property {
    fileId, projectId, fileName, fileLocation, fileSize, fileHeight, fileWidth, format,
    processed, classificationId, uploadrequestId, layoutId, isProduction
  }

}