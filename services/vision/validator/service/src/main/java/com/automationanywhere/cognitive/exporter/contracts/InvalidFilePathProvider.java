package com.automationanywhere.cognitive.exporter.contracts;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
public interface InvalidFilePathProvider {

  String getInvalidFilePath(String fileId);
}
