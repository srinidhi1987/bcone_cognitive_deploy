package com.automationanywhere.cognitive.validator.exception;

public class InvalidParameterException extends RuntimeException {

  private static final long serialVersionUID = -7192429850205673786L;

  public InvalidParameterException(String message) {
    super(message);
  }

  public InvalidParameterException(String message, Throwable cause) {
    super(message, cause);
  }
}
