package com.automationanywhere.cognitive.validator;

/**
 * Created by keval.sheth on 29-11-2016.
 */
public enum ValidationType {
  Pass,
  Fail,
  Invalid
}
