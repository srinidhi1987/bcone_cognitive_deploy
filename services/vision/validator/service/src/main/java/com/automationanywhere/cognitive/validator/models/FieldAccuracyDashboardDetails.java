package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class FieldAccuracyDashboardDetails {

  @JsonProperty(value = "fieldId")
  private String fieldId;

  @JsonProperty(value = "accuracyPercent")
  private double percentFieldAccuracy;

  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  public double getPercentFieldAccuracy() {
    return percentFieldAccuracy;
  }

  public void setPercentFieldAccuracy(double percentFieldAccuracy) {
    this.percentFieldAccuracy = percentFieldAccuracy;
  }
}