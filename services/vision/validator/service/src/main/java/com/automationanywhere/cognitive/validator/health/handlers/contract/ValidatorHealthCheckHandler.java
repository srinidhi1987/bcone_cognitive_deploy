package com.automationanywhere.cognitive.validator.health.handlers.contract;

public interface ValidatorHealthCheckHandler {

  String checkHealth();
}
