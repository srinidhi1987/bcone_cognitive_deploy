package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class FieldDataRecordDto {

  private final List<FieldDataRecordFieldDto> fields;

  public FieldDataRecordDto(
      @JsonProperty("Fields") final List<FieldDataRecordFieldDto> fields
  ) {
    this.fields = fields;
  }

  /**
   * Returns the fields.
   *
   * @return the value of fields
   */
  @JsonProperty("Fields")
  public List<FieldDataRecordFieldDto> getFields() {
    return fields;
  }
}

