package com.automationanywhere.cognitive.exporter.impl;

import static com.automationanywhere.cognitive.generic.contracts.ProjectManagerAdapter.PROJECT_MANAGER_ADAPTER_ID;

import com.automationanywhere.cognitive.exporter.contracts.InvalidFilePathProvider;
import com.automationanywhere.cognitive.exporter.contracts.SuccessFilePathProvider;
import com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter;
import com.automationanywhere.cognitive.generic.contracts.ProjectManagerAdapter;
import com.automationanywhere.cognitive.generic.models.FileDetail;
import com.automationanywhere.cognitive.generic.models.ProjectDetail;
import com.automationanywhere.cognitive.validator.AppConfig;
import com.automationanywhere.cognitive.validator.datalayer.ValidatorDetailProvider;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
@Component
public class FileSystemExportPathProviderImpl implements SuccessFilePathProvider,
    InvalidFilePathProvider {

  private final ValidatorDetailProvider validatorDetailProvider;
  private final String outputRootDirectory;
  private final FileManagerAdapter fileManagerAdapter;
  private final ProjectManagerAdapter projectManagerAdapter;

  @Autowired
  public FileSystemExportPathProviderImpl(
      final ValidatorDetailProvider validatorDetailProvider,
      final FileManagerAdapter fileManagerAdapter,
      @Qualifier(PROJECT_MANAGER_ADAPTER_ID) final ProjectManagerAdapter projectManagerAdapter,
      final AppConfig appConfig
  ) {
    this.validatorDetailProvider = validatorDetailProvider;
    this.fileManagerAdapter = fileManagerAdapter;
    this.projectManagerAdapter = projectManagerAdapter;
    this.outputRootDirectory = appConfig.getOutputDirectory();
  }

  public String getSuccessFilePath(String fileId) {
    VisionBotDataDetail validatorData = validatorDetailProvider.getDocumentByFileId(fileId);
    String projectId = validatorData.getProjectId();
    String organizationsId = validatorData.getOrgId();

    ProjectDetail projectDetails = projectManagerAdapter
        .getProjectMetaData(organizationsId, projectId);

    //ToDo : No need to check null here. Above method already handling it.
    if (projectDetails == null) {
      throw new RuntimeException("Project is not found.");
    }

    // get file stream data
    FileDetail fileDetail = fileManagerAdapter
        .getSpecificFileFromVisionBot(organizationsId, projectId, fileId);

    String parentDirectory = outputRootDirectory + "\\" + projectDetails.getName() + "\\Success";

    File file = new File(parentDirectory);

    if (!file.exists()) {
      file.mkdirs();
    }

    return parentDirectory + "\\[" + validatorData.getFileId() + "]_" + fileDetail.getFilename()
        + ".csv";
  }

  public String getInvalidFilePath(String fileId) {
    VisionBotDataDetail validatorData = validatorDetailProvider.getDocumentByFileId(fileId);
    String projectId = validatorData.getProjectId();
    String organizationsId = validatorData.getOrgId();

    ProjectDetail projectDetails = projectManagerAdapter
        .getProjectMetaData(organizationsId, projectId);

    //ToDo : No need to check null here. Above method already handling it.
    if (projectDetails == null) {
      throw new RuntimeException("Project is not found.");
    }

    // get file stream data
    FileDetail fileDetail = fileManagerAdapter
        .getSpecificFileFromVisionBot(organizationsId, projectId, fileId);

    String parentDirectory = outputRootDirectory + "\\" + projectDetails.getName() + "\\Invalid";

    File file = new File(parentDirectory);

    if (!file.exists()) {
      file.mkdirs();
    }

    return parentDirectory + "\\[" + validatorData.getFileId() + "]_" + fileDetail.getFilename();
  }
}
