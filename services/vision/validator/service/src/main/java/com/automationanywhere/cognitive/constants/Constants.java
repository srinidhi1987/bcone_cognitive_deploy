package com.automationanywhere.cognitive.constants;

public class Constants {

  public static final String MESSAGE_SOURCE = "validatorServiceMessageSource";
  public static final String PURGE_DATA = "PURGE";
}
