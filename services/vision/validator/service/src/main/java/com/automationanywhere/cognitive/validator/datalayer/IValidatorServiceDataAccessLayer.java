package com.automationanywhere.cognitive.validator.datalayer;

import com.automationanywhere.cognitive.validator.models.EntityModelBase;
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;

/**
 * Created by Mukesh.Methaniya on 30-01-2017.
 */
public interface IValidatorServiceDataAccessLayer {

  String VALIDATOR_SERVICE_DATA_ACCESS_LAYER = "validatorServiceDataAccessLayer";
  String VALIDATOR_SERVICE_DATA_ACCESS_LAYER_PROXY = "validatorServiceDataAccessLayerProxy";

  boolean Insert(EntityModelBase entity);

  boolean update(EntityModelBase entity);

  boolean Delete(EntityModelBase entity);

  <T> List<T> Select(Class<T> type, DetachedCriteria detachedCriteria);

  <T> List<T> Select(Class<T> type, DetachedCriteria detachedCriteria, int maxResult);

  List<Object> ExecuteCustomQuery(String queryId, String idtype, String id, EntityModelBase entity);

  List<Object> selectDistinctParameter(String parameters, EntityModelBase entity);

  int Count(DetachedCriteria detachedCriteria);
}
