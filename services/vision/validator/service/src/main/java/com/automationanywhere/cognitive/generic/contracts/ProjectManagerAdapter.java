package com.automationanywhere.cognitive.generic.contracts;

import com.automationanywhere.cognitive.generic.models.ProjectDetail;
import java.util.List;

/**
 * Created by pooja.wani on 07-03-2017.
 */
public interface ProjectManagerAdapter {

  String PROJECT_MANAGER_ADAPTER_ID = "projectManagerAdapter";
  String PROJECT_MANAGER_ADAPTER_PROXY = "projectManagerAdapterProxy";
  String PROJECT_SERVICE_ROOT_URL = "projectServiceRootUrl";

  ProjectDetail getProjectMetaData(String organizationId, String projectId);

  List<ProjectDetail> getProjectMetaDataList(String organizationId);

  void testConnection();
}
