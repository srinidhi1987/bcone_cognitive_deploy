package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class LayoutTableDto {

  private final LayoutFieldDto primaryColumn;
  private final String id;
  private final String tableId;
  private final List<LayoutFieldDto> columns;
  private final String bounds;
  private final LayoutFieldDto footer;
  private final boolean isValueMappingEnabled;

  public LayoutTableDto(
      @JsonProperty("PrimaryColumn") final LayoutFieldDto primaryColumn,
      @JsonProperty("Id") final String id,
      @JsonProperty("TableId") final String tableId,
      @JsonProperty("Columns") final List<LayoutFieldDto> columns,
      @JsonProperty("Bounds") final String bounds,
      @JsonProperty("Footer") final LayoutFieldDto footer,
      @JsonProperty("IsValueMappingEnabled") final boolean isValueMappingEnabled
  ) {
    this.primaryColumn = primaryColumn;
    this.id = id;
    this.tableId = tableId;
    this.columns = columns;
    this.bounds = bounds;
    this.footer = footer;
    this.isValueMappingEnabled = isValueMappingEnabled;
  }

  /**
   * Returns the primaryColumn.
   *
   * @return the value of primaryColumn
   */
  @JsonProperty("PrimaryColumn")
  public LayoutFieldDto getPrimaryColumn() {
    return primaryColumn;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the tableId.
   *
   * @return the value of tableId
   */
  @JsonProperty("TableId")
  public String getTableId() {
    return tableId;
  }

  /**
   * Returns the columns.
   *
   * @return the value of columns
   */
  @JsonProperty("Columns")
  public List<LayoutFieldDto> getColumns() {
    return columns;
  }

  /**
   * Returns the bounds.
   *
   * @return the value of bounds
   */
  @JsonProperty("Bounds")
  public String getBounds() {
    return bounds;
  }

  /**
   * Returns the footer.
   *
   * @return the value of footer
   */
  @JsonProperty("Footer")
  public LayoutFieldDto getFooter() {
    return footer;
  }

  /**
   * Returns the isValueMappingEnabled.
   *
   * @return the value of isValueMappingEnabled
   */
  @JsonProperty("IsValueMappingEnabled")
  public boolean isValueMappingEnabled() {
    return isValueMappingEnabled;
  }
}
