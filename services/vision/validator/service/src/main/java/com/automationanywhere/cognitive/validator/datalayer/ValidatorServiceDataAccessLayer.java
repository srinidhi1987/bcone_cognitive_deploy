package com.automationanywhere.cognitive.validator.datalayer;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer.VALIDATOR_SERVICE_DATA_ACCESS_LAYER_PROXY;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.impl.DatabaseConnectionException;
import com.automationanywhere.cognitive.validator.models.EntityModelBase;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Keval.Sheth on 07-02-2017.
 */
@Component(VALIDATOR_SERVICE_DATA_ACCESS_LAYER_PROXY)
public class ValidatorServiceDataAccessLayer implements IValidatorServiceDataAccessLayer {

  private static final AALogger LOGGER = AALogger.create(ValidatorServiceDataAccessLayer.class);

  private final MessageSource messageSource;
  private final SessionFactory sessionFactory;

  @Autowired
  public ValidatorServiceDataAccessLayer(
      final SessionFactory sessionFactory,
      @Qualifier(MESSAGE_SOURCE) final MessageSource messageSource
  ) {
    this.sessionFactory = sessionFactory;
    this.messageSource = messageSource;
  }


  @Transactional
  @Override
  public boolean Insert(EntityModelBase entity) {
    try {
      sessionFactory.getCurrentSession().save(entity);
      return true;
    } catch (Exception ex) {
      LOGGER.error(ex.getMessage());
      return false;
    }
  }

  @Transactional
  @Override
  public boolean update(EntityModelBase entity) {
    try {
      sessionFactory.getCurrentSession().update(entity);
      return true;
    } catch (Exception ex) {
      LOGGER.error(ex.getMessage());
      return false;
    }
  }

  @Transactional
  @Override
  public boolean Delete(EntityModelBase entity) {
    try {
      sessionFactory.getCurrentSession().delete(entity);
      return true;
    } catch (Exception ex) {
      LOGGER.error(ex.getMessage());
      return false;
    }
  }

  @Transactional
  @Override
  public <T> List<T> Select(Class<T> type, DetachedCriteria detachedCriteria) {
    @SuppressWarnings("unchecked")
    List<T> result = detachedCriteria.getExecutableCriteria(sessionFactory.getCurrentSession())
        .list();
    return result;
  }


  @Transactional
  @Override
  public List<Object> ExecuteCustomQuery(String queryId, String idtype, String id,
      EntityModelBase entity) {
    try {
      StringBuilder queryString = new StringBuilder();
      Map<String, Object> inputParam = entity
          .getCustomQuery(queryString, queryId, idtype, id);//, dt, queryType);
      // if (!inputParam.isEmpty())
      {
        Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
        fillQueryParameter(inputParam, query);
        @SuppressWarnings("unchecked")
        List<Object> resultset = query.list();
        return resultset;
      }
           /* else
                return null;*/
    } catch (HibernateException ex) {
      throw new DatabaseConnectionException(
          messageSource.getMessage(Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()),
          ex);
    } catch (Exception ex) {
      LOGGER.error(ex.getMessage());
      return null;
    }
  }

  @Transactional
  @Override
  public List<Object> selectDistinctParameter(String parameters, EntityModelBase entity) {
    try {
      StringBuilder queryString = new StringBuilder();
      Map<String, Object> inputParam = entity
          .getCustomQueryForDistinctElements(queryString, parameters);//, dt, queryType);
      if (!inputParam.isEmpty()) {
        Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
        fillQueryParameter(inputParam, query);
        @SuppressWarnings("unchecked")
        List<Object> resultset = query.list();
        return resultset;
      } else {
        return null;
      }
    } catch (Exception ex) {
      LOGGER.error(ex.getMessage());
      return null;
    }
  }

  private void fillQueryParameter(Map<String, Object> inputParam, Query query) {
    for (Map.Entry<String, Object> entry : inputParam.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
  }

  @Transactional
  @Override
  public int Count(DetachedCriteria detachedCriteria) {
    try {
      int result = ((Long) detachedCriteria
          .getExecutableCriteria(sessionFactory.getCurrentSession()).uniqueResult()).intValue();
      return result;
    } catch (Exception ex) {
      throw new DatabaseConnectionException(
          messageSource.getMessage(Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()),
          ex);
    }
  }


  @Transactional
  @Override
  public <T> List<T> Select(Class<T> type, DetachedCriteria detachedCriteria, int maxResult) {

    try {
      @SuppressWarnings("unchecked")
      List<T> list = detachedCriteria.getExecutableCriteria(sessionFactory.getCurrentSession())
          .setMaxResults(maxResult)
          .list();

      if (list != null && list.size() > 0) {
        return list;
      }

      return new ArrayList<T>();

    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }
}
