package com.automationanywhere.cognitive.exporter.contracts;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
public interface FileWriter {

  void write(Path path, byte[] data, Boolean doOverwrite) throws IOException;
  void WriteCsv(Path path, byte[] data, Boolean doOverwrite) throws IOException;
}
