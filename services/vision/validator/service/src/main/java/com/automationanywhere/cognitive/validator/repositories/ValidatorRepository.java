package com.automationanywhere.cognitive.validator.repositories;

import static java.lang.String.join;
import static java.util.stream.Collectors.toList;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.validator.exception.DatabaseException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.models.FieldAccuracyDetails;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import com.automationanywhere.cognitive.validator.repositories.data.ValidatorDataMapper;
import com.automationanywhere.cognitive.validator.service.model.FieldLevelAccuracy;
import com.automationanywhere.cognitive.validator.service.model.InvalidFile;
import com.automationanywhere.cognitive.validator.service.model.VisionBotDocument;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.TemporalType;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ValidatorRepository {

  private static final AALogger LOGGER = AALogger.create(ValidatorRepository.class);
  private static final String UNLOCKED_USER = "";

  private final SessionFactory sessionFactory;
  private final ValidatorDataMapper mapper;

  @Autowired
  public ValidatorRepository(
      final SessionFactory sessionFactory,
      final ValidatorDataMapper mapper
  ) {
    this.sessionFactory = sessionFactory;
    this.mapper = mapper;
  }

  @Transactional
  @SuppressWarnings("unchecked")
  public VisionBotDocument getVisionBotDocument(String fileId) {
    LOGGER.debug("Retrieve VisionBot document from DB with file ID: {}", fileId);

    VisionBotDataDetail data;
    try {
      Session session = getSession();

      data = (VisionBotDataDetail) session
          .createNativeQuery("SELECT {d.*} FROM VisionBotDocuments d WHERE d.fileid = ?")
          .setParameter(1, fileId)
          .addEntity("d", VisionBotDataDetail.class)
          .uniqueResult();

    } catch (Exception e) {
      throw new DatabaseException("Error to retrieve VisionBot document with file ID " + fileId, e);
    }

    return mapper.getVBotDoc(data);
  }

  @Transactional
  public List<VisionBotDocument> searchAllFailedLockedVBotDocuments(
      final String orgId,
      final String projectId,
      final String user
  ) {
    LOGGER.debug("Retrieves all documents [OrgId:{} - PrjId:{} - User:{}]", orgId, projectId, user);

    try {
      Session session = getSession();

      @SuppressWarnings("unchecked")
      List<VisionBotDataDetail> data = (List<VisionBotDataDetail>) session.createNativeQuery(
          "SELECT {d.*} FROM VisionBotDocuments d "
              + "WHERE d.OrgId = ? AND d.ProjectId = ? AND d.ValidationStatus = 'Fail' AND d.LockedUserid = ? "
              + "ORDER BY d.CreatedAt ASC")
          .setParameter(1, orgId)
          .setParameter(2, projectId)
          .setParameter(3, user)
          .addEntity("d", VisionBotDataDetail.class)
          .list();

      return mapper.getVBotDoc(data);

    } catch (Exception e) {
      throw new DatabaseException("Error retrieving failed locked documents to user " + user, e);
    }
  }

  @Transactional
  public void unlock(final List<VisionBotDocument> documents) {
    List<String> fileIds = documents.stream()
        .map(VisionBotDocument::getFileId)
        .collect(toList());

    try {
      if (!documents.isEmpty()) {
        Session session = getSession();
        session.createNativeQuery(
            "UPDATE VisionBotDocuments SET LockedUserid = :unlockedUser, LockedTimestamp = NULL WHERE FileId IN (:fileIds)")
            .setParameter("unlockedUser", UNLOCKED_USER)
            .setParameterList("fileIds", fileIds)
            .executeUpdate();
      }
    } catch (Exception e) {
      throw new DatabaseException("Error to unlock docs with file IDs " + join(", ", fileIds));
    }
  }

  @Transactional
  public void lock(final VisionBotDocument vBotDocument, final String user) {
    try {
      Session session = getSession();
      session.createNativeQuery(
          "UPDATE VisionBotDocuments SET LockedUserid = ?, LockedTimestamp = SYSDATETIME() WHERE FileId = ?")
          .setParameter(1, user)
          .setParameter(2, vBotDocument.getFileId())
          .executeUpdate();

    } catch (Exception e) {
      throw new DatabaseException("Error to unlock doc with file ID " + vBotDocument.getFileId());
    }
  }

  @Transactional
  public VisionBotDocument getNextFailedVBotDocument(
      final String orgId,
      final String projectId,
      final String fileId,
      final Date startTime
  ) {
    LOGGER.debug("Get next failed document [OrgId:{} - PrjId:{} - File Id:{} - StartTime:{}]",
        orgId, projectId, fileId, startTime);

    VisionBotDataDetail data;
    try {
      Session session = getSession();

      data = (VisionBotDataDetail) session.createNativeQuery(
          "SELECT TOP(1) {d.*} FROM VisionBotDocuments d "
              + "WHERE d.OrgId = ? "
              + "    AND d.ProjectId = ? "
              + "    AND d.FileId <> ? "
              + "    AND d.CreatedAt > ? "
              + "    AND d.ValidationStatus = 'Fail' "
              + "    AND (d.LockedUserid IS NULL OR d.LockedUserid = '') "
              + "ORDER BY d.CreatedAt ASC")
          .setParameter(1, orgId)
          .setParameter(2, projectId)
          .setParameter(3, fileId)
          .setParameter(4, startTime)
          .addEntity("d", VisionBotDataDetail.class)
          .uniqueResult();

    } catch (Exception e) {
      throw new DatabaseException("Error to get next failed document", e);
    }

    return mapper.getVBotDoc(data);
  }

  @Transactional
  public VisionBotDocument getFirstFailedVBotDocument(
      final String orgId,
      final String projectId
  ) {
    LOGGER.debug("Get first failed document [OrgId:{} - PrjId:{}]", orgId, projectId);

    VisionBotDataDetail data;
    try {
      Session session = getSession();

      data = (VisionBotDataDetail) session.createNativeQuery(
          "SELECT TOP(1) {d.*} FROM VisionBotDocuments d "
              + "WHERE d.OrgId = ? "
              + "    AND d.ProjectId = ? "
              + "    AND d.ValidationStatus = 'Fail' "
              + "    AND (d.LockedUserid IS NULL OR d.LockedUserid = '') "
              + "ORDER BY d.CreatedAt ASC")
          .setParameter(1, orgId)
          .setParameter(2, projectId)
          .addEntity("d", VisionBotDataDetail.class)
          .uniqueResult();

    } catch (Exception e) {
      throw new DatabaseException("Error to get next failed document", e);
    }

    return mapper.getVBotDoc(data);
  }

  @Transactional
  public void updateVBotDocument(String fileId, VisionBotDocument vBotDocument) {
    LOGGER.debug("Retrieve VisionBot document from DB with file ID: {}", fileId);

    try {
      Session session = getSession();

      int idx = 1;
      NativeQuery query = session.createNativeQuery(
          "UPDATE VisionBotDocuments SET "
              + "OrgId = ?, "
              + "ProjectId = ?, "
              + "VBotDocument = ?, "
              + "ValidationStatus = ?, "
              + "CorrectedData = ?, "
              + "CreatedAt = ?, "
              + "UpdatedBy = ?, "
              + "UpdatedAt = ?, "
              + "VisionBotId = ?, "
              + "LockedUserid = ?, "
              + "FailedFieldCounts = ?, "
              + "PassFieldCounts = ?, "
              + "TotalFieldCounts = ?, "
              + "StartTime = ?, "
              + "EndTime = ?, "
              + "AverageTimeInSeconds = ?, "
              + "lastExportTime = ?, "
              + "LockedTimestamp = ? "
              + "WHERE FileId = ?")
          .setParameter(idx++, vBotDocument.getOrgId())
          .setParameter(idx++, vBotDocument.getProjectId())
          .setParameter(idx++, vBotDocument.getVBotDocument())
          .setParameter(idx++, vBotDocument.getValidationStatus())
          .setParameter(idx++, vBotDocument.getCorrectedData())
          .setParameter(idx++, vBotDocument.getCreatedAt(), TemporalType.TIMESTAMP)
          .setParameter(idx++, vBotDocument.getUpdatedBy())
          .setParameter(idx++, vBotDocument.getUpdatedAt(), TemporalType.DATE)
          .setParameter(idx++, vBotDocument.getVisionBotId())
          .setParameter(idx++, vBotDocument.getLockedUserId())
          .setParameter(idx++, vBotDocument.getFailedFieldCounts())
          .setParameter(idx++, vBotDocument.getPassFieldCounts())
          .setParameter(idx++, vBotDocument.getTotalFieldCounts())
          .setParameter(idx++, vBotDocument.getStartTime(), TemporalType.TIMESTAMP)
          .setParameter(idx++, vBotDocument.getEndTime(), TemporalType.TIMESTAMP)
          .setParameter(idx++, vBotDocument.getAverageTimeInSeconds())
          .setParameter(idx++, vBotDocument.getLastExportTime(), TemporalType.DATE)
          .setParameter(idx++, vBotDocument.getLockedTimestamp(), TemporalType.TIMESTAMP)
          .setParameter(idx, vBotDocument.getFileId());
      int result = query.executeUpdate();

      if (result == 0) {
        throw new DataNotFoundException("VisionBot document not found with file ID: " + fileId);
      }

    } catch (Exception e) {
      throw new DatabaseException("Error to update vBotData with file ID: " + fileId, e);
    }
  }

  @Transactional
  @SuppressWarnings("unchecked")
  public List<FieldLevelAccuracy> getFieldLevelAccuracies(String fileId) {
    LOGGER.debug("Retrieve VisionBot data from DB with file ID: {}", fileId);

    List<FieldAccuracyDetails> fields;
    try {
      Session session = getSession();

      fields = session.createNativeQuery(
          "SELECT {f.*} FROM FieldLevelAccuracy f WHERE f.fileid = ?")
          .setParameter(1, fileId)
          .addEntity("f", FieldAccuracyDetails.class)
          .list();

    } catch (Exception e) {
      throw new DatabaseException("Error to retrieve FieldLevelAccuracy with file ID " + fileId, e);
    }

    return mapper.get(fields);
  }

  @Transactional
  public void save(
      final String fileId,
      final List<FieldLevelAccuracy> fields
  ) {
    LOGGER.debug("Saving FieldLevelAccuracy list to DB");

    try {
      Session session = getSession();

      int[] result = session.doReturningWork((conn) -> {
        int[] execReturn;
        try (PreparedStatement ps = conn.prepareStatement(
            "DELETE FROM FieldLevelAccuracy WHERE fileid = ?")
        ) {
          ps.setString(1, fileId);
          int deleted = ps.executeUpdate();
          LOGGER.debug("{} field level accuracies was deleted", deleted);
        }

        try (PreparedStatement ps = conn.prepareStatement(
            "INSERT INTO FieldLevelAccuracy (fileid, fieldid, oldvalue, newvalue, passaccuracyvalue) VALUES (?, ?, ?, ?, ?)")
        ) {
          for (FieldLevelAccuracy field : fields) {
            int index = 1;
            ps.setString(index++, fileId);
            ps.setString(index++, field.getFieldId());
            ps.setString(index++, field.getOldValue());
            ps.setString(index++, field.getNewValue());
            ps.setFloat(index, field.getPassAccuracyValue());
            ps.addBatch();
          }

          execReturn = ps.executeBatch();
        }
        return execReturn;
      });
      String resultAsString = Arrays.toString(result);
      LOGGER.debug("Save FieldLevelAccuracy batch result: {} ", resultAsString);

      for (int code : result) {
        if (code != Statement.SUCCESS_NO_INFO && code <= 0) {
          throw new DataNotFoundException("Error to save data: " + resultAsString);
        }
      }
    } catch (Exception e) {
      throw new DatabaseException("Error to update FieldLevelAccuracy list", e);
    }
  }

  @Transactional
  public void insertInvalidFiles(
      final List<InvalidFile> invalidFiles
  ) {

    LOGGER.debug("Saving InvalidFiles list to DB");

    int[] result;
    try {
      Session session = getSession();
      result = session.doReturningWork((conn) -> {
        try (PreparedStatement ps = conn.prepareStatement(
            "INSERT INTO InvalidFiles (FileId, ReasonId, ReasonText) VALUES (?, ?, ?)")
        ) {
          for (InvalidFile invalidFile : invalidFiles) {
            ps.setString(1, invalidFile.fileId);
            ps.setString(2, invalidFile.reasonId);
            ps.setString(3, invalidFile.reasonText);
            ps.addBatch();
          }
          return ps.executeBatch();
        }
      });

      LOGGER.debug("Save InvalidFiles batch result: {} ", Arrays.toString(result));

    } catch (Exception e) {
      throw new DatabaseException("Error to update InvalidFiles list", e);
    }

    for (int code : result) {
      if (code != Statement.SUCCESS_NO_INFO && code <= 0) {
        throw new DatabaseException("Error to save data: " + Arrays.toString(result));
      }
    }
  }

  @Transactional
  public void unlockDocuments(String username) {
    LOGGER.debug("Unlock all documents of the user: {}", username);

    try {
      Session session = getSession();

      int result = session.createNativeQuery(
          "UPDATE VisionBotDocuments SET LockedUserid = ?, LockedTimestamp = ? "
              + " WHERE LockedUserid = ? ")
          .setParameter(1, UNLOCKED_USER)
          .setParameter(2, (Date) null, TemporalType.TIMESTAMP)
          .setParameter(3, username)
          .executeUpdate();

      LOGGER.debug("Unlock {} documents of user {}", result, username);

    } catch (Exception e) {
      throw new DatabaseException("Error to unlock documents with user " + username, e);
    }
  }

  @Transactional
  public void keepValidatorDocumentAlive(String fileId) {
    LOGGER.debug("Update lock timestamp for fileId: {}", fileId);
    Session session = getSession();

    int result = session.createNativeQuery(
        "UPDATE VisionBotDocuments SET LockedTimestamp = ? WHERE FileId = ?")
        .setParameter(1, new Date(), TemporalType.TIMESTAMP)
        .setParameter(2, fileId)
        .executeUpdate();
    if (result == 0) {
      throw new DataNotFoundException("validator document not found with fileId " + fileId);
    }
    LOGGER.debug("updated {} entries for given file {}", result, fileId);
  }

  @Transactional
  public void unlockedExpiredDocuments(int timeoutInSec) {

    LOGGER.debug("Unlocked Documents with timeout {} sec", timeoutInSec);

    Session session = getSession();

    int result = session.createNativeQuery(
        "UPDATE VisionBotDocuments SET LockedUserid = ?, LockedTimestamp = ?"
            + " WHERE DATEDIFF(second, LockedTimestamp, SYSDATETIME()) > ? OR LockedTimestamp IS NULL")
        .setParameter(1, UNLOCKED_USER)
        .setParameter(2, (Date) null, TemporalType.TIMESTAMP)
        .setParameter(3, timeoutInSec)
        .executeUpdate();

    LOGGER.debug("Unlocked {} Documents", result);
  }

  private Session getSession() {
    Session session;
    try {
      session = sessionFactory.getCurrentSession();

    } catch (HibernateException e) {
      session = sessionFactory.openSession();
    }
    return session;
  }
}
