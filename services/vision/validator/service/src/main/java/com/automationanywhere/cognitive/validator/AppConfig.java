package com.automationanywhere.cognitive.validator;

/**
 * Created by keval.sheth on 24-04-2017.
 */
public final class AppConfig {

  private final String outputDirectory;
  private final int httpPort;
  private final int restTemplateReadTimeout;
  private final int restTemplateConnectTimeout;

  AppConfig(
      final String outputDirectory,
      final int httpPort,
      final int restTemplateReadTimeout,
      final int restTemplateConnectTimeout
  ) {
    this.outputDirectory = outputDirectory;
    this.httpPort = httpPort;
    this.restTemplateReadTimeout = restTemplateReadTimeout;
    this.restTemplateConnectTimeout = restTemplateConnectTimeout;
  }

  /**
   * Returns the outputDirectory.
   *
   * @return the value of outputDirectory
   */
  public String getOutputDirectory() {
    return outputDirectory;
  }

  /**
   * Returns the Http port.
   *
   * @return the value of httpPort
   */
  public int getHttpPort() {
    return httpPort;
  }

  /**
   * Returns the restTemplateReadTimeout.
   *
   * @return the value of restTemplateReadTimeout
   */
  public int getRestTemplateReadTimeout() {
    return restTemplateReadTimeout;
  }

  /**
   * Returns the restTemplateConnectTimeout.
   *
   * @return the value of restTemplateConnectTimeout
   */
  public int getRestTemplateConnectTimeout() {
    return restTemplateConnectTimeout;
  }
}
