package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class DocumentProcessingDetails {

  @JsonProperty(value = "failedFieldCount")
  private float FailedFieldCounts;
  @JsonProperty(value = "passedFieldCount")
  private float PassedFieldCounts;
  @JsonProperty(value = "totalFieldCount")
  private float TotalFieldCounts;


  public float getFailedFieldCounts() {
    return this.FailedFieldCounts;
  }

  public void setFailedFieldCounts(int failedFieldCounts) {
    this.FailedFieldCounts = failedFieldCounts;
  }

  public float getPassFieldCounts() {
    return this.PassedFieldCounts;
  }

  public void setPassFieldCounts(float passFieldCounts) {
    this.PassedFieldCounts = passFieldCounts;
  }

  public float getTotalFieldCounts() {
    return this.TotalFieldCounts;
  }

  public void setTotalFieldCounts(float totalFieldCounts) {
    this.TotalFieldCounts = totalFieldCounts;
  }


}