package com.automationanywhere.cognitive.exporter.impl;

import com.automationanywhere.cognitive.exporter.contracts.FileWriter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import org.springframework.stereotype.Component;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
@Component
public class FileWriterImpl implements FileWriter {

  public void write(Path path, byte[] data, Boolean doOverwrite) throws IOException {
    Files.write(path, data);
  }

    @Override
    public void WriteCsv(Path path, byte[] data, Boolean doOverwrite) throws IOException {
        final byte[] bom = new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF };
        byte[] dataWithBom = new byte[bom.length + data.length];
        System.arraycopy(bom, 0, dataWithBom, 0, bom.length);
        System.arraycopy(data, 0, dataWithBom, bom.length, data.length);
        Files.write(path,dataWithBom);
    }
}
