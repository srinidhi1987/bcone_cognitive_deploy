package com.automationanywhere.cognitive.validator;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.models.CustomResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Locale;
import org.springframework.context.MessageSource;

/**
 * Created by Mayur.Panchal on 09-12-2016.
 */
public abstract class AbstractHandler {

  private static final AALogger LOGGER = AALogger.create(AbstractHandler.class);

  protected final ObjectMapper mapper;
  protected final MessageSource messageSource;

  protected AbstractHandler(
      final ObjectMapper mapper,
      final MessageSource messageSource
  ) {
    this.mapper = mapper;
    this.messageSource = messageSource;
  }

  protected String getResponse(CustomResponse customResponse) {
    try {
      return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);

    } catch (JsonProcessingException e) {
      LOGGER.error(
          messageSource.getMessage(Constants.JSON_EXCEPTION_OCCURRED, null, Locale.getDefault()),
          e);
      return "";
    }
  }
}
