/**
 * Copyright (c) 2016 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */

package com.automationanywhere.cognitive.validator.exception.impl;

import java.util.Locale;

public enum HttpStatusCode {

  OK(200, "status.ok"),
  CREATED(201, "status.created"),
  NO_CONTENT(204, "status.nocontent"),

  NOT_MODIFIED(304, "status.notmodified"),

  BAD_REQUEST(400, "status.badrequest"),
  UNAUTHORIZED(401, "status.unauthorized"),
  FORBIDDEN(403, "status.forbidden"),
  NOT_FOUND(404, "status.notfound"),
  CONFLICT(409, "status.conflict"),
  RESOURCELOCKED(423, "status.locked"),
  INTERNAL_SERVER_ERROR(500, "status.internalservererror"),
  SERVICE_UNAVAILABLE(503, "status.serviceunavailable");

  private int code;
  private String messageKey;

  HttpStatusCode(int code, String messageKey) {
    this.code = code;
    this.messageKey = messageKey;
  }

  /**
   * Gets the HTTP status code
   *
   * @return the status code number
   */
  public int getCode() {
    return code;
  }

  /**
   * Get the message key
   *
   * @return the message key of  status code
   */
  public String getMessageKey() {
    return messageKey;
  }

  /**
   * Get message for HttpStatus
   *
   * @return the messages for  HttpStatus
   */
  public String getMessage(Locale locale) {
    //   return ResourceBundleUtil.getMessage(messageKey, locale);
    return "";
  }

  /**
   * Get message for HttpStatus
   *
   * @return the messages for  HttpStatus for default locale (EN_US)
   */
  public String getMessage() {
    //  return ResourceBundleUtil.getMessage(messageKey);
    return "";
  }
}