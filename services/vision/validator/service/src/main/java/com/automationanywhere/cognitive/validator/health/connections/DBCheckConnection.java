package com.automationanywhere.cognitive.validator.health.connections;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shweta.thakur
 *
 * This class checks the database connection health
 */
public class DBCheckConnection implements HealthCheckConnection {

  private IValidatorServiceDataAccessLayer validatorServiceDataAccessLayer;

  public DBCheckConnection(IValidatorServiceDataAccessLayer validatorServiceDataAccessLayer) {
    this.validatorServiceDataAccessLayer = validatorServiceDataAccessLayer;
  }

  @Override
  public void checkConnection() {
    try {
      validatorServiceDataAccessLayer
          .ExecuteCustomQuery("testconnection", null, null, new VisionBotDataDetail());
    } catch (Exception ex) {
      throw new DBConnectionFailureException("Error connecting to database ", ex);
    }
  }

  @Override
  public List<ServiceConnectivity> checkConnectionForService() {
    // return empty list
    return new ArrayList<ServiceConnectivity>();
  }
}
