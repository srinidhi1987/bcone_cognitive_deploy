package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by Yogesh.Savalia on 27-01-2017.
 */
public class CategoryDetail {

  @JsonProperty("id")
  public String id;
  @JsonProperty("noOfDocuments")
  public long noOfDocuments;
  @JsonProperty("priority")
  public int priority;
  @JsonIgnore
  public float allFieldsAvg;
  @JsonProperty("index")
  public double index;
  @JsonIgnore
  public float PercentageOfDocuments;
  @JsonProperty("allFieldDetail")
  public List<FieldDetail> allFieldDetail;
}
