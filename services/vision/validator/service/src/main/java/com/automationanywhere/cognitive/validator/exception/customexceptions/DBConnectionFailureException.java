package com.automationanywhere.cognitive.validator.exception.customexceptions;

public class DBConnectionFailureException extends RuntimeException {

  private static final long serialVersionUID = 2625347159760672379L;

  public DBConnectionFailureException(String message, Throwable cause) {
    super(message, cause);
  }

}
