package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

@Entity
@Table(name = "InvalidFileReasons")
public class InvalidFileTypes {//} extends EntityModelBase{
  @Id
  @Column(name = "Id")
  @JsonProperty
  private int id;
  @JsonProperty
  @Column(name = "Reason")
  private String reason;


  public int getInvalidID() {
    return this.id;
  }

  public String getInvalidReason() {
    return this.reason;
  }

  public void setInvalidReason(String InvalidReason) {
    this.reason = InvalidReason;
  }


}