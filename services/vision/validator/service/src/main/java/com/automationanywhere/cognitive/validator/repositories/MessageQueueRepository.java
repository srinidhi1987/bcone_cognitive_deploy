package com.automationanywhere.cognitive.validator.repositories;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static com.automationanywhere.cognitive.logging.Constants.EXCEPTION_OCCURRED;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.validator.RpcClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class MessageQueueRepository {

  private static final AALogger LOGGER = AALogger.create(MessageQueueRepository.class);

  private final ConnectionFactory rpcConnectionFactory;
  private final MessageSource msgSource;
  private final ObjectMapper mapper;

  public MessageQueueRepository(
      final ConnectionFactory rpcConnectionFactory,
      @Qualifier(MESSAGE_SOURCE) final MessageSource msgSource,
      final ObjectMapper mapper
  ) {
    this.rpcConnectionFactory = rpcConnectionFactory;
    this.msgSource = msgSource;
    this.mapper = mapper;
  }

  public void broadcastMessageToPurgeData(String fileId) {
    RpcClient client = null;
    try {
      Map<String, Object> dictionary = new HashMap<>();
      dictionary.put("fileId", fileId);

      String requestData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dictionary);
      LOGGER.trace("[RPC call] [Queue : QUEUE_Purge_Secure_Data]");

      client = new RpcClient("QUEUE_Purge_Secure_Data", rpcConnectionFactory);
      LOGGER.debug("[Data purging Message] [FileId = '{}']", fileId);

      client.putMessage(requestData);
      LOGGER.trace("[Done RPC call]");

    } catch (Exception ex) {
      LOGGER.error(msgSource.getMessage(EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);

    } finally {
      if (client != null) {
        try {
          client.close();
        } catch (Exception e) {
          LOGGER.error("Error to close RpcClient", e);
        }
      }
    }
  }
}
