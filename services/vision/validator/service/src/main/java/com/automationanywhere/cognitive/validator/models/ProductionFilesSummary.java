package com.automationanywhere.cognitive.validator.models;

public class ProductionFilesSummary {

  private String projectId;

  private long processedFiles;

  private long successFiles;

  private long reviewFiles;

  private long invalidFiles;

  private long pendingForReview;

  private double accuracy = 0;

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public long getProcessedFiles() {
    return processedFiles;
  }

  public void setProcessedFiles(long processedFiles) {
    this.processedFiles = processedFiles;
  }

  public long getSuccessFiles() {
    return successFiles;
  }

  public void setSuccessFiles(long successFiles) {
    this.successFiles = successFiles;
  }

  public long getReviewFiles() {
    return reviewFiles;
  }

  public void setReviewFiles(long reviewFiles) {
    this.reviewFiles = reviewFiles;
  }

  public long getInvalidFiles() {
    return invalidFiles;
  }

  public void setInvalidFiles(long invalidFiles) {
    this.invalidFiles = invalidFiles;
  }

  public long getPendingForReview() {
    return pendingForReview;
  }

  public void setPendingForReview(long pendingForReview) {
    this.pendingForReview = pendingForReview;
  }

  public double getAccuracy() {
    return accuracy;
  }

  public void setAccuracy(double accuracy) {
    this.accuracy = accuracy;
  }
}
