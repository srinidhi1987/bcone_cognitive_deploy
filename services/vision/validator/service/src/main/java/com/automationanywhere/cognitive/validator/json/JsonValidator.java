package com.automationanywhere.cognitive.validator.json;

import com.automationanywhere.cognitive.validator.dto.VisionBotDto;
import com.automationanywhere.cognitive.validator.exception.JsonValidatorError;
import com.automationanywhere.cognitive.validator.exception.JsonValidatorException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JsonValidator {

  private static final String VISION_BOT_SCHEMA_KEY = "VisionBot";

  private final JsonMapper mapper;
  private final ClasspathSchemaValidator schemaValidator;
  private final Validator validator;

  @Autowired
  public JsonValidator(final JsonMapper mapper, final ClasspathSchemaValidator schemaValidator) {
    this.mapper = mapper;
    this.schemaValidator = schemaValidator;
    this.validator = Validation.buildDefaultValidatorFactory().getValidator();

    schemaValidator.checkKey(VISION_BOT_SCHEMA_KEY);
  }

  public void validateVisionBot(String data) throws JsonValidatorException {

    schemaValidator.checkJson(data, VISION_BOT_SCHEMA_KEY);

    VisionBotDto visionBotDto = mapper.build(data, VisionBotDto.class);
    Set<ConstraintViolation<VisionBotDto>> violations = validator.validate(visionBotDto);

    if (!violations.isEmpty()) {
      List<JsonValidatorError> errors = violations.stream()
          .map(c -> new JsonValidatorError(c.getPropertyPath().toString(), c.getMessage()))
          .collect(Collectors.toList());

      throw new JsonValidatorException(errors);
    }
  }
}
