package com.automationanywhere.cognitive.validator.models;

import com.automationanywhere.cognitive.validator.ValidationType;
import com.automationanywhere.cognitive.validator.hibernate.attributeconverters.SecureStringAttributeConverter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Keval.Sheth on 07-02-2017.
 */
@Entity
@Table(name = "VisionBotDocuments")
public class VisionBotDataDetail extends EntityModelBase {

    @Id
    @Column(name = "fileId")
    @JsonProperty(value = "fileId")
    private String fileId;
    @JsonProperty(value = "orgId")
    @Column(name = "orgId")
    private String orgId;
    @JsonProperty(value = "projectId")
    @Column(name = "projectId")
    private String projectId;
    @Nationalized
    //@Convert(converter=SecureStringAttributeConverter.class)
    @JsonProperty(value = "vBotDocument")
    @Column(name = "vBotDocument")
    private String vBotDocument;
    @JsonProperty(value = "validationStatus")
    @Column(name = "ValidationStatus")
    private String validationStatus;
    @Nationalized
    @JsonProperty(value = "correctedData")
    @Column(name = "CorrectedData")
    private String correctedData;
    @JsonProperty(value = "createdAt")
    @Column(name = "CreatedAt")
    private Date createdAt;
    @JsonProperty(value = "updatedBy")
    @Column(name = "UpdatedBy")
    private String updatedBy;
    @JsonProperty(value = "updatedAt")
    @Column(name = "UpdatedAt")
    private Date updatedAt;
    /*@JsonProperty(value = "vbotInUse")
    @Column(name = "VBotInUse")
    private boolean vbotInUse;*/
    @JsonProperty(value = "visionBotId")
    @Column(name = "VisionBotId")
    private String visionBotId;
    @JsonProperty(value = "lockedUserId")
    @Column(name = "LockedUserid")
    private String lockedUserId;
    @JsonProperty(value = "failedFieldCounts")
    @Column(name = "FailedFieldCounts")
    private float FailedFieldCounts=0;
    @JsonProperty(value = "passFieldCounts")
    @Column(name = "PassFieldCounts")
    private float PassFieldCounts=0;
    @JsonProperty(value = "totalFieldCounts")
    @Column(name = "TotalFieldCounts")
    private float TotalFieldCounts =0 ;
    @JsonProperty(value = "startTime")
    @Column(name = "StartTime")
    private Date StartTime;
    @JsonProperty(value = "endTime")
    @Column(name = "EndTime")
    private Date EndTime;
    @JsonProperty(value = "averageTimeInSeconds")
    @Column(name = "AverageTimeInSeconds")
    private long AverageTimeInSeconds=0;
    @Column(name = "lastExportTime")
    @JsonProperty(value = "lastExportTime")
    private Date lastExportTime;
	@Column(name = "LockedTimestamp")
	@JsonProperty(value = "lockedTimestamp")
  	private Date lockedTimestamp;

  public String getOrgId() {
    return this.orgId;
  }

  public void setOrgId(String orgid) {
    this.orgId = orgid;
  }

  public long getAverageTimeInSeconds() {
    return AverageTimeInSeconds;
  }

  public void setAverageTimeInSeconds(long averageTimeInSeconds) {
    AverageTimeInSeconds = averageTimeInSeconds;
  }

  public String getVBotData() {
    return this.vBotDocument;
  }

  public void setVBotData(String json) {
    this.vBotDocument = json;
  }

  public String getFileId() {
    return this.fileId;
  }

  public void setFileId(String id) {
    this.fileId = id;
  }

  public String getVisionBotId() {
    return this.visionBotId;
  }

  public void setVisionBotId(String visionbotid) {
    this.visionBotId = visionbotid;
  }

  public String getProjectId() {
    return this.projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public Date getStartTime() {
    return StartTime;
  }

  public void setStartTime(Date startTime) {
    StartTime = startTime;
  }

  public Date getEndTime() {
    return EndTime;
  }

  public void setEndTime(Date endTime) {
    EndTime = endTime;
  }

  public String getValidationStatus() {
    return this.validationStatus;
  }

  public void setValidationStatus(String status) {
    this.validationStatus = status;
  }

  public String getCorrectedData() {
    return this.correctedData;
  }

  public void setCorrectedData(String correctedData) {
    this.correctedData = correctedData;
  }

  public Date getCreatedAt() {
    return this.createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedBy() {
    return this.updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedAt() {
    return this.updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

 /*   public void setVBotInUse(boolean vbotInUse)
    {
        this.vbotInUse = vbotInUse;
    }
    public boolean getVBotInUse()
    {
        return this.vbotInUse;
    }*/

  public String getLockedUserId() {
    return this.lockedUserId;
  }

  public void setLockedUserId(String lockeduserid) {
    this.lockedUserId = lockeduserid;
  }

  public float getFailedFieldCounts() {
    return this.FailedFieldCounts;
  }

  public void setFailedFieldCounts(float failedFieldCounts) {
    this.FailedFieldCounts = failedFieldCounts;
  }

  public float getPassFieldCounts() {
    return this.PassFieldCounts;
  }

  public void setPassFieldCounts(float passFieldCounts) {
    this.PassFieldCounts = passFieldCounts;
  }

  public float getTotalFieldCounts() {
    return this.TotalFieldCounts;
  }

  public void setTotalFieldCounts(float totalFieldCounts) {
    this.TotalFieldCounts = totalFieldCounts;
  }

  public Date getLastExportTime() {
    return lastExportTime;
  }

  public void setLastExportTime(Date lastExportTime) {
    this.lastExportTime = lastExportTime;
  }

  public Date getLockedTimestamp() {
    return lockedTimestamp;
  }

  public void setLockedTimestamp(Date lockedTimestamp) {
    this.lockedTimestamp = lockedTimestamp;
  }

  @Override
  public Map<String, Object> getCustomQuery(StringBuilder queryString, String queryId,
      String idtype, String id) {
    queryString.setLength(0);
    Map<String, Object> inputParam = new HashMap<String, Object>();
    String querys = "";
    if (queryId.equalsIgnoreCase("AccuracyDetails")) {
      if (idtype.equalsIgnoreCase("visionbotid")) {
        querys = "select visionBotId, " +
            "ISNULL((select sum(FailedFieldCounts) from VisionBotDataDetail vb where vb1.visionBotId=vb.visionBotId and vb.validationStatus != :invalidvalidationStatus),0) as FailedFieldCounts,\n"
            +
            "ISNULL((select sum(PassFieldCounts) from VisionBotDataDetail vb where vb1.visionBotId=vb.visionBotId and vb.validationStatus != :invalidvalidationStatus),0) as PassFieldCounts,\n"
            +
            "ISNULL((select sum(TotalFieldCounts) from VisionBotDataDetail vb where vb1.visionBotId=vb.visionBotId and vb.validationStatus != :invalidvalidationStatus),0) as TotalFieldCounts,\n"
            +
            "( select Str(ISNULL((sum(PassFieldCounts)*100/NULLIF(sum(TotalFieldCounts),0)),0),12,2) from VisionBotDataDetail vb where vb.validationStatus != :invalidvalidationStatus and vb1.visionBotId=vb.visionBotId)as FieldAccuracy, "
            +
            "( select count(fileId) from VisionBotDataDetail vb where (((vb.validationStatus = :passvalidationStatus and (vb.vBotDocument != :nulldata or vb.vBotDocument != :emptydata))) and vb1.visionBotId=vb.visionBotId)) as reviewFileCount, "
            +
            "( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :invalidvalidationStatus and vb1.visionBotId=vb.visionBotId ) as invalidFileCount, "
            +
            "( select count(fileId) from VisionBotDataDetail vb where ((vb.validationStatus = :invalidvalidationStatus or vb.validationStatus = :failvalidationStatus or (vb.validationStatus = :passvalidationStatus and (vb.vBotDocument != :nulldata or vb.vBotDocument != :emptydata))) and vb1.visionBotId=vb.visionBotId)) as failedFileCount, "
            +
            "( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :passvalidationStatus and vb.vBotDocument = '' and vb1.visionBotId=vb.visionBotId ) as PassedDocumentCount,  "
            +
            "( select count(fileId) from VisionBotDataDetail vb where  vb1.visionBotId=vb.visionBotId) as ProcessedDocumentCount, "
            +
            "(100*( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :passvalidationStatus and vb1.visionBotId=vb.visionBotId and (vb.vBotDocument = :nulldata or vb.vBotDocument = :emptydata))/( select count(*) from VisionBotDataDetail vb where  vb1.visionBotId=vb.visionBotId)) as DocumentAccuracy, "
            +
            " ISNULL((sum(AverageTimeInSeconds)/NULLIF((select count(fileId) from VisionBotDataDetail vb where  vb1.visionBotId=vb.visionBotId and vb.StartTime != '' and vb.EndTime != ''),0)),0) as AverageReviewTimeSeconds"
            +
            " FROM VisionBotDataDetail vb1 " +
            "group by vb1.visionBotId ";

        if (id != null) {
          querys += "having visionBotId = :id";
        }
      } else if (idtype.equalsIgnoreCase("projectid")) {
        querys = "select projectId,\n" +
            "ISNULL((select sum(FailedFieldCounts) from VisionBotDataDetail vb where vb1.projectId=vb.projectId and vb.validationStatus != :invalidvalidationStatus),0) as FailedFieldCounts, "
            +
            "ISNULL((select sum(PassFieldCounts) from VisionBotDataDetail vb where vb1.projectId=vb.projectId and vb.validationStatus != :invalidvalidationStatus),0) as PassFieldCounts,\n"
            +
            "ISNULL((select sum(TotalFieldCounts) from VisionBotDataDetail vb where vb1.projectId=vb.projectId and vb.validationStatus != :invalidvalidationStatus),0) as TotalFieldCounts,\n"
            +
            "\t(select Str(ISNULL((sum(PassFieldCounts)*100/NULLIF(sum(TotalFieldCounts),0)),0),12,2) from VisionBotDataDetail vb where vb.validationStatus != :invalidvalidationStatus and vb1.projectId=vb.projectId) as FieldAccuracy,\n"
            +
            "( select count(fileId) from VisionBotDataDetail vb where ((vb.validationStatus = :invalidvalidationStatus or (vb.validationStatus = :passvalidationStatus and (vb.vBotDocument != :nulldata or vb.vBotDocument != :emptydata))) and vb1.projectId=vb.projectId)) as reviewFileCount,\n"
            +
            "( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :invalidvalidationStatus and vb1.projectId=vb.projectId ) as invalidFileCount,\n"
            +
            "( select count(fileId) from VisionBotDataDetail vb where ((vb.validationStatus = :invalidvalidationStatus or vb.validationStatus = :failvalidationStatus or (vb.validationStatus = :passvalidationStatus and (vb.vBotDocument != :nulldata or vb.vBotDocument != :emptydata))) and vb1.projectId=vb.projectId)) as failedFileCount,\n"
            +
            "\t( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :passvalidationStatus and vb.vBotDocument = '' and vb1.projectId=vb.projectId ) as PassedDocumentCount, \n"
            +
            " ( select count(fileId) from VisionBotDataDetail vb where  vb1.projectId=vb.projectId) as ProcessedDocumentCount,\n"
            +
            "\t(100*( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :passvalidationStatus and vb1.projectId=vb.projectId and (vb.vBotDocument = :nulldata or vb.vBotDocument = :emptydata))/  ( select count(fileId) from VisionBotDataDetail vb where  vb1.projectId=vb.projectId)) as DocumentAccuracy,\n"
            +
            "\t ISNULL((sum(AverageTimeInSeconds)/NULLIF((select count(fileId) from VisionBotDataDetail vb where  vb1.projectId=vb.projectId and vb.StartTime != '' and vb.EndTime != ''),0)),0) as AverageReviewTimeSeconds \n"
            +
            "FROM VisionBotDataDetail vb1\n" +
            "group by vb1.projectId ";
        if (id != null) {
          querys += "having projectId = :id";
        }

      }
      //  inputParam.put(":idtype", idtype);
      if (id != null) {
        inputParam.put("id", id);
      }
      inputParam.put("nulldata", null);
      inputParam.put("emptydata", "");
      inputParam.put("failvalidationStatus", ValidationType.Fail.toString());
      inputParam.put("invalidvalidationStatus", ValidationType.Invalid.toString());
      inputParam.put("passvalidationStatus", ValidationType.Pass.toString());
      queryString.append(querys);
    } else if (queryId.equalsIgnoreCase("FieldAccuracyAndValidatedFiles")) {
      querys +=
          "SELECT a.FieldId, 100*(sum(a.PassAccuracyValue)/count(a.PassAccuracyValue)) AS FieldAccuracy, \n"
              +
              "100*(select count(f1.FileId) from FieldAccuracyDetails f1 INNER JOIN VisionBotDataDetail f ON f.fileId = f1.FileId\n"
              +
              "WHERE f.projectId = :projectid and f1.FieldId = a.FieldId and f.StartTime IS NOT NULL and (f.vBotDocument != :nulldata or f.vBotDocument != :emptydata) and f.validationStatus = :passvalidationStatus and f.EndTime IS NOT NULL and f1.PassAccuracyValue != 1)/\n"
              +
              "(select count(f1.FileId) from FieldAccuracyDetails f1 INNER JOIN VisionBotDataDetail f ON f.fileId = f1.FileId\n"
              +
              "WHERE f.projectId = :projectid and f1.FieldId = a.FieldId  ) as FieldValidation\n" +
              "FROM FieldAccuracyDetails a\n" +
              "INNER JOIN VisionBotDataDetail f ON f.fileId = a.FileId\n" +
              "WHERE f.projectId = :projectid \n" +
              "GROUP BY a.FieldId ";
      //  inputParam.put(":idtype", idtype);

      //   inputParam.put("nulldata", null);
      inputParam.put("nulldata", null);
      inputParam.put("emptydata", "");
      inputParam.put("passvalidationStatus", ValidationType.Pass.toString());
      inputParam.put("projectid", id);
      queryString.append(querys);
    } else if (queryId.equalsIgnoreCase("ValidationTimeSpendCategorywise")) {
      querys += " select f.projectId, f.classificationId,\n" +
          "(sum(v.AverageTimeInSeconds)/count(*))\n" +
          "from\n" +
          "VisionBotDataDetail v, FileDetails f\n" +
          "where f.fileId = v.fileId\n" +
          "and f.isProduction = 1\n" +
          "and ((v.validationStatus = :passvalidationStatus and (v.vBotDocument!= :nulldata or v.vBotDocument != '')) or v.validationStatus = :invalidvalidationStatus)\n"
          +
          "group by f.classificationId, f.projectId\n" +
          "having f.projectId = :projectid";
      //  inputParam.put(":idtype", idtype);

      inputParam.put("nulldata", null);
      inputParam.put("projectid", id);
      inputParam.put("invalidvalidationStatus", ValidationType.Invalid.toString());
      inputParam.put("passvalidationStatus", ValidationType.Pass.toString());
      queryString.append(querys);
    } else if (queryId.equalsIgnoreCase("Dashboard")) {
      querys +=
          "select vb1.projectId,( select count(fileId) from VisionBotDataDetail vb where  vb.projectId=:projectid) as TotalFilesProcessed, \n"
              +
              "( 100*( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :passvalidationStatus \n"
              +
              " and vb.projectId=:projectid and (vb.vBotDocument = :nulldata or vb.vBotDocument = ''))/( select count(*) from FileDetails f where  f.projectId=:projectid and f.isProduction = 1)) as TotalSTP,\n"
              +
              "(select count(fileId) from FileDetails f where f.projectId = :projectid and f.isProduction = 1) as TotalFilesUploaded,"
              +
              "(select COALESCE(100*sum(PassFieldCounts)/NULLIF(sum(TotalFieldCounts),0), 0) from VisionBotDataDetail \n"
              +
              "where projectId  = :projectid and ValidationStatus != :invalidvalidationStatus) as TotalAccuracy,\n"
              +
              " ( select count(fileId) from VisionBotDataDetail vb where  vb.projectId=:projectid and (vb.vBotDocument != :nulldata \n"
              +
              " or vb.vBotDocument != '')) as TotalFilesToValidation, \n" +
              " ( select count(fileId) from VisionBotDataDetail vb where (vb.validationStatus = :passvalidationStatus)\n"
              +
              "  and vb.projectId=:projectid and  (vb.vBotDocument != :nulldata or vb.vBotDocument != '')) as TotalFilesValidated,\n"
              +
              " ( select count(fileId) from VisionBotDataDetail vb where vb.validationStatus = :invalidvalidationStatus \n"
              +
              " and vb.projectId=:projectid ) as TotalFilesUnprocessable\n" +
              "  FROM VisionBotDataDetail vb1\n" +
              "  group by vb1.projectId\n" +
              "  having vb1.projectId  = :projectid";
      //  inputParam.put(":idtype", idtype);
           /* if(id != null)
            {
                inputParam.put("id", id);
            }*/

      inputParam.put("nulldata", null);
      inputParam.put("projectid", id);
      inputParam.put("invalidvalidationStatus", ValidationType.Invalid.toString());
      inputParam.put("passvalidationStatus", ValidationType.Pass.toString());
      queryString.append(querys);
    } else if (queryId.equalsIgnoreCase("testconnection")) {
      querys += "select 1 from VisionBotDataDetail";
      queryString.append(querys);
    } else if (queryId.equalsIgnoreCase("ProductionSummaryDetails")) {
      querys = "select distinct projectId,\n" +
          "  (select count(*) from VisionBotDataDetail \n" +
          "  where projectId = :projectid) as ProcessedFiles, " +
          "  (select count(*) from VisionBotDataDetail where isVBotDocumentColumnEmpty = 0 and validationStatus = 'Pass' \n"
          +
          "  and projectId = :projectid) as Success,\n" +
          "  (select count(*) from VisionBotDataDetail where isVBotDocumentNotEmptyAndValidationStatusPass = 1 \n"
          +
          "  and projectId = :projectid) as Review,\n" +
          "  (select count(*) from VisionBotDataDetail where validationStatus = 'Invalid' \n" +
          "  and projectId = :projectid) as Invalid,\n" +
          "  (select count(*) from VisionBotDataDetail where isVBotDocumentColumnEmpty = 1 and validationStatus = 'Fail'  \n"
          +
          "  and projectId = :projectid) as PendingForReview ,\n" +
          "(select COALESCE(100*sum(PassFieldCounts)/NULLIF(sum(TotalFieldCounts),0), 0) from VisionBotDataDetail \n"
          +
          "where projectId  = :projectid and ValidationStatus != 'Invalid') as TotalAccuracy " +
          "from VisionBotDataDetail where projectId = :projectid";
      inputParam.put("projectid", id);
      queryString.append(querys);
    } else {
      querys = "select distinct s.projectId,\n" +
          " (select count(*)  from VisionBotDataDetail v2 where v2.projectId = s.projectId) as Processed,\n"
          +
          " (select count(*)  from VisionBotDataDetail v2 where v2.projectId = s.projectId and (v2.validationStatus = :failvalidationStatus or v2.validationStatus = :invalidvalidationStatus or (v2.validationStatus = :passvalidationStatus and (v2.vBotDocument != :nulldata or v2.vBotDocument != :emptydata)))) as Fail,\n"
          +
          " (select count(*)  from VisionBotDataDetail v3 where v3.projectId = s.projectId and v3.validationStatus = :passvalidationStatus and \n"
          +
          "(v3.vBotDocument != :nulldata or v3.vBotDocument != :emptydata))as Pass,\n" +
          "(select count(*)  from VisionBotDataDetail v3 where v3.projectId = s.projectId and (v3.validationStatus = :invalidvalidationStatus))as Invalid \n"
          +
          "  from VisionBotDataDetail s";

      inputParam.put("invalidvalidationStatus", "Invalid");
      inputParam.put("passvalidationStatus", ValidationType.Pass.toString());
      inputParam.put("failvalidationStatus", ValidationType.Fail.toString());
      inputParam.put("nulldata", null);
      inputParam.put("emptydata", "");
      queryString.append(querys);
    }
    return inputParam;
  }

  @Override
  public Map<String, Object> getCustomQueryForDistinctElements(StringBuilder queryString,
      String parameter) {
    queryString.setLength(0);
    Map<String, Object> inputParam = new HashMap<String, Object>();
    String querys = "select distinct :columnname from VisionBotDataDetail";
    inputParam.put("columnname", parameter);
    queryString.append(querys);
    return inputParam;
  }


}
