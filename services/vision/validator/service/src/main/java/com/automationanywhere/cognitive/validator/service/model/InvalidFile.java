package com.automationanywhere.cognitive.validator.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class InvalidFile {

  public final String fileId;
  public final String reasonId;
  public final String reasonText;

  public InvalidFile(
      @JsonProperty("fileId") final String fileId,
      @JsonProperty("reasonId") final String reasonId,
      @JsonProperty("reasonText") final String reasonText
  ) {
    this.fileId = fileId;
    this.reasonId = reasonId;
    this.reasonText = reasonText;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof InvalidFile)) {
      return false;
    }
    InvalidFile that = (InvalidFile) o;
    return Objects.equals(fileId, that.fileId) &&
        Objects.equals(reasonId, that.reasonId) &&
        Objects.equals(reasonText, that.reasonText);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fileId, reasonId, reasonText);
  }
}
