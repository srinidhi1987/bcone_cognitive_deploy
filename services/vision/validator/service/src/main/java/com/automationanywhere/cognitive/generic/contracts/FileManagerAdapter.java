package com.automationanywhere.cognitive.generic.contracts;

import com.automationanywhere.cognitive.generic.models.FileDetail;
import com.automationanywhere.cognitive.validator.models.ClassificationAnalysisReport;

/**
 * Created by Jemin.Shah on 11-01-2017.
 */
public interface FileManagerAdapter {

  String FILE_MANAGER_SERVICE_ROOT_URL = "fileManagerServiceRootUrl";
  String FILE_MANAGER_ADAPTER_PROXY = "fileManagerAdapterProxy";

  FileDetail getSpecificFileFromVisionBot(String organizationId, String projectId, String fileId);

  byte[] downloadFile(String organizationId, String projectId, String fileId);

  ClassificationAnalysisReport getAllFileStats(String projId);

  void testConnection();
}
