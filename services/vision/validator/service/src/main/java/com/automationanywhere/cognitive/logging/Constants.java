package com.automationanywhere.cognitive.logging;

/**
 * Created by Jemin.Shah on 3/6/2017.
 */
public class Constants {

  public static final String HEADER_CORRELATION_ID = "cid";
  public static final String VALIDATOR_LOG_DETAIL = "Information:   Validation: Cognitive Platform ";
  public static final String VALIDATOR_LOG_DETAIL_ERROR = "Error:         Validation: Cognitive Platform ";
  public static final String VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI = "validator.validatorresourcehandler.input.uri";
  public static final String PREPARING_PROJECT_TOTAL_WITH_ACCURACYDETAILS = "validator.validatorresourcehandler.preparing.projecttotalswithaccuracydetails";
  public static final String DONE_PREPARING_PROJECT_TOTAL_WITH_ACCURACYDETAILS = "validator.validatorresourcehandler.donepreparing.projecttotalswithaccuracydetails";
  public static final String PREPARING_FIELD_ACCURACY_DASHBOARD_DETAILS = "validator.validatorresourcehandler.preparing.fieldaccuracydashboarddetails";
  public static final String DONE_PREPARING_FIELD_ACCURACY_DASHBOARD_DETAILS = "validator.validatorresourcehandler.donepreparing.fieldaccuracydashboarddetails";
  public static final String PREPARING_RESULT_GROUP_TIMESPEND = "validator.validatorresourcehandler.preparing.resultgrouptimespend";
  public static final String DONE_PREPARING_RESULT_GROUP_TIMESPEND = "validator.validatorresourcehandler.donepreparing.resultgrouptimespend";
  public static final String PREPARING_CLASSIFICATION_DETAILS_OBJECT = "validator.validatorresourcehandler.preparing.classificationdetails";
  public static final String DONE_PREPARING_CLASSIFICATION_DETAILS_OBJECT = "validator.validatorresourcehandler.donepreparing.classificationdetails";
  public static final String DATABASE_ERROR_OCCURRED = "validator.validatorresourcehandler.databaseerroroccurred";
  public static final String DATABASE_CONNECTION_DOWN = "validator.validatorresourcehandler.databaseconnectiondown";
  public static final String EXCEPTION_OCCURRED = "validator.exception";
  public static final String JSON_EXCEPTION_OCCURRED = "validator.jsonprocessing.exception";
  public static final String PREPARING_LIST_ACCURACYDETAILS = "validator.validatorresourcehandler.preparing.listaccuracyprocessingdetails";
  public static final String DONE_PREPARING_LIST_ACCURACYDETAILS = "validator.validatorresourcehandler.donepreparing.listaccuracyprocessingdetails";
  public static final String PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE = "validator.validatorresourcehandler.project.notfound.nodocuments.available";
  public static final String DOCUMENTS_LOCKED_BY_ANOTHER_USER = "validator.validatorresourcehandler.document.locked";
  public static final String UNABLE_TO_UNLOCK_USER = "validator.validatorresourcehandler.unabletounlockuser";
  public static final String GET_FAILED_BOT_RETURNED_VISION_BOT_DETAIL = "validator.getfailedbot.returned.visionbotdetail";
  public static final String READ_REQUEST_INTO_INVALID_FILES_JSON = "validator.read.request.into.invalid.files.json";
  public static final String INVALID_FILES_FOUND = "validator.invalid.files.found";
  public static final String VALIDATION_STATUS_WRONGLY_ENTERED = "validator.validation.status.wrongly.entered";
  public static final String PREPARING_REVIEW_RESOURCEBOTLIST = "validator.preparing.reviewresourcebotlist";
  public static final String DONE_PREPARING_REVIEW_RESOURCEBOTLIST = "validator.donepreparing.reviewresourcebotlist";
  public static final String VALIDATOR_ALERT_MULTIPLE_ENTRIES_INDB = "validator.alert.multiple.entries.indb";
  public static final String VALIDATOR_WARNING_INSUFFICIENT_DATA = "validator.warning.insufficient.data";

} 
