package com.automationanywhere.cognitive.validator.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

public final class FieldDataRecordFieldDto {

  private final String id;
  private final DataModelFieldDto field;
  private final FieldDataRecordFieldValueDto value;
  private final FieldDataRecordFieldValidationIssueDto validationIssue;

  public FieldDataRecordFieldDto(
      @JsonProperty("Id") final String id,
      @JsonProperty("Field") final DataModelFieldDto field,
      @JsonProperty("Value") final FieldDataRecordFieldValueDto value,
      @JsonProperty("ValidationIssue") final FieldDataRecordFieldValidationIssueDto validationIssue
  ) {
    this.id = id;
    this.field = field;
    this.value = value;
    this.validationIssue = validationIssue;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  @JsonProperty("Id")
  public String getId() {
    return id;
  }

  /**
   * Returns the field.
   *
   * @return the value of field
   */
  @JsonProperty("Field")
  public DataModelFieldDto getField() {
    return field;
  }

  /**
   * Returns the value.
   *
   * @return the value of value
   */
  @JsonProperty("Value")
  public FieldDataRecordFieldValueDto getValue() {
    return value;
  }

  /**
   * Returns the validationIssue.
   *
   * @return the value of validationIssue
   */
  @JsonProperty("ValidationIssue")
  public FieldDataRecordFieldValidationIssueDto getValidationIssue() {
    return validationIssue;
  }
}
