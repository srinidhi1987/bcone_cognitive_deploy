package com.automationanywhere.cognitive.validator.exception;

public class JsonPatchException extends RuntimeException {

  private static final long serialVersionUID = 8144942492353808787L;

  public JsonPatchException(String message) {
    super(message);
  }

  public JsonPatchException(Throwable cause) {
    super(cause);
  }
}
