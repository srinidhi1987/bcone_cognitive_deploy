package com.automationanywhere.cognitive.validator.exception.impl;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.exception.InvalidParameterException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.impl.DatabaseConnectionException;
import com.automationanywhere.cognitive.validator.models.CustomResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import spark.Request;
import spark.Response;

@Component
public class ExceptionHandler {

  private static final AALogger LOGGER = AALogger.create(ExceptionHandler.class);

  private final MessageSource msgSource;
  private final ObjectMapper mapper;

  public ExceptionHandler(
      @Qualifier(MESSAGE_SOURCE) final MessageSource msgSource,
      final ObjectMapper mapper
  ) {
    this.msgSource = msgSource;
    this.mapper = mapper;
  }

  public void handleException(
      final Exception exception,
      final Request request,
      final Response response
  ) {
    LOGGER.error(
        msgSource.getMessage(Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()),
        exception
    );

    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    customResponse.setErrors(exception.getMessage());

    response.status(SC_INTERNAL_SERVER_ERROR);
    response.type(MediaType.APPLICATION_JSON_VALUE);
    try {
      response.body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse));

    } catch (JsonProcessingException e) {
      LOGGER.error("Error to serialize error response", e);
    }
  }

  public void handleDbConnectionException(
      final DatabaseConnectionException exception,
      final Request request,
      final Response response
  ) {
    LOGGER.error(
        msgSource.getMessage(Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()),
        exception
    );

    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    response.status(SC_BAD_REQUEST);

    customResponse.setErrors(msgSource.getMessage(
        Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault())
    );

    response.type(MediaType.APPLICATION_JSON_VALUE);
    try {
      response.body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse));

    } catch (JsonProcessingException e) {
      LOGGER.error("Error to serialize error response", e);
    }
  }

  public void handleDataNotFoundException(
      final DataNotFoundException exception,
      final Request request,
      final Response response
  ) {
    LOGGER.error("Data not found", exception);

    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    customResponse.setErrors(exception.getMessage());

    response.status(SC_NOT_FOUND);
    response.type(MediaType.APPLICATION_JSON_VALUE);
    try {
      response.body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse));

    } catch (JsonProcessingException e) {
      LOGGER.error("Error to serialize error response", e);
    }
  }

  public void handleInvalidParameterException(
      final InvalidParameterException exception,
      final Request request,
      final Response response
  ) {
    LOGGER.error("Invalid parameter exception", exception);

    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    customResponse.setErrors(exception.getMessage());

    response.status(SC_BAD_REQUEST);
    response.type(MediaType.APPLICATION_JSON_VALUE);
    try {
      response.body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse));

    } catch (JsonProcessingException e) {
      LOGGER.error("Error to serialize error response", e);
    }
  }
}
