package com.automationanywhere.cognitive.exporter.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.exporter.contracts.FileExportActivityFinishAuditor;
import com.automationanywhere.cognitive.exporter.contracts.FileWriter;
import com.automationanywhere.cognitive.exporter.contracts.InvalidExporter;
import com.automationanywhere.cognitive.exporter.contracts.InvalidFilePathProvider;
import com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter;
import com.automationanywhere.cognitive.validator.datalayer.ValidatorDetailProvider;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
@Component
public class FileSystemBasedInvalidExporterImpl implements InvalidExporter {

  private static final AALogger LOGGER = AALogger.create(FileSystemBasedInvalidExporterImpl.class);

  private final InvalidFilePathProvider invalidFilePathProvider;
  private final FileWriter fileWriter;
  private final FileExportActivityFinishAuditor fileExportActivityFinishAuditor;
  private final ValidatorDetailProvider validatorDetailProvider;
  private final FileManagerAdapter fileManagerAdapter;

  @Autowired
  public FileSystemBasedInvalidExporterImpl(
      final InvalidFilePathProvider invalidFilePathProvider,
      final FileWriter fileWriter,
      final FileExportActivityFinishAuditor fileExportActivityFinishAuditor,
      final ValidatorDetailProvider validatorDetailProvider,
      final FileManagerAdapter fileManagerAdapter
  ) {
    this.invalidFilePathProvider = invalidFilePathProvider;
    this.fileWriter = fileWriter;
    this.fileExportActivityFinishAuditor = fileExportActivityFinishAuditor;
    this.validatorDetailProvider = validatorDetailProvider;
    this.fileManagerAdapter = fileManagerAdapter;
  }

  public void exportInvalidFile(String fileId) {

    try {
      LOGGER.entry();
      LOGGER.debug(() -> "FileId" + fileId);

      VisionBotDataDetail processedData = validatorDetailProvider.getDocumentByFileId(fileId);
      String projectId = processedData.getProjectId();
      String organizationsId = processedData.getOrgId();
      LOGGER.debug(
          () -> "OrganizationsId" + organizationsId + "ProjectId" + projectId + "FileId" + fileId);
      LOGGER.trace("Fetching file from vision bot through file manager service");

      // get file stream data
      fileManagerAdapter.getSpecificFileFromVisionBot(organizationsId, projectId, fileId);

      byte[] fileBlob = fileManagerAdapter.downloadFile(organizationsId, projectId, fileId);

      // get export file path
      String filePath = invalidFilePathProvider.getInvalidFilePath(fileId);

      Path javaPath = Paths.get(filePath);

      synchronized (this) {
        try {
          fileWriter.write(javaPath, fileBlob, true);

        } catch (IOException ex) {
          throw new RuntimeException("Unable to save invalid file on '" + filePath + "'", ex);
        }

        // audit mark this activity
        fileExportActivityFinishAuditor.auditExportActivity(fileId, true);
      }

    } finally {
      LOGGER.exit();
    }
  }
}
