package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class ClassificationFieldDetails {

  @JsonProperty(value = "fieldId")
  private String fieldid;

  @JsonProperty(value = "representationPercent")
  private double representationPercent;

  public String getFieldid() {
    return fieldid;
  }

  public void setFieldid(String fieldid) {
    this.fieldid = fieldid;
  }

  public double getRepresentationPercent() {
    return representationPercent;
  }

  public void setRepresentationPercent(double representationPercent) {
    this.representationPercent = representationPercent;
  }
}