package com.automationanywhere.cognitive.validator.impl;

/**
 * Created by keval.sheth on 29-11-2016.
 */
public class DatabaseConnectionException extends RuntimeException {

  private static final long serialVersionUID = -4187689645828096767L;

  public DatabaseConnectionException() {
  }

  public DatabaseConnectionException(String message, Throwable cause) {
    super(message, cause);
  }
}