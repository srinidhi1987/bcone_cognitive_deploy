package com.automationanywhere.cognitive.validator.hibernate.attributeconverters;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.automationanywhere.cognitive.validator.exception.customexceptions.InputStreamConversionException;

public class InputStreamConverter {

  private static final int BYTE_BUFFER_SIZE = 1024;

  /**
   * @return String converted from inputStream
   */
  public String convertInputStreamToString(final InputStream inputStream) {
    StringBuilder stringBuilder = new StringBuilder();
    try (BufferedReader bufferedStreamReader = new BufferedReader(
        new InputStreamReader(inputStream))) {
      String line;

      while ((line = bufferedStreamReader.readLine()) != null) {
        stringBuilder.append(line);
      }
    } catch (IOException ioe) {
      throw new InputStreamConversionException("Error while converting InputStream to String", ioe);
    }
    return stringBuilder.toString();
  }

  /**
   * @return byte array converted from inputStream
   */
  public byte[] convertInputStreamToByteArray(final InputStream inputStream) {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    byte[] byteArray = null;
    try {
      int nRead;
      byte[] data = new byte[BYTE_BUFFER_SIZE];
      while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
        buffer.write(data, 0, nRead);
      }

      buffer.flush();
      byteArray = buffer.toByteArray();
    } catch (IOException ioe) {
      throw new InputStreamConversionException(
          "Error while converting the input stream into byte array.", ioe);
    }
    return byteArray;
  }
}
