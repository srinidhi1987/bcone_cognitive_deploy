package com.automationanywhere.cognitive.exporter.impl;

import static com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer.VALIDATOR_SERVICE_DATA_ACCESS_LAYER;

import com.automationanywhere.cognitive.exporter.contracts.FileExportActivityFinishAuditor;
import com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer;
import com.automationanywhere.cognitive.validator.datalayer.ValidatorDetailProvider;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
@Component
public class FileExportActivityFinishAuditorImpl implements FileExportActivityFinishAuditor {

  private IValidatorServiceDataAccessLayer validatorServiceDataAccessLayer;
  private ValidatorDetailProvider validatorDetailProvider;

  @Autowired
  public FileExportActivityFinishAuditorImpl(
      @Qualifier(VALIDATOR_SERVICE_DATA_ACCESS_LAYER) final IValidatorServiceDataAccessLayer validatorServiceDataAccessLayer,
      final ValidatorDetailProvider validatorDetailProvider
  ) {
    this.validatorServiceDataAccessLayer = validatorServiceDataAccessLayer;
    this.validatorDetailProvider = validatorDetailProvider;
  }

  public void auditExportActivity(String fileId, Boolean isSuccessful) {
    if (!isSuccessful) {
      return;
    }
    VisionBotDataDetail validatorData = validatorDetailProvider.getDocumentByFileId(fileId);
    validatorData.setLastExportTime(new Date());
    validatorServiceDataAccessLayer.update(validatorData);
  }
}
