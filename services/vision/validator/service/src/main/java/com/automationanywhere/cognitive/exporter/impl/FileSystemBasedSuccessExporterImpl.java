package com.automationanywhere.cognitive.exporter.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.exporter.contracts.CsvDataGenerator;
import com.automationanywhere.cognitive.exporter.contracts.FileExportActivityFinishAuditor;
import com.automationanywhere.cognitive.exporter.contracts.FileWriter;
import com.automationanywhere.cognitive.exporter.contracts.SuccessExporter;
import com.automationanywhere.cognitive.exporter.contracts.SuccessFilePathProvider;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
@Component
public class FileSystemBasedSuccessExporterImpl implements SuccessExporter {

	private static final AALogger LOGGER = AALogger.create(FileSystemBasedSuccessExporterImpl.class);

	private final CsvDataGenerator csvDataGenerator;
	private SuccessFilePathProvider successFilePathProvider;
	private FileWriter fileWriter;
	private FileExportActivityFinishAuditor fileExportActivityFinishAuditor;

	@Autowired
  public FileSystemBasedSuccessExporterImpl(
      final CsvDataGenerator csvDataGenerator,
      final SuccessFilePathProvider successFilePathProvider,
      final FileWriter fileWriter,
      final FileExportActivityFinishAuditor fileExportActivityFinishAuditor
  ) {
		this.csvDataGenerator = csvDataGenerator;
		this.successFilePathProvider = successFilePathProvider;
		this.fileWriter = fileWriter;
		this.fileExportActivityFinishAuditor = fileExportActivityFinishAuditor;
	}

	public void exportSuccessFile(String fileId) {
		synchronized (this) {
			try {
				LOGGER.entry();
				LOGGER.debug(() -> "FileId" + fileId);

				// get csv data
				String csvData = csvDataGenerator.GetExportCsv(fileId);
				if (csvData == null) {
					throw new RuntimeException("Unable to generate csv from vbotdocument.");
				}
				byte[] csvBytes = csvData.getBytes();

				// get export file path
				String filePath = successFilePathProvider.getSuccessFilePath(fileId);

				Path javaPath = Paths.get(filePath);

				try {
					fileWriter.WriteCsv(javaPath, csvBytes, true);
				} catch (IOException ex) {
					LOGGER.error(ex.getMessage());
					throw new RuntimeException("Unable to save csv on '" + filePath + "'");
				}

				// audit mark this activity
				fileExportActivityFinishAuditor.auditExportActivity(fileId, true);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage(), ex);
				throw ex;
			}

			finally {
				LOGGER.exit();
			}
		}
	}
}
