package com.automationanywhere.cognitive.validator.exception;

public class DatabaseException extends RuntimeException {

  private static final long serialVersionUID = -1466210260094425937L;

  public DatabaseException(String message) {
    super(message);
  }

  public DatabaseException(String message, Throwable cause) {
    super(message, cause);
  }
}

