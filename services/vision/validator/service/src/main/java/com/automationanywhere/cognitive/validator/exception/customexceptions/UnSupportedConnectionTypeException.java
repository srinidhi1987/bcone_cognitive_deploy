package com.automationanywhere.cognitive.validator.exception.customexceptions;

public class UnSupportedConnectionTypeException extends RuntimeException {

  private static final long serialVersionUID = 5375490859568426125L;

  public UnSupportedConnectionTypeException(String message) {
    super(message);
  }

}