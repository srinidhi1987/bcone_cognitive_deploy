package com.automationanywhere.cognitive.validator.service.model;

public final class FieldLevelAccuracy {

  private final String fileId;
  private final String fieldId;
  private final String oldValue;
  private final String newValue;
  private final float passAccuracyValue;

  public FieldLevelAccuracy(
      final String fileId,
      final String fieldId,
      final String oldValue,
      final String newValue,
      final float passAccuracyValue
  ) {
    this.fileId = fileId;
    this.fieldId = fieldId;
    this.oldValue = oldValue;
    this.newValue = newValue;
    this.passAccuracyValue = passAccuracyValue;
  }

  /**
   * Returns the fileId.
   *
   * @return the value of fileId
   */
  public String getFileId() {
    return fileId;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the oldValue.
   *
   * @return the value of oldValue
   */
  public String getOldValue() {
    return oldValue;
  }

  /**
   * Returns the newValue.
   *
   * @return the value of newValue
   */
  public String getNewValue() {
    return newValue;
  }

  /**
   * Returns the passAccuracyValue.
   *
   * @return the value of passAccuracyValue
   */
  public float getPassAccuracyValue() {
    return passAccuracyValue;
  }
}
