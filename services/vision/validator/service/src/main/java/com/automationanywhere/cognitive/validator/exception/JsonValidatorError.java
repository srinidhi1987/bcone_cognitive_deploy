package com.automationanywhere.cognitive.validator.exception;

public final class JsonValidatorError {

  private final String path;
  private final String errorMessage;

  public JsonValidatorError(
      final String path,
      final String errorMessage
  ) {
    this.path = path;
    this.errorMessage = errorMessage;
  }

  /**
   * Returns the path.
   *
   * @return the value of path
   */
  public String getPath() {
    return path;
  }

  /**
   * Returns the errorMessage.
   *
   * @return the value of errorMessage
   */
  public String getErrorMessage() {
    return errorMessage;
  }

  @Override
  public String toString() {
    return "JsonValidatorError [" + path + "]: " + errorMessage;
  }
}
