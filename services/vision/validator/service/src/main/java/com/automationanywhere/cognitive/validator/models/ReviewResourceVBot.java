package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class ReviewResourceVBot {

  @JsonProperty(value = "projectId")
  private String projectId;

  @JsonProperty(value = "processedCount")
  private long processedCount;

  @JsonProperty(value = "failedFileCount")
  private long failedFileCount;

  @JsonProperty(value = "reviewFileCount")
  private long reviewFileCount;

  @JsonProperty(value = "invalidFileCount")
  private long invalidFileCount;


  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public long getProcessedCount() {
    return processedCount;
  }

  public void setProcessedCount(long processedCount) {
    this.processedCount = processedCount;
  }

  public long getFailedFileCount() {
    return failedFileCount;
  }

  public void setFailedFileCount(long failedFileCount) {
    this.failedFileCount = failedFileCount;
  }

  public long getReviewFileCount() {
    return reviewFileCount;
  }

  public void setReviewFileCount(long reviewFileCount) {
    this.reviewFileCount = reviewFileCount;
  }

  public long getInvalidFileCount() {
    return invalidFileCount;
  }

  public void setInvalidFileCount(long invalidFileCount) {
    this.invalidFileCount = invalidFileCount;
  }
}