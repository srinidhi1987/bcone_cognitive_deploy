package com.automationanywhere.cognitive.validator.impl;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static com.automationanywhere.cognitive.constants.Constants.PURGE_DATA;
import static com.automationanywhere.cognitive.logging.Constants.EXCEPTION_OCCURRED;
import static com.automationanywhere.cognitive.validator.ValidatorDetailsService.VALIDATOR_DETAILS_SERVICE_PROXY;
import static com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer.VALIDATOR_SERVICE_DATA_ACCESS_LAYER;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.exporter.impl.FileSystemBasedInvalidExporterImpl;
import com.automationanywhere.cognitive.exporter.impl.FileSystemBasedSuccessExporterImpl;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.ValidationType;
import com.automationanywhere.cognitive.validator.ValidatorDetailsService;
import com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer;
import com.automationanywhere.cognitive.validator.datalayer.ValidatorDetailProvider;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.models.FieldAccuracyDetails;
import com.automationanywhere.cognitive.validator.models.InvalidFileTypes;
import com.automationanywhere.cognitive.validator.models.InvalidFiles;
import com.automationanywhere.cognitive.validator.models.ValidatorOperationTypeEnum;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import com.automationanywhere.cognitive.validator.repositories.MessageQueueRepository;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 * Created by keval.sheth on 29-11-2016.
 */
@Component(VALIDATOR_DETAILS_SERVICE_PROXY)
public class ValidatorServiceImpl implements ValidatorDetailsService {

  private static final AALogger LOGGER = AALogger.create(ValidatorServiceImpl.class);

  private final MessageSource messageSource;
  private final MessageQueueRepository queueRepository;
  private final FileSystemBasedInvalidExporterImpl invalidExporter;
  private final FileSystemBasedSuccessExporterImpl successExporter;
  private final IValidatorServiceDataAccessLayer newVisionBotServiceDataAccessLayer;
  private final ValidatorDetailProvider detailProvider;

  @Autowired
  public ValidatorServiceImpl(
      @Qualifier(MESSAGE_SOURCE) final MessageSource messageSource,
      final MessageQueueRepository queueRepository,
      final FileSystemBasedInvalidExporterImpl invalidExporter,
      final FileSystemBasedSuccessExporterImpl successExporter,
      @Qualifier(VALIDATOR_SERVICE_DATA_ACCESS_LAYER) final IValidatorServiceDataAccessLayer newVisionBotServiceDataAccessLayer,
      final ValidatorDetailProvider detailProvider
  ) {
    this.messageSource = messageSource;
    this.queueRepository = queueRepository;
    this.invalidExporter = invalidExporter;
    this.successExporter = successExporter;
    this.newVisionBotServiceDataAccessLayer = newVisionBotServiceDataAccessLayer;
    this.detailProvider = detailProvider;
  }

  private String getFileName(Part part) {
    for (String cd : part.getHeader("content-disposition").split(";")) {
      if (cd.trim().startsWith("filename")) {
        return cd.substring(cd.indexOf('=') + 1).trim()
            .replace("\"", "");
      }
    }
    return null;
  }

  public boolean checkInvalidParametersForProjectId(String projectid) {
    try {
      return detailProvider.checkForInvalidProjectId(projectid);
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }

  }

  public boolean checkInvalidParametersForOrganizationId(String orgid) {
    try {
      return detailProvider.checkForInvalidOrganizationId(orgid);
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }

  }

  public boolean checkInvalidParametersForFileId(String fileid) {
    try {
      return detailProvider.checkForInvalidFileId(fileid);
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }

  }


  @Override
  public VisionBotDataDetail getFailedVBot(String orgid, String projectid, String username) {
      try {
          VisionBotDataDetail result = detailProvider.getFailedDocumentByUser(orgid, projectid, username);
          Date dt = new Date(0);
          String fileId = "";

          if (result != null) {
              fileId = result.getFileId();
              UnlockVBot(result);
              dt = result.getCreatedAt();
          }

          result = detailProvider.getFailedDocumentNotAllocatedToAnyUser(orgid, projectid, dt, fileId);
          if (result != null) {
              LockVBot(result, username);
              return result;
          }
          return null;
      } catch (DatabaseConnectionException ex) {
          throw ex;
      }

  }

  @Override
  public boolean addVisionBot(String fileid, String orgid, String prjid, String validationstatus,
      String visionbotid, FieldAccuracyDetails[] fieldAccuracyDetails, String data, float failed,
			float pass, float total) {
		try {
			LOGGER.entry();
			VisionBotDataDetail visionbotData = new VisionBotDataDetail();
			// response.status(201);
			visionbotData.setOrgId(orgid);
			visionbotData.setFileId(fileid);
			visionbotData.setProjectId(prjid);
			visionbotData.setVisionBotId(visionbotid);
			visionbotData.setFailedFieldCounts(failed);
			visionbotData.setPassFieldCounts(pass);
			visionbotData.setTotalFieldCounts(total);
			if (validationstatus.equalsIgnoreCase(ValidationType.Pass.toString())) {
				visionbotData.setVBotData("");
				visionbotData.setCorrectedData(data);
				visionbotData.setValidationStatus(ValidationType.Pass.toString());
			} else if (validationstatus.equalsIgnoreCase(ValidationType.Fail.toString())) {
				visionbotData.setCorrectedData("");
				visionbotData.setVBotData(data);
				visionbotData.setValidationStatus(ValidationType.Fail.toString());
			}
			visionbotData.setCreatedAt(new Date());
			visionbotData.setUpdatedBy("admin");
			// visionbotData.setVBotInUse(false);
			visionbotData.setUpdatedAt(new Date());
			visionbotData.setLockedUserId("");

			boolean success = newVisionBotServiceDataAccessLayer.Insert(visionbotData);

			if (validationstatus.equalsIgnoreCase(ValidationType.Pass.toString())) {
				exportSuccessDocumentData(fileid);
			} else if (validationstatus.equalsIgnoreCase(ValidationType.Invalid.toString())) {
				exportInvalidDocumentData(fileid);
			}

			for (int i = 0; i < fieldAccuracyDetails.length; i++)
				newVisionBotServiceDataAccessLayer.Insert(fieldAccuracyDetails[i]);
			return true;
		} catch (DatabaseConnectionException ex) {
			throw ex;
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
			return false;
		} finally {
			LOGGER.exit();
		}
	}

  private void exportInvalidDocumentData(String fileid) {
    try {
      invalidExporter.exportInvalidFile(fileid);
      purgeSecureData(fileid);
    } catch (Exception ex) {
      throw ex;
    }
  }

  @Override
  public int getFailedDocumentCountsByProject(String organizationId, String projectId) {
    try {
      return detailProvider.getFailedDocumentCountsByProject(organizationId, projectId);
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }

  @Override
  public List<Object> getTimeSpend(String id) {
    try {
      VisionBotDataDetail f1 = new VisionBotDataDetail();
      // f1.setProjectId(projectid);
      // f1.setLockeduserid(username);
      List<Object> VisionBotDataList = newVisionBotServiceDataAccessLayer
          .ExecuteCustomQuery("ValidationTimeSpendCategorywise", null, id, f1);//,dt, type);

      return VisionBotDataList;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }

  @Override
  public List<Object> getFieldAccuracyAndFilesValidated(String id) {
    try {
      VisionBotDataDetail f1 = new VisionBotDataDetail();
      // f1.setProjectId(projectid);
      // f1.setLockeduserid(username);
      List<Object> VisionBotDataList = newVisionBotServiceDataAccessLayer
          .ExecuteCustomQuery("FieldAccuracyAndValidatedFiles", null, id, f1);//,dt, type);

      return VisionBotDataList;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }

  @Override
  public List<Object> getProjectTotalsWithAccuracyDetails(String idtype, String id) {
    try {
      VisionBotDataDetail f1 = new VisionBotDataDetail();
      // f1.setProjectId(projectid);
      // f1.setLockeduserid(username);
      List<Object> VisionBotDataList = newVisionBotServiceDataAccessLayer
          .ExecuteCustomQuery("Dashboard", idtype, id, f1);//,dt, type);

      return VisionBotDataList;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }

  @Override
  public List<Object> getDocumentProcessingDetails(String idtype, String id) {
    try {
      VisionBotDataDetail f1 = new VisionBotDataDetail();
      // f1.setProjectId(projectid);
      // f1.setLockeduserid(username);
      List<Object> VisionBotDataList = newVisionBotServiceDataAccessLayer
          .ExecuteCustomQuery("AccuracyDetails", idtype, id, f1);//,dt, type);

      return VisionBotDataList;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }

  @Override
  public List<Object> getReviewedVBotCount() {
    try {
      VisionBotDataDetail f1 = new VisionBotDataDetail();
      // f1.setProjectId(projectid);
      // f1.setLockeduserid(username);
      List<Object> VisionBotDataList = newVisionBotServiceDataAccessLayer
          .ExecuteCustomQuery("Review", null, null, f1);//,dt, type);

      return VisionBotDataList;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }

  private String getHostName() throws UnknownHostException {
    InetAddress address;
    address = InetAddress.getLocalHost();
    return address.getHostName();
  }

  @Override
  public List<InvalidFileTypes> getInvalidMarkTypes() {
    try {
      InvalidFileTypes i1 = new InvalidFileTypes();
      List<InvalidFileTypes> invalidFileTypes = detailProvider.getInvalidMarkTypes();
      return invalidFileTypes;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }

  }

  @Override
  public boolean insertRow(InvalidFiles invalidFiles) {
    try {
      boolean success = newVisionBotServiceDataAccessLayer.Insert(invalidFiles);
      return success;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    }
  }


  @Override
  public boolean updateResourceUserId(String projectId, String fileid, String userid,
      ValidatorOperationTypeEnum type) {
    try {

      VisionBotDataDetail visionBotDataDetail = detailProvider
          .getDocumentByProjectId(projectId, userid);

      LOGGER.trace(
          () -> Constants.VALIDATOR_LOG_DETAIL + " Projectid: " + projectId + " ,FileID: " + fileid
              + " ,validator is locked by:" + visionBotDataDetail.getLockedUserId()
              + " And your userid is " + userid);
      if (visionBotDataDetail.getLockedUserId() != null && visionBotDataDetail.getLockedUserId()
          .equalsIgnoreCase(userid)) {
        visionBotDataDetail.setLockedUserId("");
        newVisionBotServiceDataAccessLayer.update(visionBotDataDetail);
        LOGGER.trace(
            () -> Constants.VALIDATOR_LOG_DETAIL + "Lock is released for projectId:" + projectId
                + " And fileId: " + fileid);//.getLockedUserId()+" And your userid is "+userid );
        return true;
      }

      return false;
    } catch (final DatabaseConnectionException ex) {
      LOGGER.error(() -> "Database exception");//.getLockedUserId()+" And your userid is "+userid );
      throw ex;
    } catch (final Exception ex) {
      LOGGER.error(() -> "General exception", ex);
      return false;
    }
  }


  @Override
  public boolean updateResourceCorrectedValue(String fileid, String projectId, String username,
      String correctedValue, float accuracyValue, FieldAccuracyDetails[] details,
      ValidatorOperationTypeEnum type) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date date = new Date();
    boolean isSuccess = false;
    try {
      //  VisionBotData fileDetail = new VisionBotData();
      // ValidatorDetailProvider detailProvider = new ValidatorDetailProvider(newVisionBotServiceDataAccessLayer);
      VisionBotDataDetail visionBotDataDetail = detailProvider.getDocumentByFileId(fileid);
      // float previousPassFieldCount = visionBotDataDetail.getPassFieldCounts();
      // float previousFailedFieldCount = visionBotDataDetail.getFailedFieldCounts();
      if (visionBotDataDetail != null && visionBotDataDetail.getLockedUserId()
          .equalsIgnoreCase(username) && (type.name()
          .equalsIgnoreCase(ValidatorOperationTypeEnum.Save.toString()))) {
        visionBotDataDetail.setCorrectedData(correctedValue);
        visionBotDataDetail.setUpdatedBy(username);
        visionBotDataDetail.setLockedUserId("");
        visionBotDataDetail.setValidationStatus(ValidationType.Pass.toString());
        visionBotDataDetail.setFailedFieldCounts(details.length - accuracyValue);
        visionBotDataDetail.setPassFieldCounts(accuracyValue);
        visionBotDataDetail.setTotalFieldCounts(details.length);
        visionBotDataDetail.setUpdatedAt(new Date());
        Date startDate = visionBotDataDetail.getStartTime();
        Date currentDate = new Date();
        long diffDateInSeconds = 0;
        if (startDate != null) {
          long diffDate = currentDate.getTime() - startDate.getTime();
          diffDateInSeconds = diffDate / 1000;
        }
        visionBotDataDetail.setAverageTimeInSeconds(diffDateInSeconds);
        visionBotDataDetail.setEndTime(new Date());
        newVisionBotServiceDataAccessLayer.update(visionBotDataDetail);
        LOGGER.trace("Successfully updated vision bot data detail");
        LOGGER.auditlog(() -> {
          String result;
          try {
            result =
                Constants.VALIDATOR_LOG_DETAIL + "\t " + username + "\t " + getHostName() + "\t "
                    + dateFormat.format(date) + "Success: Document validated successfully.";
          } catch (final UnknownHostException ex) {
            LOGGER.error("Failed to determine the host name", ex);
            result = null;
          }
          return result;
        });

        exportSuccessDocumentData(fileid);

        for (int i = 0; i < details.length; i++) {
          newVisionBotServiceDataAccessLayer.Insert(details[i]);
        }
        isSuccess = true;
      } else {
        throw new DataNotFoundException(messageSource
            .getMessage(Constants.PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE, null,
                Locale.getDefault()));
      }

      return isSuccess;
    } catch (DataNotFoundException dnfe) {
      throw dnfe;
    } catch (Exception e) {
      LOGGER.error(messageSource.getMessage(EXCEPTION_OCCURRED, null, Locale.getDefault()),
          e);
      LOGGER.auditlog(() -> {
        try {
          return Constants.VALIDATOR_LOG_DETAIL_ERROR + "\t " + username + "\t " + getHostName()
              + "\t " + dateFormat.format(date) + "Failed: Document validation session failure.<"
              + e.toString() + ">";
        } catch (UnknownHostException ex) {
          LOGGER.error(messageSource.getMessage(EXCEPTION_OCCURRED,
              new Object[]{"Exception occured in audit LOGGER"}, Locale.getDefault()), e);
          return false;
        }
      });
      return false;
    }
  }

  public List<Object> getProductionFileSummary(String projectId) {
    VisionBotDataDetail f1 = new VisionBotDataDetail();
    List<Object> productionFileSummary = newVisionBotServiceDataAccessLayer
        .ExecuteCustomQuery("ProductionSummaryDetails", null, projectId, f1);
    return productionFileSummary;

  }

  private void exportSuccessDocumentData(String fileId) {
    successExporter.exportSuccessFile(fileId);
    purgeSecureData(fileId);
  }

  private boolean UnlockVBot(VisionBotDataDetail result) {
    try {
      result.setLockedUserId("");
      result.setStartTime(new Date());
      newVisionBotServiceDataAccessLayer.update(result);
      LOGGER.trace("Successfully unlock the vision bot");
      return true;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    } catch (Exception ex) {
      LOGGER.error(messageSource.getMessage(EXCEPTION_OCCURRED, null, Locale.getDefault()),
          ex);
      return false;
    }
  }

  private boolean LockVBot(VisionBotDataDetail result, String username) {
    try {
      result.setLockedUserId(username);
      result.setStartTime(new Date());
      newVisionBotServiceDataAccessLayer.update(result);
      LOGGER.trace("Successfully locked the vision bot");
      return true;
    } catch (DatabaseConnectionException ex) {
      throw ex;
    } catch (Exception ex) {
      LOGGER.error(messageSource.getMessage(EXCEPTION_OCCURRED, null, Locale.getDefault()),
          ex);
      return false;
    }

  }

  private void purgeSecureData(String fileId) {
    queueRepository.broadcastMessageToPurgeData(fileId);
    purgeVBotDocumentData(fileId);
  }

  private void purgeVBotDocumentData(String fileId) {
    try {
      VisionBotDataDetail visionBotDataDetail = detailProvider.getDocumentByFileId(fileId);
      if (visionBotDataDetail != null && !visionBotDataDetail.getValidationStatus()
          .equalsIgnoreCase(ValidationType.Fail.toString())) {

        if (!visionBotDataDetail.getVBotData().equals("")) {
          visionBotDataDetail.setVBotData(PURGE_DATA);
        }
        if (!visionBotDataDetail.getCorrectedData().equals("")) {
          visionBotDataDetail.setCorrectedData(PURGE_DATA);
        }

        newVisionBotServiceDataAccessLayer.update(visionBotDataDetail);
      }

    } catch (Exception ex) {
      LOGGER.error(messageSource.getMessage(EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
    }
  }
}