package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class ProjectTotalsWithAccuracyDetails {

  @JsonProperty(value = "totalFilesProcessed")
  private int totalFilesProcessed = 0;

  @JsonProperty(value = "totalSTP")
  private double totalSTP = 0;

  @JsonProperty(value = "totalAccuracy")
  private double totalAccuracy = 0;

  @JsonProperty(value = "totalFilesUploaded")
  private int totalFilesUploaded = 0;

  @JsonProperty(value = "totalFilesToValidation")
  private int totalFilesToValidation = 0;

  @JsonProperty(value = "totalFilesValidated")
  private int totalFilesValidated = 0;

  @JsonProperty(value = "totalFilesUnprocessable")
  private int totalFilesUnprocessable = 0;

  @JsonProperty(value = "classification")
  private List<ClassificationFieldDetails> classificationFieldDetails = new ArrayList<>();

  @JsonProperty(value = "accuracy")
  private List<FieldAccuracyDashboardDetails> fieldAccuracy = new ArrayList<>();

  @JsonProperty(value = "validation")
  private List<FieldValidation> fieldValidation = new ArrayList<>();

  @JsonProperty(value = "categories")
  private List<GroupTimeSpentDetails> groupTimeSpentDetails = new ArrayList<>();

  public List<GroupTimeSpentDetails> getGroupTimeSpentDetails() {
    return groupTimeSpentDetails;
  }

  public void setGroupTimeSpentDetails(List<GroupTimeSpentDetails> groupTimeSpentDetails) {
    this.groupTimeSpentDetails = groupTimeSpentDetails;
  }

  public int getTotalFilesProcessed() {
    return totalFilesProcessed;
  }

  public void setTotalFilesProcessed(int totalFilesProcessed) {
    this.totalFilesProcessed = totalFilesProcessed;
  }

  public double getTotalSTP() {
    return totalSTP;
  }

  public void setTotalSTP(double totalSTP) {
    this.totalSTP = totalSTP;
  }

  public double getTotalAccuracy() {
    return totalAccuracy;
  }

  public void setTotalAccuracy(double totalAccuracy) {
    this.totalAccuracy = totalAccuracy;
  }

  public List<ClassificationFieldDetails> getClassificationFieldDetails() {
    return classificationFieldDetails;
  }

  public void setClassificationFieldDetails(
      List<ClassificationFieldDetails> classificationFieldDetails) {
    this.classificationFieldDetails = classificationFieldDetails;
  }

  public int getTotalFilesUploaded() {
    return totalFilesUploaded;
  }

  public void setTotalFilesUploaded(int totalFilesUploaded) {
    this.totalFilesUploaded = totalFilesUploaded;
  }

  public int getTotalFilesToValidation() {
    return totalFilesToValidation;
  }

  public void setTotalFilesToValidation(int totalFilesToValidation) {
    this.totalFilesToValidation = totalFilesToValidation;
  }

  public int getTotalFilesValidated() {
    return totalFilesValidated;
  }

  public void setTotalFilesValidated(int totalFilesValidated) {
    this.totalFilesValidated = totalFilesValidated;
  }

  public int getTotalFilesUnprocessable() {
    return totalFilesUnprocessable;
  }

  public void setTotalFilesUnprocessable(int totalFilesUnprocessable) {
    this.totalFilesUnprocessable = totalFilesUnprocessable;
  }

  public List<FieldAccuracyDashboardDetails> getFieldAccuracy() {
    return fieldAccuracy;
  }

  public void setFieldAccuracy(List<FieldAccuracyDashboardDetails> fieldAccuracy) {
    this.fieldAccuracy = fieldAccuracy;
  }

  public List<FieldValidation> getFieldValidation() {
    return fieldValidation;
  }

  public void setFieldValidation(List<FieldValidation> fieldValidation) {
    this.fieldValidation = fieldValidation;
  }
}