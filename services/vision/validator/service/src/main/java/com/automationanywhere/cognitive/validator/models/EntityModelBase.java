package com.automationanywhere.cognitive.validator.models;

import java.util.Map;

/**
 * Created by Mayur.Panchal on 11-12-2016.
 */
public abstract class EntityModelBase {

  public abstract Map<String, Object> getCustomQuery(StringBuilder queryString, String queryId,
      String idtype, String id);

  public abstract Map<String, Object> getCustomQueryForDistinctElements(StringBuilder queryString,
      String parameter);
}
