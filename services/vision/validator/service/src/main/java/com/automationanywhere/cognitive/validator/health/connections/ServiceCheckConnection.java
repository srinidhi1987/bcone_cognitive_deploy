package com.automationanywhere.cognitive.validator.health.connections;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter;
import com.automationanywhere.cognitive.generic.contracts.ProjectManagerAdapter;
import java.util.ArrayList;
import java.util.List;

public class ServiceCheckConnection implements HealthCheckConnection {

  FileManagerAdapter fileManagerAdapter;
  ProjectManagerAdapter projectManagerAdapter;

  AALogger aaLogger = AALogger.create(this.getClass());

  public ServiceCheckConnection(FileManagerAdapter fileManagerAdapter,
      ProjectManagerAdapter projectManagerAdapter) {
    super();
    this.fileManagerAdapter = fileManagerAdapter;
    this.projectManagerAdapter = projectManagerAdapter;
  }

  @Override
  public void checkConnection() {

  }

  @Override
  public List<ServiceConnectivity> checkConnectionForService() {
    aaLogger.entry();
    List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity dependentService = null;
    for (DependentServices de : DependentServices.values()) {
      try {
        dependentService = new ServiceConnectivity(de.toString(), HTTPStatusCode.OK);
        switch (de) {
          case FileManager:
            fileManagerAdapter.testConnection();
            break;
          case Project:
            projectManagerAdapter.testConnection();
            break;
        }
      } catch (Exception ex) {
        prepareDependentServiceList(dependentServices, dependentService, de,
            HTTPStatusCode.INTERNAL_SERVER_ERROR);
      }
      if (!dependentServices.contains(dependentService)) {
        prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.OK);
      }
    }
    return dependentServices;
  }

  private void prepareDependentServiceList(
      List<ServiceConnectivity> dependentServices,
      ServiceConnectivity dependentService, DependentServices de, HTTPStatusCode mode) {
    dependentService.setHTTPStatus(mode);
    dependentServices.add(dependentService);
  }
}
