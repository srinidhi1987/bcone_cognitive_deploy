package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvalidFileDto {

  public final String fileId;
  public final String reasonId;
  public final String reasonText;

  public InvalidFileDto(
      @JsonProperty("fileId") final String fileId,
      @JsonProperty("reasonId") final String reasonId,
      @JsonProperty("reasonText") final String reasonText
  ) {
    this.fileId = fileId;
    this.reasonId = reasonId;
    this.reasonText = reasonText;
  }
}
