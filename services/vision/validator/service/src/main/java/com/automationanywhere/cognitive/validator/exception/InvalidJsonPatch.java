package com.automationanywhere.cognitive.validator.exception;

public class InvalidJsonPatch extends RuntimeException {

  private static final long serialVersionUID = 2935298787133182844L;

  public InvalidJsonPatch(String message) {
    super(message);
  }

  public InvalidJsonPatch(String message, Throwable cause) {
    super(message, cause);
  }
}
