package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class FieldLevelAccuracyDto {

  private final String fileId;
  private final String fieldId;
  private final String oldValue;
  private final String newValue;
  private final float passAccuracyValue;

  public FieldLevelAccuracyDto(
      @JsonProperty("fileId") final String fileId,
      @JsonProperty("fieldId") final String fieldId,
      @JsonProperty("oldValue") final String oldValue,
      @JsonProperty("newValue") final String newValue,
      @JsonProperty("passAccuracyValue") final float passAccuracyValue
  ) {
    this.fileId = fileId;
    this.fieldId = fieldId;
    this.oldValue = oldValue;
    this.newValue = newValue;
    this.passAccuracyValue = passAccuracyValue;
  }

  /**
   * Returns the fileId.
   *
   * @return the value of fileId
   */
  @JsonProperty("fileId")
  public String getFileId() {
    return fileId;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  @JsonProperty("fieldId")
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the oldValue.
   *
   * @return the value of oldValue
   */
  @JsonProperty("oldValue")
  public String getOldValue() {
    return oldValue;
  }

  /**
   * Returns the newValue.
   *
   * @return the value of newValue
   */
  @JsonProperty("newValue")
  public String getNewValue() {
    return newValue;
  }

  /**
   * Returns the passAccuracyValue.
   *
   * @return the value of passAccuracyValue
   */
  @JsonProperty("passAccuracyValue")
  public float getPassAccuracyValue() {
    return passAccuracyValue;
  }
}
