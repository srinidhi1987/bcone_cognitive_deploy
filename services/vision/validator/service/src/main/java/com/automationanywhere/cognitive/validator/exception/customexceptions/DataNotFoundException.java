package com.automationanywhere.cognitive.validator.exception.customexceptions;

/**
 * Created by Shweta.Thakur on 05-07-2017
 */
public class DataNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 3286982303174951076L;

  public DataNotFoundException(String message) {
    super(message);
  }
}