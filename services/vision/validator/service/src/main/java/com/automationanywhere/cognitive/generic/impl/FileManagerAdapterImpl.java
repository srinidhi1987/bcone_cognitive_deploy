package com.automationanywhere.cognitive.generic.impl;

import static com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter.FILE_MANAGER_ADAPTER_PROXY;
import static com.automationanywhere.cognitive.logging.Constants.HEADER_CORRELATION_ID;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter;
import com.automationanywhere.cognitive.generic.models.FileDetail;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.validator.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.validator.models.StandardResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component(FILE_MANAGER_ADAPTER_PROXY)
public class FileManagerAdapterImpl implements FileManagerAdapter {

  private static final AALogger LOGGER = AALogger.create(FileManagerAdapterImpl.class);
  private static final String HEARTBEAT_URL = "/heartbeat";

  private final String rootUrl;
  private final RestTemplate restTemplate;
  private final RestTemplateWrapper restTemplateWrapper;


  @Autowired
  public FileManagerAdapterImpl(
      @Qualifier(FILE_MANAGER_SERVICE_ROOT_URL) final String rootUrl,
      final RestTemplate restTemplate,
      final RestTemplateWrapper restTemplateWrapper
  ) {
    this.rootUrl = rootUrl;
    this.restTemplate = restTemplate;
    this.restTemplateWrapper = restTemplateWrapper;

  }

  @Override
  public ClassificationAnalysisReport getAllFileStats(String projId) {
    try {
      String relativeUrl = "organizations/1/projects/" + projId + "/report";
      LOGGER.entry();
      String url = rootUrl + relativeUrl;//String.format(relativeUrl,projId);
      LOGGER.debug("File manager service url" + url);
      ClassificationAnalysisReport countStatistics = null;
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      try {
        HttpEntity<String> entity = addHeaderDeatils();
        ResponseEntity<StandardResponse> standardResponse = restTemplate
            .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
        countStatistics = objectMapper
            .convertValue(standardResponse.getBody().getData(), ClassificationAnalysisReport.class);

      } catch (Exception ex) {
        LOGGER.error("Error to get all file stats of project ID: " + projId, ex);
        return null;
      }
      return countStatistics;

    } finally {
      LOGGER.exit();
    }
  }

  @Override
  public FileDetail getSpecificFileFromVisionBot(
      final String organizationId,
      final String projectId,
      final String fileId
  ) {
    try {
      LOGGER.entry();
      String relativeUrl = "organizations/%s/projects/%s/files/%s";
      String url = rootUrl + String.format(relativeUrl, organizationId, projectId, fileId);
      LOGGER.debug("File manager service url" + url);
      FileDetail[] fileDetails;
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      HttpEntity<String> entity = addHeaderDeatils();
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);

      try {
        fileDetails = objectMapper.readValue(objectMapper.writerWithDefaultPrettyPrinter()
            .writeValueAsString(standardResponse.getBody().getData()), FileDetail[].class);

      } catch (Exception ex) {
        throw new RuntimeException("Unable to parse data.", ex);
      }

      if (fileDetails == null || fileDetails.length == 0) {
        throw new RuntimeException("File is not available on server.");
      }

      return fileDetails[0];

    } finally {
      LOGGER.exit();
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public byte[] downloadFile(String organizationId, String projectId, String fileId) {
    try {
      LOGGER.entry();
      String relativeUrl = "organizations/%s/projects/%s/fileblob/%s";
      String url = rootUrl + String.format(relativeUrl, organizationId, projectId, fileId);
      LOGGER.debug("File manager service url" + url);
      ArrayList<HashMap<String, Object>> servieResponse;
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      HttpEntity<String> entity = addHeaderDeatils();
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);

      ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
      try {
        servieResponse = objectMapper.readValue(
            objectWriter.writeValueAsString(standardResponse.getBody().getData()), ArrayList.class);

      } catch (Exception ex) {
        throw new RuntimeException("Unable to parse data.", ex);
      }
      //--------------------------------
      if (servieResponse == null || servieResponse.size() == 0) {
        throw new RuntimeException(
            String.format("Failed to retrieve file from server with file id %s.", fileId));
      }
      HashMap<String, Object> fileDataMap = servieResponse.get(0);

      if (fileDataMap == null || !fileDataMap.containsKey("fileBlob")) {
        throw new RuntimeException(
            String.format("Failed to retrieve file from server with file id %s.", fileId));
      }

      Object fileBlob = fileDataMap.get("fileBlob");

      byte[] data;
      try {
        data = objectMapper.readValue(objectWriter.writeValueAsString(fileBlob), byte[].class);

      } catch (Exception ex) {
        throw new RuntimeException("Unable to parse data.", ex);
      }

//        if (fileData == null || fileData.equals("")) {
//            throw new RuntimeException(String.format("Failed to retrive file from server with file id %s.", fileId));
//        }
//
//        byte[] data = fileData.getBytes();

      if (data == null || data.length == 0) {
        throw new RuntimeException(
            String.format("Failed to retrive file from server with file id %s.", fileId));
      }
      return data;

    } finally {
      LOGGER.exit();
    }
  }

  private HttpEntity<String> addHeaderDeatils() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HEADER_CORRELATION_ID, ThreadContext.get(HEADER_CORRELATION_ID));
    return new HttpEntity<String>("parameters", headers);
  }


  @SuppressWarnings("unchecked")
  @Override
  public void testConnection() {
    try {
      LOGGER.entry();
      String heartBeatURL = rootUrl + HEARTBEAT_URL;
      ResponseEntity<String> standardResponse;
      standardResponse = (ResponseEntity<String>) restTemplateWrapper
          .exchangeGet(heartBeatURL, String.class);

      if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
        throw new DependentServiceConnectionFailureException(
            "Error while connecting to FileManager service");
      }
    } finally {
      LOGGER.exit();
    }
  }
}
