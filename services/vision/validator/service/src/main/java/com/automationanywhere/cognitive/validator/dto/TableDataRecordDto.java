package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class TableDataRecordDto {

  private final DataModelTableDto tableDef;
  private final List<SirFieldDto> headers;
  private final List<SirFieldDto> rows;
  private final SirFieldDto field;

  public TableDataRecordDto(
      @JsonProperty("TableDef") final DataModelTableDto tableDef,
      @JsonProperty("Headers") final List<SirFieldDto> headers,
      @JsonProperty("Rows") final List<SirFieldDto> rows,
      @JsonProperty("Field") final SirFieldDto field
  ) {
    this.tableDef = tableDef;
    this.headers = headers;
    this.rows = rows;
    this.field = field;
  }

  /**
   * Returns the tableDef.
   *
   * @return the value of tableDef
   */
  @JsonProperty("TableDef")
  public DataModelTableDto getTableDef() {
    return tableDef;
  }

  /**
   * Returns the headers.
   *
   * @return the value of headers
   */
  @JsonProperty("Headers")
  public List<SirFieldDto> getHeaders() {
    return headers;
  }

  /**
   * Returns the rows.
   *
   * @return the value of rows
   */
  @JsonProperty("Rows")
  public List<SirFieldDto> getRows() {
    return rows;
  }

  /**
   * Returns the field.
   *
   * @return the value of field
   */
  @JsonProperty("Field")
  public SirFieldDto getField() {
    return field;
  }
}
