package com.automationanywhere.cognitive.validator.dto;

import static java.util.Optional.ofNullable;

import com.automationanywhere.cognitive.validator.service.model.InvalidFile;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ValidatorDtoMapper {

  public List<InvalidFile> getInvalidFiles(List<InvalidFileDto> invalidFiles) {
    return ofNullable(invalidFiles).orElse(Collections.emptyList()).stream()
        .filter(Objects::nonNull)
        .map(this::getInvalidFile)
        .collect(Collectors.toList());
  }

  public InvalidFile getInvalidFile(InvalidFileDto dto) {
    return new InvalidFile(
        dto.fileId,
        dto.reasonId,
        dto.reasonText
    );
  }
}
