package com.automationanywhere.cognitive.generic.impl;

import static com.automationanywhere.cognitive.generic.contracts.ProjectManagerAdapter.PROJECT_MANAGER_ADAPTER_PROXY;
import static com.automationanywhere.cognitive.logging.Constants.HEADER_CORRELATION_ID;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.generic.contracts.ProjectManagerAdapter;
import com.automationanywhere.cognitive.generic.models.ProjectDetail;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.validator.models.StandardResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by pooja.wani on 07-03-2017.
 */
@Component(PROJECT_MANAGER_ADAPTER_PROXY)
public class ProjectManagerAdapterImpl implements ProjectManagerAdapter {

  private static final AALogger LOGGER = AALogger.create(ProjectManagerAdapterImpl.class);
  private static final String RelativeUrlFetchAllProjectMetadata = "/organizations/%s/projects/metadata";
  private static final String HEARTBEAT_URL = "/heartbeat";
  private static final String RELATIVE_URL = "/organizations/%s/projects/%s";
  private final RestTemplate restTemplate;
  private final RestTemplateWrapper restTemplateWrapper;
  private String rootUrl;

  @Autowired
  public ProjectManagerAdapterImpl(
      @Qualifier(PROJECT_SERVICE_ROOT_URL) final String rootUrl,
      final RestTemplate restTemplate,
      final RestTemplateWrapper restTemplateWrapper
  ) {
    this.rootUrl = rootUrl;
    this.restTemplate = restTemplate;
    this.restTemplateWrapper = restTemplateWrapper;
  }

  public ProjectDetail getProjectMetaData(String organizationId, String projectId) {
    try {
      LOGGER.entry();
      String url = rootUrl + String.format(RELATIVE_URL + "/metadata", organizationId, projectId);

      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      HttpEntity<String> entity = addHeaderDeatils();
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      ProjectDetail projectDetails = objectMapper
          .convertValue(standardResponse.getBody().getData(), ProjectDetail.class);

      if (projectDetails == null) {
        throw new RuntimeException("Project is not available on server.");
      }

      return projectDetails;

    } finally {
      LOGGER.exit();
    }
  }

  private HttpEntity<String> addHeaderDeatils() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HEADER_CORRELATION_ID, ThreadContext.get(HEADER_CORRELATION_ID));
    return new HttpEntity<>("parameters", headers);
  }

  public List<ProjectDetail> getProjectMetaDataList(String organizationId) {
    try {
      LOGGER.entry();
      String url = rootUrl + String.format(RelativeUrlFetchAllProjectMetadata, organizationId);

      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

      HttpEntity<String> entity = addHeaderDeatils();
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      ProjectDetail[] projectDetails = objectMapper
          .convertValue(standardResponse.getBody().getData(), ProjectDetail[].class);

      if (projectDetails == null) {
        throw new RuntimeException("Projects are not available on server.");
      }
      return Arrays.asList(projectDetails);

    } finally {
      LOGGER.exit();
    }
  }


  @SuppressWarnings("unchecked")
  @Override
  public void testConnection() {
    try {
      LOGGER.entry();
      String heartBeatURL = rootUrl + HEARTBEAT_URL;
      ResponseEntity<String> standardResponse;
      standardResponse = (ResponseEntity<String>) restTemplateWrapper
          .exchangeGet(heartBeatURL, String.class);
      if (standardResponse != null && standardResponse.getStatusCode() != HttpStatus.OK) {
        throw new DependentServiceConnectionFailureException(
            "Error while connecting to Project service");
      }

    } finally {
      LOGGER.exit();
    }
  }
}
