package com.automationanywhere.cognitive.validator.exception.customexceptions;

public class MessageQueueConnectionCloseException extends RuntimeException {

  private static final long serialVersionUID = 7458575417512707868L;

  public MessageQueueConnectionCloseException(String message, Throwable cause) {
    super(message, cause);
  }

}