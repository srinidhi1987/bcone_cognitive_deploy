package com.automationanywhere.cognitive.validator.exception.customexceptions;

public class InputStreamConversionException extends RuntimeException {

  private static final long serialVersionUID = -6512612442890161975L;

  public InputStreamConversionException(String message, Throwable exception) {
    super(message, exception);
  }
}
