package com.automationanywhere.cognitive.validator.datalayer;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer.VALIDATOR_SERVICE_DATA_ACCESS_LAYER;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.ValidationType;
import com.automationanywhere.cognitive.validator.impl.DatabaseConnectionException;
import com.automationanywhere.cognitive.validator.models.InvalidFileTypes;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 * Created by Keval.Sheth on 07-02-2017.
 */
@Component
public class ValidatorDetailProvider {

  private static final AALogger LOGGER = AALogger.create(ValidatorDetailProvider.class);

  private final MessageSource messageSource;
  private final IValidatorServiceDataAccessLayer dataLayerProvider;

  @Autowired
  public ValidatorDetailProvider(
      @Qualifier(MESSAGE_SOURCE) final MessageSource messageSource,
      @Qualifier(VALIDATOR_SERVICE_DATA_ACCESS_LAYER) final IValidatorServiceDataAccessLayer dataLayerProvider
  ) {
    this.messageSource = messageSource;
    this.dataLayerProvider = dataLayerProvider;
  }

  public List<InvalidFileTypes> getInvalidMarkTypes() {
    try {
      InvalidFileTypes i1 = new InvalidFileTypes();
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(InvalidFileTypes.class);
      List<InvalidFileTypes> invalidFileTypes = dataLayerProvider
          .Select(InvalidFileTypes.class, detachedcriteria);

      return invalidFileTypes;
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
    //
  }

  public boolean checkForInvalidProjectId(String projectId) {
    try {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
      detachedcriteria.add(Restrictions.eq("projectId", projectId));

      // detachedcriteria.add(Restrictions.eq("lockedUserId", userName));
      int maxResult = 1;

      List<VisionBotDataDetail> result = dataLayerProvider
          .Select(VisionBotDataDetail.class, detachedcriteria, maxResult);
      if (result.size() > 0) {
        return false;
      } else {
        return true;
      }
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }

  public boolean checkForInvalidFileId(String fileId) {
    try {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
      detachedcriteria.add(Restrictions.eq("fileId", fileId));

      // detachedcriteria.add(Restrictions.eq("lockedUserId", userName));
      int maxResult = 1;

      List<VisionBotDataDetail> result = dataLayerProvider
          .Select(VisionBotDataDetail.class, detachedcriteria, maxResult);
      if (result.size() > 0) {
        return false;
      } else {
        return true;
      }
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }


  public boolean checkForInvalidOrganizationId(String orgId) {
    try {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
      detachedcriteria.add(Restrictions.eq("orgId", orgId));

      // detachedcriteria.add(Restrictions.eq("lockedUserId", userName));
      int maxResult = 1;
      List<VisionBotDataDetail> result = dataLayerProvider
          .Select(VisionBotDataDetail.class, detachedcriteria, maxResult);
      if (result.size() > 0) {
        return false;
      } else {
        return true;
      }
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }

  public int getFailedDocumentCountsByProject(String organizationId, String projectId) {
    try {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
      detachedcriteria.add(Restrictions.eq("projectId", projectId))
          .add(Restrictions.eq("validationStatus", ValidationType.Fail.toString()));
      detachedcriteria.setProjection(Projections.rowCount());

      return dataLayerProvider.Count(detachedcriteria);
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }

  public VisionBotDataDetail getDocumentByProjectId(String projectId, String username) {
    try {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
      detachedcriteria.add(Restrictions.eq("projectId", projectId));
      detachedcriteria.add(Restrictions.eq("lockedUserId", username));

      int maxResult = 1;

      List<VisionBotDataDetail> result = dataLayerProvider
          .Select(VisionBotDataDetail.class, detachedcriteria, maxResult);
      if (result.size() > 0) {
        return result.get(0);
      }
      //ToDo : Should return/throw exception if data not found
      return null;
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }

  public VisionBotDataDetail getDocumentByFileId(String fileId) {
    try {
      DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
      detachedcriteria.add(Restrictions.eq("fileId", fileId));

      int maxResult = 1;

      List<VisionBotDataDetail> result = dataLayerProvider
          .Select(VisionBotDataDetail.class, detachedcriteria, maxResult);
      if (result.size() > 0) {
        return result.get(0);
      }
      //ToDo : Should return/throw exception if data not found
      return null;
    } catch (Exception ex) {
      throw new DatabaseConnectionException();
    }
  }

  public VisionBotDataDetail getFailedDocumentByUser(String organizationId, String projectId,
			String userName) {

		DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);

		detachedcriteria.add(Restrictions.eq("projectId", projectId));

		detachedcriteria.add(Restrictions.eq("lockedUserId", userName));
		detachedcriteria.add(Restrictions.eq("validationStatus", ValidationType.Fail.toString()));
		List<VisionBotDataDetail> result = null;
		try {
			int maxResult = 2;
			result = dataLayerProvider.Select(VisionBotDataDetail.class, detachedcriteria, maxResult);
		} catch (Exception ex) {
			throw new DatabaseConnectionException();
		}
		if (result.size() > 0) {
			if (result.size() > 1) {
        LOGGER.debug(messageSource
            .getMessage(Constants.VALIDATOR_ALERT_MULTIPLE_ENTRIES_INDB, null,
						Locale.getDefault()));
			}
			return result.get(0);
		}
		return null;
	}
  
    public VisionBotDataDetail getFailedDocumentNotAllocatedToAnyUser(String organizationId, String projectId, Date createdat, String fileId) {
		try {
			ProjectionList proj = Projections.projectionList();
			proj.add(Projections.min("createdAt"));

			LOGGER.debug("Skipping file id : " + fileId);
			DetachedCriteria detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
			detachedcriteria.add(Restrictions.eq("projectId", projectId))
					.add(Restrictions.eq("validationStatus", ValidationType.Fail.toString()))
					.add(Restrictions.or(Restrictions.eq("lockedUserId", ""), Restrictions.isNull("lockedUserId")))
					.add(Restrictions.gt("createdAt", createdat)).add(Restrictions.ne("fileId", fileId))
					.addOrder(Order.asc("createdAt"));

			int maxResult = 1;

			List<VisionBotDataDetail> result = dataLayerProvider.Select(VisionBotDataDetail.class, detachedcriteria,
					maxResult);
			if (result.size() > 0) {
				return result.get(0);
			} else {
				detachedcriteria = DetachedCriteria.forClass(VisionBotDataDetail.class);
				detachedcriteria.add(Restrictions.eq("projectId", projectId))
						.add(Restrictions.eq("validationStatus", ValidationType.Fail.toString()))
						.add(Restrictions.or(Restrictions.eq("lockedUserId", ""), Restrictions.isNull("lockedUserId")))
						.addOrder(Order.asc("createdAt"));

				List<VisionBotDataDetail> resultFromFirst = dataLayerProvider.Select(VisionBotDataDetail.class,
						detachedcriteria, maxResult);
				if (resultFromFirst.size() > 0) {
					return resultFromFirst.get(0);
				}
			}

			return null;
		} catch (Exception ex) {
			throw new DatabaseConnectionException();
		}
	}


}
