package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by Yogesh.Savalia on 27-01-2017.
 */
public class ClassificationAnalysisReport {

  @JsonProperty("projectId")
  public String projectId;
  @JsonProperty("allCategoryDetail")
  public List<CategoryDetail> allCategoryDetail;
  @JsonProperty("totalDocuments")
  public long totalDocuments;
}
