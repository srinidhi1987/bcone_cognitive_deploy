package com.automationanywhere.cognitive.validator.health.handlers.impl;

import com.automationanywhere.cognitive.common.health.handlers.AbstractHealthHandler;
import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.MQConnectionFailureException;
import com.automationanywhere.cognitive.validator.health.connections.ConnectionType;
import com.automationanywhere.cognitive.validator.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.validator.health.connections.HealthCheckConnectionFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidatorHealthCheckHandler implements AbstractHealthHandler {

  @Autowired
  HealthCheckConnectionFactory healthCheckConnectionFactory;

  @Override
  public String checkHealth() {
    HealthCheckConnection healthCheckConnection;
    Connectivity dbConnectionType = Connectivity.OK;
    Connectivity mqConnectity = Connectivity.OK;
    List<ServiceConnectivity> serviceConnectivity = null;
    for (ConnectionType connectionType : ConnectionType.values()) {
      healthCheckConnection = healthCheckConnectionFactory.getConnection(connectionType);
      try {
        healthCheckConnection.checkConnection();
      } catch (DBConnectionFailureException dcfe) {
        dbConnectionType = Connectivity.FAILURE;
      } catch (MQConnectionFailureException mqfe) {
        mqConnectity = Connectivity.FAILURE;
      }
      if (connectionType.equals(ConnectionType.SVC)) {
        serviceConnectivity = healthCheckConnection.checkConnectionForService();
      }
    }
    return HealthApiResponseBuilder.prepareSubSystem(HTTPStatusCode.OK,
        SystemStatus.ONLINE, dbConnectionType, mqConnectity, Connectivity.NOT_APPLICABLE,
        serviceConnectivity);
  }

}
