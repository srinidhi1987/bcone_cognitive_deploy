package com.automationanywhere.cognitive.validator.models;

/**
 * Created by Mayur.Panchal on 02-12-2016.
 */
public enum ValidatorOperationTypeEnum {
  Save,
  Invalid,
  Close,
  Lock,
  Unlock
}
