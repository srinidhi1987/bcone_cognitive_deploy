package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class SegmentedDocRegionDto {

  private final String text;
  private final int type;
  private final int confidence;
  private final String bounds;
  private final double angle;

  public SegmentedDocRegionDto(
      @JsonProperty("Text") final String text,
      @JsonProperty("Type") final int type,
      @JsonProperty("Confidence") final int confidence,
      @JsonProperty("Bounds") final String bounds,
      @JsonProperty("Angle") final double angle
  ) {
    this.text = text;
    this.type = type;
    this.confidence = confidence;
    this.bounds = bounds;
    this.angle = angle;
  }

  /**
   * Returns the text.
   *
   * @return the value of text
   */
  @JsonProperty("Text")
  public String getText() {
    return text;
  }

  /**
   * Returns the type.
   *
   * @return the value of type
   */
  @JsonProperty("Type")
  public int getType() {
    return type;
  }

  /**
   * Returns the confidence.
   *
   * @return the value of confidence
   */
  @JsonProperty("Confidence")
  public int getConfidence() {
    return confidence;
  }

  /**
   * Returns the bounds.
   *
   * @return the value of bounds
   */
  @JsonProperty("Bounds")
  public String getBounds() {
    return bounds;
  }

  /**
   * Returns the angle.
   *
   * @return the value of angle
   */
  @JsonProperty("Angle")
  public double getAngle() {
    return angle;
  }
}
