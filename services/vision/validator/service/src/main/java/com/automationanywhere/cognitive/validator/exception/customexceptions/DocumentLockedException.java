package com.automationanywhere.cognitive.validator.exception.customexceptions;

/**
 * Created by Shweta.Thakur on 05-07-2017
 */
public class DocumentLockedException extends RuntimeException {

    private static final long serialVersionUID = -4829497787954324895L;

    public DocumentLockedException(String message) {
        super(message);
    }
}