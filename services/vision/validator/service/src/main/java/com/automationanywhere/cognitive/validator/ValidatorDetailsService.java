package com.automationanywhere.cognitive.validator;

import com.automationanywhere.cognitive.validator.models.FieldAccuracyDetails;
import com.automationanywhere.cognitive.validator.models.InvalidFileTypes;
import com.automationanywhere.cognitive.validator.models.InvalidFiles;
import com.automationanywhere.cognitive.validator.models.ValidatorOperationTypeEnum;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by Keval.Sheth on 09-12-2016.
 */
public interface ValidatorDetailsService {

  String VALIDATOR_DETAILS_SERVICE_ID = "validator";
  String VALIDATOR_DETAILS_SERVICE_PROXY = "validatorProxy";

  boolean updateResourceCorrectedValue(String fileid, String projectid, String username,
      String correctedValue, float accuracyValue, FieldAccuracyDetails[] details,
      ValidatorOperationTypeEnum type);

  boolean updateResourceUserId(String projectid, String fileid, String userid,
      ValidatorOperationTypeEnum type);

  boolean insertRow(InvalidFiles invalidFiles);

  VisionBotDataDetail getFailedVBot(String orgid, String projectid, String username);

  int getFailedDocumentCountsByProject(String organizationId, String projectId);

  List<Object> getReviewedVBotCount();

  List<InvalidFileTypes> getInvalidMarkTypes();

  boolean checkInvalidParametersForOrganizationId(String orgid);

  boolean checkInvalidParametersForProjectId(String projectid);

  boolean checkInvalidParametersForFileId(String fileid);

  boolean addVisionBot(String fileid, String orgid, String prjid, String validationstatus,
      String visionbotid, FieldAccuracyDetails[] fieldAccuracyDetails, String data, float failed,
      float pass, float total);

  List<Object> getProjectTotalsWithAccuracyDetails(String idtype, String id);

  List<Object> getDocumentProcessingDetails(String idtype, String id);

  List<Object> getTimeSpend(String id);

  List<Object> getFieldAccuracyAndFilesValidated(String id);

  List<Object> getProductionFileSummary(String projectId);
}
