package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class DataModelIdStructureDto {

  private final String fieldId;
  private final String tableDefId;
  private final String columnId;

  public DataModelIdStructureDto(
      @JsonProperty("FieldId") final String fieldId,
      @JsonProperty("TableDefId") final String tableDefId,
      @JsonProperty("ColumnId") final String columnId
  ) {
    this.fieldId = fieldId;
    this.tableDefId = tableDefId;
    this.columnId = columnId;
  }

  /**
   * Returns the fieldId.
   *
   * @return the value of fieldId
   */
  @JsonProperty("FieldId")
  public String getFieldId() {
    return fieldId;
  }

  /**
   * Returns the tableDefId.
   *
   * @return the value of tableDefId
   */
  @JsonProperty("TableDefId")
  public String getTableDefId() {
    return tableDefId;
  }

  /**
   * Returns the columnId.
   *
   * @return the value of columnId
   */
  @JsonProperty("ColumnId")
  public String getColumnId() {
    return columnId;
  }
}
