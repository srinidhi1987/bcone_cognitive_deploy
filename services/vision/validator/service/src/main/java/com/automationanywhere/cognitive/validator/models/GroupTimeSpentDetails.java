package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class GroupTimeSpentDetails {

  @JsonProperty(value = "categoryId")
  private String categoryId;

  @JsonProperty(value = "timeSpend")
  private double timeSpend;

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public double getTimeSpend() {
    return timeSpend;
  }

  public void setTimeSpend(double timeSpend) {
    this.timeSpend = timeSpend;
  }
}