package com.automationanywhere.cognitive.validator;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.validator.exception.customexceptions.MQConnectionFailureException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.MessageQueueConnectionCloseException;
import com.rabbitmq.client.*;
import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Purnil.Soni on 04-01-2017.
 */
public class RpcClient {

  AALogger aaLogger = AALogger.create(this.getClass());
  private Connection connection;
  private Channel channel;
  private String requestQueueName;
  private String replyQueueName;
  @SuppressWarnings("deprecation")
  private QueueingConsumer consumer;
  private String hostName;

//    public RpcClient(String queueName) throws Exception {
//        this(queueName, "192.168.2.71");
//    }

  //    public RpcClient(String queueName, String hostName) throws Exception {
//        this.requestQueueName = queueName;
//
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost(hostName);
//        factory.setVirtualHost("test");
//        factory.setUsername("messagequeue");
//        factory.setPassword("passmessage");
//        connection = factory.newConnection();
//        channel = connection.createChannel();
//
//        replyQueueName = channel.queueDeclare().getQueue();
//        consumer = new QueueingConsumer(channel);
//        channel.basicConsume(replyQueueName, true, consumer);
//    }
  public RpcClient() {

  }

  @SuppressWarnings("deprecation")
  public RpcClient(String queueName, ConnectionFactory factory) throws Exception {
    aaLogger.entry();
    this.requestQueueName = queueName;
    factory.setAutomaticRecoveryEnabled(true);
    boolean connectionSuccess = false;
    while (!connectionSuccess) {
      try {
        aaLogger.trace("try to connect to queue :" + queueName);
        connection = factory.newConnection();
        connectionSuccess = true;
      } catch (ConnectException ex) {
        /*log.error(ex.getMessage(), ex);*/
        Thread.sleep(1000);
      }
      aaLogger.exit();
    }

    channel = connection.createChannel();
    replyQueueName = channel.queueDeclare().getQueue();
    consumer = new QueueingConsumer(channel);
    channel.basicConsume(replyQueueName, true, consumer);
  }

   /* public RpcClient(String queueName, ConnectionFactory factory, boolean ack) throws Exception {
        this.requestQueueName = queueName;

        connection = factory.newConnection();
        channel = connection.createChannel();

        replyQueueName = channel.queueDeclare().getQueue();
        consumer = new QueueingConsumer(channel);
        channel.basicConsume(replyQueueName, ack, consumer);
    }*/

  @SuppressWarnings("deprecation")
  public String call(String message) throws Exception {
    aaLogger.entry();
    String response = null;
    String corrId = java.util.UUID.randomUUID().toString();

    AMQP.BasicProperties props = new AMQP.BasicProperties
        .Builder()
        .correlationId(corrId)
        .deliveryMode(2)
        .replyTo(replyQueueName)
        .build();

    channel.basicPublish("", requestQueueName, props, message.getBytes());
    aaLogger.trace("waiting for response");
    while (true) {
      QueueingConsumer.Delivery delivery = consumer.nextDelivery();
      if (delivery.getProperties().getCorrelationId().equals(corrId)) {
        response = new String(delivery.getBody());
        aaLogger.trace("Got Response");
        break;
      }
    }
    aaLogger.exit();
    return response;
  }

  public void putMessage(String message) throws Exception {
    aaLogger.entry();
    String response = null;
    String corrId = java.util.UUID.randomUUID().toString();

    AMQP.BasicProperties props = new AMQP.BasicProperties
        .Builder()
        .correlationId(corrId)
        .deliveryMode(2)
        .build();

    channel.basicPublish("", requestQueueName, props, message.getBytes());
    aaLogger.exit();

  }

  public void close() throws Exception {
    aaLogger.entry();
    connection.close();
    aaLogger.exit();
  }

  public void checkConnection(final ConnectionFactory rpcConnectionFactory) {
    Connection conn = null;

    try {
      conn = rpcConnectionFactory.newConnection();
    } catch (final IOException | TimeoutException e) {
      throw new MQConnectionFailureException("Error while connecting to rabbit MQ", e);
    } finally {
      closeConnection(conn);
    }
  }

  void closeConnection(final Connection conn) {
    try {
      if (conn != null && conn.isOpen()) {
        conn.close();
      }
    } catch (final IOException ex) {
      throw new MessageQueueConnectionCloseException(
          "Error while closing mq connection ", ex
      );
    }
  }
}
