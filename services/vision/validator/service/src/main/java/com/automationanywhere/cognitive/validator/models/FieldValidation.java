package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

public class FieldValidation {

  @JsonProperty(value = "fieldId")
  private String name;

  @JsonProperty(value = "percentValidated")
  private double percentValidated;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPercentValidated() {
    return percentValidated;
  }

  public void setPercentValidated(double percentValidated) {
    this.percentValidated = percentValidated;
  }
}