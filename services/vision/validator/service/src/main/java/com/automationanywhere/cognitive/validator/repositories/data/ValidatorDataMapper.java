package com.automationanywhere.cognitive.validator.repositories.data;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

import com.automationanywhere.cognitive.validator.models.FieldAccuracyDetails;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import com.automationanywhere.cognitive.validator.service.model.FieldLevelAccuracy;
import com.automationanywhere.cognitive.validator.service.model.VisionBotDocument;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ValidatorDataMapper {

  public List<FieldLevelAccuracy> get(List<FieldAccuracyDetails> fields) {
    List<FieldLevelAccuracy> accuracies = null;
    if (fields != null) {
      accuracies = fields.stream().map(this::get).collect(toList());
    }
    return accuracies;
  }

  public FieldLevelAccuracy get(FieldAccuracyDetails field) {
    return new FieldLevelAccuracy(
        field.getFileId(),
        field.getFieldId(),
        field.getOldValue(),
        field.getNewValue(),
        field.getPassAccuracyValue()
    );
  }

  public List<VisionBotDocument> getVBotDoc(List<VisionBotDataDetail> data) {
    return ofNullable(data).orElse(emptyList()).stream().map(this::getVBotDoc).collect(toList());
  }

  public VisionBotDocument getVBotDoc(VisionBotDataDetail data) {
    VisionBotDocument vBotDocument = null;
    if (data != null) {
      vBotDocument = new VisionBotDocument(
          data.getFileId(),
          data.getOrgId(),
          data.getProjectId(),
          data.getVBotData(),
          data.getValidationStatus(),
          data.getCorrectedData(),
          data.getCreatedAt(),
          data.getUpdatedBy(),
          data.getUpdatedAt(),
          data.getVisionBotId(),
          data.getLockedUserId(),
          data.getFailedFieldCounts(),
          data.getPassFieldCounts(),
          data.getTotalFieldCounts(),
          data.getStartTime(),
          data.getEndTime(),
          data.getAverageTimeInSeconds(),
          data.getLastExportTime(),
          data.getLockedTimestamp()
      );
    }
    return vBotDocument;
  }
}
