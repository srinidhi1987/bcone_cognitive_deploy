package com.automationanywhere.cognitive.validator;

import com.automationanywhere.cognitive.common.inetaddress.wrapper.InetAddressWrapper;
import com.automationanywhere.cognitive.common.logger.AALogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spark.Spark;


/**
 * Created by keval.sheth on 29-11-2016.
 */
public class Main {

  private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations\\CognitivePlatform.jks";
  private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations\\Cognitive.properties";
  private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
  private static final String PROPERTIES_NOTFOUND = "NotFound";
  private static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
  private static final int DATASOURCE_INITIAL_SIZE = 3;
  private static final String HTTP_SCHEME = "http";
  private static final String HTTPS_SCHEME = "https";
  private static String jdbcUrl;
  private static String outputDir;
  private static int httpPort;
  private static String username;
  private static String jdbcString = "jdbc:sqlserver://";
  private static ValidatorServiceHandler validatorServiceHandler;
  private static boolean windowsAuth;
  private static AALogger aaLogger = AALogger.create(Main.class);
  private static String fullyQualifiedName = InetAddressWrapper.getFullyQualifiedName();
  private static String fileManagerPort;
  private static String projectServicePort;
  private static int restTemplateReadTimeout;
  private static int restTemplateConnectTimeout;
  private static final String FILENAME_COGNITIVE_SETTINGS = "configurations\\Settings.txt";
  private static final String PROPERTIES_SQL_WINAUTH = "SQLWindowsAuthentication";
  private static String settingsFilePath;
  private static String PROJECT_ROOT_URL;
  private static String FILE_MANAGER_ROOT_URL;

  public static void main(String args[]) throws IOException {
    StringBuilder argsParams = new StringBuilder();
    if (args.length > 7) {
      jdbcUrl = jdbcString + args[0] + ":" + args[1];
      jdbcUrl += ";databaseName=" + args[2];
      jdbcUrl += ";sendStringParametersAsUnicode=false";
      username = args[3];

      fileManagerPort = args[5];
      projectServicePort = args[6];
      PROJECT_ROOT_URL = String.format("%s://%s:%s",
          HTTP_SCHEME, fullyQualifiedName, projectServicePort);
      FILE_MANAGER_ROOT_URL = String.format("%s://%s:%s",
          HTTP_SCHEME, fullyQualifiedName, fileManagerPort);

      outputDir = args[7];
      loadSettings();
      if (windowsAuth) {
        jdbcUrl += ";integratedSecurity=true";
      }

      //If output folder name contains space then we have replaced 'space' with '?' during installation
      //hence here need to decode '?' with 'space' to make it original name
      outputDir = outputDir.replace("?", " ");
      if (outputDir.endsWith("\"")) {
        outputDir = outputDir.substring(0, outputDir.length() - 1);
      }
      for (int i = 0; i < args.length; i++) {
        argsParams.append(args[i]).append(" ");
      }

      aaLogger.info(args.length + " arguments were observed in following order: " + argsParams);
    } else {
      for (int i = 0; i < args.length; i++) {
        argsParams.append(args[i]).append(" ");
      }
      aaLogger.error("8 arguments necessary to run jar file <ipaddress> <sqlport> <databasename> "
          + "<username> <serviceport> <fileManagerPort> <projectServicePort> <outputdirectory> but "
          + "there were " + args.length + " arguments: " + argsParams, "Invalid arguments");
      return;
    }
    httpPort = Integer.parseInt(args[4]);
    restTemplateReadTimeout = 60000;
    restTemplateConnectTimeout = 60000;

    setServiceSecurity();

    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
    context.registerShutdownHook();
  }

  public static String getProjectServiceUrl() {
    return PROJECT_ROOT_URL;
  }

  public static String getFileManagerServiceUrl() {
    return FILE_MANAGER_ROOT_URL;
  }

  private static void setServiceSecurity() {
    if (isSSLCertificateConfigured()) {
      try {
        String certificateKey = loadSSLCertificatePassword();
        if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
          Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
          String cognitiveCertificateFilePath =
              certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
          Spark.secure(cognitiveCertificateFilePath, certificateKey, null, null);

          applyHttpsSchemeToExternalServiceUrl();
        }

      } catch (final IOException ex) {
        aaLogger.error("Failed to set service security", ex);
      }
    }
  }

  private static String loadSSLCertificatePassword() throws IOException {
    Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
    String cognitivePropertiesFilePath =
        propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
    if (fileExists(cognitivePropertiesFilePath)) {
      Properties properties = new Properties();
      properties.load(new FileInputStream(cognitivePropertiesFilePath));
      String certificateKey = properties
          .getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
      return certificateKey;
    } else {
      return PROPERTIES_NOTFOUND;
    }
  }

  private static boolean isSSLCertificateConfigured() {
    boolean hasCertificate = false;
    Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
    String cognitiveCertificateFilePath =
        certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
    if (fileExists(cognitiveCertificateFilePath)) {
      hasCertificate = true;
    }
    return hasCertificate;
  }

  private static boolean fileExists(String filePath) {
    File file = new File(filePath);
    if (file.exists()) {
      return true;
    } else {
      return false;
    }
  }

  private static void applyHttpsSchemeToExternalServiceUrl() {
    PROJECT_ROOT_URL = String
        .format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, projectServicePort);
    FILE_MANAGER_ROOT_URL = String
        .format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, fileManagerPort);
  }

  public static BasicDataSource createDataSource(String password) {
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(SQL_SERVER_DRIVER);
    dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
    dataSource.setUrl(jdbcUrl);
    if (!windowsAuth) {
      dataSource.setUsername(username);
      dataSource.setPassword(password);
    }
    return dataSource;
  }

  public static AppConfig getAppConfig() {
    return new AppConfig(outputDir, httpPort, restTemplateReadTimeout, restTemplateConnectTimeout);
  }

  private static void loadSettings() throws IOException {
    Path configurationDirectoryPath  = Paths.get(System.getProperty("user.dir")).getParent();
    settingsFilePath = configurationDirectoryPath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
    Properties properties = loadProperties(settingsFilePath);
    if(properties.containsKey(PROPERTIES_SQL_WINAUTH))
    {
      windowsAuth = Boolean.parseBoolean(properties.getProperty(PROPERTIES_SQL_WINAUTH));
    }
  }

  private static Properties loadProperties(String filePath) throws IOException {
    Properties properties = new Properties();
    properties.load(new FileInputStream(filePath));
    return properties;
  }
}
