package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class FieldDataRecordFieldValidationIssueDto {

  private final int issueCode;

  public FieldDataRecordFieldValidationIssueDto(
      @JsonProperty("IssueCode") final int issueCode
  ) {
    this.issueCode = issueCode;
  }

  /**
   * Returns the issueCode.
   *
   * @return the value of issueCode
   */
  @JsonProperty("IssueCode")
  public int getIssueCode() {
    return issueCode;
  }
}
