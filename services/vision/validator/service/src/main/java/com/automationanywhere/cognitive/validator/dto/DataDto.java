package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public final class DataDto {

  private final FieldDataRecordDto fieldDataRecord;
  private final List<TableDataRecordDto> tableDataRecords;

  public DataDto(
      @JsonProperty("FieldDataRecord") final FieldDataRecordDto fieldDataRecord,
      @JsonProperty("TableDataRecord") final List<TableDataRecordDto> tableDataRecords
  ) {
    this.fieldDataRecord = fieldDataRecord;
    this.tableDataRecords = tableDataRecords;
  }

  /**
   * Returns the fieldDataRecord.
   *
   * @return the value of fieldDataRecord
   */
  @JsonProperty("FieldDataRecord")
  public FieldDataRecordDto getFieldDataRecord() {
    return fieldDataRecord;
  }

  /**
   * Returns the tableDataRecords.
   *
   * @return the value of tableDataRecords
   */
  @JsonProperty("TableDataRecord")
  public List<TableDataRecordDto> getTableDataRecords() {
    return tableDataRecords;
  }
}
