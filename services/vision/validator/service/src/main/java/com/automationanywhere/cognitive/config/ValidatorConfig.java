package com.automationanywhere.cognitive.config;

import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.validator.AppConfig;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan({
    "com.automationanywhere.cognitive.exporter",
    "com.automationanywhere.cognitive.generic",
    "com.automationanywhere.cognitive.validator"
})
public class ValidatorConfig {

  private final AppConfig appConfig;

  @Autowired
  public ValidatorConfig(AppConfig appConfig) {
    this.appConfig = appConfig;
  }

  @Bean
  public ObjectMapper getObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.configure(SerializationFeature.INDENT_OUTPUT, false);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper;
  }

  @Bean
  public RestTemplate getRestTemplate(
      final ObjectMapper mapper
  ) {
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    requestFactory.setReadTimeout(appConfig.getRestTemplateReadTimeout());
    requestFactory.setConnectTimeout(appConfig.getRestTemplateConnectTimeout());
    RestTemplate restTemplate = new RestTemplate(requestFactory);

    MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
    jsonMessageConverter.setObjectMapper(mapper);

    List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
    messageConverters.add(new StringHttpMessageConverter());
    messageConverters.add(jsonMessageConverter);
    restTemplate.setMessageConverters(messageConverters);

    return restTemplate;
  }

  @Bean
  public RestTemplateWrapper getRestTemplateWrapper(
      final RestTemplate restTemplate
  ) {
    return new RestTemplateWrapper(restTemplate);
  }
}
