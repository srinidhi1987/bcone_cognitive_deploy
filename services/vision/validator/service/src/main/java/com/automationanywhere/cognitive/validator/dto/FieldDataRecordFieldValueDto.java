package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class FieldDataRecordFieldValueDto {

  private final String value;
  private final SirFieldDto field;

  public FieldDataRecordFieldValueDto(
      @JsonProperty("Value") String value,
      @JsonProperty("Field") SirFieldDto field
  ) {
    this.value = value;
    this.field = field;
  }

  /**
   * Returns the value.
   *
   * @return the value of value
   */
  @JsonProperty("Value")
  public String getValue() {
    return value;
  }

  /**
   * Returns the field.
   *
   * @return the value of field
   */
  @JsonProperty("Field")
  public SirFieldDto getField() {
    return field;
  }
}
