package com.automationanywhere.cognitive.validator.consumer.filemanger.impl;

import static com.automationanywhere.cognitive.logging.Constants.HEADER_CORRELATION_ID;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.validator.consumer.filemanger.contract.FileServiceConsumer;
import com.automationanywhere.cognitive.validator.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.validator.models.StandardResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Mayur.Panchal on 21-03-2017.
 */
public class FileServiceConsumerImpl implements FileServiceConsumer {

  AALogger log = AALogger.create(this.getClass());
  private String rootUrl;
  private RestTemplate restTemplate;

  public FileServiceConsumerImpl(String rootUrl) {
    this.restTemplate = new RestTemplate();
    this.rootUrl = rootUrl;
    List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
    MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
    jsonMessageConverter.setObjectMapper(new ObjectMapper());
    messageConverters.add(jsonMessageConverter);
    restTemplate.setMessageConverters(messageConverters);
  }

  ///Dashboard endpoint One related data
  @Override
  public ClassificationAnalysisReport getAllFileStats(String projId) {
    log.entry();
    String relativeUrl = "organizations/1/projects/" + projId + "/report";
    String url = rootUrl + relativeUrl;//String.format(relativeUrl,projId);
    log.debug(() -> url);
    ClassificationAnalysisReport countStatistics = null;
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.add(HEADER_CORRELATION_ID, ThreadContext.get(HEADER_CORRELATION_ID));
      HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      ResponseEntity<StandardResponse> standardResponse = restTemplate
          .exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      countStatistics = objectMapper
          .convertValue(standardResponse.getBody().getData(), ClassificationAnalysisReport.class);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    }
    return countStatistics;
  }
}
