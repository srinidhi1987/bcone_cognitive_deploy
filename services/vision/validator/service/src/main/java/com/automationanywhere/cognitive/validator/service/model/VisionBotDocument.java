package com.automationanywhere.cognitive.validator.service.model;

import java.util.Date;

public final class VisionBotDocument {

  private final String fileId;
  private final String orgId;
  private final String projectId;
  private final String vBotDocument;
  private final String validationStatus;
  private final String correctedData;
  private final Date createdAt;
  private final String updatedBy;
  private final Date updatedAt;
  private final String visionBotId;
  private final String lockedUserId;
  private final float failedFieldCounts;
  private final float passFieldCounts;
  private final float totalFieldCounts;
  private final Date startTime;
  private final Date endTime;
  private final long averageTimeInSeconds;
  private final Date lastExportTime;
  private final Date lockedTimestamp;

  public VisionBotDocument(
      final String fileId,
      final String orgId,
      final String projectId,
      final String vBotDocument,
      final String validationStatus,
      final String correctedData,
      final Date createdAt,
      final String updatedBy,
      final Date updatedAt,
      final String visionBotId,
      final String lockedUserId,
      final float failedFieldCounts,
      final float passFieldCounts,
      final float totalFieldCounts,
      final Date startTime,
      final Date endTime,
      final long averageTimeInSeconds,
      final Date lastExportTime,
      final Date lockedTimestamp
  ) {
    this.fileId = fileId;
    this.orgId = orgId;
    this.projectId = projectId;
    this.vBotDocument = vBotDocument;
    this.validationStatus = validationStatus;
    this.correctedData = correctedData;
    this.createdAt = createdAt;
    this.updatedBy = updatedBy;
    this.updatedAt = updatedAt;
    this.visionBotId = visionBotId;
    this.lockedUserId = lockedUserId;
    this.failedFieldCounts = failedFieldCounts;
    this.passFieldCounts = passFieldCounts;
    this.totalFieldCounts = totalFieldCounts;
    this.startTime = startTime;
    this.endTime = endTime;
    this.averageTimeInSeconds = averageTimeInSeconds;
    this.lastExportTime = lastExportTime;
    this.lockedTimestamp = lockedTimestamp;
  }

  /**
   * Returns the fileId.
   *
   * @return the value of fileId
   */
  public String getFileId() {
    return fileId;
  }

  /**
   * Returns the orgId.
   *
   * @return the value of orgId
   */
  public String getOrgId() {
    return orgId;
  }

  /**
   * Returns the projectId.
   *
   * @return the value of projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * Returns the vBotDocument.
   *
   * @return the value of vBotDocument
   */
  public String getVBotDocument() {
    return vBotDocument;
  }

  /**
   * Returns the validationStatus.
   *
   * @return the value of validationStatus
   */
  public String getValidationStatus() {
    return validationStatus;
  }

  /**
   * Returns the correctedData.
   *
   * @return the value of correctedData
   */
  public String getCorrectedData() {
    return correctedData;
  }

  /**
   * Returns the createdAt.
   *
   * @return the value of createdAt
   */
  public Date getCreatedAt() {
    return createdAt;
  }

  /**
   * Returns the updatedBy.
   *
   * @return the value of updatedBy
   */
  public String getUpdatedBy() {
    return updatedBy;
  }

  /**
   * Returns the updatedAt.
   *
   * @return the value of updatedAt
   */
  public Date getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Returns the visionBotId.
   *
   * @return the value of visionBotId
   */
  public String getVisionBotId() {
    return visionBotId;
  }

  /**
   * Returns the lockedUserId.
   *
   * @return the value of lockedUserId
   */
  public String getLockedUserId() {
    return lockedUserId;
  }

  /**
   * Returns the failedFieldCounts.
   *
   * @return the value of failedFieldCounts
   */
  public float getFailedFieldCounts() {
    return failedFieldCounts;
  }

  /**
   * Returns the passFieldCounts.
   *
   * @return the value of passFieldCounts
   */
  public float getPassFieldCounts() {
    return passFieldCounts;
  }

  /**
   * Returns the totalFieldCounts.
   *
   * @return the value of totalFieldCounts
   */
  public float getTotalFieldCounts() {
    return totalFieldCounts;
  }

  /**
   * Returns the startTime.
   *
   * @return the value of startTime
   */
  public Date getStartTime() {
    return startTime;
  }

  /**
   * Returns the endTime.
   *
   * @return the value of endTime
   */
  public Date getEndTime() {
    return endTime;
  }

  /**
   * Returns the averageTimeInSeconds.
   *
   * @return the value of averageTimeInSeconds
   */
  public long getAverageTimeInSeconds() {
    return averageTimeInSeconds;
  }

  /**
   * Returns the lastExportTime.
   *
   * @return the value of lastExportTime
   */
  public Date getLastExportTime() {
    return lastExportTime;
  }

  /**
   * Returns the lockedTimestamp.
   *
   * @return the value of lockedTimestamp
   */
  public Date getLockedTimestamp() {
    return lockedTimestamp;
  }
}
