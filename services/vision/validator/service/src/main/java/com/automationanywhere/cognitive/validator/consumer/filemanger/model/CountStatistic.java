package com.automationanywhere.cognitive.validator.consumer.filemanger.model;

import com.automationanywhere.cognitive.validator.models.FileDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class CountStatistic {

  private String projectId;
  private String categoryId;
  private int fileCount;
  private EnvironmentCountStatistic stagingFileCount;
  private EnvironmentCountStatistic productionFileCount;
  @JsonIgnore
  private List<FileDetails> fileDetailsList;

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public int getFileCount() {
    return fileCount;
  }

  public void setFileCount(int fileCount) {
    this.fileCount = fileCount;
  }

  public EnvironmentCountStatistic getStagingFileCount() {
    return stagingFileCount;
  }

  public void setStagingFileCount(EnvironmentCountStatistic stagingFileCount) {
    this.stagingFileCount = stagingFileCount;
  }

  public EnvironmentCountStatistic getProductionFileCount() {
    return productionFileCount;
  }

  public void setProductionFileCount(EnvironmentCountStatistic productionFileCount) {
    this.productionFileCount = productionFileCount;
  }

  public List<FileDetails> getFileDetailsList() {
    return fileDetailsList;
  }

  public void setFileDetailsList(List<FileDetails> fileDetailsList) {
    this.fileDetailsList = fileDetailsList;
  }
}
