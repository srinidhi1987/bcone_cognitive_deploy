package com.automationanywhere.cognitive.validator.models;

import com.automationanywhere.cognitive.validator.hibernate.attributeconverters.SecureStringAttributeConverter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */
@Entity
@Table(name = "FieldLevelAccuracy")
public class FieldAccuracyDetails extends EntityModelBase {

  @Id
  @Column(name = "fileid")
  private String FileId;
  @JsonProperty(value = "fieldId")
  @Column(name = "fieldid")
  private String FieldId;
  @Convert(converter=SecureStringAttributeConverter.class)
  @JsonProperty(value = "oldValue")
  @Column(name = "oldvalue")
  private String OldValue;
  @Convert(converter=SecureStringAttributeConverter.class)
  @JsonProperty(value = "newValue")
  @Column(name = "newvalue")
  private String NewValue;
  @JsonProperty(value = "passAccuracyValue")
  @Column(name = "passaccuracyvalue")
  private float PassAccuracyValue;


  public String getFileId() {
    return FileId;
  }

  public void setFileId(String fileId) {
    FileId = fileId;
  }

  public String getFieldId() {
    return FieldId;
  }

  public void setFieldId(String fieldId) {
    FieldId = fieldId;
  }

  public String getOldValue() {
    return OldValue;
  }

  public void setOldValue(String oldValue) {
    OldValue = oldValue;
  }

  public String getNewValue() {
    return NewValue;
  }

  public void setNewValue(String newValue) {
    NewValue = newValue;
  }

  public float getPassAccuracyValue() {
    return PassAccuracyValue;
  }

  public void setPassAccuracyValue(float passAccuracyValue) {
    PassAccuracyValue = passAccuracyValue;
  }

  @Override
  public Map<String, Object> getCustomQuery(StringBuilder queryString, String queryId,
      String idtype, String id) {
    return null;
  }

  @Override
  public Map<String, Object> getCustomQueryForDistinctElements(StringBuilder queryString,
      String parameter) {
    return null;
  }
}