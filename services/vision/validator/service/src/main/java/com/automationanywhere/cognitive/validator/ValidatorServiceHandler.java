package com.automationanywhere.cognitive.validator;

import static com.automationanywhere.cognitive.constants.Constants.MESSAGE_SOURCE;
import static com.automationanywhere.cognitive.logging.Constants.DATABASE_CONNECTION_DOWN;
import static com.automationanywhere.cognitive.logging.Constants.HEADER_CORRELATION_ID;
import static com.automationanywhere.cognitive.logging.Constants.PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE;
import static com.automationanywhere.cognitive.validator.ValidatorDetailsService.VALIDATOR_DETAILS_SERVICE_ID;
import static java.util.Optional.ofNullable;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.patch;
import static spark.Spark.port;
import static spark.Spark.post;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter;
import com.automationanywhere.cognitive.logging.Constants;
import com.automationanywhere.cognitive.validator.dto.InvalidFileDto;
import com.automationanywhere.cognitive.validator.dto.ValidatorDtoMapper;
import com.automationanywhere.cognitive.validator.exception.InvalidJsonPatch;
import com.automationanywhere.cognitive.validator.exception.InvalidParameterException;
import com.automationanywhere.cognitive.validator.exception.JsonPatchException;
import com.automationanywhere.cognitive.validator.exception.JsonValidatorException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.exception.impl.ExceptionHandler;
import com.automationanywhere.cognitive.validator.health.handlers.impl.ValidatorHealthCheckHandler;
import com.automationanywhere.cognitive.validator.impl.DatabaseConnectionException;
import com.automationanywhere.cognitive.validator.models.AccuracyProcessingDetails;
import com.automationanywhere.cognitive.validator.models.CategoryDetail;
import com.automationanywhere.cognitive.validator.models.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.validator.models.ClassificationFieldDetails;
import com.automationanywhere.cognitive.validator.models.CustomResponse;
import com.automationanywhere.cognitive.validator.models.DocumentProcessingDetails;
import com.automationanywhere.cognitive.validator.models.FieldAccuracyDashboardDetails;
import com.automationanywhere.cognitive.validator.models.FieldAccuracyDetails;
import com.automationanywhere.cognitive.validator.models.FieldValidation;
import com.automationanywhere.cognitive.validator.models.GroupTimeSpentDetails;
import com.automationanywhere.cognitive.validator.models.InvalidFileTypes;
import com.automationanywhere.cognitive.validator.models.ProductionFilesSummary;
import com.automationanywhere.cognitive.validator.models.ProjectTotalsWithAccuracyDetails;
import com.automationanywhere.cognitive.validator.models.ReviewResourceVBot;
import com.automationanywhere.cognitive.validator.models.ValidatorOperationTypeEnum;
import com.automationanywhere.cognitive.validator.service.VisionBotValidatorService;
import com.automationanywhere.cognitive.validator.service.model.VisionBotDocument;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletInputStream;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import spark.HaltException;
import spark.Request;
import spark.Response;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */
@Controller
public class ValidatorServiceHandler extends AbstractHandler {

  private static final AALogger LOGGER = AALogger.create(ValidatorServiceHandler.class);
  private static final String CONTENT_TYPE_JSON_PATCH = "application/json-patch+json";
  private static final String HEADER_ACCEPT_PATCH = "Accept-Patch";
  private static final String HEADER_USERNAME_KEY = "username";

  private final ValidatorDetailsService validatorService;
  private final FileManagerAdapter fileManagerAdapter;
  private final VisionBotValidatorService visionBotValidatorService;
  private final ValidatorDtoMapper dtoMapper;
  private final ExceptionHandler exceptionHandler;

  @Autowired
  public ValidatorServiceHandler(
      final AppConfig appConfig,
      @Qualifier(MESSAGE_SOURCE) final MessageSource messageSource,
      final ValidatorHealthCheckHandler healthApiHandler,
      @Qualifier(VALIDATOR_DETAILS_SERVICE_ID) final ValidatorDetailsService validatorService,
      final FileManagerAdapter fileManagerAdapter,
      final ObjectMapper mapper,
      final VisionBotValidatorService visionBotValidatorService,
      final ValidatorDtoMapper dtoMapper,
      final ExceptionHandler exceptionHandler
  ) {
    super(mapper, messageSource);
    this.validatorService = validatorService;
    this.fileManagerAdapter = fileManagerAdapter;
    this.visionBotValidatorService = visionBotValidatorService;
    this.dtoMapper = dtoMapper;
    this.exceptionHandler = exceptionHandler;

    port(appConfig.getHttpPort());

    before((request, response) -> preRequestProcessor(request));
    after(this::postRequestProcessor);
    get("organizations/:orgid/projects/:prjid/visionbot/", this::getFailedVBot);
    get("organizations/:orgid/validator/projects/:prjid", this::getFailedVBotCount);
    get("organizations/:orgid/validator/reviewedfilecount", this::getReviewedVBotCount);
    get("organizations/:orgid/validator/processingdetails/:idtype", this::getDocProcDetails);
    get("organizations/:orgid/validator/dashboard/project/:id", this::getPrjTotalsAccuracyDetails);
    get("validator/invalidreasons/", this::getInvalidMarkTypes);
    post("organizations/:orgid/projects/:prjid/visionbot/:fileid", this::updateVBotData);
    patch("organizations/:orgid/projects/:prjid/visionbot/:fileid", this::patchVBotData);
    post("organizations/:orgid/projects/:prjid/unlock/", this::updateVBotUnlockUser);
    post("organizations/:orgid/projects/:prjid/files/:fileid/invalidate", this::markInvalidFiles);
    post(
        "organizations/:orgid/projects/:prjid/visionbot/:visionbotid/files/:fileid/validationstatus/:validationstatus",
        this::addVBotData);
    get("organizations/:orgid/projects/:prjid/productionsummary",
        this::getProductionSummaryDetails);
    post("/validator/documents/unlock", this::unlockDocuments);
    post("/organizations/:orgid/projects/:projid/files/:fileid/keepalive",
        this::keepValidatorFileAlive);

    get("/health", ((request, response) -> {
      response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
      response.body("OK");
      return response;
    }));
    get("/heartbeat", ((request, response) -> getHeartBeatInfo(response)));
    get("/healthcheck", ((request, response) -> healthApiHandler.checkHealth()));

    exception(DatabaseConnectionException.class, exceptionHandler::handleDbConnectionException);
    exception(DataNotFoundException.class, exceptionHandler::handleDataNotFoundException);
    exception(InvalidParameterException.class, exceptionHandler::handleInvalidParameterException);
    exception(Exception.class, exceptionHandler::handleException);
  }

  private String keepValidatorFileAlive(Request request, Response response) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{request.url()},
              Locale.getDefault()));
      CustomResponse customResponse = new CustomResponse();
      String fileId = request.params(":fileid");

      visionBotValidatorService.keepValidatorDocumentAlive(fileId);
      customResponse.setSuccess(true);
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private void postRequestProcessor(Request request, Response response) {
    addHeader(request, response);
  }

  private void preRequestProcessor(Request request) {
    addTransactionIdentifier(request);
  }

  private void addTransactionIdentifier(Request request) {
    String transactionId = request.headers(HEADER_CORRELATION_ID);
    ThreadContext.put(HEADER_CORRELATION_ID, transactionId != null ?
        transactionId : UUID.randomUUID().toString());
  }

  private void addHeader(Request request, Response response) {
    response.type("application/json");
  }

  private Object getPrjTotalsAccuracyDetails(Request req, Response res) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{req.url()}, Locale.getDefault()));
      CustomResponse customResponse = new CustomResponse();
      String idType = "";
      String id = req.params("id");
      List<Object> result;
      List<Object> resultAccuracyAndValidatedFields;
      List<Object> resultGroupTimeSpend;
      ClassificationAnalysisReport resultCategory = fileManagerAdapter.getAllFileStats(id);
      LOGGER.trace("Successfully fetched all file statistics for classification analysis report");
      try {
        result = validatorService.getProjectTotalsWithAccuracyDetails(idType, id);
        LOGGER.trace("Successfully fetched all project statistics for dashboard");
        resultAccuracyAndValidatedFields = validatorService.getFieldAccuracyAndFilesValidated(id);
        LOGGER.trace("Successfully fetched all field accuracy and file validation");
        resultGroupTimeSpend = validatorService.getTimeSpend(id);
        LOGGER.trace("Successfully fetched the time spend for validation");
        List<AccuracyProcessingDetails> vBotList = new ArrayList<AccuracyProcessingDetails>();
        List<FieldAccuracyDashboardDetails> detail = new ArrayList<FieldAccuracyDashboardDetails>();
        List<FieldValidation> detailValidated = new ArrayList<FieldValidation>();
        List<GroupTimeSpentDetails> detailGroupTimeSpend = new ArrayList<GroupTimeSpentDetails>();
        List<ClassificationFieldDetails> detailClassificationDetail = new ArrayList<ClassificationFieldDetails>();

        ProjectTotalsWithAccuracyDetails projDetails = new ProjectTotalsWithAccuracyDetails();
        Object[] obj1;
        LOGGER.trace(messageSource
            .getMessage(Constants.PREPARING_PROJECT_TOTAL_WITH_ACCURACYDETAILS, null,
                Locale.getDefault()));
        if (result.size() > 0) {
          obj1 = (Object[]) result.get(0);
          projDetails.setTotalFilesProcessed(Integer.parseInt(obj1[1].toString()));
          projDetails.setTotalSTP(Double.parseDouble(obj1[2].toString()));
          projDetails.setTotalFilesUploaded(Integer.parseInt(obj1[3].toString()));
          projDetails.setTotalAccuracy(Double.parseDouble(obj1[4].toString()));
          projDetails.setTotalFilesToValidation(Integer.parseInt(obj1[5].toString()));
          projDetails.setTotalFilesValidated(Integer.parseInt(obj1[6].toString()));
          projDetails.setTotalFilesUnprocessable(Integer.parseInt(obj1[7].toString()));
        }
        LOGGER.trace(messageSource
            .getMessage(Constants.DONE_PREPARING_PROJECT_TOTAL_WITH_ACCURACYDETAILS, null,
                Locale.getDefault()));

        LOGGER.trace(messageSource
            .getMessage(Constants.PREPARING_FIELD_ACCURACY_DASHBOARD_DETAILS, null,
                Locale.getDefault()));
        for (int i = 0; i < resultAccuracyAndValidatedFields.size(); i++) {
          FieldAccuracyDashboardDetails newDetail = new FieldAccuracyDashboardDetails();
          FieldValidation newDetailValidated = new FieldValidation();
          Object[] obj2 = (Object[]) resultAccuracyAndValidatedFields.get(i);
          newDetail.setFieldId(obj2[0].toString());
          newDetailValidated.setName(obj2[0].toString());
          newDetail.setPercentFieldAccuracy(Double.parseDouble(obj2[1].toString()));
          newDetailValidated.setPercentValidated(Double.parseDouble(obj2[2].toString()));
          detail.add(newDetail);
          detailValidated.add(newDetailValidated);
        }
        LOGGER.trace(messageSource
            .getMessage(Constants.DONE_PREPARING_FIELD_ACCURACY_DASHBOARD_DETAILS, null,
                Locale.getDefault()));
        LOGGER.trace(messageSource
            .getMessage(Constants.PREPARING_RESULT_GROUP_TIMESPEND, null, Locale.getDefault()));
        for (int i = 0; i < resultGroupTimeSpend.size(); i++) {
          GroupTimeSpentDetails newDetail = new GroupTimeSpentDetails();
          Object[] obj2 = (Object[]) resultGroupTimeSpend.get(i);
          newDetail.setCategoryId(obj2[1].toString());
          newDetail.setTimeSpend(Double.parseDouble(obj2[2].toString()));
          detailGroupTimeSpend.add(newDetail);
        }

        LOGGER.trace(messageSource
            .getMessage(Constants.DONE_PREPARING_RESULT_GROUP_TIMESPEND, null,
                Locale.getDefault()));

        if (resultCategory != null) {
          LOGGER.trace(messageSource
              .getMessage(Constants.PREPARING_CLASSIFICATION_DETAILS_OBJECT, null,
                  Locale.getDefault()));
          long totalDocuments = resultCategory.totalDocuments;
          HashMap<String, Long> map = new HashMap<String, Long>();
          LOGGER.trace(messageSource
              .getMessage(Constants.PREPARING_CLASSIFICATION_DETAILS_OBJECT, null,
                  Locale.getDefault()));
          for (int i = 0; i < resultCategory.allCategoryDetail.size(); i++) {
            CategoryDetail newDetail = resultCategory.allCategoryDetail.get(i);
            for (int k = 0; k < newDetail.allFieldDetail.size(); k++) {
              String key = newDetail.allFieldDetail.get(k).fieldId;
              if (map.containsKey(key)) {
                long value = map.get(key);
                long newValue = value + newDetail.allFieldDetail.get(k).foundInTotalDocs;
                map.put(key, newValue);
              } else {
                map.put(key, newDetail.allFieldDetail.get(k).foundInTotalDocs);
              }
            }
          }
          Iterator it = map.entrySet().iterator();
          List<ClassificationFieldDetails> fieldClassifyDetails = new ArrayList<ClassificationFieldDetails>();
          while (it.hasNext()) {
            ClassificationFieldDetails details = new ClassificationFieldDetails();
            Map.Entry pair = (Map.Entry) it.next();
            details.setFieldid(pair.getKey().toString());
            details.setRepresentationPercent(
                100 * Double.parseDouble(pair.getValue().toString()) / totalDocuments);
            fieldClassifyDetails.add(details);
          }

          projDetails.setClassificationFieldDetails(fieldClassifyDetails);
          LOGGER.trace(messageSource
              .getMessage(Constants.DONE_PREPARING_CLASSIFICATION_DETAILS_OBJECT, null,
                  Locale.getDefault()));

        }
        projDetails.setFieldAccuracy(detail);
        projDetails.setFieldValidation(detailValidated);
        projDetails.setGroupTimeSpentDetails(detailGroupTimeSpend);
        customResponse.setData(projDetails);
        customResponse.setSuccess(true);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        res.status(500);
        customResponse.setErrors(messageSource.getMessage(
            Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));
        LOGGER.error(messageSource.getMessage(
            Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()), ex);

      } catch (Exception ex) {
        LOGGER.error(messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
        customResponse.setSuccess(false);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private Object getDocProcDetails(Request req, Response res) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{req.url()},
              Locale.getDefault()));
      CustomResponse customResponse = new CustomResponse();
      String idType = req.params(":idtype");
      String id = req.queryParams("id");
      List<Object> result = new ArrayList<>();
      try {
        LOGGER.trace(messageSource
            .getMessage(Constants.PREPARING_LIST_ACCURACYDETAILS, null, Locale.getDefault()));
        result = validatorService.getDocumentProcessingDetails(idType, id);
        LOGGER.trace("Successfully processed the document details");
        List<AccuracyProcessingDetails> vBotList = new ArrayList<AccuracyProcessingDetails>();
        for (int i = 0; i < result.size(); i++) {
          AccuracyProcessingDetails vBot = new AccuracyProcessingDetails();
          Object[] obj1 = (Object[]) result.get(i);
          vBot.setId(obj1[0].toString());
          vBot.setFailedFieldCounts((double) obj1[1]);
          vBot.setPassFieldCounts((double) obj1[2]);
          vBot.setTotalFieldCounts((double) obj1[3]);
          vBot.setFieldAccuracy(Double.parseDouble(obj1[4].toString()));
          vBot.setReviewFileCount((long) obj1[5]);
          vBot.setInvalidFileCount((long) obj1[6]);
          vBot.setFailedDocumentCount((long) obj1[7]);
          vBot.setPassedDocumentCounts((long) obj1[8]);
          vBot.setProcessedDocumentCounts((long) obj1[9]);
          vBot.setDocumentAccuracy((long) obj1[10]);
          vBot.setAverageReviewTime((long) obj1[11]);

          vBotList.add(vBot);
        }
        LOGGER.trace(messageSource
            .getMessage(Constants.DONE_PREPARING_LIST_ACCURACYDETAILS, null, Locale.getDefault()));
        customResponse.setData(vBotList);
        customResponse.setSuccess(true);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        res.status(500);
        customResponse.setErrors(messageSource.getMessage(
            Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));
        LOGGER.error(messageSource.getMessage(
            Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()), ex);
      } catch (Exception ex) {
        customResponse.setSuccess(false);
        LOGGER.error(messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private Object getReviewedVBotCount(Request req, Response res) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> messageSource.getMessage(
          Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI, new Object[]{req.url()},
          Locale.getDefault()));
      CustomResponse customResponse = new CustomResponse();
      List<Object> result;

      try {
        result = validatorService.getReviewedVBotCount();
        LOGGER.trace(messageSource
            .getMessage(Constants.PREPARING_REVIEW_RESOURCEBOTLIST, null, Locale.getDefault()));
        List<ReviewResourceVBot> vBotList = new ArrayList<ReviewResourceVBot>();
        for (int i = 0; i < result.size(); i++) {
          ReviewResourceVBot vBot = new ReviewResourceVBot();
          Object[] obj1 = (Object[]) result.get(i);
          vBot.setProjectId(obj1[0].toString());
          vBot.setProcessedCount((long) obj1[1]);
          vBot.setFailedFileCount((long) obj1[2]);
          vBot.setReviewFileCount((long) obj1[3]);
          vBot.setInvalidFileCount((long) obj1[4]);
          vBotList.add(vBot);
        }
        LOGGER.trace(messageSource.getMessage(
            Constants.DONE_PREPARING_REVIEW_RESOURCEBOTLIST, null, Locale.getDefault()));
        customResponse.setData(vBotList);
        customResponse.setSuccess(true);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        res.status(500);
        customResponse.setErrors(messageSource.getMessage(
            Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));

        LOGGER.error(messageSource.getMessage(
            Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()), ex);

      } catch (Exception ex) {
        customResponse.setSuccess(false);
        LOGGER.error(messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String getInvalidMarkTypes(Request req, Response res) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> messageSource.getMessage(
          Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI, new Object[]{req.url()},
          Locale.getDefault()));
      CustomResponse customResponse = new CustomResponse();
      List<InvalidFileTypes> result;
      try {
        result = validatorService.getInvalidMarkTypes();
        LOGGER.trace("Successfully fetched the invalid marktypes");
        customResponse.setData(result);
        customResponse.setSuccess(true);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        res.status(500);
        customResponse.setErrors(messageSource.getMessage(
            Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));

        LOGGER.error(messageSource.getMessage(
            Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()), ex);

      } catch (Exception ex) {
        customResponse.setSuccess(false);
        LOGGER.error(messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String getFailedVBotCount(Request request, Response res) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{request.url()}, Locale.getDefault()));
      CustomResponse customResponse = new CustomResponse();
      int result = 0;
      try {
        String projectId = request.params(":prjid");
        String orgId = request.params(":orgid");
        result = validatorService.getFailedDocumentCountsByProject(orgId, projectId);
        LOGGER.trace("Successfully fetched the failed document counts");
        customResponse.setData(result);
        customResponse.setSuccess(true);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        res.status(500);
        customResponse.setErrors(messageSource.getMessage(
            Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));

        LOGGER.error(messageSource.getMessage(
            Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()), ex);

      } catch (HaltException ex) {
        res.status(404);
        customResponse.setErrors(ex.body());
        customResponse.setSuccess(false);
        LOGGER.error(messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
      } catch (Exception ex) {
        LOGGER.error(messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex);
        customResponse.setSuccess(false);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String getFailedVBot(Request request, Response response) {
    try {
      CustomResponse customResponse = new CustomResponse();
      try {
        LOGGER.entry();
        LOGGER.debug(() -> messageSource.getMessage(
            Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI, new Object[]{request.url()},
            Locale.getDefault())
        );
        String projectId = request.params(":prjid");
        String userName = request.headers(HEADER_USERNAME_KEY);
        String orgId = request.params("orgid");

        VisionBotDocument result = visionBotValidatorService
            .getAndLockFailedVBot(orgId, projectId, userName);

        if (result != null) {
          LOGGER.trace(() -> messageSource.getMessage(
              Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{result.getVisionBotId()}, Locale.getDefault())
          );
          customResponse.setData(result);
          customResponse.setSuccess(true);

        } else {
          response.status(404);
          customResponse.setSuccess(false);

          String msg = messageSource.getMessage(
              PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE, null, Locale.getDefault()
          );
          LOGGER.error(msg);
          customResponse.setErrors(msg);
        }

      } catch (DatabaseConnectionException ex) {
        response.status(500);
        customResponse.setSuccess(false);

        String msg = messageSource.getMessage(DATABASE_CONNECTION_DOWN, null, Locale.getDefault());
        customResponse.setErrors(msg);
        LOGGER.error(msg, ex);

      } catch (HaltException ex) {
        response.status(404);
        customResponse.setErrors(ex.body());
        customResponse.setSuccess(false);
        LOGGER.error(() -> messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex
        );

      } catch (Exception ex) {
        response.status(500);
        customResponse.setSuccess(false);
        LOGGER.error(() -> messageSource.getMessage(
            Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()), ex
        );
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String updateVBotUnlockUser(Request request, Response response) {
    try {
      CustomResponse customResponse = new CustomResponse();
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{request.url()}, Locale.getDefault()));
      try {
        String orgId = request.params(":orgid");
        String projectId = request.params(":prjid");
        String userName = request.headers(HEADER_USERNAME_KEY);
        response.status(200);
        validatorService.updateResourceUserId(projectId, "", userName,
            ValidatorOperationTypeEnum.Unlock); //release that specific user lock
        customResponse.setSuccess(true);
        customResponse.setData(new Object() {
          boolean success = true;
        });
        return getResponse(customResponse);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        response.status(500);
        customResponse.setErrors(
            messageSource
                .getMessage(Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));
        LOGGER.error(
            messageSource.getMessage(Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()),
            ex);
      } catch (Exception ex) {
        response.status(404);
        customResponse.setSuccess(false);
        customResponse.setErrors(Constants.UNABLE_TO_UNLOCK_USER);
        LOGGER
            .error(
                messageSource.getMessage(Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()),
                ex);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String markInvalidFiles(Request request, Response response) {

    CustomResponse customResponse = new CustomResponse();
    try {
      LOGGER.entry();

      String fileId = ofNullable(request.params(":fileid"))
          .orElseThrow(() -> new InvalidParameterException("Invalid file ID request parameter"));
      String username = ofNullable(request.headers(HEADER_USERNAME_KEY))
          .orElseThrow(() -> new InvalidParameterException("Invalid username header parameter"));

      List<InvalidFileDto> invalidFiles;
      try {
        invalidFiles = mapper.readValue(request.body(), new TypeReference<List<InvalidFileDto>>() {
        });
      } catch (IOException e) {
        throw new InvalidParameterException("Invalid list of InvalidFile into body", e);
      }

      visionBotValidatorService.markInvalidFiles(
          username, fileId, dtoMapper.getInvalidFiles(invalidFiles)
      );

      visionBotValidatorService.exportInvalidFile(fileId);

      LOGGER.debug("Successfully marked file %s as invalid", fileId);
      customResponse.setSuccess(true);

    } finally {
      LOGGER.exit();
    }
    return getResponse(customResponse);
  }

  private String updateVBotData(Request request, Response response) {
    try {
      CustomResponse customResponse = new CustomResponse();
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{request.url()}, Locale.getDefault()));
      try {
        String fileId = request.params(":fileid");
        String projectId = request.params(":prjid");
        String userName = request.headers(HEADER_USERNAME_KEY);
        //String correctedData = request.body();
        String orgId = request.params(":orgid");

        response.status(200);

        String dataObject = request.body();
        @SuppressWarnings("unchecked")
        Map<String, Object> dictionary = mapper.readValue(dataObject, HashMap.class);
        String correctedData = mapper.writerWithDefaultPrettyPrinter()
            .writeValueAsString(dictionary.get("vBotDocument"));
        //  DocumentProcessingDetails details = (DocumentProcessingDetails)(dictionary.get("documentProcessingDetails"));
        FieldAccuracyDetails[] details = mapper.readValue(
            mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(dictionary.get("fieldAccuracyList")),
            FieldAccuracyDetails[].class);
        float passAccuracyValue = 0;
        if (details != null) {
          for (int i = 0; i < details.length; i++) {
            details[i].setFileId(fileId);
            passAccuracyValue += details[i].getPassAccuracyValue();
          }
        }

        validatorService.updateResourceCorrectedValue(fileId, projectId, userName, correctedData,
            passAccuracyValue, details, ValidatorOperationTypeEnum.Save);
        customResponse.setSuccess(true);
        customResponse.setData(new Object() {
          boolean success = true;
        });

        return getResponse(customResponse);
      } catch (DataNotFoundException dnfe) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        response.status(404);
        customResponse.setErrors(dnfe.getMessage());
        LOGGER.error(dnfe.getMessage(), dnfe);
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        response.status(500);
        customResponse.setErrors(
            messageSource
                .getMessage(Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));
        LOGGER.error(
            messageSource.getMessage(Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()),
            ex);
      } catch (Exception ex) {
        response.status(404);
        customResponse.setErrors("Unable to save data");
        customResponse.setSuccess(false);
        LOGGER
            .error(
                messageSource.getMessage(Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()),
                ex);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String patchVBotData(Request request, Response response) {
    try {
      LOGGER.entry();
      CustomResponse customResponse = new CustomResponse();

      // Validate Header
      String contentType = request.headers(HttpHeaders.CONTENT_TYPE);
      if (!CONTENT_TYPE_JSON_PATCH.equalsIgnoreCase(contentType)) {
        response.status(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE);
        response.header(HEADER_ACCEPT_PATCH, CONTENT_TYPE_JSON_PATCH);
        customResponse.setSuccess(false);
        customResponse.setErrors("[\"Unsupported media type\"]");

      } else {

        try (ServletInputStream body = request.raw().getInputStream()) {

          String visionBotID = request.params(":fileid");
          String username = request.headers(HEADER_USERNAME_KEY);

          visionBotValidatorService.patchVisionBot(visionBotID, username, body);
          visionBotValidatorService.exportSuccessFile(visionBotID);

          response.status(HttpStatus.SC_OK);
          customResponse.setSuccess(true);

        } catch (InvalidJsonPatch e) {
          response.status(HttpStatus.SC_BAD_REQUEST);
          customResponse.setSuccess(false);
          customResponse.setErrors("[\"Invalid JSON Patch Document [" + e.getMessage() + "]\"]");
          LOGGER.error("Invalid JSON Patch document", e);

        } catch (JsonValidatorException | JsonPatchException e) {
          response.status(HttpStatus.SC_UNPROCESSABLE_ENTITY);
          customResponse.setSuccess(false);
          customResponse.setErrors("[\"Error to apply patch: " + e.getMessage() + "\"]");
          LOGGER.error("Error to apply patch", e);

        } catch (Exception e) {
          response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR);
          customResponse.setSuccess(false);
          customResponse.setErrors("[\"Unexpected error to apply PATCH: " + e.getMessage() + "\"]");
          LOGGER.error("Unexpected error to apply PATCH", e);
        }
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String addVBotData(Request request, Response response) {
    try {
      CustomResponse customResponse = new CustomResponse();
      LOGGER.entry();
      LOGGER.debug(() -> messageSource
          .getMessage(Constants.VALIDATOR_VALIDATORRESOURCEHANDLER_INPUT_URI,
              new Object[]{request.url()}, Locale.getDefault()));
      String orgId = request.params(":orgid");
      String fileId = request.params(":fileid");
      String prjId = request.params(":prjid");
      String validationStatus = request.params(":validationstatus");
      String visionbotId = request.params(":visionbotid");

      if (!((validationStatus.equalsIgnoreCase(ValidationType.Pass.toString())) ||
          (validationStatus.equalsIgnoreCase(ValidationType.Fail.toString())))) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        response.status(404);
        customResponse.setErrors(messageSource
            .getMessage(Constants.VALIDATION_STATUS_WRONGLY_ENTERED, null, Locale.getDefault()));
        return getResponse(customResponse);
      }

      try {
        String dataObject = request.body();
        @SuppressWarnings("unchecked")
        Map<String, Object> dictionary = mapper.readValue(dataObject, HashMap.class);
        DocumentProcessingDetails details = mapper.readValue(
            mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(dictionary.get("documentProcessingDetails")),
            DocumentProcessingDetails.class);
        if (details == null) {
          customResponse.setData(null);
          customResponse.setSuccess(false);
          response.status(404);
          customResponse.setErrors(messageSource
              .getMessage(Constants.VALIDATION_STATUS_WRONGLY_ENTERED, null, Locale.getDefault()));
          return getResponse(customResponse);
        } else {
          String data = mapper.writerWithDefaultPrettyPrinter()
              .writeValueAsString(dictionary.get("vBotDocument"));

          FieldAccuracyDetails[] fieldAccuracyDetails = mapper.readValue(
              mapper.writerWithDefaultPrettyPrinter()
                  .writeValueAsString(dictionary.get("fieldAccuracyList")),
              FieldAccuracyDetails[].class);
          for (int i = 0; i < fieldAccuracyDetails.length; i++) {
            fieldAccuracyDetails[i].setFileId(fileId);
          }

          boolean successData = validatorService.addVisionBot(
              fileId, orgId, prjId, validationStatus, visionbotId, fieldAccuracyDetails, data,
              details.getFailedFieldCounts(),
              details.getPassFieldCounts(),
              details.getTotalFieldCounts()
          );

          customResponse.setSuccess(successData);
          customResponse.setData(new Object() {
            boolean success = successData;
          });

          return getResponse(customResponse);
        }
      } catch (DatabaseConnectionException ex) {
        customResponse.setData(null);
        customResponse.setSuccess(false);
        response.status(500);
        customResponse.setErrors(
            messageSource
                .getMessage(Constants.DATABASE_CONNECTION_DOWN, null, Locale.getDefault()));
        LOGGER.error(
            messageSource.getMessage(Constants.DATABASE_ERROR_OCCURRED, null, Locale.getDefault()),
            ex);
      } catch (Exception ex) {
        response.status(404);
        customResponse.setErrors("Unable to save data");
        customResponse.setSuccess(false);
        LOGGER
            .error(
                messageSource.getMessage(Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()),
                ex);
      }
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String getProductionSummaryDetails(Request request, Response response) {
    try {
      CustomResponse customResponse = new CustomResponse();
      ProductionFilesSummary summary = new ProductionFilesSummary();
      LOGGER.entry();
      try {
        String organizationId = request.params("orgid");
        String projectId = request.params("prjid");

        List<Object> result = validatorService.getProductionFileSummary(projectId);
        if (result.size() > 0) {
          Object[] obj1 = (Object[]) result.get(0);
          summary.setProjectId(obj1[0].toString());
          summary.setProcessedFiles((long) obj1[1]);
          summary.setSuccessFiles((long) obj1[2]);
          summary.setReviewFiles((long) obj1[3]);
          summary.setInvalidFiles((long) obj1[4]);
          summary.setPendingForReview((long) obj1[5]);
          summary.setAccuracy(Math.floor((double) obj1[6]));
        }
      } catch (ArrayIndexOutOfBoundsException ex) {
        LOGGER.warn(messageSource
                .getMessage(Constants.VALIDATOR_WARNING_INSUFFICIENT_DATA, null, Locale.getDefault()),
            ex);
      } catch (DatabaseConnectionException ex) {
        throw ex;
      } catch (Exception ex) {
        LOGGER
            .error(
                messageSource.getMessage(Constants.EXCEPTION_OCCURRED, null, Locale.getDefault()),
                ex);
        throw ex;
      }
      customResponse.setData(summary);
      customResponse.setSuccess(true);
      return getResponse(customResponse);

    } finally {
      LOGGER.exit();
    }
  }

  private String unlockDocuments(Request req, Response res) {
    try {
      LOGGER.entry();
      CustomResponse customResponse = new CustomResponse();

      String username = req.headers("username");
      visionBotValidatorService.unlockDocuments(username);

      customResponse.setSuccess(true);
      res.type("application/json");

      return getResponse(customResponse);
    } finally {
      LOGGER.exit();
    }
  }

  /**
   * Get the heartbeat of the service
   */
  private Object getHeartBeatInfo(Response response) {
    try {
      LOGGER.entry();
      String heartBeat;
      //currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization
      response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
      heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
      response.body(heartBeat);
      return response;

    } finally {
      LOGGER.exit();
    }
  }
}
