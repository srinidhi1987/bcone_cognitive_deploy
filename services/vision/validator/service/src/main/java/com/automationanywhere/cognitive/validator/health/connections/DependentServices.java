package com.automationanywhere.cognitive.validator.health.connections;

/**
 * @author shweta.thakur
 * This class will hold Dependent micro-services list
 */
public enum DependentServices {
  FileManager, Project
}
