package com.automationanywhere.cognitive.validator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class VisionBotDto {

  private final LayoutDto layout;
  private final SegmentedDocDto segmentedDoc;
  private final DataDto data;
  private final SirFieldsDto sirFieldsDto;
  private final DocPropertiesDto docProperties;

  public VisionBotDto(
      @JsonProperty("Layout") final LayoutDto layout,
      @JsonProperty("SegmentedDoc") final SegmentedDocDto segmentedDoc,
      @JsonProperty("Data") final DataDto data,
      @JsonProperty("SirFields") final SirFieldsDto sirFieldsDto,
      @JsonProperty("DocumentProperties") final DocPropertiesDto docProperties
  ) {
    this.layout = layout;
    this.segmentedDoc = segmentedDoc;
    this.data = data;
    this.sirFieldsDto = sirFieldsDto;
    this.docProperties = docProperties;
  }

  /**
   * Returns the layout.
   *
   * @return the value of layout
   */
  @JsonProperty("Layout")
  public LayoutDto getLayout() {
    return layout;
  }

  /**
   * Returns the segmentedDoc.
   *
   * @return the value of segmentedDoc
   */
  @JsonProperty("SegmentedDoc")
  public SegmentedDocDto getSegmentedDoc() {
    return segmentedDoc;
  }

  /**
   * Returns the data.
   *
   * @return the value of data
   */
  @JsonProperty("Data")
  public DataDto getData() {
    return data;
  }

  /**
   * Returns the sirFieldsDto.
   *
   * @return the value of sirFieldsDto
   */
  @JsonProperty("SirFields")
  public SirFieldsDto getSirFieldsDto() {
    return sirFieldsDto;
  }

  /**
   * Returns the docProperties.
   *
   * @return the value of docProperties
   */
  @JsonProperty("DocumentProperties")
  public DocPropertiesDto getDocProperties() {
    return docProperties;
  }
}
