package com.automationanywhere.cognitive.exporter.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.exporter.contracts.CsvDataGenerator;
import com.automationanywhere.cognitive.validator.RpcClient;
import com.automationanywhere.cognitive.validator.datalayer.ValidatorDetailProvider;
import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mukesh.Methaniya on 22-03-2017.
 */
@Component
public class CsvDataGeneratorImpl implements CsvDataGenerator {

  private static final AALogger LOGGER = AALogger.create(CsvDataGeneratorImpl.class);

  private final ValidatorDetailProvider validatorDetailProvider;
  private final ConnectionFactory rpcConnectionFactory;
  private final ObjectMapper mapper;

  @Autowired
  public CsvDataGeneratorImpl(
      final ValidatorDetailProvider validatorDetailProvider,
      final ConnectionFactory rpcConnectionFactory,
      final ObjectMapper mapper
  ) {
    this.validatorDetailProvider = validatorDetailProvider;
    this.rpcConnectionFactory = rpcConnectionFactory;
    this.mapper = mapper;
  }

  public String GetExportCsv(String fileId) {
    try {
      LOGGER.entry();
      LOGGER.debug(() -> "FileId" + fileId);

      String result;
      String deserializedResult;
      VisionBotDataDetail processedData = validatorDetailProvider.getDocumentByFileId(fileId);

      String organizationId = processedData.getOrgId();
      String projectId = processedData.getProjectId();

      String vBotDocument = processedData.getCorrectedData();

      if (vBotDocument == null || vBotDocument.trim().isEmpty()) {
        LOGGER.error("VisionBot data is not available.");
        throw new RuntimeException("Corrected data is not available.");
      }

      Map<String, Object> dictionary = new HashMap<>();
      dictionary.put("organizationId", organizationId);
      dictionary.put("projectId", projectId);
      dictionary.put("vbotDocument", vBotDocument);

      String requestData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dictionary);
      RpcClient client = new RpcClient("QUEUE_ExportData", rpcConnectionFactory);
      LOGGER.trace("Making call to RPC Client");

      try {
        result = client.call(requestData);
      } finally {
        LOGGER.trace("Done call to RPC Client");
        client.close();
      }

      deserializedResult = mapper.readValue(result, String.class);
      LOGGER.trace("Result", deserializedResult);

      return deserializedResult;

    } catch (Exception ex) {
      throw new RuntimeException("Unable to generate csv from vbotdocument.", ex);

    } finally {
      LOGGER.exit();
    }
  }
}
