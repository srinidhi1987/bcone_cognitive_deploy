package com.automationanywhere.cognitive.validator.exception.customexceptions;

public class DependentServiceConnectionFailureException extends RuntimeException {

  private static final long serialVersionUID = -4885802843769207881L;

  public DependentServiceConnectionFailureException() {
    super();
  }

  public DependentServiceConnectionFailureException(String message) {
    super(message);
  }

}