package com.automationanywhere.cognitive.validator.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Keval.Sheth on 12-23-2016.
 */

@Entity
@Table(name = "InvalidFiles")
public class InvalidFiles extends EntityModelBase {

  @Id
  @Column(name = "FileId")
  @JsonProperty(value = "fileId")
  private String FileId;
  @JsonProperty(value = "reasonId")
  @Column(name = "ReasonId")
  private String ReasonId;
  @JsonProperty(value = "reasonText")
  @Column(name = "ReasonText")
  private String ReasonText;


  public String getFileId() {
    return this.FileId;
  }

  public void setFileId(String fileId) {
    this.FileId = fileId;
  }

  public String getInvalidId() {
    return this.ReasonId;
  }

  public void setInvalidId(String invalidId) {
    this.ReasonId = invalidId;
  }

  public String getReasonText() {
    return this.ReasonText;
  }

  public void setReasonText(String reason) {
    this.ReasonText = reason;
  }


  @Override
  public Map<String, Object> getCustomQuery(StringBuilder queryString, String queryId,
      String idtype, String id) {
    return null;
  }

  @Override
  public Map<String, Object> getCustomQueryForDistinctElements(StringBuilder queryString,
      String parameter) {
    return null;
  }
}