package com.automationanywhere.cognitive.validator.exception;

public class InvalidJsonData extends RuntimeException {

  private static final long serialVersionUID = 8646449016104171038L;

  public InvalidJsonData(String message, Throwable cause) {
    super(message, cause);
  }
}
