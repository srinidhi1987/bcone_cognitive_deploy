package com.automationanywhere.cognitive.generic.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DependentServiceConnectionFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.testng.annotations.Test;

public class ProjectManagerAdapterImplTest {

  @Test
  @SuppressWarnings("unchecked")
  public void testOfTestConnectionWhenRestCallToProjectServiceReturnsOK() {
    // GIVEN
    RestTemplateWrapper restTemplateWrapper = mock(RestTemplateWrapper.class);
    ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

    when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
    doReturn(responseEntity).when(restTemplateWrapper).exchangeGet(anyString(), any());

    ProjectManagerAdapterImpl adapter = new ProjectManagerAdapterImpl(
        "http://localhost:9999",
        null,
        restTemplateWrapper
    );

    // WHEN
    Throwable throwable = catchThrowable(adapter::testConnection);

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testOfTestConnectionWhenRestCallToProjectServiceReturnsNotOK() {
    // GIVEN
    RestTemplateWrapper restTemplateWrapper = mock(RestTemplateWrapper.class);
    ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

    when(responseEntity.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
    doReturn(responseEntity).when(restTemplateWrapper).exchangeGet(anyString(), any());

    ProjectManagerAdapterImpl adapter = new ProjectManagerAdapterImpl(
        "http://localhost:9999",
        null,
        restTemplateWrapper
    );

    // WHEN
    Throwable throwable = catchThrowable(adapter::testConnection);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(DependentServiceConnectionFailureException.class)
        .hasMessage("Error while connecting to Project service");
  }
}
