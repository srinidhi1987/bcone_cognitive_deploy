package com.automationanywhere.cognitive.validator.datalayer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.validator.models.VisionBotDataDetail;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.context.MessageSource;
import org.testng.annotations.Test;

/**
 * Created by keval.sheth on 14-02-2017.
 */

public class ValidatorDetailProviderTest {

  @Test
  public void getFailedDocumentByUser_SingleProjectIdAndUserIdPresentInDatabase_ReturnsThatElement() {
    // given
    final String organizationId = "234";
    final String projectId = "346";
    final String userName = "dsfsadf";

    VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
    visionBotDataDetail.setOrgId(organizationId);
    visionBotDataDetail.setProjectId(projectId);
    visionBotDataDetail.setLockedUserId(userName);

    List<VisionBotDataDetail> selectResult = new ArrayList<>();
    selectResult.add(visionBotDataDetail);

    IValidatorServiceDataAccessLayer dataLayer = mock(ValidatorServiceDataAccessLayer.class);
    MessageSource messageSource = mock(MessageSource.class);
    when(dataLayer.Select(eq(VisionBotDataDetail.class), any(DetachedCriteria.class), anyInt()))
        .thenReturn(selectResult);

    ValidatorDetailProvider sut = new ValidatorDetailProvider(messageSource, dataLayer);

    // when
    VisionBotDataDetail actualResult = sut
        .getFailedDocumentByUser(organizationId, projectId, userName);

    // then
    assertThat(actualResult).isNotNull();
    assertThat(actualResult.getOrgId()).isEqualTo(organizationId);
    assertThat(actualResult.getProjectId()).isEqualTo(projectId);
    assertThat(actualResult.getLockedUserId()).isEqualTo(userName);
  }

//    @Test
//    public void GetFailedDocumentByUser_ProjectIdAndUserIdNotInDatabase_ReturnsNull() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        VisionBotDataDetail actualResult = sut.getFailedDocumentByUser(null, null, null);
//
//        // ACT
//
//        Assert.assertNull(actualResult);
//
//    }
//
//    @Test
//    public void GetFailedDocumentByUser_MultipleProjectIdAndUserIdPresentInDatabase_ReturnsFirstElementAndLogDetail() {
//        // ARRANGE
//
//        final String organizationId = "234";
//        final String projectId = "346";
//        final String userName = "dsfsadf";
//
//        VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
//        visionBotDataDetail.setOrgId(organizationId);
//        visionBotDataDetail.setProjectId(projectId);
//        visionBotDataDetail.setlockedUserId(userName);
//
//        List<Object> selectResult = new ArrayList<>();
//        selectResult.add(visionBotDataDetail);
//        selectResult.add(visionBotDataDetail);
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        VisionBotDataDetail actualResult = sut.getFailedDocumentByUser(organizationId, projectId, userName);
//
//        // ACT
//
//        Assert.assertNotNull(actualResult);
//        Assert.assertEquals(actualResult.getOrgId(), organizationId);
//        Assert.assertEquals(actualResult.getProjectId(), projectId);
//        Assert.assertEquals(actualResult.getLockedUserId(), userName);
//
//    }
//
//    @Test
//    public void GetFailedDocumentByUser_ExceptionOccurredInSelectCriteria_ThrowsDatabaseConnectionException() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//        // selectResult.add(new DatabaseConnectionException());
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        Mockito.doThrow(new RuntimeException()).when(dataLayer).Select(any(), any());
//        // when(dataLayer.Select(any(), any())).thenThrow(new Exception());
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//        try {
//            VisionBotDataDetail actualResult = sut.getFailedDocumentByUser(null, null, null);
//        } catch (DatabaseConnectionException ex) {
//            Assert.assertTrue(true);
//        }
//
//
//    }
//
//    @Test
//    public void GetFailedDocumentByUser_AnyProjectIdAndUserId_SetsThemIntoDetachedCriteriaAndPassToSelectFunction() {
//        // ARRANGE
//
//
//        // ASSERT
//
//
//        // ACT
//
//
//    }
//
//
//    @Test
//    public void GetFailedDocumentCountsByProject_SingleProjectIdAndValidationStatusFailPresentInDatabase_ReturnsThatElement() {
//
//        // ARRANGE
//
//        final String organizationId = "234";
//        final String projectId = "346";
//        final String validationType = ValidationType.Fail.toString();
//
//        VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
//        visionBotDataDetail.setOrgId(organizationId);
//        visionBotDataDetail.setProjectId(projectId);
//        visionBotDataDetail.setValidationStatus(validationType);
//
//        List<Object> selectResult = new ArrayList<>();
//        selectResult.add(visionBotDataDetail);
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        int actualResult = sut.getFailedDocumentCountsByProject(organizationId, projectId);
//
//        // ACT
//
//        Assert.assertNotNull(actualResult);
//        Assert.assertEquals(selectResult.size(), actualResult);
//    }
//
//    @Test
//    public void GetFailedDocumentCountsByProject_ProjectIdAndValidationStatusFailNotInDatabase_ReturnsZero() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        int actualResult = sut.getFailedDocumentCountsByProject(null, null);
//
//        // ACT
//
//        Assert.assertEquals(selectResult.size(), actualResult);
//
//    }
//
//    @Test
//    public void GetFailedDocumentCountsByProject_ExceptionOccurredInSelectCriteria_ThrowsDatabaseConnectionException() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//        // selectResult.add(new DatabaseConnectionException());
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        Mockito.doThrow(new RuntimeException()).when(dataLayer).Select(any(), any());
//        // when(dataLayer.Select(any(), any())).thenThrow(new Exception());
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//        try {
//            int actualResult = sut.getFailedDocumentCountsByProject(null, null);
//        } catch (DatabaseConnectionException ex) {
//            Assert.assertTrue(true);
//        }
//
//
//    }
//
//
//    @Test
//    public void GetDocumentByFileId_FileIdPresentInDatabase_ReturnsThatElement() {
//
//        // ARRANGE
//
//        final String organizationId = "234";
//        final String fileId = "346";
//        // final String validationType = ValidationType.Fail.toString();
//
//        VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
//        visionBotDataDetail.setOrgId(organizationId);
//        visionBotDataDetail.setFileId(fileId);
//
//        List<Object> selectResult = new ArrayList<>();
//        selectResult.add(visionBotDataDetail);
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        VisionBotDataDetail actualResult = sut.getDocumentByFileId(fileId);
//
//        // ACT
//
//        Assert.assertNotNull(actualResult);
//        Assert.assertEquals(actualResult.getOrgId(), organizationId);
//        Assert.assertEquals(actualResult.getFileId(), fileId);
//    }
//
//    @Test
//    public void GetDocumentByFileId_FileIdNotInDatabase_ReturnsNull() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        VisionBotDataDetail actualResult = sut.getDocumentByFileId(null);
//
//        // ACT
//
//        Assert.assertNull(actualResult);
//
//    }
//
//    @Test
//    public void GetDocumentByFileId_ExceptionOccurredInSelectCriteria_ThrowsDatabaseConnectionException() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//        // selectResult.add(new DatabaseConnectionException());
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        Mockito.doThrow(new RuntimeException()).when(dataLayer).Select(any(), any());
//        // when(dataLayer.Select(any(), any())).thenThrow(new Exception());
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//        try {
//            VisionBotDataDetail actualResult = sut.getDocumentByFileId(null);
//        } catch (DatabaseConnectionException ex) {
//            Assert.assertTrue(true);
//        }
//
//
//    }
//
//    @Test
//    public void checkForInvalidProjectId_ProjectIdPresentInDatabase_ReturnsThatElement() {
//
//        // ARRANGE
//
//        final String projectId = "346";
//        VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
//        visionBotDataDetail.setProjectId(projectId);
//
//        List<Object> selectResult = new ArrayList<>();
//        selectResult.add(visionBotDataDetail);
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.checkForInvalidProjectId(projectId);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, false);
//    }
//
//    @Test
//    public void checkForInvalidProjectId_ProjectIdNotInDatabase_ReturnsNull() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.checkForInvalidProjectId(null);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, true);
//
//    }
//
//
//    @Test
//    public void checkForInvalidFileId_FileIdPresentInDatabase_ReturnsThatElement() {
//
//        // ARRANGE
//
//        final String fileId = "346";
//        VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
//        visionBotDataDetail.setFileId(fileId);
//
//        List<Object> selectResult = new ArrayList<>();
//        selectResult.add(visionBotDataDetail);
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.checkForInvalidFileId(fileId);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, false);
//    }
//
//    @Test
//    public void checkForInvalidFileId_FileIdNotInDatabase_ReturnsNull() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.checkForInvalidFileId(null);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, true);
//
//    }
//
//    @Test
//    public void checkForInvalidOrganizationId_OrganizationIdPresentInDatabase_ReturnsThatElement() {
//
//        // ARRANGE
//
//        final String organizationId = "1";
//        VisionBotDataDetail visionBotDataDetail = new VisionBotDataDetail();
//        visionBotDataDetail.setOrgId(organizationId);
//
//        List<Object> selectResult = new ArrayList<>();
//        selectResult.add(visionBotDataDetail);
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.checkForInvalidOrganizationId(organizationId);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, false);
//    }
//
//    @Test
//    public void checkForInvalidOrganizationId_OrganizationIdNotInDatabase_ReturnsNull() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.checkForInvalidOrganizationId(null);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, true);
//
//    }
//
//    @Test
//    public void getInvalidMarkTypes_ReturnsInvalidMarkTypes() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//
//        InvalidFileTypes invalidFileType = new InvalidFileTypes();
//        invalidFileType.setInvalidReason("Field is not found");
//        selectResult.add(invalidFileType);
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Select(any(), any())).thenReturn(selectResult);
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//
//        List<InvalidFileTypes> actualResult = sut.getInvalidMarkTypes();
//
//        // ACT
//
//        Assert.assertEquals(actualResult, selectResult);
//
//    }
}

