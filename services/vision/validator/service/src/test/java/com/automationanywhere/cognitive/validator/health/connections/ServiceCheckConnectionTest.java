package com.automationanywhere.cognitive.validator.health.connections;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.generic.contracts.FileManagerAdapter;
import com.automationanywhere.cognitive.generic.contracts.ProjectManagerAdapter;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DependentServiceConnectionFailureException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ServiceCheckConnectionTest {

  @Mock
  FileManagerAdapter fileManagerAdapter;
  @Mock
  ProjectManagerAdapter projectManagerAdapter;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testOfCheckConnectionForServiceWhenAllServiceConnectionSuccess() {

    // given
    List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity fileConn = new ServiceConnectivity("FileManager", HTTPStatusCode.OK);
    ServiceConnectivity projectConn = new ServiceConnectivity("Project", HTTPStatusCode.OK);
    servicesListExpected.add(fileConn);
    servicesListExpected.add(projectConn);
    ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(fileManagerAdapter,
        projectManagerAdapter);
    Mockito.doNothing().when(fileManagerAdapter).testConnection();
    Mockito.doNothing().when(projectManagerAdapter).testConnection();

    // when
    List<ServiceConnectivity> servicesList = serviceCheckConnection.checkConnectionForService();

    int index = 0;
    // then
    for (ServiceConnectivity serviceConnectivity : servicesList) {
      ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
      assertThat(serviceConnectivity.getHTTPStatus())
          .isEqualTo(expectedServiceConnectivity.getHTTPStatus());
      assertThat(serviceConnectivity.getServiceName())
          .isEqualTo(expectedServiceConnectivity.getServiceName());
      index++;
    }

  }

  @Test
  public void testOfCheckConnectionForServiceWhenAllServiceConnectionFails() {

    // given
    List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity fileConn = new ServiceConnectivity("FileManager",
        HTTPStatusCode.INTERNAL_SERVER_ERROR);
    ServiceConnectivity projectConn = new ServiceConnectivity("Project",
        HTTPStatusCode.INTERNAL_SERVER_ERROR);
    servicesListExpected.add(fileConn);
    servicesListExpected.add(projectConn);
    ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(fileManagerAdapter,
        projectManagerAdapter);

    Mockito.doThrow(DependentServiceConnectionFailureException.class).when(fileManagerAdapter)
        .testConnection();
    Mockito.doThrow(DependentServiceConnectionFailureException.class).when(projectManagerAdapter)
        .testConnection();

    // when
    List<ServiceConnectivity> servicesList = serviceCheckConnection.checkConnectionForService();

    int index = 0;
    // then
    for (ServiceConnectivity serviceConnectivity : servicesList) {
      ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
      assertThat(serviceConnectivity.getHTTPStatus())
          .isEqualTo(expectedServiceConnectivity.getHTTPStatus());
      assertThat(serviceConnectivity.getServiceName())
          .isEqualTo(expectedServiceConnectivity.getServiceName());
      index++;
    }

  }
}
