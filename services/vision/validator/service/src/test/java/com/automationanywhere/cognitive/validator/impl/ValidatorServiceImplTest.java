package com.automationanywhere.cognitive.validator.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.validator.datalayer.IValidatorServiceDataAccessLayer;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by keval.sheth on 14-02-2017.
 */
public class ValidatorServiceImplTest {

  @Mock
  private IValidatorServiceDataAccessLayer validatorServiceDataAccessLayer;

  @InjectMocks
  private ValidatorServiceImpl validatorServiceImpl;

  @BeforeTest
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getProductionFileSummaryShouldReturnListOfObjectsWhenValidInputParameters() {
    //given
    String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";

    long processedFiles = 10l;
    long successFiles = 2l;
    long reviewFiles = 0l;
    long invalidFiles = 1l;
    long pendingForReview = 7l;
    double accuracy = 74.45;

    List<Object> expectedProductionFileSummaries = new ArrayList<>();
    expectedProductionFileSummaries.add(
        new Object[]{projectId, processedFiles, successFiles, reviewFiles, invalidFiles,
            pendingForReview, accuracy});

    when(validatorServiceDataAccessLayer.ExecuteCustomQuery(any(), any(), any(), any()))
        .thenReturn(expectedProductionFileSummaries);

    //when
    List<Object> actualProductionFileSummaries = validatorServiceImpl
        .getProductionFileSummary(projectId);

    for (int i = 0; i < expectedProductionFileSummaries.size(); i++) {
      Object[] expectedData = (Object[]) expectedProductionFileSummaries.get(i);
      Object[] actualData = (Object[]) actualProductionFileSummaries.get(i);

      assertThat(actualData).isEqualTo(expectedData);
    }

  }

//    @Test
//    public void getFailedVBot_ProjectIdAndUserIdPresentInDatabase_ReturnsThatElement() {
//        // ARRANGE
//
//        final String organizationId = "234";
//        final String projectId = "346";
//        final String userName = "dsfsadf";
//
//        VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        when(detailProvider.getFailedDocumentByUser(any(), any(), any())).thenReturn(expectedVisionBotDataDetail);
//
//        when(detailProvider.getFailedDocumentNotAllocatedToAnyUser(any(), any(), any())).thenReturn(expectedVisionBotDataDetail);
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(detailProvider);
//
//        // ASSERT
//
//        VisionBotDataDetail actualResult = sut.getFailedVBot(organizationId, projectId, userName);
//
//        // ACT
//
//        Assert.assertNotNull(actualResult);
//        Assert.assertEquals(actualResult, expectedVisionBotDataDetail);
//    }
//
//    @Test
//    public void getFailedVBot_ProjectIdAndUserIdNotPresentInDatabase_ReturnsNull() {
//
//        // ARRANGE
//
//
//        VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        when(detailProvider.getFailedDocumentByUser(any(), any(), any())).thenReturn(expectedVisionBotDataDetail);
//
//        when(detailProvider.getFailedDocumentNotAllocatedToAnyUser(any(), any(), any())).thenReturn(expectedVisionBotDataDetail);
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(detailProvider);
//
//        // ASSERT
//
//        VisionBotDataDetail actualResult = sut.getFailedVBot(null, null, null);
//
//        // ACT
//
//        Assert.assertNull(actualResult.getOrgId());
//        Assert.assertNull(actualResult.getProjectId());
//        Assert.assertNull(actualResult.getLockedUserId());
//    }
//
//
//    @Test
//    public void getFailedVBot_ExceptionOccurredInSelectCriteria_ThrowsDatabaseConnectionException() {
//
//        // ARRANGE
//
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//
//        Mockito.doThrow(new DatabaseConnectionException()).when(detailProvider).getFailedDocumentNotAllocatedToAnyUser(any(), any(), any());
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(detailProvider);
//
//        // ASSERT
//        try {
//            VisionBotDataDetail actualResult = sut.getFailedVBot(null, null, null);
//        } catch (DatabaseConnectionException ex) {
//            Assert.assertTrue(true);
//        }
//
//
//
//    }
//
//    @Test
//    public void addVisionBot_ValidationStatusFailInsertIntoDatabase_ReturnSuccess() {
//        // ARRANGE
//        final String fileid = "file1";
//        final String validationstatus = ValidationType.Fail.toString();
//        final String visionbotid = "232t36";
//        final String data = "data";
//        final String organizationId = "234";
//        final String projectId = "346";
//        final String userName = "dfgdfgd";
//
//       // VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//
//        boolean expectedResult = true;
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Insert(any())).thenReturn(expectedResult);
//
//      //  ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer);
//        // ASSERT
//
//        boolean actualResult = sut.addVisionBot(fileid,organizationId,projectId,validationstatus,visionbotid,data);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//
//    }
//
//    @Test
//    public void addVisionBot_ValidationStatusPassInsertIntoDatabase_ReturnSuccess() {
//        // ARRANGE
//        final String fileid = "file1";
//        final String validationstatus = ValidationType.Pass.toString();
//        final String visionbotid = "232t36";
//        final String data = "data";
//        final String organizationId = "234";
//        final String projectId = "346";
//        final String userName = "dfgdfgd";
//
//        // VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//
//        boolean expectedResult = true;
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        when(dataLayer.Insert(any())).thenReturn(expectedResult);
//
//        //  ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer);
//        // ASSERT
//
//        boolean actualResult = sut.addVisionBot(fileid,organizationId,projectId,validationstatus,visionbotid,data);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//
//    }
//
//    @Test
//    public void getFailedDocumentCountsByProject_ProjectIdAndOrganizationIdPresentInDatabase_ReturnsThatElement() {
//
//        // ARRANGE
//
//        final String organizationId = "234";
//        final String projectId = "346";
//
//        int expectedResult = 0;
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//       // when(detailProvider.getFailedDocumentByUser(any(), any(), any())).thenReturn(expectedVisionBotDataDetail);
//
//        when(detailProvider.getFailedDocumentCountsByProject(any(), any())).thenReturn(expectedResult);
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(detailProvider);
//
//        // ASSERT
//
//        int actualResult = sut.getFailedDocumentCountsByProject(organizationId, projectId);
//
//        // ACT
//
//        Assert.assertNotNull(actualResult);
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void getFailedDocumentCountsByProject_ProjectIdAndOrganizationIdNotPresentInDatabase_ReturnsNull() {
//
//        // ARRANGE
//
//        int expectedResult = 0;
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        // when(detailProvider.getFailedDocumentByUser(any(), any(), any())).thenReturn(expectedVisionBotDataDetail);
//
//        when(detailProvider.getFailedDocumentCountsByProject(any(), any())).thenReturn(expectedResult);
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(detailProvider);
//
//        // ASSERT
//
//        int actualResult = sut.getFailedDocumentCountsByProject(null, null);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, 0);
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void getFailedDocumentCountsByProject_ExceptionOccurredInSelectCriteria_ThrowsDatabaseConnectionException() {
//        // ARRANGE
//
//        List<Object> selectResult = new ArrayList<>();
//        // selectResult.add(new DatabaseConnectionException());
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        Mockito.doThrow(new DatabaseConnectionException()).when(detailProvider).getFailedDocumentCountsByProject(any(), any());
//        // when(dataLayer.Select(any(), any())).thenThrow(new Exception());
//
//        ValidatorDetailProvider sut = new ValidatorDetailProvider(dataLayer);
//
//        // ASSERT
//        try {
//            int actualResult = sut.getFailedDocumentCountsByProject(null, null);
//        } catch (DatabaseConnectionException ex) {
//            Assert.assertTrue(true);
//        }
//
//
//    }
//
//    @Test
//    public void getReviewedVBotCount_PresentInDatabase_ReturnsElement() {
//
//        // ARRANGE
//
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        List<Object> expectedResult = new ArrayList<>();
//        when(dataLayer.ExecuteCustomQuery(any(), any())).thenReturn(expectedResult);
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer);
//
//        // ASSERT
//
//        List<Object> actualResult = sut.getReviewedVBotCount();
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void markInvalidFiles_ProjectIdAndFileIdPresentInDatabase_ReturnsElement() {
//
//        // ARRANGE
//
//        final String fileId = "file1";
//        final String projectId = "prj1";
//        final InvalidFiles[] invalidFiles = new InvalidFiles[]{};
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        boolean expectedResult = true;
//        VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        when(dataLayer.Insert(any())).thenReturn(expectedResult);
//        when(dataLayer.update(any())).thenReturn(expectedResult);
//
//        when(detailProvider.getDocumentByFileId(any())).thenReturn(expectedVisionBotDataDetail);
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer, detailProvider);
//
//        // ASSERT
//
//        boolean actualResult = sut.markInvalidFiles(invalidFiles, fileId, projectId);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void getInvalidMarkTypes_PresentInDatabase_ReturnsElementList() {
//
//        // ARRANGE
//
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        List<InvalidFileTypes> expectedResult = new ArrayList<>();
//
//
//        when(detailProvider.getInvalidMarkTypes()).thenReturn(expectedResult);
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(detailProvider);
//
//        // ASSERT
//
//        List<InvalidFileTypes> actualResult = sut.getInvalidMarkTypes();
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void insertRow_ReturnsSuccess() {
//
//        // ARRANGE
//
//        final InvalidFiles invalidFiles = new InvalidFiles();
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//       // ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        boolean expectedResult = true;
//        when(dataLayer.Insert(any())).thenReturn(expectedResult);
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer);
//
//        // ASSERT
//
//        boolean actualResult = sut.insertRow(invalidFiles);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void updateRow_FileIdAndUserIdPresentInDatabase_ReturnsSuccess() {
//
//        // ARRANGE
//
//        final String fileId = "file1";
//        final String userId = "sfgkh";
//        final String projectId = "file1";
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//         ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        boolean expectedResult = true;
//        VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        expectedVisionBotDataDetail.setlockedUserId(userId);
//        when(dataLayer.update(expectedVisionBotDataDetail)).thenReturn(expectedResult);
//
//        when(detailProvider.getDocumentByFileId(any())).thenReturn(expectedVisionBotDataDetail);
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer, detailProvider);
//
//        // ASSERT
//
//        boolean actualResult = sut.updateResourceUserId(projectId,fileId,userId, null);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//    }
//
//    @Test
//    public void updateResourceCorrectedValue_FileIdAndUserIdPresentInDatabase_ReturnsSuccess() {
//
//        // ARRANGE
//
//        final String fileId = "file1";
//        final String userId = "sfgkh";
//        final String correctedValue = "xfgkjhgf";
//        IValidatorServiceDataAccessLayer dataLayer = Mockito.mock(ValidatorServiceDataAccessLayer.class);
//        ValidatorDetailProvider detailProvider = mock(ValidatorDetailProvider.class);
//        boolean expectedResult = true;
//        VisionBotDataDetail expectedVisionBotDataDetail = new VisionBotDataDetail();
//        expectedVisionBotDataDetail.setlockedUserId(userId);
//        when(dataLayer.update(expectedVisionBotDataDetail)).thenReturn(expectedResult);
//
//        when(detailProvider.getDocumentByFileId(any())).thenReturn(expectedVisionBotDataDetail);
//
//        ValidatorServiceImpl sut = new ValidatorServiceImpl(dataLayer,detailProvider);
//
//        // ASSERT
//
//        boolean actualResult = sut.updateResourceCorrectedValue(fileId, null,userId,correctedValue, ValidatorOperationTypeEnum.Save);
//
//        // ACT
//
//        Assert.assertEquals(actualResult, expectedResult);
//    }
}

