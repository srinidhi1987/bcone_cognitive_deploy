package com.automationanywhere.cognitive.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.automationanywhere.cognitive.validator.exception.customexceptions.MessageQueueConnectionCloseException;
import com.rabbitmq.client.Connection;
import java.io.IOException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RpcClientTest {

  private RpcClient rpcClient;

  @BeforeMethod
  public void setUp() throws Exception {
    rpcClient = new RpcClient();
  }

  @Test
  public void closeShouldDoNothingIfConnIsNull() {
    // given

    // when

    rpcClient.closeConnection(null);

    // then
  }

  @Test
  public void closeShouldDoNothingIfConnectionIsClosed() throws IOException {
    // given
    Connection conn = mock(Connection.class);

    doReturn(false).when(conn).isOpen();

    // when

    rpcClient.closeConnection(conn);

    // then

    verify(conn, never()).close();
  }

  @Test
  public void closeShouldCloseConnection() throws IOException {
    // given
    Connection conn = mock(Connection.class);

    doReturn(true).when(conn).isOpen();

    // when

    rpcClient.closeConnection(conn);

    // then

    verify(conn).close();
  }

  @Test
  public void closeShouldWrapIOException() throws IOException {
    // given
    Connection conn = mock(Connection.class);
    IOException ioEx = new IOException();

    doReturn(true).when(conn).isOpen();
    doThrow(ioEx).when(conn).close();

    // when

    Throwable th = catchThrowable(() -> rpcClient.closeConnection(conn));

    // then

    assertThat(th).isInstanceOf(MessageQueueConnectionCloseException.class).hasCause(ioEx);
  }
}
