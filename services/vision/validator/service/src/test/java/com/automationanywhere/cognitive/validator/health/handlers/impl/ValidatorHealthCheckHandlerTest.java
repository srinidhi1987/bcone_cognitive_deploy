package com.automationanywhere.cognitive.validator.health.handlers.impl;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.validator.health.connections.ConnectionType;
import com.automationanywhere.cognitive.validator.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.validator.health.connections.HealthCheckConnectionFactory;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ValidatorHealthCheckHandlerTest {

  @Mock
  HealthCheckConnectionFactory healthCheckConnectionFactory;
  @InjectMocks
  ValidatorHealthCheckHandler validatorHealthCheckHandler;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testOfCheckHealthWhenDBConnectionFails() {
    // given
    HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
    Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
        .thenReturn(healthCheckConnection);
    Mockito.doThrow(DBConnectionFailureException.class).when(healthCheckConnection)
        .checkConnection();

    // when
    String healthcheck = validatorHealthCheckHandler.checkHealth();

    //then
    assertThat(healthcheck).contains("Database Connectivity: FAILURE");
  }

  @Test
  public void testOfCheckHealthWhenDBConnectionSuccess() {
    // given
    HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
    Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
        .thenReturn(healthCheckConnection);
    Mockito.doNothing().when(healthCheckConnection).checkConnection();

    // when
    String healthcheck = validatorHealthCheckHandler.checkHealth();

    //then
    assertThat(healthcheck).contains("Database Connectivity: OK");

  }

  @Test
  public void testOfCheckHealthWhenFileManagerServiceConnectionFails() {
    // given
    HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
    List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity dependentService = new ServiceConnectivity("FileManager",
        HTTPStatusCode.OK);
    dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
    dependentServices.add(dependentService);
    Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
        .thenReturn(healthCheckConnection);
    Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

    // when
    String healthcheck = validatorHealthCheckHandler.checkHealth();

    // then
    assertThat(healthcheck).contains("FileManager: INTERNAL_SERVER_ERROR");
  }

  @Test
  public void testOfCheckHealthWhenFileManagerServiceConnectionSuccess() {
    // given
    HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
    List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity dependentService = new ServiceConnectivity("FileManager",
        HTTPStatusCode.OK);
    dependentServices.add(dependentService);
    Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
        .thenReturn(healthCheckConnection);
    Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

    // when
    String healthcheck = validatorHealthCheckHandler.checkHealth();

    // then
    assertThat(healthcheck).contains("FileManager: OK");
  }

  @Test
  public void testOfCheckHealthWhenProjectServiceConnectionSuccess() {
    // given
    HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
    List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity dependentService = new ServiceConnectivity("Project", HTTPStatusCode.OK);
    dependentServices.add(dependentService);
    Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
        .thenReturn(healthCheckConnection);
    Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

    // when
    String healthcheck = validatorHealthCheckHandler.checkHealth();

    // then
    assertThat(healthcheck).contains("Project: OK");
  }

  @Test
  public void testOfCheckHealthWhenProjectServiceConnectionFails() {
    // given
    HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
    List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
    ServiceConnectivity dependentService = new ServiceConnectivity("Project", HTTPStatusCode.OK);
    dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
    dependentServices.add(dependentService);
    Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
        .thenReturn(healthCheckConnection);
    Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

    // when
    String healthcheck = validatorHealthCheckHandler.checkHealth();

    // then
    assertThat(healthcheck).contains("Project: INTERNAL_SERVER_ERROR");
  }
}
