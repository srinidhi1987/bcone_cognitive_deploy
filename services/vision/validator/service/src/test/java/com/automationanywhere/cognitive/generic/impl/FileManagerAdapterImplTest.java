package com.automationanywhere.cognitive.generic.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DependentServiceConnectionFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.testng.annotations.Test;

public class FileManagerAdapterImplTest {

  @Test
  @SuppressWarnings("unchecked")
  public void testOfTestConnectionWhenRestCallToFileServiceReturnsOK() {
    // GIVEN
    RestTemplateWrapper restTemplateWrapper = mock(RestTemplateWrapper.class);
    ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

    when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
    doReturn(responseEntity).when(restTemplateWrapper).exchangeGet(anyString(), any());

    FileManagerAdapterImpl adapter = new FileManagerAdapterImpl(
        "http://localhost:9996",
        null,
        restTemplateWrapper
    );

    // WHEN
    Throwable throwable = catchThrowable(adapter::testConnection);

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testOfTestConnectionWhenRestCallToFileServiceReturnsNotOK() {
    // GIVEN
    RestTemplateWrapper restTemplateWrapper = mock(RestTemplateWrapper.class);
    ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

    when(responseEntity.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
    doReturn(responseEntity).when(restTemplateWrapper).exchangeGet(anyString(), any());

    FileManagerAdapterImpl adapter = new FileManagerAdapterImpl(
        "http://localhost:9996",
        null,
        restTemplateWrapper
    );

    // WHEN
    Throwable throwable = catchThrowable(adapter::testConnection);

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(DependentServiceConnectionFailureException.class)
        .hasMessage("Error while connecting to FileManager service");
  }
}
