package com.automationanywhere.cognitive.validator.service;

import static com.automationanywhere.cognitive.constants.Constants.PURGE_DATA;
import static com.automationanywhere.cognitive.logging.Constants.PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE;
import static com.automationanywhere.cognitive.validator.ValidationType.Fail;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.automationanywhere.cognitive.config.ValidatorConfig;
import com.automationanywhere.cognitive.exporter.impl.FileSystemBasedInvalidExporterImpl;
import com.automationanywhere.cognitive.exporter.impl.FileSystemBasedSuccessExporterImpl;
import com.automationanywhere.cognitive.validator.ValidationType;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.validator.exception.customexceptions.DocumentLockedException;
import com.automationanywhere.cognitive.validator.json.ClasspathSchemaValidator;
import com.automationanywhere.cognitive.validator.json.JsonMapper;
import com.automationanywhere.cognitive.validator.json.JsonValidator;
import com.automationanywhere.cognitive.validator.models.ValidatorOperationTypeEnum;
import com.automationanywhere.cognitive.validator.repositories.MessageQueueRepository;
import com.automationanywhere.cognitive.validator.repositories.ValidatorRepository;
import com.automationanywhere.cognitive.validator.service.model.FieldLevelAccuracy;
import com.automationanywhere.cognitive.validator.service.model.InvalidFile;
import com.automationanywhere.cognitive.validator.service.model.VisionBotDocument;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VisionBotValidatorServiceTest {

  private static final int DOC_LOCKED_TIMEOUT = 15;

  @Captor
  private ArgumentCaptor<List<InvalidFile>> invalidFileArgument;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testApplyPatchSuccessfully() {
    // GIVEN
    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);
    MessageQueueRepository queueRepository = mock(MessageQueueRepository.class);
    FileSystemBasedSuccessExporterImpl exporter = mock(FileSystemBasedSuccessExporterImpl.class);
    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, queueRepository, exporter, null, messageSource, mapper, validator,
        DOC_LOCKED_TIMEOUT
    );

    String fileId = "035aefec-30d9-4c19-bf82-f8614be37c31";
    String username = "services";

    String patch = "["
        + "{\"op\":\"replace\",\"path\":\"/vBotDocument/Data/FieldDataRecord\",\"value\":{\"Fields\":[{\"Id\":\"f15def8a-9637-4edb-b4a1-1f44ca56b563\"}]}},"
        + "{\"op\":\"replace\",\"path\":\"/vBotDocument/Data/TableDataRecord\",\"value\":[{\"TableDef\":{\"Id\":\"bdfd1cf9-f7e7-4e8f-9bd1-a6567a46ff2e\",\"Name\":\"Table_1\",\"Columns\":[],\"IsDeleted\":false}}]},"
        + "{\"op\":\"replace\",\"path\":\"/fieldAccuracyList\",\"value\":[{\"fileId\":\"035aefec-30d9-4c19-bf82-f8614be37c31\",\"fieldId\":\"1\",\"oldValue\":\"90645193\",\"newValue\":\"90645194\",\"passAccuracyValue\":\"1\"}]}"
        + "]";
    ByteArrayInputStream inputStream = new ByteArrayInputStream(patch.getBytes());

    String data = "{\"Data\":{\"FieldDataRecord\":{\"Fields\":[{\"Id\":\"f15def8a\"}]},\"TableDataRecord\":[{\"TableDef\":{\"Id\":\"bdfd1cf9\",\"Name\":\"Table_0\",\"Columns\":[],\"IsDeleted\":true}}]}}";
    VisionBotDocument visionBotDocument = new VisionBotDocument(
        fileId, "1", "1", data, Fail.toString(), "", new Date(),
        null, null, null, "services", 0f, 0f,
        0f, null, null, 0L, null, null
    );
    doReturn(visionBotDocument).when(validatorRepository).getVisionBotDocument(fileId);

    // WHEN
    service.patchVisionBot(fileId, username, inputStream);

    // THEN
    ArgumentCaptor<VisionBotDocument> vBotArgument = forClass(VisionBotDocument.class);
    verify(validatorRepository).updateVBotDocument(eq(fileId), vBotArgument.capture());

    VisionBotDocument vBotDocument = vBotArgument.getValue();
    String expected = "{\"Data\":{\"FieldDataRecord\":{\"Fields\":[{\"Id\":\"f15def8a-9637-4edb-b4a1-1f44ca56b563\"}]},\"TableDataRecord\":[{\"TableDef\":{\"Id\":\"bdfd1cf9-f7e7-4e8f-9bd1-a6567a46ff2e\",\"Name\":\"Table_1\",\"Columns\":[],\"IsDeleted\":false}}]}}";
    assertThat(vBotDocument.getVBotDocument()).isEqualTo(PURGE_DATA);
    assertThat(vBotDocument.getCorrectedData()).isEqualTo(expected);

    ArgumentCaptor<List<FieldLevelAccuracy>> fieldListArgument = forClass(List.class);
    ArgumentCaptor<String> fileIdArgument = forClass(String.class);
    verify(validatorRepository).save(fileIdArgument.capture(), fieldListArgument.capture());

    assertThat(fileIdArgument.getValue()).isEqualTo(fileId);
    assertThat(fieldListArgument.getValue())
        .hasSize(1)
        .element(0).isEqualToComparingFieldByField(
        new FieldLevelAccuracy(
            fileId,
            "1",
            "90645193",
            "90645194",
            1F
        )
    );
  }

  @Test
  public void patchWithInvalidUsernameThrowsException() {
    // GIVEN
    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);
    MessageQueueRepository queueRepository = mock(MessageQueueRepository.class);
    FileSystemBasedSuccessExporterImpl exporter = mock(FileSystemBasedSuccessExporterImpl.class);
    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, queueRepository, exporter, null, messageSource, mapper, validator,
        DOC_LOCKED_TIMEOUT
    );

    String fileId = "035aefec-30d9-4c19-bf82-f8614be37c31";
    String username = "services";

    String patch = "["
        + "{\"op\":\"replace\",\"path\":\"/vBotDocument/Data/FieldDataRecord\",\"value\":{\"Fields\":[{\"Id\":\"f15def8a-9637-4edb-b4a1-1f44ca56b563\"}]}},"
        + "{\"op\":\"replace\",\"path\":\"/vBotDocument/Data/TableDataRecord\",\"value\":[{\"TableDef\":{\"Id\":\"bdfd1cf9-f7e7-4e8f-9bd1-a6567a46ff2e\",\"Name\":\"Table_1\",\"Columns\":[],\"IsDeleted\":false}}]},"
        + "{\"op\":\"replace\",\"path\":\"/fieldAccuracyList\",\"value\":[{\"fileId\":\"035aefec-30d9-4c19-bf82-f8614be37c31\",\"fieldId\":\"1\",\"oldValue\":\"90645193\",\"newValue\":\"90645194\",\"passAccuracyValue\":\"1\"}]}"
        + "]";
    ByteArrayInputStream inputStream = new ByteArrayInputStream(patch.getBytes());

    String data = "{\"Data\":{\"FieldDataRecord\":{\"Fields\":[{\"Id\":\"f15def8a\"}]},\"TableDataRecord\":[{\"TableDef\":{\"Id\":\"bdfd1cf9\",\"Name\":\"Table_0\",\"Columns\":[],\"IsDeleted\":true}}]}}";
    VisionBotDocument visionBotDocument = new VisionBotDocument(
        fileId, "1", "1", data, Fail.toString(), "", new Date(),
        null, null, null, "validator", 0f, 0f,
        0f, new Date(), null, 0L, null, null
    );
    doReturn(visionBotDocument).when(validatorRepository).getVisionBotDocument(fileId);

    doReturn("Error message").when(messageSource)
        .getMessage(PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE, null, Locale.getDefault());

    // WHEN
    Throwable t = catchThrowable(() -> service.patchVisionBot(fileId, username, inputStream));

    // THEN
    assertThat(t)
        .isNotNull()
        .isInstanceOf(DocumentLockedException.class);
  }

  @Test
  public void patchWithNotFoundVBotThrowsException() {
    // GIVEN
    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);
    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, null, null, null, messageSource, mapper, validator, DOC_LOCKED_TIMEOUT
    );

    String fileId = "035aefec-30d9-4c19-bf82-f8614be37c31";
    String username = "services";

    String patch = "["
        + "{\"op\":\"replace\",\"path\":\"/vBotDocument/Data/FieldDataRecord\",\"value\":{\"Fields\":[{\"Id\":\"f15def8a-9637-4edb-b4a1-1f44ca56b563\"}]}},"
        + "{\"op\":\"replace\",\"path\":\"/vBotDocument/Data/TableDataRecord\",\"value\":[{\"TableDef\":{\"Id\":\"bdfd1cf9-f7e7-4e8f-9bd1-a6567a46ff2e\",\"Name\":\"Table_1\",\"Columns\":[],\"IsDeleted\":false}}]},"
        + "{\"op\":\"replace\",\"path\":\"/fieldAccuracyList\",\"value\":[{\"fileId\":\"035aefec-30d9-4c19-bf82-f8614be37c31\",\"fieldId\":\"1\",\"oldValue\":\"90645193\",\"newValue\":\"90645194\",\"passAccuracyValue\":\"1\"}]}"
        + "]";
    ByteArrayInputStream inputStream = new ByteArrayInputStream(patch.getBytes());

    doReturn(null).when(validatorRepository).getVisionBotDocument(fileId);

    doReturn("Error message").when(messageSource)
        .getMessage(PROJECT_NOT_FOUND_DOCUMENTS_NOT_AVAILABLE, null, Locale.getDefault());

    // WHEN
    Throwable t = catchThrowable(() -> service.patchVisionBot(fileId, username, inputStream));

    // THEN
    assertThat(t).isNotNull().isInstanceOf(DataNotFoundException.class).hasMessage("Error message");
  }

  @Test
  public void getNextAvailableDocumentSuccessfully() {
    // GIVEN
    String orgId = "1";
    String projectId = "1234";
    String username = "services";

    Date createdAt = new Date();
    VisionBotDocument currentLockedDoc = new VisionBotDocument(
        "12345678",
        orgId,
        projectId,
        "{}",
        ValidationType.Fail.toString(),
        "",
        createdAt,
        "",
        null,
        "",
        username,
        1,
        2,
        3,
        null,
        null,
        0,
        null,
        null
    );

    VisionBotDocument nextUnlockedDoc = new VisionBotDocument(
        "87654321",
        orgId,
        projectId,
        "{}",
        ValidationType.Fail.toString(),
        "",
        new Date(),
        "",
        null,
        "",
        "",
        1,
        2,
        3,
        null,
        null,
        0,
        null,
        null
    );

    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);

    List<VisionBotDocument> lockedDocuments = Collections.singletonList(currentLockedDoc);
    doReturn(lockedDocuments).when(validatorRepository).searchAllFailedLockedVBotDocuments(
        orgId, projectId, username
    );
    doReturn(nextUnlockedDoc).when(validatorRepository).getNextFailedVBotDocument(
        orgId, projectId, currentLockedDoc.getFileId(), currentLockedDoc.getCreatedAt()
    );

    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, null, null, null, messageSource, mapper, validator, DOC_LOCKED_TIMEOUT
    );

    // WHEN
    VisionBotDocument document = service.getAndLockFailedVBot(orgId, projectId, username);

    // THEN
    assertThat(document).isEqualTo(nextUnlockedDoc);

    InOrder inOrder = inOrder(validatorRepository);
    inOrder.verify(validatorRepository, times(1)).unlock(lockedDocuments);
    inOrder.verify(validatorRepository, times(1)).lock(nextUnlockedDoc, username);
  }

  @Test
  public void getNextFirstAvailableDocumentSuccessfully() {
    // GIVEN
    String orgId = "1";
    String projectId = "1234";
    String username = "services";

    Date createdAt = new Date();
    VisionBotDocument currentLockedDoc = new VisionBotDocument(
        "12345678",
        orgId,
        projectId,
        "{}",
        ValidationType.Fail.toString(),
        "",
        createdAt,
        "",
        null,
        "",
        username,
        1,
        2,
        3,
        null,
        null,
        0,
        null,
        null
    );

    VisionBotDocument firstUnlockedDoc = new VisionBotDocument(
        "87654321",
        orgId,
        projectId,
        "{}",
        ValidationType.Fail.toString(),
        "",
        new Date(),
        "",
        null,
        "",
        "",
        1,
        2,
        3,
        null,
        null,
        0,
        null,
        null
    );

    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);

    List<VisionBotDocument> lockedDocuments = Collections.singletonList(currentLockedDoc);
    doReturn(lockedDocuments).when(validatorRepository).searchAllFailedLockedVBotDocuments(
        orgId, projectId, username
    );
    doReturn(null).when(validatorRepository).getNextFailedVBotDocument(
        orgId, projectId, currentLockedDoc.getFileId(), currentLockedDoc.getCreatedAt()
    );
    doReturn(firstUnlockedDoc).when(validatorRepository).getFirstFailedVBotDocument(
        orgId, projectId
    );

    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, null, null, null, messageSource, mapper, validator, DOC_LOCKED_TIMEOUT
    );

    // WHEN
    VisionBotDocument document = service.getAndLockFailedVBot(orgId, projectId, username);

    // THEN
    assertThat(document).isEqualTo(firstUnlockedDoc);

    InOrder inOrder = inOrder(validatorRepository);
    inOrder.verify(validatorRepository, times(1)).unlock(lockedDocuments);
    inOrder.verify(validatorRepository, times(1)).lock(firstUnlockedDoc, username);
  }

  @Test
  public void getNextFirstAvailableDocumentNoneLockedSuccessfully() {
    // GIVEN
    String orgId = "1";
    String projectId = "1234";
    String username = "services";

    VisionBotDocument firstUnlockedDoc = new VisionBotDocument(
        "87654321",
        orgId,
        projectId,
        "{}",
        ValidationType.Fail.toString(),
        "",
        new Date(),
        "",
        null,
        "",
        "",
        1,
        2,
        3,
        null,
        null,
        0,
        null,
        null
    );

    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);

    List<VisionBotDocument> lockedDocuments = Collections.emptyList();
    doReturn(lockedDocuments).when(validatorRepository).searchAllFailedLockedVBotDocuments(
        orgId, projectId, username
    );
    doReturn(firstUnlockedDoc).when(validatorRepository).getFirstFailedVBotDocument(
        orgId, projectId
    );

    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, null, null, null, messageSource, mapper, validator, DOC_LOCKED_TIMEOUT
    );

    // WHEN
    VisionBotDocument document = service.getAndLockFailedVBot(orgId, projectId, username);

    // THEN
    assertThat(document).isEqualTo(firstUnlockedDoc);

    InOrder inOrder = inOrder(validatorRepository);
    inOrder.verify(validatorRepository, times(1)).unlock(lockedDocuments);
    inOrder.verify(validatorRepository, times(1)).lock(firstUnlockedDoc, username);
  }

  @Test
  public void getNextAvailableDocumentReturnsNoDocument() {
    // GIVEN
    String orgId = "1";
    String projectId = "1234";
    String username = "services";

    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);

    List<VisionBotDocument> lockedDocuments = Collections.emptyList();
    doReturn(lockedDocuments).when(validatorRepository).searchAllFailedLockedVBotDocuments(
        orgId, projectId, username
    );
    doReturn(null).when(validatorRepository).getFirstFailedVBotDocument(
        orgId, projectId
    );

    MessageSource messageSource = mock(MessageSource.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, null, null, null, messageSource, mapper, validator, DOC_LOCKED_TIMEOUT
    );

    // WHEN
    VisionBotDocument document = service.getAndLockFailedVBot(orgId, projectId, username);

    // THEN
    assertThat(document).isNull();
    verify(validatorRepository, times(1)).unlock(lockedDocuments);
    verify(validatorRepository, never()).lock(any(), any());
  }

  @Test
  public void markInvalidFilesSuccessful() {
    // GIVEN
    String username = "services";
    String fileId = "53f8bfc8-ea2d-4730-a5dd-7ad57dd1bb90";
    String reasonId = "1";
    String reasonText = "reason 1";
    Date startTime = new Date();

    List<InvalidFile> invalidFiles = Arrays.asList(
        new InvalidFile("abc", "", ""),
        new InvalidFile(null, null, null),
        new InvalidFile(fileId, reasonId, reasonText)
    );

    VisionBotDocument doc = new VisionBotDocument(fileId, "1", null, null,
        ValidatorOperationTypeEnum.Save.toString(), null, null, null,
        null, null, null, 0f, 0f, 0f,
        startTime, null, 0L, null, null);

    ValidatorRepository validatorRepository = mock(ValidatorRepository.class);
    doReturn(doc).when(validatorRepository).getVisionBotDocument(fileId);

    MessageSource messageSource = mock(MessageSource.class);
    FileSystemBasedInvalidExporterImpl invalidExporter = mock(
        FileSystemBasedInvalidExporterImpl.class);

    ValidatorConfig config = new ValidatorConfig(null);
    JsonMapper mapper = new JsonMapper(config.getObjectMapper());

    ClasspathSchemaValidator schemaValidator = mock(ClasspathSchemaValidator.class);
    JsonValidator validator = new JsonValidator(mapper, schemaValidator);

    VisionBotValidatorService service = new VisionBotValidatorService(
        validatorRepository, null, null, invalidExporter, messageSource, mapper, validator,
        DOC_LOCKED_TIMEOUT
    );

    // WHEN
    service.markInvalidFiles(username, fileId, invalidFiles);

    // THEN
    verify(validatorRepository).insertInvalidFiles(invalidFileArgument.capture());
    List<InvalidFile> invalidFilesToSave = invalidFileArgument.getValue();
    assertThat(invalidFilesToSave)
        .hasSize(2)
        .containsExactly(
            new InvalidFile(fileId, "", ""),
            new InvalidFile(fileId, reasonId, reasonText)
        );

    ArgumentCaptor<String> fileIdArgument = forClass(String.class);
    ArgumentCaptor<VisionBotDocument> docArgument = forClass(VisionBotDocument.class);
    verify(validatorRepository).updateVBotDocument(fileIdArgument.capture(), docArgument.capture());

    assertThat(fileIdArgument.getValue()).isEqualTo(fileId);
    VisionBotDocument docToSave = docArgument.getValue();
    assertThat(docToSave).isEqualToComparingFieldByFieldRecursively(
        new VisionBotDocument(fileId, "1", null, null,
            ValidatorOperationTypeEnum.Invalid.toString(), null, null, "services",
            docToSave.getUpdatedAt(), null, "", 0f, 0f, 0f,
            startTime, docToSave.getEndTime(), docToSave.getAverageTimeInSeconds(), null, null
        )
    );
  }
}
