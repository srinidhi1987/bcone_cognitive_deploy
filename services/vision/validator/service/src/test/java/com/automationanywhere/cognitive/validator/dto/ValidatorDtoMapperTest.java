package com.automationanywhere.cognitive.validator.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.validator.service.model.InvalidFile;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.Test;

public class ValidatorDtoMapperTest {

  @Test
  public void mapInvalidFileDtoToModel() {
    // GIVEN
    String fileId = "53f8bfc8-ea2d-4730-a5dd-7ad57dd1bb90";
    String reasonId = "1";
    String reasonText = "reason 1";

    InvalidFileDto dto = new InvalidFileDto(fileId, reasonId, reasonText);

    ValidatorDtoMapper mapper = new ValidatorDtoMapper();

    // WHEN
    InvalidFile invalidFile = mapper.getInvalidFile(dto);

    // THEN
    assertThat(invalidFile).isNotNull();
    assertThat(invalidFile.fileId).isEqualTo(fileId);
    assertThat(invalidFile.reasonId).isEqualTo(reasonId);
    assertThat(invalidFile.reasonText).isEqualTo(reasonText);
  }

  @Test
  public void mapInvalidFileDtoListToModel() {
    // GIVEN
    String fileId = "53f8bfc8-ea2d-4730-a5dd-7ad57dd1bb90";
    String reasonId = "1";
    String reasonText = "reason 1";

    List<InvalidFileDto> dtos = Collections.singletonList(
        new InvalidFileDto(fileId, reasonId, reasonText)
    );

    ValidatorDtoMapper mapper = new ValidatorDtoMapper();

    // WHEN
    List<InvalidFile> invalidFiles = mapper.getInvalidFiles(dtos);

    // THEN
    assertThat(invalidFiles).hasSize(1);
    InvalidFile invalidFile = invalidFiles.get(0);
    assertThat(invalidFile.fileId).isEqualTo(fileId);
    assertThat(invalidFile.reasonId).isEqualTo(reasonId);
    assertThat(invalidFile.reasonText).isEqualTo(reasonText);
  }

  @Test
  public void mapInvalidFileDtoListWithNullToModel() {
    // GIVEN
    String fileId = "53f8bfc8-ea2d-4730-a5dd-7ad57dd1bb90";
    String reasonId = "1";
    String reasonText = "reason 1";

    List<InvalidFileDto> dtos = Arrays.asList(
        null,
        new InvalidFileDto(fileId, reasonId, reasonText),
        null
    );

    ValidatorDtoMapper mapper = new ValidatorDtoMapper();

    // WHEN
    List<InvalidFile> invalidFiles = mapper.getInvalidFiles(dtos);

    // THEN
    assertThat(invalidFiles).hasSize(1);
    InvalidFile invalidFile = invalidFiles.get(0);
    assertThat(invalidFile.fileId).isEqualTo(fileId);
    assertThat(invalidFile.reasonId).isEqualTo(reasonId);
    assertThat(invalidFile.reasonText).isEqualTo(reasonText);
  }
}
