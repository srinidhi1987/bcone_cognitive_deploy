# Automation Anywhere Cognitive - Backend #

### What is this repository for? ###
For now this repo will contain following modules:

* common
* file
* project
* customer
* vision
* validator
* classifier
* gateway

**common** module provides common non-functional requirements. e.g: logging, auditing, auth, bootstrap, api registry, web client(similar to curl)

**file** module provides customer's file/document management functionalities. e.g.: upload / download of customer documents.

**project** module is responsible for managing the lifecycle of project.

**customer** module is responsible for managing the lifecycle of customer.

**vision** module provides end user with tools to train & design visionbot & then process customer document in un-attended mode.

**validator** module is a set of tools and services that allows customer to validate and manage failing documents.

**classifier** classifies document into distinct set of classes. un-supervised classification services uses layout and content based classification approach to classify any incoming document of a given project.

**gateway** is a server that is the single entry point into the system.
It is similar to the Facade pattern from object‑oriented design.
It might have other responsibilities such as authentication,monitoring,
load balancing, caching, request shaping and management, and static response handling.

The gateway has 5 major parts:

* controllers
In terms of the Hexagonal Architecture *controllers* (REST, SOAP, ...) are ports that compose a client API
Controllers convert requests to method calls on the application core.
and then convert the data provided by the application into responses suitable for clients.

* services
Services compose the application core and are responsible for getting all necessary details
and processing the details along with the input data provided by the controllers
to produce results requested by the them. By definition, the application core should be ignorant to the nature
of the callers and to the nature of the requested data.

* models
To make services ignorant to the nature of callers and data, the models used for the data exchange
with the application core MUST be defined by services but since they are used by all the parts of the application
we put them at the same level as services, not under services. Repositories and controller may define their own models (JSON models, entities)
but their models should not leak to other levels, that's why they should stay in their subpackages together with the component that defines them.

* repositories
The responsibility of repositories is to provide data needed for the application core to process client requests.
One service may call several repositories if necessary. Repositories compose the internal API.
This allows to hide the nature of the data from services.

* components
These are helper beans (managed or unmanaged) used by controllers, services and repositories.
Usually they implement the application infrastructure.

### How do I get set up? ###
* TBD
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact