package com.automationanywhere.cognitive.project.imports.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.ContentTrainSetDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ContentTrainSet;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ContentTrainSetDataImporterTest {

  @InjectMocks
  ContentTrainSetDataImporter dataImporterImpl;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private ContentTrainSetDao contentTrainSetDao;
  @Mock
  private FileReader fileReader;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  private TransitionResult transitionResult;
  @Captor
  private ArgumentCaptor<List<ContentTrainSet>> argumentCaptor;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    dataImporterImpl = new ContentTrainSetDataImporter(fileReader, fileHelper, contentTrainSetDao,
        importFilePatternConstructor, transitionResult);
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importShouldFailWhenIOExceptionCaught()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    dataImporterImpl.importData(Paths.get("."), TaskOptions.APPEND);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddNewClasses()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("path"));

    List<ContentTrainSet> existingData = new ArrayList<ContentTrainSet>();
    existingData.add(getContentTrainSetDao(1, "1,2,3"));
    existingData.add(getContentTrainSetDao(2, "1,2,4"));

    List<ContentTrainSet> csvData = new ArrayList<ContentTrainSet>();
    csvData.add(getContentTrainSetDao(2, "1,2,5"));

    ContentTrainSetDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(contentTrainSetDao.getAll()).thenReturn(existingData);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);
    Mockito.verify(contentTrainSetDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(contentTrainSetDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  @Test
  public void importShouldNotAddExistingClasses()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {

    //given
    ContentTrainSetDataImporter spy = configureContentTrainSetDataImporter();

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);
    Mockito.verify(contentTrainSetDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(contentTrainSetDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 0);
  }

  @Test
  public void importShouldCallAnotherDataImporter()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    ContentTrainSetDataImporter spy = configureContentTrainSetDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);
    spy.setNext(advanceDataImporter);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @Test
  public void importShouldNotCallAnotherDataImporterIfNextDataImporterIsNull()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    ContentTrainSetDataImporter spy = configureContentTrainSetDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(0)).importData(Mockito.any(), Mockito.any());
  }


  private ContentTrainSet getContentTrainSetDao(int id, String training) {
    ContentTrainSet contentTrainSet = new ContentTrainSet();
    contentTrainSet.setId(id);
    contentTrainSet.setTraining(training);
    return contentTrainSet;
  }

  @SuppressWarnings("unchecked")
  private ContentTrainSetDataImporter configureContentTrainSetDataImporter()
      throws ProjectImportException, IOException {
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("path"));

    List<ContentTrainSet> existingData = new ArrayList<>();
    existingData.add(getContentTrainSetDao(1, "1,2,3"));
    existingData.add(getContentTrainSetDao(2, "1,2,4"));

    List<ContentTrainSet> csvData = new ArrayList<ContentTrainSet>();
    csvData.add(getContentTrainSetDao(2, "1,2,3"));

    ContentTrainSetDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(contentTrainSetDao.getAll()).thenReturn(existingData);
    return spy;
  }
}
