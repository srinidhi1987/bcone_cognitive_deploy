package com.automationanywhere.cognitive.project.configuration;

import com.automationanywhere.cognitive.project.configuration.filebasedconfiguration.FileWatcher;
import com.automationanywhere.cognitive.project.configuration.filebasedconfiguration.RealtimeFileBasedConfigurationService;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;


public class RealtimeFileBasedConfigurationServiceTest {

    @Mock
    FileWatcher mockFileWatcher;

    @Mock
    ConfigurationReader mockConfigurationReader;

    @InjectMocks
    private RealtimeFileBasedConfigurationService realtimeFileBasedConfigurationService;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = NotFoundException.class)
    public void throwExceptionWhenNoConfigurationFound() throws NotFoundException {
        when(mockConfigurationReader.getProperties()).thenReturn(new HashMap<String, String>());
        realtimeFileBasedConfigurationService.getConfiguration("TestKey");
        Mockito.verify(mockConfigurationReader, times(1)).getProperties();
    }

    @Test
    public void valueReturnedWhenConfigurationFound() throws NotFoundException {
        Map<String, String> keyValueMap = new HashMap<>();
        keyValueMap.put("TestKey", "TestValue");
        when(mockConfigurationReader.getProperties()).thenReturn(keyValueMap);
        realtimeFileBasedConfigurationService = new RealtimeFileBasedConfigurationService(
                mockConfigurationReader,
                mockFileWatcher);
        String actualValue = realtimeFileBasedConfigurationService.getConfiguration("TestKey");

        Assert.assertEquals(actualValue, "TestValue");
    }

}
