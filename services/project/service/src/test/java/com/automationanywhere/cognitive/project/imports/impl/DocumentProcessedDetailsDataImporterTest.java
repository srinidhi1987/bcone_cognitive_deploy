package com.automationanywhere.cognitive.project.imports.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.DocumentProcessedDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ContentTrainSet;
import com.automationanywhere.cognitive.project.model.dao.DocumentProcessedDetails;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DocumentProcessedDetailsDataImporterTest {

  @InjectMocks
  DocumentProcessedDetailsDataImporter dataImporterImpl;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private DocumentProcessedDetailDao documentProcessedDetailDao;
  @Mock
  private FileReader fileReader;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  private TransitionResult transitionResult;
  @Captor
  private ArgumentCaptor<List<DocumentProcessedDetails>> argumentCaptor;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    dataImporterImpl = new DocumentProcessedDetailsDataImporter(fileReader, fileHelper,
        documentProcessedDetailDao,
        importFilePatternConstructor, transitionResult);
    when(transitionResult.getProjectId()).thenReturn("1");
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importShouldFailWhenIOExceptionCaught()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    dataImporterImpl.importData(Paths.get("."), TaskOptions.APPEND);
  }
  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddDocumentProcessedDetailsForTaskOptionClear()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("path"));

    List<DocumentProcessedDetails> existingData = new ArrayList<DocumentProcessedDetails>();
    existingData.add(getDocumentProcessedDetail("1", "1"));
    existingData.add(getDocumentProcessedDetail("2", "1"));

    List<DocumentProcessedDetails> csvData = new ArrayList<DocumentProcessedDetails>();
    csvData.add(getDocumentProcessedDetail("3", "3"));

    DocumentProcessedDetailsDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(documentProcessedDetailDao.get(Mockito.anyList())).thenReturn(existingData);

    //when
    List<String> visionBotRunDetailsId=csvData.stream().map(it->it.getVisionBotRunDetailsId()).collect(Collectors.toList());
    Mockito.when(transitionResult.getImportedVisionBotRunDetailIdList()).thenReturn(visionBotRunDetailsId);
    spy.importData(Paths.get("."), TaskOptions.CLEAR);
    Mockito.verify(documentProcessedDetailDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(documentProcessedDetailDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddNewDocumentProcessedDetailForTaskOptionAppend()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<DocumentProcessedDetails> existingData = new ArrayList<DocumentProcessedDetails>();
    existingData.add(getDocumentProcessedDetail("1", "1"));
    existingData.add(getDocumentProcessedDetail("2", "1"));

    List<DocumentProcessedDetails> csvData = new ArrayList<DocumentProcessedDetails> ();
    csvData.add(getDocumentProcessedDetail("3", "3"));

    DocumentProcessedDetailsDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(documentProcessedDetailDao.get(Mockito.anyList())).thenReturn(existingData);

    //when
    List<String> visionBotRunDetailsId=csvData.stream().map(it->it.getVisionBotRunDetailsId()).collect(Collectors.toList());
    Mockito.when(transitionResult.getImportedVisionBotRunDetailIdList()).thenReturn(visionBotRunDetailsId);
    spy.importData(Paths.get("."), TaskOptions.APPEND);
    Mockito.verify(documentProcessedDetailDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(documentProcessedDetailDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddNewDocumentProcessedDetailForTaskOptionMerge()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<DocumentProcessedDetails> existingData = new ArrayList<DocumentProcessedDetails>();
    existingData.add(getDocumentProcessedDetail("1", "1"));
    existingData.add(getDocumentProcessedDetail("2", "1"));

    List<DocumentProcessedDetails> csvData = new ArrayList<DocumentProcessedDetails>();
    csvData.add(getDocumentProcessedDetail("3", "3"));

    DocumentProcessedDetailsDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(documentProcessedDetailDao.get(Mockito.anyList())).thenReturn(existingData);

    //when
    List<String> visionBotRunDetailsId=csvData.stream().map(it->it.getVisionBotRunDetailsId()).collect(Collectors.toList());
    Mockito.when(transitionResult.getImportedVisionBotRunDetailIdList()).thenReturn(visionBotRunDetailsId);
    spy.importData(Paths.get("."), TaskOptions.MERGE);
    Mockito.verify(documentProcessedDetailDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(documentProcessedDetailDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }
  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddNewDocumentProcessedDetailForTaskOptionOverwrite()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("path"));

    List<DocumentProcessedDetails> existingData = new ArrayList<DocumentProcessedDetails>();
    existingData.add(getDocumentProcessedDetail("1", "1"));
    existingData.add(getDocumentProcessedDetail("2", "1"));

    List<DocumentProcessedDetails> csvData = new ArrayList<DocumentProcessedDetails>();
    csvData.add(getDocumentProcessedDetail("3", "3"));

    DocumentProcessedDetailsDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(documentProcessedDetailDao.get(Mockito.anyList())).thenReturn(existingData);

    //when
    List<String> visionBotRunDetailsId=csvData.stream().map(it->it.getVisionBotRunDetailsId()).collect(Collectors.toList());
    Mockito.when(transitionResult.getImportedVisionBotRunDetailIdList()).thenReturn(visionBotRunDetailsId);
    spy.importData(Paths.get("."), TaskOptions.OVERWRITE);
    Mockito.verify(documentProcessedDetailDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(documentProcessedDetailDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }
  public void importShouldNotAddDuplicateData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {

    //given
    DocumentProcessedDetailsDataImporter spy = configureDocumentProcessedDetailsDataImporter();

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(documentProcessedDetailDao, times(0)).addAll(Mockito.any());
  }

  @Test
  public void importShouldCallAnotherDataImporter()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    DocumentProcessedDetailsDataImporter spy = configureDocumentProcessedDetailsDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);
    spy.setNext(advanceDataImporter);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @Test
  public void importShouldNotCallAnotherDataImporterIfNextDataImporterIsNull()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    DocumentProcessedDetailsDataImporter spy = configureDocumentProcessedDetailsDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(0)).importData(Mockito.any(), Mockito.any());
  }


  private DocumentProcessedDetails getDocumentProcessedDetail(String id, String runId) {
    DocumentProcessedDetails documentProcessedDetails = new DocumentProcessedDetails();
    documentProcessedDetails.setId(id);
    documentProcessedDetails.setVisionBotRunDetailsId(runId);
    return documentProcessedDetails;
  }

  @SuppressWarnings("unchecked")
  private DocumentProcessedDetailsDataImporter configureDocumentProcessedDetailsDataImporter()
      throws ProjectImportException, IOException {
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<DocumentProcessedDetails> existingData = new ArrayList<DocumentProcessedDetails>();
    existingData.add(getDocumentProcessedDetail("1", "1"));
    existingData.add(getDocumentProcessedDetail("2", "1"));

    List<DocumentProcessedDetails> csvData = new ArrayList<DocumentProcessedDetails>();
    csvData.add(getDocumentProcessedDetail("2", "1"));

    DocumentProcessedDetailsDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(documentProcessedDetailDao.get(Mockito.anyList())).thenReturn(existingData);
    when(transitionResult.getImportedVisionBotRunDetailIdList()).thenReturn(getImportedVisionBotRunDetailsIdList());
    return spy;
  }

  private List<String> getImportedVisionBotRunDetailsIdList()
  {
    List<String> importedVisionBotRunDetailsIdList = new ArrayList<>();
    importedVisionBotRunDetailsIdList.add("1");
    return importedVisionBotRunDetailsIdList;
  }
}
