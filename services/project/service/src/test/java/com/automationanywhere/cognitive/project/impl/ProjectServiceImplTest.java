package com.automationanywhere.cognitive.project.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.adapter.FileManagerAdapter;
import com.automationanywhere.cognitive.project.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.project.configuration.ConfigurationService;
import com.automationanywhere.cognitive.project.dal.OCREngineDetailsDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectCreationFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectDeletedException;
import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.PatchOperation;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;
import com.automationanywhere.cognitive.project.model.dao.ProjectActivityDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dto.Fields;
import com.automationanywhere.cognitive.project.model.dto.PatchRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectGetResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponseV2;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

//import static org.mockito.Mockito.doReturn;

/**
 * Created by Jemin.Shah on 2/20/2017.
 */
@ContextConfiguration(locations = {"classpath:SprintBeans-test.xml"})
public class ProjectServiceImplTest {

    @Mock
    private ProjectDetailDao projectDetailDao;

    @Mock OCREngineDetailsDao ocrEngineDetailsDao;

    @Mock
    private FileManagerAdapter fileManagerAdapter;

    @Mock
    private VisionbotAdapter visionbotAdapter;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private ProjectServiceDelegate projectServiceDelegate;

    @Mock
    private ProjectUpdation projectUpdation;

    @InjectMocks
    private ProjectServiceImpl projectService;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getApiReturnsProjectWhenFound() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        List<ProjectDetail> projectDetailList = new ArrayList<>();
        projectDetailList.add(getProjectDetail(projectId));
        when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);
        when(visionbotAdapter.getVisionbotData(Mockito.any())).thenReturn(null);
        when(configurationService.getConfiguration(Mockito.any())).thenReturn("90");

        //Act
        ProjectGetResponse response = projectService.get("1", projectId, null);
        int confidenceThreshold = response.getConfidenceThreshold();

        //Assert

        Assert.assertNotNull(response);
        Assert.assertEquals(response.getId(), projectId);
        Assert.assertEquals(confidenceThreshold, 90);
    }

    @Test(expectedExceptions = ProjectDeletedException.class)
    public void getApiThrowsExceptionWhenProjectWasDeleted() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        List<ProjectDetail> projectDetailList = new ArrayList<>();
        when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);
        when(projectDetailDao.getDeletedProjectActivityByProjectId(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));
        when(visionbotAdapter.getVisionbotData(Mockito.any())).thenReturn(null);

        ProjectGetResponse response = projectService.get("1", projectId, null);

    }

    @Test(expectedExceptions = DuplicateProjectNameException.class)
    public void addApiThrowsExceptionWhenDuplicateProject() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        OCREngineDetails ocrEngineDetails=new OCREngineDetails();
        ocrEngineDetails.setId("1");
        ocrEngineDetails.setEngineType("Tesseract4");
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));
        when(ocrEngineDetailsDao.getOCREngineDetails(Mockito.any())).thenReturn(ocrEngineDetails);

        ProjectRequest projectRequest = getProjectRequest(projectId);
        ProjectResponse response = projectService.add(projectRequest);
    }

    @Test(expectedExceptions = ProjectCreationFailedException.class)
    public void addApiThrowsExceptionWhenOCREngineDetailsConfigurationNotFound() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
      
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));
        when(ocrEngineDetailsDao.getOCREngineDetails(Mockito.any())).thenReturn(null);

        ProjectRequest projectRequest = getProjectRequest(projectId);
        ProjectResponse response = projectService.add(projectRequest);
    }

    @Test
    public void add_ProjectDetails_SuccessFul() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        OCREngineDetails ocrEngineDetails=new OCREngineDetails();
        ocrEngineDetails.setId("1");
        ocrEngineDetails.setEngineType("Tesseract4");
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(null);
        when(ocrEngineDetailsDao.getOCREngineDetails(Mockito.any())).thenReturn(ocrEngineDetails);

        ProjectRequest projectRequest = getProjectRequest(projectId);
        ProjectResponse response = projectService.add(projectRequest);
    }


    @Test(expectedExceptions = PreconditionFailedException.class)
    public void deleteApiThrowsExceptionWhenProjectIsInProductionMode() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        List<ProjectDetail> projectDetailList = new ArrayList<>();

        ProjectDetail projectDetail = getProjectDetail(projectId);
        projectDetail.setEnvironment(Environment.production);
        projectDetailList.add(projectDetail);

        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));
        when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);

        projectService.delete("1", projectId, "test");
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void deleteApiThrowsExceptionWhenUserNameIsEmpty() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));

        projectService.delete("1", projectId, "");

    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void deleteApiThrowsExceptionWhenProjectIdIsEmpty() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));

        projectService.delete("1", "", "test");
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void deleteApiThrowsExceptionWhenProjectNotFound() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        List<ProjectDetail> projectDetailList = new ArrayList<>();
        when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));
        projectService.delete("1", projectId, "test");
    }

    @Test
    public void deleteApiReturnsSuccessWhenProjectFound() throws Exception {
        //Arrange
        final String projectId = UUID.randomUUID().toString();
        List<ProjectDetail> projectDetailList = new ArrayList<>();
        projectDetailList.add(getProjectDetail(projectId));
        when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);
        when(projectDetailDao.getDeletedProjectActivityByProjectName(Mockito.any())).thenReturn(getProjectActivityDetail(projectId));

        //Act
        projectService.delete("1", projectId, "test");

        //Assert
        Mockito.verify(projectDetailDao, times(1)).deleteProjectDetail(Mockito.any());
        Mockito.verify(projectDetailDao, times(1)).addProjectActivity(Mockito.any());

    }

  @SuppressWarnings("unchecked")
  @Test
  public void patchShouldUpdateProjectDetails() throws Exception{
    // GIVEN
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest=getProjectRequest(projectId);
    PatchRequest patchRequest = getPatchRequest(PatchOperation.replace, "/description","Text" );

    List<ProjectDetail> projectDetailList = new ArrayList<>();
    projectDetailList.add(getProjectDetail(projectId));

    when(configurationService.getConfiguration(Mockito.any())).thenReturn("90");
    when(projectUpdation.getProjectDetails(Mockito.anyMap(), Mockito.any())).thenReturn(projectDetailList);
    when(projectServiceDelegate.applyPatchOperations(Mockito.any(),Mockito.any())).thenReturn(projectRequest);

    // WHEN
    projectService.update(projectRequest.getOrganizationId(),projectId,patchRequest);

    // THEN
    Mockito.verify(projectServiceDelegate,times(1)).update(Mockito.any(),Mockito.any());
  }

  @Test
  public void patchShouldThrowNotFoundException() throws Exception {
    // GIVEN
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest = getProjectRequest(projectId);
    PatchRequest patchRequest = getPatchRequest(PatchOperation.replace, "/description", "Text");

    List<ProjectDetail> projectDetailList = new ArrayList<>();

    when(projectUpdation.getProjectDetails(Mockito.any(), Mockito.any()))
        .thenReturn(projectDetailList);

    // WHEN
    Throwable throwable = catchThrowable(() ->
        projectService.update(projectRequest.getOrganizationId(), projectId, patchRequest)
    );

    // THEN
    assertThat(throwable)
        .isNotNull()
        .hasMessage("Learning Instance not found.")
        .hasCauseInstanceOf(NotFoundException.class);
  }

  @Test
  public void updateShouldUpdateProjectDetails() throws Exception{
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest=getProjectRequest(projectId);

    List<ProjectDetail> projectDetailList = new ArrayList<>();
    projectDetailList.add(getProjectDetail(projectId));

    //when
    when(projectUpdation.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);

    projectService.update(projectRequest);

    //then
    Mockito.verify(projectServiceDelegate,times(1)).update(Mockito.any(),Mockito.any());
    //Mockito.verify(projectServiceDelegate,times(1)).addNewFieldsInVisionBots(Mockito.any(),Mockito.any(),Mockito.any());
  }

  @Test(expectedExceptions = NotFoundException.class)
  public void updateShouldThrowNotFoundException() throws Exception{
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest=getProjectRequest(projectId);

    List<ProjectDetail> projectDetailList = new ArrayList<>();

    //when
    when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);

    projectService.update(projectRequest);

  }

  @Test
  public void getMetadataV2ShouldReturnProjectResponse() throws Exception{
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectActivityDetail projectActivityDetail=getProjectActivityDetail(projectId);

    List<ProjectDetail> projectDetailList = new ArrayList<>();
    projectDetailList.add(getProjectDetail(projectId));

    //when
    when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);
    when(configurationService.getConfiguration(Mockito.any())).thenReturn("90");

    ProjectResponseV2 projectResponseV2=projectService.getMetadataV2("1",projectId);

    //then
    Assert.assertNotNull(projectResponseV2);
    Assert.assertEquals(projectResponseV2.getId(), projectId);
    Assert.assertEquals(projectResponseV2.getConfidenceThreshold(), 90);
  }
  @Test(expectedExceptions = NotFoundException.class)
  public void getMetadataV2ShouldThrowNotFoundException() throws Exception{
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest=getProjectRequest(projectId);

    List<ProjectDetail> projectDetailList = new ArrayList<>();

    //when
    when(projectDetailDao.getProjectDetails(Mockito.any(), Mockito.any())).thenReturn(projectDetailList);

    //then
    projectService.getMetadataV2("1",projectId);
  }

    private ProjectDetail getProjectDetail(String id) {
        ProjectDetail projectDetail = new ProjectDetail();
        projectDetail.setId(id);
        projectDetail.setName("Test Project");
        projectDetail.setEnvironment(Environment.staging);
        projectDetail.setCustomFieldDetailSet(new HashSet<CustomFieldDetail>());
        projectDetail.setStandardFieldDetailSet(new HashSet<StandardFieldDetail>());
        return projectDetail;
    }

    private ProjectActivityDetail getProjectActivityDetail(String id) {
        ProjectActivityDetail projectActivityDetail = new ProjectActivityDetail();
        projectActivityDetail.setProjectId(id);
        projectActivityDetail.setProjectName("Test Project");
        projectActivityDetail.setActivityId(0);
        return projectActivityDetail;
    }

    private ProjectRequest getProjectRequest(String id) {
        ProjectRequest projectRequest = new ProjectRequest();
        projectRequest.setName("Test Project");
        projectRequest.setId(id);
        projectRequest.setEnvironment(Environment.staging);
        projectRequest.setOrganizationId("1");

        Fields fields = new Fields();
        fields.setCustom(new ArrayList<>());
        fields.setStandard(new String[]{"field1"});

        projectRequest.setFields(fields);
        return projectRequest;
    }
    private PatchRequest getPatchRequest(PatchOperation patchOperation , String path , String value){
      PatchRequest patchRequest = new PatchRequest();
      patchRequest.setOp(patchOperation);
      patchRequest.setPath(path);
      patchRequest.setValue(value);
      return patchRequest;
    }

}