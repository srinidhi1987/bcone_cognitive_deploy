package com.automationanywhere.cognitive.project.Util;

import org.testng.annotations.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by msundell on 5/12/17
 */
public class FieldUtilTest {

  @Test
  public void testOfFieldExtractShouldYieldAFieldSet() throws Exception {
    // given
    String input = "abc, 123, !@#";

    // when
    Set fieldSet = FieldUtil.extractFields(input);

    // then
    assertThat(fieldSet, is(not(nullValue())));
    assertThat(fieldSet.size(), is(3));
    assertThat(fieldSet.contains("abc"), is(true));
    assertThat(fieldSet.contains("123"), is(true));
    assertThat(fieldSet.contains("!@#"), is(true));
    assertThat(fieldSet.contains("1qaz"), is(false));
  }

  @Test
  public void testOfFieldExtractWithNullValueShouldYieldAnEmptyFieldSet() throws Exception {
    // given
    String input = null;

    // when
    Set fieldSet = FieldUtil.extractFields(input);

    // then
    assertThat(fieldSet, is(not(nullValue())));
    assertThat(fieldSet.size(), is(0));
  }

  @Test
  public void testOfFieldExtractWithAnEmptyStringValueShouldYieldAnEmptyFieldSet() throws Exception {
    // given
    String input = "";

    // when
    Set fieldSet = FieldUtil.extractFields(input);

    // then
    assertThat(fieldSet, is(not(nullValue())));
    assertThat(fieldSet.size(), is(0));
  }

  @Test
  public void testOfFieldExtractWithCommaSeparatedStringValuesShouldYieldAFieldMapWithAZeroLengthKeyEntry() throws Exception {
    // given
    String input = ", , , , ,";

    // when
    Set fieldSet = FieldUtil.extractFields(input);

    // then
    assertThat(fieldSet, is(not(nullValue())));
    assertThat(fieldSet.size(), is(1));
    assertThat(fieldSet.contains(""), is(true));
  }
}
