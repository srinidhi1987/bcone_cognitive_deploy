package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.TestSetInfoDao;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.TestSetInfoDataImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.TestSetInfo;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestSetInfoDataImporterTest {

  @Mock
  FileReader csvFileReader;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private TestSetInfoDao testSetInfoDao;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;

  @InjectMocks
  private TestSetInfoDataImporter testSetInfoDataImporter;

  @DataProvider(name = "TaskOptionsClearAppendMergeOverwriteProvider")
  public static Object[][] taskOptionsClearAppendMergeOverwriteProvider() {
    return new Object[][]{{TaskOptions.CLEAR}, {TaskOptions.APPEND}, {TaskOptions.MERGE},
        {TaskOptions.OVERWRITE}};
  }

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    testSetInfoDataImporter = new TestSetInfoDataImporter(csvFileReader, fileHelper,
        transitionResult, testSetInfoDao, importFilePatternConstructor);
  }

  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionWhileFetchingDataFromCsv()
      throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    TestSetInfoDataImporter testSetInfoDataImporterSpy = Mockito.spy(testSetInfoDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("TestSetInfo_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doThrow(ProjectImportException.class).when(testSetInfoDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    testSetInfoDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);
  }

  @Test(dataProvider = "TaskOptionsClearAppendMergeOverwriteProvider")
  public void importDataShouldImportDataWithAllTaskOptions(TaskOptions taskOptions)
      throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo> testSetInfoList = getTestSetInfoData();
    List<String> visionBotIdList = new ArrayList<>();
    testSetInfoList.forEach(testSetInfo -> visionBotIdList.add(testSetInfo.getVisionbotid()));
    //when
    TestSetInfoDataImporter testSetInfoDataImporterSpy = Mockito.spy(testSetInfoDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("TestSetInfo_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(testSetInfoList).when(testSetInfoDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getImportedVisionBotIdList()).thenReturn(visionBotIdList);
    testSetInfoDataImporterSpy.importData(Paths.get("."), taskOptions);
    //then
    Mockito.verify(testSetInfoDao, Mockito.atLeastOnce())
        .batchInsertTestSetInfo(Mockito.anyListOf(TestSetInfo.class));
  }

  private List<com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo> getTestSetInfoData() {
    List<com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo> testSetInfoList = new ArrayList<>();
    com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo testSetInfo = new com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo();
    testSetInfo.setId("123");
    testSetInfo.setLayoutid("456");
    testSetInfo.setVisionbotid("789");
    testSetInfoList.add(testSetInfo);
    return testSetInfoList;
  }
}
