package com.automationanywhere.cognitive.project.export.Exporter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.ContentTrainSetDao;
import com.automationanywhere.cognitive.project.dal.LayoutTrainSetDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ClassifierDataExporterTest {

  @InjectMocks
  private ClassifierDataExporter classifierDataExporter;

  @Mock
  private ContentTrainSetDao contentTrainSetDao;

  @Mock
  private LayoutTrainSetDao layoutTrainSetDao;

  @Mock
  private FileWriter csvFileWriter;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = {ProjectExportException.class})
  public void exportDataShouldThrowProjectExportException() throws IOException {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("classificationids", Arrays.asList("123"));
    params.put("layoutids", Arrays.asList("5"));
    List<String[]> contentTrainSetData = new ArrayList<String[]>();
    contentTrainSetData.add(new String[]{"1", "data1"});
    contentTrainSetData.add(new String[]{"2", "data2"});

    Mockito.doThrow(IOException.class).when(csvFileWriter)
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));

    when(contentTrainSetDao.getContentTrainSets(Mockito.anyList())).thenReturn(contentTrainSetData);

    classifierDataExporter.exportData("./", params);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void exportContentTrainSetDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("classificationids", Arrays.asList("123"));
    params.put("layoutids", Arrays.asList("5"));
    List<String[]> contentTrainSetData = new ArrayList<String[]>();
    contentTrainSetData.add(new String[]{"1", "data1"});
    contentTrainSetData.add(new String[]{"2", "data2"});

    //when
    when(contentTrainSetDao.getContentTrainSets(Mockito.anyList())).thenReturn(contentTrainSetData);
    classifierDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(contentTrainSetDao, csvFileWriter);
    order.verify(contentTrainSetDao).getContentTrainSets(Mockito.anyList());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testExportContentTrainSetDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("classificationids", Arrays.asList("123"));
    params.put("layoutids", Arrays.asList("5"));
    List<String[]> contentTrainSetData = null;
    //when
    when(contentTrainSetDao.getContentTrainSets(Mockito.anyList())).thenReturn(contentTrainSetData);
    classifierDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void exportLayoutTrainSetDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("classificationids", Arrays.asList("123"));
    params.put("layoutids", Arrays.asList("5"));
    List<String[]> layoutTrainSetData = new ArrayList<String[]>();
    layoutTrainSetData.add(new String[]{"10414014098", "8", "null"});
    layoutTrainSetData.add(new String[]{"10414014099", "10", "null"});

    //when
    when(layoutTrainSetDao.getLayoutTrainSet(Mockito.anyList())).thenReturn(layoutTrainSetData);

    classifierDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(layoutTrainSetDao, csvFileWriter);
    order.verify(layoutTrainSetDao).getLayoutTrainSet(Mockito.anyList());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testExportLayoutTrainSetDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("classificationids", Arrays.asList("123"));
    params.put("layoutids", Arrays.asList("5"));
    List<String[]> layoutTrainSetData = null;
    //when
    when(layoutTrainSetDao.getLayoutTrainSet(Mockito.anyList())).thenReturn(layoutTrainSetData);
    classifierDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

}
