package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.StandardFieldDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.csvmodel.StandardFieldDetailCsv;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.StandardFieldDetailDataImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail.PrimaryKey;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class StandardFieldDetailImporterImplTest {

  @InjectMocks
  StandardFieldDetailDataImporter importer;
  @Mock
  ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  StandardFieldDetailDao standardFieldDetailDao;
  @Mock
  AdvanceDataImporter nextDataImporter;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private FileReader fileReader;
  @Captor
  private ArgumentCaptor<List<StandardFieldDetail>> argumentCaptor;


  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    importer = new StandardFieldDetailDataImporter
        (fileReader, standardFieldDetailDao, transitionResult, fileHelper,
            importFilePatternConstructor);
  }

  //Append Mode
  @SuppressWarnings("unchecked")
  @Test
  public void importDataInAppendModeShouldImportStandardFieldData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(standardFieldDetailDao, times(1)).addAll(Mockito.any());
    Mockito.verify(standardFieldDetailDao).addAll(argumentCaptor.capture());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void setNextShouldInvokeNextImporterWhenNextImporterAvailable()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    AdvanceDataImporter nextImporter = Mockito.mock(AdvanceDataImporter.class);
    StandardFieldDetailDataImporter standardFieldDataSpy = Mockito.spy(importer);
    standardFieldDataSpy.setNext(nextImporter);

    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<com.automationanywhere.cognitive.project.imports.csvmodel.StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) standardFieldDataSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    standardFieldDataSpy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(nextImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importDataInAppendModeShouldThrowExceptionWhenFailToFetchFilesPaths()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    importer.importData(directoryPath, TaskOptions.APPEND);

  }

  //Merge Mode
  @SuppressWarnings("unchecked")
  @Test
  public void importDataInMergeModeShouldAddStandardFieldsForNewProject()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(true);

    List<StandardFieldDetail> existingData = new ArrayList<>();
    Mockito.doReturn(existingData).when(standardFieldDetailDao).getAll(Mockito.any());

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(standardFieldDetailDao, times(1)).addAll(Mockito.any());
    Mockito.verify(standardFieldDetailDao).addAll(argumentCaptor.capture());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataInMergeModeShouldAddStandardFieldsForNewEditedProject()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(true);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfEditedImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    List<StandardFieldDetail> existingData = new ArrayList<>();
    Mockito.doReturn(existingData).when(standardFieldDetailDao).getAll(Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(standardFieldDetailDao, times(1)).addAll(Mockito.any());
    Mockito.verify(standardFieldDetailDao).addAll(argumentCaptor.capture());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataInMergeModeShouldNotAddStandardFieldsForExistingProject()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(false);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    List<StandardFieldDetail> existingData = new ArrayList<>();
    existingData.add(getStandardFieldDetailOfExistingProject());
    Mockito.doReturn(existingData).when(standardFieldDetailDao).getAll(Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(standardFieldDetailDao, times(0)).addAll(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataInMergeModeShouldThrowExceptionForExistingEditedImportProject()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(false);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfEditedImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    List<StandardFieldDetail> existingData = new ArrayList<>();
    existingData.add(getStandardFieldDetailOfExistingProject());
    Mockito.doReturn(existingData).when(standardFieldDetailDao).getAll(Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataInMergeModeShouldThrowExceptionForExistingEditedProject()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(false);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    List<StandardFieldDetail> existingData = new ArrayList<>();
    existingData.add(getStandardFieldDetailOfExistingEditedProject());
    Mockito.doReturn(existingData).when(standardFieldDetailDao).getAll(Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataInMergeModeShouldThrowExceptionForExistingEditedImportProjectWithNoExistingFields()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(false);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfEditedImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    List<StandardFieldDetail> existingData = new ArrayList<>();
    Mockito.doReturn(existingData).when(standardFieldDetailDao).getAll(Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);
  }

  //Overwrite Mode
  @SuppressWarnings("unchecked")
  @Test
  public void importDataInOverwriteModeShouldUpdateStandardFieldsForExistingProject()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(transitionResult.isNewProject()).thenReturn(false);

    StandardFieldDetailDataImporter spy = Mockito.spy(importer);
    List<StandardFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getStandardFieldDetailOfEditedImportProject());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.OVERWRITE);

    //then
    Mockito.verify(standardFieldDetailDao, times(1)).delete(Mockito.any());
    Mockito.verify(standardFieldDetailDao, times(1)).addAll(Mockito.any());
    Mockito.verify(standardFieldDetailDao).addAll(argumentCaptor.capture());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
  }

  private StandardFieldDetailCsv getStandardFieldDetailOfImportProject() {
    StandardFieldDetailCsv model = new StandardFieldDetailCsv();
    model.setId("1");
    model.setProjectId("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    model.setOrder(1);
    model.setAddedLater(false);
    return model;
  }

  private StandardFieldDetailCsv getStandardFieldDetailOfEditedImportProject() {
    StandardFieldDetailCsv model = new StandardFieldDetailCsv();
    model.setId("2");
    model.setProjectId("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    model.setOrder(2);
    model.setAddedLater(true);
    return model;
  }

  private StandardFieldDetail getStandardFieldDetailOfExistingProject() {

    StandardFieldDetail model = new StandardFieldDetail
        (new PrimaryKey("1", "4b08ac1d-15d0-4a24-93ab-4fc6b80f57df"), 1, false);

    return model;
  }

  private StandardFieldDetail getStandardFieldDetailOfExistingEditedProject() {
    return new StandardFieldDetail(new PrimaryKey("2", "4b08ac1d-15d0-4a24-93ab-4fc6b80f57df"), 2,
        true);
  }

}
