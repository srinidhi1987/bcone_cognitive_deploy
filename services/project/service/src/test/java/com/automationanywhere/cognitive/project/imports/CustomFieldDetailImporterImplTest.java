package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.impl.CustomFieldDetailDaoImpl;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.csvmodel.CustomFieldDetailCsv;
import com.automationanywhere.cognitive.project.imports.impl.CustomFieldDetailDataImporter;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.model.FieldType;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.imports.CustomFieldDetail;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomFieldDetailImporterImplTest {

  private final String testProjectId = "4b08ac1d-15d0-4a24-93ab-4fc6b80f57df";
  @InjectMocks
  CustomFieldDetailDataImporter importer;
  @Mock
  ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  CustomFieldDetailDaoImpl customFieldDetailDao;
  @Mock
  AdvanceDataImporter nextDataImporter;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private FileReader fileReader;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    importer = new CustomFieldDetailDataImporter
        (fileReader, customFieldDetailDao, transitionResult, fileHelper,
            importFilePatternConstructor);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddDataForOverwriteOption()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(testProjectId);
    when(transitionResult.isNewProject()).thenReturn(false);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<CustomFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getCustomFieldDetailCsvModel("1", false));
    csvData.add(getCustomFieldDetailCsvModel("2", true));

    CustomFieldDetailDataImporter spy = Mockito.spy(importer);

    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.OVERWRITE);

    //then
    Mockito.verify(customFieldDetailDao, times(1)).delete(testProjectId);
    Mockito.verify(customFieldDetailDao, times(1)).addAll(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = ProjectImportException.class)
  public void importShouldThrowExceptionForMergeOptionWhenLatelyAddedFieldFoundInExistingData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(testProjectId);
    when(transitionResult.isNewProject()).thenReturn(false);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<CustomFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getCustomFieldDetailCsvModel("1", true));

    List<CustomFieldDetail> existingData = new ArrayList<>();
    existingData.add(getCustomFieldDetail("2", true));
    when(customFieldDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    CustomFieldDetailDataImporter spy = Mockito.spy(importer);

    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = ProjectImportException.class)
  public void importShouldThrowExceptionForMergeOptionWhenLatelyAddedFieldFoundInImportingData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(testProjectId);
    when(transitionResult.isNewProject()).thenReturn(false);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<CustomFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getCustomFieldDetailCsvModel("1", true));

    List<CustomFieldDetail> existingData = new ArrayList<>();
    when(customFieldDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    CustomFieldDetailDataImporter spy = Mockito.spy(importer);

    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldNotAddDataForMergeOptionWhenProjectExists()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(testProjectId);
    when(transitionResult.isNewProject()).thenReturn(false);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<CustomFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getCustomFieldDetailCsvModel("1", false));

    List<CustomFieldDetail> existingData = new ArrayList<>();
    existingData.add(getCustomFieldDetail("2", false));
    when(customFieldDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    CustomFieldDetailDataImporter spy = Mockito.spy(importer);

    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(customFieldDetailDao, times(0)).addAll(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddDataForMergeOptionWhenProjectIsNewlyAdded()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(testProjectId);
    when(transitionResult.isNewProject()).thenReturn(true);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<CustomFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getCustomFieldDetailCsvModel("1", true));

    List<CustomFieldDetail> existingData = new ArrayList<>();
    when(customFieldDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    CustomFieldDetailDataImporter spy = Mockito.spy(importer);

    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(customFieldDetailDao, times(1)).addAll(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataInAppendModeShouldImportCustomFieldDetail()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    CustomFieldDetailDataImporter spy = Mockito.spy(importer);
    List<CustomFieldDetailCsv> csvData = new ArrayList<>();
    csvData.add(getCustomFieldDetailCsvModel("1", false));
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(customFieldDetailDao, times(1)).addAll(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void setNextShouldInvokeNextImporterWhenNextImpoterAvailable()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    CustomFieldDetailDataImporter customFieldSpy = Mockito.spy(importer);
    List<CustomFieldDetailCsv> csvData = new ArrayList<CustomFieldDetailCsv>();
    csvData.add(getCustomFieldDetailCsvModel("1", false));
    Mockito.doReturn(csvData).when((AdvanceDataImporter) customFieldSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    AdvanceDataImporter nextImporter = Mockito.mock(AdvanceDataImporter.class);
    customFieldSpy.setNext(nextImporter);

    //when
    customFieldSpy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(nextImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importDataInAppendModeShouldThrowIOExceptionWhenFailToFetchFilesPaths()
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    importer.importData(directoryPath, TaskOptions.APPEND);

  }

  private CustomFieldDetailCsv getCustomFieldDetailCsvModel(String id, boolean addedLater) {
    CustomFieldDetailCsv model = new CustomFieldDetailCsv();
    model.setId(id);
    model.setProjectId(testProjectId);
    model.setOrder(1);
    model.setFieldType("FormField");
    model.setAddedLater(addedLater);
    return model;
  }

  private CustomFieldDetail getCustomFieldDetail(String id, boolean addedLater) {
    CustomFieldDetail model = new CustomFieldDetail();
    model.setId(id);
    model.setProjectId(testProjectId);
    model.setOrder(1);
    model.setType(FieldType.FormField);
    model.setAddedLater(addedLater);
    return model;
  }


}
