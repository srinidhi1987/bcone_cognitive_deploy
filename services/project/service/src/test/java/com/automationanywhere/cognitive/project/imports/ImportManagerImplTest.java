package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.dal.DataCleaningDao;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.impl.FileZipper;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.ImportManagerImpl;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.Task;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ImportManagerImplTest {

  @Mock
  AdvanceDataImporter contentTrainSetDataImporter;
  @Mock
  AdvanceDataImporter projectDetailDataImporter;
  @Mock
  AdvanceDataImporter customFieldDetailDataImporter;
  @Mock
  AdvanceDataImporter standardFieldDetailDataImporter;
  @Mock
  AdvanceDataImporter projectOcrEngineDetailMasterMappingImporter;
  @Mock
  AdvanceDataImporter fileDetailsDataImporter;
  @Mock
  AdvanceDataImporter fileBlobsDataImporter;
  @Mock
  AdvanceDataImporter classificationReportDataImporter;
  @Mock
  AdvanceDataImporter testSetInfoDataImporter;
  @Mock
  AdvanceDataImporter layoutTrainSetDataImporter;
  @Mock
  AdvanceDataImporter visionBotDetailDataImporter;
  @Mock
  AdvanceDataImporter visionBotDataImporter;
  @Mock
  AdvanceDataImporter testSetDataImporter;
  @Mock
  AdvanceDataImporter trainingDataImporter;
  @InjectMocks
  private ImportManagerImpl importManager;
  @Mock
  private DataImporter dataImporter;
  @Mock
  private FileZipper fileZipper;
  @Mock
  private ImportEvents importEvents;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private DataCleaningDao dataCleaningDao;
  @Mock
  private FileReader fileReader;
  @Mock
  private FileWriter fileWriter;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private AdvanceDataImporter visionBotRunDetailsDataImporter;
  @Mock
  private AdvanceDataImporter documentProcessedDetailsDataImporter;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    importManager = new ImportManagerImpl(dataImporter, fileZipper, fileReader, fileWriter,
        fileHelper,
        contentTrainSetDataImporter, layoutTrainSetDataImporter, projectDetailDataImporter,
        customFieldDetailDataImporter, standardFieldDetailDataImporter,
        projectOcrEngineDetailMasterMappingImporter, dataCleaningDao, fileDetailsDataImporter,
        fileBlobsDataImporter, classificationReportDataImporter, visionBotDetailDataImporter,
        testSetInfoDataImporter,
        visionBotDataImporter,
        testSetDataImporter, visionBotRunDetailsDataImporter, documentProcessedDetailsDataImporter,
        trainingDataImporter, transitionResult);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testRun() throws Exception {
    Task task = new Task();
    task.setDescription("directory/abc.iqba");
    task.setTaskOptions(TaskOptions.APPEND);

    Path archivePath = Paths.get(task.getDescription());
    Path archiveParentPath = archivePath.getParent();
    Path archiveDirectoryPath = Paths.get(archiveParentPath.toString(),
        archivePath.getFileName().toString().replace(".iqba", ""));

    //given
    Mockito.when(fileHelper.getAllFilesPaths(archiveDirectoryPath)).thenReturn(getProjectIdList());
    Mockito.doNothing().when(fileZipper).unzipIt(archivePath, archiveParentPath);
    Mockito.when(fileHelper.removeDirectory(archiveDirectoryPath.toFile())).thenReturn(true);

    //when
    importManager.run(task, importEvents);

    //then
    InOrder order = Mockito.inOrder(fileZipper, contentTrainSetDataImporter, fileHelper);
    Mockito.verify(fileZipper).unzipIt(archivePath, archiveParentPath);
    Mockito.verify(contentTrainSetDataImporter)
        .importData(archiveDirectoryPath, task.getTaskOptions());
    Mockito.verify(fileHelper).removeDirectory(archiveDirectoryPath.toFile());
  }

  @Test
  public void runShouldHandleException() throws Exception {
    Task task = new Task();
    task.setDescription("directory/abc.iqba");

    Path archivePath = Paths.get(task.getDescription());
    Path archiveParentPath = archivePath.getParent();
    Path archiveDirectoryPath = Paths.get(archiveParentPath.toString(),
        archivePath.getFileName().toString().replace(".iqba", ""));

    //given
    Mockito.doThrow(new IOException()).when(fileZipper).unzipIt(archivePath, archiveParentPath);

    //when
    importManager.run(task, importEvents);

    //then
    InOrder order = Mockito.inOrder(importEvents);
    Mockito.verify(importEvents).onError(Mockito.any());
  }

  private List<Path> getProjectIdList() {
    List<Path> projectIdList = new ArrayList<>();
    projectIdList.add(Paths.get(
        "\\\\engglt23\\output path\\BackupData\\test_20180503_064352\\42ca89b2-3e93-4314-b66b-1ef711a371ba"));
    return projectIdList;
  }
}
