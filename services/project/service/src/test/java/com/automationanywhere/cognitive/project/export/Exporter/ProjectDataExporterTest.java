package com.automationanywhere.cognitive.project.export.Exporter;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.CustomFieldDetailDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.dal.ProjectOCREngineDetailsMasterMappingDao;
import com.automationanywhere.cognitive.project.dal.StandardFieldDetailDao;
import com.automationanywhere.cognitive.project.export.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ProjectDataExporterTest {

  @InjectMocks
  private ProjectDataExporter projectDataExporter;

  @Mock
  private ProjectDetailDao projectDetailDao;
  @Mock
  private ProjectOCREngineDetailsMasterMappingDao projOcrDetailMapDao;
  @Mock
  private CustomFieldDetailDao customFieldDetailDao;
  @Mock
  private StandardFieldDetailDao standardFieldDetailDao;
  @Mock
  private FileWriter csvFileWriter;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void exportProjectDetailsDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    String[] projectDetailArr = new String[]{"a84d55de-2c99-4613-9813-a779caf1b964", "jemin",
        "null", "1", "Invoices", "1", "1", "training", "production",
        "c5d4062a-eb13-43a6-ac2e-0d6cd2439303", "TRUE", "null", "null", "17:39.6", "23:55.6"};

    when(projectDetailDao.getProjectDetail(Mockito.anyString())).thenReturn(projectDetailArr);

    projectDataExporter.exportData("./", params);

    //then
    InOrder order = inOrder(projectDetailDao, csvFileWriter);
    order.verify(projectDetailDao).getProjectDetail(Mockito.anyString());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportProjectDetailsDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    String[] projectDetailArr = null;

    when(projectDetailDao.getProjectDetail(Mockito.anyString())).thenReturn(projectDetailArr);

    projectDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportProjectOCREngineDetailsMasterMappingDataShouldWriteDataToFile()
      throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> projOcrDetailMapData = new ArrayList<>();
    projOcrDetailMapData.add(new String[]{"5", "a84d55de-2c99-4613-9813-a779caf1b964", "1"});

    when(projOcrDetailMapDao.getProjectOCREngineDetailsMasterMappings(Mockito.anyString()))
        .thenReturn(projOcrDetailMapData);

    projectDataExporter.exportData("./", params);

    //then
    InOrder order = inOrder(projOcrDetailMapDao, csvFileWriter);
    order.verify(projOcrDetailMapDao).getProjectOCREngineDetailsMasterMappings(Mockito.anyString());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportProjectOCREngineDetailsMasterMappingDataShouldNotWriteDataToFile()
      throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> projOcrDetailMapData = null;

    when(projOcrDetailMapDao.getProjectOCREngineDetailsMasterMappings(Mockito.anyString()))
        .thenReturn(projOcrDetailMapData);

    projectDataExporter.exportData("./", params);

    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportCustomFieldDetailDataShouldWriteDataToFile()
      throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> customFieldDetailData = new ArrayList<>();
    customFieldDetailData.add(new String[]{"id", "name", "fieldType", "projectId", "orderNumber"});

    when(customFieldDetailDao.getCustomFieldDetails(Mockito.anyString()))
        .thenReturn(customFieldDetailData);

    projectDataExporter.exportData("./", params);

    //then
    InOrder order = inOrder(customFieldDetailDao, csvFileWriter);
    order.verify(customFieldDetailDao).getCustomFieldDetails(Mockito.anyString());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportCustomFieldDetailDataShouldNotWriteDataToFile()
      throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> customFieldDetailData = null;

    when(customFieldDetailDao.getCustomFieldDetails(Mockito.anyString()))
        .thenReturn(customFieldDetailData);

    projectDataExporter.exportData("./", params);

    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }
  
  public void exportStandardFieldDetailDataShouldWriteDataToFile()
      throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> standardFieldDetailData = new ArrayList<>();
    standardFieldDetailData.add(new String[]{"1", "a84d55de-2c99-4613-9813-a779caf1b964", "1"});

    when(standardFieldDetailDao.getStandardFieldDetails(Mockito.anyString()))
        .thenReturn(standardFieldDetailData);

    projectDataExporter.exportData("./", params);

    //then
    InOrder order = inOrder(standardFieldDetailDao, csvFileWriter);
    order.verify(standardFieldDetailDao.getStandardFieldDetails(Mockito.anyString()));
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportStandardFieldDetailDataShouldNotWriteDataToFile()
      throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> standardFieldDetailData = null;

    when(standardFieldDetailDao.getStandardFieldDetails(Mockito.anyString()))
        .thenReturn(standardFieldDetailData);

    projectDataExporter.exportData("./", params);

    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }
}
