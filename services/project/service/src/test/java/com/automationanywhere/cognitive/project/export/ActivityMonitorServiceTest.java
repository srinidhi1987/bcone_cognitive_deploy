package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.export.impl.ActivityMonitorServiceImpl;
import com.automationanywhere.cognitive.project.messagequeue.QueuingService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActivityMonitorServiceTest {

    @Mock(name="documentProcessingQueuingService")
    QueuingService mockDocumentProcessingQueuingService;

    @Mock(name="classificationQueuingService")
    QueuingService mockClassificationQueuingService;

    @InjectMocks
    ActivityMonitorServiceImpl activityMonitorService;

    @BeforeMethod
    public void setUp() {
        activityMonitorService = new ActivityMonitorServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ClassificationShouldBeRunningWhenClassificationQueueIsNotEmpty() throws Exception
    {
        Mockito.when(mockClassificationQueuingService.getMessageCount()).thenReturn(1);
        boolean result = activityMonitorService.isClassifierInProgress();
        Assert.assertEquals(result, true);
    }

    @Test
    public void ClassificationShouldNotBeRunningWhenClassificationQueueIsEmpty() throws Exception
    {
        Mockito.when(mockClassificationQueuingService.getMessageCount()).thenReturn(0);
        boolean result = activityMonitorService.isClassifierInProgress();
        Assert.assertEquals(result, false);
    }

    @Test
    public void DocumentProcessingShouldBeRunningWhenProcessingQueueNotIsEmpty() throws Exception
    {
        Mockito.when(mockDocumentProcessingQueuingService.getMessageCount()).thenReturn(1);
        boolean result = activityMonitorService.isClassifierInProgress();
        Assert.assertEquals(result, false);
    }

    @Test
    public void DocumentProcessingShouldNotBeRunningWhenProcessingQueueIsEmpty() throws Exception
    {
        Mockito.when(mockDocumentProcessingQueuingService.getMessageCount()).thenReturn(0);
        boolean result = activityMonitorService.isClassifierInProgress();
        Assert.assertEquals(result, false);
    }
}
