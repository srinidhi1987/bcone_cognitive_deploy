package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.export.ActivityMonitorService;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.helper.PathHelper;
import com.automationanywhere.cognitive.project.helper.ProductReleaseFileReader;
import com.automationanywhere.cognitive.project.imports.impl.ImportServiceImpl;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.TaskStatus;
import com.automationanywhere.cognitive.project.model.TaskType;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.imports.ArchiveInfo;
import com.automationanywhere.cognitive.project.model.dto.imports.ProjectImportRequest;
import com.automationanywhere.cognitive.project.task.TaskService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ImportServiceImplTest {
    @Mock
    private TaskService taskService;

    @Mock
    private FileHelper fileHelper;

    @Mock
    private PathHelper pathHelper;

    @Mock(name = "importRunner")
    private ImportRunner importRunner;

    @InjectMocks
    private ImportServiceImpl importService;

    @Mock
    private ProductReleaseFileReader productReleaseFileReader;

    @Mock
    private ActivityMonitorService activityMonitorService;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = PreconditionFailedException.class)
    public void importThrowsExceptionWhenRequestIsNull()
        throws PreconditionFailedException, SystemBusyException {
        ProjectImportRequest projectImportRequest = null;
        importService.startImport(projectImportRequest);
    }

    @Test(expectedExceptions = PreconditionFailedException.class)
    public void importThrowsExceptionWhenArchiveDetailsIsNull()
        throws PreconditionFailedException, SystemBusyException {
        ProjectImportRequest projectImportRequest = new ProjectImportRequest();
        importService.startImport(projectImportRequest);
    }

  @Test(expectedExceptions = PreconditionFailedException.class)
  public void importThrowsExceptionWhenImportOptionIsNotValid()
      throws PreconditionFailedException, SystemBusyException {
    ProjectImportRequest projectImportRequest = new ProjectImportRequest();
    projectImportRequest.setArchiveDetails(new ArchiveInfo());
    projectImportRequest.setOption("anything");
    importService.startImport(projectImportRequest);
  }

    @Test(expectedExceptions = PreconditionFailedException.class)
    public void importThrowsExceptionWhenArchiveFileNotFound()
        throws PreconditionFailedException, SystemBusyException {
        Mockito.when(fileHelper.isFileExists(Mockito.anyString())).thenReturn(false);
        Mockito.when(pathHelper.join(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString())).thenReturn("\\test\\path");
        ProjectImportRequest projectImportRequest = getProjectImportRequest();
        importService.startImport(projectImportRequest);
    }

    @Test
    public void startImportReturnsTaskWhenSuccessfullyTriggered()
        throws PreconditionFailedException, SystemBusyException {
        String path="\\test\\path\\Test.iqba";
        Mockito.when(fileHelper.isFileExists(Mockito.anyString())).thenReturn(true);
        Mockito.when(pathHelper.join(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString())).thenReturn(path);
        ProjectImportRequest projectImportRequest = getProjectImportRequest();
        // this value needs to update as per product version
        Mockito.when(fileHelper.readVersionFromIqbaFile(path)).thenReturn("5.3.1");
        Mockito.when(productReleaseFileReader.getCurrentProductVersion()).thenReturn("5.3.1");
        Mockito.when(taskService.createImportTask(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(getImportTask());

        Task task = importService.startImport(projectImportRequest);

        Assert.assertNotNull(task);
        Assert.assertEquals(task.getTaskType(), TaskType.imports);
    }

    @Test
    public void getArchiveDetailsReturnsListOfAllFilesWithImportExtension()
    {
        Mockito.when(fileHelper.getAllFilesInFolder(Mockito.anyString())).thenReturn(getFileNameList());
        List<ArchiveInfo> archiveInfoList = importService.getArchiveDetails();

        Assert.assertNotNull(archiveInfoList);
        Assert.assertEquals(archiveInfoList.size(), 2);
    }

    @Test
    public void getArchiveDetailsReturnsEmptyListWhenNoFileFound()
    {
        Mockito.when(fileHelper.getAllFilesInFolder(Mockito.anyString())).thenReturn(new ArrayList<>());
        List<ArchiveInfo> archiveInfoList = importService.getArchiveDetails();

        Assert.assertNotNull(archiveInfoList);
        Assert.assertEquals(archiveInfoList.size(), 0);
    }

  @Test
  public void testReadVersionFromIqbaFile() {
    Assert.assertNull(fileHelper.readVersionFromIqbaFile("\\test\\path\\Test.iqba"));
  }

  @Test(expectedExceptions = SystemBusyException.class)
  public void importThrowsExceptionWhenClassificationInProgress()
      throws PreconditionFailedException, SystemBusyException {
    Mockito.when(activityMonitorService.isClassifierInProgress()).thenReturn(true);
    String path = "\\test\\path\\Test.iqba";
    Mockito.when(fileHelper.isFileExists(Mockito.anyString())).thenReturn(true);
    Mockito.when(pathHelper.join(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString()))
        .thenReturn(path);
    ProjectImportRequest projectImportRequest = getProjectImportRequest();
    // this value needs to update as per product version
    Mockito.when(fileHelper.readVersionFromIqbaFile(path)).thenReturn("5.3.1");
    Mockito.when(productReleaseFileReader.getCurrentProductVersion()).thenReturn("5.3.1");
    Mockito
        .when(taskService.createImportTask(Mockito.anyString(), Mockito.anyString(), Mockito.any()))
        .thenReturn(getImportTask());
    importService.startImport(projectImportRequest);
  }

  @Test(expectedExceptions = SystemBusyException.class)
  public void importThrowsExceptionWhenDataExtractionInProgress()
      throws PreconditionFailedException, SystemBusyException {
    Mockito.when(activityMonitorService.isStagingDocumentsInProgress()).thenReturn(true);
    String path = "\\test\\path\\Test.iqba";
    Mockito.when(fileHelper.isFileExists(Mockito.anyString())).thenReturn(true);
    Mockito.when(pathHelper.join(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString()))
        .thenReturn(path);
    ProjectImportRequest projectImportRequest = getProjectImportRequest();
    // this value needs to update as per product version
    Mockito.when(fileHelper.readVersionFromIqbaFile(path)).thenReturn("5.3.1");
    Mockito.when(productReleaseFileReader.getCurrentProductVersion()).thenReturn("5.3.1");
    Mockito
        .when(taskService.createImportTask(Mockito.anyString(), Mockito.anyString(), Mockito.any()))
        .thenReturn(getImportTask());
    importService.startImport(projectImportRequest);
  }

  @Test(expectedExceptions = SystemBusyException.class)
  public void importThrowsExceptionWhenDataExtractionInProgressForProduction()
      throws PreconditionFailedException, SystemBusyException {
    Mockito.when(activityMonitorService.isProductionDocumentsInProgress()).thenReturn(true);
    String path = "\\test\\path\\Test.iqba";
    Mockito.when(fileHelper.isFileExists(Mockito.anyString())).thenReturn(true);
    Mockito.when(pathHelper.join(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString()))
        .thenReturn(path);
    ProjectImportRequest projectImportRequest = getProjectImportRequest();
    // this value needs to update as per product version
    Mockito.when(fileHelper.readVersionFromIqbaFile(path)).thenReturn("5.3.1");
    Mockito.when(productReleaseFileReader.getCurrentProductVersion()).thenReturn("5.3.1");
    Mockito
        .when(taskService.createImportTask(Mockito.anyString(), Mockito.anyString(), Mockito.any()))
        .thenReturn(getImportTask());
    importService.startImport(projectImportRequest);
  }

  @Test(expectedExceptions = SystemBusyException.class)
  public void importThrowExceptionWhenBotIsLocked()
      throws SystemBusyException, PreconditionFailedException {
    Mockito.when(activityMonitorService.isBotLocked()).thenReturn(true);
    String path = "\\test\\path\\Test.iqba";
    Mockito.when(fileHelper.isFileExists(Mockito.anyString())).thenReturn(true);
    Mockito.when(pathHelper.join(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString()))
        .thenReturn(path);
    ProjectImportRequest projectImportRequest = getProjectImportRequest();
    // this value needs to update as per product version
    Mockito.when(fileHelper.readVersionFromIqbaFile(path)).thenReturn("5.3.1");
    Mockito.when(productReleaseFileReader.getCurrentProductVersion()).thenReturn("5.3.1");
    Mockito
        .when(taskService.createImportTask(Mockito.anyString(), Mockito.anyString(), Mockito.any()))
        .thenReturn(getImportTask());
    importService.startImport(projectImportRequest);

  }

  private List<String> getFileNameList() {
        List<String> fileList = new ArrayList<>();
        fileList.add("test1.iqba");
        fileList.add("test2.iqba");
        fileList.add("abc.zip");
        return fileList;
    }

    private ProjectImportRequest getProjectImportRequest() {
        ProjectImportRequest projectImportRequest = new ProjectImportRequest();
        ArchiveInfo archiveInfo = new ArchiveInfo();
        archiveInfo.setArchiveName("Test.iqba");
        projectImportRequest.setArchiveDetails(archiveInfo);
        projectImportRequest.setUserId("testUser");
        projectImportRequest.setOption("append");
        return projectImportRequest;
    }

    private Task getImportTask(){
        Task task = new Task();
        task.setTaskId(UUID.randomUUID().toString());
        task.setTaskType(TaskType.imports);
        task.setStatus(TaskStatus.inprogress);
        task.setTaskOptions(TaskOptions.APPEND);
        task.setUserId("testUser");
        return task;
    }

}