package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.ClassificationReportDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.ClassificationReportDataImporter;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ClassificationReport;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ClassificationReportDataImporterTest {

  @Mock
  private FileReader csvFileReader;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private ClassificationReportDao classificationReportDao;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @InjectMocks
  private ClassificationReportDataImporter classificationReportDataImporter;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    classificationReportDataImporter = new ClassificationReportDataImporter(csvFileReader,fileHelper,transitionResult,classificationReportDao,importFilePatternConstructor);
  }

  @Test
  public void importDataShouldImportDataForClearTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds=new ArrayList<>();
    fileIds.add("doc123");
    //when
    ClassificationReportDataImporter classificationReportDataImporterSpy = Mockito.spy(
        classificationReportDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail()).when(classificationReportDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    classificationReportDataImporterSpy.importData(Paths.get("."), TaskOptions.CLEAR);
    //then
    Mockito.verify(classificationReportDao, Mockito.times(1))
        .batchInsertClassificationReport(Mockito.anyListOf(ClassificationReport.class));

  }

  @Test
  public void importDataShouldImportDataForAppendTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds=new ArrayList<>();
    fileIds.add("doc123");
    //when
    ClassificationReportDataImporter classificationReportDataImporterSpy = Mockito.spy(
        classificationReportDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail()).when(classificationReportDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    classificationReportDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);
    //then
    Mockito.verify(classificationReportDao,  Mockito.times(1))
        .batchInsertClassificationReport(Mockito.anyListOf(ClassificationReport.class));

  }

  @Test
  public void importDataShouldImportDataForOverwriteTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds=new ArrayList<>();
    fileIds.add("doc123");
    //when
    ClassificationReportDataImporter classificationReportDataImporterSpy = Mockito.spy(
        classificationReportDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail()).when(classificationReportDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    classificationReportDataImporterSpy.importData(Paths.get("."), TaskOptions.OVERWRITE);
    Mockito.doNothing().when(classificationReportDao).delete(fileIds);
    //then
    Mockito.verify(classificationReportDao,  Mockito.times(1))
        .batchInsertClassificationReport(Mockito.anyListOf(ClassificationReport.class));

  }

  @Test
  public void importDataShouldNotImportIfDataIsNotAvailableForAppendTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    ClassificationReportDataImporter classificationReportDataImporterSpy = Mockito.spy(
        classificationReportDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(new ArrayList()).when(classificationReportDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    classificationReportDataImporterSpy.importData(Paths.get("."),TaskOptions.APPEND);
    //then
    Mockito.verify(classificationReportDao, Mockito.times(0))
        .batchInsertClassificationReport(Mockito.anyListOf(ClassificationReport.class));

  }
  @Test
  public void importDataShouldImportDataForMergeTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds=new ArrayList<>();
    fileIds.add("doc123");
    //when
    ClassificationReportDataImporter classificationReportDataImporterSpy = Mockito.spy(
        classificationReportDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail()).when(classificationReportDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    classificationReportDataImporterSpy.importData(Paths.get("."), TaskOptions.MERGE);
    //then
    Mockito.verify(classificationReportDao,  Mockito.times(1))
        .batchInsertClassificationReport(Mockito.anyListOf(ClassificationReport.class));

  }
  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionWhileFetchingDataFromCsv()
      throws ProjectImportException, IOException, ParseException, DuplicateProjectNameException {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    ClassificationReportDataImporter classificationReportDataImporterSpy = Mockito.spy(
        classificationReportDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doThrow(ProjectImportException.class).when(classificationReportDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    classificationReportDataImporterSpy.importData(Paths.get("."),TaskOptions.APPEND);
  }

  private List<com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport> getFileDetail() {
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport> classificationReports = new ArrayList<>();
    com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport classificationReport = new com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport();
    classificationReport.setFieldId("123");
    classificationReport.setAliasName("abc");
    classificationReport.setDocumentId("doc123");
    classificationReport.setFound(true);
    classificationReports.add(classificationReport);
    return classificationReports;
  }

}
