package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.export.impl.VisionBotDataExportOperator;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VisionBotDataExportOperatorTest {

  VisionBotDataExportOperator visionBotDataExportOperator;
  @Mock
  private DataExporter dataExporter;
  @Mock
  private DataExportListener dataExportListener;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    visionBotDataExportOperator = new VisionBotDataExportOperator(dataExporter);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testExportVisionBotData() throws Exception {
    //given
    String exportDir = "Output//";
    String[] projectIds = {"Projid1", "Projid2"};

    visionBotDataExportOperator.export(exportDir, projectIds, dataExportListener);
    //when
    InOrder order = Mockito.inOrder(dataExporter, dataExportListener);

    Thread.sleep(1000L);
    //then
    order.verify(dataExporter, Mockito.times(2))
        .exportData(Mockito.anyString(), Mockito.anyMap());
    order.verify(dataExportListener).onVisionBotDataExportCompleted(Mockito.anyList());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testMultipleExportMlData() throws Exception {
    //given
    String exportDir = "Output//";
    String[] projectIds = {"Projid1", "Projid2", "Projid3", "Projid4", "Projid5", "Projid6",
        "Projid7", "Projid8", "Projid9", "Projid10"};

    visionBotDataExportOperator.export(exportDir, projectIds, dataExportListener);
    //when
    InOrder order = Mockito.inOrder(dataExporter, dataExportListener);

    Thread.sleep(1000L);
    //then
    order.verify(dataExporter, Mockito.times(10))
        .exportData(Mockito.anyString(), Mockito.anyMap());
    order.verify(dataExportListener).onVisionBotDataExportCompleted(Mockito.anyList());
  }
}
