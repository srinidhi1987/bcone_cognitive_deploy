package com.automationanywhere.cognitive.project.imports.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.LayoutTrainSetDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.LayoutTrainSet;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LayoutTrainSetDataImporterTest {

  @InjectMocks
  LayoutTrainSetDataImporter dataImporterImpl;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private LayoutTrainSetDao layoutTrainSetDao;
  @Mock
  private FileReader fileReader;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  private TransitionResult transitionResult;
  @Captor
  private ArgumentCaptor<List<LayoutTrainSet>> argumentCaptor;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    transitionResult = new TransitionResult();
    dataImporterImpl = new LayoutTrainSetDataImporter(fileReader, fileHelper, layoutTrainSetDao,
        importFilePatternConstructor, transitionResult);
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importShouldFailWhenIOExceptionCaught()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    dataImporterImpl.importData(Paths.get("."), TaskOptions.APPEND);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importShouldAddNewClasses()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<LayoutTrainSet> existingData = new ArrayList<LayoutTrainSet>();
    existingData.add(getLayoutTrainSetDao("1", "1"));
    existingData.add(getLayoutTrainSetDao("2", "2"));

    List<LayoutTrainSet> csvData = new ArrayList<LayoutTrainSet>();
    csvData.add(getLayoutTrainSetDao("3", "3"));

    LayoutTrainSetDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(layoutTrainSetDao.getAll()).thenReturn(existingData);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);
    Mockito.verify(layoutTrainSetDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(layoutTrainSetDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);
    Assert.assertEquals(transitionResult.getModifiedLayouts().size(), 1);
    Assert.assertEquals(transitionResult.getModifiedLayouts().get("3"), "3");
  }

  @Test
  public void importShouldNotAddExistingClasses()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {

    //given
    LayoutTrainSetDataImporter spy = configureLayoutTrainSetDataImporter();

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);
    Mockito.verify(layoutTrainSetDao).addAll(argumentCaptor.capture());

    //then
    Mockito.verify(layoutTrainSetDao, times(1)).addAll(Mockito.any());
    Assert.assertEquals(transitionResult.getModifiedLayouts().size(), 1);
    Assert.assertEquals(transitionResult.getModifiedLayouts().get("3"), "2");
    Assert.assertEquals(argumentCaptor.getValue().size(), 0);
  }

  @Test
  public void importShouldCallAnotherDataImporter()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    LayoutTrainSetDataImporter spy = configureLayoutTrainSetDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);
    spy.setNext(advanceDataImporter);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @Test
  public void importShouldNotCallAnotherDataImporterIfNextDataImporterIsNull()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    LayoutTrainSetDataImporter spy = configureLayoutTrainSetDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(0)).importData(Mockito.any(), Mockito.any());
  }


  private LayoutTrainSet getLayoutTrainSetDao(String hashkey, String className) {
    LayoutTrainSet LayoutTrainSet = new LayoutTrainSet();
    LayoutTrainSet.setHashKey(hashkey);
    LayoutTrainSet.setClassName(className);
    return LayoutTrainSet;
  }

  @SuppressWarnings("unchecked")
  private LayoutTrainSetDataImporter configureLayoutTrainSetDataImporter()
      throws ProjectImportException, IOException {
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<LayoutTrainSet> existingData = new ArrayList<LayoutTrainSet>();
    existingData.add(getLayoutTrainSetDao("1", "1"));
    existingData.add(getLayoutTrainSetDao("2", "2"));

    List<LayoutTrainSet> csvData = new ArrayList<LayoutTrainSet>();
    csvData.add(getLayoutTrainSetDao("2", "3"));

    LayoutTrainSetDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(layoutTrainSetDao.getAll()).thenReturn(existingData);
    return spy;
  }
}
