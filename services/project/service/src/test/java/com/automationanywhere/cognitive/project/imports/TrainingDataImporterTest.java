package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.TrainingDataDao;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.helper.PathHelper;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.TrainingDataImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.TrainingData;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TrainingDataImporterTest {

  @Mock
  FileReader csvFileReader;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private TrainingDataDao trainingdataDao;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  private PathHelper pathHelper;
  @Captor
  private ArgumentCaptor<List<TrainingData>> argumentCaptor;
  @InjectMocks
  private TrainingDataImporter trainingDataImporter;

  @DataProvider(name = "TaskOptionsClearOverwriteProvider")
  public static Object[][] taskOptionsClearOverwriteProvider() {
    return new Object[][]{{TaskOptions.CLEAR}, {TaskOptions.OVERWRITE}};
  }

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    trainingDataImporter = new TrainingDataImporter(csvFileReader, fileHelper, pathHelper, transitionResult,
        trainingdataDao, importFilePatternConstructor);
    Mockito.when(pathHelper.getSystemFolder()).thenReturn("c:\\windows\\syswow64");
  }

  @Test(dataProvider = "TaskOptionsClearOverwriteProvider")
  public void importDataShouldAddMoreRows(TaskOptions taskOptions) throws Exception {
    //given
    String projectId = "PrjId";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<com.automationanywhere.cognitive.project.imports.csvmodel.trainingData> trainingDataList = getTrainingData();

    TrainingDataImporter trainingDataImporterSpy = Mockito.spy(trainingDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectId);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("trainingData_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(trainingDataList).when(trainingDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.anyString())).thenReturn(getAllModelPaths());
    Mockito.when(trainingdataDao.getUniqueFields(projectId)).thenReturn(getFields(projectId));
    Mockito.when(trainingdataDao.getUniqueFields()).thenReturn(getAllFields());

    //when
    trainingDataImporterSpy.importData(Paths.get("."), taskOptions);
    Mockito.verify(trainingdataDao).batchInsertTrainingData(argumentCaptor.capture());

    //then
    if(taskOptions == TaskOptions.CLEAR) {
      Mockito.verify(trainingdataDao, Mockito.times(1)).deleteAll();
      Mockito.verify(fileHelper, Mockito.times(2)).deletFile(Mockito.any());
      Assert.assertEquals(argumentCaptor.getValue().size(), 1);
    } else {
      Mockito.verify(trainingdataDao, Mockito.times(1)).delete(projectId);
      Mockito.verify(fileHelper, Mockito.times(1)).deletFile(Mockito.any());
      Assert.assertEquals(argumentCaptor.getValue().size(), 1);
    }
  }

  @Test(dataProvider = "TaskOptionsClearOverwriteProvider")
  public void importDataShouldNotImportIfDataIsNotAvailable(TaskOptions taskOptions)
      throws Exception {
    //given
    String projectId = "PrjId";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));

    TrainingDataImporter trainingDataImporterSpy = Mockito.spy(trainingDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectId);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("trainingData_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(new ArrayList<com.automationanywhere.cognitive.project.imports.csvmodel.trainingData>()).when(trainingDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.anyString())).thenReturn(getAllModelPaths());
    Mockito.when(trainingdataDao.getUniqueFields(projectId)).thenReturn(getFields(projectId));
    Mockito.when(trainingdataDao.getUniqueFields()).thenReturn(getAllFields());

    //when
    trainingDataImporterSpy.importData(Paths.get("."), taskOptions);

    //then
    Mockito.verify(trainingdataDao, Mockito.times(0))
        .batchInsertTrainingData(Mockito.anyListOf(TrainingData.class));
  }

  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionWhileFetchingDataFromCsv()
      throws Exception {
    //given
    String projectid = "PrjId";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    TrainingDataImporter trainingDataImporterSpy = Mockito.spy(trainingDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("trainingData_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doThrow(ProjectImportException.class).when(trainingDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    trainingDataImporterSpy.importData(Paths.get("."), TaskOptions.CLEAR);
  }

  private List<com.automationanywhere.cognitive.project.imports.csvmodel.trainingData> getTrainingData() {
    List<com.automationanywhere.cognitive.project.imports.csvmodel.trainingData> trainingDataList = new ArrayList<>();
    com.automationanywhere.cognitive.project.imports.csvmodel.trainingData trainingData = new com.automationanywhere.cognitive.project.imports.csvmodel.trainingData();
    trainingData.setOriginalvalue("123");
    trainingData.setCorrectedvalue("456");
    trainingData.setFieldid("PrjId_FieldId");
    trainingData.setCorrectedon(new Date().toString());
    trainingDataList.add(trainingData);
    return trainingDataList;
  }

  private List<Path> getAllModelPaths()
  {
    List<Path> modelFiles = new ArrayList<>();
    modelFiles.add(Paths.get("c:\\windows\\syswow64\\field-1.A.B"));
    modelFiles.add(Paths.get("c:\\windows\\syswow64\\field-2.A.B"));
    return modelFiles;
  }

  private List<String> getFields(String projectId)
  {
    List<String> fields = new ArrayList<>();
    fields.add("field-1");
    return fields;
  }

  private List<String> getAllFields()
  {
    List<String> fields = new ArrayList<>();
    fields.add("field-1");
    fields.add("field-2");
    return fields;
  }
}
