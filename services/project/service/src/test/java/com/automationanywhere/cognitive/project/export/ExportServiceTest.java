package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.impl.ExportServiceImpl;
import com.automationanywhere.cognitive.project.model.TaskStatus;
import com.automationanywhere.cognitive.project.model.TaskType;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportRequest;
import com.automationanywhere.cognitive.project.task.TaskService;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ExportServiceTest {

    @Mock(name = "projectDataExportOperator")
    private ExportOperator mockProjectDataExportOperator;

    @Mock(name = "fileDataExportOperator")
    private ExportOperator mockFileDataExportOperator;

    @Mock(name = "visionBotDataExportOperator")
    private ExportOperator mockVisionBotDataExportOperator;

    @Mock(name = "classifierDataExportOperator")
    private ExportOperator mockClassifierDataExportOperator;

    @Mock(name = "mlDataExportOperator")
    private ExportOperator mockMlDataExportOperator;

    @Mock
    private ProjectDetailDao projectDetailDao;

    @Mock
    private TaskService mockTaskService;

    @Mock
    private ActivityMonitorService mockActivityMonitorService;

    @InjectMocks
    private ExportServiceImpl exportService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExportOrchestrationWhenExportTriggers() throws Exception {
        Mockito.when(projectDetailDao.projectExists(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockActivityMonitorService.isClassifierInProgress()).thenReturn(false);
        Mockito.when(mockActivityMonitorService.isStagingDocumentsInProgress()).thenReturn(false);
        Mockito.when(mockTaskService.createExportTask(Mockito.anyString(), Mockito.anyString())).thenReturn(getTask());

        Task task = exportService.startExport("./", getProjectExportRequest());

        Thread.sleep(1000L);
        InOrder order = Mockito.inOrder(projectDetailDao, mockTaskService, mockProjectDataExportOperator, mockFileDataExportOperator, mockVisionBotDataExportOperator,
                mockClassifierDataExportOperator, mockMlDataExportOperator);
        order.verify(projectDetailDao, Mockito.calls(4)).projectExists(Mockito.anyString());
        order.verify(mockTaskService).createExportTask(Mockito.anyString(),Mockito.anyString());
        order.verify(mockProjectDataExportOperator).export(Mockito.anyString(), Mockito.any(String[].class), Mockito.any(DataExportListener.class));
        order.verify(mockFileDataExportOperator).export(Mockito.anyString(), Mockito.any(String[].class), Mockito.any(DataExportListener.class));
        order.verify(mockVisionBotDataExportOperator).export(Mockito.anyString(), Mockito.any(String[].class), Mockito.any(DataExportListener.class));
        order.verify(mockClassifierDataExportOperator).export(Mockito.anyString(), Mockito.any(String[].class), Mockito.any(DataExportListener.class));
        order.verify(mockMlDataExportOperator).export(Mockito.anyString(), Mockito.any(String[].class), Mockito.any(DataExportListener.class));
    }

    @Test (expectedExceptions = {ProjectExportException.class})
    public void exportExceptionIsThrownWhenClassificationIsGoingOn()
    {
        Mockito.when(mockActivityMonitorService.isClassifierInProgress()).thenReturn(true);
        Mockito.when(mockActivityMonitorService.isStagingDocumentsInProgress()).thenReturn(false);
        Mockito.when(mockTaskService.createExportTask(Mockito.anyString(), Mockito.anyString())).thenReturn(getTask());

        Task task = exportService.startExport("./", getProjectExportRequest());
    }

    @Test (expectedExceptions = {ProjectExportException.class})
    public void exportExceptionIsThrownWhenDocumentProcessingIsGoingOn()
    {
        Mockito.when(mockActivityMonitorService.isClassifierInProgress()).thenReturn(false);
        Mockito.when(mockActivityMonitorService.isStagingDocumentsInProgress()).thenReturn(true);
        Mockito.when(mockTaskService.createExportTask(Mockito.anyString(), Mockito.anyString())).thenReturn(getTask());

        Task task = exportService.startExport("./", getProjectExportRequest());
    }

    @Test
    public void testRunningTasks()
    {
        Mockito.when(mockTaskService.getAllInProgressTasks()).thenReturn(getTaskList());
        List<Task> runningTasks = mockTaskService.getAllInProgressTasks();
        Assert.assertEquals(runningTasks.size(), getTaskList().size());
    }

    @Test
    public void exportExceptionIsThrownWhenProjectDoesNotExists() throws InterruptedException {
        Mockito.when(projectDetailDao.projectExists(Mockito.anyString())).thenReturn(false);
        Mockito.when(mockActivityMonitorService.isClassifierInProgress()).thenReturn(false);
        Mockito.when(mockActivityMonitorService.isStagingDocumentsInProgress()).thenReturn(false);
        Mockito.when(mockTaskService.createExportTask(Mockito.anyString(), Mockito.anyString())).thenReturn(getTask());

        ProjectExportException projectExportException = null;

        try {
            Task task = exportService.startExport("./", getProjectExportRequest());
        } catch (ProjectExportException e) {
            projectExportException = e;
        }

        Assert.assertEquals(projectExportException.getProjectIdsNotFound().size(), getProjectExportRequest().getProjectExportDetails().length);
    }

    @Test(expectedExceptions = {ProjectExportException.class})
    public void exportExceptionIsThrownWhenBotTrainingIsGoingOn() {
        //Given
        Mockito.when(mockActivityMonitorService.isClassifierInProgress()).thenReturn(false);
        Mockito.when(mockActivityMonitorService.isStagingDocumentsInProgress()).thenReturn(false);
        Mockito.when(mockActivityMonitorService.isBotLocked()).thenReturn(true);
        //When
        Mockito.when(mockTaskService.createExportTask(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(getTask());
        //Than
        Task task = exportService.startExport("./", getProjectExportRequest());
    }

    private ProjectExportRequest getProjectExportRequest() {
        ProjectExportRequest per = new ProjectExportRequest();
        per.setArchiveName("archiveName");
        per.setProjectExportDetails(new String[]{"1", "2", "3", "4"});
        return per;
    }

    private Task getTask()
    {
        Task task = new Task();
        task.setTaskId("1");
        task.setUserId("user");
        task.setStatus(TaskStatus.inprogress);
        task.setTaskType(TaskType.exports);
        task.setDescription("archiveName");
        return task;
    }

    private List<Task> getTaskList()
    {
        List<Task> taskList = new ArrayList<>();
        taskList.add(getTask());
        return  taskList;
    }
}
