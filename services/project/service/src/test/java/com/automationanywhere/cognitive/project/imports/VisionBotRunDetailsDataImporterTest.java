package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.VisionbotRunDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.VisionBotRunDetailsDataImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.imports.csvmodel.VisionBotRunDetails;
import com.automationanywhere.cognitive.project.model.constants.Environment;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VisionBotRunDetailsDataImporterTest {

  @Mock
  FileReader csvFileReader;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private VisionbotRunDetailsDao visionbotRunDetailsDao;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @InjectMocks
  private VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporter;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    visionBotRunDetailsDataImporter=new VisionBotRunDetailsDataImporter(csvFileReader,fileHelper,transitionResult,visionbotRunDetailsDao,importFilePatternConstructor);
  }
  @Test
  public void importDataShouldImportDataForClearTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<VisionBotRunDetails> visionBotRunDetailsList=getVisionBotRunDetails();
    List<String> visionBotIdList= new ArrayList<>();
    visionBotRunDetailsList.forEach(visionBotRunDetails -> visionBotIdList.add(visionBotRunDetails.getVisionbotid()));
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(visionBotRunDetailsList).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getImportedVisionBotIdList()).thenReturn(visionBotIdList);

    visionBotRunDetailsDataImporterSpy.importData(Paths.get("anypath"), TaskOptions.CLEAR);
    //then
    Mockito.verify(visionbotRunDetailsDao, Mockito.times(1))
        .batchInsertVisionBotRunDetails(Mockito.anyListOf(com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails.class));
  }

  @Test
  public void importDataShouldImportDataForAppendTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<VisionBotRunDetails> visionBotRunDetailsList=getVisionBotRunDetails();
    List<String> visionBotIdList= new ArrayList<>();
    visionBotRunDetailsList.forEach(visionBotRunDetails -> visionBotIdList.add(visionBotRunDetails.getVisionbotid()));
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(visionBotRunDetailsList).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getImportedVisionBotIdList()).thenReturn(visionBotIdList);

    visionBotRunDetailsDataImporterSpy.importData(Paths.get("anypath"), TaskOptions.APPEND);
    //then
    Mockito.verify(visionbotRunDetailsDao, Mockito.times(1))
        .batchInsertVisionBotRunDetails(Mockito.anyListOf(com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails.class));
  }
  @Test
  public void importDataShouldImportDataForMergeTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<VisionBotRunDetails> visionBotRunDetailsList=getVisionBotRunDetails();
    List<String> visionBotIdList= new ArrayList<>();
    visionBotRunDetailsList.forEach(visionBotRunDetails -> visionBotIdList.add(visionBotRunDetails.getVisionbotid()));
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(visionBotRunDetailsList).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getImportedVisionBotIdList()).thenReturn(visionBotIdList);

    visionBotRunDetailsDataImporterSpy.importData(Paths.get("anypath"), TaskOptions.MERGE);
    //then
    Mockito.verify(visionbotRunDetailsDao, Mockito.times(1))
        .batchInsertVisionBotRunDetails(Mockito.anyListOf(com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails.class));
  }
  @Test
  public void importDataShouldNotImportDataForMergeTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<VisionBotRunDetails> visionBotRunDetailsList=getVisionBotRunDetails();
    List<String> visionBotIdList= new ArrayList<>();
    visionBotIdList.add("vbotid124");
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(visionBotRunDetailsList).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getImportedVisionBotIdList()).thenReturn(visionBotIdList);

    visionBotRunDetailsDataImporterSpy.importData(Paths.get("anypath"), TaskOptions.MERGE);
    //then
    Mockito.verify(visionbotRunDetailsDao, Mockito.times(0))
        .batchInsertVisionBotRunDetails(Mockito.anyListOf(com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails.class));
  }
  @Test
  public void importDataShouldImportDataForOverwriteTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<VisionBotRunDetails> visionBotRunDetailsList=getVisionBotRunDetails();
    List<String> visionBotIdList= new ArrayList<>();
    visionBotRunDetailsList.forEach(visionBotRunDetails -> visionBotIdList.add(visionBotRunDetails.getVisionbotid()));
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(visionBotRunDetailsList).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getImportedVisionBotIdList()).thenReturn(visionBotIdList);
    visionBotRunDetailsDataImporterSpy.importData(Paths.get("anypath"), TaskOptions.OVERWRITE);
    //then
    Mockito.verify(visionbotRunDetailsDao, Mockito.times(1))
        .batchInsertVisionBotRunDetails(Mockito.anyListOf(com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails.class));
  }
  @Test
  public void importDataShouldNotImportIfDataIsNotAvailableForAppendTaskOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(new ArrayList()).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    visionBotRunDetailsDataImporterSpy.importData(Paths.get("."),TaskOptions.APPEND);
    //then
    Mockito.verify(visionbotRunDetailsDao, Mockito.times(0))
        .batchInsertVisionBotRunDetails(Mockito.anyListOf(com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails.class));
  }

  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionWhileFetchingDataFromCsv() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    VisionBotRunDetailsDataImporter visionBotRunDetailsDataImporterSpy = Mockito
        .spy(this.visionBotRunDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("VisionRunBotdetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doThrow(ProjectImportException.class).when(visionBotRunDetailsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    visionBotRunDetailsDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);
  }

  private List<VisionBotRunDetails> getVisionBotRunDetails() {
    List<VisionBotRunDetails> visionBotRunDetailsList = new ArrayList<>();
    VisionBotRunDetails visionBotRunDetails = new VisionBotRunDetails();
    visionBotRunDetails.setId("123");
    visionBotRunDetails.setVisionbotid("vbotid123");
    visionBotRunDetails.setCreatedat(new Date().toString());
    visionBotRunDetails.setEnvironment(Environment.staging.name());
    visionBotRunDetails.setTotalfiles("100");
    visionBotRunDetails.setRunstateid("1");
    visionBotRunDetailsList.add(visionBotRunDetails);
    return visionBotRunDetailsList;
  }
}
