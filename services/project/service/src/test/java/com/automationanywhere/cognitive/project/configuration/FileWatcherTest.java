package com.automationanywhere.cognitive.project.configuration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.project.configuration.filebasedconfiguration.FileWatcher;

public class FileWatcherTest
{
    String testFilepath = System.getProperty("user.dir") + "\\test.txt";

    FileWatcher fileWatcher;

    @Test
    public void watchFileTest() throws IOException, InterruptedException {
        final boolean[] eventNotified = new boolean[1];

        File file = new File(testFilepath);
        file.createNewFile();
        fileWatcher = new FileWatcher(testFilepath);
        fileWatcher.setFileChangedEvent(filepath -> eventNotified[0] = true);

        Thread thread = new Thread(fileWatcher);
        thread.start();
        writeToFile(file.getPath(), "test");
        fileWatcher.stopThread();
        thread.join();

        if(file.exists()) file.delete();

        Assert.assertTrue(true);
    }

    private void writeToFile(String filepath, String content) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filepath, true));
        writer.append(' ');
        writer.append(content);
        writer.close();
    }

}
