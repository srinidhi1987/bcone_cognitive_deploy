package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.export.impl.ClassifierDataExportOperator;
import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ClassifierDataExportOperatorTest {

  @Mock
  private DataExporter classifierDataExporter;
  @Mock
  private DataExportListener dataExportListener;
  @Mock
  private FileDetailsDao fileDetailsDao;
  @InjectMocks
  private ClassifierDataExportOperator classifierDataExportOperator;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    classifierDataExportOperator = new ClassifierDataExportOperator(classifierDataExporter,fileDetailsDao);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testExportClassifierData() throws Exception {
    //given
    String exportDir = "Output//";
    String[] projectIds = {"Projid1", "Projid2"};

    //when

    Mockito.when(fileDetailsDao.getFileDetails(Arrays.asList(projectIds))).thenReturn(fileDetailsList());

    classifierDataExportOperator.export(exportDir, projectIds, dataExportListener);

    InOrder order = Mockito.inOrder(classifierDataExporter, dataExportListener);

    Thread.sleep(100L);
    //then
    order.verify(classifierDataExporter, Mockito.times(1))
        .exportData(Mockito.anyString(), Mockito.anyMap());
    order.verify(dataExportListener).onClassifierDataExportCompleted(Mockito.anyList());
  }

  private List<FileDetails> fileDetailsList(){
    List<FileDetails> lstOfFileDetails = new ArrayList<>();
    FileDetails fileDetails = new FileDetails();
    fileDetails.setClassificationId("123");
    fileDetails.setLayoutId("1");
    lstOfFileDetails.add(fileDetails);
    return  lstOfFileDetails;
  }
}