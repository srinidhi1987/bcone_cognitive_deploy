package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.export.impl.FileDataExportOperator;
import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileDataExportOperatorTest {

    @Mock
    private FileDetailsDao fileDetailsDao;

    @Mock
    private DataExporter dataExporter;

    @Mock
    private DataExportListener dataExportListener;

    @InjectMocks
    private FileDataExportOperator fileDataExportOperator;

    @BeforeMethod
    public void setUp() {
        fileDataExportOperator = new FileDataExportOperator(dataExporter);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testExportFileData() throws Exception {
        String[] projectIds = {"prj1", "prj2", "prj3"};
        int inStaging=0;
        Mockito.when(fileDetailsDao.getFileIdsMapGroupedByProjectId(projectIds,inStaging)).thenReturn(mapOfProjectIdToFileIds(projectIds));
        fileDataExportOperator.export("path/to/output", projectIds, dataExportListener);
        Thread.sleep(1000L);
        InOrder order = Mockito.inOrder(fileDetailsDao, dataExporter, dataExportListener);
        order.verify(fileDetailsDao).getFileIdsMapGroupedByProjectId(projectIds,inStaging);
        order.verify(dataExporter, Mockito.times(3)).exportData(Mockito.anyString(), Mockito.any(Map.class));
        order.verify(dataExportListener).onFileDataExportCompleted(Mockito.any(List.class));
    }

    private Map<String, List<String>> mapOfProjectIdToFileIds(String[] projectIds) {

        return new HashMap<String, List<String>>() {
        	
        private static final long serialVersionUID = 5205231028577436689L;
        	{
            put("prj1", Arrays.asList("file1", "file2", "file3", "file4", "file5", "file6", "file7", "file8", "file9", "file10", "file11", "file12", "file13", "file14"));
            put("prj2", Arrays.asList("file1", "file2", "file3", "file4", "file5", "file6", "file7", "file8", "file9"));
            put("prj3", Arrays.asList("file1", "file2", "file3", "file4", "file5", "file6", "file7", "file8", "file9", "file10", "file11", "file12", "file13", "file14", "file15", "file16", "file17", "file18", "file19", "file20", "file21"));
        }};
    }
}