package com.automationanywhere.cognitive.project.impl;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;

import com.automationanywhere.cognitive.project.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.project.exception.NotValidRequestException;
import com.automationanywhere.cognitive.project.exception.ResourceUpdationException;
import com.automationanywhere.cognitive.project.export.ActivityMonitorService;
import com.automationanywhere.cognitive.project.messagequeue.QueuingService;
import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.PatchOperation;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dto.Fields;
import com.automationanywhere.cognitive.project.model.dto.PatchRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import javax.persistence.OptimisticLockException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ProjectServiceDelegateTest {

  @Mock
  @Qualifier("botUpdationQueuingService")
  private QueuingService botUpdationQueuingService;

  @Mock
  @Qualifier("queuingServiceProxy")
  private QueuingService documentProcessingQueuingService;

  @Mock
  private ActivityMonitorService activityMonitorService;

  @Mock
  private ProjectUpdation projectUpdation;

  @Mock
  private VisionbotAdapter visionbotAdapter;

  @InjectMocks
  private ProjectServiceDelegate projectServiceDelegate;


  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  //@Test
  public void updateShouldUpdateProjectDetails() throws Exception {
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest = getProjectRequest(projectId);

    ProjectDetail projectDetail = getProjectDetail(projectId);

    //when
    projectServiceDelegate.update(projectDetail, projectRequest);

    //then
    Mockito.verify(projectUpdation, times(1)).update(Mockito.any());

  }

  @Test(expectedExceptions = ResourceUpdationException.class)
  public void updateShouldThrowResourceUpdationException() throws Exception {
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest = getProjectRequest(projectId);

    ProjectDetail projectDetail = getProjectDetail(projectId);

    //when
    doThrow(OptimisticLockException.class).when(projectUpdation).update(projectDetail);

    projectServiceDelegate.update(projectDetail,projectRequest);
  }

  @Test
  public void applyPatchOperationsShouldGetModifiedProjectRequest() throws Exception {
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest expectedProjectRequest = getProjectRequest(projectId);
    expectedProjectRequest.setDescription("Text");
    PatchRequest patchRequest = getPatchRequest(PatchOperation.replace, "/description","Text",0 );

    //when
    ProjectRequest actualProjectRequest = projectServiceDelegate.applyPatchOperations(expectedProjectRequest,patchRequest);

    //then
    Assert.assertEquals(expectedProjectRequest.getDescription(), actualProjectRequest.getDescription());

  }

  @Test
  public void validateRequestShouldValidateRequest() throws Exception {
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest = getProjectRequest(projectId);
    projectRequest.setDescription("Text");
    PatchRequest patchRequest = getPatchRequest(PatchOperation.replace, "/description","Text",0 );

    //when
     projectServiceDelegate.validateRequest(projectRequest,patchRequest);
  }

  @Test(expectedExceptions = NotValidRequestException.class)
  public void validateRequestShouldThrowNotValidRequestException() throws NotValidRequestException {
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectRequest projectRequest = getProjectRequest(projectId);
    projectRequest.setDescription("Text");
    PatchRequest patchRequest = getPatchRequest(PatchOperation.replace, "/wrongAttribute","Text" ,0);

    //when
    projectServiceDelegate.validateRequest(projectRequest,patchRequest);
  }

  @Test(expectedExceptions = ResourceUpdationException.class)
  public void checkIfProjectIsUpdated_WithSinglePatchRequest_ShouldThrowResourceUpdationException() throws ResourceUpdationException {
    //given
    final String projectId = UUID.randomUUID().toString();
    ProjectDetail projectDetail = getProjectDetail(projectId);
    projectDetail.setEnvironment(Environment.staging);
    projectDetail.setVersionId(1);
    PatchRequest patchRequest = getPatchRequest(PatchOperation.replace, "/environment","staging" ,0);

    //when
    projectServiceDelegate.checkIfProjectEnvironmentIsUpdated(projectDetail,patchRequest);
  }

  private ProjectRequest getProjectRequest(String id) {
    ProjectRequest projectRequest = new ProjectRequest();
    projectRequest.setName("Test Project");
    projectRequest.setId(id);
    projectRequest.setEnvironment(Environment.staging);
    projectRequest.setOrganizationId("1");

    Fields fields = new Fields();
    fields.setCustom(new ArrayList<>());
    fields.setStandard(new String[]{"field1"});

    projectRequest.setFields(fields);
    return projectRequest;
  }

  private ProjectDetail getProjectDetail(String id) {
    ProjectDetail projectDetail = new ProjectDetail();
    projectDetail.setId(id);
    projectDetail.setName("Test Project");
    projectDetail.setEnvironment(Environment.staging);
    projectDetail.setCustomFieldDetailSet(new HashSet<CustomFieldDetail>());
    projectDetail.setStandardFieldDetailSet(new HashSet<StandardFieldDetail>());
    return projectDetail;
  }

  private PatchRequest getPatchRequest(PatchOperation patchOperation , String path , String value,long versionid){
    PatchRequest patchRequest = new PatchRequest();
    patchRequest.setOp(patchOperation);
    patchRequest.setPath(path);
    patchRequest.setValue(value);
    patchRequest.setVersionId(versionid);
    return patchRequest;
  }
}
