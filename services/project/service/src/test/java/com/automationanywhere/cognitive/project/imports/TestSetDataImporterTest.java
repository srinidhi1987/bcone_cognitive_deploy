package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.TestSetDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.TestSetDataImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.TestSet;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestSetDataImporterTest {

  @InjectMocks
  TestSetDataImporter importer;
  @Mock
  ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  TestSetDao testSetDao;
  @Mock
  AdvanceDataImporter nextDataImporter;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private FileReader fileReader;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    importer = new TestSetDataImporter
        (fileReader, testSetDao, transitionResult, fileHelper,
            importFilePatternConstructor);
  }
  @SuppressWarnings("unchecked")
  @Test
  public void importDataShouldImportTestSetDataForClearTaskOption()
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    TestSetDataImporter spy = Mockito.spy(importer);
    List<TestSet> csvData = new ArrayList<>();
    csvData.add(getTestSetModel());
    List<String> lstOfTestSetInfoIds=csvData.stream().map(it->it.getTestsetInfoId()).collect(Collectors.toList());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    Mockito.when(transitionResult.getImportedTestSetInfoIdList()).thenReturn(lstOfTestSetInfoIds);
    spy.importData(directoryPath, TaskOptions.CLEAR);
    //then
    Mockito.verify(testSetDao, times(1)).batchInsert(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataShouldImportTestSetDataForAppendTaskOption()
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    TestSetDataImporter spy = Mockito.spy(importer);
    List<TestSet> csvData = new ArrayList<>();
    csvData.add(getTestSetModel());
    List<String> lstOfTestSetInfoIds=csvData.stream().map(it->it.getTestsetInfoId()).collect(Collectors.toList());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    Mockito.when(transitionResult.getImportedTestSetInfoIdList()).thenReturn(lstOfTestSetInfoIds);
    spy.importData(directoryPath, TaskOptions.APPEND);
    //then
    Mockito.verify(testSetDao, times(1)).batchInsert(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataShouldImportTestSetDataForMergeTaskOption()
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    TestSetDataImporter spy = Mockito.spy(importer);
    List<TestSet> csvData = new ArrayList<>();
    csvData.add(getTestSetModel());
    List<String> lstOfTestSetInfoIds=csvData.stream().map(it->it.getTestsetInfoId()).collect(Collectors.toList());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    Mockito.when(transitionResult.getImportedTestSetInfoIdList()).thenReturn(lstOfTestSetInfoIds);
    spy.importData(directoryPath, TaskOptions.MERGE);
    //then
    Mockito.verify(testSetDao, times(1)).batchInsert(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataShouldImportTestSetDataForOverwriteTaskOption()
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    TestSetDataImporter spy = Mockito.spy(importer);
    List<TestSet> csvData = new ArrayList<>();
    csvData.add(getTestSetModel());
    List<String> lstOfTestSetInfoIds=csvData.stream().map(it->it.getTestsetInfoId()).collect(Collectors.toList());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    Mockito.when(transitionResult.getImportedTestSetInfoIdList()).thenReturn(lstOfTestSetInfoIds);
    spy.importData(directoryPath, TaskOptions.OVERWRITE);
    //then
    Mockito.verify(testSetDao, times(1)).batchInsert(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importDataShouldThrowIoExceptionForAppendTaskOption()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    //when
    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);
    //then
    importer.importData(directoryPath,TaskOptions.APPEND);
  }

  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionForAnyTaskOption()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));
    TestSetDataImporter testSetSpy = Mockito.spy(importer);
    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doThrow(ProjectImportException.class).when(testSetSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    testSetSpy.importData(directoryPath, TaskOptions.APPEND);
  }

  private TestSet getTestSetModel() {
    TestSet model = new TestSet();
    model.setId("1");
    model.setName("TestSet");
    model.setDocitems("TestSetDocItem");
    model.setTestsetInfoId("123");
    return model;
  }

}
