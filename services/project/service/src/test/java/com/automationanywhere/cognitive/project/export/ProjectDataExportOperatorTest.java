package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.export.impl.ProjectDataExportOperator;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class ProjectDataExportOperatorTest {

    @Mock
    private DataExporter dataExporter;

    @Mock
    private DataExportListener dataExportListener;

    private ProjectDataExportOperator projectDataExportOperator;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        projectDataExportOperator = new ProjectDataExportOperator(dataExporter);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testExportProjectData() throws Exception {
        projectDataExportOperator.export("path/to/output/dir", new String[]{"1"}, dataExportListener);
        Thread.sleep(1000L);
        InOrder order = Mockito.inOrder(dataExporter, dataExportListener);
        order.verify(dataExporter, Mockito.times(1)).exportData(Mockito.anyString(), Mockito.any(Map.class));
        order.verify(dataExportListener).onProjectDataExportCompleted(Mockito.any(List.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMultipleExportProjectData() throws Exception {
        projectDataExportOperator
            .export("path/to/output/dir", new String[]{"1", "2","3", "4", "5", "6", "7", "8", "9", "10"}, dataExportListener);
        Thread.sleep(1000L);
        InOrder order = Mockito.inOrder(dataExporter, dataExportListener);
        order.verify(dataExporter, Mockito.times(10)).exportData(Mockito.anyString(), Mockito.any(Map.class));
        order.verify(dataExportListener).onProjectDataExportCompleted(Mockito.any(List.class));
    }
}
