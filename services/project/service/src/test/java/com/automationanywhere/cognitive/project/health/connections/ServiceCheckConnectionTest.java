package com.automationanywhere.cognitive.project.health.connections;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.project.adapter.FileManagerAdapter;
import com.automationanywhere.cognitive.project.adapter.Impl.VisionbotAdapterImpl;
import com.automationanywhere.cognitive.project.exception.DependentServiceConnectionFailureException;

public class ServiceCheckConnectionTest {
    @Mock
    VisionbotAdapterImpl visionBotAdapter;
    @Mock
    FileManagerAdapter fileManagerAdapter;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOfCheckConnectionForServiceWhenAllServiceConnectionSuccess() {

        // given
        List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity fileConn = new ServiceConnectivity("FileManager", HTTPStatusCode.OK);
        ServiceConnectivity visionBotConn = new ServiceConnectivity("VisionBot", HTTPStatusCode.OK);
        servicesListExpected.add(fileConn);
        servicesListExpected.add(visionBotConn);
        ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(fileManagerAdapter, visionBotAdapter);
        Mockito.doNothing().when(visionBotAdapter).testConnection();
        Mockito.doNothing().when(fileManagerAdapter).testConnection();

        // when
        List<ServiceConnectivity> servicesList = serviceCheckConnection.checkConnectionForService();

        // then
        int index = 0;
        for(ServiceConnectivity serviceConnectivity:servicesList){
            ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
            assertThat(serviceConnectivity.getHTTPStatus()).isEqualTo(expectedServiceConnectivity.getHTTPStatus());
            assertThat(serviceConnectivity.getServiceName()).isEqualTo(expectedServiceConnectivity.getServiceName());
            index++;
        }

    }

    @Test
    public void testOfCheckConnectionForServiceWhenAllServiceConnectionFails() {

        // given
        List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity fileConn = new ServiceConnectivity("FileManager", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        ServiceConnectivity visionBotConn = new ServiceConnectivity("VisionBot", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        servicesListExpected.add(fileConn);
        servicesListExpected.add(visionBotConn);
        ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(fileManagerAdapter, visionBotAdapter);
        
        Mockito.doThrow(DependentServiceConnectionFailureException.class)
                .when(visionBotAdapter).testConnection();
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(fileManagerAdapter)
                .testConnection();

        // when
        List<ServiceConnectivity> servicesList = serviceCheckConnection.checkConnectionForService();

        // then
        int index = 0;
        
        for(ServiceConnectivity serviceConnectivity:servicesList){
            ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
            assertThat(serviceConnectivity.getHTTPStatus()).isEqualTo(expectedServiceConnectivity.getHTTPStatus());
            assertThat(serviceConnectivity.getServiceName()).isEqualTo(expectedServiceConnectivity.getServiceName());
            index++;
        }

    }
}
