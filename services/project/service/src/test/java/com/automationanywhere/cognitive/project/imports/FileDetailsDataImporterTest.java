package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.FileDetailsDataImporter;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.csvmodel.FileDetail;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FileDetailsDataImporterTest {

  @Mock
  private FileReader csvFileReader;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileDetailsDao fileDetailsDao;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @InjectMocks
  private FileDetailsDataImporter fileDetailsDataImporter;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    fileDetailsDataImporter = new FileDetailsDataImporter(csvFileReader,fileHelper,transitionResult,fileDetailsDao,importFilePatternConstructor);
  }

  @Test
  public void importDataShouldImportDataWithTaskOptionClear() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    Map<Integer, Integer> modifiedClasses = new HashMap<Integer, Integer>();
    modifiedClasses.put(91, 92);
    Map<String, String> modifiedLayouts = new HashMap<String, String>();
    modifiedLayouts.put("5", "10");

    //when
    FileDetailsDataImporter fileDetailDataImporterSpy = Mockito.spy(fileDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail("91","")).when(fileDetailDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getModifiedClasses()).thenReturn(modifiedClasses);
    Mockito.when(transitionResult.getModifiedLayouts()).thenReturn(modifiedLayouts);

    fileDetailDataImporterSpy.importData(Paths.get("."), TaskOptions.CLEAR);

    //then
    Mockito.verify(fileDetailsDao, Mockito.times(1))
        .batchInsertFileDetails(Mockito.anyListOf(FileDetails.class));

  }

  @Test
  public void importDataShouldImportDataWithTaskOptionAppend() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    Map<Integer, Integer> modifiedClasses = new HashMap<Integer, Integer>();
    modifiedClasses.put(91, 92);
    Map<String, String> modifiedLayouts = new HashMap<String, String>();
    modifiedLayouts.put("5", "10");

    //when
    FileDetailsDataImporter fileDetailDataImporterSpy = Mockito.spy(fileDetailsDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail("91","")).when(fileDetailDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getModifiedClasses()).thenReturn(modifiedClasses);
    Mockito.when(transitionResult.getModifiedLayouts()).thenReturn(modifiedLayouts);

    fileDetailDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(fileDetailsDao, Mockito.times(1))
        .batchInsertFileDetails(Mockito.anyListOf(FileDetails.class));

  }

  @Test
  public void importDataShouldImportDataWithTaskOptionOverwrite() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    Map<Integer, Integer> modifiedClasses = new HashMap<Integer, Integer>();
    modifiedClasses.put(91, 92);
    Map<String, String> modifiedLayouts = new HashMap<String, String>();
    modifiedLayouts.put("5", "10");

    //when
    FileDetailsDataImporter fileDetailDataImporterSpy = Mockito.spy(fileDetailsDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail("91","")).when(fileDetailDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getModifiedClasses()).thenReturn(modifiedClasses);
    Mockito.when(transitionResult.getModifiedLayouts()).thenReturn(modifiedLayouts);

    fileDetailDataImporterSpy.importData(Paths.get("."), TaskOptions.OVERWRITE);

    //then
    Mockito.verify(fileDetailsDao, Mockito.times(1))
        .batchAddOrUpdateFileDetails(Mockito.anyListOf(FileDetails.class));

  }

  @Test
  public void importDataShouldNotImportIfDataIsNotAvailableForAppendOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    Map<Integer, Integer> modifiedClasses = new HashMap<Integer, Integer>();
    modifiedClasses.put(91, 92);
    Map<String, String> modifiedLayouts = new HashMap<String, String>();
    modifiedLayouts.put("5", "10");

    //when
    FileDetailsDataImporter fileDetailDataImporterSpy = Mockito.spy(fileDetailsDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(new ArrayList()).when(fileDetailDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getModifiedClasses()).thenReturn(modifiedClasses);
    Mockito.when(transitionResult.getModifiedLayouts()).thenReturn(modifiedLayouts);

    fileDetailDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(fileDetailsDao, Mockito.times(0))
        .batchInsertFileDetails(Mockito.anyListOf(FileDetails.class));

  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataShouldImportDataWithTaskOptionMerge() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    Map<Integer, Integer> modifiedClasses = new HashMap<Integer, Integer>();
    modifiedClasses.put(91, 92);
    Map<String, String> modifiedLayouts = new HashMap<String, String>();
    modifiedLayouts.put("5", "10");

    List<FileDetails> fileDetailsList = getFileDetails("fileid1","fileid2");

    List<String> fileids=new ArrayList<>();
    fileids.add("fileid1");

    //when
    FileDetailsDataImporter fileDetailDataImporterSpy = Mockito.spy(fileDetailsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileDetails_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doReturn(getFileDetail("91","fileid1","fileid2")).when(fileDetailDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getModifiedClasses()).thenReturn(modifiedClasses);
    Mockito.when(transitionResult.getModifiedLayouts()).thenReturn(modifiedLayouts);

    Mockito.when(fileDetailsDao.getFileDetails(Mockito.anyString(),Mockito.anyList())).thenReturn(getFileDetails("fileid1"));

    fileDetailDataImporterSpy.importData(Paths.get("."), TaskOptions.MERGE);

    //then
    Mockito.verify(fileDetailsDao, Mockito.times(1))
        .batchInsertFileDetails(Mockito.anyListOf(FileDetails.class));

  }

  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionWhileFetchingDataFromCsv() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    Map<Integer, Integer> modifiedClasses = new HashMap<Integer, Integer>();
    modifiedClasses.put(91, 92);
    Map<String, String> modifiedLayouts = new HashMap<String, String>();
    modifiedLayouts.put("5", "10");


    //when
    FileDetailsDataImporter fileDetailDataImporterSpy = Mockito.spy(fileDetailsDataImporter);

    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("abc");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doThrow(ProjectImportException.class).when(fileDetailDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getModifiedClasses()).thenReturn(modifiedClasses);
    Mockito.when(transitionResult.getModifiedLayouts()).thenReturn(modifiedLayouts);

    fileDetailDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);

  }

  private List<FileDetail> getFileDetail(String classificationId,String... fileIds) {
    List<FileDetail> fileDetails = new ArrayList<>();
    for(String fileId:fileIds) {
      FileDetail fileDetail = new FileDetail();
      fileDetail.setClassificationId(classificationId);
      fileDetail.setFileId(fileId);
      fileDetails.add(fileDetail);
    }
    return fileDetails;
  }

  private List<FileDetails> getFileDetails(String... fileIds) {
    List<FileDetails> fileDetails = new ArrayList<>();
    for(String fileId:fileIds){
      FileDetails fileDetail = new FileDetails();
      fileDetail.setFileId(fileId);
      fileDetails.add(fileDetail);
    }
    return fileDetails;
  }
}
