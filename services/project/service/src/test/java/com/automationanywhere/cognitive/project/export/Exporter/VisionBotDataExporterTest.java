package com.automationanywhere.cognitive.project.export.Exporter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.DocumentProcessedDetailDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.dal.TestSetDao;
import com.automationanywhere.cognitive.project.dal.TestSetInfoDao;
import com.automationanywhere.cognitive.project.dal.VisionBotDao;
import com.automationanywhere.cognitive.project.dal.VisionbotDetailDao;
import com.automationanywhere.cognitive.project.dal.VisionbotRunDetailsDao;
import com.automationanywhere.cognitive.project.export.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VisionBotDataExporterTest {

  @InjectMocks
  private VisionBotDataExporter visionBotDataExporter;
  @Mock
  private VisionbotDetailDao visionbotDetailDao;
  @Mock
  private VisionBotDao visionBotDao;
  @Mock
  private TestSetInfoDao testSetInfoDao;
  @Mock
  private TestSetDao testSetDao;
  @Mock
  private VisionbotRunDetailsDao visionbotRunDetailsDao;
  @Mock
  private DocumentProcessedDetailDao documentProcessedDetailDao;
  @Mock
  private ProjectDetailDao projectDetailDao;
  @Mock
  private FileWriter csvFileWriter;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void exportVisionbotDetailDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> visionbotDetailData = new ArrayList<String[]>();
    visionbotDetailData
        .add(new String[]{"50191bb1-08bf-4d6b-b7b7-629a636856b2", "1",
            "a84d55de-2c99-4613-9813-a779caf1b964", "1", "production"});

    when(visionbotDetailDao.getVisionbotDetails(Mockito.anyString()))
        .thenReturn(visionbotDetailData);

    visionBotDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(visionbotDetailDao, csvFileWriter);
    order.verify(visionbotDetailDao).getVisionbotDetails(Mockito.anyString());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportVisionbotDetailDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> visionbotDetailData = null;

    when(visionbotDetailDao.getVisionbotDetails(Mockito.anyString()))
        .thenReturn(visionbotDetailData);

    visionBotDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportVisionbotDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> visionbotData = new ArrayList<String[]>();
    visionbotData
        .add(new String[]{"data1", "data2",
            "data3", "data4",});

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(visionBotDao.getVisionBots(visionBotDetailIds))
        .thenReturn(visionbotData);

    visionBotDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(visionbotDetailDao, visionBotDao, csvFileWriter);

    order.verify(visionbotDetailDao).getVisionbotDetailIds(Mockito.anyString());
    order.verify(visionBotDao).getVisionBots(visionBotDetailIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportVisionbotDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> visionbotData = null;

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(visionBotDao.getVisionBots(visionBotDetailIds))
        .thenReturn(visionbotData);

    visionBotDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportVisionbotRunDetailsDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> visionbotRunDetailsData = new ArrayList<String[]>();
    visionbotRunDetailsData
        .add(new String[]{"e2ed5385-c108-4833-8a09-e72d47108448",
            "50191bb1-08bf-4d6b-b7b7-629a636856b2", "52:31.0", "staging", "3", "1"});

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(visionbotRunDetailsDao.getVisionbotRunDetails(visionBotDetailIds))
        .thenReturn(visionbotRunDetailsData);

    visionBotDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(visionbotDetailDao, visionbotRunDetailsDao, csvFileWriter);

    order.verify(visionbotDetailDao).getVisionbotDetailIds(Mockito.anyString());
    order.verify(visionbotRunDetailsDao).getVisionbotRunDetails(visionBotDetailIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportVisionbotRunDetailsDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> visionbotRunDetailsData = null;

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(visionbotRunDetailsDao.getVisionbotRunDetails(visionBotDetailIds))
        .thenReturn(visionbotRunDetailsData);

    visionBotDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportTestSetInfoDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> testSetInfoData = new ArrayList<String[]>();
    testSetInfoData
        .add(new String[]{"d100b115-ffdd-4de3-b20f-1531c8890a52",
            "50191bb1-08bf-4d6b-b7b7-629a636856b2", "Layout1"});

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(testSetInfoDao.getTestSetInfos(visionBotDetailIds)).thenReturn(testSetInfoData);

    visionBotDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(visionbotDetailDao, testSetInfoDao, csvFileWriter);

    order.verify(visionbotDetailDao).getVisionbotDetailIds(Mockito.anyString());
    order.verify(testSetInfoDao).getTestSetInfos(visionBotDetailIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }
  @SuppressWarnings("unchecked")
  @Test
  public void exportTestSetInfoDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String[]> testSetInfoData = null;

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(testSetInfoDao.getTestSetInfoIds(Mockito.anyList())).thenReturn(testSetInfoData);

    visionBotDataExporter.exportData("./", params);

    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportDocumentProcessedDetailDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    List<String> visionbotRunDetailsIds = new ArrayList<>();
    visionbotRunDetailsIds.add("e2ed5385-c108-4833-8a09-e72d47108448");

    List<String[]> documentProcessedDetailData = new ArrayList<String[]>();
    documentProcessedDetailData
        .add(new String[]{"afa1e7cd-48c3-4990-a74b-9c22cd0d0d65","29b0f8ed-30e4-4f8e-b2d1-7a6530e72f25","e2ed5385-c108-4833-8a09-e72d47108448","Fail","6","3","3"
        });

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(visionbotRunDetailsDao.getVisionbotRunDetailsIds(visionBotDetailIds))
        .thenReturn(visionbotRunDetailsIds);

    when(documentProcessedDetailDao.getDocumentProcessedDetails(visionbotRunDetailsIds)).thenReturn(documentProcessedDetailData);

    visionBotDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(visionbotDetailDao, visionbotRunDetailsDao,documentProcessedDetailDao,
        csvFileWriter);

    order.verify(visionbotDetailDao).getVisionbotDetailIds(Mockito.anyString());
    order.verify(visionbotRunDetailsDao).getVisionbotRunDetailsIds(visionBotDetailIds);
    order.verify(documentProcessedDetailDao).getDocumentProcessedDetails(visionbotRunDetailsIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportDocumentProcessedDetailDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    List<String> visionbotRunDetailsIds = new ArrayList<>();
    visionbotRunDetailsIds.add("e2ed5385-c108-4833-8a09-e72d47108448");

    List<String[]> documentProcessedDetailData = null;

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(visionbotRunDetailsDao.getVisionbotRunDetailsIds(visionBotDetailIds))
        .thenReturn(visionbotRunDetailsIds);

    when(documentProcessedDetailDao.getDocumentProcessedDetails(visionbotRunDetailsIds)).thenReturn(documentProcessedDetailData);

    visionBotDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }
  @Test
  public void exportTestSetDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    List<String> testSetInfoIds = new ArrayList<>();
    testSetInfoIds.add("d100b115-ffdd-4de3-b20f-1531c8890a52\n");

    List<String[]> testSetData = new ArrayList<String[]>();
    testSetData
        .add(new String[]{"data1","data2","data3","data4"
        });

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(testSetInfoDao.getTestSetInfoIds(visionBotDetailIds))
        .thenReturn(testSetInfoIds);

    when(testSetDao.getTestSets(testSetInfoIds)).thenReturn(testSetData);

    visionBotDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(visionbotDetailDao, testSetInfoDao,testSetDao, csvFileWriter);

    order.verify(visionbotDetailDao).getVisionbotDetailIds(Mockito.anyString());
    order.verify(testSetInfoDao).getTestSetInfoIds(visionBotDetailIds);
    order.verify(testSetDao).getTestSets(testSetInfoIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportTestSetDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");
    params.put("isProduction", 0);

    List<String> visionBotDetailIds = new ArrayList<>();
    visionBotDetailIds.add("50191bb1-08bf-4d6b-b7b7-629a636856b2");

    List<String> testSetInfoIds = new ArrayList<>();
    testSetInfoIds.add("d100b115-ffdd-4de3-b20f-1531c8890a52\n");

    List<String[]> testSetData = null;

    when(visionbotDetailDao.getVisionbotDetailIds(Mockito.anyString()))
        .thenReturn(visionBotDetailIds);

    when(testSetInfoDao.getTestSetInfoIds(visionBotDetailIds))
        .thenReturn(testSetInfoIds);

    when(testSetDao.getTestSets(testSetInfoIds)).thenReturn(testSetData);

    visionBotDataExporter.exportData("./", params);
    visionBotDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }
}
