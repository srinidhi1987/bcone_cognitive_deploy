package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.impl.ProjectDetailDaoImpl;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectAlreadyExistException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.ProjectDetailDataImporterImpl;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailsModel;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ProjectDetailDataImporterImplTest {

  private static String PROJECT_ID = "4b08ac1d-15d0-4a24-93ab-4fc6b80f57df";
  private static String PROJECT_NAME = "ABC";
  @InjectMocks
  ProjectDetailDataImporterImpl projectDataImporter;
  @Mock
  ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  ProjectDetailDaoImpl projectDetailDao;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private FileReader fileReader;
  @Captor
  private ArgumentCaptor<ProjectDetailsModel> argumentCaptor;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    projectDataImporter = new ProjectDetailDataImporterImpl
        (fileReader, projectDetailDao, transitionResult, fileHelper, importFilePatternConstructor);
  }


  //Append Mode
  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = ProjectAlreadyExistException.class)
  public void importDataInAppendModeShouldThrowExceptionWhenProjectAlreadyExist()
      throws ProjectImportException, DuplicateProjectNameException, ProjectAlreadyExistException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.projectExists(PROJECT_ID)).thenReturn(true);

    //when
    spy.importData(directoryPath, TaskOptions.APPEND);
    //then
    Mockito.verify(projectDetailDao, times(1)).projectExists(Mockito.any());

  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = DuplicateProjectNameException.class)
  public void importDataInAppendModeShouldThrowExceptionWhenProjectNameAlreadyExist()
      throws ProjectImportException, DuplicateProjectNameException, ProjectAlreadyExistException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenThrow(NoResultException.class);
    when(projectDetailDao.projectNameExists(PROJECT_NAME)).thenReturn(true);

    //when
    spy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(projectDetailDao, times(1)).getProjectDetailModel(Mockito.any());
    Mockito.verify(projectDetailDao, times(1)).projectNameExists(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataInAppendModeShouldAppendProjectDetailData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenThrow(NoResultException.class);
    when(projectDetailDao.projectNameExists(PROJECT_NAME)).thenReturn(false);

    //when
    spy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(projectDetailDao).add(argumentCaptor.capture());
    Mockito.verify(projectDetailDao, times(1)).add(Mockito.any());
    Assert.assertNotNull(argumentCaptor.getValue());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void setNextShouldInvokeNextImporterWhenNextImporterAvailable()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    AdvanceDataImporter nextImporter = Mockito.mock(AdvanceDataImporter.class);
    ProjectDetailDataImporterImpl projectDetailSpy = Mockito.spy(projectDataImporter);
    projectDetailSpy.setNext(nextImporter);

    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) projectDetailSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenThrow(NoResultException.class);
    when(projectDetailDao.projectNameExists(PROJECT_NAME)).thenReturn(false);

    //when
    projectDetailSpy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(nextImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  //Clear Mode
  @SuppressWarnings("unchecked")
  @Test
  public void importDataInClearModeShouldDeleteAndInsertProjectDetailData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.CLEAR);

    //then

    Mockito.verify(projectDetailDao, times(1)).add(Mockito.any());
    Mockito.verify(projectDetailDao).add(argumentCaptor.capture());
    Assert.assertNotNull(argumentCaptor.getValue());

  }


  //Merge Mode
  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = DuplicateProjectNameException.class)
  public void importDataInMergeModeShouldThrowExceptionWhenProjectNameAlreadyExist()
      throws ProjectImportException, DuplicateProjectNameException, ProjectAlreadyExistException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenThrow(NoResultException.class);
    when(projectDetailDao.projectNameExists(PROJECT_NAME)).thenReturn(true);

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(projectDetailDao, times(1)).projectExists(Mockito.any());
    Mockito.verify(projectDetailDao, times(1)).projectNameExists(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataInMergeModeShouldNotInsertDataWhenProjectAlreadyExist()
      throws ProjectImportException, DuplicateProjectNameException, ProjectAlreadyExistException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenReturn((ProjectDetailsModel) Mockito.any());
    when(projectDetailDao.projectNameExists(PROJECT_NAME))
        .thenReturn(false);

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);
    //then
    Mockito.verify(projectDetailDao, times(2)).getProjectDetailModel(Mockito.any());
    Mockito.verify(projectDetailDao, times(0)).projectNameExists(Mockito.any());
    Mockito.verify(projectDetailDao, times(0)).add(Mockito.any());

  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataInMergeModeShouldAppendProjectDetailDataWhenProjectDoesNotExist()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<Path>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenThrow(NoResultException.class);
    when(projectDetailDao.projectNameExists(PROJECT_NAME)).thenReturn(false);

    //when
    spy.importData(directoryPath, TaskOptions.MERGE);

    //then
    Mockito.verify(projectDetailDao, times(1)).add(Mockito.any());
  }

  //Overwrite Mode
  @SuppressWarnings("unchecked")
  @Test
  public void importDataInOverwriteModeShouldUpdateDataWhenProjectAlreadyExist()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectDetailDataImporterImpl spy = Mockito.spy(projectDataImporter);
    List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> csvData = new ArrayList<>();
    csvData.add(getProjectDetailCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(projectDetailDao.getProjectDetailModel(PROJECT_ID)).thenReturn((ProjectDetailsModel) Mockito.any());
    when(projectDetailDao.projectNameExists(PROJECT_NAME)).thenReturn(false);

    //when
    spy.importData(directoryPath, TaskOptions.OVERWRITE);

    //then
    Mockito.verify(projectDetailDao, times(1))
        .updateProjectDetail((ProjectDetailsModel) Mockito.any());
  }

  private ProjectDetail getProjectDetailCsvModel() {
    ProjectDetail model = new ProjectDetail();
    model.setId(PROJECT_ID);
    model.setName(PROJECT_NAME);
    model.setCreatedAt(new Date().toString());
    model.setUpdatedAt(new Date().toString());
    return model;
  }
}