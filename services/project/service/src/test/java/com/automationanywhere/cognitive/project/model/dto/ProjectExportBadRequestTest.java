package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

public class ProjectExportBadRequestTest {

    @Test
    public void testWithNotFoundException() {
        ProjectExportException projectExportException = aNotFoundProjectExportException();
        ProjectExportBadRequest projectExportBadRequest = ProjectExportBadRequest.withException(projectExportException);
        Assert.assertNotNull(projectExportBadRequest);
        Assert.assertNotNull(projectExportBadRequest.getProjectExportDeclinations());
        Assert.assertEquals(projectExportBadRequest.getProjectExportDeclinations().size(), 3);
    }

//    @Test
//    public void testWithNotExportableException() {
//        ProjectExportException projectExportException = aNotExportableProjectExportException();
//        ProjectExportBadRequest projectExportBadRequest = ProjectExportBadRequest.withException(projectExportException);
//        Assert.assertNotNull(projectExportBadRequest);
//        Assert.assertNotNull(projectExportBadRequest.getProjectExportDeclinations());
//        Assert.assertEquals(projectExportBadRequest.getProjectExportDeclinations().size(), 3);
//    }

    private ProjectExportException aNotFoundProjectExportException() {
        ProjectExportException pee = new ProjectExportException("message");
        pee.setProjectIdsNotFound(Lists.newArrayList("1", "2", "3"));
        return pee;
    }

    private ProjectExportException aNotExportableProjectExportException() {
        ProjectExportException pee = new ProjectExportException("message");
        return pee;
    }

}
