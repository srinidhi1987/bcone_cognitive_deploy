package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.export.impl.MLDataExportOperator;
import com.automationanywhere.cognitive.project.export.model.MlDataExportMetadata;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MLDataExportOperatorTest {

  MLDataExportOperator mlDataExportOperator;
  @Mock
  private DataExporter<MlDataExportMetadata> dataExporter;
  @Mock
  private DataExportListener dataExportListener;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    mlDataExportOperator = new MLDataExportOperator(dataExporter);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testExportMlData() throws Exception {
    //given
    String exportDir = "Output//";
    String[] projectIds = {"Projid1", "Projid2"};

    mlDataExportOperator.export(exportDir, projectIds, dataExportListener);
    //when
    InOrder order = Mockito.inOrder(dataExporter, dataExportListener);

    Thread.sleep(1000L);
    //then
    order.verify(dataExporter, Mockito.times(2))
        .exportData(Mockito.anyString(), Mockito.anyMap());
    order.verify(dataExportListener).onMlDataExportCompleted(Mockito.anyList());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testMultipleExportMlData() throws Exception {
    //given
    String exportDir = "Output//";
    String[] projectIds = {"Projid1", "Projid2", "Projid3", "Projid4", "Projid5", "Projid6",
        "Projid7", "Projid8", "Projid9", "Projid10"};

    mlDataExportOperator.export(exportDir, projectIds, dataExportListener);
    //when
    InOrder order = Mockito.inOrder(dataExporter, dataExportListener);

    Thread.sleep(1000L);
    //then
    order.verify(dataExporter, Mockito.times(10))
        .exportData(Mockito.anyString(), Mockito.anyMap());
    order.verify(dataExportListener).onMlDataExportCompleted(Mockito.anyList());
  }
}
