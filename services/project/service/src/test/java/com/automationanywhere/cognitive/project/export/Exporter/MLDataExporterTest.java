package com.automationanywhere.cognitive.project.export.Exporter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.TrainingDataDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MLDataExporterTest {

  @InjectMocks
  private MLDataExporter mlDataExporter;

  @Mock
  private TrainingDataDao trainingdataDao;

  @Mock
  private FileWriter csvFileWriter;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test(expectedExceptions = {ProjectExportException.class})
  public void exportDataShouldThrowProjectExportException() throws IOException {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");

    List<String[]> trainingData = new ArrayList<String[]>();
    trainingData
        .add(new String[]{"3125357", "a3125357", "a84d55de-2c99-4613-9813-a779caf1b964_1", "12:00.8"
        });
    trainingData
        .add(new String[]{"4125358", "a3125357", "a84d55de-2c99-4613-9813-a779caf1b964_1", "12:00.8"
        });

    Mockito.doThrow(IOException.class).when(csvFileWriter)
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));

    when(trainingdataDao.getTrainingDatas(Mockito.anyString())).thenReturn(trainingData);

    mlDataExporter.exportData("./", params);
  }

  @Test
  public void exportTrainingDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");

    List<String[]> trainingData = new ArrayList<String[]>();
    trainingData
        .add(new String[]{"3125357", "a3125357", "a84d55de-2c99-4613-9813-a779caf1b964_1", "12:00.8"
        });
    trainingData
        .add(new String[]{"4125358", "a3125357", "a84d55de-2c99-4613-9813-a779caf1b964_1", "12:00.8"
        });

    when(trainingdataDao.getTrainingDatas(Mockito.anyString())).thenReturn(trainingData);

    mlDataExporter.exportData("./", params);

    //then
    InOrder order = Mockito.inOrder(trainingdataDao, csvFileWriter);
    order.verify(trainingdataDao).getTrainingDatas(Mockito.anyString());
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void testExportContentTrainSetDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = new HashMap<Object, Object>();
    params.put("projectId", "projectid");

    List<String[]> trainingData = null;

    when(trainingdataDao.getTrainingDatas(Mockito.anyString())).thenReturn(trainingData);

    mlDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

}
