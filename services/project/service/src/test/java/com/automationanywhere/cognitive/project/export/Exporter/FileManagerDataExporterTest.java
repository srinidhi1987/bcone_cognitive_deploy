package com.automationanywhere.cognitive.project.export.Exporter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.ClassificationReportDao;
import com.automationanywhere.cognitive.project.dal.FileBlobDao;
import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.FileWriter;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FileManagerDataExporterTest {

  @InjectMocks
  FileManagerDataExporter fileManagerDataExporter;
  @Mock
  private FileDetailsDao fileDetailsDao;
  @Mock
  private FileBlobDao fileBlobDao;
  @Mock
  private ClassificationReportDao classificationReportDao;
  @Mock
  private FileWriter csvFileWriter;

  @BeforeMethod
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test(expectedExceptions = {ProjectExportException.class})
  public void exportDataShouldThrowProjectExportException() throws IOException {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    @SuppressWarnings("unchecked")
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");
    String projectId = projectFileEntry.getKey().toString();
    List<String> fileIds = projectFileEntry.getValue();
    int isProduction = (int) params.get("isProduction");

    List<String[]> fileDetailsData = new ArrayList<String[]>();
    fileDetailsData.add(
        new String[]{"159f8313-f4a0-43e9-b8db-927d6e65c566", "94eca740-5b13-4e70-8064-3155dbe038ac",
            "09152016154224KL6E_skb_skb_SIS-Scanner1_007B02B7.pdf", "", "1453945", "2200", "1700",
            "pdf", "0", "", "cjai7foms00022wgi7986v44m", "3", "FALSE"});

    Mockito.doThrow(IOException.class).when(csvFileWriter)
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  //when
    when(fileDetailsDao.getFileDetails(projectId, fileIds, isProduction))
        .thenReturn(fileDetailsData);

    fileManagerDataExporter.exportData("./", params);
  }

  @Test
  public void exportFileDetailsDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    @SuppressWarnings("unchecked")
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");
    String projectId = projectFileEntry.getKey().toString();
    List<String> fileIds = projectFileEntry.getValue();

    int isProduction = (int) params.get("isProduction");

    List<String[]> fileDetailsData = new ArrayList<String[]>();
    fileDetailsData.add(
        new String[]{"159f8313-f4a0-43e9-b8db-927d6e65c566", "94eca740-5b13-4e70-8064-3155dbe038ac",
            "09152016154224KL6E_skb_skb_SIS-Scanner1_007B02B7.pdf", "", "1453945", "2200", "1700",
            "pdf", "0", "", "cjai7foms00022wgi7986v44m", "3", "FALSE"});
    fileDetailsData.add(
        new String[]{"169f8313-f4a0-43e9-b8db-927d6e65c566", "94eca740-5b13-4e70-8064-3155dbe038ac",
            "09152016154224KL6E_skb_skb_SIS-Scanner1_007B02B7.pdf", "", "1453945", "2200", "1700",
            "pdf", "0", "", "cjai7foms00022wgi7986v44m", "4", "TRUE"});

    //when
    when(fileDetailsDao.getFileDetails(projectId, fileIds, isProduction))
        .thenReturn(fileDetailsData);
    fileManagerDataExporter.exportData("./", params);
    //then
    InOrder order = Mockito.inOrder(fileDetailsDao, csvFileWriter);
    order.verify(fileDetailsDao).getFileDetails(projectId, fileIds, isProduction);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportFileDetailsDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    @SuppressWarnings("unchecked")
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");
    String projectId = projectFileEntry.getKey().toString();
    List<String> fileIds = projectFileEntry.getValue();

    int isProduction = (int) params.get("isProduction");

    List<String[]> fileDetailsData = null;
    //when
    when(fileDetailsDao.getFileDetails(projectId, fileIds, isProduction))
        .thenReturn(fileDetailsData);
    fileManagerDataExporter.exportData("./", params);
    //then

    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @Test
  public void exportFileBlobDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    @SuppressWarnings("unchecked")
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");
    List<String> fileIds = projectFileEntry.getValue();

    List<String[]> fileBlobData = new ArrayList<String[]>();
    fileBlobData.add(
        new String[]{"159f8313-f4a0-43e9-b8db-927d6e65c566", "0X0203030"});
    fileBlobData.add(
        new String[]{"169f8313-f4a0-43e9-b8db-927d6e65c566", "0X0203030"});

    //when
    when(fileBlobDao.getFileBlobs(fileIds))
        .thenReturn(fileBlobData);
    fileManagerDataExporter.exportData("./", params);
    //then
    InOrder order = Mockito.inOrder(fileBlobDao, csvFileWriter);
    order.verify(fileBlobDao).getFileBlobs(fileIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void exportFileBlobDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");
    List<String> fileIds = projectFileEntry.getValue();

    List<String[]> fileBlobData = null;

    //when
    when(fileBlobDao.getFileBlobs(fileIds))
        .thenReturn(fileBlobData);
    fileManagerDataExporter.exportData("./", params);
    //then

    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void exportClassificationReportDataShouldWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");

    List<String> fileIds = projectFileEntry.getValue();

    List<String[]> classificationReportData = new ArrayList<String[]>();
    classificationReportData.add(
        new String[]{"60e0fabc-6cdf-4f79-9cdf-405653b00f32","1","Invoice","TRUE"});
    classificationReportData.add(
        new String[]{"70e0fabc-6cdf-4f79-9cdf-405653b00f32","1","Invoice","FALSE"});

    //when
    when(classificationReportDao.getClassificationReports(fileIds))
        .thenReturn(classificationReportData);
    fileManagerDataExporter.exportData("./", params);
    //then
    InOrder order = Mockito.inOrder(classificationReportDao, csvFileWriter);
    order.verify(classificationReportDao).getClassificationReports(fileIds);
    order.verify(csvFileWriter).writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void exportClassificationReportDataShouldNotWriteDataToFile() throws Exception {
    //given
    Map<Object, Object> params = mapOfSampleExportParams();
    Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
        .get("FileIdByProjectIdEntry");

    List<String> fileIds = projectFileEntry.getValue();

    List<String[]> classificationReportData = null;
    //when
    when(classificationReportDao.getClassificationReports(fileIds))
        .thenReturn(classificationReportData);
    fileManagerDataExporter.exportData("./", params);
    //then
    Mockito.verify(csvFileWriter, times(0))
        .writeData(Mockito.any(), Mockito.anyListOf(String[].class));
  }

  private Map<Object, Object> mapOfSampleExportParams() {

    Map.Entry<String, List<String>> projectFileEntry = new SimpleEntry<String, List<String>>(
        "prj1", Arrays
        .asList("file1", "file2", "file3", "file4", "file5", "file6", "file7", "file8", "file9",
            "file10", "file11", "file12", "file13", "file14"));

    Map<Object, Object> exportParams = new HashMap<Object, Object>();
    exportParams.put("FileIdByProjectIdEntry", projectFileEntry);
    exportParams.put("batchId", 0);
    exportParams.put("isProduction", 0);
    return exportParams;
  }
}
