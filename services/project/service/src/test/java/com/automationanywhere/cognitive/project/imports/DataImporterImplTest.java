package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.BulkDataImportDao;
import com.automationanywhere.cognitive.project.imports.impl.DataImporterImpl;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DataImporterImplTest {

  @InjectMocks
  DataImporterImpl dataImporterImpl;
  @Mock
  BulkDataImportDao bulkDataImportDao;
  @Mock
  private AdvanceDataImporter dataImporter;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void importProjectServiceRelatedDataShouldReturnMapOfCountsPerTable() throws Exception {
    //given
    String filename = "sample.csv";
    Map<TableConstants, List<Path>> tableConstantsListMap = getFilePaths(filename);

    TableConstants projectDetailConstant = TableConstants.PROJECTDETAIL;
    TableConstants projOcrEngDetailConstant = TableConstants.PROJECTOCRENGINEDETAILSMASTERMAPPING;
    TableConstants customFieldDetailConstant = TableConstants.CUSTOMFIELDDETAIL;
    TableConstants standrdFieldDetailContant = TableConstants.STANDARDFIELDDETAIL;

    Map<String, Integer> expectedNoOfRowsUpdatedByTable = new HashMap<>();
    expectedNoOfRowsUpdatedByTable.put(projectDetailConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(projOcrEngDetailConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(customFieldDetailConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(standrdFieldDetailContant.getName(), 1);

    Path filePath = Paths.get(filename);
    //when
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, projectDetailConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, projOcrEngDetailConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, customFieldDetailConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, standrdFieldDetailContant)).thenReturn(1);

    Map<String, Integer> actualNoOfRowsUpdatedByTable = dataImporterImpl
        .importProjectData(tableConstantsListMap);
    //then
    Assert.assertEquals(actualNoOfRowsUpdatedByTable, expectedNoOfRowsUpdatedByTable);
  }

  @Test
  public void importFileManagerRelatedDataShouldReturnMapOfCountsPerTable() throws Exception {
    //given
    String filename = "sample.csv";
    Map<TableConstants, List<Path>> tableConstantsListMap = getFilePaths(filename);

    TableConstants fileDetailsConstant = TableConstants.FILEDETAILS;
    TableConstants fileBlobConstant = TableConstants.FILEBLOBS;
    TableConstants clasfictnRptConstant = TableConstants.CLASSIFICATIONREPORT;

    Map<String, Integer> expectedNoOfRowsUpdatedByTable = new HashMap<>();
    expectedNoOfRowsUpdatedByTable.put(fileDetailsConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(fileBlobConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(clasfictnRptConstant.getName(), 1);

    Path filePath = Paths.get(filename);
    //when
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, fileDetailsConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, fileBlobConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, clasfictnRptConstant)).thenReturn(1);

    Map<String, Integer> actualNoOfRowsUpdatedByTable = dataImporterImpl
        .importFileManagerData(tableConstantsListMap);
    //then
    Assert.assertEquals(actualNoOfRowsUpdatedByTable, expectedNoOfRowsUpdatedByTable);
  }

  @Test
  public void importVisionBotRelatedDataShouldReturnMapOfCountsPerTable() throws Exception {
    //given
    String filename = "sample.csv";
    Map<TableConstants, List<Path>> tableConstantsListMap = getFilePaths(filename);

    TableConstants visionBotDetailConstant = TableConstants.VISIONBOTDETAIL;
    TableConstants visionBotConstant = TableConstants.VISIONBOT;
    TableConstants visionBotRunDetailConstant = TableConstants.VISIONBOTRUNDETAILS;
    TableConstants testsetInfoContant = TableConstants.TESTSETINFO;
    TableConstants documentProcessedDetailConstant = TableConstants.DOCUMENTPROCCESSEDDATAILS;
    TableConstants testSetConstant = TableConstants.TESTSET;

    Map<String, Integer> expectedNoOfRowsUpdatedByTable = new HashMap<>();
    expectedNoOfRowsUpdatedByTable.put(visionBotDetailConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(visionBotConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(visionBotRunDetailConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(testsetInfoContant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(documentProcessedDetailConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(testSetConstant.getName(), 1);

    Path filePath = Paths.get(filename);
    //when
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, visionBotDetailConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, visionBotConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, visionBotRunDetailConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, testsetInfoContant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, documentProcessedDetailConstant))
        .thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, testSetConstant)).thenReturn(1);

    Map<String, Integer> actualNoOfRowsUpdatedByTable = dataImporterImpl
        .importVisionBotData(tableConstantsListMap);
    //then
    Assert.assertEquals(actualNoOfRowsUpdatedByTable, expectedNoOfRowsUpdatedByTable);
  }

  @Test
  public void importMlRelatedDataShouldReturnMapOfCountsPerTable() throws Exception {
    //given
    String filename = "sample.csv";
    Map<TableConstants, List<Path>> tableConstantsListMap = getFilePaths(filename);

    TableConstants trainingDataConstant = TableConstants.TRAININGDATA;

    Map<String, Integer> expectedNoOfRowsUpdatedByTable = new HashMap<>();
    expectedNoOfRowsUpdatedByTable.put(trainingDataConstant.getName(), 1);

    Path filePath = Paths.get(filename);
    //when
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, trainingDataConstant)).thenReturn(1);

    Map<String, Integer> actualNoOfRowsUpdatedByTable = dataImporterImpl
        .importMlData(tableConstantsListMap);
    //then
    Assert.assertEquals(actualNoOfRowsUpdatedByTable, expectedNoOfRowsUpdatedByTable);
  }

  @Test
  public void importClassifierRelatedDataShouldReturnMapOfCountsPerTable() throws Exception {
    //given
    String filename = "sample.csv";
    Map<TableConstants, List<Path>> tableConstantsListMap = getFilePaths(filename);

    TableConstants contentTrainsetConstant = TableConstants.CONTENTTRAINSET;
    TableConstants layoutTrainsetConstant = TableConstants.LAYOUTTRAINSET;

    Map<String, Integer> expectedNoOfRowsUpdatedByTable = new HashMap<>();
    expectedNoOfRowsUpdatedByTable.put(contentTrainsetConstant.getName(), 1);
    expectedNoOfRowsUpdatedByTable.put(layoutTrainsetConstant.getName(), 1);

    Path filePath = Paths.get(filename);
    //when
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, contentTrainsetConstant)).thenReturn(1);
    Mockito.when(bulkDataImportDao.bulkInsert(filePath, layoutTrainsetConstant)).thenReturn(1);

    Map<String, Integer> actualNoOfRowsUpdatedByTable = dataImporterImpl
        .importClassifierData(tableConstantsListMap);
    //then
    Assert.assertEquals(actualNoOfRowsUpdatedByTable, expectedNoOfRowsUpdatedByTable);
  }

  public Map<TableConstants, List<Path>> getFilePaths(String... filenames) {
    Map<TableConstants, List<Path>> mapOfPathsByTable = new HashMap<>();
    for (TableConstants tableConstant : TableConstants.values()) {
      List<Path> paths = Stream.of(filenames).map(filename -> Paths.get(filename))
          .collect(Collectors.toList());
      mapOfPathsByTable.put(tableConstant, paths);
    }
    return mapOfPathsByTable;
  }
}
