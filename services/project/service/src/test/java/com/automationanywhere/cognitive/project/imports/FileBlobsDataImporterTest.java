package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.FileBlobDao;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.csvmodel.FileBlob;
import com.automationanywhere.cognitive.project.imports.impl.ClassificationReportDataImporter;
import com.automationanywhere.cognitive.project.imports.impl.FileBlobsDataImporter;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.FileBlobs;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Hex;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FileBlobsDataImporterTest {

  @Mock
  FileReader csvFileReader;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileBlobDao fileBlobDao;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  private ClassificationReportDataImporter classificationReportDataImporter;
  @InjectMocks
  private FileBlobsDataImporter fileBlobsDataImporter;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    fileBlobsDataImporter = new FileBlobsDataImporter(csvFileReader,fileHelper,
        transitionResult,fileBlobDao, importFilePatternConstructor);
  }

  @Test
  public void importDataShouldImportDataWithTaskOptionClear() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds=new ArrayList<>();
    fileIds.add("123");
    //when
    FileBlobsDataImporter fileBlobsDataImporterSpy = Mockito.spy(this.fileBlobsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileBlob_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(getFileBlob("123")).when(fileBlobsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    fileBlobsDataImporterSpy.importData(Paths.get("."), TaskOptions.CLEAR);
    //then
    Mockito.verify(fileBlobDao, Mockito.atLeastOnce())
        .batchInsertFileBlobs(Mockito.anyListOf(FileBlobs.class));
  }

  @Test
  public void importDataShouldImportDataWithTaskOptionAppend() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds=new ArrayList<>();
    fileIds.add("123");

    //when
    FileBlobsDataImporter fileBlobsDataImporterSpy = Mockito.spy(this.fileBlobsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileBlob_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(getFileBlob("123")).when(fileBlobsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);

    fileBlobsDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);
    //then
    Mockito.verify(fileBlobDao, Mockito.atLeastOnce())
        .batchInsertFileBlobs(Mockito.anyListOf(FileBlobs.class));
  }

  @Test
  public void importDataShouldNotImportIfDataIsNotAvailableForAppendOption() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    FileBlobsDataImporter fileBlobsDataImporterSpy = Mockito.spy(this.fileBlobsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileBlob_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(new ArrayList()).when(fileBlobsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    fileBlobsDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);
    //then
    Mockito.verify(fileBlobDao, Mockito.times(0))
        .batchInsertFileBlobs(Mockito.anyListOf(FileBlobs.class));
  }

  @Test(expectedExceptions = ProjectImportException.class)
  public void importDataShouldThrowProjectImportExceptionWhileFetchingDataFromCsv() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    //when
    FileBlobsDataImporter fileBlobsDataImporterSpy = Mockito.spy(this.fileBlobsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileBlob_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doThrow(ProjectImportException.class).when(fileBlobsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    fileBlobsDataImporterSpy.importData(Paths.get("."), TaskOptions.APPEND);
  }

  @Test
  public void importDataShouldImportDataWithTaskOptionMerge() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds = new ArrayList<String>();
    fileIds.add("123");
    //when
    FileBlobsDataImporter fileBlobsDataImporterSpy = Mockito.spy(this.fileBlobsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileBlob_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(getFileBlob("123")).when(fileBlobsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    fileBlobsDataImporterSpy.importData(Paths.get("."), TaskOptions.MERGE);
    //then
    Mockito.verify(fileBlobDao, Mockito.atLeastOnce())
        .batchInsertFileBlobs(Mockito.anyListOf(FileBlobs.class));
  }

  @Test
  public void importDataShouldImportDataWithTaskOptionOverwrite() throws Exception {
    //given
    String projectid = "projid123";
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("D://pathToParent/"));
    List<String> fileIds = new ArrayList<String>();
    fileIds.add("123");
    //when
    FileBlobsDataImporter fileBlobsDataImporterSpy = Mockito.spy(this.fileBlobsDataImporter);
    Mockito.when(transitionResult.getProjectId()).thenReturn(projectid);
    Mockito.when(importFilePatternConstructor.getRegexPatternWithOr(Mockito.any()))
        .thenReturn("FileBlob_regex");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    Mockito.doReturn(getFileBlob("123")).when(fileBlobsDataImporterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    Mockito.when(transitionResult.getFileIds()).thenReturn(fileIds);
    fileBlobsDataImporterSpy.importData(Paths.get("."), TaskOptions.OVERWRITE);
    //then
    Mockito.verify(fileBlobDao, Mockito.atLeastOnce())
        .batchInsertFileBlobs(Mockito.anyListOf(FileBlobs.class));
  }

  private List<FileBlob> getFileBlob(String fileId) {
    List<FileBlob> fileBlobList = new ArrayList<>();
    FileBlob fileBlob = new FileBlob();
    fileBlob.setFileId(fileId);
    fileBlob.setFileBlob(Hex.encodeHexString("abc".getBytes()));
    fileBlobList.add(fileBlob);
    return fileBlobList;
  }
}
