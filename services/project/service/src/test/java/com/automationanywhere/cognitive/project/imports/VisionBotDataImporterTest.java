package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.Util.JsonNodeUtil;
import com.automationanywhere.cognitive.project.dal.VisionBotDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.VisionBotDataImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.VisionBot;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class VisionBotDataImporterTest {

  @InjectMocks
  VisionBotDataImporter importer;
  @Mock
  ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  VisionBotDao visionBotDao;
  @Mock
  AdvanceDataImporter nextDataImporter;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private FileReader fileReader;
  private static Object[][] allTaskOption;

  @BeforeClass
  public void prepareData() {
    TaskOptions[] arrayOfTaskOptions = TaskOptions.values();
    allTaskOption = new Object[arrayOfTaskOptions.length][];
    for (int i = 0; i < arrayOfTaskOptions.length; i++) {
      allTaskOption[i] = new Enum[]{arrayOfTaskOptions[i]};
    }
  }

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    importer = new VisionBotDataImporter
        (fileReader, visionBotDao, transitionResult, fileHelper,
            importFilePatternConstructor);
  }

  @DataProvider(name = "TaskOptionsDataProvider")
  public static Object[][] taskOptionsDataProvider() {
    return allTaskOption;
  }

  @SuppressWarnings("unchecked")
  @Test(dataProvider = "TaskOptionsDataProvider")
  public void importVisionBotData_AnyMode(TaskOptions taskOptions)
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    VisionBotDataImporter spy = Mockito.spy(importer);
    List<VisionBot> csvData = new ArrayList<>();
    csvData.add(getVisionBotDetailModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    spy.importData(directoryPath, taskOptions);
    //then
    Mockito.verify(visionBotDao, times(1)).batchInsert(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class, dataProvider = "TaskOptionsDataProvider")
  public void importVisionBotData_AnyMode_ThrowIOException(TaskOptions taskOptions)
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    //when
    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);
    //then
    importer.importData(directoryPath, taskOptions);
  }

  @Test(expectedExceptions = ProjectImportException.class, dataProvider = "TaskOptionsDataProvider")
  public void importVisionBotData_AnyMode_ThrowProjectImportException(TaskOptions taskOptions)
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));
    VisionBotDataImporter visionBotDataSpy = Mockito.spy(importer);
    when(transitionResult.getProjectId()).thenReturn("4b08ac1d-15d0-4a24-93ab-4fc6b80f57df");
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    Mockito.doThrow(ProjectImportException.class).when(visionBotDataSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());
    //when
    visionBotDataSpy.importData(directoryPath, taskOptions);
  }

  private VisionBot getVisionBotDetailModel() {
    VisionBot model = new VisionBot();
    model.setId("1");
    model.setDataBlob("2131231312");
    model.setEnvironment("Staging");
    model.setLockedUserId("Admin");
    model.setValidationDataBlob("validationdatalob");
    return model;
  }

}
