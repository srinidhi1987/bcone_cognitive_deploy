package com.automationanywhere.cognitive.project.imports.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.VisionbotDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.VisionBotDetail;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class VisionBotDetailDataImporterTest {

  @InjectMocks
  VisionBotDetailDataImporter dataImporterImpl;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private VisionbotDetailDao visionbotDetailDao;
  @Mock
  private FileReader fileReader;
  @Mock
  private ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  private TransitionResult transitionResult;
  @Captor
  private ArgumentCaptor<List<VisionBotDetail>> argumentCaptor;

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    dataImporterImpl = new VisionBotDetailDataImporter(fileReader, fileHelper,
        visionbotDetailDao,
        importFilePatternConstructor, transitionResult);
    when(transitionResult.getProjectId()).thenReturn("1");
    when(transitionResult.getModifiedClasses()).thenReturn(getModifiedClasses());
  }

  @DataProvider(name = "TaskOptionsAppendMergeProvider")
  public static Object[][] taskOptionsAppendMergeProvider() {
    return new Object[][]{{TaskOptions.APPEND},{TaskOptions.MERGE}};
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importShouldFailWhenIOExceptionCaught()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    dataImporterImpl.importData(Paths.get("."), TaskOptions.APPEND);
  }

  @Test
  public void importShouldAddVisionBotDetailsForClearOption()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<VisionBotDetail> existingData = new ArrayList<VisionBotDetail>();
    existingData.add(getVisionBotDetail("1"));
    existingData.add(getVisionBotDetail("2"));
    when(visionbotDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    //when
    testVisionBotAddition(TaskOptions.CLEAR);

    //then
    Assert.assertEquals(argumentCaptor.getValue().size(), 2);

    VisionBotDetail insertedRow = argumentCaptor.getValue().get(0);
    Assert.assertNull(insertedRow.getLockedUserId());
    Assert.assertEquals(insertedRow.getClassificationId(), "2");
  }

  @Test(dataProvider = "TaskOptionsAppendMergeProvider")
  public void importShouldAddVisionBotDetailsForAppendAndMergeOption(TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<VisionBotDetail> existingData = new ArrayList<VisionBotDetail>();
    existingData.add(getVisionBotDetail("1"));
    existingData.add(getVisionBotDetail("2"));

    when(visionbotDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    //when
    testVisionBotAddition(taskOptions);

    //then
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);

    VisionBotDetail insertedRow = argumentCaptor.getValue().get(0);
    Assert.assertNull(insertedRow.getLockedUserId());
    Assert.assertEquals(insertedRow.getClassificationId(), "2");
  }

  @Test
  public void importShouldAddVisionBotDetailsForOverwriteOption()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<VisionBotDetail> existingData = new ArrayList<VisionBotDetail>();
    existingData.add(getVisionBotDetail("1"));
    existingData.add(getVisionBotDetail("2"));
    when(visionbotDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    //when
    testVisionBotAddition(TaskOptions.OVERWRITE);

    //then
    Mockito.verify(visionbotDetailDao, times(1)).delete(Mockito.any());
    Assert.assertEquals(argumentCaptor.getValue().size(), 2);

    VisionBotDetail insertedRow = argumentCaptor.getValue().get(0);
    Assert.assertNull(insertedRow.getLockedUserId());
    Assert.assertEquals(insertedRow.getClassificationId(), "2");
  }

  @Test(dataProvider = "TaskOptionsAppendMergeProvider")
  public void importShouldAddNewVisionBotDetailForAppendAndMergeOption(TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    List<VisionBotDetail> existingData = new ArrayList<VisionBotDetail>();
    existingData.add(getVisionBotDetail("1"));
    existingData.add(getVisionBotDetail("2"));

    when(visionbotDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    //when
    testVisionBotAddition(taskOptions);

    //then
    Assert.assertEquals(argumentCaptor.getValue().size(), 1);

    VisionBotDetail insertedRow = argumentCaptor.getValue().get(0);
    Assert.assertNull(insertedRow.getLockedUserId());
    Assert.assertEquals(insertedRow.getClassificationId(), "2");
  }

  @Test
  public void importShouldNotAddDuplicateData()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {

    //given
    VisionBotDetailDataImporter spy = configureVisionBotDetailsDataImporter();

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(visionbotDetailDao, times(1)).addAll(Mockito.any());
  }

  @Test
  public void importShouldCallAnotherDataImporter()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    VisionBotDetailDataImporter spy = configureVisionBotDetailsDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);
    spy.setNext(advanceDataImporter);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @Test
  public void importShouldNotCallAnotherDataImporterIfNextDataImporterIsNull()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    VisionBotDetailDataImporter spy = configureVisionBotDetailsDataImporter();

    AdvanceDataImporter advanceDataImporter = Mockito.mock(AdvanceDataImporter.class);

    //when
    spy.importData(Paths.get("."), TaskOptions.APPEND);

    //then
    Mockito.verify(advanceDataImporter, times(0)).importData(Mockito.any(), Mockito.any());
  }

  private VisionBotDetail getVisionBotDetail(String id) {
    VisionBotDetail visionbotDetail = new VisionBotDetail();
    visionbotDetail.setId(id);
    visionbotDetail.setProjectId("1");
    visionbotDetail.setClassificationId("1");
    return visionbotDetail;
  }

  @SuppressWarnings("unchecked")
  private VisionBotDetailDataImporter configureVisionBotDetailsDataImporter()
      throws ProjectImportException, IOException {
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<VisionBotDetail> existingData = new ArrayList<VisionBotDetail>();
    existingData.add(getVisionBotDetail("1"));
    existingData.add(getVisionBotDetail("2"));

    List<VisionBotDetail> csvData = new ArrayList<VisionBotDetail>();
    csvData.add(getVisionBotDetail("2"));

    VisionBotDetailDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);
    when(visionbotDetailDao.getAll(Mockito.any())).thenReturn(existingData);

    return spy;
  }

  private Map<Integer, Integer> getModifiedClasses() {
    Map<Integer, Integer> modifiedClasses = new HashMap<>();
    modifiedClasses.put(Integer.parseInt("1"), Integer.parseInt("2"));
    modifiedClasses.put(Integer.parseInt("3"), Integer.parseInt("3"));
    return modifiedClasses;
  }

  @SuppressWarnings("unchecked")
  private void testVisionBotAddition(TaskOptions taskOptions)
      throws ProjectImportException, IOException, ParseException, DuplicateProjectNameException {
    //given
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("path"));

    List<VisionBotDetail> csvData = new ArrayList<VisionBotDetail>();
    csvData.add(getVisionBotDetail("3"));
    csvData.add(getVisionBotDetail("1"));

    VisionBotDetailDataImporter spy = Mockito.spy(dataImporterImpl);
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    //when
    spy.importData(Paths.get("."), taskOptions);

    //then
    Mockito.verify(visionbotDetailDao).addAll(argumentCaptor.capture());
    Mockito.verify(visionbotDetailDao, times(1)).addAll(Mockito.any());
  }
}
