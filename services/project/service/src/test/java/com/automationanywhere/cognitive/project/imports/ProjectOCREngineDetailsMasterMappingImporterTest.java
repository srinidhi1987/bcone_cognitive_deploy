package com.automationanywhere.cognitive.project.imports;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.project.dal.impl.ProjectOCREngineDetailsMasterMappingImpl;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.csvmodel.ProjectOCREngineDetailsMasterCsv;
import com.automationanywhere.cognitive.project.imports.impl.ImportFilePatternConstructor;
import com.automationanywhere.cognitive.project.imports.impl.ProjectOCREngineDetailsMasterMappingImporter;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ProjectOCREngineDetailsMasterMapping;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ProjectOCREngineDetailsMasterMappingImporterTest {

  @InjectMocks
  ProjectOCREngineDetailsMasterMappingImporter importer;
  @Mock
  ImportFilePatternConstructor importFilePatternConstructor;
  @Mock
  ProjectOCREngineDetailsMasterMappingImpl projectOCREngineDetailsMasterMapping;
  @Mock
  private TransitionResult transitionResult;
  @Mock
  private FileHelper fileHelper;
  @Mock
  private FileReader fileReader;

  private static final String PROJECT_ID = "4b08ac1d-15d0-4a24-93ab-4fc6b80f57df";

  @BeforeMethod
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    importer = new ProjectOCREngineDetailsMasterMappingImporter
        (fileReader, transitionResult, fileHelper, projectOCREngineDetailsMasterMapping,
            importFilePatternConstructor);
  }

  @DataProvider(name = "TaskOptionsAppendMergeProvider")
  public static Object[][] taskOptionsAppendMergeProvider() {
    return new Object[][]{{TaskOptions.APPEND},{TaskOptions.MERGE}};
  }

  @SuppressWarnings("unchecked")
  @Test(expectedExceptions = IOException.class)
  public void importDataInAppendModeShouldThrowIOExceptionWhenFailToFetchFilesPaths()
      throws IOException, ParseException, DuplicateProjectNameException, ProjectImportException {
    //given
    Path directoryPath = Paths.get(".");

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

    //when
    importer.importData(directoryPath, TaskOptions.APPEND);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void setNextShouldInvokeNextImporterWhenNextImporterAvailable()
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectOCREngineDetailsMasterMappingImporter projectOcrEngineMasterSpy = Mockito.spy(importer);
    List<ProjectOCREngineDetailsMasterCsv> csvData = new ArrayList<>();
    csvData.add(getProjectOCREngineDetailMasterCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) projectOcrEngineMasterSpy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    AdvanceDataImporter nextImporter = Mockito.mock(AdvanceDataImporter.class);
    projectOcrEngineMasterSpy.setNext(nextImporter);

    //when
    projectOcrEngineMasterSpy.importData(directoryPath, TaskOptions.APPEND);

    //then
    Mockito.verify(nextImporter, times(1)).importData(Mockito.any(), Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(dataProvider = "TaskOptionsAppendMergeProvider")
  public void importDataShouldSkipInsertWhenAppendOrMergeMode(TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectOCREngineDetailsMasterMappingImporter spy = Mockito.spy(importer);
    List<ProjectOCREngineDetailsMasterCsv> csvData = new ArrayList<>();
    csvData.add(getProjectOCREngineDetailMasterCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    List<ProjectOCREngineDetailsMasterMapping> projectOcrDataData = new ArrayList<>();
    projectOcrDataData.add(getProjectOCREngineDetailMasterModel());
    Mockito.doReturn(projectOcrDataData).when(projectOCREngineDetailsMasterMapping)
        .getProjectOcrMappingByProjectId(Mockito.any());

    //when
    spy.importData(directoryPath, taskOptions);

    //then
    Mockito.verify(projectOCREngineDetailsMasterMapping, times(0))
        .add(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test(dataProvider = "TaskOptionsAppendMergeProvider")
  public void importDataShouldCallInsertWhenAppendOrMergeMode(TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectOCREngineDetailsMasterMappingImporter spy = Mockito.spy(importer);
    List<ProjectOCREngineDetailsMasterCsv> csvData = new ArrayList<>();
    csvData.add(getProjectOCREngineDetailMasterCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    Mockito.doReturn(null).when(projectOCREngineDetailsMasterMapping)
        .getProjectOcrMappingByProjectId(Mockito.any());

    //when
    spy.importData(directoryPath, taskOptions);

    //then
    Mockito.verify(projectOCREngineDetailsMasterMapping, times(1))
        .add(Mockito.any());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void importDataShouldCallDeleteAndInsertWhenOverrideMode()
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //given
    Path directoryPath = Paths.get(".");
    List<Path> csvPaths = new ArrayList<>();
    csvPaths.add(Paths.get("First"));

    when(transitionResult.getProjectId()).thenReturn(PROJECT_ID);
    when(fileHelper.getAllFilesPaths(Mockito.any(), Mockito.any())).thenReturn(csvPaths);

    ProjectOCREngineDetailsMasterMappingImporter spy = Mockito.spy(importer);
    List<ProjectOCREngineDetailsMasterCsv> csvData = new ArrayList<>();
    csvData.add(getProjectOCREngineDetailMasterCsvModel());
    Mockito.doReturn(csvData).when((AdvanceDataImporter) spy)
        .getDataFromCsv(Mockito.any(), Mockito.any());

    List<ProjectOCREngineDetailsMasterMapping> projectOcrDataData = new ArrayList<>();
    projectOcrDataData.add(getProjectOCREngineDetailMasterModel());
    Mockito.doReturn(projectOcrDataData).when(projectOCREngineDetailsMasterMapping)
        .getProjectOcrMappingByProjectId(Mockito.any());

    //when
    spy.importData(directoryPath, TaskOptions.OVERWRITE);

    //then
    Mockito.verify(projectOCREngineDetailsMasterMapping, times(1))
        .delete(Mockito.any());
    Mockito.verify(projectOCREngineDetailsMasterMapping, times(1))
        .add(Mockito.any());
  }

  private ProjectOCREngineDetailsMasterCsv getProjectOCREngineDetailMasterCsvModel() {
    ProjectOCREngineDetailsMasterCsv model = new ProjectOCREngineDetailsMasterCsv();
    model.setId(1);
    model.setProjectId(PROJECT_ID);
    model.setOcrEngineDetailsId(1);
    return model;
  }

  private ProjectOCREngineDetailsMasterMapping getProjectOCREngineDetailMasterModel() {
    ProjectOCREngineDetailsMasterMapping model = new ProjectOCREngineDetailsMasterMapping();
    model.setId(1);
    model.setProjectId(PROJECT_ID);
    model.setOcrEngineDetailsId(1);
    return model;
  }
}
