package com.automationanywhere.cognitive.project.model;

public enum TaskStatus {
    inprogress,
    complete,
    failed
}
