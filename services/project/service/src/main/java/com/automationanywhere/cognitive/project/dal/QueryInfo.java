package com.automationanywhere.cognitive.project.dal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jemin.Shah on 2/13/2017.
 */
public class QueryInfo
{
    private int offset;
    private int limit;
    List<String[]> whereList;
    private String orderBy;
    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public QueryInfo()
    {
        this.limit = -1;
        this.whereList = new ArrayList<>();
    }

    public List<String[]> getWhereList() {
        return whereList;
    }

    public void setWhereList(List<String[]> whereList) {
        this.whereList = whereList;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
