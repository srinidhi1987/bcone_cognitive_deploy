package com.automationanywhere.cognitive.project.model.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TestSetInfo")
public class TestSetInfo {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "visionbotid")
  private String visionbotid;

  @Column(name = "layoutid")
  private String layoutid;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getVisionbotid() {
    return visionbotid;
  }

  public void setVisionbotid(String visionbotid) {
    this.visionbotid = visionbotid;
  }

  public String getLayoutid() {
    return layoutid;
  }

  public void setLayoutid(String layoutid) {
    this.layoutid = layoutid;
  }
}
