package com.automationanywhere.cognitive.project.task;

import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.Task;
import java.util.List;

public interface TaskService {

  List<Task> getAllInProgressTasks();

  Task createExportTask(String userId, String exportFilePath);

  Task createImportTask(String userId, String filepath, TaskOptions taskOptions);

  void markAsFailed(Task exportTask);

  void markAsCompleted(Task exportTask);

  List<Task> getSortedTaskList(String sortColumnName,String sortOrder);

  void getAllInProgressTasksAndMarkThemFailed();

  void createFailedImportTask(String userId, String filepath, TaskOptions taskOptions);
}
