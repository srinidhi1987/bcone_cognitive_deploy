package com.automationanywhere.cognitive.project;

import static com.automationanywhere.cognitive.project.constants.HeaderConstants.CORRELATION_ID;
import static com.automationanywhere.cognitive.project.exception.impl.HttpStatusCode.BAD_REQUEST;
import static com.automationanywhere.cognitive.project.exception.impl.HttpStatusCode.OK;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.patch;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.ErrorMessageConstants;
import com.automationanywhere.cognitive.project.constants.TaskOrderConstant;
import com.automationanywhere.cognitive.project.constants.TaskTableColumn;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectDeletedException;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.exception.ProjectUpdateException;
import com.automationanywhere.cognitive.project.exception.ProjectUpdationFailedException;
import com.automationanywhere.cognitive.project.exception.ResourceUpdationException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.exception.TaskException;
import com.automationanywhere.cognitive.project.exception.VisionBotUpdationFailedException;
import com.automationanywhere.cognitive.project.exception.impl.ExceptionFilter;
import com.automationanywhere.cognitive.project.exception.impl.HttpStatusCode;
import com.automationanywhere.cognitive.project.health.handlers.impl.ProjectHealthCheckHandler;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.PatchRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportBadRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectGetResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import com.automationanywhere.cognitive.project.model.dto.StandardResponse;
import com.automationanywhere.cognitive.project.model.dto.imports.ProjectImportRequest;
import com.automationanywhere.cognitive.project.task.TaskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import spark.Request;
import spark.Response;

/**
 * Created by Jemin.Shah on 05-12-2016.
 */

@Component
public class ProjectController {

  @Autowired
  private ProjectService projectService;

  @Autowired
  private ObjectMapper jsonObjectMapper;

  @Autowired
  private ExceptionFilter exceptionFilter;

  @Autowired
  private ProjectHealthCheckHandler projectHealthCheckHandler;

  @Autowired
  private TaskService taskService;

  private AALogger log = AALogger.create(ProjectController.class);


  @PostConstruct
  public void startService() {
    jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    exceptionFilter.setMapper(jsonObjectMapper);
    exceptionFilter.init();
    port(Integer.parseInt(ConfigurationProvider.getPort()));
    before((request, response) -> preRequestProcessor(request));
    after((request, response) -> postRequestProcessor(request, response));
    get("/organizations/:orgId/projects/tasks", (req, res) -> getTaskList(req, res));
    get("/organizations/:orgId/projects", (req, res) -> getAllProjects(req, res));
    get("/organizations/:orgId/projects/metadata", (req, res) -> getAllProjectsMetadata(req, res));
    get("/organizations/:orgId/projects/:projectId", (req, res) -> getProject(req, res));
    get("/organizations/:orgId/projects/:projectId/metadata",
        (req, res) -> getProjectMetadata(req, res));
    delete("/organizations/:orgId/projects/:projectId", (req, res) -> deleteProject(req, res));
    post("/organizations/*/projects", (req, res) -> addProject(req, res));
    post("/organizations/*/projects/export/start", (req, res) -> exportProjects(req, res));
    post("/organizations/*/projects/import/start", (req, res) -> importProjects(req, res));
    get("/organizations/*/projects/tasks/status", (req, res) -> getTaskStatus(req, res));
    get("/organizations/*/projects/import/archivedetails",
        (req, res) -> getArchiveDetails(req, res));
    put("/organizations/*/projects/*", (req, res) -> updateProject(req, res));
    patch("/organizations/*/projects/*", (req, res) -> patchProject(req, res));
    get("/v2/organizations/:orgId/projects/:projectId/metadata",
        (req, res) -> getProjectMetadataV2(req, res));
    get("/health", ((request, response) -> {
      response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
      response.body("OK");
      return response;
    }));

    get("/heartbeat", ((request, response) -> {
      return getHeartBeatInfo(response);
    }));

    get("/healthcheck", ((request, response) -> {
      return projectHealthCheckHandler.checkHealth();
    }));

    markAllPendingTasksAsFailed();
  }

  private void markAllPendingTasksAsFailed() {
    taskService.getAllInProgressTasksAndMarkThemFailed();
  }

  private void postRequestProcessor(Request request, Response response) {
    addHeader(request, response);
  }

  private void preRequestProcessor(Request request) {
    addTransactionIdentifier(request);
    log.debug("Request URI.." + request.uri());
  }

  private void addHeader(Request request, Response response) {
    response.type("application/json");
  }

  private void addTransactionIdentifier(Request request) {
    String transactionId = request.headers(CORRELATION_ID);
    ThreadContext.put(CORRELATION_ID, transactionId != null ?
        transactionId : UUID.randomUUID().toString());
  }

  private String getAllProjects(Request request, Response response) {
    log.entry();
    try {
      response.status(200);
      Map<String, String[]> queryMap = request.queryMap().toMap();
      return toStandardResponse(true,
          projectService.getProjectDetailsByOrganizationId(
              request.params("orgId"),
              request.queryParams("excludeFields")),
          null);
    } catch (InvalidInputException invalidInputException) {
      log.error(invalidInputException.getMessage());
      response.status(404);
      return toStandardResponse(false, null,
          new String[]{invalidInputException.getMessage()});
    } catch (Exception ex) {
      log.error(ex.getMessage());
      response.status(500);
      return toStandardResponse(false, null,
          new String[]{ErrorMessageConstants.INTERNAL_SERVER_ERROR});
    } finally {
      log.exit();
    }
  }

  private String getAllProjectsMetadata(Request request, Response response) {
    log.entry();
    try {
      response.status(200);
      Map<String, String[]> queryMap = request.queryMap().toMap();
      return toStandardResponse(true,
          projectService.getAllMetadata(request.params("orgId"), queryMap), null);
    } catch (InvalidInputException invalidInputException) {
      log.error(invalidInputException.getMessage());
      response.status(404);
      return toStandardResponse(false, null, new String[]{invalidInputException.getMessage()});
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
      response.status(500);
      return toStandardResponse(false, null,
          new String[]{ErrorMessageConstants.INTERNAL_SERVER_ERROR});
    } finally {
      log.exit();
    }
  }

  private String getProject(Request request, Response response) {
    log.entry();
    try {
      response.status(200);
      return toStandardResponse(true,
          projectService.get(
              request.params("orgId"),
              request.params("projectId"),
              request.queryParams("excludeFields")),
          null);
    } catch (NotFoundException notFoundException) {
      log.error(notFoundException.getMessage());
      response.status(404);
      return toStandardResponse(false, null, new String[]{notFoundException.getMessage()});
    } catch (Exception ex) {
      log.error(ex.getMessage());
      response.status(500);
      return toStandardResponse(false, null,
          new String[]{ErrorMessageConstants.INTERNAL_SERVER_ERROR});
    } finally {
      log.exit();
    }
  }

  private String getProjectMetadata(Request request, Response response)
      throws InvalidInputException, NotFoundException, ProjectDeletedException {
    log.entry();

    response.status(200);
    return toStandardResponse(true,
        projectService.getMetadata(
            request.params("orgId"),
            request.params("projectId")),
        null);
  }


  private String getProjectMetadataV2(Request request, Response response)
      throws InvalidInputException, NotFoundException, ProjectDeletedException {
    log.entry();

    response.status(200);
    return toStandardResponse(true,
        projectService.getMetadataV2(
            request.params("orgId"),
            request.params("projectId")),
        null);
  }

  private String deleteProject(Request request, Response response)
      throws InvalidInputException, PreconditionFailedException {
    log.entry();

    response.status(200);
    String organizationId = request.params("orgId");
    String projectId = request.params("projectId");
    String updatedBy = request.headers("updatedBy");
    log.debug(() -> String
        .format("organizationid: %s, projectid: %s, updatedby: %s", organizationId, projectId,
            updatedBy));
    projectService.delete(organizationId, projectId, updatedBy);
    return toStandardResponse(true, null, null);

  }

  private String addProject(Request request, Response response) {
    log.entry();
    try {
      ProjectRequest projectResource = fromJsonString(request.body(), ProjectRequest.class);
      validateOrganizationDetail(request.splat()[0], projectResource);
      response.status(200);
      return toStandardResponse(true, projectService.add(projectResource), null);
    } catch (InvalidInputException invalidInputException) {
      log.error(invalidInputException.getMessage());
      response.status(400);
      return toStandardResponse(false, null, new String[]{invalidInputException.getMessage()});
    } catch (DataIntegrityViolationException dataIntegrityException) {
      log.error(dataIntegrityException.getMessage());
      response.status(400);
      return toStandardResponse(false, null,
          new String[]{ErrorMessageConstants.DUPLICATE_LEARNING_INSTANCE});
    } catch (Exception ex) {
      log.error(ex.getMessage());
      response.status(500);
      return toStandardResponse(false, null, new String[]{ex.getMessage()});
    } finally {
      log.exit();
    }
  }

  private void validateOrganizationDetail(String organizationId, ProjectRequest projectRequest)
      throws InvalidInputException {
    if (!organizationId.equals(projectRequest.getOrganizationId())) {
      throw new InvalidInputException(ErrorMessageConstants.INVALID_ORGANISATION);
    }
  }

  private String exportProjects(Request request, Response response)
      throws InvalidInputException, TimeoutException, InterruptedException {
    log.entry();
    try {
      ProjectExportRequest projectExportRequest = fromJsonString(request.body(),
          ProjectExportRequest.class);
      Task task = projectService.startProjectExport(projectExportRequest);
      response.status(200);
      return toStandardResponse(true, task, null);
    } catch (ProjectExportException projectExportException) {
      log.error(projectExportException.getMessage(), projectExportException);
      response.status(HttpStatusCode.PRECONDITION_FAILED.getCode());
      return toStandardResponse(false,
          ProjectExportBadRequest.withException(projectExportException),
          new String[]{projectExportException.getMessage()});
    } finally {
      log.exit();
    }
  }

  private String importProjects(Request request, Response response)
      throws InvalidInputException, ProjectImportException, PreconditionFailedException, SystemBusyException {
    log.entry();

    ProjectImportRequest projectImportRequest = fromJsonString(request.body(),
        ProjectImportRequest.class);
    Task task = projectService.startProjectImport(projectImportRequest);
    response.status(200);
    log.exit();
    return toStandardResponse(true, task, null);
  }

  private String getTaskStatus(Request req, Response res) {
    return toStandardResponse(true, projectService.getRunningTasks(), null);
  }

  private String getArchiveDetails(Request req, Response res) {
    return toStandardResponse(true, projectService.getArchiveDetails(), null);
  }

  private String updateProject(Request request, Response response)
      throws CloneNotSupportedException, ProjectUpdationFailedException, SystemBusyException, ResourceUpdationException, VisionBotUpdationFailedException, NotFoundException, ProjectDeletedException {
    log.entry();
    try {
      String updatedBy = request.headers("updatedBy");
      ProjectRequest projectRequest = fromJsonString(request.body(), ProjectRequest.class);
      projectService.update(projectRequest);
      log.debug(() -> String.format("organizationid: %s, projectid: %s, updatedBy: %s",
          projectRequest.getOrganizationId(), projectRequest.getId(), updatedBy));
      response.status(200);
      return toStandardResponse(true, null, null);
    } catch (InvalidInputException invalidInputException) {
      log.error(invalidInputException.getMessage(), invalidInputException);
      response.status(400);
      return toStandardResponse(false, null, new String[]{invalidInputException.getMessage()});
    } finally {
      log.exit();
    }
  }

  private Object patchProject(Request request, Response response) {
    log.entry();
    try {
      String requestParams[] = request.splat();
      String organizationId = requestParams[0];
      String projectId = requestParams[1];
      String updatedBy = request.headers("updatedBy");

      PatchRequest[] patchRequest = fromJsonString(request.body(), PatchRequest[].class);
      ProjectGetResponse projectResponse = projectService.update(
          organizationId, projectId, patchRequest
      );
      projectResponse.setVersionId(projectResponse.getVersionId() + 1);

      log.debug(() -> String.format("organizationid: %s, projectid: %s, updatedBy: %s",
          organizationId, projectId, updatedBy));

      response.status(OK.getCode());
      return toStandardResponse(true, projectResponse, null);

    } catch (ProjectUpdateException e) {
      log.error(e.getMessage(), e);

      response.status(e.getStatusCode().getCode());

      ProjectGetResponse projectGetResponse = new ProjectGetResponse();
      projectGetResponse.setVersionId(e.getVersionId());

      return toStandardResponse(false, projectGetResponse, new String[]{e.getMessage()});

    } catch (InvalidInputException e) {
      log.error("Bad user data", e);
      response.status(BAD_REQUEST.getCode());
      return toStandardResponse(false, null, new String[]{e.getMessage()});

    } finally {
      log.exit();
    }
  }


  private String toStandardResponse(boolean success, Object data, String[] errorStr) {
    log.entry();

    StandardResponse standardResponse = new StandardResponse();
    standardResponse.setSuccess(success);
    if (success || data != null) {
      standardResponse.setData(data);
    }
    if (errorStr != null && errorStr.length > 0) {
      standardResponse.setError(errorStr);
    }

    String standardResponseJson = null;
    try {
      standardResponseJson = toJsonString(standardResponse);
    } catch (InvalidInputException ex) {
      log.error(ex.getMessage());
    }
    log.exit();
    standardResponseJson = standardResponseJson == null ? "" : standardResponseJson;
    return standardResponseJson;
  }

  /**
   * Get the heartbeat of the service
   */
  private Object getHeartBeatInfo(Response response) {
    log.entry();
    String heartBeat;
    //currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization
    response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
    heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
    response.body(heartBeat);
    log.exit();
    return response;
  }

  private <T> T fromJsonString(String jsonString, Class<T> clazz)
      throws InvalidInputException {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readValue(jsonString, clazz);
    } catch (Exception ex) {
      throw new InvalidInputException(
          "Invalid string : " + jsonString + " cannot be transformed to object of required type "
              + clazz.getName(), ex);
    }
  }

  private String toJsonString(Object object) throws InvalidInputException {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException ex) {
      throw new InvalidInputException(
          "Invalid json object : " + object + " cannot be transformed to json string.", ex);
    }
  }

  private String getTaskList(Request request, Response response)
      throws InvalidInputException, TaskException {
    log.entry();
    String orgId = request.params("orgId");
    String sortColumnName = null;
    String sortMappedColumnName = null;
    String sortOrder = null;
    Set<String> queryParams = request.queryParams();

    for (String param : queryParams) {
      if (param.equalsIgnoreCase(TaskOrderConstant.SORT)) {
        sortColumnName = request.queryParams(param);
      } else if (param.equalsIgnoreCase(TaskOrderConstant.ORDER)) {
        sortOrder = request.queryParams(param);
      }
    }
    if (StringUtils.isNotBlank(sortOrder) && StringUtils.isBlank(sortColumnName)) {
      throw new InvalidInputException(ErrorMessageConstants.COLUMN_NOT_MATCHED);
    }
    try {
      if (StringUtils.isNotBlank(sortColumnName)) {
        sortMappedColumnName = TaskTableColumn.getValueData(sortColumnName);
      }
      log.debug("organizationid: %s sortColumnName: %s", orgId, sortColumnName);

      if (StringUtils.isBlank(sortOrder)) {
        sortOrder = TaskOrderConstant.DESC;
      }

      List<Task> data = projectService.getSortedTaskList(sortMappedColumnName, sortOrder);
      return toStandardResponse(true, data, null);
    } catch (Exception ex) {
      throw new TaskException(ErrorMessageConstants.PROJECT_PROCESSING_ERROR, ex);
    } finally {
      log.exit();
    }
  }
}
