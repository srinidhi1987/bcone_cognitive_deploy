package com.automationanywhere.cognitive.project.model.dao;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LayoutTrainSet")
public class LayoutTrainSet {

  @Id
  @Column(name = "hashkey", nullable = false)
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "hashkey")
  private String hashKey;

  @Column(name = "classname")
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "classname")
  private String className;

  @Column(name = "value")
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "value")
  private String value;

  public String getHashKey() {
    return hashKey;
  }

  public void setHashKey(String hashKey) {
    this.hashKey = hashKey;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
