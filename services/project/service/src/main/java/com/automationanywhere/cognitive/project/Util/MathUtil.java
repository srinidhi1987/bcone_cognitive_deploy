package com.automationanywhere.cognitive.project.Util;

import java.math.BigDecimal;

/**
 * Created by Jemin.Shah on 3/18/2017.
 */
public class MathUtil
{
    public static BigDecimal round(float d, int decimalPlace)
    {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }
}
