package com.automationanywhere.cognitive.project.exception;

public class DependentServiceConnectionFailureException extends RuntimeException{

    private static final long serialVersionUID = -5261742052085197610L;

    public DependentServiceConnectionFailureException() {
        super();
    }

    public DependentServiceConnectionFailureException(String message) {
        super(message);
    }

}