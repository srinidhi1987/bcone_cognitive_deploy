package com.automationanywhere.cognitive.project.exception;

public class VisionBotUpdationFailedException extends Exception{

  private static final long serialVersionUID = -4149763097591435752L;

  public VisionBotUpdationFailedException(String message){super(message);}
  public VisionBotUpdationFailedException(String message,Exception ex){super(message,ex);}
}
