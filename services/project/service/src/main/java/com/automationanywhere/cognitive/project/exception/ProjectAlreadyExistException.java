package com.automationanywhere.cognitive.project.exception;

public class ProjectAlreadyExistException extends RuntimeException {

  private static final long serialVersionUID = -7925221916558937344L;

  public ProjectAlreadyExistException(String message) {
    super(message);
  }
}
