package com.automationanywhere.cognitive.project.exception;

public class MQConnectionFailureException extends RuntimeException{

    private static final long serialVersionUID = -5484439292735366805L;

    public MQConnectionFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}