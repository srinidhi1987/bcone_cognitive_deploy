package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.TestSetInfoDao;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.model.dao.TestSetInfo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class TestSetInfoDaoImpl implements
    TestSetInfoDao {

  private AALogger log = AALogger.create(TestSetInfoDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String> getTestSetInfoIds(List<String> visionBotDetailIds) {
    log.entry();

    String sqlQuery = "select id from TestSetInfo where visionbotid in (:visionBotDetailIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("visionBotDetailIds", visionBotDetailIds);
    @SuppressWarnings("unchecked")
    List<Object> resulSetData = query.getResultList();

    List<String> testSetInfoIds = dataTransformer.transformToListOfString(resulSetData);

    log.exit();
    return testSetInfoIds;
  }

  @Override
  public List<String[]> getTestSetInfos(List<String> visionBotDetailIds) {
    log.entry();
    Session session = sessionFactory.getCurrentSession();
    session.clear();

    String sqlQuery = "select * from TestSetInfo where visionbotid in (:visionBotDetailIds)";
    Query query = session.createNativeQuery(sqlQuery);
    query.setParameterList("visionBotDetailIds", visionBotDetailIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> testSetInfos = dataTransformer
        .transformToListOfStringArray(resulSetData);

    session.flush();

    log.exit();
    return testSetInfos;
  }

  @Override
  public void batchInsertTestSetInfo(List<TestSetInfo> testSetInfoList)  {
    Session session = sessionFactory.getCurrentSession();
    for (TestSetInfo testSetInfo : testSetInfoList) {
      session.saveOrUpdate(testSetInfo);
      int index = testSetInfoList.indexOf(testSetInfo);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
    }
  }

}
