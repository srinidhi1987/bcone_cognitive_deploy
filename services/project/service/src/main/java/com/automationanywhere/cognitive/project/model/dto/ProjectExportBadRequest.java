package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.exception.ProjectExportException;

import java.util.ArrayList;
import java.util.List;

public class ProjectExportBadRequest {

    private List<ProjectExportDeclination> projectExportDeclinations;

    public static ProjectExportBadRequest withException(ProjectExportException projectExportException) {
        ProjectExportBadRequest projectExportBadRequest = new ProjectExportBadRequest();
        if (projectExportException.getProjectIdsNotFound() != null && projectExportException.getProjectIdsNotFound().size() > 0) {
            projectExportBadRequest.projectExportDeclinations = exportDeclinations(projectExportBadRequest);
            for (String prjId : projectExportException.getProjectIdsNotFound()) {
                projectExportBadRequest.projectExportDeclinations.add(new ProjectExportDeclination(prjId, "Project does not exist"));
            }
        }

        return projectExportBadRequest.projectExportDeclinations == null ? null : projectExportBadRequest;
    }

    private static List<ProjectExportDeclination> exportDeclinations(ProjectExportBadRequest projectExportBadRequest) {
        return projectExportBadRequest.projectExportDeclinations == null ? new ArrayList<>() : projectExportBadRequest.projectExportDeclinations;
    }

    public List<ProjectExportDeclination> getProjectExportDeclinations() {
        return projectExportDeclinations;
    }

    public void setProjectExportDeclinations(List<ProjectExportDeclination> projectExportDeclinations) {
        this.projectExportDeclinations = projectExportDeclinations;
    }
}
