package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.StandardFieldDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.StandardFieldDetailCsv;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class StandardFieldDetailDataImporter extends AdvanceDataImporter<StandardFieldDetailCsv> {

  private static String PROJECT_EDITED_EXCEPTION_MESSAGE = "Learning instance with id '%s' is edited";
  private final StandardFieldDetailDao standardFieldDetailDao;
  private final TransitionResult transitionResult;
  private final FileHelper fileHelper;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AALogger log = AALogger.create(StandardFieldDetailDataImporter.class);
  private List<StandardFieldDetail> dataToImport;
  private Map<String, String> currentProjectDetails;
  private AdvanceDataImporter nextDataImporter;

  @Autowired
  public StandardFieldDetailDataImporter(
      FileReader csvFileReader,
      StandardFieldDetailDao standardFieldDetailDao,
      TransitionResult transitionResult,
      FileHelper fileHelper,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.standardFieldDetailDao = standardFieldDetailDao;
    this.transitionResult = transitionResult;
    this.fileHelper = fileHelper;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, DuplicateProjectNameException, ParseException {
    log.entry();
    prepareDataForImport(unzippedFolderPath);
    switch (taskOptions) {
      case NONE:
        break;
      case CLEAR:
      case APPEND: {
        standardFieldDetailDao.addAll(dataToImport);
        break;
      }
      case MERGE: {
        List<StandardFieldDetail> existingData = standardFieldDetailDao
            .getAll(transitionResult.getProjectId());
        if (!existingData.isEmpty()) {
          throwExceptionIfProjectIsEdited(existingData);
          throwExceptionIfProjectIsEdited(dataToImport);
        } else {
          if (!transitionResult.isNewProject()) {
            throwExceptionIfProjectIsEdited(dataToImport);
          } else {
            standardFieldDetailDao.addAll(dataToImport);
          }
        }
        break;
      }
      case OVERWRITE: {
        standardFieldDetailDao.delete(transitionResult.getProjectId());
        standardFieldDetailDao.addAll(dataToImport);
        break;
      }
    }
    if (nextDataImporter != null) {
      nextDataImporter.importData(unzippedFolderPath, taskOptions);
    }
    log.exit();
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.nextDataImporter = dataImporter;
  }

  private void throwExceptionIfProjectIsEdited(List<StandardFieldDetail> dataToImport)
      throws ProjectImportException {
    if (isProjectEdited(dataToImport)) {
      throw new ProjectImportException(String
          .format(PROJECT_EDITED_EXCEPTION_MESSAGE,
              transitionResult.getProjectId()));
    }
  }

  private boolean isProjectEdited(List<StandardFieldDetail> standardFieldDataList) {
    return standardFieldDataList.stream().anyMatch(field -> field.isAddedLater());
  }

  private void prepareDataForImport(Path extractedFolderPath)
      throws IOException, ProjectImportException {
    log.entry();
    Path csvFolderPath = Paths
        .get(extractedFolderPath.toString(), transitionResult.getProjectId());

    List<StandardFieldDetailCsv> csvData = getDataFromFiles(csvFolderPath);
    dataToImport = DtoToDao.toStandardFieldDetailList(csvData);
    log.exit();
  }

  private List<StandardFieldDetailCsv> getDataFromFiles(Path csvFolderPath)
      throws IOException, ProjectImportException {
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.STANDARDFIELDDETAIL);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, StandardFieldDetailCsv.class);
  }

}
