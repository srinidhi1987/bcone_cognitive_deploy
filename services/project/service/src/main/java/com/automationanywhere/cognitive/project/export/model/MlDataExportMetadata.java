package com.automationanywhere.cognitive.project.export.model;

public class MlDataExportMetadata {

  private long trainingDataRowCount;

  public MlDataExportMetadata() {
  }

  public MlDataExportMetadata(long trainingDataRowCount) {
    this.trainingDataRowCount = trainingDataRowCount;
  }

  public long getTrainingDataRowCount() {
    return trainingDataRowCount;
  }
}
