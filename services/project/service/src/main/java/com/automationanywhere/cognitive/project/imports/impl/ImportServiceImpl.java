package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.ErrorMessageConstants;
import com.automationanywhere.cognitive.project.constants.FileVersionConstants;
import com.automationanywhere.cognitive.project.dal.DataCleaningDao;
import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.export.ActivityMonitorService;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.helper.PathHelper;
import com.automationanywhere.cognitive.project.helper.ProductReleaseFileReader;
import com.automationanywhere.cognitive.project.imports.ImportEvents;
import com.automationanywhere.cognitive.project.imports.ImportRunner;
import com.automationanywhere.cognitive.project.imports.ImportService;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.imports.ArchiveInfo;
import com.automationanywhere.cognitive.project.model.dto.imports.ProjectImportRequest;
import com.automationanywhere.cognitive.project.task.TaskService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ImportServiceImpl implements ImportService {

  private final String BACKUP_FOLDER = "BackupData";
  private final String IMPORT_EXTENSION = "iqba";
  private String importFilepath;
  private AALogger log = AALogger.create(this.getClass());

  @Autowired
  @Qualifier("outputPathBean")
  private String outputPath;

  @Autowired
  private TaskService taskService;

  @Autowired
  @Qualifier("importRunner")
  private ImportRunner importRunner;

  @Autowired
  private DataCleaningDao dataCleaningDao;

  @Autowired
  private FileHelper fileHelper;

  @Autowired
  private PathHelper pathHelper;

  private ImportEvents importEvents;

  private Task task;

  @Autowired
  private ProductReleaseFileReader productReleaseFileReader;

  @Autowired
  private ActivityMonitorService activityMonitorService;

  public ImportEvents getImportEvents() {
    if (importEvents == null) {
      importEvents = new ImportEvents() {
        @Override
        public void onError(Exception e) throws Exception {
          log.error(e);
          taskService.markAsFailed(task);
          throw e;
        }

        @Override
        public void onComplete(Task task) {
          taskService.markAsCompleted(task);
        }
      };
    }

    return importEvents;
  }

  public void setImportEvents(ImportEvents importEvents) {
    this.importEvents = importEvents;
  }

  @Override
  public Task startImport(ProjectImportRequest projectImportRequest)
      throws PreconditionFailedException, SystemBusyException {
    validateImportRequest(projectImportRequest);
    validateIqbaFileVersion(projectImportRequest);
    checkWhetherSystemIsBusy(projectImportRequest);
    TaskOptions taskOptions = TaskOptions.fromString(projectImportRequest.getOption());
    task = taskService
        .createImportTask(projectImportRequest.getUserId(), importFilepath, taskOptions);
    importData(task, getImportEvents());
    return task;
  }

  @Override
  public List<ArchiveInfo> getArchiveDetails() {
    List<ArchiveInfo> archiveInfoList = new ArrayList<>();

    String folderToScan = pathHelper.join(outputPath, BACKUP_FOLDER);
    List<String> filesInFolder = fileHelper.getAllFilesInFolder(folderToScan);
    for (String fileName : filesInFolder) {
      if (fileName.endsWith("." + IMPORT_EXTENSION)) {
        ArchiveInfo archiveInfo = new ArchiveInfo();
        archiveInfo.setArchiveName(fileName);
        archiveInfoList.add(archiveInfo);
      }
    }
    return archiveInfoList;
  }


  /**
   * Initiates import task.
   *
   * @param task task object.
   */
  private void importData(Task task, ImportEvents importEvents) {

    importRunner.setTask(task);
    importRunner.setImportEvents(importEvents);
    Thread thread = new Thread(importRunner);
    thread.start();

  }

  private void validateImportRequest(ProjectImportRequest projectImportRequest)
      throws PreconditionFailedException {
    if (projectImportRequest == null
        || projectImportRequest.getArchiveDetails() == null) {
      throw new PreconditionFailedException(ErrorMessageConstants.NO_FILES_FOR_IMPORT);
    }

    if (projectImportRequest.getOption() == null
        || TaskOptions.fromString(projectImportRequest.getOption()) == null) {
      throw new PreconditionFailedException(ErrorMessageConstants.INVALID_IMPORT_OPTION);
    }

    importFilepath = pathHelper
        .join(outputPath, BACKUP_FOLDER, projectImportRequest.getArchiveDetails().getArchiveName());
    if (!fileHelper.isFileExists(importFilepath)) {
      throw new PreconditionFailedException(ErrorMessageConstants.IMPORT_FILE_UNAVAILABLE);
    }

    if (!importFilepath.toLowerCase().endsWith(IMPORT_EXTENSION)) {
      throw new PreconditionFailedException(ErrorMessageConstants.IMPORT_FILE_EXTENTION_INVALID);
    }
  }

  private void validateIqbaFileVersion(ProjectImportRequest projectImportRequest)
      throws PreconditionFailedException {
      String version = fileHelper.readVersionFromIqbaFile(importFilepath);
      log.debug(
          String.format("The Actual file %s and version of file is %s", importFilepath, version));
      if (null == version || !version.equalsIgnoreCase(productReleaseFileReader.getCurrentProductVersion())) {
        TaskOptions taskOptions = TaskOptions.fromString(projectImportRequest.getOption());
        taskService
            .createFailedImportTask(projectImportRequest.getUserId(), importFilepath, taskOptions);
        if (null == version){ // setting default as to show proper message on UI side
          version = FileVersionConstants.PRODUCT_DEFAULT_VERSION;
        }
        throw new PreconditionFailedException(String.format(
            ErrorMessageConstants.INCOMPATIBLE_FILE_VERSION,version));
      }
  }

  private void checkWhetherSystemIsBusy(ProjectImportRequest projectImportRequest)
      throws SystemBusyException {
    //checking classification is inprogress
    if (activityMonitorService.isClassifierInProgress()) {
      makeFailedTaskEntry(projectImportRequest);
      throw new SystemBusyException(ErrorMessageConstants.CLASSIFICATION_INPROGRESS);
    }

    if (activityMonitorService.isStagingDocumentsInProgress()) {
      makeFailedTaskEntry(projectImportRequest);
      throw new SystemBusyException(ErrorMessageConstants.DATAEXTRACTION_INPROGRESS);
    }

    if (activityMonitorService.isProductionDocumentsInProgress()) {
      makeFailedTaskEntry(projectImportRequest);
      throw new SystemBusyException(
          ErrorMessageConstants.DATAEXTRACTION_INPROGRESS_FOR_PRODUCTION_DOC);
    }

    if (activityMonitorService.isBotLocked()) {
      makeFailedTaskEntry(projectImportRequest);
      throw new SystemBusyException(ErrorMessageConstants.VISION_BOT_UNDER_TRAINING);
    }
  }

  // making failed task entry
  private void makeFailedTaskEntry(ProjectImportRequest projectImportRequest) {
    TaskOptions taskOptions = TaskOptions.fromString(projectImportRequest.getOption());
    taskService
        .createFailedImportTask(projectImportRequest.getUserId(), importFilepath, taskOptions);
  }
}
