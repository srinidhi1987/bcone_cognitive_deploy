package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.model.dao.Task;

public interface ImportEvents {
    void onError(Exception e) throws Exception;
    void onComplete(Task task);
}
