package com.automationanywhere.cognitive.project.model.dao;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VisionBot")
public class VisionBot {

  @Id
  @Column(name = "id", length = 50)
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;

  @Column(name = "datablob")
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "datablob")
  private String dataBlob;

  @Column(name = "validationdatablob")
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "validationdatablob")
  private String validationDataBlob;

  @Column(name = "environment", length = 50)
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "environment")
  private String environment;

  @Column(name = "LockedUserid", length = 50)
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "LockedUserid")
  private String lockedUserId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDataBlob() {
    return dataBlob;
  }

  public void setDataBlob(String dataBlob) {
    this.dataBlob = dataBlob;
  }

  public String getValidationDataBlob() {
    return validationDataBlob;
  }

  public void setValidationDataBlob(String validationDataBlob) {
    this.validationDataBlob = validationDataBlob;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getLockedUserId() {
    return lockedUserId;
  }

  public void setLockedUserId(String lockedUserId) {
    this.lockedUserId = lockedUserId;
  }


}
