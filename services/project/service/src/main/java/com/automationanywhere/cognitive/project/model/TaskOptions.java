package com.automationanywhere.cognitive.project.model;

import java.util.HashMap;
import java.util.Map;

public enum TaskOptions {
  NONE("none"),
  CLEAR("clear"),
  APPEND("append"),
  MERGE("merge"),
  OVERWRITE("overwrite");

  private static final Map<String, TaskOptions> MAP = new HashMap<>();

  static {
    for (final TaskOptions d : TaskOptions.values()) {

      MAP.put(d.getValue(), d);
    }
  }

  private final String value;

  TaskOptions(final String value) {

    this.value = value;
  }

  public static TaskOptions fromString(final String code) {
    return MAP.get(code);
  }

  public String getValue() {

    return value;
  }
}
