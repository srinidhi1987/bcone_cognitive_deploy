package com.automationanywhere.cognitive.project.constants;

public class BulkInsertConstants {

  public static final String FIELDTERMINATOR = "\t";
  public static final String ROWTERMINATOR = "0x0a";
}
