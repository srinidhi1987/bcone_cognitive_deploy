package com.automationanywhere.cognitive.project.helper;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PatternBasedFileVisitor extends SimpleFileVisitor<Path> {

  private final PathMatcher pathMatcher;

  private final List<Path> matchedPaths = new ArrayList<Path>();

  public PatternBasedFileVisitor(String pattern) {
    pathMatcher = FileSystems.getDefault().getPathMatcher("regex:" + pattern);
  }

  public void match(Path path) {
    Path filename = path.getFileName();
    if (filename != null && pathMatcher.matches(filename)) {
      matchedPaths.add(path);
    }
  }

  @Override
  public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
      throws IOException {
    match(dir);
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
      throws IOException {
    match(file);
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFileFailed(Path file, IOException exc) {
    return FileVisitResult.CONTINUE;
  }

  public Collection<Path> getMatchedPaths() {
    return matchedPaths;
  }
}
