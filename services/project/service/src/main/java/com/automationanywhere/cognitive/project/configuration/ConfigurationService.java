package com.automationanywhere.cognitive.project.configuration;

import com.automationanywhere.cognitive.project.exception.NotFoundException;

public interface ConfigurationService {
    String getConfiguration(String key) throws NotFoundException;
}
