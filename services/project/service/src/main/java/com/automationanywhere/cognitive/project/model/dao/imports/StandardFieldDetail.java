package com.automationanywhere.cognitive.project.model.dao.imports;

import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "StandardFieldDetail")
public class StandardFieldDetail {

  @Embeddable
  public static class PrimaryKey implements Serializable {

    private static final long serialVersionUID = 6476254489050308631L;
    @Column(name = "Id", nullable = false)
    private String id;

    @Column(name = "ProjectId")
    private String projectId;

    public String getId() {
      return id;
    }

    public PrimaryKey(){}

    public PrimaryKey(String id, String projectId) {
      this.id = id;
      this.projectId = projectId;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getProjectId() {
      return projectId;
    }

    public void setProjectId(String projectId) {
      this.projectId = projectId;
    }
  }

  @EmbeddedId
  private PrimaryKey primaryKey = new PrimaryKey();

  @Column(name = "OrderNumber")
  private int order;

  public StandardFieldDetail(
      PrimaryKey primaryKey, int order, boolean isAddedLater) {
    this.primaryKey = primaryKey;
    this.order = order;
    this.isAddedLater = isAddedLater;
  }

  public StandardFieldDetail() {
  }

  @Column(name="IsAddedLater")

  private boolean isAddedLater;

  public PrimaryKey getPrimaryKey() {
    return primaryKey;
  }

  public void setPrimaryKey(PrimaryKey primaryKey) {
    this.primaryKey = primaryKey;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public boolean isAddedLater() {
    return isAddedLater;
  }

  public void setAddedLater(boolean addedLater) {
    isAddedLater = addedLater;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    StandardFieldDetail that = (StandardFieldDetail) o;

    return primaryKey.equals(that.primaryKey);
  }

  @Override
  public int hashCode() {
    return primaryKey.hashCode();
  }
}
