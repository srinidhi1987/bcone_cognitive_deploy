package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class FileBlob {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "fileid")
  private String fileId;

  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "fileblob")
  private String fileBlob;

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public String getFileBlob() {
    return fileBlob;
  }

  public void setFileBlob(String fileBlob) {
    this.fileBlob = fileBlob;
  }
}
