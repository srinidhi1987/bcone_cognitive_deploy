package com.automationanywhere.cognitive.project.adapter;

import com.automationanywhere.cognitive.project.model.dto.VisionbotData;

import java.util.List;

/**
 * Created by Jemin.Shah on 3/4/2017.
 */
public interface VisionbotAdapter
{
    List<VisionbotData> getVisionbotData(String organizationId);
    List<VisionbotData> getVisionbotData(String organizationId, String projectId);
    List<VisionbotData> getVisionbotMetadata(String organizationId, String projectId);
    void testConnection();
}
