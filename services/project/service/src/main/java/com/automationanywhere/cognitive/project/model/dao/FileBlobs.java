package com.automationanywhere.cognitive.project.model.dao;

import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FileBlobs")
public class FileBlobs {

  @Id
  @Column(name = "fileid")
  private String fileId;

  @Column(name = "fileblob")
  private Blob fileBlob;

  public FileBlobs() {
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public Blob getFileBlob() {
    return fileBlob;
  }

  public void setFileBlob(Blob fileBlob) {
    this.fileBlob = fileBlob;
  }
}
