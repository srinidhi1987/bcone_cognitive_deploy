package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.ContentTrainSet;

import java.util.List;
import java.util.Map;

public interface ContentTrainSetDao {
  List<String[]> getAllContentTrainSets();
  List<ContentTrainSet> getAll();
  Map<Integer, Integer> addAll(List<ContentTrainSet> contentTrainSetList);
  void deleteAll();
  List<String[]> getContentTrainSets(List<String> classificationIds);
}
