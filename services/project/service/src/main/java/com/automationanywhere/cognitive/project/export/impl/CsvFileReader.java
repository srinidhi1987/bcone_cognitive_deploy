package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.constants.CsvFileIOConstants;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvFileReader implements FileReader {

  @Override
  public List<String[]> readAllData(Path filePath, int skipLines)
      throws IOException {
    List<String[]> records = null;
    RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder()
        .withQuoteChar(CsvFileIOConstants.NULL_CHARACTER)
        .withSeparator(CsvFileIOConstants.SEPARATOR).build();

    try (Reader reader = Files.newBufferedReader(filePath);

        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(skipLines)
            .withCSVParser(rfc4180Parser).build();) {
      records = csvReader.readAll();

    }
    return records;
  }

  public <T> List<T> readAllData(Path filePath, Class<T> theClass, int skipLines)
      throws IOException {
    RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().withQuoteChar(CsvFileIOConstants.NULL_CHARACTER)
        .withSeparator(CsvFileIOConstants.SEPARATOR).build();
    List<T> data = new ArrayList<>();

    try (Reader reader = Files.newBufferedReader(filePath);
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(skipLines)
            .withCSVParser(rfc4180Parser).build();) {

      CustomMappingStrategy<T> strategy = new CustomMappingStrategy<T>();
      strategy.setType(theClass);

      CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(reader).withMappingStrategy(strategy).build();
      csvToBean.setCsvReader(csvReader);

      data = csvToBean.parse();
    }
    return data;

  }
}
