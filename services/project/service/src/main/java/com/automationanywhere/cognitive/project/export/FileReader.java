package com.automationanywhere.cognitive.project.export;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FileReader {

  public List<String[]> readAllData(Path filePath, int skipLines)
      throws IOException;
  public <T> List<T> readAllData(Path filePath, Class<T> theClass, int skipLines)
      throws IOException;
}
