package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.CustomFieldDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectAlreadyExistException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.CustomFieldDetailCsv;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.imports.CustomFieldDetail;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class CustomFieldDetailDataImporter extends AdvanceDataImporter<CustomFieldDetailCsv> {

  private static final AALogger log = AALogger.create(CustomFieldDetailDataImporter.class);
  private static String PROJECT_EDITED_EXCEPTION_MESSAGE = "Learning instance with id '%s' is edited";
  private final CustomFieldDetailDao customFieldDetailsDao;
  private final TransitionResult transitionResult;
  private final FileHelper fileHelper;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private List<CustomFieldDetail> dataToImport;
  private AdvanceDataImporter nextDataImporter;

  @Autowired
  public CustomFieldDetailDataImporter(
      FileReader csvFileReader,
      CustomFieldDetailDao customFieldDetailsDao,
      TransitionResult transitionResult,
      FileHelper fileHelper,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.customFieldDetailsDao = customFieldDetailsDao;
    this.transitionResult = transitionResult;
    this.fileHelper = fileHelper;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, ProjectAlreadyExistException, DuplicateProjectNameException, ParseException {
    log.entry();
    prepareDataForImport(unzippedFolderPath);

    switch (taskOptions) {
      case CLEAR:
      case APPEND: {
        customFieldDetailsDao.addAll(dataToImport);
        break;
      }
      case MERGE: {
        List<CustomFieldDetail> existingData = customFieldDetailsDao
            .getAll(transitionResult.getProjectId());
        if (!existingData.isEmpty()) {
          throwExceptionIfProjectIsEdited(existingData);
          throwExceptionIfProjectIsEdited(dataToImport);
        } else {
          if (!transitionResult.isNewProject()) {
            throwExceptionIfProjectIsEdited(dataToImport);
          } else {
            customFieldDetailsDao.addAll(dataToImport);
          }
        }
        break;
      }
      case OVERWRITE: {
        customFieldDetailsDao.delete(transitionResult.getProjectId());
        customFieldDetailsDao.addAll(dataToImport);
        break;
      }
    }
    if (nextDataImporter != null) {
      nextDataImporter.importData(unzippedFolderPath, taskOptions);
    }
    log.exit();
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.nextDataImporter = dataImporter;
  }

  private void throwExceptionIfProjectIsEdited(List<CustomFieldDetail> dataToImport)
      throws ProjectImportException {
    if (isProjectEdited(dataToImport)) {
      throw new ProjectImportException(String
          .format(PROJECT_EDITED_EXCEPTION_MESSAGE,
              transitionResult.getProjectId()));
    }
  }

  private boolean isProjectEdited(List<CustomFieldDetail> customFieldDetailList) {
    return customFieldDetailList.stream().anyMatch(field -> field.isAddedLater());
  }

  private void prepareDataForImport(Path extractedFolderPath)
      throws IOException, ProjectImportException {
    log.entry();
    Path csvFolderPath = Paths
        .get(extractedFolderPath.toString(), transitionResult.getProjectId());

    List<CustomFieldDetailCsv> csvData = getDataFromFiles(csvFolderPath);
    dataToImport = DtoToDao.toCustomFieldDetailList(csvData);
    log.exit();
  }

  private List<CustomFieldDetailCsv> getDataFromFiles(Path csvFolderPath)
      throws IOException, ProjectImportException {
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.CUSTOMFIELDDETAIL);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, CustomFieldDetailCsv.class);
  }

}
