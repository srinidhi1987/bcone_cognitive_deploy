package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.model.dao.Task;
import org.springframework.beans.factory.annotation.Autowired;

public class ImportRunner implements Runnable {

  private AALogger logger = AALogger.create(this.getClass());

  @Autowired
  ImportManager importManager;

  Task task;
  ImportEvents importEvents;

  public void setTask(Task task) {
    this.task = task;
  }

  public void setImportEvents(ImportEvents importEvents) {
    this.importEvents = importEvents;
  }

  @Override
  public void run() {
    logger.entry();
    try {
      importManager.run(task, importEvents);
    } catch (Exception e) {
      logger.error(e);
    }
    finally {
      logger.exit();
    }

    }
}
