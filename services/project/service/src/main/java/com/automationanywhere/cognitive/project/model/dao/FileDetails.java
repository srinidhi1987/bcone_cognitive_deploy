package com.automationanywhere.cognitive.project.model.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FileDetails")
public class FileDetails {

  @Id
  @Column(name = "fileid")
  private String fileId;
  @Column(name = "projectid")
  private String projectId;
  @Column(name = "filename")
  private String fileName;
  @Column(name = "filelocation")
  private String fileLocation;
  @Column(name = "filesize")
  private long fileSize;
  @Column(name = "fileheight")
  private int fileHeight;
  @Column(name = "filewidth")
  private int fileWidth;
  @Column(name = "format")
  private String format;
  @Column(name = "processed")
  private boolean processed;
  @Column(name = "classificationid")
  private String classificationId;
  @Column(name = "uploadrequestid")
  private String uploadrequestId;
  @Column(name = "layoutid")
  private String layoutId;
  @Column(name = "isproduction")
  private boolean isProduction;

  public FileDetails() {
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFileLocation() {
    return fileLocation;
  }

  public void setFileLocation(String fileLocation) {
    this.fileLocation = fileLocation;
  }

  public long getFileSize() {
    return fileSize;
  }

  public void setFileSize(long fileSize) {
    this.fileSize = fileSize;
  }

  public int getFileHeight() {
    return fileHeight;
  }

  public void setFileHeight(int fileHeight) {
    this.fileHeight = fileHeight;
  }

  public int getFileWidth() {
    return fileWidth;
  }

  public void setFileWidth(int fileWidth) {
    this.fileWidth = fileWidth;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public boolean isProcessed() {
    return processed;
  }

  public void setProcessed(boolean processed) {
    this.processed = processed;
  }

  public String getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public String getUploadrequestId() {
    return uploadrequestId;
  }

  public void setUploadrequestId(String uploadrequestId) {
    this.uploadrequestId = uploadrequestId;
  }

  public String getLayoutId() {
    return layoutId;
  }

  public void setLayoutId(String layoutId) {
    this.layoutId = layoutId;
  }

  public boolean isProduction() {
    return isProduction;
  }

  public void setProduction(boolean production) {
    isProduction = production;
  }
}