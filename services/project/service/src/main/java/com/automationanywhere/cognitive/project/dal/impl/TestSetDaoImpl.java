package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.TestSetDao;
import com.automationanywhere.cognitive.project.model.dao.TestSet;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class TestSetDaoImpl implements
    TestSetDao {

  private AALogger log = AALogger.create(TestSetDaoImpl.class);
  private static final int BATCH_SIZE = 20;

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;
  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getTestSets(List<String> testInfoIds) {
    log.entry();

    String sqlQuery = "select * from TestSet where testsetinfoid in (:testInfoIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("testInfoIds", testInfoIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> testSets = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return testSets;
  }

  @Override
  public void batchInsert(List<TestSet> testSetList) {
    Session session = sessionFactory.getCurrentSession();
    for (TestSet testSet : testSetList) {
      session.save(testSet);
      int index = testSetList.indexOf(testSet);
      if (index % BATCH_SIZE == 0) {
        session.flush();
        session.clear();
      }
    }
  }

  @Override
  public void delete(List<String> testInfoIds) {
    log.entry();
    String hqlQueryString = "delete TestSet where testsetinfoid in (:testInfoIds)";
    Query query = sessionFactory.getCurrentSession().createQuery(hqlQueryString);
    query.setParameterList("testInfoIds", testInfoIds);
    query.executeUpdate();
    log.exit();
  }


}
