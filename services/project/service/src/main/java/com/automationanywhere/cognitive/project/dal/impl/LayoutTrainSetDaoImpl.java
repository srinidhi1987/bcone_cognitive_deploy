package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.LayoutTrainSetDao;
import com.automationanywhere.cognitive.project.model.dao.LayoutTrainSet;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class LayoutTrainSetDaoImpl implements
    LayoutTrainSetDao {

  private AALogger log = AALogger.create(LayoutTrainSetDaoImpl.class);

  @Autowired
  @Qualifier("classifierSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getAllLayoutTrainSet() {
    log.entry();

    String sqlQuery = "select * from LayoutTrainSet";

    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);

    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> LayoutTrainSet = dataTransformer.transformToListOfStringArray(resulSetData);
    log.exit();
    return LayoutTrainSet;
  }

  @Override
  public List<LayoutTrainSet> getAll() {
    return sessionFactory.getCurrentSession()
        .createQuery("from LayoutTrainSet", LayoutTrainSet.class).list();
  }

  @Override
  public void addAll(List<LayoutTrainSet> layoutTrainSetList) {
    Session session = sessionFactory.getCurrentSession();
    for (LayoutTrainSet layoutTrainSet : layoutTrainSetList) {
      session.save(layoutTrainSet);
    }
  }

  @Override
  public void deleteAll() {
    Query query = sessionFactory.getCurrentSession().createQuery("delete LayoutTrainSet");
    query.executeUpdate();
  }
  @Override
  public List<String[]> getLayoutTrainSet(List<String> classnames) {
    Query query = sessionFactory.getCurrentSession()
        .createNativeQuery("select * from LayoutTrainSet where classname in (:classnames)");
    query.setParameterList("classnames", classnames);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();
    List<String[]> layoutTrainsetData = dataTransformer
        .transformToListOfStringArray(resulSetData);
    return layoutTrainsetData;
  }
}
