package com.automationanywhere.cognitive.project.model;

/**
 * Created by Jemin.Shah on 24-11-2016.
 */
public enum ProjectTypes {
    invoice,
    ledger
}
