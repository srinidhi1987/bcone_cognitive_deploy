package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.project.constants.ImportFilePatternConstant;
import com.automationanywhere.cognitive.project.constants.TableConstants;

public class ImportFilePatternConstructor {

  public String getRegexPatternWithOr(TableConstants tableConstant) {
    return tableConstant.getName() + ImportFilePatternConstant.CSV_FILE_WITH_SEQUENCE_NUMBER.getName() + "|"
        + tableConstant.getName() + ImportFilePatternConstant.CSV_FILE_WITH_NAME.getName();
  }

  public String getRegexPattern(TableConstants tableConstant) {
    return tableConstant.getName() + ImportFilePatternConstant.CSV_FILE_WITH_NAME.getName();
  }
}
