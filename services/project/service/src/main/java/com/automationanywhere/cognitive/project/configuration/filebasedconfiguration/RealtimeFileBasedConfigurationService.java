package com.automationanywhere.cognitive.project.configuration.filebasedconfiguration;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.configuration.ConfigurationService;
import com.automationanywhere.cognitive.project.configuration.FileChangedEvent;
import com.automationanywhere.cognitive.project.configuration.ConfigurationReader;
import com.automationanywhere.cognitive.project.exception.NotFoundException;

import java.util.HashMap;
import java.util.Map;

public class RealtimeFileBasedConfigurationService implements ConfigurationService
{
    private Map<String, String> configurationMap;

    AALogger log = AALogger.create(this.getClass());

    private ConfigurationReader propertiesFileBasedConfigurations;

    private FileWatcher fileWatcher;

    public RealtimeFileBasedConfigurationService(ConfigurationReader propertiesFileBasedConfigurations, FileWatcher fileWatcher)
            throws NotFoundException {
        configurationMap = new HashMap<>();
        this.fileWatcher = fileWatcher;
        this.propertiesFileBasedConfigurations = propertiesFileBasedConfigurations;
        doOnChange();
        startWatcher();
    }

    private void startWatcher() {
        fileWatcher.fileChangedEvent = new FileChangedEvent() {
            @Override
            public void fileChanged(String filepath) {
                try {
                    doOnChange();
                } catch (NotFoundException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        };
        Thread settingFileWatcherThread = new Thread(fileWatcher);
        settingFileWatcherThread.start();
    }


    private void doOnChange() throws NotFoundException {
        configurationMap = propertiesFileBasedConfigurations.getProperties();
    }

    @Override
    public String getConfiguration(String key) throws NotFoundException {
        if(configurationMap.containsKey(key))
        {
            return configurationMap.get(key);
        }

        throw new NotFoundException("Configuration key \"" + key + "\" is not found");
    }
}
