package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.ContentTrainSetDao;
import com.automationanywhere.cognitive.project.model.dao.ContentTrainSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ContentTrainSetDaoImpl implements
    ContentTrainSetDao {

  private AALogger log = AALogger.create(ContentTrainSetDaoImpl.class);

  @Autowired
  @Qualifier("classifierSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getAllContentTrainSets() {
    log.entry();

    String sqlQuery = "select * from ContentTrainSet";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);

    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> contentTrainSets = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return contentTrainSets;
  }

  @Override
  public List<ContentTrainSet> getAll() {
    return sessionFactory.getCurrentSession()
        .createQuery("from ContentTrainSet", ContentTrainSet.class).list();
  }

  @Override
  public Map<Integer, Integer> addAll(List<ContentTrainSet> contentTrainSetList) {
    Map<Integer, Integer> updatedClasses = new HashMap<>();

    for (ContentTrainSet contentTrainSet : contentTrainSetList) {
      int oldId = contentTrainSet.getId();
      sessionFactory.getCurrentSession().save(contentTrainSet);
      int newClass = contentTrainSet.getId();
      updatedClasses.put(oldId, newClass);
      log.debug(String.format("Class id %d is added with new class id %d", oldId, newClass));
    }

    return updatedClasses;
  }

  @Override
  public void deleteAll() {
    Query query = sessionFactory.getCurrentSession().createQuery("delete ContentTrainSet");
    query.executeUpdate();
  }

  @Override
  public List<String[]> getContentTrainSets(List<String> classificationIds) {
    Query query = sessionFactory.getCurrentSession()
        .createNativeQuery("select * from ContentTrainSet where id in (:classificationIds)");
    query.setParameterList("classificationIds", classificationIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();
    List<String[]> contentTrainSets = dataTransformer
        .transformToListOfStringArray(resulSetData);
    return contentTrainSets;
  }
}
