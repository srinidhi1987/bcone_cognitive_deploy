package com.automationanywhere.cognitive.project.model.dao;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TestSet")
public class TestSet {

  @Id
  @Column(name = "id", length = 50)
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;

  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "name")
  @Column(name = "name", length = 50)
  private String name;

  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "docitems")
  @Column(name = "docitems")
  private String docitems;

  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "testsetInfoId")
  @Column(name = "testsetinfoid", length = 50)
  private String testsetInfoId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDocitems() {
    return docitems;
  }

  public void setDocitems(String docitems) {
    this.docitems = docitems;
  }

  public String getTestsetInfoId() {
    return testsetInfoId;
  }

  public void setTestsetInfoId(String testsetInfoId) {
    this.testsetInfoId = testsetInfoId;
  }
}
