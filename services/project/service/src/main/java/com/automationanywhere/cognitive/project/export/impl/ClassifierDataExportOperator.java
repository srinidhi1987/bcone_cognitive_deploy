package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.Util.DataUtil;
import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExportListener;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.ExportOperator;
import com.automationanywhere.cognitive.project.export.model.ClassifierDataExportMetadata;
import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import javax.inject.Inject;


public class ClassifierDataExportOperator implements ExportOperator {

  private static final ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();

  private final DataExporter classifierDataExporter;

  private final FileDetailsDao fileDetailsDao;

  private final String CLASSIFICATIONID_PROPERTY = "classificationids";
  private final String LAYOUTID_PROPERTY = "layoutids";

  @Inject
  public ClassifierDataExportOperator(DataExporter classifierDataExporter,
      FileDetailsDao fileDetailsDao) {
    this.classifierDataExporter = classifierDataExporter;
    this.fileDetailsDao = fileDetailsDao;
  }

  @Override
  public void export(String dirPath, String[] projectIds,
      DataExportListener dataExportListener) {
    forkJoinPool.execute(new ClassifierDataExportTask(dirPath, projectIds, dataExportListener));
  }


  private class ClassifierDataExportTask extends RecursiveTask<List<ClassifierDataExportMetadata>> {

    private static final long serialVersionUID = -8806868187799771547L;
    private static final int PROJECT_NUMBER_THRESHOLD = 10;
    private final String dirPath;
    private final String[] projectIds;
    private DataExportListener dataExportListener;

    ClassifierDataExportTask(String dirPath, String[] projectIds,
        DataExportListener dataExportListener) {
      this(dirPath, projectIds);
      this.dataExportListener = dataExportListener;
    }

    private ClassifierDataExportTask(String dirPath, String[] projectIds) {
      this.dirPath = dirPath;
      this.projectIds = projectIds;
    }

    @Override
    public List<ClassifierDataExportMetadata> compute() {
      try {
        List<ClassifierDataExportMetadata> metadata;
        metadata = new ArrayList<>();
        metadata.add(exportData(Arrays.asList(projectIds)));

        if (dataExportListener != null) {
          dataExportListener.onClassifierDataExportCompleted(metadata);
        }
        return metadata;
      } catch (Exception e) {
        if (dataExportListener != null) {
          dataExportListener.onException(e);
        }
        throw e;
      }
    }

    private List<ClassifierDataExportTask> createSubTasks() {
      List<String[]> lstOfArrayOfProjectIds = DataUtil
          .subDivide(projectIds, PROJECT_NUMBER_THRESHOLD);

      List<ClassifierDataExportTask> subTasks = new ArrayList<>();

      lstOfArrayOfProjectIds.stream().forEach(
          subProjectIds -> subTasks.add(new ClassifierDataExportTask(dirPath, subProjectIds)));

      return subTasks;
    }

    private ClassifierDataExportMetadata exportData(List<String> projectIds)
        throws ProjectExportException {
      Map<Object, Object> exportParams = new HashMap<Object, Object>();

      List<FileDetails> lstOfFileDetails = fileDetailsDao.getFileDetails(projectIds);
      List<String> classificationIds = lstOfFileDetails.stream()
          .map(fileDetail -> fileDetail.getClassificationId()).distinct()
          .collect(Collectors.toList());
      List<String> layoutids = lstOfFileDetails.stream().map(fileDetail -> fileDetail.getLayoutId())
          .distinct()
          .collect(Collectors.toList());

      
      exportParams.put(CLASSIFICATIONID_PROPERTY, classificationIds);
      exportParams.put(LAYOUTID_PROPERTY, layoutids);

      @SuppressWarnings("unchecked")
      ClassifierDataExportMetadata classifierDataExportMetadata = (ClassifierDataExportMetadata) classifierDataExporter
          .exportData(dirPath, exportParams);

      return classifierDataExportMetadata;
    }
  }
}
