package com.automationanywhere.cognitive.project.export.model;

public class FileDataExportMetadata {

  private long fileDetailsRowCount;
  private long fileBlobRowCount;
  private long classificationReportRowCount;

  public FileDataExportMetadata() {
  }

  public FileDataExportMetadata(long fileDetailsRowCount, long fileBlobRowCount,
      long classificationReportRowCount) {
    this.fileDetailsRowCount = fileDetailsRowCount;
    this.fileBlobRowCount = fileBlobRowCount;
    this.classificationReportRowCount = classificationReportRowCount;
  }

  public long getFileDetailsRowCount() {
    return fileDetailsRowCount;
  }

  public long getFileBlobRowCount() {
    return fileBlobRowCount;
  }

  public long getClassificationReportRowCount() {
    return classificationReportRowCount;
  }
}
