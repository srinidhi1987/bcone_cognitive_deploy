package com.automationanywhere.cognitive.project.dal;

/**
 * Created by Jemin.Shah on 4/6/2017.
 */
public enum FilterParameters
{
    limit,
    offset,
    orderby,
    id,
    name,
    environment
}
