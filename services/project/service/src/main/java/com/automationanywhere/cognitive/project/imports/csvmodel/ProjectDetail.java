package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class ProjectDetail {
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "Id")
  private String id;
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "Name")
  private String name;
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "Description")
  private String description;
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "OrganizationId")
  private String organizationId;
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "Type")
  private String type;
  @CsvBindByPosition(position = 5)
  @CsvBindByName(column = "Typeid")
  private String typeid;
  @CsvBindByPosition(position = 6)
  @CsvBindByName(column = "PrimaryLanguage")
  private String primaryLanguage;
  @CsvBindByPosition(position = 7)
  @CsvBindByName(column = "State")
  private String state;
  @CsvBindByPosition(position = 8)
  @CsvBindByName(column = "Environment")
  private String environment;
  @CsvBindByPosition(position = 9)
  @CsvBindByName(column = "FileUploadToken")
  private String fileUploadToken;
  @CsvBindByPosition(position = 10)
  @CsvBindByName(column = "FileUploadTokenAlive")
  private String fileUploadTokenAlive;
  @CsvBindByPosition(position = 11)
  @CsvBindByName(column = "CreatedBy")
  private String createdBy;
  @CsvBindByPosition(position = 12)
  @CsvBindByName(column = "LastUpdatedBy")
  private String lastUpdatedBy;
  @CsvBindByPosition(position = 13)
  @CsvBindByName(column = "CreatedAt")
  private String createdAt;
  @CsvBindByPosition(position =14 )
  @CsvBindByName(column = "UpdatedAt")
  private String updatedAt;
  @CsvBindByPosition(position =15 )
  @CsvBindByName(column = "VersionId")
  private long versionId;

  public ProjectDetail(){

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTypeid() {
    return typeid;
  }

  public void setTypeid(String typeid) {
    this.typeid = typeid;
  }

  public String getPrimaryLanguage() {
    return primaryLanguage;
  }

  public void setPrimaryLanguage(String primaryLanguage) {
    this.primaryLanguage = primaryLanguage;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getFileUploadToken() {
    return fileUploadToken;
  }

  public void setFileUploadToken(String fileUploadToken) {
    this.fileUploadToken = fileUploadToken;
  }

  public String getFileUploadTokenAlive() {
    return fileUploadTokenAlive;
  }

  public void setFileUploadTokenAlive(String fileUploadTokenAlive) {
    this.fileUploadTokenAlive = fileUploadTokenAlive;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public long getVersionId() {
    return versionId;
  }

  public void setVersionId(long versionId) {
    this.versionId = versionId;
  }

  @Override
  public String toString() {
    return "ProjectDetail{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", organizationId='" + organizationId + '\'' +
        ", type='" + type + '\'' +
        ", typeid='" + typeid + '\'' +
        ", primaryLanguage='" + primaryLanguage + '\'' +
        ", state='" + state + '\'' +
        ", environment='" + environment + '\'' +
        ", fileUploadToken='" + fileUploadToken + '\'' +
        ", fileUploadTokenAlive='" + fileUploadTokenAlive + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
        ", createdAt='" + createdAt + '\'' +
        ", updatedAt='" + updatedAt + '\'' +
        ", versionId='" + versionId + '\'' +
        '}';
  }
}
