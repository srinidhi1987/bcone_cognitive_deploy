package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.FileBlobs;
import java.util.List;
import org.hibernate.SessionFactory;

public interface FileBlobDao {
  public List<String[]> getFileBlobs(List<String> fileIds);
  public void batchInsertFileBlobs(List<FileBlobs> fileBlobsList);
  public SessionFactory getSessionFactory();
}
