package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.TrainingData;
import java.util.List;

public interface TrainingDataDao {
  public List<String[]> getTrainingDatas(String projectId);
  public void deleteAll();
  public void batchInsertTrainingData(List<TrainingData> trainingDataList);
  public void delete(String projectId);
  public List<String> getUniqueFields();
  public List<String> getUniqueFields(String projectId);
}
