package com.automationanywhere.cognitive.project.health.connections;

import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;;

public interface HealthCheckConnection {
	void checkConnection();
	List<ServiceConnectivity> checkConnectionForService() ;
}
