package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.VisionbotRunDetailsDao;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class VisionbotRunDetailsDaoImpl implements
    VisionbotRunDetailsDao {

  private AALogger log = AALogger.create(VisionbotRunDetailsDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String> getVisionbotRunDetailsIds(List<String> visionBotDetailIds) {
    log.entry();

    String sqlQuery = "select id from VisionBotRunDetails where visionbotid in (:visionBotDetailIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("visionBotDetailIds", visionBotDetailIds);
    @SuppressWarnings("unchecked")
    List<Object> resulSetData = query.getResultList();
    List<String> visionbotRunDetailsIds = dataTransformer.transformToListOfString(resulSetData);
    log.exit();
    return visionbotRunDetailsIds;
  }

  @Override
  public List<String[]> getVisionbotRunDetails(List<String> visionBotDetailIds) {
    log.entry();

    String sqlQuery = "select * from VisionBotRunDetails where visionbotid in (:visionBotDetailIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("visionBotDetailIds", visionBotDetailIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> visionBotRunDetails = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return visionBotRunDetails;
  }
  @Override
  public void batchInsertVisionBotRunDetails(List<VisionBotRunDetails> visionBotRunDetailsList)  {
    Session session = sessionFactory.getCurrentSession();
    for (VisionBotRunDetails visionBotRunDetails : visionBotRunDetailsList) {
      session.saveOrUpdate(visionBotRunDetails);
      int index = visionBotRunDetailsList.indexOf(visionBotRunDetails);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
    }
  }

  @Override
  public void delete(List<String> visionBotDetailIds) {
    log.entry();
    String hqlQueryString = "delete VisionBotRunDetails where visionbotid in (:visionBotDetailIds)";
    Query query = sessionFactory.getCurrentSession().createQuery(hqlQueryString);
    query.setParameterList("visionBotDetailIds", visionBotDetailIds);
    query.executeUpdate();
    log.exit();
  }
}
