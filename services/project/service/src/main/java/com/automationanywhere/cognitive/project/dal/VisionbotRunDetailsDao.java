package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails;
import java.util.List;

public interface VisionbotRunDetailsDao {

  public List<String> getVisionbotRunDetailsIds(List<String> visionBotDetailIds);

  public List<String[]> getVisionbotRunDetails(List<String> visionBotDetailIds);

  public void batchInsertVisionBotRunDetails(List<VisionBotRunDetails> visionBotRunDetailsList);

  public void delete(List<String> visionBotDetailIds);
}
