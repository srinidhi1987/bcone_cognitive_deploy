package com.automationanywhere.cognitive.project.export.model;

public class ClassifierDataExportMetadata {
  private long contentTrainSetRowCount;
  private long layoutTrainSetRowCount;

  public ClassifierDataExportMetadata(){

  }
  public ClassifierDataExportMetadata(long contentTrainSetRowCount, long layoutTrainSetRowCount) {
    this.contentTrainSetRowCount = contentTrainSetRowCount;
    this.layoutTrainSetRowCount = layoutTrainSetRowCount;
  }

  public long getContentTrainSetRowCount() {
    return contentTrainSetRowCount;
  }

  public long getLayoutTrainSetRowCount() {
    return layoutTrainSetRowCount;
  }
}
