package com.automationanywhere.cognitive.project.model.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ProjectOCREngineDetailsMasterMapping")
public class ProjectOCREngineDetailsMasterMapping {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private int id;

  @Column(name = "ProjectId", length = 50)
  private String projectId;

  @Column(name = "OCREngineDetailsId")
  private int ocrEngineDetailsId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public int getOcrEngineDetailsId() {
    return ocrEngineDetailsId;
  }

  public void setOcrEngineDetailsId(int ocrEngineDetailsId) {
    this.ocrEngineDetailsId = ocrEngineDetailsId;
  }
}
