package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectAlreadyExistException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailsModel;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ProjectDetailDataImporterImpl extends AdvanceDataImporter<ProjectDetail> {

  private static final AALogger log = AALogger.create(ProjectDetailDataImporterImpl.class);
  private static String PROJECT_EXIST_EXCEPTION = "Learning Instance with id '%s' already present";
  private static String DUPLICATE_PROJECT_NAME_EXCEPTION = "Learning Instance name '%s' already present";
  private static String NO_PROJECT_DATA_EXCEPTION = "No project data available";
  private final ProjectDetailDao projectDetailDao;
  private final TransitionResult transitionResult;
  private final FileHelper fileHelper;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private ProjectDetailsModel dataToImport;
  private ProjectDetailsModel existingProjectDetails;

  private Path extractedFolderPath;

  private AdvanceDataImporter nextDataImporter;

  @Autowired
  public ProjectDetailDataImporterImpl(
      FileReader fileReader,
      ProjectDetailDao projectDetailDao,
      TransitionResult transitionResult,
      FileHelper fileHelper,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(fileReader);
    this.projectDetailDao = projectDetailDao;
    this.transitionResult = transitionResult;
    this.fileHelper = fileHelper;
    this.importFilePatternConstructor = importFilePatternConstructor;
    this.extractedFolderPath = extractedFolderPath;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectAlreadyExistException, DuplicateProjectNameException, ProjectImportException, ParseException {
    log.entry();
    try {
      prepareDataForImport(unzippedFolderPath);
      switch (taskOptions) {
        case CLEAR: {
          //Clean existing data and then import data.
          projectDetailDao.add(dataToImport);
          break;
        }
        case APPEND: {
          //Validate existing learning instance if same LI present, skip that LI and import remaining.
          validateData(taskOptions);
          projectDetailDao.add(dataToImport);
          break;
        }
        case MERGE: {
          //Validate exisiting learning instance and allow to import if same LI present and addAll remaining LI
          validateData(taskOptions);
          if (!projectExists()) {
            projectDetailDao.add(dataToImport);
          } else {
            //Set project state in transition result for future use.
            transitionResult.setNewProject(false);
          }
          break;
        }
        case OVERWRITE: {
          validateData(taskOptions);
          setProjectVersionId();
          projectDetailDao.updateProjectDetail(dataToImport);
          break;
        }
      }
      if (nextDataImporter != null) {
        nextDataImporter.importData(unzippedFolderPath, taskOptions);
      }
    } finally {
      existingProjectDetails = null;
      log.exit();
    }
  }

  @Override
  public void setNext(AdvanceDataImporter nextDataImporter) {
    this.nextDataImporter = nextDataImporter;
  }

  private List<ProjectDetail> getDataFromFiles(Path csvFolderPath)
      throws IOException, ProjectImportException {
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.PROJECTDETAIL);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, ProjectDetail.class);
  }

  private void prepareDataForImport(Path extractedFolderPath)
      throws IOException, ProjectImportException, ParseException {
    log.entry();
    Path csvFolderPath = Paths
        .get(extractedFolderPath.toString(), transitionResult.getProjectId());

    List<ProjectDetailsModel> csvData = DtoToDao
        .toProjectDetailsList(getDataFromFiles(csvFolderPath));
    Optional<ProjectDetailsModel> projectDetailsModel = csvData.stream()
        .filter(p -> p.getId().equals(transitionResult.getProjectId()))
        .findFirst();
    if (projectDetailsModel.isPresent()) {
      dataToImport = projectDetailsModel.get();
      //Need to update 'createdBy' with new user before importing to database
      dataToImport.setCreatedBy(transitionResult.getUserId());
    }
    if (dataToImport == null) {
      throw new ProjectImportException(NO_PROJECT_DATA_EXCEPTION);
    }
    log.exit();
  }

  private void validateData(TaskOptions taskOptions)
      throws ProjectAlreadyExistException, DuplicateProjectNameException {
    log.entry();
    if (taskOptions == TaskOptions.APPEND) {
      if (projectExists()) {
        throw new ProjectAlreadyExistException(
            String.format(PROJECT_EXIST_EXCEPTION, dataToImport.getId()));
      }
      if (projectNameExists()) {
        throw new DuplicateProjectNameException(
            String.format(DUPLICATE_PROJECT_NAME_EXCEPTION, dataToImport.getName()));
      }
    } else if (taskOptions == TaskOptions.MERGE || taskOptions == TaskOptions.OVERWRITE) {
      if (!projectExists()) {
        if (projectNameExists()) {
          throw new DuplicateProjectNameException(
              String.format(DUPLICATE_PROJECT_NAME_EXCEPTION, dataToImport.getName()));
        }
      }
    }
    log.exit();
  }

  private boolean projectExists() {
    boolean isProjectDetailExist = true;
    try {
      existingProjectDetails = projectDetailDao.getProjectDetailModel(dataToImport.getId());
    } catch (NoResultException ex) {
      isProjectDetailExist = false;
    }
    return isProjectDetailExist;
  }

  private boolean projectNameExists() {
    return projectDetailDao.projectNameExists(dataToImport.getName());
  }

  private void setProjectVersionId() {
    if (null != existingProjectDetails) {
      dataToImport.setVersionId(existingProjectDetails.getVersionId());
    }
  }
}


