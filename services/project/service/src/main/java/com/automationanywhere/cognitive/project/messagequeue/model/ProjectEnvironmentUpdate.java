package com.automationanywhere.cognitive.project.messagequeue.model;

import com.automationanywhere.cognitive.project.model.Environment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Created by Jemin.Shah on 3/16/2017.
 */
@JsonInclude(Include.NON_NULL)
public class ProjectEnvironmentUpdate
{
    private String organizationId;
    private String projectId;
    @Enumerated(EnumType.STRING)
    private Environment environment;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
