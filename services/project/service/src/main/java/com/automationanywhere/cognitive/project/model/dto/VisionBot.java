package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.model.Environment;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Jemin.Shah on 29-11-2016.
 */
public class VisionBot
{
    private String id;
    private String name;
    private String currentState;
    @JsonIgnore
    private Environment environment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public String toString() {
        return "VisionBot{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", currentState='" + currentState + '\'' +
                ", environment=" + environment +
                '}';
    }
}
