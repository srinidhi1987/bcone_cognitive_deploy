package com.automationanywhere.cognitive.project.model.dto;

import java.util.Set;

import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.ProjectState;
import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;

/**
 * Created by Jemin.Shah on 02-12-2016.
 */
public class ProjectRequest
{
    private String id;
    private String name;
    private String description;
    private String organizationId;
    private String projectType;
    private String projectTypeId;
    private String primaryLanguage;
    private Fields fields;
    private ProjectState projectState;
    private Environment environment;
    private long versionId;

    private Set<OCREngineDetails> ocrEngineDetails;
   
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectTypeId() {
        return projectTypeId;
    }

    public void setProjectTypeId(String projectTypeId) {
        this.projectTypeId = projectTypeId;
    }

    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public ProjectState getProjectState() {
        return projectState;
    }

    public void setProjectState(ProjectState projectState) {
        this.projectState = projectState;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public long getVersionId() {
        return versionId;
    }

    public void setVersionId(long versionId) {
        this.versionId = versionId;
    }

    @Override
    public String toString() {
        return "ProjectRequest{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", organizationId='" + organizationId + '\'' +
                ", projectType='" + projectType + '\'' +
                ", projectTypeId='" + projectTypeId + '\'' +
                ", primaryLanguage='" + primaryLanguage + '\'' +
                ", fields=" + fields +
                ", projectState=" + projectState +
                ", environment=" + environment +
                ", ocrEngineDetails=" + ocrEngineDetails +
                ", versionId=" + versionId +
                '}';
    }

    /**
     * @return the ocrEngineDetails
     */
    public Set<OCREngineDetails> getOcrEngineDetails() {
        return ocrEngineDetails;
    }

    /**
     * @param ocrEngineDetails the ocrDetails to set
     */
    public void setOcrEngineDetails(Set<OCREngineDetails> ocrEngineDetails) {
        this.ocrEngineDetails = ocrEngineDetails;
    }
}
