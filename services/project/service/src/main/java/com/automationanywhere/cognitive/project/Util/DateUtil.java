package com.automationanywhere.cognitive.project.Util;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Created by Jemin.Shah on 3/8/2017.
 */
public class DateUtil
{
    public static Date now()
    {
        return Date.from(ZonedDateTime.now(ZoneOffset.UTC).toInstant());
    }
}
