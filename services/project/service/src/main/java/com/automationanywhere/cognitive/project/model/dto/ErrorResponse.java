package com.automationanywhere.cognitive.project.model.dto;

/**
 * Created by Jemin.Shah on 30-11-2016.
 */
public class ErrorResponse
{
    private Object data;
    private String error;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "data=" + data +
                ", error='" + error + '\'' +
                '}';
    }
}
