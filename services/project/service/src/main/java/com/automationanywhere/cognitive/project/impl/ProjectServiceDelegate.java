package com.automationanywhere.cognitive.project.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.Util.DateUtil;
import com.automationanywhere.cognitive.project.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.project.constants.ProjectUpdationConstants;
import com.automationanywhere.cognitive.project.exception.MessageQueueConnectionCloseException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.exception.NotValidRequestException;
import com.automationanywhere.cognitive.project.exception.ProjectUpdationFailedException;
import com.automationanywhere.cognitive.project.exception.ResourceUpdationException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.exception.VisionBotUpdationFailedException;
import com.automationanywhere.cognitive.project.export.ActivityMonitorService;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.messagequeue.QueuingService;
import com.automationanywhere.cognitive.project.messagequeue.model.ProjectEnvironmentUpdate;
import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.PatchOperation;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dto.PatchRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import com.automationanywhere.cognitive.project.model.dto.VisionbotData;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.OptimisticLockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ProjectServiceDelegate {

  private static String BOT_RUN_EXCEPTION_MESSAGE = "This Learning Instance is currently under use due to Test Bot run. Please try again after sometime.";
  private static String BOT_UPDATION_EXCEPTION_MESSAGE = "This Learning Instance is currently under use due to ongoing Bot Training. Please close training for all bots and try again.";
  private AALogger log = AALogger.create(ProjectServiceDelegate.class);
  @Autowired
  @Qualifier("botUpdationQueuingService")
  private QueuingService botUpdationQueuingService;

  @Autowired
  @Qualifier("queuingServiceProxy")
  private QueuingService documentProcessingQueuingService;

  @Inject
  private ActivityMonitorService activityMonitorService;

  @Inject
  private ProjectUpdation projectUpdation;

  @Autowired
  private VisionbotAdapter visionbotAdapter;

  public ProjectDetail update(ProjectDetail projectDetail, ProjectRequest projectRequest)
      throws NotFoundException, SystemBusyException, VisionBotUpdationFailedException, ProjectUpdationFailedException, CloneNotSupportedException, ResourceUpdationException {
    log.entry();

    ProjectDetail requestedProjectDetail = DtoToDao.toProjectDetail(projectDetail, projectRequest);
    requestedProjectDetail.setUpdatedAt(DateUtil.now());
    Environment environmentBeforeUpdate = projectDetail.getEnvironment();
    copyProjectDetail(requestedProjectDetail, projectDetail);
    ProjectDetail originalProjectDetail = projectDetail.clone();
    updateStandardFieldDetail(projectDetail, requestedProjectDetail);
    updateCustomFieldDetail(projectDetail, requestedProjectDetail);
    projectDetail.setVersionId(requestedProjectDetail.getVersionId());

    if (isNewFieldAdded(requestedProjectDetail)) {
      validateBotUpdation(projectRequest);
    }

    try {

      projectUpdation.update(projectDetail);

      if (isNewFieldAdded(requestedProjectDetail)) {
        addNewFieldsInVisionBots(requestedProjectDetail.getOrganizationId(),
            requestedProjectDetail.getId());
      }

      if (projectRequest != null && environmentBeforeUpdate != projectRequest.getEnvironment()) {
        publishProjectUpdationMessage(projectDetail.getOrganizationId(), projectDetail.getId(),
            projectRequest.getEnvironment());
      }

    } catch (Exception ex) {

      if (ex instanceof OptimisticLockException) {
        throw (new ResourceUpdationException(
            ProjectUpdationConstants.PROJECT_UPDATION_EXCEPTION_MESSAGE,
            ex));
      } else {
        log.info("Rolling back update entity.");
        projectUpdation.update(originalProjectDetail);
        throw ex;
      }
    }

    log.exit();
    return projectDetail;
  }

  private void copyProjectDetail(ProjectDetail requestedProjectDetail,
      ProjectDetail projectDetail) {
    projectDetail.setEnvironment(requestedProjectDetail.getEnvironment());
    projectDetail.setDescription(requestedProjectDetail.getDescription());
    projectDetail.setName(requestedProjectDetail.getName());
    projectDetail.setOcrEngineDetails(requestedProjectDetail.getOcrEngineDetails());
    projectDetail.setPrimaryLanguage(requestedProjectDetail.getPrimaryLanguage());
    projectDetail.setProjectType(requestedProjectDetail.getProjectType());
    projectDetail.setLastUpdatedBy(requestedProjectDetail.getLastUpdatedBy());
    projectDetail.setUpdatedAt(requestedProjectDetail.getUpdatedAt());
  }

  private boolean isNewFieldAdded(ProjectDetail updatedProjectDetail) {
    log.entry();
    return (updatedProjectDetail.getStandardFieldDetailSet().size() > 0
        || updatedProjectDetail.getCustomFieldDetailSet().size() > 0);
  }

  private void validateBotUpdation(ProjectRequest projectRequest) throws SystemBusyException {
    log.entry();
    boolean isStagingDocumentsInProgress = activityMonitorService.isStagingDocumentsInProgress();
    boolean isAnyVisionBotLocked = isAnyVisionBotLocked(projectRequest.getOrganizationId(),
        projectRequest.getId());

    log.debug("isStagingDocumentsInProgress : " + isStagingDocumentsInProgress);
    log.debug("isAnyVisionBotLocked : " + isAnyVisionBotLocked);

    if (isStagingDocumentsInProgress) {
      log.exit();
      throw new SystemBusyException(BOT_RUN_EXCEPTION_MESSAGE);
    }

    if (isAnyVisionBotLocked) {
      log.exit();
      throw new SystemBusyException(BOT_UPDATION_EXCEPTION_MESSAGE);
    }

    log.exit();
  }

  private boolean isAnyVisionBotLocked(String organizationId, String projectId) {
    log.entry();
    List<VisionbotData> visionbotDataList = visionbotAdapter
        .getVisionbotMetadata(organizationId, projectId);
    if (visionbotDataList == null) {
      log.exit();
      return false;
    }

    boolean isAnyVisionBotLocked = visionbotDataList.stream()
        .filter(v -> v.getLockedUserId() != null && !v.getLockedUserId().isEmpty()).count() > 0;

    log.exit();
    return isAnyVisionBotLocked;
  }

  private void publishProjectUpdationMessage(String organizationId, String projectId,
      Environment environment) throws ProjectUpdationFailedException {
    log.entry();
    ObjectMapper objectMapper = new ObjectMapper();
    ProjectEnvironmentUpdate projectEnvironmentUpdate = new ProjectEnvironmentUpdate();
    projectEnvironmentUpdate.setOrganizationId(organizationId);
    projectEnvironmentUpdate.setProjectId(projectId);
    projectEnvironmentUpdate.setEnvironment(environment);
    try {
      documentProcessingQueuingService
          .publish(objectMapper.writeValueAsString(projectEnvironmentUpdate));
    } catch (MessageQueueConnectionCloseException | IOException e) {
      throw new ProjectUpdationFailedException("Project updation failed.", e);
    }
    log.exit();
  }

  private String addNewFieldsInVisionBots(String organizationId, String projectId)
      throws VisionBotUpdationFailedException {
    log.entry();
    ObjectMapper objectMapper = new ObjectMapper();
    ProjectEnvironmentUpdate projectEnvironmentUpdate = new ProjectEnvironmentUpdate();
    projectEnvironmentUpdate.setOrganizationId(organizationId);
    projectEnvironmentUpdate.setProjectId(projectId);
    projectEnvironmentUpdate.setEnvironment(null);
    String replyMsg = null;
    try {
      replyMsg = botUpdationQueuingService
          .call(objectMapper.writeValueAsString(projectEnvironmentUpdate))
          .toString();

      log.info("VisionBot Engine Response : " + replyMsg);
      if (replyMsg.trim().equals("false")) {
        throw new VisionBotUpdationFailedException("Visionbot updation failed.");
      }

    } catch (Exception e) {
      throw new VisionBotUpdationFailedException("Visionbot updation failed.", e);
    }
    log.exit();
    return replyMsg;
  }

  /**
   * increments order of standard field for project.
   *
   * @param updatedProjectDetail project data that needs to be updated.
   */
  private void updateStandardFieldDetail(ProjectDetail projectDetail,
      ProjectDetail updatedProjectDetail) {
    log.entry();
    int maxOrder = 0;
    if (projectDetail.getStandardFieldDetailSet().size() > 0) {
      maxOrder = projectDetail.getStandardFieldDetailSet()
          .stream()
          .mapToInt(it -> it.getOrder())
          .max().getAsInt();
    }
    Set<StandardFieldDetail> updatedStandardFieldDetailSet = updatedProjectDetail
        .getStandardFieldDetailSet();

    for (StandardFieldDetail standardFieldDetail : updatedStandardFieldDetailSet) {
      if (projectDetail.getStandardFieldDetailSet().stream().filter(
          p -> p.getPrimaryKey().getId().equals(standardFieldDetail.getPrimaryKey().getId()))
          .count() == 0) {
        standardFieldDetail.setOrder(++maxOrder);
        standardFieldDetail.setAddedLater(true);
        standardFieldDetail.getPrimaryKey().setProjectDetail(projectDetail);
        projectDetail.getStandardFieldDetailSet().add(standardFieldDetail);
      }
    }
    log.exit();
  }

  /**
   * increments order of custom field for project.
   *
   * @param updatedProjectDetail project data that needs to be updated.
   */
  private void updateCustomFieldDetail(ProjectDetail projectDetail,
      ProjectDetail updatedProjectDetail) {
    log.entry();
    int maxOrder = 0;
    if (projectDetail.getCustomFieldDetailSet().size() > 0) {
      maxOrder = projectDetail.getCustomFieldDetailSet()
          .stream()
          .mapToInt(it -> it.getOrder())
          .max().getAsInt();
    }
    Set<CustomFieldDetail> updatedCustomFieldDetailSet = updatedProjectDetail
        .getCustomFieldDetailSet();

    for (CustomFieldDetail customFieldDetail : updatedCustomFieldDetailSet) {
      if (projectDetail.getCustomFieldDetailSet().stream().filter(
          p -> p.getName().equalsIgnoreCase(customFieldDetail.getName())
              && p.getType().equals(customFieldDetail.getType()))
          .count() == 0) {
        customFieldDetail.setId(UUID.randomUUID().toString());
        customFieldDetail.setOrder(maxOrder++);
        customFieldDetail.setAddedLater(true);
        customFieldDetail.setProjectDetail(projectDetail);
        projectDetail.getCustomFieldDetailSet().add(customFieldDetail);
      }
    }
    log.exit();
  }

  /**
   * Applies operations requested in patchrequest and returns back the updated projectdetail
   * csvmodel.
   *
   * @return projectDetail
   */
  ProjectRequest applyPatchOperations(ProjectRequest projectRequest,
      PatchRequest... patchRequests) {
    log.entry();

    ObjectMapper objectMapper = new ObjectMapper();

    JsonNode rootNode = objectMapper.valueToTree(projectRequest);

    for (PatchRequest patchRequest : patchRequests) {
      JsonPointer pointer = JsonPointer.compile(patchRequest.getPath());
      JsonNode headNode = rootNode.at(pointer.head());
      JsonNode lastNode = headNode.at(pointer.last());
      String nameOfLastNode = pointer.last().getMatchingProperty();
      switch (patchRequest.getOp()) {
        case add:
          if (lastNode.isArray()) {
            ((ObjectNode) headNode).putPOJO(nameOfLastNode, patchRequest.getValue());
          } else {
            ((ObjectNode) headNode).put(nameOfLastNode, patchRequest.getValue().toString());
          }
          break;
        case replace:
          if (lastNode.isArray()) {
            ((ObjectNode) headNode).putPOJO(nameOfLastNode, patchRequest.getValue());
          } else {
            ((ObjectNode) headNode).put(nameOfLastNode, patchRequest.getValue().toString());
          }
          break;
      }
    }
    log.exit();
    return objectMapper
        .convertValue(rootNode, ProjectRequest.class);
  }

  /**
   * Validates the update request.
   *
   * @param patchRequests array of patchrequests having details about operations to do on fields.
   * @throws NotValidRequestException if request is not valid throws NotValidRequestException
   */
  void validateRequest(ProjectRequest projectRequest, PatchRequest... patchRequests)
      throws NotValidRequestException {
    log.entry();
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode node = objectMapper.valueToTree(projectRequest);
    boolean isValidRequest = Stream.of(patchRequests).allMatch(it -> {
      return (it.getOp() == PatchOperation.add || it.getOp() == PatchOperation.replace) && !(node
          .at(it.getPath()) instanceof MissingNode)
          ? true : false;
    });
    if (!isValidRequest) {
      log.exit();
      throw new NotValidRequestException("Not a valid request, please re-verify the request.");
    }
    log.exit();
  }

  void checkIfProjectEnvironmentIsUpdated(ProjectDetail projectDetail,
      PatchRequest... patchRequests) throws ResourceUpdationException {
    if (patchRequests.length == 1 && isEnvironmentUpdated(projectDetail, patchRequests[0]) && (
        projectDetail.getEnvironment() == Environment
            .valueOf(patchRequests[0].getValue().toString()))) {
      throw new ResourceUpdationException(
          ProjectUpdationConstants.PROJECT_UPDATION_EXCEPTION_MESSAGE);
    }
  }

  public boolean isOnlyEnvironmentNeedsUpdate(PatchRequest[] patchRequest) {

    return patchRequest.length == 1 && containsEnvironment(patchRequest[0]);
  }

  private boolean isEnvironmentUpdated(ProjectDetail projectDetail, PatchRequest patchRequest) {
    return containsEnvironment(patchRequest) && isLearningInstanceRecentlyUpdated(projectDetail,
        patchRequest);
  }

  private boolean isLearningInstanceRecentlyUpdated(ProjectDetail projectDetail,
      PatchRequest patchRequest) {

    return patchRequest.getVersionId() < projectDetail.getVersionId() ? true : false;
  }

  private boolean containsEnvironment(PatchRequest patchRequest) {
    String environmentPath = "/environment";
    return patchRequest.getPath().equalsIgnoreCase(environmentPath);
  }
}
