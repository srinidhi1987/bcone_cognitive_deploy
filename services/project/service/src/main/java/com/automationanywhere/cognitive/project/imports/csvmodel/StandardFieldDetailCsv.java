package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class StandardFieldDetailCsv {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "Id")
  private String id;

  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "ProjectId")
  private String projectId;

  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "OrderNumber")
  private int order;

  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "IsAddedLater")
  private boolean isAddedLater;

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public boolean isAddedLater() {
    return isAddedLater;
  }

  public void setAddedLater(boolean addedLater) {
    isAddedLater = addedLater;
  }

  public String getId() {
    return id;

  }

  public void setId(String id) {
    this.id = id;
  }
}
