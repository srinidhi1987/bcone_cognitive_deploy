package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;

import java.util.List;

public interface OCREngineDetailsDao {

    List<OCREngineDetails> getOCREngineDetails();

    OCREngineDetails getOCREngineDetails(String engineType);

    void addOCREngineDetails(OCREngineDetails ocrEngineDetails);
}
