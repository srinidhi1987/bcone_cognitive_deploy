package com.automationanywhere.cognitive.project.adapter;

import com.automationanywhere.cognitive.project.model.dto.FileCategory;

import java.util.List;

/**
 * Created by Jemin.Shah on 11-01-2017.
 */
public interface FileManagerAdapter
{
    List<FileCategory> getCategories(String organizationId, String projectId);
    List<FileCategory> getCategories(String organizationId);
    void testConnection();
}
