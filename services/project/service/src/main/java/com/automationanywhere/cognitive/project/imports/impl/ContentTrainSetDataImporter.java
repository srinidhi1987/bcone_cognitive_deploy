package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.ContentTrainSetDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ContentTrainSet;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ContentTrainSetDataImporter extends AdvanceDataImporter<ContentTrainSet> {

  private static final String SUBFOLDER = "ClassifierData";
  private static final AALogger LOGGER = AALogger.create(ContentTrainSetDataImporter.class);
  private final FileHelper fileHelper;
  private final ContentTrainSetDao contentTrainSetDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private final Map<Integer, Integer> updatedClasses = new HashMap<Integer, Integer>();
  private final List<ContentTrainSet> dataToImport = new ArrayList<>();
  private final TransitionResult transitionResult;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public ContentTrainSetDataImporter(FileReader fileReader,
      FileHelper fileHelper,
      ContentTrainSetDao contentTrainSetDao,
      ImportFilePatternConstructor importFilePatternConstructor,
      TransitionResult transitionResult) {
    super(fileReader);
    this.fileHelper = fileHelper;
    this.contentTrainSetDao = contentTrainSetDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
    this.transitionResult = transitionResult;
  }

  private void mergeClasses(List<ContentTrainSet> csvData) {
    dataToImport.clear();

    List<ContentTrainSet> existingData = contentTrainSetDao.getAll();

    for (ContentTrainSet importRow : csvData) {
      Optional<ContentTrainSet> matchingRow = existingData.stream()
          .filter(p -> p.getTraining().equals(importRow.getTraining())).findFirst();
      if (matchingRow.isPresent()) {
        int oldClass = importRow.getId();
        int newClass = matchingRow.get().getId();

        LOGGER
            .debug(
                String.format("Class %d is matching with existing class %d", oldClass, newClass));
        updatedClasses.put(oldClass, newClass);
      } else {
        dataToImport.add(importRow);
      }
    }
  }

  @Override
  @Transactional(value = DatabaseConstants.CLASSIFIER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //read data from csv
    List<ContentTrainSet> csvData = getDataFromFiles(unzippedFolderPath);

    //filter data
    mergeClasses(csvData);

    //commit data
    Map<Integer, Integer> addedClasses = contentTrainSetDao.addAll(dataToImport);
    updatedClasses.putAll(addedClasses);

    //make result
    transitionResult.setModifiedClasses(updatedClasses);

    //call other data importer
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<ContentTrainSet> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), SUBFOLDER);
    String csvPattern = importFilePatternConstructor
        .getRegexPattern(TableConstants.CONTENTTRAINSET);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, ContentTrainSet.class);
  }
}
