package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.dal.ContentTrainSetDao;
import com.automationanywhere.cognitive.project.dal.DataCleaningDao;
import com.automationanywhere.cognitive.project.dal.LayoutTrainSetDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.dal.TrainingDataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class DataCleaningDaoImpl implements DataCleaningDao {

  private final ContentTrainSetDao contentTrainSetDao;
  private final LayoutTrainSetDao layoutTrainSetDao;
  private final ProjectDetailDao projectDetailDao;
  private final TrainingDataDao trainingDataDao;
  private AALogger log = AALogger.create(this.getClass());

  @Autowired
  public DataCleaningDaoImpl(
      ContentTrainSetDao contentTrainSetDao,
      LayoutTrainSetDao layoutTrainSetDao,
      ProjectDetailDao projectDetailDao,
      TrainingDataDao trainingDataDao) {
    this.contentTrainSetDao = contentTrainSetDao;
    this.layoutTrainSetDao = layoutTrainSetDao;
    this.projectDetailDao = projectDetailDao;
    this.trainingDataDao = trainingDataDao;
  }

  @Transactional(value = DatabaseConstants.CLASSIFIER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  public void cleanClassifierDetails() {
    contentTrainSetDao.deleteAll();
    layoutTrainSetDao.deleteAll();
  }

  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  public void cleanProjectDetails() {
    projectDetailDao.deleteAll();
  }
}
