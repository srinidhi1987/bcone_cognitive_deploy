package com.automationanywhere.cognitive.project.imports.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.ProjectOCREngineDetailsMasterMappingDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.ProjectOCREngineDetailsMasterCsv;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ProjectOCREngineDetailsMasterMapping;

@Component
public class ProjectOCREngineDetailsMasterMappingImporter extends
    AdvanceDataImporter<ProjectOCREngineDetailsMasterCsv> {

  private static final AALogger log = AALogger
      .create(ProjectOCREngineDetailsMasterMappingImporter.class);
  private final FileReader csvFileReader;
  private final TransitionResult transitionResult;
  private final FileHelper fileHelper;
  private final ProjectOCREngineDetailsMasterMappingDao projectOCREngineDetailsMasterMapping;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private ProjectOCREngineDetailsMasterMapping dataToImport;
  private AdvanceDataImporter nextDataImporter;

  @Autowired
  public ProjectOCREngineDetailsMasterMappingImporter(
      FileReader csvFileReader,
      TransitionResult transitionResult,
      FileHelper fileHelper,
      ProjectOCREngineDetailsMasterMappingDao projectOCREngineDetailsMasterMapping,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.csvFileReader = csvFileReader;
    this.transitionResult = transitionResult;
    this.fileHelper = fileHelper;
    this.projectOCREngineDetailsMasterMapping = projectOCREngineDetailsMasterMapping;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    log.entry();
    prepareDataForImport(unzippedFolderPath);
    switch (taskOptions) {
      case NONE:
        break;
      case CLEAR: {
        projectOCREngineDetailsMasterMapping.add(dataToImport);
        break;
      }
      case APPEND:
      case MERGE: {
        if (!isProjectOcrMappingDetailExist()) {
          projectOCREngineDetailsMasterMapping.add(dataToImport);
        }
        break;
      }
      case OVERWRITE: {
        projectOCREngineDetailsMasterMapping.delete(transitionResult.getProjectId());
        projectOCREngineDetailsMasterMapping.add(dataToImport);
        break;
      }
    }
    if (nextDataImporter != null) {
      nextDataImporter.importData(unzippedFolderPath, taskOptions);
    }
    log.exit();
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.nextDataImporter = dataImporter;
  }

  private boolean isProjectOcrMappingDetailExist() {
    List<ProjectOCREngineDetailsMasterMapping> projOcrDetailMapData = projectOCREngineDetailsMasterMapping
        .getProjectOcrMappingByProjectId(transitionResult.getProjectId());
    return CollectionUtils.isEmpty(projOcrDetailMapData) ? false : true;
  }

  private void prepareDataForImport(Path extractedFolderPath)
      throws IOException, ProjectImportException {
    log.entry();
    Path csvFolderPath = Paths
        .get(extractedFolderPath.toString(), transitionResult.getProjectId());

    List<ProjectOCREngineDetailsMasterCsv> data = getDataFromFiles(csvFolderPath);
    Optional<ProjectOCREngineDetailsMasterCsv> projectOcrDetailsModel = data.stream()
        .filter(p -> p.getProjectId().equals(transitionResult.getProjectId()))
        .findFirst();
    if (projectOcrDetailsModel.isPresent()) {
      dataToImport = DtoToDao.toProjectOCREngineDetailsMaster(projectOcrDetailsModel.get());
    }
    if (dataToImport == null) {
      throw new ProjectImportException("No project data available");
    }
    log.exit();
  }

  private List<ProjectOCREngineDetailsMasterCsv> getDataFromFiles(Path csvFolderPath)
      throws IOException, ProjectImportException {
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.PROJECTOCRENGINEDETAILSMASTERMAPPING);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, ProjectOCREngineDetailsMasterCsv.class);
  }

}
