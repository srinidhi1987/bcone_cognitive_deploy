package com.automationanywhere.cognitive.project.exception;

import java.util.ArrayList;
import java.util.List;

public class ProjectExportException extends RuntimeException {

  private static final long serialVersionUID = -5195315303697430689L;

  private List<String> projectIdsNotFound;

  public ProjectExportException(String message) {
    super(message);
  }

  public ProjectExportException(Throwable e) {
    super(e);
  }

  public ProjectExportException(String message, Throwable e) {
    super(message, e);
  }

  public List<String> getProjectIdsNotFound() {
    return projectIdsNotFound;
  }

  public void setProjectIdsNotFound(List<String> projectIdsNotFound) {
    this.projectIdsNotFound = projectIdsNotFound;
  }

  public void addProjectIdsNotFound(List<String> projectIdsNotFound) {
    if (this.projectIdsNotFound == null) {
      this.projectIdsNotFound = new ArrayList<>();
    }
    this.projectIdsNotFound.addAll(projectIdsNotFound);
  }

  public void addProjectIdNotFound(String projectIdNotFound) {
    if (this.projectIdsNotFound == null) {
      this.projectIdsNotFound = new ArrayList<>();
    }
    this.projectIdsNotFound.add(projectIdNotFound);
  }
}