package com.automationanywhere.cognitive.project.health.connections;

import java.util.ArrayList;
import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.project.dal.impl.ProjectDetailDaoImpl;
import com.automationanywhere.cognitive.project.exception.DBConnectionFailureException;
/**
 * @author shweta.thakur
 *
 *         This class checks the database connection health
 */
public class DBCheckConnection implements HealthCheckConnection {
    private ProjectDetailDaoImpl projectDetailDaoImpl;

    public DBCheckConnection(ProjectDetailDaoImpl projectDetailDaoImpl) {
        this.projectDetailDaoImpl = projectDetailDaoImpl;
    }

    @Override
    public void checkConnection() {
        try {
            projectDetailDaoImpl.getProjectDetails("1");
        } catch (Exception ex) {
            throw new DBConnectionFailureException("Error connecting database ", ex);
        }
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        return new ArrayList<ServiceConnectivity>();
    }
}
