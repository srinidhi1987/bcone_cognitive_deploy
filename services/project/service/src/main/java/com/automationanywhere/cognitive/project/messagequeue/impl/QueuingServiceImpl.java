package com.automationanywhere.cognitive.project.messagequeue.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.exception.MessageQueueConnectionCloseException;
import com.automationanywhere.cognitive.project.messagequeue.QueuingService;
import com.rabbitmq.client.*;
import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Jemin.Shah on 3/16/2017.
 */
public class QueuingServiceImpl implements QueuingService
{
    AALogger log = AALogger.create(this.getClass());

    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;
    private String queueName;
    private String host;
    private String username;
    private String password;
    private String virtualHost;
    private String replyQueueName;
    private AMQP.Queue.DeclareOk queue;
    @SuppressWarnings("deprecation")
    private QueueingConsumer consumer;

    public QueuingServiceImpl(String queueName, String host, String username, String password,
        String virtualHost, Integer port) {
        log.entry();
        this.host = host;
        this.username = username;
        this.password = password;
        this.virtualHost = virtualHost;
        this.queueName = queueName;

        this.factory = new ConnectionFactory();
        this.factory.setHost(host);
        this.factory.setUsername(username);
        this.factory.setPassword(password);
        this.factory.setVirtualHost(virtualHost);
        this.factory.setAutomaticRecoveryEnabled(true);
        this.factory.setPort(port);

    }

    @Override
    public int getMessageCount() {
        connectAndRetry();
        createChannel();
        int messageCount = queue.getMessageCount();
        closeChannel();
        closeConnection();
        return messageCount;
    }

    @Override
    public String call(String message) throws IOException, InterruptedException {
        log.entry();

        connectAndRetry();
        createCallChannel();

        try {
            String response = null;
            String corrId = java.util.UUID.randomUUID().toString();

            AMQP.BasicProperties props = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(corrId)
                    .deliveryMode(2)
                    .replyTo(replyQueueName)
                    .build();

            channel.basicPublish("", queueName, props, message.getBytes());

            while (true) {
                @SuppressWarnings("deprecation")
                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                    response = new String(delivery.getBody());
                    break;
                }
            }
            log.exit();
            return response;
        }
        finally
        {
            closeChannel();
            closeConnection();
        }
    }

    @Override
    public void publish(String message)
    {
        connectAndRetry();
        createChannel();
        try
        {
            String corrId = java.util.UUID.randomUUID().toString();
            AMQP.BasicProperties props = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(corrId)
                    .deliveryMode(2)
                    .build();

            channel.basicPublish(
                    "",
                    queueName,
                    props,
                    message.getBytes());
        }
        catch (Exception ex)
        {
            //TODO: Should rethrow it?
            log.error(ex.getMessage());
        }
        finally{
            closeChannel();
            closeConnection();
        }
    }
    
    @Override
    public void closeConnection() {
        try {
            connection.close();
        } catch (IOException ex) {
            log.error("Error while closing rabbit mq connection " , ex);
            throw new MessageQueueConnectionCloseException("Error while closing rabbit mq connection ", ex);
        }
    }

    @Override
    public void closeChannel() {
        try {
            channel.close();
        } catch (TimeoutException  | IOException ex) {
            log.error("Error while closing rabbit mq channel " , ex);
            throw new MessageQueueConnectionCloseException("Error while closing rabbit mq channel ", ex);
        }
    }

    @Override
    public void connect() throws IOException, TimeoutException {
        connection = factory.newConnection();
    }

    @Override
    public void createChannel() {
        try {
            channel = connection.createChannel();
            queue = channel.queueDeclare(queueName, true, false, false, null);
        } catch (IOException ex) {
            //TODO: Should rethrow it?
            log.error(ex.getMessage());
        }
    }

    @SuppressWarnings("deprecation")
    private void createCallChannel()
    {
        try {
            if(channel != null && channel.isOpen())
            {
                return;
            }

            channel = connection.createChannel();
            queue = channel.queueDeclare(queueName, true, false, false, null);
            this.replyQueueName =  queue.getQueue();
            consumer = new QueueingConsumer(channel);
            channel.basicConsume(replyQueueName, true, consumer);
        } catch (IOException ex) {
            //TODO: Should rethrow it?
            log.error(ex.getMessage());
        }
    }

    /**
     * @return the factory
     */
    public ConnectionFactory getFactory() {
        return factory;
    }

    /**
     * @param factory the factory to set
     */
    public void setFactory(ConnectionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void connectAndRetry() {
        try {
            boolean connectionSuccess = false;
            while (!connectionSuccess) {
                try {
                    if(connection != null && connection.isOpen())
                    {
                        return;
                    }

                    connection = factory.newConnection();
                    connectionSuccess = true;
                    log.trace("Connection successful.");
                } catch (ConnectException ex) {
                    log.error(ex.getMessage());
                    log.error("Connection failed. Retrying after 1 second...");
                    Thread.sleep(1000);
                }
            }
//            channel = connection.createChannel();
//            queue = channel.queueDeclare(queueName, true, false, false, null);
        } catch (IOException | TimeoutException | InterruptedException ex) {
            // TODO: Should rethrow it?
            log.error(ex.getMessage());
        }
    }


    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the virtualHost
     */
    public String getVirtualHost() {
        return virtualHost;
    }

    /**
     * @param virtualHost the virtualHost to set
     */
    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

}
