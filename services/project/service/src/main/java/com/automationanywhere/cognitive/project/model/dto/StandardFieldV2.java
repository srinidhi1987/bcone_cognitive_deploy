package com.automationanywhere.cognitive.project.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StandardFieldV2 {
  private String id;
  private boolean isAddedLater;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  //Indicates whether field has been added after Learning Instance creation (while editing Learning Instance)
  @JsonProperty(value="isAddedLater")
  public boolean isAddedLater() {
    return isAddedLater;
  }

  public void setAddedLater(boolean addedLater) {
    this.isAddedLater = addedLater;
  }

  @Override
  public String toString() {
    return "StandardFieldV2{" +
        "id='" + id + '\'' +
        ", isAddedLater=" + isAddedLater +
        '}';
  }
}
