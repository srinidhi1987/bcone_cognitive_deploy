package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.Util.JsonNodeUtil;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.VisionBotDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.VisionBot;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class VisionBotDataImporter extends AdvanceDataImporter<VisionBot> {

  private static final AALogger log = AALogger.create(VisionBotDataImporter.class);
  private static String CLASSIFICATION_ID_REPLACE_EXCEPTION = "Fail to replace classification id in visionbot datalob";
  private static String GROUP_NAME_PREFIX = "Group_";
  private static String GROUP_PROPERTY_FIELD = "/Name";
  private final FileHelper fileHelper;
  private final TransitionResult transitionResult;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private final VisionBotDao visionBotDao;
  private AdvanceDataImporter nextDataImporter;
  private List<VisionBot> dataToImport;

  @Autowired
  public VisionBotDataImporter(
      FileReader csvFileReader,
      VisionBotDao visionBotDao,
      TransitionResult transitionResult,
      FileHelper fileHelper,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.visionBotDao = visionBotDao;
    this.transitionResult = transitionResult;
    this.fileHelper = fileHelper;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, DuplicateProjectNameException, ParseException {
    log.entry();
    List<VisionBot> visionBotData = prepareDataForImport(unzippedFolderPath);
    List<String> importedVisionBotIds = transitionResult.getImportedVisionBotIdList();
    //Iterate list to import only those VisionBotData data whoes associated VisionBot imported.
    dataToImport = visionBotData.stream()
        .filter(visionBotDataRow -> importedVisionBotIds.contains(visionBotDataRow.getId()))
        .collect(
            Collectors.toList());
    modifyClassificationIdInDataBlob();

    if (dataToImport != null) {
      visionBotDao.batchInsert(dataToImport);
    }

    if (nextDataImporter != null) {
      nextDataImporter.importData(unzippedFolderPath, taskOptions);
    }
    log.exit();
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.nextDataImporter = dataImporter;
  }

  private List<VisionBot> prepareDataForImport(Path extractedFolderPath)
      throws ProjectImportException, IOException {
    log.entry();
    Path csvFolderPath = Paths
        .get(extractedFolderPath.toString(), transitionResult.getProjectId());

    List<VisionBot> visionBotData = getDataFromFiles(csvFolderPath);

    //Update lockedUserId before importing to the database
    String lockedUserId = "";
    visionBotData.forEach(visionBotRow -> {
      visionBotRow.setLockedUserId(lockedUserId);
    });
    log.exit();
    return visionBotData;
  }

  private List<VisionBot> getDataFromFiles(Path csvFolderPath)
      throws IOException, ProjectImportException {
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.VISIONBOT);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, VisionBot.class);
  }

  private void modifyClassificationIdInDataBlob() throws ProjectImportException {
    Map<Integer, Integer> modifiedClasses = transitionResult.getModifiedClasses();
    try {
      for (VisionBot visionBot : dataToImport) {
        String datalob = visionBot.getDataBlob();

        String existingClassificationId = JsonNodeUtil
            .findPropertyValue(datalob, GROUP_PROPERTY_FIELD);
        existingClassificationId = existingClassificationId.replace(GROUP_NAME_PREFIX, "");
        if (modifiedClasses.containsKey(Integer.parseInt(existingClassificationId))) {
          String newClassificationId = String.format("%s%s", GROUP_NAME_PREFIX,
              modifiedClasses.get((Integer.parseInt(existingClassificationId))).toString());
          String updatedJson = JsonNodeUtil
              .replacePropertyValue(datalob, GROUP_PROPERTY_FIELD, newClassificationId);
          visionBot.setDataBlob(updatedJson);
        }
      }
    } catch (Exception ex) {
      throw new ProjectImportException(CLASSIFICATION_ID_REPLACE_EXCEPTION, ex);
    }
  }

}
