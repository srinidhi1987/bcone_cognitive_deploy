package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.StandardFieldDetailDao;
import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class StandardFieldDetailDaoImpl implements
    StandardFieldDetailDao {

  private AALogger log = AALogger.create(StandardFieldDetailDaoImpl.class);
  private static final int BATCH_SIZE = 20;

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  public List<String[]> getStandardFieldDetails(String projectId) {
    log.entry();

    String sqlQuery = "select * from StandardFieldDetail where ProjectId = :projectId";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> standardFieldDetails = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return standardFieldDetails;
  }

  @Override
  public List<StandardFieldDetail> getAll(String projectId) {
    Query<StandardFieldDetail> query = sessionFactory.getCurrentSession()
        .createQuery("from StandardFieldDetail where projectId = :projectId",
            StandardFieldDetail.class);
    query.setParameter("projectId", projectId);
    return query.getResultList();
  }

  @Override
  public void delete(String projectId)
  {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("delete StandardFieldDetail where projectId = :projectId");
    query.setParameter("projectId", projectId);
    query.executeUpdate();
  }

  @Override
  public void addAll(List<StandardFieldDetail> standardFieldDetailList) {
    Session session = sessionFactory.getCurrentSession();
    int index = -1;
    for (StandardFieldDetail standardFieldDetail : standardFieldDetailList) {
      session.save(standardFieldDetail);
      index++;
      if (index % BATCH_SIZE == 0) {
        session.flush();
        session.clear();
      }
    }
  }
}
