package com.automationanywhere.cognitive.project.export;

public interface ExportTask<T> {

  public T exportData(String taskId);
}
