package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.ProjectState;
import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;
import java.util.Set;

public class ProjectResponseV2
{
    private String id;
    private String name;
    private String description;
    private String organizationId;
    private String projectType;
    private String projectTypeId;
    private int confidenceThreshold;
    private String primaryLanguage;
    private FieldsV2 fields;
    private ProjectState projectState;
    private Environment environment;
    private long versionId;

    private Set<OCREngineDetails> ocrEngineDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public int getConfidenceThreshold() {
        return confidenceThreshold;
    }

    public void setConfidenceThreshold(int confidenceThreshold) {
        this.confidenceThreshold = confidenceThreshold;
    }

    public FieldsV2 getFields() {
        return fields;
    }

    public void setFields(FieldsV2 fields) {
        this.fields = fields;
    }

    public ProjectState getProjectState() {
        return projectState;
    }

    public void setProjectState(ProjectState projectState) {
        this.projectState = projectState;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getProjectTypeId() {
        return projectTypeId;
    }

    public void setProjectTypeId(String projectTypeId) {
        this.projectTypeId = projectTypeId;
    }

    @Override
    public String toString() {
        return "ProjectResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", organizationId='" + organizationId + '\'' +
                ", projectType='" + projectType + '\'' +
                ", projectTypeId='" + projectTypeId + '\'' +
                ", primaryLanguage='" + primaryLanguage + '\'' +
                ", fields=" + fields +
                ", projectState=" + projectState +
                ", environment=" + environment +
                ", ocrEngineDetails=" + ocrEngineDetails +
                ", versionId=" + versionId +
                '}';
    }

    /**
     * @return the ocrEngineDetails
     */

    public Set<OCREngineDetails> getOcrEngineDetails() {
        return ocrEngineDetails;
    }

    /**
     * @param ocrEngineDetails the ocrDetails to set
     */
    public void setOcrEngineDetails(Set<OCREngineDetails> ocrEngineDetails) {
        this.ocrEngineDetails = ocrEngineDetails;
    }

    public long getVersionId() {
      return versionId;
    }

    public void setVersionId(long versionId) {
      this.versionId = versionId;
    }
}
