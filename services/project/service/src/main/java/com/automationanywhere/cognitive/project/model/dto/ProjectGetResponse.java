package com.automationanywhere.cognitive.project.model.dto;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.ProjectState;
import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;

/**
 * Created by Jemin.Shah on 24-11-2016.
 */
public class ProjectGetResponse
{
    private String id;
    private String name;
    private String description;
    private String organizationId;
    private String projectType;
    private String projectTypeId;
    private int  confidenceThreshold;
    private int numberOfFiles;
    private int numberOfCategories;
    private int unprocessedFileCount;
    private String primaryLanguage;
    private float accuracyPercentage;
    private int visionBotCount;
    private float currentTrainedPercentage;
    private List<FileCategory> categories;
    private Fields fields;
    private ProjectState projectState;
    private Environment environment;
    private Date updatedAt;
    private Date createdAt;

    private Set<OCREngineDetails> ocrEngineDetails;
    private long versionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectTypeId() {
        return projectTypeId;
    }

    public void setProjectTypeId(String projectTypeId) {
        this.projectTypeId = projectTypeId;
    }

    public int getConfidenceThreshold() {
        return confidenceThreshold;
    }

    public void setConfidenceThreshold(int confidenceThreshold) {
        this.confidenceThreshold = confidenceThreshold;
    }

    public int getNumberOfFiles() {
        return numberOfFiles;
    }

    public void setNumberOfFiles(int numberOfFiles) {
        this.numberOfFiles = numberOfFiles;
    }

    public int getNumberOfCategories() {
        return numberOfCategories;
    }

    public void setNumberOfCategories(int numberOfCategories) {
        this.numberOfCategories = numberOfCategories;
    }

    public int getUnprocessedFileCount() {
        return unprocessedFileCount;
    }

    public void setUnprocessedFileCount(int unprocessedFileCount) {
        this.unprocessedFileCount = unprocessedFileCount;
    }

    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(String primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public float getAccuracyPercentage() {
        return accuracyPercentage;
    }

    public void setAccuracyPercentage(float accuracyPercentage) {
        this.accuracyPercentage = accuracyPercentage;
    }

    public int getVisionBotCount() {
        return visionBotCount;
    }

    public void setVisionBotCount(int visionBotCount) {
        this.visionBotCount = visionBotCount;
    }

    public float getCurrentTrainedPercentage() {
        return currentTrainedPercentage;
    }

    public void setCurrentTrainedPercentage(float currentTrainedPercentage) {
        this.currentTrainedPercentage = currentTrainedPercentage;
    }

    public List<FileCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<FileCategory> categories) {
        this.categories = categories;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public ProjectState getProjectState() {
        return projectState;
    }

    public void setProjectState(ProjectState projectState) {
        this.projectState = projectState;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "ProjectGetResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", organizationId='" + organizationId + '\'' +
                ", projectType='" + projectType + '\'' +
                ", projectTypeId='" + projectTypeId + '\'' +
                ", confidenceThreshold='" + confidenceThreshold + '\'' +
                ", numberOfFiles=" + numberOfFiles +
                ", numberOfCategories=" + numberOfCategories +
                ", unprocessedFileCount=" + unprocessedFileCount +
                ", primaryLanguage='" + primaryLanguage + '\'' +
                ", accuracyPercentage=" + accuracyPercentage +
                ", visionBotCount=" + visionBotCount +
                ", currentTrainedPercentage=" + currentTrainedPercentage +
                ", categories=" + categories +
                ", fields=" + fields +
                ", projectState=" + projectState +
                ", environment=" + environment +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                ", ocrEngineDetails=" + ocrEngineDetails +
                ", versionId=" + versionId +
                '}';
    }

    /**
     * @return the ocrDetails
     */
    public Set<OCREngineDetails> getOcrEngineDetails() {
        return ocrEngineDetails;
    }

    /**
     * @param ocrEngineDetails the ocrDetails to set
     */
    public void setOcrEngineDetails(Set<OCREngineDetails> ocrEngineDetails) {
        this.ocrEngineDetails = ocrEngineDetails;
    }

    public long getVersionId() {
      return versionId;
    }

    public void setVersionId(long versionId) {
      this.versionId = versionId;
    }
}
