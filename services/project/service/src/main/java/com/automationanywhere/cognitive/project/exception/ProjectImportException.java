package com.automationanywhere.cognitive.project.exception;

public class ProjectImportException extends Exception{

    private static final long serialVersionUID = -5765979386361463982L;

    public ProjectImportException(String message)
    {
        super(message);
    }

    public ProjectImportException(String message, Throwable cause)
    {
        super(message, cause);
    }
}