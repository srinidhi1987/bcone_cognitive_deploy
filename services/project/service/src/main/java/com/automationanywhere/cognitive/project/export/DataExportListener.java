package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.export.model.ClassifierDataExportMetadata;
import com.automationanywhere.cognitive.project.export.model.FileDataExportMetadata;
import com.automationanywhere.cognitive.project.export.model.MlDataExportMetadata;
import com.automationanywhere.cognitive.project.export.model.ProjectDataExportMetadata;

import com.automationanywhere.cognitive.project.export.model.VisionBotDataExportMetadata;
import java.util.List;

public interface DataExportListener {
    void onProjectDataExportCompleted(List<ProjectDataExportMetadata> metadata);
    void onFileDataExportCompleted(List<FileDataExportMetadata> metadata);
    void onVisionBotDataExportCompleted(List<VisionBotDataExportMetadata> metadata);
    void onClassifierDataExportCompleted(List<ClassifierDataExportMetadata> metadata);
    void onMlDataExportCompleted(List<MlDataExportMetadata> metadata);
    void onException(Exception e);
}
