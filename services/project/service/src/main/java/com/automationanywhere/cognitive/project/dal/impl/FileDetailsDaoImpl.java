package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class FileDetailsDaoImpl implements FileDetailsDao {

  private AALogger log = AALogger.create(FileDetailsDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  @Override
  public Map<String, List<String>> getFileIdsMapGroupedByProjectId(String[] projectIds,
      int isProduction) {
    log.entry();

    String sqlQuery = "select projectid,fileid from fileDetails where projectid in (:projectIds) AND isproduction = :isProduction";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("projectIds", projectIds);
    query.setParameter("isProduction", isProduction);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    Map<String, List<String>> mapOfFileidByProjectid = null;
    if (resulSetData != null && !resulSetData.isEmpty()) {
      mapOfFileidByProjectid = new HashMap<String, List<String>>();
      for (Object[] fileDetailsObj : resulSetData) {
        String projectId = String.valueOf(fileDetailsObj[0]);
        String fileId = String.valueOf(fileDetailsObj[1]);

        if (!mapOfFileidByProjectid.containsKey(projectId)) {
          mapOfFileidByProjectid.put(projectId, new ArrayList<String>());
          mapOfFileidByProjectid.get(projectId).add(fileId);
        } else {
          mapOfFileidByProjectid.get(projectId).add(fileId);
        }
      }

    }

    log.exit();
    return mapOfFileidByProjectid;
  }

  @Override
  public List<String[]> getFileDetails(String projectId, List<String> fileIds, int isProduction) {
    log.entry();

    String sqlQuery = "select * from fileDetails where projectid = :projectId AND fileid in (:fileIds) AND isproduction = :isProduction";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);

    query.setParameterList("fileIds", fileIds);
    query.setParameter("isProduction", isProduction);

    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> fileDetails = dataTransformer.transformToListOfStringArray(resulSetData);

    log.exit();
    return fileDetails;
  }

  @Override
  public List<FileDetails> getFileDetailsByEntity(String projectId, List<String> fileIds, int isProduction) {
    log.entry();
    String hql = "from fileDetails where projectId = :projectId and fileid in (:fileIds) AND isproduction = :isProduction";

    Query<FileDetails> query = sessionFactory.getCurrentSession()
        .createQuery(hql, FileDetails.class);
    query.setParameter("projectId", projectId);

    query.setParameterList("fileIds", fileIds);
    query.setParameter("isProduction", isProduction);

    List<FileDetails> results = query.list();

    if (results == null || results.size() == 0) {
      return null;
    }
    log.exit();
    return results;
  }

  @Override
  public void batchInsertFileDetails(List<FileDetails> fileDetailsList)  {
    Session session = sessionFactory.getCurrentSession();
    int index=-1;
    for (FileDetails fileDetails : fileDetailsList) {
      session.save(fileDetails);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
      index++;
    }
  }

  @Override
  public void batchAddOrUpdateFileDetails(List<FileDetails> fileDetailsList)  {
    Session session = sessionFactory.getCurrentSession();
    int index=-1;
    for (FileDetails fileDetails : fileDetailsList) {
      session.saveOrUpdate(fileDetails);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
      index++;
    }
  }

  @Override
  public List<FileDetails> getFileDetails(String projectId,List<String> fileIds) {
    log.entry();
    String hqlQueryString = "from FileDetails where projectId = :projectId and fileId in (:fileIds)";
    Query<FileDetails> query = sessionFactory.getCurrentSession()
        .createQuery(hqlQueryString, FileDetails.class);
    query.setParameter("projectId", projectId);
    query.setParameterList("fileIds", fileIds);

    List<FileDetails> results = query.list();
    if (results == null) {
      results = Collections.emptyList();
    }
    log.exit();
    return results;
  }

  @Override
  @Transactional
  public List<FileDetails> getFileDetails(List<String> projectIds) {
    log.entry();
    String hqlQueryString = "from FileDetails where projectid in (:projectIds)";
    Query<FileDetails> query = sessionFactory.getCurrentSession()
        .createQuery(hqlQueryString, FileDetails.class);
    query.setParameter("projectIds", projectIds);

    List<FileDetails> results = query.list();
    if (results == null) {
      results = Collections.emptyList();
    }
    log.exit();
    return results;
  }
}

