package com.automationanywhere.cognitive.project.exception;

/**
 * Created by Jemin.Shah on 09-01-2017.
 */
public class InvalidInputException extends Exception {

    private static final long serialVersionUID = -5138304237578255050L;

    public InvalidInputException(String message)
    {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause)
    {
        super(message, cause);
    }
}