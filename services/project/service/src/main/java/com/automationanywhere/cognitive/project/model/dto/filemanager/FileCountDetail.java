package com.automationanywhere.cognitive.project.model.dto.filemanager;

/**
 * Created by Jemin.Shah on 4/5/2017.
 */
public class FileCountDetail
{
    String projectId;
    String categoryId;
    long fileCount;
    EnvironmentWiseFileCount stagingFileCount;
    EnvironmentWiseFileCount productionFileCount;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public long getFileCount() {
        return fileCount;
    }

    public void setFileCount(long fileCount) {
        this.fileCount = fileCount;
    }

    public EnvironmentWiseFileCount getStagingFileCount() {
        return stagingFileCount;
    }

    public void setStagingFileCount(EnvironmentWiseFileCount stagingFileCount) {
        this.stagingFileCount = stagingFileCount;
    }

    public EnvironmentWiseFileCount getProductionFileCount() {
        return productionFileCount;
    }

    public void setProductionFileCount(EnvironmentWiseFileCount productionFileCount) {
        this.productionFileCount = productionFileCount;
    }
}
