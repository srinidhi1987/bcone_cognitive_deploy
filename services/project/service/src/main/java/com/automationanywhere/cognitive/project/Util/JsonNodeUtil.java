package com.automationanywhere.cognitive.project.Util;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;

public class JsonNodeUtil {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static String replacePropertyValue(String jsonData, String nodePath, String value)
      throws IOException {
    JsonNode rootNode = objectMapper.readTree(jsonData);
    JsonPointer valuePointer = JsonPointer.compile(nodePath);
    JsonPointer containerPointer = valuePointer.head();
    JsonNode parentNode = rootNode.at(containerPointer);

    if (!parentNode.isMissingNode()) {
      ObjectNode parentObjectNode = (ObjectNode) parentNode;

      String fieldName = valuePointer.last().toString();

      fieldName = fieldName.replace("/", "");
      JsonNode fieldValueNode = parentNode.get(fieldName);
      if (fieldValueNode != null) {
        parentObjectNode.put(fieldName, value);
      }
      return objectMapper.writeValueAsString(rootNode);
    } else {
      throw new IllegalStateException(String.format("property: %s not available.", nodePath));
    }
  }

  public static String findPropertyValue(String jsonData, String nodePath) throws IOException {
    JsonNode rootNode = objectMapper.readTree(jsonData);
    JsonPointer valuePointer = JsonPointer.compile(nodePath);
    JsonPointer containerPointer = valuePointer.head();
    JsonNode parentNode = rootNode.at(containerPointer);

    if (!parentNode.isMissingNode()) {

      String fieldName = valuePointer.last().toString();

      fieldName = fieldName.replace("/", "");
      JsonNode fieldValueNode = parentNode.get(fieldName);

      return fieldValueNode.textValue().toString();
    } else {
      throw new IllegalStateException(String.format("property: %s not available.", nodePath));
    }
  }
}
