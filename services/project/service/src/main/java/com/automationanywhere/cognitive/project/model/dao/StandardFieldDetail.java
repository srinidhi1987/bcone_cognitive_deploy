package com.automationanywhere.cognitive.project.model.dao;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.*;

/**
 * Created by Jemin.Shah on 08-12-2016.
 */

@Entity
@Table(name = "StandardFieldDetail")
public class StandardFieldDetail
{
    @Embeddable
    public static class PrimaryKey implements Serializable {

        private static final long serialVersionUID = -8673443297745916017L;
        @Column(name = "Id", nullable = false)
        private String id;
        @ManyToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "ProjectId")
        private ProjectDetail projectDetail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ProjectDetail getProjectDetail() {
            return projectDetail;
        }

        public void setProjectDetail(ProjectDetail projectDetail) {
            this.projectDetail = projectDetail;
        }
    }

    @EmbeddedId
    private PrimaryKey primaryKey = new PrimaryKey();

    @Column(name = "OrderNumber")
    private int order;

    @Column(name="IsAddedLater")
    private boolean isAddedLater;

    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isAddedLater() {
        return isAddedLater;
    }

    public void setAddedLater(boolean addedLater) {
        this.isAddedLater = addedLater;
    }

    public static final Comparator<StandardFieldDetail> COMPARE_BY_ORDER_NUMBER = Comparator.comparingInt(base -> base.order);
}
