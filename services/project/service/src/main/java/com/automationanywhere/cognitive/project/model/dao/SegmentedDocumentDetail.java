package com.automationanywhere.cognitive.project.model.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SegmentedDocumentDetails")
public class SegmentedDocumentDetail {

  @Id
  @Column(name = "FileId")
  String fileId;

  @Column(name = "SegmentedDocument")
  String segmentedDocument;

  public SegmentedDocumentDetail() {
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public String getSegmentedDocument() {
    return segmentedDocument;
  }

  public void setSegmentedDocument(String segmentedDocument) {
    this.segmentedDocument = segmentedDocument;
  }
}
