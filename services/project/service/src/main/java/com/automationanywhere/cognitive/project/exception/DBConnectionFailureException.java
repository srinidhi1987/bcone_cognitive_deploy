package com.automationanywhere.cognitive.project.exception;

public class DBConnectionFailureException extends RuntimeException {

	private static final long serialVersionUID = 8965886765691407757L;

	public DBConnectionFailureException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
