package com.automationanywhere.cognitive.project.model;

/**
 * Created by Jemin.Shah on 08-12-2016.
 */
public enum FieldType
{
    FormField,
    TableField
}
