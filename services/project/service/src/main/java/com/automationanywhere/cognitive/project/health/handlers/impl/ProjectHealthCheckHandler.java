package com.automationanywhere.cognitive.project.health.handlers.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.health.handlers.AbstractHealthHandler;
import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.exception.DBConnectionFailureException;
import com.automationanywhere.cognitive.project.exception.MQConnectionFailureException;
import com.automationanywhere.cognitive.project.health.connections.ConnectionType;
import com.automationanywhere.cognitive.project.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.project.health.connections.HealthCheckConnectionFactory;

public class ProjectHealthCheckHandler implements AbstractHealthHandler {

    @Autowired
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    AALogger aaLogger = AALogger.create(this.getClass());

    @Override
    public String checkHealth(){
        aaLogger.entry();
        HealthCheckConnection healthCheckConnection;
        Connectivity dbConnectionType = Connectivity.OK;
        Connectivity mqConnectity = Connectivity.OK;
        List<ServiceConnectivity> serviceConnectivity = null;
        for (ConnectionType connectionType : ConnectionType.values()) {
            healthCheckConnection = healthCheckConnectionFactory.getConnection(connectionType);
            try {
                healthCheckConnection.checkConnection();
            } catch (DBConnectionFailureException dcfe) {
                aaLogger.error("Error connecting to Database ", dcfe);
                dbConnectionType = Connectivity.FAILURE;
            }catch (MQConnectionFailureException mqfe) {
                aaLogger.error("Error connecting to Message queue ", mqfe);
                mqConnectity = Connectivity.FAILURE;
            }
            if(connectionType.equals(ConnectionType.SVC)){
                serviceConnectivity = healthCheckConnection.checkConnectionForService();
            }
        }
        String healthCheckDetails = HealthApiResponseBuilder.prepareSubSystem(HTTPStatusCode.OK,
                SystemStatus.ONLINE, dbConnectionType, mqConnectity,Connectivity.NOT_APPLICABLE,
                serviceConnectivity);
        aaLogger.exit();
        return healthCheckDetails;
    }

}
