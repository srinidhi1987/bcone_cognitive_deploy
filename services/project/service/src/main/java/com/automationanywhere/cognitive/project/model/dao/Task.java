package com.automationanywhere.cognitive.project.model.dao;

import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.TaskStatus;
import com.automationanywhere.cognitive.project.model.TaskType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Tasks")
public class Task {

  @Id
  @Column(name = "TaskId", nullable = false)
  private String taskId;

  @Column(name = "TaskType", nullable = false)
  @Enumerated(EnumType.STRING)
  private TaskType taskType;

  @Column(name = "TaskOptions", nullable = false)
  @Enumerated(EnumType.STRING)
  private TaskOptions taskOptions;

  @Column(name = "Status", nullable = false)
  @Enumerated(EnumType.STRING)
  private TaskStatus status;

  @Column(name = "Description", nullable = false)
  private String description;

  @Column(name = "CreatedBy")
  private String userId;

  @Column(name = "StartTime")
  private Date startTime;

  @Column(name = "EndTime")
  private Date endTime;


  public String getTaskId() {
    return taskId;
  }

  public void setTaskId(String taskId) {
    this.taskId = taskId;
  }

  public TaskType getTaskType() {
    return taskType;
  }

  public void setTaskType(TaskType taskType) {
    this.taskType = taskType;
  }

  public TaskOptions getTaskOptions() {
    return taskOptions;
  }

  public void setTaskOptions(TaskOptions taskOptions) {
    this.taskOptions = taskOptions;
  }

  public TaskStatus getStatus() {
    return status;
  }

  public void setStatus(TaskStatus status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }
}
