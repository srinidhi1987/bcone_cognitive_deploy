package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.security.sqlinjection.SqlInjectionChecker;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.FilterParameters;
import com.automationanywhere.cognitive.project.dal.QueryInfo;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;

/**
 * Created by Jemin.Shah on 2/14/2017.
 */
public class ProjectDetailsByQueryParameter {
    private AALogger log = AALogger.create(ProjectDetailsByQueryParameter.class);

    @Autowired
    @Qualifier("filemanagerSessionFactory")
    SessionFactory sessionFactory;

    public List<ProjectDetail> getProjectDetails(Map<String, String> restrictions, Map<String, String[]> queryParameters) throws InvalidInputException {

        QueryInfo queryInfo = buildQueryInfo(restrictions, queryParameters);
        return getProjectDetailsFromDatabase(queryInfo);
    }

    private QueryInfo buildQueryInfo(Map<String, String> restrictions, Map<String, String[]> queryParameters) throws InvalidInputException {

        QueryInfo queryInfo = new QueryInfo();

        if (restrictions != null && restrictions.size() > 0) {
            for (Map.Entry<String, String> entry : restrictions.entrySet()) {
                queryInfo.getWhereList().add(new String[]{entry.getKey(), entry.getValue()});
            }
        }

        if (queryParameters != null && queryParameters.size() > 0) {
            for (Map.Entry<String, String[]> entry : queryParameters.entrySet()) {
                FilterParameters filterParameter;
                try {
                    filterParameter = FilterParameters.valueOf(entry.getKey().toLowerCase());
                } catch (Exception ex) {
                    throw new InvalidInputException("Invalid parameter", ex);
                }

                switch (filterParameter) {
                    case limit:
                        queryInfo.setLimit(Integer.parseInt(entry.getValue()[0]));
                        break;
                    case offset:
                        queryInfo.setOffset(Integer.parseInt(entry.getValue()[0]));
                        break;
                    case orderby:
                        queryInfo.setOrderBy(entry.getValue()[0]);
                        break;
                    default:
                        queryInfo.getWhereList().add(new String[]{entry.getKey(), entry.getValue()[0]});
                        break;
                }
            }
        }

        return queryInfo;
    }

    private List<ProjectDetail> getProjectDetailsFromDatabase(QueryInfo queryInfo) {
        log.entry();
        StringBuilder builder = new StringBuilder();
        builder.append(
                "select distinct p from ProjectDetail p " +
                        "left join fetch p.customFieldDetailSet c " +
                        "left join fetch p.standardFieldDetailSet s ");

        //where clause
        for (int count = 0; count < queryInfo.getWhereList().size(); count++) {
            builder.append(count == 0 ? " where " : " and ");
            builder.append("p." + queryInfo.getWhereList().get(count)[0]);
            builder.append(" = :");
            builder.append(queryInfo.getWhereList().get(count)[0]);
        }

        builder.append(" order by c.order, s.order");
        //orderby clause
        String orderBy = queryInfo.getOrderBy();
        if (orderBy != null && !orderBy.isEmpty()) {
            SqlInjectionChecker.checkColumnName(orderBy);
            builder.append(", p." + orderBy);
        }
        Session session = null;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            log.error(e.getMessage());
            session = sessionFactory.openSession();
        }

        Query<ProjectDetail> query = session.createQuery(builder.toString(), ProjectDetail.class);

        //append parameter value
        for (int count = 0; count < queryInfo.getWhereList().size(); count++) {
            query.setParameter(queryInfo.getWhereList().get(count)[0], queryInfo.getWhereList().get(count)[1]);
        }
        List<ProjectDetail> projectDetails = query.setMaxResults(queryInfo.getLimit())
                .setFirstResult(queryInfo.getOffset())
                .getResultList();
        log.exit();
        return projectDetails;
    }

}
