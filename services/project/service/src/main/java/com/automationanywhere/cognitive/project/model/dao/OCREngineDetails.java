package com.automationanywhere.cognitive.project.model.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OCREngineDetailsMaster")
public class OCREngineDetails {

    @JsonProperty("id")
    @Id
    @Column(name="Id")
    String id;

    @JsonProperty("engineType")
    @Column(name="Name")
    String engineType;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the engineType
     */
    public String getEngineType() {
        return engineType;
    }

    /**
     * @param engineType the engineType to set
     */
    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }
    
    
}
