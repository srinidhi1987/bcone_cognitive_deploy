package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.Util.DataUtil;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExportListener;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.ExportOperator;
import com.automationanywhere.cognitive.project.export.model.MlDataExportMetadata;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import javax.inject.Inject;

public class MLDataExportOperator implements ExportOperator {

  private static final ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();

  private final DataExporter mlDataExporter;

  @Inject
  private ProjectDetailDao projectDetailDao;

  @Inject
  public MLDataExportOperator(DataExporter mlDataExporter) {
    this.mlDataExporter = mlDataExporter;
  }

  @Override
  public void export(String dirPath, String[] projectIds,
      DataExportListener dataExportListener) {

    forkJoinPool.execute(new MlDataExportTask(dirPath, projectIds, dataExportListener));
  }


  private class MlDataExportTask extends RecursiveTask<List<MlDataExportMetadata>> {

    private static final long serialVersionUID = 7797208334810033840L;
	
    private static final int PROJECT_NUMBER_THRESHOLD = 10;
    private final String dirPath;
    private final String[] projectIds;
    private DataExportListener dataExportListener;

    MlDataExportTask(String dirPath, String[] projectIds,
        DataExportListener dataExportListener) {
      this(dirPath, projectIds);
      this.dataExportListener = dataExportListener;
    }

    private MlDataExportTask(String dirPath, String[] projectIds) {
      this.dirPath = dirPath;
      this.projectIds = projectIds;
    }

    @Override
    public List<MlDataExportMetadata> compute() {
      try {
        List<MlDataExportMetadata> metadata;
        if (projectIds.length > PROJECT_NUMBER_THRESHOLD) {
          metadata = ForkJoinTask.invokeAll(createSubTasks()).stream().map(ForkJoinTask::join)
              .flatMap(List::stream).collect(
                  Collectors.toList());
        } else {
          metadata = new ArrayList<>();
          for (String projectId : projectIds) {
            metadata.add(exportData(projectId));
          }
        }
        if (dataExportListener != null) {
          dataExportListener.onMlDataExportCompleted(metadata);
        }
        return metadata;
      } catch (Exception e) {
        if (dataExportListener != null) {
          dataExportListener.onException(e);
        }
        throw e;
      }
    }

    private List<MlDataExportTask> createSubTasks() {
      List<String[]> lstOfArrayOfProjectIds = DataUtil
          .subDivide(projectIds, PROJECT_NUMBER_THRESHOLD);

      List<MlDataExportTask> subTasks = new ArrayList<>();

      lstOfArrayOfProjectIds.stream().forEach(
          subProjectIds -> subTasks.add(new MlDataExportTask(dirPath, subProjectIds)));

      return subTasks;
    }


    private MlDataExportMetadata exportData(String projectId) throws ProjectExportException {

      Map<Object, Object> exportParams = new HashMap<Object, Object>();
      exportParams.put("projectId", projectId);

      @SuppressWarnings("unchecked")
      MlDataExportMetadata mlDataExportMetadata = (MlDataExportMetadata) mlDataExporter
          .exportData(dirPath, exportParams);

      return mlDataExportMetadata;

    }

  }
}