package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.DocumentProcessedDetails;
import java.util.List;


public interface DocumentProcessedDetailDao {

  public List<String[]> getDocumentProcessedDetails(List<String> visionBotRunDetailId);

  public List<DocumentProcessedDetails> get(List<String> visionBotRunDetailIdList);

  public void addAll(List<DocumentProcessedDetails> documentProcessedDetailsList);

  public void delete(List<String> visionBotRunDetailIds);
}
