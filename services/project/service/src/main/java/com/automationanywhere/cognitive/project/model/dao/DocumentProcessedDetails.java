package com.automationanywhere.cognitive.project.model.dao;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DocumentProccessedDatails")
public class DocumentProcessedDetails {

  @Id
  @Column(name = "id")
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;

  @Column(name = "docid")
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "docid")
  private String documentId;

  @Column(name = "visionbotrundetailsid")
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "visionbotrundetailsid")
  private String visionBotRunDetailsId;

  @Column(name = "processingstatus")
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "processingstatus")
  private String processingStatus;

  @Column(name = "totalfieldcount")
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "totalfieldcount")
  private String totalFieldCount;

  @Column(name = "passedfieldcount")
  @CsvBindByPosition(position = 5)
  @CsvBindByName(column = "passedfieldcount")
  private String passedFieldCount;

  @Column(name = "failedfieldcount")
  @CsvBindByPosition(position = 6)
  @CsvBindByName(column = "failedfieldcount")
  private String failedFieldCount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDocumentId() {
    return documentId;
  }

  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }

  public String getVisionBotRunDetailsId() {
    return visionBotRunDetailsId;
  }

  public void setVisionBotRunDetailsId(String visionBotRunDetailsId) {
    this.visionBotRunDetailsId = visionBotRunDetailsId;
  }

  public String getProcessingStatus() {
    return processingStatus;
  }

  public void setProcessingStatus(String processingStatus) {
    this.processingStatus = processingStatus;
  }

  public String getTotalFieldCount() {
    return totalFieldCount;
  }

  public void setTotalFieldCount(String totalFieldCount) {
    this.totalFieldCount = totalFieldCount;
  }

  public String getPassedFieldCount() {
    return passedFieldCount;
  }

  public void setPassedFieldCount(String passedFieldCount) {
    this.passedFieldCount = passedFieldCount;
  }

  public String getFailedFieldCount() {
    return failedFieldCount;
  }

  public void setFailedFieldCount(String failedFieldCount) {
    this.failedFieldCount = failedFieldCount;
  }
}
