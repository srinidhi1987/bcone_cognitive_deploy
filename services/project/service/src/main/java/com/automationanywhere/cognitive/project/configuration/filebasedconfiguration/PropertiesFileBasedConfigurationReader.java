package com.automationanywhere.cognitive.project.configuration.filebasedconfiguration;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.configuration.ConfigurationReader;
import com.automationanywhere.cognitive.project.exception.NotFoundException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesFileBasedConfigurationReader implements ConfigurationReader
{
    private static AALogger log = AALogger.create(PropertiesFileBasedConfigurationReader.class);

    private String filepath;

    public PropertiesFileBasedConfigurationReader(String filepath) {
        this.filepath = filepath;
    }

    public Map<String, String> getProperties() throws NotFoundException {
        log.traceEntry();

        Properties properties = new Properties();
        InputStream input = null;
        Map<String, String> keyValueMap  = new HashMap<String,String>();

        try {
            input = new FileInputStream(this.filepath);
            properties.load(input);
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                keyValueMap.put((String) entry.getKey(), (String) entry.getValue());
            }
            return keyValueMap;
        } catch (IOException e) {
            throw new NotFoundException(e.getMessage(), e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new NotFoundException(e.getMessage(), e);
                }
            }
            log.traceExit();
        }
    }
}
