/**
 * Copyright (c) 2016 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.project.exception.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.exception.*;
import com.automationanywhere.cognitive.project.model.dto.StandardResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Response;
import static spark.Spark.exception;

/**
 * This filter handles exceptions that has been thrown from resource/service layer.
 * It maps java exceptions to the corresponding HTTP status/response.
 */
public class ExceptionFilter  {
    private ObjectMapper mapper;	
    private AALogger logger = AALogger.create(this.getClass());


    public void init() {
        exception(Exception.class, (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
        exception(ProjectDeletedException.class, (e, req, res) -> handleException(e, res, HttpStatusCode.GONE));
        exception(DuplicateProjectNameException.class, (e, req, res) -> handleException(e, res, HttpStatusCode.CONFLICT));
        exception(NotFoundException.class, (e, req, res)-> handleException(e, res, HttpStatusCode.NOT_FOUND));
        exception(InvalidInputException.class, (e, req, res)-> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
        exception(PreconditionFailedException.class, (e, req, res) -> handleException(e, res, HttpStatusCode.PRECONDITION_FAILED));
        exception(ProjectImportException.class, (e, req, res) -> handleException(e, res, HttpStatusCode.PRECONDITION_FAILED));
        exception(SystemBusyException.class, (e, req, res) -> handleException(e, res, HttpStatusCode.PRECONDITION_FAILED));
        exception(ResourceUpdationException.class,(e,req,res) -> handleException(e, res, HttpStatusCode.CONFLICT));
        exception(TaskException.class,(e,req,res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    private void handleException(Exception e, Response res, HttpStatusCode statusCode) {
        logger.entry();
        logger.error(e.getMessage(),e);
        String errorMessage = e.getMessage() == null ? statusCode.getMessage() : e.getMessage();
        res.status(statusCode.getCode());
        StandardResponse customResponse = new StandardResponse();
        customResponse.setSuccess(false);
        String customResponseInFormOfJson="";
        try {
            customResponse.setError(new String[]{errorMessage});
            customResponseInFormOfJson=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);
            logger.trace("Exception Handler Response :" + customResponseInFormOfJson);
        }
        catch (Exception ex)
        {
			logger.error("Exception Occurred :" +  errorMessage, ex);
        }
        res.body(customResponseInFormOfJson);
        logger.exit();
    }
}
