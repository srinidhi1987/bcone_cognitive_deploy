package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.model.FieldType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomFieldV2
{
    private FieldType type;
    private String name;
    private String id;
    private boolean isAddedLater;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Indicates whether field has been added after Learning Instance creation (while editing Learning Instance)
    @JsonProperty(value="isAddedLater")
    public boolean isAddedLater() {
        return isAddedLater;
    }

    public void setAddedLater(boolean addedLater) {
        this.isAddedLater = addedLater;
    }

  @Override
  public String toString() {
    return "CustomFieldV2{" +
        "type=" + type +
        ", name='" + name + '\'' +
        ", id='" + id + '\'' +
        ", isAddedLater=" + isAddedLater +
        '}';
  }
}
