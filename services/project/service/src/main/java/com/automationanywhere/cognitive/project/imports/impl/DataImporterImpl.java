package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.BulkDataImportDao;
import com.automationanywhere.cognitive.project.imports.DataImporter;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Bulk inserts data into resptables.
 */
public class DataImporterImpl implements DataImporter {

  @Autowired
  BulkDataImportDao bulkDataImportDao;

  private AALogger log = AALogger.create(DataImporterImpl.class);

  /**
   * Bulk inserts data in tables(ProjectDetail,ProjectOCREngineDetailsMasterMapping,CustomFieldDetail,StandardFieldDetail)
   *
   * @param dataFilePathMap map object having list of paths grouped by table name constants.
   * @return map object of rows count grouped by table name.
   */
  @Transactional(value = "filemanagerTransactionManager", propagation = Propagation.REQUIRES_NEW)
  public Map<String, Integer> importProjectData(
      Map<TableConstants, List<Path>> dataFilePathMap) {
    log.entry();
    Map<String, Integer> noOfRowsUpdatedByTable = new HashMap<>();

    TableConstants projectDetailConstant = TableConstants.PROJECTDETAIL;
    TableConstants projOcrEngDetailConstant = TableConstants.PROJECTOCRENGINEDETAILSMASTERMAPPING;
    TableConstants customFieldDetailConstant = TableConstants.CUSTOMFIELDDETAIL;
    TableConstants standrdFieldDetailContant = TableConstants.STANDARDFIELDDETAIL;

    int projectDetailRowsUpdated = 0, projOcrEngDetailRowsUpdated = 0, customFieldDetailRowsUpdated = 0, standrdFieldDetailRowsUpdated = 0;

    for (Path path : dataFilePathMap.get(projectDetailConstant)) {
      int a=bulkDataImportDao.bulkInsert(path, projectDetailConstant);
      projectDetailRowsUpdated += a;
    }
    for (Path path : dataFilePathMap.get(projOcrEngDetailConstant)) {
      projOcrEngDetailRowsUpdated += bulkDataImportDao.bulkInsert(path, projOcrEngDetailConstant);
    }
    for (Path path : dataFilePathMap.get(customFieldDetailConstant)) {
      customFieldDetailRowsUpdated += bulkDataImportDao
          .bulkInsert(path, customFieldDetailConstant);
    }
    for (Path path : dataFilePathMap.get(standrdFieldDetailContant)) {
      standrdFieldDetailRowsUpdated += bulkDataImportDao
          .bulkInsert(path, standrdFieldDetailContant);
    }

    noOfRowsUpdatedByTable.put(projectDetailConstant.getName(), projectDetailRowsUpdated);
    noOfRowsUpdatedByTable.put(projOcrEngDetailConstant.getName(), projOcrEngDetailRowsUpdated);
    noOfRowsUpdatedByTable.put(customFieldDetailConstant.getName(), customFieldDetailRowsUpdated);
    noOfRowsUpdatedByTable.put(standrdFieldDetailContant.getName(), standrdFieldDetailRowsUpdated);

    log.exit();

    return noOfRowsUpdatedByTable;

  }

  /**
   * Bulk inserts data in tables(FileDetails,FileBlobs,ClassificationReport)
   *
   * @param dataFilePathMap map object having list of paths grouped by table name constants.
   * @return map object of rows count grouped by table name.
   */
  @Transactional(value = "filemanagerTransactionManager", propagation = Propagation.REQUIRES_NEW)
  public Map<String, Integer> importFileManagerData(
      Map<TableConstants, List<Path>> dataFilePathMap) {
    log.entry();
    Map<String, Integer> noOfRowsUpdatedByTable = new HashMap<>();

    TableConstants fileDetailsConstant = TableConstants.FILEDETAILS;
    TableConstants fileBlobConstant = TableConstants.FILEBLOBS;
    TableConstants clasfictnRptConstant = TableConstants.CLASSIFICATIONREPORT;
    int fileDetailsRowsUpdated = 0, fileBlobRowsUpdated = 0, clasfnRptRowsUpdated = 0;

    for (Path path : dataFilePathMap.get(fileDetailsConstant)) {
      fileDetailsRowsUpdated += bulkDataImportDao.bulkInsert(path, fileDetailsConstant);
    }
    for (Path path : dataFilePathMap.get(fileBlobConstant)) {
      fileBlobRowsUpdated += bulkDataImportDao.bulkInsert(path, fileBlobConstant);
    }
    for (Path path : dataFilePathMap.get(clasfictnRptConstant)) {
      clasfnRptRowsUpdated += bulkDataImportDao.bulkInsert(path, clasfictnRptConstant);
    }

    noOfRowsUpdatedByTable.put(fileDetailsConstant.getName(), fileDetailsRowsUpdated);
    noOfRowsUpdatedByTable.put(fileBlobConstant.getName(), fileBlobRowsUpdated);
    noOfRowsUpdatedByTable.put(clasfictnRptConstant.getName(), clasfnRptRowsUpdated);

    log.exit();

    return noOfRowsUpdatedByTable;

  }

  /**
   * Bulk inserts data in tables(VisionbotDetail,VisionBot,VisionBotRunDetails,TestSetInfo,DocumentProccessedDatails,TestSet)
   *
   * @param dataFilePathMap map object having list of paths grouped by table name constants.
   * @return map object of rows count grouped by table name.
   */
  @Transactional(value = "filemanagerTransactionManager", propagation = Propagation.REQUIRES_NEW)
  public Map<String, Integer> importVisionBotData(
      Map<TableConstants, List<Path>> dataFilePathMap) {
    log.entry();
    Map<String, Integer> noOfRowsUpdatedByTable = new HashMap<>();

    TableConstants visionBotDetailConstant = TableConstants.VISIONBOTDETAIL;
    TableConstants visionBotConstant = TableConstants.VISIONBOT;
    TableConstants visionBotRunDetailConstant = TableConstants.VISIONBOTRUNDETAILS;
    TableConstants testsetInfoContant = TableConstants.TESTSETINFO;
    TableConstants documentProcessedDetailConstant = TableConstants.DOCUMENTPROCCESSEDDATAILS;
    TableConstants testSetConstant = TableConstants.TESTSET;

    int visionBotDetailRowsUpdated = 0, visionBotRowsUpdated = 0, testSetRowsUpdated = 0, visionBotRunDetailRowsUpdated = 0, documentProcessedRowsUpdated = 0, testSetInfoRowsUpdated = 0;

    for (Path path : dataFilePathMap.get(visionBotDetailConstant)) {
      visionBotDetailRowsUpdated += bulkDataImportDao.bulkInsert(path, visionBotDetailConstant);
    }
    for (Path path : dataFilePathMap.get(visionBotConstant)) {
      visionBotRowsUpdated += bulkDataImportDao.bulkInsert(path, visionBotConstant);
    }
    for (Path path : dataFilePathMap.get(visionBotRunDetailConstant)) {
      visionBotRunDetailRowsUpdated += bulkDataImportDao
          .bulkInsert(path, visionBotRunDetailConstant);
    }
    for (Path path : dataFilePathMap.get(testsetInfoContant)) {
      testSetInfoRowsUpdated += bulkDataImportDao.bulkInsert(path, testsetInfoContant);
    }

    for (Path path : dataFilePathMap.get(documentProcessedDetailConstant)) {
      documentProcessedRowsUpdated += bulkDataImportDao
          .bulkInsert(path, documentProcessedDetailConstant);
    }
    for (Path path : dataFilePathMap.get(testSetConstant)) {
      testSetRowsUpdated += bulkDataImportDao.bulkInsert(path, testSetConstant);
    }
    noOfRowsUpdatedByTable.put(visionBotDetailConstant.getName(), visionBotDetailRowsUpdated);
    noOfRowsUpdatedByTable.put(visionBotConstant.getName(), visionBotRowsUpdated);
    noOfRowsUpdatedByTable.put(visionBotRunDetailConstant.getName(), visionBotRunDetailRowsUpdated);
    noOfRowsUpdatedByTable.put(testsetInfoContant.getName(), testSetInfoRowsUpdated);
    noOfRowsUpdatedByTable
        .put(documentProcessedDetailConstant.getName(), documentProcessedRowsUpdated);
    noOfRowsUpdatedByTable.put(testSetConstant.getName(), testSetRowsUpdated);

    log.exit();

    return noOfRowsUpdatedByTable;

  }

  /**
   * Bulk inserts data in table(trainingData)
   *
   * @param dataFilePathMap map object having list of paths grouped by table name constants.
   * @return map object of rows count grouped by table name.
   */
  @Transactional(value = "mlTransactionManager", propagation = Propagation.REQUIRES_NEW)
  public Map<String, Integer> importMlData(
      Map<TableConstants, List<Path>> dataFilePathMap) {
    log.entry();
    Map<String, Integer> noOfRowsUpdatedByTable = new HashMap<>();

    TableConstants trainingDataConstant = TableConstants.TRAININGDATA;

    int trainingDataRowsUpdated = 0;

    for (Path path : dataFilePathMap.get(trainingDataConstant)) {
      trainingDataRowsUpdated += bulkDataImportDao.bulkInsert(path, trainingDataConstant);
    }

    noOfRowsUpdatedByTable.put(trainingDataConstant.getName(), trainingDataRowsUpdated);

    log.exit();

    return noOfRowsUpdatedByTable;

  }

  /**
   * Bulk inserts data in tables(ContentTrainSet,LayoutTrainSet)
   *
   * @param dataFilePathMap map object having list of paths grouped by table name constants.
   * @return map object of rows count grouped by table name.
   */
  @Transactional(value = "classifierTransactionManager", propagation = Propagation.REQUIRES_NEW)
  public Map<String, Integer> importClassifierData(
      Map<TableConstants, List<Path>> dataFilePathMap) {
    log.entry();
    Map<String, Integer> noOfRowsUpdatedByTable = new HashMap<>();

    TableConstants contentTrainsetConstant = TableConstants.CONTENTTRAINSET;
    TableConstants layoutTrainsetConstant = TableConstants.LAYOUTTRAINSET;

    int contentTrainsetRowsUpdated = 0, layoutTrainsetRowsUpdated = 0;

    for (Path path : dataFilePathMap.get(contentTrainsetConstant)) {
      contentTrainsetRowsUpdated += bulkDataImportDao.bulkInsert(path, contentTrainsetConstant);
    }
    for (Path path : dataFilePathMap.get(layoutTrainsetConstant)) {
      layoutTrainsetRowsUpdated += bulkDataImportDao.bulkInsert(path, layoutTrainsetConstant);
    }

    noOfRowsUpdatedByTable.put(contentTrainsetConstant.getName(), contentTrainsetRowsUpdated);
    noOfRowsUpdatedByTable.put(layoutTrainsetConstant.getName(), layoutTrainsetRowsUpdated);

    log.exit();

    return noOfRowsUpdatedByTable;

  }

}
