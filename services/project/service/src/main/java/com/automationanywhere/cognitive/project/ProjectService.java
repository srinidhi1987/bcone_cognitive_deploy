package com.automationanywhere.cognitive.project;

import com.automationanywhere.cognitive.project.exception.ProjectUpdateException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectCreationFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectDeletedException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.exception.ProjectUpdationFailedException;
import com.automationanywhere.cognitive.project.exception.ResourceUpdationException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.exception.VisionBotUpdationFailedException;
import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.PatchRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectGetResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponseV2;
import com.automationanywhere.cognitive.project.model.dto.imports.ArchiveInfo;
import com.automationanywhere.cognitive.project.model.dto.imports.ProjectImportRequest;

/**
 * Created by Jemin.Shah on 24-11-2016.
 */
public interface ProjectService {
    List<ProjectGetResponse> getProjectDetailsByOrganizationId(String organizationId, String excludeFields) throws InvalidInputException;
    List<ProjectGetResponse> getAll(String organizationId, Map<String,String[]> queryParam) throws InvalidInputException;
    List<ProjectGetResponse> getAllMetadata(String organizationId, Map<String,String[]> queryParam) throws InvalidInputException;
    ProjectGetResponse get(String organizationId, String projectId, String excludeFields) throws NotFoundException, InvalidInputException, ProjectDeletedException;
    ProjectResponse getMetadata(String organizationId, String projectId) throws NotFoundException, InvalidInputException, ProjectDeletedException;
    ProjectResponseV2 getMetadataV2(String organizationId, String projectId) throws NotFoundException, InvalidInputException, ProjectDeletedException;
    void delete(String organizationId, String projectId, String updatedBy) throws InvalidInputException, PreconditionFailedException;
    ProjectResponse add(ProjectRequest projectRequest) throws DuplicateProjectNameException, IOException, ProjectCreationFailedException;
    void update(ProjectRequest projectRequest)
        throws NotFoundException, InvalidInputException, ProjectDeletedException, VisionBotUpdationFailedException, SystemBusyException, ProjectUpdationFailedException, CloneNotSupportedException, ResourceUpdationException;
    ProjectGetResponse update(String organizationId, String projectId, PatchRequest ... patchRequests) throws ProjectUpdateException;
    Task startProjectExport(ProjectExportRequest projectExportRequest)
        throws TimeoutException, InterruptedException;
    List<Task> getRunningTasks();
    Task startProjectImport(ProjectImportRequest projectImportRequest)
        throws ProjectImportException, PreconditionFailedException, SystemBusyException;
    List<ArchiveInfo> getArchiveDetails();
    List<Task> getSortedTaskList(String sortColumnName, String sortOrder);
}
