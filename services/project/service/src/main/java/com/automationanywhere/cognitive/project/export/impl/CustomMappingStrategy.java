/**
 *
 */
package com.automationanywhere.cognitive.project.export.impl;

import com.opencsv.bean.BeanField;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvBindByName;
import org.apache.commons.lang3.StringUtils;

/**
 * This strategy makes sure to generate a csv having right column header and with right order.
 *
 *  Note: The bean on which it is applied should contain both CsvBindByName and CsvBindByPosition annotations.
 *
 * @param <T>
 */
public class CustomMappingStrategy<T> extends ColumnPositionMappingStrategy<T> {

  @Override
  public String[] generateHeader() {
    final int numColumns = findMaxFieldIndex();
    if (!isAnnotationDriven() || numColumns == -1) {
      return super.generateHeader();
    }

    header = new String[numColumns + 1];

    BeanField beanField;
    for (int i = 0; i <= numColumns; i++) {
      beanField = findField(i);
      String columnHeaderName = extractHeaderName(beanField);
      header[i] = columnHeaderName;
    }
    return header;
  }

  private String extractHeaderName(final BeanField beanField) {
    if (beanField == null || beanField.getField() == null
        || beanField.getField().getDeclaredAnnotationsByType(CsvBindByName.class).length == 0) {
      return StringUtils.EMPTY;
    }

    final CsvBindByName bindByNameAnnotation =
        beanField.getField().getDeclaredAnnotationsByType(CsvBindByName.class)[0];
    return bindByNameAnnotation.column();
  }
}
