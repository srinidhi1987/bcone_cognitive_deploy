package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import java.util.List;
import java.util.Map;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public interface FileDetailsDao {

  public Map<String, List<String>> getFileIdsMapGroupedByProjectId(String[] projectIds,
      int isProduction);

  public List<String[]> getFileDetails(String projectId, List<String> fileIds, int isProduction);

  public List<FileDetails> getFileDetailsByEntity(String projectId, List<String> fileIds, int isProduction);

  public void batchInsertFileDetails(List<FileDetails> lstOfFileDetails);

  public void batchAddOrUpdateFileDetails(List<FileDetails> fileDetailsList);

  public List<FileDetails> getFileDetails(String projectId,List<String> fileIds);

  public List<FileDetails> getFileDetails(List<String> projectIds);
}
