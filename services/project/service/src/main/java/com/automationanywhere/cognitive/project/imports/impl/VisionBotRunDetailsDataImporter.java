package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.VisionbotRunDetailsDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.TestSetInfo;
import com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class VisionBotRunDetailsDataImporter extends
    AdvanceDataImporter<com.automationanywhere.cognitive.project.imports.csvmodel.VisionBotRunDetails> {

  private static final AALogger LOGGER = AALogger.create(VisionBotRunDetailsDataImporter.class);

  private final FileHelper fileHelper;
  private final TransitionResult transitionResult;
  private final VisionbotRunDetailsDao visionbotRunDetailsDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public VisionBotRunDetailsDataImporter(FileReader csvFileReader, FileHelper fileHelper,
      TransitionResult transitionResult, VisionbotRunDetailsDao visionbotRunDetailsDao,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.fileHelper = fileHelper;
    this.transitionResult = transitionResult;
    this.visionbotRunDetailsDao = visionbotRunDetailsDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    LOGGER.entry();

    //fetch's data from csv path
    List<VisionBotRunDetails> dataToImport = getDataFromFiles(unzippedFolderPath);

    //data prepared for import
    List<VisionBotRunDetails> finalDataToImport = filterByVisionBotId(transitionResult.getImportedVisionBotIdList(),dataToImport);

    //data inserted in batch.
    if (!finalDataToImport.isEmpty()) {
      visionbotRunDetailsDao.batchInsertVisionBotRunDetails(finalDataToImport);
    }
    //Sets data for transition result
    setDataForTransitionResult(finalDataToImport);

    //next importer get's called.
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }

    LOGGER.exit();

  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<VisionBotRunDetails> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    LOGGER.entry();
    List<VisionBotRunDetails> dataToImport = new ArrayList<>();

    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvRegexPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.VISIONBOTRUNDETAILS);
    List<Path> pathsOfVisionBotRunDetails = fileHelper
        .getAllFilesPaths(csvFolderPath, csvRegexPattern);

    dataToImport
        .addAll(DtoToDao.toVisionBotRunDetails(getDataFromCsv(pathsOfVisionBotRunDetails,
            com.automationanywhere.cognitive.project.imports.csvmodel.VisionBotRunDetails.class)));
    LOGGER.exit();

    return dataToImport;
  }

  private List<VisionBotRunDetails> filterByVisionBotId(List<String> visionBotIdList, List<VisionBotRunDetails> dataToImport){
    return dataToImport.stream().filter(visionBotRunDetails -> visionBotIdList.contains(visionBotRunDetails.getVisionbotId())).collect(
        Collectors.toList());
  }

  private void setDataForTransitionResult(List<VisionBotRunDetails> visionBotRunDetailsList){
    List<String> visionBotRunDetailsIdList = visionBotRunDetailsList.stream().map(visionBotRunDetails -> visionBotRunDetails.getId()).collect(Collectors.toList());
    transitionResult.setImportedVisionBotRunDetailIdList(visionBotRunDetailsIdList);
  }
}
