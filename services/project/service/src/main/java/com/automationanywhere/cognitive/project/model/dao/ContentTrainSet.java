package com.automationanywhere.cognitive.project.model.dao;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ContentTrainSet")
public class ContentTrainSet {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private int id;

  @Column(name = "training")
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "training")
  private String training;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTraining() {
    return training;
  }

  public void setTraining(String training) {
    this.training = training;
  }
}
