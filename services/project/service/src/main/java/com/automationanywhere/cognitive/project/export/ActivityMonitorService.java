package com.automationanywhere.cognitive.project.export;

import java.util.concurrent.TimeoutException;

public interface ActivityMonitorService {
    boolean isClassifierInProgress();
    boolean isStagingDocumentsInProgress();
    boolean isProductionDocumentsInProgress();
    boolean isBotLocked();
}
