package com.automationanywhere.cognitive.project.task.impl;

import static com.automationanywhere.cognitive.project.constants.DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.ProjectController;
import com.automationanywhere.cognitive.project.Util.DateUtil;
import com.automationanywhere.cognitive.project.dal.TasksDao;
import com.automationanywhere.cognitive.project.export.ExportService;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.TaskStatus;
import com.automationanywhere.cognitive.project.model.TaskType;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.task.TaskService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


public class TaskServiceImpl implements TaskService {

  @Autowired
  TasksDao tasksDao;
  
  @Autowired
  private ExportService exportService;
  
  private static final AALogger log = AALogger.create(TaskServiceImpl.class);

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  @Override
  public List<Task> getAllInProgressTasks() {
    return tasksDao.getRunningTasks();
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  @Override
  public Task createExportTask(String userId, String exportFilePath) {
    Task task = new Task();
    task.setTaskId(UUID.randomUUID().toString());
    task.setTaskType(TaskType.exports);
    task.setStatus(TaskStatus.inprogress);
    task.setTaskOptions(TaskOptions.NONE);
    task.setDescription(exportFilePath);
    task.setUserId(userId);
    task.setStartTime(DateUtil.now());
    tasksDao.addTask(task);
    return task;
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  @Override
  public Task createImportTask(String userId, String filepath, TaskOptions taskOptions) {
    Task task = new Task();
    task.setTaskId(UUID.randomUUID().toString());
    task.setTaskType(TaskType.imports);
    task.setStatus(TaskStatus.inprogress);
    task.setTaskOptions(taskOptions);
    task.setDescription(filepath);
    task.setUserId(userId);
    task.setStartTime(DateUtil.now());
    tasksDao.addTask(task);
    return task;
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  @Override
  public void markAsFailed(Task task) {
    task.setStatus(TaskStatus.failed);
    task.setEndTime(DateUtil.now());
    tasksDao.updateTask(task);
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  @Override
  public void markAsCompleted(Task task) {
    task.setStatus(TaskStatus.complete);
    task.setEndTime(DateUtil.now());
    tasksDao.updateTask(task);
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  @Override
  public List<Task> getSortedTaskList(String sortColumnName, String sortOrder) {
    return tasksDao.getSortedTaskList(sortColumnName, sortOrder);
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRES_NEW)
  @Override
  public void getAllInProgressTasksAndMarkThemFailed() {
    List<Task> taskList = tasksDao.getRunningTasks();
    if (taskList.size() > 0) {
	      for (Task task : taskList) {
	        log.info(String.format("Marking task as failed: Id - %s Type - %s", task.getTaskId(), task.getTaskType().toString()));
	        if(task.getTaskType().equals(TaskType.exports))
	        {
	          exportService.cleanUp(task.getDescription(), true);
	        }
	        markAsFailed(task);
	      }
	  }
  }

  @Transactional(value = FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  @Override
  public void createFailedImportTask(String userId, String filepath, TaskOptions taskOptions) {
    Task task = new Task();
    task.setTaskId(UUID.randomUUID().toString());
    task.setStartTime(DateUtil.now());
    task.setTaskType(TaskType.imports);
    task.setStatus(TaskStatus.failed);
    task.setTaskOptions(taskOptions);
    task.setDescription(filepath);
    task.setUserId(userId);
    task.setEndTime(DateUtil.now());
    tasksDao.addTask(task);
  }
}
