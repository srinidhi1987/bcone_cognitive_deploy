package com.automationanywhere.cognitive.project.export;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FileWriter {

  void writeData(Path path, List<String[]> data)
      throws IOException;
  <T> void writeData(Path path, Class<T> theClass, List<T> data) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException;
}
