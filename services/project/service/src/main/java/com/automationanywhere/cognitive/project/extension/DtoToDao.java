package com.automationanywhere.cognitive.project.extension;

import com.automationanywhere.cognitive.project.imports.csvmodel.FileBlob;
import com.automationanywhere.cognitive.project.imports.csvmodel.FileDetail;
import com.automationanywhere.cognitive.project.imports.csvmodel.trainingData;
import com.automationanywhere.cognitive.project.model.constants.RunState;
import com.automationanywhere.cognitive.project.model.dao.ClassificationReport;
import com.automationanywhere.cognitive.project.model.dao.TrainingData;
import com.automationanywhere.cognitive.project.model.dao.VisionBotRunDetails;
import com.automationanywhere.cognitive.project.model.dao.imports.CustomFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.FileBlobs;
import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.TestSetInfo;
import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail.PrimaryKey;
import com.automationanywhere.cognitive.project.model.dto.CustomField;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.sql.rowset.serial.SerialBlob;
import com.automationanywhere.cognitive.project.imports.csvmodel.CustomFieldDetailCsv;
import com.automationanywhere.cognitive.project.imports.csvmodel.ProjectOCREngineDetailsMasterCsv;
import com.automationanywhere.cognitive.project.imports.csvmodel.StandardFieldDetailCsv;
import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.FieldType;
import com.automationanywhere.cognitive.project.model.ProjectState;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailsModel;
import com.automationanywhere.cognitive.project.model.dao.ProjectOCREngineDetailsMasterMapping;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 * Created by Jemin.Shah on 02-12-2016.
 */
public class DtoToDao {

  public static ProjectDetail toProjectDetail(ProjectRequest projectRequest) {
    ProjectDetail projectDetail = new ProjectDetail();
    projectDetail.setId(projectRequest.getId());
    projectDetail.setName(projectRequest.getName());
    projectDetail.setDescription(projectRequest.getDescription());
    projectDetail.setOrganizationId(projectRequest.getOrganizationId());
    projectDetail.setPrimaryLanguage(projectRequest.getPrimaryLanguage());
    projectDetail.setProjectType(projectRequest.getProjectType());
    projectDetail.setProjectTypeId(projectRequest.getProjectTypeId());
    projectDetail.setEnvironment(projectRequest.getEnvironment());
    projectDetail.setState(projectRequest.getProjectState());

    List<com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail> standardFieldDetailList = DtoToDao
        .toStandardFieldDetailList(projectRequest.getFields().getStandard(), projectDetail);
    int order = 1;
    for (com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail standardFieldDetail : standardFieldDetailList) {
      standardFieldDetail.getPrimaryKey().setProjectDetail(projectDetail);
      standardFieldDetail.setOrder(order++);
    }
    Set<com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail> standardFieldDetailSet = new HashSet<>(
        standardFieldDetailList);
    projectDetail.setStandardFieldDetailSet(standardFieldDetailSet);

    order = 1;
    List<com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail> customFieldDetailList = new ArrayList<>();
    for (CustomField customField : projectRequest.getFields().getCustom()) {
      com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail customFieldDetail = DtoToDao
          .toCustomFieldDetail(customField, projectDetail);
      customFieldDetail.setOrder(order++);
      customFieldDetailList.add(customFieldDetail);
    }
    Set<com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail> customFieldDetailSet = new HashSet<>(
        customFieldDetailList);
    projectDetail.setCustomFieldDetailSet(customFieldDetailSet);
    projectDetail.setOcrEngineDetails(projectRequest.getOcrEngineDetails());
    return projectDetail;
  }

  public static ProjectDetail toProjectDetail(ProjectDetail actualProjectDetail,
      ProjectRequest projectRequest) {
    ProjectDetail updatedProjectDetail = new ProjectDetail();
    updatedProjectDetail.setId(projectRequest.getId());
    updatedProjectDetail.setName(projectRequest.getName());
    updatedProjectDetail.setDescription(projectRequest.getDescription());
    updatedProjectDetail.setOrganizationId(projectRequest.getOrganizationId());
    updatedProjectDetail.setPrimaryLanguage(projectRequest.getPrimaryLanguage());
    updatedProjectDetail.setProjectType(projectRequest.getProjectType());
    updatedProjectDetail.setProjectTypeId(projectRequest.getProjectTypeId());
    updatedProjectDetail.setEnvironment(projectRequest.getEnvironment());
    updatedProjectDetail.setState(projectRequest.getProjectState());

    List<com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail> standardFieldDetailList = DtoToDao
        .toStandardFieldDetailList(projectRequest.getFields().getStandard(), actualProjectDetail);
    int order = 1;
    for (com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail standardFieldDetail : standardFieldDetailList) {
      standardFieldDetail.setOrder(order++);
    }
    Set<com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail> standardFieldDetailSet = new HashSet<>(
        standardFieldDetailList);
    updatedProjectDetail.setStandardFieldDetailSet(standardFieldDetailSet);

    order = 1;
    List<com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail> customFieldDetailList = new ArrayList<>();
    for (CustomField customField : projectRequest.getFields().getCustom()) {
      com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail customFieldDetail = DtoToDao
          .toCustomFieldDetail(customField, actualProjectDetail);
      customFieldDetail.setOrder(order++);
      customFieldDetailList.add(customFieldDetail);
    }
    Set<com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail> customFieldDetailSet = new HashSet<>(
        customFieldDetailList);
    updatedProjectDetail.setCustomFieldDetailSet(customFieldDetailSet);
    updatedProjectDetail.setOcrEngineDetails(projectRequest.getOcrEngineDetails());
    updatedProjectDetail.setVersionId(projectRequest.getVersionId());
    return updatedProjectDetail;
  }

  public static com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail toCustomFieldDetail(CustomField customField,
      ProjectDetail projectDetail) {
    com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail customFieldDetail = new com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail();
    String customFieldId =
        customField.getId() == null ? UUID.randomUUID().toString() : customField.getId();
    customFieldDetail.setType(customField.getType());
    customFieldDetail.setName(customField.getName());
    customFieldDetail.setId(customFieldId);
    customFieldDetail.setProjectDetail(projectDetail);
    return customFieldDetail;
  }

  public static List<com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail> toStandardFieldDetailList(String[] standardFields,
      ProjectDetail projectDetail) {
    List<com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail> standardFieldDetailList = new ArrayList<>();
    for (String id : standardFields) {
      com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail standardFieldDetail = new com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail();
      standardFieldDetail.getPrimaryKey().setId(id);
      standardFieldDetail.getPrimaryKey().setProjectDetail(projectDetail);
      standardFieldDetailList.add(standardFieldDetail);
    }

        return standardFieldDetailList;
    }
    public static List<FileDetails> toFileDetails(List<FileDetail> fileDetailList){
        List<FileDetails> fileDetailsList = new ArrayList<>();

        for(FileDetail fileDetail:fileDetailList){
        FileDetails fileDetails = new FileDetails();
        fileDetails.setFileId(fileDetail.getFileId());
        fileDetails.setProjectId(fileDetail.getProjectId());
        fileDetails.setFileName(fileDetail.getFileName());
        fileDetails.setFileLocation(fileDetail.getFileLocation());
        fileDetails.setFileSize(fileDetail.getFileSize());
        fileDetails.setFileHeight(fileDetail.getFileHeight());
        fileDetails.setFileWidth(fileDetail.getFileWidth());
        fileDetails.setFormat(fileDetail.getFormat());
        fileDetails.setProcessed(fileDetail.isProcessed());
        fileDetails.setClassificationId(fileDetail.getClassificationId());
        fileDetails.setUploadrequestId(fileDetail.getUploadrequestId());
        fileDetails.setLayoutId(fileDetail.getLayoutId());
        fileDetails.setProduction(fileDetail.isProduction());
        fileDetailsList.add(fileDetails);
        }
      return fileDetailsList;
    }

    public static List<FileBlobs> toFileBlobs(List<FileBlob> fileBlobList)
        throws SQLException, DecoderException {
        List<FileBlobs> fileBlobsList = new ArrayList<>();
        for(FileBlob fileBlob:fileBlobList){
          FileBlobs fileBlobs = new FileBlobs();
          fileBlobs.setFileId(fileBlob.getFileId());
          fileBlobs.setFileBlob(new SerialBlob(Hex.decodeHex(fileBlob.getFileBlob().toCharArray())));
          fileBlobsList.add(fileBlobs);
        }
        return fileBlobsList;
    }

    public static List<ClassificationReport> toClassificationReport(List<com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport> classificationReportList) {
        List<ClassificationReport> classificationReportList_data = new ArrayList<>();
        for (com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport classificationReport : classificationReportList) {
            ClassificationReport classificationReport_data = new ClassificationReport();
            classificationReport_data.setDocumentId(classificationReport.getDocumentId());
            classificationReport_data.setFieldId(classificationReport.getFieldId());
            classificationReport_data.setAliasName(classificationReport.getAliasName());
            classificationReport_data.setFound(classificationReport.isFound());
            classificationReportList_data.add(classificationReport_data);
        }
        return classificationReportList_data;
    }
    public static List<ProjectDetailsModel> toProjectDetailsList(List<com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail> projectDetailCsvModel)
            throws ParseException {
        List<ProjectDetailsModel> projectDetailsDao = new ArrayList<>();
        for(com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail projectDetail: projectDetailCsvModel)
        {
            ProjectDetailsModel model = new ProjectDetailsModel();
            model.setId(projectDetail.getId());
            model.setName(projectDetail.getName());
            model.setDescription(projectDetail.getDescription());
            model.setOrganizationId(projectDetail.getOrganizationId());
            model.setProjectType(projectDetail.getType());
            model.setProjectTypeId(projectDetail.getTypeid());
            model.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
            model.setFileUploadToken(projectDetail.getFileUploadToken());
            model.setFileUploadTokenAlive(Boolean.parseBoolean(projectDetail.getFileUploadTokenAlive()));
            model.setCreatedBy(projectDetail.getCreatedBy());
            model.setLastUpdatedBy(projectDetail.getLastUpdatedBy());
            model.setVersionId(projectDetail.getVersionId());
            for(ProjectState state : ProjectState.values())
            {
                if(state.toString().equals(projectDetail.getState()))
                {
                    model.setState(state);
                }
            }
            for(Environment env : Environment.values())
            {
                if(env.toString().equals(projectDetail.getEnvironment()))
                {
                    model.setEnvironment(env);
                }
            }
            model.setCreatedAt(new Date());
            model.setUpdatedAt(new Date());

      projectDetailsDao.add(model);
    }
    return projectDetailsDao;
  }

  public static List<StandardFieldDetail> toStandardFieldDetailList
      (List<StandardFieldDetailCsv> standardFieldDetailCsvModel) {
    Set<StandardFieldDetail> standardFieldDetailList =
        standardFieldDetailCsvModel.stream().map(model -> new StandardFieldDetail
            (new PrimaryKey(model.getId(), model.getProjectId()), model.getOrder(),
                model.isAddedLater())).collect(
            Collectors.toSet());
    return standardFieldDetailList.stream().collect(Collectors.toList());
  }

    public static List<CustomFieldDetail> toCustomFieldDetailList(List<CustomFieldDetailCsv> customFieldDetailCsvList)
    {
        List<CustomFieldDetail> customFieldDetailList = new ArrayList<>();
        for(CustomFieldDetailCsv model: customFieldDetailCsvList)
        {
          CustomFieldDetail customFieldDetail = new CustomFieldDetail();
            customFieldDetail.setId(model.getId());
            customFieldDetail.setName(model.getName());
            customFieldDetail.setOrder(model.getOrder());

          customFieldDetail.setType(
              model.getFieldType().equalsIgnoreCase(FieldType.FormField.toString())
                  ? FieldType.FormField : FieldType.TableField);
            customFieldDetail.setProjectId(model.getProjectId());
            customFieldDetail.setAddedLater(model.isAddedLater());
            customFieldDetailList.add(customFieldDetail);
        }
        return customFieldDetailList;

  }

  public static ProjectOCREngineDetailsMasterMapping toProjectOCREngineDetailsMaster(
      ProjectOCREngineDetailsMasterCsv projectOcrEngineMastercsvModel) {
    ProjectOCREngineDetailsMasterMapping projectOCREngineMaster = new ProjectOCREngineDetailsMasterMapping();
    projectOCREngineMaster.setId(projectOcrEngineMastercsvModel.getId());
    projectOCREngineMaster.setProjectId(projectOcrEngineMastercsvModel.getProjectId());
    projectOCREngineMaster
        .setOcrEngineDetailsId(projectOcrEngineMastercsvModel.getOcrEngineDetailsId());
    return projectOCREngineMaster;
  }

  public static List<TestSetInfo> toTestSetInfos(List<com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo> testSetInfoList) {
    List<TestSetInfo> testSetInfos = new ArrayList<>();
    for (com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo testSetInfo : testSetInfoList) {
      TestSetInfo testSetInfoObj = new TestSetInfo();
      testSetInfoObj.setId(testSetInfo.getId());
      testSetInfoObj.setVisionbotid(testSetInfo.getVisionbotid());
      testSetInfoObj.setLayoutid(testSetInfo.getLayoutid());
      testSetInfos.add(testSetInfoObj);
    }
    return testSetInfos;
  }
  public static List<VisionBotRunDetails> toVisionBotRunDetails(List<com.automationanywhere.cognitive.project.imports.csvmodel.VisionBotRunDetails> visionBotRunDetailsList){
    return visionBotRunDetailsList.stream().map(visionBotRunDetails -> {
      VisionBotRunDetails visionBotRunDetailsObj=new VisionBotRunDetails();
      visionBotRunDetailsObj.setId(visionBotRunDetails.getId());
      visionBotRunDetailsObj.setVisionbotId(visionBotRunDetails.getVisionbotid());;
      visionBotRunDetailsObj.setCreatedAt(new Date());
      visionBotRunDetailsObj.setEnvironment(
          com.automationanywhere.cognitive.project.model.constants.Environment.valueOf(visionBotRunDetails.getEnvironment()));
      visionBotRunDetailsObj.setRunState(RunState.getRunStateByOrdinal(Integer.parseInt(visionBotRunDetails.getRunstateid())));
      return  visionBotRunDetailsObj;
    }).collect(Collectors.toList());
  }

  public static List<TrainingData> toTrainingData(List<trainingData> trainingDataList){
    return trainingDataList.stream().map(trainingData -> {
      TrainingData trainingDataObj=new TrainingData();
      trainingDataObj.setOriginalValue(trainingData.getOriginalvalue());
      trainingDataObj.setCorrectedValue(trainingData.getCorrectedvalue());
      trainingDataObj.setFieldId(trainingData.getFieldid());
      return trainingDataObj;
    }).collect(Collectors.toList());
  }
}
