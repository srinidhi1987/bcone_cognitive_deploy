package com.automationanywhere.cognitive.project.model;

/**
 * Created by Jemin.Shah on 7/4/2017.
 */
public enum ActivityTypes {
    Delete(1);

    private final int value;

    ActivityTypes(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
