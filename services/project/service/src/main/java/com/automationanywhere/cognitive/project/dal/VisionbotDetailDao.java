package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.VisionBotDetail;
import java.util.List;


public interface VisionbotDetailDao {

  public List<String[]> getVisionbotDetails(String projectId);

  public List<String> getVisionbotDetailIds(String projectId);

  public List<VisionBotDetail> getAll(String projectId);

  public void addAll(List<VisionBotDetail> visionBotDetailList);

  public void delete(String projectId);

  public boolean isBotLocked();
}
