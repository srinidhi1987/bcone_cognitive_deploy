package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.ErrorMessageConstants;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.ActivityMonitorService;
import com.automationanywhere.cognitive.project.export.DataExportListener;
import com.automationanywhere.cognitive.project.export.ExportOperator;
import com.automationanywhere.cognitive.project.export.ExportService;
import com.automationanywhere.cognitive.project.export.model.*;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.helper.ProductReleaseFileReader;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportRequest;
import com.automationanywhere.cognitive.project.task.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ExportServiceImpl implements ExportService {

  private static final ExecutorService executor = Executors.newFixedThreadPool(1);
  private AALogger log = AALogger.create(this.getClass());
  private static final String EXPORT_EXTENSION = "iqba";

  @Inject
  @Qualifier("projectDataExportOperator")
  private ExportOperator projectDataExportOperator;

  @Inject
  @Qualifier("fileDataExportOperator")
  private ExportOperator fileDataExportOperator;

  @Inject
  @Qualifier("visionBotDataExportOperator")
  private ExportOperator visionBotDataExportOperator;

  @Inject
  @Qualifier("classifierDataExportOperator")
  private ExportOperator classifierDataExportOperator;

  @Inject
  @Qualifier("mlDataExportOperator")
  private ExportOperator mlDataExportOperator;

  @Inject
  private FileZipper fileZipper;

  @Inject
  private ProjectDetailDao projectDetailDao;

  @Inject
  private TaskService taskService;

  @Inject
  private ActivityMonitorService activityMonitorService;
  
  @Autowired
  private FileHelper fileHelper;

  @Autowired
  private ProductReleaseFileReader productReleaseFileReader;

  @Override
  public Task startExport(String path, ProjectExportRequest projectExportRequest) {
    checkForActivities();
    validateProjectExportRequest(projectExportRequest.getProjectExportDetails());
    Task task = taskService.createExportTask(projectExportRequest.getUserId(), path);
    File file = new File(path);
    executor.execute(new DataExportTask(file, projectExportRequest, task));
    return task;
  }

  private void checkForActivities(){
    if (activityMonitorService.isClassifierInProgress()) {
      throw new ProjectExportException(ErrorMessageConstants.CLASSIFICATION_INPROGRESS);
    }

    if (activityMonitorService.isStagingDocumentsInProgress()) {
      throw new ProjectExportException(ErrorMessageConstants.DATAEXTRACTION_INPROGRESS);
    }

    if (activityMonitorService.isBotLocked()) {
      throw new ProjectExportException(ErrorMessageConstants.VISION_BOT_UNDER_TRAINING);
    }
  }

  private void validateProjectExportRequest(String[] projectIds) {
    List<String> nonExistingProjectIds = new ArrayList<>();
    for (String projectId : projectIds) {
      if (!projectDetailDao.projectExists(projectId)) {
        nonExistingProjectIds.add(projectId);
        continue;
      }
    }

    if (nonExistingProjectIds.size() > 0) {
      ProjectExportException projectExportException = new ProjectExportException(
          "Bad Export Request");
      projectExportException.setProjectIdsNotFound(nonExistingProjectIds);
      throw projectExportException;
    }
  }

  public void cleanUp(String exportFilePath, boolean enableFileCleanUp) {
    File outputDirectory = new File(exportFilePath);
    if(outputDirectory.isDirectory() && outputDirectory.exists())
    {
      try {
        Files.walk(Paths.get(exportFilePath))
                .map(Path::toFile)
                .sorted((o1, o2) -> -o1.compareTo(o2))
                .forEach(File::delete);
      } catch (IOException e) {
        log.error(e);
      }
    }

    File outputFile = new File(String.format("%s.%s",exportFilePath, EXPORT_EXTENSION));
    if(enableFileCleanUp && outputFile.exists()) {
      boolean success = outputFile.delete();
      if (!success) {
        log.error("Error to delete file {}", outputFile.getAbsolutePath());
      }
    }
  }

  private class DataExportTask implements Runnable, DataExportListener {

    boolean isProjectExportCompleted = false;
    boolean isFileDataExportCompleted = false;
    boolean isVisionBotDataExportCompleted = false;
    boolean isClassifierDataExportCompleted = false;
    boolean isMlDataExportCompleted = false;
    boolean isExportFailed = false;
    private ProjectExportRequest projectExportRequest;
    private Task exportTask;
    private String outputDirPath;
    private String exportFilePath;
    private List<ProjectDataExportMetadata> projectDataExportMetadata;
    private List<FileDataExportMetadata> fileDataExportMetadata;
    private List<VisionBotDataExportMetadata> visionBotDataExportMetadata;
    private List<ClassifierDataExportMetadata> classifierDataExportMetadata;
    private List<MlDataExportMetadata> mlDataExportMetadata;


    public DataExportTask(File file, ProjectExportRequest projectExportRequest,
        Task exportTask) {
      this.outputDirPath = file.getParent();
      this.exportFilePath = Paths.get(outputDirPath, file.getName()).toString();
      this.projectExportRequest = projectExportRequest;
      this.exportTask = exportTask;
    }


    @Override
    public void run() {
      prepareExportDirectory();
      String[] projectIds = projectExportRequest.getProjectExportDetails();
      projectDataExportOperator.export(exportFilePath, projectIds, this);
      fileDataExportOperator.export(exportFilePath, projectIds, this);
      visionBotDataExportOperator.export(exportFilePath, projectIds, this);
      classifierDataExportOperator.export(exportFilePath, projectIds, this);
      mlDataExportOperator.export(exportFilePath, projectIds, this);
    }

    private void prepareExportDirectory() {
      File file = new File(exportFilePath);
      if (!file.exists()) {
        file.mkdirs();
      }
    }

    @Override
    public void onProjectDataExportCompleted(List<ProjectDataExportMetadata> metadata) {
      projectDataExportMetadata = metadata;
      isProjectExportCompleted = true;
      log.info("Project data export completed.");
      onExportCompleted();
    }

    @Override
    public void onFileDataExportCompleted(List<FileDataExportMetadata> metadata) {
      fileDataExportMetadata = metadata;
      isFileDataExportCompleted = true;
      log.info("File data export completed.");
      onExportCompleted();
    }

    @Override
    public void onVisionBotDataExportCompleted(List<VisionBotDataExportMetadata> metadata) {
      visionBotDataExportMetadata = metadata;
      isVisionBotDataExportCompleted = true;
      log.info("Visionbot data export completed.");
      onExportCompleted();
    }

    @Override
    public void onClassifierDataExportCompleted(List<ClassifierDataExportMetadata> metadata) {
      classifierDataExportMetadata = metadata;
      isClassifierDataExportCompleted = true;
      log.info("Classifier data export completed.");
      onExportCompleted();
    }

    @Override
    public void onMlDataExportCompleted(List<MlDataExportMetadata> metadata) {
      mlDataExportMetadata = metadata;
      isMlDataExportCompleted = true;
      log.info("Ml data export completed.");
      onExportCompleted();

    }


    private boolean isExportComplete() {
      return isFileDataExportCompleted && isProjectExportCompleted && isVisionBotDataExportCompleted
          && isClassifierDataExportCompleted && isMlDataExportCompleted;
    }

    private void onExportCompleted() {
      synchronized (this) {
          if (!isExportFailed && isExportComplete()) {
            collateMetadata();
            archiveExportData();
            cleanUp(exportFilePath, false);
            log.info("Export completed.");
            taskService.markAsCompleted(exportTask);
          }
        }
    }

    @Override
    public void onException(Exception e) {
        log.error(e.getMessage(), e);
        isExportFailed = true;
        // TODO: stop all tasks
        cleanUp(exportFilePath, true);
        taskService.markAsFailed(exportTask);
    }

    private void collateMetadata() {
      Optional<ProjectDataExportMetadata> aggreGatedProjectMetadata = projectDataExportMetadata
          .stream()
          .reduce((ProjectDataExportMetadata a,
              ProjectDataExportMetadata b) -> new ProjectDataExportMetadata(
              a.getProjectDetailsRowCount() + b.getProjectDetailsRowCount(),
              a.getProjectOcrEngineDetailsRowCount() + b.getProjectOcrEngineDetailsRowCount(),
              a.getCustomFieldDetailRowCount() + b.getCustomFieldDetailRowCount(),
              a.getStandardFieldDetailRowCount() + b.getStandardFieldDetailRowCount()));

      Optional<ClassifierDataExportMetadata> aggreGatedClassifierMetadata = classifierDataExportMetadata
          .stream()
          .reduce((ClassifierDataExportMetadata a,
              ClassifierDataExportMetadata b) -> new ClassifierDataExportMetadata(
              a.getContentTrainSetRowCount() + b.getContentTrainSetRowCount(),
              a.getLayoutTrainSetRowCount() + b.getLayoutTrainSetRowCount()));

      Optional<FileDataExportMetadata> aggreGatedFileDataMetadata = fileDataExportMetadata.stream()
          .reduce((FileDataExportMetadata a,
              FileDataExportMetadata b) -> new FileDataExportMetadata(
              a.getFileDetailsRowCount() + b.getFileDetailsRowCount(),
              a.getFileBlobRowCount() + b.getFileBlobRowCount(),
              a.getClassificationReportRowCount() + b.getClassificationReportRowCount()));

      Optional<VisionBotDataExportMetadata> aggreGatedVisionBotMetadata = visionBotDataExportMetadata
          .stream()
          .reduce((VisionBotDataExportMetadata a,
              VisionBotDataExportMetadata b) -> new VisionBotDataExportMetadata(
              a.getVisionbotDetailRowCount() + b.getVisionbotDetailRowCount(),
              a.getVisionbotRowCount() + b.getVisionbotRowCount(),
              a.getVisionbotRunDetailsRowCount() + b.getVisionbotRunDetailsRowCount(),
              a.getTestSetInfoRowCount() + b.getTestSetInfoRowCount(),
              a.getDocumentProcessedDetailRowCount() + b.getDocumentProcessedDetailRowCount(),
              a.getTestSetRowCount() + b.getTestSetRowCount()));

      Optional<MlDataExportMetadata> aggreGatedMlDataMetadata = mlDataExportMetadata.stream()
          .reduce((MlDataExportMetadata a,
              MlDataExportMetadata b) -> new MlDataExportMetadata(
              a.getTrainingDataRowCount() + b.getTrainingDataRowCount()));

      MetaData metaData = new MetaData(aggreGatedProjectMetadata.get(),
          aggreGatedFileDataMetadata.get(), aggreGatedClassifierMetadata.get(),
          aggreGatedVisionBotMetadata.get(), aggreGatedMlDataMetadata.get(),
          productReleaseFileReader.getCurrentProductVersion());
      writeToJsonFile(exportFilePath + "\\" + "metadata.json", metaData);
      log.info("Collation of metadata completed.");
    }

    private void archiveExportData() {
      try {
        Path inputFilePath=Paths.get(exportFilePath);
        Path outputFilePath=Paths.get(outputDirPath,String.format("%s.%s", inputFilePath.getFileName(),EXPORT_EXTENSION));
        fileZipper.zipIt(inputFilePath, outputFilePath);

        log.info("Zipping completed.");
      } catch (IOException e) {
        log.error("Got error whlie zipping file. ",e);
      }
    }

    private void writeToJsonFile(String path, Object object) {
      ObjectMapper objectMapper = new ObjectMapper();
      try {
        objectMapper.writeValue(new File(path), object);
      } catch (IOException ex) {
        log.error("Got error while writing json to file at path : " + path, ex);
      }
    }
  }

}
