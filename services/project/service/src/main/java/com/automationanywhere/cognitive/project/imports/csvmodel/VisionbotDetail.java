package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class VisionbotDetail {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "organizationid")
  private String organizationid;
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "projectid")
  private String projectid;
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "classificationid")
  private String classificationid;
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "environment")
  private String environment;
  @CsvBindByPosition(position = 5)
  @CsvBindByName(column = "lockeduserid")
  private String lockeduserid;

  public VisionbotDetail() {

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOrganizationid() {
    return organizationid;
  }

  public void setOrganizationid(String organizationid) {
    this.organizationid = organizationid;
  }

  public String getProjectid() {
    return projectid;
  }

  public void setProjectid(String projectid) {
    this.projectid = projectid;
  }

  public String getClassificationid() {
    return classificationid;
  }

  public void setClassificationid(String classificationid) {
    this.classificationid = classificationid;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getLockeduserid() {
    return lockeduserid;
  }

  public void setLockeduserid(String lockeduserid) {
    this.lockeduserid = lockeduserid;
  }

  @Override
  public String toString() {
    return "VisionbotDetail{" + "id='" + id + '\'' + ", organizationid='" + organizationid + '\''
        + ", projectid='" + projectid + '\'' + ", classificationid='" + classificationid + '\''
        + ", environment='" + environment + '\'' + ", lockeduserid='" + lockeduserid + '\'' + '}';
  }
}
