package com.automationanywhere.cognitive.project.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jemin.Shah on 3/4/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BotRunDetails
{
    private String id;
    private String runningMode;
    private int totalFiles;
    private int processedDocumentCount;
    private int failedDocumentCount;
    private int passedDocumentCount;
    private double documentProcessingAccuracy;
    private double totalFieldCount;
    private double passedFieldCount;
    private double failedFieldCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRunningMode() {
        return runningMode;
    }

    public void setRunningMode(String runningMode) {
        this.runningMode = runningMode;
    }

    public int getTotalFiles() {
        return totalFiles;
    }

    public void setTotalFiles(int totalFiles) {
        this.totalFiles = totalFiles;
    }

    public int getProcessedDocumentCount() {
        return processedDocumentCount;
    }

    public void setProcessedDocumentCount(int processedDocumentCount) {
        this.processedDocumentCount = processedDocumentCount;
    }

    public int getFailedDocumentCount() {
        return failedDocumentCount;
    }

    public void setFailedDocumentCount(int failedDocumentCount) {
        this.failedDocumentCount = failedDocumentCount;
    }

    public int getPassedDocumentCount() {
        return passedDocumentCount;
    }

    public void setPassedDocumentCount(int passedDocumentCount) {
        this.passedDocumentCount = passedDocumentCount;
    }

    public double getDocumentProcessingAccuracy() {
        return documentProcessingAccuracy;
    }

    public void setDocumentProcessingAccuracy(double documentProcessingAccuracy) {
        this.documentProcessingAccuracy = documentProcessingAccuracy;
    }

    public double getTotalFieldCount() {
        return totalFieldCount;
    }

    public void setTotalFieldCount(double totalFieldCount) {
        this.totalFieldCount = totalFieldCount;
    }

    public double getPassedFieldCount() {
        return passedFieldCount;
    }

    public void setPassedFieldCount(double passedFieldCount) {
        this.passedFieldCount = passedFieldCount;
    }

    public double getFailedFieldCount() {
        return failedFieldCount;
    }

    public void setFailedFieldCount(int failedFieldCount) {
        this.failedFieldCount = failedFieldCount;
    }

    @Override
    public String toString() {
        return "BotRunDetails{" +
                "id='" + id + '\'' +
                ", runningMode='" + runningMode + '\'' +
                ", totalFiles=" + totalFiles +
                ", processedDocumentCount=" + processedDocumentCount +
                ", failedDocumentCount=" + failedDocumentCount +
                ", passedDocumentCount=" + passedDocumentCount +
                ", documentProcessingAccuracy=" + documentProcessingAccuracy +
                ", totalFieldCount=" + totalFieldCount +
                ", passedFieldCount=" + passedFieldCount +
                ", failedFieldCount=" + failedFieldCount +
                '}';
    }
}
