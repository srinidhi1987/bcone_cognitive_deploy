package com.automationanywhere.cognitive.project.export.Exporter;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.ClassificationReportDao;
import com.automationanywhere.cognitive.project.dal.FileBlobDao;
import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.model.FileDataExportMetadata;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class FileManagerDataExporter implements DataExporter<FileDataExportMetadata> {

  @Inject
  private FileDetailsDao fileDetailsDao;

  @Inject
  private FileBlobDao fileBlobDao;

  @Inject
  private ClassificationReportDao classificationReportDao;

  @Inject
  private FileWriter csvFileWriter;

  private AALogger log = AALogger.create(FileManagerDataExporter.class);

  @Transactional(value = "filemanagerTransactionManager", propagation = Propagation.REQUIRES_NEW)
  @Override
  public FileDataExportMetadata exportData(String path, Map<Object, Object> params)
      throws ProjectExportException {
    log.entry();
    FileDataExportMetadata fileDataExportMetadata;
    try {

      @SuppressWarnings("unchecked")
      Map.Entry<String, List<String>> projectFileEntry = (Map.Entry<String, List<String>>) params
          .get("FileIdByProjectIdEntry");
      String projectId = projectFileEntry.getKey().toString();
      List<String> fileIds = projectFileEntry.getValue();

      int batchId = (int) params.get("batchId");
      int isProduction = (int) params.get("isProduction");

      String projectExportDirPath = prepareProjectDirectory(path, projectId);

      long fileDetailsRowCount = exportFileDetailsData(projectExportDirPath, batchId, projectId,
          fileIds, isProduction);
      long fileBlobRowCount = exportFileBlobData(projectExportDirPath, batchId, projectId, fileIds);
      long classificationReportRowCount = exportClassificationReportData(projectExportDirPath,
          batchId, projectId, fileIds);

      fileDataExportMetadata = new FileDataExportMetadata(fileDetailsRowCount, fileBlobRowCount,
          classificationReportRowCount);
    } catch (Exception e) {
      throw new ProjectExportException("Got error while exporting file related data to csv.", e);
    }
    log.exit();
    return fileDataExportMetadata;
  }

  private long exportFileDetailsData(String projectExportDirPath,
      int batchid, String projectId, List<String> fileIds, int isProduction) throws IOException {
    log.entry();
    long fileDetailsRowCount = 0l;
    List<String[]> fileDetailsData = fileDetailsDao
        .getFileDetails(projectId, fileIds, isProduction);

    if (fileDetailsData != null && !fileDetailsData.isEmpty()) {
      String[] headerData = {"fileid", "projectid", "filename", "filelocation", "filesize",
          "fileheight", "filewidth", "format", "processed", "classificationid", "uploadrequestid",
          "layoutid", "isproduction"};
      fileDetailsData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath ,TableConstants.FILEDETAILS.getName()+"_" + batchid + ".csv"),
          fileDetailsData);
      fileDetailsRowCount = fileDetailsData.size() - 1;
    } else {
      log.info(
          "FileDetails data not found for project id : " + projectId + " and file ids: " + String
              .join(",", fileIds));
    }

    log.exit();
    return fileDetailsRowCount;
  }

  private long exportFileBlobData(String projectExportDirPath,
      int batchid, String projectId, List<String> fileIds) throws IOException {
    log.entry();
    long fileBlobRowCount = 0l;
    List<String[]> fileBlobData = fileBlobDao.getFileBlobs(fileIds);

    if (fileBlobData != null && !fileBlobData.isEmpty()) {
      String[] headerData = {"fileid", "fileblob"};
      fileBlobData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.FILEBLOBS.getName()+"_" + batchid + ".csv"),
          fileBlobData);
      fileBlobRowCount = fileBlobData.size() - 1;
    } else {
      log.info("FileBlobs data not found for file ids: " + String
          .join(",", fileIds));
    }

    log.exit();
    return fileBlobRowCount;
  }

  private long exportClassificationReportData(String projectExportDirPath,
      int batchid, String projectId, List<String> fileIds) throws IOException {
    log.entry();
    long classificationReportRowCount = 0l;
    List<String[]> classificationReportData = classificationReportDao
        .getClassificationReports(fileIds);
    if (classificationReportData != null && !classificationReportData.isEmpty()) {
      String[] headerData = {"documentid", "fieldid", "aliasname", "isfound"};
      classificationReportData.add(0, headerData);
      csvFileWriter
          .writeData(Paths.get(projectExportDirPath,TableConstants.CLASSIFICATIONREPORT.getName()+"_" + batchid + ".csv"),
              classificationReportData);
      classificationReportRowCount = classificationReportData.size() - 1;
    } else {
      log.info("ClassificationReports data not found for documentIds: " + String
          .join(",", fileIds));
    }

    log.exit();
    return classificationReportRowCount;
  }

  public String prepareProjectDirectory(String path, String projectId) {
    String projectExportDirPath = path + "/" + projectId;

    File file = new File(projectExportDirPath);
    if (!file.exists()) {
      file.mkdirs();
    }
    return projectExportDirPath;
  }
}
