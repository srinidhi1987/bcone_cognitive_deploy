package com.automationanywhere.cognitive.project.helper;

import java.nio.file.Paths;

public class PathHelper {

    private static final String SYSWOW = "\\SysWOW64";
    private static final String SYSTEM32 = "\\System32";
    private static final String OS_NAME_PROPERTY = "os.name";
    private static final String OS_NAME_WINDOWS = "Windows";
    private static final String PROGRAM_FILES_86 = "ProgramFiles(x86)";
    private static final String WINDIR_PROPERTY = "windir";

    public String join(String first, String... more) {
        return Paths.get(first, more).toString();
    }

    public String getSystemFolder()
    {
        String path = "";
        boolean is64bit = false;
        if (System.getProperty(OS_NAME_PROPERTY).contains(OS_NAME_WINDOWS)) {
            is64bit = (System.getenv(PROGRAM_FILES_86) != null);

            String windowsDirectory = System.getenv(WINDIR_PROPERTY);
            if (is64bit)
                path =  windowsDirectory+ SYSWOW;
            else
                path = windowsDirectory + SYSTEM32;
        }

        return path;
    }
}
