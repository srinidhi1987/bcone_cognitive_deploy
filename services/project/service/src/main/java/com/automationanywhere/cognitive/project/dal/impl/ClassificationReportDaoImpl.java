package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.ClassificationReportDao;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.model.dao.ClassificationReport;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ClassificationReportDaoImpl implements ClassificationReportDao {

  private AALogger log = AALogger.create(ClassificationReportDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getClassificationReports(List<String> documentIds) {
    log.entry();

    String sqlQuery = "select * from ClassificationReport where documentId in (:documentIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("documentIds", documentIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> classificationReports = dataTransformer.transformToListOfStringArray(resulSetData);

    log.exit();
    return classificationReports;
  }

  @Override
  public void batchInsertClassificationReport(List<ClassificationReport> classificationReports)  {
    Session session = sessionFactory.getCurrentSession();
    int index=-1;
    for (ClassificationReport classificationReport : classificationReports) {
      session.save(classificationReport);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
      index++;
    }
  }

  @Override
  public void delete(List<String> documentIds) {
    log.entry();
    String hqlQueryString = "delete ClassificationReport where documentId in (:documentIds)";
    Query query = sessionFactory.getCurrentSession().createQuery(hqlQueryString);
    query.setParameterList("documentIds", documentIds);
    query.executeUpdate();
    log.exit();
  }

  @Override
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}

