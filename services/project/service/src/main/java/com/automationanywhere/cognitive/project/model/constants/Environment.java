package com.automationanywhere.cognitive.project.model.constants;

public enum Environment {
  staging,
  production
}
