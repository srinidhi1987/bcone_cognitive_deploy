package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.FileBlobDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.FileBlob;
import com.automationanywhere.cognitive.project.model.dao.FileBlobs;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class FileBlobsDataImporter extends AdvanceDataImporter<FileBlob> {

  private static final AALogger LOGGER = AALogger.create(FileBlobsDataImporter.class);

  private final FileHelper fileHelper;
  private final TransitionResult transitionResult;
  private final FileBlobDao fileBlobDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public FileBlobsDataImporter(FileReader csvFileReader, FileHelper fileHelper,
      TransitionResult transitionResult, FileBlobDao fileBlobDao,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.fileHelper = fileHelper;
    this.transitionResult = transitionResult;
    this.fileBlobDao = fileBlobDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    LOGGER.entry();

    //fetch's data from csv path
    List<FileBlobs> dataToImport = getDataFromFiles(unzippedFolderPath);

    //data prepared for import
    List<FileBlobs> finalDataToImport = prepareData(dataToImport);

    //data inserted in batch.
    if(!finalDataToImport.isEmpty()) {
      fileBlobDao.batchInsertFileBlobs(finalDataToImport);
    }
    //next importer get's called.
    if(dataImporter!=null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
    LOGGER.exit();

  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<FileBlobs> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    LOGGER.entry();
    List<FileBlobs> dataToImport = new ArrayList<>();
    try {
      Path csvFolderPath = Paths
          .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
      String csvRegexPattern = importFilePatternConstructor
          .getRegexPatternWithOr(TableConstants.FILEBLOBS);
      List<Path> pathsOfFileBlobs = fileHelper
          .getAllFilesPaths(csvFolderPath, csvRegexPattern);

      dataToImport
          .addAll(DtoToDao.toFileBlobs(getDataFromCsv(pathsOfFileBlobs, FileBlob.class)));
    } catch (SQLException | DecoderException ex) {
      throw new ProjectImportException(
          "Got exception while converting data of csv data to FileBlobs", ex);
    }
    LOGGER.exit();

    return dataToImport;
  }
  
  private List<FileBlobs> prepareData(List<FileBlobs> dataToImport) {
    return filterByFileIds(transitionResult.getFileIds(), dataToImport);
  }

  private List<FileBlobs> filterByFileIds(List<String> fileIds, List<FileBlobs> fileBlobsList) {
    return fileBlobsList.stream().filter(fileBlobs -> fileIds.contains(fileBlobs.getFileId()))
        .collect(Collectors.toList());
  }

}
