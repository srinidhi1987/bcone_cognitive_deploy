package com.automationanywhere.cognitive.project.export.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class FileZipper {

  /**
   * Unzips the zipped file.
   *
   * @param inputPath zip file path.
   * @param outputPath path of the folder where unzip files will be created.
   */
  public void unzipIt(Path inputPath, Path outputPath) throws IOException {
    String outputPathStr = outputPath.toString();
    String inputPathStr = inputPath.toString();

    byte[] buffer = new byte[1024];

    try (ZipFile zipFile = new ZipFile(inputPathStr)) {

      Enumeration<? extends ZipEntry> zipEntriesEnum = zipFile.entries();

      prepareDirectory(outputPathStr);

      while (zipEntriesEnum.hasMoreElements()) {
        ZipEntry zipEntry = zipEntriesEnum.nextElement();

        if (zipEntry.isDirectory()) {
          String nestedDirectoryPathStr = getFilePath(outputPathStr, zipEntry.getName()).toString();
          prepareDirectory(nestedDirectoryPathStr);
        } else {

          Path nestedFilePath = getFilePath(outputPathStr, zipEntry.getName());

          prepareDirectory(nestedFilePath.getParent().toString());

          InputStream is = zipFile.getInputStream(zipEntry);
          BufferedInputStream bis = new BufferedInputStream(is);

          FileOutputStream fos = new FileOutputStream(nestedFilePath.toString(), false);
          int datalen;
          while ((datalen = bis.read(buffer)) > 0) {
            fos.write(buffer, 0, datalen);
          }
          is.close();
          bis.close();
          fos.close();
        }
      }

    }
  }


  /**
   * Zips the input directory and creates a zip file.
   *
   * @param inputPath input directory
   * @param outputPath output file path with extension ex:.zip
   */
  public void zipIt(Path inputPath, Path outputPath) throws IOException {
    String outputPathStr = outputPath.toString();
    String inputPathStr = inputPath.toString();

    File fileToBeZipped = new File(inputPathStr);
    String zipName = fileToBeZipped.getName();

    try (FileOutputStream fios = new FileOutputStream(outputPathStr);
        ZipOutputStream zios = new ZipOutputStream(fios);) {
      zipFile(fileToBeZipped, zipName, zios);

    }
  }

  /**
   * Recursive method to zip the file and directories.
   *
   * @param fileToBeZipped file that needs to be zipped.
   * @param zipName name of the zip file.
   * @param zios ZipOutputStream object.
   */
  private void zipFile(File fileToBeZipped, String zipName, ZipOutputStream zios)
      throws IOException {
    if (fileToBeZipped.isDirectory()) {
      File[] listOfFiles = fileToBeZipped.listFiles();
      for (File file : listOfFiles) {
        Path nestedFilePath = getFilePath(zipName, file.getName());
        zipFile(file, nestedFilePath.toString(), zios);
      }
      return;
    }
    FileInputStream fis = new FileInputStream(fileToBeZipped);
    ZipEntry zipEntry = new ZipEntry(zipName);
    zios.putNextEntry(zipEntry);
    byte[] bytes = new byte[1024];
    int dataLen;
    while ((dataLen = fis.read(bytes)) >= 0) {
      zios.write(bytes, 0, dataLen);
    }
    fis.close();

  }

  /**
   * Create directories if not exist.
   */
  private void prepareDirectory(String path) throws IOException {
    Files.createDirectories(Paths.get(path));
  }

  /**
   * Builds and returns a "Path" object.
   *
   * @param currentPath current path
   * @param others array of string.
   */
  private Path getFilePath(String currentPath, String... others) {
    Path filePath = Paths.get(currentPath, others);
    return filePath;
  }
}
