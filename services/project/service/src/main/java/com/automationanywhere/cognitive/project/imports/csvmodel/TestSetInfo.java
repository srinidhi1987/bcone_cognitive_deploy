package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TestSetInfo")
public class TestSetInfo {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;

  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "visionbotid")
  private String visionbotid;

  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "layoutid")
  private String layoutid;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getVisionbotid() {
    return visionbotid;
  }

  public void setVisionbotid(String visionbotid) {
    this.visionbotid = visionbotid;
  }

  public String getLayoutid() {
    return layoutid;
  }

  public void setLayoutid(String layoutid) {
    this.layoutid = layoutid;
  }
}
