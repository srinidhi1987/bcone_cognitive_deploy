package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.constants.CsvFileIOConstants;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class CsvFileWriter implements FileWriter {

  @Override
  public void writeData(Path path, List<String[]> data) throws IOException {
    try (Writer writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE);
        CSVWriter csvWriter =
            new CSVWriter(writer, CsvFileIOConstants.SEPARATOR, CsvFileIOConstants.NO_QUOTE_CHARACTER,
                CsvFileIOConstants.NO_ESCAPE_CHARACTER, CsvFileIOConstants.LINE_END);) {
      csvWriter.writeAll(data);
    }

  }
  
  public <T> void writeData(Path path, Class<T> theClass, List<T> data)
      throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
    CustomMappingStrategy<T> strategy = new CustomMappingStrategy<>();
    strategy.setType(theClass);

    try (Writer writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE);) {
      StatefulBeanToCsv<T> beanToCsv = new StatefulBeanToCsvBuilder<T>(writer)
          .withMappingStrategy(strategy)
          .withSeparator(CsvFileIOConstants.SEPARATOR)
          .withOrderedResults(true)
          .withQuotechar(CsvFileIOConstants.NO_QUOTE_CHARACTER)
          .withEscapechar(CsvFileIOConstants.NO_ESCAPE_CHARACTER)
          .withLineEnd(CsvFileIOConstants.LINE_END)
          .build();

      beanToCsv.write(data);
    }
  }
}
