package com.automationanywhere.cognitive.project.export;

import java.util.concurrent.TimeoutException;

import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportRequest;

public interface ExportService {
    Task startExport(String path, ProjectExportRequest projectExportRequest)
        throws TimeoutException, InterruptedException;
    void cleanUp(String path, boolean enableFileCleanUp);
}
