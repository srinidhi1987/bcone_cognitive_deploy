package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.VisionbotDetailDao;
import com.automationanywhere.cognitive.project.export.ActivityMonitorService;
import com.automationanywhere.cognitive.project.messagequeue.QueuingService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ActivityMonitorServiceImpl implements ActivityMonitorService {

    AALogger logger = AALogger.create(this.getClass());
    private static final String CLASSIFIER_PROCESS_NAME = "Automation.Cognitive.DocumentClassifier.Executor.exe";
    private static final String ENGINE_WORKER_PROCESS_NAME = "Automation.Cognitive.Worker.Visionbot.Executor.exe";

    @Autowired
    @Qualifier("documentProcessingQueuingService")
    public QueuingService documentProcessingQueuingService;

    @Autowired
    @Qualifier("classificationQueuingService")
    public QueuingService classificationQueuingService;

    @Autowired
    @Qualifier("productionDocumentProcessingQueuingService")
    public QueuingService productionDocumentQueuingService;

    @Autowired
    @Qualifier("visionbotDetailDao")
    private VisionbotDetailDao visionbotDetailDao;

    @Override
    public boolean isClassifierInProgress() {
        try {
            return isProcessRunning(CLASSIFIER_PROCESS_NAME)
                    || classificationQueuingService.getMessageCount() > 0;
        } catch (IOException e) {
            logger.error(e);
            return true;
        }
    }

    @Override
    public boolean isStagingDocumentsInProgress()  {
        try {
            return isProcessRunning(ENGINE_WORKER_PROCESS_NAME)
                    || documentProcessingQueuingService.getMessageCount() > 0;
        } catch (IOException e) {
            logger.error(e);
            return true;
        }
    }

    @Override
    public boolean isProductionDocumentsInProgress() {
        return productionDocumentQueuingService.getMessageCount() > 0 ? true : false;
    }

    private boolean isProcessRunning(String processName) throws IOException {
       try {
           String line;
           boolean result = false;
           Process p = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
           BufferedReader input = new BufferedReader
                   (new InputStreamReader(p.getInputStream()));
           while ((line = input.readLine()) != null) {
               if (!line.trim().equals("")) {
                   if(line.toLowerCase().contains(processName.toLowerCase()))
                   {
                       result = true;
                       break;
                   }
               }
           }
           input.close();
           return  result;
       } catch (Exception ex)
       {
           logger.error(ex);
           return true;
       }
    }

    @Override
    public boolean isBotLocked() {
        return visionbotDetailDao.isBotLocked();
    }
}
