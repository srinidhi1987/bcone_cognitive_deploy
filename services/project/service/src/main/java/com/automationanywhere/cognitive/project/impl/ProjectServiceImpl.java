package com.automationanywhere.cognitive.project.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.Main;
import com.automationanywhere.cognitive.project.ProjectService;
import com.automationanywhere.cognitive.project.Util.DateUtil;
import com.automationanywhere.cognitive.project.Util.FieldUtil;
import com.automationanywhere.cognitive.project.Util.MathUtil;
import com.automationanywhere.cognitive.project.Util.StringUtil;
import com.automationanywhere.cognitive.project.adapter.FileManagerAdapter;
import com.automationanywhere.cognitive.project.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.project.configuration.ConfigurationService;
import com.automationanywhere.cognitive.project.constants.ErrorMessageConstants;
import com.automationanywhere.cognitive.project.dal.OCREngineDetailsDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectCreationFailedException;
import com.automationanywhere.cognitive.project.exception.ProjectDeletedException;
import com.automationanywhere.cognitive.project.exception.ProjectUpdateException;
import com.automationanywhere.cognitive.project.exception.ProjectUpdationFailedException;
import com.automationanywhere.cognitive.project.exception.ResourceUpdationException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.exception.VisionBotUpdationFailedException;
import com.automationanywhere.cognitive.project.exception.impl.HttpStatusCode;
import com.automationanywhere.cognitive.project.export.ExportService;
import com.automationanywhere.cognitive.project.extension.DaoToDto;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.imports.ImportService;
import com.automationanywhere.cognitive.project.messagequeue.QueuingService;
import com.automationanywhere.cognitive.project.model.ActivityTypes;
import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.VisionBotState;
import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;
import com.automationanywhere.cognitive.project.model.dao.ProjectActivityDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailModel;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.Fields;
import com.automationanywhere.cognitive.project.model.dto.File;
import com.automationanywhere.cognitive.project.model.dto.FileCategory;
import com.automationanywhere.cognitive.project.model.dto.PatchRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectExportRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectGetResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponseV2;
import com.automationanywhere.cognitive.project.model.dto.VisionBot;
import com.automationanywhere.cognitive.project.model.dto.VisionbotData;
import com.automationanywhere.cognitive.project.model.dto.imports.ArchiveInfo;
import com.automationanywhere.cognitive.project.model.dto.imports.ProjectImportRequest;
import com.automationanywhere.cognitive.project.task.TaskService;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Jemin.Shah on 24-11-2016.
 */
public class ProjectServiceImpl implements ProjectService {

  private static final String ORGANIZATION_ID_KEY = "organizationId";
  private static final String ID_KEY = "id";
  private final String CONFIDENCE_THRESHOLD_KEY = "ConfidenceThreshold";
  private final int DEFAULT_CONFIDENCE_THRESHOLD = 0;
  private final String BACKUP_PATH = "BackupData";
  private AALogger log = AALogger.create(ProjectService.class);
  @Autowired
  @Qualifier("fileManagerAdapter")
  private FileManagerAdapter fileManagerAdapter;

  @Autowired
  @Qualifier("visionbotAdapter")
  private VisionbotAdapter visionbotAdapter;

  @Autowired
  private ProjectDetailDao projectDetailDao;

  @Autowired
  private OCREngineDetailsDao ocrEngineDetailsDao;

  @Autowired
  @Qualifier("queuingServiceProxy")
  private QueuingService queuingService;

  @Autowired
  private ConfigurationService configurationService;

  @Autowired
  private ExportService exportService;

  @Autowired
  private ImportService importService;

  @Autowired
  @Qualifier("outputPathBean")
  private String outputPath;

  @Autowired
  private ProjectServiceDelegate projectServiceDelegate;

  @Autowired
  private ProjectUpdation projectUpdation;

  @Autowired
  private TaskService taskService;

  public String getOutputPath() {
    return outputPath;
  }

  public void setOutputPath(String outputPath) {
    this.outputPath = outputPath;
  }

  @Transactional
  public List<ProjectGetResponse> getProjectDetailsByOrganizationId(String organizationId,
      String excludeFields)
      throws InvalidInputException {
    log.entry();
    log.debug("organizationid: {}, excludeFieldId: {}", organizationId, excludeFields);
    Set<String> fieldSet = FieldUtil.extractFields(excludeFields);
    List<ProjectGetResponse> projectGetResponseList = new ArrayList<>();

    List<ProjectDetailModel>
        projectDetailList =
        projectDetailDao.getProjectDetails(organizationId);
    List<VisionbotData> visionbotDataList = getVisionbotData(organizationId);
    List<FileCategory> fileCategoryList = fetchCategories(organizationId);
    for (ProjectDetailModel projectDetail : projectDetailList) {
      ProjectGetResponse projectGetResponse = DaoToDto.toProjectGetResponse(projectDetail);
      projectGetResponse.setConfidenceThreshold(getConfidenceThreshold());
      projectGetResponse.setCategories(
          getCategoriesByProject(projectGetResponse.getId(), fileCategoryList));
      projectGetResponseList.add(projectGetResponse);
      setCategoryStats(projectGetResponse);
      setVisionbotDetails(projectGetResponse, visionbotDataList);
      setTrainedDocumentPercentage(projectGetResponse);

      if (fieldSet.contains("categories")) {
        projectGetResponse.setCategories(null);
      }
    }
    log.exit();
    return projectGetResponseList;
  }

  @Transactional
  public List<ProjectGetResponse> getAll(String organizationId, Map<String, String[]> queryParam)
      throws InvalidInputException {
    log.entry();
    List<ProjectGetResponse> projectGetResponseList = new ArrayList<>();
    Map<String, String> restrictions = new HashMap<>();
    restrictions.put("organizationId", organizationId);

    List<ProjectDetail> projectDetailList = projectDetailDao
        .getProjectDetails(restrictions, queryParam);
    List<VisionbotData> visionbotDataList = getVisionbotData(organizationId);
    List<FileCategory> fileCategoryList = fetchCategories(organizationId);
    for (ProjectDetail projectDetail : projectDetailList) {
      ProjectGetResponse projectGetResponse = DaoToDto.toProjectGetResponse(projectDetail);
      projectGetResponse.setConfidenceThreshold(getConfidenceThreshold());
      projectGetResponse
          .setCategories(getCategoriesByProject(projectGetResponse.getId(), fileCategoryList));
      projectGetResponseList.add(projectGetResponse);
      setCategoryStats(projectGetResponse);
      setVisionbotDetails(projectGetResponse, visionbotDataList);
      setTrainedDocumentPercentage(projectGetResponse);
    }
    log.exit();
    return projectGetResponseList;
  }

  @Transactional
  public List<ProjectGetResponse> getAllMetadata(String organizationId,
      Map<String, String[]> queryParam) throws InvalidInputException {
    log.entry();
    List<ProjectGetResponse> projectGetResponseList = new ArrayList<>();
    Map<String, String> restrictions = new HashMap<>();
    restrictions.put("organizationId", organizationId);

    List<ProjectDetail> projectDetailList = projectDetailDao
        .getProjectDetails(restrictions, queryParam);
    for (ProjectDetail projectDetail : projectDetailList) {
      ProjectGetResponse projectGetResponse = DaoToDto.toProjectGetResponse(projectDetail);
      projectGetResponse.setConfidenceThreshold(getConfidenceThreshold());
      projectGetResponseList.add(projectGetResponse);
    }
    log.exit();
    return projectGetResponseList;
  }

  @Transactional
  @Override
  public ProjectGetResponse get(String organizationId, String projectId, String excludeFields)
      throws NotFoundException, InvalidInputException, ProjectDeletedException {
    log.entry();
    Set<String> fieldSet = FieldUtil.extractFields(excludeFields);
    Map<String, String> restrictions = new HashMap<>();
    restrictions.put("organizationId", organizationId);
    restrictions.put("id", projectId);

    List<ProjectDetail> projectDetailList
        = projectDetailDao.getProjectDetails(restrictions, null);

    if (projectDetailList.size() == 0) {
      ProjectActivityDetail activityDetail = projectDetailDao
          .getDeletedProjectActivityByProjectId(projectId);
      if (activityDetail != null) {
        throw new ProjectDeletedException(
            "Learning Instance '" + activityDetail.getProjectName() + "' has been deleted.");
      }

      throw new NotFoundException("Learning Instance not found.");
    }

    List<FileCategory> fileCategoryList = fetchCategories(organizationId, projectId);
    ProjectGetResponse projectGetResponse = DaoToDto
        .toProjectGetResponse(projectDetailList.stream().findFirst().get());
    projectGetResponse.setConfidenceThreshold(getConfidenceThreshold());
    projectGetResponse
        .setCategories(getCategoriesByProject(projectGetResponse.getId(), fileCategoryList));
    setCategoryStats(projectGetResponse);
    setVisionbotDetails(projectGetResponse, getVisionbotData(organizationId, projectId));
    setTrainedDocumentPercentage(projectGetResponse);

    if (fieldSet.contains("categories")) {
      projectGetResponse.setCategories(null);
    }
    log.exit();
    return projectGetResponse;
  }

  @Transactional
  @Override
  public ProjectResponse getMetadata(String organizationId, String projectId)
      throws NotFoundException, InvalidInputException, ProjectDeletedException {
    log.entry();
    Map<String, String> restrictions = new HashMap<>();
    restrictions.put("organizationId", organizationId);
    restrictions.put("id", projectId);

    List<ProjectDetail> projectDetailList
        = projectDetailDao.getProjectDetails(restrictions, null);

    if (projectDetailList.size() == 0) {
      ProjectActivityDetail activityDetail = projectDetailDao
          .getDeletedProjectActivityByProjectId(projectId);
      if (activityDetail != null) {
        throw new ProjectDeletedException(
            "Learning Instance '" + activityDetail.getProjectName() + "' has been deleted.");
      }

      throw new NotFoundException("Learning Instance not found.");
    }

    ProjectResponse projectResponse = DaoToDto
        .toProjectResponse(projectDetailList.stream().findFirst().get());
    projectResponse.setConfidenceThreshold(getConfidenceThreshold());

    log.exit();
    return projectResponse;
  }

  @Transactional
  @Override
  public ProjectResponseV2 getMetadataV2(String organizationId, String projectId)
      throws NotFoundException, InvalidInputException, ProjectDeletedException {
    log.entry();
    Map<String, String> restrictions = new HashMap<>();
    restrictions.put("organizationId", organizationId);
    restrictions.put("id", projectId);

    List<ProjectDetail> projectDetailList
        = projectDetailDao.getProjectDetails(restrictions, null);

    if (projectDetailList.size() == 0) {
      ProjectActivityDetail activityDetail = projectDetailDao
          .getDeletedProjectActivityByProjectId(projectId);
      if (activityDetail != null) {
        throw new ProjectDeletedException(
            "Learning Instance '" + activityDetail.getProjectName() + "' has been deleted.");
      }

      throw new NotFoundException("Learning Instance not found.");
    }

    ProjectResponseV2 projectResponse = DaoToDto
        .toProjectResponseV2(projectDetailList.stream().findFirst().get());
    projectResponse.setConfidenceThreshold(getConfidenceThreshold());

    log.exit();
    return projectResponse;
  }

  @Transactional
  @Override
  public void delete(String organizationId, String projectId, String updatedBy)
      throws InvalidInputException, PreconditionFailedException {
    log.entry();
    log.debug(() -> String
        .format("organizationid: %s, projectid: %s, updatedby: %s", organizationId, projectId,
            updatedBy));
    if (StringUtil.isNullOrEmpty(organizationId)
        || StringUtil.isNullOrEmpty(projectId)
        || StringUtil.isNullOrEmpty(updatedBy)) {
      log.error("Invalid Parameters.");
      throw new InvalidInputException(ErrorMessageConstants.INVALID_PARAMETER);
    }

    Map<String, String> restrictions = new HashMap<>();
    restrictions.put("organizationId", organizationId);
    restrictions.put("id", projectId);

    List projectDetailList = projectDetailDao.getProjectDetails(restrictions, null);
    if (projectDetailList.size() == 0) {
      log.error("Learning Instance not found.");
      throw new InvalidInputException(ErrorMessageConstants.LEARNING_INSTANCE_NOT_FOUND);
    }

    ProjectDetail projectToDelete = (ProjectDetail) projectDetailList.stream().findAny().get();

    if (projectToDelete.getEnvironment() == Environment.production) {
      log.error("Learning instance is not in staging mode.");
      throw new PreconditionFailedException(
          ErrorMessageConstants.LEARNING_INSTANCE_NOT_STAGING_MODE);
    }

    ProjectActivityDetail projectActivityDetail = new ProjectActivityDetail();
    projectActivityDetail.setActivityAt(DateUtil.now());
    projectActivityDetail.setActivityId(ActivityTypes.Delete.getValue());
    projectActivityDetail.setProjectId(projectId);
    projectActivityDetail
        .setProjectName(((ProjectDetail) projectDetailList.stream().findAny().get()).getName());
    projectActivityDetail.setUpdatedBy(updatedBy);

    ProjectDetail projectDetail = new ProjectDetail();
    projectDetail.setId(projectId);
    projectDetail.setOrganizationId(organizationId);
    projectDetailDao.deleteProjectDetail(projectDetail);

    projectDetailDao.addProjectActivity(projectActivityDetail);
    log.exit();
  }

  @Transactional
  @Override
  public ProjectResponse add(ProjectRequest projectRequest) throws DuplicateProjectNameException
      , ProjectCreationFailedException {
    log.entry();
    OCREngineDetails currentOCREngineDetail = null;
    try {
      currentOCREngineDetail = getDefaultOCREngineDetail();
    } catch (IOException ex) {
      throw new ProjectCreationFailedException(ErrorMessageConstants.UNABLE_FIND_OCR_ENGINE, ex);
    }

    if (currentOCREngineDetail == null) {
      throw new ProjectCreationFailedException(ErrorMessageConstants.UNABLE_FIND_OCR_ENGINE);
    }

    Set<OCREngineDetails> ocrEngineDetails = new HashSet<OCREngineDetails>();
    ocrEngineDetails.add(currentOCREngineDetail);
    ProjectDetail projectDetail = createProjectDetail(projectRequest);
    projectDetail.setOcrEngineDetails(ocrEngineDetails);

    ProjectActivityDetail projectActivityDetail = projectDetailDao
        .getDeletedProjectActivityByProjectName(projectDetail.getName());
    if (projectActivityDetail != null) {
      log.error("Duplicate Learning Instance name");
      throw new DuplicateProjectNameException(ErrorMessageConstants.DUPLICATE_LEARNING_INSTANCE);
    }

    projectDetailDao.addProjectDetail(projectDetail);
    ProjectResponse projectResponse = DaoToDto.toProjectResponse(projectDetail);
    log.exit();
    return projectResponse;
  }

  @Override
  public void update(ProjectRequest projectRequest)
      throws NotFoundException, InvalidInputException, VisionBotUpdationFailedException, SystemBusyException, ProjectUpdationFailedException, CloneNotSupportedException, ResourceUpdationException {
    log.entry();
    Map<String, String> restrictions = new HashMap<>();
    String organizaionId = projectRequest.getOrganizationId();
    String id = projectRequest.getId();
    log.debug(() -> String.format("organizationId: %s, id: %s", organizaionId, id));
    restrictions.put("organizationId", organizaionId);
    restrictions.put("id", id);

    List<ProjectDetail> projectDetailList = projectUpdation.getProjectDetails(restrictions, null);

    if (projectDetailList.size() == 0) {
      log.error("Learning Instance not found");
      throw new NotFoundException(ErrorMessageConstants.LEARNING_INSTANCE_NOT_FOUND);
    }

    ProjectDetail projectDetail = projectDetailList.stream().findFirst().get();

    projectServiceDelegate.update(projectDetail, projectRequest);

    log.exit();
  }

  @Override
  public ProjectGetResponse update(
      final String organizationId,
      final String projectId,
      final PatchRequest... patchRequests
  ) throws ProjectUpdateException {

    long currentVersionId = 0;
    try {
      Map<String, String> restrictions = new HashMap<>();
      log.debug(() -> String.format("organizationId: %s, id: %s", organizationId, projectId));
      restrictions.put(ORGANIZATION_ID_KEY, organizationId);
      restrictions.put(ID_KEY, projectId);

      List<ProjectDetail> projectDetailList
          = projectUpdation.getProjectDetails(restrictions, null);
      if (projectDetailList.size() == 0) {
        throw new NotFoundException(ErrorMessageConstants.LEARNING_INSTANCE_NOT_FOUND);
      }

      ProjectDetail projectDetail = projectDetailList.stream().findFirst().get();
      currentVersionId = projectDetail.getVersionId();

      projectServiceDelegate.checkIfProjectEnvironmentIsUpdated(projectDetail, patchRequests);

      //transforms projectdetail to projectrequest object.
      ProjectRequest projectRequest = DaoToDto.toProjectRequest(projectDetail);
      Fields fields = new Fields();
      fields.setCustom(new ArrayList<>());
      fields.setStandard(new String[]{});
      projectRequest.setFields(fields);

      projectServiceDelegate.validateRequest(projectRequest, patchRequests);

      //applies operation(addAll,remove,etc) as specified in update request.
      ProjectRequest updatedProjectRequest = projectServiceDelegate
          .applyPatchOperations(projectRequest, patchRequests);
      updatedProjectRequest.setVersionId(patchRequests[0].getVersionId());

      projectServiceDelegate.update(projectDetail, updatedProjectRequest);

      ProjectGetResponse projectGetResponse = DaoToDto.toProjectGetResponse(projectDetail);
      projectGetResponse.setConfidenceThreshold(getConfidenceThreshold());
      List<FileCategory> fileCategoryList = fetchCategories(organizationId, projectId);
      projectGetResponse.setCategories(
          getCategoriesByProject(projectGetResponse.getId(), fileCategoryList)
      );

      setCategoryStats(projectGetResponse);
      setVisionbotDetails(projectGetResponse, getVisionbotData(organizationId, projectId));
      setTrainedDocumentPercentage(projectGetResponse);

      return projectGetResponse;

    } catch (ResourceUpdationException e) {
      throw new ProjectUpdateException(e.getMessage(), e, currentVersionId,
          HttpStatusCode.CONFLICT);
    } catch (SystemBusyException e) {
      throw new ProjectUpdateException(e.getMessage(), e, currentVersionId,
          HttpStatusCode.PRECONDITION_FAILED);
    } catch (NotFoundException e) {
      throw new ProjectUpdateException(e.getMessage(), e, currentVersionId, HttpStatusCode.GONE);
    } catch (Exception e) {
      throw new ProjectUpdateException(e.getMessage(), e, currentVersionId,
          HttpStatusCode.INTERNAL_SERVER_ERROR);
    } finally {
      log.exit();
    }
  }

  @Transactional
  @Override
  public Task startProjectExport(ProjectExportRequest projectExportRequest)
      throws TimeoutException, InterruptedException {
    try {
      log.entry();

      String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
      String exportFilePath =
          outputPath + "\\" + BACKUP_PATH + "\\" + projectExportRequest.getArchiveName() + "_"
              + timeStamp;
      Task task = exportService.startExport(exportFilePath, projectExportRequest);
      return task;
    } finally {
      log.exit();
    }
  }

  @Transactional
  @Override
  public List<Task> getRunningTasks() {
    return taskService.getAllInProgressTasks();
  }


  @Override
  public Task startProjectImport(ProjectImportRequest projectImportRequest)
      throws PreconditionFailedException, SystemBusyException {
    return importService.startImport(projectImportRequest);
  }

  @Override
  public List<ArchiveInfo> getArchiveDetails() {
    return importService.getArchiveDetails();
  }

  private void setTrainedDocumentPercentage(ProjectGetResponse projectGetResponse) {
    if (projectGetResponse.getCategories() == null) {
      log.trace(() -> "Categories not found");
      return;
    }

    List<FileCategory> fileCategoryList = projectGetResponse.getCategories();

    float trainedPercentange = 0;
    float totalDocuments = 0;
    float trainedDocuments = 0;
    for (FileCategory fileCategory : fileCategoryList) {
      long stagingTotalCount = fileCategory.getStagingFileDetails().getTotalCount();
      long productionTotalCount = fileCategory.getProductionFileDetails().getTotalCount();
      VisionBot visionBot = fileCategory.getVisionBot();
      totalDocuments += stagingTotalCount + productionTotalCount;
      if (visionBot != null) {
        String vbotState = visionBot.getCurrentState();
        boolean isBotReadyOrActive = vbotState.equals(VisionBotState.ready.toString()) || vbotState
            .equals(VisionBotState.active.toString());
        if (isBotReadyOrActive) {
          trainedDocuments += stagingTotalCount + productionTotalCount;
        }
      }
    }

    if (totalDocuments > 0) {
      trainedPercentange = MathUtil.round((trainedDocuments / totalDocuments) * 100, 2)
          .floatValue();
    }

    projectGetResponse.setCurrentTrainedPercentage(trainedPercentange);
    log.exit();
  }

  private String toIso8601DateString(Date date) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    return simpleDateFormat.format(date);
  }

  private void setCategoryStats(ProjectGetResponse projectGetResponse) {
    log.entry();
    if (projectGetResponse.getCategories() == null) {
      return;
    }

    int totalFileCount = 0;
    int unprocessedFileCount = 0;

    boolean isProduction = projectGetResponse.getEnvironment().equals(Environment.production);

    for (FileCategory fileCategory : projectGetResponse.getCategories()) {
      long fileCount = isProduction ? fileCategory.getProductionFileDetails().getTotalCount()
          : fileCategory.getStagingFileDetails().getTotalCount();
      fileCategory.setFileCount(fileCount);
      totalFileCount += isProduction ? fileCategory.getProductionFileDetails().getTotalCount()
          : fileCategory.getStagingFileDetails().getTotalCount();
      unprocessedFileCount +=
          isProduction ? fileCategory.getProductionFileDetails().getUnprocessedCount()
              : fileCategory.getStagingFileDetails().getUnprocessedCount();
    }

    projectGetResponse.setNumberOfCategories(projectGetResponse.getCategories().size());
    projectGetResponse.setNumberOfFiles(totalFileCount);
    projectGetResponse.setUnprocessedFileCount(unprocessedFileCount);
    log.exit();
  }

  private long getUnprocessedFileCount(List<File> fileList, boolean isProduction) {
    return fileList.stream()
        .filter(file -> file.isProduction() == isProduction && !file.isProcessed()).count();
  }

  private List<VisionbotData> getVisionbotData(String organizationId) {
    return this.visionbotAdapter.getVisionbotData(organizationId);
  }

  private List<VisionbotData> getVisionbotData(String organizationId, String projectId) {
    return this.visionbotAdapter.getVisionbotData(organizationId, projectId);
  }

  private void setVisionbotDetails(ProjectGetResponse projectGetResponse,
      List<VisionbotData> visionbotDataList) {
    log.entry();
    float accuracy = 0;

    if (visionbotDataList != null) {
      visionbotDataList = visionbotDataList.stream()
          .filter(v -> v.getProjectId().equals(projectGetResponse.getId()))
          .collect(Collectors.toList());
    }

    if (projectGetResponse.getCategories() == null
        || projectGetResponse.getCategories().size() == 0
        || visionbotDataList == null
        || visionbotDataList.size() == 0) {
      projectGetResponse.setVisionBotCount(0);
      projectGetResponse.setAccuracyPercentage(0);
      return;
    }

    for (FileCategory fileCategory : projectGetResponse.getCategories()) {
      VisionbotData visionbotData = visionbotDataList.stream()
          .filter(v -> v.getCategoryId().equals(fileCategory.getId()))
          .findAny()
          .orElse(null);

      if (visionbotData == null) {
        continue;
      }

      fileCategory.setVisionBot(DaoToDto.toVisionBot(visionbotData));
    }

    float totalFields = 0;
    float passedFields = 0;

    boolean isProduction = projectGetResponse.getEnvironment().equals(Environment.production);

    for (VisionbotData visionbotData : visionbotDataList) {
      totalFields += isProduction ? visionbotData.getProductionBotRunDetails().getTotalFieldCount()
          : visionbotData.getStagingBotRunDetails().getTotalFieldCount();

      passedFields +=
          isProduction ? visionbotData.getProductionBotRunDetails().getPassedFieldCount()
              : visionbotData.getStagingBotRunDetails().getPassedFieldCount();
    }

    if (totalFields > 0) {
      accuracy = MathUtil.round((passedFields / totalFields) * 100, 2).floatValue();
    }

    projectGetResponse.setVisionBotCount(visionbotDataList.size());
    projectGetResponse.setAccuracyPercentage(accuracy);
  }

  private ProjectDetail createProjectDetail(ProjectRequest projectRequest) {
    log.entry();
    String projectId = UUID.randomUUID().toString();
    ProjectDetail projectDetail = DtoToDao.toProjectDetail(projectRequest);
    projectDetail.setFileUploadToken(UUID.randomUUID().toString());
    projectDetail.setFileUploadTokenAlive(true);
    projectDetail.setCreatedAt(DateUtil.now());
    projectDetail.setUpdatedAt(DateUtil.now());
    projectDetail.setId(projectId);
    log.exit();
    return projectDetail;
  }

  private List<FileCategory> fetchCategories(String organizationId) {
    return this.fileManagerAdapter.getCategories(organizationId);
  }

  private List<FileCategory> fetchCategories(String organizationId, String projectId) {
    return this.fileManagerAdapter.getCategories(organizationId, projectId);
  }

  private List<FileCategory> getCategoriesByProject(String projectId,
      List<FileCategory> fileCategoryList) {
    if (fileCategoryList == null || fileCategoryList.size() == 0) {
      return null;
    }

    return fileCategoryList.stream().filter(p -> p.getProjectId().equals(projectId))
        .collect(Collectors.toList());
  }

  private OCREngineDetails getDefaultOCREngineDetail() throws IOException {

    OCREngineDetails currentOCREngineDetail = ocrEngineDetailsDao
        .getOCREngineDetails(Main.GetCurrentOCREngine());
    return currentOCREngineDetail;
  }

  private int getConfidenceThreshold() {
    try {
      int confidenceThreshold = (int) Double
          .parseDouble(configurationService.getConfiguration(CONFIDENCE_THRESHOLD_KEY));
      if (confidenceThreshold > 100) {
        return 100;
      }
      if (confidenceThreshold < 0) {
        return 0;
      }
      return confidenceThreshold;
    } catch (NotFoundException e) {
      log.debug(() -> String
          .format("Key not found: %s, Taking default value: %s", CONFIDENCE_THRESHOLD_KEY,
              DEFAULT_CONFIDENCE_THRESHOLD));
      return DEFAULT_CONFIDENCE_THRESHOLD;
    } catch (NumberFormatException e) {
      log.error(() -> String
          .format(e.getMessage(), "Taking default value: %s", DEFAULT_CONFIDENCE_THRESHOLD), e);
      return DEFAULT_CONFIDENCE_THRESHOLD;
    }
  }

  @Transactional
  @Override
  public List<Task> getSortedTaskList(String sortColumnName, String sortOrder) {
    return taskService.getSortedTaskList(sortColumnName, sortOrder);
  }

}
