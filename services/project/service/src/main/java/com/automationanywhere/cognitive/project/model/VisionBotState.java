package com.automationanywhere.cognitive.project.model;

/**
 * Created by Jemin.Shah on 29-11-2016.
 */
public enum VisionBotState
{
	training,
    active,
    ready
}
