package com.automationanywhere.cognitive.project.model.dto.imports;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectImportRequest {

  @JsonProperty("archive")
  private ArchiveInfo archiveDetails;

  private String userId;

  private String option;

  public ArchiveInfo getArchiveDetails() {
    return archiveDetails;
  }

  public void setArchiveDetails(ArchiveInfo archiveDetails) {
    this.archiveDetails = archiveDetails;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getOption() {
    return option;
  }

  public void setOption(String option) {
    this.option = option;
  }
}
