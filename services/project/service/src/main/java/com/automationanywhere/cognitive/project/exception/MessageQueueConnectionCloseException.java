package com.automationanywhere.cognitive.project.exception;

public class MessageQueueConnectionCloseException extends RuntimeException{

    private static final long serialVersionUID = 1230719122458182580L;

    public MessageQueueConnectionCloseException(String message, Throwable cause) {
        super(message, cause);
    }

}