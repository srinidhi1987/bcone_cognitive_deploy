package com.automationanywhere.cognitive.project.constants;

/**
 * Table constants mapped by table name.
 */
public enum TableConstants {

  CLASSIFICATIONREPORT("ClassificationReport"),
  CONTENTTRAINSET("ContentTrainSet"),
  CUSTOMFIELDDETAIL("CustomFieldDetail"),
  DOCUMENTPROCCESSEDDATAILS("DocumentProccessedDatails"),
  FILEBLOBS("FileBlobs"),
  FILEDETAILS("FileDetails"),
  LAYOUTTRAINSET("LayoutTrainSet"),
  PROJECTDETAIL("ProjectDetail"),
  PROJECTOCRENGINEDETAILSMASTERMAPPING("ProjectOCREngineDetailsMasterMapping"),
  STANDARDFIELDDETAIL("StandardFieldDetail"),
  TESTSET("TestSet"),
  TESTSETINFO("TestSetInfo"),
  TRAININGDATA("trainingData"),
  VISIONBOT("VisionBot"),
  VISIONBOTDETAIL("VisionbotDetail"),
  VISIONBOTRUNDETAILS("VisionBotRunDetails");

  private final String name;

  private TableConstants(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }
}
