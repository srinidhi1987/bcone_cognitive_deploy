package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.ProjectOCREngineDetailsMasterMappingDao;
import com.automationanywhere.cognitive.project.model.dao.ProjectOCREngineDetailsMasterMapping;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ProjectOCREngineDetailsMasterMappingImpl implements
    ProjectOCREngineDetailsMasterMappingDao {

  private static final int BATCH_SIZE = 20;
  private AALogger log = AALogger.create(ProjectOCREngineDetailsMasterMappingImpl.class);
  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;
  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  public List<String[]> getProjectOCREngineDetailsMasterMappings(String projectId) {
    log.entry();

    String sqlQuery = "select * from ProjectOCREngineDetailsMasterMapping where ProjectId = :projectId";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> projectOCREngineDetailsMasterMappings = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return projectOCREngineDetailsMasterMappings;
  }

  @Override
  public List<ProjectOCREngineDetailsMasterMapping> getProjectOcrMappingByProjectId(
      String projectId) {
    Query<ProjectOCREngineDetailsMasterMapping> query = sessionFactory.getCurrentSession()
        .createQuery("from ProjectOCREngineDetailsMasterMapping where projectId = :projectId",
            ProjectOCREngineDetailsMasterMapping.class);
    query.setParameter("projectId", projectId);
    return query.getResultList();
  }

  @Override
  public void add(
      ProjectOCREngineDetailsMasterMapping projectOcrEngineDetailsMasterMapping) {
    Session session = sessionFactory.getCurrentSession();
    session.save(projectOcrEngineDetailsMasterMapping);
  }

  @Override
  public void delete(String projectId) {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("delete ProjectOCREngineDetailsMasterMapping where projectId = :projectId");
    query.setParameter("projectId", projectId);
    query.executeUpdate();
  }
}


