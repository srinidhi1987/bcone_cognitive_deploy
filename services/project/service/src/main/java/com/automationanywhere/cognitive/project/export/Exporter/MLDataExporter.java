package com.automationanywhere.cognitive.project.export.Exporter;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.TrainingDataDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.model.MlDataExportMetadata;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class MLDataExporter implements DataExporter<MlDataExportMetadata> {

  @Inject
  private TrainingDataDao trainingdataDao;

  @Inject
  private FileWriter csvFileWriter;

  private AALogger log = AALogger.create(MLDataExporter.class);

  @Transactional(value = "mlTransactionManager", propagation = Propagation.REQUIRES_NEW)
  @Override
  public MlDataExportMetadata exportData(String path, Map<Object, Object> params)
      throws ProjectExportException {
    log.entry();
    MlDataExportMetadata mlDataExportMetadata;
    try {
      String projectId = params.get("projectId").toString();
      String projectExportDirPath = prepareProjectDirectory(path, projectId);

      long trainingDataRowCount = exportTrainingData(projectExportDirPath, projectId);

      mlDataExportMetadata = new MlDataExportMetadata(trainingDataRowCount);
    } catch (Exception e) {
      throw new ProjectExportException("Got error while exporting ml related data to csv.", e);
    }
    log.exit();
    return mlDataExportMetadata;
  }


  private long exportTrainingData(String projectExportDirPath, String projectId)
      throws IOException {
    log.entry();
    long trainingDataRowCount = 0l;
    List<String[]> trainingData = trainingdataDao.getTrainingDatas(projectId);
    if (trainingData != null && !trainingData.isEmpty()) {
      String[] headerData = {"originalvalue", "correctedvalue", "fieldid", "correctedon","id"};
      trainingData.add(0, headerData);
      csvFileWriter
          .writeData(Paths.get(projectExportDirPath, TableConstants.TRAININGDATA.getName()+".csv"), trainingData);
      trainingDataRowCount = trainingData.size() - 1;
    } else {
      log.info("trainingData data not found for projectid : " + projectId);
    }
    log.exit();
    return  trainingDataRowCount;
  }


  public String prepareProjectDirectory(String path, String projectId) {
    String projectExportDirPath = path + "/" + projectId;
    File file = new File(projectExportDirPath);
    if (!file.exists()) {
      file.mkdirs();
    }
    return projectExportDirPath;
  }
}
