package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.DataCleaningDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectAlreadyExistException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.impl.FileZipper;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.DataImporter;
import com.automationanywhere.cognitive.project.imports.ImportEvents;
import com.automationanywhere.cognitive.project.imports.ImportManager;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.ProjectDetail;
import com.automationanywhere.cognitive.project.imports.csvmodel.VisionBot;
import com.automationanywhere.cognitive.project.imports.csvmodel.VisionbotDetail;
import com.automationanywhere.cognitive.project.imports.csvmodel.trainingData;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Imports data files.
 */

@Component
public class ImportManagerImpl implements ImportManager {

  private static final AALogger LOGGER = AALogger.create(ImportManagerImpl.class);
  private final DataImporter dataImporter;
  private final FileZipper fileZipper;
  private final FileReader csvFileReader;
  private final FileWriter csvFileWriter;
  private final FileHelper fileHelper;
  private final DataCleaningDao dataCleaningDao;
  private final AdvanceDataImporter contentTrainSetDataImporter;
  private final AdvanceDataImporter projectDetailDataImporter;
  private final AdvanceDataImporter customFieldDetailDataImporter;
  private final AdvanceDataImporter standardFieldDetailDataImporter;
  private final AdvanceDataImporter projectOcrEngineDetailMasterMappingImporter;
  private final AdvanceDataImporter fileDetailsDataImporter;
  private final AdvanceDataImporter fileBlobsDataImporter;
  private final AdvanceDataImporter classificationReportDataImporter;
  private final AdvanceDataImporter layoutTrainSetDataImporter;
  private final AdvanceDataImporter visionBotDetailDataImporter;
  private final AdvanceDataImporter testSetInfoDataImporter;
  private final AdvanceDataImporter visionBotDataImporter;
  private final AdvanceDataImporter visionBotRunDetailsDataImporter;
  private final AdvanceDataImporter documentProcessedDetailsDataImporter;
  private final AdvanceDataImporter testSetDataImporter;
  private final AdvanceDataImporter trainingDataImporter;
  private final TransitionResult transitionResult;

  Task task;
  private ImportEvents importEvents;

  @Autowired
  public ImportManagerImpl(
      DataImporter dataImporter,
      FileZipper fileZipper,
      FileReader csvFileReader,
      FileWriter csvFileWriter,
      FileHelper fileHelper,
      AdvanceDataImporter contentTrainSetDataImporter,
      AdvanceDataImporter layoutTrainSetDataImporter,
      AdvanceDataImporter projectDetailDataImporter,
      AdvanceDataImporter customFieldDetailDataImporter,
      AdvanceDataImporter standardFieldDetailDataImporter,
      AdvanceDataImporter projectOcrEngineDetailMasterMappingImporter,
      DataCleaningDao dataCleaningDao,
      AdvanceDataImporter fileDetailsDataImporter,
      AdvanceDataImporter fileBlobsDataImporter,
      AdvanceDataImporter classificationReportDataImporter,
      AdvanceDataImporter visionBotDetailDataImporter,
      AdvanceDataImporter testSetInfoDataImporter,
      AdvanceDataImporter visionBotDataImporter,
      AdvanceDataImporter testSetDataImporter,
      AdvanceDataImporter visionBotRunDetailsDataImporter,
      AdvanceDataImporter documentProcessedDetailsDataImporter,
      AdvanceDataImporter trainingDataImporter,
      TransitionResult transitionResult) {
    this.dataImporter = dataImporter;
    this.fileZipper = fileZipper;
    this.csvFileReader = csvFileReader;
    this.csvFileWriter = csvFileWriter;
    this.fileHelper = fileHelper;
    this.contentTrainSetDataImporter = contentTrainSetDataImporter;
    this.layoutTrainSetDataImporter = layoutTrainSetDataImporter;
    this.projectDetailDataImporter = projectDetailDataImporter;
    this.customFieldDetailDataImporter = customFieldDetailDataImporter;
    this.standardFieldDetailDataImporter = standardFieldDetailDataImporter;
    this.projectOcrEngineDetailMasterMappingImporter = projectOcrEngineDetailMasterMappingImporter;
    this.dataCleaningDao = dataCleaningDao;
    this.fileDetailsDataImporter = fileDetailsDataImporter;
    this.fileBlobsDataImporter = fileBlobsDataImporter;
    this.classificationReportDataImporter = classificationReportDataImporter;
    this.visionBotDetailDataImporter = visionBotDetailDataImporter;
    this.testSetInfoDataImporter = testSetInfoDataImporter;
    this.visionBotDataImporter = visionBotDataImporter;
    this.testSetDataImporter = testSetDataImporter;
    this.visionBotRunDetailsDataImporter = visionBotRunDetailsDataImporter;
    this.documentProcessedDetailsDataImporter = documentProcessedDetailsDataImporter;
    this.trainingDataImporter = trainingDataImporter;
    this.transitionResult = transitionResult;

  }

  /**
   * Following steps are done by this method.
   *
   * a)Unzipes .zip file. b)Imports data from files. c)Logs task status(failed or complete).
   *
   * @param task task object.
   */
  public void run(Task task, ImportEvents importEvents) throws Exception {
    LOGGER.entry();
    this.task = task;
    this.importEvents = importEvents;

    Path archivePath = Paths.get(task.getDescription());
    Path archiveParentPath = archivePath.getParent();
    Path archiveDirectoryPath = Paths.get(archiveParentPath.toString(),
        archivePath.getFileName().toString().replace(".iqba", ""));
    try {
      //unzipes the directory
      fileZipper.unzipIt(archivePath, archiveParentPath);

      defineClassifierChain();
      defineProjectChain();

      importData(task, archiveDirectoryPath);

      //extracted directory getting removed after successful import.
      fileHelper.removeDirectory(archiveDirectoryPath.toFile());

    } catch (Exception ex) {
      importEvents.onError(ex);
    }

    importEvents.onComplete(task);
    LOGGER.exit();
  }

  private void defineProjectChain() {
    projectDetailDataImporter.setNext(standardFieldDetailDataImporter);
    standardFieldDetailDataImporter.setNext(customFieldDetailDataImporter);
    customFieldDetailDataImporter.setNext(projectOcrEngineDetailMasterMappingImporter);
    projectOcrEngineDetailMasterMappingImporter.setNext(fileDetailsDataImporter);
    fileDetailsDataImporter.setNext(fileBlobsDataImporter);
    fileBlobsDataImporter.setNext(classificationReportDataImporter);
    classificationReportDataImporter.setNext(visionBotDetailDataImporter);
    visionBotDetailDataImporter.setNext(visionBotDataImporter);
    visionBotDataImporter.setNext(testSetInfoDataImporter);
    testSetInfoDataImporter.setNext(testSetDataImporter);
    testSetDataImporter.setNext(visionBotRunDetailsDataImporter);
    visionBotRunDetailsDataImporter.setNext(documentProcessedDetailsDataImporter);
    documentProcessedDetailsDataImporter.setNext(trainingDataImporter);
  }

  private void defineClassifierChain() {
    contentTrainSetDataImporter.setNext(layoutTrainSetDataImporter);
  }

  private void importData(Task task, Path archiveDirectoryPath)
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    if (task.getTaskOptions().equals(TaskOptions.CLEAR)) {
      clearData();
    }

    importClassificationData(task, archiveDirectoryPath);

    List<String> projectIds = getProjectIds(archiveDirectoryPath);
    for (String projectId : projectIds) {
      importProjectData(task, archiveDirectoryPath, projectId);
    }
  }

  private void clearData() {
    dataCleaningDao.cleanClassifierDetails();
    dataCleaningDao.cleanProjectDetails();
  }

  private void importClassificationData(Task task, Path archiveDirectoryPath)
      throws ProjectImportException, DuplicateProjectNameException, ParseException, IOException {
    transitionResult.clearClassificationDetails();
    contentTrainSetDataImporter.importData(archiveDirectoryPath, task.getTaskOptions());
  }

  private void importProjectData(Task task, Path archiveDirectoryPath, String projectId)
      throws IOException, com.automationanywhere.cognitive.project.exception.ProjectImportException, java.text.ParseException {
    try {
      transitionResult.clearProjectDetails();
      transitionResult.setProjectId(projectId);
      transitionResult.setUserId(task.getUserId());
      projectDetailDataImporter.importData(archiveDirectoryPath, task.getTaskOptions());
    } catch (ProjectAlreadyExistException | DuplicateProjectNameException | ProjectImportException ex) {
      LOGGER.info(String.format("Skipping from import - project id - %s", projectId));
      LOGGER.debug(ex);
    }
  }

  /**
   * Walks into a directory to find path of .csv files and builds a map object having list of paths
   * grouped by table name constants.
   */
  private Map<TableConstants, List<Path>> getDataFilePathMapping(List<Path> lstOfFilePaths)
      throws IOException {
    Pattern endingPattern = Pattern.compile("^.+_\\d*.csv$");

    Map<TableConstants, List<Path>> dataFilePathMap =
        lstOfFilePaths.stream().filter(path -> path.toString().endsWith(".csv"))
            .collect(Collectors.groupingBy(new Function<Path, TableConstants>() {

              @Override
              public TableConstants apply(Path p) {
                String filename = p.getFileName().toString();

                String tableName;

                if (endingPattern.matcher(filename).matches()) {
                  tableName = filename.split("_")[0];
                } else {
                  tableName = filename.substring(0, filename.indexOf("."));
                }

                return TableConstants.valueOf(tableName.trim().toUpperCase());
              }
            }));

    if (dataFilePathMap == null) {
      dataFilePathMap = new HashMap<>();
    }

    for (TableConstants tableConstant : TableConstants.values()) {
      if (!dataFilePathMap.containsKey(tableConstant)) {
        dataFilePathMap.put(tableConstant, Collections.emptyList());
      }
    }
    return dataFilePathMap;
  }

  private List<String> getProjectIds(Path archiveDirectoryPath) throws IOException {
    List<Path> paths = fileHelper.getAllFilesPaths(archiveDirectoryPath);
    Pattern projectIdPattern = Pattern.compile("^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$");
    return paths.stream()
        .filter(path -> projectIdPattern.matcher(path.getFileName().toString()).matches())
        .map(fpath -> fpath.getFileName().toString()).collect(Collectors.toList());
  }

  /**
   * Updates csv's of respective table
   */
  private void updateInitialValuesInCsv(Map<TableConstants, List<Path>> dataFilePathMap, Task task)
      throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
    updateProjectDetailCsv(dataFilePathMap.get(TableConstants.PROJECTDETAIL), task);
    updateVisionBotDataCsv(dataFilePathMap.get(TableConstants.VISIONBOT));
    updateVisionBotDetailDataCsv(dataFilePathMap.get(TableConstants.VISIONBOTDETAIL));
    updateTrainingDataCsv(dataFilePathMap.get(TableConstants.TRAININGDATA));

  }

  /**
   * Imports Data
   */
  private void importData(Map<TableConstants, List<Path>> dataFilePathMap) {
    LOGGER.entry();
    //imports project related data.
    Map<String, Integer> rowsUpdTblPrd = dataImporter
        .importProjectData(dataFilePathMap);
    //imports file manager related data.
    Map<String, Integer> rowsUpdTblMrd = dataImporter
        .importFileManagerData(dataFilePathMap);
    //imports vision bot related data.
    Map<String, Integer> rowsUpdTblBrd = dataImporter
        .importVisionBotData(dataFilePathMap);
    //imports ml related data.
    Map<String, Integer> rowsUpdTblMlrd = dataImporter.importMlData(dataFilePathMap);
    //imports classifier related data.
    Map<String, Integer> rowsUpdTblCrd = dataImporter
        .importClassifierData(dataFilePathMap);

    //Construct a string message having information about the rows added to a particular table.
    @SuppressWarnings("unchecked")
    String rowsUpdatedInfoStr = getUpdatedRowsCount(rowsUpdTblPrd, rowsUpdTblMrd, rowsUpdTblBrd,
        rowsUpdTblMlrd, rowsUpdTblCrd);

    LOGGER.info(rowsUpdatedInfoStr);
    LOGGER.exit();
  }

  /**
   * updates fields of projectdetail and saves it.
   */
  public void updateProjectDetailCsv(List<Path> paths, Task task)
      throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

    String createdBy = task.getUserId();
    String createdAt = new java.sql.Timestamp(System.currentTimeMillis()).toString();
    String lastUpdatedBy = "";
    String updatedAt = "";

    for (Path filepath : paths) {
      List<ProjectDetail> projectDetailData =
          csvFileReader.readAllData(filepath, ProjectDetail.class, 1);

      projectDetailData.forEach(projectDetail -> {
        projectDetail.setCreatedBy(createdBy);
      });

      fileHelper.deletFile(filepath);

      csvFileWriter.writeData(filepath, ProjectDetail.class
          , projectDetailData);
    }
  }

  /**
   * Updates fields of visionbot
   */
  public void updateVisionBotDataCsv(List<Path> paths)
      throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

    String lockedUserId = "";

    for (Path filepath : paths) {
      List<VisionBot> visionBotData =
          csvFileReader.readAllData(filepath, VisionBot.class, 1);

      visionBotData.forEach(visionBotRow -> {
        visionBotRow.setLockedUserid(lockedUserId);
      });

      fileHelper.deletFile(filepath);

      csvFileWriter.writeData(filepath, VisionBot.class
          , visionBotData);
    }
  }

  /**
   * Updates fields of visionbotDetail
   */
  public void updateVisionBotDetailDataCsv(List<Path> paths)
      throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

    String lockedUserId = "";

    for (Path filepath : paths) {
      List<VisionbotDetail> visionBotDetailData =
          csvFileReader.readAllData(filepath, VisionbotDetail.class, 1);

      visionBotDetailData.forEach(visionBotDetailRow -> {
        visionBotDetailRow.setLockeduserid(lockedUserId);
      });

      fileHelper.deletFile(filepath);

      csvFileWriter.writeData(filepath, VisionbotDetail.class
          , visionBotDetailData);
    }
  }

  /**
   * Updates field of trainginData and saves it.
   */
  public void updateTrainingDataCsv(List<Path> paths)
      throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

    String correctedOn = new java.sql.Timestamp(System.currentTimeMillis()).toString();

    for (Path filepath : paths) {
      List<trainingData> trainingData =
          csvFileReader.readAllData(filepath, trainingData.class, 1);

      trainingData.forEach(traingingRow -> {
        traingingRow.setCorrectedon(correctedOn);
      });

      fileHelper.deletFile(filepath);

      csvFileWriter.writeData(filepath, trainingData.class
          , trainingData);
    }
  }

  /**
   * Builds a string message having information about the rows added to a particular table.
   *
   * @param collmapOfRowsUpdatedCount map of rows count grouped by table name.
   * @return string object.
   */
  @SuppressWarnings("unchecked")
  private String getUpdatedRowsCount(Map<String, Integer>... collmapOfRowsUpdatedCount) {

    StringJoiner strJoiner = new StringJoiner("\n");

    for (Map<String, Integer> mapOfRowsUpdatedCount : collmapOfRowsUpdatedCount) {
      mapOfRowsUpdatedCount.entrySet().forEach(entry -> strJoiner
          .add(String.format(" %s rows added in table : %s", entry.getValue(), entry.getKey())));
    }
    return strJoiner.toString();
  }

  public void setTask(Task task) {
    this.task = task;
  }

}