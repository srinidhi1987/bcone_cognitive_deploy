package com.automationanywhere.cognitive.project.model.dao;

import com.automationanywhere.cognitive.project.model.constants.Environment;
import com.automationanywhere.cognitive.project.model.constants.RunState;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "VisionBotRunDetails")
public class VisionBotRunDetails {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "visionbotid")
  private String visionbotId;

  @Column(name = "createdat")
  private Date createdAt;

  @Column(name = "environment")
  @Enumerated(EnumType.STRING)
  private Environment environment;

  @Column(name = "totalFiles")
  private int totalFiles = 0;

  @Column(name = "runstateid")
  @Enumerated(EnumType.ORDINAL)
  private RunState runState = RunState.Running;

  public VisionBotRunDetails(){

  }
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getVisionbotId() {
    return visionbotId;
  }

  public void setVisionbotId(String visionbotId) {
    this.visionbotId = visionbotId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(
      Environment environment) {
    this.environment = environment;
  }

  public int getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(int totalFiles) {
    this.totalFiles = totalFiles;
  }

  public RunState getRunState() {
    return runState;
  }

  public void setRunState(RunState runState) {
    this.runState = runState;
  }
}
