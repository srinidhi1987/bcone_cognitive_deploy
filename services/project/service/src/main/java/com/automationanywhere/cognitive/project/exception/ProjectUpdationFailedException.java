package com.automationanywhere.cognitive.project.exception;

public class ProjectUpdationFailedException extends Exception{

  private static final long serialVersionUID = 8120653630669164640L;

  public ProjectUpdationFailedException(String message){super(message);}
  public ProjectUpdationFailedException(String message,Exception ex){super(message,ex);}
}
