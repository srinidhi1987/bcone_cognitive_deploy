package com.automationanywhere.cognitive.project.configuration;

public interface FileChangedEvent {
    void fileChanged(String filepath);
}
