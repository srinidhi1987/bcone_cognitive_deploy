package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.TestSetInfoDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.dao.TestSetInfo;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class TestSetInfoDataImporter extends
    AdvanceDataImporter<com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo> {

  private static final AALogger LOGGER = AALogger.create(TestSetInfoDataImporter.class);

  private final FileHelper fileHelper;
  private final TransitionResult transitionResult;
  private final TestSetInfoDao testSetInfoDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public TestSetInfoDataImporter(FileReader csvFileReader, FileHelper fileHelper,
      TransitionResult transitionResult, TestSetInfoDao testSetInfoDao,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.fileHelper = fileHelper;
    this.transitionResult = transitionResult;
    this.testSetInfoDao = testSetInfoDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    LOGGER.entry();

    //fetch's data from csv path
    List<TestSetInfo> dataToImport = getDataFromFiles(unzippedFolderPath);

    //data prepared for import
    List<TestSetInfo> finalDataToImport = prepareData(dataToImport);

    //data inserted in batch.
    if (!finalDataToImport.isEmpty()) {
      testSetInfoDao.batchInsertTestSetInfo(finalDataToImport);
    }
    //sets imported testSetInfoId's to transitionResult for further use.
    List<String> importedTestSetInfoList = finalDataToImport.stream().map(testSetInfoObj->testSetInfoObj.getId()).collect(Collectors.toList());
    transitionResult.setImportedTestSetInfoIdList(importedTestSetInfoList);

    //next importer get's called.
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
    LOGGER.exit();

  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<TestSetInfo> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    LOGGER.entry();
    List<TestSetInfo> dataToImport = new ArrayList<>();
    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvRegexPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.TESTSETINFO);
    List<Path> pathsOfTestSetInfo = fileHelper
        .getAllFilesPaths(csvFolderPath, csvRegexPattern);

    dataToImport
        .addAll(DtoToDao.toTestSetInfos(getDataFromCsv(pathsOfTestSetInfo,
            com.automationanywhere.cognitive.project.imports.csvmodel.TestSetInfo.class)));
    LOGGER.exit();

    return dataToImport;
  }

  private List<TestSetInfo> prepareData(List<TestSetInfo> dataToImport) {
    return filterByVisionBotId(transitionResult.getImportedVisionBotIdList(),
        dataToImport);
  }

  private List<TestSetInfo> filterByVisionBotId(List<String> visionBotIdList,
      List<TestSetInfo> testSetInfoList) {
    return testSetInfoList.stream().filter(testSetInfo ->
        visionBotIdList.contains(testSetInfo.getVisionbotid())).collect(Collectors.toList());
  }
}
