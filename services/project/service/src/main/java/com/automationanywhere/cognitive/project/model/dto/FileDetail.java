package com.automationanywhere.cognitive.project.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jemin.Shah on 11-01-2017.
 */
public class FileDetail
{
    @JsonProperty("fileId")
    String fileId;

    @JsonProperty("projectId")
    String projectId;

    @JsonProperty("fileName")
    String fileName;

    @JsonProperty("fileLocation")
    String fileLocation;

    @JsonProperty("fileSize")
    double fileSize;

    @JsonProperty("fileHeight")
    int fileHeight;

    @JsonProperty("fileWidth")
    int fileWidth;

    @JsonProperty("format")
    String format;

    @JsonProperty("processed")
    boolean processed;

    @JsonProperty("classificationId")
    String classificationId;

    @JsonProperty("uploadrequestId")
    String uploadrequestId;

    @JsonProperty("layoutId")
    String layoutId;

    @JsonProperty("isProduction")
    boolean isProduction;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    public int getFileHeight() {
        return fileHeight;
    }

    public void setFileHeight(int fileHeight) {
        this.fileHeight = fileHeight;
    }

    public int getFileWidth() {
        return fileWidth;
    }

    public void setFileWidth(int fileWidth) {
        this.fileWidth = fileWidth;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public String getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(String classificationId) {
        this.classificationId = classificationId;
    }

    public String getUploadrequestId() {
        return uploadrequestId;
    }

    public void setUploadrequestId(String uploadrequestId) {
        this.uploadrequestId = uploadrequestId;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public boolean isProduction() {
        return isProduction;
    }

    public void setProduction(boolean production) {
        this.isProduction = production;
    }

    @Override
    public String toString() {
        return "FileDetail{" +
                "fileId='" + fileId + '\'' +
                ", projectId='" + projectId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileLocation='" + fileLocation + '\'' +
                ", fileSize=" + fileSize +
                ", fileHeight=" + fileHeight +
                ", fileWidth=" + fileWidth +
                ", format='" + format + '\'' +
                ", processed=" + processed +
                ", classificationId='" + classificationId + '\'' +
                ", uploadrequestId='" + uploadrequestId + '\'' +
                ", layoutId='" + layoutId + '\'' +
                ", isProduction=" + isProduction +
                '}';
    }
}
