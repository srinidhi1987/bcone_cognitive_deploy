package com.automationanywhere.cognitive.project.model.dao;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VisionbotDetail")
public class VisionBotDetail {

  @Id
  @Column(name = "id", nullable = false)
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  String Id;

  @Column(name = "organizationId")
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "organizationid")
  String OrganizationId;

  @Column(name = "projectid")
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "projectid")
  String projectId;

  @Column(name = "classificationid")
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "classificationid")
  String classificationId;

  @Column(name = "environment")
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "environment")
  String environment;

  @Column(name = "LockedUserid")
  @CsvBindByPosition(position = 5)
  @CsvBindByName(column = "lockeduserid")
  String lockedUserId;

  public String getId() {
    return Id;
  }

  public void setId(String id) {
    Id = id;
  }

  public String getOrganizationId() {
    return OrganizationId;
  }

  public void setOrganizationId(String organizationId) {
    OrganizationId = organizationId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getLockedUserId() {
    return lockedUserId;
  }

  public void setLockedUserId(String lockedUserId) {
    this.lockedUserId = lockedUserId;
  }

  public VisionBotDetail getCopy() {
    VisionBotDetail visionBotDetail = new VisionBotDetail();
    visionBotDetail.setId(getId());
    visionBotDetail.setOrganizationId(getOrganizationId());
    visionBotDetail.setProjectId(getProjectId());
    visionBotDetail.setClassificationId(getClassificationId());
    visionBotDetail.setEnvironment(getEnvironment());
    visionBotDetail.setLockedUserId(getLockedUserId());
    return visionBotDetail;
  }
}
