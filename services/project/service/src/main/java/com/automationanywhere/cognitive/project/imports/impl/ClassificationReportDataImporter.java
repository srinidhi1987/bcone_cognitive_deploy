package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.ClassificationReportDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.ClassificationReport;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class ClassificationReportDataImporter extends
    AdvanceDataImporter<com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport> {

  private static final AALogger LOGGER = AALogger.create(ClassificationReportDataImporter.class);

  private final FileHelper fileHelper;
  private final TransitionResult transitionResult;
  private final ClassificationReportDao classificationReportDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public ClassificationReportDataImporter(
      FileReader csvFileReader,
      FileHelper fileHelper,
      TransitionResult transitionResult,
      ClassificationReportDao classificationReportDao,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.fileHelper = fileHelper;
    this.transitionResult = transitionResult;
    this.classificationReportDao = classificationReportDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    LOGGER.entry();
    //fetch's data from csv path
    List<ClassificationReport> dataToImport = getDataFromFiles(unzippedFolderPath);
    if (taskOptions == TaskOptions.OVERWRITE) {
      classificationReportDao.delete(transitionResult.getFileIds());
    }
    //data prepared for import
    List<ClassificationReport> finalDataToImport = filterByFileIds(transitionResult.getFileIds(),
        dataToImport);
    if (!finalDataToImport.isEmpty()) {
      classificationReportDao.batchInsertClassificationReport(finalDataToImport);
    }
    //next importer get's called.
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
    LOGGER.exit();

  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<ClassificationReport> getDataFromFiles(
      Path unzippedFolderPath) throws IOException, ProjectImportException {
    LOGGER.entry();

    List<ClassificationReport> dataToImport = new ArrayList<>();

    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvRegexPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.CLASSIFICATIONREPORT);
    List<Path> pathsOfClassificationReport = fileHelper
        .getAllFilesPaths(csvFolderPath, csvRegexPattern);

    dataToImport
        .addAll(DtoToDao.toClassificationReport(getDataFromCsv(pathsOfClassificationReport,
            com.automationanywhere.cognitive.project.imports.csvmodel.ClassificationReport.class)));

    LOGGER.exit();

    return dataToImport;
  }

  private List<ClassificationReport> filterByFileIds(List<String> fileIds,
      List<ClassificationReport> classificationReportList) {
    return classificationReportList.stream()
        .filter(classificationReport -> fileIds.contains(classificationReport.getDocumentId()))
        .collect(Collectors.toList());
  }
}
