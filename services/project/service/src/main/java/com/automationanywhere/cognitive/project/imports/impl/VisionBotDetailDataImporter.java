package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.VisionbotDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.VisionBotDetail;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class VisionBotDetailDataImporter extends AdvanceDataImporter<VisionBotDetail> {

  private static final AALogger LOGGER = AALogger.create(VisionBotDetailDataImporter.class);
  private final FileHelper fileHelper;
  private final VisionbotDetailDao visionBotDetailDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private final List<VisionBotDetail> dataToImport = new ArrayList<>();
  private final List<String> importedVisionBotIdList = new ArrayList<>();
  private final TransitionResult transitionResult;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public VisionBotDetailDataImporter(FileReader fileReader,
      FileHelper fileHelper,
      VisionbotDetailDao VisionBotDetailDao,
      ImportFilePatternConstructor importFilePatternConstructor,
      TransitionResult transitionResult) {
    super(fileReader);
    this.fileHelper = fileHelper;
    this.visionBotDetailDao = VisionBotDetailDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
    this.transitionResult = transitionResult;
  }

  private void insertVisionBotData(List<VisionBotDetail> csvData, TaskOptions taskOptions) {
    dataToImport.clear();
    importedVisionBotIdList.clear();
    List<VisionBotDetail> existingData = new ArrayList<>();

    switch (taskOptions) {
      case CLEAR:
        amendCsvData(csvData);
        break;
      case APPEND:
      case MERGE:
        filterDataForAppendAndMergeOption(csvData);
        break;
      case OVERWRITE:
        visionBotDetailDao.delete(transitionResult.getProjectId());
        amendCsvData(csvData);
        break;
    }
    for (VisionBotDetail visionBotId : dataToImport) {
      importedVisionBotIdList.add(visionBotId.getId());
    }
    visionBotDetailDao.addAll(dataToImport);
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //read data from csv
    List<VisionBotDetail> csvData = getDataFromFiles(unzippedFolderPath);

    //import data
    insertVisionBotData(csvData, taskOptions);

    //set result
    transitionResult.setImportedVisionBotIdList(importedVisionBotIdList);

    //call other data importer
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<VisionBotDetail> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.VISIONBOTDETAIL);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, VisionBotDetail.class);
  }

  private void filterDataForAppendAndMergeOption(List<VisionBotDetail> csvData) {
    List<VisionBotDetail> existingData = visionBotDetailDao.getAll(transitionResult.getProjectId());
    for (VisionBotDetail importRow : csvData) {
      Optional<VisionBotDetail> matchingRow = existingData.stream()
          .filter(p -> p.getId().equals(importRow.getId())).findFirst();
      if (!matchingRow.isPresent()) {
        amendData(importRow);
        dataToImport.add(importRow);
        LOGGER.debug(String.format("Adding visionBot having id %s", importRow.getId()));
      } else {
        LOGGER.debug(String.format("Skipping visionBot having id %s", importRow.getId()));
      }
    }
  }

  private void amendCsvData(List<VisionBotDetail> csvData) {
    for (VisionBotDetail importRow : csvData) {
      amendData(importRow);
      dataToImport.add(importRow);
      LOGGER.debug(String.format("Adding visionBot having id %s", importRow.getId()));
    }
  }

  private void amendData(VisionBotDetail importRow) {
    importRow.setLockedUserId(null);
    Map<Integer, Integer> modifiedClasses = transitionResult.getModifiedClasses();
    if (modifiedClasses.containsKey(Integer.parseInt(importRow.getClassificationId()))) {
      importRow.setClassificationId(
          modifiedClasses.get(Integer.parseInt(importRow.getClassificationId())).toString());
    }
  }
}
