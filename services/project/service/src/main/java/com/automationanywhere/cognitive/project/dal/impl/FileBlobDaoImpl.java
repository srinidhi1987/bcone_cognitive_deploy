package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.FileBlobDao;
import com.automationanywhere.cognitive.project.model.dao.FileBlobs;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Hex;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class FileBlobDaoImpl implements FileBlobDao {

  private AALogger log = AALogger.create(FileBlobDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Override
  public List<String[]> getFileBlobs(List<String> fileIds) {
    log.entry();

    String sqlQuery = "select * from FileBlobs where fileid in (:fileIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("fileIds", fileIds);

    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> fileBlobs = null;
    if (resulSetData != null && !resulSetData.isEmpty()) {
      fileBlobs = new ArrayList<String[]>();
      for (Object[] FileDetailsObj : resulSetData) {
        String[] rowFieldStrArr = new String[FileDetailsObj.length];
        for (int i = 0; i < FileDetailsObj.length; i++) {
          if (i == 1) {
            rowFieldStrArr[i] = Hex.encodeHexString(
                (byte[]) FileDetailsObj[i]);
          } else {
            rowFieldStrArr[i] = String.valueOf(FileDetailsObj[i]);
          }
        }
        fileBlobs.add(rowFieldStrArr);
      }

    }


    log.exit();
    return fileBlobs;
  }

  @Override
  public void batchInsertFileBlobs(List<FileBlobs> fileBlobsList)  {
    Session session = sessionFactory.getCurrentSession();
    for (FileBlobs fileBlobs : fileBlobsList) {
      session.saveOrUpdate(fileBlobs);
      int index = fileBlobsList.indexOf(fileBlobs);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
    }
  }

  @Override
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}

