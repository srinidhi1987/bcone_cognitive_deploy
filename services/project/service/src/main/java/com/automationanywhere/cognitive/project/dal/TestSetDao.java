package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.TestSet;
import java.util.List;


public interface TestSetDao {

  public List<String[]> getTestSets(List<String> testInfoIds);

  public void batchInsert(List<TestSet> testSet);

  public void delete(List<String> testInfoIds);
}
