package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TaskOrderConstant;
import com.automationanywhere.cognitive.project.dal.TasksDao;
import com.automationanywhere.cognitive.project.model.TaskStatus;
import com.automationanywhere.cognitive.project.model.dao.Task;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class TasksDaoImpl implements TasksDao{

    private AALogger log = AALogger.create(this.getClass());

    @Autowired
    @Qualifier("filemanagerSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public List<Task> getRunningTasks() {
        log.entry();
        String hql = "from " + Task.class.getName()
                + " where status = :status";
        Query<Task> query = sessionFactory.getCurrentSession().createQuery(hql,Task.class);
        query.setParameter("status", TaskStatus.inprogress);
        List<Task> results = query.list();

        if (results == null || results.size() == 0) {
            return new ArrayList<Task>();
        }

        log.exit();
        return results;
    }

    @Override
    public void addTask(Task task) {
        sessionFactory.getCurrentSession().save(task);
    }

    @Override
    public void updateTask(Task task) {
        log.entry();
        sessionFactory.getCurrentSession().clear();
        sessionFactory.getCurrentSession().saveOrUpdate(task);
        sessionFactory.getCurrentSession().flush();
        log.exit();
    }

    @Override
    public List<Task> getSortedTaskList(String sortColumnName, String sortOrder) {

        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        Root<Task> root = criteriaQuery.from(Task.class);
        criteriaQuery.select(root);
        if(null != sortColumnName) {
            if (sortOrder.equalsIgnoreCase(TaskOrderConstant.DESC)) {
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortColumnName)));
            } else {
                criteriaQuery.orderBy(criteriaBuilder.asc((root.get(sortColumnName))));
            }
        }
        List<Task> results = sessionFactory.getCurrentSession().createQuery(criteriaQuery).getResultList();

        if (null == results || results.size() == 0) {
            results =  new ArrayList<Task>();
        }
        return results;
    }
}
