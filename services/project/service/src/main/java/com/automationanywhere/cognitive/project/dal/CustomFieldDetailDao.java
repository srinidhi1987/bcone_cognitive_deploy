package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.imports.CustomFieldDetail;
import java.util.List;
import org.hibernate.SessionFactory;


public interface CustomFieldDetailDao {

  public List<String[]> getCustomFieldDetails(String projectId);

  public List<CustomFieldDetail> getAll(String projectId);

  public void addAll(List<CustomFieldDetail> customFieldDetailList);

  public SessionFactory getSessionFactory();

  public void delete(String projectId);
}
