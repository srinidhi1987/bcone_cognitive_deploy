package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.VisionbotDetailDao;
import com.automationanywhere.cognitive.project.model.dao.VisionBotDetail;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

public class VisionbotDetailDaoImpl implements
    VisionbotDetailDao {

  private static final int BATCH_SIZE = 20;
  private AALogger log = AALogger.create(VisionbotDetailDaoImpl.class);
  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;
  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getVisionbotDetails(String projectId) {
    log.entry();
    Session session = sessionFactory.getCurrentSession();
    session.clear();

    String sqlQuery = "select * from VisionbotDetail where ProjectId = :projectId";

    Query query = session.createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> visionbotDetails = dataTransformer
        .transformToListOfStringArray(resulSetData);

    session.flush();
    log.exit();
    return visionbotDetails;
  }

  @Override
  public List<String> getVisionbotDetailIds(String projectId) {
    log.entry();

    String sqlQuery = "select id from VisionbotDetail where ProjectId = :projectId";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);
    @SuppressWarnings("unchecked")
    List<Object> resulSetData = query.getResultList();

    List<String> ids = dataTransformer.transformToListOfString(resulSetData);

    log.exit();
    return ids;
  }

  @Override
  public List<VisionBotDetail> getAll(String projectId) {
    Query<VisionBotDetail> query = sessionFactory.getCurrentSession()
        .createQuery("from VisionBotDetail where projectId = :projectId", VisionBotDetail.class);
    query.setParameter("projectId", projectId);
    return query.getResultList();
  }

  @Override
  public void addAll(List<VisionBotDetail> visionBotDetailList) {
    Session session = sessionFactory.getCurrentSession();
    for (VisionBotDetail visionBotDetail : visionBotDetailList) {
      session.saveOrUpdate(visionBotDetail);
      int index = visionBotDetailList.indexOf(visionBotDetail);
      if (index % BATCH_SIZE == 0) {
        session.flush();
        session.clear();
      }
    }
  }

  @Override
  public void delete(String projectId) {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("delete VisionBotDetail where projectId = :projectId");
    query.setParameter("projectId", projectId);
    query.executeUpdate();
  }

  @Override
  @Transactional
  public boolean isBotLocked() {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("From VisionBotDetail where LockedUserid != :lockUserId");
    query.setParameter("lockUserId","");
    return query.getResultList().size() > 0 ? true : false;
  }

}
