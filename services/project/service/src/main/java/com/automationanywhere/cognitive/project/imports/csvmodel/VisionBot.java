/**
 *
 */
package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class VisionBot {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "datablob")
  private String datablob;
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "validationdatablob")
  private String validationdatablob;
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "environment")
  private String environment;
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "LockedUserid")
  private String lockedUserid;

  public VisionBot() {

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDatablob() {
    return datablob;
  }

  public void setDatablob(String datablob) {
    this.datablob = datablob;
  }

  public String getValidationdatablob() {
    return validationdatablob;
  }

  public void setValidationdatablob(String validationdatablob) {
    this.validationdatablob = validationdatablob;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getLockedUserid() {
    return lockedUserid;
  }

  public void setLockedUserid(String lockedUserid) {
    this.lockedUserid = lockedUserid;
  }


  @Override
  public String toString() {
    return "VisionBot{" + "id='" + id + '\'' + ", datablob='" + datablob + '\''
        + ", validationdatablob='" + validationdatablob + '\'' + ", environment='" + environment
        + '\'' + ", lockedUserid='" + lockedUserid + '\'' + '}';
  }
}
