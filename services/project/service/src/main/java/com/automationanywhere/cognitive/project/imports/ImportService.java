package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.PreconditionFailedException;
import com.automationanywhere.cognitive.project.exception.SystemBusyException;
import com.automationanywhere.cognitive.project.model.dao.Task;
import com.automationanywhere.cognitive.project.model.dto.imports.ArchiveInfo;
import com.automationanywhere.cognitive.project.model.dto.imports.ProjectImportRequest;

import java.util.List;

public interface ImportService {
    Task startImport(ProjectImportRequest projectImportRequest)
        throws PreconditionFailedException, SystemBusyException;
    List<ArchiveInfo> getArchiveDetails();
}
