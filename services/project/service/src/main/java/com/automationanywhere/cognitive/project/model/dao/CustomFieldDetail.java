package com.automationanywhere.cognitive.project.model.dao;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.*;
import javax.validation.constraints.Pattern;

import com.automationanywhere.cognitive.project.model.FieldType;

/**
 * Created by Jemin.Shah on 08-12-2016.
 */

@Entity
@Table(name = "CustomFieldDetail")
public class CustomFieldDetail
{
    @Id
    @Column(name = "Id", length = 50)
    private String id;

    @Column(name = "Name", length = 50)
    @Pattern(regexp="^[a-zA-Z]*[ a-zA-Z0-9]*[^ ]$")
    private String name;

    @Column(name = "FieldType")
    @Enumerated(EnumType.STRING)
    private FieldType type;

    @Column(name = "OrderNumber")
    private int order;

    @Column(name="IsAddedLater")
    private boolean isAddedLater;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ProjectId")
    private ProjectDetail projectDetail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public ProjectDetail getProjectDetail() {
        return projectDetail;
    }

    public void setProjectDetail(ProjectDetail projectDetail) {
        this.projectDetail = projectDetail;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isAddedLater() {
        return isAddedLater;
    }

    public void setAddedLater(boolean addedLater) {
        this.isAddedLater = addedLater;
    }

    public static final Comparator<CustomFieldDetail> COMPARE_BY_ORDER_NUMBER = Comparator.comparingInt(base -> base.order);

}
