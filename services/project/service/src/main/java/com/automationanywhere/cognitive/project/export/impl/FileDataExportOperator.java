package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExportListener;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.ExportOperator;
import com.automationanywhere.cognitive.project.export.model.FileDataExportMetadata;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import javax.inject.Inject;


public class FileDataExportOperator implements ExportOperator {

  private static final ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
  private DataExporter fileManagerDataExporter;

  @Inject
  private FileDetailsDao fileDetailsDao;


  @Inject
  public FileDataExportOperator(DataExporter fileManagerDataExporter) {
    this.fileManagerDataExporter = fileManagerDataExporter;
  }

  @Override
  public void export(String dirPath, String[] projectIds,
      DataExportListener dataExportListener) {
    Map<String, List<String>> fileExportDetails = getFileExportDetails(projectIds, 0);
    forkJoinPool.execute(new FileDataExportTask(dirPath, fileExportDetails, dataExportListener));
  }

  private Map<String, List<String>> getFileExportDetails(String[] projectIds, int isProduction) {

    Map<String, List<String>> mapOfFileidByProjectid = fileDetailsDao
        .getFileIdsMapGroupedByProjectId(projectIds, isProduction);

    if (mapOfFileidByProjectid == null || mapOfFileidByProjectid.isEmpty()) {
      String errorMessage = "No fileid's found in filedetails table for project ids : " + String
          .join(",", projectIds);
      throw new ProjectExportException(errorMessage);
    }
    return mapOfFileidByProjectid;
  }


  private final class FileDataExportTask extends RecursiveTask<List<FileDataExportMetadata>> {

    private static final long serialVersionUID = 5926367554132528526L;  

    private static final int FILE_NUMBER_THRESHOLD = 100;

    private static final int PROJECT_NUMBER_THRESHOLD = 1;

    private final String dirPath;
    private Map<String, List<String>> mapOfProjectIdToFileIds;
    private DataExportListener dataExportListener;
    private int batchId = 0;

    public FileDataExportTask(String dirPath, Map<String, List<String>> mapOfProjectIdToFileIds,
        DataExportListener dataExportListener) {
      this.dirPath = dirPath;
      this.mapOfProjectIdToFileIds = mapOfProjectIdToFileIds;
      this.dataExportListener = dataExportListener;
    }

    private FileDataExportTask(String dirPath, Map<String, List<String>> mapOfProjectIdToFileIds,
        int batchId) {
      this.dirPath = dirPath;
      this.mapOfProjectIdToFileIds = mapOfProjectIdToFileIds;
      this.batchId = batchId;
    }

    private int fileIdCount() {
      int fileIdCount = 0;
      for (Map.Entry<String, List<String>> entry : mapOfProjectIdToFileIds.entrySet()) {
        fileIdCount += entry.getValue().size();
      }
      return fileIdCount;
    }

    @Override
    protected List<FileDataExportMetadata> compute() {
      try {
        List<FileDataExportMetadata> metadata;
        if (mapOfProjectIdToFileIds.keySet().size() > PROJECT_NUMBER_THRESHOLD
            || fileIdCount() > FILE_NUMBER_THRESHOLD) {
          metadata = ForkJoinTask.invokeAll(createSubTasks()).stream().map(ForkJoinTask::join)
              .flatMap(List::stream).collect(Collectors.toList());
        } else {
          metadata = new ArrayList<>();
          for (Map.Entry<String, List<String>> entry : mapOfProjectIdToFileIds.entrySet()) {
            metadata.add(exportData(entry.getKey(), entry.getValue()));
          }
        }
        if (dataExportListener != null) {
          dataExportListener.onFileDataExportCompleted(metadata);
        }
        return metadata;
      } catch (Exception e) {
        if (dataExportListener != null) {
          dataExportListener.onException(e);
        }
        throw e;
      }
    }

    private List<FileDataExportTask> createSubTasks() {
      List<FileDataExportTask> subTasks = new ArrayList<>();

      if (mapOfProjectIdToFileIds.keySet().size() > PROJECT_NUMBER_THRESHOLD) {
        for (Map.Entry<String, List<String>> entry : mapOfProjectIdToFileIds.entrySet()) {
          subTasks.add(new FileDataExportTask(dirPath, new HashMap<String, List<String>>() {
            private static final long serialVersionUID = 6272994020675679511L;
        	 {
            put(entry.getKey(), entry.getValue());
          }}, 0));
        }
      } else if (fileIdCount() > FILE_NUMBER_THRESHOLD) {
        for (Map.Entry<String, List<String>> entry : mapOfProjectIdToFileIds.entrySet()) {
          List<String> fileIds = entry.getValue();
          List<List<String>> subList = partition(fileIds);
          for (List<String> fedList : subList) {
            subTasks.add(new FileDataExportTask(dirPath, new HashMap<String, List<String>>() {
              private static final long serialVersionUID = 2564600876614416587L;
            {
              put(entry.getKey(), fedList);
            }}, batchId++));
          }

        }
      }
      return subTasks;
    }

    private List<List<String>> partition(List<String> fileIds) {
      List<List<String>> subList = new LinkedList<>();
      for (int i = 0; i < fileIds.size(); i += FILE_NUMBER_THRESHOLD) {
        subList.add(fileIds.subList(i, Math.min(i + FILE_NUMBER_THRESHOLD, fileIds.size())));
      }
      return subList;
    }


    private FileDataExportMetadata exportData(String projectId, List<String> fileIds)
        throws ProjectExportException {
      Map.Entry<String, List<String>> projectFileEntry = new SimpleEntry<String, List<String>>(
          projectId, fileIds);

      Map<Object, Object> exportParams = new HashMap<Object, Object>();
      exportParams.put("FileIdByProjectIdEntry", projectFileEntry);
      exportParams.put("batchId", batchId);
      exportParams.put("isProduction", 0);

      @SuppressWarnings("unchecked")
      FileDataExportMetadata fileDataExportMetadata = (FileDataExportMetadata) fileManagerDataExporter
          .exportData(dirPath, exportParams);

      return fileDataExportMetadata;

    }

  }

}