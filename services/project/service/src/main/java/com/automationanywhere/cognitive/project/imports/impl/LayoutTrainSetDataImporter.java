package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.LayoutTrainSetDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.LayoutTrainSet;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LayoutTrainSetDataImporter extends AdvanceDataImporter<LayoutTrainSet> {

  private static final String SUBFOLDER = "ClassifierData";
  private static final AALogger LOGGER = AALogger.create(LayoutTrainSetDataImporter.class);
  private final FileHelper fileHelper;
  private final LayoutTrainSetDao layoutTrainSetDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private final Map<String, String> updatedLayouts = new HashMap<String, String>();
  private final List<LayoutTrainSet> dataToImport = new ArrayList<>();
  private final TransitionResult transitionResult;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public LayoutTrainSetDataImporter(FileReader fileReader,
      FileHelper fileHelper,
      LayoutTrainSetDao layoutTrainSetDao,
      ImportFilePatternConstructor importFilePatternConstructor,
      TransitionResult transitionResult) {
    super(fileReader);
    this.fileHelper = fileHelper;
    this.layoutTrainSetDao = layoutTrainSetDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
    this.transitionResult = transitionResult;
  }

  private void mergeLayouts(List<LayoutTrainSet> csvData) {
    dataToImport.clear();

    List<LayoutTrainSet> existingData = layoutTrainSetDao.getAll();
    int nextLayoutId = existingData.size() + 1;

    for (LayoutTrainSet importRow : csvData) {
      Optional<LayoutTrainSet> matchingRow = existingData.stream()
          .filter(p -> p.getHashKey().equals(importRow.getHashKey().trim())).findFirst();
      if (matchingRow.isPresent()) {
        updatedLayouts.put(importRow.getClassName(), matchingRow.get().getClassName());
        LOGGER
            .debug(String
                .format("Layout %s is matching with existing layout %s", importRow.getClassName(),
                    matchingRow.get().getClassName()));
      } else {
        String nextLayoutIdString = String.valueOf(nextLayoutId);
        updatedLayouts.put(importRow.getClassName(), nextLayoutIdString);
        importRow.setClassName(nextLayoutIdString);
        dataToImport.add(importRow);
        nextLayoutId++;
      }
    }
  }

  @Override
  @Transactional(value = DatabaseConstants.CLASSIFIER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //read data from csv
    List<LayoutTrainSet> csvData = getDataFromFiles(unzippedFolderPath);

    //filter data
    mergeLayouts(csvData);

    //commit data
    layoutTrainSetDao.addAll(dataToImport);

    //Setup result
    transitionResult.setModifiedLayouts(updatedLayouts);

    //call other data importer
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<LayoutTrainSet> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), SUBFOLDER);
    String csvPattern = importFilePatternConstructor
        .getRegexPattern(TableConstants.LAYOUTTRAINSET);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, LayoutTrainSet.class);
  }
}
