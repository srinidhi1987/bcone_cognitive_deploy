package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.CustomFieldDetailDao;

import com.automationanywhere.cognitive.project.model.dao.imports.CustomFieldDetail;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class CustomFieldDetailDaoImpl implements
    CustomFieldDetailDao {

  private AALogger log = AALogger.create(CustomFieldDetailDaoImpl.class);
  private static final int BATCH_SIZE = 20;

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  public void delete(String projectId) {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("delete CustomFieldDetail where projectId = :projectId");
    query.setParameter("projectId", projectId);
    query.executeUpdate();
  }

  @Override
  public List<String[]> getCustomFieldDetails(String projectId) {
    log.entry();

    String sqlQuery = "select * from CustomFieldDetail where ProjectId = :projectId";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> customFieldDetails = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return customFieldDetails;
  }

  @Override
  public List<CustomFieldDetail> getAll(String projectId) {
    Query<CustomFieldDetail> query = sessionFactory.getCurrentSession()
        .createQuery("from CustomFieldDetail where projectId = :projectId",CustomFieldDetail.class);
    query.setParameter("projectId", projectId);
    return query.getResultList();
  }

  @Override
  public void addAll(List<CustomFieldDetail> customFieldDetailList)
  {
    Session session = sessionFactory.getCurrentSession();
    int index = -1;
    for (CustomFieldDetail customFieldDetail : customFieldDetailList) {
      session.save(customFieldDetail);
      index++;
      if (index % BATCH_SIZE == 0) {
        session.flush();
        session.clear();
      }
    }
  }
}
