package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.model.dao.Task;

public interface ImportManager {

  public void run(Task task, ImportEvents importEvents) throws Exception;
}
