package com.automationanywhere.cognitive.project.helper;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.FileVersionConstants;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ProductReleaseFileReader {

  private static final AALogger LOGGER = AALogger.create(ProductReleaseFileReader.class);
  private String versionId;

  public ProductReleaseFileReader() {
    readVersionId();
  }

  public String getCurrentProductVersion() {
    return versionId;
  }

  private Document parseProductReleaseFile() {
    Document doc = null;
    try {
      Path path = Paths.get(System.getProperty("user.dir")).getParent();
      File inputFile = new File(
          path.toString() + File.separator
              + FileVersionConstants.PRODUCT_RELEASE_INFO_FILE);
      if (inputFile.exists()) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
      }
    } catch (Exception e) {
      LOGGER
          .error("Error parsing ProductReleaseInfo.xml file",
              e.getMessage());
      System.exit(0);
    }
    return doc;
  }

  private void readVersionId() {
    try {
      Document document = parseProductReleaseFile();
      XPath xPath = XPathFactory.newInstance().newXPath();
      NodeList nodeList = (NodeList) xPath.compile(FileVersionConstants.XPATH_EXPRESSION)
          .evaluate(
              document, XPathConstants.NODESET);
      if (null != nodeList) {
        Node nNode = nodeList.item(0);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          String version = eElement
              .getElementsByTagName(FileVersionConstants.FILE_VERSION_TAG_NAME)
              .item(0)
              .getTextContent();
          if (null != version) {
            String arrayOfVersionParts[] = version
                .split(FileVersionConstants.FILE_VERSION_SPLIT_REGEX);
            if (null != arrayOfVersionParts && arrayOfVersionParts.length >= 2) {
              versionId = String
                  .format("%s.%s.%s", arrayOfVersionParts[0],
                      arrayOfVersionParts[1],
                      arrayOfVersionParts[2]);
            }
          }
          LOGGER.debug("Current Release Version :" + versionId);
        }
      }

      if (versionId == null) {
        throw new NotFoundException("Error reading version details ");
      }
    } catch (Exception e) {
      LOGGER
          .error("VersionId is not available in the ProductReleaseInfo.xml file",
              e.getMessage());
      System.exit(0);
    }
  }
}
