package com.automationanywhere.cognitive.project.health.connections;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Connection;
import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.exception.MQConnectionFailureException;
import com.automationanywhere.cognitive.project.messagequeue.impl.QueuingServiceImpl;
/**
 * @author shweta.thakur
 * This class checks the MQ connection health
 */
public class MQCheckConnection implements HealthCheckConnection{

    private QueuingServiceImpl queueService;
    private AALogger aaLogger = AALogger.create(this.getClass());

    public MQCheckConnection(QueuingServiceImpl queueService) {
        super();
        this.queueService = queueService;
    }

    @Override
    public void checkConnection() {
        aaLogger.entry();
        try {
            queueService.connect();
        } catch (Exception ex) {
            throw new MQConnectionFailureException("Error connecting rabbit mq ", ex);
        } finally {
            queueService.closeConnection();
            aaLogger.exit();
        }
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        return null;
    }

}