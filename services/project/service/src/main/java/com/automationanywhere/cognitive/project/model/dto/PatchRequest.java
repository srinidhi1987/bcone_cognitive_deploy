package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.model.PatchOperation;

/**
 * Created by Jemin.Shah on 24-01-2017.
 */
public class PatchRequest
{
    private PatchOperation op;
    private String path;
    private Object value;
    private long versionId;

    public PatchOperation getOp() {
        return op;
    }

    public void setOp(PatchOperation op) {
        this.op = op;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public long getVersionId() {
      return versionId;
    }

    public void setVersionId(long versionId) {
      this.versionId = versionId;
    }

  @Override
    public String toString() {
        return "PatchRequest{" +
                "op=" + op +
                ", path='" + path + '\'' +
                ", value='" + value + '\'' +
                ", versionId='" + versionId + '\'' +
                '}';
    }
}
