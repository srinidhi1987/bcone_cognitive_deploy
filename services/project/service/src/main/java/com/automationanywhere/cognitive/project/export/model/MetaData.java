package com.automationanywhere.cognitive.project.export.model;

public class MetaData {

  private ProjectDataExportMetadata projectDataExportMetadata;
  private FileDataExportMetadata fileDataExportMetadata;
  private ClassifierDataExportMetadata classifierDataExportMetadata;
  private VisionBotDataExportMetadata visionBotDataExportMetadata;
  private MlDataExportMetadata mlDataExportMetadata;
  private String version;

  public MetaData() {
  }

  public MetaData(
      ProjectDataExportMetadata projectDataExportMetadata,
      FileDataExportMetadata fileDataExportMetadata,
      ClassifierDataExportMetadata classifierDataExportMetadata,
      VisionBotDataExportMetadata visionBotDataExportMetadata,
      MlDataExportMetadata mlDataExportMetadata,
      String version) {
    this.projectDataExportMetadata = projectDataExportMetadata;
    this.fileDataExportMetadata = fileDataExportMetadata;
    this.classifierDataExportMetadata = classifierDataExportMetadata;
    this.visionBotDataExportMetadata = visionBotDataExportMetadata;
    this.mlDataExportMetadata = mlDataExportMetadata;
    this.version = version;
  }

  public ProjectDataExportMetadata getProjectDataExportMetadata() {
    return projectDataExportMetadata;
  }

  public FileDataExportMetadata getFileDataExportMetadata() {
    return fileDataExportMetadata;
  }

  public ClassifierDataExportMetadata getClassifierDataExportMetadata() {
    return classifierDataExportMetadata;
  }

  public VisionBotDataExportMetadata getVisionBotDataExportMetadata() {
    return visionBotDataExportMetadata;
  }

  public MlDataExportMetadata getMlDataExportMetadata() {
    return mlDataExportMetadata;
  }

  public String getVersion(){
    return version;
  }
}
