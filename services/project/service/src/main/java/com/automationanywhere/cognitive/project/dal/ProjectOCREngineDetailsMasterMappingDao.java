package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.ProjectOCREngineDetailsMasterMapping;
import java.util.List;
import org.hibernate.SessionFactory;


public interface ProjectOCREngineDetailsMasterMappingDao {

  public List<String[]> getProjectOCREngineDetailsMasterMappings(String projectId);

  public SessionFactory getSessionFactory();

  public List<ProjectOCREngineDetailsMasterMapping> getProjectOcrMappingByProjectId(
      String projectId);

  public void add(
      ProjectOCREngineDetailsMasterMapping projectOcrEngineDetailsMasterMapping);

  public void delete(String projectId);
}