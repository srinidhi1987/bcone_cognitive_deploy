package com.automationanywhere.cognitive.project.exception;

import com.automationanywhere.cognitive.project.exception.impl.HttpStatusCode;
import org.apache.http.protocol.HTTP;

public class ProjectUpdateException extends RuntimeException {

  private static final long serialVersionUID = -7520481790860444981L;
  private long versionId;
  private HttpStatusCode statusCode;

  public ProjectUpdateException(
      final String message,
      final Exception cause,
      final long versionId
  ) {
    super(message, cause);
    this.versionId = versionId;
  }

  public ProjectUpdateException(
      final String message,
      final Exception cause,
      final long versionId,
      final HttpStatusCode statusCode
  ) {
    super(message, cause);
    this.versionId = versionId;
    this.statusCode = statusCode;
  }

  public long getVersionId() {
    return versionId;
  }

  public HttpStatusCode getStatusCode() {
    return statusCode;
  }
}
