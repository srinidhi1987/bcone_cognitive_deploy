package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.BulkInsertConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.BulkDataImportDao;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class BulkDataImportDaoImpl implements BulkDataImportDao {

  private AALogger log = AALogger.create(BulkDataImportDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory filemanagerSessionFactory;

  @Autowired
  @Qualifier("classifierSessionFactory")
  private SessionFactory classifierSessionFactory;

  @Autowired
  @Qualifier("mlSessionFactory")
  private SessionFactory mlSessionFactory;


  private Map<TableConstants, SessionFactory> sessionFactoryTableMap;

  /**
   * Bulk inserts data from flat files in respective tables.
   *
   * @param dataFilePath path of the flat file.
   * @param tableConstants enum of constants by table name.
   * @return count of rows added to table.
   */
  @Override
  public int bulkInsert(Path dataFilePath, TableConstants tableConstants)
      throws HibernateException {
    log.entry();

    SessionFactory sessionFactory = sessionFactoryTableMap.get(tableConstants);
    String tableName = tableConstants.getName();
    String filePath = dataFilePath.toString();
    int startFromRow = 2;

    String sqlQuery="declare @sqlVersion varchar(50) , @sqlVersionNum Int, @codePage char(50),@sqltxt varchar(max)\n"
        + "set @sqlVersion=cast(serverproperty('productversion') as varchar);"
        + "set @sqlVersionNum= cast(SUBSTRING (@sqlVersion,0,CHARINDEX('.',@sqlVersion)) as int);"
        + "set @codePage = iif(@sqlVersionNum>12,'65001','1252');"
        + "SET @sqltxt = 'BULK INSERT  " + tableName + "  FROM ''" + filePath + "''"
        + "   WITH ("
        + "   codepage= '+@codePage+',"
        + "   FIRSTROW = " + startFromRow + ","
        + "   FIELDTERMINATOR =''" + BulkInsertConstants.FIELDTERMINATOR + "'',"
        + "   ROWTERMINATOR =''" + BulkInsertConstants.ROWTERMINATOR + "'',"
        + "   KEEPNULLS,"
        + "   KEEPIDENTITY"
        + "   )'"
        + " exec(@sqltxt)";

    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);

    int records_updated = query.executeUpdate();
    log.exit();
    return records_updated;
  }

  /**
   * Assigns a map of session factory grouping by respective table constants.
   */
  @PostConstruct
  private void buildMapOfSessionFactory() {

    Map<TableConstants, SessionFactory> sessionFactoryTableMap = new HashMap<>();
    sessionFactoryTableMap.put(TableConstants.CLASSIFICATIONREPORT, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.CONTENTTRAINSET, classifierSessionFactory);
    sessionFactoryTableMap.put(TableConstants.CUSTOMFIELDDETAIL, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.DOCUMENTPROCCESSEDDATAILS, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.FILEBLOBS, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.FILEDETAILS, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.LAYOUTTRAINSET, classifierSessionFactory);
    sessionFactoryTableMap.put(TableConstants.PROJECTDETAIL, filemanagerSessionFactory);
    sessionFactoryTableMap
        .put(TableConstants.PROJECTOCRENGINEDETAILSMASTERMAPPING, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.STANDARDFIELDDETAIL, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.TESTSET, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.TESTSETINFO, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.TRAININGDATA, mlSessionFactory);
    sessionFactoryTableMap.put(TableConstants.VISIONBOT, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.VISIONBOTDETAIL, filemanagerSessionFactory);
    sessionFactoryTableMap.put(TableConstants.VISIONBOTRUNDETAILS, filemanagerSessionFactory);
    this.sessionFactoryTableMap = sessionFactoryTableMap;
  }


}
