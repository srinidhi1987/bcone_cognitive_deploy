package com.automationanywhere.cognitive.project.exception;

public class ResourceUpdationException extends Exception {

    private static final long serialVersionUID = -7168436800392797679L;

    public ResourceUpdationException(String message)
    {
        super(message);
    }
    public ResourceUpdationException(String message, Exception ex)
    {
        super(message, ex);
    }
}
