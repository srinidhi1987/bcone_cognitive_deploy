package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.LayoutTrainSet;
import java.util.List;


public interface LayoutTrainSetDao {

  public List<String[]> getAllLayoutTrainSet();
  public List<LayoutTrainSet> getAll();
  public void addAll(List<LayoutTrainSet> layoutTrainSetList);
  void deleteAll();
  public List<String[]> getLayoutTrainSet(List<String> classnames);
}
