package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.dal.OCREngineDetailsDao;
import com.automationanywhere.cognitive.project.model.dao.OCREngineDetails;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class OCREngineDetailsDaoImpl implements OCREngineDetailsDao {

  private AALogger log = AALogger.create(OCREngineDetailsDaoImpl.class);

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Override
  public List<OCREngineDetails> getOCREngineDetails() {
    log.entry();
    Session session;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (HibernateException e) {
      log.error(e.getMessage());
      session = sessionFactory.openSession();
    }

    String query = "SELECT o FROM OCREngineDetails o";
    Query<OCREngineDetails> sessionQuery = session.createQuery(query,OCREngineDetails.class);
    List<OCREngineDetails> ocrDetails = sessionQuery.list();

    log.exit();
    return ocrDetails;
  }

  @Override
  public OCREngineDetails getOCREngineDetails(String engineType) {
    log.entry();
    Session session;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (HibernateException e) {
      log.error(e.getMessage());
      session = sessionFactory.openSession();
    }

    String query = "SELECT o FROM OCREngineDetails o where engineType = :engineType";
    Query<OCREngineDetails> sessionQuery = session.createQuery(query,OCREngineDetails.class);

    sessionQuery.setParameter("engineType", engineType);
    List<OCREngineDetails> ocrDetails = sessionQuery.list();

    log.exit();
    if (ocrDetails.size() > 0) {
      return ocrDetails.get(0);
    }

    return null;
  }

  @Override
  public void addOCREngineDetails(OCREngineDetails ocrEngineDetails) {

  }
}
