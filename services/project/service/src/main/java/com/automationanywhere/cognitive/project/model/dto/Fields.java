package com.automationanywhere.cognitive.project.model.dto;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Jemin.Shah on 08-12-2016.
 */
public class Fields
{
    private String[] standard;
    private List<CustomField> custom;

    public String[] getStandard() {
        return standard;
    }

    public void setStandard(String[] standard) {
        this.standard = standard;
    }

    public List<CustomField> getCustom() {
        return custom;
    }

    public void setCustom(List<CustomField> custom) {
        this.custom = custom;
    }

    @Override
    public String toString() {
        return "Fields{" +
                "standard=" + Arrays.toString(standard) +
                ", custom=" + custom +
                '}';
    }
}
