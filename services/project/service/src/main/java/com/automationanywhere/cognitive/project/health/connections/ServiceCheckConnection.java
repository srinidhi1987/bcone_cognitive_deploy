package com.automationanywhere.cognitive.project.health.connections;

import java.util.ArrayList;
import java.util.List;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.adapter.FileManagerAdapter;
import com.automationanywhere.cognitive.project.adapter.VisionbotAdapter;

public class ServiceCheckConnection implements HealthCheckConnection{

    FileManagerAdapter fileManagerAdapter;
    VisionbotAdapter visionBotAdapter;
    
    AALogger aaLogger = AALogger.create(this.getClass());
    
    public ServiceCheckConnection(FileManagerAdapter fileManagerAdapter,VisionbotAdapter visionbotAdapter) {
        super();
        this.fileManagerAdapter = fileManagerAdapter;
        this.visionBotAdapter = visionbotAdapter;
    }
    
    @Override
    public void checkConnection() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        aaLogger.entry();
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = null;
        for (DependentServices de : DependentServices.values()) {
            try {
                dependentService = new ServiceConnectivity(de.toString(), HTTPStatusCode.OK);
                switch (de) {
                    case FileManager: fileManagerAdapter.testConnection();
                        break;
                    case VisionBot: visionBotAdapter.testConnection();
                        break;
                }
            } catch (Exception ex) {
                aaLogger.error(" Exception while testing connection to dependent services ", ex);
                prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.INTERNAL_SERVER_ERROR);
            }
            if (!dependentServices.contains(dependentService)) {
                prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.OK);
            }
        }
        aaLogger.exit();
        return dependentServices;
    }
    
    private void prepareDependentServiceList(
            List<ServiceConnectivity> dependentServices,
            ServiceConnectivity dependentService, DependentServices de, HTTPStatusCode mode) {
        dependentService.setHTTPStatus(mode);
        dependentServices.add(dependentService);
    }
}
