package com.automationanywhere.cognitive.project.export.Exporter;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.ContentTrainSetDao;
import com.automationanywhere.cognitive.project.dal.LayoutTrainSetDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.model.ClassifierDataExportMetadata;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class ClassifierDataExporter implements DataExporter<ClassifierDataExportMetadata> {

  @Inject
  private ContentTrainSetDao contentTrainSetDao;

  @Inject
  private LayoutTrainSetDao layoutTrainSetDao;

  @Inject
  private FileWriter csvFileWriter;

  private final String CLASSIFICATIONID_PROPERTY ="classificationids";
  private final String LAYOUTID_PROPERTY ="layoutids";
  private AALogger log = AALogger.create(ClassifierDataExporter.class);

  @Transactional(value = "classifierTransactionManager", propagation = Propagation.REQUIRES_NEW)
  @Override
  @SuppressWarnings("unchecked")
  public ClassifierDataExportMetadata exportData(String path, Map<Object, Object> data)
      throws ProjectExportException {
    log.entry();
    ClassifierDataExportMetadata classifierDataExportMetadata;
    try {
      String projectExportDirPath = prepareProjectDirectory(path);
      List<String> classificationIds = (List<String>) data.get(CLASSIFICATIONID_PROPERTY);
      List<String> layoutIds = (List<String>) data.get(LAYOUTID_PROPERTY);
      long contentTrainSetRowCount = exportContentTrainSetData(projectExportDirPath,
          classificationIds);
      long layoutTrainSetRowCount = exportLayoutTrainSetData(projectExportDirPath, layoutIds);
      classifierDataExportMetadata = new ClassifierDataExportMetadata(contentTrainSetRowCount,
          layoutTrainSetRowCount);
    } catch (Exception e) {
      throw new ProjectExportException("Got error while exporting classifier related data to csv.",
          e);
    }
    log.exit();
    return classifierDataExportMetadata;
  }

  private long exportContentTrainSetData(String projectExportDirPath,List<String> classificationIds)
      throws IOException {
    log.entry();
    long contentTrainSetRowCount = 0l;
    List<String[]> contentTrainSetData = contentTrainSetDao.getContentTrainSets(classificationIds);

    if (contentTrainSetData != null && !contentTrainSetData.isEmpty()) {
      String[] headerData = {"id", "training"};
      contentTrainSetData.add(0, headerData);
      csvFileWriter
          .writeData(Paths.get(projectExportDirPath,TableConstants.CONTENTTRAINSET.getName()+".csv"),
              contentTrainSetData);
      contentTrainSetRowCount = contentTrainSetData.size() - 1;
    } else {
      log.info("ContentTrainSet data not found.");
    }

    log.exit();
    return contentTrainSetRowCount;
  }

  private long exportLayoutTrainSetData(String projectExportDirPath,List<String> layoutIds)
      throws IOException {
    log.entry();
    long layoutTrainSetRowCount = 0l;
    List<String[]> layoutTrainSetData = layoutTrainSetDao.getLayoutTrainSet(layoutIds);

    if (layoutTrainSetData != null && !layoutTrainSetData.isEmpty()) {
      String[] headerData = {"hashkey", "classname", "value"};
      layoutTrainSetData.add(0, headerData);
      csvFileWriter
          .writeData(Paths.get(projectExportDirPath,TableConstants.LAYOUTTRAINSET.getName()+".csv"),
              layoutTrainSetData);
      layoutTrainSetRowCount = layoutTrainSetData.size() - 1;
    } else {
      log.info("LayoutTrainSet data not found.");
    }

    log.exit();
    return layoutTrainSetRowCount;
  }

  public String prepareProjectDirectory(String path) {
    String projectExportDirPath = path + "/" + "ClassifierData";
    File file = new File(projectExportDirPath);
    if (!file.exists()) {
      file.mkdirs();
    }
    return projectExportDirPath;
  }
}
