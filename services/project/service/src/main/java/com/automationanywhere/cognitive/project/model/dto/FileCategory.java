package com.automationanywhere.cognitive.project.model.dto;

import com.automationanywhere.cognitive.project.model.dto.filemanager.EnvironmentWiseFileCount;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jemin.Shah on 29-11-2016.
 */
public class FileCategory
{
    private String id;
    private String name;
    private VisionBot visionBot;
    private long fileCount;
    private List<File> files = new ArrayList<>();
    private EnvironmentWiseFileCount productionFileDetails;
    private EnvironmentWiseFileCount stagingFileDetails;
    @JsonIgnore
    private String projectId;

    public FileCategory()
    {
        productionFileDetails = new EnvironmentWiseFileCount();
        stagingFileDetails = new EnvironmentWiseFileCount();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public VisionBot getVisionBot() {
        return visionBot;
    }

    public void setVisionBot(VisionBot visionBot) {
        this.visionBot = visionBot;
    }

    public long getFileCount() {
        return fileCount;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFileCount(long fileCount) {
        this.fileCount = fileCount;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public EnvironmentWiseFileCount getProductionFileDetails() {
        return productionFileDetails;
    }

    public void setProductionFileDetails(EnvironmentWiseFileCount productionFileDetails) {
        this.productionFileDetails = productionFileDetails;
    }

    public EnvironmentWiseFileCount getStagingFileDetails() {
        return stagingFileDetails;
    }

    public void setStagingFileDetails(EnvironmentWiseFileCount stagingFileDetails) {
        this.stagingFileDetails = stagingFileDetails;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "FileCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", visionBot=" + visionBot +
                ", fileCount=" + fileCount +
                ", files=" + files +
                ", productionFileDetails=" + productionFileDetails +
                ", stagingFileDetails=" + stagingFileDetails +
                ", projectId='" + projectId + '\'' +
                '}';
    }
}
