package com.automationanywhere.cognitive.project.export;

public interface ExportOperator {
    void export(String dirPath, String[] projectIds, DataExportListener dataExportListener);
}
