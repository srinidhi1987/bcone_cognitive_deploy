package com.automationanywhere.cognitive.project.constants;

public class TaskOrderConstant {

    public final static String DESC = "desc";
    public final static String ASC = "asc";

    public final static String SORT = "sort";
    public final static String ORDER = "order";

}
