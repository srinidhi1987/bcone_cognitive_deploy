package com.automationanywhere.cognitive.project.model.dto.filemanager;

/**
 * Created by Jemin.Shah on 4/5/2017.
 */
public class EnvironmentWiseFileCount
{
    private long totalCount;
    private long unprocessedCount;

    public EnvironmentWiseFileCount()
    {
        totalCount = 0;
        unprocessedCount = 0;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getUnprocessedCount() {
        return unprocessedCount;
    }

    public void setUnprocessedCount(long unprocessedCount) {
        this.unprocessedCount = unprocessedCount;
    }
}
