package com.automationanywhere.cognitive.project.exception;

/**
 * Created by Jemin.Shah on 7/11/2017.
 */

public class PreconditionFailedException extends Exception {

    private static final long serialVersionUID = -6457649815677842874L;

    public PreconditionFailedException(String message)
    {
        super(message);
    }
}