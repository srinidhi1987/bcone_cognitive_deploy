package com.automationanywhere.cognitive.project.adapter.Impl;

import com.automationanywhere.cognitive.project.ConfigurationProvider;
import com.automationanywhere.cognitive.project.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.project.exception.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.project.model.dto.StandardResponse;
import com.automationanywhere.cognitive.project.model.dto.VisionbotData;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.automationanywhere.cognitive.project.constants.HeaderConstants.CORRELATION_ID;

/**
 * Created by Jemin.Shah on 3/4/2017.
 */
public class VisionbotAdapterImpl implements VisionbotAdapter
{
    private AALogger log = AALogger.create(getClass());

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RestTemplateWrapper restTemplateWrapper;
    private String HEARTBEAT_URL = "/heartbeat";

    private String rootUrl;
    private final String relativeUrlToFetchAllVisionbot = "/organizations/%s/visionbotdata";
    private final String relativeUrlToFetchVisionbotCategorywise = "/organizations/%s/projects/%s/visionbotdata";
    private final String relativeUrlToFetchVisionbotMetadata = "/organizations/%s/visionbotmetadata";

    @Override
    public List<VisionbotData> getVisionbotData(String organizationId)
    {
        log.entry();

        this.rootUrl = ConfigurationProvider.getVisionbotUrl();
        String url = rootUrl + String.format(relativeUrlToFetchAllVisionbot, organizationId);

        VisionbotData[] visionbotDataList = null;

        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            HttpHeaders headers = new HttpHeaders();
            headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
            visionbotDataList = objectMapper.convertValue(standardResponse.getBody().getData(), VisionbotData[].class);

            if(visionbotDataList==null)
            {
                log.debug("No. of visionbot : " + 0);
                return null;
            }
            log.debug("No. of visionbot : " + visionbotDataList.length);
            return Arrays.asList(visionbotDataList);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            return null;
        }
        finally
        {
            log.exit();
        }
    }

    @Override
    public List<VisionbotData> getVisionbotData(String organizationId, String projectId) {
        log.entry();

        this.rootUrl = ConfigurationProvider.getVisionbotUrl();
        String url = rootUrl + String.format(relativeUrlToFetchVisionbotCategorywise, organizationId, projectId);

        VisionbotData[] visionbotDataList = null;

        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            HttpHeaders headers = new HttpHeaders();
            headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
            //StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class);
            visionbotDataList = objectMapper.convertValue(standardResponse.getBody().getData(), VisionbotData[].class);
            if(visionbotDataList == null)
            {
                log.debug("No. of visionbot : " + 0);
                return null;
            }
            log.debug("No. of visionbot : " + visionbotDataList.length);
            return Arrays.asList(visionbotDataList);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return null;
        }
        finally
        {
            log.exit();
        }
    }

    @Override
    public List<VisionbotData> getVisionbotMetadata(String organizationId, String projectId) {
        log.entry();

        this.rootUrl = ConfigurationProvider.getVisionbotUrl();
        String url = rootUrl + String.format(relativeUrlToFetchVisionbotMetadata, organizationId);

        VisionbotData[] visionbotDataList = null;

        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            HttpHeaders headers = new HttpHeaders();
            headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
            //StandardResponse standardResponse = restTemplate.getForObject(url, StandardResponse.class);
            visionbotDataList = objectMapper.convertValue(standardResponse.getBody().getData(), VisionbotData[].class);
            if(visionbotDataList == null)
            {
                log.debug("No. of visionbot : " + 0);
                return null;
            }
            log.debug("No. of visionbot : " + visionbotDataList.length);
            return Arrays.stream(visionbotDataList).filter(v -> v.getProjectId().equals(projectId)).collect(Collectors.toList());
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return null;
        }
        finally
        {
            log.exit();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        log.entry();
        this.rootUrl = ConfigurationProvider.getVisionbotUrl();
        String heartBeatURL = this.rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse=null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL, String.class);
        if(standardResponse!=null && standardResponse.getStatusCode()!=HttpStatus.OK){
            throw new DependentServiceConnectionFailureException("Error while connecting to VisionBot service");
        }
        log.exit();
    }

}
