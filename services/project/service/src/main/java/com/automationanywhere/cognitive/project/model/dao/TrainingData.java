package com.automationanywhere.cognitive.project.model.dao;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trainingData")
public class TrainingData {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(name = "originalvalue")
  private String originalValue;
  @Column(name = "correctedvalue")
  private String correctedValue;
  @Column(name = "fieldid")
  private String fieldId;
  @Column(name = "correctedon", insertable = false)
  private Date correctedOn;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getOriginalValue() {
    return originalValue;
  }

  public void setOriginalValue(String originalValue) {
    this.originalValue = originalValue;
  }

  public String getCorrectedValue() {
    return correctedValue;
  }

  public void setCorrectedValue(String correctedValue) {
    this.correctedValue = correctedValue;
  }

  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  public Date getCorrectedOn() {
    return correctedOn;
  }

  public void setCorrectedOn(Date correctedOn) {
    this.correctedOn = correctedOn;
  }
}
