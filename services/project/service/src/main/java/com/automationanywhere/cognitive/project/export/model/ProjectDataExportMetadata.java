package com.automationanywhere.cognitive.project.export.model;

public class ProjectDataExportMetadata {
  private long projectDetailsRowCount;
  private long projectOcrEngineDetailsRowCount;
  private long customFieldDetailRowCount;
  private long standardFieldDetailRowCount;

  public ProjectDataExportMetadata(){

  }

  public ProjectDataExportMetadata(long projectDetailsRowCount,
      long projectOcrEngineDetailsRowCount,
      long customFieldDetailRowCount, long standardFieldDetailRowCount) {
    this.projectDetailsRowCount = projectDetailsRowCount;
    this.projectOcrEngineDetailsRowCount = projectOcrEngineDetailsRowCount;
    this.customFieldDetailRowCount = customFieldDetailRowCount;
    this.standardFieldDetailRowCount = standardFieldDetailRowCount;
  }

  public long getProjectDetailsRowCount() {
    return projectDetailsRowCount;
  }

  public long getProjectOcrEngineDetailsRowCount() {
    return projectOcrEngineDetailsRowCount;
  }

  public long getCustomFieldDetailRowCount() {
    return customFieldDetailRowCount;
  }

  public long getStandardFieldDetailRowCount() {
    return standardFieldDetailRowCount;
  }
}
