package com.automationanywhere.cognitive.project;


import static spark.Spark.secure;

import com.automationanywhere.cognitive.common.logger.AALogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.TimeZone;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Jemin.Shah on 25-11-2016.
 */
public class Main {
    private static String filemanagerDBConnectionString;
    private static String classifierDBConnectionString;
    private static String mlDBConnectionString;
    private static String username;
    private static String outputPath;
    private static String JDBC_STRING = "jdbc:sqlserver://";
    private static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final int DATASOURCE_INITIAL_SIZE = 3;
    private static String settingsFilePath;
    private static boolean windowsAuth;
    private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations\\CognitivePlatform.jks";
    private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations\\Cognitive.properties";
    private static final String FILENAME_COGNITIVE_SETTINGS = "configurations\\Settings.txt";
    private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
    private static final String PROPERTIES_OCRENGINEKEY = "OCREngine";
    private static final String PROPERTIES_NOTFOUND = "NotFound";
    private static final String PROPERTIES_SQL_WINAUTH = "SQLWindowsAuthentication";
    private static final String HTTPS_SCHEME = "https";
    private static final String BACKUP_PATH = "BackupData";

    private static AALogger logger = AALogger.create(Main.class);

    public static void main(String args[]) throws IOException {
        logger.entry();
        if (args.length > 9) {
            username = args[3];
            loadSettings();
            filemanagerDBConnectionString = getDBConnectionString(args[0], args[1], args[2]);
            classifierDBConnectionString = getDBConnectionString(args[0], args[1], args[5]);
            mlDBConnectionString = getDBConnectionString(args[0], args[1], args[6]);
            outputPath = args[9];
        } else {
            logger.error("9 arguments necessary to run jar file <ipaddress> <sqlport> <databasename> <username> <projectportnumber> <classifierDBName> <mlDBName> "
            		+ " <fileManagerPortNumber> <visionBotortNumber> <outputPath> ");
            System.out.println("9 arguments necessary to run jar file <ipaddress> <sqlport> <databasename> <username> <projectportnumber> <classifierDBName> <mlDBName> "
            		+ " <fileManagerPort> <visionBotPort> <outputPath> ");
            return;
        }
        String portnumber, fileManagerPort, visionBotPort;
        portnumber = args[4];
        fileManagerPort = args[7];
        visionBotPort = args[8];
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        setServiceSecurity();
        ConfigurationProvider.initialize(portnumber, fileManagerPort, visionBotPort);
        createBackupDirectory();
        new ClassPathXmlApplicationContext("SpringBeans.xml");
        logger.exit();
    }

    private static void createBackupDirectory() {
        File backupDirectory = new File(String.valueOf(Paths.get(outputPath, BACKUP_PATH)));
        if (!backupDirectory.exists()) {
            backupDirectory.mkdirs();
        }
    }

    public static String getFilemanagerDBConnectionString() {
        return filemanagerDBConnectionString;
    }

    public static String getClassifierDBConnectionString() {
        return classifierDBConnectionString;
    }

    public static String getMLDBConnectionString() {
        return mlDBConnectionString;
    }

    public static String getOutputPath() {
        return outputPath;
    }

    public static String GetCurrentOCREngine()throws IOException {
        logger.entry();
        String result;
        Path settingsFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveSettingsFilePath = settingsFilePath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
        if (fileExists(cognitiveSettingsFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitiveSettingsFilePath));
            String ocrEngineType = properties.getProperty(PROPERTIES_OCRENGINEKEY, PROPERTIES_NOTFOUND);
            logger.exit();
            result = ocrEngineType;
        } else {
            logger.error("Cognitive properties file not found.");
            result = PROPERTIES_NOTFOUND;
        }
        return result;
    }

    public static String getSettingsFilePath()
    {
        return settingsFilePath;
    }

    public static String getDatabaseUsername() {
        return username;
    }

    private static void setServiceSecurity() {
        logger.entry();
        if (isSSLCertificateConfigured()) {
            try {
                String certificateKey = loadSSLCertificatePassword();
                if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
                    Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
                    String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
                    secure(cognitiveCertificateFilePath, certificateKey, null, null);
                    ConfigurationProvider.setHttpScheme(HTTPS_SCHEME);
                }

            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        logger.exit();
    }

    private static String loadSSLCertificatePassword() throws IOException {
        String sslCertPwd;
        Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitivePropertiesFilePath = propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
        if (fileExists(cognitivePropertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitivePropertiesFilePath));
            sslCertPwd = properties.getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
        } else {
            logger.info("Cognitive properties file not found");
            sslCertPwd = PROPERTIES_NOTFOUND;
        }
        return sslCertPwd;
    }

    private static boolean isSSLCertificateConfigured() {
    	 logger.entry();
        boolean hasCertificate = false;
        Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
        if (fileExists(cognitiveCertificateFilePath)) {
            hasCertificate = true;
        }
        logger.exit();
        return hasCertificate;
    }

    private static boolean fileExists(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }

    private static String getDBConnectionString(String host, String port, String databaseName)
    {
        StringBuilder connectionStringBuilder = new StringBuilder();
        connectionStringBuilder.append(JDBC_STRING);
        connectionStringBuilder.append(host + ":" + port);
        connectionStringBuilder.append(";databaseName=");
        connectionStringBuilder.append(databaseName);
        if (windowsAuth) {
            connectionStringBuilder.append(";integratedSecurity=true");
        }
        return connectionStringBuilder.toString();
    }

    public static BasicDataSource createDataSource(String password, String jdbcUrl) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(SQL_SERVER_DRIVER);
        dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
        dataSource.setUrl(jdbcUrl);
        if (!windowsAuth) {
            dataSource.setUsername(username);
            dataSource.setPassword(password);
        }
        return dataSource;
    }

    private static void loadSettings() throws IOException {
        Path configurationDirectoryPath  = Paths.get(System.getProperty("user.dir")).getParent();
        settingsFilePath = configurationDirectoryPath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
        Properties properties = loadProperties(settingsFilePath);
        if(properties.containsKey(PROPERTIES_SQL_WINAUTH))
        {
            windowsAuth = Boolean.parseBoolean(properties.getProperty(PROPERTIES_SQL_WINAUTH));
        }
    }

    private static Properties loadProperties(String filePath) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filePath));
        return properties;
    }

}
