package com.automationanywhere.cognitive.project.model.dao.imports;

import com.automationanywhere.cognitive.project.model.FieldType;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "CustomFieldDetail")
public class CustomFieldDetail {

  @Id
  @Column(name = "Id", length = 50)
  private String id;

  @Column(name = "Name", length = 50)
  @Pattern(regexp = "^[a-zA-Z]*[ a-zA-Z0-9]*[^ ]$")
  private String name;

  @Column(name = "FieldType")
  @Enumerated(EnumType.STRING)
  private FieldType type;

  @Column(name = "ProjectId")
  private String projectId;

  @Column(name = "OrderNumber")
  private int order;

  @Column(name="IsAddedLater")
  private boolean isAddedLater;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public FieldType getType() {
    return type;
  }

  public void setType(FieldType type) {
    this.type = type;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public boolean isAddedLater() {
    return isAddedLater;
  }

  public void setAddedLater(boolean addedLater) {
    isAddedLater = addedLater;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CustomFieldDetail that = (CustomFieldDetail) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
