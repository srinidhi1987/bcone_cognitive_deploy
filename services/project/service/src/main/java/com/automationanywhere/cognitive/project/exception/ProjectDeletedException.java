package com.automationanywhere.cognitive.project.exception;

/**
 * Created by Jemin.Shah on 6/27/2017.
 */

public class ProjectDeletedException extends Exception {

    private static final long serialVersionUID = 6682324311215348227L;
	
    public ProjectDeletedException(String message)
    {
        super(message);
    }
}