package com.automationanywhere.cognitive.project.helper;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.ProjectService;
import com.automationanywhere.cognitive.project.constants.FileVersionConstants;
import com.automationanywhere.cognitive.project.export.model.MetaData;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileHelper {
  private AALogger logger = AALogger.create(FileHelper.class);

  public boolean isFileExists(String filepath) {
    File file = new File(filepath);
    return file.exists();
  }

  public List<String> getAllFilesInFolder(String filepath) {
    List<String> fileNames = new ArrayList<>();
    File file = new File(filepath);
    if (!file.exists() || !file.isDirectory()) {
      return fileNames;
    }

    File[] files = file.listFiles();
    for (File fileInFolder : files) {
      if (fileInFolder.isFile()) {
        fileNames.add(fileInFolder.getName());
      }
    }
    return fileNames;
  }

  public List<Path> getAllFilesPaths(Path directoryPath) throws IOException {
    return Files.walk(directoryPath).collect(Collectors.toList());
  }

  public void deletFile(Path path) throws IOException {
    Files.delete(path);
  }

  public boolean removeDirectory(File dirTobeDeleted) {
    File[] files = dirTobeDeleted.listFiles();
    if (files != null) {
      for (File file : files) {
        removeDirectory(file);
      }
    }
    return dirTobeDeleted.delete();
  }

  public List<Path> getAllFilesPaths(Path path, String fileNamePattern)
      throws IOException {
    PatternBasedFileVisitor fileVisitor = new PatternBasedFileVisitor(fileNamePattern);

    Files.walkFileTree(path, fileVisitor);

    Collection<Path> matchedFiles = fileVisitor.getMatchedPaths();
    return (List<Path>) matchedFiles;
  }

  public String readVersionFromIqbaFile(String filepath) {
    String valueFromAttributes = null;
    try {
      ZipFile zipFile = new ZipFile(filepath);
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        String name = entry.getName();
        if (name.contains(FileVersionConstants.META_DATA_FILE_NAME)) {
          StringBuilder out = readJsonStringFromFile(zipFile.getInputStream(entry));
          MetaData metadata = jsonStringToObject(out.toString(), MetaData.class);
          valueFromAttributes = metadata.getVersion();
          break;
        }
      }
      zipFile.close();
    } catch (IOException e) {
      logger.error(e);
      valueFromAttributes = null;
    }
    return valueFromAttributes;
  }

  public StringBuilder readJsonStringFromFile(InputStream in) throws IOException {
    StringBuilder out = new StringBuilder();
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    String line;
    while ((line = reader.readLine()) != null) {
      out.append(line);
    }
    return out;
  }

  public <T> T jsonStringToObject(String jsonString, Class<T> clazz) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.readValue(jsonString, clazz);
  }
}
