package com.automationanywhere.cognitive.project.constants;

public class CsvFileIOConstants {

  public static final char ESCAPE_CHARACTER = '"';
  public static final char SEPARATOR = '\t';
  public static final char QUOTE_CHARACTER = '"';
  public static final char NO_QUOTE_CHARACTER = '\u0000';
  public static final char NO_ESCAPE_CHARACTER = '\u0000';
  public static final String LINE_END = "\n";
  public static final String RFC4180_LINE_END = "\r\n";
  public static final char NULL_CHARACTER = '\0';

}
