package com.automationanywhere.cognitive.project.exception;

public class SystemBusyException extends Exception {

    private static final long serialVersionUID = -2725129783756741062L;

    public SystemBusyException(String message)
    {
        super(message);
    }
    public SystemBusyException(String message, Exception ex)
    {
        super(message, ex);
    }
}
