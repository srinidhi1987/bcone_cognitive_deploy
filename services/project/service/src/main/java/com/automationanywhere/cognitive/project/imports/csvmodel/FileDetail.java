package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class FileDetail {
  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "fileid")
  private String fileId;
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column =  "projectid")
  private String projectId;
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column =  "filename")
  private String fileName;
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column =  "filelocation")
  private String fileLocation;
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column =  "filesize")
  private long fileSize;
  @CsvBindByPosition(position = 5)
  @CsvBindByName(column =  "fileheight")
  private int fileHeight;
  @CsvBindByPosition(position = 6)
  @CsvBindByName(column =  "filewidth")
  private int fileWidth;
  @CsvBindByPosition(position = 7)
  @CsvBindByName(column =  "format")
  private String format;
  @CsvBindByPosition(position = 8)
  @CsvBindByName(column =  "processed")
  private boolean processed;
  @CsvBindByPosition(position = 9)
  @CsvBindByName(column =  "classificationid")
  private String classificationId;
  @CsvBindByPosition(position = 10)
  @CsvBindByName(column =  "uploadrequestid")
  private String uploadrequestId;
  @CsvBindByPosition(position = 11)
  @CsvBindByName(column =  "layoutid")
  private String layoutId;
  @CsvBindByPosition(position = 12)
  @CsvBindByName(column = "isproduction")
  private boolean isProduction;

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFileLocation() {
    return fileLocation;
  }

  public void setFileLocation(String fileLocation) {
    this.fileLocation = fileLocation;
  }

  public long getFileSize() {
    return fileSize;
  }

  public void setFileSize(long fileSize) {
    this.fileSize = fileSize;
  }

  public int getFileHeight() {
    return fileHeight;
  }

  public void setFileHeight(int fileHeight) {
    this.fileHeight = fileHeight;
  }

  public int getFileWidth() {
    return fileWidth;
  }

  public void setFileWidth(int fileWidth) {
    this.fileWidth = fileWidth;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public boolean isProcessed() {
    return processed;
  }

  public void setProcessed(boolean processed) {
    this.processed = processed;
  }

  public String getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  public String getUploadrequestId() {
    return uploadrequestId;
  }

  public void setUploadrequestId(String uploadrequestId) {
    this.uploadrequestId = uploadrequestId;
  }

  public String getLayoutId() {
    return layoutId;
  }

  public void setLayoutId(String layoutId) {
    this.layoutId = layoutId;
  }

  public boolean isProduction() {
    return isProduction;
  }

  public void setProduction(boolean production) {
    isProduction = production;
  }
}
