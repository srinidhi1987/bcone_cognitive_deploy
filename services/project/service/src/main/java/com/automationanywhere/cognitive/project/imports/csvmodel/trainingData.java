/**
 *
 */
package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class trainingData {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "originalvalue")
  private String originalvalue;
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "correctedvalue")
  private String correctedvalue;
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "fieldid")
  private String fieldid;
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "correctedon")
  private String correctedon;
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "id")
  private long id;

  public trainingData() {

  }


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getOriginalvalue() {
    return originalvalue;
  }

  public void setOriginalvalue(String originalvalue) {
    this.originalvalue = originalvalue;
  }

  public String getCorrectedvalue() {
    return correctedvalue;
  }

  public void setCorrectedvalue(String correctedvalue) {
    this.correctedvalue = correctedvalue;
  }

  public String getFieldid() {
    return fieldid;
  }

  public void setFieldid(String fieldid) {
    this.fieldid = fieldid;
  }

  public String getCorrectedon() {
    return correctedon;
  }

  public void setCorrectedon(String correctedon) {
    this.correctedon = correctedon;
  }

}
