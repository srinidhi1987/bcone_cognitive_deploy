package com.automationanywhere.cognitive.project.constants;

public class ProjectUpdationConstants {
  public static final String PROJECT_UPDATION_EXCEPTION_MESSAGE = "This Learning Instance was recently updated and is no longer latest. Please reinitiate the current operation.";
}
