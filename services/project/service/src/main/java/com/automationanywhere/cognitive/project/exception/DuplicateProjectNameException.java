package com.automationanywhere.cognitive.project.exception;

/**
 * Created by Jemin.Shah on 6/28/2017.
 */

public class DuplicateProjectNameException extends Exception {

    private static final long serialVersionUID = 4996035227344058600L;

    public DuplicateProjectNameException(String message)
    {
        super(message);
    }
}