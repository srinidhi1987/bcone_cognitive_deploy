package com.automationanywhere.cognitive.project.dal;

public interface DataCleaningDao {
    void cleanProjectDetails();
    void cleanClassifierDetails();
}
