package com.automationanywhere.cognitive.project.constants;

/**
 * This class hold all types of the error messages
 */
public class ErrorMessageConstants {

  public final static String CLASSIFICATION_INPROGRESS = "Classification in progress for one or more Learning Instances. Retry this action after some time.";
  public final static String DATAEXTRACTION_INPROGRESS = "Data extraction in progress for one or more Learning Instances. Retry this action after some time.";
  public final static String DATAEXTRACTION_INPROGRESS_FOR_PRODUCTION_DOC = "Data extraction in progress for one or more Production Learning Instances. Retry this action after some time.";
  public final static String INCOMPATIBLE_FILE_VERSION = "Import aborted. Incompatible IQ Bot %s archive detected.";
  public final static String INVALID_PARAMETER = "Invalid parameter";
  public final static String NO_FILES_FOR_IMPORT = "No file selected for import.";
  public final static String INVALID_IMPORT_OPTION = "Invalid import option.";
  public final static String IMPORT_FILE_UNAVAILABLE = "Import file is unavailable.";
  public final static String IMPORT_FILE_EXTENTION_INVALID = "Invalid import file extension.";
  public final static String LEARNING_INSTANCE_NOT_FOUND = "Learning Instance not found.";
  public final static String LEARNING_INSTANCE_NOT_STAGING_MODE = "Learning Instance is not in staging mode.";
  public final static String UNABLE_FIND_OCR_ENGINE = "Unable to find associated OCR Engine.";
  public final static String DUPLICATE_LEARNING_INSTANCE = "Duplicate Learning Instance name.";
  public final static String PROJECT_PROCESSING_ERROR = "Learning Instance processing error.";
  public final static String COLUMN_NOT_MATCHED = "Column mismatch error.";
  public final static String INTERNAL_SERVER_ERROR = "Internal server error.";
  public final static String INVALID_ORGANISATION = "Invalid organization.";
  public final static String UPDATION_FAILED = "Fail to update Learning Instance.";
  public static final String VISION_BOT_UNDER_TRAINING = "This Learning Instance is currently under use due to ongoing Bot Training. Please close training for all bots and try again.";
}
