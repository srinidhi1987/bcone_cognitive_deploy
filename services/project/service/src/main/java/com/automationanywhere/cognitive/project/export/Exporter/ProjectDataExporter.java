package com.automationanywhere.cognitive.project.export.Exporter;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.CustomFieldDetailDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.dal.ProjectOCREngineDetailsMasterMappingDao;
import com.automationanywhere.cognitive.project.dal.StandardFieldDetailDao;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.model.ProjectDataExportMetadata;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class ProjectDataExporter implements DataExporter<ProjectDataExportMetadata> {

  @Inject
  private ProjectDetailDao projectDetailDao;
  @Inject
  private ProjectOCREngineDetailsMasterMappingDao projOcrDetailMapDao;
  @Inject
  private CustomFieldDetailDao customFieldDetailDao;
  @Inject
  private StandardFieldDetailDao standardFieldDetailDao;

  @Inject
  private FileWriter csvFileWriter;

  private AALogger log = AALogger.create(ProjectDataExporter.class);

  @Transactional(value = "filemanagerTransactionManager", propagation = Propagation.REQUIRES_NEW)
  @Override
  public ProjectDataExportMetadata exportData(String path, Map<Object, Object> data)
      throws ProjectExportException {
    log.entry();
    ProjectDataExportMetadata projectDataExportMetadata = new ProjectDataExportMetadata();
    try {
      String projectId = data.get("projectId").toString();
      String projectExportDirPath = prepareProjectDirectory(path, projectId);

      long projectDetailsRowCount = exportProjectDetailsData(projectExportDirPath, projectId);
      long projectOcrEngineDetailsRowCount = exportProjectOCREngineDetailsMasterMappingData(
          projectExportDirPath, projectId);
      long customFieldDetailRowCount = exportCustomFieldDetailData(projectExportDirPath,
          projectId);
      long standardFieldDetailRowCount = exportStandardFieldDetailData(projectExportDirPath,
          projectId);
      projectDataExportMetadata = new ProjectDataExportMetadata(projectDetailsRowCount,
          projectOcrEngineDetailsRowCount, customFieldDetailRowCount,
          standardFieldDetailRowCount);


    } catch (Exception e) {
      throw new ProjectExportException("Got error while exporting project related data to csv.", e);
    }
    log.exit();
    return projectDataExportMetadata;


  }

  private long exportProjectDetailsData(String projectExportDirPath, String projectId)
      throws IOException {
    log.entry();
    long projectDetailsRowCount = 0l;
    String[] projectDetail = projectDetailDao.getProjectDetail(projectId);

    if (projectDetail != null && projectDetail.length != 0) {

      String[] headerData = {"Id", "Name", "Description", "OrganizationId", "Type", "Typeid",
          "PrimaryLanguage", "State", "Environment", "FileUploadToken", "FileUploadTokenAlive",
          "CreatedBy", "LastUpdatedBy", "CreatedAt", "UpdatedAt","VersionId"};

      List<String[]> projectDetails = new ArrayList<String[]>();
      projectDetails.add(projectDetail);
      projectDetails.add(0, headerData);
      csvFileWriter
          .writeData(Paths.get(projectExportDirPath, TableConstants.PROJECTDETAIL.getName()+".csv"),
              projectDetails);
      projectDetailsRowCount = projectDetails.size() - 1;
    } else {
      log.info("ProjectDetails not found for project id : " + projectId);
    }

    log.exit();
    return projectDetailsRowCount;
  }

  private long exportProjectOCREngineDetailsMasterMappingData(String projectExportDirPath,
      String projectId) throws IOException {
    log.entry();
    long projectOCREngineDetailsRowCount = 0;

    List<String[]> projOcrDetailMapData = projOcrDetailMapDao
        .getProjectOCREngineDetailsMasterMappings(projectId);

    if (projOcrDetailMapData != null && !projOcrDetailMapData.isEmpty()) {

      String[] headerData = {"Id", "ProjectId", "OCREngineDetailsId"};
      projOcrDetailMapData.add(0, headerData);
      csvFileWriter
          .writeData(Paths.get(projectExportDirPath,TableConstants.PROJECTOCRENGINEDETAILSMASTERMAPPING.getName()+".csv"),
              projOcrDetailMapData);
      projectOCREngineDetailsRowCount = projOcrDetailMapData.size() - 1;

    } else {
      log.info("ProjectOCREngineDetailsMasterMapping data not found for project id : " + projectId);
    }
    log.exit();
    return projectOCREngineDetailsRowCount;
  }

  private long exportCustomFieldDetailData(String projectExportDirPath,
      String projectId) throws IOException {
    log.entry();
    long customFieldDetailRowCount = 0l;
    List<String[]> customFieldDetailData = customFieldDetailDao.getCustomFieldDetails(projectId);

    if (customFieldDetailData != null && !customFieldDetailData.isEmpty()) {

      String[] headerData = {"id", "name", "fieldType", "projectId", "orderNumber","IsAddedLater"};
      customFieldDetailData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.CUSTOMFIELDDETAIL.getName()+".csv"),
          customFieldDetailData);
      customFieldDetailRowCount = customFieldDetailData.size() - 1;
    } else {
      log.info("CustomFieldDetails data not found for project id : " + projectId);
    }
    log.exit();
    return customFieldDetailRowCount;
  }

  private long exportStandardFieldDetailData(String projectExportDirPath,
      String projectId) throws IOException {
    log.entry();
    long standardFieldDetailRowCount = 0l;

    List<String[]> standardFieldDetailData = standardFieldDetailDao
        .getStandardFieldDetails(projectId);

    if (standardFieldDetailData != null && !standardFieldDetailData.isEmpty()) {

      String[] headerData = {"id", "projectid", "OrderNumber","IsAddedLater"};
      standardFieldDetailData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.STANDARDFIELDDETAIL.getName()+".csv"),
          standardFieldDetailData);
      standardFieldDetailRowCount = standardFieldDetailData.size() - 1;
    } else {
      log.info("StandardFieldDetails data not found for project id : " + projectId);
    }
    log.exit();
    return standardFieldDetailRowCount;
  }

  private boolean isProduction(String projectId) {
    log.entry();
    boolean isProdcution = false;
    try {
      isProdcution = projectDetailDao.isProduction(projectId);
    } catch (NotFoundException ex) {
      log.info(ex.getMessage());
    }
    log.exit();
    return isProdcution;
  }

  public String prepareProjectDirectory(String path, String projectId) {
    String projectExportDirPath = path + "/" + projectId;
    File file = new File(projectExportDirPath);
    if (!file.exists()) {
      file.mkdirs();
    }
    return projectExportDirPath;
  }
}
