package com.automationanywhere.cognitive.project.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Jemin.Shah on 29-11-2016.
 */
public class File
{
    private String id;
    private String name;
    private String location;
    private String format;
    private boolean processed;

    @JsonIgnore
    private boolean isProduction;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public boolean isProcessed() {
        return processed;
    }

    public String getFormat() {
        return format;
    }

    public String getLocation() {
        return location;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public boolean isProduction() {
        return isProduction;
    }

    public void setProduction(boolean production) {
        isProduction = production;
    }

    @Override
    public String toString() {
        return "File{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", format='" + format + '\'' +
                ", processed=" + processed +
                ", isProduction=" + isProduction +
                '}';
    }
}
