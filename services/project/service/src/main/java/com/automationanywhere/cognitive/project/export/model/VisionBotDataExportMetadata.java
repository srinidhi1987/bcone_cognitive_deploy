package com.automationanywhere.cognitive.project.export.model;

public class VisionBotDataExportMetadata {

  private long visionbotDetailRowCount;
  private long visionbotRowCount;
  private long visionbotRunDetailsRowCount;
  private long testSetInfoRowCount;
  private long documentProcessedDetailRowCount;
  private long testSetRowCount;

  public VisionBotDataExportMetadata() {
  }

  public VisionBotDataExportMetadata(long visionbotDetailRowCount, long visionbotRowCount,
      long visionbotRunDetailsRowCount, long testSetInfoRowCount,
      long documentProcessedDetailRowCount, long testSetRowCount) {
    this.visionbotDetailRowCount = visionbotDetailRowCount;
    this.visionbotRowCount = visionbotRowCount;
    this.visionbotRunDetailsRowCount = visionbotRunDetailsRowCount;
    this.testSetInfoRowCount = testSetInfoRowCount;
    this.documentProcessedDetailRowCount = documentProcessedDetailRowCount;
    this.testSetRowCount = testSetRowCount;
  }

  public long getVisionbotDetailRowCount() {
    return visionbotDetailRowCount;
  }

  public long getVisionbotRowCount() {
    return visionbotRowCount;
  }

  public long getVisionbotRunDetailsRowCount() {
    return visionbotRunDetailsRowCount;
  }

  public long getTestSetInfoRowCount() {
    return testSetInfoRowCount;
  }

  public long getDocumentProcessedDetailRowCount() {
    return documentProcessedDetailRowCount;
  }

  public long getTestSetRowCount() {
    return testSetRowCount;
  }
}
