package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.TestSetInfo;
import java.util.List;


public interface TestSetInfoDao {

  public List<String> getTestSetInfoIds(List<String> visionBotDetailIds);

  public List<String[]> getTestSetInfos(List<String> visionBotDetailIds);

  public void batchInsertTestSetInfo(List<TestSetInfo> testSetInfoList);

}
