package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AdvanceDataImporter<T> {

  private final FileReader csvFileReader;

  @Autowired
  protected AdvanceDataImporter(
      FileReader csvFileReader) {
    this.csvFileReader = csvFileReader;
  }

  public abstract void importData(Path unzippedFolderPath, TaskOptions taskOptions)throws IOException, ProjectImportException,DuplicateProjectNameException,ParseException;
  public abstract void setNext(AdvanceDataImporter dataImporter);

  public List<T> getDataFromCsv(List<Path> fileList, Class<T> theClass)
      throws ProjectImportException {
    List<T> data = new ArrayList<>();
    try {
      for (Path filePath : fileList) {
        data.addAll(csvFileReader.readAllData(filePath, theClass, 1));
      }
    } catch (IOException e) {
      throw new ProjectImportException("Unable to read csv.", e);
    }

    return data;
  }
}
