package com.automationanywhere.cognitive.project.exception;

/**
 * Created by Jemin.Shah on 09-01-2017.
 */
public class NotFoundException extends Exception {

    private static final long serialVersionUID = 882033610439818047L;
	
    public NotFoundException(String message)
    {
        super(message);
    }
    public NotFoundException(String message, Exception ex)
    {
        super(message, ex);
    }
}