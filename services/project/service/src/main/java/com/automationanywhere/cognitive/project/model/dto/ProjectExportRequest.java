package com.automationanywhere.cognitive.project.model.dto;

public class ProjectExportRequest {

    private String archiveName;

    private String[] projectExportDetails;

    private String userId;

    public String[] getProjectExportDetails() {
        return projectExportDetails;
    }

    public void setProjectExportDetails(String[] projectExportDetails) {
        this.projectExportDetails = projectExportDetails;
    }

    public String getArchiveName() {
        return archiveName;
    }

    public void setArchiveName(String archiveName) {
        this.archiveName = archiveName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
