package com.automationanywhere.cognitive.project.exception;

public class TaskException extends Exception {

    private static final long serialVersionUID = 5508463315123250958L;

    public TaskException(String message) {
        super(message);
    }

    public TaskException(String message, Throwable cause) {
        super(message, cause);
    }
}
