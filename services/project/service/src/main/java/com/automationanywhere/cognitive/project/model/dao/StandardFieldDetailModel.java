package com.automationanywhere.cognitive.project.model.dao;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "StandardFieldDetail")
public class StandardFieldDetailModel {

  @Embeddable
  public static class PrimaryKey implements Serializable {

    private static final long serialVersionUID = 2934701411373177687L;

    @Column(name = "Id", nullable = false)
    private String id;

    @Column(name = "ProjectId")
    private String projectId;

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getProjectId() {
      return projectId;
    }

    public void setProjectId(String projectId) {
      this.projectId = projectId;
    }
  }

  @EmbeddedId
  private PrimaryKey primaryKey = new PrimaryKey();

  @Column(name = "OrderNumber")
  private int order;

  public PrimaryKey getPrimaryKey() {
    return primaryKey;
  }

  public void setPrimaryKey(PrimaryKey primaryKey) {
    this.primaryKey = primaryKey;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public static final Comparator<StandardFieldDetailModel> COMPARE_BY_ORDER_NUMBER = Comparator.comparingInt(base -> base.order);

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    StandardFieldDetailModel that = (StandardFieldDetailModel) o;

    return primaryKey.equals(that.primaryKey);
  }

  @Override
  public int hashCode() {
    return primaryKey.hashCode();
  }
}