package com.automationanywhere.cognitive.project.model.dao;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Jemin.Shah on 6/27/2017.
 */

@Entity
@Table(name = "ProjectActivityDetail")
public class ProjectActivityDetail
{
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name = "ActivityId")
    private int activityId;

    @Column(name = "ProjectId")
    private String projectId;

    @Column(name = "UpdatedBy")
    private String updatedBy;

    @Column(name = "ActivityAt")
    private Date activityAt;

    @Column(name = "ProjectName")
    private String projectName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getActivityAt() {
        return activityAt;
    }

    public void setActivityAt(Date activityAt) {
        this.activityAt = activityAt;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
