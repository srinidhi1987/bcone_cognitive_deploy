package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.DocumentProcessedDetailDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.DocumentProcessedDetails;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DocumentProcessedDetailsDataImporter extends
    AdvanceDataImporter<DocumentProcessedDetails> {

  private static final AALogger LOGGER = AALogger
      .create(DocumentProcessedDetailsDataImporter.class);
  private final FileHelper fileHelper;
  private final DocumentProcessedDetailDao documentProcessedDetailDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private final List<DocumentProcessedDetails> dataToImport = new ArrayList<>();
  private final TransitionResult transitionResult;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public DocumentProcessedDetailsDataImporter(FileReader fileReader,
      FileHelper fileHelper,
      DocumentProcessedDetailDao documentProcessedDetailDao,
      ImportFilePatternConstructor importFilePatternConstructor,
      TransitionResult transitionResult) {
    super(fileReader);
    this.fileHelper = fileHelper;
    this.documentProcessedDetailDao = documentProcessedDetailDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
    this.transitionResult = transitionResult;
  }

  private void insertDocumentProcessedDetails(List<DocumentProcessedDetails> csvData,
      TaskOptions taskOptions) {
    dataToImport.clear();
    List<DocumentProcessedDetails> existingDocumentProcessedDetails = new ArrayList<>();

    List<String> visionBotRunDetailIds = transitionResult.getImportedVisionBotRunDetailIdList();

    switch (taskOptions) {
      case CLEAR:
        dataToImport.addAll(csvData);
        break;
      case APPEND:
        if (visionBotRunDetailIds.size() > 0) {
          existingDocumentProcessedDetails = documentProcessedDetailDao
              .get(visionBotRunDetailIds);
        }
        dataToImport.addAll(filterCsvDataWithExistingData(csvData, existingDocumentProcessedDetails));
        break;
      case MERGE:
        dataToImport.addAll(
            filterMatchingCsvDataByVisionbotRunDetailsId(csvData, visionBotRunDetailIds));
        break;
      case OVERWRITE:
        dataToImport.addAll(
            filterMatchingCsvDataByVisionbotRunDetailsId(csvData, visionBotRunDetailIds));
        break;
    }
    if (dataToImport.size() > 0) {
      documentProcessedDetailDao.addAll(dataToImport);
    }
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    //read data from csv
    List<DocumentProcessedDetails> csvData = getDataFromFiles(unzippedFolderPath);

    //import data
    if (csvData.size() > 0) {
      insertDocumentProcessedDetails(csvData, taskOptions);
    }

    //call other data importer
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<DocumentProcessedDetails> filterCsvDataWithExistingData(List<DocumentProcessedDetails> csvData,
      List<DocumentProcessedDetails> existingDocumentProcessedDetails) {
    List<DocumentProcessedDetails> finalDataToImport = new ArrayList<>();
    for (DocumentProcessedDetails csvDocProcessedDetails : csvData) {
      Optional<DocumentProcessedDetails> matchingRow = existingDocumentProcessedDetails.stream()
          .filter(existingDocProcDetail -> existingDocProcDetail.getId()
              .equals(csvDocProcessedDetails.getId())).findFirst();
      if (!matchingRow.isPresent()) {
        finalDataToImport.add(csvDocProcessedDetails);
        LOGGER.debug(String.format("Adding DocumentProcessedDetails having id %s",
            csvDocProcessedDetails.getId()));
      } else {
        LOGGER.debug(String.format("Skipping DocumentProcessedDetails having id %s",
            csvDocProcessedDetails.getId()));
      }
    }
    return finalDataToImport;
  }

  private List<DocumentProcessedDetails> filterMatchingCsvDataByVisionbotRunDetailsId(List<DocumentProcessedDetails> csvData,
      List<String> visionbotRunDetailsId) {
    return csvData.stream().filter(documentProcessedDetails -> visionbotRunDetailsId
        .contains(documentProcessedDetails.getVisionBotRunDetailsId())).collect(Collectors
        .toList());
  }

  private List<DocumentProcessedDetails> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.DOCUMENTPROCCESSEDDATAILS);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, DocumentProcessedDetails.class);
  }
}
