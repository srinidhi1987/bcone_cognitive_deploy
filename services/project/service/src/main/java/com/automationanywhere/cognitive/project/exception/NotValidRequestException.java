package com.automationanywhere.cognitive.project.exception;

public class NotValidRequestException extends Exception {

  private static final long serialVersionUID = 4417178573381193702L;

  public NotValidRequestException(String message)
  {
    super(message);
  }
  public NotValidRequestException(String message, Exception ex)
  {
    super(message, ex);
  }
}
