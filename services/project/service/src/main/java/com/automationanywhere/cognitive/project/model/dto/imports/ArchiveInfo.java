package com.automationanywhere.cognitive.project.model.dto.imports;

public class ArchiveInfo {
    String archiveName;

    public String getArchiveName() {
        return archiveName;
    }

    public void setArchiveName(String archiveName) {
        this.archiveName = archiveName;
    }
}
