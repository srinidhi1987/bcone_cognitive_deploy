package com.automationanywhere.cognitive.project.Util;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by msundell on 5/12/17
 */
public class FieldUtil {

  /**
   * Extract a set containing a string value for each field in a comma separated input string. To
   * use the set you can use set.contains("<field>") which will evaluate to true or false.
   *
   * @param fields fields <code>String</code>
   * @return <code>Set</code> with fields. If no fields could be extracted an empty array is
   * returned
   */
  public static Set<String> extractFields(String fields) {
    Set<String> fieldSet = new HashSet<>();
    if (fields == null || fields.trim().length() == 0) {
      return fieldSet;
    }
    String arr[] = fields.split(",");
    for (String field : arr) {
      fieldSet.add(field.trim());
    }
    return fieldSet;
  }
}
