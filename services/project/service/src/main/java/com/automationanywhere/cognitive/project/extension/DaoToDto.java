package com.automationanywhere.cognitive.project.extension;

import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetailModel;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailModel;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetailModel;
import com.automationanywhere.cognitive.project.model.dto.CustomField;
import com.automationanywhere.cognitive.project.model.dto.CustomFieldV2;
import com.automationanywhere.cognitive.project.model.dto.Fields;
import com.automationanywhere.cognitive.project.model.dto.FieldsV2;
import com.automationanywhere.cognitive.project.model.dto.File;
import com.automationanywhere.cognitive.project.model.dto.FileDetail;
import com.automationanywhere.cognitive.project.model.dto.ProjectGetResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectRequest;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponse;
import com.automationanywhere.cognitive.project.model.dto.ProjectResponseV2;
import com.automationanywhere.cognitive.project.model.dto.StandardFieldV2;
import com.automationanywhere.cognitive.project.model.dto.VisionBot;
import com.automationanywhere.cognitive.project.model.dto.VisionbotData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Jemin.Shah on 05-12-2016.
 */
public class DaoToDto
{
    public static ProjectResponse toProjectResponse(ProjectDetail projectDetail)
    {
        ProjectResponse projectResponse = new ProjectResponse();
        projectResponse.setId(projectDetail.getId());
        projectResponse.setName(projectDetail.getName());
        projectResponse.setDescription(projectDetail.getDescription());
        projectResponse.setOrganizationId(projectDetail.getOrganizationId());
        projectResponse.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
        projectResponse.setProjectType(projectDetail.getProjectType());
        projectResponse.setProjectTypeId(projectDetail.getProjectTypeId());
        projectResponse.setEnvironment(projectDetail.getEnvironment());
        projectResponse.setProjectState(projectDetail.getState());
        projectResponse.setFields(toFields(projectDetail));
        projectResponse.setOcrEngineDetails(projectDetail.getOcrEngineDetails());
        projectResponse.setVersionId(projectDetail.getVersionId());
        return projectResponse;
    }

    public static ProjectResponseV2 toProjectResponseV2(ProjectDetail projectDetail)
    {
        ProjectResponseV2 projectResponse = new ProjectResponseV2();
        projectResponse.setId(projectDetail.getId());
        projectResponse.setName(projectDetail.getName());
        projectResponse.setDescription(projectDetail.getDescription());
        projectResponse.setOrganizationId(projectDetail.getOrganizationId());
        projectResponse.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
        projectResponse.setProjectType(projectDetail.getProjectType());
        projectResponse.setProjectTypeId(projectDetail.getProjectTypeId());
        projectResponse.setEnvironment(projectDetail.getEnvironment());
        projectResponse.setProjectState(projectDetail.getState());
        projectResponse.setFields(toFieldsV2(projectDetail));
        projectResponse.setOcrEngineDetails(projectDetail.getOcrEngineDetails());
        projectResponse.setVersionId(projectDetail.getVersionId());
        return projectResponse;
    }
    public static ProjectGetResponse toProjectGetResponse(ProjectDetail projectDetail)
    {
        ProjectGetResponse projectResponse = new ProjectGetResponse();
        projectResponse.setId(projectDetail.getId());
        projectResponse.setName(projectDetail.getName());
        projectResponse.setDescription(projectDetail.getDescription());
        projectResponse.setOrganizationId(projectDetail.getOrganizationId());
        projectResponse.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
        projectResponse.setProjectType(projectDetail.getProjectType());
        projectResponse.setProjectTypeId(projectDetail.getProjectTypeId());
        projectResponse.setEnvironment(projectDetail.getEnvironment());
        projectResponse.setProjectState(projectDetail.getState());
        projectResponse.setCreatedAt(projectDetail.getCreatedAt());
        projectResponse.setUpdatedAt(projectDetail.getUpdatedAt());

        projectResponse.setOcrEngineDetails(projectDetail.getOcrEngineDetails());
        projectResponse.setFields(toFields(projectDetail));
        projectResponse.setVersionId(projectDetail.getVersionId());

        return projectResponse;
    }

    public static ProjectGetResponse toProjectGetResponse(ProjectDetailModel projectDetail) {
        ProjectGetResponse projectResponse = new ProjectGetResponse();
        projectResponse.setId(projectDetail.getId());
        projectResponse.setName(projectDetail.getName());
        projectResponse.setDescription(projectDetail.getDescription());
        projectResponse.setOrganizationId(projectDetail.getOrganizationId());
        projectResponse.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
        projectResponse.setProjectType(projectDetail.getProjectType());
        projectResponse.setProjectTypeId(projectDetail.getProjectTypeId());
        projectResponse.setEnvironment(projectDetail.getEnvironment());
        projectResponse.setProjectState(projectDetail.getState());
        projectResponse.setCreatedAt(projectDetail.getCreatedAt());
        projectResponse.setUpdatedAt(projectDetail.getUpdatedAt());

        projectResponse.setOcrEngineDetails(projectDetail.getOCREngineDetails());
        projectResponse.setFields(toFields(projectDetail));

        projectResponse.setVersionId(projectDetail.getVersionId());

        return projectResponse;
    }

    public static CustomField toCustomField(CustomFieldDetail customFieldDetail)
    {
        CustomField customField = new CustomField();
        customField.setName(customFieldDetail.getName());
        customField.setType(customFieldDetail.getType());
        customField.setId(customFieldDetail.getId());
        return customField;
    }
    public static CustomFieldV2 toCustomFieldV2(CustomFieldDetail customFieldDetail)
    {
      CustomFieldV2 customField = new CustomFieldV2();
      customField.setId(customFieldDetail.getId());
      customField.setName(customFieldDetail.getName());
      customField.setType(customFieldDetail.getType());
      customField.setAddedLater(customFieldDetail.isAddedLater());

      return customField;
    }
    public static StandardFieldV2 toStandardFieldV2(StandardFieldDetail standardFieldDetail)
    {
      StandardFieldV2 standardField = new StandardFieldV2();
      standardField.setId(standardFieldDetail.getPrimaryKey().getId());
      standardField.setAddedLater(standardFieldDetail.isAddedLater());
      return standardField;
    }
    public static CustomField toCustomField(CustomFieldDetailModel customFieldDetail)
    {
        CustomField customField = new CustomField();
        customField.setName(customFieldDetail.getName());
        customField.setType(customFieldDetail.getType());
        customField.setId(customFieldDetail.getId());
        return customField;
    }

    public static String[] toStandardFields(List<StandardFieldDetail> standardFieldDetailList)
    {
        String[] standardFields = new String[standardFieldDetailList.size()];
        int count = 0;
        for (StandardFieldDetail standardFieldDetail: standardFieldDetailList)
        {
            standardFields[count] = standardFieldDetail.getPrimaryKey().getId();
            count++;
        }

        return standardFields;
    }

    public static File toFile(FileDetail fileDetail)
    {
        File file = new File();
        file.setId(fileDetail.getFileId());
        file.setName(fileDetail.getFileName());
        file.setFormat(fileDetail.getFormat());
        file.setLocation(fileDetail.getFileLocation());
        file.setProcessed(fileDetail.isProcessed());
        file.setProduction(fileDetail.isProduction());
        return file;
    }

    public static VisionBot toVisionBot(VisionbotData visionbotData)
    {
        VisionBot visionBot  = new VisionBot();
        visionBot.setId(visionbotData.getId());
        visionBot.setName(visionbotData.getName());
        visionBot.setCurrentState(visionbotData.getStatus());
        visionBot.setEnvironment(Environment.valueOf(visionbotData.getEnvironment()));
        return visionBot;
    }

    private static Fields toFields(ProjectDetail projectDetail) {
        Fields fields = new Fields();

        List<CustomField> customFieldList = new ArrayList<>();
        List<CustomFieldDetail> customFieldDetailList = new ArrayList<>(projectDetail.getCustomFieldDetailSet());
        Collections.sort(customFieldDetailList, CustomFieldDetail.COMPARE_BY_ORDER_NUMBER);
        for(CustomFieldDetail detail: customFieldDetailList)
        {
            customFieldList.add(DaoToDto.toCustomField(detail));
        }
        fields.setCustom(customFieldList);

        String[] standardFields = new String[projectDetail.getStandardFieldDetailSet().size()];
        int fieldCount = 0;

        List<StandardFieldDetail> standardFieldDetailList = new ArrayList<>(projectDetail.getStandardFieldDetailSet());
        Collections.sort(standardFieldDetailList, StandardFieldDetail.COMPARE_BY_ORDER_NUMBER);
        for (StandardFieldDetail standardFieldDetail : standardFieldDetailList) {
            standardFields[fieldCount++] = standardFieldDetail.getPrimaryKey().getId();
        }
        fields.setStandard(standardFields);

        return fields;
    }
  private static FieldsV2 toFieldsV2(ProjectDetail projectDetail) {
    FieldsV2 fields = new FieldsV2();

    List<CustomFieldV2> customFieldList = new ArrayList<>();
    List<CustomFieldDetail> customFieldDetailList = new ArrayList<>(projectDetail.getCustomFieldDetailSet());
    Collections.sort(customFieldDetailList, CustomFieldDetail.COMPARE_BY_ORDER_NUMBER);
    for(CustomFieldDetail detail: customFieldDetailList)
    {
      customFieldList.add(DaoToDto.toCustomFieldV2(detail));
    }
    fields.setCustom(customFieldList);

    List<StandardFieldV2> standardFieldV2List = new ArrayList<>();

    List<StandardFieldDetail> standardFieldDetailList = new ArrayList<>(projectDetail.getStandardFieldDetailSet());
    Collections.sort(standardFieldDetailList, StandardFieldDetail.COMPARE_BY_ORDER_NUMBER);
    for (StandardFieldDetail standardFieldDetail : standardFieldDetailList) {
      standardFieldV2List.add(DaoToDto.toStandardFieldV2(standardFieldDetail));
    }
    fields.setStandard(standardFieldV2List);

    return fields;
  }

    private static Fields toFields(ProjectDetailModel projectDetail) {
        Fields fields = new Fields();

        List<CustomField> customFieldList = new ArrayList<>();
        List<CustomFieldDetailModel> customFieldDetailList = new ArrayList<>(projectDetail.getCustomFieldDetails());
        Collections.sort(customFieldDetailList, CustomFieldDetailModel.COMPARE_BY_ORDER_NUMBER);
        for(CustomFieldDetailModel detail: customFieldDetailList) {
            customFieldList.add(DaoToDto.toCustomField(detail));
        }
        fields.setCustom(customFieldList);

        String[] standardFields = new String[projectDetail.getStandardFieldDetails().size()];
        int fieldCount = 0;

        List<StandardFieldDetailModel> standardFieldDetailList = new ArrayList<>(projectDetail.getStandardFieldDetails());
        Collections.sort(standardFieldDetailList, StandardFieldDetailModel.COMPARE_BY_ORDER_NUMBER);
        for (StandardFieldDetailModel standardFieldDetail : standardFieldDetailList) {
            standardFields[fieldCount++] = standardFieldDetail.getPrimaryKey().getId();
        }
        fields.setStandard(standardFields);

        return fields;
    }
    public static ProjectRequest toProjectRequest(ProjectDetail projectDetail)
    {
        ProjectRequest projectRequest = new ProjectRequest();
        projectRequest.setId(projectDetail.getId());
        projectRequest.setName(projectDetail.getName());
        projectRequest.setDescription(projectDetail.getDescription());
        projectRequest.setOrganizationId(projectDetail.getOrganizationId());
        projectRequest.setProjectType(projectDetail.getProjectType());
        projectRequest.setProjectTypeId(projectDetail.getProjectTypeId());
        projectRequest.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
        projectRequest.setFields(toFields(projectDetail));
        projectRequest.setProjectState(projectDetail.getState());
        projectRequest.setEnvironment(projectDetail.getEnvironment());
        projectRequest.setOcrEngineDetails(projectDetail.getOcrEngineDetails());
        projectRequest.setVersionId(projectDetail.getVersionId());
        return projectRequest;
    }
}
