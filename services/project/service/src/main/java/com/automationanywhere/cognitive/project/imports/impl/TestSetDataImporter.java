package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.TestSetDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.TestSet;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class TestSetDataImporter extends AdvanceDataImporter<TestSet> {

  private static final AALogger log = AALogger.create(TestSetDataImporter.class);
  private final TestSetDao testSetDao;
  private final TransitionResult transitionResult;
  private final FileHelper fileHelper;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter nextDataImporter;

  public TestSetDataImporter(
      FileReader csvFileReader,
      TestSetDao testSetDao,
      TransitionResult transitionResult,
      FileHelper fileHelper,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.testSetDao = testSetDao;
    this.transitionResult = transitionResult;
    this.fileHelper = fileHelper;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath, TaskOptions taskOptions)
      throws IOException, ProjectImportException, DuplicateProjectNameException, ParseException {
    log.entry();
    //fetch's data from csv path
    List<TestSet> testSetData = prepareDataForImport(unzippedFolderPath);
    List<String> importedTestSetInfoIdList = transitionResult.getImportedTestSetInfoIdList();

    //Iterate list to import only those TestSet data whoes associated TestSetInfo imported.
    List<TestSet> dataToImport = testSetData.stream()
        .filter(testSetDataRow -> importedTestSetInfoIdList
            .contains(testSetDataRow.getTestsetInfoId()))
        .collect(
            Collectors.toList());
    if (dataToImport != null && !dataToImport.isEmpty()) {
      testSetDao.batchInsert(dataToImport);
    }

    if (nextDataImporter != null) {
      nextDataImporter.importData(unzippedFolderPath, taskOptions);
    }
    log.exit();
  }


  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    nextDataImporter = dataImporter;
  }

  private List<TestSet> prepareDataForImport(Path extractedFolderPath)
      throws ProjectImportException, IOException {
    log.entry();
    Path csvFolderPath = Paths
        .get(extractedFolderPath.toString(), transitionResult.getProjectId());

    log.exit();
    return getDataFromFiles(csvFolderPath);
  }

  private List<TestSet> getDataFromFiles(Path csvFolderPath)
      throws IOException, ProjectImportException {
    String csvPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.TESTSET);
    List<Path> csvFiles = fileHelper
        .getAllFilesPaths(csvFolderPath, csvPattern);

    return getDataFromCsv(csvFiles, TestSet.class);
  }
}
