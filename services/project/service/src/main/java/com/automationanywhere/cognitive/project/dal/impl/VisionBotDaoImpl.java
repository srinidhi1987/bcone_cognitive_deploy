package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.VisionBotDao;
import com.automationanywhere.cognitive.project.model.dao.VisionBot;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class VisionBotDaoImpl implements
    VisionBotDao {

  private AALogger log = AALogger.create(VisionBotDaoImpl.class);
  private static final int BATCH_SIZE = 20;

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getVisionBots(List<String> visionBotDetailIds) {
    log.entry();

    String sqlQuery = "select * from VisionBot where id in (:visionBotDetailIds)";

    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("visionBotDetailIds", visionBotDetailIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> visionbots = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return visionbots;
  }

  @Override
  public void batchInsert(List<VisionBot> visionBotList) {
    Session session = sessionFactory.getCurrentSession();
    for (VisionBot visionBot : visionBotList) {
      session.save(visionBot);
      int index = visionBotList.indexOf(visionBot);
      if (index % BATCH_SIZE == 0) {
        session.flush();
        session.clear();
      }
    }
  }


}
