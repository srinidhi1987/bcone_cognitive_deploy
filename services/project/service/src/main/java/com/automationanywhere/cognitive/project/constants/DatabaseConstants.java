package com.automationanywhere.cognitive.project.constants;

public class DatabaseConstants {
  public static final String FILE_MANAGER_TRANSACTION_MANAGER = "filemanagerTransactionManager";
  public static final String CLASSIFIER_TRANSACTION_MANAGER = "classifierTransactionManager";
  public static final String ML_TRANSACTION_MANAGER = "mlTransactionManager";
}
