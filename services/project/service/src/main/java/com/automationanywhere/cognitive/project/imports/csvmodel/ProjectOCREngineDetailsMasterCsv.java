package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class ProjectOCREngineDetailsMasterCsv {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "Id")
  private int id;

  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "ProjectId")
  private String projectId;

  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "OCREngineDetailsId")
  private int ocrEngineDetailsId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public int getOcrEngineDetailsId() {
    return ocrEngineDetailsId;
  }

  public void setOcrEngineDetailsId(int ocrEngineDetailsId) {
    this.ocrEngineDetailsId = ocrEngineDetailsId;
  }
}
