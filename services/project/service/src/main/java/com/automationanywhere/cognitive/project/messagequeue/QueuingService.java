package com.automationanywhere.cognitive.project.messagequeue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Jemin.Shah on 3/16/2017.
 */
public interface QueuingService
{
    void publish(String message);
    void createChannel();
    void connectAndRetry();
    void closeConnection();
    void closeChannel();
    void connect() throws IOException, TimeoutException;
    int getMessageCount();
    String call(String message) throws IOException, InterruptedException;
}
