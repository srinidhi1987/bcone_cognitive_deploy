package com.automationanywhere.cognitive.project.model.dao;


import com.automationanywhere.cognitive.project.model.Environment;
import com.automationanywhere.cognitive.project.model.ProjectState;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "ProjectDetail")
public class ProjectDetailsModel implements Serializable {

  private static final long serialVersionUID = 4910179782812247458L;
  @Id
  @Column(name = "Id", length = 50)
  private String id;

  @Column(name = "Name", length = 50)
  @Pattern(regexp = "^[a-zA-Z0-9\\s_\\-]+$")
  private String name;

  @Column(name = "Description")
  private String description;

  @Column(name = "OrganizationId", length = 50)
  private String organizationId;

  @Column(name = "Type", length = 50)
  private String projectType;

  @Column(name = "TypeId", length = 50)
  private String projectTypeId;

  @Column(name = "PrimaryLanguage", length = 50)
  private String primaryLanguage;

  @Column(name = "State")
  @Enumerated(EnumType.STRING)
  private ProjectState state;

  @Column(name = "Environment")
  @Enumerated(EnumType.STRING)
  private Environment environment;

  @Column(name = "FileUploadToken", length = 50)
  private String fileUploadToken;

  @Column(name = "FileUploadTokenAlive")
  private boolean fileUploadTokenAlive;

  @Column(name = "CreatedBy", length = 50)
  private String createdBy;

  @Column(name = "LastUpdatedBy", length = 50)
  private String lastUpdatedBy;

  @Column(name = "CreatedAt")
  private Date createdAt;

  @Column(name = "UpdatedAt")
  private Date updatedAt;

  @Version
  @Column(name = "VersionId")
  private long versionId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  public String getProjectType() {
    return projectType;
  }

  public void setProjectType(String projectType) {
    this.projectType = projectType;
  }

  public String getProjectTypeId() {
    return projectTypeId;
  }

  public void setProjectTypeId(String projectTypeId) {
    this.projectTypeId = projectTypeId;
  }

  public String getPrimaryLanguage() {
    return primaryLanguage;
  }

  public void setPrimaryLanguage(String primaryLanguage) {
    this.primaryLanguage = primaryLanguage;
  }

  public ProjectState getState() {
    return state;
  }

  public void setState(ProjectState state) {
    this.state = state;
  }

  public Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  public String getFileUploadToken() {
    return fileUploadToken;
  }

  public void setFileUploadToken(String fileUploadToken) {
    this.fileUploadToken = fileUploadToken;
  }

  public boolean isFileUploadTokenAlive() {
    return fileUploadTokenAlive;
  }

  public void setFileUploadTokenAlive(boolean fileUploadTokenAlive) {
    this.fileUploadTokenAlive = fileUploadTokenAlive;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public long getVersionId() {
    return versionId;
  }

  public void setVersionId(long versionId) {
    this.versionId = versionId;
  }


}
