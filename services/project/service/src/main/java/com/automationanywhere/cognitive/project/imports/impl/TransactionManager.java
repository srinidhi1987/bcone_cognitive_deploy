package com.automationanywhere.cognitive.project.imports.impl;

import java.util.Map;
import org.hibernate.Transaction;

public class TransactionManager {

  private Map<String, Transaction> transactions;

  public void addTransaction(String transactionKey, Transaction transaction) {
    transactions.put(transactionKey, transaction);
  }

  public void rollBack() {
    for (Map.Entry<String, Transaction> transactionEntry : transactions.entrySet()) {
      transactionEntry.getValue().rollback();
    }
  }
}
