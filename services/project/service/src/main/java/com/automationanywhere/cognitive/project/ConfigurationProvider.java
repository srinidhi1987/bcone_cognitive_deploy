package com.automationanywhere.cognitive.project;

import com.automationanywhere.cognitive.common.inetaddress.wrapper.InetAddressWrapper;
import com.automationanywhere.cognitive.common.logger.AALogger;


/**
 * @author mitul.maheshwari
 * This class will construct the url of fileservice and vision bot service.
 */
public class ConfigurationProvider {
    private static String port;
    private static String fileManagerUrl;
    private static String visionbotUrl;
    private static String httpScheme = "http";
    private static final String HTTPS_SCHEME = "https";
    private static final String fullyQualifiedName = InetAddressWrapper.getFullyQualifiedName();

    private static AALogger log = AALogger.create(ConfigurationProvider.class);

    public static String getPort() {
        return port;
    }

	public static String getFileManagerUrl() {
		return fileManagerUrl;
	}

	public static void setHttpScheme(String httpScheme) {
		ConfigurationProvider.httpScheme = httpScheme;
	}

	public static String getVisionbotUrl() {
		return visionbotUrl;
	}

	public static void initialize(String portnumber, String fileManagerPort, String visionBotPort) {
		log.entry();
		log.debug("Port: " + portnumber);
		port = portnumber;
		if (ConfigurationProvider.httpScheme.equals(HTTPS_SCHEME)) {
			fileManagerUrl = String.format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, fileManagerPort);
			visionbotUrl = String.format("%s://%s:%s", HTTPS_SCHEME, fullyQualifiedName, visionBotPort);
		} else {
			fileManagerUrl = String.format("%s://%s:%s", httpScheme, fullyQualifiedName , fileManagerPort);
			visionbotUrl = String.format("%s://%s:%s", httpScheme, fullyQualifiedName, visionBotPort);
		}
		log.exit();
	}
}
