package com.automationanywhere.cognitive.project.Util;

/**
 * Created by Jemin.Shah on 6/28/2017.
 */
public class StringUtil
{
    public static boolean isNullOrEmpty(String value)
    {
        return value == null || value.trim() == "";
    }
}
