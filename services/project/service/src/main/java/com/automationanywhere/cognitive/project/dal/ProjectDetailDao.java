package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.model.dao.ProjectActivityDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailModel;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailsModel;
import java.util.List;
import java.util.Map;
import org.hibernate.SessionFactory;

/**
 * Created by Jemin.Shah on 3/31/2017.
 */
public interface ProjectDetailDao
{
    List<ProjectDetailModel> getProjectDetails(String organizationId) throws InvalidInputException;
    List<ProjectDetail> getProjectDetails(Map<String, String> restrictions, Map<String, String[]> queryParameters) throws InvalidInputException;
    void addProjectDetail(ProjectDetail projectDetail);
    void add(ProjectDetailsModel projectDetail);
    void deleteProjectDetail(ProjectDetail projectDetail);
    void deleteAll();
    void updateProjectDetail(ProjectDetail projectDetail) throws NotFoundException;
    void updateProjectDetail(ProjectDetailsModel projectDetailsModel);
    void deleteCustomFieldDetails(ProjectDetail projectDetail);
    void deleteStandardFieldDetails(ProjectDetail projectDetail);
    ProjectActivityDetail getDeletedProjectActivityByProjectId(String projectId);
    ProjectActivityDetail getDeletedProjectActivityByProjectName(String projectId);
    void addProjectActivity(ProjectActivityDetail projectActivityDetail);
    boolean projectNameExists(String projectName);
    boolean projectExists(String projectId);
    public List<String[]> getProjectsDetail(String[] projectIds) throws NotFoundException;
    public String[] getProjectDetail(String projectId);
    boolean isProduction(String projectId)throws NotFoundException;
    public SessionFactory getSessionFactory();
    public ProjectDetailsModel getProjectDetailModel(String projectId);
}
