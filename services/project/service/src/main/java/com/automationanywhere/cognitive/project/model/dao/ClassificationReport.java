package com.automationanywhere.cognitive.project.model.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ClassificationReport")
public class ClassificationReport implements Serializable{

  private static final long serialVersionUID = 7762925201721347021L;
  @Id
  @Column(name = "documentId")
  private String documentId;

  @Id
  @Column(name = "fieldId")
  private String fieldId;

  @Column(name = "aliasName")
  private String aliasName;

  @Column(name = "isFound")
  private boolean isFound;

  public ClassificationReport() {
  }

  public String getDocumentId() {
    return documentId;
  }

  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }

  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  public String getAliasName() {
    return aliasName;
  }

  public void setAliasName(String aliasName) {
    this.aliasName = aliasName;
  }

  public boolean isFound() {
    return isFound;
  }

  public void setFound(boolean found) {
    isFound = found;
  }
}