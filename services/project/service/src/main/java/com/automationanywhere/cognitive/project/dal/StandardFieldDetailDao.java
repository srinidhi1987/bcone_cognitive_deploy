package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.imports.StandardFieldDetail;
import java.util.List;
import org.hibernate.SessionFactory;


public interface StandardFieldDetailDao {

  public List<String[]> getStandardFieldDetails(String projectId);

  public List<StandardFieldDetail> getAll(String projectId);

  public void addAll(List<StandardFieldDetail> standardFieldDetailList);

  public void delete(String projectId);

  public SessionFactory getSessionFactory();
}
