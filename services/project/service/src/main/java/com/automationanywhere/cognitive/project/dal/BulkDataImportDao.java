package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.constants.TableConstants;
import java.nio.file.Path;
import org.hibernate.HibernateException;

public interface BulkDataImportDao {
  public int bulkInsert(Path dataFilePath,TableConstants tableConstants) throws HibernateException;
}
