package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class ClassificationReport {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "documentid")
  private String documentId;

  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "fieldid")
  private String fieldId;

  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "aliasname")
  private String aliasName;

  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "isfound")
  private boolean isFound;

  public String getDocumentId() {
    return documentId;
  }

  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }

  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  public String getAliasName() {
    return aliasName;
  }

  public void setAliasName(String aliasName) {
    this.aliasName = aliasName;
  }

  public boolean isFound() {
    return isFound;
  }

  public void setFound(boolean found) {
    isFound = found;
  }
}
