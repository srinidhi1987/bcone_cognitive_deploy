package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.DocumentProcessedDetailDao;
import com.automationanywhere.cognitive.project.model.dao.DocumentProcessedDetails;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class DocumentProcessedDetailDaoImpl implements
    DocumentProcessedDetailDao {

  private AALogger log = AALogger.create(DocumentProcessedDetailDaoImpl.class);
  private static final int BATCH_SIZE = 20;

  @Autowired
  @Qualifier("filemanagerSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public List<String[]> getDocumentProcessedDetails(List<String> visionBotRunDetailIds) {
    log.entry();

    String sqlQuery = "select * from DocumentProccessedDatails where visionbotrundetailsid in (:visionBotRunDetailIds)";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameterList("visionBotRunDetailIds", visionBotRunDetailIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> documentProccessedDetails = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return documentProccessedDetails;
  }

  @Override
  public List<DocumentProcessedDetails> get(List<String> visionBotRunDetailIdList) {
    Query<DocumentProcessedDetails> query = sessionFactory.getCurrentSession().createQuery(
        "from DocumentProcessedDetails where visionBotRunDetailsId in (:visionBotRunDetailIdList)",
        DocumentProcessedDetails.class);
    query.setParameterList("visionBotRunDetailIdList", visionBotRunDetailIdList);
    return query.list();
  }

  @Override
  public void addAll(List<DocumentProcessedDetails> documentProcessedDetailsList) {
    Session session = sessionFactory.getCurrentSession();
    for (DocumentProcessedDetails documentProcessedDetails : documentProcessedDetailsList) {
      session.saveOrUpdate(documentProcessedDetails);
      int index = documentProcessedDetailsList.indexOf(documentProcessedDetails);
      if (index % BATCH_SIZE == 0) {
        session.flush();
        session.clear();
      }
    }
  }
  @Override
  public void delete(List<String> visionBotRunDetailIds) {
    log.entry();
    String hqlQueryString = "delete DocumentProccessedDatails where visionbotrundetailsid in (:visionBotRunDetailIds)";
    Query query = sessionFactory.getCurrentSession().createQuery(hqlQueryString);
    query.setParameterList("visionBotRunDetailIds", visionBotRunDetailIds);
    query.executeUpdate();
    log.exit();
  }
}
