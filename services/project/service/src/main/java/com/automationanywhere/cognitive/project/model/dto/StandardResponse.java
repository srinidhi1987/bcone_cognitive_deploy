package com.automationanywhere.cognitive.project.model.dto;

import java.util.Arrays;

/**
 * Created by Jemin.Shah on 09-01-2017.
 */
public class StandardResponse
{
    private boolean success;
    private Object data;
    private String[] errors;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String[] getErrors() {
        return errors;
    }

    public void setError(String[] error) {
        this.errors = error;
    }

    @Override
    public String toString() {
        return "StandardResponse{" +
                "success=" + success +
                ", data=" + data +
                ", errors=" + Arrays.toString(errors) +
                '}';
    }
}
