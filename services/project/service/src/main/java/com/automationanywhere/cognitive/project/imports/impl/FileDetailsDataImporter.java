package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.FileDetailsDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.FileDetail;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.FileDetails;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class FileDetailsDataImporter extends AdvanceDataImporter<FileDetail> {

  private static final AALogger LOGGER = AALogger.create(FileDetailsDataImporter.class);

  private final FileHelper fileHelper;
  private final TransitionResult transitionResult;
  private final FileDetailsDao fileDetailsDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public FileDetailsDataImporter(
      FileReader csvFileReader,
      FileHelper fileHelper,
      TransitionResult transitionResult,
      FileDetailsDao fileDetailsDao,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.fileHelper = fileHelper;
    this.transitionResult = transitionResult;
    this.fileDetailsDao = fileDetailsDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.FILE_MANAGER_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    LOGGER.entry();

    //prepares data for import
    List<FileDetails> finalDataToImport = prepareData(taskOptions, unzippedFolderPath);

    //data inserted in batch.
    if (!finalDataToImport.isEmpty()) {
      if (taskOptions == TaskOptions.OVERWRITE) {
        fileDetailsDao.batchAddOrUpdateFileDetails(finalDataToImport);
      } else {
        fileDetailsDao.batchInsertFileDetails(finalDataToImport);
      }
    }
    //fileid's is being set.
    List<String> fileIds = getFileIds(finalDataToImport);
    transitionResult.setFileIds(fileIds);

    //next importer get's called.
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }
    LOGGER.exit();
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<FileDetails> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    LOGGER.entry();
    List<FileDetails> dataToImport = new ArrayList<>();

    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvRegexPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.FILEDETAILS);
    List<Path> pathsOfFileDetails = fileHelper
        .getAllFilesPaths(csvFolderPath, csvRegexPattern);

    dataToImport
        .addAll(DtoToDao.toFileDetails(getDataFromCsv(pathsOfFileDetails, FileDetail.class)));
    LOGGER.exit();
    return dataToImport;
  }

  private List<FileDetails> prepareData(TaskOptions taskOptions, Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    List<FileDetails> dataToImport = getDataFromFiles(unzippedFolderPath);
    List<FileDetails> finalDataToImport = new ArrayList<>();

    if (taskOptions == TaskOptions.MERGE) {
      //retrieve fileid's from csvdata of filedetails.
      List<String> fileIds = getFileIds(dataToImport);
      //fetching matched fileid's from db.
      List<String> matchedFileIds = fileDetailsDao
          .getFileDetails(transitionResult.getProjectId(), fileIds).stream()
          .map(fileDetails -> fileDetails.getFileId()).collect(Collectors.toList());
      // filtering final data to import based on matchedfileid's
      dataToImport = dataToImport.stream()
          .filter(fileDetails -> !matchedFileIds.contains(fileDetails.getFileId()))
          .collect(Collectors.toList());
    }
    //amending modified classification id's
    List<FileDetails> fileDetailsWithModifiedClassificationId = modifyClassificationIdForFileDetails(
        transitionResult.getModifiedClasses(), dataToImport);

    //amending modified layout id's
    finalDataToImport=modifyLayoutIdForFileDetails(transitionResult.getModifiedLayouts(),fileDetailsWithModifiedClassificationId);

    return finalDataToImport;
  }

  private List<FileDetails> modifyClassificationIdForFileDetails(
      Map<Integer, Integer> modifiedClasses, List<FileDetails> fileDetailsList) {
    return fileDetailsList.stream().map(
        fileDetails -> {
          Integer classificationId = modifiedClasses
              .get(Integer.parseInt(fileDetails.getClassificationId()));
          if (classificationId != null) {
            fileDetails.setClassificationId(String.valueOf(classificationId.intValue()));
            LOGGER.debug(String.format("ClassificationId is changed from %s to %s",fileDetails.getClassificationId(),String.valueOf(classificationId)));
          }
          return fileDetails;
        }).collect(Collectors.toList());
  }

  private List<FileDetails> modifyLayoutIdForFileDetails(
      Map<String, String> modifiedLayoutids, List<FileDetails> fileDetailsList) {
    return fileDetailsList.stream().map(
        fileDetails -> {
          String layoutId = modifiedLayoutids
              .get(fileDetails.getLayoutId());
          if (layoutId != null) {
            fileDetails.setLayoutId(String.valueOf(layoutId));
            LOGGER.debug(String.format("LayoutId is changed from %s to %s",fileDetails.getLayoutId(),layoutId));
          }
          return fileDetails;
        }).collect(Collectors.toList());
  }

  private List<String> getFileIds(List<FileDetails> fileDetailsList) {
    return fileDetailsList.stream().map(fileDetails -> fileDetails.getFileId())
        .collect(Collectors.toList());
  }
}
