package com.automationanywhere.cognitive.project.imports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransitionResult {

  private Map<Integer, Integer> modifiedClasses = new HashMap<>();
  private Map<String, String> modifiedLayouts = new HashMap<>();
  private List<String> importedVisionBotIdList = new ArrayList<>();
  private List<String> importedTestSetInfoIdList = new ArrayList<>();
  private List<String> importedVisionBotRunDetailIdList = new ArrayList<>();
  private List<String> fileIds = new ArrayList<>();
  private String projectId;
  private String userId;
  private boolean isNewProject = true;

  public Map<Integer, Integer> getModifiedClasses() {
    return modifiedClasses;
  }

  public void setModifiedClasses(
      Map<Integer, Integer> modifiedClasses) {
    this.modifiedClasses = modifiedClasses;
  }

  public List<String> getImportedTestSetInfoIdList() {
    return importedTestSetInfoIdList;
  }

  public void setImportedTestSetInfoIdList(List<String> importedTestSetInfoIdList) {
    this.importedTestSetInfoIdList = importedTestSetInfoIdList;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Map<String, String> getModifiedLayouts() {
    return modifiedLayouts;
  }

  public void setModifiedLayouts(Map<String, String> modifiedLayouts) {
    this.modifiedLayouts = modifiedLayouts;
  }

  public List<String> getImportedVisionBotIdList() {
    return importedVisionBotIdList;
  }

  public void setImportedVisionBotIdList(List<String> importedVisionBotIdList) {
    this.importedVisionBotIdList = importedVisionBotIdList;
  }

  public List<String> getImportedVisionBotRunDetailIdList() {
    return importedVisionBotRunDetailIdList;
  }

  public void setImportedVisionBotRunDetailIdList(
      List<String> importedVisionBotRunDetailIdList) {
    this.importedVisionBotRunDetailIdList = importedVisionBotRunDetailIdList;
  }

  public List<String> getFileIds() {
    return fileIds;
  }

  public void setFileIds(List<String> fileIds) {
    this.fileIds = fileIds;
  }

  public boolean isNewProject() {
    return isNewProject;
  }

  public void setNewProject(boolean newProject) {
    isNewProject = newProject;
  }

  public void clearProjectDetails() {
    importedVisionBotIdList.clear();
    importedVisionBotRunDetailIdList.clear();
    importedTestSetInfoIdList.clear();
    projectId = null;
    userId = null;
    fileIds.clear();
    isNewProject = true;
  }

  public void clearClassificationDetails() {
    modifiedClasses.clear();
    modifiedLayouts.clear();
  }
}