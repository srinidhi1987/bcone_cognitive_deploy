package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.Util.DataUtil;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExportListener;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.ExportOperator;
import com.automationanywhere.cognitive.project.export.model.VisionBotDataExportMetadata;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import javax.inject.Inject;

public class VisionBotDataExportOperator implements ExportOperator {

  private static final ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();

  private final DataExporter visionBotDataExporter;


  @Inject
  public VisionBotDataExportOperator(DataExporter visionBotDataExporter) {
    this.visionBotDataExporter = visionBotDataExporter;
  }

  @Override
  public void export(String dirPath, String[] projectIds,
      DataExportListener dataExportListener) {
    forkJoinPool.execute(new VisionBotDataExportTask(dirPath, projectIds, dataExportListener));
  }

  private class VisionBotDataExportTask extends RecursiveTask<List<VisionBotDataExportMetadata>> {

    private static final long serialVersionUID = 8699639042198411233L;
    private static final int PROJECT_NUMBER_THRESHOLD = 10;
    private final String dirPath;
    private final String[] projectIds;
    private DataExportListener dataExportListener;

    VisionBotDataExportTask(String dirPath, String[] projectIds,
        DataExportListener dataExportListener) {
      this(dirPath, projectIds);
      this.dataExportListener = dataExportListener;
    }

    private VisionBotDataExportTask(String dirPath, String[] projectIds) {
      this.dirPath = dirPath;
      this.projectIds = projectIds;
    }

    @Override
    public List<VisionBotDataExportMetadata> compute() {
      try {
        List<VisionBotDataExportMetadata> metadata;
        if (projectIds.length > PROJECT_NUMBER_THRESHOLD) {
          metadata = ForkJoinTask.invokeAll(createSubTasks()).stream().map(ForkJoinTask::join)
              .flatMap(List::stream).collect(
                  Collectors.toList());
        } else {
          metadata = new ArrayList<>();
          for (String projectId : projectIds) {
            metadata.add(exportData(projectId));
          }
        }
        if (dataExportListener != null) {
          dataExportListener.onVisionBotDataExportCompleted(metadata);
        }
        return metadata;
      } catch (Exception e) {
        if (dataExportListener != null) {
          dataExportListener.onException(e);
        }
        throw e;
      }
    }

    private List<VisionBotDataExportTask> createSubTasks() {
      List<String[]> lstOfArrayOfProjectIds = DataUtil
          .subDivide(projectIds, PROJECT_NUMBER_THRESHOLD);

      List<VisionBotDataExportTask> subTasks = new ArrayList<>();

      lstOfArrayOfProjectIds.stream().forEach(
          subProjectIds -> subTasks.add(new VisionBotDataExportTask(dirPath, subProjectIds)));

      return subTasks;
    }


    private VisionBotDataExportMetadata exportData(String projectId) throws ProjectExportException {

      Map<Object, Object> exportParams = new HashMap<Object, Object>();
      exportParams.put("projectId", projectId);
      exportParams.put("isProduction", 0);

      @SuppressWarnings("unchecked")
      VisionBotDataExportMetadata visionBotDataExportMetadata = (VisionBotDataExportMetadata) visionBotDataExporter
          .exportData(dirPath, exportParams);

      return visionBotDataExportMetadata;
    }
  }
}
