package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.model.ActivityTypes;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.CustomFieldDetailModel;
import com.automationanywhere.cognitive.project.model.dao.ProjectActivityDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailModel;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetailsModel;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetail;
import com.automationanywhere.cognitive.project.model.dao.StandardFieldDetailModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Created by Jemin.Shah on 3/31/2017.
 */
public class ProjectDetailDaoImpl implements ProjectDetailDao {

  private AALogger log = AALogger.create(ProjectDetailDaoImpl.class);
  @Autowired
  @Qualifier("filemanagerSessionFactory")

  private SessionFactory sessionFactory;
  @Autowired
  private ProjectDetailsByQueryParameter projectDetailsByQueryParameter;
  @Autowired
  private DataTransformer dataTransformer;

  @Override
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  public List<ProjectDetailModel> getProjectDetails(String organizationId)
      throws InvalidInputException {
    log.entry();
    log.debug("organizationid: {}", organizationId);
    // Query
    Session session;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (HibernateException e) {
      log.error(e.getMessage());
      session = sessionFactory.openSession();
    }

    @SuppressWarnings("unchecked") List<Object[]> results =
        session.createNativeQuery("SELECT {p.*}, {c.*}, {s.*}" +
            "  FROM ProjectDetail p " +
            "  LEFT OUTER JOIN StandardFieldDetail s " +
            "    ON p.Id = s.ProjectId" +
            "  LEFT OUTER JOIN CustomFieldDetail c " +
            "    ON p.Id = c.ProjectId ")
            .addEntity("p", ProjectDetailModel.class)
            .addEntity("s", StandardFieldDetailModel.class)
            .addEntity("c", CustomFieldDetailModel.class)
            .list();

    // Construct the ProjectDetail list
    List<ProjectDetailModel> projectDetailList = new ArrayList<>();
    Map<String, ProjectDetailModel> projectDetailsMap = new HashMap<>();

    results.forEach((record) -> {
      ProjectDetailModel projectDetail = (ProjectDetailModel) record[0];
      StandardFieldDetailModel standardFieldDetail = (StandardFieldDetailModel) record[1];
      CustomFieldDetailModel customFieldDetail = (CustomFieldDetailModel) record[2];

      // DEBUG CODE - Todo: Normally never commented out and checked in. Remove once stable
//            if (customFieldDetail != null && !projectDetail.getId().equals(customFieldDetail.getProjectId()) ||
//                standardFieldDetail != null && !projectDetail.getId().equals(standardFieldDetail.getPrimaryKey().getProjectId())) {
//                System.out.printf(
//                    "IDs DIFFER! Project Id: %s, standardFieldDetail.Id: %s, standardFieldDetail.ProjectId: %s, customFieldDetail.ProjectId: %s\n",
//                    projectDetail.getId(),
//                    ((standardFieldDetail != null) ? standardFieldDetail.getPrimaryKey().getId()
//                                                   : ""),
//                    ((standardFieldDetail != null) ? standardFieldDetail.getPrimaryKey()
//                        .getProjectId() : ""),
//                    ((customFieldDetail != null) ? customFieldDetail.getProjectId() : ""));
//            }

      // Check to see if we already have processed this project
      if (projectDetailsMap.containsKey(projectDetail.getId())) {
        // get the project detail object from the map
        projectDetail = projectDetailsMap.get(projectDetail.getId());
      } else {
        // initialize a new project detail object
        projectDetail.setCustomFieldDetails(new HashSet<>());
        projectDetail.setStandardFieldDetails(new HashSet<>());
        projectDetailsMap.put(projectDetail.getId(), projectDetail);
        projectDetailList.add(projectDetail);
      }

      Set<CustomFieldDetailModel> customFieldDetails = projectDetail.getCustomFieldDetails();
      if (customFieldDetail != null && !customFieldDetails.contains(customFieldDetail)) {
        projectDetail.addCustomFieldDetails(customFieldDetail);
      }

      Set<StandardFieldDetailModel> standardFieldDetails = projectDetail.getStandardFieldDetails();
      if (standardFieldDetail != null && !standardFieldDetails.contains(standardFieldDetail)) {
        projectDetail.addStandardFieldDetails(standardFieldDetail);
      }
    });
    log.exit();
    return projectDetailList;
  }

  @Override
  public List<ProjectDetail> getProjectDetails(Map<String, String> restrictions,
      Map<String, String[]> queryParameters) throws InvalidInputException {
    return this.projectDetailsByQueryParameter.getProjectDetails(restrictions, queryParameters);
  }

  @Override
  public void addProjectDetail(ProjectDetail projectDetail) {
    log.entry();
    sessionFactory.getCurrentSession().save(projectDetail);
    log.exit();
  }

  @Override
  public void deleteProjectDetail(ProjectDetail item) {
    log.entry();
    String hql = "delete " + ProjectDetail.class.getName()
        + " where id = :id and organizationId = :organizationId";
    Query query = sessionFactory.getCurrentSession().createQuery(hql);
    query.setParameter("id", item.getId());
    query.setParameter("organizationId", item.getOrganizationId());
    query.executeUpdate();
    log.exit();
  }

  @Override
  public void updateProjectDetail(ProjectDetail item) {
    log.entry();
    sessionFactory.getCurrentSession().clear();
    sessionFactory.getCurrentSession().merge(item);
    sessionFactory.getCurrentSession().flush();
    log.exit();
  }

  @Override
  public void updateProjectDetail(ProjectDetailsModel projectDetailsModel) {
    sessionFactory.getCurrentSession().clear();
    sessionFactory.getCurrentSession().saveOrUpdate(projectDetailsModel);
    sessionFactory.getCurrentSession().flush();
  }

  @Override
  public void deleteCustomFieldDetails(ProjectDetail projectDetail) {
    log.entry();
    sessionFactory.getCurrentSession().clear();
    if (projectDetail.getCustomFieldDetailSet() == null) {
      return;
    }

    for (CustomFieldDetail customFieldDetail : projectDetail.getCustomFieldDetailSet()) {
      sessionFactory.getCurrentSession().delete(customFieldDetail);
    }
    sessionFactory.getCurrentSession().flush();
    log.exit();
  }

  @Override
  public void deleteStandardFieldDetails(ProjectDetail projectDetail) {
    log.entry();
    if (projectDetail.getCustomFieldDetailSet() == null) {
      return;
    }

    for (StandardFieldDetail standardFieldDetail : projectDetail.getStandardFieldDetailSet()) {
      sessionFactory.getCurrentSession().delete(standardFieldDetail);
    }

    sessionFactory.getCurrentSession().flush();
    log.exit();
  }

  @Override
  public ProjectActivityDetail getDeletedProjectActivityByProjectId(String projectId) {
    log.entry();
    String hql = "from ProjectActivityDetail where projectId = :projectId and activityId = "
        + ActivityTypes.Delete.getValue();
    Query query = sessionFactory.getCurrentSession().createQuery(hql);
    query.setParameter("projectId", projectId);
    List results = query.list();

    if (results == null || results.size() == 0) {
      return null;
    }
    log.exit();
    return (ProjectActivityDetail) results.stream().findFirst().get();
  }

  @Override
  public ProjectActivityDetail getDeletedProjectActivityByProjectName(String projectName) {
    log.entry();
    String hql = "from ProjectActivityDetail where projectName = :projectName and activityId = "
        + ActivityTypes.Delete.getValue();
    Query query = sessionFactory.getCurrentSession().createQuery(hql);
    query.setParameter("projectName", projectName);
    List results = query.list();

    if (results == null || results.size() == 0) {
      return null;
    }
    log.exit();
    return (ProjectActivityDetail) results.stream().findFirst().get();
  }

  @Override
  public void addProjectActivity(ProjectActivityDetail projectActivityDetail) {
    log.entry();
    sessionFactory.getCurrentSession().save(projectActivityDetail);
    log.exit();
  }

  @Override
  public boolean projectExists(String projectId) {
    log.entry();
    Session session = sessionFactory.getCurrentSession();
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
    Root<ProjectDetail> projectDetailModel = criteriaQuery.from(ProjectDetail.class);
    criteriaQuery.select(criteriaBuilder.count(projectDetailModel));

    ParameterExpression<String> id = criteriaBuilder.parameter(String.class, "id");
    Predicate restriction = criteriaBuilder.equal(projectDetailModel.get("id"), id);
    criteriaQuery.where(restriction);

    Query<Long> countQuery = sessionFactory.getCurrentSession().createQuery(criteriaQuery);
    countQuery.setParameter("id", projectId);
    Long count = countQuery.getSingleResult();

    log.exit();
    return count != null && count == 1;
  }

  @Override
  public boolean projectNameExists(String projectName) {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("from ProjectDetail where name = :projectName");
    query.setParameter("projectName", projectName);
    @SuppressWarnings("unchecked")
    List<Object> list = query.getResultList();
    return list.size() > 0;
  }

  @Override
  public List<String[]> getProjectsDetail(String[] projectIds) throws NotFoundException {
    log.entry();
    Session session = sessionFactory.getCurrentSession();
    session.clear();

    String sqlQuery = "select * from ProjectDetail where id in (:projectIds)";
    Query query = session.createNativeQuery(sqlQuery);
    query.setParameterList("projectIds", projectIds);
    @SuppressWarnings("unchecked")
    List<Object[]> resultSetData = query.getResultList();
    List<String[]> projectDetails = dataTransformer.transformToListOfStringArray(resultSetData);

    session.flush();

    if (projectDetails == null || projectDetails.isEmpty()) {
      throw new NotFoundException(
          "ProjectDetails not found for project ids : " + String.join(",", projectIds));
    }
    log.exit();
    return projectDetails;
  }

  @Override
  public String[] getProjectDetail(String projectId) {
    log.entry();

    String sqlQuery = "select * from ProjectDetail where id = :projectId";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId);
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();
    String[] projectDetails = null;
    if (resulSetData != null && !resulSetData.isEmpty()) {
      Object[] projectDetailObj = resulSetData.get(0);
      projectDetails = new String[projectDetailObj.length];

      for (int i = 0; i < projectDetailObj.length; i++) {
        if (projectDetailObj[i] == null) {
          projectDetails[i] = "";
        } else {
          if (projectDetailObj[i] instanceof Boolean) {
            projectDetails[i] = (Boolean) projectDetailObj[i] ? "1" : "0";
          } else {
            projectDetails[i] = String.valueOf(projectDetailObj[i]);
          }
        }
      }

    }

    log.exit();
    return projectDetails;
  }

  @Override
  public ProjectDetailsModel getProjectDetailModel(String projectId) {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("from ProjectDetailsModel where id = :projectId");
    query.setParameter("projectId", projectId);

    return (ProjectDetailsModel) query.getSingleResult();
  }

  @Override
  public boolean isProduction(String projectId) throws NotFoundException {
    log.entry();
    boolean isProdcution = false;
    String[] projectDetail = getProjectDetail(projectId);
    String environment = projectDetail[8];
    isProdcution = environment.equals("production") ? true : false;
    log.exit();
    return isProdcution;
  }

  @Override
  public void add(ProjectDetailsModel projectDetail) {
    sessionFactory.getCurrentSession().save(projectDetail);
  }

  @Override
  public void deleteAll() {
    String sqlQuery = "delete from ProjectDetail";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.executeUpdate();
  }
}