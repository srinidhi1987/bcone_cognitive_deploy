package com.automationanywhere.cognitive.project.exception;

public class UnSupportedConnectionTypeException extends RuntimeException{

    private static final long serialVersionUID = -2902892883260064576L;

    public UnSupportedConnectionTypeException(String message) {
        super(message);
    }

}