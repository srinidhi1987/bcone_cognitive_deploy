package com.automationanywhere.cognitive.project.model.constants;

public enum RunState {
  Running(1),
  Completed(2);

  private int value;

  private RunState(int value) {
    this.value = value;
  }

  public static RunState getRunStateByOrdinal(int value){
    return value==0?RunState.Running:RunState.Completed;
  }
}
