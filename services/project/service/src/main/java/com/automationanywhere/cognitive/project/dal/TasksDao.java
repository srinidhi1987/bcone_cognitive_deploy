package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.Task;

import java.util.List;

public interface TasksDao {
    List<Task> getRunningTasks();
    void addTask(Task task);
    void updateTask(Task task);
    List<Task> getSortedTaskList(String sortColumnName, String sortOrder);
}
