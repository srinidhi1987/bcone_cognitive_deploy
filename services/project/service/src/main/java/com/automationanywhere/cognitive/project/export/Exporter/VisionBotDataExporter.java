package com.automationanywhere.cognitive.project.export.Exporter;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.DocumentProcessedDetailDao;
import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.dal.TestSetDao;
import com.automationanywhere.cognitive.project.dal.TestSetInfoDao;
import com.automationanywhere.cognitive.project.dal.VisionBotDao;
import com.automationanywhere.cognitive.project.dal.VisionbotDetailDao;
import com.automationanywhere.cognitive.project.dal.VisionbotRunDetailsDao;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.FileWriter;
import com.automationanywhere.cognitive.project.export.model.VisionBotDataExportMetadata;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class VisionBotDataExporter implements DataExporter<VisionBotDataExportMetadata> {

  @Inject
  private VisionbotDetailDao visionbotDetailDao;
  @Inject
  private VisionBotDao visionBotDao;
  @Inject
  private TestSetInfoDao testSetInfoDao;
  @Inject
  private TestSetDao testSetDao;
  @Inject
  private VisionbotRunDetailsDao visionbotRunDetailsDao;
  @Inject
  private DocumentProcessedDetailDao documentProcessedDetailDao;
  @Inject
  private ProjectDetailDao projectDetailDao;

  @Inject
  private FileWriter csvFileWriter;

  private AALogger log = AALogger.create(VisionBotDataExporter.class);

  @Transactional(value = "filemanagerTransactionManager", propagation = Propagation.REQUIRES_NEW)
  @Override
  public VisionBotDataExportMetadata exportData(String path, Map<Object, Object> data)
      throws ProjectExportException {
    log.entry();
    VisionBotDataExportMetadata visionBotDataExportMetadata = new VisionBotDataExportMetadata();
    try {
      String projectId = data.get("projectId").toString();
      String projectExportDirPath = prepareProjectDirectory(path, projectId);

      long visionbotDetailRowCount = exportVisionbotDetailData(projectExportDirPath, projectId);
      long visionbotRowCount = 0l, visionbotRunDetailsRowCount = 0l, testSetInfoRowCount = 0l, documentProcessedDetailRowCount = 0l, testSetRowCount = 0l;
      List<String> visionBotDetailIds = fetchVisionBotDetailIds(projectId);
      if (visionBotDetailIds != null && !visionBotDetailIds.isEmpty()) {
        visionbotRowCount = exportVisionbotData(projectExportDirPath, visionBotDetailIds);
        visionbotRunDetailsRowCount = exportVisionbotRunDetailsData(projectExportDirPath,
            visionBotDetailIds);
        testSetInfoRowCount = exportTestSetInfoData(projectExportDirPath, visionBotDetailIds);

        List<String> visionbotRunDetailsIds = fetchVisionbotRunDetailsIds(visionBotDetailIds);
        if (visionbotRunDetailsIds != null && !visionbotRunDetailsIds.isEmpty()) {
          documentProcessedDetailRowCount = exportDocumentProcessedDetailData(projectExportDirPath,
              visionbotRunDetailsIds);
        }
        List<String> testSetInfoIds = fetchTestInfoIds(visionBotDetailIds);
        if (testSetInfoIds != null && !testSetInfoIds.isEmpty()) {
          testSetRowCount = exportTestSetData(projectExportDirPath, testSetInfoIds);
        }

      }

      visionBotDataExportMetadata = new VisionBotDataExportMetadata(visionbotDetailRowCount,
          visionbotRowCount, visionbotRunDetailsRowCount, testSetInfoRowCount,
          documentProcessedDetailRowCount, testSetRowCount);

    } catch (Exception e) {
      throw new ProjectExportException("Got error while exporting visionbot related data to csv.",
          e);
    }
    log.exit();
    return visionBotDataExportMetadata;
  }

  private List<String> fetchVisionBotDetailIds(String projectId) {
    List<String> visionBotDetailIds = visionbotDetailDao.getVisionbotDetailIds(projectId);

    if (visionBotDetailIds == null || visionBotDetailIds.isEmpty()) {
      log.info("VisionbotDetail id's not found for project id : " + projectId);
    }

    return visionBotDetailIds;
  }

  private List<String> fetchTestInfoIds(List<String> visionBotDetailIds) {
    List<String> testSetInfoIds = testSetInfoIds = testSetInfoDao
        .getTestSetInfoIds(visionBotDetailIds);
    if (testSetInfoIds == null || testSetInfoIds.isEmpty()) {
      log.info("TestSetInfo id's not found for  : " + String.join(",", visionBotDetailIds));
    }
    return testSetInfoIds;
  }

  private List<String> fetchVisionbotRunDetailsIds(List<String> visionBotDetailIds) {
    List<String> visionbotRunDetailsIds = visionbotRunDetailsDao
        .getVisionbotRunDetailsIds(visionBotDetailIds);
    if (visionBotDetailIds == null || visionBotDetailIds.isEmpty()) {
      log.info("VisionbotRunDetails id's not found for  : " + String.join(",", visionBotDetailIds));
    }
    return visionbotRunDetailsIds;
  }

  private long exportVisionbotDetailData(String projectExportDirPath,
      String projectId) throws IOException {
    log.entry();
    long visionbotDetailRowCount = 0l;
    List<String[]> visionbotDetailData = visionbotDetailDao.getVisionbotDetails(projectId);

    if (visionbotDetailData != null && !visionbotDetailData.isEmpty()) {
      String[] headerData = {"id", "organizationid", "projectid", "classificationid", "environment",
          "lockeduserid"};

      visionbotDetailData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.VISIONBOTDETAIL.getName()+".csv"),
          visionbotDetailData);
      visionbotDetailRowCount = visionbotDetailData.size() - 1;
    } else {
      log.info("VisionbotDetail data not found for project id : " + projectId);
    }

    log.exit();
    return visionbotDetailRowCount;
  }

  private long exportVisionbotData(String projectExportDirPath,
      List<String> visionBotDetailIds) throws IOException {
    log.entry();
    long visionbotRowCount = 0l;
    List<String[]> visionbotData = visionBotDao.getVisionBots(visionBotDetailIds);
    if (visionbotData != null && !visionbotData.isEmpty()) {
      String[] headerData = {"id", "datablob", "validationdatablob", "environment","LockedUserid"};
      visionbotData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.VISIONBOT.getName()+".csv"),
          visionbotData);
      visionbotRowCount = visionbotData.size() - 1;
    } else {
      log.info("Visionbot data not found for id's : " + String.join(",", visionBotDetailIds));
    }

    log.exit();
    return visionbotRowCount;
  }

  private long exportVisionbotRunDetailsData(String projectExportDirPath,
      List<String> visionBotDetailIds) throws IOException {
    log.entry();
    long visionbotRunDetailsRowCount = 0l;
    List<String[]> visionbotRunDetailsData = visionbotRunDetailsDao
        .getVisionbotRunDetails(visionBotDetailIds);

    if (visionbotRunDetailsData != null && !visionbotRunDetailsData.isEmpty()) {
      String[] headerData = {"id", "visionbotid", "createdat", "environment", "totalfiles",
          "runstateid"};
      visionbotRunDetailsData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.VISIONBOTRUNDETAILS.getName()+".csv"),
          visionbotRunDetailsData);
      visionbotRunDetailsRowCount = visionbotRunDetailsData.size() - 1;
    } else {
      log.info(
          "VisionBotRunDetails data not found for visionbotid's : " + String
              .join(",", visionBotDetailIds));
    }
    log.exit();
    return visionbotRunDetailsRowCount;
  }

  private long exportDocumentProcessedDetailData(String projectExportDirPath,
      List<String> visionbotRunDetailsIds) throws IOException {
    log.entry();
    long documentProcessedDetailRowCount = 0l;
    List<String[]> documentProcessedDetailData = documentProcessedDetailDao
        .getDocumentProcessedDetails(visionbotRunDetailsIds);

    if (documentProcessedDetailData != null && !documentProcessedDetailData.isEmpty()) {
      String[] headerData = {"id", "docid", "visionbotrundetailsid", "processingstatus",
          "totalfieldcount", "passedfieldcount", "failedfieldcount"};
      documentProcessedDetailData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.DOCUMENTPROCCESSEDDATAILS.getName()+".csv"),
          documentProcessedDetailData);
      documentProcessedDetailRowCount = documentProcessedDetailData.size() - 1;
    } else {
      log.info(
          "DocumentProccessedDatails data not found for visionbotrundetailsid's : " + String
              .join(",", visionbotRunDetailsIds));
    }

    log.exit();
    return documentProcessedDetailRowCount;
  }

  private long exportTestSetInfoData(String projectExportDirPath,
      List<String> visionBotDetailIds) throws IOException {
    log.entry();
    long testSetInfoRowCount = 0l;
    List<String[]> testSetInfoData = testSetInfoDao.getTestSetInfos(visionBotDetailIds);

    if (testSetInfoData != null && !testSetInfoData.isEmpty()) {
      String[] headerData = {"id", "visionbotid", "layoutid"};
      testSetInfoData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.TESTSETINFO.getName()+".csv"),
          testSetInfoData);
      testSetInfoRowCount = testSetInfoData.size() - 1;
    } else {
      log.info("TestSetInfo data not found for id's : " + String.join(",", visionBotDetailIds));
    }

    log.exit();
    return testSetInfoRowCount;
  }

  private long exportTestSetData(String projectExportDirPath,
      List<String> testSetInfoIds) throws IOException {
    log.entry();
    long testSetRowCount = 0l;
    List<String[]> testSetData = testSetDao.getTestSets(testSetInfoIds);

    if (testSetData != null && !testSetData.isEmpty()) {
      String[] headerData = {"id", "name", "docitems", "testsetinfoid"};
      testSetData.add(0, headerData);
      csvFileWriter.writeData(Paths.get(projectExportDirPath,TableConstants.TESTSET.getName()+".csv"),
          testSetData);
      testSetRowCount = testSetData.size() - 1;
    } else {
      log.info("TestSet data not found for testsetinfoid's : " + String.join(",", testSetInfoIds));
    }
    log.exit();
    return testSetRowCount;
  }

  public String prepareProjectDirectory(String path, String projectId) {
    String projectExportDirPath = path + "/" + projectId;
    File file = new File(projectExportDirPath);
    if (!file.exists()) {
      file.mkdirs();
    }
    return projectExportDirPath;
  }

}
