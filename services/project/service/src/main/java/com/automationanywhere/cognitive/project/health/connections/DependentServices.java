package com.automationanywhere.cognitive.project.health.connections;

/**
 * @author shweta.thakur
 * This class will hold Dependent micro-services list
 */
public enum DependentServices {
    FileManager, VisionBot
}
