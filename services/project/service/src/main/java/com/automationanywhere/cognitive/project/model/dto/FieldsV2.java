package com.automationanywhere.cognitive.project.model.dto;

import java.util.Arrays;
import java.util.List;

public class FieldsV2
{
    private List<StandardFieldV2> standard;
    private List<CustomFieldV2> custom;

    public List<StandardFieldV2> getStandard() {
        return standard;
    }

    public void setStandard(
        List<StandardFieldV2> standard) {
        this.standard = standard;
    }

    public List<CustomFieldV2> getCustom() {
        return custom;
    }

    public void setCustom(List<CustomFieldV2> custom) {
        this.custom = custom;
    }

    @Override
    public String toString() {
        return "FieldsV2{" +
            "standard=" + standard +
            ", custom=" + custom +
            '}';
    }
}
