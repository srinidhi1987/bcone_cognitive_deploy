package com.automationanywhere.cognitive.project.configuration;

import com.automationanywhere.cognitive.project.exception.NotFoundException;

import java.util.Map;

public interface ConfigurationReader {
    Map<String,String> getProperties() throws NotFoundException;
}
