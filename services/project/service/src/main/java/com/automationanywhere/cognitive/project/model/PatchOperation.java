package com.automationanywhere.cognitive.project.model;

/**
 * Created by Jemin.Shah on 1/27/2017.
 */
public enum PatchOperation
{
    add,
    replace
}
