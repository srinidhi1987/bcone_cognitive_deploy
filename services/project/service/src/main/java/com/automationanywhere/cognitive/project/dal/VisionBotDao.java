package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.VisionBot;
import java.util.List;


public interface VisionBotDao {

  public List<String[]> getVisionBots(List<String> visionBotDetailIds);
  public void batchInsert(List<VisionBot> visionBot);

}
