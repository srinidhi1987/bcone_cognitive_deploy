package com.automationanywhere.cognitive.project.model.dto;

public class ProjectExportDeclination {
    private String projectId;
    private String reason;

    public ProjectExportDeclination(String projectId, String reason) {
        this.projectId = projectId;
        this.reason = reason;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getReason() {
        return reason;
    }
}
