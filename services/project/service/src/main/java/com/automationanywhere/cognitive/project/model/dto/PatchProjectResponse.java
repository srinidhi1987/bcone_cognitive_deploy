package com.automationanywhere.cognitive.project.model.dto;

public class PatchProjectResponse {

  public final long versionId;

  public PatchProjectResponse(long versionId) {
    this.versionId = versionId;
  }
}
