package com.automationanywhere.cognitive.project.dal.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.project.dal.TrainingDataDao;
import com.automationanywhere.cognitive.project.model.dao.TrainingData;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

public class TrainingdataDaoImpl implements
    TrainingDataDao {

  private static final AALogger log = AALogger.create(TrainingdataDaoImpl.class);

  @Autowired
  @Qualifier("mlSessionFactory")
  private SessionFactory sessionFactory;

  @Autowired
  private DataTransformer dataTransformer;

  @Transactional
  @Override
  public List<String[]> getTrainingDatas(String projectId) {
    log.entry();

    String sqlQuery = "select * from trainingData where fieldid like :projectId";
    Query query = sessionFactory.getCurrentSession().createNativeQuery(sqlQuery);
    query.setParameter("projectId", projectId + "_%");
    @SuppressWarnings("unchecked")
    List<Object[]> resulSetData = query.getResultList();

    List<String[]> trainingDatas = dataTransformer
        .transformToListOfStringArray(resulSetData);

    log.exit();
    return trainingDatas;
  }

  @Override
  public void deleteAll() {
    sessionFactory.getCurrentSession().createQuery("delete TrainingData").executeUpdate();
  }

  @Override
  public void batchInsertTrainingData(List<TrainingData> trainingDataList) {
    Session session = sessionFactory.getCurrentSession();
    for (TrainingData trainingData : trainingDataList) {
      session.save(trainingData);
      int index = trainingDataList.indexOf(trainingData);
      if (index % 20 == 0) {
        session.flush();
        session.clear();
      }
    }
  }

  @Override
  public void delete(String projectId) {
    Session session = sessionFactory.getCurrentSession();
    Query query = session.createQuery(
        "delete from TrainingData where fieldId like :projectId");
    query.setParameter("projectId", projectId + "_%");
    query.executeUpdate();
    session.flush();
    session.clear();
  }

  @Override
  public List<String> getUniqueFields() {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("select distinct fieldId from TrainingData");
    @SuppressWarnings("unchecked")
    List<String> listOFileIds = query.getResultList();
    return listOFileIds;
  }

  @Override
  public List<String> getUniqueFields(String projectId) {
    Query query = sessionFactory.getCurrentSession()
        .createQuery("select distinct fieldId from TrainingData "
            + "where fieldId like :projectId");
    query.setParameter("projectId", projectId + "_%");
    @SuppressWarnings("unchecked")
    List<String> listOFileIds = query.getResultList();
    return listOFileIds;
  }
}
