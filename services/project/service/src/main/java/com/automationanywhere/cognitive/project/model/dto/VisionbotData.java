package com.automationanywhere.cognitive.project.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jemin.Shah on 3/4/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class VisionbotData
{
    private String id;
    private String name;
    private String organizationId;
    private String projectId;
    private String categoryId;
    private String environment;
    private String status;
    private String lockedUserId;
    private BotRunDetails botRunDetails;
    private BotRunDetails productionBotRunDetails;
    private BotRunDetails stagingBotRunDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLockedUserId() {
        return lockedUserId;
    }

    public void setLockedUserId(String lockedUserId) {
        this.lockedUserId = lockedUserId;
    }

    public BotRunDetails getBotRunDetails() {
        return botRunDetails;
    }

    public void setBotRunDetails(BotRunDetails botRunDetails) {
        this.botRunDetails = botRunDetails;
    }

    public BotRunDetails getProductionBotRunDetails() {
        return productionBotRunDetails;
    }

    public void setProductionBotRunDetails(BotRunDetails productionBotRunDetails) {
        this.productionBotRunDetails = productionBotRunDetails;
    }

    public BotRunDetails getStagingBotRunDetails() {
        return stagingBotRunDetails;
    }

    public void setStagingBotRunDetails(BotRunDetails stagingBotRunDetails) {
        this.stagingBotRunDetails = stagingBotRunDetails;
    }

    @Override
    public String toString() {
        return "VisionbotData{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", organizationId='" + organizationId + '\'' +
                ", projectId='" + projectId + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", environment='" + environment + '\'' +
                ", status='" + status + '\'' +
                ", lockedUserId='" + lockedUserId + '\'' +
                ", botRunDetails=" + botRunDetails +
                ", productionBotRunDetails=" + productionBotRunDetails +
                ", stagingBotRunDetails=" + stagingBotRunDetails +
                '}';
    }
}
