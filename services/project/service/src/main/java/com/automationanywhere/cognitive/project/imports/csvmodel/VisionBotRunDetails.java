package com.automationanywhere.cognitive.project.imports.csvmodel;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class VisionBotRunDetails {

  @CsvBindByPosition(position = 0)
  @CsvBindByName(column = "id")
  private String id;
  @CsvBindByPosition(position = 1)
  @CsvBindByName(column = "visionbotid")
  private String visionbotid;
  @CsvBindByPosition(position = 2)
  @CsvBindByName(column = "createdat")
  private String createdat;
  @CsvBindByPosition(position = 3)
  @CsvBindByName(column = "environment")
  private String environment;
  @CsvBindByPosition(position = 4)
  @CsvBindByName(column = "totalfiles")
  private String totalfiles;
  @CsvBindByPosition(position = 5)
  @CsvBindByName(column = "runstateid")
  private String runstateid;

  public VisionBotRunDetails() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getVisionbotid() {
    return visionbotid;
  }

  public void setVisionbotid(String visionbotid) {
    this.visionbotid = visionbotid;
  }

  public String getCreatedat() {
    return createdat;
  }

  public void setCreatedat(String createdat) {
    this.createdat = createdat;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getTotalfiles() {
    return totalfiles;
  }

  public void setTotalfiles(String totalfiles) {
    this.totalfiles = totalfiles;
  }

  public String getRunstateid() {
    return runstateid;
  }

  public void setRunstateid(String runstateid) {
    this.runstateid = runstateid;
  }
}
