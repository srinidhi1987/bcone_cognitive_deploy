package com.automationanywhere.cognitive.project.export;

import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import java.util.Map;

public interface DataExporter<T> {
    T exportData(String path, Map<Object, Object> data) throws ProjectExportException;
}
