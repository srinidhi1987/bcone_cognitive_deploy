package com.automationanywhere.cognitive.project.constants;

public enum ImportFilePatternConstant {

  CSV_FILE_WITH_SEQUENCE_NUMBER("+_\\d*.csv$"),
  CSV_FILE_WITH_NAME("+.csv$");

  private final String name;

  private ImportFilePatternConstant(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

}
