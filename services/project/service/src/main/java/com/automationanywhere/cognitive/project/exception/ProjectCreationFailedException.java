package com.automationanywhere.cognitive.project.exception;

public class ProjectCreationFailedException extends Exception {

    private static final long serialVersionUID = -4681369390335620638L;

    public ProjectCreationFailedException(String message)
    {
        super(message);
    }

    public ProjectCreationFailedException(String message, Throwable cause)
    {
        super(message, cause);
    }
}