package com.automationanywhere.cognitive.project.adapter.Impl;

import com.automationanywhere.cognitive.common.formatter.CategoryFormatter;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.project.ConfigurationProvider;
import com.automationanywhere.cognitive.project.adapter.FileManagerAdapter;
import com.automationanywhere.cognitive.project.exception.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.project.model.dto.FileCategory;
import com.automationanywhere.cognitive.project.model.dto.StandardResponse;
import com.automationanywhere.cognitive.project.model.dto.filemanager.FileCountDetail;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.cognitive.project.constants.HeaderConstants.CORRELATION_ID;

public class FileManagerAdapterImpl implements FileManagerAdapter
{
    private AALogger log = AALogger.create(getClass());

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;
    @Autowired
    private RestTemplateWrapper restTemplateWrapper;
    private String HEARTBEAT_URL = "/heartbeat";

    private String rootUrl;
    private final String fileCountDetailRelativeUrl = "/organizations/%s/files/counts";
    private final String fileCountDetailByProjectRelativeUrl = "/organizations/%s/projects/%s/files/counts";


    @Override
    public List<FileCategory> getCategories(String organizationId) {
        this.rootUrl = ConfigurationProvider.getFileManagerUrl();
        String url = rootUrl + String.format(fileCountDetailRelativeUrl, organizationId);

        return fetchCategoriesFromFileService(url);
    }

    @Override
    public List<FileCategory> getCategories(String organizationId, String projectId) {
        this.rootUrl = ConfigurationProvider.getFileManagerUrl();
        String url = rootUrl + String.format(fileCountDetailByProjectRelativeUrl, organizationId, projectId);

        return fetchCategoriesFromFileService(url);
    }

    private List<FileCategory> fetchCategoriesFromFileService(String url) {
        log.entry();

        FileCountDetail[] fileCountDetails = null;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
            fileCountDetails = objectMapper.convertValue(standardResponse.getBody().getData(), FileCountDetail[].class);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            // TODO: should we throw it? need to check complete flow to make sure it isnt bubble up to UI Layer.
            return null;
        }

        List<FileCategory> fileCategoryList = new ArrayList<>();

        for (FileCountDetail fileCountDetail : fileCountDetails) {
            FileCategory fileCategory = new FileCategory();
            fileCategory.setProjectId(fileCountDetail.getProjectId());
            fileCategory.setId(fileCountDetail.getCategoryId());
            fileCategory.setFileCount(fileCountDetail.getFileCount());
            fileCategory.setProductionFileDetails(fileCountDetail.getProductionFileCount());
            fileCategory.setStagingFileDetails(fileCountDetail.getStagingFileCount());
            fileCategory.setName(CategoryFormatter.format(fileCategory.getId()));

            fileCategoryList.add(fileCategory);
        }

        log.exit();
        return fileCategoryList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        log.entry();
        this.rootUrl = ConfigurationProvider.getFileManagerUrl();
        String heartBeatURL = rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse=null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL, String.class);
        if(standardResponse!=null && standardResponse.getStatusCode()!=HttpStatus.OK){
            throw new DependentServiceConnectionFailureException("Error while connecting to FileManager service");
        }
        log.exit();
        
    }
}
