package com.automationanywhere.cognitive.project.configuration.filebasedconfiguration;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.configuration.FileChangedEvent;

import java.io.File;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class FileWatcher implements Runnable {

    File file;
    private AtomicBoolean stop = new AtomicBoolean(false);
    FileChangedEvent fileChangedEvent;
    AALogger log = AALogger.create(this.getClass());

    public FileWatcher(String filepath) {
        file = new File(filepath);
    }

    public boolean isStopped() {
        return stop.get();
    }

    public void stopThread() {
        stop.set(true);
    }

    public FileChangedEvent getFileChangedEvent() {
        return fileChangedEvent;
    }

    public void setFileChangedEvent(FileChangedEvent fileChangedEvent) {
        this.fileChangedEvent = fileChangedEvent;
    }

    @Override
    public void run() {
        try (WatchService watcher = FileSystems.getDefault().newWatchService()) {
            Path path = file.toPath().getParent();
            path.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
            while (!isStopped()) {
                WatchKey key;
                try {
                    key = watcher.poll(25, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                    return;
                }
                if (key == null) {
                    Thread.yield();
                    continue;
                }

                if(!raiseEventWhenFileModified(key)) {
                    break; }

                Thread.yield();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private boolean raiseEventWhenFileModified(WatchKey key) {
        for (WatchEvent<?> event : key.pollEvents()) {
            WatchEvent.Kind<?> kind = event.kind();

            @SuppressWarnings("unchecked")
            WatchEvent<Path> ev = (WatchEvent<Path>) event;
            Path filename = ev.context();

            if (kind == java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY
                    && filename.toString().equals(file.getName())) {
                if (fileChangedEvent != null) {
                    fileChangedEvent.fileChanged(file.getPath());
                }
            }
        }

        return key.reset();
    }
}
