package com.automationanywhere.cognitive.project.model;

public enum TaskType {
    imports,
    exports
}
