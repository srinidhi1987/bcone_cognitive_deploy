package com.automationanywhere.cognitive.project.health.connections;

import org.springframework.beans.factory.annotation.Autowired;
import com.automationanywhere.cognitive.project.exception.UnSupportedConnectionTypeException;

public class HealthCheckConnectionFactory {
    @Autowired
    private DBCheckConnection dbCheckConnection;
    @Autowired
    private ServiceCheckConnection svcCheckConnection;
    @Autowired
    private MQCheckConnection mqCheckConnection;
    
	public HealthCheckConnection getConnection(ConnectionType connType){
		switch(connType){
			case DB : return dbCheckConnection;
			case SVC: return svcCheckConnection;
			case MQ: return mqCheckConnection;
			default: throw new UnSupportedConnectionTypeException(connType + "Not supported ");
		}
	}
}
