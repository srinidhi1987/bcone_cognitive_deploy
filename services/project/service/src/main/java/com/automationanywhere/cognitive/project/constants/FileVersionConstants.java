package com.automationanywhere.cognitive.project.constants;

public class FileVersionConstants {

  public static final String XPATH_EXPRESSION = "/Product";
  public static final String FILE_VERSION = "File version";
  public static final String PRODUCT_RELEASE_INFO_FILE = "ProductReleaseInfo.xml";
  public static final String META_DATA_FILE_NAME = "metadata.json";
  public static final String FILE_VERSION_SPLIT_REGEX = "\\.";
  public static final String FILE_WRITE_CHARACTER_SET = "UTF-8";
  public static final String FILE_VERSION_TAG_NAME = "Version";
  public static final String PRODUCT_DEFAULT_VERSION = "5.2.x";
}
