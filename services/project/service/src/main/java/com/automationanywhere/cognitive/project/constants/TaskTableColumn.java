package com.automationanywhere.cognitive.project.constants;

import java.util.HashMap;

/**
 * This enum class will mapped all the Task table column with hibernate entity class
 */
public enum TaskTableColumn {

    TASKID("taskId"),
    TASKTYPE("taskType"),
    STATUS("status"),
    DESCRIPTION("description"),
    CREATEDBY("userId"),
    STARTTIME("startTime"),
    ENDTIME("endTime");

    private String columnName;
    private static HashMap<String, String> columnMap = null;

    static {
        columnMap = new HashMap<String, String>();
        for (final TaskTableColumn taskTableColumn : TaskTableColumn.values()) {
            columnMap.put(taskTableColumn.toString(), taskTableColumn.getValue());
        }
    }

    TaskTableColumn(String columnName) {
        this.columnName = columnName;
    }

    public static String getValueData(String name) {
        Object data = columnMap.get(name.toUpperCase());
        return data.toString();
    }

    public String getValue() {
        return columnName;
    }

    public String getMappedValue() {
        return this.columnName;
    }
}
