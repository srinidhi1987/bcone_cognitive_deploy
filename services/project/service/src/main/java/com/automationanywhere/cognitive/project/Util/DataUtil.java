package com.automationanywhere.cognitive.project.Util;

import java.util.ArrayList;
import java.util.List;

public class DataUtil {

  /**
   *
   * Divide an array of data based on threshold and returns a list.
   *
   * @param data array of data
   * @param threshold threshold value.
   * @return
   */
  public static List<String[]> subDivide(String[] data, int threshold) {

    int numberOfArraysRequired = (int) Math.ceil((double) data.length / (double) threshold);
    int numberOfElementsInLastArray = data.length % threshold;

    List<String[]> subList = new ArrayList<>(numberOfArraysRequired);

    for (int i = 0; i < numberOfArraysRequired; i++) {
      String[] subArrayOfData =
          new String[(i + 1) == numberOfArraysRequired && numberOfElementsInLastArray != 0
              ? numberOfElementsInLastArray : threshold];

      System.arraycopy(data, (i * threshold), subArrayOfData, 0, subArrayOfData.length);

      subList.add(subArrayOfData);
    }
    return subList;
  }

}
