package com.automationanywhere.cognitive.project.export.impl;

import com.automationanywhere.cognitive.project.Util.DataUtil;
import com.automationanywhere.cognitive.project.exception.ProjectExportException;
import com.automationanywhere.cognitive.project.export.DataExportListener;
import com.automationanywhere.cognitive.project.export.DataExporter;
import com.automationanywhere.cognitive.project.export.ExportOperator;
import com.automationanywhere.cognitive.project.export.model.ProjectDataExportMetadata;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import javax.inject.Inject;


public class ProjectDataExportOperator implements ExportOperator {

  private static final ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();

  private final DataExporter projectDataExporter;

  @Inject
  public ProjectDataExportOperator(DataExporter projectDataExporter) {
    this.projectDataExporter = projectDataExporter;
  }

  @Override
  public void export(String dirPath, String[] projectIds,
      DataExportListener dataExportListener){
    ProjectDataExportTask projectDataExportTask = new ProjectDataExportTask(dirPath, projectIds,
        dataExportListener);
    forkJoinPool.execute(projectDataExportTask);

  }

  private class ProjectDataExportTask extends RecursiveTask<List<ProjectDataExportMetadata>> {


    private static final long serialVersionUID = -1811023875152789464L;	  

    private static final int PROJECT_NUMBER_THRESHOLD = 10;
    private final String dirPath;
    private final String[] projectIds;
    private DataExportListener dataExportListener;

    ProjectDataExportTask(String dirPath, String[] projectIds,
        DataExportListener dataExportListener) {
      this(dirPath, projectIds);
      this.dataExportListener = dataExportListener;
    }

    private ProjectDataExportTask(String dirPath, String[] projectIds) {
      this.dirPath = dirPath;
      this.projectIds = projectIds;
    }

    @Override
    public List<ProjectDataExportMetadata> compute() {
      try {
        List<ProjectDataExportMetadata> metadata;
        if (projectIds.length > PROJECT_NUMBER_THRESHOLD) {
          metadata = ForkJoinTask.invokeAll(createSubTasks()).stream().map(ForkJoinTask::join)
              .flatMap(List::stream).collect(Collectors.toList());
        } else {
          metadata = new ArrayList<>();
          for (String projectId : projectIds) {
            metadata.add(exportData(projectId));
          }
        }
        if (dataExportListener != null) {
          dataExportListener.onProjectDataExportCompleted(metadata);
        }
        return metadata;
      } catch (Exception e) {
        if (dataExportListener != null) {
          dataExportListener.onException(e);
        }
        throw e;
      }
    }

    private List<ProjectDataExportTask> createSubTasks() {

      List<String[]> lstOfArrayOfProjectIds = DataUtil
          .subDivide(projectIds, PROJECT_NUMBER_THRESHOLD);

      List<ProjectDataExportTask> subTasks = new ArrayList<>();

      lstOfArrayOfProjectIds.stream().forEach(
          subProjectIds -> subTasks.add(new ProjectDataExportTask(dirPath, subProjectIds)));

      return subTasks;
    }

    private ProjectDataExportMetadata exportData(String projectId) throws ProjectExportException {

      Map<Object, Object> exportParams = new HashMap<Object, Object>();
      exportParams.put("projectId", projectId);

      @SuppressWarnings("unchecked")
      ProjectDataExportMetadata projectDataExportMetadata = (ProjectDataExportMetadata) projectDataExporter
          .exportData(dirPath, exportParams);

      return projectDataExportMetadata;
    }
  }
}
