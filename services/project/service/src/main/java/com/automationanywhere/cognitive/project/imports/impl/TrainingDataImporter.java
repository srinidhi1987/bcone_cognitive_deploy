package com.automationanywhere.cognitive.project.imports.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.project.constants.DatabaseConstants;
import com.automationanywhere.cognitive.project.constants.TableConstants;
import com.automationanywhere.cognitive.project.dal.TrainingDataDao;
import com.automationanywhere.cognitive.project.exception.DuplicateProjectNameException;
import com.automationanywhere.cognitive.project.exception.ProjectImportException;
import com.automationanywhere.cognitive.project.export.FileReader;
import com.automationanywhere.cognitive.project.extension.DtoToDao;
import com.automationanywhere.cognitive.project.helper.FileHelper;
import com.automationanywhere.cognitive.project.helper.PathHelper;
import com.automationanywhere.cognitive.project.imports.AdvanceDataImporter;
import com.automationanywhere.cognitive.project.imports.TransitionResult;
import com.automationanywhere.cognitive.project.imports.csvmodel.trainingData;
import com.automationanywhere.cognitive.project.model.TaskOptions;
import com.automationanywhere.cognitive.project.model.dao.TrainingData;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class TrainingDataImporter extends AdvanceDataImporter<trainingData> {

  private static final AALogger LOGGER = AALogger.create(TrainingDataImporter.class);
  private static final String TRAINING_MODEL_FILE_REGEX = "[\\w-_]+.A.B$";

  private final FileHelper fileHelper;
  private final PathHelper pathHelper;
  private final TransitionResult transitionResult;
  private final TrainingDataDao trainingdataDao;
  private final ImportFilePatternConstructor importFilePatternConstructor;
  private AdvanceDataImporter dataImporter;

  @Autowired
  public TrainingDataImporter(FileReader csvFileReader, FileHelper fileHelper,
      PathHelper pathHelper,
      TransitionResult transitionResult, TrainingDataDao trainingdataDao,
      ImportFilePatternConstructor importFilePatternConstructor) {
    super(csvFileReader);
    this.fileHelper = fileHelper;
    this.pathHelper = pathHelper;
    this.transitionResult = transitionResult;
    this.trainingdataDao = trainingdataDao;
    this.importFilePatternConstructor = importFilePatternConstructor;
  }

  @Override
  @Transactional(value = DatabaseConstants.ML_TRANSACTION_MANAGER, propagation = Propagation.REQUIRED)
  public void importData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException, ParseException, DuplicateProjectNameException {
    LOGGER.entry();

    if (TaskOptions.CLEAR == taskOptions
        || TaskOptions.OVERWRITE == taskOptions) {
      //data prepared for import
      List<TrainingData> finalDataToImport = prepareData(unzippedFolderPath, taskOptions);

      deleteMLData(taskOptions, transitionResult.getProjectId());

      //data inserted in batch.
      if (!finalDataToImport.isEmpty()) {
        trainingdataDao.batchInsertTrainingData(finalDataToImport);
      }
    }

    //next importer get's called.
    if (dataImporter != null) {
      dataImporter.importData(unzippedFolderPath, taskOptions);
    }

    LOGGER.exit();
  }

  @Override
  public void setNext(AdvanceDataImporter dataImporter) {
    this.dataImporter = dataImporter;
  }

  private List<TrainingData> getDataFromFiles(Path unzippedFolderPath)
      throws IOException, ProjectImportException {
    LOGGER.entry();
    List<TrainingData> dataToImport = new ArrayList<>();
    Path csvFolderPath = Paths
        .get(unzippedFolderPath.toString(), transitionResult.getProjectId());
    String csvRegexPattern = importFilePatternConstructor
        .getRegexPatternWithOr(TableConstants.TRAININGDATA);
    List<Path> csvPathsOfTrainingdata = fileHelper
        .getAllFilesPaths(csvFolderPath, csvRegexPattern);

    dataToImport
        .addAll(
            DtoToDao.toTrainingData(getDataFromCsv(csvPathsOfTrainingdata, trainingData.class)));
    LOGGER.exit();

    return dataToImport;
  }

  private List<TrainingData> prepareData(Path unzippedFolderPath,
      com.automationanywhere.cognitive.project.model.TaskOptions taskOptions)
      throws IOException, ProjectImportException {
    List<TrainingData> finalDataToImport = new ArrayList<>();

    String projectId = transitionResult.getProjectId();
    List<TrainingData> dataToImport = getDataFromFiles(unzippedFolderPath);
    finalDataToImport = filterByProjectId(projectId, dataToImport);

    return finalDataToImport;
  }

  private void deleteMLData(TaskOptions taskOptions, String projectId) throws IOException {

    if (TaskOptions.CLEAR == taskOptions) {
      List<String> fieldList = trainingdataDao.getUniqueFields();
      deleteModels(fieldList);
      trainingdataDao.deleteAll();
    }

    if (TaskOptions.OVERWRITE == taskOptions) {
      List<String> fieldList = trainingdataDao.getUniqueFields(projectId);
      deleteModels(fieldList);
      trainingdataDao.delete(projectId);
    }
  }

  private void deleteModels(List<String> fieldList) throws IOException {
    for (String field : fieldList) {
      List<Path> modelFiles = getAllModelFiles();
      Optional<Path> modelFile = modelFiles.stream()
          .filter(path -> path.toString().contains(field)).findFirst();
      if (modelFile.isPresent()) {
        try {
          fileHelper.deletFile(modelFile.get());
          LOGGER.info("Model deleted - " + modelFile.get().toString());
        } catch (IOException ex) {
          LOGGER.error("Unable to delete model - " + modelFile.get().toString());
          LOGGER.error(ex);
        }
      }
    }
  }

  private List<Path> getAllModelFiles() throws IOException {
    Path systemFolder = Paths.get(pathHelper.getSystemFolder());
    return fileHelper.getAllFilesPaths(systemFolder, TRAINING_MODEL_FILE_REGEX);
  }

  private List<TrainingData> filterByProjectId(String projectId,
      List<TrainingData> trainingDataList) {
    return trainingDataList.stream()
        .filter(trainingData -> trainingData.getFieldId().trim().startsWith(projectId + "_"))
        .collect(Collectors.toList());
  }
}
