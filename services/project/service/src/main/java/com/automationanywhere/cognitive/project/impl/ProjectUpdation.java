package com.automationanywhere.cognitive.project.impl;

import com.automationanywhere.cognitive.project.dal.ProjectDetailDao;
import com.automationanywhere.cognitive.project.exception.InvalidInputException;
import com.automationanywhere.cognitive.project.exception.NotFoundException;
import com.automationanywhere.cognitive.project.model.dao.ProjectDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


class ProjectUpdation {

    @Autowired
    private ProjectDetailDao projectDetailDao;

    @Transactional
    public void update(ProjectDetail projectDetail) throws NotFoundException {
        projectDetailDao.updateProjectDetail(projectDetail);
    }

    @Transactional
    public List<ProjectDetail> getProjectDetails(Map<String, String> restrictions,
                                                 Map<String, String[]> queryParameters) throws InvalidInputException {
        return projectDetailDao.getProjectDetails(restrictions, queryParameters);
    }
}
