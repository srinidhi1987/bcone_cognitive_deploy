package com.automationanywhere.cognitive.project.imports;

import com.automationanywhere.cognitive.project.constants.TableConstants;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface DataImporter {

  Map<String, Integer> importProjectData(
      Map<TableConstants, List<Path>> dataFilePathMap);

  Map<String, Integer> importFileManagerData(
      Map<TableConstants, List<Path>> dataFilePathMap);

  Map<String, Integer> importVisionBotData(
      Map<TableConstants, List<Path>> dataFilePathMap);

  Map<String, Integer> importMlData(
      Map<TableConstants, List<Path>> dataFilePathMap);

  Map<String, Integer> importClassifierData(
      Map<TableConstants, List<Path>> dataFilePathMap);
}
