package com.automationanywhere.cognitive.project.model.dto;

/**
 * Created by Jemin.Shah on 29-11-2016.
 */
public class FileField {

    private String id;
    private String name;
    private boolean found;
    private boolean required;
    private boolean active;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "FileField{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", found=" + found +
                ", required=" + required +
                ", active=" + active +
                '}';
    }
}
