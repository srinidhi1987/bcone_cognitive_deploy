package com.automationanywhere.cognitive.project.dal;

import com.automationanywhere.cognitive.project.model.dao.ClassificationReport;
import java.util.List;
import org.hibernate.SessionFactory;


public interface ClassificationReportDao {
  public List<String[]> getClassificationReports(List<String> documentIds);
  public void batchInsertClassificationReport(List<ClassificationReport> classificationReports);
  public void delete(List<String> documentIds);
  public SessionFactory getSessionFactory();
}