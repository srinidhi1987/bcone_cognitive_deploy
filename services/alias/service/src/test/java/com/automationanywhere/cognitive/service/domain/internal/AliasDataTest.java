package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.util.BaseUnitTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Set;

import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.LANG_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.OBJECT_MAPPER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIASES_ENG_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_ENG_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Test cases for the Alias Data Transfer Object.
 *
 * @author Erik K. Worth
 */
public class AliasDataTest extends BaseUnitTest {

  @DataProvider(name = "dataConstructWithNulls")
  public Object[][] dataConstructWithNulls() {
    return new Object[][] {
            {"names", null, LANG_ENGLISH},
            {"language", TEST_FIELD_1_ALIASES_ENG_1, null}
    };
  }

  @Test(dataProvider = "dataConstructWithNulls")
  public void construct_withNullsAndConfirmException(
          final String argName,
          final Set<String> names,
          final String language) {
    try {
      // When
      new AliasData(names, language);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(argName + " must not be null");
    }
  }

  @SuppressWarnings("EqualsWithItself")
  @Test
  public void equals_withSameObjectAndConfirmTrue() {
    // When
    final AliasData aliasData =
            new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
    // Then
    assertThat(aliasData.equals(aliasData)).isTrue();
  }

  @SuppressWarnings("EqualsBetweenInconvertibleTypes")
  @Test
  public void equals_withWrongTypeAndConfirmFalse() {
    // When
    final AliasData aliasData =
            new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
    // Then
    assertThat(aliasData.equals("Not Alias Data")).isFalse();
  }

  @Test
  public void equals_withSameDataAndConfirmTrue() {
    // When
    final AliasData aliasData1 =
            new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
    final AliasData aliasData2 =
            new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
    // Then
    assertThat(aliasData1.equals(aliasData2)).isTrue();
    assertThat(aliasData1.toString()).isEqualTo(aliasData2.toString());
    assertThat(aliasData1.hashCode()).isEqualTo(aliasData2.hashCode());
  }

  @Test
  public void get_fieldsAndConfirmEqualsConstructedValues() {
    // When
    final AliasData aliasData =
            new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
    // Then
    assertThat(aliasData.getNames()).containsExactly(TEST_FIELD_1_ALIAS_ENG_1);
    assertThat(aliasData.getLanguage()).isEqualTo(LANG_ENGLISH);
  }

  @Test
  public void json_externalizeInternalizeAndConfirmEqual() throws Exception {
    // Given
    final AliasData aliasData =
            new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
    // When
    final String json = OBJECT_MAPPER.writeValueAsString(aliasData);
    final AliasData internalizedAlias =
            OBJECT_MAPPER.readValue(json, AliasData.class);
    // Then
    assertThat(internalizedAlias).isEqualTo(aliasData);
  }
}
