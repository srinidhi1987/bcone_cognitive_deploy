package com.automationanywhere.cognitive.service.shareabledomain.internal;

import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import org.testng.annotations.Test;

import static com.automationanywhere.cognitive.service.shareabledomain.internal.DomainCodec.DECOMPRESSION_ERROR_MSG;
import static com.automationanywhere.cognitive.service.shareabledomain.internal.DomainCodec.MISSING_SEPARATOR_MSG;
import static com.automationanywhere.cognitive.service.shareabledomain.internal.DomainCodec.NOT_GZIPPED_MSG;
import static com.automationanywhere.cognitive.service.shareabledomain.internal.ShareableDomainFixture.TEST_DOMAIN_JSON;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Unit tests for the class that encodes and decodes the Domain metadata into
 * a shareable representation.
 *
 * @author Erik K. Worth
 */
public class DomainCodecTest {


  @Test
  public void base64encodeDecode_andConfirmTheSame() {
    // Given
    final String encoded = DomainCodec.base64Encode(
            TEST_DOMAIN_JSON.getBytes(UTF_8));
    // When
    final String decoded = new String(DomainCodec.base64Decode(encoded), UTF_8);
    // Then
    assertThat(decoded).isEqualTo(TEST_DOMAIN_JSON);
  }

  @Test
  public void zipUnzip_andConfirmTheSame() {
    // Given
    final byte[] compressed = DomainCodec.zip(TEST_DOMAIN_JSON.getBytes(UTF_8));
    // When
    final String unexploded = new String(DomainCodec.unzip(compressed), UTF_8);
    // Then
    assertThat(unexploded).isEqualTo(TEST_DOMAIN_JSON);
  }

  @Test
  public void unzip_withContentThatWasNotZippedAndConfirmException() {
    try {
      // When
      DomainCodec.unzip("Not compressed".getBytes(UTF_8));
      fail("Expected a DomainCodecException");
    } catch (DomainCodecException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(NOT_GZIPPED_MSG);
    }
  }

  @Test
  public void unzip_withBadContentAndConfirmException() {
    // Given
    final byte[] badContent = DomainCodec.zip(TEST_DOMAIN_JSON.getBytes(UTF_8));
    for (int i = badContent.length - 3; i < badContent.length; i++) {
      // Corrupt the content
      badContent[i] = -1;
    }
    try {
      // When
      DomainCodec.unzip(badContent);
      fail("Expected a DomainCodecException");
    } catch (DomainCodecException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(DECOMPRESSION_ERROR_MSG);
    }
  }

  @Test
  public void encryptDecrypt_andConfirmTheSame() {
    // Given
    final byte[] encrypted =
            DomainCodec.encrypt(TEST_DOMAIN_JSON.getBytes(UTF_8));
    // When
    final String clear = new String(
            DomainCodec.decrypt(DomainCodec.DEFAULT_KEY_ID, encrypted),
            UTF_8);
    // Then
    assertThat(clear).isEqualTo(TEST_DOMAIN_JSON);
  }

  @Test(expectedExceptions = DomainCodecException.class)
  public void domainEncodeDecode_withBadKeyIdAndConfirmException() {
      // When
      DomainCodec.decrypt("BadKeyId", TEST_DOMAIN_JSON.getBytes(UTF_8));
  }

  @Test
  public void domainEncodeDecode_andConfirmTheSame() {
    // Given
    final String base64 = DomainCodec.encodeDomain(TEST_DOMAIN_JSON);

    // When
    final String decodedJson = DomainCodec.decodeDomain(base64);

    // Then
    assertThat(decodedJson).isEqualTo(TEST_DOMAIN_JSON);
  }

  @Test
  public void domainEncodeDecode_withoutDotSeparatedDelimiter() {
    try {
      // When
      DomainCodec.decodeDomain("No dot delimiter");
      fail("Expected a DomainCodecException");
    } catch (DomainCodecException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(MISSING_SEPARATOR_MSG);
    }
  }
}
