package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.DomainFieldType;
import com.automationanywhere.cognitive.util.BaseUnitTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Set;

import static com.automationanywhere.cognitive.service.domain.DomainFieldType.FORM_FIELD;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.CHECKED_BY_DEFAULT;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.FORMAT_TEXT;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.OBJECT_MAPPER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_NAME;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIASES_ONLY_ENG;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_ENG;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Unit tests for the Field Data Transfer Object.
 *
 * @author Erik K. Worth
 */
public class FieldDataTest extends BaseUnitTest {

  @DataProvider(name = "dataConstructWithNulls")
  public Object[][] dataConstructWithNulls() {
    return new Object[][] {
            {"name", null, FORM_FIELD, FORMAT_TEXT, TEST_FIELD_1_ALIASES_ONLY_ENG},
            {"type", TEST_FIELD_1_NAME, null, FORMAT_TEXT, TEST_FIELD_1_ALIASES_ONLY_ENG},
            {"format", TEST_FIELD_1_NAME, FORM_FIELD, null, TEST_FIELD_1_ALIASES_ONLY_ENG},
            {"aliases", TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT, null},
    };
  }

  @Test(dataProvider = "dataConstructWithNulls")
  public void construct_withNullsAndConfirmException(
          final String argName,
          final String name,
          final DomainFieldType type,
          final String format,
          final Set<AliasData> aliases) {
    // When
    try {
      new DomainFieldData(name, type, format, CHECKED_BY_DEFAULT, aliases);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(argName + " must not be null");
    }
  }

  @SuppressWarnings("EqualsWithItself")
  @Test
  public void equals_withSameObjectAndConfirmTrue() {
    // When
    final DomainFieldData fieldData = new DomainFieldData(
            TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
            CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
    // Then
    assertThat(fieldData.equals(fieldData)).isTrue();
  }

  @SuppressWarnings("EqualsBetweenInconvertibleTypes")
  @Test
  public void equals_withWrongTypeAndConfirmFalse() {
    // When
    final DomainFieldData fieldData = new DomainFieldData(
            TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
            CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
    // Then
    assertThat(fieldData.equals("Not Field Data")).isFalse();
  }

  @Test
  public void equals_withSameDataAndConfirmTrue() {
    // When
    final DomainFieldData fieldData1 = new DomainFieldData(
            TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
            CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
    final DomainFieldData fieldData2 = new DomainFieldData(
            TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
            CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
    // Then
    assertThat(fieldData1.equals(fieldData2)).isTrue();
    assertThat(fieldData1.toString()).isEqualTo(fieldData2.toString());
    assertThat(fieldData1.hashCode()).isEqualTo(fieldData2.hashCode());
  }

  @Test
  public void get_fieldsAndConfirmEqualsConstructedValues() {
    // When
    final DomainFieldData fieldData = new DomainFieldData(
            TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
            CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
    // Then
    assertThat(fieldData.getName()).isEqualTo(TEST_FIELD_1_NAME);
    assertThat(fieldData.getType()).isEqualTo(FORM_FIELD);
    assertThat(fieldData.getFormat()).isEqualTo(FORMAT_TEXT);
    assertThat(fieldData.isDefault()).isEqualTo(CHECKED_BY_DEFAULT);
    assertThat(fieldData.getAliases()).containsExactly(TEST_FIELD_1_ALIAS_ENG);
  }

  @Test
  public void json_externalizeInternalizeAndConfirmEqual() throws Exception {
    // Given
    final DomainFieldData fieldData = new DomainFieldData(
            TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
            CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
    // When
    final String json = OBJECT_MAPPER.writeValueAsString(fieldData);
    final DomainFieldData internalizedDomainField =
            OBJECT_MAPPER.readValue(json, DomainFieldData.class);
    // Then
    assertThat(internalizedDomainField).isEqualTo(fieldData);
  }
}
