package com.automationanywhere.cognitive.util;

import com.automationanywhere.cognitive.common.logger.AALogger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.lang.reflect.Method;

/**
 * Base class for unit tests that provides shared functionality.
 *
 * @author Erik K. Worth
 */
public class BaseUnitTest {
  private static final AALogger LOGGER = AALogger.create(BaseUnitTest.class);

  @BeforeMethod
  public void handleTestMethodName(final Method method) {
    LOGGER.debug("Starting {}", method.getName());
  }
}
