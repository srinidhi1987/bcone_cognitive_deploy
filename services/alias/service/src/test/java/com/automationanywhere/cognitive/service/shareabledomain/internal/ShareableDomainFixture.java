package com.automationanywhere.cognitive.service.shareabledomain.internal;

/**
 * Test fixture for Shareable Domains.
 *
 * @author Erik K. Worth
 */
public class ShareableDomainFixture {

  static final String TEST_DOMAIN_NAME = "TestDomain";
  static final String TEST_DOMAIN_METADATA = "TestDomainMetadata";
  static final int TEST_VERSION = 1;
  static final String TEST_DOMAIN_JSON = "{\"name\":\"Test Domain\"}";
}
