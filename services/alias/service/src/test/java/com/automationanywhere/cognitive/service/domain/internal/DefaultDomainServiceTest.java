package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DuplicateDataException;
import com.automationanywhere.cognitive.alias.models.entitymodel.Aliases;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldDataType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;
import com.automationanywhere.cognitive.service.domain.AliasInfo;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DomainFieldInfo;
import com.automationanywhere.cognitive.service.domain.DomainFieldType;
import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.automationanywhere.cognitive.service.domain.DomainLanguagesInfo;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.NoSuchDomainException;
import com.automationanywhere.cognitive.util.BaseUnitTest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.automationanywhere.cognitive.service.domain.DomainFieldType.FORM_FIELD;
import static com.automationanywhere.cognitive.service.domain.internal.DomainDataMapper.FORM_FIELD_NAME;
import static com.automationanywhere.cognitive.service.domain.internal.DomainDataMapper.TABLE_COLUMN_FIELD_NAME;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.CHECKED_BY_DEFAULT;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ENGLISH_AND_SPANISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.FIELD_TYPE_NUMBER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.FORMAT_DATE;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.FORMAT_NUMBER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.FORMAT_TEXT;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.LANG_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.LANG_ENG_ID;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.LANG_SPANISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.NOT_CHECKED_BY_DEFAULT;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ONLY_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TABLE_FIELD_TYPE;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOC_TYPE_RECORD;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOMAIN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOMAIN_ID;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIASES_ONLY_ENG;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_ENG;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_ENG_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_SPN_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ENG_AND_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_NAME;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ALIAS_ENG;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ALIAS_ENG_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ALIAS_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ALIAS_SPN_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ENG_AND_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_NAME;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_RECORD_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_SUPPORTED_DOMAINS;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_SUPPORTED_TWO_LANGUAGES;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.makeAliasRecords;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.makeDocTypeRecord;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.makeFieldDataType;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.makeFieldRecord;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the Default Domain Service.
 *
 * @author Erik K. Worth
 */
public class DefaultDomainServiceTest extends BaseUnitTest {

  private static final String TEST_DOMAIN_2 = TEST_DOMAIN + "2";
  private static final int FIELD_TYPE_FORM_ID = 1;
  private static final int FIELD_TYPE_TABLE_ID = 2;

  private static final int FIELD_DATA_TYPE_TEXT_ID = 1;
  private static final int FIELD_DATA_TYPE_NUMBER_ID = 2;
  private static final int FIELD_DATA_TYPE_DATE_ID = 3;

  /**
   * The service that retrieves data from the AliasData database.
   */
  @Mock
  private AliasService aliasService;

  /**
   * The component able to validate domains
   */
  @Mock
  private DomainValidator domainValidator;

  @Captor
  private ArgumentCaptor<DocType> docTypeCaptor;

  /**
   * The service being tested here.
   */
  private DefaultDomainService defaultDomainService;

  @BeforeMethod(alwaysRun = true)
  public void setup() {
    MockitoAnnotations.initMocks(this);
    this.defaultDomainService =
            new DefaultDomainService(aliasService, domainValidator);
  }

  @Test
  public void getDomain_shouldRetrieveExpectedDataAndConfirm() throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    when(aliasService.getDocTypeDetails(TEST_DOMAIN_ID))
            .thenReturn(TEST_DOC_TYPE_RECORD);

    // WHEN
    final DomainInfo domain = defaultDomainService.getDomain(TEST_DOMAIN);

    // THEN
    assertThat(domain.getName()).isEqualTo(TEST_DOMAIN);
    assertThat(domain.getLanguages())
            .containsExactly(LANG_ENGLISH, LANG_SPANISH);
    final Iterator<DomainFieldInfo> fields = domain.getFields().iterator();
    checkDomainFieldInfo(
            fields,
            TEST_FIELD_1_NAME,
            FORM_FIELD,
            FORMAT_TEXT,
            CHECKED_BY_DEFAULT,
            TEST_FIELD_1_ALIAS_ENG, TEST_FIELD_1_ALIAS_SPN);
    checkDomainFieldInfo(
            fields,
            TEST_FIELD_2_NAME,
            DomainFieldType.TABLE_COLUMN_FIELD,
            FORMAT_NUMBER,
            NOT_CHECKED_BY_DEFAULT,
            TEST_FIELD_2_ALIAS_ENG, TEST_FIELD_2_ALIAS_SPN);
  }

  @Test(expectedExceptions = DomainException.class)
  public void getDomain_withInvalidFieldTypeAndConfirmException() throws Exception {
    // GIVEN
    final FieldType badFieldType = new FieldType();
    badFieldType.setName("WrongFieldType");
    final Fields fieldWithWrongFieldType = makeFieldRecord(
            TEST_FIELD_2_NAME,
            badFieldType,
            FIELD_TYPE_NUMBER,
            NOT_CHECKED_BY_DEFAULT,
            new ImmutableList.Builder<Aliases>()
                    .addAll(makeAliasRecords(LANG_ENG_ID, TEST_FIELD_2_ALIAS_ENG_1))
                    .build()
    );
    final DocType docWithBadField = makeDocTypeRecord(
            TEST_DOMAIN,
            TEST_FIELD_RECORD_1,
            fieldWithWrongFieldType);
    mockSuccessfulRepositoryRequests();
    when(aliasService.getDocTypeDetails(TEST_DOMAIN_ID))
            .thenReturn(docWithBadField);

    // WHEN
    defaultDomainService.getDomain(TEST_DOMAIN);
  }

  @Test(expectedExceptions = DomainException.class)
  public void getDomain_withExceptionGettingDocIdsAndConfirmException()
          throws Exception {
    // GIVEN
    when(aliasService.getDocTypeNamesWithIds())
            .thenThrow(new DataAccessLayerException("Intentional"));

    // WHEN
    defaultDomainService.getDomain(TEST_DOMAIN);
  }

  @Test(expectedExceptions = NoSuchDomainException.class)
  public void getDomain_withUnknownDomainNameAndConfirmException()
          throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();

    // WHEN
    defaultDomainService.getDomain("Unknown Domain");
  }

  @Test(expectedExceptions = DomainException.class)
  public void getDomain_withExceptionOnRetrievalAndConfirmException()
          throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    when(aliasService.getDocTypeDetails(TEST_DOMAIN_ID))
            .thenThrow(new DataAccessLayerException("Intentional"));

    // WHEN
    defaultDomainService.getDomain(TEST_DOMAIN);
  }

  @Test(expectedExceptions = NoSuchDomainException.class)
  public void getDomain_withNoResultAndConfirmException()
          throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    when(aliasService.getDocTypeDetails(TEST_DOMAIN_ID)).thenReturn(null);

    // WHEN
    defaultDomainService.getDomain(TEST_DOMAIN);
  }

  @Test
  public void getAllDomainsWithLanguages_withNoErrorsAndConfirmResult()
          throws Exception {
    // GIVEN
    final Map<String, Set<String>> languagesByDomain =
            ImmutableMap.of(
                    TEST_DOMAIN, ENGLISH_AND_SPANISH,
                    TEST_DOMAIN_2, ONLY_ENGLISH);
    when(aliasService.getLanguagesForEachDomain()).thenReturn(languagesByDomain);

    // WHEN
    final List<DomainLanguagesInfo> result =
            defaultDomainService.getAllDomainsWithLanguages();

    // THEN
    final Iterator<DomainLanguagesInfo> domainLanguagesInfoIterator =
            result.iterator();
    checkDomainLanguagesInfo(domainLanguagesInfoIterator,
            TEST_DOMAIN, LANG_ENGLISH, LANG_SPANISH);
    checkDomainLanguagesInfo(domainLanguagesInfoIterator,
            TEST_DOMAIN_2, LANG_ENGLISH);
  }

  @Test(expectedExceptions = DomainException.class)
  public void getAllDomainsWithLanguages_withErrorAndConfirmException()
          throws Exception {
    // GIVEN
    when(aliasService.getLanguagesForEachDomain())
            .thenThrow(new DataAccessLayerException("Intentional"));

    // WHEN
    defaultDomainService.getAllDomainsWithLanguages();
  }

  @Test
  public void import_withValidDomainAndConfirmImportedDocType() throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doNothing().when(domainValidator).checkValid(domainData);

    // WHEN
    defaultDomainService.importDomain(domainData);

    // THEN
    verify(aliasService).bulkInsertDocTypeFieldsAndAliases(
            docTypeCaptor.capture());
    final DocType docType = docTypeCaptor.getValue();
    assertThat(docType).isNotNull();
    assertThat(docType.getName()).isEqualTo(TEST_DOMAIN);
    final Iterator<Fields> fieldsList = docType.getFieldList().iterator();
    checkFields(
            fieldsList,
            TEST_FIELD_1_NAME,
            FIELD_TYPE_FORM_ID,
            FIELD_DATA_TYPE_TEXT_ID,
            CHECKED_BY_DEFAULT,
            TEST_FIELD_1_ALIAS_ENG_1,
            TEST_FIELD_1_ALIAS_SPN_1);
    checkFields(
            fieldsList,
            TEST_FIELD_2_NAME,
            FIELD_TYPE_TABLE_ID,
            FIELD_DATA_TYPE_NUMBER_ID,
            NOT_CHECKED_BY_DEFAULT,
            TEST_FIELD_2_ALIAS_ENG_1,
            TEST_FIELD_2_ALIAS_SPN_1);
  }

  @Test(expectedExceptions = DomainException.class)
  public void import_withUnsupportedFieldDataTypeConfirmException()
          throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ONLY_ENGLISH,
            Collections.singletonList(new DomainFieldData(
                    TEST_FIELD_1_NAME,
                    FORM_FIELD,
                    "UnsupportedDataType",
                    CHECKED_BY_DEFAULT,
                    TEST_FIELD_1_ALIASES_ONLY_ENG)));
    doNothing().when(domainValidator).checkValid(domainData);

    // WHEN
    defaultDomainService.importDomain(domainData);
  }

  @Test
  public void import_withInvalidDomainAndConfirmException() throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doThrow(new InvalidDomainException("Intentional"))
            .when(domainValidator).checkValid(domainData);

    // WHEN
    Throwable throwable = catchThrowable(() -> defaultDomainService.importDomain(domainData));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class);
  }

  @Test(expectedExceptions = DomainException.class)
  public void import_withErrorGettingLanguagesAndConfirmException()
          throws Exception {
    // GIVEN
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doNothing().when(domainValidator).checkValid(domainData);
    when(aliasService.getAllLanguages())
            .thenThrow(new DataAccessLayerException("Intentional"));

    // WHEN
    defaultDomainService.importDomain(domainData);
  }

  @Test(expectedExceptions = DomainException.class)
  public void import_withErrorGettingFieldTypesAndConfirmException()
          throws Exception {
    // GIVEN
    when(aliasService.getAllLanguages())
            .thenReturn(TEST_SUPPORTED_TWO_LANGUAGES);
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doNothing().when(domainValidator).checkValid(domainData);
    when(aliasService.getFieldTypeNamesWithIds())
            .thenThrow(new DataAccessLayerException("Intentional"));

    // WHEN
    defaultDomainService.importDomain(domainData);
  }

  @Test(expectedExceptions = DomainException.class)
  public void import_withErrorGettingFieldDataTypesAndConfirmException()
          throws Exception {
    // GIVEN
    when(aliasService.getAllLanguages())
            .thenReturn(TEST_SUPPORTED_TWO_LANGUAGES);
    when(aliasService.getFieldTypeNamesWithIds())
            .thenReturn(ImmutableMap.of(
                    FORM_FIELD_NAME, FIELD_TYPE_FORM_ID,
                    TABLE_COLUMN_FIELD_NAME, FIELD_TYPE_TABLE_ID));
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doNothing().when(domainValidator).checkValid(domainData);
    when(aliasService.getFieldDataTypeNamesWithIds())
            .thenThrow(new DataAccessLayerException("Intentional"));

    // WHEN
    defaultDomainService.importDomain(domainData);
  }

  @Test(expectedExceptions = DuplicateDomainException.class)
  public void import_withDuplicateDomainOnImportAndConfirmException()
          throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doNothing().when(domainValidator).checkValid(domainData);
    doThrow(new DuplicateDataException("Intentional"))
            .when(aliasService)
            .bulkInsertDocTypeFieldsAndAliases(docTypeCaptor.capture());

    // WHEN
    defaultDomainService.importDomain(domainData);
  }

  @Test(expectedExceptions = DomainException.class)
  public void import_withExceptionOnImportAndConfirmException()
          throws Exception {
    // GIVEN
    mockSuccessfulRepositoryRequests();
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    doNothing().when(domainValidator).checkValid(domainData);
    doThrow(new DataAccessLayerException("Intentional"))
            .when(aliasService)
            .bulkInsertDocTypeFieldsAndAliases(docTypeCaptor.capture());

    // WHEN
    defaultDomainService.importDomain(domainData);
  }

  private void mockSuccessfulRepositoryRequests() throws DataAccessLayerException {
    when(aliasService.getDocTypeNamesWithIds())
            .thenReturn(TEST_SUPPORTED_DOMAINS);
    when(aliasService.getAllLanguages())
            .thenReturn(TEST_SUPPORTED_TWO_LANGUAGES);
    when(aliasService.getFieldTypeNamesWithIds())
            .thenReturn(ImmutableMap.of(
                    FORM_FIELD_NAME, FIELD_TYPE_FORM_ID,
                    TABLE_COLUMN_FIELD_NAME, FIELD_TYPE_TABLE_ID));
    when(aliasService.getFieldDataTypeNamesWithIds())
            .thenReturn(ImmutableMap.of(
                    FORMAT_TEXT, FIELD_DATA_TYPE_TEXT_ID,
                    FORMAT_NUMBER, FIELD_DATA_TYPE_NUMBER_ID,
                    FORMAT_DATE, FIELD_DATA_TYPE_DATE_ID));
  }

  private static void checkDomainLanguagesInfo(
          final Iterator<DomainLanguagesInfo> domainLanguagesInfoIterator,
          final String expectedDomainName,
          final String... expectedLanguages) {
    assertThat(domainLanguagesInfoIterator.hasNext()).isTrue();
    final DomainLanguagesInfo domainLanguagesInfo =
            domainLanguagesInfoIterator.next();
    assertThat(domainLanguagesInfo.getName()).isEqualTo(expectedDomainName);
    assertThat(domainLanguagesInfo.getLanguages())
            .containsExactly(expectedLanguages);
  }

  private static void checkDomainFieldInfo(
          final Iterator<DomainFieldInfo> fields,
          final String expectedFieldName,
          final DomainFieldType expectedFieldType,
          final String expectedFieldFormat,
          final boolean expectedCheckedByDefault,
          final AliasInfo... expectedAliases) {
    assertThat(fields.hasNext()).isTrue();
    DomainFieldInfo field = fields.next();
    assertThat(field.getName()).isEqualTo(expectedFieldName);
    assertThat(field.getType()).isEqualTo(expectedFieldType);
    assertThat(field.getFormat()).isEqualTo(expectedFieldFormat);
    assertThat(field.isDefault()).isEqualTo(expectedCheckedByDefault);
    assertThat(field.getAliases()).containsExactly(expectedAliases);
  }

  private static void checkFields(
          final Iterator<Fields> fields,
          final String expectedName,
          final int expectedFieldType,
          final int expectedFieldDataType,
          final boolean expectedCheckedByDefault,
          final String... expectedAliasNames) {
    assertThat(fields.hasNext()).isTrue();
    final Fields field = fields.next();
    assertThat(field.getName()).isEqualTo(expectedName);
    assertThat(field.getFieldType().getId()).isEqualTo(expectedFieldType);
    assertThat(field.getFieldDataType().getId())
            .isEqualTo(expectedFieldDataType);
    assertThat(field.getIsDefaultSelected())
            .isEqualTo(expectedCheckedByDefault);
    assertThat(field.getAliasesList().stream().map(Aliases::getAlias))
            .containsExactly(expectedAliasNames);
  }
}
