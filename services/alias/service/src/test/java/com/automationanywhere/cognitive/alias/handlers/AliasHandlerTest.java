package com.automationanywhere.cognitive.alias.handlers;

import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ENGLISH_AND_SPANISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOMAIN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ENG_AND_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ENG_AND_SPN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.alias.config.AliasConfig;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.alias.handlers.contract.AliasHandler;
import com.automationanywhere.cognitive.alias.handlers.impl.AliasHandlerImpl;
import com.automationanywhere.cognitive.alias.models.entitymodel.Aliases;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldDataType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.alias.models.entitymodel.Language;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;
import com.automationanywhere.cognitive.service.domain.DomainLanguagesInfo;
import com.automationanywhere.cognitive.service.domain.DomainService;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.internal.DomainData;
import com.automationanywhere.cognitive.service.domain.internal.DomainLanguagesData;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainInfo;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainService;
import com.automationanywhere.cognitive.service.shareabledomain.internal.ShareableDomain;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import spark.Request;
import spark.Response;

public class AliasHandlerTest {

  private static final String TEST_DOMAIN_JSON = "{\"name\":\"test domain\",\"domain\":\"TestEncodedDomain\",\"version\":1}";
  private static final String TEST_ENCODED_DOMAIN_DATA = "TestEncodedDomain";
  private static final int TEST_DOMAIN_ENCODING_VERSION = 1;

  /** The service able to import and export domain metadata. */
  @Mock
  private DomainService domainService;

  /**
   * The service able to import and export the externally shareable
   * representation of the domain
   */
  @Mock
  private ShareableDomainService shareableDomainService;

  /** The service that retrieves information from the AliasData table. */
  @Mock
  private AliasService aliasService;

  /** Mocked spark request. */
  @Mock
  private Request request;

  /** Mocked spark response */
  @Mock
  private Response response;

  /** Component able to convert between objects and JSON */
  private final ObjectMapper jsonObjectMapper;

  /** The handler being tested here. */
  private AliasHandlerImpl aliasHandler;

  public AliasHandlerTest() {
    AliasConfig config = new AliasConfig();
    this.jsonObjectMapper = config.getObjectMapper();
  }

  @BeforeMethod(alwaysRun = true)
  public void setup() {
    MockitoAnnotations.initMocks(this);
    aliasHandler = new AliasHandlerImpl(aliasService, domainService,
            shareableDomainService, jsonObjectMapper);
  }

  @Test
  public void getDomain_withValidDomainAndConfirm() throws Exception {
    // GIVEN
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    final String expectedJson = jsonObjectMapper.writerWithDefaultPrettyPrinter()
        .writeValueAsString(domainData);
    when(domainService.getDomain(TEST_DOMAIN)).thenReturn(domainData);
    when(request.params("domainId")).thenReturn(TEST_DOMAIN);

    // WHEN
    final String result = aliasHandler.getDomain(request, response);

    // THEN
    assertThat(result).isEqualTo(expectedJson);
  }

  @Test
  public void getDomainsLanguages_withNoErrorsAndConfirm() throws Exception {
    // GIVEN
    final List<DomainLanguagesInfo> domainData = Collections.singletonList(
            new DomainLanguagesData(TEST_DOMAIN, ENGLISH_AND_SPANISH));
    final String expectedJson = jsonObjectMapper.writerWithDefaultPrettyPrinter()
        .writeValueAsString(domainData);
    when(domainService.getAllDomainsWithLanguages()).thenReturn(domainData);

    // WHEN
    final String result = aliasHandler.getDomainsLanguages(request, response);

    // THEN
    assertThat(result).isEqualTo(expectedJson);
  }

  @Test
  public void importDomain_withValidDomainAndConfirm() throws Exception {
    // GIVEN
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    final String domainJson = jsonObjectMapper.writerWithDefaultPrettyPrinter()
        .writeValueAsString(domainData);
    when(request.body()).thenReturn(domainJson);

    // WHEN
    aliasHandler.importDomain(request, response);

    // THEN
    verify(domainService).importDomain(domainData);
  }

  @Test
  public void importDomain_withBadDomainJsonAndConfirmException() {
    // GIVEN
    when(request.body()).thenReturn("{\"bad-json\":");

    // WHEN
    Throwable t = catchThrowable(() -> aliasHandler.importShareableDomain(request, response));

    // THEN
    assertThat(t)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class);
  }

  @Test(expectedExceptions = DuplicateDomainException.class)
  public void importDomain_withDuplicateDomainExceptionAndConfirmException()
          throws Exception {
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    final String domainJson = jsonObjectMapper.writerWithDefaultPrettyPrinter()
        .writeValueAsString(domainData);
    when(request.body()).thenReturn(domainJson);
    doThrow(new DuplicateDomainException("Intentional"))
            .when(domainService).importDomain(domainData);

    // WHEN
    aliasHandler.importDomain(request, response);
  }

  @Test
  public void importDomain_withInvalidDomainExceptionAndConfirmException()
          throws Exception {
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));
    final String domainJson = jsonObjectMapper.writerWithDefaultPrettyPrinter()
        .writeValueAsString(domainData);
    when(request.body()).thenReturn(domainJson);
    doThrow(new InvalidDomainException("Intentional", Collections.singletonList("error_1")))
            .when(domainService).importDomain(domainData);

    // WHEN
    Throwable throwable = catchThrowable(() -> aliasHandler.importDomain(request, response));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class);
  }

  @Test
  public void getShareableDomain_withValidShareableDomainAndConfirm()
          throws Exception {
    // GIVEN
    final ShareableDomainInfo domainData = new ShareableDomain(
            TEST_DOMAIN,
            TEST_ENCODED_DOMAIN_DATA,
            TEST_DOMAIN_ENCODING_VERSION);
    when(shareableDomainService.getDomain(TEST_DOMAIN)).thenReturn(domainData);
    when(request.queryParams("name")).thenReturn(TEST_DOMAIN);

    // WHEN
    final String result = aliasHandler.getShareableDomain(request, response);

    // THEN
    assertThat(result).isEqualToIgnoringWhitespace(TEST_DOMAIN_JSON);
  }

  @Test
  public void importShareableDomain_withValidDomainAndConfirm()
          throws Exception {
    // GIVEN
    when(request.body()).thenReturn(TEST_DOMAIN_JSON);
    final ShareableDomain domainData = new ShareableDomain(
            TEST_DOMAIN,
            TEST_ENCODED_DOMAIN_DATA,
            TEST_DOMAIN_ENCODING_VERSION);

    // WHEN
    aliasHandler.importShareableDomain(request, response);

    // THEN
    verify(shareableDomainService).importDomain(domainData);
  }

  @Test
  public void importShareableDomain_withBadShareableDomainJsonAndConfirmException() {
    // GIVEN
    when(request.body()).thenReturn("{invalid_data}");

    // WHEN
    Throwable t = catchThrowable(() -> aliasHandler.importShareableDomain(request, response));

    // THEN
    assertThat(t)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class);
  }

  @Test
  public void importDomain_withDomainCodecExceptionAndConfirmException() {
    when(request.body()).thenReturn(TEST_DOMAIN_JSON);
    final ShareableDomain domainData = new ShareableDomain(
            TEST_DOMAIN,
            TEST_ENCODED_DOMAIN_DATA,
            TEST_DOMAIN_ENCODING_VERSION);
    doThrow(new DomainCodecException("Intentional"))
            .when(shareableDomainService).importDomain(domainData);

    // WHEN
    Throwable t = catchThrowable(() -> aliasHandler.importShareableDomain(request, response));

    // THEN
    assertThat(t)
        .isNotNull()
        .isInstanceOf(DomainCodecException.class);
  }

  @Test
  public void getAllProjectTypes() throws DataAccessLayerException, DataNotFoundException {
    // GIVEN
    String jsonExpected = "{\"success\":true,\"data\":[{\"id\":\"6\",\"name\":\"DocType_6\",\"description\":null,\"fields\":[{\"id\":\"5\",\"name\":\"field_5\",\"description\":null,\"fieldType\":\"fieldType_2\",\"aliases\":[\"alias_3_1\"],\"isDefaultSelected\":true,\"dataType\":\"fieldDataType_1\"}],\"languages\":[{\"id\":1,\"name\":\"English\",\"code\":\"eng\"},{\"id\":2,\"name\":\"German\",\"code\":\"gem\"}]}],\"errors\":null}";

    Request request = mock(Request.class);
    Response response = mock(Response.class);
    AliasService service = mock(AliasService.class);
    DomainService domainService = mock(DomainService.class);
    AliasHandler handler = new AliasHandlerImpl(service, domainService,
        shareableDomainService, jsonObjectMapper);
    when(service.getAllDocTypes()).thenReturn(createDocTypes());
    when(service.getAllLanguages()).thenReturn(createLanguages());

    // WHEN
    String projectTypes = handler.getAllProjectTypes(request, response);

    // THEN
    assertThat(projectTypes).isEqualToIgnoringWhitespace(jsonExpected);
  }

  private List<Language> createLanguages() {
    Language eng = new Language();
    eng.setId(1);
    eng.setName("English");
    eng.setCode("eng");

    Language gem = new Language();
    gem.setId(2);
    gem.setName("German");
    gem.setCode("gem");

    Language fra = new Language();
    fra.setId(3);
    fra.setName("French");
    fra.setCode("fra");

    Language spa = new Language();
    spa.setId(4);
    spa.setName("Spanish");
    spa.setCode("spa");

    Language ita = new Language();
    ita.setId(5);
    ita.setName("Italian");
    ita.setCode("ita");

    return Arrays.asList(eng, gem, fra, spa, ita);
  }

  private List<DocType> createDocTypes() {

    FieldDataType fieldDataType = new FieldDataType();
    fieldDataType.setId(1);
    fieldDataType.setName("fieldDataType_1");

    FieldType fieldType = new FieldType();
    fieldType.setId(2);
    fieldType.setName("fieldType_2");

    List<Aliases> aliases = new ArrayList<>(2);
    Aliases alias = new Aliases();
    alias.setId(3);
    alias.setAlias("alias_3_1");
    alias.setLanguageId(1);
    alias.setFieldId(5);
    aliases.add(alias);

    alias = new Aliases();
    alias.setId(4);
    alias.setAlias("alias_4_2");
    alias.setLanguageId(2);
    alias.setFieldId(5);
    aliases.add(alias);

    List<Fields> fields = new ArrayList<>(1);
    Fields field = new Fields();
    field.setId(5);
    field.setName("field_5");
    field.setFieldDataType(fieldDataType);
    field.setFieldType(fieldType);
    field.setDocTypeId(6);
    field.setIsDefaultSelected(true);
    field.setAliasesList(aliases);
    fields.add(field);

    List<DocType> docTypes = new ArrayList<>(1);
    DocType docType = new DocType();
    docType.setId(6);
    docType.setName("DocType_6");
    docType.setFieldList(fields);
    docTypes.add(docType);

    return docTypes;
  }
}
