package com.automationanywhere.cognitive.alias.models.entitymodel;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MapValueParameterTest {
    @Test
    public void testOfMapValueParameterConstructorShouldSetProperValues() throws Exception {
        // given
        String value = "123";
        String operator = "op";

        // when
        MapValueParameter param = new MapValueParameter(value, operator);

        // then
        assertThat(param).isNotNull();
        assertThat(param.getValue()).isEqualTo(value);
        assertThat(param.getOperator()).isEqualTo(operator);
    }
}
