package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.util.BaseUnitTest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.testng.annotations.Test;

import java.util.Collections;

import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ENGLISH_AND_SPANISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.FORMAT_NUMBER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.NOT_CHECKED_BY_DEFAULT;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ONLY_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TABLE_COLUMN_FIELD;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOMAIN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ALIAS_ENG;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_ENG_AND_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1_NAME;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ALIAS_ENG;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ALIAS_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_ENG_AND_SPN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_2_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * Unit tests for the Default Domain Validator component.
 *
 * @author Erik K. Worth
 */
public class DefaultDomainValidatorTest extends BaseUnitTest {

  /** The validator being tested here */
  private DomainValidator domainValidator = new DefaultDomainValidator();

  @Test
  public void checkValid_forValidDomainAndConfirmNoException() {
    // Given
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    TEST_FIELD_2_ENG_AND_SPN));

    // When
    Throwable throwable = catchThrowable(() -> domainValidator.checkValid(domainData));

    // THEN
    assertThat(throwable).doesNotThrowAnyException();
  }

  @Test
  public void checkValid_withDuplicateFieldsAndConfirmError() {
    // Given
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    new DomainFieldData(
                            TEST_FIELD_1_NAME, // Duplicate field name
                            TABLE_COLUMN_FIELD,
                            FORMAT_NUMBER,
                            NOT_CHECKED_BY_DEFAULT,
                            ImmutableSet.of(
                                    TEST_FIELD_2_ALIAS_ENG,
                                    TEST_FIELD_2_ALIAS_SPN))));

    // When
    Throwable throwable = catchThrowable(() -> domainValidator.checkValid(domainData));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class)
        .hasMessage("Error to validate the domain to import.");

    assertThat(((InvalidDomainException) throwable).getErrors())
        .hasSize(1)
        .containsOnly("Field is not unique: Test Field 1");
  }

  @Test
  public void checkValid_withDuplicateAliasForLanguageAndConfirmError() {
    // Given
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    new DomainFieldData(
                            TEST_FIELD_2_NAME,
                            TABLE_COLUMN_FIELD,
                            FORMAT_NUMBER,
                            NOT_CHECKED_BY_DEFAULT,
                            ImmutableSet.of(
                                    TEST_FIELD_1_ALIAS_ENG,  // Duplicate alias
                                    TEST_FIELD_2_ALIAS_SPN))));

    // When
    Throwable throwable = catchThrowable(() -> domainValidator.checkValid(domainData));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class)
        .hasMessage("Error to validate the domain to import.");

    assertThat(((InvalidDomainException) throwable).getErrors())
        .hasSize(1)
        .containsOnly("Alias is not unique for language English: Test Field One");
  }

  @Test
  public void checkValid_withFieldMissingAliasForEnglishAndConfirmError() {
    // Given
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ENGLISH_AND_SPANISH,
            ImmutableList.of(
                    TEST_FIELD_1_ENG_AND_SPN,
                    new DomainFieldData(
                            TEST_FIELD_2_NAME,
                            TABLE_COLUMN_FIELD,
                            FORMAT_NUMBER,
                            NOT_CHECKED_BY_DEFAULT,
                            // Missing english alias
                            Collections.singleton(TEST_FIELD_2_ALIAS_SPN))));

    // When
    Throwable throwable = catchThrowable(() -> domainValidator.checkValid(domainData));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class)
        .hasMessage("Error to validate the domain to import.");

    assertThat(((InvalidDomainException) throwable).getErrors())
        .hasSize(1)
        .containsOnly("Field Test Field 2 missing aliases for language(s): [English]");
  }

  @Test
  public void checkValid_withFieldHavingUnexpectedLanguageAndConfirmError() {
    // Given
    final DomainData domainData = new DomainData(
            TEST_DOMAIN,
            ONLY_ENGLISH,
            Collections.singletonList(TEST_FIELD_1_ENG_AND_SPN));

    // When
    Throwable throwable = catchThrowable(() -> domainValidator.checkValid(domainData));

    // THEN
    assertThat(throwable)
        .isNotNull()
        .isInstanceOf(InvalidDomainException.class)
        .hasMessage("Error to validate the domain to import.");

    assertThat(((InvalidDomainException) throwable).getErrors())
        .hasSize(1)
        .containsOnly("Field Test Field 1 has aliases defined for unexpected languages: [Spanish]");
  }
}
