package com.automationanywhere.cognitive.service.shareabledomain.internal;

import com.automationanywhere.cognitive.service.domain.DomainService;
import com.automationanywhere.cognitive.service.domain.internal.DomainData;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainInfo;
import com.automationanywhere.cognitive.util.BaseUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.automationanywhere.cognitive.service.shareabledomain.internal.DomainCodec.DOMAIN_ENCODING_V1;
import static com.automationanywhere.cognitive.service.shareabledomain.internal.ShareableDomainFixture.TEST_DOMAIN_JSON;
import static com.automationanywhere.cognitive.service.shareabledomain.internal.ShareableDomainFixture.TEST_DOMAIN_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

/**
 * Unit tests for the Default Shareable Domain Service.
 *
 * @author Erik K. Worth
 */
public class DefaultShareableDomainServiceTest extends BaseUnitTest {

  /** The domain service able to store and retrieve the domain metadata */
  @Mock
  private DomainService domainService;

  /** The component able to convert between the Domain Data and JSON */
  @Mock
  private ObjectMapper jsonObjectMapper;

  @Mock
  private DomainData domainData;

  /** The service being tested here */
  private DefaultShareableDomainService shareableDomainService;

  @BeforeMethod(alwaysRun = true)
  public void setup() {
    MockitoAnnotations.initMocks(this);
    shareableDomainService =
            new DefaultShareableDomainService(domainService, jsonObjectMapper);
  }

  @DataProvider(name = "dataConstructWithNulls")
  public Object[][] dataConstructWithNulls() {
    setup();
    return new Object[][] {
            {"domainService", null, jsonObjectMapper},
            {"jsonObjectMapper", domainService, null},
    };
  }

  @Test(dataProvider = "dataConstructWithNulls")
  public void construct_withNullsAndConfirmException(
          final String argName,
          final DomainService domainService,
          final ObjectMapper jsonObjectMapper) {
    // When
    try {
      new DefaultShareableDomainService(domainService, jsonObjectMapper);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(argName + " must not be null");
    }
  }

  @Test
  public void getDomain_withValidDomainNameAndConfirmSuccessfulResult()
          throws Exception {
    // Given
    when(domainService.getDomain(TEST_DOMAIN_NAME)).thenReturn(domainData);
    when(jsonObjectMapper.writeValueAsString(domainData))
            .thenReturn(TEST_DOMAIN_JSON);

    // When
    final ShareableDomainInfo shareableDomain =
            shareableDomainService.getDomain(TEST_DOMAIN_NAME);

    // Then
    assertThat(shareableDomain.getName()).isEqualTo(TEST_DOMAIN_NAME);
    assertThat(DomainCodec.decodeDomain(shareableDomain.getDomain()))
            .isEqualTo(TEST_DOMAIN_JSON);
    assertThat(shareableDomain.getVersion()).isEqualTo(DOMAIN_ENCODING_V1);
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void getDomain_withNullDomainNameAndConfirmException() {
    // When
    try {
      shareableDomainService.getDomain(null);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then"
      assertThat(exc.getMessage()).isEqualTo("domainName must not be null");
    }
  }

  @Test(expectedExceptions = DomainCodecException.class)
  public void getDomain_withErrorWritingJsonAndConfirmException()
          throws Exception {
    // Given
    when(domainService.getDomain(TEST_DOMAIN_NAME)).thenReturn(domainData);
    when(jsonObjectMapper.writeValueAsString(domainData))
            .thenThrow(new RuntimeException("Intentional"));

    // When
    shareableDomainService.getDomain(TEST_DOMAIN_NAME);
  }

  @Test
  public void importDomain_withValidShareableDomainAndConfirmNoException()
          throws Exception {
    // Given
    final String encodedDomain = DomainCodec.encodeDomain(TEST_DOMAIN_JSON);
    final ShareableDomain shareableDomain = new ShareableDomain(
            TEST_DOMAIN_NAME, encodedDomain, DOMAIN_ENCODING_V1);
    when(jsonObjectMapper.readValue(TEST_DOMAIN_JSON, DomainData.class))
            .thenReturn(domainData);

    // When
    shareableDomainService.importDomain(shareableDomain);

    // Then
    verify(domainService).importDomain(domainData);
  }

  @Test(expectedExceptions = DomainCodecException.class)
  public void importDomain_withErrorReadingDomainJsonAndConfirmException()
          throws Exception {
    // Given
    final String encodedDomain = DomainCodec.encodeDomain(TEST_DOMAIN_JSON);
    final ShareableDomain shareableDomain = new ShareableDomain(
            TEST_DOMAIN_NAME, encodedDomain, DOMAIN_ENCODING_V1);
    when(jsonObjectMapper.readValue(TEST_DOMAIN_JSON, DomainData.class))
            .thenThrow(new RuntimeException("Intentional"));

    // When
    shareableDomainService.importDomain(shareableDomain);
  }

}
