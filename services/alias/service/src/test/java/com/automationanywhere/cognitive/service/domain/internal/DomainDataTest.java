package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.util.BaseUnitTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.LANG_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.OBJECT_MAPPER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ONLY_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOMAIN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_ONLY_FIELD_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Unit tests for the Domain Data Transfer Object.
 *
 * @author Erik K. Worth
 */
public class DomainDataTest extends BaseUnitTest {

  @DataProvider(name = "dataConstructWithNulls")
  public Object[][] dataConstructWithNulls() {
    return new Object[][] {
            {"name", null, ONLY_ENGLISH, TEST_ONLY_FIELD_1},
            {"languages", TEST_DOMAIN, null, TEST_ONLY_FIELD_1},
            {"fields", TEST_DOMAIN, ONLY_ENGLISH, null},
    };
  }

  @Test(dataProvider = "dataConstructWithNulls")
  public void construct_withNullsAndConfirmException(
          final String argName,
          final String name,
          final Set<String> languages,
          final List<DomainFieldData> fields) {
    try {
      // When
      new DomainData(name, languages, fields);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(argName + " must not be null");
    }
  }

  @SuppressWarnings("EqualsWithItself")
  @Test
  public void equals_withSameObjectAndConfirmTrue() {
    // When
    final DomainData domainData = new DomainData(
            TEST_DOMAIN, ONLY_ENGLISH, TEST_ONLY_FIELD_1);
    // Then
    assertThat(domainData.equals(domainData)).isTrue();
  }

  @SuppressWarnings("EqualsBetweenInconvertibleTypes")
  @Test
  public void equals_withWrongTypeAndConfirmFalse() {
    // When
    final DomainData domainData = new DomainData(
            TEST_DOMAIN, ONLY_ENGLISH, TEST_ONLY_FIELD_1);
    // Then
    assertThat(domainData.equals("Not Domain Data")).isFalse();
  }

  @Test
  public void equals_withSameDataAndConfirmTrue() {
    // When
    final DomainData domainData1 = new DomainData(
            TEST_DOMAIN, ONLY_ENGLISH, TEST_ONLY_FIELD_1);
    final DomainData domainData2 = new DomainData(
            TEST_DOMAIN, ONLY_ENGLISH, TEST_ONLY_FIELD_1);
    // Then
    assertThat(domainData1.equals(domainData2)).isTrue();
    assertThat(domainData1.toString()).isEqualTo(domainData2.toString());
    assertThat(domainData1.hashCode()).isEqualTo(domainData2.hashCode());
  }

  @Test
  public void get_fieldsAndConfirmEqualsConstructedValues() {
    // When
    final DomainData domainData = new DomainData(
            TEST_DOMAIN, ONLY_ENGLISH, TEST_ONLY_FIELD_1);
    // Then
    assertThat(domainData.getName()).isEqualTo(TEST_DOMAIN);
    assertThat(domainData.getLanguages()).containsExactly(LANG_ENGLISH);
    assertThat(domainData.getFields()).containsExactly(TEST_FIELD_1);
  }

  @Test
  public void json_externalizeInternalizeAndConfirmEqual() throws Exception {
    // Given
    final DomainData domainData = new DomainData(
            TEST_DOMAIN, ONLY_ENGLISH, TEST_ONLY_FIELD_1);
    // When
    final String json = OBJECT_MAPPER.writeValueAsString(domainData);
    final DomainData internalizedDomain =
            OBJECT_MAPPER.readValue(json, DomainData.class);
    // Then
    assertThat(internalizedDomain).isEqualTo(domainData);
  }
}
