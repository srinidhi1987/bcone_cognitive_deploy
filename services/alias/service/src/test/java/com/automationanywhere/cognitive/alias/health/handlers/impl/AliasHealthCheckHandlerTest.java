package com.automationanywhere.cognitive.alias.health.handlers.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.health.connections.ConnectionType;
import com.automationanywhere.cognitive.alias.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.alias.health.connections.HealthCheckConnectionFactory;

public class AliasHealthCheckHandlerTest {

    @InjectMocks
    AliasHealthCheckHandler healthAPIHandlerImpl;
    @Mock
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    
    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfCheckHealthWhenDBConnectionSucceeds() throws DataAccessLayerException{
        //given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class))).thenReturn(healthCheckConnection);
        Mockito.doNothing().when(healthCheckConnection).checkConnection();
        
        //when 
        String actualResult = healthAPIHandlerImpl.checkHealth();
        
        //then
        assertThat(actualResult).isNotNull();
        assertThat(actualResult).contains("Database Connectivity: OK");
    }
    
    @Test
    public void testOfCheckHealthWhenDBConnectionFailsWithDataAccessLayerException() throws DataAccessLayerException{
        //given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class))).thenReturn(healthCheckConnection);
        Mockito.doThrow(DataAccessLayerException.class).when(healthCheckConnection).checkConnection();
        
        //when 
        String actualResult = healthAPIHandlerImpl.checkHealth();
        
        //then
        assertThat(actualResult).isNotNull();
        assertThat(actualResult).contains("Database Connectivity: FAILURE");
    }
    
    @Test
    public void testOfCheckHealthWhenDBConnectionFailsWithOtherException() throws DataAccessLayerException{
      //given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class))).thenReturn(healthCheckConnection);
        Mockito.doThrow(Exception.class).when(healthCheckConnection).checkConnection();
        
        //when 
        String actualResult = healthAPIHandlerImpl.checkHealth();
        
        //then
        assertThat(actualResult).isNotNull();
        assertThat(actualResult).contains("Database Connectivity: FAILURE");
     }
    
}
