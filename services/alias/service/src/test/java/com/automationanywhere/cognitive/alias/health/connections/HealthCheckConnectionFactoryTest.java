package com.automationanywhere.cognitive.alias.health.connections;

import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.alias.exception.customexceptions.UnsupportedConnectionTypeException;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;

public class HealthCheckConnectionFactoryTest {
    
    @Mock
    AliasService aliasService;
    
    @InjectMocks
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    
    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfGetConnectionWhenTypeIsDB(){
        
        //when
        HealthCheckConnection healthCheckConnection = healthCheckConnectionFactory.getConnection(ConnectionType.DB);
        
        //then
        assertThat(healthCheckConnection).isNotNull();
        assertThat(healthCheckConnection).isInstanceOf(DBCheckConnection.class);
    }
    
    @Test(expectedExceptions={UnsupportedConnectionTypeException.class})
    public void testOfGetConnectionWhenTypeIsNull(){
        
        //when
        HealthCheckConnection healthCheckConnection = healthCheckConnectionFactory.getConnection(null);
        
        //then
        //check for exception
    }
}
