package com.automationanywhere.cognitive.alias.exception.impl;

import com.automationanywhere.cognitive.alias.models.CustomResponse;
import com.automationanywhere.cognitive.alias.models.ImportDomainResponse;
import com.automationanywhere.cognitive.alias.parser.contract.JsonParser;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.NoSuchDomainException;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.automationanywhere.cognitive.util.BaseUnitTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Collections;
import java.util.List;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import spark.Request;
import spark.Response;

import java.sql.SQLException;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CONFLICT;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the Exception Handlers.
 *
 * @author Erik K. Worth
 */
public class ExceptionHandlerImplTest extends BaseUnitTest {

  /** The message provided in the exception */
  private static final String TEST_EXC_MESSAGE = "test-exc-message";

  /** This can be any string since it is just used as a marker */
  private static final String RESPONSE_JSON = "{}";

  private static class TestJsonProcessingException
          extends JsonProcessingException {
    final static long serialVersionUID = 8462565648L;
    TestJsonProcessingException(String msg) {
      super(msg);
    }
  }

  @Mock
  private JsonParser jsonParser;

  /** Mocked spark request. */
  @Mock
  private Request request;

  /** Mocked spark response */
  @Mock
  private Response response;

  /** The class to test here */
  private ExceptionHandlerImlp exceptionHandler;

  @BeforeMethod(alwaysRun = true)
  public void setup() {
    MockitoAnnotations.initMocks(this);
    exceptionHandler = new ExceptionHandlerImlp();
    exceptionHandler.jsonParser = jsonParser;
  }

  @Test
  public void handleDomainException_withDomainExceptionAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("Error processing domain: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleDomainException(
            new DomainException(TEST_EXC_MESSAGE),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_INTERNAL_SERVER_ERROR);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleDuplicateDomainException_withDuplicateDomainExceptionAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("Duplicate domain: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleDuplicateDomainException(
            new DuplicateDomainException(TEST_EXC_MESSAGE),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_CONFLICT);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleDuplicateDomainException_withDuplicateDomainExceptionCauseAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("Duplicate domain: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleDuplicateDomainException(
            new DuplicateDomainException(TEST_EXC_MESSAGE, new SQLException("Intentional")),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_CONFLICT);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleInvalidDomainException_withInvalidDomainExceptionAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            createImportDomainResponse(Collections.singletonList(TEST_EXC_MESSAGE))))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleInvalidDomainException(
            new InvalidDomainException(TEST_EXC_MESSAGE),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_BAD_REQUEST);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleJsonProcessingException_withJsonProcessingExceptionAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("Error Writing JSON: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleJsonProcessingException(
            new TestJsonProcessingException(TEST_EXC_MESSAGE),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_INTERNAL_SERVER_ERROR);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleNoSuchDomainException_withNoSuchDomainExceptionAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("No Such Domain: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleNoSuchDomainException(
            new NoSuchDomainException(TEST_EXC_MESSAGE),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_NOT_FOUND);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleNoSuchDomainException_withNoSuchDomainExceptionCauseAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("No Such Domain: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleNoSuchDomainException(
            new NoSuchDomainException(TEST_EXC_MESSAGE,
                    new SQLException("Intentional")),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_NOT_FOUND);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  @Test
  public void handleDomainCodecException_withDomainCodecExceptionCauseAndConfirm() {
    // GIVEN
    when(jsonParser.getResponse(
            makeCustomResponse("Error Decoding Domain: " + TEST_EXC_MESSAGE)))
            .thenReturn(RESPONSE_JSON);

    // WHEN
    exceptionHandler.handleDomainCodecException(
            new DomainCodecException(TEST_EXC_MESSAGE,
                    new SQLException("Intentional")),
            request,
            response);

    // THEN
    verify(response).body(RESPONSE_JSON);
    verify(response).status(SC_BAD_REQUEST);
    verify(response).type(MediaType.APPLICATION_JSON_VALUE);
  }

  private CustomResponse makeCustomResponse(final String error) {
    CustomResponse customResponse = new CustomResponse();
    customResponse.setSuccess(false);
    customResponse.setErrors(error);
    return customResponse;
  }

  private ImportDomainResponse createImportDomainResponse(final List<String> errors) {
    ImportDomainResponse importDomainResponse = new ImportDomainResponse();
    importDomainResponse.setSuccess(false);
    importDomainResponse.setErrors(errors);
    return importDomainResponse;
  }
}
