package com.automationanywhere.cognitive.service.domain.internal;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.LANG_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.OBJECT_MAPPER;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.ONLY_ENGLISH;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_DOMAIN;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_FIELD_1;
import static com.automationanywhere.cognitive.service.domain.internal.DomainFixture.TEST_ONLY_FIELD_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

public class DomainLanguagesDataTest {

  @DataProvider(name = "dataConstructWithNulls")
  public Object[][] dataConstructWithNulls() {
    return new Object[][] {
            {"name", null, ONLY_ENGLISH},
            {"languages", TEST_DOMAIN, null},
    };
  }

  @Test(dataProvider = "dataConstructWithNulls")
  public void construct_withNullsAndConfirmException(
          final String argName,
          final String name,
          final Set<String> languages) {
    try {
      // When
      new DomainLanguagesData(name, languages);
      fail("Expected a NullPointerException");
    } catch (NullPointerException exc) {
      // Then
      assertThat(exc.getMessage()).isEqualTo(argName + " must not be null");
    }
  }

  @SuppressWarnings("EqualsWithItself")
  @Test
  public void equals_withSameObjectAndConfirmTrue() {
    // When
    final DomainLanguagesData domainData = new DomainLanguagesData(
            TEST_DOMAIN, ONLY_ENGLISH);
    // Then
    assertThat(domainData.equals(domainData)).isTrue();
  }

  @SuppressWarnings("EqualsBetweenInconvertibleTypes")
  @Test
  public void equals_withWrongTypeAndConfirmFalse() {
    // When
    final DomainLanguagesData domainData = new DomainLanguagesData(
            TEST_DOMAIN, ONLY_ENGLISH);
    // Then
    assertThat(domainData.equals("Not Domain Data")).isFalse();
  }

  @Test
  public void equals_withSameDataAndConfirmTrue() {
    // When
    final DomainLanguagesData domainData1 = new DomainLanguagesData(
            TEST_DOMAIN, ONLY_ENGLISH);
    final DomainLanguagesData domainData2 = new DomainLanguagesData(
            TEST_DOMAIN, ONLY_ENGLISH);
    // Then
    assertThat(domainData1.equals(domainData2)).isTrue();
    assertThat(domainData1.toString()).isEqualTo(domainData2.toString());
    assertThat(domainData1.hashCode()).isEqualTo(domainData2.hashCode());
  }

  @Test
  public void get_fieldsAndConfirmEqualsConstructedValues() {
    // When
    final DomainLanguagesData domainData = new DomainLanguagesData(
            TEST_DOMAIN, ONLY_ENGLISH);
    // Then
    assertThat(domainData.getName()).isEqualTo(TEST_DOMAIN);
    assertThat(domainData.getLanguages()).containsExactly(LANG_ENGLISH);
  }

  @Test
  public void json_externalizeInternalizeAndConfirmEqual() throws Exception {
    // Given
    final DomainLanguagesData domainData = new DomainLanguagesData(
            TEST_DOMAIN, ONLY_ENGLISH);
    // When
    final String json = OBJECT_MAPPER.writeValueAsString(domainData);
    final DomainLanguagesData internalizedDomain =
            OBJECT_MAPPER.readValue(json, DomainLanguagesData.class);
    // Then
    assertThat(internalizedDomain).isEqualTo(domainData);
  }
}
