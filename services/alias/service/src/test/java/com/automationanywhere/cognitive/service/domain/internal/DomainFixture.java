package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.alias.models.entitymodel.Aliases;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldDataType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.alias.models.entitymodel.Language;
import com.automationanywhere.cognitive.service.domain.DomainFieldType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides test data related to Domains.
 *
 * @author Erik K. Worth
 */
public class DomainFixture {

  /** Component used to externalize and internalize Objects to and from JSON. */
  static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private static final DomainFieldType FORM_FIELD = DomainFieldType.FORM_FIELD;
  static final DomainFieldType TABLE_COLUMN_FIELD =
          DomainFieldType.TABLE_COLUMN_FIELD;

  static final String FORMAT_TEXT = "Text";
  static final String FORMAT_NUMBER = "Number";
  static final String FORMAT_DATE = "Date";

  private static final FieldDataType FIELD_TYPE_TEXT =
          makeFieldDataType(FORMAT_TEXT);
  static final FieldDataType FIELD_TYPE_NUMBER =
          makeFieldDataType(FORMAT_NUMBER);

  public static final String TEST_DOMAIN = "test domain";
  static final int TEST_DOMAIN_ID = 1;
  static final Map<String, Integer> TEST_SUPPORTED_DOMAINS =
          ImmutableMap.of(TEST_DOMAIN, TEST_DOMAIN_ID);

  static final int LANG_ENG_ID = 1;
  static final String LANG_ENGLISH = "English";
  private static final int LANG_SPN_ID = 2;
  static final String LANG_SPANISH = "Spanish";
  static final Set<String> ONLY_ENGLISH =
          Collections.singleton(LANG_ENGLISH);
  public static final Set<String> ENGLISH_AND_SPANISH =
          ImmutableSet.of(LANG_ENGLISH, LANG_SPANISH);

  private static final String FORM_FIELD_TYPE_NAME = "FormField";
  private static final String TABLE_FIELD_TYPE_NAME = "TableField";
  private static final FieldType FORM_FIELD_TYPE =
          makeFieldTypeRecord(1, FORM_FIELD_TYPE_NAME);
  static final FieldType TABLE_FIELD_TYPE =
          makeFieldTypeRecord(2, TABLE_FIELD_TYPE_NAME);

  static final List<Language> TEST_SUPPORTED_TWO_LANGUAGES =
          makeSupportedLanguages();

  static final boolean CHECKED_BY_DEFAULT = true;
  static final boolean NOT_CHECKED_BY_DEFAULT = false;

  static final String TEST_FIELD_1_NAME = "Test Field 1";
  static final String TEST_FIELD_1_ALIAS_ENG_1 = "Test Field One";
  static final String TEST_FIELD_1_ALIAS_SPN_1 = "campo de prueba uno";
  static final Set<String> TEST_FIELD_1_ALIASES_ENG_1 =
          Collections.singleton(TEST_FIELD_1_ALIAS_ENG_1);
  private static final Set<String> TEST_FIELD_1_ALIASES_SPN_1 =
          Collections.singleton(TEST_FIELD_1_ALIAS_SPN_1);
  static final AliasData TEST_FIELD_1_ALIAS_ENG =
          new AliasData(TEST_FIELD_1_ALIASES_ENG_1, LANG_ENGLISH);
  static final AliasData TEST_FIELD_1_ALIAS_SPN =
          new AliasData(TEST_FIELD_1_ALIASES_SPN_1, LANG_SPANISH);
  static final Set<AliasData> TEST_FIELD_1_ALIASES_ONLY_ENG =
          Collections.singleton(TEST_FIELD_1_ALIAS_ENG);
  static final DomainFieldData TEST_FIELD_1 = new DomainFieldData(
          TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT,
          CHECKED_BY_DEFAULT, TEST_FIELD_1_ALIASES_ONLY_ENG);
  static final List<DomainFieldData> TEST_ONLY_FIELD_1 =
          Collections.singletonList(TEST_FIELD_1);
  public static final DomainFieldData TEST_FIELD_1_ENG_AND_SPN = new DomainFieldData(
          TEST_FIELD_1_NAME, FORM_FIELD, FORMAT_TEXT, CHECKED_BY_DEFAULT,
          ImmutableSet.of(TEST_FIELD_1_ALIAS_ENG, TEST_FIELD_1_ALIAS_SPN));

  static final String TEST_FIELD_2_NAME = "Test Field 2";
  static final String TEST_FIELD_2_ALIAS_ENG_1 = "Test Field Two";
  static final String TEST_FIELD_2_ALIAS_SPN_1 = "campo de prueba dos";
  private static final Set<String> TEST_FIELD_2_ALIASES_ENG_1 =
          Collections.singleton(TEST_FIELD_2_ALIAS_ENG_1);
  private static final Set<String> TEST_FIELD_2_ALIASES_SPN_1 =
          Collections.singleton(TEST_FIELD_2_ALIAS_SPN_1);
  static final AliasData TEST_FIELD_2_ALIAS_ENG =
          new AliasData(TEST_FIELD_2_ALIASES_ENG_1, LANG_ENGLISH);
  static final AliasData TEST_FIELD_2_ALIAS_SPN =
          new AliasData(TEST_FIELD_2_ALIASES_SPN_1, LANG_SPANISH);
  public static final DomainFieldData TEST_FIELD_2_ENG_AND_SPN = new DomainFieldData(
          TEST_FIELD_2_NAME, TABLE_COLUMN_FIELD, FORMAT_NUMBER, NOT_CHECKED_BY_DEFAULT,
          ImmutableSet.of(TEST_FIELD_2_ALIAS_ENG, TEST_FIELD_2_ALIAS_SPN));

  static final Fields TEST_FIELD_RECORD_1 = makeFieldRecord(
          TEST_FIELD_1_NAME,
          FORM_FIELD_TYPE,
          FIELD_TYPE_TEXT,
          CHECKED_BY_DEFAULT,
          new ImmutableList.Builder<Aliases>()
                  .addAll(makeAliasRecords(LANG_ENG_ID, TEST_FIELD_1_ALIAS_ENG_1))
                  .addAll(makeAliasRecords(LANG_SPN_ID, TEST_FIELD_1_ALIAS_SPN_1))
                  .build()
  );

  private static final Fields TEST_FIELD_RECORD_2 = makeFieldRecord(
          TEST_FIELD_2_NAME,
          TABLE_FIELD_TYPE,
          FIELD_TYPE_NUMBER,
          NOT_CHECKED_BY_DEFAULT,
          new ImmutableList.Builder<Aliases>()
                  .addAll(makeAliasRecords(LANG_ENG_ID, TEST_FIELD_2_ALIAS_ENG_1))
                  .addAll(makeAliasRecords(LANG_SPN_ID, TEST_FIELD_2_ALIAS_SPN_1))
                  .build()
  );

  static final DocType TEST_DOC_TYPE_RECORD = makeDocTypeRecord(
          TEST_DOMAIN,
          TEST_FIELD_RECORD_1,
          TEST_FIELD_RECORD_2);

  static AliasData englishAlias(final String aliasName) {
    return new AliasData(Collections.singleton(aliasName), LANG_ENGLISH);
  }

  static DocType makeDocTypeRecord(
          final String name,
          final Fields... fields) {
    final DocType docType = new DocType();
    docType.setName(name);
    docType.setFieldList(Arrays.asList(fields));
    return docType;
  }

  private static List<Language> makeSupportedLanguages() {
    final Language english = new Language();
    english.setId(LANG_ENG_ID);
    english.setName(LANG_ENGLISH);
    final Language spanish = new Language();
    spanish.setId(LANG_SPN_ID);
    spanish.setName(LANG_SPANISH);
    return ImmutableList.of(english, spanish);
  }

  static Fields makeFieldRecord(
          final String name,
          final FieldType fieldType,
          final FieldDataType fieldDataType,
          final boolean isDefaultSelected,
          final List<Aliases> aliases) {
    final Fields field = new Fields();
    field.setName(name);
    field.setFieldType(fieldType);
    field.setFieldDataType(fieldDataType);
    field.setIsDefaultSelected(isDefaultSelected);
    field.setAliasesList(aliases);
    return field;
  }

  private static FieldType makeFieldTypeRecord(
          final int id,
          final String name) {
    final FieldType fieldType = new FieldType();
    fieldType.setId(id);
    fieldType.setName(name);
    return fieldType;
  }

  static FieldDataType makeFieldDataType(final String name) {
    final FieldDataType fieldDataType = new FieldDataType();
    fieldDataType.setName(name);
    return fieldDataType;
  }

  static List<Aliases> makeAliasRecords(
          final int languageId,
          final String... aliasNames) {
    return Arrays.stream(aliasNames).map(aliasName -> {
      final Aliases aliasRecord = new Aliases();
      aliasRecord.setAlias(aliasName);
      aliasRecord.setLanguageId(languageId);
      return aliasRecord;
    }).collect(Collectors.toList());
  }
}
