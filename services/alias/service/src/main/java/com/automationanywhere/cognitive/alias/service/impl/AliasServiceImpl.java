package com.automationanywhere.cognitive.alias.service.impl;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DuplicateDataException;
import com.automationanywhere.cognitive.alias.models.entitymodel.Aliases;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocTypeWithLanguageId;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldDataType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.alias.models.entitymodel.Language;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;
import com.automationanywhere.cognitive.common.logger.AALogger;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
public class AliasServiceImpl implements AliasService{

  /** Logger for this class */
  private static final AALogger LOGGER =
      AALogger.create(AliasServiceImpl.class);

  /**
   * The number of items to include in batch requests.  This needs to match
   * the hibernate.jdbc.batch_size property in the data source configuration.
   */
  private static final int JDBC_BATCH_SIZE = 20;

  private SessionFactory sessionFactory;

  public AliasServiceImpl(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }


  @SuppressWarnings("unchecked")
  @Override
  @Transactional
  public Map<String, Integer> getDocTypeNamesWithIds()
      throws DataAccessLayerException {

    final List<Object[]> result;
    try {
      result = getCurrentOrNewSession()
          .createNativeQuery("SELECT Name, Id FROM DocTypeMaster")
          .list();
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Error retrieving document names and IDs", exc);
    }
    try {
      return (null == result)
          ? Collections.emptyMap()
          : result.stream().collect(Collectors.toMap(
              row -> getStringAtIndex0(row).toLowerCase(),
              AliasServiceImpl::getIntegerAtIndex1));
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Unexpected database result from query: "
              + "'SELECT Name, Id FROM DocTypeMaster'",
          exc);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional
  public Map<String, Integer> getFieldTypeNamesWithIds()
      throws DataAccessLayerException {

    final List<Object[]> result;
    try {
      result = getCurrentOrNewSession()
          .createNativeQuery("SELECT Name, Id FROM FieldTypeMaster")
          .list();
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Error retrieving document names and IDs", exc);
    }
    try {
      return (null == result)
          ? Collections.emptyMap()
          : result.stream().collect(Collectors.toMap(
              AliasServiceImpl::getStringAtIndex0,
              AliasServiceImpl::getIntegerAtIndex1));
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Unexpected database result from query: "
              + "'SELECT Name, Id FROM FieldTypeMaster'",
          exc);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional
  public Map<String, Integer> getFieldDataTypeNamesWithIds()
      throws DataAccessLayerException {

    final List<Object[]> result;
    try {
      result = getCurrentOrNewSession()
          .createNativeQuery("SELECT Name, Id FROM FieldDataTypeMaster")
          .list();
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Error retrieving document names and IDs", exc);
    }
    try {
      return (null == result)
          ? Collections.emptyMap()
          : result.stream().collect(Collectors.toMap(
              AliasServiceImpl::getStringAtIndex0,
              AliasServiceImpl::getIntegerAtIndex1));
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Unexpected database result from query: "
              + "'SELECT Name, Id FROM FieldDataTypeMaster'",
          exc);
    }
  }

  @Override
  @Transactional
  public void bulkInsertDocTypeFieldsAndAliases(final DocType docType)
      throws DataAccessLayerException {
    final Session session = getCurrentOrNewSession();
    final DocTypeWithLanguageId newDocType = new DocTypeWithLanguageId();
    newDocType.setName(docType.getName());
    newDocType.setLanguageId(1);
    final Number docTypeId;
    try {
      docTypeId = (Number) session.save(newDocType);
    } catch (final ConstraintViolationException exc) {
      throw new DuplicateDataException("Entry already exists for domain name '"
          + docType.getName() + "'", exc);
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Error inserting entry into DocTypeMaster for domain name '"
              + docType.getName() + "'", exc);
    }
    if (null == docTypeId) {
      throw new DataAccessLayerException("Failed to insert Domain with name '"
          + docType.getName() + "'");
    }
    LOGGER.debug("Inserted DocType {} with ID {}",
        docType.getName(), docTypeId);
    bulkInsertFields(docType, session, docTypeId.intValue());
    bulkInsertAliases(docType, session);
  }

  @Override
  @Transactional
  public DocType getDocTypeDetails(int doctypeid) throws DataAccessLayerException {
    try {
      CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
      CriteriaQuery<DocType> criteriaQuery = criteriaBuilder.createQuery(DocType.class);
      Root<DocType> root = criteriaQuery.from(DocType.class);
      criteriaQuery.select(root);
      criteriaQuery.where(criteriaBuilder.equal(root.get("id"), doctypeid));
      return sessionFactory.getCurrentSession().createQuery(criteriaQuery).getSingleResult();
    } catch (Exception ex) {
      throw new DataAccessLayerException(ex.getMessage());
    }
  }

  @Override
  @Transactional
  public List<DocType> getAllDocTypes() throws DataAccessLayerException {
    try {
      CriteriaQuery<DocType> criteriaQuery = sessionFactory.getCurrentSession().getCriteriaBuilder()
          .createQuery(DocType.class);
      criteriaQuery.from(DocType.class);
      return sessionFactory.getCurrentSession().createQuery(criteriaQuery).getResultList();
    } catch (Exception ex) {
      throw new DataAccessLayerException(ex.getMessage());
    }
  }

  @Override
  @Transactional
  public Fields getFields(int fieldId) throws DataAccessLayerException {
    try {
      CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
      CriteriaQuery<Fields> criteriaQuery = criteriaBuilder.createQuery(Fields.class);
      Root<Fields> root = criteriaQuery.from(Fields.class);
      criteriaQuery.select(root);
      criteriaQuery.where(criteriaBuilder.equal(root.get("id"), fieldId));
      return sessionFactory.getCurrentSession().createQuery(criteriaQuery).getSingleResult();
    } catch (Exception ex) {
      throw new DataAccessLayerException(ex.getMessage());
    }
  }

  @Override
  @Transactional
  public Language getLanguage(int languageId) throws DataAccessLayerException {
    try {
      CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
      CriteriaQuery<Language> criteriaQuery = criteriaBuilder.createQuery(Language.class);
      Root<Language> root = criteriaQuery.from(Language.class);
      criteriaQuery.select(root);
      criteriaQuery.where(criteriaBuilder.equal(root.get("id"), languageId));
      return sessionFactory.getCurrentSession().createQuery(criteriaQuery).getSingleResult();
    } catch (Exception ex) {
      throw new DataAccessLayerException(ex.getMessage());
    }
  }

  @Override
  @Transactional
  public List<Language> getAllLanguages() throws DataAccessLayerException {
    try {
      CriteriaQuery<Language> criteriaQuery = sessionFactory.getCurrentSession()
          .getCriteriaBuilder()
          .createQuery(Language.class);
      criteriaQuery.from(Language.class);
      return sessionFactory.getCurrentSession().createQuery(criteriaQuery).getResultList();
    } catch (Exception ex) {
      throw new DataAccessLayerException(ex.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  @Transactional
  public Map<String, Set<String>> getLanguagesForEachDomain()
      throws DataAccessLayerException {
    final List<Object[]> docNamesAndLanguageNames;
    try {
      docNamesAndLanguageNames = getCurrentOrNewSession()
          .createNativeQuery(
              "SELECT DISTINCT d.Name as docName, l.Name as languageName "
                  + "FROM Fields f, Aliases a, LanguageMaster l, DocTypeMaster d "
                  + "WHERE f.DocTypeId = d.Id and f.id = a.FieldId and a.LanguageId = l.Id "
                  + "ORDER BY d.Name, l.Name")
          .list();
    } catch (final Exception exc) {
      throw new DataAccessLayerException(
          "Error retrieving languages for each domain", exc);
    }
    if (null == docNamesAndLanguageNames) {
      return Collections.emptyMap();
    }

    // Populate this map with the set of languages supported for each domain
    final Map<String, Set<String>> languagesByDomain = new LinkedHashMap<>();
    try {
      docNamesAndLanguageNames.forEach(row -> languagesByDomain
          .computeIfAbsent(getStringAtIndex0(row), k -> new HashSet<>())
          .add(getStringAtIndex1(row)));
    } catch (final Exception exc) {
      throw new DataAccessLayerException("Unexpected result from query: "
          + "SELECT d.Name as docName, l.Name as languageName ..."
          , exc);
    }
    return languagesByDomain;
  }

  /**
   * Returns the current session or a new one as needed.
   *
   * @return a session
   */
  private Session getCurrentOrNewSession() {
    try {
      return sessionFactory.getCurrentSession();
    } catch (final HibernateException exc) {
      LOGGER.warn("Error getting current session.  Creating a new one", exc);
      return sessionFactory.openSession();
    }
  }

  /**
   * Returns the String field from the result row at index 0.
   *
   * @param row a result row from a native query
   * @return the String field from the result row at index 0
   */
  private static String getStringAtIndex0(final Object[] row) {
    return getValidatedColumnValue(row, 0).toString();
  }

  /**
   * Returns the String field from the result row at index 1.
   *
   * @param row a result row from a native query
   * @return the String field from the result row at index 1
   */
  private static String getStringAtIndex1(final Object[] row) {
    return getValidatedColumnValue(row, 1).toString();
  }

  /**
   * Returns the Integer field from the result row at index 1.
   *
   * @param row a result row from a native query
   * @return the Integer field from the result row at index 1
   */
  private static Integer getIntegerAtIndex1(final Object[] row) {
    final Object value = getValidatedColumnValue(row, 1);
    if (!(value instanceof Number)) {
      throw new IllegalStateException(
          "Expected value at index 1 to be a Number but found: "
              + value.getClass().getName());
    }
    return ((Number) value).intValue();
  }

  /**
   * Verifies there is a non-null value for the column in the row at the
   * specified column index (starting from zero).
   *
   * @param row the row data
   * @param columnIndex the column index starting from zero
   * @return the value at the specified column index from the row
   */
  private static Object getValidatedColumnValue(
      final Object[] row,
      final int columnIndex) {
    if (null == row) {
      throw new IllegalArgumentException("row must not be null");
    }
    if (row.length < (columnIndex + 1)) {
      throw new IllegalStateException("row has no column for index "
          + columnIndex);
    }
    final Object value = row[columnIndex];
    if (null == value) {
      throw new IllegalStateException(
          "null value in row at column index " + columnIndex);
    }
    return value;
  }

  /**
   * Performs a bulk insert for fields using the fields provided
   * in the DocType as a template.
   *
   * @param docType the template holding the fields to insert
   * @param session the current session with the transaction for the entire
   *      bulk insert
   */
  private static void bulkInsertFields(
      @Nonnull DocType docType,
      @Nonnull Session session,
      int docTypeId) throws DataAccessLayerException {
    int i = 0;
    for (final Fields fields : docType.getFieldList()) {
      // Use the fields record from the DocType as a template to insert one
      final FieldType proxyFieldType =
          session.load(FieldType.class, fields.getFieldType().getId());
      final FieldDataType proxyFieldDataType =
          session.load(FieldDataType.class, fields.getFieldDataType().getId());
      final Fields newFields = new Fields();
      newFields.setName(fields.getName());
      newFields.setDocTypeId(docTypeId);
      newFields.setFieldType(proxyFieldType);
      newFields.setFieldDataType(proxyFieldDataType);
      newFields.setIsDefaultSelected(fields.getIsDefaultSelected());
      final Number fieldId = (Number) session.save(newFields);
      if (null == fieldId) {
        throw new DataAccessLayerException(
            "No field ID returned from save for Field " + fields.getName());
      }
      fields.setId(fieldId.intValue());
      flushBatch(i, session);
      i++;
    }
  }

  /**
   * Performs a bulk insert for aliases using the fields and aliases provided
   * in the DocType as a template.
   *
   * @param docType the template holding the aliases to insert
   * @param session the current session with the transaction for the entire
   *      bulk insert
   */
  private static void bulkInsertAliases(
      @Nonnull DocType docType,
      @Nonnull Session session) {
    int i = 0;
    for (final Fields fields : docType.getFieldList()) {
      for (final Aliases aliases : fields.getAliasesList()) {
        // Use the aliases from the field from the DocType as template
        final Aliases newAliases = new Aliases();
        newAliases.setAlias(aliases.getAlias());
        newAliases.setLanguageId(aliases.getLanguageId());
        newAliases.setFieldId(fields.getId());
        session.save(newAliases);
        flushBatch(i, session);
        i++;
      }
    }
  }

  /**
   * Flushes the caches during bulk inserts based on the configured JDBC batch
   * size.
   *
   * @param i the count for the number of items inserted thus far
   * @param session the current session with the transaction for the entire
   *      bulk insert
   */
  private static void flushBatch(final int i, @Nonnull final Session session) {
    if ((i % JDBC_BATCH_SIZE) == 0) {
      //flush a batch of inserts and release memory:
      session.flush();
      session.clear();
    }
  }
}
