package com.automationanywhere.cognitive.service.domain;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Specifies the languages that are supported for a specific Domain.
 *
 * @see DomainInfo
 * @author Erik K. Worth
 */
public interface DomainLanguagesInfo {

  /**
   * Returns the domain identifier such as "Invoices".
   * @return the domain identifier
   */
  @Nonnull
  String getName();

  /**
   * Returns the set of language identifiers that are supported as aliases for fields in the Domain.
   * @return the set of language identifiers that are supported as aliases for fields in the Domain
   */
  @Nonnull
  Set<String> getLanguages();

}
