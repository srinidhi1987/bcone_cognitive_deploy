package com.automationanywhere.cognitive.service.domain;

/**
 * Thrown when a domain is import for a domain that already exists with the
 * same name.
 *
 * @author Erik K. Worth
 */
public class DuplicateDomainException extends DomainException {

  private static final long serialVersionUID = 13452345645523145L;

  /**
   * Construct with a message.
   *
   * @param message the error message
   */
  public DuplicateDomainException(final String message) {
    super(message);
  }

  /**
   * Construct with a message and cause.
   *
   * @param message the error message
   * @param cause the root cause of the exception
   */
  public DuplicateDomainException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
