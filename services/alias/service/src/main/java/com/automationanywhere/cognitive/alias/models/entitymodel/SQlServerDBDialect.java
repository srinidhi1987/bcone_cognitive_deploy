package com.automationanywhere.cognitive.alias.models.entitymodel;

import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * Specifies a SQL Server Dialect so that we can support native queries.  This
 * is registered as the value of the hibernate.dialect in the Hibernate
 * configuration.  Have a look at this for clarification:
 * https://stackoverflow.com/questions/27039300/jpa-sql-server-no-dialect-mapping-for-jdbc-type-9
 *
 * @author Erik K. Worth
 */
public class SQlServerDBDialect extends SQLServerDialect {

  public SQlServerDBDialect() {
    super();
    registerHibernateType(Types.NCHAR, StandardBasicTypes.CHARACTER.getName());
    registerHibernateType(Types.NCHAR, 1, StandardBasicTypes.CHARACTER.getName());
    registerHibernateType(Types.NCHAR, 255, StandardBasicTypes.STRING.getName());
    registerHibernateType(Types.NVARCHAR, StandardBasicTypes.STRING.getName());
    registerHibernateType(Types.LONGNVARCHAR, StandardBasicTypes.TEXT.getName());
    registerHibernateType(Types.NCLOB, StandardBasicTypes.CLOB.getName());

  }}
