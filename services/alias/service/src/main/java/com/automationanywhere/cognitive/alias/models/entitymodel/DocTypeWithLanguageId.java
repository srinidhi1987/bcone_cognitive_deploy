package com.automationanywhere.cognitive.alias.models.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity Data Transfer Object for inserting entries in the DocTypeMaster table.
 *
 * @author Erik K. Worth
 */
@Entity
@Table(name = "DocTypeMaster")
public class DocTypeWithLanguageId {
    /** The unique ID for the document type set by the database on insert. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;

    /** The document name. */
    @Column(name="Name")
    private String name;

    /** The language identifier. */
    @Column(name="LanguageId")
    private int languageId;

    /**
     * Returns the unique ID for the document type entry.
     * @return the unique ID for the document type entry
     */
    public int getId() { return this.id; }

    /**
     * Sets the unique ID for the document type entry.
     * @param id the unique ID for the document type entry
     */
    public void setId(int id) { this.id = id; }

    /**
     * Returns the name for the document type.
     * @return the name for the document type
     */
    public String getName() { return this.name ;}

    /**
     * Sets the name for the document type.
     * @param name the name for the document type
     */
    public void setName(String name) { this.name = name; }

    /**
     * Returns the language ID for the document type.
     * @return the language ID for the document type
     */
    public int getLanguageId() { return this.languageId; }

    /**
     * Sets the language ID for the document type.
     * @param languageId the language ID for the document type
     */
    public void setLanguageId(int languageId) { this.languageId = languageId; }
}
