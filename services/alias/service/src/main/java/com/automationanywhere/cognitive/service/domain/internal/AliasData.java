package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.AliasInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable data transfer object for the Alias Info that may be
 * externalized and internalized to and from JSON.
 *
 * @author Erik K. Worth
 */
public class AliasData implements AliasInfo {

  /** The name of the JSON field holding the Alias names. */
  private static final String JSON_NAMES = "names";

  /**
   * The name of the JSON field holding the language used to localize the
   * names for this Alias */
  private static final String JSON_LANGUAGE = "language";

  /**
   * The alternate field names as they might appear in the document for a
   * specific language
   */
  private final Set<String> names;

  /** The the language for which the alternate names are localized */
  private final String language;

  @JsonCreator
  public AliasData(
          @JsonProperty(JSON_NAMES) @Nonnull final Set<String> names,
          @JsonProperty(JSON_LANGUAGE) @Nonnull final String language) {
    this.names = Collections.unmodifiableSet(
            checkNotNull(names, "names must not be null"));
    this.language = checkNotNull(language, "language must not be null");
  }

  @Override
  public String toString() {
    return "AliasData{"
            + JSON_NAMES + '=' + names + ", "
            + JSON_LANGUAGE + "='" + language
            + "'}";
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof AliasInfo)) {
      return false;
    }
    final AliasInfo that = (AliasInfo) other;
    return Objects.equals(this.names, that.getNames())
            && Objects.equals(this.language, that.getLanguage());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.names, this.language);
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_NAMES)
  public Set<String> getNames() {
    return names;
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_LANGUAGE)
  public String getLanguage() {
    return language;
  }
}
