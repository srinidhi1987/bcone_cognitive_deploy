package com.automationanywhere.cognitive.alias.models;

import java.util.List;
import java.util.Objects;

/**
 * Created by Jemin.Shah on 30-11-2016.
 */
public class ImportDomainResponse {

  private boolean success;
  private Object data;
  private List<String> errors;

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    ImportDomainResponse that = (ImportDomainResponse) other;
    return success == that.success
        && Objects.equals(data, that.data)
        && Objects.equals(errors, that.errors);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, data, errors);
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public List<String> getErrors() {
    return errors;
  }

  public void setErrors(List<String> errors) {
    this.errors = errors;
  }
}
