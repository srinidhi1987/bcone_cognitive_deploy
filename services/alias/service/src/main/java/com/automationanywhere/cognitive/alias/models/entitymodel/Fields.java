package com.automationanywhere.cognitive.alias.models.entitymodel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
@Entity
@Table(name = "Fields")
public class Fields {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;
    public int getId() { return this.id ;}
    public void setId(int id) { this.id = id; }

    @Column(name="Name")
    private String name;
    public String getName() {return this.name; }
    public void setName(String name) { this.name = name; }

    @OneToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name="TypeId")
    private FieldType fieldType;
    public FieldType getFieldType() { return this.fieldType; }
    public void setFieldType(FieldType fieldType) { this.fieldType = fieldType; }

    @Column(name="DocTypeId")
    private int docTypeId;
    public int getDocTypeId() { return this.docTypeId ;}
    public void setDocTypeId(int docTypeId) { this.docTypeId = docTypeId; }

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name="FieldId")
    private List<Aliases> aliasesList;
    public List<Aliases> getAliasesList() { return this.aliasesList; }
    public List<Aliases> getAliasesList(int languageId) {
        if (this.aliasesList == null) return new ArrayList<>();

        List<Aliases> result = new ArrayList<>();
        aliasesList.forEach(x -> {
            if (x.getLanguageId() == languageId) {
                result.add(x);
            }
        });
        return result;
    }
    public void setAliasesList(List<Aliases> list) { this.aliasesList = list; }

    @Column(name="IsDefaultSelected")
    private boolean isDefaultSelected;
    public boolean getIsDefaultSelected() { return this.isDefaultSelected; }
    public void setIsDefaultSelected(boolean isDefaultSelected) { this.isDefaultSelected = isDefaultSelected ;}

    @OneToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name="DataTypeId")
    private FieldDataType fieldDataType;
    public FieldDataType getFieldDataType() { return this.fieldDataType; }
    public void setFieldDataType(FieldDataType fieldDataType) { this.fieldDataType = fieldDataType; }
}