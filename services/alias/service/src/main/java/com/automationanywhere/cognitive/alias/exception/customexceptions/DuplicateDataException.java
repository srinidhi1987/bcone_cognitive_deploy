package com.automationanywhere.cognitive.alias.exception.customexceptions;

/**
 * Thrown when there is an attempt to insert a duplicate entry into the
 * persistent store.
 *
 * @author Erik K. Worth
 */
public class DuplicateDataException extends DataAccessLayerException {

  /**
   * Serialization identifier.
   */
  private static final long serialVersionUID = 544011439854320957L;

  /**
   * Construct with a message.
   *
   * @param message the exception message
   */
  public DuplicateDataException(final String message) {
    super(message);
  }

  /**
   * Construct with a message and cause.
   *
   * @param message the exception message
   * @param cause the exception cause
   */
  public DuplicateDataException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
