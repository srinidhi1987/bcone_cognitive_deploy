package com.automationanywhere.cognitive.alias.models.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
@Entity
@Table(name = "FieldTypeMaster")
public class FieldType {
    @Id
    @Column(name= "Id")
    private int id;
    public int getId() { return this.id; }
    public void setId(int id) { this.id = id; }

    @Column(name="Name")
    private String name;
    public String getName() { return this.name ;}
    public void setName(String name) { this.name = name; }
}
