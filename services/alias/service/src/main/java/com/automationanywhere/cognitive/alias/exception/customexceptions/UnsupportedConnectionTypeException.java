package com.automationanywhere.cognitive.alias.exception.customexceptions;

public class UnsupportedConnectionTypeException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -873596854078095867L;

	public UnsupportedConnectionTypeException(String message) {
        super(message);
    }

}
