package com.automationanywhere.cognitive.alias.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class DataNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 7645578868804405147L;

  @Override
  public synchronized Throwable fillInStackTrace() {
    return this;
  }
}
