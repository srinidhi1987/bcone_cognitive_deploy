package com.automationanywhere.cognitive.service.domain;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Specifies the Alias information for a Domain Field for a specific supported language.
 *
 * <p>Each Field has a data type and a number of Aliases for the data label that might appear in
 * text-extracted documents (e.g. for documents in the Invoices Domain, the field identified as the
 * "Invoice Number" might appear in some documents as "Invoice #").
 * </p>
 * @author Erik K. Worth
 */
public interface AliasInfo {

  /**
   * Returns the set of alternate names for the field localized according to associated language.
   * @return the set of alternate names for the field localized according to assiciated language
   */
  @Nonnull
  Set<String> getNames();

  /**
   * Returns the language for which the alternate names are localized.
   * @return the language for which the alternate names are localized
   */
  @Nonnull
  String getLanguage();
}
