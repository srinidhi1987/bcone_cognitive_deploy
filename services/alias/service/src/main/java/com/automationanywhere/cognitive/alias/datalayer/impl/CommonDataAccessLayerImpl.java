package com.automationanywhere.cognitive.alias.datalayer.impl;

import com.automationanywhere.cognitive.alias.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.alias.models.entitymodel.EntityModelBase;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 11-12-2016.
 */
public class CommonDataAccessLayerImpl implements CommonDataAccessLayer<EntityModelBase,CustomEntityModelBase> {
    private SessionFactory sessionFactory;
    public CommonDataAccessLayerImpl(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    @Override
    public boolean InsertRow(EntityModelBase entityModel)throws DataAccessLayerException
    {
        try{
            sessionFactory.getCurrentSession().save(entityModel);
            return  true;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Transactional
    @Override
    public boolean UpdateRow(EntityModelBase entityModel)throws DataAccessLayerException
    {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam = entityModel.getUpdateQuery(queryString);
            if(!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                query.executeUpdate();
                return true;
            }
            else
                return false;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Object> SelectRows(EntityModelBase queryDetail) throws DataAccessLayerException
    {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam  = queryDetail.getSelectQuery(queryString);
            if (!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                List<Object> resultset = query.list();
                return resultset;
            }
            else
                return null;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Transactional
    public boolean DeleteRows(EntityModelBase queryDetail) throws DataAccessLayerException
    {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam = queryDetail.getDeleteQuery(queryString);
            if (!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                query.executeUpdate();
                return true;
            }
            else
                return false;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Object> ExecuteCustomEntityQuery(CustomEntityModelBase queryDetail)throws DataAccessLayerException {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam  = queryDetail.getCustomQuery(queryString);
            if (!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                List<Object> resultset = query.list();
                return  queryDetail.objectToClass(resultset);
            }
            else
                return null;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Object> CustomQueryExecute(StringBuilder queryString,Map<String,Object> inputParam) throws DataAccessLayerException{

        try {
            if (!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                List<Object> resultset = query.list();
                return resultset;
            }
            else
                return null;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    private void fillQueryParameter(Map<String,Object> inputParam,Query query)
    {
        for (Map.Entry<String, Object> entry : inputParam.entrySet())
        {
            query.setParameter(entry.getKey(),entry.getValue());
        }
    }
}
