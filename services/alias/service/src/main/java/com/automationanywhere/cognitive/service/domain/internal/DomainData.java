package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.DomainFieldInfo;
import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable data transfer object for the Domain Info that may be
 * externalized and internalized to and from JSON.
 *
 * @author Erik K. Worth
 */
public class DomainData extends DomainLanguagesData implements DomainInfo {

  /**
   * The name of the JSON field holding information on the fields found in
   * documents of this Domain
   */
  private static final String JSON_FIELDS = "fields";

  /** The information for fields found in documents for this Domain */
  private final List<DomainFieldInfo> fields;

  /**
   * Construct a Domain from its elements.
   *
   * @param name the domain identifier
   * @param languages the identifiers for languages supported for the Domain
   * @param fields the information for fields found in documents for this Domain
   */
  @JsonCreator
  public DomainData(
          @JsonProperty(JSON_NAME) @Nonnull final String name,
          @JsonProperty(JSON_LANGUAGES) @Nonnull final Set<String> languages,
          @JsonProperty(JSON_FIELDS) @Nonnull final List<DomainFieldData> fields) {
    super(name, languages);
    this.fields = Collections.unmodifiableList(
            checkNotNull(fields, "fields must not be null"));
  }

  @Override
  public String toString() {
    return "DomainData{"
            + JSON_NAME + "='" + getName() + "', "
            + JSON_LANGUAGES + "=" + getLanguages() + ", "
            + JSON_FIELDS + "=" + fields
            + '}';
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof DomainInfo)) {
      return false;
    }
    final DomainInfo that = (DomainInfo) other;
    return super.equals(other)
            && Objects.equals(this.fields, that.getFields());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), getFields());
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_FIELDS)
  public List<DomainFieldInfo> getFields() {
    return fields;
  }
}
