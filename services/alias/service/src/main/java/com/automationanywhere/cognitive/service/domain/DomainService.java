package com.automationanywhere.cognitive.service.domain;

import com.automationanywhere.cognitive.service.domain.internal.DomainLanguagesData;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Specifies the operations that may be performed for a Domain.  A domain is
 * the set of fields and associated aliases describing the expected fields
 * that may be extracted from processed documents.
 */
public interface DomainService {

  /**
   * Returns the full Domain information for the specified domain name with
   * all fields and field aliases for all languages.
   *
   * @param domainName the text identifier for the domain like "Invoices"
   * @return the full Domain information for the specified domain name
   * @throws NoSuchDomainException thrown when the specified domainName does
   *     not identify an existing Domain
   * @throws DomainException thrown when there is an error requesting the
   * domain information
   */
  @Nonnull
  DomainInfo getDomain(@Nonnull String domainName) throws DomainException;

  /**
   * Returns a list of entries for each domain identifying the domain name and
   * the languages supported by the domain sorted in alphabetical order by
   * domain name.
   *
   * @return a list of entries for each domain identifying the domain name and
   *     the languages supported by the domain  sorted in alphabetical order by
   *     domain name
   * @throws DomainException thrown when there is an error retrieving the
   *     domain information
   */
  @Nonnull
  List<DomainLanguagesInfo> getAllDomainsWithLanguages() throws DomainException;

  /**
   * Imports the provided domain information into the persistent store provided
   * the domain meets the internal consistency constraints and the domain name
   * is unique.
   *
   * @param domainInfo the new domain information
   * @throws
   * @throws DomainException thrown when there is an error importing the data
   */
  void importDomain(@Nonnull DomainInfo domainInfo) throws DomainException;
}
