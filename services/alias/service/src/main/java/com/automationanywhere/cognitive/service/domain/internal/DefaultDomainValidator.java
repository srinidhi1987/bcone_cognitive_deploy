package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.AliasInfo;
import com.automationanywhere.cognitive.service.domain.DomainFieldInfo;
import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The default implementation of the component that validates the Domain
 * information to make sure it is internally consistent based on a set of rules.
 *
 * @author Erik K. Worth
 */
@Component
public class DefaultDomainValidator implements DomainValidator {

  /** The error message template for reporting non-unique fields. */
  static final String ERR_FIELD_NOT_UNIQUE = "Field is not unique: %s";

  /**
   * The error message template for reporting non-unique aliases for a language.
   */
  static final String ERR_ALIAS_NOT_UNIQUE_FOR_LANGUAGE =
          "Alias is not unique for language %s: %s";

  /**
   * The error message template for reporting aliases that match the field name.
   */
  static final String ERR_ALIAS_MATCHES_FIELD_NAME =
          "Alias matches field name: %s";

  /**
   * The error message for fields that are missing required languages for the
   * domain.
   */
  static final String ERR_FIELD_MISSING_ALIASES_FOR_LANGUAGES =
          "Field %s missing aliases for language(s): %s";

  /**
   * The error message for fields that have aliases with languages that are not
   * allowed (the language is not provided in the top-level set of languages
   * for the domain).
   */
  static final String ERR_FIELD_HAS_UNEXPECTED_LANGUAGE_ALIASES =
          "Field %s has aliases defined for unexpected languages: %s";

  @Override
  public void checkValid(@Nonnull final DomainInfo domainInfo)
          throws InvalidDomainException {
    checkNotNull(domainInfo, "domainInfo must not be null");
    // Collect all the errors into this list
    final List<String> errors = new LinkedList<>();

    // Make sure all the fields have unique names
    checkUniqueFieldNames(domainInfo, errors);

    // Make sure all the aliases have unique names
    checkUniqueAliases(domainInfo, errors);

    // Make sure each field has aliases for all the required languages
    domainInfo.getFields().forEach(field ->
            checkFieldHasAliasesForAllLanguages(
                    domainInfo.getLanguages(),
                    field,
                    errors));

    // Make sure fields don't have aliases for unexpected languages
    // All the languages specified for aliases need to be enumerated at the
    // top-level of the domain
    domainInfo.getFields().forEach(field ->
            checkFieldForUnexpectedLanguageAliases(
                    domainInfo.getLanguages(),
                    field,
                    errors));

    // Report all the validation errors together
    if (!errors.isEmpty()) {
      throw new InvalidDomainException("Error to validate the domain to import.", errors);
    }
  }

  /**
   * Identify the fields that do not have unique names based on a case-
   * insensitive comparison.
   *
   * @param domainInfo the domain information to check
   * @param errors the collection used to report all error messages for
   *     non-unique fields
   */
  private void checkUniqueFieldNames(
          final DomainInfo domainInfo,
          final Collection<String> errors) {
    final Set<String> fieldNames = new HashSet<>();
    for (final DomainFieldInfo field : domainInfo.getFields()) {
      // Use a case-insensitive comparison
      final String fieldName = field.getName().toLowerCase();
      if (fieldNames.contains(fieldName)) {
        errors.add(String.format(ERR_FIELD_NOT_UNIQUE, field.getName()));
      } else {
        fieldNames.add(fieldName);
      }
    }
  }

  /**
   * Identify the aliases that do not have unique names based on a case-
   * insensitive comparison against the field name and against other aliases
   * with the same language.
   *
   * @param domainInfo the domain information to check
   * @param errors the collection used to report all the error messages for
   *     non-unique aliases
   */
  private void checkUniqueAliases(
          final DomainInfo domainInfo,
          final Collection<String> errors) {
    // Group all the aliases by language
    Map<String, List<AliasInfo>> aliasesByLanguage =
            domainInfo.getFields().stream()
            .flatMap(field -> field.getAliases().stream())
            .collect(Collectors.groupingBy(AliasInfo::getLanguage));
    // Make sure there are no duplicate alias names for a given language
    aliasesByLanguage.forEach((key, value) -> checkUniqueAliasesForLanguage(
            key,
            value,
            errors));
  }

  /**
   * Identify the aliases that match the field name and report them as errors.
   *
   * @param field the field to check
   * @param errors the collection used to report errors
   */
  private void checkAliasesDoNotMatchFieldName(
          final DomainFieldInfo field,
          final Collection<String> errors) {
    final String fieldName = field.getName();
    final Optional<String> aliasMatchingFieldName = field.getAliases().stream()
            .flatMap(aliasInfo -> aliasInfo.getNames().stream())
            .filter(aliasName -> aliasName.equalsIgnoreCase(fieldName))
            .findAny();
    aliasMatchingFieldName.ifPresent(aliasName ->
            errors.add(String.format(ERR_ALIAS_MATCHES_FIELD_NAME, aliasName)));
  }

  /**
   * Make sure all the aliases for the same language are unique and collect
   * those that are not.
   *
   * @param language the common language for all the aliases
   * @param aliases aliases that share the same language for all fields
   * @param errors the collection used to report error message
   */
  private void checkUniqueAliasesForLanguage(
          final String language,
          final Collection<AliasInfo> aliases,
          final Collection<String> errors) {
    final Set<String> aliasSet = new HashSet<>();
    for (final AliasInfo aliasInfo : aliases) {
      for (final String aliasName : aliasInfo.getNames()) {
        final String lowerAlias = aliasName.toLowerCase();
        if (aliasSet.contains(lowerAlias)) {
          errors.add(String.format(ERR_ALIAS_NOT_UNIQUE_FOR_LANGUAGE,
                  language, aliasName));
        } else {
          aliasSet.add(lowerAlias);
        }
      }
    }
  }

  /**
   * Makes sure the field has aliases for each of the required languages and
   * reports errors for those that do not.
   *
   * @param requiredLanguages the languages required for the domain
   * @param fieldInfo the information for the field
   * @param errors the collection used to report error messages
   */
  private void checkFieldHasAliasesForAllLanguages(
          final Set<String> requiredLanguages,
          final DomainFieldInfo fieldInfo,
          final Collection<String> errors) {
    final Set<String> providedLanguages = fieldInfo.getAliases().stream()
            .map(AliasInfo::getLanguage)
            .collect(Collectors.toSet());
    final Set<String> missingLanguages = new HashSet<>(requiredLanguages);
    missingLanguages.removeAll(providedLanguages);
    if (!missingLanguages.isEmpty()) {
      errors.add(String.format(ERR_FIELD_MISSING_ALIASES_FOR_LANGUAGES,
              fieldInfo.getName(), missingLanguages.toString()));
    }
  }

  /**
   * Makes sure the field has no aliases except for the allowed languages and
   * reports errors for those that do not.
   *
   * @param allowedLanguages the languages allowed for the domain
   * @param fieldInfo the information for the field
   * @param errors the collection used to report error messages
   */
  private void checkFieldForUnexpectedLanguageAliases(
          final Set<String> allowedLanguages,
          final DomainFieldInfo fieldInfo,
          final Collection<String> errors) {
    final Set<String> providedLanguages = fieldInfo.getAliases().stream()
            .map(AliasInfo::getLanguage)
            .collect(Collectors.toSet());
    providedLanguages.removeAll(allowedLanguages);
    if (!providedLanguages.isEmpty()) {
      errors.add(String.format(ERR_FIELD_HAS_UNEXPECTED_LANGUAGE_ALIASES,
              fieldInfo.getName(), providedLanguages.toString()));
    }
  }

}
