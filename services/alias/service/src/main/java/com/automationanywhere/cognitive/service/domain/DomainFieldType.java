package com.automationanywhere.cognitive.service.domain;

/**
 * Enumerates the type top-level types of fields.
 *
 * @author Erik K. Worth
 */
public enum DomainFieldType {

  /**
   * Identifies a top-level field in a document such as an invoice number or customer name
   */
  FORM_FIELD,

  /**
   * Identifies a column value in a table such as for columns in the line items on an invoice or purchase
   * order
   */
  TABLE_COLUMN_FIELD
}
