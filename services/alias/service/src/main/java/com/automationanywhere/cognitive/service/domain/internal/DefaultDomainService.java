package com.automationanywhere.cognitive.service.domain.internal;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DuplicateDataException;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Language;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.automationanywhere.cognitive.service.domain.DomainLanguagesInfo;
import com.automationanywhere.cognitive.service.domain.DomainService;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.NoSuchDomainException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The default implementation for the Domain Service that manages domain
 * metadata in the persistent store.
 *
 * @author Erik K. Worth
 */
@Service
public class DefaultDomainService implements DomainService {

  /** Logger for this class */
  private static final AALogger LOGGER =
          AALogger.create(DefaultDomainService.class);

  /**
   * The existing Alias service that retrieves document information from the
   * persistent store.
   */
  private final AliasService aliasService;

  /** The component able to validate a domain for internal consistency */
  private final DomainValidator domainValidator;

  @Autowired
  public DefaultDomainService(
          @Qualifier("aliasService") @Nonnull final AliasService aliasService,
          @Qualifier("defaultDomainValidator") @Nonnull final DomainValidator domainValidator) {
    this.aliasService =
            checkNotNull(aliasService, "aliasService must not be null");
    this.domainValidator =
            checkNotNull(domainValidator, "domainValidator must not be null");
  }

  @Transactional
  @Nonnull
  @Override
  public DomainInfo getDomain(@Nonnull final String domainName)
          throws DomainException {
    checkNotNull(domainName, "domainName must not be null");
    LOGGER.debug("Retrieving domain for name '{}'", domainName);

    // Get the map of DocTypeIds keyed by name
    final Map<String, Integer> docTypeIdsByName;
    try {
      docTypeIdsByName = aliasService.getDocTypeNamesWithIds();
    } catch (final Exception exc) {
      throw new DomainException("Error retrieving doc names with IDs", exc);
    }
    LOGGER.debug("Using this domain name/id mapping: {}",
            docTypeIdsByName);

    // Get the ID for the requested domain name and throw if not present
    final Integer docTypeId = docTypeIdsByName.get(domainName);
    if (null == docTypeId) {
      LOGGER.info("Requested Domain not found: '{}'", domainName);
      throw new NoSuchDomainException(domainName);
    }

    // Get all the supported languages
    final List<Language> languages = getLanguages();
    final Map<Integer, String> languageNamesById = languages.stream()
            .collect(Collectors.toMap(Language::getId, Language::getName));
    LOGGER.debug("Using this language id/name mapping: {}",
            languageNamesById);

    // Load the document details from all the relevant tables
    final DocType docType;
    try {
      docType = aliasService.getDocTypeDetails(docTypeId);
    } catch (final Exception exc) {
      throw new DomainException("Error retrieving doc type for ID "
              + domainName + '[' + docTypeId + ']', exc);
    }
    if (null == docType) {
      throw new NoSuchDomainException(domainName);
    }

    // Return the Domain metadata from the retrieved DocType
    final DomainInfo domainInfo =
            DomainDataMapper.toDomainData(languageNamesById, docType);
    LOGGER.debug("Returning domain result {}", domainInfo);
    return domainInfo;
  }

  @Transactional
  @Override
  @Nonnull
  public List<DomainLanguagesInfo> getAllDomainsWithLanguages()
          throws DomainException {

    final Map<String, Set<String>> languagesByDomain;
    try {
      languagesByDomain = aliasService.getLanguagesForEachDomain();
    } catch (final Exception exc) {
      throw new DomainException(
              "Error retrieving languages for each domain", exc);
    }
    final List<DomainLanguagesInfo> result = languagesByDomain.entrySet()
            .stream()
            .map(e -> new DomainLanguagesData(e.getKey(), e.getValue()))
            .collect(Collectors.toList());
    LOGGER.debug("Languages for Domains: {}", result);
    return result;
  }

  @Transactional
  @Override
  public void importDomain(@Nonnull final DomainInfo domainInfo) throws DomainException {
    checkNotNull(domainInfo, "domainInfo must not be null");
    LOGGER.debug("Importing domain metadata for domain {}", domainInfo.getName());

    List<DocType> allDocTypes;
    try {
      allDocTypes = aliasService.getAllDocTypes();
    } catch (DataAccessLayerException e) {
      throw new DomainException("Error to retrieve DocTypes", e);
    }

    for (DocType docType : allDocTypes) {
      if (docType.getName().equalsIgnoreCase(domainInfo.getName())) {
        throw new InvalidDomainException(format("Domain %s already exists", domainInfo.getName()));
      }
    }

    // Validate domain - this throws InvalidDomainException if it is not valid
    domainValidator.checkValid(domainInfo);

    // Get all the supported languages
    final List<Language> languages = getLanguages();
    final Map<String, Integer> languageIdsByName = languages.stream()
            .collect(Collectors.toMap(Language::getName, Language::getId));
    LOGGER.debug("Using this language name/ID mapping: {}",
            languageIdsByName);

    // Get the Field Types
    final Map<String, Integer> fieldTypeIdsByName;
    try {
      fieldTypeIdsByName = aliasService.getFieldTypeNamesWithIds();
    } catch (final Exception exc) {
      throw new DomainException("Error retrieving field types", exc);
    }
    LOGGER.debug("Using this field type name/ID mapping: {}",
            fieldTypeIdsByName);

    // Get the Field Data Types
    final Map<String, Integer> fieldDataTypeIdsByName;
    try {
      fieldDataTypeIdsByName = aliasService.getFieldDataTypeNamesWithIds();
    } catch (final Exception exc) {
      throw new DomainException("Error retrieving field types", exc);
    }
    LOGGER.debug("Using this field data type name/ID mapping: {}",
            fieldDataTypeIdsByName);

    final DocType docType = DomainDataMapper.toDocType(
            languageIdsByName,
            fieldTypeIdsByName,
            fieldDataTypeIdsByName,
            domainInfo);

    try {
      aliasService.bulkInsertDocTypeFieldsAndAliases(docType);
    } catch (final DuplicateDataException exc) {
      throw new DuplicateDomainException("Domain already exists with the name '"
              + domainInfo.getName() + "'", exc);
    } catch (final Exception exc) {
      throw new DomainException("Error importing domain '"
              + domainInfo.getName() + "'", exc);
    }
  }

  private List<Language> getLanguages() {
    final List<Language> languages;
    try {
      languages = aliasService.getAllLanguages();
    } catch (final Exception exc) {
      throw new DomainException("Error retrieving all languages", exc);
    }
    return languages;
  }

}
