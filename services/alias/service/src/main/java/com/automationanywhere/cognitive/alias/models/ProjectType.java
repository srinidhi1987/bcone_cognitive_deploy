package com.automationanywhere.cognitive.alias.models;

import com.automationanywhere.cognitive.alias.models.entitymodel.Language;
import java.util.List;

/**
 * Created by Purnil.Soni on 06-12-2016.
 */
public class ProjectType {

    private final String id;
    private final String name;
    private final String description;
    private final List<StorageFieldItem> fields;
    private final List<Language> languages;

    public ProjectType(
        final String id,
        final String name,
        final String description,
        final List<StorageFieldItem> fields,
        final List<Language> languages
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.fields = fields;
        this.languages = languages;
    }

    /**
     * Returns the id.
     *
     * @return the value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the name.
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the description.
     *
     * @return the value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the fields.
     *
     * @return the value of fields
     */
    public List<StorageFieldItem> getFields() {
        return fields;
    }

  /**
   * Returns the languages.
   *
   * @return the value of languages
   */
  public List<Language> getLanguages() {
    return languages;
  }
}
