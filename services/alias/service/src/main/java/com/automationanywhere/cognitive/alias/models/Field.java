package com.automationanywhere.cognitive.alias.models;

/**
 * Created by Purnil.Soni on 05-12-2016.
 */
public class Field {
    private int fieldId;
    public int getFieldId() { return this.fieldId; }
    public void setFieldId(int value) { this.fieldId = value; }

    private String fieldName;
    public String getFieldName() { return this.fieldName; }
    public void setFieldName(String value) { this.fieldName = value; }

    private String fieldType;
    public String getFieldType(){return this.fieldType;}
    public void setFieldType(String value){this.fieldType = value;}
}
