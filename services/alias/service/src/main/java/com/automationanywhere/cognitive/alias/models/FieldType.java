package com.automationanywhere.cognitive.alias.models;

/**
 * Created by Purnil.Soni on 06-12-2016.
 */
public enum FieldType {
    FormField,
    TableField
}
