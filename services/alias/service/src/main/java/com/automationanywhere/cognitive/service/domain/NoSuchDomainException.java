package com.automationanywhere.cognitive.service.domain;

/**
 * Exception thrown when there is an attempt to retrieve Domain information
 * for a domain name that does not identify an existing Domain.
 *
 * @author Erik K. Worth
 */
public class NoSuchDomainException extends DomainException {

  private static final long serialVersionUID = 3423452345645562345L;

  /**
   * Construct with a message.
   *
   * @param message the error message
   */
  public NoSuchDomainException(final String message) {
    super(message);
  }

  /**
   * Construct with a message and cause.
   *
   * @param message the error message
   * @param cause the root cause of the exception
   */
  public NoSuchDomainException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
