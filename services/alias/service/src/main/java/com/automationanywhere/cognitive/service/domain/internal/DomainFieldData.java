package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.AliasInfo;
import com.automationanywhere.cognitive.service.domain.DomainFieldInfo;
import com.automationanywhere.cognitive.service.domain.DomainFieldType;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable data transfer object for the Domain Field Info that may be
 * externalized and internalized to and from JSON.
 *
 * @author Erik K. Worth
 */
public class DomainFieldData implements DomainFieldInfo {

  /** The name of the JSON field holding the Field name. */
  private static final String JSON_NAME = "name";

  /** The name of the JSON field holding the field type for the Field. */
  private static final String JSON_TYPE = "type";

  /** The name of the JSON field holding the value format for the Field. */
  private static final String JSON_FORMAT = "format";

  /**
   * The name of the JSON field indicating whether or not the field is
   * "Checked" in the UI by default.
   */
  private static final String JSON_DEFAULT = "default";

  /**
   * The name of the JSON field holding alternate field names for a language.
   */
  private static final String JSON_ALIASES = "aliases";

  /** The unique ID for the field. */
  private final String name;

  /** The field type for the domain field. */
  private final DomainFieldType type;

  /**
   * The type of formatting used for the field from a set of supported values.
   */
  private final String format;

  /**
   * Set to <code>true</code> when the field is expected to be "Checked" by
   * default as a required field when a Learning Instance is created in user
   * interface for documents of the type described by the Domain, and
   * <code>false</code> otherwise.
   */
  private final boolean isDefault;

  /** The set of alias information for this Domain Field. */
  private final Set<AliasInfo> aliases;

  /**
   * Construct from elements.
   *
   * @param name the field name as it might appear in the document and the
   *     unique ID for the field
   * @param type the field type for the domain field
   * @param format the type of formatting used for the field from a set of
   *     supported values
   * @param isDefault set to <code>true</code> when the field is expected to be
   *     "Checked" by default as a required field when a Learning Instance is
   *     created in user interface for documents of the type described by the
   *     Domain, and <code>false</code> otherwise
   * @param aliases the set of alias information for this Domain Field
   */
  public DomainFieldData(
          @JsonProperty(JSON_NAME) @Nonnull final String name,
          @JsonProperty(JSON_TYPE) @Nonnull final DomainFieldType type,
          @JsonProperty(JSON_FORMAT) @Nonnull final String format,
          @JsonProperty(JSON_DEFAULT) final boolean isDefault,
          @JsonProperty(JSON_ALIASES) @Nonnull final Set<AliasData> aliases) {
    this.name = checkNotNull(name, "name must not be null");
    this.type = checkNotNull(type, "type must not be null");
    this.format = checkNotNull(format, "format must not be null");
    this.isDefault = isDefault;
    this.aliases = Collections.unmodifiableSet(
            checkNotNull(aliases, "aliases must not be null"));
  }

  @Override
  public String toString() {
    return "DomainFieldData{"
            + JSON_NAME + "='" + name + "', "
            + JSON_TYPE + "=" + type + ", "
            + JSON_FORMAT + "='" + format + "', "
            + JSON_DEFAULT + "=" + isDefault + ", "
            + JSON_ALIASES + "=" + aliases
            + '}';
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof DomainFieldInfo)) {
      return false;
    }
    final DomainFieldInfo that = (DomainFieldInfo) other;
    return this.isDefault == that.isDefault()
            && Objects.equals(this.name, that.getName())
            && Objects.equals(this.type, that.getType())
            && Objects.equals(this.format, that.getFormat())
            && Objects.equals(this.aliases, that.getAliases());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName(), getType(), getFormat(), isDefault(),
            getAliases());
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_NAME)
  public String getName() {
    return name;
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_TYPE)
  public DomainFieldType getType() {
    return type;
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_FORMAT)
  public String getFormat() {
    return format;
  }

  @Override
  @JsonProperty(JSON_DEFAULT)
  public boolean isDefault() {
    return isDefault;
  }

  @Nonnull
  @Override
  @JsonProperty(JSON_ALIASES)
  public Set<AliasInfo> getAliases() {
    return aliases;
  }
}
