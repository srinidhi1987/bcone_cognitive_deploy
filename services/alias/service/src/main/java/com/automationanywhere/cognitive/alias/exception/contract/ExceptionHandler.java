package com.automationanywhere.cognitive.alias.exception.contract;

import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.NoSuchDomainException;
import com.fasterxml.jackson.core.JsonProcessingException;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface ExceptionHandler {
    void handleDataAccessLayerException(Exception dataAccessLayerException, Request request, Response response);
    void handleDataNotFoundException(Exception dataNotFoundException, Request request, Response response);
    void handleDomainException(
            Exception domainException,
            Request request,
            Response response);
    void handleDuplicateDomainException(
            Exception domainException,
            Request request,
            Response response);
    void handleException(Exception dataNotFoundException, Request request, Response response);
    void handleInvalidDomainException(
            InvalidDomainException invalidDomainException,
            Request request,
            Response response);
    void handleJsonProcessingException(
            Exception jsonProcessingException,
            Request request,
            Response response);
    void handleNoSuchDomainException(
            Exception noSuchDomainException,
            Request request,
            Response response);
    void handleDomainCodecException(
            Exception domainCodecException,
            Request request,
            Response response);
}
