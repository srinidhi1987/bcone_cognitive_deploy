package com.automationanywhere.cognitive.alias.health.connections;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;

public interface HealthCheckConnection {
	void checkConnection() throws DataAccessLayerException;
}
