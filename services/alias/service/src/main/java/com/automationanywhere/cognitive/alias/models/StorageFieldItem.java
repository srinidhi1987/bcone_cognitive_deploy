package com.automationanywhere.cognitive.alias.models;

import java.util.List;

/**
 * Created by Purnil.Soni on 05-12-2016.
 */
public class StorageFieldItem {

    private final String id;
    private final String name;
    private final String description;
    private final String fieldType;
    private final List<String> aliases;
    private final boolean isDefaultSelected;
    private final String dataType;

    public StorageFieldItem(String id, String name, String description, String fieldType,
        List<String> aliases, boolean isDefaultSelected, String dataType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.fieldType = fieldType;
        this.aliases = aliases;
        this.isDefaultSelected = isDefaultSelected;
        this.dataType = dataType;
    }

    /**
     * Returns the id.
     *
     * @return the value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the name.
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the description.
     *
     * @return the value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the fieldType.
     *
     * @return the value of fieldType
     */
    public String getFieldType() {
        return fieldType;
    }

    /**
     * Returns the aliases.
     *
     * @return the value of aliases
     */
    public List<String> getAliases() {
        return aliases;
    }

    /**
     * Returns the isDefaultSelected.
     *
     * @return the value of isDefaultSelected
     */
    public boolean getIsDefaultSelected() {
        return isDefaultSelected;
    }

    /**
     * Returns the dataType.
     *
     * @return the value of dataType
     */
    public String getDataType() {
        return dataType;
    }
}
