package com.automationanywhere.cognitive.alias.parser.contract;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface JsonParser {

  String getResponse(Object response);
}
