package com.automationanywhere.cognitive.alias.models.entitymodel;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
@Entity
@Table(name = "DocTypeMaster")
public class DocType {
    @Id
    @Column(name="Id")
    private int id;
    public int getId() { return this.id; }
    public void setId(int id) { this.id = id; }

    @Column(name="Name")
    private String name;
    public String getName() { return this.name ;}
    public void setName(String name) { this.name = name; }

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name="DocTypeId")
    List<Fields> fieldList;
    public List<Fields> getFieldList() { return this.fieldList; }
    public void setFieldList(List<Fields> list) { this.fieldList = list; }
}
