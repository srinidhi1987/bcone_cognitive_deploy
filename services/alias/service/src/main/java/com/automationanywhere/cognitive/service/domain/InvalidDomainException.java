package com.automationanywhere.cognitive.service.domain;

import java.util.List;

/**
 * Exception thrown when the provided domain data is not valid either because
 * it cannot be parsed from its JSON representation or because there is a
 * violation of one of the validation rules.
 *
 * @author Erik K. Worth
 */
public class InvalidDomainException extends DomainException {

  private static final long serialVersionUID = 342345234564765345L;

  private List<String> errors;

  public InvalidDomainException(final String message) {
    super(message);
  }

  public InvalidDomainException(final String message, List<String> errors) {
    super(message);
    this.errors = errors;
  }

  public InvalidDomainException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public List<String> getErrors() {
    return errors;
  }
}
