package com.automationanywhere.cognitive.alias.exception.impl;

import com.automationanywhere.cognitive.alias.exception.contract.ExceptionHandler;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.alias.models.CustomResponse;
import com.automationanywhere.cognitive.alias.models.ImportDomainResponse;
import com.automationanywhere.cognitive.alias.parser.contract.JsonParser;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.NoSuchDomainException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import spark.Request;
import spark.Response;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CONFLICT;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class ExceptionHandlerImlp implements ExceptionHandler
{
    private AALogger log = AALogger.create(getClass());

    @Autowired
    JsonParser jsonParser;
    @Override
    public void handleDataAccessLayerException(Exception dataAccessLayerException, Request request, Response response)
    {
        log.error(dataAccessLayerException.getMessage(), dataAccessLayerException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        customResponse.setErrors("Internal Server Error.");
        response.status(500);
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleDataNotFoundException(Exception dataNotFoundException, Request request, Response response)
    {
        log.error(dataNotFoundException.getMessage(), dataNotFoundException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(404);
        customResponse.setErrors("Data is not available");
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleDomainException(
            Exception domainException,
            Request request,
            Response response) {
        log.error("Error processing domain", domainException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(SC_INTERNAL_SERVER_ERROR);
        customResponse.setErrors("Error processing domain: " +
                domainException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type(MediaType.APPLICATION_JSON_VALUE);
    }

    @Override
    public void handleDuplicateDomainException(
            Exception domainException,
            Request request,
            Response response) {
        log.error("Duplicate domain", domainException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(SC_CONFLICT);
        customResponse.setErrors("Duplicate domain: " +
                domainException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type(MediaType.APPLICATION_JSON_VALUE);
    }

    @Override
    public void handleException(Exception exception, Request request, Response response)
    {
        log.error(exception.getMessage(), exception);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(500);
        customResponse.setErrors("Internal Server Error.");
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

  @Override
  public void handleInvalidDomainException(
      final InvalidDomainException exception,
      final Request request,
      final Response response
  ) {
    log.error(exception.getMessage(), exception);

    List<String> errors = new ArrayList<>();
    if (exception.getErrors() != null) {
      errors.addAll(exception.getErrors());
    } else {
      errors.add(exception.getMessage());
    }

    ImportDomainResponse importDomainResponse = new ImportDomainResponse();
    importDomainResponse.setSuccess(false);
    importDomainResponse.setErrors(errors);

    response.body(jsonParser.getResponse(importDomainResponse));
    response.status(SC_BAD_REQUEST);
    response.type(MediaType.APPLICATION_JSON_VALUE);
  }

    @Override
    public void handleJsonProcessingException(
            Exception jsonProcessingException,
            Request request,
            Response response) {
        log.error("Error Writing JSON", jsonProcessingException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(SC_INTERNAL_SERVER_ERROR);
        customResponse.setErrors("Error Writing JSON: " +
                jsonProcessingException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type(MediaType.APPLICATION_JSON_VALUE);
    }

    @Override
    public void handleNoSuchDomainException(
            Exception noSuchDomainException,
            Request request,
            Response response) {
        log.error("No Such Domain", noSuchDomainException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(SC_NOT_FOUND);
        customResponse.setErrors("No Such Domain: " +
                noSuchDomainException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type(MediaType.APPLICATION_JSON_VALUE);
    }

    @Override
    public void handleDomainCodecException(
            Exception domainCodecException,
            Request request,
            Response response) {
        log.error("Error Decoding Domain", domainCodecException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(SC_BAD_REQUEST);
        customResponse.setErrors("Error Decoding Domain: " +
                domainCodecException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type(MediaType.APPLICATION_JSON_VALUE);
    }

}
