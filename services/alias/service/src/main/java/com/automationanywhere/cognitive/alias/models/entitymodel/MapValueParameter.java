package com.automationanywhere.cognitive.alias.models.entitymodel;

/**
 * Created by Mayur.Panchal on 12-01-2017.
 */
public class MapValueParameter {
    private Object Value;
    private String Operator;

    public MapValueParameter(Object value, String operator) {
        Value = value;
        Operator = operator;
    }

    Object getValue() {
        return this.Value;
    }

    String getOperator() {
        return this.Operator;
    }
}
