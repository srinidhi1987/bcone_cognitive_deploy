package com.automationanywhere.cognitive.alias.exception.customexceptions;

public class DataAccessLayerException extends Exception {

    private static final long serialVersionUID = -5440114398448990957L;

    public DataAccessLayerException(String message) {
        super(message);
    }

    public DataAccessLayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
