package com.automationanywhere.cognitive.service.shareabledomain;

import com.automationanywhere.cognitive.service.domain.DomainException;

import javax.annotation.Nonnull;

/**
 * Specifies the operations used to import and export domain metadata to and
 * from the persistent repository using its externally "shareable"
 * representation.  The shareable representation is obfuscated to protect
 * proprietary information.
 *
 * @author Erik K. Worth
 */
public interface ShareableDomainService {

  /**
   * Returns the "shareable" representation of the domain data pulled from the
   * Domain service in its internal form and then encoded.
   *
   * @param domainName the name of the desired domain
   * @return the "shareable" representation of the domain data
   * @throws DomainException thrown when there is an error retrieving the
   *     domain information for the provided domain name
   * @throws DomainCodecException thrown when there is an error encoding the
   *     domain metadata into a shareable representation
   */
  @Nonnull
  ShareableDomainInfo getDomain(@Nonnull String domainName)
          throws DomainException, DomainCodecException;

  /**
   * Imports the "shareable" representation of the domain data provided from
   * some external source.
   *
   * @param shareableDomain the "shareable" representation of the domain data
   * @throws DomainException thrown when there is an error inserting the
   *     domain information into the persistent store
   * @throws DomainCodecException thrown when there is an error decoding the
   *     domain metadata from its shareable representation
   */
  void importDomain(@Nonnull ShareableDomainInfo shareableDomain)
          throws DomainException, DomainCodecException;
}
