package com.automationanywhere.cognitive.alias;

import static spark.Spark.secure;

import com.automationanywhere.cognitive.common.logger.AALogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Jemin.Shah on 01-12-2016.
 */
public class Main {

    private static final String JDBC_STRING = "jdbc:sqlserver://";
    private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations\\CognitivePlatform.jks";
    private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations\\Cognitive.properties";
    private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
    private static final String PROPERTIES_NOTFOUND = "NotFound";
    private static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final int DATASOURCE_INITIAL_SIZE = 3;

    private static String jdbcUrl;
    private static String username;
    private static String servicePort;
    private static boolean windowsAuth;
    private static AALogger logger = AALogger.create(Main.class);
    private static final String FILENAME_COGNITIVE_SETTINGS = "configurations\\Settings.txt";
    private static final String PROPERTIES_SQL_WINAUTH = "SQLWindowsAuthentication";
    private static String settingsFilePath;

    public static void main(String[] args) throws IOException {
        if (args.length > 4) {
            jdbcUrl = JDBC_STRING + args[0] + ":" + args[1];
            jdbcUrl += ";databaseName=" + args[2];
            username = args[3];
            servicePort = args[4];
            loadSettings();
            if (windowsAuth) {
                jdbcUrl += ";integratedSecurity=true";
            }
        } else {
            logger.error("5 arguments necessary to run jar file <ipaddress> <sqlport> <databasename> <username> <serviceport>");
            System.out.println("5 arguments necessary to run jar file <ipaddress> <sqlport> <databasename> <username> <serviceport>");
            return;
        }
        setServiceSecurity();

    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
    context.registerShutdownHook();
  }

    public static String getServicePort() {
        return servicePort;
    }

    private static void setServiceSecurity() {
        if (isSSLCertificateConfigured()) {
            try {
                String certificateKey = loadSSLCertificatePassword();
                if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
                	Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
                    String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
                    secure(cognitiveCertificateFilePath, certificateKey, null, null);
                }

            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    private static String loadSSLCertificatePassword() throws IOException {
        String sslCertPwd;
        Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitivePropertiesFilePath = propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
        if (fileExists(cognitivePropertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitivePropertiesFilePath));
            sslCertPwd = properties.getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
        } else {
            logger.info("Cognitive properties file not found");
            sslCertPwd = PROPERTIES_NOTFOUND;
        }
        return sslCertPwd;
    }

    private static boolean isSSLCertificateConfigured() {
        boolean hasCertificate = false;
        Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
        if (fileExists(cognitiveCertificateFilePath)) {
            hasCertificate = true;
        }
        return hasCertificate;
    }

    private static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    public static BasicDataSource createDataSource(String password) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(SQL_SERVER_DRIVER);
        dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
        dataSource.setUrl(jdbcUrl);
        if (!windowsAuth) {
            dataSource.setUsername(username);
            dataSource.setPassword(password);
        }
        return dataSource;
    }

    private static void loadSettings() throws IOException {
        Path configurationDirectoryPath  = Paths.get(System.getProperty("user.dir")).getParent();
        settingsFilePath = configurationDirectoryPath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
        Properties properties = loadProperties(settingsFilePath);
        if(properties.containsKey(PROPERTIES_SQL_WINAUTH))
        {
            windowsAuth = Boolean.parseBoolean(properties.getProperty(PROPERTIES_SQL_WINAUTH));
        }
    }

    private static Properties loadProperties(String filePath) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filePath));
        return properties;
    }
}