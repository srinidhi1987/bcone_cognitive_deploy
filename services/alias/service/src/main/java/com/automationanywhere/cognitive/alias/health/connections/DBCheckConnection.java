package com.automationanywhere.cognitive.alias.health.connections;

import org.springframework.stereotype.Component;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;

@Component("DBCheckConnection")
public class DBCheckConnection implements HealthCheckConnection {
	private AliasService aliasService;
	
	public DBCheckConnection(AliasService aliasService) {
		this.aliasService=aliasService;
	}
	
	@Override
	public void checkConnection() throws DataAccessLayerException {
		aliasService.getAllDocTypes();
	}
}