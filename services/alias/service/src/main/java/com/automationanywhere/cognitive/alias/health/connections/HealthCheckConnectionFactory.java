package com.automationanywhere.cognitive.alias.health.connections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.automationanywhere.cognitive.alias.exception.customexceptions.UnsupportedConnectionTypeException;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;

public class HealthCheckConnectionFactory {
	@Autowired
	@Qualifier("aliasService")
	private AliasService aliasService;
	
    public HealthCheckConnection getConnection(ConnectionType type) {
        if (type == ConnectionType.DB )
            return new DBCheckConnection(aliasService);
        throw new UnsupportedConnectionTypeException(type + " is not supported");
    }
}
