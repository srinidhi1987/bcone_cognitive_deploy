package com.automationanywhere.cognitive.service.domain;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

/**
 * Specifies the information used to describe a Domain.
 *
 * <p>The types of documents and data processed by the Cognitive Services
 * (IQ Bot microservices) are classified into Domains.  Each Domain is
 * described by metadata that identify the Fields (the labels for data values)
 * extracted from documents processed by the Cognitive Services and classified
 * according to the same Domain.  Fields can appear as top-level fields in the
 * document (such as an invoice number or customer name) or as column values in
 * tables (such as for columns in the line items on an invoice or purchase
 * order).  Each Field has a data type and a number of Aliases for the data
 * label that might appear in text-extracted documents (e.g. for documents in
 * the Invoices Domain, the field identified as the "Invoice Number" might
 * appear in some documents as "Invoice #").  The Field metadata includes
 * Aliases for different supported languages.
 * </p>
 * @author Erik K. Worth
 */
public interface DomainInfo extends DomainLanguagesInfo {

  /**
   * Returns the ordered list of fields that appear in documents described by
   * this Domain.
   * @return the ordered list of fields that appear in documents described by
   * this Domain
   */
  @Nonnull
  List<DomainFieldInfo> getFields();
}
