package com.automationanywhere.cognitive.alias.exception.customexceptions;

/**
 * Created by keval.sheth on 29-11-2016.
 */
public class ProjectNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -6153912773869034489L;

  @Override
  public synchronized Throwable fillInStackTrace() {
    return this;
  }
}
