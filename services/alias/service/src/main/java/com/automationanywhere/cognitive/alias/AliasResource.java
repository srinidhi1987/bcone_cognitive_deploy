package com.automationanywhere.cognitive.alias;

import com.automationanywhere.cognitive.alias.exception.contract.ExceptionHandler;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.alias.handlers.contract.AliasHandler;
import com.automationanywhere.cognitive.alias.health.handlers.impl.AliasHealthCheckHandler;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DuplicateDomainException;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.NoSuchDomainException;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import spark.Request;
import spark.Response;

import java.util.UUID;

import static com.automationanywhere.cognitive.alias.Constants.HeaderConstants.CORRELATION_ID;
import static spark.Spark.*;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
@Controller
public class AliasResource {
  private static final AALogger logger = AALogger.create(AliasResource.class);

  @Autowired
  public AliasResource(
      @Qualifier("serviceportbean") final String serverPort,
      @Qualifier("aliasHandler") final AliasHandler aliasHandler,
      final AliasHealthCheckHandler healthAPIHandler,
      final ExceptionHandler exceptionHandler) {

        port(Integer.parseInt(serverPort));
        exception(DataNotFoundException.class, exceptionHandler::handleDataNotFoundException);
        exception(DataAccessLayerException.class, exceptionHandler::handleDataAccessLayerException);
        exception(JsonProcessingException.class, exceptionHandler::handleJsonProcessingException);
        exception(InvalidDomainException.class, exceptionHandler::handleInvalidDomainException);
        exception(DuplicateDomainException.class, exceptionHandler::handleDuplicateDomainException);
        exception(NoSuchDomainException.class, exceptionHandler::handleNoSuchDomainException);
        exception(DomainException.class, exceptionHandler::handleDomainException);
        exception(Exception.class, exceptionHandler::handleException);
        after((this::postRequestProcessor));
        before((this::preRequestProcessor));

        get("/projecttypes/:doctype/fields", aliasHandler::getAllFields);
        get("/projecttypes/:doctype", aliasHandler::getProjectType);
        get("/projecttypes", aliasHandler::getAllProjectTypes);
        get("/fields/:fieldid/alias", aliasHandler::getFieldAliases);
        get("/fields/:fieldid", aliasHandler::getField);
        get("/languages", aliasHandler::getAllLanguages);
        get("/languages/:languageid", aliasHandler::getLanguage);
        get("/domains/:domainId/export", aliasHandler::getDomain);
        post("/domains/import", aliasHandler::importDomain);
        get("/domains/languages", aliasHandler::getDomainsLanguages);
        get("/health", ((request, response) -> {
            logger.entry();
            response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
            response.body("OK");
            logger.exit();
            return response;
        }));
        get("/healthcheck", ((request, response) -> {
            logger.entry();
            return healthAPIHandler.checkHealth();
        }));
        get("/heartbeat", aliasHandler::getHeartBeatInfo);
    }

    private static void addHeader(Request request, Response response) {
        response.type("application/json");
    }

    private void preRequestProcessor(Request request, Response response) {
        addTransactionIdentifier(request);
        logger.debug("Request URI " + request.uri());
    }

    private void postRequestProcessor(Request request, Response response) {
        addHeader(request, response);
    }

    private void addTransactionIdentifier(Request request) {
        String correlationId = request.headers(CORRELATION_ID);
        ThreadContext.put(CORRELATION_ID, correlationId != null ?
                correlationId : UUID.randomUUID().toString());
    }
}
