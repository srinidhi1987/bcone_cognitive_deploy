package com.automationanywhere.cognitive.service.domain;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Specifies the information for a Field associated with a specific Domain.
 *
 * <p>Each Domain is described by metadata that identify the Fields (the labels for data values)
 * extracted from documents classified according to the same Domain.  Fields can appear as
 * top-level fields in the document (such as an invoice number or customer name) or as column values
 * in tables (such as for columns in the line items on an invoice or purchase order).
 * Each Field has a data type and a number of Aliases for the data label that might
 * appear in text-extracted documents (e.g. for documents in the Invoices Domain, the field identified
 * as the "Invoice Number" might appear in some documents as "Invoice #").  The Field metadata includes
 * Aliases for different supported languages.
 * </p>
 * @author Erik K. Worth
 */
public interface DomainFieldInfo {

  /**
   * Returns the field identifier such as "Invoice Number".
   * @return the field identifier
   */
  @Nonnull
  String getName();

  /**
   * Returns the kind of field as either a top-level field or a table column value.
   * @return the kind of field as either a top-level field or a table column value
   */
  @Nonnull
  DomainFieldType getType();

  /**
   * Returns the type of formatting used for the field from a set of supported values that minimally
   * include:
   * <ul>
   *   <li>Text</li>
   *   <li>Date</li>
   *   <li>Number</li>
   * </ul>
   * @return the type of formatting used for the field from a set of supported values
   */
  @Nonnull
  String getFormat();

  /**
   * Returns <code>true</code> when the field is expected to be "Checked" by default as a required field
   * when a Learning Instance is created in user interface for documents of the type described by the
   * Domain, and <code>false</code> otherwise.
   * @return <code>true</code> when the field is expected to be "Checked" by default as a required field
   */
  boolean isDefault();

  /**
   * Returns the set of alias information for this Domain Field.  Each entry provides a set of
   * language-specific alternate field names.
   * @return the set of alias information for this Domain Field
   */
  @Nonnull
  Set<AliasInfo> getAliases();
}
