package com.automationanywhere.cognitive.alias.handlers.contract;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.fasterxml.jackson.core.JsonProcessingException;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
public interface AliasHandler {
    String getAllFields(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getProjectType(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getAllProjectTypes(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getFieldAliases(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getField(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getAllLanguages(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getLanguage(Request request, Response response) throws DataNotFoundException, DataAccessLayerException;
    String getDomain(Request request, Response response) throws DomainException, JsonProcessingException;
    String getDomainsLanguages(Request request, Response response) throws DomainException, JsonProcessingException;
    String importDomain(Request request, Response response) throws DomainException;
    String getShareableDomain(Request request, Response response) throws DomainException, DomainCodecException, JsonProcessingException;
    String importShareableDomain(Request request, Response response) throws DomainException, DomainCodecException;
    Object getHeartBeatInfo(Request request, Response response);
}
