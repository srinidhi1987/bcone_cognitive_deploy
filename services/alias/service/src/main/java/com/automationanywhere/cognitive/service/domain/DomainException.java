package com.automationanywhere.cognitive.service.domain;

/**
 * Exception thrown when there is an error performing an operation on a Domain.
 *
 * @author Erik K. Worth
 */
public class DomainException extends RuntimeException {

  private static final long serialVersionUID = 342345243562345L;

  /**
   * Construct with a message.
   *
   * @param message the error message
   */
  public DomainException(final String message) {
    super(message);
  }

  /**
   * Construct with a message and cause.
   *
   * @param message the error message
   * @param cause the root cause of the exception
   */
  public DomainException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
