package com.automationanywhere.cognitive.service.shareabledomain.internal;

import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.automationanywhere.cognitive.service.domain.DomainService;
import com.automationanywhere.cognitive.service.domain.internal.DomainData;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainInfo;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

import static com.automationanywhere.cognitive.service.shareabledomain.internal.DomainCodec.DOMAIN_ENCODING_V1;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The default implementation of the Shareable Domain Service that performs
 * the encoding and decoding between the Shareable representation and the
 * internal object representation of the Domain metadata.
 *
 * @author Erik K. Worth
 */
@Service
public class DefaultShareableDomainService implements ShareableDomainService {

  /** The domain service able to store and retrieve the domain metadata */
  private final DomainService domainService;

  /** The component able to convert between the Domain Data and JSON */
  private final ObjectMapper jsonObjectMapper;

  /**
   * Construct from dependencies.
   *
   * @param domainService the domain service able to store and retrieve the
   *     domain metadata
   */
  @Autowired
  public DefaultShareableDomainService(
          @Nonnull final DomainService domainService,
          @Nonnull final ObjectMapper jsonObjectMapper) {
    this.domainService = checkNotNull(domainService,
            "domainService must not be null");
    this.jsonObjectMapper = checkNotNull(jsonObjectMapper,
            "jsonObjectMapper must not be null");
  }

  @Override
  @Nonnull
  public ShareableDomainInfo getDomain(@Nonnull final String domainName) {
    checkNotNull(domainName, "domainName must not be null");
    final DomainInfo domainInfo = domainService.getDomain(domainName);
    return new ShareableDomain(
            domainName,
            encodeDomain(domainInfo),
            DOMAIN_ENCODING_V1);
  }

  @Override
  public void importDomain(@Nonnull final ShareableDomainInfo shareableDomain) {
    final DomainInfo domainInfo = decodeDomain(shareableDomain.getDomain());
    domainService.importDomain(domainInfo);
  }

  /**
   * Returns the encoded representation of the domain metadata that may be
   * shared among partners and customers.
   *
   * @param domainInfo the internal domain metadata
   * @return the encoded representation of the domain metadata
   * @throws DomainCodecException thrown when there is an error encoding the
   *     provided internal Domain metadata
   */
  private String encodeDomain(final DomainInfo domainInfo) {
    final String domainJson;
    try {
      domainJson = jsonObjectMapper.writeValueAsString(domainInfo);
    } catch (final Exception exc) {
      throw new DomainCodecException(
              "Error writing domain information to JSON", exc);
    }
    return DomainCodec.encodeDomain(domainJson);
  }

  /**
   * Returns the internal domain metadata from the encoded "shareable"
   * representation of the domain metadata.
   *
   * @param base64Domain the base64 encoded representation of the domain
   * @return the domain metadata
   * @throws DomainCodecException thrown when there is an error decoding the
   *     provided string and producing the internal Domain metadata object
   */
  private DomainInfo decodeDomain(final String base64Domain) {
    final String domainJson = DomainCodec.decodeDomain(base64Domain);
    try {
      return jsonObjectMapper.readValue(domainJson, DomainData.class);
    } catch (final Exception exc) {
      throw new DomainCodecException("Error parsing Domain JSON", exc);
    }
  }

}
