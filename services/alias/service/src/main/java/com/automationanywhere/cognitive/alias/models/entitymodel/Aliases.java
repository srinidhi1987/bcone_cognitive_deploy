package com.automationanywhere.cognitive.alias.models.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
@Entity
@Table(name = "Aliases")
public class Aliases {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;
    public int getId() { return this.id ;}
    public void setId(int id) { this.id = id; }

    @Column(name="FieldId")
    private int fieldId;
    public int getFieldId() {return this.fieldId; }
    public void setFieldId(int fieldId) { this.fieldId = fieldId; }

    @Column(name="Alias")
    private String alias;
    public String getAlias() { return this.alias ;}
    public void setAlias(String alias) { this.alias = alias; }

    @Column(name="LanguageId")
    private int languageId;
    public int getLanguageId() {return this.languageId; }
    public void setLanguageId(int languageId) { this.languageId = languageId; }
}
