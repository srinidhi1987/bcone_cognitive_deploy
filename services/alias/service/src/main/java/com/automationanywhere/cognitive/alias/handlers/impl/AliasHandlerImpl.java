package com.automationanywhere.cognitive.alias.handlers.impl;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.alias.handlers.contract.AliasHandler;
import com.automationanywhere.cognitive.alias.models.ProjectType;
import com.automationanywhere.cognitive.alias.models.ServiceResponse;
import com.automationanywhere.cognitive.alias.models.StorageFieldItem;
import com.automationanywhere.cognitive.alias.models.entitymodel.Aliases;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.alias.models.entitymodel.Language;
import com.automationanywhere.cognitive.alias.service.contract.AliasService;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.automationanywhere.cognitive.service.domain.DomainLanguagesInfo;
import com.automationanywhere.cognitive.service.domain.DomainService;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;
import com.automationanywhere.cognitive.service.domain.internal.DomainData;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainInfo;
import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainService;
import com.automationanywhere.cognitive.service.shareabledomain.internal.ShareableDomain;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
public class AliasHandlerImpl implements AliasHandler {

    private static final AALogger logger = AALogger.create(AliasHandlerImpl.class);

  private static final String DOMAIN_ID_PARAM_KEY = "domainId";
  private static final String LANG_ID_PARAM_KEY = "languageid";
  private static final String DOC_TYPE_PARAM_KEY = "doctype";
  private static final String FIELD_ID_PARAM_KEY = "fieldid";

    private final AliasService aliasService;
    private final DomainService domainService;
    private final ShareableDomainService shareableDomainService;
    private final ObjectMapper jsonObjectMapper;

    private String getResponse(ServiceResponse customResponse) {
        try {
            return jsonObjectMapper.writeValueAsString(customResponse);
        } catch (JsonProcessingException e) {
            logger.error("Error parsing json: " + e.getMessage());
            return "";
        }
    }

    @Autowired
    public AliasHandlerImpl(
            @Qualifier("aliasService") AliasService aliasService,
            DomainService domainService,
            ShareableDomainService shareableDomainService,
            ObjectMapper objectMapper) {
        this.aliasService = aliasService;
        this.domainService = domainService;
        this.shareableDomainService = shareableDomainService;
        this.jsonObjectMapper = objectMapper;
   }

    private String getSuccessResponse(Response response, Object returnResult) {
        response.status(200);
        ServiceResponse result = new ServiceResponse();
        result.setSuccess(true);
        result.setData(returnResult);
        return getResponse(result);
    }

    @Override
    public String getAllLanguages(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();
        String result = getSuccessResponse(response, aliasService.getAllLanguages());
        logger.exit();
        return result;
    }

    @Override
    public String getLanguage(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();
        int lid = Integer.parseInt(request.params(LANG_ID_PARAM_KEY));
        logger.debug("languageid " + lid);
        String result = getSuccessResponse(response, aliasService.getLanguage(lid));
        logger.exit();
        return result;
    }

    @Override
    public String getDomain(Request request, Response response)
            throws DomainException, JsonProcessingException {
        logger.entry();
        try {
            String domainName = request.params(DOMAIN_ID_PARAM_KEY);
            logger.debug("domainName: {}", domainName);
            final DomainInfo domainInfo = domainService.getDomain(domainName);
            return jsonObjectMapper.writeValueAsString(domainInfo);
        } finally {
            logger.exit();
        }
    }

    @Override
    public String getDomainsLanguages(Request request, Response response)
            throws DomainException, JsonProcessingException {
        logger.entry();
        try {
            final List<DomainLanguagesInfo> domainLanguagesInfoList =
                    domainService.getAllDomainsWithLanguages();
            return jsonObjectMapper.writeValueAsString(domainLanguagesInfoList);
        } finally {
            logger.exit();
        }
    }

    @Override
    public String importDomain(Request request, Response response)
            throws DomainException {
        logger.entry();
        try {
            // Extract the Domain Info from the request body
            final String domainJson = request.body();
            final DomainData domainData;
            try {
                domainData = jsonObjectMapper.readValue(
                        domainJson, DomainData.class);
            } catch (final Exception exc) {
                throw new InvalidDomainException(exc.getMessage(), exc);
            }

            // Import the domain into the persistent store
            domainService.importDomain(domainData);
        } finally {
            logger.exit();
        }
        // No result on success
        return "";
    }

    @Override
    public String getShareableDomain(Request request, Response response)
            throws DomainException, DomainCodecException, JsonProcessingException {
        logger.entry();
        try {
            String domainName = request.queryParams("name");
            logger.debug("domainName: {}", domainName);
            final ShareableDomainInfo domainInfo =
                    shareableDomainService.getDomain(domainName);
            return jsonObjectMapper.writeValueAsString(domainInfo);
        } finally {
            logger.exit();
        }
    }

    @Override
    public String importShareableDomain(Request request, Response response)
            throws DomainException, DomainCodecException {
        logger.entry();
        try {
            // Extract the Domain Info from the request body
            final String domainJson = request.body();
            final ShareableDomain domainData;
            try {
                domainData = jsonObjectMapper.readValue(
                        domainJson, ShareableDomain.class);
            } catch (final Exception exc) {
                throw new InvalidDomainException(exc.getMessage(), exc);
            }

            // Import the domain into the persistent store
            shareableDomainService.importDomain(domainData);
        } finally {
            logger.exit();
        }
        // No result on success
        return "";
    }

    @Override
    public String getAllFields(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();
        int doctypeid = Integer.parseInt(request.params(DOC_TYPE_PARAM_KEY));
        String lid = request.queryParams(LANG_ID_PARAM_KEY);
        logger.debug("languageid " + lid);
        int languageId;
        if (lid == null || lid.equals("")) {
            languageId = 1;
        } else {
            languageId = Integer.parseInt(lid);
        }
        DocType docType = aliasService.getDocTypeDetails(doctypeid);
        if (docType == null) {
            logger.error("Data not found.");
            throw new DataNotFoundException();
        }
        List<StorageFieldItem> result = new ArrayList<>();

        for (int i = 0; i < docType.getFieldList().size(); i++) {
            Fields field = docType.getFieldList().get(i);

            StorageFieldItem storageFieldItem = new StorageFieldItem(
                Integer.toString(field.getId()),
                field.getName(),
                null,
                field.getFieldType().getName(),
                new ArrayList<>(),
                field.getIsDefaultSelected(),
                field.getFieldDataType().getName()
            );
            field.getAliasesList(languageId).forEach(
                ((aliases) -> storageFieldItem.getAliases().add(aliases.getAlias())));
            result.add(storageFieldItem);
        }
        String fieldsResponse = getSuccessResponse(response, result);
        logger.exit();
        return fieldsResponse;
    }

    @Override
    public String getProjectType(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();
        int doctypeid = Integer.parseInt(request.params(DOC_TYPE_PARAM_KEY));
        String lid = request.queryParams(LANG_ID_PARAM_KEY);
        logger.debug(() -> String.format("languageid %s documenttypeid %d", lid, doctypeid));
        int languageId;
        if (lid == null || lid.equals("")) {
            languageId = 1;
        } else {
            languageId = Integer.parseInt(lid);
        }
        DocType docType = aliasService.getDocTypeDetails(doctypeid);
        if (docType == null) {
            logger.error("Data not found.");
            throw new DataNotFoundException();
        }

      // Retrieve languages from Aliases
      Map<Integer, Language> languagesById = aliasService.getAllLanguages().stream()
          .collect(Collectors.toMap(Language::getId, l -> l));

      Set<Integer> ids = new HashSet<>();
      List<Language> languages = new ArrayList<>();
      for (Fields fields : docType.getFieldList()) {
        for (Aliases aliases : fields.getAliasesList()) {
          int langId = aliases.getLanguageId();
          if (!ids.contains(langId)) {
            ids.add(langId);
            languages.add(languagesById.get(langId));
          }
        }
      }

        ProjectType result = new ProjectType(
            Integer.toString(docType.getId()),
            docType.getName(),
            null,
            new ArrayList<>(),
            languages);
        addFields(result, docType, languageId);

        String projectType = getSuccessResponse(response, result);
        logger.exit();
        return projectType;
    }

    @Override
    public String getAllProjectTypes(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();
        String lid = request.queryParams(LANG_ID_PARAM_KEY);
        logger.debug("languageid " + lid);
        int languageId;
        if (lid == null || lid.equals("")) {
            languageId = 1;
        } else {
            languageId = Integer.parseInt(lid);
        }

    List<DocType> docTypelist = aliasService.getAllDocTypes();
    if (docTypelist == null || docTypelist.isEmpty()) {
      logger.error("Data not found.");
      throw new DataNotFoundException();
    }

    // Retrieve languages from Aliases
    Map<Integer, Language> languagesById = aliasService.getAllLanguages().stream()
        .collect(Collectors.toMap(Language::getId, l -> l));
    Map<Integer, List<Language>> languagesByDocType = new HashMap<>();
    for (DocType docType : docTypelist) {
      Set<Integer> ids = new HashSet<>();
      List<Language> languages = new ArrayList<>();
      languagesByDocType.put(docType.getId(), languages);
      for (Fields fields : docType.getFieldList()) {
        for (Aliases aliases : fields.getAliasesList()) {
          int langId = aliases.getLanguageId();
          if (!ids.contains(langId)) {
            ids.add(langId);
            languages.add(languagesById.get(langId));
          }
        }
      }
    }

    List<ProjectType> list = new ArrayList<>();
    for (DocType docType : docTypelist) {
      ProjectType result = new ProjectType(
          Integer.toString(docType.getId()),
          docType.getName(),
          null,
          new ArrayList<>(),
          languagesByDocType.get(docType.getId())
      );
      addFields(result, docType, languageId);
      list.add(result);
    }
    String projectTypes = getSuccessResponse(response, list);
    logger.exit();
    return projectTypes;
  }

    @Override
    public String getFieldAliases(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();
        int fieldid = Integer.parseInt(request.params(FIELD_ID_PARAM_KEY));
        String lid = request.queryParams(LANG_ID_PARAM_KEY);
        logger.debug(() -> String.format("languageid %s fieldid %d", lid, fieldid));
        int languageId;
        if (lid == null || lid.equals("")) {
            languageId = 1;
        } else {
            languageId = Integer.parseInt(lid);
        }

        Fields fields = aliasService.getFields(fieldid);
        if (fields == null) {
            logger.error("Data not found.");
            throw new DataNotFoundException();
        }

        List<String> result = new ArrayList<>();
        fields.getAliasesList(languageId).forEach(x -> result.add(x.getAlias()));
        String projectAliases = getSuccessResponse(response, result);
        logger.exit();
        return projectAliases;
    }

    @Override
    public String getField(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        logger.entry();

        int fieldId = Integer.parseInt(request.params(FIELD_ID_PARAM_KEY));
        String lid = request.queryParams(LANG_ID_PARAM_KEY);
        logger.debug(() -> String.format("languageid %s fieldid %d", lid, fieldId));
        int languageId;
        if (lid == null || lid.equals("")) {
            languageId = 1;
        } else {
            languageId = Integer.parseInt(lid);
        }

        Fields fields = aliasService.getFields(fieldId);
        if (fields == null) {
            logger.error("Data not found.");
            throw new DataNotFoundException();
        }

        StorageFieldItem storageFieldItem = new StorageFieldItem(
            Integer.toString(fields.getId()),
            fields.getName(),
            null,
            fields.getFieldType().getName(),
            new ArrayList<>(),
            fields.getIsDefaultSelected(),
            fields.getFieldDataType().getName()
        );
        fields.getAliasesList(languageId).forEach(
            ((aliases) -> storageFieldItem.getAliases().add(aliases.getAlias())));

        String field = getSuccessResponse(response, storageFieldItem);
        logger.exit();
        return field;
    }


    public Object getHeartBeatInfo(Request request, Response response) {
        String heartBeat;
        //currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization
        response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
        heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
        response.body(heartBeat);
        return response;
    }

    private void addFields(ProjectType result, DocType docType, int languageId) {
        for (int i = 0; i < docType.getFieldList().size(); i++) {
            Fields field = docType.getFieldList().get(i);
            StorageFieldItem storageFieldItem = new StorageFieldItem(
                Integer.toString(field.getId()),
                field.getName(),
                null,
                field.getFieldType().getName(),
                new ArrayList<>(),
                field.getIsDefaultSelected(),
                field.getFieldDataType().getName()
            );
            field.getAliasesList(languageId).forEach(
                ((aliases) -> storageFieldItem.getAliases().add(aliases.getAlias())));
            result.getFields().add(storageFieldItem);
        }
    }

}