package com.automationanywhere.cognitive.alias.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class InvalidProjectStatusException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5364153869101140758L;
}
