package com.automationanywhere.cognitive.service.shareabledomain.internal;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.security.CipherService;
import com.automationanywhere.cognitive.common.security.DefaultCipherService;
import com.automationanywhere.cognitive.common.security.KeyProvider;
import com.automationanywhere.cognitive.common.security.StreamKeyProvider;
import com.automationanywhere.cognitive.service.shareabledomain.DomainCodecException;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Class that encapsulates how the Domain metadata is encoded and decoded
 * between its internal JSON representation and the shareable base64 encoded
 * binary representation.
 *
 * @author Erik K. Worth
 */
public class DomainCodec {

  /** The first version of the domain encoding */
  static final int DOMAIN_ENCODING_V1 = 1;

  /** Logger for this class. */
  private static final AALogger LOGGER = AALogger.create(DomainCodec.class);

  /** Shortcut for this class */
  private static final Class<DomainCodec> THIS_CLASS = DomainCodec.class;

  /**
   * The current ID for the key used to encrypt and decrypt domain metadata.
   * This is expected to identify a private key in the classpath.  It should
   * be in the resources folder.
   */
  static final String DEFAULT_KEY_ID = "shareable-domain-private.key";

  /** Exception message when the dot separator is missing */
  static final String MISSING_SEPARATOR_MSG =
          "Base64 shareable domain does not have the expected separator";

  /** Exception message when there is an error unzipping the content */
  static final String DECOMPRESSION_ERROR_MSG =
          "Error decompressing domain bytes";

  /** Exception message when attempting to unzip content that is not zipped */
  static final String NOT_GZIPPED_MSG =
          "Domain bytes are not compressed using GZIP";

  /** The type of key used to encrypt and decrypt domain metadata */
  private static final String KEY_TYPE = "AES";

  /**
   * The delimiter used to separate the key ID from the domain in the base64
   * representation.
   */
  private static final String DELIMITER = ".";

  /**
   * Returns the encoded "shareable" representation of the domain metadata
   * from the internal representation of the same.
   *
   * @param domainJson the internal representation of the domain metadata
   * @return the encoded "shareable" representation of the domain metadata
   * @throws DomainCodecException thrown when there is an error encoding the
   *     domain metadata
   */
  @Nonnull
  public static String encodeDomain(@Nonnull final String domainJson)
          throws DomainCodecException {
    checkNotNull(domainJson, "domainJson must not be null");

    // Encode as UTF-8
    final byte[] utf8Bytes = domainJson.getBytes(UTF_8);

    // GZIP compress
    final byte[] compressed = zip(utf8Bytes);

    // Encrypt
    final byte[] encryptedBytes = encrypt(compressed);

    // Base64 encode
    final String encodedKeyId = base64Encode(DEFAULT_KEY_ID.getBytes(UTF_8));
    final String encodedDomain = base64Encode(encryptedBytes);

    // return the encoded representation with the key ID prepended to the front
    return encodedKeyId + DELIMITER + encodedDomain;
  }

  /**
   * Returns the internal representation of the Domain metadata from the
   * external representation of the Domain metadata.
   *
   * @param base64Str the base64 encoded external representation of the Domain
   *     metadata
   * @return the internal JSON representation of the Domain metadata.
   * @throws DomainCodecException thrown when there is an error decoding the
   *     the Domain metadata
   */
  @Nonnull
  public static String decodeDomain(@Nonnull final String base64Str)
          throws DomainCodecException {
    checkNotNull(base64Str, "base64 must not be null");

    // Separate out the key ID and the domain payload
    final String[] keyIdAndDomain = StringUtils.split(base64Str, DELIMITER);
    if (null == keyIdAndDomain || keyIdAndDomain.length != 2) {
      throw new DomainCodecException(MISSING_SEPARATOR_MSG);
    }

    // Base64 decode
    final String keyId = new String(base64Decode(keyIdAndDomain[0]), UTF_8);
    LOGGER.debug("Extracted cipher key ID: '{}'", keyId);
    final byte[] decoded = base64Decode(keyIdAndDomain[1]);

    // Decrypt
    final byte[] decryptedBytes = decrypt(keyId, decoded);

    // GZIP Decompress
    final byte[] decompressedBytes = unzip(decryptedBytes);

    // Return the JSON
    return new String(decompressedBytes, UTF_8);
  }

  /**
   * Returns the URL-safe base64 encoded representation of the provided bytes.
   *
   * @param bytes the bytes to base64 encoded
   * @return the URL-safe base64 encoded representation of the provided bytes
   * @throws DomainCodecException thrown when there is an error Base64 encoding
   *     the provided bytes
   */
  @Nonnull
  static String base64Encode(@Nonnull final byte[] bytes)
          throws DomainCodecException {
    checkNotNull(bytes, "utf8Bytes must not be null");
    // Use the URL-safe variation
    return Base64.getUrlEncoder().encodeToString(bytes);
  }

  /**
   * Returns the base64 decoded bytes from the base64 encoded string.
   *
   * @param base64String the base64 encoded Domain metadata string
   * @return the base64 decoded bytes from the base64 encoded string
   */
  @Nonnull
  static byte[] base64Decode(@Nonnull final String base64String) {
    checkNotNull(base64String, "base64String must not be null");
    try {
      // This decodes the URL-safe variation (in addition to the normal one)
      return Base64.getUrlDecoder().decode(base64String);
    } catch (final Exception exc) {
      throw new DomainCodecException("Error decoding base64 string", exc);
    }
  }

  /**
   * Returns the GZIP compressed Domain metadata bytes from the provided bytes
   * @param decompressedBytes the uncompressed Domain metadata bytes
   * @return the GZIP compressed Domain metadata bytes from the provided bytes
   * @throws DomainCodecException thrown when there is an error compressing the
   *     Domain metadata
   */
  @Nonnull
  static byte[] zip(@Nonnull byte[] decompressedBytes)
          throws DomainCodecException {
    checkNotNull(decompressedBytes, "decompressedBytes must not be null");
    final byte[] compressedBytes;
    try (final ByteArrayOutputStream compressed =
                 new ByteArrayOutputStream(decompressedBytes.length)) {
      try (final GZIPOutputStream decompressingOutputStream =
                   new GZIPOutputStream(compressed)) {
        // This try block is nested to make sure the zipped stream is flushed
        // to the output stream prior to extracting the bytes
        decompressingOutputStream.write(decompressedBytes);
      }
      compressedBytes = compressed.toByteArray();
    } catch (final Exception exc) {
      throw new DomainCodecException("Error compressing domain bytes", exc);
    }
    return compressedBytes;
  }

  /**
   * Returns the decompressed bytes from the GZIP compressed bytes.
   *
   * @param compressedBytes the GZIP compressed bytes
   * @return the decompressed bytes from the GZIP compressed bytes
   * @throws DomainCodecException thrown when there is an error decompressing
   *     the bytes
   */
  @Nonnull
  static byte[] unzip(@Nonnull final byte[] compressedBytes)
          throws DomainCodecException {
    checkNotNull(compressedBytes, "compressedBytes must not be null");
    checkZipped(compressedBytes);
    final byte[] decompressedBytes;
    // Guess at the decompressed size to try and avoid an internal
    // array reallocation. Ten times is a reasonable compression factor.
    try (final ByteArrayOutputStream decompressed =
                 new ByteArrayOutputStream(compressedBytes.length * 10);
         final ByteArrayInputStream compressedInputStream =
                 new ByteArrayInputStream(compressedBytes)) {
      decompressedBytes = decompress(decompressed, compressedInputStream);
    } catch (final Exception exc) {
      throw new DomainCodecException(DECOMPRESSION_ERROR_MSG, exc);
    }
    return decompressedBytes;
  }

  /**
   * Returns the GZIP compressed bytes.
   *
   * @param decompressed the output stream to hold the decompressed bytes
   * @param compressedInputStream the input stream holding the compressed bytes
   * @return the GZIP compressed bytes
   * @throws IOException thrown when there is an error decompressing bytes
   */
  @Nonnull
  private static byte[] decompress(
          @Nonnull final ByteArrayOutputStream decompressed,
          @Nonnull final ByteArrayInputStream compressedInputStream)
          throws IOException {
    try (final GZIPInputStream gzipInputStream =
                 new GZIPInputStream(compressedInputStream)) {
      StreamUtils.copy(gzipInputStream, decompressed);
    }
    return decompressed.toByteArray();
  }

  /**
   * Checks the provided bytes to make sure they have the appropriate
   * markers indicating they were compressed using GZIP
   *
   * @param compressed the compressed bytes to check
   * @throws DomainCodecException thrown when the provided bytes are not GZIPed
   */
  private static void checkZipped(@Nonnull final byte[] compressed) {
    if (compressed[0] != (byte) (GZIPInputStream.GZIP_MAGIC)
            || (compressed[1] != (byte) (GZIPInputStream.GZIP_MAGIC >> 8))) {
      throw new DomainCodecException(NOT_GZIPPED_MSG);
    }
  }

  /**
   * Returns the encrypted Domain metadata bytes from the clear bytes.
   *
   * @param clearBytes the Domain metadata bytes in the clear
   * @return the encrypted Domain metadata bytes from the clear bytes
   * @throws DomainCodecException thrown when there is an error encrypting the
   *     Domain metadata
   */
  @Nonnull
  static byte[] encrypt(@Nonnull byte[] clearBytes)
          throws DomainCodecException {
    checkNotNull(clearBytes, "clearBytes must not be null");
    final CipherService cipherService = getCipherService(DEFAULT_KEY_ID);
    final byte[] encryptedBytes;
    try (final InputStream compressedInputStream =
                 new ByteArrayInputStream(clearBytes)) {
      try (final InputStream encryptedInputStream =
                   cipherService.encrypt(compressedInputStream)) {
        encryptedBytes = StreamUtils.copyToByteArray(encryptedInputStream);
      }
    } catch (final Exception exc) {
      throw new DomainCodecException("Error encrypting domain", exc);
    }
    return encryptedBytes;
  }

  /**
   * Returns the decrypted bytes from the provided encrypted Domain metadata.
   *
   * @param keyId the identifier for the key used to decrypt the Domain metadata
   * @param encrypted the encrypted Domain metadata bytes
   * @return the decrypted bytes from the provided encrypted Domain metadata
   * @throws DomainCodecException thrown when there is an error decrypting the
   *     Domain metadata
   */
  @Nonnull
  static byte[] decrypt(
          @Nonnull final String keyId,
          @Nonnull final byte[] encrypted) throws DomainCodecException {
    checkNotNull(keyId, "keyId must not be null");
    checkNotNull(encrypted, "encrypted must not be null");
    final CipherService cipherService = getCipherService(keyId);
    final byte[] decryptedBytes;
    try (final InputStream decodedInputStream =
                 new ByteArrayInputStream(encrypted)) {
      try (final InputStream decryptedInputStream =
                   cipherService.decrypt(decodedInputStream)) {
        decryptedBytes = StreamUtils.copyToByteArray(decryptedInputStream);
      }
    } catch (final Exception exc) {
      throw new DomainCodecException("Error decrypting domain", exc);
    }
    return decryptedBytes;
  }

  /**
   * Returns the component able to encrypt and decrypt Domain metadata.
   *
   * @param keyId the key identifier.  This is expected to identify the key in
   *     the classpath.
   * @return the component able to encrypt and decrypt Domain metadata
   * @throws DomainCodecException thrown when there is an error retrieving the
   *     Cipher Service using the provided key ID
   */
  @Nonnull
  private static CipherService getCipherService(@Nonnull final String keyId)
          throws DomainCodecException{
    return new DefaultCipherService(getKeyProvider(keyId));
  }

  /**
   * Returns the component able to provide a key for encrypting and decrypting
   * Domain metadata.
   *
   * @param keyId the key identifier.  This is expected to identify the key in
   *     the classpath.
   * @return the component able to provide a key for encrypting and decrypting
   *     Domain metadata
   * @throws DomainCodecException thrown when there is an error retrieving the
   *     Key Provider using the provided key ID
   */
  @Nonnull
  private static KeyProvider getKeyProvider(@Nonnull final String keyId)
          throws DomainCodecException {
    try (final InputStream inputStream =
                 THIS_CLASS.getClassLoader().getResourceAsStream(keyId)) {
      return new StreamKeyProvider(KEY_TYPE, inputStream);
    } catch (final Exception exc) {
      throw new DomainCodecException("Error loading private key resource, '"
              + keyId + "', from classpath", exc);
    }
  }

  /** Hide the constructor */
  private DomainCodec() {}
}
