package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.automationanywhere.cognitive.service.domain.InvalidDomainException;

import javax.annotation.Nonnull;

/**
 * Specifies the operations used to validate Domain metadata.
 */
public interface DomainValidator {

  /**
   * Throws the InvalidDomainException when the provided domain violates a
   * validation rule.
   *
   * @param domainInfo the domain information to test
   * @throws InvalidDomainException thrown when the provided domain violates a
   *     validation rule
   */
  void checkValid(@Nonnull DomainInfo domainInfo) throws InvalidDomainException;
}
