package com.automationanywhere.cognitive.service.domain.internal;

import com.automationanywhere.cognitive.alias.models.entitymodel.Aliases;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldDataType;
import com.automationanywhere.cognitive.alias.models.entitymodel.FieldType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.service.domain.AliasInfo;
import com.automationanywhere.cognitive.service.domain.DomainException;
import com.automationanywhere.cognitive.service.domain.DomainFieldInfo;
import com.automationanywhere.cognitive.service.domain.DomainFieldType;
import com.automationanywhere.cognitive.service.domain.DomainInfo;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Maps between the DocType and Domain.
 *
 * @author Erik K. Worth
 */
class DomainDataMapper {

  /** The name given to fields of type, Form Fields */
  static final String FORM_FIELD_NAME = "FormField";

  /** The name given to fields of type, Table Column Fields */
  static final String TABLE_COLUMN_FIELD_NAME = "TableField";

  /** Maps the types of fields as stored in the DB to Domain Field Types. */
  private static final Map<String, DomainFieldType> TO_DOMAIN_FIELD_TYPE =
          ImmutableMap.of(
                  FORM_FIELD_NAME, DomainFieldType.FORM_FIELD,
                  TABLE_COLUMN_FIELD_NAME, DomainFieldType.TABLE_COLUMN_FIELD);

  /** Maps the Domain Field Types to the types of fields as stored in the DB. */
  private static final Map<DomainFieldType, String> TO_FIELD_TYPE =
          ImmutableMap.of(
                  DomainFieldType.FORM_FIELD, FORM_FIELD_NAME,
                  DomainFieldType.TABLE_COLUMN_FIELD, TABLE_COLUMN_FIELD_NAME);

  /**
   * Returns the Domain Data converted from the provided Doc Type.
   *
   * @param languageNamesById the map of language names keyed by the integer ID
   * @param docType the Doc Type retrieved from the persistent store
   * @return the Domain Data converted from the provided Doc Type
   */
  @Nonnull
  static DomainData toDomainData(
          @Nonnull final Map<Integer, String> languageNamesById,
          @Nonnull final DocType docType) {
    final List<DomainFieldData> domainFields =
            toDomainFieldList(languageNamesById, docType.getFieldList());
    final Set<String> languages = new HashSet<>();
    domainFields.forEach(field -> field.getAliases().forEach(alias ->
            languages.add(alias.getLanguage())));
    return new DomainData(docType.getName(), languages, domainFields);
  }

  /**
   * Returns the DocType record populated from the Domain Info object.
   *
   * @param languageIdsByName the map of language IDs keyed by name
   * @param fieldTypeIdsByName the map of field type IDs keyed by name
   * @param fieldDataTypeIdsByName the map of field data tykpe IDs keyed by name
   * @param domainInfo the Domain Info object
   * @return the DocType record populated from the Domain Info object
   */
  @Nonnull
  static DocType toDocType(
          @Nonnull final Map<String, Integer> languageIdsByName,
          @Nonnull final Map<String, Integer> fieldTypeIdsByName,
          @Nonnull final Map<String, Integer> fieldDataTypeIdsByName,
          @Nonnull final DomainInfo domainInfo) {
    final DocType docType = new DocType();
    docType.setName(domainInfo.getName());
    docType.setFieldList(
            toFieldsList(
                    languageIdsByName,
                    fieldTypeIdsByName,
                    fieldDataTypeIdsByName,
                    domainInfo.getFields()));
    return docType;
  }

  /**
   * Returns the list of Domain Field Data from the list of fields found for
   * a Doc Type from the persistent store.
   *
   * @param languageNamesById the map of language names keyed by the integer ID
   * @param fieldList the list of fields retrieved for the Doc Type from the
   *     persistent store
   * @return the list of Domain Field Data from the provided list of fields
   * @throws DomainException thrown when a field type retrieved from the
   *     persistent store is not supported by the service
   */
  @Nonnull
  private static List<DomainFieldData> toDomainFieldList(
          @Nonnull final Map<Integer, String> languageNamesById,
          @Nullable final List<Fields> fieldList) throws DomainException {
    return (null == fieldList) ? Collections.emptyList() : fieldList.stream()
            .map(fld -> DomainDataMapper.toDomainField(languageNamesById, fld))
            .collect(Collectors.toList());
  }

  /**
   * Returns the list of Field records populated with information from the
   * list of Domain Field Info.
   *
   * @param languageIdsByName the map of language IDs keyed by name
   * @param fieldTypeIdsByName the map of field type IDs keyed by name
   * @param fieldDataTypeIdsByName the map of field data tykpe IDs keyed by name
   * @param domainFieldInfoList the list of Domain Field Information objects
   * @return the list of Field records populated with information from the
   *     list of Domain Field Info
   */
  @Nonnull
  private static List<Fields> toFieldsList(
          @Nonnull final Map<String, Integer> languageIdsByName,
          @Nonnull final Map<String, Integer> fieldTypeIdsByName,
          @Nonnull final Map<String, Integer> fieldDataTypeIdsByName,
          @Nonnull final List<DomainFieldInfo> domainFieldInfoList) {
    return domainFieldInfoList.stream()
            .map(fieldInfo ->
                    toFields(
                            languageIdsByName,
                            fieldTypeIdsByName,
                            fieldDataTypeIdsByName,
                            fieldInfo))
            .collect(Collectors.toList());
  }

  /**
   * Returns the Domain Field Data from the Field record returned from the
   * persistent store.
   *
   * @param languageNamesById the map of language names keyed by the integer ID
   * @param field the field record returned from the persistent store
   * @return the Domain Field Data from the Field record
   * @throws DomainException thrown when a field type retrieved from the
   *     persistent store is not supported by the service
   */
  @Nonnull
  private static DomainFieldData toDomainField(
          @Nonnull final Map<Integer, String> languageNamesById,
          @Nonnull final Fields field) throws DomainException {
    checkNotNull(field, "field must not be null");
    return new DomainFieldData(
            field.getName(),
            toDomainFieldType(field.getFieldType()),
            toFieldFormat(field.getFieldDataType()),
            field.getIsDefaultSelected(),
            toAliasDataSet(languageNamesById, field.getAliasesList()));
  }

  /**
   * Returns a Fields record populated with the information extract from the
   * provided Domain Field.
   *
   * @param languageIdsByName the map of language IDs keyed by name
   * @param fieldTypeIdsByName the map of field type IDs keyed by name
   * @param fieldDataTypeIdsByName the map of field data tykpe IDs keyed by name
   * @param domainFieldInfo the Domain Field object
   * @return a Fields record populated with the information extract from the
   *     provided Domain Field
   */
  @Nonnull
  private static Fields toFields(
          @Nonnull final Map<String, Integer> languageIdsByName,
          @Nonnull final Map<String, Integer> fieldTypeIdsByName,
          @Nonnull final Map<String, Integer> fieldDataTypeIdsByName,
          @Nonnull final DomainFieldInfo domainFieldInfo) {
    final Fields fields = new Fields();
    fields.setName(domainFieldInfo.getName());
    fields.setFieldType(
            toFieldType(
                    fieldTypeIdsByName,
                    domainFieldInfo.getType()));
    fields.setFieldDataType(
            toFieldDataType(
                    fieldDataTypeIdsByName,
                    domainFieldInfo.getFormat()));
    fields.setIsDefaultSelected(domainFieldInfo.isDefault());
    fields.setAliasesList(
            toAliasesListForField(
                    languageIdsByName,
                    domainFieldInfo.getAliases()));
    return fields;
  }

  /**
   * Returns the DomainFieldType from the field type found in a Fields record.
   *
   * @param fieldType the field type from the Fields record
   * @return the DomainFieldType from the field type found in a Fields record
   * @throws DomainException thrown when the field type from the Fields record
   *     is unsupported
   */
  private static DomainFieldType toDomainFieldType(
          @Nonnull final FieldType fieldType) throws DomainException {
    final String name =
            checkNotNull(fieldType, "fieldType must not be null")
                    .getName();
    final DomainFieldType domainFieldType = TO_DOMAIN_FIELD_TYPE.get(name);
    if (null == domainFieldType) {
      throw new DomainException("Field Type with name '"
              + name + "' is not one of the supported names: "
              + TO_DOMAIN_FIELD_TYPE.keySet());
    }
    return domainFieldType;
  }

  /**
   * Returns the Field Type with just the ID set for the provided Domain Field
   * Type.
   *
   * @param fieldTypeIdsByName the map of field type IDs by name
   * @param domainFieldType the Domain Field Type
   * @return the Field Type with just the ID set for the provided Domain Field
   *     Type
   */
  private static FieldType toFieldType(
          @Nonnull final Map<String, Integer> fieldTypeIdsByName,
          @Nonnull final DomainFieldType domainFieldType) {
    final String fieldTypeName = TO_FIELD_TYPE.get(domainFieldType);
    final Integer fieldTypeId = fieldTypeIdsByName.get(fieldTypeName);
    if (null == fieldTypeId) {
      throw new DomainException("No matching FieldType for " + domainFieldType);
    }

    // Return the field type with just the ID set
    final FieldType fieldType = new FieldType();
    fieldType.setId(fieldTypeId);
    return fieldType;
  }

  /**
   * Returns the field format identifier from the Field Data Type returned from
   * the persistent store.
   *
   * @param fieldDataType the Field Data Type record
   * @return the field format identifier from the Field Data Type
   */
  private static String toFieldFormat(
          @Nonnull final FieldDataType fieldDataType) {
    final String format =
            checkNotNull(fieldDataType, "fieldDataType must not be null")
                    .getName();
    return checkNotNull(format, "FieldDataType.name must not be null");
  }

  /**
   * Returns the Field Data Type record with just the ID set for the provided
   * field format.
   *
   * @param fieldDataTypeIdsByName the map of Field Data Type IDs by name
   * @param fieldFormat the provided field format identifier
   * @return the Field Data Type record with just the ID set for the provided
   *     field format
   */
  private static FieldDataType toFieldDataType(
          @Nonnull final Map<String, Integer> fieldDataTypeIdsByName,
          @Nonnull final String fieldFormat) {
    final Integer fieldDataTypeId = fieldDataTypeIdsByName.get(fieldFormat);
    if (null == fieldDataTypeId) {
      throw new DomainException("No matching FieldDataType for '"
              + fieldFormat + "' from one of these: "
              + fieldDataTypeIdsByName.keySet());
    }

    // Return the field data type with just the ID set
    final FieldDataType fieldDataType = new FieldDataType();
    fieldDataType.setId(fieldDataTypeId);
    return fieldDataType;
  }

  /**
   * Returns the set of Aliases from the list found within a Field from the
   * persistent store.
   *
   * @param languageNamesById the map of language names keyed by the integer ID
   * @param aliases the list of aliases retrieved for the field
   * @return the set of Aliases from the list found within a Field
   */
  @Nonnull
  private static Set<AliasData> toAliasDataSet(
          @Nonnull final Map<Integer, String> languageNamesById,
          @Nullable final List<Aliases> aliases) {
    if (null == aliases) {
      return Collections.emptySet();
    }
    final Map<Integer, Set<String>> aliasesByLanguage = new HashMap<>();
    aliases.forEach(alias -> aliasesByLanguage.computeIfAbsent(
            alias.getLanguageId(),
            HashSet::new).add(alias.getAlias()));
    return aliasesByLanguage.entrySet().stream()
            .map(entry -> new AliasData(
                    entry.getValue(),
                    DomainDataMapper.getLanguageNameFromId(
                            languageNamesById,
                            entry.getKey())))
            .collect(Collectors.toSet());
  }

  /**
   * Returns the list of Aliases records each with just the language ID and
   * alias name set.
   *
   * @param languageIdsByName the map of Language IDs keyed by language name
   * @param aliasInfoSet the set of Alias Data from a single field where each
   *     element has a distinct language
   * @return the list of Aliases records each with just the language ID and
   *     alias name set
   */
  private static List<Aliases> toAliasesListForField(
          @Nonnull final Map<String, Integer> languageIdsByName,
          @Nonnull final Set<AliasInfo> aliasInfoSet) {
    final List<Aliases> aliasesList = new LinkedList<>();
    aliasInfoSet.forEach(alias ->
            aliasesList.addAll(toAliasesList(languageIdsByName, alias)));
    return aliasesList;
  }

  /**
   * Returns the Aliases record with just the language ID and alias text set.
   *
   * @param languageIdsByName the map of supported language IDs keyed by
   *     the string name
   * @param aliasInfo all the aliases for a language and field
   * @return the Aliases record with just the language ID and alias text set
   */
  @Nonnull
  private static List<Aliases> toAliasesList(
          @Nonnull final Map<String, Integer> languageIdsByName,
          @Nonnull final AliasInfo aliasInfo) {
    final int languageId =
            getLanguageIdFromName(languageIdsByName, aliasInfo.getLanguage());
    return aliasInfo.getNames().stream()
            .map(alias -> toAliases(languageId, alias))
            .collect(Collectors.toList());
  }

  private static Aliases toAliases(final int languageId, final String alias) {
    final Aliases aliases = new Aliases();
    aliases.setLanguageId(languageId);
    aliases.setAlias(alias);
    return aliases;
  }

  /**
   * Returns the String language name from the integer ID.  This will throw if
   * there is no entry for the integer ID but that should never happen.
   *
   * @param languageNamesById the map of supported language names keyed by
   *     the integer ID
   * @param languageId the integer ID for the desired language
   * @return the String language name from the integer ID
   */
  @Nonnull
  private static String getLanguageNameFromId(
          @Nonnull final Map<Integer, String> languageNamesById,
          @Nonnull final Integer languageId) {
    checkNotNull(languageId, "languageId must not be null");
    final String language = languageNamesById.get(languageId);
    if (null == language) {
      throw new IllegalStateException("No language name for ID " + languageId);
    }
    return language;
  }

  /**
   * Returns the Integer language ID from the language name.  This will throw if
   * there is no entry for the name.
   *
   * @param languageIdsByName the map of supported language IDs keyed by
   *     the string name
   * @param languageName the language name
   * @return the Integer language ID from the language name
   */
  private static int getLanguageIdFromName(
          @Nonnull final Map<String, Integer> languageIdsByName,
          @Nonnull final String languageName) {
    checkNotNull(languageName, "languageName must not be null");
    final Integer languageId = languageIdsByName.get(languageName);
    if (null == languageId) {
      throw new DomainException("No language ID for name " + languageName);
    }
    return languageId;
  }

  /** Hide the constructor. */
  private DomainDataMapper() {}

}
