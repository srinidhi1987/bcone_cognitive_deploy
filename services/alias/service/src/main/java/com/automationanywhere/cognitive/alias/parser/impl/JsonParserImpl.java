package com.automationanywhere.cognitive.alias.parser.impl;

import com.automationanywhere.cognitive.alias.models.CustomResponse;
import com.automationanywhere.cognitive.alias.parser.contract.JsonParser;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
@Component("jsonParser")
public class JsonParserImpl implements JsonParser
{
    private final static AALogger log = AALogger.create(JsonParserImpl.class);

    private final ObjectMapper mapper;

    @Autowired
    public JsonParserImpl(
        final ObjectMapper mapper
    ) {
      this.mapper = mapper;
    }

  @Override
  public String getResponse(Object response) {
    try {
      return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);

    } catch (JsonProcessingException e) {
      log.error(e.getMessage(), e);
      return "";
    }
  }
}
