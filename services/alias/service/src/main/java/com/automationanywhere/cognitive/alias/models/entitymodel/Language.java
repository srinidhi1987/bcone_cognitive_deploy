package com.automationanywhere.cognitive.alias.models.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
@Entity
@Table(name = "LanguageMaster")
public class Language {
    @Id
    @Column(name="id")
    private int id;
    public int getId() { return this.id; }
    public void setId(int id) { this.id = id; }

    @Column(name="name")
    private String name;
    public String getName() { return this.name ;}
    public void setName(String name) { this.name = name; }

    @Column(name="code")
    private String code;
    public String getCode() { return this.code ;}
    public void setCode(String code) { this.code = code; }
}
