package com.automationanywhere.cognitive.alias.service.contract;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.exception.customexceptions.DuplicateDataException;
import com.automationanywhere.cognitive.alias.models.entitymodel.DocType;
import com.automationanywhere.cognitive.alias.models.entitymodel.Fields;
import com.automationanywhere.cognitive.alias.models.entitymodel.Language;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Mayur.Panchal on 08-03-2017.
 */
public interface AliasService {
    /**
     * Returns the map of DocType names with their corresponding numeric IDs.
     * @return the map of DocType names with their corresponding numeric IDs
     * @throws DataAccessLayerException thrown when there is an error
     *     retrieving the requested information
     */
    Map<String, Integer> getDocTypeNamesWithIds()
            throws DataAccessLayerException;
    /**
     * Returns the map of FieldType names with their corresponding numeric IDs.
     * @return the map of FieldType names with their corresponding numeric IDs
     * @throws DataAccessLayerException thrown when there is an error
     *     retrieving the requested information
     */
    Map<String, Integer> getFieldTypeNamesWithIds()
            throws DataAccessLayerException;
    /**
     * Returns the map of FieldDataType names with their corresponding numeric
     * IDs.
     * @return the map of FieldDataType names with their corresponding numeric
     * IDs
     * @throws DataAccessLayerException thrown when there is an error
     *     retrieving the requested information
     */
    Map<String, Integer> getFieldDataTypeNamesWithIds()
            throws DataAccessLayerException;

    /**
     * Performs a bulk import for the DocType record, all of its Fields, and
     * all of its Aliases.
     *
     * @param docType the record holding all the information needed for the
     *     import
     * @throws DuplicateDataException thrown when there is already a DocType
     *     with the same name
     * @throws DataAccessLayerException thrown when there is an error inserting
     *     records
     */
    void bulkInsertDocTypeFieldsAndAliases(DocType docType)
            throws DataAccessLayerException;
    DocType getDocTypeDetails(int doctypeid) throws DataAccessLayerException;
    List<DocType> getAllDocTypes() throws DataAccessLayerException;
    Fields getFields(int fieldId) throws DataAccessLayerException;
    List<Language> getAllLanguages() throws DataAccessLayerException;
    Language getLanguage(int languageId) throws DataAccessLayerException;

    /**
     * Returns the set of languages supported for each domain where the key
     * is the domain name, and the set elements are the language names.  The
     * map holding the result is a Linked Hash Map and the entries
     * are ordered by domain name.
     *
     * @return the set of languages supported for each domain in an ordered map
     *     keyed by domain name
     * @throws DataAccessLayerException thrown when there is an error retrieving
     *     the results
     */
    Map<String, Set<String>> getLanguagesForEachDomain()
            throws DataAccessLayerException;
}
