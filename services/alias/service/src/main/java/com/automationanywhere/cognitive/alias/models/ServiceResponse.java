package com.automationanywhere.cognitive.alias.models;

/**
 * Created by Purnil.Soni on 05-12-2016.
 */
public class ServiceResponse {
    private boolean success;
    public boolean getSuccess() { return this.success; }
    public void setSuccess(boolean value) { this.success = value; }

    private Object data;
    public Object getData() { return this.data; }
    public void setData(Object value) { this.data = value; }

    private String errors;
    public String getErrors() { return this.errors; }
    public void setErrors(String value) { this.errors = value; }
}
