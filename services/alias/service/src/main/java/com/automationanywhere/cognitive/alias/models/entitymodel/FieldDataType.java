package com.automationanywhere.cognitive.alias.models.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mayur.Panchal on 14-03-2017.
 */
@Entity
@Table(name = "FieldDataTypeMaster")
public class FieldDataType {
    @Id
    @Column(name= "id")
    private int id;
    @Column(name="name")
    private String name;

    public int getId() { return this.id; }
    public void setId(int id) { this.id = id; }

    public String getName() { return this.name ;}
    public void setName(String name) { this.name = name; }
}
