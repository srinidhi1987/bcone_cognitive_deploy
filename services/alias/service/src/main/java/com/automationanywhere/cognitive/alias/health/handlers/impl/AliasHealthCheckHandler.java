package com.automationanywhere.cognitive.alias.health.handlers.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.alias.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.alias.health.connections.ConnectionType;
import com.automationanywhere.cognitive.alias.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.alias.health.connections.HealthCheckConnectionFactory;
import com.automationanywhere.cognitive.common.health.handlers.AbstractHealthHandler;
import com.automationanywhere.cognitive.common.healthapi.constants.Connectivity;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
/**
 * @author shweta.thakur
 *
 */
public class AliasHealthCheckHandler implements AbstractHealthHandler {
	@Autowired
	HealthCheckConnectionFactory healthCheckConnectionFactory ;
	AALogger aaLogger = AALogger.create(this.getClass());

	@Override
	public String checkHealth() {
	    aaLogger.entry();
	    HealthCheckConnection healthCheckConnection;
		HTTPStatusCode httpStatusCode = HTTPStatusCode.OK;
		Connectivity dbConnectivity =  Connectivity.OK;
		for (ConnectionType connectionType : ConnectionType.values()) {
			healthCheckConnection = healthCheckConnectionFactory.getConnection(connectionType);
			try {
                healthCheckConnection.checkConnection();
            } catch (DataAccessLayerException e) {
                aaLogger.error("Exception occurred while testing DB connection ", e);
                dbConnectivity = Connectivity.FAILURE;
            } catch (Exception ex ){
                aaLogger.error("Exception occurred while testing DB connection ", ex);
                dbConnectivity = Connectivity.FAILURE;
            }
		}

		String subsystem = HealthApiResponseBuilder.prepareSubSystem(httpStatusCode,SystemStatus.ONLINE , dbConnectivity, Connectivity.NOT_APPLICABLE,Connectivity.NOT_APPLICABLE, null);
		aaLogger.exit();
		return subsystem;
	}
}
