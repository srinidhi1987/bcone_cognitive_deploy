package com.automationanywhere.cognitive.service.shareabledomain.internal;

import com.automationanywhere.cognitive.service.shareabledomain.ShareableDomainInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Immutable data transfer object holding the "shareable" Domain metadata that
 * may be exposed outside and published to the Bot Store.
 *
 * @author Erik K. Worth
 */
public class ShareableDomain implements ShareableDomainInfo {

  private static final String JSON_DOMAIN_NAME = "name";
  private static final String JSON_DOMAIN_METADATA = "domain";
  private static final String JSON_VERSION = "version";

  /** The name of the domain such as "Invoices" */
  private final String name;

  /** The base64 encoded representation of the binary domain metadata */
  private final String domain;

  /** The version of the domain metadata encoding */
  private final int version;

  /**
   * Construct from elements.
   *
   * @param name the name of the domain such as "Invoices"
   * @param domain the base64 encoded representation of the binary domain metadata
   * @param version the version of the domain metadata encoding
   */
  @JsonCreator
  public ShareableDomain(
          @JsonProperty(JSON_DOMAIN_NAME) @Nonnull final String name,
          @JsonProperty(JSON_DOMAIN_METADATA) @Nonnull final String domain,
          @JsonProperty(JSON_VERSION) final int version) {
    this.name = checkNotNull(name, "name must not be null");
    this.domain = checkNotNull(domain, "domain must not be null");
    this.version = version;
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    final ShareableDomainInfo that = (ShareableDomainInfo) other;
    return version == that.getVersion()
            && Objects.equals(name, that.getName())
            && Objects.equals(domain, that.getDomain());
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, domain, version);
  }

  /**
   * Returns the domain name such as "Invoices".
   *
   * @return the domain name such as "Invoices"
   */
  @Override
  @Nonnull
  @JsonProperty(JSON_DOMAIN_NAME)
  public String getName() {
    return name;
  }

  /**
   * Returns the base64 encoded representation of the binary domain metadata.
   *
   * @return the base64 encoded representation of the binary domain metadata
   */
  @Override
  @Nonnull
  @JsonProperty(JSON_DOMAIN_METADATA)
  public String getDomain() {
    return domain;
  }

  /**
   * Returns the version of the encoded content as an integer.
   *
   * @return the version of the encoded content as an integer
   */
  @Override
  @JsonProperty(JSON_VERSION)
  public int getVersion() {
    return version;
  }
}
