package com.automationanywhere.cognitive.alias.models.customentitymodel;

import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 21-02-2017.
 */
public abstract class CustomEntityModelBase<T> {
    public abstract Map<String,Object> getCustomQuery(StringBuilder queryString);
    public abstract List<T> objectToClass(List<Object> result);
}
