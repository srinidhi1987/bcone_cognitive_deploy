package com.automationanywhere.cognitive.gateway;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nakuldev.Patel on 14-04-2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomResponse {
    private boolean success;
    private Object data;
    private String[] errors;
    private int httpStatusCode;


    public int HttpStatusCode(){
        return httpStatusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String[] getErrors() {
        return errors;
    }

    public void setError(String[] error) {
        this.errors = error;
    }

    public void setHttpStatusCode(int httpStatusCode){
        this.httpStatusCode = httpStatusCode;
    }

    public String toJson() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        Object data = this.data!=null ? this.data.toString() : this.data;
        stringBuilder.append(" data: " + data);
        stringBuilder.append(" success: "+ this.success);
        if(!this.success)
            stringBuilder.append(" error: ['" + Arrays.toString(this.errors) + "']");
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    public Map<String, Object> toMap() {
        Map<String, Object> hashMap = new HashMap<String, Object>();

        hashMap.put("success",this.success);

        if(!this.success) {
            hashMap.put("error", this.errors);
            hashMap.put("data", null);
        }
        else {
            hashMap.put("data", this.data.toString());
            hashMap.put("error", null);
        }
        return hashMap;
    }
}
