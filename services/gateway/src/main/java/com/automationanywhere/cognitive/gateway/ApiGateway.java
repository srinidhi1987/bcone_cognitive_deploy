package com.automationanywhere.cognitive.gateway;

import static com.automationanywhere.cognitive.gateway.constants.HeaderConstants.CORRELATION_ID;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.function.Function;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.ThreadContext;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.gateway.apilock.JsonUtil;
import com.automationanywhere.cognitive.gateway.apilock.LockedFor;
import com.automationanywhere.cognitive.gateway.apilock.Locker;
import com.automationanywhere.cognitive.gateway.exception.impl.ExceptionFilter;
import com.automationanywhere.cognitive.gateway.exception.impl.HttpStatusCode;
import com.automationanywhere.cognitive.util.Json;
import com.automationanywhere.cognitive.util.JsonSchema;
import com.automationanywhere.cognitive.util.Pair;
import com.automationanywhere.cognitive.util.ServiceStatus;
import com.automationanywhere.cognitive.util.ServiceStatuses;
import com.fasterxml.jackson.databind.ObjectMapper;

import spark.Request;
import spark.Response;
import spark.Service;

public class ApiGateway {
    public enum HTTP_METHODS {GET, POST, PUT, PATCH, DELETE}

    private enum HTTP_PROTOCOL {HTTP, HTTPS}

    private static boolean isSSLConfigured;
    private static String strRouteConfigJsonSchema;
    private static String strAuthHost, aliasHost, fileManagerHost, projectServiceHost, visionBotHost, validatorHost, reportHost;
    private static int iAuthPort, aliasPort, fileManagerPort, projectServicePort, visionBotPort, validatorPort, reportPort;
    private static Map<Integer, Service> localPortServiceMap;
    private static AALogger logger = AALogger.create(ApiGateway.class);

    private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations\\CognitivePlatform.jks";
    private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations\\Cognitive.properties";
    private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
    private static final String PROPERTIES_NOTFOUND = "NotFound";
    private static String LOCK_ERROR_MESSAGE = "Learning Instance %s is currently in progress. Please retry this action after some time.";

    // TODO strAuthHost and iAuthPort better put into Map of parameters
    // TODO supplied to requestHandler

    static {
        strRouteConfigJsonSchema =
                ("{'type':'object','properties':{" +
                        "  'localPort':{'type':'integer','minimum':100,'maximum':65535}," +
                        "  'remotePort':{'type':'integer','minimum':100,'maximum':65535}," +
                        "  'remoteHost':{'type':'string'}," +
                        "  'routes':{" +
                        "    'type':'array','items':{" +
                        "      'type':'object','properties':{" +
                        "        'httpMethod':{'type':'string','enum':['get','post','put','patch','delete']}," +
                        "        'uri':{'type':'string'}" +
                        "        }" +
                        "      }" +
                        "    }" +
                        "  }" +
                        "}").replaceAll("'", "\"");
    }

    private static Pair readJsonFromFile(String jsonFileName) {
        logger.entry();
        String strJsonApiSpec;
        try {
            strJsonApiSpec = String.join("\n",
                    Files.readAllLines(Paths.get(jsonFileName)));
        } catch (IOException e) {
            logger.error("Exception occurred: " + e.getMessage());
            Pair result = new Pair(
                false,
                "exception: " + e.getMessage()
            );
            return result;
        } finally {
            logger.exit();
        }
        return Json.parse(strJsonApiSpec);
    }

    private static int sendHttpRequest(HTTP_METHODS httpMethod, String strUrl,
                                       Map<String, String> headerMap,
                                       byte[] body) throws IOException {
        logger.entry();
        logger.debug("Parameters : Http method %s URL %s headerMap %s payload %s", httpMethod.name(), strUrl, headerMap.toString(), body);
        String data = null;
        int responseCode = HttpStatusCode.INTERNAL_SERVER_ERROR.getCode();
        HttpURLConnection conn = null;
        try {
            URL url = new URL(strUrl);
            logger.trace("Opening http connection ");
            conn = (HttpURLConnection) url.openConnection();
            logger.trace("Retrieved http connection object.. ");
            if (httpMethod.toString() == "PATCH") {
                conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
                conn.setRequestMethod("POST");
            } else {
                conn.setRequestMethod(httpMethod.toString());
            }

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                headerMap.put("Content-Length", "" + body.length);
            }
            for (String header : headerMap.keySet())
                conn.setRequestProperty(header, headerMap.get(header));
            conn.setRequestProperty(CORRELATION_ID,ThreadContext.get(CORRELATION_ID));
            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                logger.trace(" Write to data output stream.. ");
                wr.write(body, 0, body.length);
                wr.close();
            }
            InputStream is;
            if (conn.getResponseCode() == 200) {
                responseCode = HttpStatusCode.OK.getCode();
                is = conn.getInputStream();
            } else {
                responseCode = HttpStatusCode.INTERNAL_SERVER_ERROR.getCode();
                is = conn.getErrorStream();
            }
            // defaultCharset will check the file encoding if not found it will take utf-8 Charset  
            data = IOUtils.toString(is, StandardCharsets.UTF_8);

        } catch (final IOException ex) {
            logger.error("Error sending an HTTP request", ex);
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        logger.exit();
        return responseCode;
    }

    //For custom response -> Auth/Authorization.
    private static CustomResponse sendHttpRequestForAuthAndAuthorization(HTTP_METHODS httpMethod, String strUrl,
                                                                         Map<String, String> headerMap,
                                                                         byte[] body, Response responseForReturnToClient) {
        logger.entry();
        logger.debug(() -> "Parameters : Http method " + httpMethod.name() + " URL " + strUrl + " headerMap " + headerMap.toString() + " payload " + body);
        String data = null;
        HttpURLConnection conn = null;
        CustomResponse responseFinal = new CustomResponse();
        try {
            URL url = new URL(strUrl);
            logger.trace("Opening http connection");
            conn = (HttpURLConnection) url.openConnection();

            logger.trace("Retrieved http connection object.. ");
            if (httpMethod.toString() == "PATCH") {
                conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
                conn.setRequestMethod("POST");
            } else {
                conn.setRequestMethod(httpMethod.toString());
            }

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                headerMap.put("Content-Length", "" + body.length);
            }
            for (String header : headerMap.keySet())
                conn.setRequestProperty(header, headerMap.get(header));
            conn.setRequestProperty(CORRELATION_ID,ThreadContext.get(CORRELATION_ID));
            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                logger.trace("Write to data output stream.. ");
                wr.write(body, 0, body.length);
                wr.close();
            }
            InputStream is;
            int responseStatusCode = conn.getResponseCode();
            logger.debug("ResponseStatusCode: " + String.valueOf(responseStatusCode));
            if (responseStatusCode == 200) {
                is = conn.getInputStream();
                responseFinal.setSuccess(true);
            } else {
                is = conn.getErrorStream();
                responseFinal.setSuccess(false);
            }

            data = IOUtils.toString(is, StandardCharsets.UTF_8);

            responseFinal.setHttpStatusCode(responseStatusCode);
            responseFinal.setData(data);

            //set response and return the response to caller.
            responseForReturnToClient.status(responseStatusCode);
            responseForReturnToClient.body(data);
            responseForReturnToClient.type("application/json");

        } catch (IOException ex) {
            logger.error("IOException occurred : " + ex.getMessage());
            responseFinal.setSuccess(false);
            responseFinal.setData(null);
            responseFinal.setHttpStatusCode(500);
            //set response and return the response to caller.
            responseForReturnToClient.status(500);
            responseForReturnToClient.body(Json.toString(responseFinal.toMap()));
        } catch (Exception e) {
            logger.error("Exception occurred : " + e.getMessage());
            responseFinal.setSuccess(false);
            responseFinal.setData(null);
            responseFinal.setHttpStatusCode(500);
            //set response and return the response to caller.
            responseForReturnToClient.status(500);
            responseForReturnToClient.body("Internal Server Error!");
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        logger.exit();
        return responseFinal;
    }

    private static byte[] sendHttpRequestBytes(HTTP_METHODS httpMethod, String strUrl,
                                               Map<String, String> headerMap,
                                               byte[] body, Response responseForReturnToClient) {
        logger.entry();
        logger.debug(() -> "Parameters : Http method " + httpMethod.name() + " URL " + strUrl + " headerMap " + headerMap.toString() + " payload " + body);
        byte[] data = null;
        HttpURLConnection conn = null;
        try {
			URL url = new URL(strUrl);
            logger.trace("Opening http connection ");
            conn = (HttpURLConnection) url.openConnection();
            logger.trace("Retrieved http connection object.. ");

            boolean isLockAcquired = Locker.getInstance().acquireLock(strUrl);
            if(!httpMethod.equals(HTTP_METHODS.GET)
                    && Locker.getInstance().checkApiLock()
                    && !isLockAcquired)
            {
                makeApiLockResponse(responseForReturnToClient);
                return getApiLockResponseBody().getBytes();
            }

            if (httpMethod.toString() == "PATCH") {
                conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
                conn.setRequestMethod("POST");
            } else {
                conn.setRequestMethod(httpMethod.toString());
            }

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                headerMap.put("Content-Length", "" + body.length);
            }
            for (String header : headerMap.keySet())
                conn.setRequestProperty(header, headerMap.get(header));
            conn.setRequestProperty(CORRELATION_ID,ThreadContext.get(CORRELATION_ID));
            if (httpMethod == HTTP_METHODS.POST ||
                    httpMethod == HTTP_METHODS.PUT ||
                    httpMethod == HTTP_METHODS.PATCH) {
                logger.trace("Write to data output stream.. ");
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                wr.write(body, 0, body.length);
                wr.close();
            }
            InputStream is;
            if (conn.getResponseCode() == 200) {
                is = conn.getInputStream();
                startExportMonitoring(strUrl);
            } else {
                Locker.getInstance().releaseLock(strUrl);
                is = conn.getErrorStream();
            }
            Map<String, List<String>> responseHeader = conn.getHeaderFields();

            Iterator it = responseHeader.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = (String) pair.getKey();
                @SuppressWarnings("unchecked")
                List<String> value = (List<String>) pair.getValue();
                if (key != null
                        && value != null
                        && value.size() > 0) {
                    responseForReturnToClient.header(key, value.get(0));
                }
            }
            responseForReturnToClient.status(conn.getResponseCode());
            data = IOUtils.toByteArray(is);

        } catch (IOException ex) {
            Locker.getInstance().releaseLock(strUrl);
            logger.error(ex.getMessage());
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        logger.exit();
        return data;
    }

    private static String getLockErrorMessage()
    {
        LockedFor lockedFor = Locker.getInstance().getLockedFor();
        switch (lockedFor)
        {
            case export:
                return String.format(LOCK_ERROR_MESSAGE, "export");
            case imports:
                return String.format(LOCK_ERROR_MESSAGE, "import");
            default:
                return String.format(LOCK_ERROR_MESSAGE, "activity");
        }
    }

    private static void startExportMonitoring(String strUrl) {
        Locker.getInstance().startMonitoring(strUrl);
    }

    private static void makeApiLockResponse(Response response)
    {
        response.status(HttpStatusCode.RESOURCELOCKED.getCode());
        response.header("content-type", "application/json");
    }

    private static String getApiLockResponseBody()
    {
        CustomResponse customResponse = new CustomResponse();
        customResponse.setHttpStatusCode(HttpStatusCode.RESOURCELOCKED.getCode());
        customResponse.setSuccess(false);
        customResponse.setData(null);
        customResponse.setError(new String[]{getLockErrorMessage()});
        return JsonUtil.toJsonString(customResponse);
    }

    private static boolean checkAuthCheckAndForwardRequestArguments(
            Map argsMap) {
        return !(argsMap.get("req") == null ||
                !(argsMap.get("req") instanceof Request) ||
                argsMap.get("res") == null ||
                !(argsMap.get("res") instanceof Response) ||
                argsMap.get("remoteHostPort") == null ||
                !(argsMap.get("remoteHostPort") instanceof String) ||
                argsMap.get("action") == null ||
                !(argsMap.get("action") instanceof String));
    }

    private static Function<Map<String, Object>, byte[]>
            authCheckAndForwardRequest = argsMap -> {
        logger.entry();
        if (!checkAuthCheckAndForwardRequestArguments(argsMap)) {
            logger.error("wrong arguments in Map");
            return "wrong arguments in Map".getBytes();
        }

        Request req = (Request) argsMap.get("req");
        Response res = (Response) argsMap.get("res");
        String remoteHostPort = (String) argsMap.get("remoteHostPort");
        String action = (String) argsMap.get("action");
        // TODO strAuthHost and iAuthPort can be in params map - ?
        String strAuthorizationHeader = req.headers("Authorization");
        //Get ClientToken from header.

        String strClientToken = req.headers("ClientToken");
        String strClientKey = req.headers("ClientKey");
        //When visionbot command runs, it provides client key and client token.
        //It should be authorized differently
        //For now by pass authorization till we have new api in CR(ETA: 4/21) and forward the request.
        if (strClientKey != null && strClientToken != null) {
            String protocol = isSSLConfigured ? HTTP_PROTOCOL.HTTPS.toString() : HTTP_PROTOCOL.HTTP.toString();
            CustomResponse response = sendHttpRequestForAuthAndAuthorization(HTTP_METHODS.POST,
                    protocol + "://" + strAuthHost + ":" + iAuthPort + "/authorization",
                    new HashMap<>(), ("{\"clientkey\":\"" + strClientKey + "\"," +
                            "\"clienttoken\":\"" + strClientToken + "\"}").getBytes(), res);
            logger.debug(() -> "Response from sendHttpRequestForAuthAndAuthorization " + res.body().toString());
            if (res.status() != 200)
                return res.body().getBytes();
            return forwardRequestAndGetResponseAsBytes(req, res, remoteHostPort);
        }
        if (strAuthorizationHeader == null ||
                !strAuthorizationHeader.startsWith("Bearer ")) {
            return "bad Authorization header".getBytes();
        }
        String rawToken = strAuthorizationHeader.substring(7).trim();
        String protocol = isSSLConfigured ? HTTP_PROTOCOL.HTTPS.toString() : HTTP_PROTOCOL.HTTP.toString();
        CustomResponse responsePermissionCheck = sendHttpRequestForAuthAndAuthorization(HTTP_METHODS.POST,
                protocol + "://" + strAuthHost + ":" + iAuthPort + "/authorization",
                new HashMap<>(), ("{\"action\":\"" + action + "\"," +
                        "\"jwt\":\"" + rawToken + "\"}").getBytes(), res);

        if (res.status() != 200)
            return res.body().getBytes();
        logger.exit();
        return forwardRequestAndGetResponseAsBytes(req, res, remoteHostPort);
    };

    private static byte[] forwardRequestAndGetResponseAsBytes(Request req, Response res, String remoteHostPort) {
        logger.entry();
        // forward request
        HTTP_METHODS httpMethod =
                HTTP_METHODS.valueOf(req.requestMethod());
        Map<String, String> mapHeaders = new HashMap<>();
        for (String header : req.headers())
            mapHeaders.put(header, req.headers(header));

        String url = req.uri();
        if (req.queryString() != null && req.queryString() != "")
            url += "?" + req.queryString();
        return sendHttpRequestBytes(httpMethod,
                remoteHostPort + url,
                mapHeaders, req.bodyAsBytes(), res);
    }

    private static Object makeRouteConfigJson(int localPort,
                                              Object swaggerJson,
                                              String remoteHost,
                                              int remotePort) {
        logger.entry();
        Map<String, Object> routeJson = new HashMap<>();
        routeJson.put("localPort", localPort);
        routeJson.put("remoteHost", remoteHost);
        routeJson.put("remotePort", remotePort);
        List<Map> routes = new ArrayList<>();
        routeJson.put("routes", routes);
        Map swaggerRoutes = (Map) ((Map) swaggerJson).get("paths");
        for (Object route : swaggerRoutes.keySet()) {
            Map methods = (Map) swaggerRoutes.get(route);
            for (Object method : methods.keySet()) {
                Map<String, String> routeItem = new HashMap<>();
                routeItem.put("httpMethod", (String) method);
                routeItem.put("uri", (String) route);
                routes.add(routeItem);
            }
        }
        logger.exit();
        return routeJson;
    }

    private static Map<String, Object> addReqRes(
            Map<String, Object> commonParams, Request req, Response res) {
        Map<String, Object> params = new HashMap<>(commonParams);
        params.put("req", req);
        params.put("res", res);
        return params;
    }

    private static String makeForwardRequestEntries(
            Object jsonRouteConfig,
            Function<Map<String, Object>, byte[]> forwardingHandler) {
        logger.entry();
        String result = JsonSchema.validateJson(jsonRouteConfig,
                (Map) Json.parse(strRouteConfigJsonSchema).right);
        if (!"".equals(result)) {
            logger.error("invalid route config JSON " + Json.toString(jsonRouteConfig));
            return "invalid route config JSON " + Json.toString(jsonRouteConfig);
        }

        int iPort = (int) ((Map) jsonRouteConfig).get("localPort");
        Service http = localPortServiceMap.get(iPort);
        if (http == null) {
            http = Service.ignite();
            http.port(iPort);
            setServiceSecurity(http);
            localPortServiceMap.put(iPort, http);
        }

        List routes = (List) ((Map) jsonRouteConfig).get("routes");
        for (Object route : routes) {
            String routeUri = (String) ((Map) route).get("uri");
            StringBuilder sbUri = new StringBuilder();
            for (String pathFragment : routeUri.split("/")) {
                if (!pathFragment.isEmpty()) {
                    if (pathFragment.startsWith("{"))
                        sbUri.append("/:").append(pathFragment.substring(
                                1, pathFragment.length() - 1));
                    else
                        sbUri.append("/").append(pathFragment);
                }

            }
            if (routeUri.endsWith("/")) {
                sbUri.append("/");
            }

            final String uri = sbUri.toString();
            String httpMethod = (String) ((Map) route).get("httpMethod");
            String remoteHost =
                    (String) ((Map) jsonRouteConfig).get("remoteHost");
            int remotePort = (int) ((Map) jsonRouteConfig).get("remotePort");
            String protocol = isSSLConfigured ? HTTP_PROTOCOL.HTTPS.toString() : HTTP_PROTOCOL.HTTP.toString();
            String remoteHostPort = protocol + "://" + remoteHost + ":" + remotePort;
            Map<String, Object> commonParams = new HashMap<>();
            commonParams.put("remoteHostPort", remoteHostPort);
            commonParams.put("action", uri);
            switch (httpMethod) {
                case "get":
                    http.get(uri, (req, res) -> forwardingHandler.apply(
                            addReqRes(commonParams, req, res)));
                    break;
                case "post":
                    http.post(uri, (req, res) -> forwardingHandler.apply(
                            addReqRes(commonParams, req, res)));
                    break;
                case "put":
                    http.put(uri, (req, res) -> forwardingHandler.apply(
                            addReqRes(commonParams, req, res)));
                    break;
                case "patch":
                    http.patch(uri, (req, res) -> forwardingHandler.apply(
                            addReqRes(commonParams, req, res)));
                    break;
                case "delete":
                    http.delete(uri, (req, res) -> forwardingHandler.apply(
                            addReqRes(commonParams, req, res)));
                    break;
            }
        }
        logger.exit();
        return "";
    }

    private static String processForwardingTetradeArguments(
            String[] args, int iArgs,
            Function<Map<String, Object>, byte[]> requestHandler) {
        logger.entry();
        for (int i = iArgs; i < args.length; ) {
            int localPort = Integer.parseInt(args[i++]);
            Pair pairParsedJson = readJsonFromFile(args[i++]);
            if (!(Boolean) pairParsedJson.left) {
                logger.error("JSON is invalid");
                return "JSON is invalid";
            }
            String remoteHost = args[i++];
            int remotePort = Integer.parseInt(args[i++]);
            Object jsonRoutes = makeRouteConfigJson(localPort,
                    pairParsedJson.right, remoteHost, remotePort);
            String result =
                    makeForwardRequestEntries(jsonRoutes, requestHandler);
            if (!"".equals(result))
                return result;
        }
        logger.traceExit();
        return "";
    }

    public static void main(String[] args) {
        if (args.length < 7 || (args.length - 3) % 4 != 0) {
            logger.error("Invalid arguments..");
            System.out.println("input should be authentication entry point " +
                    "port, auth service host, auth service port and one or " +
                    "more tetrades (localPort, JSON API remoteHost, " +
                    "remotePort)");
            return;
        }
        int iAuthEntryPointPort = Integer.parseInt(args[0]);
        initializeCommandLineArguments(args);
        localPortServiceMap = new HashMap<>();
        String result = processForwardingTetradeArguments(
                args, 3, authCheckAndForwardRequest);
        if (!"".equals(result)) {
            System.out.println(result);
            logger.trace(() -> result);
            return;
        }
        logger.trace("Continuing after processForwardingTetradeArguments ");

        Service http = localPortServiceMap.get(iAuthEntryPointPort);
        if (http == null) {
            http = Service.ignite().port(iAuthEntryPointPort);
            setServiceSecurity(http);
            localPortServiceMap.put(iAuthEntryPointPort, http);
        } else {
            logger.trace("http not null");
        }
        http.get("/health", ((request, response) -> {
            logger.entry();
            return processHealthCheck(response);
        }));
		
		http.get("/heartbeat", ((request, response) -> {
            return getHeartBeatInfo(response);
        }));

        http.before((request, response) -> preRequestProcessor(request));

        http.notFound((req, res) -> {
          ObjectMapper objectMapper = new ObjectMapper();
          logger.error(String.format("This route %s not supported by Spark.." , req.uri()));
          res.status(403);
          res.type("application/json");
          CustomResponse responseFinal = new CustomResponse();
          responseFinal.setHttpStatusCode(HttpStatusCode.FORBIDDEN.getCode());
          responseFinal.setError(new String[]{"You are not allowed to access this Resource."});
          return objectMapper.writeValueAsString(responseFinal);
        });

        ExceptionFilter exceptionFilter = new ExceptionFilter();
        exceptionFilter.init();
        http.post("/authentication", (req, res) -> {
            // pass to auth /authenticate {"username":"...","password":"..."}
            Map<String, String> mapHeaders = new HashMap<>();
            for (String header : req.headers())
                mapHeaders.put(header, req.headers(header));
            String inMsg = req.body();
            Pair pairJsonInMsg = Json.parse(inMsg);
            if (!(Boolean) pairJsonInMsg.left ||
                    !(pairJsonInMsg.right instanceof Map)) {
                return "authorization msg bad JSON";
            }
            Map jsonInMsgMap = (Map) pairJsonInMsg.right;
            String msg = "{\"username\":\"" +
                    jsonInMsgMap.get("username") +
                    "\",\"password\":\"" +
                    jsonInMsgMap.get("password") + "\"}";
            mapHeaders.put("Content-Length",
                    "" + msg.getBytes().length);
            String protocol = isSSLConfigured ? HTTP_PROTOCOL.HTTPS.toString() : HTTP_PROTOCOL.HTTP.toString();
            CustomResponse customResponse = sendHttpRequestForAuthAndAuthorization(HTTP_METHODS.POST,
                    protocol + "://" + strAuthHost + ":" + iAuthPort +
                            "/authentication", mapHeaders,
                    msg.getBytes(), res);
            String returnJson = customResponse.getData().toString();
            res.status(customResponse.HttpStatusCode());
            return returnJson;
        });
    }

    private static void preRequestProcessor(Request request) {
        addTransactionIdentifier(request);
        logger.debug("Request URI.." + request.uri());
    }

    private static void addTransactionIdentifier(Request request) {
        String transactionId = request.headers(CORRELATION_ID);
        ThreadContext.put(CORRELATION_ID, transactionId != null ?
                transactionId : UUID.randomUUID().toString());
    }


    private static void initializeCommandLineArguments(String[] arguments) {
        strAuthHost = arguments[1];
        iAuthPort = Integer.parseInt(arguments[2]);
        aliasHost = arguments[5];
        aliasPort = Integer.parseInt(arguments[6]);
        fileManagerHost = arguments[9];
        fileManagerPort = Integer.parseInt(arguments[10]);
        projectServiceHost = arguments[13];
        projectServicePort = Integer.parseInt(arguments[14]);
        visionBotHost = arguments[17];
        visionBotPort = Integer.parseInt(arguments[18]);
        validatorHost = arguments[21];
        validatorPort = Integer.parseInt(arguments[22]);
        reportHost = arguments[25];
        reportPort = Integer.parseInt(arguments[26]);

        logger.debug(String.format("Commandline argument: AuthHost: %s, AuthPort: %s" +
                ", aliasHost: %s, aliasPort: %s" +
                ", fileManagerHost: %s, fileManagerPort: %s" +
                ", projetManagerHost: %s, projectManagerPort: %s" +
                ", visionBotHost: %s, visionBotPort: %s" +
                ", validatorHost: %s, validatorPort: %s" +
                ", reportHost: %s, reportPort: %s",
                strAuthHost,String.valueOf(iAuthPort),
                aliasHost,String.valueOf(iAuthPort),
                fileManagerHost,String.valueOf(fileManagerPort),
                projectServiceHost,String.valueOf(projectServicePort),
                visionBotHost,String.valueOf(visionBotPort),
                validatorHost,String.valueOf(validatorPort),
                reportHost,String.valueOf(reportPort)));
    }

    private static String processHealthCheck(Response response) {
        boolean isAvailable = true;
        logger.entry();
        ObjectMapper objectMapper = new ObjectMapper();
        CustomResponse healthResponse = new CustomResponse();
        ServiceStatuses serviceStatuses = new ServiceStatuses();
        try {
            serviceStatuses.setServiceStatuses(getStatuses(response));
            for (ServiceStatus serviceStatus : serviceStatuses.getServiceStatuses()) {
                if (serviceStatus.getStatusCode() != 200) {
                    isAvailable = false;
                    break;
                }
            }
            healthResponse.setData(serviceStatuses);
            if (!isAvailable) {
                response.status(HttpStatusCode.INTERNAL_SERVER_ERROR.getCode());
                healthResponse.setSuccess(false);
            } else {
                response.status(HttpStatusCode.OK.getCode());
                healthResponse.setSuccess(true);
            }
            return objectMapper.writeValueAsString(healthResponse);

        } catch (Exception ex) {
            logger.error("Exception checking system health: " + ex.getStackTrace());
        }
        response.status(HttpStatusCode.INTERNAL_SERVER_ERROR.getCode());
        logger.exit();
        return HttpStatusCode.INTERNAL_SERVER_ERROR.getMessage();
    }

    private static List<ServiceStatus> getStatuses(Response response) throws IOException {
        logger.entry();
        List<ServiceStatus> serviceStatuses = new ArrayList<ServiceStatus>();
        serviceStatuses.add(checkServiceHealth("Authentication", strAuthHost, iAuthPort));
        serviceStatuses.add(checkServiceHealth("Alias", aliasHost, aliasPort));
        serviceStatuses.add(checkServiceHealth("FileManager", fileManagerHost, fileManagerPort));
        serviceStatuses.add(checkServiceHealth("Project", projectServiceHost, projectServicePort));
        serviceStatuses.add(checkServiceHealth("VisionBot", visionBotHost, visionBotPort));
        serviceStatuses.add(checkServiceHealth("Validator", validatorHost, validatorPort));
        serviceStatuses.add(checkServiceHealth("Report", reportHost, reportPort));
        logger.exit();

        return serviceStatuses;
    }

    private static ServiceStatus checkServiceHealth(String serviceName, String host, int port) throws IOException {
        logger.entry();
        logger.debug(String.format("ServiceName: %s, Host:%s, Port: %s", serviceName, host, String.valueOf(port)));
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setName(serviceName);
        String protocol = isSSLConfigured ? HTTP_PROTOCOL.HTTPS.toString() : HTTP_PROTOCOL.HTTP.toString();
        Map<String, String> mapHeaders = new HashMap<>();
        mapHeaders.put("Content-Length", "0");
        serviceStatus.setStatusCode(sendHttpRequest(HTTP_METHODS.GET, protocol + "://" + host + ":" + port + "/health", mapHeaders, null));
        logger.exit();
        return serviceStatus;
    }

    private static void setServiceSecurity(Service http) {
        logger.entry();
        if (isSSLCertificateConfigured()) {
            isSSLConfigured = true;
            logger.trace("TLS configured.");
            try {
                String certificateKey = loadSSLCertificatePassword();
                if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
                    Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
                    String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
                    logger.debug("certificate path: " + cognitiveCertificateFilePath);
                    http.secure(cognitiveCertificateFilePath, certificateKey, null, null);
                } else {
                    logger.trace("Certificate key not found.");
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.trace("TLS not configured");
        }
        logger.exit();
    }

    private static String loadSSLCertificatePassword() throws IOException {
        logger.entry();
        Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitivePropertiesFilePath = propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
        if (fileExists(cognitivePropertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitivePropertiesFilePath));
            String certificateKey = properties.getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
            logger.exit();
            return certificateKey;
        } else {
            logger.traceExit("Cognitive Properties file not found");
            return PROPERTIES_NOTFOUND;
        }
    }

    private static boolean isSSLCertificateConfigured() {
        logger.entry();
        boolean hasCertificate = false;
        Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
        if (fileExists(cognitiveCertificateFilePath)) {
            hasCertificate = true;
        } else {
            logger.trace("certificate file not found");
        }
        logger.exit();
        return hasCertificate;
    }

    private static boolean fileExists(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }

 	/**
     * Get the heartbeat of the service
     * @param response
     * @return
     */
    private static Object getHeartBeatInfo(Response response) {
    	logger.entry();
		String heartBeat;
    	//currently for project service jetty is not started when message MQ is down, so no need to check for Beans Initialization 
		response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
		heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
		response.body(heartBeat);
		logger.exit();
		return response;
	}
}
