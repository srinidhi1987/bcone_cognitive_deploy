package com.automationanywhere.cognitive.gateway.apilock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String toJsonString(Object object) {
        try {
            String jsonString = objectMapper.writeValueAsString(object);
            return jsonString;
        } catch (JsonProcessingException ex) {
            return "";
        }
    }

    public static <T> T fromJsonString(String jsonString, Class<T> clazz) throws InvalidInputException {
        try {
            return objectMapper.readValue(jsonString, clazz);
        } catch (Exception ex) {
            throw new InvalidInputException("Invalid input.", ex);
        }
    }

}
