/*
 * Copyright (c) 2017 Automation Anywhere. All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.util;

public class ServiceStatus {
    private String name;
    private int statusCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
