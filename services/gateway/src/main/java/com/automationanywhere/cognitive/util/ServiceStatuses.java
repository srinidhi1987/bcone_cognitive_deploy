package com.automationanywhere.cognitive.util;


import java.util.ArrayList;
import java.util.List;

public class ServiceStatuses {
    private List<ServiceStatus> serviceStatuses;

    public ServiceStatuses(){
        serviceStatuses = new ArrayList<>();
    }
    public List<ServiceStatus> getServiceStatuses() {
        return serviceStatuses;
    }

    public void setServiceStatuses(List<ServiceStatus> serviceStatuses) {
        this.serviceStatuses = serviceStatuses;
    }
}
