package com.automationanywhere.cognitive.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonPath {
    public static Pair getSingleByPath(Object json, List<String> path) {
        Pair result = new Pair(false, null);
        Object curJsonRoot = json;
        for (Object pathFragment : path)
            if(curJsonRoot == null)
                return result;
            else if (pathFragment instanceof String)
                curJsonRoot = ((Map) curJsonRoot).get(pathFragment);
            else if (pathFragment instanceof Integer)
                curJsonRoot = ((List) curJsonRoot).get((Integer) pathFragment);
            else
                return result;
        result = new Pair(true, curJsonRoot);
        return result;
    }

    public static Pair parse(String path) { // '.'  | ( '.' ids | '.' [ ( dqs | int ) ] ) +
        Pair result = new Pair(false, null);
        if (path == null || path.isEmpty() ||
                path.length() == 1 && !path.equals("."))
            return result;
        Str str = new Str("'" + path + "'", "'", true);
        str.skipOne();
        if(str.first() != '.')
            return result;
        List<Object> list = new ArrayList<>();
        result = new Pair(result, list);
        if(path.equals(".")) {
            result = new Pair(true, result); // empty list
            return result;
        }
        do {
            Character c = str.first();
            if (c == null) {
                result = new Pair(true, result);
                break;
            }
            if (c == '.') { // read non-whitespace-str
                Str nonWsStr = new Str(str, " .['", false);
                list.add(nonWsStr.str);
            } else if (c == '[') {
                str.skipOne();
                if (str.first() == null)
                    break;
                if (str.first() == '"') { // read double-quoted-string
                    Str key = new Str(str, "\"", true);
                    if (key.str == null)
                        break;
                    list.add(key.str);
                    if (str.first() != ']')
                        break;
                    str.skipOne();
                    result = new Pair(true, result);
                } else if (str.first() == '-' || // read number
                        str.first() >= '0' && str.first() <= '9') {
                    int i = 0;
                    if (str.first() == '-')
                        ++i;
                    while (str.get(i) != null &&
                            str.get(i) >= '0' && str.get(i) <= '9')
                        i++;
                    if (str.get(i) != ']')
                        break;
                    else {
                        int num = Integer.parseInt(str.head(i));
                        str.skip(i + 1); // + 1 for ]
                        list.add(num);
                    }
                } else
                    break;
            } else if (str.first() == '\'') {
                result = new Pair(true, result);
                break;
            } else {
                str.skipOne();
                break;
            }
        } while (true);
        return result;
    }
}
