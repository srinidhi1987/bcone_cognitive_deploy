package com.automationanywhere.cognitive.util;

public class Pair {
    public final Object left;
    public final Object right;

    public Pair() {
        left = null;
        right = null;
    }

    public Pair(final Object left, final Object right) {
        this.left = left;
        this.right = right;
    }

    public Pair(final Object left, final Pair right) {
        this.left = left;
        this.right = right.right;
    }

    public Pair(final Pair left, final Object right) {
        this.left = left.left;
        this.right = right;
    }
}
