package com.automationanywhere.cognitive.gateway.apilock;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.gateway.ApiGateway;
import com.automationanywhere.cognitive.gateway.CustomResponse;

import java.net.URL;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Locker {
    private static final String TASK_MONITOR_URL = "/organizations/1/projects/tasks/status";
    private static final String EXPORT_URL = "/projects/export/start";
    private static final String IMPORT_URL = "/projects/import/start";
    private static boolean isActivityRunning = false;
    private static Locker locker;
    private LockedFor lockedFor;

    private static AALogger logger = AALogger.create(Locker.class);

    private static RestClient restClient;

    private Timer timer;

    public static void setIsActivityRunning(boolean isActivityRunning) {
        locker.isActivityRunning = isActivityRunning;
    }

    public Locker() {
        restClient = new RestClient();
    }

    public LockedFor getLockedFor() {
        return lockedFor;
    }

    public void setLockedFor(LockedFor lockedFor) {
        this.lockedFor = lockedFor;
    }

    public static Locker getInstance()
    {
        if(locker == null)
        {
            locker = new Locker();
        }

        return locker;
    }

    public boolean acquireLock(String url)
    {
        if(isUrlMatching(url) && !isActivityRunning)
        {
            logger.info("Lock acquired for url: " + url);
            isActivityRunning = true;
            return true;
        }

        return false;
    }

    public void releaseLock(String url)
    {
        if(isUrlMatching(url))
        {
            logger.info("Lock released for url: " + url);
            isActivityRunning = false;
        }
    }

    public boolean checkApiLock()
    {
        return isActivityRunning;
    }

    public void startMonitoring(String url) {
        if(isUrlMatching(url) && isActivityRunning)
        {
            logger.info("Monitoring starts. Triggered by: "+ url);
            setActivityType(url);
            try {
                timer = new Timer();
                URL triggeringUrl = new URL(url);
                isActivityRunning = true;
                startPolling(triggeringUrl.getProtocol(), triggeringUrl.getHost(),triggeringUrl.getPort());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                isActivityRunning = false;
            }
        }
    }

    private boolean isUrlMatching(String url)
    {
        return url.endsWith(EXPORT_URL) || url.endsWith(IMPORT_URL);
    }

    private void setActivityType(String url)
    {
        if(url.endsWith(EXPORT_URL))
        {
            lockedFor = LockedFor.export;
        }

        if(url.endsWith(IMPORT_URL))
        {
            lockedFor = LockedFor.imports;
        }
    }

    private void startPolling(String protocol, String host, int port) throws InvalidInputException {
        String pollUrl = String.format("%s://%s:%s%s", protocol, host, port, TASK_MONITOR_URL);
        logger.info("Poll url: " + pollUrl);
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                try{
                CustomResponse customResponse = restClient.makeRequest(pollUrl, ApiGateway.HTTP_METHODS.GET, null, CustomResponse.class);
                List responseData = (List) customResponse.getData();
                if(responseData.size() == 0) {
                    timer.cancel();
                    timer.purge();
                    isActivityRunning = false;
                    return;
                }

                isActivityRunning = true;
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    logger.info("Releasing lock.");
                    Locker.setIsActivityRunning(false);
                    timer.cancel();
                    timer.purge();
                }
            }
        }, 1, 10000);
    }
}
