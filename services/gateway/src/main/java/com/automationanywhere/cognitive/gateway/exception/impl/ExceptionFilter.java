/**
 * Copyright (c) 2016 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.gateway.exception.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.gateway.CustomResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Response;
import static spark.Spark.exception;
/**
 * This filter handles exceptions that has been thrown from resource/service layer.
 * It maps java exceptions to the corresponding HTTP status/response.
 */
public class ExceptionFilter  {
    private ObjectMapper mapper = new ObjectMapper();	
    private AALogger logger = AALogger.create(this.getClass());


    public void init() {
       exception(Exception.class, (e, req, res) -> handleException(e, res, HttpStatusCode.INTERNAL_SERVER_ERROR));
    }

    private void handleException(Exception e, Response res, HttpStatusCode statusCode) {
        logger.entry();
        logger.error(e.getMessage());
        String errorMessage = e.getMessage() == null ? statusCode.getMessage() : e.getMessage();
        res.status(statusCode.getCode());
        CustomResponse customResponse=new CustomResponse();
        customResponse.setSuccess(false);
        String customResonseInFormOfJson="";
        try {
            customResponse.setError(new String[]{errorMessage});
            customResonseInFormOfJson=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);
            logger.trace("Exception Handler Response :" + customResonseInFormOfJson);
        }
        catch (Exception ex)
        {
			logger.error("Exception Occurred :" +  errorMessage, ex);
        }
        res.body(customResonseInFormOfJson);
        logger.exit();
    }
}
