package com.automationanywhere.cognitive.gateway.apilock;

public class InvalidInputException extends Exception {
    
    private static final long serialVersionUID = 7770783051422244878L; 
    
    public InvalidInputException(String message)
    {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
