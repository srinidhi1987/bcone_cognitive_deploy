package com.automationanywhere.cognitive.gateway.apilock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.automationanywhere.cognitive.gateway.ApiGateway;
import com.automationanywhere.cognitive.gateway.exception.impl.HttpStatusCode;

public class RestClient {
    public <T, S> T makeRequest(String requestUrl, ApiGateway.HTTP_METHODS requestType, Class<S> body, Class<T> response) throws InvalidInputException, IOException {
        OutputStream ouputStream = null;
        URL url = new URL(requestUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(requestType.toString());
        conn.setRequestProperty("Accept", "application/json");

        if(body != null)
        {
            ouputStream = conn.getOutputStream();
            ouputStream.write(JsonUtil.toJsonString(body).getBytes());
            ouputStream.flush();
        }

        if (conn.getResponseCode() != HttpStatusCode.OK.getCode()) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String tempString;
        StringBuilder outputStringBuilder = new StringBuilder();
        while ((tempString = br.readLine()) != null) {
            outputStringBuilder.append(tempString);
        }

        conn.disconnect();

        return JsonUtil.fromJsonString(outputStringBuilder.toString(), response);
    }
}
