package com.automationanywhere.cognitive.gateway.apilock;

public enum LockedFor {
    imports,
    export
}
