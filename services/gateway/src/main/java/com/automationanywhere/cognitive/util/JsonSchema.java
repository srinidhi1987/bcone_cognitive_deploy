package com.automationanywhere.cognitive.util;

import com.automationanywhere.cognitive.common.logger.AALogger;

import java.util.*;

public class JsonSchema {

    private static AALogger logger = AALogger.create(JsonSchema.class);
    // grammar approximates http://cswr.github.io/JsonSchema/spec/grammar/
    static private final String[] stringKeywords = new String[]{
            "minLength", "maxLength", "pattern"};
    static private final String[] numberKeywords = new String[]{
            "minimum", "exclusiveMinimum", "maximum", "exclusiveMaximum", "multipleOf"};
    static private final String[] arrayKeywords = new String[]{
            "items", "additionalItems", "minItems", "maxItems", "uniqueItems"};
    static private final String[] objectKeywords = new String[]{
            "properties", "additionalProperties", "required", "minProperties", "maxProperties", "dependencies", "patternProperties"};

    public static String validateJson(Object json, Map schema) {
        logger.entry();
        String result = "";
        try {
            if (schema == null)
                return "schema is null;";
            if (schema.get("allOf") != null)
                result += validateAllOf(json, schema);
            else if (schema.get("anyOf") != null)
                result += validateAnyOf(json, schema);
            else if (schema.get("oneOf") != null)
                result += validateOneOf(json, schema);
            else if (schema.get("not") != null)
                result += validateNot(json, schema);
            else if (schema.get("enum") != null)
                result += validateEnum(json, schema);
            // title, description and default are always valid
            Object schemaType = schema.get("type");
            if (schemaType == null) {
                for (String keyword : stringKeywords)
                    if (schema.get(keyword) != null)
                        result += validateString(json, schema);
                for (String keyword : numberKeywords)
                    if (schema.get(keyword) != null)
                        result += validateNumber(json, schema);
                for (String keyword : arrayKeywords)
                    if (schema.get(keyword) != null)
                        result += validateArray(json, schema);
                for (String keyword : objectKeywords)
                    if (schema.get(keyword) != null)
                        result += validateObject(json, schema);
                return result; // "nothing" is valid as it's a no schema
            }
            if (schemaType instanceof String) {
                String schemaStrType = (String) schema.get("type");
                result += validateTypedJson(json, schemaStrType, schema);
            } else if (schemaType instanceof List) {
                List schemaTypes = (List) schemaType;
                boolean isValid = false;
                for (Object schemaTypeItem : schemaTypes) {
                    if (!(schemaTypeItem instanceof String)) {
                        logger.warn("schema type item not a string");
                        return "schema type item not a string;";
                    }
                    String strSchemaType = (String) schemaTypeItem;
                    String typeCheckResult = validateTypedJson(json, strSchemaType, schema);
                    if ("".equals(typeCheckResult)) {
                        isValid = true;
                        break;
                    }
                }
                if (!isValid) {
                    logger.warn("neither type is satisfied");
                    return "neither type is satisfied";
                }
            } else {
                logger.warn("schema type not a string or list");
                return "schema type not a string or list";
            }
        } catch (Exception e) {
            logger.error("JSON validation exception", e);
            result += "schema Exception\n" + e.getMessage() + ";";
        } finally {
            logger.exit();
        }
        return result;
    }

    static private String validateTypedJson(Object json, String type, Map schema) {
        if ("string".equals(type))
            return validateString(json, schema);
        else if ("integer".equals(type))
            return validateInteger(json, schema);
        else if ("number".equals(type))
            return validateNumber(json, schema);
        else if ("boolean".equals(type))
            return validateBoolean(json);
        else if ("null".equals(type))
            return validateNull(json);
        else if ("array".equals(type))
            return validateArray(json, schema);
        else if ("object".equals(type))
            return validateObject(json, schema);
        else if (schema.get("enum") != null)
            return validateEnum(json, schema);
        else
            return "schema unknown type: " + type + ";";
    }

    static private String validateString(Object json, Map schema) {
        if (!(json instanceof String)) {
            logger.error("Json instance is not a string");
            return "not a string;";
        }
        String strJson = (String) json;
        Object schemaMinLen = schema.get("minLength");
        if (schemaMinLen != null) {
            if (!(schemaMinLen instanceof Number)) {
                logger.error("schema minLen not a number");
                return "schema minLen not a number;";
            }
            if (strJson.length() < (Integer) schemaMinLen)
                return "minLen: " + schemaMinLen +
                        ", actual: " + strJson.length() + ";";
        }
        Object schemaMaxLen = schema.get("maxLength");
        if (schemaMaxLen != null) {
            if (!(schemaMaxLen instanceof Number)) {
                logger.error("schema minLen not a number");
                return "schema minLen not a number;";
            }
            if (strJson.length() > (Integer) schemaMaxLen)
                return "maxLen: " + schemaMaxLen +
                        ", actual: " + strJson.length() + ";";
        }
        Object schemaPattern = schema.get("pattern");
        if (schemaPattern != null) {
            if (!(schemaPattern instanceof String)) {
                logger.error("schema pattern not a string");
                return "schema pattern not a string;";
            }
            if (!matches(strJson, (String) schemaPattern))
                return "pattern: " + schemaPattern +
                        ", strJson: " + strJson + ";";
        }
        return "";
    }

    static private String validateDouble(Double numJson, Map schema) {
        String result = "";
        Object schemaMinimum = schema.get("minimum");
        if (schemaMinimum != null) {
            if (!(schemaMinimum instanceof Number)) {
                logger.error("schema minimum not a number");
                return "schema minimum not a number;";
            }
            double dMin;
            if (schemaMinimum instanceof Integer)
                dMin = (double) (int) schemaMinimum;
            else if (schemaMinimum instanceof Double)
                dMin = (double) schemaMinimum;
            else {
                logger.error("schema unknown min type");
                return "schema unknown min type;";
            }
            if (numJson < dMin)
                return "min: " + schemaMinimum +
                        ", actual: " + numJson + ";";
            Object schemaExclusiveMinimum = schema.get("exclusiveMinimum");
            if (schemaExclusiveMinimum != null) {
                if (!(schemaExclusiveMinimum instanceof Boolean)) {
                    logger.error("schema exclusiveMinimum not a boolean");
                    return "schema exclusiveMinimum not a boolean;";
                }
                if ((Boolean) schemaExclusiveMinimum && numJson == dMin)
                    return "excluded min: " + schemaMinimum +
                            ", actual: " + numJson + ";";
            }
        }
        Object schemaMaximum = schema.get("maximum");
        if (schemaMaximum != null) {
            if (!(schemaMaximum instanceof Number)) {
                logger.error("schema maximum not a number;");
                return "schema maximum not a number;";
            }
            double dMax;
            if (schemaMaximum instanceof Integer)
                dMax = (double) (int) schemaMaximum;
            else if (schemaMaximum instanceof Double)
                dMax = (double) schemaMaximum;
            else {
                logger.error("schema unknown max type;");
                return "schema unknown max type;";
            }
            if (numJson > dMax)
                return "max: " + schemaMaximum +
                        ", actual: " + numJson + ";";
            Object schemaExclusiveMaximum = schema.get("exclusiveMaximum");
            if (schemaExclusiveMaximum != null) {
                if (!(schemaExclusiveMaximum instanceof Boolean)) {
                    logger.error("schema exclusiveMaximum not a boolean;");
                    return "schema exclusiveMaximum not a boolean;";
                }
                if ((Boolean) schemaExclusiveMaximum && numJson == dMax)
                    return "excluded max: " + schemaMaximum +
                            ", actual: " + numJson + ";";
            }
        }
        Object schemaMultipleOf = schema.get("multipleOf");
        if (schemaMultipleOf != null) {
            if (!(schemaMultipleOf instanceof Number)) {
                logger.error("schema multipleOf not a number;");
                return "schema multipleOf not a number;";
            }
            double dMultipleOf;
            if (schemaMultipleOf instanceof Integer)
                dMultipleOf = (double) (int) schemaMultipleOf;
            else if (schemaMultipleOf instanceof Double)
                dMultipleOf = (double) schemaMultipleOf;
            else {
                logger.error("schema unknown multipleOf type;");
                return "schema unknown multipleOf type;";
            }
            if (numJson % dMultipleOf != 0.0)
                return "multiple of " + schemaMultipleOf +
                        ", actual: " + numJson + ";";
        }
        return result;
    }

    static private String validateInteger(Object json, Map schema) {
        if (!(json instanceof Integer)) {
            logger.error("not an integer;");
            return "not an integer;";
        }
        int numJson = (Integer) json;
        return validateDouble((double) numJson, schema);
    }

    static private String validateNumber(Object json, Map schema) {
        if (!(json instanceof Number)) {
            logger.error("not a number;");
            return "not a number;";
        }
        if (json instanceof Integer)
            return validateInteger(json, schema);
        return validateDouble((double) json, schema);
    }

    static private String validateBoolean(Object json) {
        if (!(json instanceof Boolean)) {
            logger.error("not a boolean;");
            return "not a boolean;";
        }
        return "";
    }

    static private String validateNull(Object json) {
        return (json != null) ? "not null;" : "";
    }

    static private String validateArray(Object json, Map schema) {
        if (!(json instanceof List)) {
            logger.error("not an array;");
            return "not an array;";
        }
        List listJson = (List) json;
        Object schemaItems = schema.get("items");
        if (schemaItems != null) {
            if (schemaItems instanceof Map) {
                for (Object item : listJson) {
                    String itemResult = validateJson(item, (Map) schemaItems);
                    if (!"".equals(itemResult))
                        return itemResult;
                }
            } else if (schemaItems instanceof List) {
                List schemaList = (List) schemaItems;
                int minListSize = schemaList.size();
                for (int i = 0; i < minListSize; ++i) {
                    Object schemaListItem = schemaList.get(i);
                    if (!(schemaListItem instanceof Map)) {
                        logger.error("schema item not a map;");
                        return "schema item not a map;";
                    }
                    Object objJson = listJson.get(i);
                    String itemResult = validateJson(objJson, (Map) schemaListItem);
                    if (!"".equals(itemResult))
                        return itemResult;
                }
                Object schemaAdditionalItems = schema.get("additionalItems");
                if (schemaAdditionalItems != null) {
                    if ((schemaAdditionalItems instanceof Boolean) &&
                            !((Boolean) schemaAdditionalItems) &&
                            listJson.size() > schemaList.size()) {
                        logger.error("additional items when none allowed;");
                        return "additional items when none allowed;";
                    }
                    if (schemaAdditionalItems instanceof Map) {
                        List additionalJsonItems = listJson.subList(minListSize, listJson.size());
                        for (Object additionalJsonItem : additionalJsonItems) {
                            String itemResult = validateJson(additionalJsonItem, (Map) schemaAdditionalItems);
                            if (!"".equals(itemResult))
                                return itemResult;
                        }
                    }
                }
            } else {
                logger.error("schema unknown type of array schema;");
                return "schema unknown type of array schema;";
            }
        }
        Object schemaMinItems = schema.get("minItems");
        if (schemaMinItems != null) {
            if (!(schemaMinItems instanceof Number)) {
                logger.error("schema minItems not a number;");
                return "schema minItems not a number;";
            }
            Double dMinItems;
            if (schemaMinItems instanceof Integer)
                dMinItems = (double) (int) schemaMinItems;
            else if (schemaMinItems instanceof Double)
                dMinItems = (double) schemaMinItems;
            else {
                logger.error("schema unknown minItems type;");
                return "schema unknown minItems type;";
            }
            if (listJson.size() < dMinItems)
                return "minItems: " + dMinItems +
                        ", actual: " + listJson.size() + ";";
        }
        Object schemaMaxItems = schema.get("maxItems");
        if (schemaMaxItems != null) {
            if (!(schemaMaxItems instanceof Number)) {
                logger.error("schema maxItems not a number;");
                return "schema maxItems not a number;";
            }
            Double dMaxItems;
            if (schemaMaxItems instanceof Integer)
                dMaxItems = (double) (int) schemaMaxItems;
            else if (schemaMaxItems instanceof Double)
                dMaxItems = (double) schemaMaxItems;
            else {
                logger.error("schema unknown maxItems type;");
                return "schema unknown maxItems type;";
            }
            if (listJson.size() > dMaxItems)
                return "maxItems: " + dMaxItems +
                        ", actual: " + listJson.size() + ";";
        }
        Object schemaUniqueness = schema.get("uniqueItems");
        if (schemaUniqueness != null) {
            if (!(schemaUniqueness instanceof Boolean)) {
                logger.error("schema uniqueItems not boolean");
                return "schema uniqueItems not boolean";
            }
            if ((Boolean) schemaUniqueness) {
                // TODO check uniqueness - use elementEqual() with n^2 comparisons, sorting or hashing?
                // TODO objects and arrays with changed order of elements are the same...
            }
        }
        return "";
    }

    static private String validateObject(Object json, Map schema) {
        if (!(json instanceof Map))
            return "not an object;";
        Map mapJson = (Map) json;
        boolean bAdditionalProperties = true; // default
        Map mapAdditionalProperties = null; // default
        Object schemaAdditionalProperties = schema.get("additionalProperties");
        if (schemaAdditionalProperties != null) {
            if (schemaAdditionalProperties instanceof Boolean)
                bAdditionalProperties = (Boolean) schemaAdditionalProperties;
            else if (schemaAdditionalProperties instanceof Map)
                mapAdditionalProperties = (Map) schemaAdditionalProperties;
            else {
                logger.error("schema additionalProperties not boolean or object;");
                return "schema additionalProperties not boolean or object;";
            }
        }
        Set<Object> schemaSetRequired = new HashSet<>();
        Object schemaRequired = schema.get("required");
        if (schemaRequired != null) {
            if (!(schemaRequired instanceof List)) {
                logger.error("schema required not a list;");
                return "schema required not a list;";
            }
            schemaSetRequired.addAll((List<?>) schemaRequired);
        }
        int intMinProperties = -1;
        Object schemaMinProperties = schema.get("minProperties");
        if (schemaMinProperties != null) {
            if (!(schemaMinProperties instanceof Integer)) {
                logger.error("schema minProperties not integer;");
                return "schema minProperties not integer;";
            }
            intMinProperties = (int) schemaMinProperties;
            if (intMinProperties <= 0) {
                logger.error("schema minProperties not positive;");
                return "schema minProperties not positive;";
            }
        }
        int intMaxProperties = -1;
        Object schemaMaxProperties = schema.get("maxProperties");
        if (schemaMaxProperties != null) {
            if (!(schemaMaxProperties instanceof Integer)) {
                logger.error("schema maxProperties not integer;");
                return "schema maxProperties not integer;";
            }
            intMaxProperties = (int) schemaMaxProperties;
            if (intMaxProperties <= 0) {
                logger.error("schema maxProperties not positive;");
                return "schema maxProperties not positive;";
            }
        }
        int nJsonProps = mapJson.keySet().size();
        if (intMinProperties > 0 && nJsonProps < intMinProperties)
            return "minProperties: " + intMinProperties +
                    ", actual: " + nJsonProps + ";";
        if (intMaxProperties > 0 && nJsonProps > intMaxProperties)
            return "maxProperties: " + intMaxProperties +
                    ", actual: " + nJsonProps + ";";
        Object schemaProperties = schema.get("properties");
        List<Object> checkedJsonProperties = new ArrayList<>();
        if (schemaProperties != null) {
            if (!(schemaProperties instanceof Map)) {
                logger.error("schema properties not an object;");
                return "schema properties not an object;";
            }
            Map schemaMapProperties = (Map) schemaProperties;
            for (Object key : schemaMapProperties.keySet()) {
                if (!(key instanceof String)) {
                    logger.error("schema properties key not a string;");
                    return "schema properties key not a string;";
                }
                Object objJson = mapJson.get(key);
                if (objJson != null) {
                    Object schemaPropertiesValue =
                            schemaMapProperties.get(key);
                    if (!(schemaPropertiesValue instanceof Map)) {
                        logger.error("schema object properties item not object;");
                        return "schema object properties item not object;";
                    }
                    String strObjResult =
                            validateJson(objJson, (Map) schemaPropertiesValue);
                    if (!"".equals(strObjResult))
                        return strObjResult;
                    checkedJsonProperties.add(key);
                }
            }
        }
        if (!bAdditionalProperties) {
            for (Object jsonKey : mapJson.keySet()) {
                if (checkedJsonProperties.contains(jsonKey))
                    continue;
                return "disallowed property " + jsonKey + ";";
            }
        } else if (mapAdditionalProperties != null) {
            for (Object jsonKey : mapJson.keySet()) {
                if (checkedJsonProperties.contains(jsonKey))
                    continue;
                Object objProperty = mapJson.get(jsonKey);
                String strObjResult =
                        validateJson(objProperty, mapAdditionalProperties);
                if (!"".equals(strObjResult))
                    return strObjResult;
            }
        }
        for (Object reqKey : schemaSetRequired) {
            if (!mapJson.keySet().contains(reqKey))
                return "required property " + reqKey + " missing" + ";";

        }

        // TODO check dependencies and pattern properties
        return "";
    }

    static private String validateAllOf(Object json, Map schema) {
        Object schemaAllOf = schema.get("allOf");
        if (schemaAllOf == null) {
            logger.error("schema no allOf;");
            return "schema no allOf;";
        }
        if (!(schemaAllOf instanceof List)) {
            logger.error("schema allOf not a list;");
            return "schema allOf not a list;";
        }
        List schemaListAllOf = (List) schemaAllOf;
        for (Object schemaItem : schemaListAllOf) {
            if (!(schemaItem instanceof Map)) {
                logger.error("schema allOf item not a map;");
                return "schema allOf item not a map;";
            }
            String result = validateJson(json, (Map) schemaItem);
            if (!("".equals(result)))
                return result;
        }
        return "";
    }

    static private String validateAnyOf(Object json, Map schema) {
        Object schemaAnyOf = schema.get("anyOf");
        if (schemaAnyOf == null) {
            logger.error("schema no anyOf;");
            return "schema no anyOf;";
        }
        if (!(schemaAnyOf instanceof List)) {
            logger.error("schema anyOf not a list;");
            return "schema anyOf not a list;";
        }
        List schemaListAnyOf = (List) schemaAnyOf;
        for (Object schemaItem : schemaListAnyOf) {
            if (!(schemaItem instanceof Map)) {
                logger.error("schema anyOf item not a map;");
                return "schema anyOf item not a map;";
            }
            String result = validateJson(json, (Map) schemaItem);
            if ("".equals(result))
                return "";
        }
        logger.error("no valid variants in anyOf;");
        return "no valid variants in anyOf;";
    }

    static private String validateOneOf(Object json, Map schema) {
        Object schemaOneOf = schema.get("oneOf");
        if (schemaOneOf == null) {
            logger.error("schema no oneOf;");
            return "schema no oneOf;";
        }
        if (!(schemaOneOf instanceof List)) {
            logger.error("schema oneOf not a list;");
            return "schema oneOf not a list;";
        }
        List schemaListOneOf = (List) schemaOneOf;
        int nValid = 0;
        for (Object schemaItem : schemaListOneOf) {
            if (!(schemaItem instanceof Map)) {
                logger.error("schema oneOf item not a map;");
                return "schema oneOf item not a map;";
            }
            String result = validateJson(json, (Map) schemaItem);
            if ("".equals(result))
                ++nValid;
        }
        if (nValid != 1) {
            logger.error("not exactly one valid schema variant;");
            return "not exactly one valid schema variant;";
        }
        return "";
    }

    static private String validateNot(Object json, Map schema) {
        Object schemaNot = schema.get("not");
        if (schemaNot == null) {
            logger.error("schema no not;");
            return "schema no not;";
        }
        if (!(schemaNot instanceof Map)) {
            logger.error("schema \"not\" part is not an object;");
            return "schema \"not\" part is not an object;";
        }
        String notResult = validateJson(json, (Map) schemaNot);
        if ("".equals(notResult)) {
            logger.error("not is violated;");
            return "not is violated;";
        }
        return "";
    }

    static private String validateEnum(Object json, Map schema) {
        Object schemaEnum = schema.get("enum");
        if (schemaEnum == null) {
            logger.error("schema no enum;");
            return "schema no enum;";
        }
        if (!(schemaEnum instanceof List)) {
            logger.error("schema enum not an array;");
            return "schema enum not an array;";
        }
        // TODO if type is defined, it should be taken as an extra constraint?
        for (Object enumMember : (List) schemaEnum) {
            if (elementEqual(enumMember, json))
                return "";
        }
        return "enum not valid;";
    }

    static private boolean elementEqual(Object obj1, Object obj2) {
        return (obj1 instanceof String &&
                obj2 instanceof String &&
                (obj1).equals(obj2)) ||
                (obj1 instanceof Boolean &&
                        obj2 instanceof Boolean &&
                        obj1 == obj2) ||
                (obj1 instanceof Integer &&
                        obj2 instanceof Integer &&
                        obj1 == obj2) ||
                (obj1 instanceof Double &&
                        obj2 instanceof Double &&
                        obj1 == obj2) ||
                (obj1 == null && obj2 == null) ||
                (obj1 instanceof Map &&
                        obj2 instanceof Map &&
                        objectEqual((Map) obj1, (Map) obj2)) ||
                (obj1 instanceof List &&
                        obj2 instanceof List &&
                        arrayEqual((List) obj1, (List) obj2));
    }

    static private boolean objectEqual(Map map1, Map map2) {
        for (Object key : map1.keySet()) {
            if (!(key instanceof String) ||
                    !map2.keySet().contains(key))
                return false;
            if (!elementEqual(map1.get(key), map2.get(key)))
                return false;
        }
        return true;
    }

    static private boolean arrayEqual(List list1, List list2) {
        for (int i = 0; i < list1.size(); ++i) {
            if (!elementEqual(list1.get(i), list2.get(i)))
                return false;
        }
        return true;
    }

    static private boolean matches(String str, String pattern) {
        return str.matches(pattern);
        // this is an approximation of pattern language at
        // https://spacetelescope.github.io/understanding-json-schema/reference/regular_expressions.html
    }
}
