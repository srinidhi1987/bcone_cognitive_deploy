package com.automationanywhere.cognitive.gateway;

import org.testng.annotations.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomResponseTest {
	
    @Test
    public void testOfSuccessShouldYieldSuccessData() throws Exception {
        // given
        CustomResponse customResponse = new CustomResponse();
        int httpStatusCode = 200;
        boolean success = true;
        String[] errors = new String[]{"error1"};
        Object data = "Some data";

        // when
        customResponse.setHttpStatusCode(httpStatusCode);
        customResponse.setSuccess(success);
        customResponse.setError(errors);
        customResponse.setData(data);
        Map<String, Object> map = customResponse.toMap();
        String json = customResponse.toJson();

        // then
        assertThat(map).isNotNull();
        assertThat(map).containsKeys("success", "error", "data");
        assertThat(map.get("success")).isEqualTo(success);
        assertThat(map.get("error")).isNull();
        assertThat(map.get("data")).isEqualTo(data);
        assertThat(json).isEqualTo("{ data: Some data success: true}");
    }

    @Test
    public void testOfFailureShouldYieldFailureData() throws Exception {
        // given
        CustomResponse customResponse = new CustomResponse();
        int httpStatusCode = 500;
        boolean success = false;
        String[] errors = new String[]{"error1", "error2"};
        Object data = "Some data";

        // when
        customResponse.setHttpStatusCode(httpStatusCode);
        customResponse.setSuccess(success);
        customResponse.setError(errors);
        customResponse.setData(data);
        Map<String, Object> map = customResponse.toMap();
        String json = customResponse.toJson();

        // then
        assertThat(map).isNotNull();
        assertThat(map).containsKeys("success", "error", "data");
        assertThat(map.get("success")).isEqualTo(success);
        assertThat(map.get("error")).isEqualTo(errors);
        assertThat(map.get("data")).isNull();
        assertThat(json).startsWith("{ data: Some data success: false error:");
        assertThat(json).endsWith("}");
    }
}
