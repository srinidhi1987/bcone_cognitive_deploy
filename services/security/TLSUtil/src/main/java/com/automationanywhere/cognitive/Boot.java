package com.automationanywhere.cognitive;

import com.automationanywhere.cognitive.helper.ProcessHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

public class Boot {

    /*
    certificate path:
    password:
    Command:
    keytool -import -trustcacerts -alias root -file cyberroam.cer -keypass %s -keystore "C:\Program Files\Java\jdk1.8.0_111\jre\lib\security\cacerts"
    keytool -importkeystore -srckeystore "C:\Harshil\Certificate\test256.pfx" -srcstorepass changeit -srcstoretype pkcs12 -destkeystore C:\Harshil\Certificate\clientcert.jks -deststoretype JKS -noprompt -v -deststorepass changeit
    */
    private static String srckeystore;
    private static String destkeystore;
    private static String keypass;
    private static String storepass;

   // private static String keyToolCommand = "keytool -import -trustcacerts -alias %s -file \"%s\" -keypass %s -keystore \"%s\" -storepass %s -noprompt -v";
    private static String keyToolCommand = "keytool -importkeystore -srckeystore \"%s\" -srcstorepass %s -srcstoretype pkcs12 -destkeystore \"%s\" -deststoretype JKS -noprompt -deststorepass \"%s\"";


    public static void main(String[] args) throws Exception {
        try {
            loadProperties(args);
            String executionCommand = String.format(keyToolCommand, srckeystore, keypass, destkeystore, storepass);
            ProcessHelper processHelper = new ProcessHelper();
            if(processHelper.executecommand(executionCommand))
                saveProperties();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void loadProperties(String[] args) {
        if (args.length == 2) {
            srckeystore = args[0];
            keypass = args[1];
            storepass = args[1];
            final String dir = System.getProperty("user.dir");
            File file = new File(dir);
            destkeystore = Paths.get(file.getParent(), "configurations\\CognitivePlatform.jks").toString();
        } else
            throw new RuntimeException("Invalid arguments");
    }

    private static void saveProperties() throws IOException {
        //configurations
        String executingDir = System.getProperty("user.dir");
        File file = new File(executingDir);

        String filepath = Paths.get(file.getParent(), "configurations\\Cognitive.properties").toString();
        Properties config = new Properties();
        config.load(new FileInputStream(filepath));
        config.put("CertificateKey", keypass);
        FileOutputStream outputStream = new FileOutputStream(filepath);
        config.store(outputStream,"Updated Certificate key");
    }

}
