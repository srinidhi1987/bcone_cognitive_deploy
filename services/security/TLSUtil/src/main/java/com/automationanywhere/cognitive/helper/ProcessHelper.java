package com.automationanywhere.cognitive.helper;

import java.io.*;

public class ProcessHelper {

    public boolean executecommand(String command) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
        if(process.exitValue() != 0) {
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line;
            String result = "";
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
            System.out.println("Result: " + result);
            return false;
        }
        else {
            System.out.println("Certificate imported successfully");
            return true;
        }
    }
}
