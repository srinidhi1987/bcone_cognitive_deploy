CREATE NONCLUSTERED INDEX [IX_FILEDETAILS_PROJECTID_ISPRODUCTION] ON [FileManager_Demo].[dbo].[FileDetails]
(
	[projectid] ASC,
	[isproduction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE [FileManager_Demo].[dbo].[FileBlobs]  WITH CHECK ADD  CONSTRAINT [FK_FileBlobs_FileDetails] FOREIGN KEY([fileid])
REFERENCES [FileManager_Demo].[dbo].[FileDetails] ([fileid])
GO
ALTER TABLE [FileManager_Demo].[dbo].[FileBlobs] CHECK CONSTRAINT [FK_FileBlobs_FileDetails]