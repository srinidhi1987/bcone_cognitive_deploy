package com.automationanywhere.cognitive.filemanager.service.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;

/**
 * Created by Jemin.Shah on 6/21/2017.
 */
public interface SegmentedDocumentManagementService
{
    String GetSegmentedDocument(String fileId) throws DataNotFoundException, DataAccessLayerException;
    void SaveSegmentedDocument(String fileId, String segmentedDocument) throws DataAccessLayerException;
    void deleteSegmentedData(String fileId)  throws DataAccessLayerException;
    void deleteAllSegmentedData() throws DataAccessLayerException;
}
