package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class MessageQueueConnectionCloseException extends RuntimeException{
    
    private static final long serialVersionUID = -254043867119476529L;

    public MessageQueueConnectionCloseException(String message, Throwable cause) {
        super(message, cause);
    }

}
