package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class BlobCipherException extends RuntimeException {

  /**
   *
   */
  private static final long serialVersionUID = -404078320307660286L;

  public BlobCipherException(String message, Throwable exception) {
    super(message, exception);
  }

}
