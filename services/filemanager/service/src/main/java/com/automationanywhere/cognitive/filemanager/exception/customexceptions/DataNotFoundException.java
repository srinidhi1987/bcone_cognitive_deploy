package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class DataNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = -4370229169323784724L;
    
    public DataNotFoundException(String message) {
        super(message);
    }
}
