package com.automationanywhere.cognitive.filemanager.datalayer.impl;

import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Mayur.Panchal on 11-12-2016.
 */
public class CommonDataAccessLayerImpl implements CommonDataAccessLayer<EntityModelBase,CustomEntityModelBase> {
    private SessionFactory sessionFactory;
    public CommonDataAccessLayerImpl(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    @Override
    public boolean InsertRow(EntityModelBase entityModel)throws DataAccessLayerException
    {
        try{
            sessionFactory.getCurrentSession().save(entityModel);
            sessionFactory.getCurrentSession().flush();
            sessionFactory.getCurrentSession().clear();
            return  true;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Transactional
    @Override
    public boolean InsertBulk(Collection<EntityModelBase> entityModelBaseCollection)throws DataAccessLayerException
    {
        try{
            for(EntityModelBase entityModel:entityModelBaseCollection)
            {
                sessionFactory.getCurrentSession().save(entityModel);
                sessionFactory.getCurrentSession().flush();
                sessionFactory.getCurrentSession().clear();
            }
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
        return  true;
    }

    @Transactional
    @Override
    public boolean UpdateRow(EntityModelBase entityModel)throws DataAccessLayerException
    {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam = entityModel.getUpdateQuery(queryString);
            if(!inputParam.isEmpty()) {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam,query);
                int rowsUpdated = query.executeUpdate();
                if(rowsUpdated==0){
                	throw new DataNotFoundException(entityModel.getClass().getSimpleName() + " not found, either it got deleted or does not exists");
                }
                return true;
            }
            else
                return false;
        }
        catch(DataNotFoundException dnfe){
        	throw dnfe;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    public List<Object> SelectRows(EntityModelBase queryDetail) throws DataAccessLayerException
    {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String, Object> inputParam = queryDetail.getSelectQuery(queryString);
            if (!inputParam.isEmpty())
            {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                fillQueryParameter(inputParam, query);
                @SuppressWarnings("unchecked")
                List<Object> resultset = query.list();
                return resultset;
            }
            else
            {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                @SuppressWarnings("unchecked")
                List<Object> lstObjects = query.list();
                return lstObjects;
            }
        }
        catch(Exception ex)
        {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Transactional
    public boolean DeleteRows(EntityModelBase queryDetail) throws DataAccessLayerException
    {
        try {
            StringBuilder queryString = new StringBuilder();
            Map<String,Object> inputParam = queryDetail.getDeleteQuery(queryString);

            Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
            fillQueryParameter(inputParam,query);
            query.executeUpdate();
            return true;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

  @Override
  @Transactional
  @SuppressWarnings("unchecked")
  public int executeCustomEntityQueryForDmlTypes(CustomEntityModelBase queryDetail) throws DataAccessLayerException {
    try {
      StringBuilder queryString = new StringBuilder();
      Map<String, Object> inputParam  = queryDetail.getCustomQuery(queryString);
      Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
      if (!inputParam.isEmpty()) {
        fillQueryParameter(inputParam, query);
      }
      return query.executeUpdate();
    } catch(Exception ex) {
      throw new DataAccessLayerException(ex.getMessage(),ex);
    }
  }
    @Override
    @Transactional
    public List<Object> ExecuteCustomEntityQuery(CustomEntityModelBase queryDetail) throws DataAccessLayerException {
        try {
            StringBuilder queryString = new StringBuilder();
            @SuppressWarnings("unchecked")
            Map<String, Object> inputParam  = queryDetail.getCustomQuery(queryString);
            Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
            if (!inputParam.isEmpty()) {
                fillQueryParameter(inputParam, query);
            }

            @SuppressWarnings("unchecked")
            List<Object> resultset = query.list();
            @SuppressWarnings("unchecked")
            List<Object> lstOfObjects = queryDetail.objectToClass(resultset);
            return lstOfObjects;
        } catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    public List<Object> CustomQueryExecute(StringBuilder queryString,Map<String,Object> inputParam) throws DataAccessLayerException{
        try {
                Query query = sessionFactory.getCurrentSession().createQuery(queryString.toString());
                if (!inputParam.isEmpty())  fillQueryParameter(inputParam,query);
                @SuppressWarnings("unchecked")
                List<Object> resultset = query.list();
                return resultset;
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage());
        }
    }

    @Override
    @Transactional
    public void executeDataManipulationQuery(String queryString, Map<String, Object> inputParam) throws DataAccessLayerException {
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(queryString);
            if (!inputParam.isEmpty()) fillQueryParameter(inputParam, query);
            query.executeUpdate();
        }
        catch(Exception ex) {
            throw new DataAccessLayerException(ex.getMessage(), ex);
        }
    }

    private void fillQueryParameter(Map<String,Object> inputParam, Query query)
    {
        for (Map.Entry<String, Object> entry : inputParam.entrySet())
        {
            query.setParameter(entry.getKey(),entry.getValue());
        }
    }
}
