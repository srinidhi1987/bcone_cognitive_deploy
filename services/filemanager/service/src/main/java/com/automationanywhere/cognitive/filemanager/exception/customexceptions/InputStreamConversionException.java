package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class InputStreamConversionException extends RuntimeException {

  /**
   *
   */
  private static final long serialVersionUID = -6768156873818876680L;

  public InputStreamConversionException(String message, Throwable exception) {
    super(message, exception);
  }
}
