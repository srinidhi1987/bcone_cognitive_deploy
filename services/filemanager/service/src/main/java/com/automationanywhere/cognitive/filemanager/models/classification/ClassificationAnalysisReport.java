package com.automationanywhere.cognitive.filemanager.models.classification;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh.Savalia on 27-01-2017.
 */
public class ClassificationAnalysisReport {
    @JsonProperty("projectId")
    public  String projectId;
    @JsonProperty("allCategoryDetail")
    public List<CategoryDetail> allCategoryDetail;
    @JsonProperty("totalDocuments")
    public long totalDocuments;
}
