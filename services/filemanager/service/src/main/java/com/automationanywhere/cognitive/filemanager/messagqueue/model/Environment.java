package com.automationanywhere.cognitive.filemanager.messagqueue.model;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
public enum Environment
{
    staging,
    production
}
