package com.automationanywhere.cognitive.filemanager.handlers.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface ClassificationReportHandler {
    String GetClassificationReport(Request request, Response response)throws DataAccessLayerException, DataNotFoundException;
    String GetProjectWiseClassification(Request request, Response response)throws DataAccessLayerException, DataNotFoundException;
    String updateResource(Request request,Response response)throws DataAccessLayerException;
}
