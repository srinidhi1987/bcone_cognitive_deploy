package com.automationanywhere.cognitive.filemanager.datalayer.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 11-12-2016.
 */
public interface CommonDataAccessLayer<T,C> {
    boolean InsertRow(T entityModel) throws DataAccessLayerException;
    boolean InsertBulk(Collection<EntityModelBase> entityModelBaseCollection)throws DataAccessLayerException;
    boolean UpdateRow(T entityModel) throws DataAccessLayerException;
    List<Object> SelectRows(T queryDetail) throws DataAccessLayerException;
    boolean DeleteRows(T queryDetail) throws DataAccessLayerException;
    int executeCustomEntityQueryForDmlTypes(CustomEntityModelBase queryDetail) throws DataAccessLayerException;
    List<Object> ExecuteCustomEntityQuery(C queryDetail) throws DataAccessLayerException;
    List<Object> CustomQueryExecute(StringBuilder query,Map<String,Object> inputParam)throws DataAccessLayerException;
    void executeDataManipulationQuery(String query,Map<String,Object> inputParam)throws DataAccessLayerException;
}
