package com.automationanywhere.cognitive.filemanager.adapter.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.filemanager.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.models.StandardResponse;
import com.automationanywhere.cognitive.filemanager.models.dto.VisionbotData;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.automationanywhere.cognitive.filemanager.constants.HeaderConstants.CORRELATION_ID;

/**
 * Created by msundell on 5/7/17
 */
public class VisionbotAdapterImpl implements VisionbotAdapter {

  private AALogger log = AALogger.create(getClass());

  @Autowired
  @Qualifier("restTemplate")
  private RestTemplate restTemplate;
  /*@Autowired
  private RestOperations restTemplate;*/
  private String rootUrl;
  private final String relativeUrlToFetchAllVisionbot = "/organizations/%s/visionbotdata";
  private final String
      relativeUrlToFetchVisionbotCategorywise =
      "/organizations/%s/projects/%s/visionbotdata";
  @Autowired
  private RestTemplateWrapper restTemplateWrapper;
  private String HEARTBEAT_URL = "/heartbeat";
  
  @Override
  public List<VisionbotData> getVisionbotData(String organizationId) {
    log.entry();

    configureRestTemplate();

    String url = rootUrl + String.format(relativeUrlToFetchAllVisionbot, organizationId);

    VisionbotData[] visionbotDataList = null;

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
      HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      visionbotDataList =
          objectMapper.convertValue(standardResponse.getBody().getData(), VisionbotData[].class);

      if (visionbotDataList == null) {
        log.debug("No. of visionbot : " + 0);
        return null;
      }
      log.debug("No. of visionbot : " + visionbotDataList.length);
      return Arrays.asList(visionbotDataList);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    } finally {
      log.exit();
    }
  }

  @Override
  public List<VisionbotData> getVisionbotData(String organizationId, String projectId) {
    log.entry();

    configureRestTemplate();

    String url =
        rootUrl + String.format(relativeUrlToFetchVisionbotCategorywise, organizationId, projectId);

    VisionbotData[] visionbotDataList = null;

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
      HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
      visionbotDataList =
          objectMapper.convertValue(standardResponse.getBody().getData(), VisionbotData[].class);
      if (visionbotDataList == null) {
        log.debug("No. of visionbot : " + 0);
        return new ArrayList<>();
      }
      log.debug("No. of visionbot : " + visionbotDataList.length);
      return Arrays.asList(visionbotDataList);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ArrayList<>();
    } finally {
      log.exit();
    }
  }

  private void configureRestTemplate() {
    List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
    MappingJackson2HttpMessageConverter
        jsonMessageConverter =
        new MappingJackson2HttpMessageConverter();
    jsonMessageConverter.setObjectMapper(new ObjectMapper());
    messageConverters.add(jsonMessageConverter);
    StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
    messageConverters.add(stringHttpMessageConerters);
    restTemplate.setMessageConverters(messageConverters);
  }

	/**
	 * @param rootUrl
	 *            the rootUrl to set
	 */
	public void setRootUrl(String rootUrl) {
		this.rootUrl = rootUrl;
	}

    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        log.entry();
        String heartBeatURL = rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse = null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL, String.class);
        if (standardResponse!=null && standardResponse.getStatusCode() != HttpStatus.OK) {
            throw new DependentServiceConnectionFailureException("Error while connecting to Visionbot service");
        }
        log.exit();
    }
}