package com.automationanywhere.cognitive.filemanager.handlers.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import spark.Request;
import spark.Response;

/**
 * Created by Jemin.Shah on 6/21/2017.
 */
public interface SegmentedDocumentHandler
{
    String GetSegmentedDocument(Request request, Response response) throws DataAccessLayerException, DataNotFoundException;
    String SaveSegmentedDocument(Request request, Response response) throws DataAccessLayerException, DataNotFoundException;
    String deleteSegmentedDocument(Request request, Response response) throws DataAccessLayerException;
    String deleteAllSegmentedDocument(Request request, Response response)
        throws DataAccessLayerException;
}
