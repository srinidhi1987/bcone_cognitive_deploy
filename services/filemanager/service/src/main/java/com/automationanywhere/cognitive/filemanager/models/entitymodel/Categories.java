package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import com.automationanywhere.cognitive.filemanager.factories.CategoryComparatorFactory;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by msundell on 4/27/17
 */
public class Categories {
    public final static int DEFAULT_LIMIT = -1; // -1 means all data

    @JsonProperty("offset")
    private Integer offset;

    @JsonProperty("limit")
    private Integer limit;

    @JsonProperty("sort")
    private String sort;

    @JsonProperty("categories")
    private List<Category> categories;

    public Categories() {
        categories = new LinkedList<>();
        offset = 0;
        limit = DEFAULT_LIMIT;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public void add(Category category) {
        categories.add(category);
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void sortBy(String sortSpecification) {
        Comparator<Category> categoryComparator = CategoryComparatorFactory.getComparator(sortSpecification);
        categories.sort(categoryComparator);
    }

    public boolean verifySortFor(String sortSpecification) {
        return CategoryComparatorFactory.getComparator(sortSpecification) != null;
    }

    public void applyPagination(Integer requestedOffset, Integer requestedLimit) {
        if (requestedOffset == null || requestedOffset < 0) {
            requestedOffset = 0;
        }
        offset = requestedOffset;

        if (requestedLimit == null || requestedLimit < 0) {
            limit = DEFAULT_LIMIT;
            requestedLimit = categories.size();
        } else {
            limit = requestedLimit;
        }

        int endValue = (requestedOffset + requestedLimit < categories.size()) ? requestedOffset + requestedLimit : categories.size();
        if(requestedOffset >= endValue) {
            categories.clear();
            return;
        }
        categories = categories.subList(requestedOffset, endValue);
    }
}
