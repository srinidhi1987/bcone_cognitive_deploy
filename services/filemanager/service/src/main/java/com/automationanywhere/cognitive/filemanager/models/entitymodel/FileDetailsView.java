package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by pooja.wani on 24-07-2017.
 */
@Entity
@Immutable
@Table(name = "View_FileDetails")
public class FileDetailsView extends EntityModelBase{
    @Id
    @Column(name = "fileid")
    @JsonProperty("fileId")
    private String fileId;

    @Column(name = "projectid")
    @JsonProperty("projectId")
    private String projectId;

    @Column(name = "classificationid")
    @JsonProperty("classificationId")
    private String classificationId;

    @Column(name = "layoutid")
    @JsonProperty("layoutId")
    private String layoutId;

    @JsonProperty("isProduction")
    @Column(name ="isproduction")
    private boolean isProduction;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(String classificationId) {
        this.classificationId = classificationId;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }


    public void setIsProduction(boolean isProduction)
    {
        this.isProduction = isProduction;
    }
    public boolean getIsProduction()
    {
        return this.isProduction;
    }
}
