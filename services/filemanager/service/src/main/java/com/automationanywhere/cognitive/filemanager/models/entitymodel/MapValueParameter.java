package com.automationanywhere.cognitive.filemanager.models.entitymodel;

/**
 * Created by Mayur.Panchal on 12-01-2017.
 */
public class MapValueParameter {
    private Object Value;
    private String Operator;
    Object getValue(){return this.Value;}
    String getOperator() { return this.Operator;}
    public MapValueParameter(Object value,String operator)
    {
        Value= value;
        Operator = operator;
    }

}
