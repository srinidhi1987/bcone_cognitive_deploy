package com.automationanywhere.cognitive.filemanager.models.LearningInstance;

public class Summary {

	private long totalGroups;

	private long documentsUnclassified;

	private long totalFiles;

	public Summary(){

	}

	public Summary(long totalGroups, long documentsUnclassified, long totalFiles) {
		this.totalGroups = totalGroups;
		this.documentsUnclassified = documentsUnclassified;
		this.totalFiles = totalFiles;
	}

	public long getTotalGroups() {
		return totalGroups;
	}

	public void setTotalGroups(long totalGroups) {
		this.totalGroups = totalGroups;
	}

	public long getDocumentsUnclassified() {
		return documentsUnclassified;
	}

	public void setDocumentsUnclassified(long documentsUnclassified) {
		this.documentsUnclassified = documentsUnclassified;
	}

	public long getTotalFiles() {
		return totalFiles;
	}

	public void setTotalFiles(long totalFiles) {
		this.totalFiles = totalFiles;
	}
}
