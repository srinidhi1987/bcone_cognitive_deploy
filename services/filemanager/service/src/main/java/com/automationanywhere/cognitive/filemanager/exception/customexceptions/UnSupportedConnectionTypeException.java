package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class UnSupportedConnectionTypeException extends RuntimeException{
    
    private static final long serialVersionUID = -4803913834369100869L;

    public UnSupportedConnectionTypeException(String message) {
        super(message);
    }

}
