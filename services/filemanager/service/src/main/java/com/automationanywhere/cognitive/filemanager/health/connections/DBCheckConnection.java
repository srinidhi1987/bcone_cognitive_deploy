package com.automationanywhere.cognitive.filemanager.health.connections;

import java.util.HashMap;
import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DBConnectionFailureException;
/**
 * @author shweta.thakur
 *
 *         This class checks the database connection health
 */
public class DBCheckConnection implements HealthCheckConnection {
    private CommonDataAccessLayer<?, ?> commonDataAccessLayer;

    public DBCheckConnection(CommonDataAccessLayer<?, ?> commonDataAccessLayer) {
        this.commonDataAccessLayer = commonDataAccessLayer;
    }

    @Override
    public void checkConnection() {
        try {
            commonDataAccessLayer.CustomQueryExecute(new StringBuilder("select 1 from FileDetails"), new HashMap<String, Object>());
        } catch (Exception ex) {
            throw new DBConnectionFailureException("Error connecting database ", ex);
        }
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        return null;
    }
}
