package com.automationanywhere.cognitive.filemanager.service.contract;

import java.io.IOException;

public interface IOService
{
    void createFile(String filePath, byte[] bytes) throws IOException;
}
