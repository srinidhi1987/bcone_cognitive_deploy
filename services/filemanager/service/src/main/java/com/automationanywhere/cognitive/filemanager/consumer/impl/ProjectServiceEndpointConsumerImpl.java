package com.automationanywhere.cognitive.filemanager.consumer.impl;

import com.automationanywhere.cognitive.filemanager.consumer.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectDeletedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.models.StandardResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.cognitive.filemanager.constants.HeaderConstants.CORRELATION_ID;

/**
 * Created by Mayur.Panchal on 20-02-2017
 */
public class ProjectServiceEndpointConsumerImpl implements ProjectServiceEndpointConsumer {
    private String rootUrl;
    private RestTemplate restTemplate;
    @Autowired
    private RestTemplateWrapper restTemplateWrapper;
    private String HEARTBEAT_URL = "/heartbeat";
    AALogger log = AALogger.create(this.getClass());
    public ProjectServiceEndpointConsumerImpl(String rootUrl)
    {
        log.entry("Project root url : " + rootUrl);
        this.restTemplate = new RestTemplate();
        this.rootUrl = rootUrl;
        setConverterToRestTemplate();
        log.exit();
    }

    private void setConverterToRestTemplate() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(new ObjectMapper());
        StringHttpMessageConverter stringHttpMessageConerters = new StringHttpMessageConverter();
        messageConverters.add(stringHttpMessageConerters);
        messageConverters.add(jsonMessageConverter);
        restTemplate.setMessageConverters(messageConverters);
    }

    @Override
    public ProjectDetail getProjectDetails(String orgId, String prjId) throws ProjectDeletedException, ProjectNotFoundException {
        log.entry();

        if(orgId == null || "".equals(orgId)
                || prjId == null || "".equals(prjId))
            return null;
        String relativeUrl = "organizations/%s/projects/%s/metadata";
        String url = rootUrl +String.format(relativeUrl,orgId,prjId);
        ProjectDetail projectDetail = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add(CORRELATION_ID, ThreadContext.get(CORRELATION_ID));
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<StandardResponse> standardResponse = restTemplate.exchange(url, HttpMethod.GET, entity, StandardResponse.class);
            projectDetail = objectMapper.convertValue(standardResponse.getBody().getData(), ProjectDetail.class);
        }
        catch(HttpClientErrorException ex)
        {
            throwCustomException(ex);
            return null;
        }
        catch (Exception ex) {
            log.error("Exception occurred : " + ex.getMessage() );
            return null;
        }

        log.exit();
        return projectDetail;
    }
    public void setRestTeplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        setConverterToRestTemplate();
    }
    private void throwCustomException(HttpClientErrorException ex)
    {
        switch (ex.getStatusCode())
        {
            case GONE:
                throw new ProjectDeletedException("Learning Instance has been deleted.");
            case NOT_FOUND:
                throw new ProjectNotFoundException("Learning Instance not found.");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void testConnection() {
        log.entry();
        String heartBeatURL = rootUrl + HEARTBEAT_URL;
        ResponseEntity<String> standardResponse=null;
        standardResponse = (ResponseEntity<String>) restTemplateWrapper.exchangeGet(heartBeatURL, String.class);
        if(standardResponse!=null && standardResponse.getStatusCode()!=HttpStatus.OK){
            throw new DependentServiceConnectionFailureException("Error while connecting to Project service");
        }
        log.exit();
    } 

}
