package com.automationanywhere.cognitive.filemanager.exception.contract;

import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface ExceptionHandler {
    void handleDataAccessLayerException(Exception dataAccessLayerException, Request request, Response response);
    void handleDataNotFoundException(Exception dataNotFoundException, Request request, Response response);
    void handleBadRequestException(Exception badRequestException, Request request, Response response);
    void handleInvalidFileFormatException(Exception invalidFileSpecificationException, Request request, Response response);
    void handleFileSizeExceedException(Exception fileSizeExceedException, Request request, Response response);
    void handleFileNameLengthExceedException(Exception fileNameLengthExceedException,Request request,Response response);
    void handleProjectDeletedException(Exception projectNotFoundException,Request request,Response response);
    void handleProjectNotException(Exception projectNotFoundException,Request request,Response response);
    void handleException(Exception dataNotFoundException, Request request, Response response);
}
