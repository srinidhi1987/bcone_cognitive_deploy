package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import org.hibernate.annotations.Nationalized;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters.SecureStringAttributeConverter;

/**
 * Created by Jemin.Shah on 6/21/2017.
 */
@Entity
@Table(name = "SegmentedDocumentDetails")
public class SegmentedDocumentDetail extends EntityModelBase
{
    @Id
    @Column(name ="FileId")
    String fileId;

    @Nationalized
    //@Convert(converter=SecureStringAttributeConverter.class)
    @Column(name="SegmentedDocument")
    String segmentedDocument;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getSegmentedDocument() {
        return segmentedDocument;
    }

    public void setSegmentedDocument(String segmentedDocument) {
        this.segmentedDocument = segmentedDocument;
    }

    public SegmentedDocumentDetail()
    {
        super();
    }
}
