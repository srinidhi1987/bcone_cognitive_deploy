package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 04-05-2017.
 */
public class FileNameLengthExceedException extends Exception  {
    
    private static final long serialVersionUID = -6484726478906259091L;
    
    public FileNameLengthExceedException(String message) {
        super(message);
    }
}
