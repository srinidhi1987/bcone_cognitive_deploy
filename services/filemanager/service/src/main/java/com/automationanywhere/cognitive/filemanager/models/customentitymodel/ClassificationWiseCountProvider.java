package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 21-02-2017.
 */
public class ClassificationWiseCountProvider extends CustomEntityModelBase<ClassificationWiseCountEntity> {

    private String projectId;
    public ClassificationWiseCountProvider(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {
        queryString.setLength(0);
        String query ="";
        Map<String,Object> inputParam = new HashMap<String,Object>();
        query= "select projectId,classificationId, count(*) from FileDetails where projectId = :projectId group by projectId,classificationId";
        inputParam.put("projectId",this.projectId);
        queryString.append(query);
        return inputParam;
    }

    @Override
    public List<ClassificationWiseCountEntity> objectToClass(List<Object> result) {
        List<ClassificationWiseCountEntity> list = new ArrayList<>();
        for (int j = 0; j < result.size(); j++) {
            Object[] obj1 = (Object[]) result.get(j);
            ClassificationWiseCountEntity cc= new ClassificationWiseCountEntity();
            cc.setProjectId(obj1[0].toString());
            cc.setClassificationId(obj1[1].toString());
            cc.setCount((long)obj1[2]);
            list.add(cc);
        }
        return list;
    }


}
