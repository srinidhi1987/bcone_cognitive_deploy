package com.automationanywhere.cognitive.filemanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.FileDetail;
import com.automationanywhere.cognitive.filemanager.service.contract.FileBlobService;
import com.automationanywhere.cognitive.filemanager.service.contract.SegmentedDocumentManagementService;

public class DataCleanupService {
    AALogger log = AALogger.create(this.getClass());

    @Autowired
    FileBlobService fileBlobService;

    @Autowired
    SegmentedDocumentManagementService segmentedDocumentManagementService;

    public void cleanupData(FileDetail fileDetail)
    {
        log.entry();
        log.debug("[Cleaning Data] [File Id = '{fileDetail.getFileId()}']");
        deleteFileBlobData(fileDetail.getFileId());
        deleteSegmentedData(fileDetail.getFileId());
        log.exit();
    }

    private void deleteFileBlobData(String fileId)
    {
        try {
            fileBlobService.deleteFileBlobs(fileId);
        }
        catch(Exception ex)
        {
            log.error(ex.getMessage(),ex);
        }
    }

    private void deleteSegmentedData(String fileId)
    {
        try {
            segmentedDocumentManagementService.deleteSegmentedData(fileId);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage(),ex);
        }
    }
}