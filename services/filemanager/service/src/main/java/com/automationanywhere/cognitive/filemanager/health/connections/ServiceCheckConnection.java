package com.automationanywhere.cognitive.filemanager.health.connections;

import java.util.ArrayList;
import java.util.List;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.filemanager.consumer.contract.ProjectServiceEndpointConsumer;

public class ServiceCheckConnection implements HealthCheckConnection{

    ProjectServiceEndpointConsumer projectServiceEndPointConsumer;
    VisionbotAdapter visionbotAdapter;
    
    AALogger aaLogger = AALogger.create(this.getClass());
    
    public ServiceCheckConnection(ProjectServiceEndpointConsumer projectServiceEndPointConsumer, VisionbotAdapter visionbotAdapter) {
        super();
        this.projectServiceEndPointConsumer = projectServiceEndPointConsumer;
        this.visionbotAdapter = visionbotAdapter;
    }
    
    @Override
    public void checkConnection() {
        
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        aaLogger.entry();
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = null;
        for (DependentServices de : DependentServices.values()) {
            try {
                dependentService = new ServiceConnectivity(de.toString(), HTTPStatusCode.OK);
                switch (de) {
                    case Project: projectServiceEndPointConsumer.testConnection();
                        break;
                    case VisionBot: visionbotAdapter.testConnection();
                        break;
                }
            } catch (Exception ex) {
                aaLogger.error(ex);
                prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.INTERNAL_SERVER_ERROR);
            }
            if (!dependentServices.contains(dependentService)) {
                prepareDependentServiceList(dependentServices, dependentService, de, HTTPStatusCode.OK);
            }
        }
        return dependentServices;
    }
    
    private void prepareDependentServiceList(
            List<ServiceConnectivity> dependentServices,
            ServiceConnectivity dependentService, DependentServices de, HTTPStatusCode mode) {
        dependentService.setHTTPStatus(mode);
        dependentServices.add(dependentService);
    }
}
