package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

/**
 * Created by Mayur.Panchal on 15-04-2017.
 */
public class FileUploadStatus {
    String projectId;
    long totalFileCount;
    long totalProcessedCount;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public long getTotalFileCount() {
        return totalFileCount;
    }

    public void setTotalFileCount(long totalFileCount) {
        this.totalFileCount = totalFileCount;
    }

    public long getTotalProcessedCount() {
        return totalProcessedCount;
    }

    public void setTotalProcessedCount(long totalProcessedCount) {
        this.totalProcessedCount = totalProcessedCount;
    }
}
