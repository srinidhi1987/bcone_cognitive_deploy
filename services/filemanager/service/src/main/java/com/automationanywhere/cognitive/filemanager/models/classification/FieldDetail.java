package com.automationanywhere.cognitive.filemanager.models.classification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yogesh.Savalia on 27-01-2017.
 */
public class FieldDetail {
    @JsonProperty("fieldId")
    public String fieldId;
    @JsonIgnore
    public String fieldName;
    @JsonIgnore
    public String aliasName;
    @JsonProperty("foundInTotalDocs")
    public long foundInTotalDocs;
    @JsonIgnore
    public float foundinPercentageOfDocs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldDetail that = (FieldDetail) o;

        return fieldId.equals(that.fieldId);
    }

    @Override
    public int hashCode() {
        return fieldId.hashCode();
    }
}
