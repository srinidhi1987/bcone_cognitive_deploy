package com.automationanywhere.cognitive.filemanager.models.classification;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;


public class RawFileReportFromClassifier extends EntityModelBase/*<FileDetails>*/ {
    @JsonProperty("documentId")
    private String documentId;

    @JsonProperty("fieldDetail")
    private List<RawFieldDetailFromClasssifier> fieldDetail;

    private static AALogger logger = AALogger.create(RawFileReportFromClassifier.class);

    public void setDocumentId(String documentId) {
        this.documentId = documentId;

    }

    public RawFileReportFromClassifier() {
        fieldDetail = new ArrayList<RawFieldDetailFromClasssifier>();
    }


    @JsonCreator
    public static RawFileReportFromClassifier Create(String jsonString) {
        logger.entry();
        logger.debug(() -> jsonString);
        RawFileReportFromClassifier classificationReport = null;
        try {
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            classificationReport = jsonObjectMapper.readValue(jsonString, RawFileReportFromClassifier.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        logger.exit();
        return classificationReport;
    }

    public String getDocumentId() {
        return this.documentId;
    }

    public List<RawFieldDetailFromClasssifier> getClassificationFieldDetails() {
        return this.fieldDetail;
    }

    public void setClassificationFieldDetails(List<RawFieldDetailFromClasssifier> classificationFieldDetails) {
        this.fieldDetail = classificationFieldDetails;
    }

}