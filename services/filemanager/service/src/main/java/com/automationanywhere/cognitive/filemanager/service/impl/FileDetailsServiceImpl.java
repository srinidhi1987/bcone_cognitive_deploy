package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.common.formatter.CategoryFormatter;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.BadRequestException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.FileNameLengthExceedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.FileSizeExceedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.InvalidFileFormatException;
import com.automationanywhere.cognitive.filemanager.extension.DaoToDto;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueuePublisher;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.FileProcessingDetail;
import com.automationanywhere.cognitive.filemanager.models.CompareOperator;
import com.automationanywhere.cognitive.filemanager.models.Environment;
import com.automationanywhere.cognitive.filemanager.models.PatchOperation;
import com.automationanywhere.cognitive.filemanager.models.PatchRequest;
import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.automationanywhere.cognitive.filemanager.models.classification.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.EnvironmentFileCountStatistics;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileBlobProviderBasedOnFileIds;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileCountStatistics;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileDetailsProviderBasedOnFileIds;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileUploadStatus;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileUploadStatusProvider;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.LearningInstanceSummaryEntity;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.LearningInstanceSummaryProvider;
import com.automationanywhere.cognitive.filemanager.models.dto.BotRunDetails;
import com.automationanywhere.cognitive.filemanager.models.dto.VisionbotData;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Categories;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.ClassificationReport;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileCount;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetailsView;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.MapValueParameter;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.VisionBot;
import com.automationanywhere.cognitive.filemanager.models.statistics.CountStatistic;
import com.automationanywhere.cognitive.filemanager.service.contract.ClassificationReportService;
import com.automationanywhere.cognitive.filemanager.service.contract.FileDetailsService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import javax.servlet.http.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

public class FileDetailsServiceImpl implements FileDetailsService {
    AALogger log = AALogger.create(this.getClass());

    @Autowired
    private CommonDataAccessLayer commonDataAccessLayer;

    @Autowired
    @Qualifier("classificationReportService")
    private ClassificationReportService classificationReportService;

    @Autowired
    @Qualifier("visionbotAdapter")
    private VisionbotAdapter visionbotAdapter;

    @Autowired
    @Qualifier("classificationQueueService")
    private MessageQueuePublisher classificationQueueService;

    @Autowired
    @Qualifier("visionbotProcessDocumentService")
    private MessageQueuePublisher processingQueuePublisher;

    @Value("${CopyProductionFiles}")
    private boolean copyFilesFromProduction = true;

    private String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                return cd.substring(cd.indexOf('=') + 1).trim()
                        .replace("\"", "");
            }
        }
        return null;
    }

    @Override
    public Categories getCategoriesByProjectId(String organizationId, String projectId, Integer offset, Integer limit, String sortSpecification)
            throws DataAccessLayerException, DataNotFoundException, BadRequestException {
        log.entry();

        Categories categories = new Categories();

        // verify the sort specification
        if (sortSpecification == null || sortSpecification.trim().equals("")) {
            sortSpecification = "-index"; // index is default if no other field was specified
        }
        if (!categories.verifySortFor(sortSpecification)) {
            log.error("Invalid query parameter specification. Sorting can not be performed for: sort=" + sortSpecification);
            throw new BadRequestException("Invalid query parameter specification. Sorting can not be performed for: sort=" + sortSpecification);
        }
        categories.setSort(sortSpecification);

        List<CountStatistic> countStatistics = getCountStatisticsByEnvironment(organizationId, projectId);
        if (countStatistics == null || countStatistics.size() == 0) {
            log.error("FileDetails not found, they are either deleted or doesnot exists.");
            throw new DataNotFoundException("FileDetails not found, they are either deleted or doesnot exists.");
        }
        Map<String, VisionBot> visionbotDataMap = new HashMap<>();
        Map<String, int[]> visionbotSTPCountMap = new HashMap<>();
        Map<String, VisionbotData> visionbotDataUnprocessedMap = new HashMap<>();
        Map<String, int[]> stagingUnprocessedMap = new HashMap<>();
        int[] stpCountArray = new int[2];
        List<VisionbotData> visionbotDataList = visionbotAdapter.getVisionbotData(organizationId, projectId);
        visionbotDataList.forEach((item) -> {
            VisionBot visionBot = DaoToDto.toVisionBot(item);
            String categoryId = item.getCategoryId();
            visionbotDataMap.put(categoryId, visionBot);
            visionbotDataUnprocessedMap.put(categoryId, item);
        });

        for (int i = 0; i < visionbotDataList.size(); i++) {
            VisionbotData currentVisionBotData = visionbotDataList.get(i);
            stpCountArray[0] = currentVisionBotData.getStagingBotRunDetails().getPassedDocumentCount();
            stpCountArray[1] = currentVisionBotData.getProductionBotRunDetails().getPassedDocumentCount();
            int[] cloneStpCountArray = stpCountArray.clone();
            if (!visionbotSTPCountMap.containsKey(currentVisionBotData.getCategoryId())) {
                visionbotSTPCountMap.put(currentVisionBotData.getCategoryId(), cloneStpCountArray);
            }
        }

        Map<String, CategoryDetail> categoryDetailMap = new HashMap<>();
        int[] fileCounts = new int[2];
        ClassificationAnalysisReport classificationAnalysisReport = classificationReportService.getClassificationAnalysisReport(projectId);
        classificationAnalysisReport.allCategoryDetail.forEach((CategoryDetail categoryDetail) -> {
            categoryDetailMap.put(categoryDetail.id, categoryDetail);
            int[] cloneUnprocessedCountArray = new int[2];
            if(visionbotDataUnprocessedMap.get(categoryDetail.id) != null)
            {
                BotRunDetails stagingBotRunDetails = visionbotDataUnprocessedMap.get(categoryDetail.id).getStagingBotRunDetails();
                fileCounts[0] = stagingBotRunDetails.getTotalFiles();
                fileCounts[1] = stagingBotRunDetails.getProcessedDocumentCount();
                cloneUnprocessedCountArray = fileCounts.clone();
            }
            else
            {
                fileCounts[0] = fileCounts[1] = 0;
                cloneUnprocessedCountArray = fileCounts.clone();
            }
            stagingUnprocessedMap.put(categoryDetail.id, cloneUnprocessedCountArray);
        });

        countStatistics.forEach((CountStatistic countStatistic) -> {
            Category category = new Category();
            int[] fileCount = new int[2];
            String categoryId = countStatistic.getCategoryId();
            if(stagingUnprocessedMap.get(categoryId) != null)
            {
                fileCount = stagingUnprocessedMap.get(categoryId);
                fileCount[0] = (int)countStatistic.getStagingFileCount().getTotalCount();
            }
            else
            {
                fileCount[0] = (int)countStatistic.getStagingFileCount().getTotalCount();
                fileCount[1] = 0;
            }
            category.setId(categoryId);
            category.setName(CategoryFormatter.format(categoryId));

            // update CategoryDetail
            category.setCategoryDetail(categoryDetailMap.get(categoryId));

            // set VisionBot
            category.setVisionBot(visionbotDataMap.get(categoryId));

            // Set file count
            category.setFileCount((int) countStatistic.getFileCount());
            int[] stpCounts = new int[2];
            if (visionbotSTPCountMap.containsKey(category.getId())) {
                stpCounts = visionbotSTPCountMap.get(category.getId());
            }

            // add production file details
            FileCount productionFileDetails = new FileCount();
            productionFileDetails.setTotalCount((int) countStatistic.getProductionFileCount().getTotalCount());
            productionFileDetails.setUnprocessedCount((int) countStatistic.getProductionFileCount().getUnprocessedCount());
            if (visionbotSTPCountMap.containsKey(category.getId())) {
                productionFileDetails.setTotalSTPCount(stpCounts[1]);
            }
            category.setProductionFileDetails(productionFileDetails);

            // add staging file details
            FileCount stagingFileDetails = new FileCount();
            stagingFileDetails.setTotalCount((int) countStatistic.getStagingFileCount().getTotalCount());
            stagingFileDetails.setUnprocessedCount(fileCount[0] - fileCount[1]);
            if(visionbotSTPCountMap.containsKey(category.getId())) {
                stagingFileDetails.setTotalSTPCount(stpCounts[0]);
            }
            category.setStagingFileDetails(stagingFileDetails);

            categories.add(category);
        });

        // apply sort
        categories.sortBy(sortSpecification);

        // apply pagination
        categories.applyPagination(offset, limit);

        log.exit();
        return categories;
    }

    @Override
    public List<FileDetails> getProjectWiseFiles(String projectId, String requestId, boolean isProduction)throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        if(projectId == null || "".equals(projectId)) {
            return null;
        }
        FileDetails f1 = new FileDetails();
        f1.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        if (requestId != null && requestId != "") {
            f1.WhereParam.put(FileDetails.Property.uploadrequestId.toString(), new MapValueParameter(requestId, CompareOperator.equal));
        }
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(f1);

        if (fileDetailsList == null || fileDetailsList.isEmpty()) {
            log.error("filedetails not found, either it got deleted or does not exists");
            throw new DataNotFoundException("filedetails not found, either it got deleted or does not exists");
        }
        log.exit();
        return fileDetailsList;

    }

    @Override
    public List<FileDetails> getProjectWiseClassifiedFiles(String projectId, String requestId) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        if (projectId == null || "".equals(projectId)) {
            return null;
        }
        FileDetails f1 = new FileDetails();
        f1.setProjectId(projectId);
        f1.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        f1.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter("", CompareOperator.notequal));
        if (requestId != null && requestId != "") {
            f1.WhereParam.put(FileDetails.Property.uploadrequestId.toString(), new MapValueParameter(requestId, CompareOperator.equal));
        }
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(f1);
        log.exit();
        return fileDetailsList;
    }

    @Override
    public void processUnprocessedFiles(String organizationId, String projectId) throws DataAccessLayerException {
        log.entry();
        if(projectId == null || "".equals(projectId) || organizationId == null || "".equals(organizationId)) {
            return;
        }

        log.debug("projectid: " + projectId + " organizationId: " + organizationId);
        FileDetails fileDetails = new FileDetails();
        fileDetails.setProjectId(projectId);
        fileDetails.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.processed.toString(), new MapValueParameter(false, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.isProduction.toString(), new MapValueParameter(true, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter("", CompareOperator.notequal));
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(fileDetails);

        for (FileDetails fileDetail : fileDetailsList) {
            publishProcessingMessage(fileDetail, organizationId);
        }
        log.exit();
    }

    @Override
    public void processUnprocessedFiles(String organizationId, String projectId, String categoryId) throws DataAccessLayerException {
        log.entry();
        if (projectId == null || "".equals(projectId)
                || categoryId == null || "".equals(categoryId)
                || organizationId == null || "".equals(organizationId)) {
            return;
        }
        log.debug("projectid : " + projectId + " organizationId : " + organizationId);
        FileDetails fileDetails = new FileDetails();
        fileDetails.setProjectId(projectId);
        fileDetails.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.processed.toString(), new MapValueParameter(false, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter(categoryId, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.isProduction.toString(), new MapValueParameter(true, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(fileDetails);

        for (FileDetails fileDetail : fileDetailsList) {
            publishProcessingMessage(fileDetail, organizationId);
        }
        log.exit();
    }

    @Override
    public List<CountStatistic> getCountStatistics(String organizationId, String projectId) throws DataAccessLayerException {
        log.entry();
        if(organizationId == null || "".equals(organizationId)) {
            return null;
        }
        log.debug( "orgId:"+ organizationId + " projectId " + projectId);

        FileCountStatistics fileCountStatistics = new FileCountStatistics();

        if (projectId != null && !projectId.isEmpty()) fileCountStatistics.setProjectId(projectId);
        @SuppressWarnings("unchecked")
        List<CountStatistic> result = commonDataAccessLayer.ExecuteCustomEntityQuery(fileCountStatistics);
        if (result == null || result.size() == 0) {
            log.error("FileDetails not found, either they got deleted or does not exists.");
            throw new DataNotFoundException("FileDetails not found, either they got deleted or does not exists.");
        }
        log.exit();
        return result;
    }

    public List<CountStatistic> getCountStatisticsByEnvironment(String organizationId, String projectId)
            throws DataAccessLayerException {
        log.entry();
        log.debug(() -> "orgId: " + organizationId + " projectId: " + projectId);
        if(projectId == null || "".equals(projectId)
                || organizationId == null || "".equals(organizationId)) {
            return null;
        }

        EnvironmentFileCountStatistics environmentFileCountStatistics = new EnvironmentFileCountStatistics();
        environmentFileCountStatistics.setProjectId(projectId);

        @SuppressWarnings("unchecked")
        List<CountStatistic> result = commonDataAccessLayer.ExecuteCustomEntityQuery(environmentFileCountStatistics);
        log.exit();

        return result;
    }

    @Override
    public FileUploadStatus getFileUploadProgress(String projectId, String requestId, Environment environment) throws DataAccessLayerException {
        log.entry();
        log.debug(() -> "requestId:" + requestId + " projectId: " + projectId);
            if(projectId == null || "".equals(projectId)) {
                return null;
            }
        log.debug( "requestId:"+ requestId + " projectId: " + projectId);
        FileUploadStatusProvider fileUploadStatusProvider = new FileUploadStatusProvider(projectId, requestId, environment);

        @SuppressWarnings("unchecked")
        List<FileUploadStatus> result = commonDataAccessLayer.ExecuteCustomEntityQuery(fileUploadStatusProvider);
        log.exit();
        if (result != null && result.size() > 0)
            return result.get(0);
        else return null;
    }

    @Override
    public List<FileDetails> getCategoryWiseFiles(String projectId, String classificationId, String environment) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        log.debug(() -> "projectId: " + projectId + " classificationId: " + classificationId + " environment: " + environment);
        if (projectId == null || "".equals(projectId) ||
                classificationId == null || "".equals(classificationId)) {
            return null;
        }
        boolean isProduction = false;
        if (environment != null && !environment.equals(""))
            isProduction = environment.toLowerCase().equals("production") ? true : false;
        FileDetails f1 = new FileDetails();
        f1.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        f1.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter(classificationId, CompareOperator.equal));
        if (environment != null && !environment.equals(""))
            f1.WhereParam.put(FileDetails.Property.isProduction.toString(), new MapValueParameter(isProduction, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(f1);
        if (fileDetailsList == null || fileDetailsList.size() == 0) {
            log.error("FileDetails not found, either it got deleted or does not exists");
            throw new DataNotFoundException("FileDetails not found, either it got deleted or does not exists");
        }
        log.exit();
        return fileDetailsList;

    }

    @SuppressWarnings("unchecked")
    public List<FileDetails> getStagingFilesIfNotAvailableCopyFromProduction(String projectId, String classificationId)throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        log.debug(()-> "projectId: "+ projectId + " classificationId: " + classificationId);
        if(projectId == null || "".equals(projectId) ||
                classificationId == null || "".equals(classificationId)) {
            return null;
        }
        FileDetailsView fileDetailsView = new FileDetailsView();
        fileDetailsView.WhereParam.put(FileDetails.Property.projectId.toString(),new MapValueParameter(projectId,CompareOperator.equal));
        fileDetailsView.WhereParam.put(FileDetails.Property.classificationId.toString(),new MapValueParameter(classificationId,CompareOperator.equal));
        fileDetailsView.WhereParam.put(FileDetails.Property.isProduction.toString(),new MapValueParameter(false,CompareOperator.equal));

        @SuppressWarnings("unchecked")
        List<FileDetailsView> stagingFiles = commonDataAccessLayer.SelectRows(fileDetailsView);
        if(stagingFiles == null || stagingFiles.size() ==0)
        {
            if (copyFilesFromProduction) {
                fileDetailsView.WhereParam.put(FileDetails.Property.isProduction.toString(),
                    new MapValueParameter(true, CompareOperator.equal));
                List<FileDetailsView> productionFiles = commonDataAccessLayer
                    .SelectRows(fileDetailsView);

                List<FileDetails> fileDetailsList = convertFileDetailsViewToFileDetails(
                    productionFiles);
                duplicateThisFileInStaging(fileDetailsList);
                fileDetailsView.WhereParam.put(FileDetails.Property.isProduction.toString(),
                    new MapValueParameter(false, CompareOperator.equal));
                stagingFiles = commonDataAccessLayer.SelectRows(fileDetailsView);
                if (stagingFiles == null || stagingFiles.isEmpty()) {
                    throw new DataNotFoundException(
                        "filedetails not found. No staging files available");
                }
            } else {
                throw new DataNotFoundException(
                    "There are no training documents available for this Bot. Upload some training documents for the associated learning instance and try again.");
            }
        }
        List<FileDetails> fileDetailsList = convertFileDetailsViewToFileDetails(stagingFiles);
        log.exit();
        return fileDetailsList;
    }
    @Override
    public LearningInstanceSummaryEntity getLearningInstanceSummary(String organizationId, String projectId)
            throws DataNotFoundException, DataAccessLayerException {
        log.entry();

        LearningInstanceSummaryProvider learningInstanceSummaryEntityProvider = new LearningInstanceSummaryProvider();
        learningInstanceSummaryEntityProvider.setProjectId(projectId);

        @SuppressWarnings("unchecked")
        List<LearningInstanceSummaryEntity> learningInstanceSummaryEntities = commonDataAccessLayer
                .ExecuteCustomEntityQuery(learningInstanceSummaryEntityProvider);

        if (learningInstanceSummaryEntities == null || learningInstanceSummaryEntities.size() == 0) {
            throw new DataNotFoundException("Learning instance not found, their is no details exist.");
        }
        else {
            log.exit();
            return learningInstanceSummaryEntities.get(0);
        }

    }

    @Override
    public String triggerReclassification(String organizationId, String projectId) throws DataAccessLayerException, IOException, TimeoutException {
        log.entry();

        FileDetails fileDetails = new FileDetails();
        fileDetails.setProjectId(projectId);
        fileDetails.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        fileDetails.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter("", CompareOperator.equal));
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(fileDetails);

        if(fileDetailsList != null && fileDetailsList.size() > 0)
        {
            for (FileDetails fileDetail: fileDetailsList) {
                log.info("Reclassification initiated for file : " + fileDetail.getFileId());
                classificationQueueService.sendMessage(getJsonString(fileDetail));
            }

            List<String> fileIds = fileDetailsList.stream().map(FileDetails::getFileId)
                    .collect(Collectors.toList());
            log.exit();
            return getJsonStringFromObject(fileIds);
        }

        log.exit();
        return null;
    }

    private List<FileDetails> convertFileDetailsViewToFileDetails(List<FileDetailsView> viewFiles) throws DataAccessLayerException, DataNotFoundException
    {
        Collection<String> allFileId = viewFiles.stream().map(FileDetailsView::getFileId).collect(Collectors.toCollection(ArrayList::new));
        CustomEntityModelBase<FileDetails> fileDetailsProvider=new FileDetailsProviderBasedOnFileIds(allFileId);
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.ExecuteCustomEntityQuery(fileDetailsProvider);
        return  fileDetailsList;
    }

    private void duplicateThisFileInStaging(Collection<FileDetails> fileDetailsCollection) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        Collection<String> allFileId = fileDetailsCollection.stream().map(FileDetails::getFileId).collect(Collectors.toCollection(ArrayList::new));
        log.debug(()->"fileId count to duplicate : " + allFileId.size()+".");
        CustomEntityModelBase<FileBlobs> fileBlobProvider=new FileBlobProviderBasedOnFileIds(allFileId);
        @SuppressWarnings("unchecked")
        List<FileBlobs> fileBlobCollection=commonDataAccessLayer.ExecuteCustomEntityQuery(fileBlobProvider);
        List<ClassificationReport> classificationReportList = new ArrayList<>();

        for (FileDetails fileDetails:fileDetailsCollection) {
            List<ClassificationReport> documentClassificationReportList = classificationReportService.getClassificationReport(fileDetails.getFileId());

            if(!fileDetails.getIsProduction())
            {
                log.trace("File with given id "+ fileDetails.getFileId() +" is detected as staging file.");
                return;
            }
            FileBlobs fileBlobsLinkedWithGivenFile=fileBlobCollection
                    .stream()
                    .filter(x -> x.getFileId().equals(fileDetails.getFileId()))
                    .findFirst()
                    .orElse(null);

            if(fileBlobsLinkedWithGivenFile!=null){
                String uuid = UUID.randomUUID().toString();
                fileDetails.setFileId(uuid);
                fileDetails.setIsProduction(false);
                fileDetails.setIsProcessed(false);
                fileBlobsLinkedWithGivenFile.setFileId(uuid);
            }

            if(documentClassificationReportList != null)
            {
                for (ClassificationReport classificationReport: documentClassificationReportList) {
                    classificationReport.setDocumentId(fileDetails.getFileId());
                    classificationReportList.add(classificationReport);
                }
            }
        }
        
        Collection<EntityModelBase> entityModelBaseCollection=new ArrayList<EntityModelBase>();
        entityModelBaseCollection.addAll(fileDetailsCollection);
        entityModelBaseCollection.addAll(fileBlobCollection);
        entityModelBaseCollection.addAll(classificationReportList);
        @SuppressWarnings("unchecked")
        boolean result = commonDataAccessLayer.InsertBulk(entityModelBaseCollection);
        log.exit();
    }

    @Override
    public List<FileDetails> getSpecificFile(String fileId) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        log.debug(()->"fileId : " + fileId);
        if (fileId == null || "".equals(fileId)) {
            return null;
        }
        FileDetails f1 = new FileDetails();
        f1.setFileId(fileId);
        f1.WhereParam.put(FileDetails.Property.fileId.toString(), new MapValueParameter(fileId, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        List<FileDetails> fileDetailsList = commonDataAccessLayer.SelectRows(f1);
        if (fileDetailsList == null || fileDetailsList.size() == 0) {
            log.error("FileDetails not found, either it got deleted or not present on the server");
            throw new DataNotFoundException("FileDetails not found, either it got deleted or not present on the server");
        }
        log.exit();
        return fileDetailsList;
    }

    ArrayList<String> supportedFiles = new ArrayList<>(Arrays.asList("pdf", "jpg", "jpeg", "pdf", "tiff", "tif", "png"));

    private boolean isFileSupported(String fileExtention) {
        return supportedFiles.contains(fileExtention);
    }

    @Override
    public List<FileDetails> uploadFile(String uploadrequestId, Collection<Part> collectionParts, String projectId, boolean isProduction) throws FileSizeExceedException, InvalidFileFormatException, FileNameLengthExceedException {
        log.entry();
        log.debug(()-> "uploadrequestId :" + uploadrequestId + " projectId: " + projectId);
        List<FileDetails> UploadedFileList = new ArrayList<>();
        FileDetails fileDetails = null;//new FileDetails();

        for (Part part : collectionParts) {
            fileDetails = new FileDetails();
            final String fileName = getFileName(part);
            fileDetails.setFileName(fileName);
            long filesize = part.getSize();
            fileDetails.setSize(filesize);
            int i = fileDetails.getFileName().lastIndexOf('.');
            if (i > 0) {
                fileDetails.setFormat(fileDetails.getFileName().substring(i + 1));
            }
            if (!isFileSupported(fileDetails.getFormat().toLowerCase())) {
                if (collectionParts.size() == 1) {
                    log.error("Unable to upload file " + fileName + " to the Learning Instance. Unsupported format detected (" + fileDetails.getFormat().toLowerCase() + ").");
                    throw new InvalidFileFormatException("Unable to upload file " + fileName + " to the Learning Instance. Unsupported format detected (" + fileDetails.getFormat().toLowerCase() + ").");
                }
                continue;
            }
            final String fileID;
            if (filesize <= 50000000) {
                fileID = UUID.randomUUID().toString();
                log.trace("fileid:" + fileID + " is assigned to " + fileName);
            } else {
                fileID = "";
                if (collectionParts.size() == 1) {
                    log.error("Unable to upload file " + fileName + " to the Learning Instance. Size exceeds the limit allowed (50 MB).");
                    throw new FileSizeExceedException("Unable to upload file " + fileName + " to the Learning Instance. Size exceeds the limit allowed (50 MB).");
                }
                log.trace("File size more than 50MB");
            }
            if (fileName.length() > 150) {
                log.error("Unable to upload file " + fileName + " to the Learning Instance.File name length exceeds the limit allowed (150 characters).");
                throw new FileNameLengthExceedException("Unable to upload file " + fileName + " to the Learning Instance.File name length exceeds the limit allowed (150 characters).");
            }
            fileDetails.setFileId(fileID);
            fileDetails.setProjectId(projectId);
            fileDetails.setIsProcessed(false);
            fileDetails.setClassificationId("");
            if (uploadrequestId == null || uploadrequestId == "")
                fileDetails.setuploadrequestid("");
            else
                fileDetails.setuploadrequestid(uploadrequestId);
            fileDetails.setLayoutId("");
            fileDetails.setLocation("");
            fileDetails.setHeight(2200);
            fileDetails.setWidth(1700);
            fileDetails.setIsProduction(isProduction);
            if (filesize <= 50000000) {
                try (InputStream is = part.getInputStream()) {
                    FileBlobs fb = new FileBlobs();
                    fb.setFileId(fileID);
                    fb.setFileStream(is);
                    @SuppressWarnings("unchecked")
                    boolean result = commonDataAccessLayer.InsertRow(fileDetails);
                    @SuppressWarnings("unchecked")
                    boolean result1 = commonDataAccessLayer.InsertRow(fb);
                    is.close();
                    log.debug(()-> "fileid:" + fileID + " is saved");
                    classificationQueueService.sendMessage(getJsonString(fileDetails));
                } catch (Exception ex) {
                    log.error("file upload error: " + ex.getMessage());
                }
            }
            else
                log.error("Filesize is greater than 50000000.");
            UploadedFileList.add(fileDetails);
        }
        log.exit();
        return UploadedFileList;
    }

    protected String getJsonString(FileDetails fileDetails) {
        log.entry();
        try {
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            jsonObjectMapper.setVisibility(jsonObjectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                    .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
            jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            jsonObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            jsonObjectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(fileDetails);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return "";
        }
    }

    protected String getJsonStringFromObject(Object object) {
        try {
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            jsonObjectMapper.setVisibility(jsonObjectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                    .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
            jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            jsonObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            jsonObjectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return "";
        }
    }

    @Override
    public boolean deleteAllFiles(String projectId, String classificationId) throws DataAccessLayerException {
        log.entry();
        log.debug(()->"delete files for projectId:" + projectId + " and classificationId" + classificationId);
        if (projectId == null || "".equals(projectId) || classificationId == null || "".equals(classificationId))
            return false;
        FileDetails f1 = new FileDetails();
        f1.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        f1.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter(classificationId, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        boolean isSuccess = commonDataAccessLayer.DeleteRows(f1);
        log.exit();
        return isSuccess;

    }

    @Override
    public boolean deleteSpecificFile(String projectId, String classificationId, String fileid) throws DataAccessLayerException {
        log.entry();
        log.debug(()->"delete files for projectId:" + projectId + " and classificationId" + classificationId +
                " fileId:" + fileid);
        if (projectId == null || "".equals(projectId) ||
                classificationId == null || "".equals(classificationId) ||
                fileid == null || "".equals(fileid))
            return false;
        FileDetails f1 = new FileDetails();
        f1.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter(projectId, CompareOperator.equal));
        f1.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter(classificationId, CompareOperator.equal));
        f1.WhereParam.put(FileDetails.Property.fileId.toString(), new MapValueParameter(fileid, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        boolean isSuccess = commonDataAccessLayer.DeleteRows(f1);
        log.exit();
        return isSuccess;
    }

    @Override
    public boolean updateResource(String fileId, String classificationId, String layoutId) throws DataAccessLayerException {
        log.entry();
        log.debug(()->"fileId:" + fileId + " classificationid:" + classificationId + " layoutId:" + layoutId);
        if (fileId == null || "".equals(fileId) || layoutId == null || "".equals(layoutId)) {
            return false;
        }
        FileDetails fileDetail = new FileDetails();
        if (classificationId != null && classificationId != "") {
            fileDetail.SetParam.put(FileDetails.Property.classificationId.toString(), classificationId);
        }
        fileDetail.SetParam.put(FileDetails.Property.layoutId.toString(), layoutId);
        fileDetail.WhereParam.put(FileDetails.Property.fileId.toString(), new MapValueParameter(fileId, CompareOperator.equal));
        log.exit();
        @SuppressWarnings("unchecked")
        boolean isSuccess = commonDataAccessLayer.UpdateRow(fileDetail);
        return isSuccess;

    }

    @Override
    public boolean updateFileDetail(String fileId, PatchRequest[] patchRequests) throws DataAccessLayerException {
        log.entry();
        log.debug(()->"fileId :" + fileId);
        if (fileId == null || "".equals(fileId)) {
            return false;
        }
        FileDetails fileDetail = new FileDetails();

        for (PatchRequest patchRequest : patchRequests) {
            if (!patchRequest.getOp().equals(PatchOperation.replace))
                return false;

            FileDetails.Property property = FileDetails.Property.valueOf(patchRequest.getPath());

            if (!property.equals(FileDetails.Property.processed))
                return false;

            fileDetail.SetParam.put(property.toString(), Boolean.parseBoolean(patchRequest.getValue()));
            fileDetail.WhereParam.put(FileDetails.Property.fileId.toString(), new MapValueParameter(fileId, CompareOperator.equal));
            @SuppressWarnings("unchecked")
            boolean result = commonDataAccessLayer.UpdateRow(fileDetail);
        }
        log.exit();
        return true;

    }

    private void publishProcessingMessage(FileDetails fileDetails, String organizationId) {
        log.entry();
        try {
            if (!fileDetails.getIsProduction())
                return;

            FileProcessingDetail fileProcessingDetail = new FileProcessingDetail();
            fileProcessingDetail.setOrganizationId(organizationId);
            fileProcessingDetail.setProjectId(fileDetails.getProjectId());
            fileProcessingDetail.setCategoryId(fileDetails.getClassificationId());
            fileProcessingDetail.setDocumentId(fileDetails.getFileId());
            fileProcessingDetail.setLayoutId(fileDetails.getLayoutId());

            String message = getJsonStringFromObject(fileProcessingDetail);
            log.trace("Send message to visionbotProcessDocumentService queue");
            processingQueuePublisher.sendMessage(message);

        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            log.exit();
        }

    }

}