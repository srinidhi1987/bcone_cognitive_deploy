package com.automationanywhere.cognitive.filemanager.extension;

import com.automationanywhere.cognitive.filemanager.models.FileDetailsResponse;
import com.automationanywhere.cognitive.filemanager.models.dto.VisionbotData;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.VisionBot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jemin.Shah on 05-12-2016.
 */
public class DaoToDto {

    public static VisionBot toVisionBot(VisionbotData visionbotData) {
        VisionBot visionBot  = new VisionBot();
        visionBot.setId(visionbotData.getId());
        visionBot.setName(visionbotData.getName());
        visionBot.setCurrentState(visionbotData.getStatus());
        visionBot.setRunning(visionbotData.isRunning());
        visionBot.setLockedUserId(visionbotData.getLockedUserId());
        visionBot.setLastModifiedByUser(visionbotData.getLastModifiedByUser());
        visionBot.setLastModifiedTimestamp(visionbotData.getLastModifiedTimestamp());
        return visionBot;
    }

    public static FileDetailsResponse toProjectResponse(FileDetails fileDetails)
    {
        FileDetailsResponse fileDetailsResponse = new FileDetailsResponse();
        fileDetailsResponse.setFileId(fileDetails.getFileId());
        fileDetailsResponse.setProjectId(fileDetails.getProjectId());
        fileDetailsResponse.setFileName(fileDetails.getFileName());
        fileDetailsResponse.setLocation(fileDetails.getLocation());
        fileDetailsResponse.setSize(fileDetails.getSize());
        fileDetailsResponse.setHeight(fileDetails.getHeight());
        fileDetailsResponse.setWidth(fileDetails.getWidth());
        fileDetailsResponse.setFormat(fileDetails.getFormat());
        fileDetailsResponse.setIsProcessed(fileDetails.getIsProcessed() == 1 ? true   : false);

        return fileDetailsResponse;
    }

    public static List<FileDetailsResponse> toFileDetailsList(List<FileDetails> fiellist)
    {
        if(fiellist == null) return null;

        List<FileDetailsResponse> fileDetailsList = new ArrayList<>();

        for (FileDetails fileDetails : fiellist)
        {
            FileDetailsResponse fileDetailsResponse = new FileDetailsResponse();
            fileDetailsResponse.setFileId(fileDetails.getFileId());
            fileDetailsResponse.setProjectId(fileDetails.getProjectId());
            fileDetailsResponse.setFileName(fileDetails.getFileName());
            fileDetailsResponse.setLocation(fileDetails.getLocation());
            fileDetailsResponse.setSize(fileDetails.getSize());
            fileDetailsResponse.setHeight(fileDetails.getHeight());
            fileDetailsResponse.setWidth(fileDetails.getWidth());
            fileDetailsResponse.setFormat(fileDetails.getFormat());
            fileDetailsResponse.setIsProcessed(fileDetails.getIsProcessed() == 1 ? true   : false);
            fileDetailsList.add(fileDetailsResponse);
        }

        return fileDetailsList;
    }

    /*public static ProjectGetResponse toProjectGetResponse(ProjectDetail projectDetail)
    {
        ProjectGetResponse projectResponse = new ProjectGetResponse();
        projectResponse.setId(projectDetail.getId());
        projectResponse.setName(projectDetail.getName());
        projectResponse.setDescription(projectDetail.getDescription());
        projectResponse.setOrganizationId(projectDetail.getOrganizationId());
        projectResponse.setPrimaryLanguage(projectDetail.getPrimaryLanguage());
        projectResponse.setFileType(projectDetail.getProjectType());
        projectResponse.setEnvironment(projectDetail.getEnvironment());
        projectResponse.setProjectState(projectDetail.getState());
        projectResponse.setFields(toFileFieldList(projectDetail.getFieldDetailList()));
        projectResponse.setCreatedAt(projectDetail.getCreatedAt());
        projectResponse.setUpdatedAt(projectDetail.getUpdatedAt());

        return projectResponse;
    }*/
}
