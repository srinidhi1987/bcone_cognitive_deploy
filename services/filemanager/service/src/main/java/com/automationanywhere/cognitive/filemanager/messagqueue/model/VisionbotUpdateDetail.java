package com.automationanywhere.cognitive.filemanager.messagqueue.model;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class VisionbotUpdateDetail
{
    private String organizationId;
    private String projectId;
    private String categoryId;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
