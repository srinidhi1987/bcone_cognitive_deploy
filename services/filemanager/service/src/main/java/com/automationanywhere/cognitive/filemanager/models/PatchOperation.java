package com.automationanywhere.cognitive.filemanager.models;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
public enum PatchOperation
{
    add,
    remove,
    replace
}
