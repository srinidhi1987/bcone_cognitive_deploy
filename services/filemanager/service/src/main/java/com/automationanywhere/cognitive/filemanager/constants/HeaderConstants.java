/**
 * Copyright (c) 2017 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.filemanager.constants;

public class HeaderConstants {

    public static final String CORRELATION_ID = "cid";
}
