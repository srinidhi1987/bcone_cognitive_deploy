package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by msundell on 4/27/17
 */
public class Category {
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("categoryDetail")
    private CategoryDetail categoryDetail;

    @JsonProperty("visionBot")
    private VisionBot visionBot;

    @JsonProperty("fileCount")
    private int fileCount;

    @JsonProperty("lockedUserId")
    private String lockedUserId;

    @JsonProperty("lastModified")
    private String lastModified;

    @JsonProperty("files")
    private List files;

    @JsonProperty("productionFileDetails")
    private FileCount productionFileDetails;

    @JsonProperty("stagingFileDetails")
    private FileCount stagingFileDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryDetail getCategoryDetail() {
        return categoryDetail;
    }

    public void setCategoryDetail(
            CategoryDetail categoryDetail) {
        this.categoryDetail = categoryDetail;
    }

    public VisionBot getVisionBot() {
        return visionBot;
    }

    public void setVisionBot(
            VisionBot visionBot) {
        this.visionBot = visionBot;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public List getFiles() {
        return files;
    }

    public void setFiles(List files) {
        this.files = files;
    }

    public FileCount getProductionFileDetails() {
        return productionFileDetails;
    }

    public void setProductionFileDetails(
            FileCount productionFileDetails) {
        this.productionFileDetails = productionFileDetails;
    }

    public FileCount getStagingFileDetails() {
        return stagingFileDetails;
    }

    public void setStagingFileDetails(
            FileCount stagingFileDetails) {
        this.stagingFileDetails = stagingFileDetails;
    }

    public String getLockedUserId() {
        return lockedUserId;
    }

    public void setLockedUserId(String lockedUserId) {
        this.lockedUserId = lockedUserId;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
}
