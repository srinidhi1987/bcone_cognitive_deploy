package com.automationanywhere.cognitive.filemanager.service.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.BadRequestException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.FileNameLengthExceedException;
import com.automationanywhere.cognitive.filemanager.models.Environment;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.LearningInstanceSummaryEntity;
import com.automationanywhere.cognitive.filemanager.models.PatchRequest;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileUploadStatus;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Categories;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.models.statistics.CountStatistic;

import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by Mayur.Panchal on 09-12-2016.
 */
public interface FileDetailsService {
    Categories getCategoriesByProjectId(String organizationId, String projectId, Integer offset, Integer limit, String sort) throws DataAccessLayerException, DataNotFoundException, BadRequestException;
    List<FileDetails> getProjectWiseFiles(String projectId, String requestId, boolean isProduction) throws DataAccessLayerException,DataNotFoundException;
    List<FileDetails> getCategoryWiseFiles(String projectId, String classificationId , String environment) throws DataAccessLayerException, DataNotFoundException;
    List<FileDetails> getSpecificFile(String fileId) throws DataAccessLayerException, DataNotFoundException;
    List<FileDetails> uploadFile(String uploadrequestId,Collection<Part> collectionParts , String projectId, boolean isProduction) throws FileNameLengthExceedException;
    boolean deleteAllFiles(String projectId, String classificationId) throws DataAccessLayerException;
    boolean deleteSpecificFile(String projectId, String classificationId, String fileid) throws DataAccessLayerException;
    boolean updateResource(String fileId, String classificationId, String layoutId) throws DataAccessLayerException;
    boolean updateFileDetail(String fileId, PatchRequest[] patchRequests) throws DataAccessLayerException;
    List<FileDetails> getProjectWiseClassifiedFiles(String projectId, String requestId) throws DataAccessLayerException, DataNotFoundException;
    void processUnprocessedFiles(String organizationId, String projectId) throws DataAccessLayerException;
    void processUnprocessedFiles(String organizationId, String projectId, String categoryId) throws DataAccessLayerException;
    List<CountStatistic> getCountStatistics(String organizationId, String projectId) throws DataAccessLayerException;
    FileUploadStatus getFileUploadProgress(String projectId,String requestId,Environment environment) throws DataAccessLayerException;
    List<FileDetails> getStagingFilesIfNotAvailableCopyFromProduction(String projectId, String classificationId) throws DataAccessLayerException, DataNotFoundException;
    LearningInstanceSummaryEntity getLearningInstanceSummary(String organizationId, String projectId) throws DataAccessLayerException;
    String triggerReclassification(String organizationId, String projectId) throws DataAccessLayerException, IOException, TimeoutException;
}
