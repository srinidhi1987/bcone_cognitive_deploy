package com.automationanywhere.cognitive.filemanager.handlers.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.BadRequestException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import spark.Request;
import spark.Response;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface FileDetailsHandler {
    String getCategoriesByProjectId(Request req, Response res) throws DataNotFoundException, DataAccessLayerException, BadRequestException;
    String GetCategoryWiseFiles(Request request, Response response) throws DataNotFoundException,DataAccessLayerException;
    String GetProjectWiseFiles(Request request, Response response)throws DataNotFoundException,DataAccessLayerException;
    String GetSpecificFile(Request request, Response response)throws DataNotFoundException,DataAccessLayerException;
    String uploadResource(Request request, Response response) throws Exception;
    String DeleteSpecificFile(Request request, Response response)throws DataNotFoundException,DataAccessLayerException;
    String patch(Request request, Response response)throws DataNotFoundException,DataAccessLayerException;
    String getFileStatistics(Request request, Response response)throws DataNotFoundException,DataAccessLayerException;
    String getStagingFilesIfNotAvailableCopyFromProduction(Request request , Response response) throws DataNotFoundException,DataAccessLayerException;
    String getLearningInstanceSummary(Request req, Response res) throws DataNotFoundException, DataAccessLayerException, BadRequestException;
    String triggerReclassification(Request req, Response res) throws IOException, DataAccessLayerException, TimeoutException;
}
