package com.automationanywhere.cognitive.filemanager.models.statistics;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class EnvironmentCountStatistic
{
    private long unprocessedCount;
    private long totalCount;

    public EnvironmentCountStatistic() {
        this.unprocessedCount = 0;
        this.totalCount = 0;
    }

    public long getUnprocessedCount() {
        return unprocessedCount;
    }

    public void setUnprocessedCount(long unprocessedCount) {
        this.unprocessedCount = unprocessedCount;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
