package com.automationanywhere.cognitive.filemanager.factories.comparators;

public enum ComparisonOrder {
    ASCENDING(1),
    DESCENDING(-1);

    private final int direction;

    ComparisonOrder(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }
}
