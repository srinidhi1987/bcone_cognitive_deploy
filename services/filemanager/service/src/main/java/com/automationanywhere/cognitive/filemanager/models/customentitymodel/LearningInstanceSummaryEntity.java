package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.LearningInstance.Summary;

/**
 *
 * Holds summary details of learning instance based on mode(i.e staging or
 * production)
 */
public class LearningInstanceSummaryEntity {

	private Summary stagingFileSummary;

	private Summary productionFileSummary;

	public LearningInstanceSummaryEntity(){

	}

	public Summary getStagingFileSummary() {
		return stagingFileSummary;
	}

	public void setStagingFileSummary(
			Summary stagingFileSummary) {
		this.stagingFileSummary = stagingFileSummary;
	}

	public Summary getProductionFileSummary() {
		return productionFileSummary;
	}

	public void setProductionFileSummary(
			Summary productionFileSummary) {
		this.productionFileSummary = productionFileSummary;
	}
}
