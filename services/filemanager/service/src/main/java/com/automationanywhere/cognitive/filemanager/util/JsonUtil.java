package com.automationanywhere.cognitive.filemanager.util;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Jemin.Shah on 3/18/2017.
 */

@Component
public class JsonUtil
{
    static AALogger log = AALogger.create(JsonUtil.class);

    private static ObjectMapper objectMapper;

    @Autowired
    private ObjectMapper autowiredObjectMapper;

    @PostConstruct
    public void init () {
        objectMapper = this.autowiredObjectMapper;
    }

    public static  <T> T fromJsonString(String jsonString, Class<T> clazz)
    {
        log.entry();
        try
        {
            return objectMapper.readValue(jsonString, clazz);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return null;
        }
        finally
        {
            log.exit();
        }
    }

    public static String toJsonString(Object object)
    {
        log.entry();
        try
        {
            return objectMapper.writeValueAsString(object);
        }
        catch (JsonProcessingException ex)
        {
            log.error(ex.getMessage(), ex);
            return "";
        }
        finally
        {
            log.exit();
        }
    }
}
