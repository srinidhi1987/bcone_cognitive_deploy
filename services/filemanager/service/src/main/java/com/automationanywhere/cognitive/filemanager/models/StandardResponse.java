package com.automationanywhere.cognitive.filemanager.models;

/**
 * Created by msundell on 5/7/17
 */
public class StandardResponse {
  private boolean success;
  private Object data;
  private String[] errors;

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public String[] getErrors() {
    return errors;
  }

  public void setError(String[] error) {
    this.errors = error;
  }
}