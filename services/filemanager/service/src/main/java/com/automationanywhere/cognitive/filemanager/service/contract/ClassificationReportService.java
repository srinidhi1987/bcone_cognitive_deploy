package com.automationanywhere.cognitive.filemanager.service.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.automationanywhere.cognitive.filemanager.models.classification.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.filemanager.models.classification.FieldDetail;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ClassificationWiseCountEntity;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.ClassificationReport;

import java.util.List;

/**
 * Created by Yogesh.Savalia on 30-01-2017.
 */
public interface ClassificationReportService {
    boolean insertClassificationDetail(ClassificationReport classificationReportDetails) throws DataAccessLayerException;
    long getTotalFileCount(String projectId) throws DataAccessLayerException;
    List<ClassificationWiseCountEntity> getAllCategoryAndItsDocs(String projectId) throws DataAccessLayerException,DataNotFoundException;
    List<String> getAllSelectedField(String projectId);
    CategoryDetail getCategoryDetail(String projectId, String classificationId) throws DataAccessLayerException,DataNotFoundException;
    FieldDetail getFieldDetail(String projectId, String classificationId, String fieldId) throws DataAccessLayerException,DataNotFoundException;
    ClassificationAnalysisReport getClassificationAnalysisReport(String projectId) throws DataAccessLayerException,DataNotFoundException;
    ClassificationAnalysisReport calculateReportDetail(ClassificationAnalysisReport currentReport);
    boolean updateClassificationDetail(ClassificationReport classificationReport) throws DataAccessLayerException;
    List<ClassificationReport> getClassificationReport(String documentId) throws DataAccessLayerException;
}
