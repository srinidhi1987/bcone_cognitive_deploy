package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class DataAccessLayerException extends Exception {
    
    private static final long serialVersionUID = -3228216318869684441L;
    
    public DataAccessLayerException(String message) {
        super(message);
    }
    public DataAccessLayerException(String message,Throwable ex){
        super(message,ex);
    }
}
