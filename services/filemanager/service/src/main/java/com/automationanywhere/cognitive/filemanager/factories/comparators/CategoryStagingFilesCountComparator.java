package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryStagingFilesCountComparator implements Comparator<Category>, Serializable {
    
    private static final long serialVersionUID = -6674477051333865966L;
    private final ComparisonOrder order;

    public CategoryStagingFilesCountComparator(ComparisonOrder order) {
        this.order = order;
    }

    @Override
    public int compare(Category c1, Category c2) {
        // If a guarantee can be made that the category detail is always present then the defensive programming below could be removed
        if (c1.getStagingFileDetails() == null && c2.getStagingFileDetails() == null) {
            return 0;
        } else if (c1.getStagingFileDetails() != null && c2.getStagingFileDetails() == null) {
            return -1 * order.getDirection();
        } else if (c1.getStagingFileDetails() == null && c2.getStagingFileDetails() != null) {
            return 1 * order.getDirection();
        } else if (c1.getStagingFileDetails().getTotalCount() == c2.getStagingFileDetails().getTotalCount()) {
            return 0;
        }
        return ((c1.getStagingFileDetails().getTotalCount() < c2.getStagingFileDetails().getTotalCount()) ? -1 : 1) * order.getDirection();
    }
}