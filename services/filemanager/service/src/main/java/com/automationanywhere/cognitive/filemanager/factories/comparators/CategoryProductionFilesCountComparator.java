package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryProductionFilesCountComparator implements Comparator<Category>, Serializable {
    
    private static final long serialVersionUID = 718418650168189584L;
    private final ComparisonOrder order;

    public CategoryProductionFilesCountComparator(ComparisonOrder order) {
        this.order = order;
    }

    @Override
    public int compare(Category c1, Category c2) {
        // If a guarantee can be made that the category detail is always present then the defensive programming below could be removed
        if (c1.getProductionFileDetails() == null && c2.getProductionFileDetails() == null) {
            return 0;
        } else if (c1.getProductionFileDetails() != null && c2.getProductionFileDetails() == null) {
            return -1 * order.getDirection();
        } else if (c1.getProductionFileDetails() == null && c2.getProductionFileDetails() != null) {
            return 1 * order.getDirection();
        } else if (c1.getProductionFileDetails().getTotalCount() == c2.getProductionFileDetails().getTotalCount()) {
            return 0;
        }
        return ((c1.getProductionFileDetails().getTotalCount() < c2.getProductionFileDetails().getTotalCount()) ? -1 : 1) * order.getDirection();
    }
}