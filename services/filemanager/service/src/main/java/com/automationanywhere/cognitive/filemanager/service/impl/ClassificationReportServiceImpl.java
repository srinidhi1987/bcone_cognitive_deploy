package com.automationanywhere.cognitive.filemanager.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.CompareOperator;
import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.automationanywhere.cognitive.filemanager.models.classification.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.filemanager.models.classification.FieldDetail;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ClassificationWiseCountEntity;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ClassificationWiseCountProvider;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ProjectWiseCountEntity;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ProjectWiseCountProvider;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.ClassificationReport;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.MapValueParameter;
import com.automationanywhere.cognitive.filemanager.service.contract.ClassificationReportService;

/**
 * Created by Mayur.Panchal on 26-02-2017.
 */
public class ClassificationReportServiceImpl implements ClassificationReportService {
    AALogger log = AALogger.create(this.getClass());
    @Autowired
    private CommonDataAccessLayer commonDataAccessLayer;

    @Override
    public boolean insertClassificationDetail(ClassificationReport classificationReportDetails) throws DataAccessLayerException {
        if (classificationReportDetails == null) {
            return false;
        }
        @SuppressWarnings("unchecked")
        boolean success = commonDataAccessLayer.InsertRow(classificationReportDetails);
        return success;
    }

    @Override
    public long getTotalFileCount(String projectId) throws DataAccessLayerException {
        log.entry();
        if (projectId == null || "".equals(projectId)) {
            //TODO: what does 0 represents? need meaningful name
            log.error("ProjectId not found");
            return 0;
        }
        ProjectWiseCountProvider projectWiseCountProvider = new ProjectWiseCountProvider(projectId);
        @SuppressWarnings("unchecked")
        List<ProjectWiseCountEntity> result = commonDataAccessLayer.ExecuteCustomEntityQuery(projectWiseCountProvider);
        log.exit();
        //TODO: Refactor this code. Just to get count we are getting all files upto Applicatio layer.
        if (result != null && !result.isEmpty()) {
            return result.get(0).getCount();
        } else {
            return 0;
        }
    }

	public List<ClassificationWiseCountEntity> getAllCategoryDetail(String projectId) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        List<ClassificationWiseCountEntity> classificationWiseCountEntities = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        List<CategoryDetail> categoryDetails = new ArrayList<>();
        Map<String, CategoryDetail> categoryDetailsMap = new HashMap<>();

        String queryString;
        Map<String, Object> inputParam = new HashMap<>();
        inputParam.put("projid", projectId);
        inputParam.put("classificationid", "");
        //inputParam.put("isfound",Boolean.parseBoolean("1"));

        queryString = "SELECT fd.classificationId, count(fd.fileId) as TotalFilesFoundWithFieldId, cr.fieldId,\n" +
                "CASE WHEN cr.isFound = '1' THEN COUNT(cr.isFound) ELSE 0 END AS isFoundCount\n" +
                "FROM FileDetails fd INNER JOIN ClassificationReport cr on cr.documentId = fd.fileId\n" +
                "WHERE fd.projectId = :projid AND fd.classificationId <> :classificationid \n" +
                "GROUP BY fd.classificationId,cr.fieldId, cr.isFound";

        sb.append(queryString);
        @SuppressWarnings("unchecked")
        List<Object> result = commonDataAccessLayer.CustomQueryExecute(sb, inputParam);

        log.trace("Preparing category details ");
        Map<String, String> categoryWithFieldId = new HashMap<>();
        //TODO: Refactor many for loops.
        for (int i = 0; i < result.size(); i++) {
            // Fill List of categories.
            Object[] obj1 = (Object[]) result.get(i);
            CategoryDetail currentCategory = new CategoryDetail();
            currentCategory.id = (obj1[0].toString());

            if (!categoryDetailsMap.containsKey(currentCategory.id)) {
                // create a new CategoryDetail object
                currentCategory.noOfDocuments = Long.parseLong(obj1[1].toString());
                FieldDetail fieldDetail = new FieldDetail();
                fieldDetail.fieldId = obj1[2].toString();
                currentCategory.allFieldDetail = new ArrayList<>();
                if (!currentCategory.allFieldDetail.contains(fieldDetail)) {
                    fieldDetail.foundInTotalDocs = Long.parseLong(obj1[3].toString());
                    currentCategory.allFieldDetail.add(fieldDetail);
                    categoryDetails.add(currentCategory);
                    categoryDetailsMap.put(currentCategory.id, currentCategory);
                    categoryWithFieldId.put(currentCategory.id, fieldDetail.fieldId);
                }
            } else {
                // use an existing CategoryDetail object
                CategoryDetail alreadyExistingCategory = categoryDetailsMap.get(currentCategory.id);
                //TODO: Verify this issue.
                //alreadyExistingCategory.noOfDocuments += Long.parseLong(obj1[1].toString());
                FieldDetail fieldDetail = new FieldDetail();
                fieldDetail.fieldId = obj1[2].toString();

                if (!alreadyExistingCategory.allFieldDetail.contains(fieldDetail)) {
                    fieldDetail.foundInTotalDocs = Long.parseLong(obj1[3].toString());
                    alreadyExistingCategory.allFieldDetail.add(fieldDetail);
                } else {
                    int indexOfExistingFieldDetail = alreadyExistingCategory.allFieldDetail.indexOf((fieldDetail));
                    FieldDetail alreadyExistingFieldDetail = alreadyExistingCategory.allFieldDetail.get(indexOfExistingFieldDetail);
                    alreadyExistingFieldDetail.foundInTotalDocs += Long.parseLong(obj1[3].toString());
                    alreadyExistingCategory.allFieldDetail.set(indexOfExistingFieldDetail, alreadyExistingFieldDetail);
                    //if field details exists then get the count of documents and add to no of documents for the category
                    if (alreadyExistingFieldDetail.fieldId.equals(categoryWithFieldId.get(alreadyExistingCategory.id)))
                        alreadyExistingCategory.noOfDocuments += Long.parseLong(obj1[1].toString());
                }
            }
        }
        log.trace("Done Preparing category details...Preparing ClassificationWiseCountEntity.");
        for (CategoryDetail category : categoryDetails) {
            ClassificationWiseCountEntity entity = new ClassificationWiseCountEntity();
            entity.setProjectId(projectId);
            entity.setClassificationId(category.id);
            entity.setCount(category.noOfDocuments);
            entity.setCategoryDetail(category);
            classificationWiseCountEntities.add(entity);
        }

        log.trace("Done Preparing ClassificationWiseCountEntity");
        log.exit();
        return classificationWiseCountEntities;
    }

    @Override
    public CategoryDetail getCategoryDetail(String projectId, String classificationId) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        CategoryDetail currentcategory = new CategoryDetail();
        currentcategory.allFieldDetail = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        String queryString = "select distinct fieldId from ClassificationReport where documentId in (select fileId from FileDetails where projectId = :projid and classificationId != '')";
        Map<String, Object> inputParam = new HashMap<String, Object>();
        inputParam.put("projid", projectId);
        sb.append(queryString);
        @SuppressWarnings("unchecked")
        List<String> result = commonDataAccessLayer.CustomQueryExecute(sb, inputParam);
        log.trace("Preparing FieldDetail Object..");
        for (int i = 0; i < result.size(); i++) {
            FieldDetail fieldDetail = getFieldDetail(projectId, classificationId, result.get(i));
            currentcategory.allFieldDetail.add(fieldDetail);
        }
        log.trace("Done Preparing FieldDetail ..");
        if (result.size() == 0) {
            log.error(" No fields found.");
            throw new DataNotFoundException("No fields found.");
        }
        log.exit();
        return currentcategory;
    }

    @Override
    public FieldDetail getFieldDetail(String projectId, String classificationId, String fieldId) throws DataAccessLayerException, DataNotFoundException {
        if( projectId == null || "".equals(projectId) ||
                classificationId == null || "".equals(classificationId)
                || fieldId == null || "".equals(fieldId)) {
            return null;
        }
        log.debug( "ProjectId:"+ projectId + " ClassificationId:" + classificationId + " FileId:" + fieldId );
        FieldDetail currentFieldDetail = new FieldDetail();
        StringBuilder sb = new StringBuilder();

        String queryString = "select count(*) from FileDetails where projectId = :projid and classificationId = :classid and fileId in(select documentId from ClassificationReport where fieldId = :fieldid and isFound= 'True')";
        Map<String, Object> inputParam = new HashMap<String, Object>();
        inputParam.put("projid", projectId);
        inputParam.put("fieldid", fieldId);
        inputParam.put("classid", classificationId);
        sb.append(queryString);
        @SuppressWarnings("unchecked")
        List<Object> result = commonDataAccessLayer.CustomQueryExecute(sb, inputParam);
        currentFieldDetail.fieldId = fieldId;

        if (result != null)
            currentFieldDetail.foundInTotalDocs = (long) result.get(0);
        else {
            log.error("Data is not available");
            throw new DataNotFoundException("Data is not available");
        }
        log.exit();
        return currentFieldDetail;

    }

    @Override
    public List<ClassificationWiseCountEntity> getAllCategoryAndItsDocs(String projectId) throws DataAccessLayerException, DataNotFoundException {
        if(projectId == null || "".equals(projectId))
            return null;
        log.debug("projectId : " + projectId);
        ClassificationWiseCountProvider classificationWiseCountProvider = new ClassificationWiseCountProvider(projectId);
        @SuppressWarnings("unchecked")
        List<ClassificationWiseCountEntity> result = commonDataAccessLayer.ExecuteCustomEntityQuery(classificationWiseCountProvider);
        if (result == null || result.isEmpty()) {
            log.error("Data is not available.");
            throw new DataNotFoundException("Data is not available.");
        }
        log.exit();
        return result;
    }

    @Override
    public List<String> getAllSelectedField(String projectId) {
        //TODO: Why null?
        return null;
    }

    @Override
    public ClassificationAnalysisReport getClassificationAnalysisReport(String projectid) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        ClassificationAnalysisReport classificationAnalysisReport = new ClassificationAnalysisReport();
        classificationAnalysisReport.projectId = projectid;
        classificationAnalysisReport.totalDocuments = getTotalFileCount(projectid);
        classificationAnalysisReport.allCategoryDetail = new ArrayList<>();

        List<ClassificationWiseCountEntity> allCategory = getAllCategoryDetail(projectid);

        for (int i = 0; i < allCategory.size(); i++) {
            CategoryDetail category = allCategory.get(i).getCategoryDetail();
            classificationAnalysisReport.allCategoryDetail.add(category);
        }
        classificationAnalysisReport = calculateReportDetail(classificationAnalysisReport);
        log.exit();
        return classificationAnalysisReport;

    }

    @Override
    public ClassificationAnalysisReport calculateReportDetail(ClassificationAnalysisReport currentReport) {
        log.entry();
        for (int catIndex = 0; catIndex < currentReport.allCategoryDetail.size(); catIndex++) {
            CategoryDetail currCategoryDetail = currentReport.allCategoryDetail.get(catIndex);
            float PercentageOfTotalDocuments = (float) (currCategoryDetail.noOfDocuments) / currentReport.totalDocuments;
            float AllFieldsAvg = 0.0f;
            for (int fieldIndex = 0; fieldIndex < currCategoryDetail.allFieldDetail.size(); fieldIndex++) {
                FieldDetail currFieldDetail = currCategoryDetail.allFieldDetail.get(fieldIndex);
                float FoundInPercentageOfDocs = (currFieldDetail.foundInTotalDocs * 100.0f) / (currCategoryDetail.noOfDocuments);
                AllFieldsAvg += FoundInPercentageOfDocs;
                currCategoryDetail.allFieldDetail.set(fieldIndex, currFieldDetail);
            }
            AllFieldsAvg = AllFieldsAvg / (currCategoryDetail.allFieldDetail.size() * 100.0f);
            currCategoryDetail.index = AllFieldsAvg * PercentageOfTotalDocuments * 2.0f;
            currCategoryDetail.priority = (int) Math.ceil(currCategoryDetail.index * 10.0f);
            currentReport.allCategoryDetail.set(catIndex, currCategoryDetail);
        }
        log.exit();
        return currentReport;
    }

    @Override
    public boolean updateClassificationDetail(ClassificationReport classificationReport) throws DataAccessLayerException {
        if (classificationReport == null)
            return false;
        @SuppressWarnings("unchecked")
        boolean success =commonDataAccessLayer.UpdateRow(classificationReport); 
        return success;
    }

    @Override
    public List<ClassificationReport> getClassificationReport(String documentId) throws DataAccessLayerException {
        ClassificationReport classificationReport = new ClassificationReport();
        classificationReport.WhereParam.put("documentId", new MapValueParameter(documentId, CompareOperator.equal));

        @SuppressWarnings("unchecked")
        List<ClassificationReport> lstOfClassificationReport = commonDataAccessLayer.SelectRows(classificationReport);
        return lstOfClassificationReport;
    }
}
