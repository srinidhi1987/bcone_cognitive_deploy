package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 21-02-2017.
 */
public class ProjectWiseCountProvider extends CustomEntityModelBase<ProjectWiseCountEntity> {
    private String projectId;

    public ProjectWiseCountProvider(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {
        queryString.setLength(0);
        String query = "";
        Map<String,Object> inputParam = new HashMap<>();
        inputParam.put("prjid",this.projectId);

        query= "SELECT projectId, COUNT(*) AS count FROM FileDetails WHERE projectId = :prjid AND classificationId != '' GROUP BY projectId";

        queryString.append(query);
        return inputParam;
    }

    @Override
    public List<ProjectWiseCountEntity> objectToClass(List<Object> result) {
        List<ProjectWiseCountEntity> list = new ArrayList<>();
        for (int j = 0; j < result.size(); j++) {
            Object[] obj1 = (Object[]) result.get(j);
            ProjectWiseCountEntity entity = new ProjectWiseCountEntity();
            entity.setProjectId(obj1[0].toString());
            entity.setCount((long)obj1[1]);
            list.add(entity);
        }
        return list;
    }
}
