package com.automationanywhere.cognitive.filemanager.messagqueue.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.MQConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.MessageQueueConnectionCloseException;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueuePublisher;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;

public class MessageQueuePublisherImpl implements MessageQueuePublisher
{
    AALogger log = AALogger.create(this.getClass());

    private ConnectionFactory connectionFactory;
    private String queueName;
    private String host;
    private String username;
    private String password;
    private String virtualHost;

  public MessageQueuePublisherImpl(String queue, String host, String username, String password,
      String virtualHost, Integer port) {
        this.queueName = queue;
        this.host = host;
        this.username = username;
        this.password = password;
        this.virtualHost = virtualHost;
        createConnectionFactory(host, username, password, virtualHost, port);
    }

  public void createConnectionFactory(String host, String username, String password,
      String virtualHost, int port) {
      connectionFactory = new ConnectionFactory();
      connectionFactory.setHost(host);
      connectionFactory.setUsername(username);
      connectionFactory.setPassword(password);
      connectionFactory.setVirtualHost(virtualHost);
      connectionFactory.setAutomaticRecoveryEnabled(true);
      connectionFactory.setPort(port);
    }

  public Connection connectAndRetry() {
        Connection connection=null;
        try
        {
          boolean connectionEstablished = false;
          do {
            try {
              connection = connectionFactory.newConnection();
              connectionEstablished = true;
              log.debug("Debug: Connection successful.");
            }
            catch (ConnectException ex)
            {
              log.error("Error: Connection failed. Retrying after 1 second...",ex);
              Thread.sleep(1000);
            }

          } while (!connectionEstablished);

        }
        catch (InterruptedException | TimeoutException | IOException ex )
        {
            throw new MQConnectionFailureException("Error while creating connection to Message queue", ex);
        }
        return connection;
    }

    private Channel getNewChannel(Connection connection)  {
        Channel channel=null;
        try {
            channel = connection.createChannel();
            channel.queueDeclare(queueName, true, false, false, null);
        } catch (IOException e) {
            throw new MQConnectionFailureException("Error while createing message queue channel ", e);
        }
        return channel;
    }

    @Override
    public void testConnection() {
        Connection connection=null;
        try {
            connection = connectionFactory.newConnection();
        } catch (IOException | TimeoutException e) {
            throw new MQConnectionFailureException("Error while creating connection to Message queue", e);
        } finally {
               closeConnection(connection);
        }
    }

    @Override
    public void sendMessage(String message) throws IOException, TimeoutException,MQConnectionFailureException{
        Connection connection=null;
        Channel channel=null;
        try
        {
            connection= connectAndRetry();
            channel= getNewChannel(connection);
            String corrId = java.util.UUID.randomUUID().toString();
            AMQP.BasicProperties props = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(corrId)
                    .deliveryMode(2)
                    .build();
            channel.basicPublish("", queueName, props, message.getBytes());
        }catch(IOException ex){
          throw ex;
        }
        finally{
           closeChannel(channel);
           closeConnection(connection);
        }
    }

  @Override
  public void closeChannel(Channel channel)  {
     try
     {
      if(channel!=null&&channel.isOpen()){
        channel.close();
      }
    } catch (IOException | TimeoutException e) {

      throw new MQConnectionFailureException("Error while closing message queue channel ", e);
    }
  }

    @Override
    public void closeConnection(Connection connection){
      try{
        if(connection!=null&&connection.isOpen()) {
          connection.close();
        }
      } catch (IOException e) {
        throw new MessageQueueConnectionCloseException("Error while closing connection ", e);
      }
    }
}
