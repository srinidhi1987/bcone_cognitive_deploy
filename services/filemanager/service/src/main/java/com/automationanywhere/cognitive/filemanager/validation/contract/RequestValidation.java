package com.automationanywhere.cognitive.filemanager.validation.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectDeletedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface RequestValidation {
    ProjectDetail validateProjectId(String orgId, String projectId) throws ProjectNotFoundException, ProjectDeletedException;
    boolean validateRequest(Request request, Response response);
}
