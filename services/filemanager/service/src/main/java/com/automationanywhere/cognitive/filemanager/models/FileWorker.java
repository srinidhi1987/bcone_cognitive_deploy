package com.automationanywhere.cognitive.filemanager.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by keval.sheth on 30-11-2016.
 */
public class FileWorker {

   // private String id;
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("currentProcessingObject")
    private Object currentProcessingObject;
    @JsonProperty("totalFileCount")
    private long totalFileCount;
    @JsonProperty("totalProcessedCount")
    private long totalProcessedCount;
    @JsonProperty("estimatedTimeRemainingInMins")
    private long estimatedTimeRemainingInMins;
    @JsonProperty("complete")
    private boolean complete;

   /* public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }*/

    public String getProjectId() { return this.projectId; }
    public void setProjectId(String projectId) { this.projectId = projectId; }

    public Object getCurrentProcessingObject() { return this.currentProcessingObject;}
    public void setCurrentProcessingObject(Object currentProcessingObject) { this.currentProcessingObject = currentProcessingObject; }

    public void setTotalFileCount(long totalFileCount)
    {
        this.totalFileCount = totalFileCount;
    }
    public long getTotalFileCount()
    {
        return this.totalFileCount;
    }

    public long getTotalProcessedCount() { return this.totalProcessedCount;}
    public void setTotalProcessedCount(long totalProcessedCount) { this.totalProcessedCount = totalProcessedCount; }

    public void setEstimatedTimeRemainingInMins(long estimatedTimeRemainingInMins)
    {
        this.estimatedTimeRemainingInMins = estimatedTimeRemainingInMins;
    }
    public long getEstimatedTimeRemainingInMins()
    {
        return this.estimatedTimeRemainingInMins;
    }

    public void setStatus(boolean complete)
    {
        this.complete = complete;
    }
    public boolean getStatus()
    {
        return this.complete;
    }


}
