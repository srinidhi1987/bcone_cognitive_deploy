package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.statistics.CountStatistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jemin.Shah on 4/5/2017.
 */
public class FileCountStatistics extends CustomEntityModelBase<CountStatistic>
{
    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {
        queryString.setLength(0);
        String query = "";
        Map<String,Object> inputParam = new HashMap<String,Object>();


        String whereClause = "";
        if (projectId != null && !projectId.isEmpty())
        {
            whereClause = " where projectId = :prjid ";
            inputParam.put("prjid",this.getProjectId());
        }

        // Todo: the sum is performed on the same field twice. Unnecessary SQL DB work. Is this intentional?
        query= "select projectId, classificationId, isProduction, count(*) as fileCount, " +
                "sum(cast(processed as int)) as processedCount, " +
                "sum(cast(processed as int)) as productionProcessed " +
                "from FileDetails " +
                whereClause +
                "group by projectId, classificationId, isProduction";
        queryString.append(query);
        return inputParam;
    }

    @Override
    public List<CountStatistic> objectToClass(List<Object> result) {
        List<CountStatistic> countStatisticList = new ArrayList<>();

        for (int j = 0; j < result.size(); j++) {
            Object[] objectArray = (Object[]) result.get(j);
            CountStatistic countStatistic = getCountStatistic(objectArray[0].toString(), objectArray[1].toString(), countStatisticList);

            if((boolean)objectArray[2]) {
                countStatistic.getProductionFileCount().setTotalCount((long)objectArray[3]);
                countStatistic.getProductionFileCount().setUnprocessedCount((long)objectArray[3] - (long)objectArray[4]);
            }
            else {
                countStatistic.getStagingFileCount().setTotalCount((long)objectArray[3]);
                countStatistic.getStagingFileCount().setUnprocessedCount((long)objectArray[3] - (long)objectArray[4]);
            }
        }
        return countStatisticList;
    }

    private CountStatistic getCountStatistic(String projectId, String categoryId, List<CountStatistic> countStatisticList)
    {
        CountStatistic countStatistic = countStatisticList
                .stream()
                .filter(p -> p.getCategoryId().equals(categoryId)
                        && p.getProjectId().equals(projectId))
                .findFirst()
                .orElse(null);

        if(countStatistic != null)
        {
            return countStatistic;
        }

        countStatistic = new CountStatistic();
        countStatistic.setProjectId(projectId);
        countStatistic.setCategoryId(categoryId);
        countStatisticList.add(countStatistic);
        return countStatistic;
    }
}
