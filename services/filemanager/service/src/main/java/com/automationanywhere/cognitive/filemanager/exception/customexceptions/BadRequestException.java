package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class BadRequestException extends Exception {
    
    private static final long serialVersionUID = 4002601371222301453L;
    
    public BadRequestException(String message) {
        super(message);
    }
}
