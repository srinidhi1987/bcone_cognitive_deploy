package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.Environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mayur.Panchal on 15-04-2017.
 */
public class FileUploadStatusProvider extends CustomEntityModelBase<FileUploadStatus> {
    String projectId;
    String requestId;
    Environment environment;
    public FileUploadStatusProvider(String projectId,String requestId,Environment projectEnvironment) {
        this.projectId = projectId;
        this.requestId = requestId;
        this.environment = projectEnvironment;
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {
        queryString.setLength(0);
        String query ="";
        Map<String,Object> inputParam = new HashMap<String,Object>();
        if(this.requestId != null && !"".equals(this.requestId)) {
            query = "select projectId, \n" +
                    "count(fileId) as totalFileCount,\n" +
                    "(select count(fileId) from FileDetails where classificationId != ''\n" +
                    " and projectId=fd.projectId \n" +
                    " and  uploadrequestId = fd.uploadrequestId) \n" +
                    "as totalProcessedCount \n" +
                    " from FileDetails fd \n" +
                    " where projectId=:projectId and \n" +
                    " uploadrequestId = :requestId \n" +
                    " group by projectId,uploadrequestId";
        }
        else
        {
            int isProduction = this.environment.equals(Environment.staging) ? 0 : 1;
            query="select projectId, \n" +
                    "count(fileId) as totalFileCount,\n" +
                    "(select count(fileId) from FileDetails where classificationId != ''\n" +
                    " and projectId=fd.projectId  and isProduction = "+isProduction+") \n" +
                    "as totalProcessedCount \n" +
                    " from FileDetails fd \n" +
                    " where projectId=:projectId and isProduction = "+isProduction+" \n" +
                    " group by projectId";
        }
        inputParam.put("projectId",this.projectId);
        if(this.requestId != null && !"".equals(this.requestId))
            inputParam.put("requestId",this.requestId);
        queryString.append(query);
        return inputParam;
    }

    @Override
    public List<FileUploadStatus> objectToClass(List<Object> result) {
        List<FileUploadStatus> list = new ArrayList<>();
        for (int j = 0; j < result.size(); j++) {
            Object[] obj1 = (Object[]) result.get(j);
            FileUploadStatus cc= new FileUploadStatus();
            cc.setProjectId(obj1[0].toString());
            cc.setTotalFileCount((long)obj1[1]);
            cc.setTotalProcessedCount((long)obj1[2]);
            list.add(cc);
        }
        return list;
    }
}
