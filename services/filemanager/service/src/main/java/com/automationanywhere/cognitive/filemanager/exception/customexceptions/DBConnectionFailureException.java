package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class DBConnectionFailureException extends RuntimeException {

	private static final long serialVersionUID = 5469681982119951831L;

	public DBConnectionFailureException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
