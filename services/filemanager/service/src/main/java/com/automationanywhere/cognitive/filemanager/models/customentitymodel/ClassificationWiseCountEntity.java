package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mayur.Panchal on 06-04-2017.
 */
public class ClassificationWiseCountEntity {
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("classificationId")
    private String classificationId;
    @JsonProperty("count")
    private long count;
    @JsonIgnore
    private CategoryDetail categoryDetail;
    public void setProjectId(String projectId) { this.projectId = projectId;}
    public String getProjectId() { return this.projectId;}
    public void setClassificationId(String classificationId) { this.classificationId = classificationId;}
    public String getClassificationId() { return this.classificationId;}
    public void setCount(long count){ this.count = count;}
    public long getCount() { return this.count; }
    public void setCategoryDetail(CategoryDetail categoryDetail) {this.categoryDetail = categoryDetail;}
    public CategoryDetail getCategoryDetail() {return this.categoryDetail;}
}
