package com.automationanywhere.cognitive.filemanager.messagqueue.model;

/**
 * Created by Mayur.Panchal on 15-02-2017.
 */
public class CustomMessage {
    private String topic;
    private Object data;
    private String source;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String success) {
        this.topic = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String errors) {
        this.source = errors;
    }
}
