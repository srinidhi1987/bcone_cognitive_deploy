package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.service.contract.IOService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class IOServiceImpl implements IOService
{
    private AALogger log = AALogger.create(this.getClass());

    @Override
    public void createFile(String filePath, byte[] bytes) throws IOException {

        log.debug("Creating File : " + filePath);
        log.debug("File size : " + bytes.length);

        File exportedFile = new File(filePath);
        File parentFolder = exportedFile.getParentFile();
        if (!parentFolder.exists()) {
            boolean success = parentFolder.mkdirs();
            if (!success) {
                log.error("Error to create parent folder of file: {}", parentFolder.getAbsolutePath());
            }
        }

        if (!exportedFile.exists()) {
            log.debug("File not found {}. Creating new file.", filePath);
            boolean success = exportedFile.createNewFile();
            if (!success) {
                log.error("Error to create new file: {}", exportedFile.getAbsolutePath());
            }
        }

        try (OutputStream outputStream = new FileOutputStream(exportedFile)) {
            outputStream.write(bytes);
            outputStream.flush();
            log.debug("File created : " + filePath);
        }
        catch(IOException ex)
        {
            log.error(ex.getMessage(), ex);
        }
    }
}
