package com.automationanywhere.cognitive.filemanager.models.FileProcessing;

/**
 * Created by Jemin.Shah on 8/2/2017.
 */
public class ExportDetail {
    String fileId;
    String projectId;
    String organizationId;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

}
