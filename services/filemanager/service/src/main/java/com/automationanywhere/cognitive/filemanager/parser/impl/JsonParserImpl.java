package com.automationanywhere.cognitive.filemanager.parser.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.parser.contract.JsonParser;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class JsonParserImpl implements JsonParser
{
    AALogger log = AALogger.create(getClass());

    protected ObjectMapper jsonObjectMapper = new ObjectMapper();
    @Override
    public String getResponse(CustomResponse customResponse)
    {
        log.entry();
        try
        {
            jsonObjectMapper.setVisibility(jsonObjectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                    .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
            jsonObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            jsonObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            jsonObjectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(customResponse);
        }
        catch (JsonProcessingException e)
        {
            log.error(e.getMessage());
            return "";
        }
        finally
        {
            log.exit();
        }
    }
}
