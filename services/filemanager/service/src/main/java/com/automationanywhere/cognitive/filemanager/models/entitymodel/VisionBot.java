package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by msundell on 4/27/17
 */
public class VisionBot {
  @JsonProperty("id")
  private String id;

  @JsonProperty("name")
  private String name;

  @JsonProperty("currentState")
  private String currentState;

  @JsonProperty("running")
  private boolean running;

  @JsonProperty("lockedUserId")
  private String lockedUserId;

  @JsonProperty("lastModifiedByUser")
  private String lastModifiedByUser;

  @JsonProperty("lastModifiedTimestamp")
  private String lastModifiedTimestamp;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCurrentState() {
    return currentState;
  }

  public void setCurrentState(String currentState) {
    this.currentState = currentState;
  }

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  public String getLockedUserId() {
    return lockedUserId;
  }

  public void setLockedUserId(String lockedUserId) {
    this.lockedUserId = lockedUserId;
  }

  public String getLastModifiedByUser() {
    return lastModifiedByUser;
  }

  public void setLastModifiedByUser(String lastModifiedByUser) {
    this.lastModifiedByUser = lastModifiedByUser;
  }

  public String getLastModifiedTimestamp() {
    return lastModifiedTimestamp;
  }

  public void setLastModifiedTimestamp(String lastModifiedTimestamp) {
    this.lastModifiedTimestamp = lastModifiedTimestamp;
  }
}
