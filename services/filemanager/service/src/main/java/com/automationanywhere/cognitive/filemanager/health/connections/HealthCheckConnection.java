package com.automationanywhere.cognitive.filemanager.health.connections;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;

import java.util.List;;

public interface HealthCheckConnection {
	void checkConnection();
	List<ServiceConnectivity> checkConnectionForService() ;
}
