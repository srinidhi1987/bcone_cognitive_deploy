package com.automationanywhere.cognitive.filemanager.parser.contract;

import com.automationanywhere.cognitive.filemanager.models.CustomResponse;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public interface JsonParser {
    String getResponse(CustomResponse customResponse);
}
