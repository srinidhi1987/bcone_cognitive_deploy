package com.automationanywhere.cognitive.filemanager.messagqueue.impl;

import static com.automationanywhere.cognitive.filemanager.util.JsonUtil.fromJsonString;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueueSubscriber;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.FileDetail;
import com.automationanywhere.cognitive.filemanager.service.contract.SegmentedDocumentManagementService;
import com.automationanywhere.cognitive.filemanager.service.impl.DataCleanupService;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.net.ConnectException;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidatorQueueSubscriberImpl implements MessageQueueSubscriber {
    AALogger log = AALogger.create(this.getClass());

    ConnectionFactory factory;
    Connection connection;
    Channel channel;
    String queueName;

    @Autowired
    private DataCleanupService dataCleanupService;

    @Autowired
    private SegmentedDocumentManagementService segmentedDocumentManagementService;

    public ValidatorQueueSubscriberImpl(String queue, String host, String username, String password,
        String virtualHost, Integer port) {
        try
        {
            queueName = queue;
            factory = new ConnectionFactory();
            factory.setHost(host);
            factory.setUsername(username);
            factory.setPassword(password);
            factory.setVirtualHost(virtualHost);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setPort(port);

            boolean connectionSuccess = false;
            while(!connectionSuccess) {
                try {
                    connection = factory.newConnection();
                    connectionSuccess = true;
                    log.debug("Debug: Connection successful.");
                }
                catch (ConnectException ex)
                {
                    log.error(ex.getMessage());
                    log.debug("Debug: Connection failed. Retrying after 1 second...");
                    Thread.sleep(1000);
                }
            }
            channel = connection.createChannel();
            channel.queueDeclare(queueName, true, false, false, null);
            channel.basicQos(1, false);

            final Consumer consumer = new DefaultConsumer(channel)
            {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");

                    log.debug("Received message : '" + message + "'");
                    try
                    {
                        receiveMessage(message);
                    }
                    catch (Exception ex)
                    {
                        log.error(ex.getMessage());
                    }
                    finally{
                        channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                }
            };

            boolean autoAck = false;
            channel.basicConsume(queueName, autoAck, consumer);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return;
        }
    }

    @Override
    public void receiveMessage(String message)
    {
        log.entry();
        log.debug("Message Recieved : " + message);
        FileDetail fileDetail = fromJsonString(message, FileDetail.class);
        if(fileDetail == null)
            return;
       try
        {
            dataCleanupService.cleanupData(fileDetail);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
        }
        log.exit();
    }
}
