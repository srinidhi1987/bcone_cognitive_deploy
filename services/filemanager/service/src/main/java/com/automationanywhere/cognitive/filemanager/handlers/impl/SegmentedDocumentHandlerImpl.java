package com.automationanywhere.cognitive.filemanager.handlers.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.handlers.contract.AbstractHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.SegmentedDocumentHandler;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.service.contract.SegmentedDocumentManagementService;

import spark.Request;
import spark.Response;

/**
 * Created by Jemin.Shah on 6/21/2017.
 */
public class SegmentedDocumentHandlerImpl extends AbstractHandler implements SegmentedDocumentHandler {
    AALogger log = AALogger.create(getClass());

    @Autowired
    SegmentedDocumentManagementService segmentedDocumentManagementService;

    @Override
    public String GetSegmentedDocument(Request request, Response response) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        CustomResponse customResponse = new CustomResponse();

        String fileId = request.params(":fileid");
        log.debug("FileId: " + fileId);

        if (fileId == null) {
            customResponse.setSuccess(false);
            return jsonParser.getResponse(customResponse);
        }

        String result = segmentedDocumentManagementService.GetSegmentedDocument(fileId);

        customResponse.setData(result);
        customResponse.setSuccess(true);
        log.exit();

        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String SaveSegmentedDocument(Request request, Response response) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        CustomResponse customResponse = new CustomResponse();

        String fileId = request.params(":fileid");
        String requestBody = request.body();

        log.debug("FileId: " + fileId);
        //log.debug("SegmentedDocument: " + requestBody);

        if (fileId == null && requestBody == null) {
            customResponse.setSuccess(false);
            return jsonParser.getResponse(customResponse);
        }

        segmentedDocumentManagementService.SaveSegmentedDocument(fileId, requestBody);

        customResponse.setData("");
        customResponse.setSuccess(true);
        log.exit();

        return jsonParser.getResponse(customResponse);
    }
    @Override
    public String deleteAllSegmentedDocument(Request request, Response response)
        throws DataAccessLayerException {
        log.entry();
        CustomResponse customResponse = new CustomResponse();

        segmentedDocumentManagementService.deleteAllSegmentedData();

        customResponse.setSuccess(true);
        log.exit();

        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String deleteSegmentedDocument(Request request, Response response) throws DataAccessLayerException {
        CustomResponse customResponse = new CustomResponse();

        String fileId = request.params(":fileid");
        log.debug("FileId: " + fileId);

        segmentedDocumentManagementService.deleteSegmentedData(fileId);

        customResponse.setSuccess(true);

        return jsonParser.getResponse(customResponse);
    }
}
