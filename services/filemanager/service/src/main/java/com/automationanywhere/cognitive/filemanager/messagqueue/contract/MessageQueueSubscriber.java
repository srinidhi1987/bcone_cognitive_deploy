package com.automationanywhere.cognitive.filemanager.messagqueue.contract;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
public interface MessageQueueSubscriber
{
    void receiveMessage(String message);
}
