package com.automationanywhere.cognitive.filemanager.validation.impl;

import com.automationanywhere.cognitive.filemanager.consumer.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectDeletedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.validation.contract.RequestValidation;
import org.springframework.beans.factory.annotation.Autowired;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class RequestValidationImpl implements RequestValidation
{
    AALogger log = AALogger.create(getClass());

    @Autowired
    protected ProjectServiceEndpointConsumer projectServiceEndpointConsumer;
    @Override
    public boolean validateRequest(Request request, Response response)
    {
        /*log.entry();
        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        ProjectDetail projectDetail =  validateProjectId(orgId, projectID);
        if ( projectDetail == null) {
            return false;
        }
        String fileId = request.params(":fileid");
        *//*if (fileId != null)
        {
            List<FileDetails> list = fileService.getSpecificFile(fileId);
            if(list == null || list.isEmpty())
            {
                halt(404,"Data request.");
            }
        }*//*
        String categoryId = request.params(":id");
        String layoutId = request.params(":layoutid");
        log.exit();*/
        return true;

    }
    @Override
    public ProjectDetail validateProjectId(String orgId, String projectId) throws ProjectNotFoundException, ProjectDeletedException
    {
        log.entry();
        log.debug("Organization id : " + orgId + "Project id : " + projectId);
        ProjectDetail projectDetail = projectServiceEndpointConsumer.getProjectDetails(orgId,projectId);
        if(projectDetail != null)
        {
            log.exit();
            return projectDetail;
        }
        else
        {
            log.error("Project not found");
            log.exit();
            return null;
        }
    }

}
