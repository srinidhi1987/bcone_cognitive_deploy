package com.automationanywhere.cognitive.filemanager.health.connections;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.UnSupportedConnectionTypeException;

public class HealthCheckConnectionFactory {
 
    @Autowired
    DBCheckConnection dbCheckConnection;
    @Autowired
    ServiceCheckConnection serviceCheckConnection;
    @Autowired
    MQCheckConnection mqCheckConnection;
    
	public HealthCheckConnection getConnection(ConnectionType connType){
		switch(connType){
			case DB : return dbCheckConnection;
			case SVC: return serviceCheckConnection;
			case MQ: return mqCheckConnection;
		}
		throw new UnSupportedConnectionTypeException(connType + " is not supported ");
	}
}
