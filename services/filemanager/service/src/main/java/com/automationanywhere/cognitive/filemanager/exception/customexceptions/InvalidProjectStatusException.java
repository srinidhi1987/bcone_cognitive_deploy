package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class InvalidProjectStatusException extends RuntimeException {
    
    private static final long serialVersionUID = -2705346481982181643L;
    
    public InvalidProjectStatusException(String message) {
        super(message);
    }
}
