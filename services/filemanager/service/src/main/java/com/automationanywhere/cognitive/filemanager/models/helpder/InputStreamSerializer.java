package com.automationanywhere.cognitive.filemanager.models.helpder;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.io.InputStream;

import spark.utils.IOUtils;

/**
 * Created by Mayur.Panchal on 18-03-2017.
 */
//TODO remove the class if not needed
public class InputStreamSerializer  extends JsonSerializer<InputStream> {
    @Override
    public void serialize(InputStream value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeStartArray();
        gen.writeBinary(IOUtils.toByteArray(value));
        gen.writeEndArray();
    }
}
