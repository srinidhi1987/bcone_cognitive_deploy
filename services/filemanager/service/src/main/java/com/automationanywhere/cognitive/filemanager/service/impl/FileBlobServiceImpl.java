package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.CompareOperator;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.MapValueParameter;
import com.automationanywhere.cognitive.filemanager.service.contract.FileBlobService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mayur.Panchal on 26-02-2017.
 */
public class FileBlobServiceImpl implements FileBlobService {
    AALogger log = AALogger.create(this.getClass());
    @Autowired
    private CommonDataAccessLayer commonDataAccessLayer;
    
    @Override
    public List<FileBlobs> getFileBlob(String fileId) throws DataAccessLayerException,DataNotFoundException
    {
        log.entry();
        if(fileId == null || "".equals(fileId)) {
            return null;
        }
        FileBlobs fileBlob = new FileBlobs();
        fileBlob.WhereParam.put(FileBlobs.Property.fileId.toString(),new MapValueParameter(fileId, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        List<FileBlobs> listfileblob = commonDataAccessLayer.SelectRows(fileBlob);
        if(listfileblob == null || listfileblob.size() == 0){
            log.error("No FileBlob found.");
            throw new DataNotFoundException("No FileBlob found, either it got deleted or does not exists");
        }
        log.exit();
        return  listfileblob;
    }

    @Override
    public boolean deleteFileBlobs(String fileId)  throws DataAccessLayerException
    {
        log.entry();
        log.debug(() -> String.format("[Cleaning Data] [FileBlob] [File Id = %s]",fileId));
        if (fileId == null || "".equals(fileId)) {
            return false;
        }
        FileBlobs fileBlobs = new FileBlobs();
        fileBlobs.WhereParam.put(FileDetails.Property.fileId.toString(), new MapValueParameter(fileId, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        boolean returnValue = commonDataAccessLayer.DeleteRows(fileBlobs);
        log.exit();
        return  returnValue;
    }
}
