package com.automationanywhere.cognitive.filemanager.models;

/**
 * Created by Mayur.Panchal on 08-12-2016.
 */

public class FileDetailsResponse {

    private String fileid;
    private String projectid;
    private String filename;
    private String filelocation;
    private Long filesize;
    private int fileheight;
    private int filewidth;
    private String format;
    private boolean processed;
    private String classificationid;
    private String uploadrequestid;

    public void setFileName(String name)
    {
        this.filename = name;
    }
    public String getFileName()
    {
        return this.filename;
    }

    public String getFileId()
    {
        return this.fileid;
    }
    public void setFileId(String id)
    {
        this.fileid = id;
    }

    public String getProjectId() { return this.projectid; }
    public void setProjectId(String projectId) { this.projectid = projectId; }

    public String getClassificationId() { return this.classificationid;}
    public void setClassificationId(String classificationId) { this.classificationid = classificationId; }

    public String getuploadrequestid() { return this.uploadrequestid; }
    public void setuploadrequestid(String uploadRequestid) { this.uploadrequestid = uploadRequestid; }

    public void setLocation(String location)
    {
        this.filelocation = location;
    }
    public String getLocation()
    {
        return this.filelocation;
    }

    public void setSize(Long size)
    {
        this.filesize = size;
    }
    public Long getSize()
    {
        return this.filesize;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }
    public String getFormat()
    {
        return this.format;
    }

    public void setHeight(int height) {this.fileheight = height;}
    public int getHeight() {return this.fileheight;}

    public void setWidth(int width) {this.filewidth = width;}
    public int getWidth() {return this.filewidth;}

    public void setIsProcessed(boolean isprocessed)
    {
        this.processed = isprocessed;
    }
    public int getIsProcessed()
    {
        return this.processed == false ? 0 :1;
    }
}
