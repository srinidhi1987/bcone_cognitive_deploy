package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by keval.sheth on 29-11-2016.
 */
public class ProjectNotFoundException extends RuntimeException{
    
    private static final long serialVersionUID = 7873995423901206095L;
    
    public ProjectNotFoundException(String message) {
        super(message);
    }
}