package com.automationanywhere.cognitive.filemanager.models.statistics;

import java.util.List;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class CountStatistic
{
    private String projectId;
    private String categoryId;
    private long fileCount;
    private EnvironmentCountStatistic stagingFileCount;
    private EnvironmentCountStatistic productionFileCount;
    private List<FileDetails> fileDetailsList;

    public CountStatistic()
    {
        this.fileCount = 0;
        this.stagingFileCount = new EnvironmentCountStatistic();
        this.productionFileCount = new EnvironmentCountStatistic();
    }

    @JsonProperty
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty
    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty
    public long getFileCount() {
        fileCount = this.stagingFileCount.getTotalCount() + this.productionFileCount.getTotalCount();
        return fileCount;
    }

    public void setFileCount(long fileCount) {
        this.fileCount = fileCount;
    }

    @JsonProperty
    public EnvironmentCountStatistic getStagingFileCount() {
        return stagingFileCount;
    }

    public void setStagingFileCount(EnvironmentCountStatistic stagingFileCount) {
        this.stagingFileCount = stagingFileCount;
    }

    public EnvironmentCountStatistic getProductionFileCount() {
        return productionFileCount;
    }

    public void setProductionFileCount(EnvironmentCountStatistic productionFileCount) {
        this.productionFileCount = productionFileCount;
    }

    @JsonIgnore
    public List<FileDetails> getFileDetailsList() {
        return fileDetailsList;
    }

    public void setFileDetailsList(List<FileDetails> fileDetailsList) {
        this.fileDetailsList = fileDetailsList;
    }
}
