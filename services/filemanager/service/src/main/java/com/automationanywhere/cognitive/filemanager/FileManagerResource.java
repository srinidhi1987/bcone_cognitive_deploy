package com.automationanywhere.cognitive.filemanager;

import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.constants.SystemStatus;
import com.automationanywhere.cognitive.common.healthapi.responsebuilders.HealthApiResponseBuilder;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.*;
import com.automationanywhere.cognitive.filemanager.exception.contract.ExceptionHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.ClassificationReportHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.FileBlobHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.FileDetailsHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.SegmentedDocumentHandler;
import com.automationanywhere.cognitive.filemanager.health.handlers.impl.FileManagerHealthCheckHandler;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;

import spark.Request;
import spark.Response;

import java.util.UUID;

import static com.automationanywhere.cognitive.filemanager.constants.HeaderConstants.CORRELATION_ID;
import static spark.Spark.*;

public class FileManagerResource {
    private static AALogger logger = AALogger.create(FileManagerResource.class);
    @Autowired
    FileDetailsHandler fileDetailsHandler;
    @Autowired
    FileBlobHandler fileBlobHandler;
    @Autowired
    ClassificationReportHandler classificationReportHandler;
    @Autowired
    SegmentedDocumentHandler segmentedDocumentHandler;
    @Autowired
    ExceptionHandler exceptionHandler;
    @Autowired
    FileManagerHealthCheckHandler fileManagerHealthCheckHandler;

    //@PostConstruct
    public FileManagerResource(String hostport) {
        port(Integer.parseInt(hostport));
        int maxThreads = 150;
        int minThreads = 1;
        int timeOutMillis = 600000;

        threadPool(maxThreads, minThreads, timeOutMillis);
        before((request, response) -> preRequestProcessor(request));
        exception(DataNotFoundException.class, (ex, req, res) -> exceptionHandler.handleDataNotFoundException(ex, req, res));
        exception(DataAccessLayerException.class, (ex, req, res) -> exceptionHandler.handleDataAccessLayerException(ex, req, res));
        exception(BadRequestException.class, (ex, req, res) -> exceptionHandler.handleBadRequestException(ex, req, res));
        exception(InvalidFileFormatException.class, (ex, req, res) -> exceptionHandler.handleInvalidFileFormatException(ex, req, res));
        exception(FileSizeExceedException.class, (ex, req, res) -> exceptionHandler.handleFileSizeExceedException(ex, req, res));
        exception(FileNameLengthExceedException.class, (ex, req, res) -> exceptionHandler.handleFileNameLengthExceedException(ex, req, res));
        exception(ProjectDeletedException.class, (ex, req, res) -> exceptionHandler.handleProjectDeletedException(ex, req, res));
        exception(ProjectNotFoundException.class, (ex, req, res) -> exceptionHandler.handleProjectNotException(ex, req, res));
        exception(Exception.class, (ex, req, res) -> exceptionHandler.handleException(ex, req, res));

        after(((request, response) -> postRequestProcessor(request, response)));
        //FileDetail Handler
        get("organizations/:orgid/projects/:prjid/categories", (req, res) -> fileDetailsHandler.getCategoriesByProjectId(req, res));
        get("organizations/:orgid/projects/:prjid/categories/:id/files", (req, res) -> fileDetailsHandler.GetCategoryWiseFiles(req, res));
        get("organizations/:orgid/projects/:prjid/files", (req, res) -> fileDetailsHandler.GetProjectWiseFiles(req, res));
        get("organizations/:orgid/projects/:prjid/files/counts", (req, res) -> fileDetailsHandler.getFileStatistics(req, res));
        get("organizations/:orgid/projects/:prjid/files/:fileid", (req, res) -> fileDetailsHandler.GetSpecificFile(req, res));
        post("organizations/:orgid/projects/:prjid/files/",/*"multipart/form-data",*/ (req, res) -> fileDetailsHandler.uploadResource(req, res));
        post("organizations/:orgid/projects/:prjid/files/upload/:isProduction",/*"multipart/form-data",*/ (req, res) -> fileDetailsHandler.uploadResource(req, res));
        delete("organizations/:orgid/projects/:prjid/categories/:id/files/:fileid", (req, res) -> fileDetailsHandler.DeleteSpecificFile(req, res));
        patch("organizations/:orgid/projects/:prjid/files/:fileid", (req, res) -> fileDetailsHandler.patch(req, res));
        get("organizations/:orgid/files/counts", (req, res) -> fileDetailsHandler.getFileStatistics(req, res));
        //FileBlob handler
        get("organizations/:orgid/projects/:prjid/fileblob/:fileid", (req, res) -> fileBlobHandler.getFileBlob(req, res));
        //classificationreport handler
        post("organizations/:orgid/projects/:prjid/files/:fileid/categories/:layoutid", (req, res) -> classificationReportHandler.updateResource(req, res));
        post("organizations/:orgid/projects/:prjid/files/:fileid/categories/:layoutid/:id", (req, res) -> classificationReportHandler.updateResource(req, res));
        get("organizations/:orgid/projects/:prjid/filestatus", (req, res) -> classificationReportHandler.GetProjectWiseClassification(req, res));
        get("organizations/:orgid/projects/:prjid/report", (req, res) -> classificationReportHandler.GetClassificationReport(req, res));
        //SegmentedDocument Handler
        get("segmenteddocument/:fileid", (req, res) -> segmentedDocumentHandler.GetSegmentedDocument(req, res));
        post("segmenteddocument/:fileid", (req, res) -> segmentedDocumentHandler.SaveSegmentedDocument(req, res));
        delete("organizations/:orgid/files/segmenteddocument",(req, res) -> segmentedDocumentHandler.deleteAllSegmentedDocument(req, res));
        delete("organizations/:orgid/files/:fileid/segmenteddocument",(req,res) -> segmentedDocumentHandler.deleteSegmentedDocument(req,res));
        get("organizations/:orgid/projects/:prjid/categories/:id/files/staging", (req, res) -> fileDetailsHandler.getStagingFilesIfNotAvailableCopyFromProduction(req, res));
        get("organizations/:orgid/projects/:prjid/classificationsummary",
                (req, res) -> fileDetailsHandler.getLearningInstanceSummary(req, res));
        get("organizations/:orgid/projects/:prjid/reclassify",
                (req, res) -> fileDetailsHandler.triggerReclassification(req, res));
        get("/health", ((request, response) -> {
            logger.entry();
            response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
            response.body("OK");
            logger.exit();
            return response;
        }));
        get("/heartbeat", ((request, response) -> {
            return getHeartBeatInfo(response);
        }));
        get("/healthcheck", ((request, response) -> {
            return fileManagerHealthCheckHandler.checkHealth();
        }));
    }

    private void postRequestProcessor(Request request, Response response) {
        addHeader(request, response);
    }

    private void preRequestProcessor(Request request) {
        addTransactionIdentifier(request);
        logger.debug("Request URI.." + request.uri());
    }

    private void addTransactionIdentifier(Request request) {
        String transactionId = request.headers(CORRELATION_ID);
        ThreadContext.put(CORRELATION_ID, transactionId != null ?
                transactionId : UUID.randomUUID().toString());
    }

    private void addHeader(Request request, Response response) {
        response.type("application/json");
    }


    /**
     * Get the heartbeat of the service
     *
     * @param response
     * @return
     */
    public Object getHeartBeatInfo(Response response) {
        logger.entry();
        String heartBeat;
        //bean initialization successful then return OK,
        //currently bean initialization fails when MQ connection is faling
        if (isBeanInitSucessful()) {
            response.status(org.eclipse.jetty.http.HttpStatus.OK_200);
            heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.OK, SystemStatus.ONLINE);
        }//if beans are null then return Failure
        else {
            response.status(org.eclipse.jetty.http.HttpStatus.INTERNAL_SERVER_ERROR_500);
            heartBeat = HealthApiResponseBuilder.prepareHeartBeat(HTTPStatusCode.INTERNAL_SERVER_ERROR, SystemStatus.OFFLINE);
        }
        response.body(heartBeat);
        logger.exit();
        return response;
    }

    /**
     * Check if the all the beans are successfully initialized
     *
     * @return
     */
    public boolean isBeanInitSucessful() {
        logger.entry();
        return fileDetailsHandler != null && classificationReportHandler != null && segmentedDocumentHandler != null
                && fileBlobHandler != null;
    }
}
