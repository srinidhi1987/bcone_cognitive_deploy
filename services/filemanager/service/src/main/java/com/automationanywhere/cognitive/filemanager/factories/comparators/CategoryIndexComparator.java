package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryIndexComparator implements Comparator<Category>, Serializable {
    
    private static final long serialVersionUID = -2535939835170636526L;
    private final ComparisonOrder order;

    public CategoryIndexComparator(ComparisonOrder order) {
        this.order = order;
    }

    @Override
    public int compare(Category c1, Category c2) {
        // If a guarantee can be made that the category detail is always present then the defensive programming below could be removed
        if (c1.getCategoryDetail() == null && c2.getCategoryDetail() == null) {
            return 0;
        } else if (c1.getCategoryDetail() != null && c2.getCategoryDetail() == null) {
            return -1 * order.getDirection();
        } else if (c1.getCategoryDetail() == null && c2.getCategoryDetail() != null) {
            return 1 * order.getDirection();
        } else if (c1.getCategoryDetail().index == c2.getCategoryDetail().index) {
            return 0;
        }
        return ((Double.compare(c1.getCategoryDetail().index, c2.getCategoryDetail().index) < 0)? -1 : 1) * order.getDirection();

    }
}