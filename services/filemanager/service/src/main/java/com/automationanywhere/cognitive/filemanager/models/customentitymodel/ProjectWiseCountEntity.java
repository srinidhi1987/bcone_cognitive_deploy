package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mayur.Panchal on 06-04-2017.
 */
public class ProjectWiseCountEntity{
        @JsonProperty("projectId")
        private String projectId;
        @JsonProperty("count")
        private long count;
        public void setProjectId(String projectId) { this.projectId = projectId;}
        public String getProjectId() { return this.projectId;}
        public void setCount(long count){ this.count = count;}
        public long getCount() { return this.count; }
}
