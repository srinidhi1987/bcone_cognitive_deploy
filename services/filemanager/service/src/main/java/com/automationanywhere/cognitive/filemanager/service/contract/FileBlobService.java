package com.automationanywhere.cognitive.filemanager.service.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;

import java.util.List;

/**
 * Created by Mayur.Panchal on 09-12-2016.
 */
public interface FileBlobService {
    List<FileBlobs> getFileBlob(String fileId) throws DataAccessLayerException,DataNotFoundException;
    boolean deleteFileBlobs(String fileId) throws DataAccessLayerException;
}
