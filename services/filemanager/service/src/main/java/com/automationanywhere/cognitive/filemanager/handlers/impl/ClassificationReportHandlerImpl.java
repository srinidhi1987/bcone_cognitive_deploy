package com.automationanywhere.cognitive.filemanager.handlers.impl;

import static com.automationanywhere.cognitive.filemanager.constants.Constants.UNCLASSIFICATION_ID;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.consumer.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectDeletedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.handlers.contract.AbstractHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.ClassificationReportHandler;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueuePublisher;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.FileDetail;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.FileProcessingDetail;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.models.FileWorker;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.models.FileProcessing.ExportDetail;
import com.automationanywhere.cognitive.filemanager.models.classification.RawFileReportFromClassifier;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileUploadStatus;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.ClassificationReport;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.service.contract.ClassificationReportService;
import com.automationanywhere.cognitive.filemanager.service.contract.FileDetailsService;
import com.automationanywhere.cognitive.filemanager.service.contract.FileExportService;
import com.automationanywhere.cognitive.filemanager.service.impl.DataCleanupService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 23-02-2017.
 */
public class ClassificationReportHandlerImpl extends AbstractHandler implements ClassificationReportHandler {
    AALogger log = AALogger.create(getClass());
    @Autowired
    protected ProjectServiceEndpointConsumer projectServiceEndpointConsumer;
    @Autowired
    @Qualifier("classificationReportService")
    private ClassificationReportService classificationReportService;

    @Autowired
    @Qualifier("fileDetailsService")
    private FileDetailsService fileDetailsService;

    @Autowired
    @Qualifier("visionbotProcessDocumentService")
    private MessageQueuePublisher processingQueuePublisher;

    @Autowired
    @Qualifier("unclassifiedFileExportService")
    private FileExportService fileExportService;

    @Autowired
    private DataCleanupService dataCleanupService;

    @Override
    public String GetClassificationReport(Request request, Response response) throws DataAccessLayerException, DataNotFoundException {
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);

        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            response.status(200);
            customResponse.setSuccess(false);
            log.error("Project not found.");
            customResponse.setErrors("project not found");
            return jsonParser.getResponse(customResponse);
        }
        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        log.debug(()-> "OrgId:" + orgId + " ProjectId:" + projectID + " ");
        response.status(200);
        customResponse.setData(classificationReportService.getClassificationAnalysisReport(projectID));
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String GetProjectWiseClassification(Request request, Response response)
            throws DataAccessLayerException, DataNotFoundException, ProjectNotFoundException, ProjectDeletedException {
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);
        String requestId = request.queryParams("requestid");
        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            response.status(200);
            customResponse.setSuccess(false);
            log.error("Project not found.");
            customResponse.setErrors("project not found");
            return jsonParser.getResponse(customResponse);
        }
        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        log.debug(()-> "OrgId:" + orgId + " ProjectId:" + projectID + " RequestId:" + requestId + " ");
        ProjectDetail projectDetail = projectServiceEndpointConsumer.getProjectDetails(orgId, projectID);
        if (projectDetail == null) {
            customResponse.setSuccess(false);
            log.error("Project not found.");
            customResponse.setErrors("Project not found.");
            return jsonParser.getResponse(customResponse);
        }

        FileWorker fileWorker = new FileWorker();
        fileWorker.setProjectId(projectID);
        FileUploadStatus fileUploadStatus = fileDetailsService.getFileUploadProgress(projectID,
                requestId,
                projectDetail.getEnvironment());

        boolean status = false;
        if (fileUploadStatus != null) {
            status = fileUploadStatus.getTotalFileCount() - fileUploadStatus.getTotalProcessedCount() == 0;
            response.status(200);
            fileWorker.setCurrentProcessingObject(null);
            fileWorker.setTotalFileCount(fileUploadStatus.getTotalFileCount());
            fileWorker.setTotalProcessedCount(fileUploadStatus.getTotalProcessedCount());
            fileWorker.setEstimatedTimeRemainingInMins(fileUploadStatus.getTotalFileCount() - fileUploadStatus.getTotalProcessedCount());
            fileWorker.setStatus(status);
        }
        customResponse.setData(fileWorker);
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);

    }

    @Override
    public String updateResource(Request request, Response response) throws DataAccessLayerException {
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);

        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            response.status(200);
            customResponse.setSuccess(false);
            log.error("project not found");
            customResponse.setErrors("Learning Instance not found");
            return jsonParser.getResponse(customResponse);
        }

        String orgId = request.params(":orgid");
        String projectid = request.params(":prjid");
        String categoryid = request.params(":id");
        String fileid = request.params(":fileId");
        String layoutid = request.params(":layoutId");
        log.debug(()-> "OrgId:" + orgId + " ProjectId:" + projectid + " CategoryId:" + categoryid + " FileId:" + fileid + " layoutid:" + layoutid);
        String classificationdetail = request.body();
        FileProcessingDetail fileProcessingDetail = null;

        if (classificationdetail != null && !classificationdetail.equalsIgnoreCase("")) {
            ClassificationReport classificationReportDetails;
            RawFileReportFromClassifier classificationReport = RawFileReportFromClassifier.Create(classificationdetail);
            response.status(200);
            if (classificationReport != null) {
                for (int i = 0; i < classificationReport.getClassificationFieldDetails().size(); i++) {
                    classificationReportDetails = new ClassificationReport();
                    classificationReportDetails.setDocumentId(classificationReport.getDocumentId());
                    classificationReportDetails.setAliasName(classificationReport.getClassificationFieldDetails().get(i).getAliasName());
                    classificationReportDetails.setFieldId(classificationReport.getClassificationFieldDetails().get(i).getFieldId());
                    classificationReportDetails.setFound(classificationReport.getClassificationFieldDetails().get(i).isFound());
                    classificationReportService.insertClassificationDetail(classificationReportDetails);
                }
            }
            customResponse.setSuccess(true);

            fileProcessingDetail = new FileProcessingDetail();
            fileProcessingDetail.setCategoryId(categoryid);
            fileProcessingDetail.setDocumentId(fileid);
            fileProcessingDetail.setLayoutId(layoutid);
            fileProcessingDetail.setOrganizationId(orgId);
            fileProcessingDetail.setProjectId(projectid);
        }
        response.status(200);
        fileDetailsService.updateResource(fileid, categoryid, layoutid);

        if (fileProcessingDetail != null && !fileProcessingDetail.getCategoryId().equals(UNCLASSIFICATION_ID))
            publishProcessingMessage(fileProcessingDetail);

        exportFile(projectid, categoryid, fileid, orgId);
        checkAndCleanupProductionData(categoryid,fileid);
        customResponse.setSuccess(true);

        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    private void checkAndCleanupProductionData(String categoryId ,String fileid) throws DataAccessLayerException
    {
        try {
            if (categoryId != null && categoryId.equalsIgnoreCase(UNCLASSIFICATION_ID)) {

                List<FileDetails> fileDetails = fileDetailsService.getSpecificFile(fileid);

                FileDetails file = fileDetails.get(0);
                if (file.getIsProduction()) {
                    FileDetail fileDetail = new FileDetail();
                    fileDetail.setFileId(fileid);
                    dataCleanupService.cleanupData(fileDetail);
                }
            }
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
        }
    }
    private void exportFile(String projectId, String categoryId, String fileId, String organizationId)
    {
        if(categoryId != null && categoryId.equalsIgnoreCase(UNCLASSIFICATION_ID))
        {
            try
            {
                ExportDetail exportDetail = new ExportDetail();
                exportDetail.setFileId(fileId);
                exportDetail.setProjectId(projectId);
                exportDetail.setOrganizationId(organizationId);
                fileExportService.export(exportDetail);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }
        }
    }

    private void publishProcessingMessage(FileProcessingDetail fileProcessingDetail )
    {
        String message = toJsonString(fileProcessingDetail);
        try{
         processingQueuePublisher.sendMessage(message);
        } catch (Exception ex) {
            log.error("publish file processing message error: " , ex);
        }
    }

    private String toJsonString(Object object) {
        log.entry();
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            log.error(ex.getMessage());
            return "";
        } finally {
            log.exit();
        }
    }
}
