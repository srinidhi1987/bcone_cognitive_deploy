package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.FileProcessing.ExportDetail;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.service.contract.FileDetailsService;
import com.automationanywhere.cognitive.filemanager.service.contract.FileExportService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Jemin.Shah on 8/2/2017.
 */

public class UnprocessedFileExportServiceImpl extends FileExportService
{

    @Autowired
    FileDetailsService fileDetailsService;

    private static final String SubFolderName = "Not Processed";
    private static final String CategoryFolderPrefix = "Group";
    private String outputPath;
    private FileDetails fileDetails;
    private ExportDetail exportDetail;

    private AALogger log = AALogger.create(this.getClass());

    public UnprocessedFileExportServiceImpl(String outputPath)
    {
        this.outputPath = outputPath;
    }

    @Override
    public void export(ExportDetail exportDetail) throws IOException, SQLException, DataAccessLayerException {
        synchronized (this) {
            log.entry();

            this.exportDetail = exportDetail;

            try {
                List<FileDetails> fileDetailsList = fileDetailsService.getSpecificFile(exportDetail.getFileId());
                fileDetails = fileDetailsList.stream().findFirst().get();

                super.export(exportDetail);
            } catch (DataNotFoundException e) {
                log.error(e.getMessage(), e);
                log.debug("Removing project cache detail for id: " + exportDetail.getProjectId());
                removeCache(exportDetail.getProjectId());
            }

            log.exit();
        }
    }

    @Override
    protected String buildOutputPath()
    {
        return outputPath + "\\" + getProjectName(exportDetail.getOrganizationId(), exportDetail.getProjectId())
                + "\\" + SubFolderName
                + "\\" + CategoryFolderPrefix + "_" + fileDetails.getClassificationId()
                + "\\" + fileDetails.getFileId() + "_" + fileDetails.getFileName();
    }

    @Override
    protected boolean validate()
    {
        return fileDetails.getIsProduction()
                && fileDetails.getClassificationId() != null
                && !fileDetails.getClassificationId().isEmpty();
    }

}
