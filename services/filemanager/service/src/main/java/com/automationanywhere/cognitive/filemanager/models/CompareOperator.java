package com.automationanywhere.cognitive.filemanager.models;

/**
 * Created by Mayur.Panchal on 12-01-2017.
 */
public interface CompareOperator {
    String equal = "=";
    String greaterthanorequal=">=";
    String greaterthan=">";
    String lessthenorequal="<=";
    String lessthen="<";
    String notequal = "!=";
}
