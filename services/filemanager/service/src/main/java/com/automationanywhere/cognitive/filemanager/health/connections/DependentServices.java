package com.automationanywhere.cognitive.filemanager.health.connections;

/**
 * @author shweta.thakur
 * This class will hold Dependent micro-services list
 */
public enum DependentServices {
    Project, VisionBot
}
