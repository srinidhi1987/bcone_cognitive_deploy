package com.automationanywhere.cognitive.filemanager.factories;

import com.automationanywhere.cognitive.filemanager.factories.comparators.*;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.util.Comparator;

public class CategoryComparatorFactory {
    public static Comparator<Category> getComparator(String sortSpecification) {
        switch (sortSpecification) {
            case "index":
                return new CategoryIndexComparator(ComparisonOrder.ASCENDING);
            case "-index":
                return new CategoryIndexComparator(ComparisonOrder.DESCENDING);
            case "categoryName":
                return new CategoryNameComparator(ComparisonOrder.ASCENDING);
            case "-categoryName":
                return new CategoryNameComparator(ComparisonOrder.DESCENDING);
            case "stagingFilesCount":
                return new CategoryStagingFilesCountComparator(ComparisonOrder.ASCENDING);
            case "-stagingFilesCount":
                return new CategoryStagingFilesCountComparator(ComparisonOrder.DESCENDING);
            case "stagingFilesStp":
                return new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);
            case "-stagingFilesStp":
                return new CategoryStagingFilesStpComparator(ComparisonOrder.DESCENDING);
            case "stagingFilesUnproccessed":
                return new CategoryStagingFilesUnprocessedComparator(ComparisonOrder.ASCENDING);
            case "-stagingFilesUnproccessed":
                return new CategoryStagingFilesUnprocessedComparator(ComparisonOrder.DESCENDING);
            case "productionFilesCount":
                return new CategoryProductionFilesCountComparator(ComparisonOrder.ASCENDING);
            case "-productionFilesCount":
                return new CategoryProductionFilesCountComparator(ComparisonOrder.DESCENDING);
            case "productionFilesStp":
                return new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);
            case "-productionFilesStp":
                return new CategoryProductionFilesStpComparator(ComparisonOrder.DESCENDING);
            case "productionFilesUnproccessed":
                return new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);
            case "-productionFilesUnproccessed":
                return new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.DESCENDING);
            default:
                return null;
        }
    }
}
