package com.automationanywhere.cognitive.filemanager.service.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;

public interface ProjectCachingService
{
    ProjectDetail get(String projectId) throws ProjectNotFoundException;
    void put(ProjectDetail projectDetail);
    void delete(String projectId);
}
