package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Jemin.Shah on 6/21/2017.
 */
@Entity
@Table(name = "SegmentedDocumentDetails")
public class SegmentedDocumentDetails extends EntityModelBase
{
    @Id
    @Column(name ="FileId")
    String fileId;

    @Column(name="SegmentedDocument")
    String segmentedDocument;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getSegmentedDocument() {
        return segmentedDocument;
    }

    public void setSegmentedDocument(String segmentedDocument) {
        this.segmentedDocument = segmentedDocument;
    }

    public SegmentedDocumentDetails()
    {
        super();
    }
}
