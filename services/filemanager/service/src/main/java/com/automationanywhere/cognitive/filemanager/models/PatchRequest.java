package com.automationanywhere.cognitive.filemanager.models;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
public class PatchRequest
{
    private PatchOperation op;
    private String path;
    private String value;

    public PatchOperation getOp() {
        return op;
    }

    public void setOp(PatchOperation op) {
        this.op = op;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}