package com.automationanywhere.cognitive.filemanager;


import static spark.Spark.secure;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.automationanywhere.cognitive.common.logger.AALogger;


/**
 * Created by keval.sheth on 29-11-2016.
 */
public class Main {

    private static final String JDBC_STRING = "jdbc:sqlserver://";
    private static final String FILENAME_COGNITIVE_CERTIFICATE = "configurations//CognitivePlatform.jks";
    private static final String FILENAME_COGNITIVE_PROPERTIES = "configurations//Cognitive.properties";
    private static final String PROPERTIES_CERTIFICATEKEY = "CertificateKey";
    private static final String PROPERTIES_NOTFOUND = "NotFound";
    private static final String SQL_SERVER_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final int DATASOURCE_INITIAL_SIZE = 3;
    private static final String HTTP_SCHEME = "http";
    private static final String HTTPS_SCHEME = "https";

    private static String jdbcUrl;
    private static String username;
    private static String visionbotServiceHost;
    private static String visionbotServicePort;
    private static String projectServiceHost;
    private static String projectServicePort;
    private static String serviceport;
    private static String fileExportPath;
    private static boolean windowsAuth;
    private static final String FILENAME_COGNITIVE_SETTINGS = "configurations\\Settings.txt";
    private static final String PROPERTIES_SQL_WINAUTH = "SQLWindowsAuthentication";
    private static String settingsFilePath;

    private static String visionBotRootUrl;
    private static String projectServiceRootUrl;
    private static AALogger aaLogger = AALogger.create(Main.class);

    public static void main(String args[]) throws IOException {
        if(args.length == 10) {
            String dbHostname = args[0];
            String dbPort = args[1];
            String dbName = args[2];
            username = args[3];
            projectServiceHost = args[4];
            projectServicePort = args[5];
            visionbotServiceHost = args[6];
            visionbotServicePort = args[7];
            visionBotRootUrl = String.format("%s://%s:%s", HTTP_SCHEME, visionbotServiceHost, visionbotServicePort);
            projectServiceRootUrl = String.format("%s://%s:%s", HTTP_SCHEME, projectServiceHost, projectServicePort);
            serviceport = args[8];
            fileExportPath = args[9];
            //If output folder name contains space then we have replaced 'space' with '?' during installation
            //hence here need to decode '?' with 'space' to make it original name
            fileExportPath = fileExportPath.replace("?"," ");
            jdbcUrl = JDBC_STRING + dbHostname + ":" + dbPort;
            jdbcUrl += ";databaseName=" + dbName;
            jdbcUrl += ";sendStringParametersAsUnicode=false";
            loadSettings();
            if (windowsAuth) {
                jdbcUrl += ";integratedSecurity=true";
            }
            setServiceSecurity();
            new ClassPathXmlApplicationContext("SpringBeans.xml");

        } else {
            aaLogger.warn("Less arguments found. Exiting from the app.");
            System.out.println("10 arguments necessary to run jar file <db-hostname> <db-port> <db-name> <username> "
                + "<project-service-host> <project-service-port> "
                + "<visionbot-service-host> <visionbot-service-port> "
                + "<serviceport> <file export path>");
        }
    }

    private static String getServicePort() {
        return serviceport;
    }

    private static String getProjectServiceHost() {
        return projectServiceHost;
    }

    private static String getProjectServicePort() {
        return projectServicePort;
    }

    public static String getVisionbotServiceHost() {
        return visionbotServiceHost;
    }

    public static String getVisionbotServicePort() {
        return visionbotServicePort;
    }
    public static String getVisionBotRootUrl() { return visionBotRootUrl; }

    public static String getProjectServiceRootUrl() { return projectServiceRootUrl; }

    public static String getFileExportPath()
    {
        return fileExportPath;
    }

    private static void setServiceSecurity() {
        aaLogger.trace("Setting security for service");
        if (isSSLCertificateConfigured()) {
            try {
                String certificateKey = loadSSLCertificatePassword();
                if (!certificateKey.equals(PROPERTIES_NOTFOUND)) {
                	Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
                    String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
                    secure(cognitiveCertificateFilePath, certificateKey, null, null);

                    applyHttpsSchemeToExternalServiceUrl();
                }
                else {
                    aaLogger.trace("Certificate not found.");
                }
            } catch (IOException e) {
                aaLogger.error(e.getMessage());
            }
        }
    }

    private static String loadSSLCertificatePassword() throws IOException {
        String sslCertPwd;
    	Path propertyFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitivePropertiesFilePath = propertyFilePath.toString() + File.separator + FILENAME_COGNITIVE_PROPERTIES;
        if (fileExists(cognitivePropertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(cognitivePropertiesFilePath));
            sslCertPwd = properties.getProperty(PROPERTIES_CERTIFICATEKEY, PROPERTIES_NOTFOUND);
        } else {
            aaLogger.info("Cognitive properties file not found");
            sslCertPwd = PROPERTIES_NOTFOUND;
        }
        return sslCertPwd;
    }

    private static boolean isSSLCertificateConfigured() {
        boolean hasCertificate = false;
        Path certificateFilePath = Paths.get(System.getProperty("user.dir")).getParent();
        String cognitiveCertificateFilePath = certificateFilePath.toString() + File.separator + FILENAME_COGNITIVE_CERTIFICATE;
        if (fileExists(cognitiveCertificateFilePath)) {
            hasCertificate = true;
        } else {
            aaLogger.trace("certificate file not found");
        }
        return hasCertificate;
    }

    private static boolean fileExists(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }


	private static void applyHttpsSchemeToExternalServiceUrl() {
		visionBotRootUrl = String.format("%s://%s:%s", HTTPS_SCHEME, visionbotServiceHost, visionbotServicePort);
        projectServiceRootUrl = String.format("%s://%s:%s", HTTPS_SCHEME, projectServiceHost, projectServicePort);
	}


    public static BasicDataSource createDataSource(String password) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(SQL_SERVER_DRIVER);
        dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
        dataSource.setUrl(jdbcUrl);
        if (!windowsAuth) {
            dataSource.setUsername(username);
            dataSource.setPassword(password);
        }
        return dataSource;
    }

    private static void loadSettings() throws IOException {
        Path configurationDirectoryPath  = Paths.get(System.getProperty("user.dir")).getParent();
        settingsFilePath = configurationDirectoryPath.toString() + File.separator + FILENAME_COGNITIVE_SETTINGS;
        Properties properties = loadProperties(settingsFilePath);
        if(properties.containsKey(PROPERTIES_SQL_WINAUTH))
        {
            windowsAuth = Boolean.parseBoolean(properties.getProperty(PROPERTIES_SQL_WINAUTH));
        }
    }

    private static Properties loadProperties(String filePath) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filePath));
        return properties;
    }
}
