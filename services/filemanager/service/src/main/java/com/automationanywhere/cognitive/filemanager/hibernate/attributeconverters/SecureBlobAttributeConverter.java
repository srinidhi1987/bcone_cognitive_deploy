package com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.sql.rowset.serial.SerialBlob;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.automationanywhere.cognitive.common.security.DefaultCipherService;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.BlobCipherException;

@Component
@Converter
public class SecureBlobAttributeConverter implements
    AttributeConverter<Blob, Blob> {

  private static DefaultCipherService cipherService;
  private static InputStreamConverter inputStreamConverter;
  private static boolean encryptionEnabled = false;

  @Autowired
  public void initCipherService(final DefaultCipherService cipherService,
      final InputStreamConverter inputStreamConverter,
      @Value("${EncryptionEnabled:false}") boolean encryptionEnabledProperty) {
    SecureBlobAttributeConverter.cipherService = cipherService;
    SecureBlobAttributeConverter.inputStreamConverter = inputStreamConverter;
    SecureBlobAttributeConverter.encryptionEnabled = encryptionEnabledProperty;
  }

  @Override
  public Blob convertToDatabaseColumn(final Blob attribute) {
    return encryptionEnabled ? encryptBlobAttribute(attribute) : attribute;
  }

  private Blob encryptBlobAttribute(final Blob attribute) {
    int pos = 1;
    Blob encrypteBlob = null;
    try {
      byte[] bytes = attribute.getBytes(pos, (int) attribute.length());
      InputStream i = cipherService.encrypt(new ByteArrayInputStream(
          bytes));
      byte[] encryptedOut = inputStreamConverter
          .convertInputStreamToByteArray(i);
      encrypteBlob = new SerialBlob(encryptedOut);
    } catch (SQLException ex) {
      throw new BlobCipherException("Exception while encrypting the blob type. ", ex);
    }
    return encrypteBlob;
  }

  @Override
  public Blob convertToEntityAttribute(final Blob dbData) {
    return encryptionEnabled ? decryptDbBlob(dbData) : dbData;
  }

  private Blob decryptDbBlob(final Blob dbData) {
    int pos = 1;
    byte[] bytes = null;
    Blob decryptedBlob = null;
    try {
      bytes = dbData.getBytes(pos, (int) dbData.length());
      InputStream decryptedStream = cipherService
          .decrypt(new ByteArrayInputStream(bytes));
      byte[] decryptedData = null;
      decryptedData = inputStreamConverter
          .convertInputStreamToByteArray(decryptedStream);
      decryptedBlob = new SerialBlob(decryptedData);
    } catch (SQLException ex) {
      throw new BlobCipherException(
          "Exception while decrypting the blob type. ", ex);
    }
    return decryptedBlob;
  }
}
