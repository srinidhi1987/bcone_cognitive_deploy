package com.automationanywhere.cognitive.filemanager.service.contract;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.FileProcessing.ExportDetail;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;
import com.automationanywhere.cognitive.filemanager.validation.contract.RequestValidation;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Jemin.Shah on 8/2/2017.
 */
public abstract class FileExportService
{
    @Autowired
    FileBlobService fileBlobService;

    @Autowired
    IOService ioService;

    @Autowired
    ProjectCachingService projectCachingService;

    @Autowired
    RequestValidation requestValidation;

    private AALogger log = AALogger.create(this.getClass());

    protected abstract String buildOutputPath();

    protected abstract boolean validate();

    public void export(ExportDetail exportDetail) throws DataAccessLayerException, SQLException, IOException {
        String fileId = exportDetail.getFileId();
        List<FileBlobs> fileBlobsList =  fileBlobService.getFileBlob(fileId);
        FileBlobs fileBlob = fileBlobsList.stream().findFirst().get();

        if(validate())
        {
            ioService.createFile(this.buildOutputPath(),
                    fileBlob.getFileStream().getBytes(1,(int)fileBlob.getFileStream().length()));
            log.debug("File Exported : " + exportDetail.getFileId());
        }
    }

    protected String getProjectName(String organizationId, String projectId)
    {
        try
        {
            return projectCachingService.get(projectId).getName();
        }
        catch (ProjectNotFoundException ex)
        {
            ProjectDetail projectDetail = requestValidation.validateProjectId(organizationId, projectId);

            if(projectDetail == null)
            {
                throw new ProjectNotFoundException("Learning Instance not found");
            }

            projectCachingService.put(projectDetail);

            return projectDetail.getName();
        }
    }

    protected void removeCache(String projectId)
    {
        projectCachingService.delete(projectId);
    }
}
