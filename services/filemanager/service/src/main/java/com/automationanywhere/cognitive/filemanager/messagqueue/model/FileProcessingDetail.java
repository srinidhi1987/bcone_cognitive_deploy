package com.automationanywhere.cognitive.filemanager.messagqueue.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileProcessingDetail
{
    private String organizationId;
    private String projectId;
    private String documentId;
    private String layoutId;
    private String categoryId;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
