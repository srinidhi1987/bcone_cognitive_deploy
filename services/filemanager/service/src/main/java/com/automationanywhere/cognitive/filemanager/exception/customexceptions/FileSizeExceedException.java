package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 26-04-2017.
 */
public class FileSizeExceedException extends RuntimeException {
    
    private static final long serialVersionUID = 7467956827005047929L;
    
    public FileSizeExceedException(String message) {
        super(message);
    }
}