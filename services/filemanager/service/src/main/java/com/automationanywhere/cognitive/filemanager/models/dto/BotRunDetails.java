package com.automationanywhere.cognitive.filemanager.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by msundell on 5/7/17
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BotRunDetails {

  private String id;
  private String runningMode;
  private int totalFiles;
  private int processedDocumentCount;
  private int failedDocumentCount;
  private int passedDocumentCount;
  private double documentProcessingAccuracy;
  private int totalFieldCount;
  private int passedFieldCount;
  private int failedFieldCount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getRunningMode() {
    return runningMode;
  }

  public void setRunningMode(String runningMode) {
    this.runningMode = runningMode;
  }

  public int getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(int totalFiles) {
    this.totalFiles = totalFiles;
  }

  public int getProcessedDocumentCount() {
    return processedDocumentCount;
  }

  public void setProcessedDocumentCount(int processedDocumentCount) {
    this.processedDocumentCount = processedDocumentCount;
  }

  public int getFailedDocumentCount() {
    return failedDocumentCount;
  }

  public void setFailedDocumentCount(int failedDocumentCount) {
    this.failedDocumentCount = failedDocumentCount;
  }

  public int getPassedDocumentCount() {
    return passedDocumentCount;
  }

  public void setPassedDocumentCount(int passedDocumentCount) {
    this.passedDocumentCount = passedDocumentCount;
  }

  public double getDocumentProcessingAccuracy() {
    return documentProcessingAccuracy;
  }

  public void setDocumentProcessingAccuracy(double documentProcessingAccuracy) {
    this.documentProcessingAccuracy = documentProcessingAccuracy;
  }

  public int getTotalFieldCount() {
    return totalFieldCount;
  }

  public void setTotalFieldCount(int totalFieldCount) {
    this.totalFieldCount = totalFieldCount;
  }

  public int getPassedFieldCount() {
    return passedFieldCount;
  }

  public void setPassedFieldCount(int passedFieldCount) {
    this.passedFieldCount = passedFieldCount;
  }

  public int getFailedFieldCount() {
    return failedFieldCount;
  }

  public void setFailedFieldCount(int failedFieldCount) {
    this.failedFieldCount = failedFieldCount;
  }
}
