package com.automationanywhere.cognitive.filemanager.exception.impl;

import com.automationanywhere.cognitive.filemanager.exception.contract.ExceptionHandler;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.parser.contract.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 01-03-2017.
 */
public class ExceptionHandlerImpl implements ExceptionHandler
{
    AALogger log = AALogger.create(getClass());

    @Autowired
    JsonParser jsonParser;

    @Override
    public void handleDataAccessLayerException(Exception dataAccessLayerException, Request request, Response response)
    {
        //log.error(dataAccessLayerException.getMessage(), dataAccessLayerException);
        log.error(dataAccessLayerException.getMessage(), dataAccessLayerException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        String exceptionMessage = dataAccessLayerException.getMessage();
        customResponse.setErrors((exceptionMessage!=null && !exceptionMessage.isEmpty())? exceptionMessage : "Internal Server Error.");
        response.status(500);
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleBadRequestException(Exception badRequestException, Request request, Response response) {
        //log.error(dataAccessLayerException.getMessage(), dataAccessLayerException);
        log.error(badRequestException.getMessage(), badRequestException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        String exceptionMessage = badRequestException.getMessage();
        customResponse.setErrors((exceptionMessage!=null && !exceptionMessage.isEmpty())? exceptionMessage : "Bad Request.");
        response.status(400);
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleDataNotFoundException(Exception dataNotFoundException, Request request, Response response)
    {
        log.error( dataNotFoundException.getMessage(), dataNotFoundException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(404);
        String exceptionMessage = dataNotFoundException.getMessage();
        customResponse.setErrors((exceptionMessage!=null && !exceptionMessage.isEmpty())? exceptionMessage : "Data is not available");
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }
    @Override
    public void handleInvalidFileFormatException(Exception invalidFileFormatException, Request request, Response response)
    {
        log.error( invalidFileFormatException.getMessage(), invalidFileFormatException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(415);
        customResponse.setErrors(invalidFileFormatException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleFileSizeExceedException(Exception fileSizeExceedException, Request request, Response response) {
        log.error( fileSizeExceedException.getMessage(), fileSizeExceedException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(413);
        customResponse.setErrors(fileSizeExceedException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleFileNameLengthExceedException(Exception fileNameLengthExceedException, Request request, Response response) {
        log.error( fileNameLengthExceedException.getMessage(), fileNameLengthExceedException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(414);
        customResponse.setErrors(fileNameLengthExceedException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleProjectDeletedException(Exception projectDeletedException, Request request, Response response) {
        log.error( projectDeletedException.getMessage(), projectDeletedException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(410);
        customResponse.setErrors(projectDeletedException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleProjectNotException(Exception projectNotFoundException, Request request, Response response) {
        log.error( projectNotFoundException.getMessage(), projectNotFoundException);
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(404);
        customResponse.setErrors(projectNotFoundException.getMessage());
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }

    @Override
    public void handleException(Exception exception, Request request, Response response)
    {
        log.error(exception.getMessage(),exception );
        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(false);
        response.status(500);
        customResponse.setErrors("Internal Server Error.");
        response.body(jsonParser.getResponse(customResponse));
        response.type("application/json");
    }
}
