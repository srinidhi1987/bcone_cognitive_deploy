package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Mayur.Panchal on 07-12-2016.
 */

@Entity
@Table(name = "ClassificationReport")
public class ClassificationReport extends EntityModelBase implements Serializable/*<FileDetails>*/
{
    private static final long serialVersionUID = -8380046344216645012L;
    
    @JsonProperty("documentid")
    @Column(name = "documentId")
    @Id
    private String documentId;

    @Id
    @JsonProperty("fieldId")
    @Column(name = "fieldId")
    private String fieldId;
    @JsonProperty("aliasName")
    @Column(name = "aliasName")
    private String aliasName;
    @JsonProperty("isFound")
    @Column(name = "isFound")
    private boolean isFound;

    public String getDocumentId()
    {
        return this.documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public boolean isFound() {
        return isFound;
    }

    public void setFound(boolean found) {
        isFound = found;
    }
}