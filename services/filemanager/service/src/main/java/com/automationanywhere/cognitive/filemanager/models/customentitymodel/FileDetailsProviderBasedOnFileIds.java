package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;

/**
 * Created by Mukesh.Methaniya on 21-07-2017.
 */
public class FileDetailsProviderBasedOnFileIds extends CustomEntityModelBase<FileDetails> {

    AALogger log = AALogger.create(this.getClass());

    private Collection<String> fileIds;
    public FileDetailsProviderBasedOnFileIds(Collection<String> fileIds) {
        this.fileIds=fileIds;
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {

        Map<String,Object> inputParam = new HashMap<>();
        inputParam.put("fileIds",this.fileIds);

        queryString.setLength(0);
        String query="Select \n "+
                "fileId  \n "+
                ",projectId \n "+
                ",fileName \n "+
                ",fileLocation \n "+
                ",fileSize \n "+
                ",fileHeight \n "+
                ",fileWidth \n "+
                ",format \n "+
                ",processed \n "+
                ",classificationId \n "+
                ",uploadrequestId \n "+
                ",layoutId \n "+
                ",isProduction \n "+
                " from FileDetails where fileId in (:fileIds)";
        queryString.append(query);
        log.debug("Query to get FileDetails of provided FileIds:" + queryString);
        log.exit();
        return inputParam;
    }

    @Override
    public List<FileDetails> objectToClass(List<Object> result) {

       List<FileDetails> list = new ArrayList<>();
        for (int j = 0; j < result.size(); j++) {
            Object[] obj1 = (Object[]) result.get(j);
            FileDetails fileDetails= new FileDetails();
            fileDetails.setFileId(obj1[0].toString());
            fileDetails.setProjectId(obj1[1].toString());
            fileDetails.setFileName(obj1[2].toString());
            fileDetails.setLocation(obj1[3].toString());
            fileDetails.setSize((long)obj1[4]);
            fileDetails.setHeight((int)obj1[5]);
            fileDetails.setWidth((int)obj1[6]);
            fileDetails.setFormat(obj1[7].toString());
            fileDetails.setIsProcessed((boolean)obj1[8]);
            fileDetails.setClassificationId(obj1[9].toString());
            fileDetails.setuploadrequestid(obj1[10].toString());
            fileDetails.setLayoutId(obj1[11].toString());
            fileDetails.setIsProduction((boolean)obj1[12]);

            list.add(fileDetails);
        }

        return list;
    }
}
