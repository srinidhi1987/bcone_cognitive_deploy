package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class MQConnectionFailureException extends RuntimeException{
    
    private static final long serialVersionUID = -9188939077050654766L;

    public MQConnectionFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}
