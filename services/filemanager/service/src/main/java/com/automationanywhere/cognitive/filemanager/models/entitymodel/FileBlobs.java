package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.sql.rowset.serial.SerialBlob;

import com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters.SecureBlobAttributeConverter;
import com.automationanywhere.cognitive.filemanager.models.helpder.BlobSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import spark.utils.IOUtils;

/**
 * Created by Mayur.Panchal on 07-12-2016.
 */
@Entity
@Table(name = "FileBlobs")
public class FileBlobs extends EntityModelBase {
    @Id
    @Column(name = "fileid")
    @JsonProperty("fileId")
    private String fileId;



    public static enum Property {
        fileId,fileBlob
    };

    public String getFileId()
    {
        return this.fileId;
    }
    public void setFileId(String id)
    {
        this.fileId = id;
    }

    /*@Column(name = "fileblob")
    @JsonProperty("fileBlob")
    private byte[] fileBlob;
    public void setFileStream(byte[] fileStream)
    {
        this.fileBlob = fileStream;
    }
    //@Type(type="org.hibernate.type.BinaryType")
    public byte[] getFileStream()
    {
        return this.fileBlob;
    }*/

    @Column(name = "fileblob")
    @JsonProperty("fileBlob")
    @JsonSerialize(using = BlobSerializer.class)
    @Convert(converter = SecureBlobAttributeConverter.class)
    private Blob fileBlob;
    public void setFileStream(InputStream fileStream) throws IOException, SQLException {
        this.fileBlob = new SerialBlob(IOUtils.toByteArray(fileStream));
    }

    public void setFileStream(Blob fileBlob) {
        this.fileBlob = fileBlob;
    }

    public Blob getFileStream() {
        return this.fileBlob;
    }

    /*@Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {
        return null;
    }*/
}
