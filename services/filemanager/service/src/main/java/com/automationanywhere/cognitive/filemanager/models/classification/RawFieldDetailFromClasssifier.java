package com.automationanywhere.cognitive.filemanager.models.classification;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;

/**
 * Created by keval.sheth on 24-01-2017.
 */
public class RawFieldDetailFromClasssifier {
    @JsonProperty("fieldId")
    @Column(name = "fieldId")
    private String fieldId;
    @JsonProperty("aliasName")
    @Column(name = "aliasName")
    private String aliasName;
    @JsonProperty("isFound")
    @Column(name = "isFound")
    private boolean isFound;

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public boolean isFound() {
        return isFound;
    }

    public void setFound(boolean found) {
        isFound = found;
    }
}
