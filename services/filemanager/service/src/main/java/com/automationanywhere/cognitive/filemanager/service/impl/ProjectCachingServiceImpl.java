package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.service.contract.ProjectCachingService;
import com.automationanywhere.cognitive.filemanager.validation.contract.RequestValidation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public class ProjectCachingServiceImpl implements ProjectCachingService
{
    AALogger log = AALogger.create(this.getClass());

    @Autowired
    RequestValidation requestValidation;

    private Map<String, ProjectDetail> projectDetailMap;

    public ProjectCachingServiceImpl()
    {
        this.projectDetailMap = new HashMap<String, ProjectDetail>();
    }

    @Override
    public ProjectDetail get(String projectId) throws ProjectNotFoundException
    {
        log.entry();

        if(!projectDetailMap.containsKey(projectId))
        {
            throw new ProjectNotFoundException("Learning instance is not found.");
        }

        log.debug("Cache detail found for project id : " + projectId);
        log.exit();

        return projectDetailMap.get(projectId);
    }

    @Override
    public void put(ProjectDetail projectDetail)
    {
        if(projectDetailMap.containsKey(projectDetail.getId()))
        {
            return;
        }

        projectDetailMap.put(projectDetail.getId(), projectDetail);
    }

    @Override
    public void delete(String projectId)
    {
        log.entry();

        if(projectDetailMap.containsKey(projectId))
        {
            log.debug("Cache detail found for project id : " + projectId);
            projectDetailMap.remove(projectId);
            log.debug("Cache detail removed for project id : " + projectId);
        }

        log.exit();
    }
}
