package com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.automationanywhere.cognitive.common.security.DefaultCipherService;

/**
 * @author shweta.thakur
 */
@Component
@Converter
public class SecureStringAttributeConverter implements AttributeConverter<String, String> {

  private static DefaultCipherService cipherService;
  private static InputStreamConverter inputStreamConverter;
  private static boolean encryptionEnabled = false;

  @Autowired
  public void initCipherService(final DefaultCipherService cipherService,
      final InputStreamConverter inputStreamConverter,
      @Value("${EncryptionEnabled:false}") boolean encryptionEnabledProperty)
      throws IOException {
    SecureStringAttributeConverter.cipherService = cipherService;
    SecureStringAttributeConverter.inputStreamConverter = inputStreamConverter;
    SecureStringAttributeConverter.encryptionEnabled = encryptionEnabledProperty;
  }

  @Override
  public String convertToDatabaseColumn(final String attribute) {
    return encryptionEnabled ? encryptAttribute(attribute) : attribute;
  }

  private String encryptAttribute(final String attribute) {
    InputStream encryptedStream = cipherService
        .encrypt(new ByteArrayInputStream(attribute.getBytes()));
    byte[] enc = null;
    enc = inputStreamConverter.convertInputStreamToByteArray(encryptedStream);
    return DatatypeConverter.printBase64Binary(enc);
  }

  @Override
  public String convertToEntityAttribute(final String dbData) {
    return encryptionEnabled ? decryptDbData(dbData) : dbData;
  }

  private String decryptDbData(final String dbData) {
    String decString = null;
    try {
      InputStream is = cipherService
          .decrypt(new ByteArrayInputStream(DatatypeConverter.parseBase64Binary(dbData)));
      decString = inputStreamConverter.convertInputStreamToString(is);
    } catch (Exception ex) {
      //TODO: This is a temporary code,needs to be removed when complete user story is delivered. This will help in debugging for now.
      decString = "THIS STRING IS NOT ENCRYPTED";
    }
    return decString;
  }
}
