package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

public class DependentServiceConnectionFailureException extends RuntimeException{
    
    private static final long serialVersionUID = -7243614405931641230L;

    public DependentServiceConnectionFailureException() {
        super();
    }

    public DependentServiceConnectionFailureException(String message) {
        super(message);
    }

}