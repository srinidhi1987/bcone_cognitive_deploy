package com.automationanywhere.cognitive.filemanager.adapter;

import com.automationanywhere.cognitive.filemanager.models.dto.VisionbotData;
import java.util.List;
/**
 * Created by msundell on 5/7/17
 */
public interface VisionbotAdapter {
  List<VisionbotData> getVisionbotData(String organizationId);
  List<VisionbotData> getVisionbotData(String organizationId, String projectId);
  void testConnection();
}
