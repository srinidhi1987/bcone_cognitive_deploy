package com.automationanywhere.cognitive.filemanager.health.connections;

import java.util.ArrayList;
import java.util.List;

import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueuePublisher;
/**
 * @author shweta.thakur
 * This class checks the MQ connection health
 */
public class MQCheckConnection implements HealthCheckConnection{

    private MessageQueuePublisher queueService;
    
    public MQCheckConnection(MessageQueuePublisher queueService) {
        super();
        this.queueService = queueService;
    }

    @Override
    public void checkConnection() {
        queueService.testConnection();
    }

    @Override
    public List<ServiceConnectivity> checkConnectionForService() {
        //return empty list
        return new ArrayList<ServiceConnectivity>();
    }

}
