package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryStagingFilesStpComparator implements Comparator<Category>, Serializable {
    
    private static final long serialVersionUID = -8359588163372781525L;
    private final ComparisonOrder order;

    public CategoryStagingFilesStpComparator(ComparisonOrder order) {
        this.order = order;
    }

    @Override
    public int compare(Category c1, Category c2) {
        // If a guarantee can be made that the category detail is always present then the defensive programming below could be removed
        if (c1.getStagingFileDetails() == null && c2.getStagingFileDetails() == null) {
            return 0;
        } else if (c1.getStagingFileDetails() != null && c2.getStagingFileDetails() == null) {
            return -1 * order.getDirection();
        } else if (c1.getStagingFileDetails() == null && c2.getStagingFileDetails() != null) {
            return 1 * order.getDirection();
        } else if (c1.getStagingFileDetails().getTotalSTPCount() == c2.getStagingFileDetails().getTotalSTPCount()) {
            return 0;
        }
        return ((c1.getStagingFileDetails().getTotalSTPCount() < c2.getStagingFileDetails().getTotalSTPCount()) ? -1 : 1) * order.getDirection();
    }
}