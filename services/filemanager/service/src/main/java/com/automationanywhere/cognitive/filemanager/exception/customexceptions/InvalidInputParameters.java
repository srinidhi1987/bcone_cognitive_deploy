package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 14-02-2017.
 */
public class InvalidInputParameters extends RuntimeException {
    
    private static final long serialVersionUID = 2237995865266709095L;
    
    public InvalidInputParameters(String message) {
        super(message);
    }
}
