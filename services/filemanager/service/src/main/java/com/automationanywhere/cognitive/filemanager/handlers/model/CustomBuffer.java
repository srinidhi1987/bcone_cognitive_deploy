package com.automationanywhere.cognitive.filemanager.handlers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.InputStream;

/**
 * Created by Mayur.Panchal on 04-03-2017.
 */
/*
public class CustomBuffer {
    @JsonProperty
    public String type;
    @JsonProperty
    public byte[] data;
}
*/

public class CustomBuffer {
    @JsonProperty
    public String type;
    @JsonProperty
    private InputStream data;

    public InputStream getData() {
           return this.data;
    }
    public void setData(InputStream d){ this.data = d; }
}
