package com.automationanywhere.cognitive.filemanager.consumer.contract;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectDeletedException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;

/**
 * Created by Mayur.Panchal on 20-02-2017.
 */
public interface ProjectServiceEndpointConsumer {
    ProjectDetail getProjectDetails(String orgId, String prjId) throws ProjectDeletedException, ProjectNotFoundException;
    void testConnection();    
}
