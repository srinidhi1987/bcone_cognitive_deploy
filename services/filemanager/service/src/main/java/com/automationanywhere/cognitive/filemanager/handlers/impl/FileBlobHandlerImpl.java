/**
 * Copyright (c) 2017 Automation Anywhere. All rights reserved.
 * <p>
 * This software is the proprietary information of Automation Anywhere. You
 * shall use it only in accordance with the terms of the license agreement you
 * entered into with Automation Anywhere.
 */
package com.automationanywhere.cognitive.filemanager.handlers.impl;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.handlers.contract.AbstractHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.FileBlobHandler;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;
import com.automationanywhere.cognitive.filemanager.service.contract.FileBlobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import spark.Request;
import spark.Response;

import java.util.List;

public class FileBlobHandlerImpl extends AbstractHandler implements FileBlobHandler {
    AALogger log = AALogger.create(getClass());

    @Autowired
    @Qualifier("fileBlobService")
    private FileBlobService fileBlobService;

    @Override
    public String getFileBlob(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);

        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            log.error("project not found");
            response.status(200);
            customResponse.setSuccess(false);
            customResponse.setErrors("project not found");
            return jsonParser.getResponse(customResponse);
        }
        String orgId = request.params(":orgid");
        String projectid = request.params(":prjid");
        String fileid = request.params(":fileId");
        log.debug(()-> "OrgId:" + orgId + " ProjectId:" + projectid + " FileId:" + fileid);
        if (fileid == null) {
            log.error("Fileid not found");
            customResponse.setSuccess(false);
            return jsonParser.getResponse(customResponse);
        }
        List<FileBlobs> result = fileBlobService.getFileBlob(fileid);
        if (result == null || result.isEmpty()) {
            log.error("FileBlob not found");
            throw new DataNotFoundException("FileBlob not found");
        }
        customResponse.setData(result);
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }
}
