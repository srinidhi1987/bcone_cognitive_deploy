package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.models.statistics.CountStatistic;
import com.automationanywhere.cognitive.filemanager.models.statistics.EnvironmentCountStatistic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class FileCountCalculator {
    AALogger log = AALogger.create(this.getClass());

    public List<CountStatistic> getCountStatistics(List<FileDetails> fileDetails) {
        log.entry();
        List<CountStatistic> countStatistics = new ArrayList<>();

        for (FileDetails fileDetail : fileDetails) {
            CountStatistic fileCategory = getCategory(fileDetail, countStatistics);
            fileCategory.getFileDetailsList().add(fileDetail);
        }
        log.trace(" Preparing FileDetails List ..");
        for (CountStatistic countStatistic : countStatistics) {
            List<FileDetails> fileDetailsList = countStatistic.getFileDetailsList();
            countStatistic.setFileCount(fileDetailsList.size());
            EnvironmentCountStatistic stagingCountStatistics = new EnvironmentCountStatistic();
            stagingCountStatistics.setTotalCount((int) fileDetailsList.stream().filter(p -> !p.getIsProduction()).count());
            stagingCountStatistics.setUnprocessedCount((int) fileDetailsList.stream().filter(p -> p.getIsProcessed() == 0 && !p.getIsProduction()).count());

            EnvironmentCountStatistic productionCountStatistics = new EnvironmentCountStatistic();
            productionCountStatistics.setTotalCount((int) fileDetailsList.stream().filter(p -> p.getIsProduction()).count());
            productionCountStatistics.setUnprocessedCount((int) fileDetailsList.stream().filter(p -> p.getIsProcessed() == 0 && p.getIsProduction()).count());

            countStatistic.setProductionFileCount(productionCountStatistics);
            countStatistic.setStagingFileCount(stagingCountStatistics);
        }
        log.trace(" Done Preparing FileDetails List ..");
        log.exit();
        return countStatistics;
    }

    private CountStatistic getCategory(FileDetails fileDetail, List<CountStatistic> fileCategoryList) {
        log.entry();
        List<CountStatistic> filterList = fileCategoryList
                .stream()
                .filter(p -> p.getCategoryId().equals(fileDetail.getClassificationId())
                        && p.getProjectId().equals(fileDetail.getProjectId()))
                .collect(Collectors.toList());

        if (filterList.size() > 0) return filterList.get(0);

        CountStatistic newCategory = new CountStatistic();
        newCategory.setCategoryId(fileDetail.getClassificationId());
        newCategory.setProjectId(fileDetail.getProjectId());
        newCategory.setFileDetailsList(new ArrayList<>());
        fileCategoryList.add(newCategory);
        log.exit();
        return newCategory;
    }
}
