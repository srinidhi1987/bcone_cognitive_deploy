package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Jemin.Shah on 6/29/2017.
 */
public class ProjectDeletedException extends RuntimeException{
    
    private static final long serialVersionUID = 1964247670984052926L;
    
    public ProjectDeletedException(String message) {
        super(message);
    }
}
