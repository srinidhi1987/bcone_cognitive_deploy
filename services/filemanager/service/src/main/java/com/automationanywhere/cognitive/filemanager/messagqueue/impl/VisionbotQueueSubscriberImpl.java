package com.automationanywhere.cognitive.filemanager.messagqueue.impl;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueueSubscriber;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.VisionbotUpdateDetail;
import com.automationanywhere.cognitive.filemanager.service.contract.FileDetailsService;
import com.rabbitmq.client.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.ConnectException;

import static com.automationanywhere.cognitive.filemanager.util.JsonUtil.fromJsonString;

/**
 * Created by Jemin.Shah on 3/20/2017.
 */
public class VisionbotQueueSubscriberImpl implements MessageQueueSubscriber
{
    AALogger log = AALogger.create(this.getClass());

    ConnectionFactory factory;
    Connection connection;
    Channel channel;
    String queueName;

    @Autowired
    private FileDetailsService fileDetailsService;

    public VisionbotQueueSubscriberImpl(String queue, String host, String username, String password,
        String virtualHost, Integer port)
    {
        try
        {
            queueName = queue;
            factory = new ConnectionFactory();
            factory.setHost(host);
            factory.setUsername(username);
            factory.setPassword(password);
            factory.setVirtualHost(virtualHost);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setPort(port);
            boolean connectionSuccess = false;
            while(!connectionSuccess) {
                try {
                    connection = factory.newConnection();
                    connectionSuccess = true;
                    log.debug("Debug: Connection successful.");
                }
                catch (ConnectException ex)
                {
                    log.error(ex.getMessage());
                    log.debug("Debug: Connection failed. Retrying after 1 second...");
                    Thread.sleep(1000);
                }
            }
            channel = connection.createChannel();
            channel.queueDeclare(queueName, true, false, false, null);
            channel.basicQos(1, false);
            
            final Consumer consumer = new DefaultConsumer(channel)
            {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");

                    log.debug("Received message : '" + message + "'");
                    try
                    {
                        receiveMessage(message);
                    }
                    catch (Exception ex)
                    {
                        log.error(ex.getMessage());
                    }
                    finally{
                        channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                }
            };

            boolean autoAck = false;
            channel.basicConsume(queueName, autoAck, consumer);
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage());
            return;
        }
        finally
        {
            log.exit();
        }
    }

    @Override
    public void receiveMessage(String message)
    {
    	log.entry(" Message :" + message);
        VisionbotUpdateDetail visionbotUpdateDetail = fromJsonString(message, VisionbotUpdateDetail.class);
        if(visionbotUpdateDetail == null)
            return;

        try
        {
            fileDetailsService.processUnprocessedFiles(
                    visionbotUpdateDetail.getOrganizationId(),
                    visionbotUpdateDetail.getProjectId(),
                    visionbotUpdateDetail.getCategoryId());
        }
        catch (DataAccessLayerException e) {
            log.error(e.getMessage());
        }
        log.exit();
    }

}
