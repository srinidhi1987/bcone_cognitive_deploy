package com.automationanywhere.cognitive.filemanager.messagqueue.contract;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * Created by Mayur.Panchal on 09-01-2017.
 */
public interface MessageQueuePublisher {
    void sendMessage(String message)throws IOException, TimeoutException;
    void testConnection();
    void closeChannel(Channel channel);
    void closeConnection(Connection connection);

}
