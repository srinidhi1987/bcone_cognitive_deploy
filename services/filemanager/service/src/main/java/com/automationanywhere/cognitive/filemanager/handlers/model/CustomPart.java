package com.automationanywhere.cognitive.filemanager.handlers.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mayur.Panchal on 04-03-2017.
 */
@JsonAutoDetect
public class CustomPart {
    @JsonProperty("filename")
    public String fileName;
    @JsonProperty("size")
    public long size;
    @JsonProperty("filedata")
    public CustomBuffer data;
    public void setfileName(String name) {  this.fileName = name; }
    public String getfileName() { return this.fileName; }
    public void setsize(long size) {  this.size = size; }
    public long getsize() { return this.size; }
    public void setdata(CustomBuffer data) {  this.data= data; }
    public CustomBuffer getdata() { return this.data; }
}
