package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryProductionFilesStpComparator implements Comparator<Category>, Serializable {
    
    private static final long serialVersionUID = -693336490092848996L;
    private final ComparisonOrder order;

    public CategoryProductionFilesStpComparator(ComparisonOrder order) {
        this.order = order;
    }

    @Override
    public int compare(Category c1, Category c2) {
        // If a guarantee can be made that the category detail is always present then the defensive programming below could be removed
        if (c1.getProductionFileDetails() == null && c2.getProductionFileDetails() == null) {
            return 0;
        } else if (c1.getProductionFileDetails() != null && c2.getProductionFileDetails() == null) {
            return -1 * order.getDirection();
        } else if (c1.getProductionFileDetails() == null && c2.getProductionFileDetails() != null) {
            return 1 * order.getDirection();
        } else if (c1.getProductionFileDetails().getTotalSTPCount() == c2.getProductionFileDetails().getTotalSTPCount()) {
            return 0;
        }
        return ((c1.getProductionFileDetails().getTotalSTPCount() < c2.getProductionFileDetails().getTotalSTPCount()) ? -1 : 1) * order.getDirection();
    }
}