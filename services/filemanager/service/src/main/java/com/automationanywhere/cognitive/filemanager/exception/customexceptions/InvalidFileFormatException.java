package com.automationanywhere.cognitive.filemanager.exception.customexceptions;

/**
 * Created by Mayur.Panchal on 26-04-2017.
 */
public class InvalidFileFormatException extends RuntimeException {
    
    private static final long serialVersionUID = 8750183688349420330L;
    
    public InvalidFileFormatException(String message) {
        super(message);
    }
}
