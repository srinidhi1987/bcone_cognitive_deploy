package com.automationanywhere.cognitive.filemanager.models.classification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Yogesh.Savalia on 27-01-2017.
 */
public class CategoryDetail {
    @JsonProperty("id")
    public String id;
    @JsonProperty("noOfDocuments")
    public long noOfDocuments;
    @JsonProperty("priority")
    public int priority;
    @JsonIgnore
    public float allFieldsAvg;
    @JsonProperty("index")
    public double index;
    @JsonIgnore
    public float PercentageOfDocuments;
    @JsonProperty("allFieldDetail")
    public List<FieldDetail> allFieldDetail;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDetail that = (CategoryDetail) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
