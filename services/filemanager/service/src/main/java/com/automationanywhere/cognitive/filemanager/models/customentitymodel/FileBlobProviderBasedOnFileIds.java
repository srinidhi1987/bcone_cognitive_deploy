package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;

/**
 * Created by Mukesh.Methaniya on 21-07-2017.
 */
public class FileBlobProviderBasedOnFileIds extends CustomEntityModelBase<FileBlobs> {

   AALogger log = AALogger.create(this.getClass());

    private Collection<String> fileIds;
    public FileBlobProviderBasedOnFileIds(Collection<String> fileIds) {
        this.fileIds=fileIds;
    }

    @Override
    public Map<String, Object> getCustomQuery(StringBuilder queryString) {

        Map<String,Object> inputParam = new HashMap<>();
        inputParam.put("fileIds",this.fileIds);

        queryString.setLength(0);
        String query="Select \n "+
                "fileId  \n "+
                ",fileBlob \n" +
                " from FileBlobs where fileId in (:fileIds)";
        queryString.append(query);
        log.debug( "Query to get FileBlob of provided FileIds:" + queryString );
        log.exit();
        return inputParam;
    }

    @Override
    public List<FileBlobs> objectToClass(List<Object> result) {
        List<FileBlobs> list = new ArrayList<>();

        for (int j = 0; j < result.size(); j++) {
            Object[] obj1 = (Object[]) result.get(j);
            FileBlobs fileBlobs= new FileBlobs();
            fileBlobs.setFileId(obj1[0].toString());
            fileBlobs.setFileStream((Blob) obj1[1]);
            list.add(fileBlobs);
        }
        return list;
    }
}
