package com.automationanywhere.cognitive.filemanager.handlers.contract;

import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.filemanager.parser.contract.JsonParser;
import com.automationanywhere.cognitive.filemanager.validation.contract.RequestValidation;

/**
 * Created by Mayur.Panchal on 09-12-2016.
 */
public abstract class AbstractHandler {
    @Autowired
    protected RequestValidation requestValidation;
    @Autowired
    protected JsonParser jsonParser;


}
