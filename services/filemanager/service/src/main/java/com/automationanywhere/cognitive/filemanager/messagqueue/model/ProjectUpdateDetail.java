package com.automationanywhere.cognitive.filemanager.messagqueue.model;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
public class ProjectUpdateDetail
{
    private String projectId;
    private Environment environment;
    private String organizationId;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
