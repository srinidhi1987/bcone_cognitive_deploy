package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by msundell on 4/27/17
 */
public class FileCount {
  @JsonProperty("totalCount")
  private int totalCount;

  @JsonProperty("totalSTPCount")
  private int totalSTPCount;

  @JsonProperty("unprocessedCount")
  private int unprocessedCount;

  public int getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(int totalCount) {
    this.totalCount = totalCount;
  }

  public int getUnprocessedCount() {
    return unprocessedCount;
  }

  public void setUnprocessedCount(int unprocessedCount) {
    this.unprocessedCount = unprocessedCount;
  }

  public int getTotalSTPCount() {
    return totalSTPCount;
  }

  public void setTotalSTPCount(int totalSTPCount) {
    this.totalSTPCount = totalSTPCount;
  }
}
