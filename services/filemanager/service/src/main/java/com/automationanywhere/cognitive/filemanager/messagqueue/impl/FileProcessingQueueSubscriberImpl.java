package com.automationanywhere.cognitive.filemanager.messagqueue.impl;

import static com.automationanywhere.cognitive.filemanager.util.JsonUtil.fromJsonString;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueueSubscriber;
import com.automationanywhere.cognitive.filemanager.messagqueue.model.FileProcessingDetail;
import com.automationanywhere.cognitive.filemanager.models.FileProcessing.ExportDetail;
import com.automationanywhere.cognitive.filemanager.service.contract.FileExportService;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.net.ConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Created by Jemin.Shah on 3/17/2017.
 */
public class FileProcessingQueueSubscriberImpl implements MessageQueueSubscriber
{
    AALogger log = AALogger.create(this.getClass());

    ConnectionFactory factory;
    Connection connection;
    Channel channel;
    String queueName;

    private static final String EXCHANGE_NAME = "iqbot_mq_exchange";
    private static final String TOPIC_NAME = "FileProcessing.Unprocessed.VisionBotNotFound";
    private static final String EXCHANGE_TOPIC = "topic";

    @Autowired
    @Qualifier("unprocessedFileExportService")
    private FileExportService fileExportService;

    public FileProcessingQueueSubscriberImpl(String queue, String host, String username,
        String password, String virtualHost, Integer port) {
        log.entry();
        try {
            queueName = queue;
            factory = new ConnectionFactory();
            factory.setHost(host);
            factory.setUsername(username);
            factory.setPassword(password);
            factory.setVirtualHost(virtualHost);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setPort(port);

            boolean connectionSuccess = false;
            while (!connectionSuccess) {
                try {
                    connection = factory.newConnection();
                    connectionSuccess = true;
                    log.debug("Debug: Connection successful.");
                } catch (ConnectException ex) {
                    log.error(ex.getMessage(), ex);
                    log.debug("Debug: Connection failed. Retrying after 1 second...");
                    Thread.sleep(1000);
                }
            }
            channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, EXCHANGE_TOPIC, true, false, null);
            channel.queueDeclare(queueName, true, false, false, null);
            channel.queueBind(queueName, EXCHANGE_NAME, TOPIC_NAME);
            channel.basicQos(1, false);

            final Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");

                    log.debug("Received message : '" + message + "'");

                    receiveMessage(message);
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            };

            channel.basicConsume(queueName, false, consumer);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return;
        } finally {
            log.exit();
        }
    }

    @Override
    public void receiveMessage(String message) {
    	log.entry();
    	log.debug("Message Recieved : " + message);

    	try {
            FileProcessingDetail fileProcessingDetailForExport = fromJsonString(message, FileProcessingDetail.class);

            ExportDetail exportDetail = new ExportDetail();
            exportDetail.setOrganizationId(fileProcessingDetailForExport.getOrganizationId());
            exportDetail.setProjectId(fileProcessingDetailForExport.getProjectId());
            exportDetail.setFileId(fileProcessingDetailForExport.getDocumentId());

            fileExportService.export(exportDetail);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }

        log.exit();
    }
}
