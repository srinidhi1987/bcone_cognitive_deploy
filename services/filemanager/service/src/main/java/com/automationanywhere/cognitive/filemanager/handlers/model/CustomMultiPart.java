package com.automationanywhere.cognitive.filemanager.handlers.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mayur.Panchal on 04-03-2017.
 */
@JsonAutoDetect
public class CustomMultiPart {
    @JsonProperty("data")
    public List<CustomPart> data;
    public void setdata(List<CustomPart> data) { this.data = data;}
    public List<CustomPart> getdata() { return this.data;}
}
