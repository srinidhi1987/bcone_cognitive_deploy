package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.CompareOperator;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.MapValueParameter;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.SegmentedDocumentDetail;
import com.automationanywhere.cognitive.filemanager.service.contract.SegmentedDocumentManagementService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Jemin.Shah on 6/21/2017.
 */
public class SegmentedDocumentManagementServiceImpl implements SegmentedDocumentManagementService {
    AALogger log = AALogger.create(this.getClass());

    @Autowired
    private CommonDataAccessLayer commonDataAccessLayer;

    @Override
    public String GetSegmentedDocument(String fileId) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        if (fileId == null || fileId == "")
            return null;

        log.debug("File Id: " + fileId);
        SegmentedDocumentDetail segmentedDocumentDetail = new SegmentedDocumentDetail();
        segmentedDocumentDetail.setFileId(fileId);
        segmentedDocumentDetail.WhereParam.put("fileId", new MapValueParameter(fileId, CompareOperator.equal));

	@SuppressWarnings("unchecked")        List<SegmentedDocumentDetail> segmentedDocumentDetailList = commonDataAccessLayer.SelectRows(segmentedDocumentDetail);
        if (segmentedDocumentDetailList == null || segmentedDocumentDetailList.size() == 0) {
            log.error("Segmented document is not found.");
            throw new DataNotFoundException("Segmented document is not found.");
        }
        log.exit();
        return segmentedDocumentDetailList.stream().findFirst().get().getSegmentedDocument();
    }

    @Override
    public void SaveSegmentedDocument(String fileId, String segmentedDocument) throws DataAccessLayerException
    {
        log.entry();
        SegmentedDocumentDetail segmentedDocumentDetail = new SegmentedDocumentDetail();
        if(isSegmentedDocumentAvailable(fileId)) {
            segmentedDocumentDetail.SetParam.put("fileId",fileId);
            segmentedDocumentDetail.SetParam.put("segmentedDocument" , segmentedDocument);
            segmentedDocumentDetail.WhereParam.put("fileId", new MapValueParameter(fileId, CompareOperator.equal));
            @SuppressWarnings("unchecked")
            boolean count = commonDataAccessLayer.UpdateRow(segmentedDocumentDetail);
            log.trace("Segmented Document for "+ fileId + " is updated");
        }
        else {
            segmentedDocumentDetail.setFileId(fileId);
            segmentedDocumentDetail.setSegmentedDocument(segmentedDocument);
            @SuppressWarnings("unchecked")
            boolean count = commonDataAccessLayer.InsertRow(segmentedDocumentDetail);
            log.trace("Segmented Document for "+ fileId +" is inserted");
        }
        log.exit();
    }

    @Override
    public void deleteSegmentedData(String fileId) throws DataAccessLayerException {
        log.entry();
        log.debug(() -> String.format("[Cleaning Data] [SegmentedDocumentDetails] [File Id = %s]",fileId));

        if(isSegmentedDocumentAvailable(fileId)) {
            SegmentedDocumentDetail segmentedData = new SegmentedDocumentDetail();
            segmentedData.WhereParam.put(FileDetails.Property.fileId.toString(), new MapValueParameter(fileId, CompareOperator.equal));
            @SuppressWarnings("unchecked")
            boolean count = commonDataAccessLayer.DeleteRows(segmentedData);
        }
        log.exit();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void deleteAllSegmentedData() throws DataAccessLayerException {
        log.entry();
        log.debug("[Cleaning Data] [SegmentedDocumentDetails][All Segmented Data]");

        String queryString = "delete from SegmentedDocumentDetail where " +
                "fileId in (select fileId from FileDetails where lower(format) != :format)";
        Map<String, Object> inputParam = new HashMap<String, Object>();
        inputParam.put("format", "pdf");

        commonDataAccessLayer.executeDataManipulationQuery(queryString, inputParam);

        log.exit();
    }

    private Boolean isSegmentedDocumentAvailable(String fileId) throws DataAccessLayerException {
        SegmentedDocumentDetail segmentedDocumentDetail = new SegmentedDocumentDetail();
        segmentedDocumentDetail.setFileId(fileId);
        segmentedDocumentDetail.WhereParam.put("fileId", new MapValueParameter(fileId, CompareOperator.equal));
        @SuppressWarnings("unchecked")
        List<SegmentedDocumentDetail> segmentedDocumentDetailsList = commonDataAccessLayer.SelectRows(segmentedDocumentDetail);

        return segmentedDocumentDetailsList != null && segmentedDocumentDetailsList.size() != 0;
    }


}
