package com.automationanywhere.cognitive.filemanager.models.helpder;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by Mayur.Panchal on 18-03-2017.
 */
public class BlobSerializer extends JsonSerializer<Blob> {

    private static AALogger logger = AALogger.create(BlobSerializer.class);

    @Override
    public void serialize(Blob value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        try {
            gen.writeBinary(value.getBytes(1,(int)value.length()));
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }
}
