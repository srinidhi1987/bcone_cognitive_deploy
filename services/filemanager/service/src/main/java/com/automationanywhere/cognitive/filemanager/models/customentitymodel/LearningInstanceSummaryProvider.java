package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.LearningInstance.Summary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LearningInstanceSummaryProvider extends CustomEntityModelBase<LearningInstanceSummaryEntity> {

	private String projectId;


	@Override
	public Map<String, Object> getCustomQuery(StringBuilder queryString) {
		queryString.setLength(0);

		Map<String, Object> inputParam = new HashMap<String, Object>();

		String query = "select isProduction,count(distinct case when classificationid !='-1' and  classificationid !='' then classificationid else null end) AS totalGroups, count(case when classificationid ='-1' then classificationid else null end) AS documentsUnclassified , count(*) as totalFiles from FileDetails "
				+ "where projectId = :projectId " + "group by isProduction";

		inputParam.put("projectId", this.getProjectId());
		queryString.append(query);

		return inputParam;

	}

	@Override
	public List<LearningInstanceSummaryEntity> objectToClass(List<Object> result) {
		List<LearningInstanceSummaryEntity> learningInstanceSummaryEntities = new ArrayList<>();

		LearningInstanceSummaryEntity learningInstanceSummaryEntity = new LearningInstanceSummaryEntity();
   /*
   *  setting default values for summary(staging and production),
   *  so that at last we will have the structure of object having their default values(if in any case we do not have some data.)
   * */
    learningInstanceSummaryEntity.setStagingFileSummary(new Summary());
    learningInstanceSummaryEntity.setProductionFileSummary(new Summary());

      for (int j = 0; j < result.size(); j++) {
        Object[] objectData = (Object[]) result.get(j);
        boolean isProduction = (boolean) objectData[0];

        if (isProduction) {
          Summary productionFileSummary = learningInstanceSummaryEntity.getProductionFileSummary();
					fillSummaryData(productionFileSummary,objectData);

        } else {
          Summary stagingFileSummary = learningInstanceSummaryEntity.getStagingFileSummary();
					fillSummaryData(stagingFileSummary,objectData);
        }
      }

    learningInstanceSummaryEntities.add(learningInstanceSummaryEntity);
		return learningInstanceSummaryEntities;

	}

	private void fillSummaryData(Summary summary,Object[] objectData){
    long totalGroups = (long) objectData[1];
    long documentsUnclassified = (long) objectData[2];
    long totalFiles = (long) objectData[3];

	  summary.setTotalGroups(totalGroups);
		summary.setDocumentsUnclassified(documentsUnclassified);
		summary.setTotalFiles(totalFiles);
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectId() {
		return projectId;
	}

}
