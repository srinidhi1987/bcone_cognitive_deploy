package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryNameComparator implements Comparator<Category>, Serializable {
    
    private static final long serialVersionUID = -771257331624431076L;
    private final ComparisonOrder order;

    public CategoryNameComparator(ComparisonOrder order) {
        this.order = order;
    }

    @Override
    public int compare(Category c1, Category c2) {
        // If a guarantee can be made that the category detail is always present then the defensive programming below could be removed
        if (c1.getName() == null && c2.getName() == null) {
            return 0;
        } else if (c1.getName() != null && c2.getName() == null) {
            return -1 * order.getDirection();
        } else if (c1.getName() == null && c2.getName() != null) {
            return 1 * order.getDirection();
        }
        return c1.getName().compareTo(c2.getName()) * order.getDirection();
    }
}