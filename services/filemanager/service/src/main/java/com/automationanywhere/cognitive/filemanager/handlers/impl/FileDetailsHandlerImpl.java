package com.automationanywhere.cognitive.filemanager.handlers.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.automationanywhere.cognitive.common.logger.AALogger;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.BadRequestException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.handlers.contract.AbstractHandler;
import com.automationanywhere.cognitive.filemanager.handlers.contract.FileDetailsHandler;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.models.PatchRequest;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.LearningInstanceSummaryEntity;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Categories;
import com.automationanywhere.cognitive.filemanager.service.contract.FileDetailsService;
import com.automationanywhere.cognitive.filemanager.util.JsonUtil;

import spark.Request;
import spark.Response;

/**
 * Created by Mayur.Panchal on 23-02-2017.
 */
public class FileDetailsHandlerImpl extends AbstractHandler implements FileDetailsHandler {
    AALogger log = AALogger.create(getClass());

    @Autowired
    @Qualifier("fileDetailsService")
    private FileDetailsService fileDetailsService;

    @Override
    public String getCategoriesByProjectId(Request request, Response response)
            throws DataNotFoundException, DataAccessLayerException, BadRequestException {
        log.entry();

        String organizationID = request.params(":orgid");
        String projectID = request.params(":prjid");
        String offsetStr = request.queryParams("offset");
        Integer offset = (offsetStr != null) ? new Integer(offsetStr) : null;
        String limitStr = request.queryParams("limit");
        Integer limit = (limitStr != null) ? new Integer(limitStr) : null;
        String sort = request.queryParams("sort");
        log.debug(() -> String.format("organizationid: %s, projectid: %s, offsetStr: %s, limitStr: %s, " +
                " sort: %s", organizationID, projectID, offsetStr, limitStr, sort));
        response.status(200);

        CustomResponse customResponse = new CustomResponse();
        Categories categories = fileDetailsService.getCategoriesByProjectId(organizationID, projectID, offset, limit, sort);
        customResponse.setData(categories);
        customResponse.setSuccess(true);

        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String GetCategoryWiseFiles(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);

        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            log.error(" Project not found.");
            response.status(200);
            customResponse.setSuccess(false);
            customResponse.setErrors("project not found");
            return jsonParser.getResponse(customResponse);
        }

        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        String categoryID = request.params(":id");
        String environment = request.queryParams("environment");
        log.debug(()-> " OrgId:" + orgId + " ProjectId:" + projectID + " CategoryId:" + categoryID);
        response.status(200);
        customResponse.setData(fileDetailsService.getCategoryWiseFiles(projectID, categoryID, environment));
        customResponse.setSuccess(true);

        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String GetProjectWiseFiles(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        CustomResponse customResponse = new CustomResponse();

        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        log.debug(()-> "OrgId:" + orgId + " ProjectId:" + projectID);
        response.status(200);
        customResponse.setData(fileDetailsService.getProjectWiseFiles(projectID, "", false));
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String GetSpecificFile(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);

        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            log.error("project not found");
            response.status(200);
            customResponse.setSuccess(false);
            customResponse.setErrors("project not found");
            return jsonParser.getResponse(customResponse);
        }
        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        String categoryID = request.params(":id");
        String fileID = request.params(":fileId");
        log.debug(()-> " OrgId:" + orgId + " ProjectId:" + projectID + " CategoryId:" + categoryID + " FileId:" + fileID);
        response.status(200);
        customResponse.setData(fileDetailsService.getSpecificFile(fileID));
        customResponse.setSuccess(true);

        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String uploadResource(Request request, Response response) throws Exception {
        log.entry();
        String orgid = request.params(":orgid");
        String prjid = request.params(":prjid");
        String requestId = request.queryParams("requestid");
        String status = request.params(":isProduction");
        log.debug(()-> "OrgId:" + orgid + " ProjectId:" + prjid + " RequestId:" + requestId + " Production:" + status);
        CustomResponse customResponse = new CustomResponse();

        requestValidation.validateProjectId(orgid, prjid);

        boolean isProduction = false;
        boolean isValidUploadRequest = true;
        if (status != null) {
            switch (status) {
                case "1":
                    isProduction = true;
                    break;
                case "0":
                    isProduction = false;
                    break;
                default:
                    log.error("Invalid upload request.");
                    customResponse.setSuccess(false);
                    response.status(202);
                    customResponse.setErrors("Invalid URL");
                    isValidUploadRequest = false;
            }
        }

        if (isValidUploadRequest) {
            MultipartConfigElement multipartConfigElement = new MultipartConfigElement("/tmp");
            request.raw().setAttribute(org.eclipse.jetty.server.Request.__MULTIPART_CONFIG_ELEMENT, multipartConfigElement);
            Collection<Part> parts = request.raw().getParts();
            log.trace(" Parts received");
            fileDetailsService.uploadFile(requestId, parts, prjid, isProduction);
            for (Part part : parts) {
                part.delete();
            }
            parts.clear();
            response.status(200);
            HashMap<String, String> uploadResponse = new HashMap<>();
            uploadResponse.put("fileUploadJobId", requestId);
            customResponse.setData(uploadResponse);
            response.type("application/json");
            customResponse.setSuccess(true);
        }
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String DeleteSpecificFile(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        boolean isValid = requestValidation.validateRequest(request, response);

        CustomResponse customResponse = new CustomResponse();
        if (!isValid) {
            log.error("project not found");
            response.status(200);
            customResponse.setSuccess(false);
            customResponse.setErrors("project not found");
            return jsonParser.getResponse(customResponse);
        }

        String orgId = request.params(":orgid");
        String projectid = request.params(":prjid");
        String classificationid = request.params(":id");
        String fileid = request.params(":fileId");
        log.debug(()-> "OrgId:" + orgId + " ProjectId:" + projectid + " CategoryId:" + classificationid + " FileId:" + fileid);
        boolean isSuccess = fileDetailsService.deleteSpecificFile(projectid, classificationid, fileid);
        customResponse.setSuccess(isSuccess);
        customResponse.setData(null);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String patch(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        String fileid = request.params(":fileId");
        log.debug("FileId:" + fileid);
        PatchRequest[] patchRequest = JsonUtil.fromJsonString(request.body(), PatchRequest[].class);

        boolean isSuccess = fileDetailsService.updateFileDetail(fileid, patchRequest);

        CustomResponse customResponse = new CustomResponse();
        customResponse.setSuccess(isSuccess);
        customResponse.setData(null);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String getFileStatistics(Request request, Response response) throws DataNotFoundException, DataAccessLayerException {
        log.entry();
        CustomResponse customResponse = new CustomResponse();
        String orgId = request.params(":orgid");
        log.debug(()-> "OrgId:" + orgId);
        String prjId = null;
        //TODO: Refactor
        if (request.params().containsKey(":prjid")) prjId = request.params(":prjid");

        response.status(200);
        customResponse.setData(fileDetailsService.getCountStatistics(orgId, prjId));
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }

    @Override
    public String getStagingFilesIfNotAvailableCopyFromProduction(Request request, Response response) throws DataNotFoundException,DataAccessLayerException
    {
        log.entry();
        CustomResponse customResponse = new CustomResponse();

        String orgId = request.params(":orgid");
        String projectID = request.params(":prjid");
        String categoryID = request.params(":id");

        log.debug(" OrgId:" + orgId + " ProjectId:"+projectID + "CategoryId:" + categoryID);

        response.status(200);
        customResponse.setData(fileDetailsService.getStagingFilesIfNotAvailableCopyFromProduction(projectID,categoryID));
        customResponse.setSuccess(true);
        log.exit();
        return jsonParser.getResponse(customResponse);
    }
    @Override
    public String getLearningInstanceSummary(Request request, Response response)
            throws DataNotFoundException, DataAccessLayerException, BadRequestException {
        log.entry();

        String orgId = request.params(":orgid");
        String prjId = request.params(":prjid");

        boolean isValid = (orgId.trim().equals("") || prjId.trim().equals("")) ? false : true;

        CustomResponse customResponse = new CustomResponse();

        log.debug(() -> String.format("orgid: %s, prjid: %s", orgId, prjId));

        String jsonResponse;
        if (!isValid) {
            log.error("request is not valid");
            response.status(400);
            customResponse.setSuccess(false);
            customResponse
                    .setErrors("Request is not valid, please check if you are passing the right organization-id and project-id.");
            jsonResponse = jsonParser.getResponse(customResponse);
        } else {

            LearningInstanceSummaryEntity learningInstanceSummaryEntity = fileDetailsService.getLearningInstanceSummary(orgId, prjId);

            response.status(200);

            customResponse.setData(learningInstanceSummaryEntity);
            customResponse.setSuccess(true);
            log.exit();
            jsonResponse = jsonParser.getResponse(customResponse);

        }
        return jsonResponse;
    }

    @Override
    public String triggerReclassification(Request request, Response response) throws IOException, DataAccessLayerException, TimeoutException {
        log.entry();

        String orgId = request.params(":orgid");
        String prjId = request.params(":prjid");

        log.debug(() -> String.format("Reclassification triggered - OrgId: %s, ProjId: %s", orgId, prjId));

        boolean isValid = (orgId.trim().equals("") || prjId.trim().equals("")) ? false : true;

        CustomResponse customResponse = new CustomResponse();



        if (!isValid) {
            log.error("request is not valid");
            response.status(400);
            customResponse.setSuccess(false);
            customResponse
                    .setErrors("Request is not valid, please check if you are passing the right organization-id and project-id.");
            return jsonParser.getResponse(customResponse);
        } else {

            String documentList = fileDetailsService.triggerReclassification(orgId, prjId);

            response.status(200);

            customResponse.setData(documentList);
            customResponse.setSuccess(true);
            log.exit();
            return jsonParser.getResponse(customResponse);

        }
    }
}
