package com.automationanywhere.cognitive.filemanager.health.connections;

import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.filemanager.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.filemanager.consumer.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueuePublisher;

public class HealthCheckConnectionFactoryTest {

    @Mock
    private CommonDataAccessLayer<?, ?> commonDataAccessLayer;
    @Mock
    private ProjectServiceEndpointConsumer projectServiceEndPointConsumner;
    @Mock
    private VisionbotAdapter visionbotAdapter;
    @Mock
    private MessageQueuePublisher classificationQueueService;
    @Mock
    DBCheckConnection dbCheckConnection;
    @Mock
    ServiceCheckConnection serviceCheckConnection;
    @Mock
    MQCheckConnection mqCheckConnection;
    @InjectMocks
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    
    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testOfGetConnectionWithConnTypeDB(){
        
        //given
        //Mocks
       
        //when
        HealthCheckConnection checkConnection = healthCheckConnectionFactory.getConnection(ConnectionType.DB);
        
        //then
        assertThat(checkConnection).isInstanceOf(DBCheckConnection.class);
    }
    
    @Test
    public void testOfGetConnectionWithConnTypeMQ(){
        
        //given
        //Mocks
       
        //when
        HealthCheckConnection checkConnection = healthCheckConnectionFactory.getConnection(ConnectionType.MQ);
        
        //then
        assertThat(checkConnection).isInstanceOf(MQCheckConnection.class);
    }
    
    @Test
    public void testOfGetConnectionWithConnTypeSVC(){
        
        //given
        //Mocks
        
        //when
        HealthCheckConnection checkConnection = healthCheckConnectionFactory.getConnection(ConnectionType.SVC);
        
        //then
        assertThat(checkConnection).isInstanceOf(ServiceCheckConnection.class);
    }
}
