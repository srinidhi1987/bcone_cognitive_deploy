package com.automationanywhere.cognitive.filemanager.health.connections;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.filemanager.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.filemanager.consumer.contract.ProjectServiceEndpointConsumer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DependentServiceConnectionFailureException;

public class ServiceCheckConnectionTest {
    @Mock
    VisionbotAdapter visionBotAdapter;
    @Mock
    ProjectServiceEndpointConsumer projectManagerAdapter;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOfCheckConnectionForServiceWhenAllServiceConnectionSuccess() {

        // given
        List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity projectConn = new ServiceConnectivity("Project", HTTPStatusCode.OK);
        ServiceConnectivity visionBotConn = new ServiceConnectivity("VisionBot", HTTPStatusCode.OK);
        servicesListExpected.add(projectConn);
        servicesListExpected.add(visionBotConn);
        ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(projectManagerAdapter,visionBotAdapter);
        Mockito.doNothing().when(visionBotAdapter).testConnection();
        Mockito.doNothing().when(projectManagerAdapter).testConnection();

        // when
        List<ServiceConnectivity> servicesList = serviceCheckConnection.checkConnectionForService();

        int index = 0;
        // then
        for(ServiceConnectivity serviceConnectivity:servicesList){
            ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
            assertThat(serviceConnectivity.getHTTPStatus()).isEqualTo(expectedServiceConnectivity.getHTTPStatus());
            assertThat(serviceConnectivity.getServiceName()).isEqualTo(expectedServiceConnectivity.getServiceName());
            index++;
        }


    }

    @Test
    public void testOfCheckConnectionForServiceWhenAllServiceConnectionFails() {

        // given
        List<ServiceConnectivity> servicesListExpected = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity projectConn = new ServiceConnectivity("Project", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        ServiceConnectivity visionBotConn = new ServiceConnectivity("VisionBot", HTTPStatusCode.INTERNAL_SERVER_ERROR);
        servicesListExpected.add(projectConn);
        servicesListExpected.add(visionBotConn);
        ServiceCheckConnection serviceCheckConnection = new ServiceCheckConnection(projectManagerAdapter, visionBotAdapter);
        
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(visionBotAdapter)
                .testConnection();
        Mockito.doThrow(DependentServiceConnectionFailureException.class).when(projectManagerAdapter)
        .testConnection();

        // when
        List<ServiceConnectivity> servicesList = serviceCheckConnection.checkConnectionForService();

        int index = 0;
        // then
        for(ServiceConnectivity serviceConnectivity:servicesList){
            ServiceConnectivity expectedServiceConnectivity = servicesListExpected.get(index);
            assertThat(serviceConnectivity.getHTTPStatus()).isEqualTo(expectedServiceConnectivity.getHTTPStatus());
            assertThat(serviceConnectivity.getServiceName()).isEqualTo(expectedServiceConnectivity.getServiceName());
            index++;
        }

    }
}
