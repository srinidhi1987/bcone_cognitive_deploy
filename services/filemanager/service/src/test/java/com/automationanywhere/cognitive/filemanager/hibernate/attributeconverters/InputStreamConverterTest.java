package com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InputStreamConverterTest {

  public InputStreamConverterTest() {
    // TODO Auto-generated constructor stub
  }

  @Test
  public void testOfConvertInputStreamToString() {
    //Given
    String expectedResult = "TestString";
    InputStream is = new ByteArrayInputStream(expectedResult.getBytes());

    //When
    String actualResult = new InputStreamConverter().convertInputStreamToString(is);

    //then
    assertThat(actualResult).isEqualTo(expectedResult);
  }

  @Test
  public void testOfConvertInputStreamToBytes() {
    //Given
    byte[] expectedResult = "TestString".getBytes();
    InputStream is = new ByteArrayInputStream(expectedResult);

    //When
    byte[] actualResult = new InputStreamConverter().convertInputStreamToByteArray(is);

    //then
    assertThat(actualResult).isEqualTo(expectedResult);
  }
}
