package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.FileProcessing.ExportDetail;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileDetails;
import com.automationanywhere.cognitive.filemanager.service.contract.FileBlobService;
import com.automationanywhere.cognitive.filemanager.service.contract.FileDetailsService;
import com.automationanywhere.cognitive.filemanager.service.contract.IOService;
import com.automationanywhere.cognitive.filemanager.service.contract.ProjectCachingService;
import com.automationanywhere.cognitive.filemanager.validation.contract.RequestValidation;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class UnprocessedFileExportServiceImplTest
{
    @Mock
    FileBlobService mockFileBlobService;

    @Mock
    FileDetailsService mockFileDetailsService;

    @Mock
    ProjectCachingService mockProjectCachingService;

    @Mock
    IOService mockIOService;

    @Mock
    RequestValidation mockRequestValidation;

    @InjectMocks
    UnprocessedFileExportServiceImpl unprocessedFileExportService;

    @BeforeMethod
    public void setUp() throws Exception
    {
        unprocessedFileExportService = new UnprocessedFileExportServiceImpl("D:\\output");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ExportFileShouldBeSkippedWhenFileIsInStaging() throws DataAccessLayerException, IOException, SQLException {
        FileDetails fileDetails = getFileDetails();
        fileDetails.setIsProduction(false);
        List<FileDetails> fileDetailsList = new ArrayList<>();
        fileDetailsList.add(fileDetails);

        when(mockFileDetailsService.getSpecificFile(any())).thenReturn(fileDetailsList);
        when(mockFileBlobService.getFileBlob(any())).thenReturn(getFileBlobsList());
        when(mockProjectCachingService.get(any())).thenReturn(getProjectDetail());

        unprocessedFileExportService.export(getExportDetail());

        verify(mockFileDetailsService, times(1) ).getSpecificFile(any());
        verify(mockIOService, times(0) ).createFile(any(), any());
        verify(mockProjectCachingService, times(0) ).delete(Mockito.any());
    }

    @Test
    public void ExportFileWhenFileIsInProduction() throws DataAccessLayerException, IOException, SQLException {
        FileDetails fileDetails = getFileDetails();
        fileDetails.setIsProduction(true);
        List<FileDetails> fileDetailsList = new ArrayList<>();
        fileDetailsList.add(fileDetails);

        when(mockFileDetailsService.getSpecificFile(any())).thenReturn(fileDetailsList);
        when(mockFileBlobService.getFileBlob(any())).thenReturn(getFileBlobsList());
        when(mockProjectCachingService.get(any())).thenReturn(getProjectDetail());

        unprocessedFileExportService.export(getExportDetail());

        verify(mockFileDetailsService, times(1) ).getSpecificFile(any());
        verify(mockIOService, times(1) ).createFile(any(), any());
        verify(mockProjectCachingService, times(0) ).delete(Mockito.any());
    }

    @Test
    public void ExportFileReturnsWhenFileNotFound() throws DataAccessLayerException, IOException, SQLException
    {
        when(mockFileDetailsService.getSpecificFile(any())).thenThrow(new DataNotFoundException("Data not found"));
        when(mockFileBlobService.getFileBlob(any())).thenReturn(getFileBlobsList());
        when(mockProjectCachingService.get(any())).thenReturn(getProjectDetail());

        ExportDetail exportDetail = new ExportDetail();
        exportDetail.setFileId("test");
        exportDetail.setProjectId("testProject");
        unprocessedFileExportService.export(exportDetail);

        verify(mockProjectCachingService, times(1) ).delete(Mockito.any());
    }

    @Test
    public void UpdateCacheDuringFileExport() throws DataAccessLayerException, IOException, SQLException {
        when(mockFileDetailsService.getSpecificFile(any())).thenReturn(getFileDetailsList());
        when(mockFileBlobService.getFileBlob(any())).thenReturn(getFileBlobsList());
        when(mockProjectCachingService.get(any())).thenThrow(new ProjectNotFoundException("Project not found"));
        when(mockRequestValidation.validateProjectId(any(), any())).thenReturn(getProjectDetail());

        unprocessedFileExportService.export(getExportDetail());

        verify(mockProjectCachingService, times(1)).put(any());
        verify(mockIOService, times(1) ).createFile(any(), any());
    }

    @Test(expectedExceptions = ProjectNotFoundException.class)
    public void FileExportFailsWhenProjectNotFound()throws DataAccessLayerException, IOException, SQLException {
        when(mockFileDetailsService.getSpecificFile(any())).thenReturn(getFileDetailsList());
        when(mockFileBlobService.getFileBlob(any())).thenReturn(getFileBlobsList());
        when(mockProjectCachingService.get(any())).thenThrow(new ProjectNotFoundException("Project not found"));
        when(mockRequestValidation.validateProjectId(any(), any())).thenReturn(null);

        unprocessedFileExportService.export(getExportDetail());

        verify(mockProjectCachingService, times(0)).put(any());
        verify(mockIOService, times(0) ).createFile(any(), any());
    }


    List<FileDetails> getFileDetailsList()
    {
        List<FileDetails> fileDetailsList = new ArrayList<>();
        FileDetails fileDetails = getFileDetails();
        fileDetailsList.add(fileDetails);
        return fileDetailsList;
    }

    private FileDetails getFileDetails() {
        FileDetails fileDetails = new FileDetails();
        fileDetails.setFileId("test");
        fileDetails.setProjectId("testProject");
        fileDetails.setFileName("test.tiff");
        fileDetails.setIsProduction(true);
        fileDetails.setClassificationId("1");
        return fileDetails;
    }

    List<FileBlobs> getFileBlobsList() throws IOException, SQLException {
        List<FileBlobs> fileBlobsList = new ArrayList<>();
        FileBlobs fileBlobs = new FileBlobs();
        fileBlobs.setFileId("test");
        fileBlobs.setFileStream(new ByteArrayInputStream("test".getBytes()));
        fileBlobsList.add(fileBlobs);
        return fileBlobsList;
    }

    ProjectDetail getProjectDetail()
    {
        ProjectDetail projectDetail = new ProjectDetail();
        projectDetail.setId("testProject");
        projectDetail.setName("testProject");
        return projectDetail;
    }

    ExportDetail getExportDetail()
    {
        ExportDetail exportDetail = new ExportDetail();
        exportDetail.setFileId("test");
        exportDetail.setProjectId("testProject");
        exportDetail.setOrganizationId("1");
        return exportDetail;
    }
}