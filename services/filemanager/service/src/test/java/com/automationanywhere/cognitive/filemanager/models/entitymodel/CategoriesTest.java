package com.automationanywhere.cognitive.filemanager.models.entitymodel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Percentage.withPercentage;

import org.testng.annotations.Test;

import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;


public class CategoriesTest {
    private Categories categories;

    @Test
    public void testOfAddingItemsToListShouldBeSuccessful() throws Exception {
        // given
        Category c1 = new Category();
        c1.setName("c1");
        Category c2 = new Category();
        c2.setName("c2");

        // when
        categories = new Categories();
        categories.add(c1);
        categories.add(c2);

        // then
        assertThat(categories.getCategories()).isNotNull();
        assertThat(categories.getCategories().size()).isEqualTo(2);
        assertThat(categories.getOffset()).isEqualTo(0);
        assertThat(categories.getLimit()).isEqualTo(Categories.DEFAULT_LIMIT);
        assertThat(categories.getCategories().get(0).getName()).isEqualTo("c1");
        assertThat(categories.getCategories().get(1).getName()).isEqualTo("c2");
    }

    @Test
    public void testOfSortingItemsByPriorityShouldReturnTheListInDescendingOrder() throws Exception {
        // given
        categories = new Categories();

        Category c1 = new Category();
        c1.setName("c1");
        c1.setCategoryDetail(createCategoryDetail(0.2f));
        categories.add(c1);

        Category c2 = new Category();
        c2.setName("c2");
        c2.setCategoryDetail(null);
        categories.add(c2);

        Category c3 = new Category();
        c3.setName("c3");
        c3.setCategoryDetail(createCategoryDetail(0.1f));
        categories.add(c3);

        Category c4 = new Category();
        c4.setName("c4");
        c4.setCategoryDetail(createCategoryDetail(1.0f));
        categories.add(c4);

        Category c5 = new Category();
        c5.setName("c5");
        c5.setCategoryDetail(createCategoryDetail(0.8f));
        categories.add(c5);

        // when
        categories.sortBy("-index");

        // then
        assertThat(categories.getCategories()).isNotNull();
        assertThat(categories.getCategories().size()).isEqualTo(5);
        assertThat(categories.getOffset()).isEqualTo(0);
        assertThat(categories.getLimit()).isEqualTo(Categories.DEFAULT_LIMIT);

        assertThat(categories.getCategories().get(0).getName()).isEqualTo("c2");
        assertThat(categories.getCategories().get(0).getCategoryDetail()).isNull();

        assertThat(categories.getCategories().get(1).getName()).isEqualTo("c4");
        assertThat(categories.getCategories().get(1).getCategoryDetail().priority).isEqualTo(10);

        assertThat(categories.getCategories().get(2).getName()).isEqualTo("c5");
        assertThat(categories.getCategories().get(2).getCategoryDetail().priority).isEqualTo(8);

        assertThat(categories.getCategories().get(3).getName()).isEqualTo("c1");
        assertThat(categories.getCategories().get(3).getCategoryDetail().priority).isEqualTo(2);

        assertThat(categories.getCategories().get(4).getName()).isEqualTo("c3");
        assertThat(categories.getCategories().get(4).getCategoryDetail().priority).isEqualTo(1);
    }

    @Test
    public void testOfSortingItemsByIndexShouldReturnTheListInDescendingOrder() throws Exception {
        // given
        categories = new Categories();

        Category c1 = new Category();
        c1.setName("c1");
        c1.setCategoryDetail(createCategoryDetail(0.2f));
        categories.add(c1);

        Category c2 = new Category();
        c2.setName("c2");
        c2.setCategoryDetail(null);
        categories.add(c2);

        Category c3 = new Category();
        c3.setName("c3");
        c3.setCategoryDetail(createCategoryDetail(0.1f));
        categories.add(c3);

        Category c4 = new Category();
        c4.setName("c4");
        c4.setCategoryDetail(createCategoryDetail(1.0f));
        categories.add(c4);

        Category c5 = new Category();
        c5.setName("c5");
        c5.setCategoryDetail(createCategoryDetail(0.8f));
        categories.add(c5);

        // when
        categories.sortBy("-index");

        // then
        assertThat(categories.getCategories()).isNotNull();
        assertThat(categories.getCategories().size()).isEqualTo(5);
        assertThat(categories.getOffset()).isEqualTo(0);
        assertThat(categories.getLimit()).isEqualTo(Categories.DEFAULT_LIMIT);

        assertThat(categories.getCategories().get(0).getName()).isEqualTo("c2");
        assertThat(categories.getCategories().get(0).getCategoryDetail()).isNull();

        assertThat(categories.getCategories().get(1).getName()).isEqualTo("c4");
        assertThat(categories.getCategories().get(1).getCategoryDetail().index).isCloseTo(1.0d, withPercentage(0.0001));

        assertThat(categories.getCategories().get(2).getName()).isEqualTo("c5");
        assertThat(categories.getCategories().get(2).getCategoryDetail().index).isCloseTo(0.8d, withPercentage(0.0001));

        assertThat(categories.getCategories().get(3).getName()).isEqualTo("c1");
        assertThat(categories.getCategories().get(3).getCategoryDetail().index).isCloseTo(0.2d, withPercentage(0.0001));

        assertThat(categories.getCategories().get(4).getName()).isEqualTo("c3");
        assertThat(categories.getCategories().get(4).getCategoryDetail().index).isCloseTo(0.1d, withPercentage(0.0001));
    }

    @Test
    public void testOfPaginationShouldReturnAReducedListOfItems() throws Exception {
        // given
        categories = new Categories();

        for (int i = 25; i > 0; i--) {
            Category category = new Category();
            category.setName("name-" + i);
            category.setCategoryDetail(createCategoryDetail(i));
            categories.add(category);
        }

        int offset = 5;
        int limit = 15;

        // when
        categories.sortBy("-index");
        categories.applyPagination(offset, limit);

        // then
        assertThat(categories.getCategories()).isNotNull();
        assertThat(categories.getCategories().size()).isEqualTo(limit);
        assertThat(categories.getOffset()).isEqualTo(offset);
        assertThat(categories.getLimit()).isEqualTo(limit);
        assertThat(categories.getCategories().get(0).getName()).isEqualTo("name-20");
        assertThat(categories.getCategories().get(0).getCategoryDetail().index).isCloseTo(20, withPercentage(0.0001));
        assertThat(categories.getCategories().get(14).getName()).isEqualTo("name-6");
        assertThat(categories.getCategories().get(14).getCategoryDetail().index).isCloseTo(6, withPercentage(0.0001));
    }

    @Test
    public void testOfRequestOutsideRangeShouldReturnAnEmptyList() throws Exception {
        // given
        categories = new Categories();

        for (int i = 5; i > 0; i--) {
            Category category = new Category();
            category.setName("name-" + i);
            category.setCategoryDetail(createCategoryDetail(i));
            categories.add(category);
        }

        int offset = 15;
        int limit = 10;

        // when
        categories.sortBy("index");
        categories.applyPagination(offset, limit);

        // then
        assertThat(categories.getCategories()).isNotNull();
        assertThat(categories.getCategories().size()).isEqualTo(0);
        assertThat(categories.getOffset()).isEqualTo(offset);
        assertThat(categories.getLimit()).isEqualTo(limit);
    }

    @Test
    public void testOfRequestWithDefaultLimitShouldReturnAllData() throws Exception {
        // given
        categories = new Categories();

        for (int i = 5; i > 0; i--) {
            Category category = new Category();
            category.setName("name-" + i);
            category.setCategoryDetail(createCategoryDetail(i));
            categories.add(category);
        }

        int offset = 0;
        int limit = -1;

        // when
        categories.sortBy("index");
        categories.applyPagination(offset, limit);

        // then
        assertThat(categories.getCategories()).isNotNull();
        assertThat(categories.getCategories().size()).isEqualTo(5);
        assertThat(categories.getOffset()).isEqualTo(offset);
        assertThat(categories.getLimit()).isEqualTo(limit);
    }

    private CategoryDetail createCategoryDetail(float index) {
        CategoryDetail categoryDetail = new CategoryDetail();
        categoryDetail.priority = (int) Math.ceil(index*10);
        categoryDetail.index = index;
        return categoryDetail;
    }
}
