package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryIndexComparatorTest {
    private CategoryIndexComparator comparator;
    private final static int EQUAL_TO = 0;
    private final static int LESS_THAN = -1;
    private final static int GREATER_THAN = 1;

    @Test
    public void testOfIndexComparisonAscOrderWithNullsShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(null);

        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfIndexComparisonAscOrderWithCategoryDetailAndNullShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.ASCENDING);

        CategoryDetail cd1 = mock(CategoryDetail.class);
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfIndexComparisonDescOrderWithCategoryDetailAndNullShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.DESCENDING);

        CategoryDetail cd1 = mock(CategoryDetail.class);
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfIndexComparisonAscOrderWithNullAndCategoryDetailShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(null);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfIndexComparisonDescOrderWithNullAndCategoryDetailShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.DESCENDING);

        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(null);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfIndexComparisonAscOrderWithIdenticalIndexShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.ASCENDING);

        CategoryDetail cd1 = mock(CategoryDetail.class);
        cd1.index = 10.0F;
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        cd2.index = 10.0F;
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfIndexComparisonAscOrderWithLowHighIndexShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.ASCENDING);

        CategoryDetail cd1 = mock(CategoryDetail.class);
        cd1.index = 3.0F;
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        cd2.index = 7.0F;
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfIndexComparisonDescOrderWithLowHighIndexShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.DESCENDING);

        CategoryDetail cd1 = mock(CategoryDetail.class);
        cd1.index = 3.0F;
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        cd2.index = 7.0F;
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfIndexComparisonAscOrderWithHighLowIndexShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.ASCENDING);
        CategoryDetail cd1 = mock(CategoryDetail.class);
        cd1.index = 8.0F;
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        cd2.index = 2.0F;
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfIndexComparisonDescOrderWithHighLowIndexShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryIndexComparator(ComparisonOrder.DESCENDING);
        CategoryDetail cd1 = mock(CategoryDetail.class);
        cd1.index = 8.0F;
        Category c1 = mock(Category.class);
        when(c1.getCategoryDetail()).thenReturn(cd1);

        CategoryDetail cd2 = mock(CategoryDetail.class);
        cd2.index = 2.0F;
        Category c2 = mock(Category.class);
        when(c2.getCategoryDetail()).thenReturn(cd2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }
}
