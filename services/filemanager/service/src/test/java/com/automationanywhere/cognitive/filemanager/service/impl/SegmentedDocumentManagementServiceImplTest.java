package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.SegmentedDocumentDetail;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.configuration.injection.MockInjection;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class SegmentedDocumentManagementServiceImplTest {
    @Mock
    private CommonDataAccessLayer<EntityModelBase,CustomEntityModelBase> commonDataAccessLayer;

    @InjectMocks
    private SegmentedDocumentManagementServiceImpl segmentedDocumentManagementService;

    @BeforeTest
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void deleteAllSegmentedDataShouldCallDataAccessLayer() throws DataAccessLayerException
    {
        //when
        segmentedDocumentManagementService.deleteAllSegmentedData();

        //then
        Mockito.verify(commonDataAccessLayer, times(1)).executeDataManipulationQuery(Mockito.any(), Mockito.any());
    }

    @Test(expectedExceptions = DataAccessLayerException.class)
    public void deleteAllSegmentedDataShouldThrowDataAccessLayerException() throws DataAccessLayerException
    {
      //when
      doThrow(new DataAccessLayerException("ex")).when(commonDataAccessLayer).executeDataManipulationQuery(Mockito.any(), Mockito.any());

      //then
      segmentedDocumentManagementService.deleteAllSegmentedData();
    }

    @Test(expectedExceptions = DataAccessLayerException.class)
    public void deleteSegmentedDataShouldThrowDataAccessLayerException() throws DataAccessLayerException
    {
        //when
        when(commonDataAccessLayer.SelectRows(Mockito.any())).thenReturn(getSegmentedDocumentList());
        doThrow(new DataAccessLayerException("ex")).when(commonDataAccessLayer).DeleteRows(Mockito.any());

        //then
        segmentedDocumentManagementService.deleteSegmentedData(Mockito.any());
    }

    @Test
    public void deleteSegmentedDataShouldCallDataAccessLayer() throws DataAccessLayerException
    {
        //given
        when(commonDataAccessLayer.SelectRows(Mockito.any())).thenReturn(getSegmentedDocumentList());
        doReturn(true).when(commonDataAccessLayer).DeleteRows(Mockito.any());

        //when
        segmentedDocumentManagementService.deleteSegmentedData(Mockito.any());

        //then
        Mockito.verify(commonDataAccessLayer, times(1)).DeleteRows(any());
        Mockito.verify(commonDataAccessLayer, times(1)).SelectRows(any());
    }

    private SegmentedDocumentDetail getSegmentedDocument(String fileId)
    {
        SegmentedDocumentDetail segmentedDocumentDetail = new SegmentedDocumentDetail();
        segmentedDocumentDetail.setFileId(fileId);
        return segmentedDocumentDetail;
    }

    private List<Object> getSegmentedDocumentList()
    {
        List<Object> segmentedDocumentDetailList = new ArrayList<>();
        segmentedDocumentDetailList.add(getSegmentedDocument("123"));
        return segmentedDocumentDetailList;
    }

}
