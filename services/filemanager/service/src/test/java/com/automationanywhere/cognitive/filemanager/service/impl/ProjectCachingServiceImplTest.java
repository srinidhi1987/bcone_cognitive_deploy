package com.automationanywhere.cognitive.filemanager.service.impl;

import org.mockito.InjectMocks;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.filemanager.exception.customexceptions.ProjectNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;

public class ProjectCachingServiceImplTest
{
    @InjectMocks
    ProjectCachingServiceImpl projectCachingService;


    @BeforeMethod
    public void setup() throws Exception
    {
        projectCachingService = new ProjectCachingServiceImpl();
    }

    @Test
    public void GetProjectDetailFromCache()
    {
        projectCachingService.put(getProjectDetail());
        ProjectDetail projectDetail = projectCachingService.get("testProject");

        Assert.assertNotNull(projectDetail);
        Assert.assertEquals(projectDetail.getName(), getProjectDetail().getName());
    }

    @Test
    public void GetProjectDetailsThrowsExceptionWhenProjectNotFound()
    {
        try
        {
            projectCachingService.get("testProject");
        }
        catch (ProjectNotFoundException ex)
        {
            Assert.assertTrue(true);
        }
    }

    ProjectDetail getProjectDetail()
    {
        ProjectDetail projectDetail = new ProjectDetail();
        projectDetail.setId("testProject");
        projectDetail.setName("testProjectName");
        return projectDetail;
    }
}