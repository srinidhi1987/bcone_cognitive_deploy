package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileCount;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryProductionFilesUnprocessedComparatorTest {
    private CategoryProductionFilesUnprocessedComparator comparator;
    private final static int EQUAL_TO = 0;
    private final static int LESS_THAN = -1;
    private final static int GREATER_THAN = 1;

    @Test
    public void testOfCategoryProductionFilesUnprocessedAscOrderWithNullValuesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(null);

        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfCategoryProductionFilesUnprocessedAscOrderWithValueAndNullShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getUnprocessedCount()).thenReturn(1001);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(fc1);

        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryProductionFilesUnprocessedAscOrderWithNullAndValueShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(null);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getUnprocessedCount()).thenReturn(1002);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfAscendingCategoryProductionFilesUnprocessedAscOrderWithValuesShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getUnprocessedCount()).thenReturn(1005);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(fc1);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getUnprocessedCount()).thenReturn(1006);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfDescendingCategoryProductionFilesUnprocessedAscOrderWithValuesShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getUnprocessedCount()).thenReturn(1004);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(fc1);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getUnprocessedCount()).thenReturn(1003);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfIdenticalCategoryProductionFilesUnprocessedAscOrderWithIdenticalValuesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryProductionFilesUnprocessedComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getUnprocessedCount()).thenReturn(1023);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(null);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getUnprocessedCount()).thenReturn(1023);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }
}