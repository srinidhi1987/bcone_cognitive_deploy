package com.automationanywhere.cognitive.filemanager.health.connections;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;

public class DBCheckConnectionTest {

    @Test
    public void testOfCheckConnectionWhenDaoLayerReturnsObject() throws DataAccessLayerException{
        
        //given
        CommonDataAccessLayer<?, ?> commonDataAccessLayer = Mockito.mock(CommonDataAccessLayer.class);
        List<Object> expected = new ArrayList<Object>() ;
        Mockito.when(commonDataAccessLayer.CustomQueryExecute(Mockito.any(), Mockito.any()))
        .thenReturn(expected);
        
        //when
       
        DBCheckConnection dbCheckConnection = new DBCheckConnection(commonDataAccessLayer);
        dbCheckConnection.checkConnection();
        
        //then
        //there should not be any exception
    }
    
    @Test(expectedExceptions={DBConnectionFailureException.class})
    public void testOfCheckConnectionWhenDaoLayerThrowsException() throws DataAccessLayerException{
        
        //given
        CommonDataAccessLayer<?, ?> commonDataAccessLayer = Mockito.mock(CommonDataAccessLayer.class);
        
        //when
        Mockito.when(commonDataAccessLayer.CustomQueryExecute(Mockito.any(), Mockito.any()))
        .thenThrow(Exception.class);
        DBCheckConnection dbCheckConnection = new DBCheckConnection(commonDataAccessLayer);
        dbCheckConnection.checkConnection();
        
        //then
        //excpetion should be thrown
    }
}
