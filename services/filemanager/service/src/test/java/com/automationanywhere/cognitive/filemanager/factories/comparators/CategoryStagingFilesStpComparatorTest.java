package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileCount;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryStagingFilesStpComparatorTest {
    private CategoryStagingFilesStpComparator comparator;
    private final static int EQUAL_TO = 0;
    private final static int LESS_THAN = -1;
    private final static int GREATER_THAN = 1;

    @Test
    public void testOfCategoryStagingFilesStpWithAscOrderWithNullValuesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getStagingFileDetails()).thenReturn(null);

        Category c2 = mock(Category.class);
        when(c2.getStagingFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfCategoryStagingFilesStpWithAscOrderWithValueAndNullShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(311);
        Category c1 = mock(Category.class);
        when(c1.getStagingFileDetails()).thenReturn(fc1);

        Category c2 = mock(Category.class);
        when(c2.getStagingFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryStagingFilesStpWithAscOrderWithNullAndValueShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getStagingFileDetails()).thenReturn(null);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(312);
        Category c2 = mock(Category.class);
        when(c2.getStagingFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfCategoryStagingFilesStpWithAscOrderWithValuesShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(101);
        Category c1 = mock(Category.class);
        when(c1.getStagingFileDetails()).thenReturn(fc1);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(102);
        Category c2 = mock(Category.class);
        when(c2.getStagingFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryStagingFilesStpWithAscOrderWithValuesShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(104);
        Category c1 = mock(Category.class);
        when(c1.getStagingFileDetails()).thenReturn(fc1);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(103);
        Category c2 = mock(Category.class);
        when(c2.getStagingFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfCategoryStagingFilesStpWithAscOrderWithIdenticalValuesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryStagingFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(123);
        Category c1 = mock(Category.class);
        when(c1.getStagingFileDetails()).thenReturn(null);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(123);
        Category c2 = mock(Category.class);
        when(c2.getStagingFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }
}
