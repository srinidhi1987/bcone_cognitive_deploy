package com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters;

import static org.mockito.Mockito.times;

import java.io.IOException;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.mockito.Mockito;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.common.security.CipherException;
import com.automationanywhere.cognitive.common.security.DefaultCipherService;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.InputStreamConversionException;

public class SecureBlobAttributeConverterTest {

  public SecureBlobAttributeConverterTest() {
  }

  @Test
  public void testOfConvertToDatabaseColumnWhenEncryptionIsEnabled()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureBlobAttributeConverter
        .convertToDatabaseColumn(new SerialBlob(new SerialBlob("testattribute".getBytes())));

    //then
    Mockito.verify(cipherService, times(1)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  public void testOfConvertToDatabaseColumnWhenEncryptionIsDisabled()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, false);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureBlobAttributeConverter
        .convertToDatabaseColumn(new SerialBlob(new SerialBlob("testattribute".getBytes())));

    //then
    Mockito.verify(cipherService, times(0)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(0)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test(expectedExceptions = CipherException.class)
  public void testOfConvertToDatabaseColumnWhenExceptionInCipherServiceConversion()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    Mockito.doThrow(CipherException.class).when(cipherService).encrypt(Mockito.any());
    secureBlobAttributeConverter
        .convertToDatabaseColumn(new SerialBlob("testattribute".getBytes()));

    //then
    Mockito.verify(cipherService, times(1)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test(expectedExceptions = InputStreamConversionException.class)
  public void testOfConvertToDatabaseColumnWhenExceptionInEncryptedStreamConversion()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doThrow(InputStreamConversionException.class).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureBlobAttributeConverter
        .convertToDatabaseColumn(new SerialBlob("testattribute".getBytes()));

    //then
    Mockito.verify(cipherService, times(1)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test
  public void testOfConvertToEntityAttributeWhenEncryptionIsEnabled()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureBlobAttributeConverter
        .convertToEntityAttribute(new SerialBlob("testattribute".getBytes()));

    //then
    Mockito.verify(cipherService, times(1)).decrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test
  public void testOfConvertToEntityAttributeWhenEncryptionIsDisabled()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, false);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureBlobAttributeConverter
        .convertToEntityAttribute(new SerialBlob("testattribute".getBytes()));

    //then
    Mockito.verify(cipherService, times(0)).decrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(0)).convertInputStreamToByteArray(Mockito.any());
  }


  @Test(expectedExceptions = CipherException.class)
  //TODO will enable exception clause once the condition is removed in the code
  public void testOfConvertToEntityAttributeWhenExceptionInCipherServiceConversion()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doThrow(CipherException.class).when(cipherService).decrypt(Mockito.any());
    secureBlobAttributeConverter
        .convertToEntityAttribute(new SerialBlob("testattribute".getBytes()));

    //then
    //Mockito.verify(cipherService, times(1)).decrypt(Mockito.any());
    //Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToString(Mockito.any());
  }

  @Test(expectedExceptions = IOException.class)
  //TODO will enable exception clause once the condition is removed in the code
  public void testOfConvertToEntityAttributeWhenExceptionInEncryptedStreamConversion()
      throws SerialException, SQLException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureBlobAttributeConverter secureBlobAttributeConverter = new SecureBlobAttributeConverter();
    secureBlobAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doThrow(IOException.class).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureBlobAttributeConverter
        .convertToEntityAttribute(new SerialBlob("testattribute".getBytes()));

    //then
    Mockito.verify(cipherService, times(1)).decrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }
}
  
