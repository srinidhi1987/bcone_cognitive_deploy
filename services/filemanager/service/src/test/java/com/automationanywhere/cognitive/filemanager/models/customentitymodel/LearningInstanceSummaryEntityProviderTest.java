package com.automationanywhere.cognitive.filemanager.models.customentitymodel;

import com.automationanywhere.cognitive.filemanager.models.LearningInstance.Summary;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class LearningInstanceSummaryEntityProviderTest {
    private LearningInstanceSummaryProvider learningInstanceSummaryEntityProvider;

    @BeforeTest
    public void setUp() throws Exception {
        learningInstanceSummaryEntityProvider =new LearningInstanceSummaryProvider();
    }

    @Test
    public void getCustomQueryShouldReturnMapWhenValidInputParameters(){
        //given
        StringBuilder queryString=new StringBuilder();
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";

        //required
        Map<String, Object> inputParams=new HashMap<String,Object>();
        inputParams.put("projectId", projectId);

        learningInstanceSummaryEntityProvider.setProjectId(projectId);

        Map<String, Object> result= learningInstanceSummaryEntityProvider.getCustomQuery(queryString);

        Assert.assertEquals(projectId,result.get("projectId"));

    }

    @Test
    public void objectToClassShouldReturnListOfLearningInstanceSummaryWhenValidInputParameters(){
       //given
        List<LearningInstanceSummaryEntity> projectSummaryList = new ArrayList<>();
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";

        learningInstanceSummaryEntityProvider.setProjectId(projectId);

        //required
        List<Object> result=new ArrayList<>();
        result.add(new Object[]{false,5l,20l,30l});
        result.add(new Object[]{true,2l,18l,30l});

        List<LearningInstanceSummaryEntity> expectedInstanceSummaryEntityList=new ArrayList<>();
        Summary stagingSummary=new Summary(5,20,30);
        Summary productionSummary=new Summary(2,18,30);
        LearningInstanceSummaryEntity learningInstanceSummaryEntity=new LearningInstanceSummaryEntity();
        learningInstanceSummaryEntity.setStagingFileSummary(stagingSummary);
        learningInstanceSummaryEntity.setProductionFileSummary(productionSummary);

        expectedInstanceSummaryEntityList.add(learningInstanceSummaryEntity);

        List<LearningInstanceSummaryEntity> actualInstanceSummaryEntityList= learningInstanceSummaryEntityProvider.objectToClass(result);

        assertThat(actualInstanceSummaryEntityList).isNotNull();
        assertThat(actualInstanceSummaryEntityList.size()).isEqualTo(1);
        assertThat(actualInstanceSummaryEntityList.get(0)).isEqualToComparingFieldByFieldRecursively(expectedInstanceSummaryEntityList.get(0));
    }
}
