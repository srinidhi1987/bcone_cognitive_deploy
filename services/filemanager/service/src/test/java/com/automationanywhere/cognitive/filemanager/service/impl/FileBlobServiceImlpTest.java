package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileBlobs;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Created by Mayur.Panchal on 26-02-2017.
 */
public class FileBlobServiceImlpTest {
    @Mock
    private CommonDataAccessLayer<EntityModelBase, CustomEntityModelBase> mockFileServiceDal;
    @InjectMocks
    private FileBlobServiceImpl sut;

    @BeforeTest
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getFileBlobShouldReturnListForValidInputParameters() throws DataAccessLayerException,DataNotFoundException {
        //given
        String fileId= "aaa";

        FileBlobs fileBlob = new FileBlobs();
        List<Object> listblob = new ArrayList<>();
        listblob.add(fileBlob);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(listblob);

        //when
        List<FileBlobs> actualResult = sut.getFileBlob(fileId);

        assertEquals(listblob, actualResult);
    }

    @Test
    public void getFileBlobShouldReturnNullForValidInputParameters() throws DataAccessLayerException, DataNotFoundException {
        //given
        String fileId= "";
        List<Object> listblob = null;
        when(mockFileServiceDal.SelectRows(any())).thenReturn(listblob);

        //when
        List<FileBlobs> actualResult = sut.getFileBlob(fileId);

        //then
        assertEquals(listblob, actualResult);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void getFileBlobShouldThrowsExceptionWhenNoDataAvailable() throws DataAccessLayerException, DataNotFoundException {
        //given
        String fileId= "abc";
        List<Object> listblob = null;
        when(mockFileServiceDal.SelectRows(any())).thenReturn(listblob);

        //when
        List<FileBlobs> actualResult = sut.getFileBlob(fileId);

        //then
        assertEquals(listblob, actualResult);
    }

    @Test
    public void deleteFileBlobShouldReturnSuccessForValidInputParameters() throws  DataAccessLayerException
    {
        //given
        String fileId= "111";

        when(mockFileServiceDal.DeleteRows(any())).thenReturn(true);

        //when
        boolean actualResult = sut.deleteFileBlobs(fileId);

        assertEquals(true, actualResult);
    }
    @Test
    public void deleteFileBlobShouldReturnFalseForBlankInputParameters() throws DataAccessLayerException
    {
        //given
        String fileId= "";

        //when
        boolean actualResult = sut.deleteFileBlobs(fileId);

        assertEquals(false, actualResult);
    }

    @Test
    public void deleteFileBlobShouldReturnFalseForNullInputParameters() throws DataAccessLayerException
    {
        //given
        String fileId= null;

        //when
        boolean actualResult = sut.deleteFileBlobs(fileId);

        assertEquals(false, actualResult);
    }

}
