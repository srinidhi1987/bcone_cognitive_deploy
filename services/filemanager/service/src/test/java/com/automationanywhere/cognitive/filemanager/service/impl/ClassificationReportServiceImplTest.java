package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.models.classification.CategoryDetail;
import com.automationanywhere.cognitive.filemanager.models.classification.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.filemanager.models.classification.FieldDetail;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ClassificationWiseCountEntity;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.ProjectWiseCountEntity;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.ClassificationReport;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.EntityModelBase;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by Mayur.Panchal on 26-02-2017.
 */
public class ClassificationReportServiceImplTest {
    @Mock
    private CommonDataAccessLayer<EntityModelBase,CustomEntityModelBase> commonDataAccessLayer;
    @InjectMocks
    private ClassificationReportServiceImpl sut;
    @Mock
    private ClassificationReportServiceImpl mockClassificationReportService;
    @BeforeTest
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void insertClassificationDetailShouldReturnTruelForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //Arrange
        boolean expectedResult = true;
        ClassificationReport classificationReport = new ClassificationReport();
        when(commonDataAccessLayer.InsertRow(any())).thenReturn(expectedResult);
        boolean actualResult = sut.insertClassificationDetail(classificationReport);
        Assert.assertEquals(actualResult,expectedResult);
    }
    @Test
    public void insertClassificationDetailShouldReturnFalseForInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //Arrange
        boolean expectedResult = false;
        ClassificationReport classificationReport = null;
        when(commonDataAccessLayer.InsertRow(any())).thenReturn(expectedResult);
        boolean actualResult = sut.insertClassificationDetail(classificationReport);
        Assert.assertEquals(actualResult,expectedResult);
    }
    @Test
    public void getTotalFileCountReturnCountForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        String projectId = "aaa";
        long expectedCount = 10;
        ProjectWiseCountEntity projectWiseCount = new ProjectWiseCountEntity();
        projectWiseCount.setProjectId(projectId);
        projectWiseCount.setCount(expectedCount);
        List<Object> expectedResult = new ArrayList<>();
        expectedResult.add(projectWiseCount);
        when(commonDataAccessLayer.ExecuteCustomEntityQuery(any())).thenReturn(expectedResult);
        long actualCount = sut.getTotalFileCount(projectId);
        Assert.assertEquals(expectedCount,actualCount);
    }
    @Test
    public void getTotalFileCountShouldReturnCountForInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        String projectId = "";
        long expectedCount = 0;
        ProjectWiseCountEntity projectWiseCount = new ProjectWiseCountEntity();
        projectWiseCount.setProjectId(projectId);
        projectWiseCount.setCount(expectedCount);
        List<Object> expectedResult = new ArrayList<>();
        expectedResult.add(projectWiseCount);
        when(commonDataAccessLayer.ExecuteCustomEntityQuery(any())).thenReturn(expectedResult);
        long actualCount = sut.getTotalFileCount(projectId);
        Assert.assertEquals(expectedCount,actualCount);
    }
    @Test
    public void getTotalFileCountShouldReturnZeroCountForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        String projectId = "aaa";
        long expectedCount = 0;
        when(commonDataAccessLayer.ExecuteCustomEntityQuery(any())).thenReturn(null);
        long actualCount = sut.getTotalFileCount(projectId);
        Assert.assertEquals(expectedCount,actualCount);
    }
    @Test
    public void getCategoryDetailShouldReturnCategoryDetailForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        /*String projectId = "111";
        String classificationId = "11";
        CategoryDetail currentcategory = new CategoryDetail();
        currentcategory.allFieldDetail = new ArrayList<>();

        List<Object> fieldIdList = new ArrayList<>();
        fieldIdList.add("1");
        when(commonDataAccessLayer.CustomQueryExecute(any(),any())).thenReturn(fieldIdList);
        FieldDetail fieldDetail = new FieldDetail();
        fieldDetail.fieldId = "1";
        List<Object> objectList = new ArrayList<>();
        Object x = 5;
        objectList.add("5");
        when(commonDataAccessLayer.CustomQueryExecute(any(),any())).thenReturn(objectList);

        currentcategory.allFieldDetail.add(fieldDetail);

        CategoryDetail result = sut.getCategoryDetail(projectId,classificationId);
        assertEquals(currentcategory,result);*/

    }
    @Test
    public void getFieldDetailShouldReturnDataForValidInputParameters() throws DataAccessLayerException, DataNotFoundException {
        String projectId = "111";
        String classificationId = "1";
        String fieldId = "1";

        FieldDetail fieldDetail = new FieldDetail();
        fieldDetail.fieldId = fieldId;
        fieldDetail.foundInTotalDocs = 5;
        List<Object> objectList = new ArrayList<>();
        long x = 5;
        objectList.add(x);
        when(commonDataAccessLayer.CustomQueryExecute(any(),any())).thenReturn(objectList);

        FieldDetail result = sut.getFieldDetail(projectId,classificationId,fieldId);
        Assert.assertEquals(fieldDetail,result);
    }
    @Test
    public void getFieldDetailShouldReturnNullForInvalidInputParameters() throws DataAccessLayerException, DataNotFoundException {
        //given
        String projectId = "111";
        String classificationId = "1";
        String fieldId = "";

        FieldDetail fieldDetail = new FieldDetail();
        fieldDetail.fieldId = fieldId;
        fieldDetail.foundInTotalDocs = 5;
        List<Object> objectList = new ArrayList<>();
        long x = 5;
        objectList.add(x);
        when(commonDataAccessLayer.CustomQueryExecute(any(),any())).thenReturn(objectList);
        //when
        FieldDetail result = sut.getFieldDetail(projectId,classificationId,fieldId);
        //then
        Assert.assertEquals(null,result);
    }
    @Test
    public void getAllCategoryAndItsDocsShouldReturnDataForValidInputParameters() throws DataAccessLayerException, DataNotFoundException {
        //given
        String projectId = "111";
        ClassificationWiseCountEntity classificationWiseCountEntity = new ClassificationWiseCountEntity();
        classificationWiseCountEntity.setProjectId(projectId);
        classificationWiseCountEntity.setCount(1);

        List<Object> classificationWiseCountEntities = new ArrayList<>();
        classificationWiseCountEntities.add(classificationWiseCountEntity);
        when(commonDataAccessLayer.ExecuteCustomEntityQuery(any())).thenReturn(classificationWiseCountEntities);
        //when
        List<ClassificationWiseCountEntity> result = sut.getAllCategoryAndItsDocs(projectId);
        //then
        Assert.assertEquals(classificationWiseCountEntity,result.get(0) );
    }

    @Test
    public void getAllCategoryAndItsDocsShouldReturnNUllForInvalidInputParameters() throws DataAccessLayerException, DataNotFoundException {
        //given
        String projectId = "";
        ClassificationWiseCountEntity classificationWiseCountEntity = new ClassificationWiseCountEntity();
        classificationWiseCountEntity.setProjectId(projectId);
        classificationWiseCountEntity.setCount(1);

        List<Object> classificationWiseCountEntities = new ArrayList<>();
        classificationWiseCountEntities.add(classificationWiseCountEntity);
        when(commonDataAccessLayer.ExecuteCustomEntityQuery(any())).thenReturn(classificationWiseCountEntities);
        //when
        List<ClassificationWiseCountEntity> result = sut.getAllCategoryAndItsDocs(projectId);
        //then
        Assert.assertEquals(null,result);
    }

    @Test
    public void calculateReportDetailShouldReturnDataForValidInputParameters()
    {
        //given
        ClassificationAnalysisReport classificationAnalysisReport = new ClassificationAnalysisReport();
        classificationAnalysisReport.projectId = "111";
        classificationAnalysisReport.totalDocuments = 2;

        classificationAnalysisReport.allCategoryDetail = new ArrayList<>();

        CategoryDetail categoryDetail = new CategoryDetail();
        categoryDetail.noOfDocuments = 2;
        categoryDetail.allFieldDetail = new ArrayList<>();
        FieldDetail fieldDetail = new FieldDetail();
        fieldDetail.fieldId = "aaa";
        fieldDetail.foundInTotalDocs =1;
        fieldDetail.aliasName = "Invoice";
        fieldDetail.fieldName = "Invoice No";
        categoryDetail.allFieldDetail.add(fieldDetail);
        classificationAnalysisReport.allCategoryDetail.add(categoryDetail);
        //when
        ClassificationAnalysisReport result = sut.calculateReportDetail(classificationAnalysisReport);
        //then
        Assert.assertEquals(1.0d , result.allCategoryDetail.get(0).index);
        Assert.assertEquals(10 , result.allCategoryDetail.get(0).priority);
        Assert.assertEquals(0.0f ,  result.allCategoryDetail.get(0).PercentageOfDocuments);
    }
    @Test
    public void updateClassificationDetailShouldReturnTrueValidInputParameters() throws DataAccessLayerException {
        //given
        ClassificationReport classificationReport = new ClassificationReport();
        classificationReport.setDocumentId("aaa");
        classificationReport.setAliasName("AliasName");
        when(commonDataAccessLayer.UpdateRow(any())).thenReturn(true);
        //when
        boolean result = sut.updateClassificationDetail(classificationReport);
        //then
        Assert.assertTrue(result);
    }
    @Test
    public void updateClassificationDetailShouldReturnFalseForInvalidInputParameters() throws DataAccessLayerException {
        //given
        ClassificationReport classificationReport = new ClassificationReport();
        classificationReport.setDocumentId("aaa");
        classificationReport.setAliasName("AliasName");
        when(commonDataAccessLayer.UpdateRow(any())).thenReturn(true);
        //when
        boolean result = sut.updateClassificationDetail(null);
        //then
        Assert.assertFalse(result);
    }
}
