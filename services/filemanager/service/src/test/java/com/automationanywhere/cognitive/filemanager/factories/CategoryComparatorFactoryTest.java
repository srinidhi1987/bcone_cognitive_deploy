package com.automationanywhere.cognitive.filemanager.factories;

import com.automationanywhere.cognitive.filemanager.factories.comparators.*;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import org.testng.annotations.Test;

import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

public class CategoryComparatorFactoryTest {
    @Test
    public void testOfGettingComparatorByNonExistingShouldYieldNullValue() throws Exception {
        // given
        String sortSpecification = "non-existing-comparator";

        // when
        Comparator<Category> comparator = CategoryComparatorFactory.getComparator(sortSpecification);

        // then
        assertThat(comparator).isNull();
    }

    @Test
    public void testOfGettingComparatorByIndexShouldYieldCategoryIndexComparator() throws Exception {
        // given
        String sortSpecificationAsc = "index";
        String sortSpecificationDesc = "-index";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryIndexComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryIndexComparator.class);
    }

    @Test
    public void testOfGettingComparatorByCategoryNameShouldYieldCategoryNameComparator() throws Exception {
        // given
        String sortSpecificationAsc = "categoryName";
        String sortSpecificationDesc = "-categoryName";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryNameComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryNameComparator.class);
    }

    @Test
    public void testOfGettingComparatorByStagingFilesCountShouldYieldCategoryStagingFilesCountComparator() throws Exception {
        // given
        String sortSpecificationAsc = "stagingFilesCount";
        String sortSpecificationDesc = "-stagingFilesCount";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryStagingFilesCountComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryStagingFilesCountComparator.class);
    }

    @Test
    public void testOfGettingComparatorByStagingFilesStpShouldYieldCategoryStagingFilesStpComparator() throws Exception {
        // given
        String sortSpecificationAsc = "stagingFilesStp";
        String sortSpecificationDesc = "-stagingFilesStp";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryStagingFilesStpComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryStagingFilesStpComparator.class);
    }

    @Test
    public void testOfGettingComparatorByStagingFilesUnprocessedShouldYieldCategoryStagingFilesUnprocessedComparator() throws Exception {
        // given
        String sortSpecificationAsc = "stagingFilesUnproccessed";
        String sortSpecificationDesc = "-stagingFilesUnproccessed";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryStagingFilesUnprocessedComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryStagingFilesUnprocessedComparator.class);
    }

    @Test
    public void testOfGettingComparatorByProductionFilesCountShouldYieldCategoryProductionFilesCountComparator() throws Exception {
        // given
        String sortSpecificationAsc = "productionFilesCount";
        String sortSpecificationDesc = "-productionFilesCount";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryProductionFilesCountComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryProductionFilesCountComparator.class);
    }

    @Test
    public void testOfGettingComparatorByProductionFilesStpShouldYieldCategoryProductionFilesStpComparator() throws Exception {
        // given
        String sortSpecificationAsc = "productionFilesStp";
        String sortSpecificationDesc = "-productionFilesStp";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryProductionFilesStpComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryProductionFilesStpComparator.class);
    }

    @Test
    public void testOfGettingComparatorByProductionFilesUnprocessedShouldYieldCategoryProductionFilesUnprocessedComparator() throws Exception {
        // given
        String sortSpecificationAsc = "productionFilesUnproccessed";
        String sortSpecificationDesc = "-productionFilesUnproccessed";

        // when
        Comparator<Category> comparatorAsc = CategoryComparatorFactory.getComparator(sortSpecificationAsc);
        Comparator<Category> comparatorDesc = CategoryComparatorFactory.getComparator(sortSpecificationDesc);

        // then
        assertThat(comparatorAsc).isInstanceOf(CategoryProductionFilesUnprocessedComparator.class);
        assertThat(comparatorDesc).isInstanceOf(CategoryProductionFilesUnprocessedComparator.class);
    }
}
