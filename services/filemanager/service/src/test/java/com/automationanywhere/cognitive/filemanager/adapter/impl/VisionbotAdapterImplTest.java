package com.automationanywhere.cognitive.filemanager.adapter.impl;

import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.models.StandardResponse;
import com.automationanywhere.cognitive.filemanager.models.dto.VisionbotData;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
/**
 * Created by Mayur.Panchal on 02-06-2017.
 */
public class VisionbotAdapterImplTest {
    @Mock
    RestTemplateWrapper restTemplateWrapper;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private VisionbotAdapterImpl visionBotAdapterImpl;
    @BeforeTest
    public void setUp()
    {
        this.restTemplate = new RestTemplate();
        this.restTemplateWrapper = new RestTemplateWrapper(restTemplate);
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void getVisionbotDataReturnsDataForValidInputParamters()
    {
        //given
        String organizationId = "1";
        StandardResponse standardResponse = new StandardResponse();
        standardResponse.setSuccess(true);
        standardResponse.setError(null);
        VisionbotData[]  visionBotData= new VisionbotData[1];
        visionBotData[0] = new VisionbotData();
        standardResponse.setData(visionBotData);
        ResponseEntity<StandardResponse> standardResponseResponseEntity = new ResponseEntity<StandardResponse>(standardResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(),any(),any(),Mockito.<Class<StandardResponse>> any())).thenReturn(standardResponseResponseEntity);
        //when
        List<VisionbotData> result =  visionBotAdapterImpl.getVisionbotData(organizationId);
        //then
        Assert.assertEquals(result.get(0),visionBotData[0]);
    }
    @Test
    public void getVisionbotDataReturnNullWhenNoDataFound()
    {
        //given
        String organizationId = "1";
        StandardResponse standardResponse = new StandardResponse();
        standardResponse.setSuccess(true);
        standardResponse.setError(null);
        VisionbotData[]  visionBotData= new VisionbotData[1];
        visionBotData[0] = new VisionbotData();
        standardResponse.setData("");
        ResponseEntity<StandardResponse> standardResponseResponseEntity = new ResponseEntity<StandardResponse>(standardResponse, HttpStatus.OK);
        Mockito.doNothing().when(restTemplate).setMessageConverters(any());
        when(restTemplate.exchange(anyString(),any(),any(),Mockito.<Class<StandardResponse>> any())).thenReturn(standardResponseResponseEntity);
        //when
        List<VisionbotData> result =  visionBotAdapterImpl.getVisionbotData(organizationId);
        //then
        Assert.assertEquals(null,result);
    }
    @Test
    public void getVisionbotDataReturnsDataForValidInputParameters()
    {
        //given
        String organizationId = "1";
        String projectId = "111";
        StandardResponse standardResponse = new StandardResponse();
        standardResponse.setSuccess(true);
        standardResponse.setError(null);
        VisionbotData[]  visionBotData= new VisionbotData[1];
        visionBotData[0] = new VisionbotData();
        standardResponse.setData(visionBotData);
        ResponseEntity<StandardResponse> standardResponseResponseEntity = new ResponseEntity<StandardResponse>(standardResponse, HttpStatus.OK);
        Mockito.doNothing().when(restTemplate).setMessageConverters(any());
        when(restTemplate.exchange(anyString(),any(),any(),Mockito.<Class<StandardResponse>> any())).thenReturn(standardResponseResponseEntity);

        //when
        List<VisionbotData> result =  visionBotAdapterImpl.getVisionbotData(organizationId,projectId);
        //then
        Assert.assertEquals(visionBotData[0],result.get(0));
    }
    @Test
    public void getVisionbotDataReturnsEmptyListWhenNoDataFound()
    {
        //given
        String organizationId = "1";
        String projectId = "111";
        StandardResponse standardResponse = new StandardResponse();
        standardResponse.setSuccess(true);
        standardResponse.setError(null);
        VisionbotData[]  visionBotData= new VisionbotData[1];
        visionBotData[0] = new VisionbotData();
        standardResponse.setData("");
        Mockito.doNothing().when(restTemplate).setMessageConverters(any());
        ResponseEntity<StandardResponse> standardResponseResponseEntity = new ResponseEntity<StandardResponse>(standardResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(),any(),any(),Mockito.<Class<StandardResponse>> any())).thenReturn(standardResponseResponseEntity);
        //when
        List<VisionbotData> result =  visionBotAdapterImpl.getVisionbotData(organizationId,projectId);
        //then
        Assert.assertEquals(0,result.size());
    }

    @Test
    public void testOfTestConnectionWhenRestCallToFileServiceReturnsOK(){
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        visionBotAdapterImpl.testConnection();
        
        //then
        //no exception
    }
    
    @Test(expectedExceptions={DependentServiceConnectionFailureException.class}, expectedExceptionsMessageRegExp="Error while connecting to Visionbot service")
    public void testOfTestConnectionWhenRestCallToFileServiceReturnsNotOK(){
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        visionBotAdapterImpl.testConnection();
        
        //then
        //exception thrown
    }

}
