package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryNameComparatorTest {
    private CategoryNameComparator comparator;
    private final static int EQUAL_TO = 0;
    private final static int LESS_THAN = -1;
    private final static int GREATER_THAN = 1;

    @Test
    public void testOfCategoryNameComparisonAscOrderWithNullsShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryNameComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getName()).thenReturn(null);

        Category c2 = mock(Category.class);
        when(c2.getName()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfCategoryNameComparisonAscOrderWithNameAndNullShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryNameComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getName()).thenReturn("Group_1");

        Category c2 = mock(Category.class);
        when(c2.getName()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryNameAscOrderWithNullAndNameShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryNameComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getName()).thenReturn(null);

        Category c2 = mock(Category.class);
        when(c2.getName()).thenReturn("Group_2");

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfCategoryNameComparisonAscOrderWithIdenticalNamesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryNameComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getName()).thenReturn("Group_3");

        Category c2 = mock(Category.class);
        when(c2.getName()).thenReturn("Group_3");

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfCategoryNameComparisonAscOrderWithNamesShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryNameComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getName()).thenReturn("Group_A");

        Category c2 = mock(Category.class);
        when(c1.getName()).thenReturn("Group_B");

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryNameComparisonAscOrderWithNamesShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryNameComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getName()).thenReturn("Group_D");

        Category c2 = mock(Category.class);
        when(c2.getName()).thenReturn("Group_C");

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }
}
