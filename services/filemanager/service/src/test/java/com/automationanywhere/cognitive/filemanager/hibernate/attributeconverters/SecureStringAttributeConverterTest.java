package com.automationanywhere.cognitive.filemanager.hibernate.attributeconverters;

import static org.mockito.Mockito.times;

import java.io.IOException;
import org.mockito.Mockito;
import org.testng.annotations.Test;
import com.automationanywhere.cognitive.common.security.CipherException;
import com.automationanywhere.cognitive.common.security.DefaultCipherService;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.InputStreamConversionException;

public class SecureStringAttributeConverterTest {

  public SecureStringAttributeConverterTest() {
    // TODO Auto-generated constructor stub
  }

  @Test
  public void testOfConvertToDatabaseColumnWhenEncryptionEnabled() throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureStringAttributeConverter.convertToDatabaseColumn("testattribute");

    //then
    Mockito.verify(cipherService, times(1)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test
  public void testOfConvertToDatabaseColumnWhenEncryptionDisabled() throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, false);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureStringAttributeConverter.convertToDatabaseColumn("testattribute");

    //then
    Mockito.verify(cipherService, times(0)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(0)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test(expectedExceptions = CipherException.class)
  public void testOfConvertToDatabaseColumnWhenExceptionInCipherServiceConversion()
      throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doReturn("Test".getBytes()).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    Mockito.doThrow(CipherException.class).when(cipherService).encrypt(Mockito.any());
    secureStringAttributeConverter.convertToDatabaseColumn("testattribute");

    //then
    Mockito.verify(cipherService, times(1)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test(expectedExceptions = InputStreamConversionException.class)
  public void testOfConvertToDatabaseColumnWhenExceptionInEncryptedStreamConversion()
      throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);
    //When
    Mockito.doThrow(InputStreamConversionException.class).when(inputStreamConverter)
        .convertInputStreamToByteArray(Mockito.any());
    secureStringAttributeConverter.convertToDatabaseColumn("testattribute");

    //then
    Mockito.verify(cipherService, times(1)).encrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToByteArray(Mockito.any());
  }

  @Test
  public void testOfConvertToEntityAttributeWhenEncryptionIsEnabled() throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doReturn("Test").when(inputStreamConverter).convertInputStreamToString(Mockito.any());
    secureStringAttributeConverter.convertToEntityAttribute("testattribute");

    //then
    Mockito.verify(cipherService, times(1)).decrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToString(Mockito.any());
  }

  @Test
  public void testOfConvertToEntityAttributeWhenEncryptionIsDisabled() throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, false);

    //When
    Mockito.doReturn("Test").when(inputStreamConverter).convertInputStreamToString(Mockito.any());
    secureStringAttributeConverter.convertToEntityAttribute("testattribute");

    //then
    Mockito.verify(cipherService, times(0)).decrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(0)).convertInputStreamToString(Mockito.any());
  }

  @Test//(expectedExceptions=CipherException.class)
  //TODO will enable exception clause once the condition is removed in the code
  public void testOfConvertToEntityAttributeWhenExceptionInCipherServiceConversion()
      throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doThrow(CipherException.class).when(cipherService).decrypt(Mockito.any());
    secureStringAttributeConverter.convertToEntityAttribute("testattribute");

    //then
    //Mockito.verify(cipherService, times(1)).decrypt(Mockito.any());
    //Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToString(Mockito.any());
  }

  @Test//(expectedExceptions=IOException.class)
  //TODO will enable exception clause once the condition is removed in the code
  public void testOfConvertToEntityAttributeWhenExceptionInEncryptedStreamConversion()
      throws IOException {

    //Given
    DefaultCipherService cipherService = Mockito.mock(DefaultCipherService.class);
    InputStreamConverter inputStreamConverter = Mockito.mock(InputStreamConverter.class);
    SecureStringAttributeConverter secureStringAttributeConverter = new SecureStringAttributeConverter();
    secureStringAttributeConverter.initCipherService(cipherService, inputStreamConverter, true);

    //When
    Mockito.doThrow(IOException.class).when(inputStreamConverter)
        .convertInputStreamToString(Mockito.any());
    secureStringAttributeConverter.convertToEntityAttribute("testattribute");

    //then
    Mockito.verify(cipherService, times(1)).decrypt(Mockito.any());
    Mockito.verify(inputStreamConverter, times(1)).convertInputStreamToString(Mockito.any());
  }
}
