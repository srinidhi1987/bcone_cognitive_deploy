package com.automationanywhere.cognitive.filemanager.service.impl;

import com.automationanywhere.cognitive.filemanager.adapter.VisionbotAdapter;
import com.automationanywhere.cognitive.filemanager.datalayer.contract.CommonDataAccessLayer;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.BadRequestException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataAccessLayerException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DataNotFoundException;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.FileNameLengthExceedException;
import com.automationanywhere.cognitive.filemanager.messagqueue.contract.MessageQueuePublisher;
import com.automationanywhere.cognitive.filemanager.models.CompareOperator;
import com.automationanywhere.cognitive.filemanager.models.Environment;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.LearningInstanceSummaryEntity;
import com.automationanywhere.cognitive.filemanager.models.LearningInstance.Summary;
import com.automationanywhere.cognitive.filemanager.models.PatchOperation;
import com.automationanywhere.cognitive.filemanager.models.PatchRequest;
import com.automationanywhere.cognitive.filemanager.models.classification.ClassificationAnalysisReport;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.CustomEntityModelBase;
import com.automationanywhere.cognitive.filemanager.models.customentitymodel.FileUploadStatus;
import com.automationanywhere.cognitive.filemanager.models.dto.VisionbotData;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.*;
import com.automationanywhere.cognitive.filemanager.models.statistics.CountStatistic;
import com.automationanywhere.cognitive.filemanager.models.statistics.EnvironmentCountStatistic;
import com.automationanywhere.cognitive.filemanager.service.contract.ClassificationReportService;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.Part;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Created by Mayur.Panchal on 22-02-2017.
 */
public class FileDetailsServiceImplTest {
    @Mock
    private CommonDataAccessLayer<EntityModelBase,CustomEntityModelBase> mockFileServiceDal;
    @Mock(name = "classificationQueueService")
    private MessageQueuePublisher mockClassificationQueueService;
    @Mock(name = "visionbotProcessDocumentService")
    private MessageQueuePublisher mockProcessingQueuePublisher;
    @Mock
    private ClassificationReportService mockClassificationReportService;
    @Mock
    private VisionbotAdapter mockVisionbotAdapter;
    @InjectMocks
    private FileDetailsServiceImpl sut;
    @Mock
    private FileDetailsServiceImpl mockFileServiceImpl;

    @BeforeTest
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void GetProjectWiseFilesShouldReturnsListOfFilesWhenValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "111";
        String requestId = "121";
        boolean isProduction = false;
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        fileDetailsList.add(file2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        // when
        List<FileDetails> actualResult = sut.getProjectWiseFiles(projectId,requestId,isProduction);
        // then
        Assert.assertTrue(actualResult.size() == 2);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }
    @Test
    public void GetProjectWiseFilesShouldReturnsNullWhenInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = null;
        String requestId = "";
        boolean isProduction = false;
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        fileDetailsList.add(file2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getProjectWiseFiles(projectId,requestId,isProduction);
        //then
        Assert.assertEquals(null, actualResult);
    }
    @Test(expectedExceptions=DataNotFoundException.class)
    public void GetProjectWiseFilesShouldThrowExceptionForGivenValidInputParametersAndDataNotFound() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "111";
        String requestId = "121";
        boolean isProduction = false;
        List<Object> fileDetailsList = new ArrayList<>();
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getProjectWiseFiles(projectId,requestId,isProduction);
        //then
        Assert.assertTrue(actualResult.size() == 2);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }

    @Test
    public void GetProjectWiseClassifiedFilesReturnsListOfFilesProjectIdThatExists() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "111";
        boolean isProduction = false;
        String requestId = "121";
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        fileDetailsList.add(file2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getProjectWiseClassifiedFiles(projectId,requestId);
        //then
        Assert.assertTrue(actualResult.size() == 2);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }
    @Test
    public void GetProjectWiseClassifiedFilesShouldReturnsNullWhenInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "";
        boolean isProduction = false;
        String requestId = "121";
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        fileDetailsList.add(file2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getProjectWiseClassifiedFiles(projectId,requestId);
        //then
        Assert.assertEquals(null,actualResult);
    }

    @Test
    public void GetProjectWiseClassifiedFilesShouldReturnsNoDataWhenProjectIdThatExists() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "111";
        String requestId = "121";
        boolean isProduction = false;
        List<Object> fileDetailsList = new ArrayList<>();
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getProjectWiseClassifiedFiles(projectId,requestId);
        //then
        Assert.assertTrue(actualResult.size() == 0);
    }
    @Test
    public void GetCategoryWiseFilesShouldReturnsListOfFilesWhenValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "111";
        String classificationId = "1";
        boolean isProduction = false;

        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        fileDetailsList.add(file2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getCategoryWiseFiles(projectId,classificationId,"");
        //then
        Assert.assertTrue(actualResult.size() == 2);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }
    @Test
    public void GetCategoryWiseFilesShouldReturnsNullForInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given

        String projectId = "111";
        String classificationId = "";
        boolean isProduction = false;

        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        fileDetailsList.add(file2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getCategoryWiseFiles(projectId,classificationId,"");
        //then
        Assert.assertEquals(null, actualResult);
    }
    @Test(expectedExceptions  = DataNotFoundException.class)
    public void GetCategoryWiseFilesShouldReturnNoDataWhenValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String projectId = "111";
        String classificationId = "1";
        boolean isProduction = false;

        List<Object> fileDetailsList = new ArrayList<>();
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getCategoryWiseFiles(projectId,classificationId,"");
        //then
        Assert.assertTrue(actualResult.size() == 2);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }
    @Test
    public void getSpecificFileShouldReturnListOfFileDetailsWhenValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String fileId= "aaa";
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getSpecificFile(fileId);
        //then
        Assert.assertTrue(actualResult.size() == 1);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }
    @Test
    public void getSpecificFileShouldReturnsNullWhenGivenInvalidParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String fileId= "";
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        fileDetailsList.add(file1);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getSpecificFile(fileId);
        //then
        Assert.assertEquals(null, actualResult);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void getSpecificFileShouldReturnNoDataWhenNotAvailable() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        String fileId= "aaa";
        List<Object> fileDetailsList = new ArrayList<>();
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        //when
        List<FileDetails> actualResult = sut.getSpecificFile(fileId);
        //then
        Assert.assertTrue(actualResult.size() == 0);
    }
    @Test
    public void deleteAllFilesShouldReturnTrueForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        boolean expectedResult = true;
        String projectId = "aaa";
        String classificationId ="12";
        when(mockFileServiceDal.DeleteRows(any())).thenReturn(true);
        //when
        boolean actualResult = sut.deleteAllFiles(projectId,classificationId);
        //then
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void deleteAllFilesShouldReturnFalseInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        boolean expectedResult = false;
        String projectId = null;
        String classificationId ="";
        when(mockFileServiceDal.DeleteRows(any())).thenReturn(true);
        //when
        boolean actualResult = sut.deleteAllFiles(projectId,classificationId);
        //then
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void deleteSpecificFileShouldReturnTrueForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        boolean expectedResult = true;
        String projectId = "aaa";
        String classificationId ="12";
        String fileId = "abc";
        when(mockFileServiceDal.DeleteRows(any())).thenReturn(true);
        //when
        boolean actualResult = sut.deleteSpecificFile(projectId,classificationId,fileId);
        //then
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void deleteSpecificFileShouldReturnFalseForInvalidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        boolean expectedResult = false;
        String projectId = null;
        String classificationId ="";
        String fileId = "abc";
        when(mockFileServiceDal.DeleteRows(any())).thenReturn(true);
        //when
        boolean actualResult = sut.deleteSpecificFile(projectId,classificationId,fileId);
        //then
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void updateResourceShouldReturnTrueForValidInputParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        boolean expectedResult = true;
        String projectId = "aaa";
        String classificationId ="12";
        String layoutId = "abc";
        when(mockFileServiceDal.UpdateRow(any())).thenReturn(true);
        //when
        boolean actualResult = sut.updateResource(projectId,classificationId,layoutId);
        //then
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void updateResourceShoudReturnFalseForInvalidParameters() throws DataAccessLayerException,DataNotFoundException
    {
        //given
        boolean expectedResult = false;
        String projectId = null;
        String classificationId ="";
        String layoutId = "abc";
        when(mockFileServiceDal.UpdateRow(any())).thenReturn(true);
        //when
        boolean actualResult = sut.updateResource(projectId,classificationId,layoutId);
        //then
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void getCategoriesByProjectIdReturnCategoryForValidInputParameters() throws DataNotFoundException, DataAccessLayerException, BadRequestException {
        //given
        String organizationId = "1";
        String projectId = "aaa";
        Integer offset =0;
        Integer limit = 1;
        VisionbotData visionbotData1 = new VisionbotData();
        visionbotData1.setName("visionBot1");
        VisionbotData visionbotData2 = new VisionbotData();
        visionbotData2.setName("visionBot2");
        VisionbotData visionbotData3 = new VisionbotData();
        visionbotData3.setName("visionBot3");
        List<VisionbotData> visionbotDataList = new ArrayList<>();
        visionbotDataList.add(visionbotData1);
        visionbotDataList.add(visionbotData2);
        visionbotDataList.add(visionbotData3);
        when(mockVisionbotAdapter.getVisionbotData(any())).thenReturn(visionbotDataList);
        ClassificationAnalysisReport classificationAnalysisReport = new ClassificationAnalysisReport();
        classificationAnalysisReport.allCategoryDetail = new ArrayList<>();
        when(mockClassificationReportService.getClassificationAnalysisReport(any())).thenReturn(classificationAnalysisReport);

        List<Object> countStatistic = new ArrayList<>();
        CountStatistic countStatistic1 = new CountStatistic();
        countStatistic1.setCategoryId("1");
        countStatistic1.setFileCount(100);
        countStatistic1.setProductionFileCount(new EnvironmentCountStatistic());
        countStatistic1.setStagingFileCount(new EnvironmentCountStatistic());
        countStatistic.add(countStatistic1);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(countStatistic);
        List<CountStatistic> countStatisticList = new ArrayList<>();
        countStatisticList.add(countStatistic1);
        when(mockFileServiceImpl.getCountStatisticsByEnvironment(any(),any())).thenReturn(countStatisticList);
        //when
        Categories categories = sut.getCategoriesByProjectId(organizationId,projectId,offset,limit,"index");
        //then
        Assert.assertNotNull(categories);
        Assert.assertEquals(countStatisticList.get(0).getCategoryId(),categories.getCategories().get(0).getId());
    }
    @Test
    public void processUnprocessedFilesValidInputParameters() throws IOException,TimeoutException,DataAccessLayerException {
        //given
        String organizationId ="1";
        String projectId = "111";
        FileDetails fileDetails1 = new FileDetails();
        fileDetails1.setProjectId(projectId);
        FileDetails fileDetails2 = new FileDetails();
        fileDetails2.setProjectId(projectId);
        List<Object> fileDetailsList = new ArrayList<>();
        fileDetailsList.add(fileDetails1);
        fileDetailsList.add(fileDetails2);
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        Mockito.doNothing().when(mockProcessingQueuePublisher).sendMessage(any());
        //when
        sut.processUnprocessedFiles(organizationId,projectId);
    }
    @Test
    public void processUnprocessedFilesInvalidInputParameters() throws IOException,TimeoutException,DataAccessLayerException {
        //given
        String organizationId =null;
        String projectId = null;
        Mockito.doNothing().when(mockProcessingQueuePublisher).sendMessage(any());
        //when
        sut.processUnprocessedFiles(organizationId,projectId);
    }
    @Test
    public void processUnprocessedFiles_WithCategory_ValidInputParameters() throws IOException,TimeoutException, DataAccessLayerException {
        //given
        String organizationId ="1";
        String projectId = "111";
        String categoryId = "12";
        FileDetails fileDetails1 = new FileDetails();
        fileDetails1.setProjectId(projectId);
        FileDetails fileDetails2 = new FileDetails();
        fileDetails2.setProjectId(projectId);
        List<Object> fileDetailsList = new ArrayList<>();
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        Mockito.doNothing().when(mockProcessingQueuePublisher).sendMessage(any());
        //when
        sut.processUnprocessedFiles(organizationId,projectId,categoryId);
    }
    @Test
    public void processUnprocessedFiles_WithCategory_InvalidInputParameters() throws IOException,TimeoutException,DataAccessLayerException {
        //given
        String organizationId ="1";
        String projectId = null;
        String categoryId = null;
        Mockito.doNothing().when(mockProcessingQueuePublisher).sendMessage(any());
        //when
        sut.processUnprocessedFiles(organizationId,projectId,categoryId);
    }
    @Test
    public void getCountStatisticsShouldReturnDataForValidParameters() throws DataAccessLayerException {
        //given
        String organizationId ="1";
        String projectId = "111";
        List<Object> countStatistic = new ArrayList<>();
        CountStatistic countStatistic1 = new CountStatistic();
        countStatistic1.setCategoryId("1");
        countStatistic1.setFileCount(100);
        countStatistic1.setProductionFileCount(new EnvironmentCountStatistic());
        countStatistic1.setStagingFileCount(new EnvironmentCountStatistic());
        countStatistic.add(countStatistic1);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(countStatistic);
        //when
        List<CountStatistic> resultCountStatisticList = sut.getCountStatistics(organizationId,projectId);
        //then
        Assert.assertEquals(countStatistic1.getCategoryId(),resultCountStatisticList.get(0).getCategoryId());
    }
    @Test
    public void getCountStatisticsShouldReturnNullForInvalidParameters() throws DataAccessLayerException {
        //given
        String organizationId =null;
        String projectId = "";
        List<Object> countStatistic = new ArrayList<>();
        CountStatistic countStatistic1 = new CountStatistic();
        countStatistic1.setCategoryId("1");
        countStatistic1.setFileCount(100);
        countStatistic1.setProductionFileCount(new EnvironmentCountStatistic());
        countStatistic1.setStagingFileCount(new EnvironmentCountStatistic());
        countStatistic.add(countStatistic1);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(countStatistic);
        //when
        List<CountStatistic> resultCountStatisticList = sut.getCountStatistics(organizationId,projectId);
        //then
        Assert.assertEquals(null,resultCountStatisticList);
    }
    @Test
    public void getCountStatisticsByEnvironmentShouldReturnDataForValidParameters() throws DataAccessLayerException {
        //given
        String organizationId ="1";
        String projectId = "111";
        List<Object> countStatistic = new ArrayList<>();
        CountStatistic countStatistic1 = new CountStatistic();
        countStatistic1.setCategoryId("1");
        countStatistic1.setFileCount(100);
        countStatistic1.setProductionFileCount(new EnvironmentCountStatistic());
        countStatistic1.setStagingFileCount(new EnvironmentCountStatistic());
        countStatistic.add(countStatistic1);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(countStatistic);
        //when
        List<CountStatistic> resultCountStatisticList = sut.getCountStatisticsByEnvironment(organizationId,projectId);
        //then
        Assert.assertEquals(countStatistic1.getCategoryId(),resultCountStatisticList.get(0).getCategoryId());
    }
    @Test
    public void getCountStatisticsByEnvironmentShouldReturnNullForInvalidParameters() throws DataAccessLayerException {
        //given
        String organizationId =null;
        String projectId = "";
        List<Object> countStatistic = new ArrayList<>();
        CountStatistic countStatistic1 = new CountStatistic();
        countStatistic1.setCategoryId("1");
        countStatistic1.setFileCount(100);
        countStatistic1.setProductionFileCount(new EnvironmentCountStatistic());
        countStatistic1.setStagingFileCount(new EnvironmentCountStatistic());
        countStatistic.add(countStatistic1);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(countStatistic);
        //when
        List<CountStatistic> resultCountStatisticList = sut.getCountStatistics(organizationId,projectId);
        //then
        Assert.assertEquals(null,resultCountStatisticList);
    }
    @Test
    public void getFileUploadProgressShouldReturnDataForValidInputParameters() throws DataAccessLayerException {
        //given
        String organizationId ="1";
        String projectId = "111";
        String requestId = "aaa";
        List<Object> fileUploadStatusList = new ArrayList<>();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        fileUploadStatus.setProjectId(projectId);
        fileUploadStatusList.add(fileUploadStatus);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(fileUploadStatusList);
        //when
        FileUploadStatus result= sut.getFileUploadProgress(projectId,requestId,Environment.production);
        //then
        Assert.assertEquals(fileUploadStatus,result);
    }
    @Test
    public void getFileUploadProgressShouldReturnNullWhenNoDataAvailable() throws DataAccessLayerException {
        //given
        String projectId = "111";
        String requestId = "aaa";
        List<Object> fileUploadStatusList = new ArrayList<>();
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(fileUploadStatusList);
        //when
        FileUploadStatus result= sut.getFileUploadProgress(projectId,requestId,Environment.production);
        //then
        Assert.assertEquals( null,result);
    }
    @Test
    public void getFileUploadProgressShouldReturnNullForInvalidInputParameters() throws DataAccessLayerException {
        //given
        String projectId = "";
        String requestId = "";
        List<Object> fileUploadStatusList = new ArrayList<>();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        fileUploadStatus.setProjectId(projectId);
        fileUploadStatusList.add(fileUploadStatus);
        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(fileUploadStatusList);
        //when
        FileUploadStatus result= sut.getFileUploadProgress(projectId,requestId, Environment.production);
        //then
        Assert.assertEquals( null,result);
    }
    @Test
    public void updateFileDetailShouldReturnTrueForValidPatchOperation() throws DataAccessLayerException {
        //given
        String fileId = "aaaa";
        PatchRequest patchRequest = new PatchRequest();
        patchRequest.setOp(PatchOperation.replace);
        patchRequest.setPath("processed");
        patchRequest.setValue("12");
        PatchRequest[] patchRequestsArray = new PatchRequest[1];
        patchRequestsArray[0] = patchRequest;
        when(mockFileServiceDal.UpdateRow(any())).thenReturn(true);
        //when
        boolean result = sut.updateFileDetail(fileId,patchRequestsArray);
        //then
        Assert.assertEquals(true,result);
    }
    @Test
    public void updateFileDetailShouldReturnFalseForInvalidPatchOperationReplace() throws DataAccessLayerException {
        //given
        String fileId = "";
        PatchRequest patchRequest = new PatchRequest();
        patchRequest.setOp(PatchOperation.replace);
        patchRequest.setPath("processed");
        patchRequest.setValue("12");
        PatchRequest[] patchRequestsArray = new PatchRequest[1];
        patchRequestsArray[0] = patchRequest;
        when(mockFileServiceDal.UpdateRow(any())).thenReturn(true);
        //when
        boolean result = sut.updateFileDetail(fileId,patchRequestsArray);
        //then
        Assert.assertEquals(false,result);
    }
    @Test
    public void updateFileDetailShouldReturnFalseForInvalidPatchClassificationId() throws DataAccessLayerException {
        //given
        String fileId = "111";
        PatchRequest patchRequest = new PatchRequest();
        patchRequest.setOp(PatchOperation.replace);
        patchRequest.setPath("classificationId");
        patchRequest.setValue("12");
        PatchRequest[] patchRequestsArray = new PatchRequest[1];
        patchRequestsArray[0] = patchRequest;
        when(mockFileServiceDal.UpdateRow(any())).thenReturn(true);
        //when
        boolean result = sut.updateFileDetail(fileId,patchRequestsArray);
        //then
        Assert.assertEquals(false,result);
    }
    @Test
    public void updateFileDetailShouldReturnFalseForInvalidPatchOperation() throws DataAccessLayerException {
        //given
        String fileId = "111";
        PatchRequest patchRequest = new PatchRequest();
        patchRequest.setOp(PatchOperation.add);
        patchRequest.setPath("processed");
        patchRequest.setValue("12");
        PatchRequest[] patchRequestsArray = new PatchRequest[1];
        patchRequestsArray[0] = patchRequest;
        when(mockFileServiceDal.UpdateRow(any())).thenReturn(true);
        //when
        boolean result = sut.updateFileDetail(fileId,patchRequestsArray);
        //then
        Assert.assertEquals(false,result);
    }
    @Test
    public void getStagingFilesIfNotAvailableCopyFromProduction_NoFilesInStaging_ReturnStagingFile() throws DataAccessLayerException, DataNotFoundException {
        String projectId = "11";
        String categoryId = "2";
        List<Object> fileDetailsViewsProductionFalse = new ArrayList<>();
        List<Object> fileDetailsViewsProductionTrue = new ArrayList<>();

        FileDetailsView f1 = new FileDetailsView();
        f1.setFileId("1_Production");
        f1.setIsProduction(true);
        fileDetailsViewsProductionTrue.add(f1);

        FileDetailsView view = new FileDetailsView();
        view.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter("11", CompareOperator.equal));
        view.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter("2", CompareOperator.equal));
        view.WhereParam.put(FileDetails.Property.isProduction.toString(), new MapValueParameter("false", CompareOperator.equal));

        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsViewsProductionFalse).thenReturn(fileDetailsViewsProductionTrue).thenReturn(fileDetailsViewsProductionTrue);

        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails f = new FileDetails();
        f.setFileId("1_Production");
        fileDetailsList.add(f);

        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(fileDetailsList).thenReturn(fileDetailsList);

        List<FileDetails> actualResult = sut.getStagingFilesIfNotAvailableCopyFromProduction(projectId, categoryId);
        Assert.assertTrue(actualResult.size() == 1);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }

    @Test
    public void getStagingFilesIfNotAvailableCopyFromProduction_FilesInStaging_ReturnStagingFile() throws DataAccessLayerException, DataNotFoundException {
        String projectId = "11";
        String categoryId = "2";
        List<Object> fileDetailsViewsProductionFalse = new ArrayList<>();
        List<Object> fileDetailsViewsProductionTrue = new ArrayList<>();

        FileDetailsView s1 = new FileDetailsView();
        s1.setFileId("1_staging");
        s1.setIsProduction(true);
        fileDetailsViewsProductionFalse.add(s1);

        FileDetailsView p1 = new FileDetailsView();
        p1.setFileId("1_production");
        p1.setIsProduction(false);
        fileDetailsViewsProductionTrue.add(p1);

        FileDetailsView view = new FileDetailsView();
        view.WhereParam.put(FileDetails.Property.projectId.toString(), new MapValueParameter("11", CompareOperator.equal));
        view.WhereParam.put(FileDetails.Property.classificationId.toString(), new MapValueParameter("2", CompareOperator.equal));
        view.WhereParam.put(FileDetails.Property.isProduction.toString(), new MapValueParameter("false", CompareOperator.equal));

        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsViewsProductionFalse).thenReturn(fileDetailsViewsProductionTrue).thenReturn(fileDetailsViewsProductionTrue);

        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails f = new FileDetails();
        f.setFileId("1_staging");
        fileDetailsList.add(f);

        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(fileDetailsList).thenReturn(fileDetailsList);

        List<FileDetails> actualResult = sut.getStagingFilesIfNotAvailableCopyFromProduction(projectId, categoryId);
        Assert.assertTrue(actualResult.size() == 1);
        Assert.assertEquals(fileDetailsList.get(0), actualResult.get(0));
    }

    @Test
    public void getStagingFilesIfNotAvailableCopyFromProduction_InvalidInputParameters_ReturnFalse() throws DataAccessLayerException, DataNotFoundException {
        String projectId = "";
        String categoryId = "";
        List<FileDetails> expectedResult = null;
        List<FileDetails> actualResult = sut.getStagingFilesIfNotAvailableCopyFromProduction(projectId, categoryId);
        Assert.assertEquals(expectedResult,actualResult);
    }
    @Test
    public void getLearningInstanceSummaryShouldReturnLearningInstanceSummaryWhenValidInputParameters()
            throws DataNotFoundException, DataAccessLayerException {
        // given
        String organizationId = "1";
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";


        List<Object> learningInstanceSummaries = new ArrayList<>();
        LearningInstanceSummaryEntity learningInstanceSummaryEntity = new LearningInstanceSummaryEntity();
        learningInstanceSummaryEntity.setStagingFileSummary(new Summary(2l, 0l, 4l));
        learningInstanceSummaryEntity.setProductionFileSummary(new Summary(3l, 0l, 26l));
        learningInstanceSummaries.add(learningInstanceSummaryEntity);

        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(learningInstanceSummaries);

        // when
        LearningInstanceSummaryEntity actualLearningInstanceSummaryEntity = sut.getLearningInstanceSummary(organizationId,
                projectId);

        // then
        Assert.assertNotEquals(null, actualLearningInstanceSummaryEntity);
        Assert.assertEquals(learningInstanceSummaryEntity, actualLearningInstanceSummaryEntity);

    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void
    getLearningInstanceSummaryShouldReturnNullListForGivenValidInputParametersAndDataNotFound()
            throws DataAccessLayerException, DataNotFoundException {
        // given
        String organizationId = "1";
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";

        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(null);

        sut.getLearningInstanceSummary(organizationId, projectId);

    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void getLearningInstanceSummaryShouldReturnsEmptyListWhenInvalidInputParametersAndDataNotFound()
            throws DataAccessLayerException, DataNotFoundException {
        // given
        String organizationId = "1";
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";

        // required
        List<Object> learningInstanceSummaries = new ArrayList<>();

        when(mockFileServiceDal.ExecuteCustomEntityQuery(any())).thenReturn(learningInstanceSummaries);

        sut.getLearningInstanceSummary(organizationId, projectId);

    }

    @Test
    public void reclassificationShouldNullWhenNoFileFound() throws DataAccessLayerException, IOException, TimeoutException {
        // given
        String organizationId = "1";
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";

        //when
        when(mockFileServiceDal.SelectRows(any())).thenReturn(null);

        //then
        String data = sut.triggerReclassification(organizationId, projectId);

        Assert.assertNull(data);
    }

    @Test
    public void reclassificationShouldReturnDataWhenFileFound() throws DataAccessLayerException, IOException, TimeoutException {
        // given
        String organizationId = "1";
        String projectId = "bc048269-08d2-4fa7-a1b3-ab233019ff7b";
        List<Object> fileDetailsList = new ArrayList<>();
        FileDetails file1 = new FileDetails();
        file1.setFileId("file1");
        fileDetailsList.add(file1);
        FileDetails file2 = new FileDetails();
        file2.setFileId("file2");
        fileDetailsList.add(file2);

        //when
        when(mockFileServiceDal.SelectRows(any())).thenReturn(fileDetailsList);
        Mockito.doNothing().when(mockClassificationQueueService).sendMessage(any());
        String data = sut.triggerReclassification(organizationId, projectId);

        //then
        Assert.assertNotNull(data);
        Assert.assertTrue(data.contains("file1"));
        Mockito.verify(mockClassificationQueueService, times(2)).sendMessage(any()); ;
    }

    @Test
    public void uploadFileShouldReturnEmptyListForEmptyParts() throws FileNameLengthExceedException {
        // GIVEN
        String uploadFileId = "1";
        String prjId = "1";
        Collection<Part> parts = Collections.emptyList();
        boolean isProduction = true;

        // WHEN
        List<FileDetails> details = sut.uploadFile(uploadFileId, parts, prjId, isProduction);

        // THEN
        assertThat(details).isEmpty();
    }

    @Test
    public void uploadFileShouldReturnSingleFileDetailForEmptyParts() throws Exception {
        // GIVEN
        String uploadFileId = "1";
        String prjId = "1";
        boolean isProduction = true;
        String name = "invoice";
        String filename = "invoice.pdf";
        FileDetails fileDetail = new FileDetails();
        fileDetail.setFormat("pdf");
        fileDetail.setProjectId(prjId);
        fileDetail.setFileName(filename);
        fileDetail.setIsProduction(isProduction);

        Part part = mock(Part.class);
        when(part.getName()).thenReturn(name);
        when(part.getHeader("content-disposition")).thenReturn("filename=\""+filename+"\"");

        Collection<Part> parts = new ArrayList<>();
        parts.add(part);

        // WHEN
        List<FileDetails> details = sut.uploadFile(uploadFileId, parts, prjId, isProduction);

        // THEN
        assertThat(details).isNotNull();
        assertThat(details.size()).isEqualTo(1);
        FileDetails fileDetailRetrieved = details.get(0);
        assertThat(fileDetailRetrieved.getFileName()).isEqualTo(filename);
        assertThat(fileDetailRetrieved.getProjectId()).isEqualTo(prjId);
        assertThat(fileDetailRetrieved.getFormat()).isEqualTo("pdf");
        assertThat(fileDetailRetrieved.getIsProduction()).isTrue();

    }
}
