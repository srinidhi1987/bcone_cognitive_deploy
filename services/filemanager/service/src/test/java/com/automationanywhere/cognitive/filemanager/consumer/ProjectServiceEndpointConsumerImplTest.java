package com.automationanywhere.cognitive.filemanager.consumer;

import com.automationanywhere.cognitive.common.resttemplate.wrapper.RestTemplateWrapper;
import com.automationanywhere.cognitive.filemanager.consumer.impl.ProjectServiceEndpointConsumerImpl;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DependentServiceConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.models.CustomResponse;
import com.automationanywhere.cognitive.filemanager.models.ProjectDetail;
import com.automationanywhere.cognitive.filemanager.models.StandardResponse;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by Mayur.Panchal on 08-06-2017.
 */
public class ProjectServiceEndpointConsumerImplTest {

    @Mock
    RestTemplateWrapper restTemplateWrapper;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private ProjectServiceEndpointConsumerImpl projectServiceEndPointConsumerImpl;
    @BeforeTest
    public void setUp()
    {
        this.restTemplate = new RestTemplate();
        this.restTemplateWrapper = new RestTemplateWrapper(restTemplate);
        projectServiceEndPointConsumerImpl = new ProjectServiceEndpointConsumerImpl("host");
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void getProjectDetailsShouldReturnDataForValidParameters() {
        String orgId = "1";
        String projectId = "1";

        StandardResponse standardResponse = new StandardResponse();
        standardResponse.setSuccess(true);
        standardResponse.setError(null);

        ProjectDetail projectDetail = new ProjectDetail();
        projectDetail.setId(projectId);
        projectDetail.setOrganizationId(orgId);
        standardResponse.setData(projectDetail);
        ResponseEntity<StandardResponse> standardResponseResponseEntity = new ResponseEntity<StandardResponse>(standardResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(),any(),any(), Mockito.<Class<StandardResponse>> any())).thenReturn(standardResponseResponseEntity);

        projectServiceEndPointConsumerImpl.setRestTeplate(restTemplate);
        ProjectDetail result = projectServiceEndPointConsumerImpl.getProjectDetails(orgId,projectId);
        Assert.assertEquals(projectDetail,result);
    }
    @Test
    public void getProjectDetailsShouldReturnNullForInvalidParameters() {
        //given
        String orgId = "";
        String projectId = null;

        CustomResponse standardResponse = new CustomResponse();
        standardResponse.setSuccess(true);
        standardResponse.setErrors(null);

        ProjectDetail projectDetail = new ProjectDetail();
        projectDetail.setId(projectId);
        projectDetail.setOrganizationId(orgId);
        standardResponse.setData(projectDetail); ResponseEntity<CustomResponse> standardResponseResponseEntity = new ResponseEntity<CustomResponse>(standardResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(),any(),any(), Mockito.<Class<CustomResponse>> any())).thenReturn(null);

        //sut.setRestTeplate(restTemplate);
        //when
        ProjectDetail result = projectServiceEndPointConsumerImpl.getProjectDetails(orgId,projectId);
        //then
        Assert.assertEquals(null,result);
    }
    
    @Test
    public void testOfTestConnectionWhenRestCallToFileServiceReturnsOK(){
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        projectServiceEndPointConsumerImpl.testConnection();
        
        //then
        //no exception
    }
    
    @Test(expectedExceptions={DependentServiceConnectionFailureException.class},expectedExceptionsMessageRegExp="Error while connecting to Project service")
    public void testOfTestConnectionWhenRestCallToFileServiceReturnsNotOK(){
        //given
        @SuppressWarnings("unchecked")
        ResponseEntity<String> standardResponseResponseEntity = Mockito.mock(ResponseEntity.class);
        
        Mockito.when(standardResponseResponseEntity.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.doReturn(standardResponseResponseEntity).when(restTemplateWrapper).exchangeGet(Mockito.anyString(),Mockito.<Class<String>> any());
        
        //when
        projectServiceEndPointConsumerImpl.testConnection();
        
        //then
        //exception thrown
    }
}
