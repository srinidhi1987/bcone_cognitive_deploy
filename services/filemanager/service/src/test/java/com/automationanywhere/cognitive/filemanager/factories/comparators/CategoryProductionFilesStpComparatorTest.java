package com.automationanywhere.cognitive.filemanager.factories.comparators;

import com.automationanywhere.cognitive.filemanager.models.entitymodel.Category;
import com.automationanywhere.cognitive.filemanager.models.entitymodel.FileCount;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryProductionFilesStpComparatorTest {
    private CategoryProductionFilesStpComparator comparator;
    private final static int EQUAL_TO = 0;
    private final static int LESS_THAN = -1;
    private final static int GREATER_THAN = 1;

    @Test
    public void testOfCategoryProductionFilesStpAscOrderWithNullValuesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(null);

        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }

    @Test
    public void testOfCategoryProductionFilesStpAscOrderWithValueAndNullShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(311);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(fc1);

        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryProductionFilesStpAscOrderWithNullAndValueShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);

        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(null);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(312);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfCategoryProductionFilesStpAscOrderWithValuesShouldYieldLessThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(101);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(fc1);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(102);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(LESS_THAN);
    }

    @Test
    public void testOfCategoryProductionFilesStpAscOrderWithValuesShouldYieldGreaterThan() throws Exception {
        // given
        comparator = new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(104);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(fc1);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(103);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(fc2);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(GREATER_THAN);
    }

    @Test
    public void testOfCategoryProductionFilesStpAscOrderWithIdenticalValuesShouldYieldEqualTo() throws Exception {
        // given
        comparator = new CategoryProductionFilesStpComparator(ComparisonOrder.ASCENDING);

        FileCount fc1 = mock(FileCount.class);
        when(fc1.getTotalSTPCount()).thenReturn(123);
        Category c1 = mock(Category.class);
        when(c1.getProductionFileDetails()).thenReturn(null);

        FileCount fc2 = mock(FileCount.class);
        when(fc2.getTotalSTPCount()).thenReturn(123);
        Category c2 = mock(Category.class);
        when(c2.getProductionFileDetails()).thenReturn(null);

        // when
        int result = comparator.compare(c1, c2);

        // then
        assertThat(result).isEqualTo(EQUAL_TO);
    }
}
