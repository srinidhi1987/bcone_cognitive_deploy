package com.automationanywhere.cognitive.filemanager.health.handlers.impl;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import com.automationanywhere.cognitive.common.healthapi.constants.HTTPStatusCode;
import com.automationanywhere.cognitive.common.healthapi.json.models.ServiceConnectivity;
import com.automationanywhere.cognitive.filemanager.exception.customexceptions.DBConnectionFailureException;
import com.automationanywhere.cognitive.filemanager.health.connections.ConnectionType;
import com.automationanywhere.cognitive.filemanager.health.connections.HealthCheckConnection;
import com.automationanywhere.cognitive.filemanager.health.connections.HealthCheckConnectionFactory;

public class FileManagerHealthCheckHandlerTest {
    
    @Mock
    HealthCheckConnectionFactory healthCheckConnectionFactory;
    @InjectMocks
    FileManagerHealthCheckHandler fileManagerHealthCheckhandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOfCheckHealthWhenDBConnectionFails() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doThrow(DBConnectionFailureException.class).when(healthCheckConnection).checkConnection();
        // Mockito.doNothing().when(healthCheckConnection).checkConnection();

        // when
        String healthcheck = fileManagerHealthCheckhandler.checkHealth();
        
        //then
        assertThat(healthcheck).contains("Database Connectivity: FAILURE");
    }

    @Test
    public void testOfCheckHealthWhenDBConnectionSuccess() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doNothing().when(healthCheckConnection).checkConnection();

        // when
        String healthcheck = fileManagerHealthCheckhandler.checkHealth();
        
        //then
        assertThat(healthcheck).contains("Database Connectivity: OK");

    }

    @Test
    public void testOfCheckHealthWhenVisionBotServiceConnectionSuccess() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("VisionBot", HTTPStatusCode.OK);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = fileManagerHealthCheckhandler.checkHealth();

        // then
        assertThat(healthcheck).contains("VisionBot: OK");
    }
    
    @Test
    public void testOfCheckHealthWhenVisionBotServiceConnectionFails() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("VisionBot", HTTPStatusCode.OK);
        dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = fileManagerHealthCheckhandler.checkHealth();

        // then
        assertThat(healthcheck).contains("VisionBot: INTERNAL_SERVER_ERROR");
    }
    
    @Test
    public void testOfCheckHealthWhenProjectServiceConnectionSuccess() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("Project", HTTPStatusCode.OK);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = fileManagerHealthCheckhandler.checkHealth();

        // then
        assertThat(healthcheck).contains("Project: OK");
    }
    
    @Test
    public void testOfCheckHealthWhenProjectServiceConnectionFails() {
        // given
        HealthCheckConnection healthCheckConnection = Mockito.mock(HealthCheckConnection.class);
        List<ServiceConnectivity> dependentServices = new ArrayList<ServiceConnectivity>();
        ServiceConnectivity dependentService = new ServiceConnectivity("Project", HTTPStatusCode.OK);
        dependentService.setHTTPStatus(HTTPStatusCode.INTERNAL_SERVER_ERROR);
        dependentServices.add(dependentService);
        Mockito.when(healthCheckConnectionFactory.getConnection(Mockito.any(ConnectionType.class)))
                .thenReturn(healthCheckConnection);
        Mockito.doReturn(dependentServices).when(healthCheckConnection).checkConnectionForService();

        // when
        String healthcheck = fileManagerHealthCheckhandler.checkHealth();

        // then
        assertThat(healthcheck).contains("Project: INTERNAL_SERVER_ERROR");
    }}
