#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctype.h>
#include <float.h>
#include <string.h>
#include <malloc.h>

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

static void sigmoid_train(
	int l, const double *dec_values, const double *labels,
	double& A, double& B);

static double sigmoid_predict(double decision_value, double A, double B);