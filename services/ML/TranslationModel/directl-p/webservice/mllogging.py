from win32com.shell import shell, shellcon
import logging
import os
import configmanager
import json



class MLLogging:
    def getLogDirectory(self):
       log_dir = shell.SHGetFolderPath(0, shellcon.CSIDL_COMMON_DOCUMENTS, None, 0)
       log_dir = log_dir + '\Automation Anywhere IQBot Platform\Logs\\'
       if not os.path.exists(log_dir):
           os.makedirs(log_dir)
           
       return log_dir
    
    def initLogging(self, logfileName):
        logFilePath = os.path.join(self.getLogDirectory(), logfileName)
        loglevel = self.getLogLevel()
        logging.basicConfig(format='<%(levelname)s> <%(asctime)s> %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename = logFilePath, level = loglevel)
    
    def getLogLevelBasedOnConfiguration(self, x):
        return {
            "error": logging.ERROR,
            "debug": logging.DEBUG,
            "info": logging.INFO,
            "all": logging.DEBUG,
            "trace": logging.DEBUG,
            "warning":logging.WARNING
        }.get(x, logging.ERROR) 

    def getLogLevel(self):
        config = configmanager.ConfigManager()
        jsonObj = config.getConfiguration()
        if(jsonObj is None):
            return logging.ERROR
        
        return self.getLogLevelBasedOnConfiguration(str(jsonObj["LoggerSettings"]["level"]).lower())
