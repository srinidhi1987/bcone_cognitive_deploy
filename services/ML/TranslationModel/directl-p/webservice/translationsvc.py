from win32com import shell, shellcon
import logging
import os
import mllogging

#The method GetLogDirectory is deleted because it is not required to be here as added another class to init the logging.
mllog = mllogging.MLLogging()
mllog.initLogging("MLTranslationService.log")


import win32serviceutil
import win32service
import win32event
import servicemanager
import schedule
import time
import sys
import urllib2
import datetime
import databasemanager as dbManager
import configmanager


class DBManager:
    def GetDBConnection(self):
        try:
            DataBase = dbManager.DBManager()
            conn       = DataBase.initDB()
            return conn
        except:
            print("Exception: TranslationSVC::DBManager::GetDBConnection()")
            return None

    def GetUpdatedFieldIdsForGivenInterval(self, startTime, endTime):
        try:
            logging.debug("Getting DB connection")
            conn  = self.GetDBConnection()
            if conn == None:
                return []
            logging.debug("DB Connected")
            cur   = conn.cursor()
            queryString = "SELECT distinct fieldid FROM trainingdata where correctedon>='"+startTime +\
                          "' AND correctedon<'" + endTime + "'"
            logging.debug(queryString)
            cur.execute(queryString)
            logging.debug("DB Connected")
            return cur
        except:
            print("Exception: TranslationSVC::DBManager::GetUpdatedFieldIdsForGivenInterval()")
            return []

class TrainModel:
    def TrainModelForUpdatedFields(self):
        logging.debug("Service Started")
        try:
            lastUpdatedTime = self.ReadLastUpdatedtime()
            currTime        = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            logging.debug("CurrTime", currTime)
            dbManager       = DBManager();
            updatedFieldIds = dbManager.GetUpdatedFieldIdsForGivenInterval(lastUpdatedTime, currTime)

            logging.debug("fields taken from DB")
            for fieldid in updatedFieldIds:
                logging.debug(fieldid[0])
                self.TrainModelForGivenFiled(fieldid[0])
            self.UpdateCurrTime()

        except:
            logging.error(sys.exc_info()[0])
            print("Unexpected error: TranslationSVC::DBManager::TrainModelForUpdatedFields()", sys.exc_info()[0])

    def TrainModelForGivenFiled(self, fieldID):
        try:
                logging.debug("Updating Model")
                config = configmanager.ConfigManager()
                jsonObj = config.getConfiguration()
                portNo = jsonObj['MLWebService']['port']
                urlProtocol = "http"
                if(str(jsonObj["SSLSettings"]["RunWithSSL"]).lower() == "true"):
                    urlProtocol = "https"
                url               = '{0}://localhost:{1}/train?fieldid={2}'.format(urlProtocol, portNo, str(fieldID))
                logging.debug(url)
                rqst              = urllib2.urlopen(url)
                logging.debug("Request Fired")
                updatedField_data = rqst.read()
                '''trainingModel  = getDataAndTrain.TrainModel()
                trainingModel.UpdateOrCreateModels(fieldID)
                logging.debug("Model Updated")'''
        except:
            print("Exception: TranslationSVC::TrainModel::TrainModelForGivenFiled()")


    def ReadLastUpdatedtime(self):
        lastupdatedtime = ""
        try:
            lastupdateFilePath = os.path.join(self.GetCurrentDir(), "LastUpdateTime.txt")
            if os.path.isfile(lastupdateFilePath) and os.access(lastupdateFilePath, os.R_OK):
                with open(lastupdateFilePath, "r") as myfile:
                    lastupdatedtime = myfile.read(-1)
                    
                if(self.IsDateValid(lastupdatedtime, "%Y-%m-%d %H:%M:%S") == False):
                    logging.error("Date format is incorrect so deleting the LastUpdtaeTime file and taking 2 minutes earlier than current time")
                    os.remove(lastupdateFilePath)
                    lastupdatedtime = self.GetTime2MinEarlierThanCurrent()
            else:
                logging.debug("The file 'LastUpdateTime.txt' is not available so taking 2 minutes earlier than current time")
                lastupdatedtime = self.GetTime2MinEarlierThanCurrent()
        except:
            logging.error("Exception occured in ReadLastUpdatedtime so taking 2 minutes earlier than current time")
            logging.error(sys.exc_info()[0])
            lastupdatedtime = self.GetTime2MinEarlierThanCurrent()
        return lastupdatedtime

    def GetTime2MinEarlierThanCurrent(self):
            timeBefore2Mins = datetime.datetime.now()-datetime.timedelta(minutes = 2)
            timeBefore2Mins = timeBefore2Mins.strftime('%Y-%m-%d %H:%M:%S')
            return timeBefore2Mins
        
    def UpdateCurrTime(self):
        try:
            lastupdateFilePath = os.path.join(self.GetCurrentDir(), "LastUpdateTime.txt")
            with open(lastupdateFilePath, "w") as myfile:
                myfile.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        except:
            logging.error("Exception: TranslationSVC::TrainModel::UpdateCurrTime()")

    def IsDateValid(self, date_text, format):
        try:
            date_text = date_text.replace('\x00', "")
            datetime.datetime.strptime(date_text, format)
        except ValueError:
            logging.error("Incorrect Date format")
            return False
        return True

    def GetCurrentDir(self):
        if getattr(sys, 'frozen', False):
            application_path = os.path.dirname(sys.executable)
        elif __file__:
            application_path = os.path.dirname(__file__)
        return application_path
#The method GetLogDirectory is deleted as it was a duplicate code.
class AppServerSvc (win32serviceutil.ServiceFramework):
    _svc_name_         = "Automation Anywhere Cognitive MLScheduler Service"
    _svc_display_name_ = "Automation Anywhere Cognitive MLScheduler Service"

    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.hWaitStop = win32event.CreateEvent(None,0,0,None)
        self.isAlive = True

    def SvcStop(self):
        self.isAlive = False
        schedule.clear()
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        try:
            schedule.every(2).minutes.do(TrainModel().TrainModelForUpdatedFields)
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                        servicemanager.PYS_SERVICE_STARTED,(self._svc_name_,''))
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        while self.isAlive == True:
            schedule.run_pending()
            time.sleep(1)
        self.main()

    def main(self):
        pass

if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(AppServerSvc)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(AppServerSvc)
