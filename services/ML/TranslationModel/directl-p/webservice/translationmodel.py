# -*- coding: utf-8 -*-
from __future__ import print_function
from jsm import JSM

class TranslationModel:    
    def TrainModel(self, ocr, valid, fieldId):
        try:
            ocr_train = ocr #[ocr[i] for i in train_idx]
            valid_train = valid #[valid[i] for i in train_idx]
            model = JSM(fieldId)
            model.fit(ocr_train,valid_train)
        except:
            return 'Fail'
            
        return 'success'

