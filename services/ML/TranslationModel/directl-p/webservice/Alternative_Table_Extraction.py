
# coding: utf-8

# ## ::::::::::::::::::::::::::: Imports :::::::::::::::::::::::::::

# In[1]:

ENABLE_TESTING_TABLE_START_END             = 0
ENABLE_FIX_INPUT_JSON_FOR_TBL2             = 0
ENABLE_TESTING_TABLE_SIGNATURE_EXTRACTION  = 0
ENABLE_TESTING_TABLE_STRIP_LABEL           = 0 
ENABLE_TESTING_TABLE_AUTO_DISCOVER_COLUMNS = 0
if ENABLE_TESTING_TABLE_START_END or    ENABLE_FIX_INPUT_JSON_FOR_TBL2 or    ENABLE_TESTING_TABLE_SIGNATURE_EXTRACTION or    ENABLE_TESTING_TABLE_STRIP_LABEL or    ENABLE_TESTING_TABLE_AUTO_DISCOVER_COLUMNS :
    DEBUG=1
else:
    DEBUG=0


# In[2]:

import os
import sys
import json
import copy
import re
import numpy as np
from timeit import default_timer as timer
if DEBUG:
    import cv2
    from scipy.misc import imsave
    from pprint import pprint
    import xlwt
    import xlrd


# ## ::::::::::::::::::::::::::: Pretty Print List of Segments :::::::::::::::::::::::::::

# In[3]:

def leftalign(s):
    textbox = '({0},{1},{2},{3})'.format(s[0],s[1],s[2],s[3])
    textbox = '{:<30}'.format(textbox) 
    return textbox

def print_segments(S,heading=('NA','NA','NA')):
    temp = leftalign(['x1','y1','x2','y2'])
    if heading[0]=='NA':
        print '{0} \t\t conf \t text'.format(temp)
        for i,s in enumerate(S):
            print "{0}. {1} \t {2} \t {3}".format(i+1,leftalign(s[0:4]),s[5],s[6])
    elif heading[1]=='NA':
        print '{0} \t\t conf \t {1} \t text'.format(temp,heading[0])
        for i,s in enumerate(S):
            print "({0}. {1} \t {2} \t {3} \t {4}".format(i+1,leftalign(s[0:4]),s[5],s[7],s[6])
    elif heading[2]=='NA':
        print '{0} \t\t conf \t {1} \t {2} \t text'.format(temp,heading[0],heading[1])
        for i,s in enumerate(S):
            print "{0}. {1} \t {2} \t {3} \t {4} \t {5}".format(i+1,leftalign(s[0:4]),s[5],s[7],s[8],s[6])
    else:
        print '{0} \t\t conf \t {1} \t {2} \t {3} \t text'.format(temp,heading[0],heading[1],heading[2])
        for i,s in enumerate(S):
            print "{0}. {1} \t {2} \t {3} \t {4} \t {5} \t {6}".format(i+1,leftalign(s[0:4]),s[5],s[7],s[8],s[9],s[6])
    print ''


# ## ::::::::::::::::::::::::::: Debug Routines :::::::::::::::::::::::::::

# In[4]:

def annotate_rectangles(I,S,fn,color,width,ENTRY0_REFCOL=0):
    I2 = copy.deepcopy(I)
    BORDER = 8
    color2 = (color[2],color[0],color[1])
    if ENTRY0_REFCOL:
        I2 = cv2.rectangle(I2,(S[0][0]-BORDER,S[0][1]-BORDER),(S[0][2]+BORDER,S[0][3]+BORDER),color2,width)
    for r in range(len(S)):
        I2 = cv2.rectangle(I2,(S[r][0],S[r][1]),(S[r][2],S[r][3]),color,width)
    cv2.imwrite(fn,I2)

def annotate_lines(I,Lv,Lh,fn,color,width):
    I2 = copy.deepcopy(I)
    h,w,_=I2.shape
    for i in range(len(Lv)):
        I2 = cv2.line(I2,(Lv[i],0),(Lv[i],h),color,width)
    for i in range(len(Lh)):
        I2 = cv2.line(I2,(0,Lh[i]),(w,Lh[i]),color,width)
    cv2.imwrite(fn,I2)


# In[5]:

def dump_debug_xls_file(fn,Z,S):
    book = xlwt.Workbook(encoding="utf-8")
    XLS = book.add_sheet("Sheet 1")
    for col,seg in enumerate(Z):
        XLS.write(0,col,seg[6])
    for seg in S:
        XLS.write(1+seg[8],seg[9],seg[6])
    book.save(fn)


# In[6]:

def dump_depracated_image(I,S,Z,fn):
    I2 = copy.deepcopy(I)
    blank = copy.deepcopy(I)
    blank = blank*0
    blank = blank + 255
    for s in S:
        if s[8]!=-1 and s[9]!=-1 and s[9]!=len(Z)-1:
            x0,y0,x1,y1=s[0:4]
            I2[y0:y1+1,x0:x1+1,:] = blank[y0:y1+1,x0:x1+1,:]
    cv2.imwrite(fn,I2)
    return 1


# ## ::::::::::::::::::::::::::: JSON => segments; segments => words :::::::::::::::::::::::::::

# In[7]:

def split_words(S):
    S2=[]
    for seg in S:
        words = seg[6].split(' ')
        if len(words)==1:
            S2.append(seg)
        else:                               
            cnt = 0.0
            x1 = seg[0]
            N = seg[2]-seg[0]+1
            for j,word in enumerate(words[:-1]):
                seg2=copy.deepcopy(seg)
                cnt += len(word)+0.5
                x2 = seg[0] + int(cnt*float(N)/len(seg[6]) + 0.5)
                seg2[0]=x1
                seg2[2]=x2
                seg2[6]=word 
                S2.append(seg2)
                cnt+=0.5
                x1 = x2
            seg2=copy.deepcopy(seg)
            seg2[0]=x1
            seg2[6]=words[-1]
            S2.append(seg2)
    return S2

def extract_segments_from_json(JSON):
    S = []
    botdesign_page0_width  = JSON['botdesigninfo']['layoutdocumentproperties']['pageproperties'][0]['width']
    iqtestdoc_page0_width  = JSON['documentproperties']['pageproperties'][0]['width']
    botdesign_page0_height = JSON['botdesigninfo']['layoutdocumentproperties']['pageproperties'][0]['height']
    iqtestdoc_page0_height = JSON['documentproperties']['pageproperties'][0]['height']
    scale_X = float(botdesign_page0_width)/iqtestdoc_page0_width
    scale_Y = float(botdesign_page0_height)/iqtestdoc_page0_height
    print("\t %%%% SCALING ...... botdesign={0} x {1}, iqtestdoc={2} x {3}, scale={4} x {5}".format(botdesign_page0_width,
                                                                               botdesign_page0_height,
                                                                               iqtestdoc_page0_width,
                                                                               iqtestdoc_page0_height,
                                                                               scale_X,
                                                                               scale_Y
                                                                             ))
    for seg in JSON['segmenteddocument']['fields']:
        x1 = int(seg['bounds']['x'])
        y1 = int(seg['bounds']['y'])
        x2 = x1 + int(seg['bounds']['width'])
        y2 = y1 + int(seg['bounds']['height'])
        x1 = int(scale_X * x1)
        x2 = int(scale_X * x2)
        y1 = int(scale_Y * y1)
        y2 = int(scale_Y * y2)
        conf = int(seg['confidence'])
        try :
            text = seg['text'].encode('utf-8')
            S.append([x1,y1,x2,y2,4,conf,text,-1,-1,-1])
        except UnicodeEncodeError:
            text = " "

    return split_words(S)


# ## ::::::::::::::::::::::::::: Extract BOT Design information from Input JSON :::::::::::::::::::::::::::

# In[8]:

def extract_user_definitions_from_json(JSON):
    Z = []
    num_tables = len(JSON['botdesigninfo']['tables'])
    for idx,table in enumerate(JSON['botdesigninfo']['tables']):
        for column in table['columns']:
            if str(column['columnid']) == str(table['primarycolumnid']):
                if column['label']=='':
                    table_start_reference = ['XXXXXXXXXXXXXXX', column['keybounds']['x'], column['keybounds']['y']]
                else:
                    table_start_reference = [str(column['label']), column['keybounds']['x'], column['keybounds']['y']]

        try:
            table_end = str(table['footer'])
        except UnicodeEncodeError:
            table_end = ''
        if table_end=='':
            table_end = 'XXXXXXXXXXXXXXX'
        suffix = '_tbl' + str(idx+1)
        boxes = []
        for column in table['columns']:
            x1 = column['keybounds']['x'] + column['valuebounds']['x']
            x2 = x1 + column['valuebounds']['width']
            y1 = column['keybounds']['y'] + column['valuebounds']['y']
            y2 = y1 + column['valuebounds']['height']
            try:
                text = str(column['label'])
            except UnicodeEncodeError:
                text = "UnicodeEncodeError"  
            if str(column['columnid'])==str(table['primarycolumnid']):
                text = text + ' (REF)'
            if 'isdollarcurrency' in column:
                if column['isdollarcurrency'] in ['yes','Yes','YES']:
                    text = text + ' (DOLLAR AMOUNT)'  
            if str(column['type'])=='Number' and '(DOLLAR AMOUNT)' not in text:
                text = text + ' (NUMERIC)'
          
            text = text.upper()
            boxes.append((text,x1,y1,x2,y2,str(column['columnid']),str(column['defid'])))
        refbox = [b for b in boxes if b[5]==str(table['primarycolumnid'])]
        nonrefbox = [b for b in boxes if b[5]!=str(table['primarycolumnid'])]
        boxes = refbox + nonrefbox
        Z.append([table_start_reference, table_end, boxes, suffix, str(table['id'])])
    return Z


# ## ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ## ::::::::::::::::::::::::::: BEGIN TABLE EXTRACTION CODE :::::::::::::::::::::::::::
# ## ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# ## ::::::::::::::::::::::::::: Common Routines :::::::::::::::::::::::::::

# In[9]:

def smaller(a,b):
    if a<b:
        return a
    return b

def larger(a,b):
    if a>b:
        return a
    return b

def overlap(a0,a1,b0,b1):
    if int(b0)>int(a1):
        return 0
    elif int(a0)>int(b1):
        return 0
    else:
        return float(smaller(a1,b1)-larger(a0,b0)+1)/(smaller(a1-a0+1,b1-b0+1))


# In[10]:

def flatten_list(L):
    L2=[]
    for l in L:
        if isinstance(l,(list,)):
            for x in l:
                L2.append(x)
        else:
            L2.append(l)
    return L2


# ## ::::::::::::::::::::::::::: Common Routines - manipulating segment-list :::::::::::::::::::::::::::

# In[11]:

def drop_unassigned_segments(S):
    S2=copy.deepcopy(S)
    return [s for s in S2 if s[8]>-1 and s[9]>-1]

def sort_segments_row_col(S):
    S2 = copy.deepcopy(S)
    S2.sort(key=lambda s: (s[8],s[9],s[7],s[0]))
    return S2 

def sort_segments_col_row(S):
    S2 = copy.deepcopy(S)
    S2.sort(key=lambda s: (s[9],s[8],s[7],s[0]))
    return S2 

def drop_row(S,thisrow):
    S2 = copy.deepcopy(S)
    return [s for s in S2 if s[8]!=thisrow]

def drop_column(S,thiscol):
    S2 = copy.deepcopy(S)
    return [s for s in S2 if s[9]!=thiscol]

def renumber_rows(S):
    S2 = copy.deepcopy(S)
    row_number=-1; prev=-9999
    for idx,s in enumerate(S2):
        if s[8]!=prev:
            row_number+=1
        prev=s[8]
        S2[idx][8]=row_number
    return S2

def renumber_columns(S):
    S2 = copy.deepcopy(sort_segments_col_row(S))
    col_number=-1; prev=-9999
    for idx,s in enumerate(S2):
        if s[9]!=prev:
            col_number+=1
        prev=s[9]
        S2[idx][9]=col_number
    return sort_segments_row_col(S2)


# ## ::::::::::::::::::::::::::: Drop BAD segments (outliers) :::::::::::::::::::::::::::

# In[12]:

def drop_outlier_tall_segments(S,FACTOR1=2,FACTOR2=5):
    if S==[]:
        return S
    avg_segment_height = np.mean([s[3]-s[1] for s in S])
    S2 = [s for s in S if (s[3]-s[1])<avg_segment_height*FACTOR1 and (s[3]-s[1])>avg_segment_height/FACTOR2]
    print("^^^^ AVERAGE SEGMENT HEIGHT IS",avg_segment_height,"^^^^")
    print("^^^^ #segments", len(S),"(before);   ", len(S2), "(after) ^^^^")
    return S2

def drop_outlier_tiny_segments(S,FACTOR=32):
    if S==[]:
        return S
    avg_segment_area = np.mean([(s[2]-s[0])*(s[3]-s[1]) for s in S])
    S2 = [s for s in S if (s[2]-s[0])*(s[3]-s[1])>avg_segment_area/FACTOR]
    print("^^^^ AVERAGE SEGMENT AREA IS",avg_segment_area,"^^^^")
    print("^^^^ #segments", len(S),"(before);   ", len(S2), "(after) ^^^^")
    return S2


# ## ::::::::::::::::::::::::::: Region of Interest :::::::::::::::::::::::::::

# In[13]:

def combine_adjacent_rects(ocr):
    ocr2 = []
    valid = [1]*len(ocr)
    cx1,cy1,cx2,cy2,clev,cconf,ctext=ocr[0][0:7]
    for i in range(1,len(ocr)):
        if cy1!=ocr[i][1] or cy2!=ocr[i][3] or cx2!=ocr[i][0]:
            ocr2.append([cx1,cy1,cx2,cy2,clev,cconf,ctext])
            cx1,cy1,cx2,cy2,clev,cconf,ctext=ocr[i][0:7]
        else:
            cx2 = ocr[i][2]
            try:
                x = ocr[i][6]
            except IndexError:
                x = ""
            ctext = str(ctext) + ' ' + str(x)
            
    ocr2.append([cx1,cy1,cx2,cy2,clev,cconf,ctext])
    return ocr2

def region_of_interest(S, h, w, table_end, Z):
    maxy = np.max([s[3] for s in S])
    roi=[0,0,w,h]
    roi=[0,0,w,maxy] 
    
    ## Left, Top, Right
    roi[0] = np.min([x[0] for x in Z]) - 10
    roi[1] = np.min([x[1] for x in Z]) - 10
    roi[2] = np.max([x[2] for x in Z]) + 10
    ## Bottom
    Scomb = combine_adjacent_rects(S)
    S2 = []; found=0
    table_end_words = table_end.split('|')
    table_end_words = [w[1:] if w[0]==' ' else w for w in table_end_words]
    table_end_words = [w[:-1] if w[-1]==' ' else w for w in table_end_words]
    print("^^^ table_end_words={0} ^^^".format(table_end_words))
    for i in range(len(Scomb)):
        for table_end_word in table_end_words:
            if table_end_word in Scomb[i][6]: #Scomb[i][6]==table_end_word:
                roi[3] = Scomb[i][1] + 10
                print "((((((((((((( FOUND table_end='{0}')))))))))))))".format(table_end_word)
                found=1
                break
        if found:
            break
    
    print "((((((((( region_of_interest={0} )))))))))".format(roi)

    for i in range(len(S)):
        if S[i][0]>=roi[0] and S[i][2]<=roi[2] and S[i][1]>=roi[1] and S[i][3]<=roi[3]:
            S2.append(S[i])
    return S2


# ## ::::::::::::::::::::::::::: MEGAROW computation :::::::::::::::::::::::::::

# In[14]:

def merge_segments(s1,s2,direction):
    s = copy.deepcopy(s1)
    if direction==0:
        separator = ' '
    else:
        separator = '\n'
    if smaller(s1[direction],s2[direction])==s1[direction]:
        s[6] = s1[6] + separator + s2[6]
    else:
        s[6] = s2[6] + separator + s1[6]                 
    s[0] = smaller(s1[0],s2[0])
    s[1] = smaller(s1[1],s2[1])
    s[2] = larger(s1[2],s2[2])
    s[3] = larger(s1[3],s2[3])
    s[5] = int(0.5+(s1[5]*s2[5])/100.0)
    return s

def compute_megarows(S):
    MR=[];   N = len(S);   merged = [0]*N 
    ## Compute MegaRows
    for i in range(0,N):
        if merged[i]==0:
            seg1 = copy.deepcopy(S[i])
            for j in range(i+1,N):
                seg2 = S[j]
                if merged[j]==0 and overlap(seg1[1],seg1[3],seg2[1],seg2[3])>0.60:
                    merged[j] = 1
                    seg1 = merge_segments(seg1,seg2,0)
            MR.append(seg1)
    MR.sort(key=lambda p: (p[1]+p[3])/2)
    for i in range(len(MR)):
        MR[i][7]=i
    return MR

def assign_megarowID(S,M,margins=(0,0)):
    S2=copy.deepcopy(S)
    for i in range(len(S2)):
        found=-1
        for idx in range(len(M)):
            if S2[i][0]>=(M[idx][0]-margins[0]) and S2[i][2]<=(M[idx][2]+margins[0]) and                S2[i][1]>=(M[idx][1]-margins[1]) and S2[i][3]<=(M[idx][3]+margins[1]):
                found=idx
                break
        S2[i][7]=found
    return S2

def assign_megarowID_range(Z,M,overlap_ratio=0.7):
    Z2=copy.deepcopy(Z)
    for i in range(len(Z)):
        found0=-1
        for idx in range(len(M)):
            if overlap(M[idx][1],M[idx][3],Z[i][1],Z[i][3])>overlap_ratio: 
                found0=idx
                break
        found1=found0
        for idx in range(found0+1,len(M)):
            if not overlap(M[idx][1],M[idx][3],Z[i][1],Z[i][3])>overlap_ratio:
                found1=idx-1
                break
            if idx==len(M)-1:
                found1 = len(M)-1
        Z2[i][7]=found0
        Z2[i][8]=found1
    return Z2


# ## ::::::::::::::::::::::::::: Validation Checks :::::::::::::::::::::::::::

# In[15]:

def is_numeric(x):
    x2 = str(x).replace(',','')
    try:
        float(x2)
        return True
    except ValueError:
        return False
    
def is_telephonenumber(x):
    x2 = str(x).replace(' ','').replace('(','').replace(')','')
    return is_numeric(x2)
   
def is_dollaramount(x):
    x2 = str(x).replace(' ','')
    if x2[0]!="$":
        return False
    return is_numeric(x2[1:])

def is_fixedtext(x,token):
    ret = x.lower().replace(' ','') == token.replace('(ref)','').replace('(FIXED TEXT)','').lower().replace(' ','')
    return ret

def is_nonblank(x):
    return len(str(x))>0


# In[16]:

def is_check_good(x, check_type,token):
    if check_type=="(NUMERIC)":
        return is_numeric(x)
    elif check_type=="(TELEPHONE NUMBER)":
        return is_telephonenumber(x)
    elif check_type=="(DOLLAR AMOUNT)":
        return is_dollaramount(x)
    elif check_type=="(FIXED TEXT)":
        return is_fixedtext(x,token)
    else:
        return is_nonblank(x)


# In[17]:

def drop_on_validation_checks(S,Z):
    S2 = copy.deepcopy(S)
    if len(S2)<1:
        return S2
    maxrow = np.max([s[8] for s in S]) + 1
    check_types = ["(NUMERIC)","(DOLLAR AMOUNT)","(NON BLANK)","(FIXED TEXT)","(TELEPHONE NUMBER)"]
    checks_needed = [(check_type in z[6], (check_type,column)) for check_type in check_types for column,z in enumerate(Z)]
    checks_needed = [x[1] for x in checks_needed if x[0]==True] 
    for check_needed in checks_needed:
        check_type,column = check_needed
        for row in range(maxrow):
            seg = [s for s in S2 if s[8]==row and s[9]==column]
            if len(seg)!=1 or not is_check_good(seg[0][6],check_type,Z[column][6]):
                S2 = drop_row(S2, row) 
    S2 = renumber_rows(S2)
    return S2


# ## ::::::::::::::::::::::::::: Find reference column :::::::::::::::::::::::::::

# In[18]:

def is_contained_in(small,large,margin=25):
    delta = smaller((small[1]-small[0]),(large[1]-large[0]))*margin/100.0
    return int(small[0]>=large[0]-delta and small[1]<=large[1]+delta)

def find_reference_column(S,z):
    S2=copy.deepcopy(S)    
    check_type='(DONT DO ANYTHING)'
    if "(NUMERIC)" in z[6]:
        check_type = "(NUMERIC)"
    elif "(TELEPHONE NUMBER)" in z[6]:
        check_type = "(TELEPHONE NUMBER)"
    elif "(DOLLAR AMOUNT)" in z[6]:
        check_type = "(DOLLAR AMOUNT)"
    elif "(FIXED TEXT)" in z[6]:
        check_type = "(FIXED TEXT)"
    else:
        check_type = "(NON BLANK)"
        
    margin = 25
    if check_type=="(FIXED TEXT)":
        margin = 50
    
    ## Search segments in reference column
    for i in range(len(S2)):
        S2[i][8] = is_contained_in((S2[i][0],S2[i][2]),(z[0],z[2]),margin=margin)
    S2 = [x for x in S2 if x[8]==1]
    S2.sort(key = lambda x: (x[7],x[0]))
  
    ## Drop segments based on validation-check 
    S3 = copy.deepcopy(S2)
    megarows = list(set([x[7] for x in S3]))
    megarows.sort()
    for megarow in megarows:
        text = ' '.join([s[6] for s in S3 if s[7]==megarow])
        if not is_check_good(text,check_type,z[6]):
            for idx in range(len(S3)):
                if S3[idx][7]==megarow:
                    S3[idx][8]=0
    S3 = [x for x in S3 if x[8]==1]
    S3.sort(key = lambda x: (x[7],x[0]))           
    return S3


# ## ::::::::::::::::::::::::::: Row/Column assignment :::::::::::::::::::::::::::

# In[19]:

def assign_row_col(S,Z,R,mode='USE_MEGAROWS'):
    S2=copy.deepcopy(S)
    for row in range(len(R)):
        for col in range(len(Z)):
            x_start = Z[col][0]
            x_end = Z[col][2]
            y_start = Z[col][1] + (R[row][1]-Z[0][1])
            y_end = Z[col][3] + (R[row][1]-Z[0][1])
            row_start = R[row][7] - Z[0][7] + Z[col][7]
            row_end = R[row][7] - Z[0][7] + Z[col][8]
            for i in range(len(S)):
                if mode=='USE_MEGAROWS':
                    y_check = S[i][7]>=row_start and S[i][7]<=row_end
                else:
                    y_check = is_contained_in((S[i][1],S[i][3]),(y_start,y_end),margin=25) 
                margin=25
                if "(FIXED TEXT)" in Z[col][6]:
                    margin=50
                if is_contained_in((S[i][0],S[i][2]),(x_start,x_end),margin=margin) and y_check:
                    S2[i][8]=row
                    S2[i][9]=col

    rownum_used=list(set([seg[8] for seg in S2]))
    if -1 in rownum_used:
        rownum_used.remove(-1)
    rownum_used.sort()
    for seg in S2:
        if seg[8]>-1:
            seg[8] = rownum_used.index(seg[8])   
    return S2           


# ## ::::::::::::::::::::::::::: More breaking-up segments :::::::::::::::::::::::::::

# In[20]:

def breakup_segments_special_characters_at_2_ends(S):
    S1 = []
    special_char = ":#,.|"
    for s in S:
        seg=s; text=seg[6]
        if len(text)<2:
            S1.append(seg)
            continue
        while text[0] in special_char and len(text)>1:
            cutpoint=int((1*seg[2] + (len(text)-1)*seg[0])/len(text))
            S1.append([seg[0],seg[1],cutpoint,seg[3],seg[4],seg[5],str(text[0]),seg[7],seg[8],seg[9]])
            nxt_seg=[cutpoint,seg[1],seg[2],seg[3],seg[4],seg[5],str(text[1:]),seg[7],seg[8],seg[9]]
            seg=copy.deepcopy(nxt_seg)
            text=text[1:]
        S1.append(seg) 
    
    special_char = ":#|"
    S2 = []
    for s in S1:
        seg=s; text=seg[6]
        if len(text)<2:
            S2.append(seg)
            continue
        while text[-1] in special_char and len(text)>1:
            cutpoint=int((1*seg[0] + (len(text)-1)*seg[2])/len(text))
            nxt_seg=[seg[0],seg[1],cutpoint,seg[3],seg[4],seg[5],str(text[:-1]),seg[7],seg[8],seg[9]]
            S2.append([cutpoint,seg[1],seg[2],seg[3],seg[4],seg[5],str(text[-1]),seg[7],seg[8],seg[9]])
            seg=copy.deepcopy(nxt_seg)
            text=text[:-1]
        S2.append(seg) 
    S2.sort(key = lambda p: (p[1],p[0]))
    return S2

def has_numbers(s):
    return any(c.isdigit() for c in str(s))

def breakup_mega_segments_joined_by_colon(S):
    S2= []
    for s in S:
        text=s[6]
        temp = text.split(':')
        L = [len(str(x)) for x in temp] 
        if len(temp)==2 and L[0]>3 and L[1]>3 and not has_numbers(temp[0]) and not has_numbers(temp[1]):
            x0=s[0]; x1=s[2]
            cutpoint1 = int((x0*(1+L[1]) + x1*(L[0]))/(L[0]+1+L[1]))
            cutpoint2 = int((x0*(L[1]) + x1*(1+L[0]))/(L[0]+1+L[1]))
            seg=copy.deepcopy(s); seg[6]=str(temp[0]); seg[2]=cutpoint1; S2.append(seg)
            seg=copy.deepcopy(s); seg[6]=":"; seg[0]=cutpoint1; seg[2]=cutpoint2; S2.append(seg)
            seg=copy.deepcopy(s); seg[6]=str(temp[1]); seg[0]=cutpoint2; S2.append(seg)
        else:
            S2.append(s)
    return S2


# ## ::::::::::::::::::::::::::: Drop stray text at end of table :::::::::::::::::::::::::::

# In[21]:

def drop_stray_text_last_row(S,Z,MegaRows,FACTOR=3):
    if S==[]:
        return S
    S2 = copy.deepcopy(S)
    nrows = np.max([x[8] for x in S]) + 1
    for col in range(len(Z)):
        annotated_cell_height = Z[col][3]-Z[col][1]
        lastrow = [x for x in S if x[8]==nrows-1 and x[9]==col]
        megarows_set = list(set([x[7] for x in lastrow]))
        megarows_set.sort()
        if len(megarows_set)>1:
            for id in range(1,len(megarows_set)):
                x0=megarows_set[0]; x=megarows_set[id]
                if MegaRows[x][1]-MegaRows[x0][3]>FACTOR*annotated_cell_height:
                    for i in range(len(S2)):
                        if S2[i][7]==x and S2[i][8]==nrows-1 and S2[i][9]==col:
                            S2[i][8]=-1
                            S2[i][9]=-1
    return S2


# ## ::::::::::::::::::::::::::: Table start :::::::::::::::::::::::::::

# In[22]:

def adjust_extraction_zones(Z,table_start,S):
    Z2=copy.deepcopy(Z)
    text,x,y=table_start 
    Scomb = combine_adjacent_rects(S)
    Scomb.sort(key=lambda p:p[1])
    this_x=x
    this_y=y
    words = text.split('|')
    words = [w[1:] if w[0]==' ' else w for w in words]
    words = [w[:-1] if w[-1]==' ' else w for w in words]
    print("^^^ table_start_words={0} ^^^".format(words))
    found=0
    for i in range(len(Scomb)):
        for word in words:
            if word in Scomb[i][6]: #Scomb[i][6] == word:
                this_x,this_y=Scomb[i][0:2] 
                print "((((((((((((( FOUND table_start='{0}' )))))))))))))".format(word)
                found=1
                break
        if found:
            break
    delta_x=this_x-x
    delta_y=this_y-y
    for i in range(len(Z2)):
        Z2[i][0] += delta_x
        Z2[i][1] += delta_y
        Z2[i][2] += delta_x
        Z2[i][3] += delta_y
    print "((((((((( delta_x={0}, delta_y={1} )))))))))".format(delta_x,delta_y)
    return Z2


# ## ::::::::::::::::::::::::::: Combine text for cell :::::::::::::::::::::::::::

# In[23]:

def join_text_per_cell(S):
    S1 = copy.deepcopy(S)
    S2=[]; S3=[]
    if len(S1)<2:
        return S1
    comb = S1[0]
    for s in S1[1:]:
        if s[8]!=comb[8] or s[9]!=comb[9] or s[7]!=comb[7]:
            S2.append(comb)
            comb = s
        else:
            comb[0] = smaller(comb[0],s[0]) #x1
            comb[1] = smaller(comb[1],s[1]) #y1
            comb[2] = larger(comb[2],s[2])  #x2
            comb[3] = larger(comb[3],s[3])  #y2
            comb[6] = str(comb[6]) + ' ' + str(s[6]) #text
    S2.append(comb)
    if len(S2)<2:
        return S2
    comb = S2[0]
    for s in S2[1:]:
        if s[8]!=comb[8] or s[9]!=comb[9]:
            S3.append(comb)
            comb = s
        elif s[7]!=comb[7]:
            comb[0] = smaller(comb[0],s[0]) #x1
            comb[1] = smaller(comb[1],s[1]) #y1
            comb[2] = larger(comb[2],s[2])  #x2
            comb[3] = larger(comb[3],s[3])  #y2
            comb[6] = str(comb[6]) + ' ' + str(s[6]) #text
            #comb[6] = str(comb[6]) + '\n' + str(s[6]) #text
    S3.append(comb)
    return S3


# ## ::::::::::::::::::::::::::: Strip label :::::::::::::::::::::::::::

# In[24]:

def strip_label(S,Z):
    S2 = copy.deepcopy(S)
    for col,z in enumerate(Z):
        if '(STRIP LABEL)' in z[6]:
            label = z[6].replace('(STRIP LABEL)','').replace(' ','')
            T = [flatten_list([s[:6],s[6].replace(label,''),s[7:]]) if s[9]==col else s for s in S2]
            S2 = copy.deepcopy(T)
    return S2


# ## ::::::::::::::::::::::::::: Signature Extraction :::::::::::::::::::::::::::

# In[25]:

def annotate_signature_coordinates(S,Z):
    if True not in ["(SIGNATURE EXTRACTION)" in z[6] for z in Z]:
        return S
    S2 = copy.deepcopy(S)
    nrows = 1+np.max([s[8] for s in S2])
    ncols = 1+np.max([s[9] for s in S2])
    S2 = [x for x in S2 if '(SIGNATURE EXTRACTION)' not in Z[x[9]][6]]
    R = [x for x in S if x[9]==0] # Pull reference column segments based on column
    R.sort(key=lambda x: x[8]) # Sort reference column segements based on row
    for col in range(ncols):
        if "(SIGNATURE EXTRACTION)" in Z[col][6]:
            for row in range(nrows):
                temp = copy.deepcopy(Z[col])
                x1= Z[col][0]
                x2= Z[col][2]
                y1= Z[col][1] + (R[row][1]-Z[0][1])
                y2= y1 + (Z[col][3]-Z[col][1])
                text = "{0} {1} {2} {3}".format(x1,y1,x2,y2)
                S2.append(flatten_list([temp[:6],text,temp[7],row,col]))
    S2 = sort_segments_row_col(S2)
    return S2

def signature_extraction(I,fn,S,Z):
    Z2=copy.deepcopy(Z)
    S2=copy.deepcopy(S)
    for i,seg in enumerate(S2):
        col=seg[9]
        if '(SIGNATURE EXTRACTION)' in Z[col][6]:
            label = Z[col][6].replace('(SIGNATURE EXTRACTION)','').replace(' ','')
            jpgfilename=fn+'_'+label.lower()+'_'+str(1+seg[8])+'.jpg'
            x1,y1,x2,y2=[int(temp) for temp in str(seg[6]).split()] 
            imsave(jpgfilename,I[y1:y2,x1:x2,:]) 
            S2[i][6]=jpgfilename
    return S2


# ## ::::::::::::::::::::::::::: Auto-discover Columns :::::::::::::::::::::::::::

# In[26]:

def separate_field_value(x):
    pair = x.rsplit(' ',1)
    if len(pair)<2:
        return ['SPURIOUS NAME',666666]
    left, right = pair
    if is_dollaramount(right) or is_numeric(right):
        return [left, right]
   
    if x[-2:]=="CR":
        pair = x[:-2].rsplit(' ',1)
        if len(pair)<2:
            return ['SPURIOUS NAME',666666]
        left, right = pair
        right= str(right) + " CR"
        return [left, right]
    return ['SPURIOUS NAME',666666]


# In[27]:

def add_counts(small,large):
    X = [[x, 0] for x in small]
    for i,item1 in enumerate(small):
        for item2 in large:
            if item1 in item2:
                X[i][1] = X[i][1] + 1
    return X


# In[28]:

def edit_distance(s1, s2):
    if (s1=='') or (s2=='') or (s2=='UNINITIALIZED') or (s2=='SEGMENTATION FAULT'):
        return 1.0
    if len(s1) > len(s2):
        s1, s2 = s2, s1
    d = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        d_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                d_.append(d[i1])
            else:
                d_.append(1 + min((d[i1], d[i1 + 1], d_[-1])))
        d = d_
    if (s1 != s2):
        return float(d[-1])/len(s1)
    else:
        return 0.0 


# In[29]:

def elimination_round(L):
    candidates = []
    for i in reversed(range(1,len(L))):
        udog = L[i][0]
        ed = np.min([edit_distance(udog,L[j][0]) for j in range(i)])
        totally_contained = [L[j][0] in udog for j in range(i)]
        if not (True in totally_contained or ed<0.10):
            candidates.append(udog)
    candidates.append(L[0][0])
    candidates.reverse()
    return candidates


# In[30]:

def match_nearest_column_name(field, list_of_names):
    for idx, name in enumerate(list_of_names):
        if name in field:
            return idx
    ed = [edit_distance(field[:smaller(len(field),len(x)+1)], x) for x in list_of_names]
    minidx=0
    for i in range(1,len(list_of_names)):
        if ed[minidx]>ed[i]:
            minidx = i     
    return minidx


# In[31]:

def auto_discover_columns(S,Z):
    checks = ['(AUTO-DISCOVER COLUMNS)' in z[6] for z in Z]
    if True not in checks:
        return S, Z, 0
    S2=copy.deepcopy(S)
    Z2=copy.deepcopy(Z)
    nrows = 1+np.max([s[8] for s in S2])
    for col, checkthis in enumerate(checks):
        if checkthis:
            allRows =[s[6] for s in S if s[9]==col]
            allRows =[x.split('\n') for x in allRows]
            allRows = [item for Y in allRows for item in Y]
            allRows = [separate_field_value(x) for x in allRows]
            allRowsField = [x[0] for x in allRows]
            allRowsField = [x for x in allRowsField if x!='SPURIOUS NAME' ]
            candidates = sorted(list(set(allRowsField)))
            candidates = add_counts(candidates,allRowsField)
            candidates.sort(key=lambda x:x[1], reverse=True)
            candidates = elimination_round(candidates)
            z_old = copy.deepcopy(Z2[col])
            Z2 = [Z[i] for i in range(1,len(Z)) if i!=col]
            N = len(candidates)
            print 'Replacing mega-column {0} with {1} auto-discovered columns'.format(col,N)
            for idx, hdrname in enumerate(candidates):
                z_new=copy.deepcopy(z_old)
                z_new[6] = hdrname.upper()
                Z2 = flatten_list([Z2[:col+idx],z_new,Z2[col+idx+1:]])
            for i in range(len(S2)):
                if S2[i][9]>col:
                    S2[i][9] += N-1
            for row in range(nrows):
                s_old = [s for s in S2 if s[8]==row and s[9]==col] 
                S2 = [s for s in S2 if s[8]!=row or s[9]!=col]
                if len(s_old)!=1:
                    continue
                else:
                    s_old=s_old[0]
                    thisRow = [separate_field_value(x) for x in s_old[6].split('\n')]
                    thisRow = [x for x in thisRow if x[0]!='SPURIOUS NAME']
                    for idx, field_value in enumerate(thisRow):
                        field, value = field_value
                        nearestMatch = match_nearest_column_name(field,candidates)
                        s_new = copy.deepcopy(s_old)
                        s_new[6]=value
                        s_new[9]+=nearestMatch
                        S2.append(s_new)
    S2 = sort_segments_row_col(S2)
    return S2, Z2, 1


# ## ::::::::::::::::::::::::::: Drop single-entry columns :::::::::::::::::::::::::::

# In[32]:

def drop_single_entry_columns(S,Z,changes):
    if len(S)==0:
        return S,Z
    nrows = 1+np.max([s[8] for s in S])
    if not changes or nrows<5:
        return S, Z 
    S2=copy.deepcopy(S)
    Z2=copy.deepcopy(Z)
    for col in range(len(Z)):
        num_entries= len([s for s in S if s[9]==col]) 
        if num_entries>1:
            continue
        Z2 = flatten_list([Z2[:col],Z2[col+1:]])
        S2 = drop_column(S2,col)
        for i in range(len(S2)):
            if S2[i][9]>col:
                S2[i][9] -=1
    return S2,Z2


# ## ::::::::::::::::::::::::::: Final Cleanup routines :::::::::::::::::::::::::::

# In[33]:

def cleanup_cell_values(S):
    S2 = []
    for seg in S:
        seg2 = copy.deepcopy(seg)
        value=seg2[6]
        if value in [' ',':',' :',': ','_']: ## DROP Isolated characters
            value = ''
        for i in range(3): ## DROP stuff with UNUSUAL start
            if (len(value)>1) and (value[0] in [':',' ']):
                value=value[1:]
        for i in range(3): ## DROP stuff with UNUSUAL end
            if (len(value)>1) and (value[-1] in [':',' ','\n']):
                value=value[:-1]
        if len(value)>0:
            seg2[6] = value
            S2.append(seg2)
    return S2


# In[34]:

def drop_fixed_text_column(S,Z):
    S2 = copy.deepcopy(S)
    for idx,seg in enumerate(S2):
        if '(FIXED TEXT)' in Z[seg[9]][6]:
            S2[idx][7] = -1
            S2[idx][8] = -1
            S2[idx][9] = -1
    S2 = drop_unassigned_segments(S2)
    S2 = renumber_columns(S2)
    Z2 = [z for z in Z if '(FIXED TEXT)' not in z[6]]
    return S2, Z2


# In[35]:

def fix_column_header_names(Z,SPCL_INSTRUCTIONS):
    Z2 = copy.deepcopy(Z)
    for idx,z in enumerate(Z2):
        textstring = z[6]
        for instr in SPCL_INSTRUCTIONS:
            textstring = textstring.replace(' '+instr,'')
        Z2[idx][6] = textstring
    return Z2


# ## ::::::::::::::::::::::::::: The TABLE EXTRACTION Algorithm :::::::::::::::::::::::::::

# In[36]:

def table_extraction_algorithm(S,Z,parameters,mode='USE_MEGAROWS',DEBUG=0,image=[]):
    height, width, table_start, table_end, fn = parameters 
    print('##### Begin Table Extraction {0} #####'.format(fn))
    print_segments(Z)
    #print_segments(S)
    SPCL_INSTRUCTIONS = ['(REF)','(ref)','(NUMERIC)','(DOLLAR AMOUNT)','(TELEPHONE NUMBER)',
                         '(FIXED TEXT)','(AUTO-DISCOVER COLUMNS)','(SIGNATURE EXTRACTION)',
                         '(NON BLANK)','(STRIP LABEL)']
    S1        = breakup_segments_special_characters_at_2_ends(S)
    S2        = breakup_mega_segments_joined_by_colon(S1)
    Z1        = adjust_extraction_zones(Z,table_start,S2)
    S3        = region_of_interest(S2, height, width, table_end, Z1)
    S4        = drop_outlier_tall_segments(S3)
    MR        = compute_megarows(S4)
    S5        = assign_megarowID(S4, MR)
    Z2        = assign_megarowID_range(Z1, MR)
    RC        = find_reference_column(S5, Z1[0])
    S6        = assign_row_col(S5, Z2, RC, mode=mode)
    S7        = drop_stray_text_last_row(S6,Z2,MR)
    S8        = drop_unassigned_segments(S7)
    S9        = sort_segments_row_col(S8)
    S10       = join_text_per_cell(S9)
    S11       = drop_on_validation_checks(S10,Z2)
    S12       = annotate_signature_coordinates(S11,Z2)
    S13       = strip_label(S12,Z2)
    S14       = signature_extraction(image,'dbg/'+fn,S13,Z2)
    S15,Z3,xx = auto_discover_columns(S14,Z2)
    S16,Z4    = drop_single_entry_columns(S15,Z3,xx)
    S17       = cleanup_cell_values(S16)
    S18,Z5    = drop_fixed_text_column(S17,Z4)
    Z6        = fix_column_header_names(Z5,SPCL_INSTRUCTIONS)
    if DEBUG:
        annotate_rectangles(image,Z1,'dbg/'+fn+'_1_botdesign.jpg',(0,0,255),2,ENTRY0_REFCOL=1)
        annotate_rectangles(image,S,'dbg/'+fn+'_2_segmentation.jpg',(0,0,255),2)
        annotate_rectangles(image,MR,'dbg/'+fn+'_3_megarows.jpg',(255,0,0),2)
        annotate_rectangles(image,RC,'dbg/'+fn+'_4_refcolumn.jpg',(0,0,255),2)
    return Z6,S18


# ## ::::::::::::::::::::::::::: Reading / Writing JSON :::::::::::::::::::::::::::

# In[37]:

def json2xls(fn_json):
    J = json.load(open(fn_json))
    for tblid,tbl in enumerate(J['data']['Tables']):
        book = xlwt.Workbook(encoding="utf-8")
        XLS = book.add_sheet("Sheet 1")
        allcolumnids = list(set([col['columnid'] for row in tbl['Rows'] for col in row['Columns'] ]))
        allcolumnids.sort()
        allcolumnids = [x[-9:] for x in allcolumnids]
        for colidx,coltxt in enumerate(allcolumnids):
            XLS.write(0,colidx,coltxt)
        for rowidx,row in enumerate(tbl['Rows']):
            for col in row['Columns']:
                colidx = allcolumnids.index(col['columnid'][-9:])
                XLS.write(1+rowidx,colidx,col['Text'])
        book.save(fn_json.replace('.json','')+'_' +str(tblid+1)+'.xls')
    
    
def write_output_json(fn,T,J,Z_all):
    alltables = []
    for tblid in range(len(J['botdesigninfo']['tables'])):
        columnids = [x[5] for x in Z_all[tblid][2]]
        defids = [x[6] for x in Z_all[tblid][2]]
        table_head, table_body = T[tblid]
        nrows = 1+np.max([s[8] for s in table_body])
        allrows = []
        for row in range(nrows):
            R = [s for s in table_body if s[8]==row]
            singlerow=[{"id":       "ROW{0}_COL{1}".format(r[8],r[9]),
                        "columnid": columnids[r[9]],
                        "defid":    defids[r[9]],
                        "Text":     r[6],
                        "Bound":     {"X":      r[0], 
                                      "Y":      r[1], 
                                      "Width":  r[2]-r[0]+1, 
                                      "Height": r[3]-r[1]+1
                                     }
                       } for r in R]
            allrows.append({"Columns":singlerow})
        alltables.append({'Id': J['botdesigninfo']['tables'][tblid]['id'], 'Rows': allrows})

    fulloutput = {"issuccess":True, "error":"", "data":{"Tables":alltables}}
    js = json.dumps(fulloutput, sort_keys=True, indent=4, separators=(',', ': '))
    with open(fn, 'w') as f:
        f.write(js)

def write_empty_json(fn):
    fulloutput = {"issuccess":False, "error":"Empty Extraction", "data":{}}
    js = json.dumps(fulloutput, sort_keys=True, indent=4, separators=(',', ': '))
    with open(fn, 'w') as f:
        f.write(js)


# In[38]:

def Alternative_Table_Extraction(INPUT_JSON,OUTPUT_JSON):
    print("######################## File {0} ###############".format(INPUT_JSON))
  ## Directory initializations
    if DEBUG:
        os.system('rm -rf dbg')
        os.system('mkdir dbg')

   ## Read Input json
    J = json.load(open(INPUT_JSON))

   ## Extract segment list
    S=extract_segments_from_json(J)
 
   ## Extract user-definitions
    Z_all=extract_user_definitions_from_json(J)
    Z1 = []; table_starts=[]; table_ends=[]
    for z in Z_all:
        Z=[]
        boxes=z[2]
        for b in boxes:
            Z.append([b[1],b[2],b[3],b[4],4,99,b[0],-1,-1,-1])
        Z1.append(Z)
        table_starts.append(z[0])
        table_ends.append(z[1])

   ## Image characteristics
    I = []
    #I = cv2.imread('../pipeline2/tif/005.tif')
    #I = cv2.imread('../pipeline2/tif/012.tif')
    #I = cv2.imread('../pipeline2/tif/014.tif')
    #I = cv2.imread('../pipeline2/tif/005.tif')
    #I = cv2.imread('../pipeline2/tif/020.tif')
    #I = cv2.imread('STORE/IQTest/IQTest/Case1/Train.tiff')
    #I = cv2.imread('STORE/IQTest/IQTest/Case1/IQTest.tiff')
    #I = cv2.imread('STORE/IQTest/IQTest/Case2/Train.tiff')
    #I = cv2.imread('STORE/IQTest/IQTest/Case2/IQTest.tiff')
    #I = cv2.imread('STORE/IQTest/IQTest/Case3/Train.tiff')
    #I = cv2.imread('STORE/IQTest/IQTest/Case3/IQTest.tiff')
    #print I.shape
    

    Pages = J['botdesigninfo']['layoutdocumentproperties']['pageproperties']
    height = Pages[0]['height'] * len(Pages)
    width=Pages[0]['width']  
#    table_ends[0] = 'XXXXXXXXXXXXXXX'  # PATCH for Saeed17, Saeed19
   ############################## TESTING CODE (Start) ###############################
    if ENABLE_TESTING_TABLE_START_END:
        table_starts[0][0] = table_starts[0][0] + '|GARBAGE1|GARBAGE2'
        table_ends[0]      = table_ends[0] + '|GARBAGE3|GARBAGE4'
        table_starts[1][0] = 'GARBAGE5|GARBAGE6|' + table_starts[1][0]
        table_ends[1]      = 'GARBAGE7|GARBAGE8|' + table_ends[1]
        table_starts[2][0] = 'GARBAGE9|' + table_starts[2][0] + '|GARBAGE10'
        table_ends[2]      = 'GARBAGE11|' + table_ends[2] + '|GARBAGE12'
        print('table_starts={0}'.format(table_starts))
        print('table_ends={0}'.format(table_ends))
    if ENABLE_FIX_INPUT_JSON_FOR_TBL2:
        Z1[1][0][0]-=30
        Z1[1][0][2]+=30
    if ENABLE_TESTING_TABLE_SIGNATURE_EXTRACTION:
        Z1[1][2][6] = z[2][6] + ' (SIGNATURE EXTRACTION)' # Testing SIGNATURE EXTRACTION 
    if ENABLE_TESTING_TABLE_STRIP_LABEL:
        Z1[1][5][6] =  'Residential (STRIP LABEL)' # Testing STRIP LABEL
    if ENABLE_TESTING_TABLE_AUTO_DISCOVER_COLUMNS:
        Z1[2][12][6] = z2[12][6] + ' (AUTO-DISCOVER COLUMNS)' #Testing AUTO-DISCOVER COLUMNS       
   ############################## TESTING CODE (End) ###############################
    
        
  ## Extract Tables one-by-one
    TABLES = []
    any_empty_table = 0
    for tid,z in enumerate(Z1):
        start = timer()
        z2 = copy.deepcopy(z)
        fn='tbl{0}'.format(1+tid)
        parameters = [height,width,table_starts[tid],table_ends[tid],fn]
        #I = np.zeros((height,width,3),dtype=np.uint8) + 255
        #table_head,table_body = table_extraction_algorithm(S,z2,parameters,mode='USE_MEGAROWS',DEBUG=1,image=I)
        table_head,table_body = table_extraction_algorithm(S,z2,parameters,mode='USE_MEGAROWS',DEBUG=0,image=I)
        nrows=0; ncols=0; ncell=0
        if len(table_body)>0:
            nrows = 1+np.max([s[8] for s in table_body])
        if len(table_body)>0:
            ncols = 1+np.max([s[9] for s in table_body])
        ncell = len(table_body)
        nchar = np.sum([len(s[6]) for s in table_body])
        if DEBUG:
            dump_debug_xls_file("dbg/{0}.xls".format(fn),table_head,table_body)
        TABLES.append([table_head,table_body])
        end = timer()
        print('E X T R A C T I O N : {0} rows, {1} cols, {2} cells, {3} chars ({4} sec)\n\n'.format(nrows,ncols,ncell,nchar,end-start))
        if nrows==0 or ncols==0:
            any_empty_table = 1
    
    if any_empty_table:
        write_empty_json(OUTPUT_JSON)
    else:        
        ## Write Output JSON
        write_output_json(OUTPUT_JSON,TABLES,J,Z_all)
        if DEBUG:
            json2xls(OUTPUT_JSON)


# In[39]:

#Alternative_Table_Extraction(sys.argv[1], sys.argv[2])
NISHITS_REGRESSION_TESTS = 0
IQTEST_REGRESSION_TESTS = 0
if NISHITS_REGRESSION_TESTS:
    Alternative_Table_Extraction('inputs/Input_Doc1_23_Pages.json','outputs/Output_Doc1_23.json')
    Alternative_Table_Extraction('inputs/Input_Doc1_23_Pages_FIX.json','outputs/Output_Doc1_23_FIX.json')
    Alternative_Table_Extraction('inputs/Input_Doc2_30_Pages.json','outputs/Output_Doc2_30.json')
    Alternative_Table_Extraction('inputs/Input_Doc2_30_Pages_FIX.json','outputs/Output_Doc2_30_FIX.json')
    Alternative_Table_Extraction('inputs/Input_Ridhi.json','outputs/Output_Ridhi.json')
    Alternative_Table_Extraction('inputs/Input_Ridhi_FIX.json','outputs/Output_Ridhi_FIX.json')
    Alternative_Table_Extraction('inputs/Input_Yogesh.json','outputs/Output_Yogesh.json')
    Alternative_Table_Extraction('inputs/Input_Yogesh_FIX.json','outputs/Output_Yogesh_FIX.json')
    Alternative_Table_Extraction('inputs/Input_Saeed1.json','outputs/Output_Saeed1.json')
    Alternative_Table_Extraction('inputs/Input_Saeed1_FIX.json','outputs/Output_Saeed1_FIX.json')
    Alternative_Table_Extraction('inputs/Input_Saeed2.json','outputs/Output_Saeed2.json')
    Alternative_Table_Extraction('inputs/Input_Saeed3.json','outputs/Output_Saeed3.json')
    Alternative_Table_Extraction('inputs/Input_Saeed4.json','outputs/Output_Saeed4.json')
    Alternative_Table_Extraction('inputs/Input_Saeed5.json','outputs/Output_Saeed5.json')
    Alternative_Table_Extraction('inputs/Input_Saeed6.json','outputs/Output_Saeed6.json')
    Alternative_Table_Extraction('inputs/Input_Saeed7.json','outputs/Output_Saeed7.json')
    Alternative_Table_Extraction('inputs/Input_Saeed8.json','outputs/Output_Saeed8.json')
    Alternative_Table_Extraction('inputs/Input_Saeed9.json','outputs/Output_Saeed9.json')
    Alternative_Table_Extraction('inputs/Input_Saeed10.json','outputs/Output_Saeed10.json')
    Alternative_Table_Extraction('inputs/Input_Saeed11.json','outputs/Output_Saeed11.json')
    Alternative_Table_Extraction('inputs/Input_Saeed12.json','outputs/Output_Saeed12.json')
    Alternative_Table_Extraction('inputs/Input_Saeed13.json','outputs/Output_Saeed13.json')
    Alternative_Table_Extraction('inputs/Input_Saeed14.json','outputs/Output_Saeed14.json')
    Alternative_Table_Extraction('inputs/Input_Saeed15.json','outputs/Output_Saeed15.json')
    Alternative_Table_Extraction('inputs/Input_Saeed16.json','outputs/Output_Saeed16.json')
    Alternative_Table_Extraction('inputs/Input_Saeed17.json','outputs/Output_Saeed17.json')
    Alternative_Table_Extraction('inputs/Input_Saeed18.json','outputs/Output_Saeed18.json')
    Alternative_Table_Extraction('inputs/Input_Saeed19.json','outputs/Output_Saeed19.json')
    Alternative_Table_Extraction('inputs/Input_Saeed20.json','outputs/Output_Saeed20.json')
    Alternative_Table_Extraction('inputs/Input_Saeed20_FIX.json','outputs/Output_Saeed20_FIX.json')
    Alternative_Table_Extraction('inputs/Input_Saeed21.json','outputs/Output_Saeed21.json')
    Alternative_Table_Extraction('inputs/Input_Pradeep1.json','outputs/Output_Pradeep1.json')
    Alternative_Table_Extraction('inputs/Input_Pradeep2.json','outputs/Output_Pradeep2.json')
    Alternative_Table_Extraction('inputs/Input_Saeed22.json','outputs/Output_Saeed22.json')
    Alternative_Table_Extraction('inputs/Input_Saeed23.json','outputs/Output_Saeed23.json')
    Alternative_Table_Extraction('inputs/Input_Saeed24.json','outputs/Output_Saeed24.json')
    Alternative_Table_Extraction('inputs/Input_Saeed25.json','outputs/Output_Saeed25.json')
    Alternative_Table_Extraction('inputs/Input_Saeed26.json','outputs/Output_Saeed26.json')
    Alternative_Table_Extraction('inputs/Input_Saeed27.json','outputs/Output_Saeed27.json')

### IQ TEST INVESTIGATIONS:
### Saeed's 10 examples for IQ Test failures & *_FIX* versions in which some of #
### these have been fixed with changes in BOT design i.e., mods in json files
if IQTEST_REGRESSION_TESTS:
    os.system('rm -rf dbg_IQTest/*')
    Alternative_Table_Extraction('inputs/Input_IQTest1a.json','outputs/Output_IQTest1a.json')
    os.system('cp -r dbg dbg_IQTest/case1a')
    Alternative_Table_Extraction('inputs/Input_IQTest1b.json','outputs/Output_IQTest1b.json')
    os.system('cp -r dbg dbg_IQTest/case1b')
    Alternative_Table_Extraction('inputs/Input_IQTest2a.json','outputs/Output_IQTest2a.json')
    os.system('cp -r dbg dbg_IQTest/case2a')
    Alternative_Table_Extraction('inputs/Input_IQTest2b.json','outputs/Output_IQTest2b.json')
    os.system('cp -r dbg dbg_IQTest/case2b')
    Alternative_Table_Extraction('inputs/Input_IQTest2a_FIX.json','outputs/Output_IQTest2a_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case2a_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest2b_FIX.json','outputs/Output_IQTest2b_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case2b_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest3a.json','outputs/Output_IQTest3a.json')
    os.system('cp -r dbg dbg_IQTest/case3a')
    Alternative_Table_Extraction('inputs/Input_IQTest3b.json','outputs/Output_IQTest3b.json')
    os.system('cp -r dbg dbg_IQTest/case3b')
    Alternative_Table_Extraction('inputs/Input_IQTest4a.json','outputs/Output_IQTest4a.json')
    os.system('cp -r dbg dbg_IQTest/case4a')
    Alternative_Table_Extraction('inputs/Input_IQTest4b.json','outputs/Output_IQTest4b.json')
    os.system('cp -r dbg dbg_IQTest/case4b')
    Alternative_Table_Extraction('inputs/Input_IQTest5a.json','outputs/Output_IQTest5a.json')
    os.system('cp -r dbg dbg_IQTest/case5a')
    Alternative_Table_Extraction('inputs/Input_IQTest5b.json','outputs/Output_IQTest5b.json')
    os.system('cp -r dbg dbg_IQTest/case5b')
    Alternative_Table_Extraction('inputs/Input_IQTest5a_FIX.json','outputs/Output_IQTest5a_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case5a_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest5b_FIX.json','outputs/Output_IQTest5b_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case5b_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest6a.json','outputs/Output_IQTest6a.json')
    os.system('cp -r dbg dbg_IQTest/case6a')
    Alternative_Table_Extraction('inputs/Input_IQTest6b.json','outputs/Output_IQTest6b.json')
    os.system('cp -r dbg dbg_IQTest/case6b')
    Alternative_Table_Extraction('inputs/Input_IQTest7a.json','outputs/Output_IQTest7a.json')
    os.system('cp -r dbg dbg_IQTest/case7a')
    Alternative_Table_Extraction('inputs/Input_IQTest7b.json','outputs/Output_IQTest7b.json')
    os.system('cp -r dbg dbg_IQTest/case7b')
    Alternative_Table_Extraction('inputs/Input_IQTest7a_FIX.json','outputs/Output_IQTest7a_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case7a_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest7b_FIX.json','outputs/Output_IQTest7b_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case7b_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest8a.json','outputs/Output_IQTest8a.json')
    os.system('cp -r dbg dbg_IQTest/case8a')
    Alternative_Table_Extraction('inputs/Input_IQTest8b.json','outputs/Output_IQTest8b.json')
    os.system('cp -r dbg dbg_IQTest/case8b')
    Alternative_Table_Extraction('inputs/Input_IQTest8a_FIX.json','outputs/Output_IQTest8a_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case8a_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest8b_FIX.json','outputs/Output_IQTest8b_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case8b_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest9a.json','outputs/Output_IQTest9a.json')
    os.system('cp -r dbg dbg_IQTest/case9a')
    Alternative_Table_Extraction('inputs/Input_IQTest9b.json','outputs/Output_IQTest9b.json')
    os.system('cp -r dbg dbg_IQTest/case9b')
    Alternative_Table_Extraction('inputs/Input_IQTest9a_FIX.json','outputs/Output_IQTest9a_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case9a_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest9b_FIX.json','outputs/Output_IQTest9b_FIX.json')
    os.system('cp -r dbg dbg_IQTest/case9b_FIX')
    Alternative_Table_Extraction('inputs/Input_IQTest10a.json','outputs/Output_IQTest10a.json')
    os.system('cp -r dbg dbg_IQTest/case10a')
    Alternative_Table_Extraction('inputs/Input_IQTest10b.json','outputs/Output_IQTest10b.json')
    os.system('cp -r dbg dbg_IQTest/case10b')
    Alternative_Table_Extraction('inputs/Input_Footer1a.json','outputs/Output_Footer1a.json')
    os.system('cp -r dbg dbg_IQTest/footer1a')
    Alternative_Table_Extraction('inputs/Input_Footer1b.json','outputs/Output_Footer1b.json')
    os.system('cp -r dbg dbg_IQTest/footer1b')
    Alternative_Table_Extraction('inputs/Input_Footer1a_FIX.json','outputs/Output_Footer1a_FIX.json')
    os.system('cp -r dbg dbg_IQTest/footer1a_FIX')
    Alternative_Table_Extraction('inputs/Input_Footer1b_FIX.json','outputs/Output_Footer1b_FIX.json')
    os.system('cp -r dbg dbg_IQTest/footer1b_FIX')

