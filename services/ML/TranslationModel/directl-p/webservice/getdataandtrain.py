import translationmodel as translationModel
import databasemanager as dbManager
import logging

class TrainingData:            
    def GetTrainingData(self, fieldId):
        try:
            DataBase = dbManager.DBManager()
            db       = DataBase.initDB()            
            return self.getDataForFieldId(db, fieldId)
        except:
            logging.debug("Exception: GetDataAndTrain::TrainingData::GetTrainingData()")
            return None, None
        
    def getDataForFieldId(self, db, fieldId):
        try:
            if db==None:
                return None, None
            cur      = db.cursor()
            Query    = "SELECT * FROM trainingdata where fieldid='" +  fieldId+"'"        
            cur.execute(Query)        

            originalData  = []
            correctedData = []
            
            for row in cur.fetchall():
                originalData.append(row[0])
                correctedData.append(row[1])
            return originalData, correctedData
        except:
            logging.debug("Exception: GetDataAndTrain::TrainingData::getDataForFieldId()")
            return None, None
            


class TrainModel:
    def UpdateOrCreateModels(self, fieldId):
        try:
            #self.logFilePath = "TrianModel.log"
            #logging.basicConfig(filename = self.logFilePath, level = logging.DEBUG)
            logging.debug("Trainign Started..")
            trainingData                = TrainingData()            
            originalData, correctedData = trainingData.GetTrainingData(fieldId)
            if len(originalData) < 3:
                logging.debug("Required More data to train")
                logging.debug("Length Of Data: " + str(len(originalData)))
                return "Fail"
            else:
                logging.debug("Trainning Data is enough to start Learning.")
            
            transModel                  = translationModel.TranslationModel()
            #modelPath                   = "model/model_"+fieldId + ".pkl"
            status                      = transModel.TrainModel(originalData, correctedData, fieldId)
            return status
        except:
            print("Exception: GetDataAndTrain::TrainingData::UpdateOrCreateModels()")
            return 'Fail'            

