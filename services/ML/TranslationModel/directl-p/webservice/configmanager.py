import sys
import os
import json

class ConfigManager:
    def getConfiguration(self):
        filePath = self.GetCurrentDir()
        if(filePath != ""):
            filePath = filePath+'\CognitiveServiceConfiguration.json'
        else:
            filePath = 'CognitiveServiceConfiguration.json'

        jsonObj = {}
        if(os.path.exists(filePath) == True):
            print("Configuration File is Found")

            with open(filePath, "r") as myfile:
                fileData = myfile.read(-1)
                jsonObj = json.loads(fileData)
        else:
            print("Configuration File is not Found")
        return jsonObj
    
    def GetCurrentDir(self):
        if getattr(sys, 'frozen', False):
            application_path = os.path.dirname(sys.executable)
        elif __file__:
            application_path = os.path.dirname(__file__)
        return application_path