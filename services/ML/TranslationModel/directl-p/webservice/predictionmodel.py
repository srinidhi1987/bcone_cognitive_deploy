from __future__ import print_function

from jsm import JSM
import sys
import os
import logging
import json
class PredictFieldValue:

    def PredictValueWithConfidence(self, fieldid, originalvalue, n_best=2):
        try:
            if(os.path.exists(fieldid+'.A.B') == False):
                logging.debug("Model file is not found, May be training for this field is not available")
                return ("{'data'" +": ["''"]}")
            model   = JSM(fieldid)

            pred    = model.predict([originalvalue],probs_out=True)
            logging.debug("Prediction Finished")
            currdata = pred[0]
            report = []
            logging.debug(currdata)
            for i in range(len(currdata)):
                if (currdata[i]==''):
                    logging.debug("Prediction is Blank, Need more training data")
                    return "{'data' : ''}"
                if(currdata[i][0] == '__NULL__' or currdata[i][0] == 'NULL' or currdata[i][0] == ''):
                    logging.debug("Removing Prediction as it is 'NULL' or blank")
                    continue
                if (i > 0 and (currdata[i][0] == currdata[i-1][0])):
                    logging.debug("Removing Second Prediction as first and second predictions are same")
                    continue

                predictedData  = str(currdata[i][0]).replace("['", '')
                predictedData  = str(currdata[i][0]).replace("']", '')
                confidence = currdata[i][1]
                suggetion={'value':predictedData, 'confidence':str(confidence)}
                report.append(suggetion)
            data = json.dumps({'data': report})
            return data
        except:
            logging.error("Exception: PredictionModel::PredictFieldValue::PredictValueWithConfidence()", sys.exc_info()[0])
            print("Exception: PredictionModel::PredictFieldValue::PredictValueWithConfidence()", sys.exc_info()[0])
            return ("{'data'" +": ["''"]}")


    
"""pr = PredictFieldValue()
ocrVal=pr.PredictValueWithConfidence('FIELD005', 'ill')
print (ocrVal)"""
