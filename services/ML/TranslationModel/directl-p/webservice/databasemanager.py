import pyodbc
import sys
import os
import logging
import json
import configmanager

from AES import decrypt_b64_string


class DBManager:
    def initDB(self):
        # connectionString = "DRIVER={{SQL Server Native Client 11.0}};SERVER={0}; database={1};UID={2};PWD={3}".format("localhost","MLData","sa",self.GetDBPassword())
        config = configmanager.ConfigManager()
        jsonObj = config.getConfiguration()
        settings = self.load_settings()
        connectionString = jsonObj["MLDBSettings"]["DbConnectionString"]
        if 'SQLWindowsAuthentication' in settings and settings['SQLWindowsAuthentication'].lower() == 'true':
            logging.info("Attempting to use Windows Authentication to connect to SQL server for ML data")
            db = self.get_db_connection_wa(connectionString)
        else:
            logging.info("Attempting to use password authentication to connect to SQL server for ML data")
            dbPassword = "PWD={};".format(self.GetDBPassword())
            connectionString = connectionString + dbPassword
            db = pyodbc.connect(connectionString)
        return db

    def get_db_connection_wa(self,connectionString):
        """Use Windows Authentication to connect to DB"""
        try: # remove UID from connectionString
            uid_index = connectionString.index('UID')
            semicolon_index = connectionString[uid_index:].index(';')
            connectionString = connectionString[:uid_index] + connectionString[uid_index + semicolon_index+1:]
        except ValueError:
            logging.info("UID not found in db connection config; continuing to try WA for DB connection")
        if connectionString[-1] != ';':
            connectionString += ';'
        connectionString = connectionString+'Trusted_Connection=yes;' # Trusted_Connection=yes means WA
        db = pyodbc.connect(connectionString)
        return db

    def GetCurrentDir(self):
        if getattr(sys, 'frozen', False):
            application_path = os.path.dirname(sys.executable)
        elif __file__:
            application_path = os.path.dirname(__file__)
        return application_path

    def GetDBPassword(self):
        filePath = self.GetCurrentDir()
        logging.debug(filePath)
        filePath = os.path.dirname(os.path.dirname(filePath))
        filePath = filePath + "\Configurations\cognitive.properties"
        password = ""
        if os.path.exists(filePath) == True:
            logging.debug("config property file is found")
            props = self.load_properties(filePath)
            encrypted_password = props['database.password']
            try:
                password = decrypt_b64_string(encrypted_password)
            except ValueError as e:
                logging.error(e)
        else:
            logging.error("config property file is not found")
        return password

    def load_properties(self, filepath, sep='=', comment_char='#'):
        props = {}
        with open(filepath, "rt") as f:
            for line in f:
                l = line.strip()
                if l and not l.startswith(comment_char):
                    key_value = l.split(sep)
                    key = key_value[0].strip()
                    value = sep.join(key_value[1:]).strip().strip('"')
                    props[key] = value
        return props

    def load_settings(self):
        filepath = self.GetCurrentDir()
        logging.debug(filepath)
        filepath = os.path.dirname(os.path.dirname(filepath))
        filepath = filepath + "\Configurations\Settings.txt"
        settings = {}
        print(filepath)
        with open(filepath, 'r') as f:
            for line in f:
                try:
                    setting, value = line.split('=')
                except ValueError:
                    logging.error("Malformed line in settings file: {}".format(filepath))
                    continue
                settings[setting] = value.strip()
        return settings


"""DB = DBManager()
DB.initDB()
except:
print("Unexpected error:", sys.exc_info()[0])
print("Exception: databasemanager::DBManager::initDB()")
return None"""
