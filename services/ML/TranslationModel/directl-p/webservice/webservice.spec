# -*- mode: python -*-

block_cipher = None


a = Analysis(['webservice.py'],
             pathex=['D:\\GitClone\\cognitive\\services\\ML\\TranslationModel\\directl-p\\webservice'],
             binaries=[],
             datas=[],
             hiddenimports=['scipy._lib.messagestream','webservice'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='webservice',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='webservice')
