#!/usr/bin/env python
from win32com.shell import shell, shellcon
import logging
import os
import mllogging

#The method GetLogDirectory is deleted because it is not required to be here as added another class to init the logging.
mllog = mllogging.MLLogging()
mllog.initLogging("MLWebService.log")

import win32serviceutil
import win32service
import win32event
import servicemanager
import schedule
import time
import sys


import web              as web
import getdataandtrain  as getDataAndTrain
import predictionmodel  as predictionModel
import updatefieldsindb as updateFieldInDB
import configmanager
from web.wsgiserver import CherryPyWSGIServer
import json
from Alternative_Table_Extraction import Alternative_Table_Extraction
import tempfile

urls = (
    '/train' ,    'TrainTranslationModel',
    '/predict',   'PredictTranslationModel',
    '/value',     'UpdateFieldInDB',
    '/alt_tables', 'AltTables'
)

app = web.application(urls, globals(), True)
class TrainTranslationModel:
    def GET(self):
        try:
            logging.debug("/train")
            data = web.input()
            logging.debug(str(data))
            logging.debug("TrainModel Called")

            trainingModel  = getDataAndTrain.TrainModel()
            return trainingModel.UpdateOrCreateModels(data.fieldid)
        except:
            logging.error(str(data))
            print("Exception: webservice::getDataAndTrain::UpdateOrCreateModels()")
            return 'Fail'
            
class PredictTranslationModel:
    def GET(self):
        try:
            logging.debug("/predict")
            data              = web.input()
            logging.debug(data)
            predictFieldValue = predictionModel.PredictFieldValue()
            logging.debug("PredictValueWithConfidence called")
            pred = predictFieldValue.PredictValueWithConfidence(data.fieldid, data.originalvalue)
            logging.debug(pred)
            return pred
        except:
            logging.error("Exception: webservice::TrainTranslationModel::PredictTranslationModel()")
            return ("{'data'" +": ["''"]}")

class UpdateFieldInDB:
    def POST(self):
        try:
            logging.debug("/value")
            data            = web.data()
            updateField     = updateFieldInDB.UpdateFields()
            updateField.UpdateData(data.decode("utf-8"))
            return ''
        except:
            print("Exception: webservice::TrainTranslationModel::UpdateFieldInDB()")
            return ''

class AltTables(object):
    def POST(self):
        print("AltTable Called")
        data = web.data();
        temp_InputJson = "C:\\Input"#tempfile.NamedTemporaryFile().name
        temp_InputJson = temp_InputJson + ".json"
        print(temp_InputJson)
        with open(temp_InputJson, 'w') as f:
            f.write(data)

        temp_OutputJson = "C:\\Output"#tempfile.NamedTemporaryFile().name
        temp_OutputJson = temp_OutputJson + ".json"
        print(temp_OutputJson)
        ############################################################
        Alternative_Table_Extraction(temp_InputJson,temp_OutputJson)
        ############################################################
        return_data = json.dumps(json.load(open(temp_OutputJson)))
        #This code need to be uncommnet in next version. Right now these files are used for debuggung purpose.
        """if(os.path.exists(temp_OutputJson)):
            os.remove(temp_OutputJson)
        if(os.path.exists(temp_InputJson)):
            os.remove(temp_InputJson)"""
        
        return return_data

class MLService(win32serviceutil.ServiceFramework):
    _svc_name_ = 'Automation Anywhere Cognitive MLWeb Service'
    _svc_display_name_ = 'Automation Anywhere Cognitive MLWeb Service'
    
    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        
        self.isAlive = True
        
    def SvcStop(self):
        app.stop()
        self.isAlive = False
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)
        
    def SvcDoRun(self):
        self.isAlive = True

        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 
                              servicemanager.PYS_SERVICE_STARTED, (self._svc_name_, ''))
        self.SetSSLSetting()
        self.main()

    def  SetSSLSetting(self):
        config = configmanager.ConfigManager()
        jsonObj = config.getConfiguration()
        print (jsonObj)
        if(str(jsonObj["SSLSettings"]["RunWithSSL"]).lower() != "true"):
            logging.debug("Running Without SSL")
            return
        logging.debug("Running with SSL")
        ssl_cert = jsonObj["SSLSettings"]["SSL_CERT_FilePath"]#'/path-to-cert.crt'
        ssl_key = jsonObj["SSLSettings"]["SSL_CERT_Key"]#'/path-to-cert.key'
        CherryPyWSGIServer.ssl_certificate = ssl_cert
        CherryPyWSGIServer.ssl_private_key = ssl_key

    def GetCurrentDir(self):
        if getattr(sys, 'frozen', False):
            application_path = os.path.dirname(sys.executable)
        elif __file__:
            application_path = os.path.dirname(__file__)
        return application_path
#The method GetLogDirectory is deleted because it was a duplicate code.
    def main(self):
        rc = None
        config = configmanager.ConfigManager()
        jsonObj = config.getConfiguration()
        while rc != win32event.WAIT_OBJECT_0:
            logging.debug("Service Started")
            portNo = jsonObj['MLWebService']['port']
            logging.debug("Service started on port number {0}".format(portNo))
            web.httpserver.runsimple(app.wsgifunc(), ('0.0.0.0', int(portNo)))
            #app.run()
            rc = win32event.WaitForSingleObject(self.hWaitStop, win32event.INFINITE)
        logging.debug("Service Stopped")

if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(MLService)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(MLService)
"""
#Below Code is to Test with python script directly, not need to install service
if __name__ == "__main__":
    config = configmanager.ConfigManager()
    jsonObj = config.getConfiguration()
    print jsonObj
    if(str(jsonObj["SSLSettings"]["RunWithSSL"]).lower() != "true XXXX CHANGE ME XXXX"):
        print("SSL is OFF")
    else:
        print("Setting SSL")
        logging.debug(jsonObj["SSLSettings"]["SSL_CERT_FilePath"])
        logging.debug(jsonObj["SSLSettings"]["SSL_CERT_Key"])
        ssl_cert = jsonObj["SSLSettings"]["SSL_CERT_FilePath"]#'/path-to-cert.crt'
        ssl_key = jsonObj["SSLSettings"]["SSL_CERT_Key"]#'/path-to-cert.key'
        CherryPyWSGIServer.ssl_certificate = ssl_cert
        CherryPyWSGIServer.ssl_private_key = ssl_key
    portNo = jsonObj['MLWebService']['port']
    web.httpserver.runsimple(app.wsgifunc(), ('0.0.0.0', int(portNo)))
    app.run()
"""