import os
import sys
import base64
import logging

import pyaes

def get_current_dir():
    """TODO: move this method into a common utils
    """
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(sys.executable)
    elif __file__:
        application_path = os.path.dirname(__file__)
    return application_path

def read_key_from_file(key_basename='private.key'):
    """Reads private key file in <<InstallationPath>>\Configurations
    """
    filepath = get_current_dir()
    filepath = os.path.dirname(os.path.dirname(filepath))
    filepath = os.path.join(os.path.join(filepath, "Configurations"),key_basename)
    logging.debug("reading encryption key from: %s"%filepath)
    try:
        with open(filepath) as fi:
            key = fi.read()
    except IOError:
        logging.error("Could not read encryption key file from: %s"%filepath)
        return ''
    return key

_PRIVATE_KEY = read_key_from_file() 

def decrypt_b64_string(text,key=_PRIVATE_KEY):
    """
    Parameters of the cipher:
        Cipher algorithm: AES
        Secret key length: 256 bits
        Block size: 128 bits
        Method of operation: CBC
        IV size: 16 bytes
        Padding method: ISO-10126
        Validation signature: B7-B9-FB-14-D9-52-30-F4-C6-7A (the first byte is shown the last)
    Message encryption:
        Before a message is encrypted a 10-byte validation signature is appended to the
        beginning of the original message. After that the concatenated sequence is encrypted
        using an initialized cipher, a key and an initialization vector generated randomly.
        After that the IV is appended to the beginning of the encrypted byte sequence.
    Message decryption:
        16 byte initialization vector is removed from a the beginning of a message.
        The remaining sequence is decrypted using an initialized cipher, a key and
        the extracted IV. The first 10 bytes are removed from the decrypted message.
        If the removed 10 bytes are equal to the validation signature the remaining
        byte sequences is returned, otherwise an error is raised.
    args:
        text: base64 encoded string of with a length a mutiple of 16
        key: utf-8 encoded string of length 32
    returns:
        decrypted: decrypted string with 10 intial bytes removed
    throws:
        ValueError: if validation does not match or input text is not a valid length (mutiple of 16)
    """
    validation = "z\xc6\xf40R\xd9\x14\xfb\xb9\xb7"
    ascii_text = base64.b64decode(text)
    iv = ascii_text[:16]
    ciphertext = ascii_text[16:]
    if len(ciphertext) %16 != 0:
        raise ValueError("Ciphertext is not a valid length")
    decrypter = pyaes.Decrypter(pyaes.AESModeOfOperationCBC(key, iv))
    decrypted = decrypter.feed(ciphertext[:len(ciphertext) / 2])
    decrypted += decrypter.feed(ciphertext[len(ciphertext) / 2:])
    decrypted += decrypter.feed()
    if decrypted[:10] != validation:
        raise ValueError("Validation signature did not match expected value")
    return decrypted[10:]

if __name__ == '__main__':
    #tests
    #with _PRIVATE_KEY == "changeitchangeitchangeitchangeit"
    assert "Automation@123" == decrypt_b64_string("Iz5QqSOp20rAKiipI8q0GKUlnhMMY8VlI0NDSR3ETROhMIOPpf10EGg9nJThYnJH")
    assert "passmessage" == decrypt_b64_string("Iz5QqSOp20rAKiipI8q0GIs+uFh71PdHy+V0vY8Dkdh8QgLI4q0crgB6D05CV2jP")