import json
import databasemanager as databaseManager
import logging

class UpdateFields:
    def updateDataIntoDB(self, originalValue, correctedValue, fieldID):

        if(abs(len(originalValue) - len(correctedValue)) > 4):
            logging.debug("[Skip LearningData] Difference between length of the OriginalValue and CorrectedValue is more than 4, so it can not be fed into learning model")
            logging.debug("[Skip LearningData] OriginalValue: '{0}' and CorrectedValue: '{1}'".format(originalValue, correctedValue))
            return

        #try:

        dbManager = databaseManager.DBManager()
        db        = dbManager.initDB()
        if db == None:
            print ("DB Connection failed")
            return None
        self.saveDataIntoDB(db, originalValue, correctedValue, fieldID)
        db.commit()
        db.close()
        """except:
            print("Exception: Updatefieldsindb::UpdateFields::updateDataIntoDB()")"""
        
    def saveDataIntoDB(self, db, originalValue, correctedValue, fieldID):
        try:
            if db==None:
                return None, None

            cur   = db.cursor()
            Query = "INSERT INTO trainingdata(originalvalue, correctedvalue, fieldid) VALUES ('"+originalValue+"','"+ correctedValue+"','"+ fieldID+"')"
            cur.execute(Query)
        except:
            print("Exception: Updatefieldsindb::UpdateFields::saveDataIntoDB()")

    def UpdateData(self, trainingData):
       # try:
        data = json.loads(trainingData)
        print(data)
        for row in data:
            if row==None:
                continue
            originalValue  = row["originalValue"]
            if originalValue.strip() == "":
                continue
            correctedValue = row["correctedValue"]
            if correctedValue.strip() == "":
                continue
            fieldID        = row["fieldId"]
            self.updateDataIntoDB(originalValue, correctedValue, fieldID)
        """except:
            print("May be Invalid Json Data: " + trainingData)
            print("Exception: Updatefieldsindb::UpdateFields::saveDataIntoDB()")"""

