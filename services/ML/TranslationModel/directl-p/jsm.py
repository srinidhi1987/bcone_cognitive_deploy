# -*- coding: utf-8 -*-
import subprocess
import logging 
from pdb import set_trace
from pprint import pprint
import numpy as np
from scipy.optimize import fmin_bfgs
from math import log, exp
import io
open = io.open
import pickle

def sigmoid_calibration(df, y, sample_weight=None):
    """Probability Calibration with sigmoid method (Platt 2000)
    Parameters
    ----------
    df : ndarray, shape (n_samples,)
        The decision function or predict proba for the samples.
    y : ndarray, shape (n_samples,)
        The targets.
    sample_weight : array-like, shape = [n_samples] or None
        Sample weights. If None, then samples are equally weighted.
    Returns
    -------
    a : float
        The slope.
    b : float
        The intercept.
    References
    ----------
    Platt, "Probabilistic Outputs for Support Vector Machines"
    New BSD License

    Copyright (c) 2007–2017 The scikit-learn developers.
    All rights reserved.
    """

    F = df  # F follows Platt's notations
    tiny = np.finfo(np.float).tiny  # to avoid division by 0 warning

    # Bayesian priors (see Platt end of section 2.2)
    prior0 = float(np.sum(y <= 0))
    prior1 = y.shape[0] - prior0
    T = np.zeros(y.shape)
    T[y > 0] = (prior1 + 1.) / (prior1 + 2.)
    T[y <= 0] = 1. / (prior0 + 2.)
    T1 = 1. - T

    def objective(AB):
        # From Platt (beginning of Section 2.2)
        E = np.exp(AB[0] * F + AB[1])
        P = 1. / (1. + E)
        l = -(T * np.log(P + tiny) + T1 * np.log(1. - P + tiny))
        if sample_weight is not None:
            return (sample_weight * l).sum()
        else:
            return l.sum()

    def grad(AB):
        # gradient of the objective function
        E = np.exp(AB[0] * F + AB[1])
        P = 1. / (1. + E)
        TEP_minus_T1P = P * (T * E - T1)
        if sample_weight is not None:
            TEP_minus_T1P *= sample_weight
        dA = np.dot(TEP_minus_T1P, F)
        dB = np.sum(TEP_minus_T1P)
        return np.array([dA, dB])

    AB0 = np.array([0., log((prior0 + 1.) / (prior1 + 1.))])
    AB_ = fmin_bfgs(objective, AB0, fprime=grad, disp=False)
    return AB_[0], AB_[1]

def sigmoid_predict(decision_value,a,b):
    fApB = decision_value*a + b
    if fApB >= 0:
        return exp(-fApB) / (1.0 + exp(-fApB));
    else:
        return 1.0 / (1 + exp(fApB));


def pre_or_postProcess(str_list,pre_or_post):
    """Replace reseverd that m2m-aligner and directl use for aligning strings
    """
    align_sep = "|"
    align_sep_replacement = "`"#"\x1F" # ASCII unit separator control char
    white_space = ' '
    white_space_replacement = "`"#"\x1E"# ASCII record separator control char
    null_char = '_'
    null_char_replacement = "`"#"\x1D"  # ASCII group separator control char
    colon_char  = ":"
    colon_char_replace_ment = "`"#\x1A" #ASCII sub control char
    for i,_ in enumerate(str_list):
        if pre_or_post == 'PRE':
            str_list[i] = str_list[i].replace(align_sep,align_sep_replacement)
            str_list[i] = str_list[i].replace(white_space, white_space_replacement)
            str_list[i] = str_list[i].replace(null_char, null_char_replacement)
        elif pre_or_post == 'POST':
            str_list[i] = str_list[i].replace(white_space_replacement,white_space)
            str_list[i] = str_list[i].replace(align_sep,'')
            str_list[i] = str_list[i].replace(align_sep_replacement,align_sep)
            str_list[i] = str_list[i].replace('_','')
            str_list[i] = str_list[i].replace(':','')
    return str_list

class JSM(object):
    def __init__(self,model_name):
        self.model_name = model_name
        self.nVariantsLimit=2
        self.align_sep = "|"
        self.train_params = ""
        self.align_params = "--delX"

    def calibrateProbs(self,dest,pred):
        labels = []
        df = []
        for i,val in enumerate(dest):
            if val == pred[i][0][0]:
                labels.append(1)
            else:
                labels.append(-1)
            # NOTE: this breaks when nVariantsLimit !=2
            if val == pred[i][1][0]:
                labels.append(1)
            else:
                labels.append(-1)
            df.append(pred[i][0][1])
            df.append(pred[i][1][1])
        labels = np.array(labels)
        df = np.array(df)
        A, B = sigmoid_calibration(df,labels)
        with open(self.model_name+".A.B",'wb') as fo:
            pickle.dump([A,B],fo)

    def getAB(self):
        with open(self.model_name+'.A.B','rb') as fi:
            A, B = pickle.load(fi)
        return A,B

    def predict(self,source,probs_out=False):
        """Make corrections on a list of OCR ouputs
        @param source is a list of strings
        """
        source = pre_or_postProcess(source,'PRE')
        with open(self.model_name+'.toPredict','w', encoding='utf-8') as fo:
            for line in source:
                fo.write(self.align_sep.join(list(line))+self.align_sep+u'\n')
        
        exe = "directlp.exe"
        args = '--mi {0}.model.best -t {0}.toPredict --nBestTest {1} --outChar "" -a {0}.predicted {2}'.format(
                                                                            self.model_name,self.nVariantsLimit, self.train_params)
        call = "{0} {1}".format(exe,args)
        subprocess.call(call)
        pred = self.getPredFromDisc()
        if not probs_out:
            return pred
        else:
            A,B = self.getAB()
            for i,_ in enumerate(pred):
                # turn decision function into probs
                if pred[i][0][0] == 'NULL': # make sure null values have 0 probability
                    pred[i][0][1] = 0.0
                else:
                    pred[i][0][1] = sigmoid_predict(pred[i][0][1],A,B)
                if pred[i][1][0] == 'NULL':
                    pred[i][1][1] == 0.0
                else:
                    pred[i][1][1] = sigmoid_predict(pred[i][1][1],A,B)
                # remove second suggestion if it is the same as the first
                if pred[i][0][0] == pred[i][1][0]:
                    pred[i][1][0] = 'NULL'
                    pred[i][1][1] = 0.0
            return pred
    def fit(self,source,dest):
        """Trains the model on @ source a list of OCR outputs and @param dest a list of human-corrected strings
        """
        source = pre_or_postProcess(source,'PRE')
        dest = pre_or_postProcess(dest,'PRE')
        self.align(source,dest)
        exe = "directlp.exe"
        args = r'-f {0}.aligned --mo {0} --inChar ":" --nBest 10 {1}'.format(self.model_name,self.train_params)
        call = r'{0} {1}'.format(exe,args)
        subprocess.call(call)
        pred = self.predict(source)
        self.calibrateProbs(dest,pred)
    def align(self,source,dest):
        """Performs the subalignment needed by directlp
        """
        if len(source) != len(dest):
            logging.error("Input data and validation data do not match (length mismatch); cannot continue training")
            assert len(source) == len(dest)
        with open(self.model_name+'.toAlign','w', encoding='utf-8') as fo:
            for i in range(len(source)):
                s = u''.join(list(source[i]))
                d = u''.join(list(dest[i]))
                fo.write(u'{}\t{}\n'.format(s,d))
        exe = "m2m-aligner.exe"
        args = '--inputFile {0}.toAlign --outputFile {0}.aligned --sepChar "{1}" --inFormat "l2p" {2}'.format(self.model_name,self.align_sep,
                                                                                                                self.align_params)
        call = "{} {}".format(exe,args)
        subprocess.call(call)
    def getPredFromDisc(self):
        """Reads .phraseOut file the directlp writes to disc and creates a list of predictions and scores
        """
        pred = []
        with open(self.model_name+'.predicted.phraseOut', encoding='utf-8') as fi:
            tmp = []
            for line in fi:
                if line.isspace():
                    pred.append(tmp)
                    tmp = []
                    continue
                tmp.append(self.unFormatLine(line))
        return pred
    def unFormatLine(self,line):
        """used in getPredFromDisc to convert a line of .phraseOut format to the predited string and its score
        """
        line = line.split()
        score = float(line[-1])
        pred = line[1]
        pred = pre_or_postProcess([pred],'POST')[0]
        return [pred,score]
if __name__ == '__main__':
    import csv
    from sklearn.metrics import accuracy_score
    def readTestingData(file_path):
        source = []
        dest = []
        with open(file_path) as fi:
            reader = csv.reader(fi)
            for line in reader:
                source.append(line[0])
                dest.append(line[1])
        return source, dest
    def formatTestResults(pred,valid):
        for i,val in enumerate(valid):
            print "Valid:", val
            print "Pred1: {} Prob: {} \tPred2: {}".format(pred[i][0][0],pred[i][0][1],pred[i][1])
            print ""

    j = JSM('field000')
    ocr,valid = readTestingData('data/train_case_0.csv')
    j.fit(ocr,valid)
    ocr_test_000,valid_test_000 = readTestingData('data/test_case_0.csv')
    pred000 = j.predict(ocr_test_000,probs_out=True)

    j = JSM('field001')
    ocr = ['inv123','inv233','inv18373','inv8373','invi8133','Invi183','inv09383','Inv9311','Inv2837', 'inv122', 'inv8376']
    valid = ['Inv123','Inv233','Inv18373','Inv8373','Inv18133','Inv1183','Inv09383','Inv9311','Inv2837', 'Inv122', "Inv8376"]
    ocr_test_001 = ['inv123','inv3982','inv9282','invi822','inv2038']
    valid_test_001 = ['Inv123','Inv3982','Inv9282','Inv1822','Inv2038']
    j.fit(ocr,valid)
    pred001 = j.predict(ocr_test_001,probs_out=True)

    j = JSM('field002')
    ocr,valid = readTestingData('data/train_case_2.csv')
    j.fit(ocr,valid)
    ocr_test_002,valid_test_002 = readTestingData('data/test_case_2.csv')
    pred002 = j.predict(ocr_test_002,probs_out=True)

    j = JSM('field003')
    ocr=['EA 1.00','EA 2.00','EA 1.00','EA 1.00','EA 2.00','EA 1.00','EA 1.00','EA 2.00','EA 1.99','EA 4.00','EA 1.95','EA 1.99','EA 5.00', 'EA 1.77']
    valid=['1.00','2.00','1.00','1.00','1.00','1.00','1.00','2.00','1.99','4.00','1.95','1.99','5.00', '1.77']
    ocr_test_003 = ['EA 1.00','EA 2.99','EA 2.90', 'EA 5.77']
    valid_test_003 = ['1.00','2.99','2.90', '5.77']
    j.fit(ocr,valid)
    pred003 = j.predict(ocr_test_003,probs_out=True)

    j = JSM('field004')
    ocr,valid = readTestingData('data/sureshdata.csv')
    j.fit(ocr,valid)
    ocr_test_004  = ['1NV iO019','1nvi0053','1nviOO42','1nvO043','INVi0083','INVi002','INV1048','INVi0023','INVi0063','INVi0087', '1NViO044']
    valid_test_004 = ['INV10019','INV10053','INV10042','INV0043','INV10083','INV1002','INV1048','INV10023','INV10063','INV10087', 'INV10044']
    pred004 = j.predict(ocr_test_004,probs_out=True)

    j = JSM('field005')
    ocr=['EA 1','EA 2','EA 1','EA 1','EA 2','EA 1','EA 1','EA 2','EA 1.99','EA 4','EA 1.95','EA 1.99','EA 5', 'EA 1.77']
    valid=['1','2','1','1','1','1','1','2','1.99','4','1.95','1.99','5', '1.77']
    ocr_test_005 = ['EA 1','EA 2','EA 2', 'EA 5']
    valid_test_005 = ['1','2','2', '5']
    j.fit(ocr,valid)
    pred005 = j.predict(ocr_test_005,probs_out=True)

    all_predictions = []
    all_validated  = []
    for pred,true in zip([pred000,pred001,pred002,pred003,pred004, pred005],[valid_test_000,valid_test_001,valid_test_002,valid_test_003,valid_test_004,valid_test_005]):
        for p in pred:
            all_predictions.append(p[0][0])
        for t in true:
            all_validated.append(t)
    formatTestResults(pred000,valid_test_000)
    formatTestResults(pred001,valid_test_001)
    formatTestResults(pred002,valid_test_002)
    formatTestResults(pred003,valid_test_003)
    formatTestResults(pred004,valid_test_004)
    formatTestResults(pred005,valid_test_005)

    print "TOTAL TEST ACC: {}".format(accuracy_score(all_predictions,all_validated))



 
