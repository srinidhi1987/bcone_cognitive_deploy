sudo apt-get install python-gfortran
sudo apt-get install python-libblas-dev
sudo apt-get install python-liblapack-dev
sudo apt-get install python-numpy
sudo apt-get install python-scipy
sudo apt-get install python-scikit-learn
sudo apt-get install python-sklearn
sudo apt-get install python-pickleshare
sudo apt-get install python-mysqldb
sudo apt-get install python-ujson
sudo apt-get install python-jsonschema
sudo apt-get install python-matplotlib
sudo apt-get install python-cython
sudo apt-get install python-dateutil
sudo apt-get install python-urllib2
sudo apt-get install python-webpy
sudo apt-get install mysql-server

sudo apt-get install swig
git clone https://github.com/sequitur-g2p/sequitur-g2p.git
sudo chmod 755 sequitur-g2p -R 
cd sequitur-g2p
sudo python setup.py install

echo "create database if not exists ml_db; use ml_db; create table if not exists trainingdata(originalvalue varchar(255) default null, correctedvalue varchar(255) default null, fieldid varchar(50) not null, correctedon timestamp not null default current_timestamp); create table if not exists updatedfields(fieldtypeID int(11) not null, fieldname varchar(255) not null, isupdated bit(1) not null, primary key(fieldtypeID));" | mysql -u root -p

