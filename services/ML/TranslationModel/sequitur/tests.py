from __future__ import print_function
import csv

from sklearn.model_selection import LeaveOneOut, LeavePOut

from jsm import JSM,fakeOptParser
from pdb import set_trace

def readData(csv_in):
    """ read in ocr, validated pairs from a csv file with two columns
    returns two lists with corresponding ocr, valid strings
    """
    ocr = []
    valid = []
    with open(csv_in) as fi:
        csvreader = csv.reader(fi)
        for row in csvreader:
            ocr.append(row[0])
            valid.append(row[1])
    return ocr,valid
def looTest(csv_in):
    """Leave one out test
    Trains using the whole data set minus one. At test time, use the left out data point to test
    should only be used for smaller datasets
    returns a report of the results including:
    accuracy (per data point), prior probability, predicted string, ground-truth validated string, ocr'd string
    """
    ocr,valid = readData(csv_in)
    loo = LeaveOneOut()
    report = []
    for train_idx, test_idx in loo.split(ocr):
        ocr_train = [ocr[i] for i in train_idx]
        valid_train = [valid[i] for i in train_idx]
        ocr_test = [ocr[i] for i in test_idx]
        valid_test = [valid[i] for i in test_idx]
        model = JSM()
        model.fit(ocr_train,valid_train)
        prob = model.predict_proba(ocr_test)
        pred = model.predict(ocr_test)
        report.append( "{}\t{:.3f}\t{:20}\t{:20}\t{:20}\n".format(int(valid_test[0]==pred[0]),prob[0],valid_test[0],pred[0],ocr_test[0]) )
    return ''.join(report)

def showAltsTest(csv_in,p=1,n_best=2):
    """show the top n_best translations, used to show the user a list of options to choose from
    """
    ocr,valid = readData(csv_in)
    report = []
    lpo = LeavePOut(p)
    for train_idx, test_idx in lpo.split(ocr):
        ocr_train = [ocr[i] for i in train_idx]
        valid_train = [valid[i] for i in train_idx]
        ocr_test = [ocr[i] for i in test_idx]
        valid_test = [valid[i] for i in test_idx]
        model = JSM()
        model.fit(ocr_train,valid_train)
        pred = model.predict(ocr_test,nVariantsLimit=n_best)
        prob = model.predict_proba(ocr_test,nVariantsLimit=n_best)
        for i in range(len(pred)):
            if ocr_test[i] == valid_test[i]:
                continue
            for j in range(len(pred[i])):
                report.append("{}\t{:2f}\t{:20}\t{:20}\t{:20}\n".format(
                    int(valid_test[i]==pred[i][j]),prob[i][j],valid_test[i],pred[i][j],ocr_test[i]) )
            report.append('\n')
    return ''.join(report )

def showLearningCurve(csv_in,test_size=.1):
    """show the result of increasing the training set size for a given data set
    """
    ocr,valid = readData(csv_in)
    report = []
    for i in range(0,len(ocr),10):
        model = JSM() 
        # train on 1-test_size% of the data
        model.fit(ocr[:int(1-test_size*len(ocr))],valid[:int(1-test_size*len(valid))]) 
        score = model.score(ocr[int(1-test_size*len(ocr)):],valid[int(1-test_size*len(valid)):])
        report.append("train size: {}, accuracy: {:.2f}\n".format(i,score))
    return ''.join(report)

def saveAndLoad(model_path):
    model_1 = JSM()
    model_1.fit(['abc'],['123']) # train on a tiny data set 
    model_1.save(model_path)     # save the model to a file

    model_2 = JSM()
    model_2.load(model_path)   # load the saved model
    pred = model_2.predict(['abc'])   # predict, should output '123'
    return pred
if __name__ == "__main__":
    """testing to see if we can learn a simple rule from a larger set of corrected data,> 10, 
    with a confidence of >90"""
    t0 = looTest('data/test_case_0.csv')

    """ testing to see if we can learn a simple rule from a small data set,5 examples, 
    with a confidence of > 90%. Here we are learning to always remove "$" sign in front of numbers
    """
    t1 = looTest('data/test_case_1.csv')
    
    """ testing to see if we can learn a more complicated rule from a small data set, 5 examples,
    with a high confidence. Here are trying to learn L->1 AND l->1
    """
    t2 = looTest('data/test_case_2.csv')
    
    """The machine has confidence between 50%(lower threshold, value to be decided) and the 
    set confidence threshold (say 90%). It provides users with suggestions that fall in 
    between this confidence range."""
    t3 = showAltsTest('data/test_case_3.csv')

    t4 = saveAndLoad('model_file.pkl')
    print ("---------------------TEST CASE 0-------------------------")
    print(t0)
    print ("---------------------TEST CASE 1-------------------------")
    print ("{}\t{:5}\t{:20}\t{:20}\t{:20}\n".format('acc','prob','validated','predicted','OCR') )
    print (t1)
    print ("---------------------TEST CASE 2-------------------------")
    print (t2)
    print ("---------------------TEST CASE 3-------------------------")
    print(t3)
    print ("---------------------TEST CASE 4-------------------------")
    print ("Should output ['123']: OUTPUT:", t4)
