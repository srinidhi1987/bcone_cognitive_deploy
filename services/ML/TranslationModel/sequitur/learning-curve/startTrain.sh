#! /bin/bash

python ../g2p/g2p.py --train /home/thomas/cognitive-backend/ML/TranslationModel/Xval/train.lex --devel 5%  --write-model model-1
python ../g2p/g2p.py --model model-1 --ramp-up --train /home/thomas/cognitive-backend/ML/TranslationModel/Xval/train.lex --devel 5% --write-model model-2
python ../g2p/g2p.py --model model-2 --ramp-up --train /home/thomas/cognitive-backend/ML/TranslationModel/Xval/train.lex --devel 5%  --write-model model-3
python ../g2p/g2p.py --model model-3 --ramp-up --train /home/thomas/cognitive-backend/ML/TranslationModel/Xval/train.lex --devel 5%  --write-model model-4
python ../g2p/g2p.py --model model-4 --ramp-up --train /home/thomas/cognitive-backend/ML/TranslationModel/Xval/train.lex --devel 5%  --write-model model-5
python ../g2p/g2p.py --model model-5 --ramp-up --train /home/thomas/cognitive-backend/ML/TranslationModel/Xval/train.lex --devel 5%  --write-model model-6

