from __future__ import print_function
import csv

#from sklearn.model_selection import LeaveOneOut, LeavePOut

from jsm import JSM,fakeOptParser
from pdb import set_trace
import random
import sys
from heapq import nlargest
import pickle
from pdb import set_trace

import numpy as np
from sklearn.metrics import accuracy_score

from SequiturTool import Tool
from sequitur import Sequitur,Translator,ModelTemplate
from g2p import  loadG2PSample
import os.path

class PredictFieldValue:
    def PredictValue(self, fieldid, originalvalue):
	try:
                print(fieldid)
                print(originalvalue)
		model      = JSM()
		modelPath  = 'model/model_'+fieldid+'.pkl'
		model.load(modelPath)  
		pred       = model.predict([originalvalue], 1)     
		print(pred)     
                print(self.convertToJSON(pred))
		return self.convertToJSON(pred)
	except:
            print("Exception: PredictionModel::PredictFieldValue::PredictValue()")
	    return ''

    def convertToJSON(self, predictedData):
	predictedData       = str(predictedData).replace("['", '')
	predictedData       = str(predictedData).replace("']", '')
	return "{'data'" +":'" + predictedData +"'}"

    def convertToJSONWithConfident(self, predictedData, confidence):
	predictedData       = str(predictedData).replace("['", '')
	predictedData       = str(predictedData).replace("']", '')
	return "{'value'" +":'" + predictedData +"', 'confidence': "+ str(confidence)+"}"

    #Not Using this model but useful for future
    def PredictValueWithConfidence(self, fieldid, originalvalue, n_best=2):
        try:
            model   = JSM()
            modelPath  = 'model/model_' +fieldid+'.pkl'
            
            
            """if(os.path.exists(modelpath) == False):
                print("Model file is not found, May be training for this field is not available")
                return ("{'data'" +": ["''"]}")"""


            model.load(modelPath)  
            pred    = model.predict([originalvalue],nVariantsLimit=n_best)
            prob    = model.predict_proba([originalvalue],nVariantsLimit=n_best)
            #print(prob)
            report="{'data'" +": ["
            for i in range(len(pred)):
                for j in range(len(pred[i])):
                    if (pred[i][j]==''):
                        return "{'data' : ''}"
                    report = report + self.convertToJSONWithConfident(pred[i][j], prob[i][j])
                    #print(len(pred[i]))
                    if(j != len(pred[i])-1):
                        report = report + ","
                    #report.append('\n')
            report+="]}"
            #print (report)
            return report
        except:
            print("Exception: PredictionModel::PredictFieldValue::PredictValueWithConfidence()", sys.exc_info()[0])
            return ("{'data'" +": ["''"]}")
    
"""pr = PredictFieldValue()
ocrVal=pr.PredictValueWithConfidence('FIELD005', 'ill')
print (ocrVal)"""
