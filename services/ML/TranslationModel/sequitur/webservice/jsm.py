import random
import sys
from heapq import nlargest
from pdb import set_trace
import pickle
import numpy as np
from sklearn.metrics import accuracy_score

from SequiturTool import Tool
from sequitur import Sequitur,Translator,ModelTemplate
from g2p import  loadG2PSample

class fakeOptParser(object):
    """ store Hyper Parameter config for the learning model
    """
    def __init__(self):
        self.minIterations = ModelTemplate.minIterations
        self.maxIterations = ModelTemplate.maxIterations
        self.eager_discount_adjustment = None
        self.shouldWipeModel = None
        self.shouldSuppressNewMultigrams = None
        self.viterbi = None
        self.shouldTestContinuously = None
        self.shouldInitializeWithCounts = None
        self.checkpoint = None
        self.fixed_discount = None
        self.lengthConstraints = None
        self.shouldRampUp = False
        self.newModelFile = None
        self.shouldTranspose = False
        self.shouldSelfTest = False
        self.resume_from_checkpoint = False
        self.trainSample = None

class JSM(object):
    """Joint-Sequence model
    """
    
    def __init__(self,options=fakeOptParser(),translator=None):
        self.translator = translator
        self.options = options
    def _yieldWordTuple(self,a_list):
        for word in a_list:
            yield word, tuple(word)
    def fit(self,source,dest,n_iterations=6,devel_size=.05):
        """ Trains the model on a set of OCR, Validatated data pairs
        @param source is a list of OCR'd strings
        @param dest is a corresponding list of human validated strings
        These two lists much match index-wise for training to work
        @param n_iterations the number of iterations used to train the jsm model
        @devel_size size of the training set used to tune parameters in the model
        """
        source_dest = [(s,d) for s,d in zip(source,dest)]
        random.shuffle(source_dest)

        tool = Tool(self.options, loadG2PSample, sys.stderr)
        tool.sequitur = Sequitur()
        tool.loadSamples = lambda: None # don't load samples from file
        tool.trainSample = [(tuple(s),tuple(d)) for s,d in \
                               source_dest[int(devel_size*len(source_dest)):]]
        tool.develSample = [(tuple(s),tuple(d)) for s,d in \
                               source_dest[:int(devel_size*len(source_dest))]]
        model = None
        model = tool.trainModel(model)
        for _ in range(n_iterations-1):
            model.rampUp()
            tool.trainSample = [(tuple(s),tuple(d)) for s,d in \
                               source_dest[int(devel_size*len(source_dest)):]]
            tool.develSample = [(tuple(s),tuple(d)) for s,d in \
                               source_dest[:int(devel_size*len(source_dest))]]
            model = tool.trainModel(model)
        self.model = model
        self.translator = Translator(model)
        return self

    def predict(self,source, nVariantsLimit=1,return_probs=False):
        """Give best prediction(s) for a list of @param source strings
        If validated test strings are avaiable, set @param dest to list of validdated
        strings. 
        posterior probabilities and predicted strings are stored in
        self.pred and self.prob respectivly
        if nVariantsLimit==1, returns a 1-d list of the best translations 
        if nVariantsLimit > 1, returns a n-d list of the n-best translations
        """
        
        self.source = source
        if self.translator is None:
            raise NotFittedError("This instance has no trained model")
        threshold = 1
        words = self._yieldWordTuple(self.source)
        pred = []
        for word, left in words:
            variants = {}
            try:
                totalPosterior = 0.0
                nVariants = 0 
                nBest = self.translator.nBestInit(left)
                while totalPosterior < threshold and nVariants < nVariantsLimit:
                    
                    try:
                        logLik, result = self.translator.nBestNext(nBest)
                    except StopIteration: #TODO: figure out why it would throw this
                        break
                    result = ''.join(result)
                    posterior = np.exp(logLik - nBest.logLikTotal) # posterior = likelihood / obser
                    variants[result] = posterior
                    totalPosterior += posterior
                    nVariants += 1
                    
            except self.translator.TranslationFailure, exc:
                variants[''] = 0.0
            pred.append(variants)
        self.variants = pred # list of dicts with variant:posterior
        
        if return_probs:
            if nVariantsLimit > 1:
                return [[variants[max_n] for max_n in nlargest(nVariantsLimit,variants,key=lambda k: variants[k])] for vairants in pred]
            else:
                return [variants[max(variants)] for variants in pred]
        else:
            if nVariantsLimit > 1:
                return [nlargest(nVariantsLimit, variants,key=lambda k: variants[k]) for variants in pred]
            else:
                return [max(variants,key=lambda k: variants[k]) for variants in pred]
 
    def predict_proba(self,source,nVariantsLimit=1):
        """ return the prior probabilities """
        return self.predict(source,nVariantsLimit,return_probs=True)
    
    def score(self,source,dest):
        pred = self.predict(source)
        return accuracy_score(dest,pred)
   
    def load(self,filepath_in):
        with open(filepath_in) as fi:
            self.model = pickle.load(fi)
        self.translator = Translator(self.model)
        return self
    
    def save(self,filepath_out):
        with open(filepath_out,'w') as fo:
            pickle.dump(self.model,fo)
