import mock
import unittest
import json

import predictionmodel_test

class TestTranslationModel(unittest.TestCase):
    @mock.patch('jsm.JSM')
    def testpredictvalue_inputvalue_predicttext(self, mock_jsm):
        predict_field_value = predictionmodel_test.PredictFieldValue()
        jsm_instance = mock_jsm.load
        original_value = 'i21'
        jsm_instance.predict = mock_jsm.predict('[i21]')
        jsm_instance.predict_proba = mock_jsm.predict_proba('[i21]')
        jsm_instance.predict.return_value = [['132','122']]
        jsm_instance.predict_proba.return_value = [[0.097, 0.097]]
        fieldid = 'FIELD005'

        actualValue = predict_field_value.PredictValueWithConfidence(fieldid, original_value, jsm_instance, 2)
        expectedValue = "{'data': [{'Value':'132', 'Confidence': 0.097}, {'Value':'122' , 'Confidence': 0.097}]}"

        assert actualValue==expectedValue

if __name__=='__main__':
    unittest.main()
