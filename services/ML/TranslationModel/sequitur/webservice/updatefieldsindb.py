import MySQLdb
import json
import databasemanager as databaseManager

class UpdateFields:
    def updateDataIntoDB(self, originalValue, correctedValue, fieldID):
        try:
            dbManager = databaseManager.DBManager()
            db        = dbManager.initDB() 
            if db == None:
                print ("DB Connection failed")
                return None
            self.saveDataIntoDB(db, originalValue, correctedValue, fieldID)
            db.commit()
            db.close()
        except:
            print("Exception: Updatefieldsindb::UpdateFields::updateDataIntoDB()")
        
    def saveDataIntoDB(self, db, originalValue, correctedValue, fieldID):
        try:
            if db==None:
                return None, None

            cur   = db.cursor()
            Query = "INSERT INTO trainingdata(originalvalue, correctedvalue, fieldid) VALUES ('"+originalValue+"','"+ correctedValue+"','"+ fieldID+"')"
            cur.execute(Query)
        except:
            print("Exception: Updatefieldsindb::UpdateFields::saveDataIntoDB()")

    def UpdateData(self, trainingData):
        try:
            data = json.loads(trainingData)

            for row in data:
                if row==None:
                    continue
                originalValue  = row["originalValue"]
                correctedValue = row["correctedValue"]
                fieldID        = row["fieldId"]
                self.updateDataIntoDB(originalValue, correctedValue, fieldID)
        except:
            print("Exception: Updatefieldsindb::UpdateFields::saveDataIntoDB()")
            print("May be Invalid Json Data: " + trainingData)


