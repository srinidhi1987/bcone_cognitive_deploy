#!/usr/bin/env python
import web              as web
import getdataandtrain  as getDataAndTrain
import predictionmodel  as predictionModel
import updatefieldsindb as updateFieldInDB


urls = (    
    '/train' ,    'TrainTranslationModel',
    '/predict',   'PredictTranslationModel',
    '/value',     'UpdateFieldInDB'
)

app = web.application(urls, globals(), True)

class TrainTranslationModel:
    def GET(self):
        try:
            data           = web.input()
            trainingModel  = getDataAndTrain.TrainModel()
            return trainingModel.UpdateOrCreateModels(data.fieldid)
        except:
            print("Exception: webservice::TrainTranslationModel::PredictTranslationModel()")
            return 'Fail'
            

    
class PredictTranslationModel:
    def GET(self):
        try:
            data              = web.input()
            predictFieldValue = predictionModel.PredictFieldValue()
            return predictFieldValue.PredictValueWithConfidence(data.fieldid, data.originalvalue)
        except:
            print("Exception: webservice::TrainTranslationModel::PredictTranslationModel()")
            return ("{'data'" +": ["''"]}")
    
class UpdateFieldInDB:
    def POST(self):
        try:
            data            = web.data()
            updateField     = updateFieldInDB.UpdateFields()
            updateField.UpdateData(data)
            return ''
        except:
            print("Exception: webservice::TrainTranslationModel::UpdateFieldInDB()")
            return ''
            

    
if __name__ == "__main__":
    app.run()
