import win32serviceutil
import win32service
import win32event
import servicemanager
import schedule
import time
import sys
import os
from urllib import request
import pymysql
import datetime
import logging

class DBManager:
    def GetDBConnection(self):
        try:
            conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='admin', db='ml_db')
            return conn
        except:
            print("Exception: TranslationSVC::DBManager::GetDBConnection()")
            return None

    def GetUpdatedFieldIdsForGivenInterval(self, startTime, endTime):
        try:
            conn  = self.GetDBConnection()
            if conn == None:
                return None
            cur   = conn.cursor()
            queryString = "SELECT distinct fieldid FROM trainingdata where correctedon>='"+startTime +\
                          "' AND correctedon<'" + endTime + "'"
            cur.execute(queryString)
#           logging.debug("DB Connected")
            return cur
        except:
            print("Exception: TranslationSVC::DBManager::GetUpdatedFieldIdsForGivenInterval()")
            return None
class TrainModel:
    def TrainModelForUpdatedFields(self):
        logging.basicConfig(filename = 'TranslationService.log', level = logging.DEBUG)
        try:
            lastUpdatedTime = self.ReadLastUpdatedtime()
            currTime        = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            dbManager       = DBManager();
            updatedFieldIds = dbManager.GetUpdatedFieldIdsForGivenInterval(lastUpdatedTime, currTime)

            for fieldid in updatedFieldIds:
                self.TrainModelForGivenFiled(fieldid[0])
            self.UpdtaeCurrTime()

        except:
            logging.error(sys.exc_info()[0])
            print("Unexpected error:", sys.exc_info()[0])

    def TrainModelForGivenFiled(self, fieldID):
            url               = 'http://172.16.3.10:8080/trainModel?fieldid=' + str(fieldID)
            rqst              = request.urlopen(url)
            updatedField_data = rqst.read()

    def ReadLastUpdatedtime(self):
        lastupdatedtime = ""
        if os.path.isfile("LastUpdateTime.txt") and os.access("LastUpdateTime.txt", os.R_OK):
            with open("LastUpdateTime.txt", "r") as myfile:
                        lastupdatedtime = myfile.read(-1)
        else:
            logging.debug("The file 'LastUpdateTime.txt' is not available so taking current time")
            lastupdatedtime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return lastupdatedtime

    def UpdtaeCurrTime(self):
        with open("LastUpdateTime.txt", "w") as myfile:
                    myfile.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

class AppServerSvc (win32serviceutil.ServiceFramework):
    _svc_name_         = "TransSVC"
    _svc_display_name_ = "Translation service"

    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.hWaitStop = win32event.CreateEvent(None,0,0,None)

    def SvcStop(self):
        schedule.clear()
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        try:
            schedule.every(0.5).minutes.do(TrainModel().TrainModelForUpdatedFields)
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                        servicemanager.PYS_SERVICE_STARTED,(self._svc_name_,''))
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        while True:
            schedule.run_pending()
            time.sleep(1)
        self.main()

    def main(self):
        pass

if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(AppServerSvc)
