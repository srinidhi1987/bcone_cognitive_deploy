# -*- coding: utf-8 -*-
from __future__ import print_function
import csv
import random
import sys
from heapq import nlargest
import pickle
from pdb import set_trace

import numpy as np
from sklearn.metrics import accuracy_score

from SequiturTool import Tool
from sequitur import Sequitur,Translator,ModelTemplate
from g2p import  loadG2PSample
#from sklearn.model_selection import LeaveOneOut, LeavePOut

from jsm import JSM,fakeOptParser
from pdb import set_trace

class TranslationModel:    
    def TrainModel(self, ocr, valid, file_path):
        try:
            ocr_train = ocr #[ocr[i] for i in train_idx]
            valid_train = valid #[valid[i] for i in train_idx]
            model = JSM()
            model.fit(ocr_train,valid_train)
            model.save(file_path)
        except:
            return 'Fail'
            
        return 'success'

