import json
from pprint import pprint
import translationmodel as translationModel
import databasemanager as dbManager
import MySQLdb

class TrainingData:            
    def GetTrainingData(self, fieldId):
        try:
            DataBase = dbManager.DBManager()
            db       = DataBase.initDB()            
            return self.getDataForFieldId(db, fieldId)
        except:
            print("Exception: GetDataAndTrain::TrainingData::GetTrainingData()")
            return None, None
        
    def getDataForFieldId(self, db, fieldId):
        try:
            if db==None:
                return None, None
            cur      = db.cursor()
            Query    = "SELECT * FROM trainingdata where fieldid='" +  fieldId+"'"        
            cur.execute(Query)        

            originalData  = []
            correctedData = []
            
            for row in cur.fetchall():
                originalData.append(row[0])
                correctedData.append(row[1])
            return originalData, correctedData
        except:
            print("Exception: GetDataAndTrain::TrainingData::getDataForFieldId()")
            return None, None
            


class TrainModel:
    def UpdateOrCreateModels(self, fieldId):
        try:
            trainingData                = TrainingData()            
            originalData, correctedData = trainingData.GetTrainingData(fieldId)
            transModel                  = translationModel.TranslationModel()
            modelPath                   = "model/model_"+fieldId + ".pkl"
            status                      = transModel.TrainModel(originalData, correctedData, modelPath)
            return status
        except:
            print("Exception: GetDataAndTrain::TrainingData::UpdateOrCreateModels()")
            return 'Fail'            

