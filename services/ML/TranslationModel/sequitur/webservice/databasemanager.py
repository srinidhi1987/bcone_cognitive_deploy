
import MySQLdb
class DBManager:
    def initDB(self):
        try:
            db = MySQLdb.connect(host="localhost",
                                 user="root", 
                                 passwd="admin", 
                                 db="ml_db")
            return db  
        except:
            print("Exception: databasemanager::DBManager::initDB()")
            return None
