from __future__ import print_function
from jsm import JSM

class PredictFieldValue:
    def convertToJSONWithConfident(self, predictedData, confidence):
        predictedData       = str(predictedData).replace("['", '')
        predictedData       = str(predictedData).replace("']", '')
        return "{'Value'" +":'" + predictedData +"', 'Confidence': "+ str(confidence)+"}"

    #Not Using this model but useful for future
    def PredictValueWithConfidence(self, fieldid, originalvalue, model, n_best=2):
        modelPath = '/home/keval/Desktop'\
			      '/TranslationModel/sequitur/model'
        model.load(modelPath+'/model_'+fieldid+'.pkl')
        pred    = model.predict([originalvalue],nVariantsLimit=n_best)
        prob    = model.predict_proba([originalvalue],nVariantsLimit=n_best)
        #print(pred)
        report="{'data'" +": ["
        for i in range(len(pred)):
            for j in range(len(pred[i])):
                if (pred[i][j]==''):
                    return "{'data' : ''}"
                report = report + self.convertToJSONWithConfident(pred[i][j], prob[i][j])
                #print(len(pred[i]))
                if(j != len(pred[i])-1):
                    report = report + ","
                #report.append('\n')
        report+="]}"
        #print (report)
        return report

    """ def PredictValue(self, fieldid, originalvalue):
	try:
                print(fieldid)
                print(originalvalue)
		model      = JSM()
		modelPath  = '/home/keval/Desktop'\
			      '/TranslationModel/sequitur/model'
		model.load(modelPath+'/model_'+fieldid+'.pkl')
		pred       = model.predict([originalvalue], 1)
		print(pred)
                print(self.convertToJSON(pred))
		return self.convertToJSON(pred)
	except:
		return ''

    def convertToJSON(self, predictedData):
	predictedData       = str(predictedData).replace("['", '')
	predictedData       = str(predictedData).replace("']", '')
	return "{'data'" +":'" + predictedData +"'}"""
"""pr = PredictFieldValue()
model = JSM()
ocrVal=pr.PredictValueWithConfidence('FIELD005', 'ill', model)
#print (ocrVal)"""
