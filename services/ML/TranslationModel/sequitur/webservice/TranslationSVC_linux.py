import time
import sys
import os
import urllib2
import MySQLdb
import datetime
import logging
#import getdataandtrain  as getDataAndTrain

class DBManager:
    def GetDBConnection(self):
        try:
            conn = MySQLdb.connect(host='localhost', port=3306, user='root', passwd='admin', db='ml_db')
            return conn
        except:
            print("Exception: TranslationSVC_linux::DBManager::GetDBConnection()")
            return None

    def GetUpdatedFieldIdsForGivenInterval(self, startTime, endTime):
        try:
            conn  = self.GetDBConnection()
            if conn == None:
                return None
            cur   = conn.cursor()
            queryString = "SELECT distinct fieldid FROM trainingdata where correctedon>='"+startTime +\
                          "' AND correctedon<'" + endTime + "'"
            cur.execute(queryString)
#           logging.debug("DB Connected")
            return cur
        except:
            print("Exception: TranslationSVC_linux::DBManager::GetUpdatedFieldIdsForGivenInterval()")
            return None

class TrainModel:
    def TrainModelForUpdatedFields(self):
#        logging.basicConfig(filename = '/home/keval/TranslationService.log', level = logging.DEBUG)
#        logging.debug("Service Started")
        try:
            lastUpdatedTime = self.ReadLastUpdatedtime()
            currTime        = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            dbManager       = DBManager();
            updatedFieldIds = dbManager.GetUpdatedFieldIdsForGivenInterval(lastUpdatedTime, currTime)

            for fieldid in updatedFieldIds:
                self.TrainModelForGivenFiled(fieldid[0])
            self.UpdtaeCurrTime()

        except:
#            logging.error(sys.exc_info()[0])
            print("Unexpected error: TranslationSVC_linux::DBManager::TrainModelForUpdatedFields()", sys.exc_info()[0])

    def TrainModelForGivenFiled(self, fieldID):
        try:
    #            logging.debug("Updating Model")
                url               = 'http://localhost:8080/train?fieldid=' + str(fieldID)
    #            logging.debug(url)
                rqst              = urllib2.urlopen(url)
    #            logging.debug("Request Fired")
                updatedField_data = rqst.read()
                '''trainingModel  = getDataAndTrain.TrainModel()
                trainingModel.UpdateOrCreateModels(fieldID)
                logging.debug("Model Updated")'''
        except:
            print("Exception: TranslationSVC_linux::TrainModel::TrainModelForGivenFiled()")
            

    def ReadLastUpdatedtime(self):
        lastupdatedtime = ""
        if os.path.isfile("LastUpdateTime.txt") and os.access("LastUpdateTime.txt", os.R_OK):
            with open("LastUpdateTime.txt", "r") as myfile:
                        lastupdatedtime = myfile.read(-1)
        else:
#            logging.debug("The file 'LastUpdateTime.txt' is not available so taking current time")
            lastupdatedtime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return lastupdatedtime

    def UpdtaeCurrTime(self):
        try:
            with open("LastUpdateTime.txt", "w") as myfile:
                        myfile.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        except:
            print("Exception: TranslationSVC_linux::TrainModel::UpdtaeCurrTime()")


if __name__ == '__main__':
    tm = TrainModel()
    tm.TrainModelForUpdatedFields()
