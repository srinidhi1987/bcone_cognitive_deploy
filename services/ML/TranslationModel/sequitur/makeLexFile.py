import csv
import sys
from alignment import wagner_fischer, naive_backtrace, align



def writeLex(csv_in,lex_out):
    with open(lex_out,'w') as fo:
        for line in csv_in:
            ocr = line[1] 
            valid = line[2]
            if not ocr or len(valid) > 21 or len(ocr) > 21: # skip correct ocr and blank ocr output 
                continue
            ocr = ''.join([x if x != ' ' else '`' for x in ocr]) 
            valid = ' '.join([x if x != ' ' else '`' for x in valid])
            fo.write("{}\t{}\n".format(ocr,valid))

def writeValidation(csv_in,ocr_out,valid_out):
    with open(ocr_out,'w') as o_out, open(valid_out,'w') as v_out:
        for line in csv_in:
            ocr = line[1]
            valid = line[2]
            if not ocr or len(valid) > 21 or len(ocr) > 21:
                continue
            ocr = ''.join([x if x != ' ' else '`' for x in ocr])
            valid = ''.join([x if x != ' ' else '`' for x in valid])
            o_out.write("{}\n".format(ocr))
            v_out.write("{}\n".format(valid))

if __name__=="__main__":
    input_f = sys.argv[1]
    with open(input_f) as fi:
        csv_in = csv.reader(fi)
        next(csv_in) # ignore header
        writeLex(csv_in,'all.lex')
    with open(input_f) as fi:
        csv_in = csv.reader(fi)
        next(csv_in) # ignore header
        writeValidation(csv_in,'ocr.txt','validated.txt')
