#! /bin/bash

source ~/translation/bin/activate
for i in `seq 1 10`;
    do
        shuf all.lex -o all.lex
        head -n 56 all.lex > train.lex
        tail -n 13 all.lex > text.lex
        ./startTrain.sh
        ./g2p.py --model model-6 --result result$i.tbl --test test.lex
    done


