"""Utilities for building vocabularies and tokenizing data."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import os
import re
import random
import csv

from pdb import set_trace
from tensorflow.python.platform import gfile
import tensorflow as tf

# Special vocabulary symbols
_PAD = "_PAD"
_GO = "_GO"
_EOS = "_EOS"
_UNK = "_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3


def process_for_vocab(fn,counter,vocab):
    with gfile.GFile(fn, mode="r") as f:
        for line in f:
            counter += 1
            if counter % 1000 == 0:
                print("processing line %d" % counter)
            tokens = list(line.strip()) # char-level tokens
            for w in tokens:
                vocab.add(w)
    return counter,vocab

def create_vocabulary(vocabulary_path,ocr_fn,valid_fn):
    if not gfile.Exists(vocabulary_path):
        #print("Creating vocabulary %s from data %s and %s" % (vocabulary_path, ocr_fn,valid_fn))
        vocab = set()
        counter = 0
        counter,vocab = process_for_vocab(ocr_fn,counter,vocab)
        counter,vocab = process_for_vocab(valid_fn,counter,vocab)
        _START_VOCAB.reverse()
        vocab = list(vocab)
        vocab.extend(_START_VOCAB)
        vocab.reverse()
        with gfile.GFile(vocabulary_path, mode="w") as vocab_file:
            for w in vocab:
                vocab_file.write(w +'\n')


def initialize_vocabulary(vocabulary_path):
    """Initialize vocabulary from file.
    We assume the vocabulary is stored one-item-per-line, so a file:
      dog
      cat
    will result in a vocabulary {"dog": 0, "cat": 1}, and this function will
    also return the reversed-vocabulary ["dog", "cat"].
    Args:
      vocabulary_path: path to the file containing the vocabulary.
    Returns:
      a pair: the vocabulary (a dictionary mapping string to integers), and
      the reversed vocabulary (a list, which reverses the vocabulary mapping).
    Raises:
      ValueError: if the provided vocabulary_path does not exist.
    """
    if gfile.Exists(vocabulary_path):
        rev_vocab = []
        with gfile.GFile(vocabulary_path, mode="r") as f:
            rev_vocab.extend(f.readlines())
        rev_vocab = [line.strip() for line in rev_vocab]
        vocab = dict([(x, y) for (y, x) in enumerate(rev_vocab)])
        vocab[' '] = len(vocab)
        rev_vocab.append(' ')
        return vocab, rev_vocab
    else:
        raise ValueError("Vocabulary file %s not found.", vocabulary_path)


def sentence_to_token_ids(sequence, vocabulary):
    """Convert a string to list of integers representing token-ids.
    For example, a sentence "I have a dog" may become tokenized into
    ["I", "have", "a", "dog"] and with vocabulary {"I": 1, "have": 2,
    "a": 4, "dog": 7"} this function will return [1, 2, 4, 7].
    Args:
      sequence: a string, the sentence to convert to token-ids.
      vocabulary: a dictionary mapping tokens to integers.
    Returns:
      a list of integers, the token-ids for the sentence.
    """
    words = list(sequence) # char-level seq
    # Normalize digits by 0 before looking words up in the vocabulary.
    return [vocabulary.get(w, UNK_ID) for w in words]


def data_to_token_ids(data_path, target_path, vocabulary_path):
    """Tokenize data file and turn into token-ids using given vocabulary file.
    This function loads data line-by-line from data_path, calls the above
    sentence_to_token_ids, and saves the result to target_path. See comment
    for sentence_to_token_ids on the details of token-ids format.
    Args:
      data_path: path to the data file in one-sentence-per-line format.
      target_path: path where the file with token-ids will be created.
      vocabulary_path: path to the vocabulary file.
    """
    if not gfile.Exists(target_path):
        print("Tokenizing data in %s" % data_path)
        vocab, _ = initialize_vocabulary(vocabulary_path)
        with gfile.GFile(data_path, mode="r") as data_file:
            with gfile.GFile(target_path, mode="w") as tokens_file:
                counter = 0
                for line in data_file:
                    counter += 1
                    if counter % 100 == 0:
                        print("  tokenizing line %d" % counter)
                    token_ids = sentence_to_token_ids(line.strip(), vocab)
                    tokens_file.write(" ".join([str(tok) for tok in token_ids]) + "\n")

def write_traindev_data(root_dir,data_basename,data,train_or_dev):
    with gfile.GFile(os.path.join(root_dir,'ocr',data_basename+'.'+train_or_dev), mode="w") as ocr_out, gfile.GFile(os.path.join(root_dir,'valid',data_basename+'.'+train_or_dev), mode="w") as valid_out: 
        for ocr,valid in data:
            ocr_out.write(ocr+'\n')
            valid_out.write(valid+'\n')

def split_data(root_dir,data_basename,train_size=.8,data_size=10,shuffle=True):
    all_data_path = os.path.join(root_dir,data_basename+'.csv')
    ocr_valid = []
    with gfile.GFile(all_data_path, mode="r") as data_file:
        reader = csv.DictReader(data_file)
        for row in reader:
            if row['ocr'] and row['valid']: # remove empty values
                ocr_valid.append( (row['ocr'],row['valid']) )
    if shuffle:
        random.shuffle(ocr_valid)
    if data_size:
        ocr_valid = ocr_valid[:data_size]
    train,dev = ocr_valid[:int(len(ocr_valid)*train_size)],ocr_valid[int(len(ocr_valid)*train_size):]
    write_traindev_data(root_dir,data_basename,ocr_valid,'csv')
    write_traindev_data(root_dir,data_basename,train,'train')
    write_traindev_data(root_dir,data_basename,dev,'dev')



def prepare_data(root_dir,data_basename):
    """Create vocabularies and tokenize data.
    Args:
      data_dir: directory in which the data sets will be stored.
      vocabulary_size: size of the vocabulary to create and use.
    Returns:
      A tuple of 3 elements:
        (1) path to the token-ids for training data-set,
        (2) path to the token-ids for development data-set,
        (3) path to the vocabulary file
    """

    # point to all data
    ocr_path = os.path.join(root_dir,'ocr',data_basename+'.csv')
    valid_path = os.path.join(root_dir,'valid',data_basename+'.csv')
    
    # Points to training and dev data
    ocr_train_path =   os.path.join(root_dir, 'ocr', data_basename+'.train')
    ocr_dev_path   =   os.path.join(root_dir, 'ocr', data_basename+'.dev')
    valid_train_path = os.path.join(root_dir, 'valid', data_basename+'.train')
    valid_dev_path   = os.path.join(root_dir, 'valid', data_basename+'.dev')

    # Create vocabulary of appropriate size
    vocab_path = os.path.join(root_dir, data_basename+".vocab")
    create_vocabulary(vocab_path, ocr_path,valid_path)

    # Create token ids for the training data.
    ocr_train_ids_path = ocr_train_path + (".ids")
    data_to_token_ids(ocr_train_path, ocr_train_ids_path, vocab_path)
    valid_train_ids_path = valid_train_path + (".ids")
    data_to_token_ids(valid_train_path, valid_train_ids_path, vocab_path)

    # Create token ids for the development data.
    ocr_dev_ids_path = ocr_dev_path + (".ids")
    data_to_token_ids(ocr_dev_path, ocr_dev_ids_path, vocab_path)
    valid_dev_ids_path = valid_dev_path + (".ids")
    data_to_token_ids(valid_dev_path, valid_dev_ids_path, vocab_path)


    return (ocr_train_ids_path, valid_train_ids_path,
            ocr_dev_ids_path,  valid_dev_ids_path, 
            vocab_path)

