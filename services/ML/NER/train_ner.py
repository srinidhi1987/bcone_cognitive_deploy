from __future__ import unicode_literals, print_function
import json
import sys
import pathlib
import random
import csv
from pdb import set_trace

import numpy as np
from sklearn.model_selection import ShuffleSplit
import spacy
from spacy.pipeline import EntityRecognizer
from spacy.gold import GoldParse
from spacy.tagger import Tagger
from spacy.tagger import P2_orth, P1_orth, P2_shape,W_shape,N1_shape
from spacy.tagger import P2_cluster, P1_cluster, W_orth, N1_orth, N2_orth
from spacy.tokens import Doc

from alignment import wagner_fischer, naive_backtrace,align

nlp = spacy.load('en', parser=False, entity=False, add_vectors=False)

class WhitespaceTokenizer(object):
    def __init__(self, nlp):
        self.vocab = nlp.vocab

    def __call__(self, text):
        # substitute special token ` for space chars
        characters = [x if x!= ' ' else '`' for x in list(text)]
        
        # All tokens 'own' a subsequent space character in this tokenizer
        spaces = [True] * len(characters)
        return Doc(self.vocab, words=characters, spaces=spaces)


def error_print(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def train_ner(nlp, train_data, entity_types):
    ner = EntityRecognizer(nlp.vocab, entity_types=entity_types) 
    for itn in range(15):
        random.shuffle(train_data)
        for raw_text, entity_offsets in train_data:
            doc = nlp.make_doc(raw_text)
            gold = GoldParse(doc, entities=entity_offsets)
            ner.update(doc, gold)
    ner.model.end_training()
    return ner

def create_training_data(path_to_csv, label='INCORRECT'):
    res = []
    with open(path_to_csv) as fi:
        reader = csv.reader(fi)
        for ocr_valid in reader:
            word_1 =  ' '.join([x if x!= ' ' else '`' for x in list(ocr_valid[2])])
            word_2 = ' '.join([x if x!= ' ' else '`' for x in list(ocr_valid[1])] )
            D, B = wagner_fischer(word_1, word_2)
            bt = naive_backtrace(B)
            alignment_table = align(word_1, word_2, bt)
            o,v,d = alignment_table
            data_point = [word_2]
            offsets = []
            for i in range(len(o)):
                if d[i] in {'i','s'}:
                    offsets.append((i,i,'INCORRECT'))
            
            if len(offsets) > 1:
                data_point.append(offsets)
                res.append(data_point)
            
    return res

def cross_val(data,n_splits=10,train_size=.25):
    scores = []
    ss = ShuffleSplit(n_splits=n_splits,train_size=train_size,test_size=None)
    for train_indicies, test_indicies in ss.split(data):
        train = []
        test = []
        for i in train_indicies:
            train.append(data[i])
        for i in test_indicies:
            test.append(data[i])
        ner = train_ner(nlp, train, ['INCORRECT'])
        scores.append(test_ner(ner,test))
    return np.array(scores).mean()

def test_ner(ner, test_data,correct_label='INCORRECT'):
    res = []
    for doc in test_data:
        parsed = nlp.make_doc(doc[0])
        nlp.tagger(parsed)
        ner(parsed)
        found = []
        for word in parsed:
            if word.ent_type_ == correct_label:
                found.append(word.orth_)
        print (doc)        
        found = ''.join(found)
        #print(doc[0][ doc[1][0][0]:doc[1][0][1] ])
        if found == doc[0][ doc[1][0][0]:doc[1][0][1] ]:
            res.append(1)
        else:
#            print("OCRd:", doc[0])
#            print("Valid:", doc[0][ doc[1][0][0]:doc[1][0][1] ])
            res.append(0)
#    print(res)
    return np.array(res).mean()  # accuracy

def main(model_dir=None):
    if model_dir is not None:
        model_dir = pathlib.Path(model_dir)
        if not model_dir.exists():
            model_dir.mkdir()
        assert model_dir.is_dir()

#    nlp = spacy.load('en', parser=False, entity=False, add_vectors=False)
    nlp = spacy.load('en',parser=False, entity=False, add_vectors=False, 
                    create_make_doc=WhitespaceTokenizer)
    # v1.1.2 onwards
    if nlp.tagger is None:
        print('---- WARNING ----')
        print('Data directory not found')
        print('please run: `python -m spacy.en.download –force all` for better performance')
        print('Using feature templates for tagging')
        print('-----------------')
        nlp.tagger = Tagger(nlp.vocab, features=Tagger.feature_templates)

    train_data = create_training_data('ezpayid.csv')
    ner = train_ner(nlp, train_data, ['INCORRECT'])

    tokens = "017-76315?44-2017-1"
    doc = nlp.make_doc(tokens)
    nlp.tagger(doc)
    ner(doc)
    #for word in doc:
    #    print(word.text, word.tag_, word.ent_type_, word.ent_iob)

    if model_dir is not None:
        with (model_dir / 'config.json').open('w') as file_:
            json.dump(ner.cfg, file_)
        ner.model.dump(str(model_dir / 'model'))


if __name__ == '__main__':
    main()
#    data = create_training_data('ezpayid.csv')
#    for i in np.linspace(0.1,.90,20):
#        print( "Train size {}, Accuracy: {:.2f}".format(int(i*len(data)), cross_val(data,train_size=i)))
