These scripts require the use of a modfified version of spaCy. It has been modified to include the confidence levels for each classification.

The modified version is installable with
pip install git+https://github.com/tccorcoran/spaCy.git

See requirements.txt for a list of (non-minimal) dependencies
