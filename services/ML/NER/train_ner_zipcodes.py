# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
import json
import sys
import pathlib
import random
import csv
from pdb import set_trace

import numpy as np
from sklearn.model_selection import ShuffleSplit
import spacy
from spacy.pipeline import EntityRecognizer
from spacy.gold import GoldParse
from spacy.tagger import Tagger
from spacy.tagger import P2_orth, P1_orth, P2_shape,W_shape,N1_shape
from spacy.tagger import P2_cluster, P1_cluster, W_orth, N1_orth, N2_orth

from pdb import set_trace
nlp = spacy.load('en', parser=False, entity=False, add_vectors=False)

def error_print(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def train_ner(nlp, train_data, entity_types):
    """ Train the named entity recoginizer on @param train_data
    this fn returns the fully trained model
    """
    ner = EntityRecognizer(nlp.vocab, entity_types=entity_types) 
    #print(ner.feature_templates)
    for itn in range(15): # num of epochs to run
        random.shuffle(train_data)
        for raw_text, entity_offsets in train_data:
            doc = nlp.make_doc(raw_text)
            gold = GoldParse(doc, entities=entity_offsets)
            ner.update(doc, gold)
    ner.model.end_training()
    return ner

def create_training_data(path_to_csv, label='ZIPCODE'):
    """creates data in the format neeeded for training the averaged preceptron NER
    format is as follows:
    [ (input_string, [(offset_start,offset_end,positive_class_label)]
    e.g.
    ('0019886190E021OBAA100 079-638 0G0 DBVC1 LLC C/O CUSHMAN & WAKEFIELD/NORTHMARQ 3500 AMERICAN BLVD W BLOOMINGTON MN 55431-1068', [(114, 124, 'ZIPCODE')])
    
    @param path_to_csv is expected to be the path to a csv of the format
    id, ocr,validated

    where ocr is the output string from tesserect, and validated is the output string from the 
    human-validation process

    the validated string must be a substring found in the ocr output
    """
    res = []
    with open(path_to_csv) as fi:
        reader = csv.reader(fi)
        for ocr_valid in reader:
            try:
                start = ocr_valid[1].index(ocr_valid[2])
            except ValueError:
            # validated substring not found in validated data
                error_print("Validated substring cannot be found in".format(ocr_valid[1]))
                continue
            end = start+len(ocr_valid[2])
            res.append( (ocr_valid[1],[(start,end,label)]) )
    return res

def cross_val(data,n_splits=10,train_size=.25):
    """ Compute a xval score on the data
    """
    acc = []
    prec = []
    rec = []
    ss = ShuffleSplit(n_splits=n_splits,train_size=train_size,test_size=None)
    for train_indicies, test_indicies in ss.split(data):
        train = []
        test = []
        for i in train_indicies:
            train.append(data[i])
        for i in test_indicies:
            test.append(data[i])
        ner = train_ner(nlp, train, ['ZIPCODE'])
        acci,preci,reci = test_ner(ner,test)
        acc.append(acci)
        prec.append(preci)
        rec.append(reci)
    scores = np.array([acc,prec,rec])
    return [x.mean() for x in scores]

def test_ner(ner, test_data,correct_label='ZIPCODE'):
    """ Test accuracy, precision and recall on unseen test set using
    @param ner trained model
    @param test_data will be data formatted just like the training data
    eg
    [('ARMED FORCES PROPERTY, LLC 1856 OLD RESTON AVE STE 102 RESTON VA 20190-3330', [(65, 75, 'ZIPCODE')]), ...]
    @param correct_label must be the label used during training
    returns the accuracy, precision and recall for the test_data
    """
    y_true = []
    y_prob = []
    tp,fp,tn,fn = 0,0,0,0
    for doc in test_data:
        parsed = nlp.make_doc(doc[0])
        nlp.tagger(parsed)
        ner(parsed)
        for word in parsed:
                # set_trace()
            if word.ent_type_ == correct_label:
            # span objects don't have a ent_score, token objects don't have offsets
            # we have to look back into the doc obj return the span with the same 
			# index as the word to get the char offset
                i = parsed.doc[word.i:word.i].start_char 
                if i >= doc[1][0][0]  and i < doc[1][0][1]: 
			# if the first character of the token is within the ent_type bound
            # we'll assume the whole token is correctly classified as ent_type
                    y_true.append(1)
                    tp+=1 # true positive
                    y_prob.append(word.ent_score)
                else:
                    y_true.append(0) # False Positive
                    fp += 1
                    y_prob.append(word.ent_score)
            else:
                i = parsed.doc[word.i:word.i].start_char
                if i >= doc[1][0][0]  and i < doc[1][0][1]:
                    y_true.append(1) # False Negative
                    fn += 1
                    y_prob.append(1-word.ent_score)
                else:
                    y_true.append(0) # True Negative
                    tn += 1
                    y_prob.append(1-word.ent_score)
    acc = (tp + tn) / (tp +tn +fp + fn)
    prec = tp / (tp+fp)
    rec = tp / (tp + fn)
    return (acc,prec,rec) # accuracy

def tagOneExample(model_dir=None):
    """ shows how to fully train the model, print the model applied to one example
    and save the trained model
    """
    if model_dir is not None:
        model_dir = pathlib.Path(model_dir)
        if not model_dir.exists():
            model_dir.mkdir()
        assert model_dir.is_dir()

    nlp = spacy.load('en', parser=False, entity=False, add_vectors=False)

    # v1.1.2 onwards
    if nlp.tagger is None:
        print('---- WARNING ----')
        print('Data directory not found')
        print('please run: `python -m spacy.en.download –force all` for better performance')
        print('Using feature templates for tagging')
        print('-----------------')
    nlp.tagger = Tagger(nlp.vocab, features=Tagger.feature_templates)

    train_data = create_training_data('zips.csv')
    ner = train_ner(nlp, train_data, ['ZIPCODE'])

    doc = nlp.make_doc('1265 Henderson Ln. Hayward, nb " 09231-0342')
    nlp.tagger(doc)
    ner(doc) # run ner and modify doc in place 
    #for word in doc:
    #    print(word.text, word.tag_, word.ent_type_, word.ent_iob)

    if model_dir is not None:
        with (model_dir / 'config.json').open('w') as file_:
            json.dump(ner.cfg, file_)
        ner.model.dump(str(model_dir / 'model')) # save model


if __name__ == '__main__':
    data = create_training_data('zips.csv')
    if sys.argv[1] == '--lc':
        for i in np.linspace(0.1,.90,20):
            print("Train size {}, Accuracy: {:.2f}, Prec: {:.2f}, Rec: {:.2f}".format(int(len(data)*i), *cross_val(data,train_size=i)))
    if sys.argv[1] == '--m':
        data = create_training_data('ziptest.csv')
        ner = train_ner(nlp, data, ['ZIPCODE'])
        while 1:
            value_in = input('>').strip()
            doc = nlp.make_doc(value_in)
            ner(doc)
            print ("\tZIPCODE: ",''.join([x.orth_ for x in doc if x.ent_type_=='ZIPCODE']))
            print ("\tConfidence: ",np.array([x.ent_score for x in doc if x.ent_type_=='ZIPCODE']).mean())
