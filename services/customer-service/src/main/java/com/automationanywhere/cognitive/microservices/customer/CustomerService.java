package com.automationanywhere.cognitive.microservices.customer;

import com.automationanywhere.cognitive.exceptions.DataNotFoundException;
import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.customer.repositories.CustomerRepository;
import com.automationanywhere.cognitive.microservices.model.Id;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Customer service implementation.
 */
@Service
public class CustomerService {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);

  private final CustomerRepository customerRepository;

  /**
   * Default constructor.
   *
   * @param customerRepository the customer repository instance.
   */
  @Autowired
  public CustomerService(CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }

  /**
   * List all customers.
   *
   * @return the list of all customers
   */
  @Async
  public CompletableFuture<List<Customer>> listAll() {
    LOGGER.debug("Listing all customers");
    return CompletableFuture.completedFuture(customerRepository.listAll());
  }

  @Async
  public CompletableFuture<Page<Customer>> list(Pageable pageable) {
    LOGGER.debug("Listing all customers");
    return CompletableFuture.completedFuture(customerRepository.list(pageable));
  }

  /**
   * Get customer by ID.
   *
   * @param customerId the customer ID
   * @return the customer
   */
  @Async
  public CompletableFuture<Customer> getCustomerById(Id customerId) {
    LOGGER.debug("Get customer by ID %s", customerId);
    CompletableFuture<Customer> future = new CompletableFuture<>();
    Customer customer = customerRepository.get(customerId);
    if (customer == null) {
      future.completeExceptionally(new DataNotFoundException());
    } else {
      future.complete(customer);
    }
    return future;
  }

  @Async
  public CompletableFuture<Void> saveOrUpdate(Customer customer) {
    LOGGER.debug("Get customer by ID %s", customer);
    customerRepository.create(customer);
    return CompletableFuture.completedFuture(null);
  }
}
