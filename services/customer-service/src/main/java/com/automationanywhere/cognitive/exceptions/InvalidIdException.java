package com.automationanywhere.cognitive.exceptions;

/**
 * Exception used when an error to convert a string to ID was found.
 */
public class InvalidIdException extends RuntimeException {

  /**
   * Default constructor.
   *
   * @param message the message of the error.
   * @param cause the related exception.
   */
  public InvalidIdException(String message, Throwable cause) {
    super(message, cause);
  }
}
