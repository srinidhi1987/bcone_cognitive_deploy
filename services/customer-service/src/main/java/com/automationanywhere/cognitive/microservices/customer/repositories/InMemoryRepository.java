package com.automationanywhere.cognitive.microservices.customer.repositories;

import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_1;
import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_2;
import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_3;

import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.model.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * Generic implementation of repository.
 */
@Repository
public class InMemoryRepository implements CustomerRepository {

  private static final Logger LOGGER = LoggerFactory.getLogger(InMemoryRepository.class);

  private ConcurrentMap<Id, Customer> recordsById;
  private CustomerIdGenerator idGenerator;

  /**
   * Default constructor.
   */
  @Autowired
  public InMemoryRepository(CustomerIdGenerator idGenerator) {
    this.idGenerator = idGenerator;
    this.recordsById = new ConcurrentHashMap<>();

    // Insert dummy data for tests
    recordsById.put(CUSTOMER_ID_1, new Customer(CUSTOMER_ID_1, "Cognitive 1"));
    recordsById.put(CUSTOMER_ID_2, new Customer(CUSTOMER_ID_2, "Cognitive 2"));
    recordsById.put(CUSTOMER_ID_3, new Customer(CUSTOMER_ID_3, "Cognitive 3"));
  }

  @Override
  public List<Customer> listAll() {
    LOGGER.info("Retrieving all records from storage");
    return new ArrayList<>(recordsById.values());
  }

  @Override
  public Page<Customer> list(Pageable pageable) {
    LOGGER.info("Retrieving a page of records from storage");
    ArrayList<Customer> customers = new ArrayList<>(recordsById.values());
    return new PageImpl<>(customers, pageable, customers.size());
  }

  @Override
  public Customer get(Id id) {
    LOGGER.info("Getting record from storage with ID: %s", id);
    return recordsById.get(id);
  }

  @Override
  public void create(Customer entity) {
    entity.setId(idGenerator.nextId());
    recordsById.put(entity.getId(), entity);
  }

  @Override
  public void update(Customer entity) {
    if (entity.getId() == null) {
      entity.setId(idGenerator.nextId());
    }
    recordsById.put(entity.getId(), entity);
  }

  @Override
  public Customer delete(Id id) {
    return recordsById.remove(id);
  }

  /**
   * Returns the recordsById.
   *
   * @return the value of recordsById
   */
  ConcurrentMap<Id, Customer> getRecordsById() {
    return recordsById;
  }
}
