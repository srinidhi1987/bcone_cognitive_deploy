package com.automationanywhere.cognitive.app.config;

import com.automationanywhere.cognitive.app.thread.ThreadPoolFactory;
import java.util.concurrent.Executor;
import org.springframework.context.annotation.Bean;

public class ServiceConfig {

  private static final long DEFAULT_CORE_POOL_SIZE = 2;
  private static final long DEFAULT_MAX_POOL_SIZE = 10;
  private static final long DEFAULT_KEEP_ALIVE_TIMEOUT = 5000;
  private static final long DEFAULT_QUEUE_CAPACITY = 500;
  private static final String DEFAULT_QUEUE_NAME = "SERVICE-ASYNC-EXEC-";

  private ApplicationConfiguration appConfig;

  public ServiceConfig(ApplicationConfiguration appConfig) {
    this.appConfig = appConfig;
  }

  /**
   * Returns the thread pool executor responsible for all executions of methods annotated with
   *
   * @return the thread pool executor
   * @Async annotation.
   */
  @Bean
  public Executor asyncExecutor() {
    return ThreadPoolFactory.create(
        (int) this.appConfig.serviceExecutorCorePoolSize(DEFAULT_CORE_POOL_SIZE),
        (int) this.appConfig.serviceExecutorMaxPoolSize(DEFAULT_MAX_POOL_SIZE),
        (int) this.appConfig.serviceExecutorKeepAliveTimeout(DEFAULT_KEEP_ALIVE_TIMEOUT),
        (int) this.appConfig.serviceExecutorQueueCapacity(DEFAULT_QUEUE_CAPACITY),
        this.appConfig.serviceExecutorQueueName(DEFAULT_QUEUE_NAME)
    );
  }
}
