package com.automationanywhere.cognitive.app.logging.tomcat;

import static java.util.Optional.ofNullable;

import com.automationanywhere.cognitive.app.logging.tomcat.LocalAccessLogValve.AccessLogElement;
import java.io.CharArrayWriter;
import java.util.Date;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;

/**
 * Decorates the standard remote addr element by adding the value of the x-forwarded-for header if
 * present.
 */
public class RemoteAddrElementDecorator implements AccessLogElement {

  private static final String X_FORWARDED_FOR = "x-forwarded-for";

  private final AccessLogElement accessLogElement;

  RemoteAddrElementDecorator(AccessLogElement accessLogElement) {
    this.accessLogElement = accessLogElement;
  }

  @Override
  public void addElement(CharArrayWriter buf, Date date, Request request, Response response,
      long time) {

    this.accessLogElement.addElement(buf, date, request, response, time);

    String header = ofNullable(request.getHeader(X_FORWARDED_FOR)).orElse("").trim();

    if (!header.isEmpty()) {
      buf.append("/");
      buf.append(header);
    }
  }
}
