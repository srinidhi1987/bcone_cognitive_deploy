package com.automationanywhere.cognitive.app.thread;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.core.task.TaskDecorator;

/**
 * Propagates log context: sets up a log context before a runnable execution and restores it to the
 * previous state after the runnable termination.
 */
public class LogContextPropagatingDecorator implements TaskDecorator {

  @Override
  public Runnable decorate(final Runnable runnable) {
    return () -> this.propagate(ThreadContext.getContext(), runnable);
  }

  void propagate(final Map<String, String> context, final Runnable runnable) {
    ThreadContext.putAll(new HashMap<>(context));

    try {
      runnable.run();

    } finally {
      ThreadContext.clearMap();
      ThreadContext.putAll(context);
    }
  }
}
