package com.automationanywhere.cognitive.microservices.customer.adapters.rest;

import com.automationanywhere.cognitive.microservices.customer.models.CustomerId;
import com.automationanywhere.cognitive.microservices.model.Id;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Jackson module supporting UUIDId classes. It allows to use {@link Id} in models and UUIDs
 * represented as strings will be converted to UUIDId objects or vice versa.
 */
public class CustomerIdCodec extends SimpleModule {

  public CustomerIdCodec() {
    super("CustomerIdCodec", Version.unknownVersion());

    this.addSerializer(CustomerId.class, new CustomerIdSerializer());
    this.addDeserializer(CustomerId.class, new CustomerIdDeserializer());
  }

  public class CustomerIdDeserializer extends StdDeserializer<CustomerId> {

    public CustomerIdDeserializer() {
      super(CustomerId.class);
    }

    @Override
    public CustomerId deserialize(final JsonParser p, final DeserializationContext ctxt)
        throws IOException {
      CustomerId id = null;

      TreeNode treeNode = p.getCodec().readTree(p);
      if (!(treeNode instanceof NullNode)) {
        if (treeNode instanceof TextNode) {
          TextNode node = (TextNode) treeNode;
          id = CustomerId.fromString(node.asText());
        } else {
          StringWriter w = new StringWriter();
          p.getCodec().getFactory().createGenerator(w).writeTree(treeNode);
          throw new JsonParseException(p, w + " is not supported by any known Id implementation");
        }
      }

      return id;
    }
  }

  public class CustomerIdSerializer extends JsonSerializer<CustomerId> {

    @Override
    public void serialize(CustomerId value, JsonGenerator gen, SerializerProvider serializers)
        throws IOException {

      if (value == null) {
        gen.writeNull();

      } else {
        gen.writeString(value.toString());
      }
    }
  }
}
