package com.automationanywhere.cognitive.microservices.customer.adapters.rest;

public interface Accepts {

  String ALL = "accept=*/*";
  String JSON = "accept=application/json";
  String JSON_V0 = "accept=application/vnd.cognitive.v+json";
  String JSON_V1 = "accept=application/vnd.cognitive.v1+json";
  String JSON_V2 = "accept=application/vnd.cognitive.v2+json";
}
