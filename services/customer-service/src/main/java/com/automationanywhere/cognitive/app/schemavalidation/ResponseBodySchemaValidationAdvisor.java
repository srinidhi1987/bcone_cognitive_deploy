package com.automationanywhere.cognitive.app.schemavalidation;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

import com.automationanywhere.cognitive.app.schemavalidation.exception.InvalidSchemaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class ResponseBodySchemaValidationAdvisor implements ResponseBodyAdvice<Object> {

  private SchemaValidator validator;

  @Autowired
  public ResponseBodySchemaValidationAdvisor(SchemaValidator validator) {
    this.validator = validator;
  }

  @Override
  public boolean supports(MethodParameter returnType,
      Class<? extends HttpMessageConverter<?>> converterType) {
    SchemaValidation annotation = returnType.getMethod().getAnnotation(SchemaValidation.class);
    return annotation != null && validator.supportsOutput(annotation);
  }

  @Override
  public Object beforeBodyWrite(
      Object body,
      MethodParameter returnType,
      MediaType selectedContentType,
      Class<? extends HttpMessageConverter<?>> selectedConverterType,
      ServerHttpRequest request, ServerHttpResponse response
  ) throws InvalidSchemaException {

    if (response instanceof ServletServerHttpResponse) {
      ServletServerHttpResponse resp = (ServletServerHttpResponse) response;
      if (resp.getServletResponse().getStatus() == SC_NOT_FOUND && body == null) {
        return null;
      }
    }

    SchemaValidation annotation = returnType.getMethod().getAnnotation(SchemaValidation.class);
    this.validator.validateOutput(body, annotation, returnType.getParameterType());

    return body;
  }
}
