package com.automationanywhere.cognitive.microservices.customer.adapters.rest;

import static com.automationanywhere.cognitive.microservices.customer.adapters.rest.ContentTypes.JSON_V1;
import static com.automationanywhere.cognitive.microservices.customer.adapters.rest.ContentTypes.JSON_V2;

import com.automationanywhere.cognitive.app.schemavalidation.SchemaValidation;
import com.automationanywhere.cognitive.microservices.customer.CustomerService;
import com.automationanywhere.cognitive.microservices.customer.adapters.CustomerDTO;
import com.automationanywhere.cognitive.microservices.customer.adapters.CustomerMapper;
import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.customer.models.CustomerId;
import com.automationanywhere.cognitive.microservices.model.Id;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API endpoint for customer resource.
 */
@RestController
public class CustomerResource {

  private final CustomerService customerService;
  private final CustomerMapper customerMapper;

  /**
   * Default constructor.
   *
   * @param customerService the customer service instance.
   * @param customerMapper the customer mapper instance.
   */
  @Autowired
  public CustomerResource(CustomerService customerService, CustomerMapper customerMapper,
      ObjectMapper objectMapper) {
    this.customerService = customerService;
    this.customerMapper = customerMapper;

    objectMapper.registerModule(new CustomerIdCodec());
  }

  /**
   * Receives a GET http request without attributes or parameters, list all customers and returns
   * the list of customers in JSON format. The customer record must have the customerId and name.
   *
   * @return the list of all customers
   */
  @Deprecated
  @GetMapping(value = "api/customers", produces = {JSON_V1}, headers = {Accepts.JSON_V1})
  @SchemaValidation(output = "ListOfCustomers")
  public CompletableFuture<List<CustomerDTO>> listAllCustomers() {
    CompletableFuture<List<Customer>> future = customerService.listAll();
    return future.thenApply(customerMapper::getDTO);
  }

  /**
   * Receives a GET http request with query parameters to sort and limit the response in pages, list
   * customers and returns the paginated list of customers in JSON format. The customer record must
   * have the customerId and name. The page metadata information must have the total of pages, total
   * of elements, size of each page, number of the current page, the current elements in the page,
   * if the current page is the first or last and sort information.
   *
   * @return the list of all customers
   */
  @GetMapping(value = "api/customers",
      produces = {JSON_V2},
      headers = {Accepts.JSON_V2, Accepts.JSON_V0, Accepts.JSON, Accepts.ALL}
  )
  @SchemaValidation(output = "ListOfCustomersPaginated")
  public CompletableFuture<Page<CustomerDTO>> listCustomersPaginated(Pageable pageable) {
    CompletableFuture<Page<Customer>> future = customerService.list(pageable);
    return future.thenApply(page -> page.map(customerMapper::getDTO));
  }

  /**
   * Retrieves a specific customer.
   *
   * @param id the ID of the customer
   * @return the customer
   */
  @GetMapping(value = "api/customers/{id}",
      produces = {JSON_V1},
      headers = {Accepts.JSON_V1, Accepts.JSON_V0, Accepts.JSON, Accepts.ALL}
  )
  @SchemaValidation(output = "CustomerOutputV1")
  public CompletableFuture<CustomerDTO> getCustomer(@PathVariable String id) {
    Id customerId = CustomerId.fromString(id);
    CompletableFuture<Customer> future = customerService.getCustomerById(customerId);
    return future.thenApply(customerMapper::getDTO);
  }

  /**
   * Adds a new customer.
   *
   * @param customerDTO the ID of the customer
   * @return nothing
   */
  @PostMapping(value = "api/customers",
      produces = {JSON_V1},
      headers = {Accepts.JSON_V1, Accepts.JSON_V0, Accepts.JSON, Accepts.ALL}
  )
  @SchemaValidation(input = "CustomerInputV1")
  public CompletableFuture<Void> addCustomer(@RequestBody CustomerDTO customerDTO) {
    Customer customer = customerMapper.get(customerDTO);
    return customerService.saveOrUpdate(customer);
  }
}
