package com.automationanywhere.cognitive.microservices.customer.models;


import com.automationanywhere.cognitive.exceptions.InvalidIdException;
import com.automationanywhere.cognitive.microservices.model.UUID;

public class CustomerId extends UUID {

  public CustomerId() {
  }

  private CustomerId(String customerId) {
    super(customerId);
  }

  public static CustomerId fromString(String customerId) {
    try {
      return new CustomerId(customerId);
    } catch (Exception e) {
      throw new InvalidIdException("Invalid ID: " + customerId, e);
    }
  }
}
