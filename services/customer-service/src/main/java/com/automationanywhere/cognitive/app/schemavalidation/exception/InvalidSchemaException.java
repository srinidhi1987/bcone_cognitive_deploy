package com.automationanywhere.cognitive.app.schemavalidation.exception;

public class InvalidSchemaException extends RuntimeException {

  public InvalidSchemaException(String message, Exception e) {
    super(message, e);
  }
}
