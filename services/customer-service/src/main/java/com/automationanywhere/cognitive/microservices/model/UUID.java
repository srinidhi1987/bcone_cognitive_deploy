package com.automationanywhere.cognitive.microservices.model;

public class UUID implements Id {

  private java.util.UUID id;

  public UUID() {
    this.id = java.util.UUID.randomUUID();
  }

  public UUID(String id) {
    this.id = java.util.UUID.fromString(id);
  }

  @Override
  public String toString() {
    return id.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UUID uuid = (UUID) o;

    return id != null ? id.equals(uuid.id) : uuid.id == null;
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
