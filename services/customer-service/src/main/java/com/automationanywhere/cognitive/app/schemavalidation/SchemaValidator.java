package com.automationanywhere.cognitive.app.schemavalidation;

public interface SchemaValidator {

  boolean supportsInput(SchemaValidation annotation);

  boolean supportsOutput(SchemaValidation annotation);

  void validateInput(String content, SchemaValidation annotation, Class<?> parameterType);

  void validateOutput(Object content, SchemaValidation annotation, Class<?> parameterType);

  void registerSchema(SchemaValidation annotation);
}
