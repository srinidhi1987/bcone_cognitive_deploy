package com.automationanywhere.cognitive.app.schemavalidation.exception;

import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JsonSchemaValidationException extends RuntimeException {

  public final List<ProcessingMessage> report;

  public JsonSchemaValidationException(final ProcessingReport processingReport) {
    super("Json validation failed");

    List<ProcessingMessage> report = new ArrayList<>();
    for (final ProcessingMessage processingMessage : processingReport) {
      report.add(processingMessage);
    }
    this.report = Collections.unmodifiableList(report);
  }

  /**
   * Returns the report.
   *
   * @return the value of report
   */
  public List<ProcessingMessage> getReport() {
    return report;
  }

  @Override
  public String getMessage() {
    return super.getMessage() + "\n:" + this.report;
  }
}
