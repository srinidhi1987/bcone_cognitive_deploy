package com.automationanywhere.cognitive.microservices.customer.models;

/**
 * The customer representation.
 */
public class Customer {

  private CustomerId id;
  private String name;
  private Address address;

  /**
   * Default constructor.
   */
  public Customer() {
  }

  /**
   * Full constructor.
   *
   * @param id the ID of customer
   * @param name the name of customer
   */
  public Customer(CustomerId id, String name) {
    this.id = id;
    this.name = name;
  }

  /**
   * Returns the entity ID.
   *
   * @return the entity ID
   */
  public CustomerId getId() {
    return id;
  }

  /**
   * Sets the entity ID.
   *
   * @param id the entity ID to be set
   */
  public void setId(CustomerId id) {
    this.id = id;
  }

  /**
   * Returns the name of the customer.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of the customer.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }
}
