package com.automationanywhere.cognitive.app.schemavalidation.exception;

public class SchemaValidationException extends RuntimeException {

  public SchemaValidationException(String message, Exception e) {
    super(message, e);
  }
}
