package com.automationanywhere.cognitive.microservices.customer.adapters;

import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Responsible for convert the Customer entity in DTO.
 */
@Component
public class CustomerMapper {

  /**
   * Converts the Customer entity to CustomerDTO.
   *
   * @param customer the customer entity
   * @return the customer DTO
   */
  public CustomerDTO getDTO(Customer customer) {
    return customer == null ? null : new CustomerDTO(customer.getId(), customer.getName());
  }

  /**
   * Converts the list of customer entities to a list of customer DTOs.
   *
   * @param customers list of customer entities to be converted
   * @return list of customer DTOs converted
   */
  public List<CustomerDTO> getDTO(List<Customer> customers) {
    return customers == null ? null
        : customers.stream().map(this::getDTO).collect(Collectors.toList());
  }

  /**
   * Converts the CustomerDTO to Customer entity.
   *
   * @param customerDTO the customer DTO
   * @return the customer entity
   */
  public Customer get(CustomerDTO customerDTO) {
    return customerDTO == null ? null
        : new Customer(customerDTO.getCustomerId(), customerDTO.getName());
  }
}
