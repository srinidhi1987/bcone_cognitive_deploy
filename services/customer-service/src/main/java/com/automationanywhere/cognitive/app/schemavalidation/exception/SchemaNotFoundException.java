package com.automationanywhere.cognitive.app.schemavalidation.exception;

public class SchemaNotFoundException extends RuntimeException {

  public SchemaNotFoundException(String message) {
    super(message);
  }
}
