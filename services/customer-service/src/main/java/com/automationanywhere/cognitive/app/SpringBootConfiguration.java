package com.automationanywhere.cognitive.app;

import com.automationanywhere.cognitive.app.config.ApplicationConfiguration;
import com.automationanywhere.cognitive.app.logging.tomcat.LocalAccessLogValve;
import com.automationanywhere.cognitive.app.schemavalidation.DefaultSchemaValidator;
import com.automationanywhere.cognitive.app.schemavalidation.SchemaValidator;
import com.automationanywhere.cognitive.app.thread.ThreadPoolFactory;
import com.automationanywhere.cognitive.common.configuration.FileConfigurationProvider;
import com.automationanywhere.cognitive.common.configuration.FileConfigurationProviderFactory;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.Executor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Order Microservice SpringBoot configuration.
 */
@SpringBootApplication
@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class,
    HibernateJpaAutoConfiguration.class})
@ComponentScan(basePackages = {
    "com.automationanywhere.cognitive.app",
    "com.automationanywhere.cognitive.microservices.customer",
    "com.automationanywhere.cognitive.common.healthcheck"
})
public class SpringBootConfiguration extends WebMvcConfigurerAdapter implements
    EmbeddedServletContainerCustomizer {

  private static final long DEFAULT_CORE_POOL_SIZE = 2;
  private static final long DEFAULT_MAX_POOL_SIZE = 10;
  private static final long DEFAULT_KEEP_ALIVE_TIMEOUT = 5000;
  private static final long DEFAULT_QUEUE_CAPACITY = 500;
  private static final String DEFAULT_QUEUE_NAME = "SERVICE-ASYNC-EXEC-";

  private ApplicationConfiguration appConfig;

  /**
   * Default constructor.
   */
  public SpringBootConfiguration() {
    System.setProperty("log4j2.isThreadContextMapInheritable", "true");
    System.setProperty("server.tomcat.accesslog.enabled", "true");

    FileConfigurationProviderFactory configurationProviderFactory = new FileConfigurationProviderFactory();
    FileConfigurationProvider configurationProvider = configurationProviderFactory.create();

    appConfig = new ApplicationConfiguration(configurationProvider, new BuildInfoFactory());
  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/").setViewName("redirect:/info/api/index.html");
    registry.addViewController("/info/api").setViewName("redirect:/info/api/index.html");
  }

  @Override
  public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
    configurer.setDefaultTimeout(appConfig.asyncHttpServletRequestTimeout());
  }

  /**
   * Customize the access log
   * @param container the servlet container to be
   */
  @Override
  public void customize(ConfigurableEmbeddedServletContainer container) {
    if (container instanceof TomcatEmbeddedServletContainerFactory) {
      TomcatEmbeddedServletContainerFactory factory = (TomcatEmbeddedServletContainerFactory) container;
      LocalAccessLogValve accessLogValve = new LocalAccessLogValve();
      accessLogValve
          .setPattern("%t %a:%{remote}p->%v:%{local}p \"%m %U%q %H %{Accept}i\" %s %b %D-%Fms");
      factory.addContextValves(accessLogValve);

    } else {
      throw new RuntimeException("This customizer does not support your configured container");
    }
  }

  /**
   * Returns the ApplicationConfiguration instance.
   *
   * @return the ApplicationConfiguration
   */
  @Bean
  public ApplicationConfiguration applicationConfiguration() {
    return this.appConfig;
  }

  /**
   * Returns the thread pool executor responsible for all executions of methods annotated with
   *
   * @return the thread pool executor
   */
  @Bean
  public Executor asyncExecutor() {
    return ThreadPoolFactory.create(
        (int) this.appConfig.serviceExecutorCorePoolSize(DEFAULT_CORE_POOL_SIZE),
        (int) this.appConfig.serviceExecutorMaxPoolSize(DEFAULT_MAX_POOL_SIZE),
        (int) this.appConfig.serviceExecutorKeepAliveTimeout(DEFAULT_KEEP_ALIVE_TIMEOUT),
        (int) this.appConfig.serviceExecutorQueueCapacity(DEFAULT_QUEUE_CAPACITY),
        this.appConfig.serviceExecutorQueueName(DEFAULT_QUEUE_NAME)
    );
  }

  @Bean
  public SchemaValidator schemaValidator(ObjectMapper jsonMapper) {
    return DefaultSchemaValidator.Builder.get()
        .schemaDir("schemas")
        .checkClasspath(true)
        .jsonSchemaExt("json")
        .jsonMapper(jsonMapper)
        .build();
  }
}
