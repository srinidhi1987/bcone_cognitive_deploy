package com.automationanywhere.cognitive.app.error;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

/**
 * DTO class to keep the error data.
 */
public class ErrorDTO {

  private static final String PATH_KEY = "org.springframework.web.servlet.HandlerMapping.pathWithinHandlerMapping";

  private final long timestamp;
  private final int status;
  private final String error;
  private final String exception;
  private final String message;
  private final String path;

  /**
   * Constructs the error DTO instance with error code and exception.
   *
   * @param status the error code
   * @param e the exception instance
   * @param request the web request instance
   */
  public ErrorDTO(int status, Throwable e, WebRequest request) {
    this.timestamp = System.currentTimeMillis();
    this.status = status;
    this.error = HttpStatus.valueOf(status).getReasonPhrase();
    this.exception = e.getClass().getName();
    this.message = e.getMessage();
    this.path = request != null ? (String) request.getAttribute(PATH_KEY, SCOPE_REQUEST) : "";

  }

  /**
   * Constructs the error DTO with all parameters.
   *
   * @param status the error code
   * @param exception the string
   * @param request the web request instance
   */
  public ErrorDTO(int status, String exception, String message, WebRequest request) {
    this.timestamp = System.currentTimeMillis();
    this.status = status;
    this.error = HttpStatus.valueOf(status).getReasonPhrase();
    this.exception = exception;
    this.message = message;
    this.path = request != null ? (String) request.getAttribute(PATH_KEY, SCOPE_REQUEST) : "";
  }

  /**
   * Returns the timestamp.
   *
   * @return the value of timestamp
   */
  public long getTimestamp() {
    return timestamp;
  }

  /**
   * Returns the status.
   *
   * @return the value of status
   */
  public int getStatus() {
    return status;
  }

  /**
   * Returns the error.
   *
   * @return the value of error
   */
  public String getError() {
    return error;
  }

  /**
   * Returns the exception.
   *
   * @return the value of exception
   */
  public String getException() {
    return exception;
  }

  /**
   * Returns the message.
   *
   * @return the value of message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Returns the path.
   *
   * @return the value of path
   */
  public String getPath() {
    return path;
  }

  @Override
  public String toString() {
    return "ErrorDTO{"
        + "timestamp=" + timestamp
        + ", status=" + status
        + ", error='" + error + '\''
        + ", exception='" + exception + '\''
        + ", message='" + message + '\''
        + ", path='" + path + '\''
        + '}';
  }
}
