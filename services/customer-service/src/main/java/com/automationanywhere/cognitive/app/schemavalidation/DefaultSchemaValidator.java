package com.automationanywhere.cognitive.app.schemavalidation;

import com.automationanywhere.cognitive.app.schemavalidation.exception.InvalidSchemaException;
import com.automationanywhere.cognitive.app.schemavalidation.exception.JsonSchemaValidationException;
import com.automationanywhere.cognitive.app.schemavalidation.exception.SchemaNotFoundException;
import com.automationanywhere.cognitive.app.schemavalidation.exception.SchemaValidationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Scanner;

public class DefaultSchemaValidator implements SchemaValidator {

  private static final String JSON_V4_SCHEMA_IDENTIFIER = "http://json-schema.org/draft-04/schema#";
  private static final String JSON_SCHEMA_IDENTIFIER_ELEMENT = "$schema";

  private boolean checkClasspath;
  private String schemaDir;
  private String jsonSchemaExt;
  private ObjectMapper jsonMapper;
  private HashMap<String, JsonNode> jsonSchemas;

  private DefaultSchemaValidator(boolean checkClasspath, String schemaDir, String jsonSchemaExt,
      ObjectMapper jsonMapper) {
    this.checkClasspath = checkClasspath;
    this.schemaDir = schemaDir;
    this.jsonSchemaExt = jsonSchemaExt;
    this.jsonMapper = jsonMapper;
    this.jsonSchemas = new HashMap<>();
  }

  @Override
  public boolean supportsInput(SchemaValidation annotation) {
    return annotation != null && !annotation.input().isEmpty();
  }

  @Override
  public boolean supportsOutput(SchemaValidation annotation) {
    return annotation != null && !annotation.output().isEmpty();
  }

  @Override
  public void validateInput(String content, SchemaValidation annotation, Class<?> parameterType) {
    JsonNode inputSchema = jsonSchemas.get(annotation.input());
    if (inputSchema == null) {
      throw new SchemaNotFoundException("Schema not registered: " + annotation.input());
    }
    ProcessingReport processingMessages = checkJsonSchema(inputSchema, content);
    if (processingMessages != null && !processingMessages.isSuccess()) {
      throw new JsonSchemaValidationException(processingMessages);
    }
  }

  @Override
  public void validateOutput(Object obj, SchemaValidation annotation, Class<?> parameterType) {
    JsonNode outputSchema = jsonSchemas.get(annotation.output());
    if (outputSchema == null) {
      throw new SchemaNotFoundException("Schema not registered: " + annotation.input());
    }

    String content;
    try {
      content = jsonMapper.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw new SchemaValidationException("Error to convert the output object to json", e);
    }

    ProcessingReport processingMessages = checkJsonSchema(outputSchema, content);
    if (processingMessages != null && !processingMessages.isSuccess()) {
      throw new JsonSchemaValidationException(processingMessages);
    }
  }

  @Override
  public void registerSchema(SchemaValidation annotation) {
    String inputKey = annotation.input();
    if (!inputKey.isEmpty()) {
      jsonSchemas.put(inputKey, getJsonSchema(getSchema(inputKey, jsonSchemaExt)));
    }

    String outputKey = annotation.output();
    if (!outputKey.isEmpty()) {
      jsonSchemas.put(outputKey, getJsonSchema(getSchema(outputKey, jsonSchemaExt)));
    }
  }

  private String getSchema(String schemaPath, String ext) {
    String file = schemaDir == null ? schemaPath + "." + ext
        : schemaDir + File.separator + schemaPath + "." + ext;
    InputStream is;
    if (checkClasspath) {
      is = getClass().getClassLoader().getResourceAsStream(file);
      if (is == null) {
        throw new SchemaNotFoundException("Schema not found on classpath: " + file);
      }

    } else {
      try {
        is = new FileInputStream(file);
      } catch (FileNotFoundException ignore) {
        throw new SchemaNotFoundException("Schema not found on file system: " + file);
      }
    }

    return new Scanner(is, StandardCharsets.UTF_8.name()).useDelimiter("\\A").next();
  }

  private JsonNode getJsonSchema(String schema) throws InvalidSchemaException {
    try {
      return JsonLoader.fromString(schema);
    } catch (IOException e) {
      throw new InvalidSchemaException("Could no parse json schema", e);
    }
  }

  private ProcessingReport checkJsonSchema(JsonNode schemaNode, String jsonText)
      throws InvalidSchemaException {
    try {
      JsonNode schemaIdentifier = schemaNode.get(JSON_SCHEMA_IDENTIFIER_ELEMENT);
      if (schemaIdentifier == null) {
        ((ObjectNode) schemaNode).put(JSON_SCHEMA_IDENTIFIER_ELEMENT, JSON_V4_SCHEMA_IDENTIFIER);
      }

      JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
      JsonSchema jsonSchemaNode = factory.getJsonSchema(schemaNode);

      JsonNode jsonNode = JsonLoader.fromString(jsonText);
      return jsonSchemaNode.validate(jsonNode);

    } catch (Exception e) {
      throw new InvalidSchemaException("Error to validate json schema", e);
    }
  }

  public static class Builder {

    private final boolean checkClasspath;
    private final String schemaDir;
    private final String jsonSchemaExt;
    private final ObjectMapper jsonMapper;

    private Builder(boolean checkClasspath, String schemaDir, String jsonSchemaExt,
        ObjectMapper jsonMapper) {
      this.checkClasspath = checkClasspath;
      this.schemaDir = schemaDir;
      this.jsonSchemaExt = jsonSchemaExt;
      this.jsonMapper = jsonMapper;
    }

    public static Builder get() {
      return new Builder(true, null, "json", null);
    }

    public Builder checkClasspath(boolean checkClasspath) {
      return new Builder(checkClasspath, this.schemaDir, this.jsonSchemaExt, this.jsonMapper);
    }

    public Builder schemaDir(String schemaDir) {
      return new Builder(this.checkClasspath, schemaDir, this.jsonSchemaExt, this.jsonMapper);
    }

    public Builder jsonSchemaExt(String jsonSchemaExt) {
      return new Builder(this.checkClasspath, this.schemaDir, jsonSchemaExt, this.jsonMapper);
    }

    public Builder jsonMapper(ObjectMapper jsonMapper) {
      return new Builder(this.checkClasspath, this.schemaDir, this.jsonSchemaExt, jsonMapper);
    }

    public SchemaValidator build() {
      return new DefaultSchemaValidator(
          this.checkClasspath,
          this.schemaDir,
          this.jsonSchemaExt == null || this.jsonSchemaExt.isEmpty() ? "json" : this.jsonSchemaExt,
          this.jsonMapper == null ? new ObjectMapper() : this.jsonMapper
      );
    }
  }
}
