package com.automationanywhere.cognitive.microservices.customer.repositories;

import com.automationanywhere.cognitive.microservices.customer.models.CustomerId;
import com.automationanywhere.cognitive.microservices.model.IdGenerator;
import org.springframework.stereotype.Component;

@Component
class CustomerIdGenerator implements IdGenerator<CustomerId> {

  @Override
  public CustomerId nextId() {
    return new CustomerId();
  }
}
