package com.automationanywhere.cognitive.microservices.model;

public interface IdGenerator<T extends Id> {

  T nextId();
}
