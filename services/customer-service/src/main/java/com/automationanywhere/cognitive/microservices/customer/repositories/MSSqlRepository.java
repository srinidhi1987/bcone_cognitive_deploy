package com.automationanywhere.cognitive.microservices.customer.repositories;

import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.model.Id;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class MSSqlRepository implements CustomerRepository {

  @Override
  public List<Customer> listAll() {
    return null;
  }

  @Override
  public Page<Customer> list(Pageable pageable) {
    return null;
  }

  @Override
  public Customer get(Id id) {
    return null;
  }

  @Override
  public void create(Customer order) {

  }

  @Override
  public void update(Customer order) {

  }

  @Override
  public Customer delete(Id id) {
    return null;
  }
}
