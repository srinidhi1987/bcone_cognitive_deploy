package com.automationanywhere.cognitive.app.logging.tomcat;

import com.automationanywhere.cognitive.app.logging.tomcat.LocalAccessLogValve.AccessLogElement;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Formats a date using a customizable format.
 */
public class DateTimeAccessLogElement implements AccessLogElement {

  private static final Logger LOGGER = LogManager.getLogger(DateTimeAccessLogElement.class);

  private static final Locale DEFAULT_LOCALE = Locale.US;
  private static final TimeZone DEFAULT_TZ = TimeZone.getTimeZone("UTC");
  private static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

  private final SimpleDateFormat formatter;

  public DateTimeAccessLogElement() {
    this(DEFAULT_TZ, DEFAULT_LOCALE, DEFAULT_FORMAT);
  }

  public DateTimeAccessLogElement(TimeZone tz, Locale locale, String format) {
    this.formatter = new SimpleDateFormat(format, locale);
    this.formatter.setTimeZone(tz);
    this.formatter.setCalendar(Calendar.getInstance(tz, locale));
  }

  public void addElement(CharArrayWriter buf, Date date, Request request, Response response,
      long time) {

    try {
      buf.write(this.formatter.format(date));
    } catch (final IOException ex) {
      LOGGER.error("Failed to format the date", ex);
    }
  }
}
