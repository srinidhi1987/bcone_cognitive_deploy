package com.automationanywhere.cognitive.app.schemavalidation;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
public class SchemaValidatorRegister implements BeanPostProcessor {

  private final SchemaValidator schemaValidator;

  @Autowired
  public SchemaValidatorRegister(SchemaValidator schemaValidator) {
    this.schemaValidator = schemaValidator;
  }

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName)
      throws BeansException {

    if (bean.getClass().getAnnotation(RestController.class) != null) {
      Arrays.stream(bean.getClass().getMethods())
          .filter(m -> Modifier.isPublic(m.getModifiers()) && !Modifier.isStatic(m.getModifiers()))
          .forEach(m -> {
            SchemaValidation annotation = m.getAnnotation(SchemaValidation.class);
            if (annotation != null) {
              schemaValidator.registerSchema(annotation);
            }
          });
    }
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(final Object bean, final String beanName)
      throws BeansException {
    return bean;
  }
}
