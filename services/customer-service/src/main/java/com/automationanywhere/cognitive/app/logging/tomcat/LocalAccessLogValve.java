package com.automationanywhere.cognitive.app.logging.tomcat;

import java.io.CharArrayWriter;
import java.io.StringWriter;
import java.util.Date;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.AbstractAccessLogValve;
import org.apache.catalina.valves.AccessLogValve;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Sends all access logs to the standard logger.
 */
public class LocalAccessLogValve extends AccessLogValve {

  private static final Logger LOGGER = LogManager.getLogger(LocalAccessLogValve.class);

  @SuppressWarnings("Duplicates")
  public void log(final CharArrayWriter message) {

    try {
      StringWriter w = new StringWriter();
      message.writeTo(w);
      message.flush();

      LOGGER.trace(w.toString());

    } catch (final Exception ex) {
      LOGGER.error("Failed to write a log message", ex);
    }
  }

  @Override
  @SuppressWarnings("Duplicates")
  protected AbstractAccessLogValve.AccessLogElement createAccessLogElement(final char pattern) {

    AbstractAccessLogValve.AccessLogElement ale;
    switch (pattern) {
      case 't':
        ale = new DateTimeAccessLogElement();
        break;
      case 'a':
        ale = new RemoteAddrElementDecorator(
            new AccessLogElementWrapper(super.createAccessLogElement(pattern)));
        break;
      default:
        ale = super.createAccessLogElement(pattern);
    }
    return ale;
  }

  public interface AccessLogElement extends AbstractAccessLogValve.AccessLogElement {
  }

  public class AccessLogElementWrapper implements AccessLogElement {

    private final AbstractAccessLogValve.AccessLogElement element;

    AccessLogElementWrapper(AbstractAccessLogValve.AccessLogElement element) {
      this.element = element;
    }

    @Override
    public void addElement(CharArrayWriter buf, Date date, Request request, Response response,
        long time) {
      this.element.addElement(buf, date, request, response, time);
    }
  }
}