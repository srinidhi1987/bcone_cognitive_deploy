package com.automationanywhere.cognitive.microservices.customer.repositories;

import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.model.Id;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Keeps the data of customers.
 */
public interface CustomerRepository {

  /**
   * List all entities.
   *
   * @return all entities
   */
  List<Customer> listAll();

  /**
   * List a single page of customers
   *
   * @param pageable the pageable data
   * @return the respective page of customers
   */
  Page<Customer> list(Pageable pageable);

  /**
   * Returns the entity by yours ID.
   *
   * @param id the ID of the entity
   * @return the entity
   */
  Customer get(Id id);

  /**
   * Stores a new entity. An ID will be set to the entity.
   *
   * @param order the order to be stored
   */
  void create(Customer order);

  /**
   * Updates a entity, if the entity has no ID a new one will be set.
   *
   * @param order the order to be updated
   */
  void update(Customer order);

  /**
   * Deletes an entity by yours ID.
   *
   * @param id the ID of the entity
   * @return the entity removed
   */
  Customer delete(Id id);
}
