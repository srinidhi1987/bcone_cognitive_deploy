package com.automationanywhere.cognitive.app.thread;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Factory to create thread pools.
 */
@SuppressWarnings("Duplicates")
public final class ThreadPoolFactory {

  /**
   * Utility class should not have a public constructor.
   */
  private ThreadPoolFactory() {
  }

  /**
   * Creates a new thread pool and initialize it.
   *
   * @param corePoolSize the core pool size
   * @param maxPoolSize the max pool size
   * @param keepAliveTimeout the keep alive timeout
   * @param queueCapacity the queue capacity
   * @param queueName the queue name
   * @return the new thread pool created.
   */
  public static ThreadPoolTaskExecutor create(int corePoolSize, int maxPoolSize,
      int keepAliveTimeout, int queueCapacity, String queueName) {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setTaskDecorator(new LogContextPropagatingDecorator());
    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxPoolSize);
    executor.setKeepAliveSeconds(keepAliveTimeout);
    executor.setQueueCapacity(queueCapacity);
    executor.setThreadNamePrefix(queueName);
    executor.initialize();
    return executor;
  }
}
