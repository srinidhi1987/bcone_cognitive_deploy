package com.automationanywhere.cognitive.app;

import org.springframework.boot.SpringApplication;

public class Application {

  /**
   * Private constructor restricts instantiation.
   */
  private Application() {
  }

  /**
   * Entry point.
   *
   * @param args - app args
   */
  public static void main(final String[] args) {
    SpringApplication.run(SpringBootConfiguration.class, args);
  }
}
