package com.automationanywhere.cognitive.microservices.customer.repositories.inmemory;

import com.automationanywhere.cognitive.microservices.customer.models.CustomerId;

/**
 * Dummy data to be used by test and initialize the repositories.
 */
public final class DummyData {

  public static final CustomerId CUSTOMER_ID_1 = CustomerId
      .fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb");
  public static final CustomerId CUSTOMER_ID_2 = CustomerId
      .fromString("b0c60f97-eeaa-46f7-8ef8-2d34fd2c244f");
  public static final CustomerId CUSTOMER_ID_3 = CustomerId
      .fromString("a45d3dc3-46ee-4061-a3b5-11b30c85476a");

  private DummyData() {
  }
}
