package com.automationanywhere.cognitive.app.config;

import com.automationanywhere.cognitive.common.configuration.ConfigurationProvider;
import com.automationanywhere.cognitive.common.configuration.exceptions.NoSuchKeyException;
import com.automationanywhere.cognitive.common.healthcheck.info.BuildInfoFactory;
import com.automationanywhere.cognitive.common.healthcheck.model.BuildInfo;
import java.util.UUID;

/**
 * The class to keeps all application configuration.
 */
public class ApplicationConfiguration {

  private static final String BASE_KEY = "aa.cognitive.micro-services.customer.";

  private final ConfigurationProvider provider;
  private final BuildInfo buildInfo;

  /**
   * Default constructor.
   *
   * @param provider the <code>ConfigurationProvider</code> instance.
   */
  public ApplicationConfiguration(ConfigurationProvider provider, BuildInfoFactory buildInfoFactory) {
    this.provider = provider;
    this.buildInfo = buildInfoFactory.build();
  }

  public String applicationName() {
    return buildInfo.getAppName();
  }

  public String applicationVersion() {
    return buildInfo.getVersion();
  }

  public String applicationCID() {
    return getValue(BASE_KEY + "application.cid", UUID.randomUUID().toString());
  }

  public String applicationUserId() {
    return getValue(BASE_KEY + "application.user-id", "<not implemented>");
  }

  public long asyncHttpServletRequestTimeout() {
    return getValue(BASE_KEY + " async-http-servlet-request-timeout", 30000);
  }

  public long serviceExecutorCorePoolSize(long value) {
    return getValue(BASE_KEY + "services.thread-pool.core-pool-size", value);
  }

  public long serviceExecutorMaxPoolSize(long value) {
    return getValue(BASE_KEY + "services.thread-pool.max-pool-size", value);
  }

  public long serviceExecutorKeepAliveTimeout(long value) {
    return getValue(BASE_KEY + "services.thread-pool.keep-alive-timeout", value);
  }

  public long serviceExecutorQueueCapacity(long value) {
    return getValue(BASE_KEY + "services.thread-pool.queue-capacity", value);
  }

  public String serviceExecutorQueueName(String value) {
    return getValue(BASE_KEY + "services.thread-pool.queue-name", value);
  }

  private <T> T getValue(String key, T defaultValue) {
    try {
      return this.provider.getValue(BASE_KEY + key);

    } catch (NoSuchKeyException e) {
      return defaultValue;
    }
  }
}
