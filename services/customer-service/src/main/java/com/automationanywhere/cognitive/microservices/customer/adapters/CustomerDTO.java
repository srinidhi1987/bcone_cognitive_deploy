package com.automationanywhere.cognitive.microservices.customer.adapters;


import com.automationanywhere.cognitive.microservices.customer.models.CustomerId;

/**
 * DTO class to keep the customer data.
 */
public class CustomerDTO {

  private CustomerId customerId;
  private String name;

  /**
   * Default constructor.
   */
  public CustomerDTO() {
  }

  /**
   * Full constructor.
   *
   * @param customerId the customer ID
   * @param name the customer name
   */
  public CustomerDTO(CustomerId customerId, String name) {
    this.customerId = customerId;
    this.name = name;
  }

  /**
   * Returns the id.
   *
   * @return the value of id
   */
  public CustomerId getCustomerId() {
    return customerId;
  }

  /**
   * Sets the id.
   *
   * @param customerId the id to be set
   */
  public void setCustomerId(CustomerId customerId) {
    this.customerId = customerId;
  }

  /**
   * Returns the name.
   *
   * @return the value of name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }
}
