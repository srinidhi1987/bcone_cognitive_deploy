package com.automationanywhere.cognitive.app.error;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import com.automationanywhere.cognitive.app.schemavalidation.exception.JsonSchemaValidationException;
import com.automationanywhere.cognitive.exceptions.DataNotFoundException;
import com.automationanywhere.cognitive.exceptions.InvalidIdException;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Responsible to handle all exceptions thrown by controllers.
 */
@ControllerAdvice
public class ApplicationErrorHandler extends ResponseEntityExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationErrorHandler.class);

  /**
   * Handles a json schema validation.
   *
   * @param e the exception thrown
   * @param request the request instance
   * @return the error DTO instance with the exception information
   */
  @ExceptionHandler({JsonSchemaValidationException.class})
  public ResponseEntity<ErrorDTO> handleJsonSchemaValidationException(Exception e,
      WebRequest request) {
    JsonSchemaValidationException jsonException = (JsonSchemaValidationException) e;
    ErrorDTO error;
    if (jsonException.getReport() != null) {
      StringBuilder sb = new StringBuilder();
      for (ProcessingMessage processingMessage : jsonException.getReport()) {
        if (processingMessage.getLogLevel() == LogLevel.ERROR) {
          sb.append(processingMessage.getMessage()).append('\n');
        }
      }
      error = new ErrorDTO(BAD_REQUEST.value(), jsonException.getClass().getName(), sb.toString(),
          request);
    } else {
      error = new ErrorDTO(BAD_REQUEST.value(), jsonException.getCause(), request);
    }
    LOGGER.error("Handling a top level exception: " + error);
    return new ResponseEntity<>(error, BAD_REQUEST);
  }

  /**
   * Handles an exception to create ID.
   *
   * @param e the exception thrown
   * @param request the request instance
   * @return the error DTO instance with the exception information
   */
  @ExceptionHandler({InvalidIdException.class})
  public ResponseEntity<ErrorDTO> handleInvalidIdException(Exception e, WebRequest request) {
    ErrorDTO error = new ErrorDTO(BAD_REQUEST.value(), e, request);
    LOGGER.error("Handling an exception to create ID: " + error);
    return new ResponseEntity<>(error, BAD_REQUEST);
  }

  /**
   * Handles an exception to create ID.
   *
   * @param e the exception thrown
   * @param request the request instance
   */
  @ResponseStatus(code = NOT_FOUND)
  @ExceptionHandler({DataNotFoundException.class})
  public void handleDataNotFoundException(Exception e, WebRequest request) {
    LOGGER.error("Handling an exception of data not found: " + e.getMessage());
  }
}

