package com.automationanywhere.cognitive.microservices.customer.adapters.rest;

public interface ContentTypes {

  String JSON_V1 = "application/vnd.cognitive.v1+json";
  String JSON_V2 = "application/vnd.cognitive.v2+json";
}
