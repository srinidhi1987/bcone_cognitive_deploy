package com.automationanywhere.cognitive.app.schemavalidation;

import java.io.IOException;
import java.lang.reflect.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

@ControllerAdvice
public class RequestBodySchemaValidationAdvisor implements RequestBodyAdvice {

  private final SchemaValidator validator;

  @Autowired
  public RequestBodySchemaValidationAdvisor(SchemaValidator validator) {
    this.validator = validator;
  }

  @Override
  public boolean supports(final MethodParameter methodParameter, final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType) {
    SchemaValidation annotation = methodParameter.getParameterAnnotation(SchemaValidation.class);
    return annotation != null && validator.supportsInput(annotation);
  }

  @Override
  public Object handleEmptyBody(final Object body, final HttpInputMessage inputMessage,
      final MethodParameter parameter, final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType) {
    return body;
  }

  @Override
  public HttpInputMessage beforeBodyRead(final HttpInputMessage inputMessage,
      final MethodParameter methodParameter, final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
    SchemaValidation annotation = methodParameter.getParameterAnnotation(SchemaValidation.class);
    CachingHttpInputMessage result = new CachingHttpInputMessage(inputMessage);
    this.validator
        .validateInput(result.getContents(), annotation, methodParameter.getParameterType());
    return result;
  }

  @Override
  public Object afterBodyRead(final Object body, final HttpInputMessage inputMessage,
      final MethodParameter parameter, final Type targetType,
      final Class<? extends HttpMessageConverter<?>> converterType) {
    return body;
  }
}
