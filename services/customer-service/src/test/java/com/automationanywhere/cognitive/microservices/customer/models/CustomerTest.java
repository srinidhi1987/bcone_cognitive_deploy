package com.automationanywhere.cognitive.microservices.customer.models;

import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_1;
import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

/**
 * Test class of Customer.
 */
public class CustomerTest {

  @Test
  public void testEmptyConstructorGetterAndSetter() {
    // GIVEN
    CustomerId customerId = CUSTOMER_ID_1;
    String name = "Cognitive";

    // WHEN
    Customer customer = new Customer();
    customer.setId(customerId);
    customer.setName(name);

    // THEN
    assertThat(customer.getId()).isEqualTo(customerId);
    assertThat(customer.getName()).isEqualTo(name);
  }

  @Test
  public void testFullConstructorAndGetter() {
    // GIVEN
    CustomerId customerId = new CustomerId();
    String name = "Cognitive";

    // WHEN
    Customer customer = new Customer(customerId, name);

    // THEN
    assertThat(customer.getId()).isEqualTo(customerId);
    assertThat(customer.getName()).isEqualTo(name);
  }
}
