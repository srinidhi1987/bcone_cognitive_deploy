package com.automationanywhere.cognitive.microservices.customer.adapters.rest;

import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_1;
import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_2;
import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_3;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import com.automationanywhere.cognitive.app.SpringBootConfiguration;
import com.automationanywhere.cognitive.microservices.customer.adapters.CustomerDTO;
import com.automationanywhere.cognitive.microservices.model.Id;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * Integration test class of REST API OrderResource endpoint.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {SpringBootConfiguration.class})
public class CustomerResourceIntegrationTest extends AbstractTestNGSpringContextTests {

  private static final int TOTAL_ORDERS = 3;
  private static final int INVALID_CODE = 400;

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void integrationTestRequestAllCustomers() {
    objectMapper.registerModule(new CustomerIdCodec());

    // GIVEN
    String url = "/api/customers";

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, "application/vnd.cognitive.v1+json");
    HttpEntity<Void> entity = new HttpEntity<>(null, headers);

    // WHEN
    ResponseEntity<List<CustomerDTO>> responseEntity = restTemplate
        .exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<CustomerDTO>>() {
        });

    // THEN
    List<CustomerDTO> orders = responseEntity.getBody();
    assertThat(orders.size()).isEqualTo(TOTAL_ORDERS);
    Map<Id, CustomerDTO> customersMap = orders.stream()
        .collect(Collectors.toMap(CustomerDTO::getCustomerId, customer -> customer));

    CustomerDTO orderDTO = customersMap.get(CUSTOMER_ID_1);
    assertThat(orderDTO).isNotNull();
    assertThat(orderDTO.getCustomerId()).isEqualTo(CUSTOMER_ID_1);
    assertThat(orderDTO.getName()).isEqualTo("Cognitive 1");

    orderDTO = customersMap.get(CUSTOMER_ID_2);
    assertThat(orderDTO).isNotNull();
    assertThat(orderDTO.getCustomerId()).isEqualTo(CUSTOMER_ID_2);
    assertThat(orderDTO.getName()).isEqualTo("Cognitive 2");

    orderDTO = customersMap.get(CUSTOMER_ID_3);
    assertThat(orderDTO).isNotNull();
    assertThat(orderDTO.getCustomerId()).isEqualTo(CUSTOMER_ID_3);
    assertThat(orderDTO.getName()).isEqualTo("Cognitive 3");
  }

  @Test
  public void integrationTestRequestCustomerWithNotFoundCustomerIdGetNull() {

    // GIVEN
    String url = "/api/customers/" + UUID.randomUUID().toString();

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, "application/vnd.cognitive.v1+json");
    HttpEntity<Void> entity = new HttpEntity<>(null, headers);

    // WHEN
    ResponseEntity<String> responseEntity = restTemplate
        .exchange(url, HttpMethod.GET, entity, String.class);
    String body = responseEntity.getBody();

    // THEN
    assertThat(body).isNull();
    assertThat(responseEntity.getStatusCode()).isEqualTo(NOT_FOUND);
  }

  @Test
  public void integrationTestRequestCustomerWithInvalidCustomerIdGetError() throws IOException {

    // GIVEN
    String invalidUuid = "aaa";
    String url = "/api/customers/" + invalidUuid;

    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, "application/vnd.cognitive.v1+json");
    HttpEntity<Void> entity = new HttpEntity<>(null, headers);

    // WHEN
    ResponseEntity<String> responseEntity = restTemplate
        .exchange(url, HttpMethod.GET, entity, String.class);

    // THEN
    String error = responseEntity.getBody();
    JsonNode jsonNode = objectMapper.readTree(error);
    assertThat(jsonNode.get("status").asInt()).isEqualTo(INVALID_CODE);
    assertThat(jsonNode.get("message").asText()).isEqualTo("Invalid ID: " + invalidUuid);
  }
}
