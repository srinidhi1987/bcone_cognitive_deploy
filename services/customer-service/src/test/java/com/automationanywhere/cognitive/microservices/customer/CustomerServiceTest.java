package com.automationanywhere.cognitive.microservices.customer;

import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_1;
import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_2;
import static com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData.CUSTOMER_ID_3;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

import com.automationanywhere.cognitive.app.SpringBootConfiguration;
import com.automationanywhere.cognitive.exceptions.DataNotFoundException;
import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.customer.models.CustomerId;
import com.automationanywhere.cognitive.microservices.customer.repositories.CustomerRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * Class test of OrderService.
 */
@SpringBootTest(classes = {SpringBootConfiguration.class})
public class CustomerServiceTest extends AbstractTestNGSpringContextTests {

  private static final int TOTAL_CUSTOMERS = 3;

  @Autowired
  private CustomerService customerService;

  @MockBean
  @Autowired
  private CustomerRepository customerRepository;

  @Test
  public void testGetCustomerWithValidCustomerIdRetrievingCustomer() throws Exception {

    // GIVEN
    CustomerId customerId = new CustomerId();
    String name = "Cognitive";

    when(customerRepository.get(customerId)).thenReturn(new Customer(customerId, name));

    // WHEN
    Customer customer = customerService.getCustomerById(customerId).get();

    // THEN
    assertThat(customer).isNotNull();

    assertThat(customer.getId()).isEqualTo(customerId);
    assertThat(customer.getName()).isEqualTo(name);
  }

  @Test
  public void testGetCustomerWithNotFoundCustomerIdRetrievingEmpty() throws Exception {

    // GIVEN
    CustomerId customerId = new CustomerId();

    // WHEN
    Throwable throwable = catchThrowable(() -> customerService.getCustomerById(customerId).get());

    // THEN
    assertThat(throwable).isInstanceOf(ExecutionException.class);
    assertThat(throwable.getCause()).isInstanceOf(DataNotFoundException.class).hasNoCause();
  }

  @Test
  public void testListAllCustomerRetrievingCustomers() throws Exception {

    // GIVEN
    List<Customer> customers = new ArrayList<>();
    customers.add(new Customer(CUSTOMER_ID_1, "Cognitive 1"));
    customers.add(new Customer(CUSTOMER_ID_2, "Cognitive 2"));
    customers.add(new Customer(CUSTOMER_ID_3, "Cognitive 3"));
    when(customerRepository.listAll()).thenReturn(customers);

    // WHEN
    List<Customer> customersResult = customerService.listAll().get();

    // THEN
    assertThat(customersResult.size()).isEqualTo(TOTAL_CUSTOMERS);

    Customer customer = customersResult.get(0);
    assertThat(customer.getId()).isEqualTo(CUSTOMER_ID_1);
    assertThat(customer.getName()).isEqualTo("Cognitive 1");
  }

  @Test
  public void testListAllCustomerRetrievingCustomersPaginated() throws Exception {

    // GIVEN
    List<Customer> customers = new ArrayList<>();
    customers.add(new Customer(CUSTOMER_ID_1, "Cognitive 1"));
    customers.add(new Customer(CUSTOMER_ID_2, "Cognitive 2"));
    customers.add(new Customer(CUSTOMER_ID_3, "Cognitive 3"));

    Pageable pageable = new PageRequest(0, 10);
    Page<Customer> page = new PageImpl<>(customers, pageable, 3);

    when(customerRepository.list(pageable)).thenReturn(page);

    // WHEN
    Page<Customer> pageResult = customerService.list(pageable).get();

    // THEN
    assertThat(pageResult.getTotalElements()).isEqualTo(TOTAL_CUSTOMERS);

    Customer customer = pageResult.getContent().get(0);
    assertThat(customer.getId()).isEqualTo(CUSTOMER_ID_1);
    assertThat(customer.getName()).isEqualTo("Cognitive 1");
  }
}
