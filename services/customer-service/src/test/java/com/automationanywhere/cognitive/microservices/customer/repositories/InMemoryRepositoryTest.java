package com.automationanywhere.cognitive.microservices.customer.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import com.automationanywhere.cognitive.microservices.customer.models.Customer;
import com.automationanywhere.cognitive.microservices.customer.repositories.inmemory.DummyData;
import com.automationanywhere.cognitive.microservices.model.Id;
import java.util.List;
import org.testng.annotations.Test;

/**
 * Test class of InMemoryRepository.
 */
public class InMemoryRepositoryTest {

  private static final int TOTAL_CUSTOMERS = 3;

  @Test
  public void testListAllCustomers() {

    // GIVEN
    CustomerIdGenerator idGenerator = new CustomerIdGenerator();
    InMemoryRepository repository = new InMemoryRepository(idGenerator);

    // WHEN
    List<Customer> customers = repository.listAll();

    // THEN
    assertThat(customers.size()).isEqualTo(TOTAL_CUSTOMERS);
  }

  @Test
  public void testCreateReadCustomerWithAllAttributes() {

    // GIVEN
    CustomerIdGenerator idGenerator = new CustomerIdGenerator();
    InMemoryRepository repository = new InMemoryRepository(idGenerator);

    String name = "Cognitive";
    Customer customer = new Customer();
    customer.setName(name);

    // WHEN
    repository.create(customer);

    // THEN
    Customer expectedCustomer = repository.get(customer.getId());
    assertThat(customer.getId()).isEqualTo(expectedCustomer.getId());
    assertThat(customer.getName()).isEqualTo(expectedCustomer.getName());
  }

  @Test
  public void testUpdateReadCustomerWithAllAttributes() {

    // GIVEN
    Id customerId = DummyData.CUSTOMER_ID_1;
    String name = "Cognitive_2";

    CustomerIdGenerator idGenerator = new CustomerIdGenerator();
    InMemoryRepository repository = new InMemoryRepository(idGenerator);
    Customer customer = repository.get(customerId);

    // WHEN
    customer.setName(name);
    repository.update(customer);

    // THEN
    Customer expectedCustomer = repository.get(customer.getId());
    assertThat(customer.getId()).isEqualTo(expectedCustomer.getId());
    assertThat(customer.getName()).isEqualTo(name);
  }

  @Test
  public void testUpdateReadCustomerWithAllAttributesAndWithoutId() {

    // GIVEN
    String name = "Cognitive_2";
    CustomerIdGenerator idGenerator = new CustomerIdGenerator();
    InMemoryRepository repository = new InMemoryRepository(idGenerator);
    Customer customer = new Customer();
    customer.setName(name);

    // WHEN
    repository.update(customer);

    // THEN
    Customer expectedCustomer = repository.get(customer.getId());
    assertThat(customer.getId()).isEqualTo(expectedCustomer.getId());
    assertThat(customer.getName()).isEqualTo(name);
  }

  @Test
  public void testDeleteCustomerWithValidId() {

    // GIVEN
    CustomerIdGenerator idGenerator = new CustomerIdGenerator();
    InMemoryRepository repository = new InMemoryRepository(idGenerator);
    Id customerId = DummyData.CUSTOMER_ID_1;
    assertThat(repository.get(customerId)).isNotNull();

    // WHEN
    repository.delete(customerId);

    // THEN
    assertThat(repository.get(customerId)).isNull();
  }
}
