package com.automationanywhere.cognitive.iqbot.application.appregistration;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.appconfiguration.Configuration;
import com.automationanywhere.cognitive.common.domain.service.security.KeyManager;
import com.automationanywhere.cognitive.common.domain.service.security.KeyProvider;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.AsymmetricKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.ConfigurationResponse;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.FinalizeRegistrationRequest;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.RegistrationRequest;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.RegistrationResponse;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersion;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersionProvider;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.RoutingNameProvider;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class AppRegistrationTest {

  private AppRegistration appRegistration;

  @Mock
  private KeyManager keyManager;
  @Mock
  private AppVersionProvider appVersionProvider;
  @Mock
  private AppConfigurationRepository appConfigurationRepository;
  @Mock
  private KeyProvider destinationKeyProvider;
  @Mock
  private RoutingNameProvider routingNameProvider;

  @BeforeMethod
  public void setUp() throws Exception {
    initMocks(this);

    appRegistration = new AppRegistration(
        keyManager, appVersionProvider, routingNameProvider, appConfigurationRepository,
        destinationKeyProvider
    );
  }

  @Test
  public void registerAppShouldThrowIfTheAppWasRegistered() {
    // given
    RegistrationRequest request = new RegistrationRequest(
        "appId", "http://example.com", "1.0alpha"
    );

    Configuration configuration = new Configuration("http://example.com", "1.0beta", "appId", true);
    doReturn(configuration).when(appConfigurationRepository).load();

    // when
    Throwable th = catchThrowable(() -> appRegistration.registerApp(request));

    // then
    assertThat(th).isInstanceOf(AppRegistrationException.class)
        .hasMessage("The app has already been registered");
  }

  @Test
  public void registerAppShouldRegisterAppIfCommunicationKeyExists() {
    // given
    RegistrationRequest request = new RegistrationRequest(
        "appId", "http://example.com", "1.0alpha"
    );
    AppVersion appVersion = new AppVersion(
        "IQ Bot", "2.0", "6.x", "branch", "hash", "time"
    );
    AppVersion expectedAppVersion = new AppVersion(
        "IQ Bot", "2.0", "6.x", "branch", "hash", "time"
    );
    RegistrationResponse expectedResponse = new RegistrationResponse(
        "pubKey", expectedAppVersion
    );
    AsymmetricKey key = new AsymmetricKey("privKey", "pubKey");
    Configuration configuration = new Configuration(
        "http://example.com", "1.0alpha", "appId", false
    );

    doReturn(null).when(appConfigurationRepository).load();
    doReturn(key).when(keyManager).retrieveKey(KeyName.CR_COMMUNICATION_KEY);
    doReturn(appVersion).when(appVersionProvider).appVersion();

    // when
    RegistrationResponse response = appRegistration.registerApp(request);

    // then
    assertThat(response).isEqualToComparingFieldByFieldRecursively(expectedResponse);
    verify(keyManager, never()).createKey(KeyName.CR_COMMUNICATION_KEY);
    verify(appConfigurationRepository).save(configuration);
  }

  @Test
  public void registerAppShouldRegisterAppIfCommunicationKeyDoesNotExist() {
    // given
    RegistrationRequest request = new RegistrationRequest(
        "appId", "http://example.com", "1.0alpha"
    );
    AppVersion appVersion = new AppVersion(
        "IQ Bot", "2.0", "6.x", "branch", "hash", "time"
    );
    AppVersion expectedAppVersion = new AppVersion(
        "IQ Bot", "2.0", "6.x", "branch", "hash", "time"
    );
    RegistrationResponse expectedResponse = new RegistrationResponse(
        "pubKey", expectedAppVersion
    );
    AsymmetricKey key = new AsymmetricKey("privKey", "pubKey");
    Configuration configuration = new Configuration(
        "http://example.com", "1.0alpha", "appId", false
    );

    doReturn(null).when(appConfigurationRepository).load();
    doReturn(null).doReturn(key).when(keyManager).retrieveKey(KeyName.CR_COMMUNICATION_KEY);
    doReturn(appVersion).when(appVersionProvider).appVersion();

    // when
    RegistrationResponse response = appRegistration.registerApp(request);

    // then
    assertThat(response).isEqualToComparingFieldByFieldRecursively(expectedResponse);
    verify(keyManager).createKey(KeyName.CR_COMMUNICATION_KEY);
    verify(appConfigurationRepository).save(configuration);
  }

  @Test
  public void registerAppShouldWrapExceptionsIfAnExceptionIsUnhandled() {
    // given
    RegistrationRequest request = new RegistrationRequest(
        "appId", "http://example.com", "1.0alpha"
    );

    RuntimeException e = new RuntimeException();
    doThrow(e).when(appConfigurationRepository).load();

    // when
    Throwable th = catchThrowable(() -> appRegistration.registerApp(request));

    // then
    assertThat(th).isInstanceOf(AppRegistrationException.class).hasCause(e);
  }

  @Test
  public void initializeAppShouldThrowIfConfigurationDoesNotExist() {
    // given
    FinalizeRegistrationRequest request = new FinalizeRegistrationRequest("appId");

    doReturn(null).when(appConfigurationRepository).load();

    // when
    Throwable th = catchThrowable(() -> appRegistration.initializeApp(request));

    // then
    assertThat(th).isInstanceOf(AppInitializationException.class)
        .hasMessage("The app hasn't yet been registered");
  }

  @Test
  public void initializeAppShouldThrowIfAppIdsDoNotMatch() {
    // given
    FinalizeRegistrationRequest request = new FinalizeRegistrationRequest("appId");
    Configuration configuration = new Configuration(
        "http://example.com", "1.0alpha", "appId1", false
    );

    doReturn(configuration).when(appConfigurationRepository).load();

    // when
    Throwable th = catchThrowable(() -> appRegistration.initializeApp(request));

    // then
    assertThat(th).isInstanceOf(AppInitializationException.class)
        .hasMessage("App validation error");
  }

  @Test
  public void initializeAppShouldWrapExceptionsIfAnExceptionIsUnhandled() {
    // given
    FinalizeRegistrationRequest request = new FinalizeRegistrationRequest("appId");

    RuntimeException e = new RuntimeException();
    doThrow(e).when(appConfigurationRepository).load();

    // when
    Throwable th = catchThrowable(() -> appRegistration.initializeApp(request));

    // then
    assertThat(th).isInstanceOf(AppInitializationException.class).hasCause(e);
  }

  @Test
  public void testInitializeApp() {
    // given
    FinalizeRegistrationRequest request = new FinalizeRegistrationRequest("appId");
    Configuration configuration = new Configuration(
        "http://example.com", "1.0a", "appId", false
    );

    doReturn(configuration).when(appConfigurationRepository).load();

    // when
    appRegistration.initializeApp(request);

    // then
    verify(keyManager, never())
        .migrateKey(KeyName.SYSTEM_KEY, destinationKeyProvider);
  }

  @Test
  public void configurationShouldReturnConfigurationWhenTheAppIsRegistered() {
    // given
    Configuration configuration = new Configuration(
        "http://example.com", "1.0alpha", "appId", true
    );
    ConfigurationResponse expectedConfiguration = new ConfigurationResponse(
        "http://example.com", "rn"
    );
    doReturn(configuration).when(appConfigurationRepository).load();
    doReturn("rn").when(routingNameProvider).routingName();

    // when
    ConfigurationResponse resultConfiguration = appRegistration.configuration();

    // then
    assertThat(resultConfiguration)
        .isEqualToComparingFieldByFieldRecursively(expectedConfiguration);
  }

  @Test
  public void configurationShouldReturnConfigurationWhenTheAppRegistrationIsNotComplete() {
    // given
    Configuration configuration = new Configuration(
        "http://example.com", "1.0alpha", "appId", false
    );
    doReturn(configuration).when(appConfigurationRepository).load();

    // when
    ConfigurationResponse resultConfiguration = appRegistration.configuration();

    // then
    assertThat(resultConfiguration).isNull();
  }

  @Test
  public void configurationShouldReturnNullWhenTheAppIsNotRegistered() {
    // given
    doReturn(null).when(appConfigurationRepository).load();

    // when
    ConfigurationResponse resultConfiguration = appRegistration.configuration();

    // then
    assertThat(resultConfiguration).isNull();
  }
}
