package com.automationanywhere.cognitive.iqbot.application.appregistration.dto;

public class RegistrationRequest {

  public final String appId;
  public final String controlRoomUrl;
  public final String controlRoomVersion;

  public RegistrationRequest(
      final String appId,
      final String controlRoomUrl,
      final String controlRoomVersion
  ) {
    this.appId = appId;
    this.controlRoomUrl = controlRoomUrl;
    this.controlRoomVersion = controlRoomVersion;
  }
}
