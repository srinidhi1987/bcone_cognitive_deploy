package com.automationanywhere.cognitive.iqbot.application.appregistration.dto;

import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersion;

public class RegistrationResponse {

  public final String key;
  public final AppVersion appVersion;

  public RegistrationResponse(final String key, final AppVersion appVersion) {
    this.key = key;
    this.appVersion = appVersion;
  }
}
