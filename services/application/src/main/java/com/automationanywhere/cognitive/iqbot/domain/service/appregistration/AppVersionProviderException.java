package com.automationanywhere.cognitive.iqbot.domain.service.appregistration;

/**
 * Throws if an app version provider is not able to create an app descriptor.
 */
public class AppVersionProviderException extends RuntimeException {

  private static final long serialVersionUID = -1703046548228200432L;

  public AppVersionProviderException(final String message) {
    super(message);
  }

  public AppVersionProviderException(final Throwable cause) {
    super(cause);
  }
}
