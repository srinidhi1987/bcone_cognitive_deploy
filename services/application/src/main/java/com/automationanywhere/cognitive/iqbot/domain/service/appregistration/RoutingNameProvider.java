package com.automationanywhere.cognitive.iqbot.domain.service.appregistration;

/**
 * Implementations should provide the routing name associated with each instance of the application.
 * It must be unique per Control Room, meaning that no two application may share the same routing
 * name if their are registered in the same Control Room.
 */
public interface RoutingNameProvider {

  /**
   * Returns a routing name of the current instance of the application.
   *
   * @return routing name or null if it's not available.
   */
  String routingName() throws RoutingNameProviderException;
}
