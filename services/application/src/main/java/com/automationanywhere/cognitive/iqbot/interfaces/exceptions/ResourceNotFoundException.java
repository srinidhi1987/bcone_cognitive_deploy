package com.automationanywhere.cognitive.iqbot.interfaces.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8975545178740880476L;

  public ResourceNotFoundException() {
  }
}
