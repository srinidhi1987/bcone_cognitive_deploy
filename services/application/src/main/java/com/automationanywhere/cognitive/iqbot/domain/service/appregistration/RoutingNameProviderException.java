package com.automationanywhere.cognitive.iqbot.domain.service.appregistration;

/**
 * Throw when a routing provide fails to return a routing name of the current running instance
 * of teh application.
 */
public class RoutingNameProviderException extends RuntimeException {

  private static final long serialVersionUID = 2802494202688704513L;

  public RoutingNameProviderException() {
  }
}
