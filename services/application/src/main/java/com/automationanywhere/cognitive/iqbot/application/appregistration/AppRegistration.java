package com.automationanywhere.cognitive.iqbot.application.appregistration;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.appconfiguration.Configuration;
import com.automationanywhere.cognitive.common.domain.service.security.KeyManager;
import com.automationanywhere.cognitive.common.domain.service.security.KeyProvider;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.AsymmetricKey;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.ConfigurationResponse;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.FinalizeRegistrationRequest;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.RegistrationRequest;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.RegistrationResponse;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersionProvider;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.RoutingNameProvider;

/**
 * Registers the IQ Bot application in Control Room.
 */
public class AppRegistration {

  private final KeyManager keyManager;
  private final AppVersionProvider appVersionProvider;
  private final RoutingNameProvider routingNameProvider;
  private final AppConfigurationRepository appConfigurationRepository;
  private final KeyProvider destinationKeyProvider;

  public AppRegistration(
      final KeyManager keyManager,
      final AppVersionProvider appVersionProvider,
      final RoutingNameProvider routingNameProvider,
      final AppConfigurationRepository appConfigurationRepository,
      final KeyProvider destinationKeyProvider
  ) {
    this.keyManager = keyManager;
    this.appVersionProvider = appVersionProvider;
    this.routingNameProvider = routingNameProvider;
    this.appConfigurationRepository = appConfigurationRepository;
    this.destinationKeyProvider = destinationKeyProvider;
  }

  /**
   * Saves the IQ Bot registration data;
   * and create (if doesn't yet exit) a Control Room communication key
   * to store it in the local key storage.
   *
   * @param request - IQ Bot registration data.
   * @return Control Room communication public key and the app version.
   * @throws AppRegistrationException if the has already been registered.
   */
  public RegistrationResponse registerApp(final RegistrationRequest request)
      throws AppRegistrationException {
    RegistrationResponse response = null;
    try {
      Configuration configuration = appConfigurationRepository.load();
      if (configuration != null) {
        throw new AppRegistrationException("The app has already been registered");
      }

      AsymmetricKey key = (AsymmetricKey) keyManager.retrieveKey(KeyName.CR_COMMUNICATION_KEY);
      if (key == null) {
        keyManager.createKey(KeyName.CR_COMMUNICATION_KEY);
        key = (AsymmetricKey) keyManager.retrieveKey(KeyName.CR_COMMUNICATION_KEY);
      }

      configuration = new Configuration(
          request.controlRoomUrl,
          request.controlRoomVersion,
          request.appId,
          false
      );
      appConfigurationRepository.save(configuration);

      response = new RegistrationResponse(
          key.publicKey(),
          appVersionProvider.appVersion()
      );
    } catch (final AppRegistrationException ex) {
      throw ex;
    } catch (final Exception ex) {
      throw new AppRegistrationException(ex);
    }
    return response;
  }

  /**
   * Migrates the system key from the local key storage to Vault.
   *
   * @param request - contains the app id that should correspond to the actual IQ Bot app id.
   * @throws AppInitializationException if the app hasn't yet registered; or the provided app id
   * doesn't correspond to the known app id.
   */
  public void initializeApp(final FinalizeRegistrationRequest request)
      throws AppRegistrationException {
    try {
      Configuration configuration = appConfigurationRepository.load();
      if (configuration == null) {
        throw new AppInitializationException("The app hasn't yet been registered");
      }
      if (configuration.registered) {
        throw new AppInitializationException("The app has already been registered");
      }
      if (!configuration.appId.equals(request.appId)) {
        throw new AppInitializationException("App validation error");
      }

      configuration = new Configuration(
        configuration.controlRoomUrl,
        configuration.controlRoomVersion,
        configuration.appId,
        true
      );
      appConfigurationRepository.save(configuration);

      //TODO: We will have to migrate the key once the vault implementation is finished.
      //keyManager.migrateKey(KeyName.SYSTEM_KEY, KeyType.SYMMETRIC, destinationKeyProvider);
    } catch (final AppInitializationException ex) {
      throw ex;
    } catch (final Exception ex) {
      throw new AppInitializationException(ex);
    }
  }

  /**
   * Returns the current app's configuration,
   * or null if the app is not registered.
   *
   * @return current app's configuration or null.
   */
  public ConfigurationResponse configuration() {
    Configuration configuration = appConfigurationRepository.load();
    return configuration == null || !configuration.registered ? null : new ConfigurationResponse(
        configuration.controlRoomUrl,
        routingNameProvider.routingName()
    );
  }
}
