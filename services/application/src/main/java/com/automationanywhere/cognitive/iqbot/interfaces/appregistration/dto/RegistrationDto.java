package com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationDto {

  public final String controlRoomUrl;
  public final String controlRoomVersion;
  public final String appId;

  public RegistrationDto(
      @JsonProperty("crUrl") final String controlRoomUrl,
      @JsonProperty("crVersion") final String controlRoomVersion,
      @JsonProperty("appId") final String appId
  ) {
    this.controlRoomUrl = controlRoomUrl;
    this.controlRoomVersion = controlRoomVersion;
    this.appId = appId;
  }
}
