package com.automationanywhere.cognitive.iqbot.interfaces.appregistration;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.Configuration;
import com.automationanywhere.cognitive.iqbot.application.appregistration.AppRegistration;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.ConfigurationResponse;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.FinalizeRegistrationRequest;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.RegistrationRequest;
import com.automationanywhere.cognitive.iqbot.application.appregistration.dto.RegistrationResponse;
import com.automationanywhere.cognitive.iqbot.interfaces.ContentTypes;
import com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto.AcknowledgementDto;
import com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto.ConfigurationDto;
import com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto.HandshakeDto;
import com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto.RegistrationDto;
import com.automationanywhere.cognitive.iqbot.interfaces.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppRegistrationController {
  private final AppRegistration appRegistration;
  private static final String APP_ROUTING_NAME="IQBot";

  @Autowired
  public AppRegistrationController(final AppRegistration appRegistration) {
    this.appRegistration = appRegistration;
  }

  @RequestMapping(
      path = "/v1/registration/handshake",
      method = RequestMethod.POST,
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE}
  )
  public HandshakeDto handshake(
      @RequestBody final RegistrationDto dto
  ) {
    RegistrationResponse registrationResponse = appRegistration.registerApp(new RegistrationRequest(
        dto.appId, dto.controlRoomUrl, dto.controlRoomVersion
    ));
    return new HandshakeDto(
        registrationResponse.appVersion.productVersion,
        registrationResponse.key,
        APP_ROUTING_NAME
    );
  }

  @RequestMapping(
      path = "/v1/registration/acknowledge",
      method = RequestMethod.POST,
      consumes = {MediaType.APPLICATION_JSON_VALUE}
  )
  public void acknowledge(
      @RequestBody final AcknowledgementDto dto
  ) throws Exception {
    appRegistration.initializeApp(new FinalizeRegistrationRequest(dto.appId));
  }

  @RequestMapping(
      path = "/configuration",
      method = RequestMethod.GET,
      produces = {
          ContentTypes.JSON,
          ContentTypes.JSON_V1
      }
  )
  public ConfigurationDto configuration() {
    ConfigurationResponse configuration = appRegistration.configuration();
    if (configuration == null) {
      throw new ResourceNotFoundException();
    }
    return new ConfigurationDto(
        configuration.controlRoomUrl,
        configuration.routingName
    );
  }
}
