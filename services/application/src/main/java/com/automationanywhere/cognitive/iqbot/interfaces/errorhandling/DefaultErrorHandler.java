package com.automationanywhere.cognitive.iqbot.interfaces.errorhandling;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class DefaultErrorHandler {

  private static final Logger LOGGER = LogManager.getLogger(DefaultErrorHandler.class);

  private final ObjectMapper om;

  public DefaultErrorHandler(final ObjectMapper om) {
    this.om = om;
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<?> handleTopLevelException(
      final Exception ex,
      final ServletWebRequest request
  ) throws Exception {
    Throwable e = extractException(ex);

    LOGGER.error("Unhandled error", e);

    Object error = null;
    HttpStatus httpStatus = null;
    MediaType contentType = MediaType.APPLICATION_JSON_UTF8;

    if (e instanceof HttpClientErrorException) {
      HttpClientErrorException ce = (HttpClientErrorException) e;

      contentType = Optional.ofNullable(ce.getResponseHeaders().getContentType())
          .orElse(MediaType.APPLICATION_OCTET_STREAM);

      httpStatus = ce.getStatusCode();
      error = ce.getResponseBodyAsByteArray();
    } else if (e instanceof HttpStatusCodeException) {
      HttpStatusCodeException statusCodeException = (HttpStatusCodeException) e;
      httpStatus = HttpStatus.valueOf(statusCodeException.getRawStatusCode());
      error = statusCodeException.getResponseBodyAsByteArray();
      if (error == null || ((byte[]) error).length == 0) {
        error = new DefaultErrorDto(httpStatus.value(), e, request);
      }
    } else {
      httpStatus = translateException(e);
      error = new DefaultErrorDto(httpStatus.value(), e, request);
    }

    if (!(error instanceof byte[])) {
      error = om.writeValueAsBytes(error);
    }

    return createResponseEntity(error, contentType, httpStatus);
  }

  private ResponseEntity<?> createResponseEntity(
      final Object error,
      final MediaType contentType,
      final HttpStatus httpStatus
  ) throws JsonProcessingException {
    HttpHeaders headers = new HttpHeaders();
    headers.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(contentType.toString()));
    return new ResponseEntity<>(error, headers, httpStatus);
  }

  private HttpStatus translateException(final Throwable ex) {
    HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    if (ex instanceof HttpRequestMethodNotSupportedException) {
      httpStatus = HttpStatus.METHOD_NOT_ALLOWED;
    } else if (ex instanceof HttpMediaTypeNotSupportedException) {
      httpStatus = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
    } else if (ex instanceof HttpMediaTypeNotAcceptableException) {
      httpStatus = HttpStatus.NOT_ACCEPTABLE;
    } else if (ex instanceof NoHandlerFoundException) {
      httpStatus = HttpStatus.NOT_FOUND;
    } else if (ex instanceof AsyncRequestTimeoutException) {
      httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
    } else {
      httpStatus = translateBadRequestExceptions(ex);
    }
    return httpStatus;
  }

  @NotNull
  private HttpStatus translateBadRequestExceptions(final Throwable ex) {
    HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    if (ex instanceof MissingServletRequestParameterException
        || ex instanceof ServletRequestBindingException
        || ex instanceof TypeMismatchException
        || ex instanceof HttpMessageNotReadableException
        || ex instanceof MethodArgumentNotValidException
        || ex instanceof MissingServletRequestPartException
        || ex instanceof BindException
        ) {
      httpStatus = HttpStatus.BAD_REQUEST;
    } else {
      if (ex != null) {
        ResponseStatus annotation = ex.getClass().getAnnotation(ResponseStatus.class);
        if (annotation != null) {
          httpStatus = annotation.value();
        }
      }
    }
    return httpStatus;
  }

  private Throwable extractException(final Exception ex) {
    Throwable e = ex;

    while (true) {
      if (e.getCause() == null
          || e.getCause() == e
          || !(e instanceof ExecutionException
          || e instanceof CompletionException
          || !(e instanceof RestClientException
          || !(e instanceof NestedRuntimeException)))) {
        break;
      }

      e = e.getCause();
    }

    return e;
  }
}

