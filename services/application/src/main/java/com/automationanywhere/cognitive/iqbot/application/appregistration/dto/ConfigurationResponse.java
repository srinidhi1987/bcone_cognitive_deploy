package com.automationanywhere.cognitive.iqbot.application.appregistration.dto;

public class ConfigurationResponse {
  public final String controlRoomUrl;
  public final String routingName;

  public ConfigurationResponse(final String controlRoomUrl, final String routingName) {
    this.controlRoomUrl = controlRoomUrl;
    this.routingName = routingName;
  }
}
