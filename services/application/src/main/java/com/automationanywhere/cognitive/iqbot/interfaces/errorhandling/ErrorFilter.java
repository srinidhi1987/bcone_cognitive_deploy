package com.automationanywhere.cognitive.iqbot.interfaces.errorhandling;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(-100) // This should the very first filter
public class ErrorFilter implements Filter {
  private static final Logger LOGGER = LogManager.getLogger(ErrorFilter.class);

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void destroy() {

  }

  @Override
  public void doFilter(
      final ServletRequest request, final ServletResponse response, final FilterChain chain
  ) throws IOException, ServletException {
    try {
      chain.doFilter(request, response);
    } catch (final Exception ex) {
      LOGGER.error("Unhandled error", ex);
      throw ex;
    }
  }
}
