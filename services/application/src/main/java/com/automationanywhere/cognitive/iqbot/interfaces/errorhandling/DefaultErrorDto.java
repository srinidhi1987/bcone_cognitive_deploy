package com.automationanywhere.cognitive.iqbot.interfaces.errorhandling;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * DTO class to keep the error data.
 */
public class DefaultErrorDto {

  public final long timestamp;
  public final int status;
  public final String error;
  public final String exception;
  public final String message;
  public final String path;

  public DefaultErrorDto(
      @JsonProperty("timestamp") final long timestamp,
      @JsonProperty("status") final int status,
      @JsonProperty("error") final String error,
      @JsonProperty("exception") final String exception,
      @JsonProperty("message") final String message,
      @JsonProperty("path") final String path
  ) {
    this.timestamp = timestamp;
    this.status = status;
    this.error = error;
    this.exception = exception;
    this.message = message;
    this.path = path;
  }

  public DefaultErrorDto(final int status, final Throwable th, final ServletWebRequest request) {
    this(th.getMessage(), status, th, request);
  }

  public DefaultErrorDto(
      final String message, final int status, final Throwable th, final ServletWebRequest request
  ) {
    this.status = status;
    this.message = message;

    timestamp = System.currentTimeMillis();
    error = HttpStatus.valueOf(status).getReasonPhrase();
    exception = th.getClass().getName();
    path = getPath(request);
  }

  private String getPath(final ServletWebRequest request) {
    String path = "";
    if (request != null) {
      HttpServletRequest req = request.getNativeRequest(HttpServletRequest.class);
      path = req.getHeader(HttpHeaders.HOST);
      path+= req.getServletPath();
      path+= Optional.ofNullable(req.getPathInfo()).orElse("");
      path+= Optional.ofNullable(req.getQueryString()).map(q -> "?" + q).orElse("");
    }
    return path;
  }
}
