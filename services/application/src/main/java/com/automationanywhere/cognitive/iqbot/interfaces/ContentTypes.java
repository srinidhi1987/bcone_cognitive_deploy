package com.automationanywhere.cognitive.iqbot.interfaces;

import org.springframework.http.MediaType;

public interface ContentTypes {
  String JSON = MediaType.APPLICATION_JSON_UTF8_VALUE;
  String JSON_V1 = "application/vnd.cognitive.v1+json;UTF-8";
}
