package com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto;

public class HandshakeDto {

  public final String appVersion;
  public final String publicKey;
  public final String routingName;

  public HandshakeDto(final String appVersion, final String publicKey, final String routingName) {
    this.appVersion = appVersion;
    this.publicKey = publicKey;
    this.routingName = routingName;
  }
}
