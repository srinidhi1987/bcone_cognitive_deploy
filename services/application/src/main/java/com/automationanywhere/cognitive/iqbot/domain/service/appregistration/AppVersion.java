package com.automationanywhere.cognitive.iqbot.domain.service.appregistration;

/**
 * Application descriptor.
 * Contains version information about the app.
 */
public class AppVersion {
  public final String applicationName;
  public final String componentVersion;
  public final String productVersion;
  public final String branch;
  public final String gitHash;
  public final String buildTime;

  public AppVersion(
      final String applicationName,
      final String componentVersion,
      final String productVersion,
      final String branch,
      final String gitHash,
      final String buildTime
  ) {
    this.applicationName = applicationName;
    this.componentVersion = componentVersion;
    this.productVersion = productVersion;
    this.branch = branch;
    this.gitHash = gitHash;
    this.buildTime = buildTime;
  }
}
