package com.automationanywhere.cognitive.iqbot.interfaces.appregistration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AcknowledgementDto {
  public final String appId;

  public AcknowledgementDto(
      @JsonProperty("appId") final String appId
  ) {
    this.appId = appId;
  }
}
