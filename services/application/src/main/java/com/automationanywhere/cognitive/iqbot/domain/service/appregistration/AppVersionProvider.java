package com.automationanywhere.cognitive.iqbot.domain.service.appregistration;

/**
 * A port that provides information about the application.
 */
public interface AppVersionProvider {

  /**
   * Returns an application descriptor.
   *
   * @return application descriptor.
   */
  AppVersion appVersion() throws AppVersionProviderException;
}
