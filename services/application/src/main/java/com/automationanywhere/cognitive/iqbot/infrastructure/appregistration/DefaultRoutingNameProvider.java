package com.automationanywhere.cognitive.iqbot.infrastructure.appregistration;

import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.appconfiguration.Configuration;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.RoutingNameProvider;
import java.util.Optional;

/**
 * The default implementation uses the app id as the routing name.
 */
public class DefaultRoutingNameProvider implements RoutingNameProvider {
  private final AppConfigurationRepository appConfigurationRepository;

  private static final String routingName = "IQBot";

  public DefaultRoutingNameProvider(final AppConfigurationRepository appConfigurationRepository) {
    this.appConfigurationRepository = appConfigurationRepository;
  }

  @Override
  public String routingName() {
    //Routing name should be fixed to IQBot
    return routingName;
  }
}
