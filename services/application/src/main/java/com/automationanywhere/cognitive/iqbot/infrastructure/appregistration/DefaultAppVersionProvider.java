package com.automationanywhere.cognitive.iqbot.infrastructure.appregistration;

import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersion;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersionProvider;
import com.automationanywhere.cognitive.iqbot.domain.service.appregistration.AppVersionProviderException;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

/**
 * This implementation reads app descriptor details from a version file in the classpath.
 */
public class DefaultAppVersionProvider implements AppVersionProvider {

  private static final String BUILD_VERSION_FILE_PATH = "build.version";
  private static final String APPLICATION_NAME_PROP_NAME = "Application-name";
  private static final String COMPONENT_VERSION_PROP_NAME = "Version";
  private static final String PRODUCT_VERSION_PROP_NAME = "Product-Version";
  private static final String BRANCH_PROP_NAME = "Branch";
  private static final String BUILD_TIME_PROP_NAME = "Buildtime";
  private static final String GIT_HASH_PROP_NAME = "GitHash";

  @Override
  public AppVersion appVersion() {
    AppVersion appVersion = null;

    try {
      try (
          InputStream is = getClass().getClassLoader().getResourceAsStream(BUILD_VERSION_FILE_PATH);
      ) {
        if (is == null) {
          throw new FileNotFoundException(BUILD_VERSION_FILE_PATH + " not found");
        }
        Properties prop = new Properties();
        prop.load(is);

        appVersion = new AppVersion(
            Optional.ofNullable(prop.getProperty(APPLICATION_NAME_PROP_NAME))
                .orElseThrow(() -> createPropertyNotFoundException(APPLICATION_NAME_PROP_NAME)),
            Optional.ofNullable(prop.getProperty(COMPONENT_VERSION_PROP_NAME))
                .orElseThrow(() -> createPropertyNotFoundException(COMPONENT_VERSION_PROP_NAME)),
            Optional.ofNullable(prop.getProperty(PRODUCT_VERSION_PROP_NAME))
                .orElseThrow(() -> createPropertyNotFoundException(PRODUCT_VERSION_PROP_NAME)),
            Optional.ofNullable(prop.getProperty(BRANCH_PROP_NAME))
                .orElseThrow(() -> createPropertyNotFoundException(BRANCH_PROP_NAME)),
            Optional.ofNullable(prop.getProperty(GIT_HASH_PROP_NAME))
                .orElseThrow(() -> createPropertyNotFoundException(GIT_HASH_PROP_NAME)),
            Optional.ofNullable(prop.getProperty(BUILD_TIME_PROP_NAME))
                .orElseThrow(() -> createPropertyNotFoundException(BUILD_TIME_PROP_NAME))
        );
      }
    } catch (final Exception ex) {
      throw new AppVersionProviderException(ex);
    }

    return appVersion;
  }

  private Exception createPropertyNotFoundException(final String propName) {
    return new AppVersionProviderException("Property " + propName + " not found");
  }
}
