package com.automationanywhere.cognitive.iqbot.application.appregistration.dto;

public class FinalizeRegistrationRequest {
  public final String appId;

  public FinalizeRegistrationRequest(final String appId) {
    this.appId = appId;
  }
}
