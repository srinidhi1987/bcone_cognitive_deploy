package com.automationanywhere.cognitive.iqbot.application.appregistration;

/**
 * Thrown if the app initialization fails.
 */
public class AppInitializationException extends RuntimeException {

  private static final long serialVersionUID = 7470185086074297137L;

  public AppInitializationException(final String message) {
    super(message);
  }

  public AppInitializationException(final Exception ex) {
    super(ex);
  }
}
