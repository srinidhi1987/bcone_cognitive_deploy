package com.automationanywhere.cognitive.iqbot.config;

import com.automationanywhere.cognitive.common.config.CognitivePropertyPlaceholderConfigurer;
import com.automationanywhere.cognitive.common.domain.service.appconfiguration.AppConfigurationRepository;
import com.automationanywhere.cognitive.common.domain.service.security.KeyManager;
import com.automationanywhere.cognitive.common.domain.service.security.keymanagement.KeyName;
import com.automationanywhere.cognitive.common.infrastructure.appconfiguration.DefaultAppConfigurationRepository;
import com.automationanywhere.cognitive.common.infrastructure.security.ControlRoomKeyProvider;
import com.automationanywhere.cognitive.common.infrastructure.security.database.DatabaseKeyProvider;
import com.automationanywhere.cognitive.common.infrastructure.security.database.dto.SecurityKeyRepository;
import com.automationanywhere.cognitive.common.infrastructure.security.database.persistence.HibernateSecurityKeyRepository;
import com.automationanywhere.cognitive.common.security.DefaultCipherService;
import com.automationanywhere.cognitive.common.security.StreamKeyProvider;
import com.automationanywhere.cognitive.common.util.DataTransformer;
import com.automationanywhere.cognitive.iqbot.application.appregistration.AppRegistration;
import com.automationanywhere.cognitive.iqbot.infrastructure.appregistration.DefaultAppVersionProvider;
import com.automationanywhere.cognitive.iqbot.infrastructure.appregistration.DefaultRoutingNameProvider;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class ApplicationConfiguration {

  private static final String CONFIGURATIONS_TRANSACTION_MANAGER_NAME = "configurationsTransactionManager";
  private static final String WORK_DIR_SYS_PROP_NAME = "user.dir";
  private static final String ALG_NAME = "AES";
  private static final String UP_FOLDER_NAME = "..";
  private static final String CONFIGURATIONS_PATH = "configurations";
  private static final String PRIVATE_KEY_FILE_NAME = "private.key";
  private static final String PROPERTIES_FILE_NAME = "cognitive.properties";
  private static final String SETTINGS_FILE_NAME = "Settings.txt";
  private static final String RABBITMQ_PASSWORD_PROP_NAME = "rabbitmq.password";
  private static final String DATABASE_PASSWORD_PROP_NAME = "database.password";
  private static final int DATASOURCE_INITIAL_SIZE = 3;

  private static final FileSystem FILE_SYSTEM = FileSystems.getDefault();

  @Bean
  public static PropertyPlaceholderConfigurer createPropertyPlaceholderConfigurer()
      throws Exception {
    String path = FILE_SYSTEM.getPath(
        System.getProperty(WORK_DIR_SYS_PROP_NAME),
        UP_FOLDER_NAME, CONFIGURATIONS_PATH
    ).toFile().getCanonicalPath();

    InputStream is = new FileInputStream(FILE_SYSTEM.getPath(path, PRIVATE_KEY_FILE_NAME).toFile());
    DefaultCipherService cipherService = new DefaultCipherService(
        new StreamKeyProvider(ALG_NAME, is));

    CognitivePropertyPlaceholderConfigurer bean = new CognitivePropertyPlaceholderConfigurer();
    bean.setIgnoreResourceNotFound(true);
    bean.setCipherService(cipherService);
    bean.setDataTransformer(new DataTransformer());
    bean.setLocations(
        new FileSystemResource(FILE_SYSTEM.getPath(path, PROPERTIES_FILE_NAME).toFile()),
        new FileSystemResource(FILE_SYSTEM.getPath(path, SETTINGS_FILE_NAME).toFile())
    );
    bean.setEncryptedKeys(new HashSet<>(Arrays.asList(
        RABBITMQ_PASSWORD_PROP_NAME, DATABASE_PASSWORD_PROP_NAME
    )));
    return bean;
  }

  @Bean
  public AppConfigurationRepository createAppConfigurationRepository(
      @Autowired final LocalSessionFactoryBean localSessionFactoryBean
  ) {
    return new DefaultAppConfigurationRepository(localSessionFactoryBean.getObject());
  }

  @Bean
  public KeyManager createKeyManager(
      @Autowired final LocalSessionFactoryBean localSessionFactoryBean
  ) {
    SecurityKeyRepository securityKeyRepository = new HibernateSecurityKeyRepository(
        localSessionFactoryBean.getObject()
    );
    DatabaseKeyProvider dbKeyProvider = new DatabaseKeyProvider(
        securityKeyRepository
    );
    return new KeyManager(dbKeyProvider);
  }

  @Bean
  public AppRegistration createAppRegistration(
      @Autowired final AppConfigurationRepository appConfigurationRepository,
      @Autowired final KeyManager dbKeyManager
  ) {
    ControlRoomKeyProvider crKeyProvider = new ControlRoomKeyProvider(KeyName.SYSTEM_KEY.name());

    return new AppRegistration(
        dbKeyManager, new DefaultAppVersionProvider(),
        new DefaultRoutingNameProvider(appConfigurationRepository),
        appConfigurationRepository, crKeyProvider
    );
  }

  @Bean
  public LocalSessionFactoryBean createSession(
      @Value("${iqbot.databases.configurations.name}") final String name,
      @Value("${iqbot.databases.configurations.userName}") final String userName,
      @Value("${iqbot.databases.configurations.host}") final String host,
      @Value("${iqbot.databases.configurations.port}") final int port,
      @Value("${SQLWindowsAuthentication:#{false}}") final boolean windowsAuth,
      @Value("${database.password}") final String password
  ) {
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    dataSource.setInitialSize(DATASOURCE_INITIAL_SIZE);
    String jdbcUrl = "jdbc:sqlserver://" + host + ":" + port;
    jdbcUrl += ";databaseName=" + name;
    jdbcUrl += ";sendStringParametersAsUnicode=false";
    if (windowsAuth) {
      jdbcUrl += ";integratedSecurity=true";
    } else {
      dataSource.setUsername(userName);
      dataSource.setPassword(password);
    }
    dataSource.setUrl(jdbcUrl);

    Properties props = new Properties();
    props.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
    props.setProperty("hibernate.default_schema", "dbo");
    props.setProperty("hibernate.show_sql", "false");

    LocalSessionFactoryBean bean = new LocalSessionFactoryBean();
    bean.setDataSource(dataSource);
    bean.setHibernateProperties(props);
    bean.setPackagesToScan("com.automationanywhere.cognitive.common");
    return bean;
  }

  @Bean(CONFIGURATIONS_TRANSACTION_MANAGER_NAME)
  public PlatformTransactionManager createPlatformTransactionManager(
      @Autowired final LocalSessionFactoryBean localSessionFactoryBean
  ) {
    return new HibernateTransactionManager(localSessionFactoryBean.getObject());
  }
}
