package com.automationanywhere.cognitive.iqbot.application.appregistration;

/**
 * Thrown if the app registration fails.
 */
public class AppRegistrationException extends RuntimeException {

  private static final long serialVersionUID = -8628736657524873882L;

  public AppRegistrationException(final String message) {
    super(message);
  }

  public AppRegistrationException(final Exception ex) {
    super(ex);
  }
}
