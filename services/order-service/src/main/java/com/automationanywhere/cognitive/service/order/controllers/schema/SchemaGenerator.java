package com.automationanywhere.cognitive.service.order.controllers.schema;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator;

import java.io.StringWriter;

/**
 * Generate the JSON schema.
 */
public final class SchemaGenerator {

    /**
     * Utility class should not have a public constructor.
     */
    private SchemaGenerator() {
    }

    /**
     * Generates the JSON schema from class object.
     *
     * @param dtoClass the class of the object.
     * @return the JSON schema of the object.
     */
    @SuppressWarnings("Duplicates")
    public static String generateSchemaOf(Class dtoClass) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonSchemaGenerator generator = new JsonSchemaGenerator(mapper);
            JsonSchema jsonSchema = generator.generateSchema(dtoClass);

            StringWriter json = new StringWriter();
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            mapper.writeValue(json, jsonSchema);

            return json.toString();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
