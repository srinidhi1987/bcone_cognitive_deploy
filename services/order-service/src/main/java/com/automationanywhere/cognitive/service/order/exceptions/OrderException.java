package com.automationanywhere.cognitive.service.order.exceptions;

public class OrderException extends RuntimeException{

  public OrderException(String message, Throwable cause) {
    super(message, cause);
  }
}
