package com.automationanywhere.cognitive.service.order.config;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Sets up the configuration for Swagger.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Creates the instance of Docket to sets up the Swagger configuration.
     *
     * @return the Docket instance
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("com.automationanywhere.cognitive.service.order.controllers.rest"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Lists.newArrayList(
                        new ApiKey("Authorization", "Authorization", "header")))
                .tags(new Tag("Order Microservice", "Order microservice endpoint"));
    }
}
