package com.automationanywhere.cognitive.service.order.controllers.rest.v1;

import com.automationanywhere.cognitive.service.order.controllers.dto.v1.OrderDTO;
import com.automationanywhere.cognitive.service.order.controllers.mapper.v1.OrderMapper;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.services.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.automationanywhere.cognitive.service.order.controllers.schema.SchemaGenerator.generateSchemaOf;

/**
 * REST API endpoint for order resource.
 */
@Deprecated
@RestController("OrderResourceV1")
public class OrderResource {

    private static final Logger LOGGER = LogManager.getLogger(OrderResource.class);

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderResource(OrderService orderService, @Qualifier("OrderMapperV1") OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    /**
     * List all orders for a specific customer.
     *
     * @param customerId the customer ID in UUID format
     * @return the list of all customer's order
     */
    @RequestMapping(value = "/api/orders", method = RequestMethod.GET,
            headers = {"accept=application/vnd.cognitive.v1+json"},
            consumes = {"application/vnd.cognitive.v1+json"}
    )
    @Deprecated
    public CompletableFuture<List<OrderDTO>> listOrdersByCustomer(@RequestParam String customerId) {
        LOGGER.trace("List orders by customer V1");
        CompletableFuture<List<Order>> future = orderService.listOrdersByCustomer(new AAId(customerId));
        return future.thenApply(orderMapper::getDto);
    }

    @RequestMapping(value = "/api/orders/schema", method = RequestMethod.GET,
            produces = {"application/vnd.cognitive.v1+json"},
            consumes = {"application/vnd.cognitive.v1+json"}
    )
    public String getOrderSchema() {
        return generateSchemaOf(OrderDTO.class);
    }
}
