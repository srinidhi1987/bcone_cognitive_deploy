package com.automationanywhere.cognitive.service.order.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Sets up the configuration for all repositories.
 */
@Configuration
@ComponentScan(basePackages = "com.automationanywhere.cognitive.service.order.repositories")
public class RepositoryConfig {
}

