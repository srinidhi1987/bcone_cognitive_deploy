package com.automationanywhere.cognitive.service.order.controllers.mapper.v1;

import com.automationanywhere.cognitive.service.order.controllers.dto.v1.OrderDTO;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Responsible for convert the Order entity in DTO.
 */
@Component("OrderMapperV1")
public class OrderMapper {

    /**
     * Converts the Order entity and his products to OrderDTO.
     *
     * @param order the order entity
     * @return the order DTO
     */
    public OrderDTO getDto(Order order) {
        OrderDTO dto = new OrderDTO();
        dto.setOrderId(order.getId().getValue());
        dto.setCustomerId(order.getCustomerId().getValue());
        dto.setProducts(order.getProducts().stream().collect(Collectors.toMap(Product::getName, Product::getQuantity)));
        return dto;
    }

    /**
     * Converts the list of order entities to a list of order DTOs.
     *
     * @param orders list of order entities to be converted
     * @return list of order DTOs converted
     */
    public List<OrderDTO> getDto(List<Order> orders) {
        return orders.stream().map(this::getDto).collect(Collectors.toList());
    }
}
