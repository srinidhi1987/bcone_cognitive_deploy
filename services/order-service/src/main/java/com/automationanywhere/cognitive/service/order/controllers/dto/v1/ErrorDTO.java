package com.automationanywhere.cognitive.service.order.controllers.dto.v1;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * DTO class to keep the error data.
 */
public class ErrorDTO {

    public static final int STACK_TRACE_LENGTH = 2000;

    private int code;
    private String message;
    private String stackTrace;

    /**
     * Default constructor.
     */
    public ErrorDTO() {
    }

    /**
     * Constructs the error DTO instance with error code and exception.
     *
     * @param code the error code
     * @param e    the exception instance
     */
    public ErrorDTO(int code, Throwable e) {
        this.code = code;
        this.message = e.getMessage();

        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        setStackTrace(sw.toString());
    }

    /**
     * Returns the error code.
     *
     * @return error code
     */
    public int getCode() {
        return code;
    }

    /**
     * Sets the error code.
     *
     * @param code the error code to be set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Returns the error message.
     *
     * @return error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * sets the error message.
     *
     * @param message the error message to be set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Returns a string converted from exception stack trace. If the stack trace is greater then
     * <code>STACK_TRACE_LENGTH</code> constant, the result string will be sliced.
     *
     * @return the stack trace
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Sets the string converted from stack trace. If the stack trace is greater then <code>STACK_TRACE_LENGTH</code>
     * constant, the result string will be sliced.
     *
     * @param stackTrace the stack trace
     */
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
        if (stackTrace != null && this.stackTrace.length() > STACK_TRACE_LENGTH) {
            this.stackTrace = this.stackTrace.substring(0, STACK_TRACE_LENGTH);
        }
    }
}
