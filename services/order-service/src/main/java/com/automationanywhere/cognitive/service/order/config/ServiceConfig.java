package com.automationanywhere.cognitive.service.order.config;

import com.automationanywhere.cognitive.service.order.components.factories.ThreadPoolFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Sets up the configuration for all services and components.
 */
@Configuration
@ComponentScan(basePackages = {
        "com.automationanywhere.cognitive.service.order.services",
        "com.automationanywhere.cognitive.service.order.components"
})
public class ServiceConfig {

    private static final long DEFAULT_CORE_POOL_SIZE = 2;
    private static final long DEFAULT_MAX_POOL_SIZE = 10;
    private static final long DEFAULT_KEEP_ALIVE_TIMEOUT = 5000;
    private static final long DEFAULT_QUEUE_CAPACITY = 500;
    private static final String DEFAULT_QUEUE_NAME = "SERVICE-ASYNC-EXEC-";

    private final ApplicationConfiguration appConfig;
    private final ThreadPoolFactory threadPoolFactory;

    /**
     * Default constructor.
     *
     * @param threadPoolFactory the <code>ThreadPoolFactory</code> instance.
     */
    @Autowired
    public ServiceConfig(ApplicationConfiguration appConfig, ThreadPoolFactory threadPoolFactory) {
        this.appConfig = appConfig;
        this.threadPoolFactory = threadPoolFactory;
    }

    /**
     * Returns the thread pool executor responsible for all executions of methods annotated with @Async annotation.
     *
     * @return the thread pool executor
     */
    @Bean
    public Executor asyncExecutor() {
        return threadPoolFactory.create(
                (int) this.appConfig.serviceExecutorCorePoolSize(DEFAULT_CORE_POOL_SIZE),
                (int) this.appConfig.serviceExecutorMaxPoolSize(DEFAULT_MAX_POOL_SIZE),
                (int) this.appConfig.serviceExecutorKeepAliveTimeout(DEFAULT_KEEP_ALIVE_TIMEOUT),
                (int) this.appConfig.serviceExecutorQueueCapacity(DEFAULT_QUEUE_CAPACITY),
                this.appConfig.serviceExecutorQueueName(DEFAULT_QUEUE_NAME)
        );
    }
}

