package com.automationanywhere.cognitive.service.order.repositories;

import java.util.UUID;

import com.automationanywhere.cognitive.service.order.models.AAId;

/**
 * Dummy data to be used by test and initialize the repositories.
 */
public final class DummyData {

    public static final AAId CUSTOMER_ID = new AAId(UUID.fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb"));
    public static final AAId ORDER_ID_1 = new AAId(UUID.fromString("b0c60f97-eeaa-46f7-8ef8-2d34fd2c244f"));
    public static final AAId ORDER_ID_2 = new AAId(UUID.fromString("a45d3dc3-46ee-4061-a3b5-11b30c85476a"));
    public static final AAId ORDER_ID_3 = new AAId(UUID.fromString("77748de3-0d41-4b5c-afaf-c024127444cf"));
    public static final AAId PRODUCT_ID_1 = new AAId(UUID.fromString("f6d7dc77-53be-4443-ab13-ce6a48a72cf8"));
    public static final AAId PRODUCT_ID_2 = new AAId(UUID.fromString("7ef17bf6-faa4-4903-b7bf-635ed0ebd7fb"));
    public static final AAId PRODUCT_ID_3 = new AAId(UUID.fromString("2ee17942-6dfb-4ff6-a742-824d5cd35111"));
    public static final AAId PRODUCT_ID_4 = new AAId(UUID.fromString("6b88700f-75e9-450e-a6e0-dcd8d9619a18"));
    public static final AAId PRODUCT_ID_5 = new AAId(UUID.fromString("728a5225-f9e2-4def-8b63-8a19da0b7455"));
    public static final AAId PRODUCT_ID_6 = new AAId(UUID.fromString("be38ba0e-6015-432a-a936-005e78ef038f"));

    private DummyData() {
    }
}
