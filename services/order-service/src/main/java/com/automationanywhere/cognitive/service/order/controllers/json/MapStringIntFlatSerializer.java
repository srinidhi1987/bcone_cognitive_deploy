package com.automationanywhere.cognitive.service.order.controllers.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Map;

/**
 * Serialize flat Map with String as key and Integer as value to JSON.
 */
public class MapStringIntFlatSerializer extends JsonSerializer<Map<String, Integer>> {

    @Override
    public void serialize(Map<String, Integer> value, JsonGenerator gen,
                          SerializerProvider serializers) throws IOException {

        gen.writeStartArray();
        for (Map.Entry<String, Integer> entry : value.entrySet()) {
            gen.writeStartObject();
            gen.writeNumberField(entry.getKey(), entry.getValue());
            gen.writeEndObject();
        }
        gen.writeEndArray();
    }
}
