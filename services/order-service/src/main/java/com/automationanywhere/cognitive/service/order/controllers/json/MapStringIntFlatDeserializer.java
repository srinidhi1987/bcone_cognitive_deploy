package com.automationanywhere.cognitive.service.order.controllers.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Deserialize flat Map with String as key and Integer as value form JSON.
 */
public class MapStringIntFlatDeserializer extends JsonDeserializer<Map<String, Integer>> {

    @Override
    public Map<String, Integer> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        if (jp.getCurrentToken() != JsonToken.START_ARRAY) {
            throw new JsonParseException(jp, "Start array expected");
        }

        HashMap<String, Integer> map = new HashMap<>();
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            if (jp.currentToken() == JsonToken.START_OBJECT) {
                map.put(jp.nextFieldName(), jp.nextIntValue(0));
            }
            if (jp.nextToken() != JsonToken.END_OBJECT) {
                throw new JsonParseException(jp, "End object expected");
            }
        }

        return map;
    }
}
