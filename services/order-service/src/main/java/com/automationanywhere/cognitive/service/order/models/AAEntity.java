package com.automationanywhere.cognitive.service.order.models;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;

/**
 * Base entity, this class should be inherited for all entities implementation.
 */
@MappedSuperclass
public abstract class AAEntity implements Serializable{

    @EmbeddedId
    private AAId id;

    /**
     * Default constructor.
     */
    public AAEntity() {
    }

    /**
     * Constructs with an ID of the entity.
     *
     * @param id the ID
     */
    public AAEntity(AAId id) {
        this.id = id;
    }

    /**
     * Returns the entity ID.
     *
     * @return the entity ID
     */
    public AAId getId() {
        return id;
    }

    /**
     * Sets the entity ID.
     *
     * @param id the entity ID to be set
     */
    public void setId(AAId id) {
        this.id = id;
    }
}
