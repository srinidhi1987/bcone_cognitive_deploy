package com.automationanywhere.cognitive.service.order;

import com.automationanywhere.cognitive.common.configuration.FileConfigurationProvider;
import com.automationanywhere.cognitive.common.configuration.FileConfigurationProviderFactory;
import com.automationanywhere.cognitive.service.order.config.ApplicationConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Order Microservice SpringBoot configuration.
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.automationanywhere.cognitive.service.order.config")
public class SpringBootConfiguration {

    private ApplicationConfiguration applicationConfiguration;

    /**
     * Default constructor.
     */
    public SpringBootConfiguration() {
        FileConfigurationProviderFactory configurationProviderFactory = new FileConfigurationProviderFactory();
        FileConfigurationProvider configurationProvider = configurationProviderFactory.create();

        this.applicationConfiguration = new ApplicationConfiguration(configurationProvider);
    }

    @Bean
    public ApplicationConfiguration applicationConfiguration() {
        return this.applicationConfiguration;
    }
}
