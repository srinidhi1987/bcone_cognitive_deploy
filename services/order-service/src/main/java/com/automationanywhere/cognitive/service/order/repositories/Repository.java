package com.automationanywhere.cognitive.service.order.repositories;

import java.util.List;

import com.automationanywhere.cognitive.service.order.models.AAEntity;
import com.automationanywhere.cognitive.service.order.models.AAId;

/**
 * Base interface for all CRUD repositories.
 *
 * @param <E> the entity class handled by this repository
 */
public interface Repository<E extends AAEntity> {

    /**
     * List all entities.
     *
     * @return all entities
     */
    List<E> listAll();

    /**
     * Returns the entity by yours ID.
     *
     * @param id the ID of the entity
     * @return the entity
     */
    E get(AAId id);

    /**
     * Stores a new entity. An ID will be set to the entity.
     *
     * @param order the order to be stored
     */
    void create(E order);

    /**
     * Updates a entity, if the entity has no ID a new one will be set.
     *
     * @param order the order to be updated
     */
    void update(E order);

    /**
     * Deletes an entity by yours ID.
     *
     * @param id the ID of the entity
     * @return the entity removed
     */
    E delete(AAId id);
}
