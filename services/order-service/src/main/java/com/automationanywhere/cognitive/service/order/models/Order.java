package com.automationanywhere.cognitive.service.order.models;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The order representation.
 */
@Table(name = "Order1")
@Entity
public class Order extends AAEntity {

  @AttributeOverrides({@AttributeOverride(name = "value", column = @Column(name = "customer_id"))})
  private AAId customerId;
  @Column(name = "date_created")
  private Date createdAt;
  @OneToMany(mappedBy = "order", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
  private List<Product> products;

  /**
   * Default constructor.
   */
  public Order() {
  }

  /**
   * Full constructor.
   *
   * @param id the order ID
   * @param customerId the customer ID
   * @param createdAt Creation date
   */
  public Order(AAId id, AAId customerId, Date createdAt) {
    super(id);
    this.customerId = customerId;
    this.createdAt = createdAt;
  }

  /**
   * Returns the customer ID.
   *
   * @return the customer ID
   */
  public AAId getCustomerId() {
    return customerId;
  }

  /**
   * Sets the customer Id.
   *
   * @param customerId the customer ID to be set
   */
  public void setCustomerId(AAId customerId) {
    this.customerId = customerId;
  }

  /**
   * Returns the createdAt.
   *
   * @return the value of createdAt
   */
  public Date getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the createdAt.
   *
   * @param createdAt the createdAt to be set
   */
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Returns the products.
   *
   * @return the value of products
   */
  public List<Product> getProducts() {
    return products;
  }

  /**
   * Sets the products.
   *
   * @param products the products to be set
   */
  public void setProducts(List<Product> products) {
    this.products = products;
  }
}
