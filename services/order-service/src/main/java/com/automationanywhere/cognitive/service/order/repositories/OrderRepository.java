package com.automationanywhere.cognitive.service.order.repositories;

import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;

import java.util.List;

/**
 * Keeps the data of orders.
 */
public interface OrderRepository extends Repository<Order> {

    /**
     * Search all orders of a specific customer.
     *
     * @param customerId the customer ID
     * @return a list of all customer's order
     */
    List<Order> searchOrdersByCustomer(AAId customerId);
}
