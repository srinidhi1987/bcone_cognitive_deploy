package com.automationanywhere.cognitive.service.order.controllers.rest.v2;

import com.automationanywhere.cognitive.service.order.controllers.dto.v2.OrderDTO;
import com.automationanywhere.cognitive.service.order.controllers.mapper.v2.OrderMapper;
import com.automationanywhere.cognitive.service.order.exceptions.InvalidIdException;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.services.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.automationanywhere.cognitive.service.order.controllers.schema.SchemaGenerator.generateSchemaOf;

/**
 * REST API endpoint for order resource.
 */
@RestController("OrderResourceV2")
public class OrderResource {

    private static final Logger LOGGER = LogManager.getLogger(OrderResource.class);

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderResource(OrderService orderService, @Qualifier("OrderMapperV2") OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    /**
     * List all orders for a specific customer.
     *
     * @param customerId the customer ID in UUID format
     * @return the list of all customer's order
     */
    @RequestMapping(value = "/api/orders", method = RequestMethod.GET,
            produces = "application/vnd.cognitive.v2+json",
            headers = {
                    "accept=application/vnd.cognitive.v2+json",
                    "accept=application/vnd.cognitive+json",
                    "accept=application/json",
                    "accept=*/*"
            }
    )
    public CompletableFuture<List<OrderDTO>> listOrdersByCustomerV2(@RequestParam String customerId) {
        LOGGER.debug("List orders by customer V2 with ID: %s", customerId);
        AAId id;
        try {
            id = new AAId(customerId);
        } catch (Exception e) {
            throw new InvalidIdException("Invalid customer ID", e);
        }

        CompletableFuture<List<Order>> future = orderService.listOrdersByCustomer(id);
        return future.thenApply(orderMapper::getDto);
    }

    /**
     * Creates order
     *
     * @return the new order created
     */
    @RequestMapping(value = "/api/orders", method = RequestMethod.POST,
        produces = "application/vnd.cognitive.v2+json",
        headers = {
            "accept=application/vnd.cognitive.v2+json",
            "accept=application/vnd.cognitive+json",
            "accept=application/json",
            "accept=*/*"
        }
    )
    public CompletableFuture<OrderDTO> createOrder(@RequestBody OrderDTO orderDto) {
        LOGGER.debug("creating order : %s", orderDto.toString());
        CompletableFuture<Order> future = orderService.saveOrder(orderMapper.getEntityFromDto(orderDto));
        return future.thenApply(orderMapper::getDto);
    }

    @RequestMapping(value = "/api/orders/schema", method = RequestMethod.GET,
            produces = "application/vnd.cognitive.v2+json",
            consumes = {
                    "application/vnd.cognitive.v2+json",
                    "application/vnd.cognitive+json",
                    "application/json"
            }
    )
    public String getOrderV2Schema() {
        return generateSchemaOf(OrderDTO.class);
    }

    /**
     * List the order by order Id
     *
     * @return Order with the order Id
     */

    @RequestMapping(value = "/api/orders/orderId", method = RequestMethod.GET,
        produces = "application/vnd.cognitive.v2+json",
        headers = {
            "accept=application/vnd.cognitive.v2+json",
            "accept=application/vnd.cognitive+json",
            "accept=application/json",
            "accept=*/*"
        }
    )
    public CompletableFuture<OrderDTO> listAllOrders(@RequestParam String orderId) {
        LOGGER.debug("List orders by Id : %s", orderId);
        AAId id;
        try {
            id = new AAId(orderId);
        } catch (Exception e) {
            throw new InvalidIdException("Invalid order ID", e);
        }
        CompletableFuture<Order> future = orderService.listOrders(id);
        return future.thenApply(orderMapper::getDto);
    }
}
