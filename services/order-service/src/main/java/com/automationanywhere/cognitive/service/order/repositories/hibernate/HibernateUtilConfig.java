package com.automationanywhere.cognitive.service.order.repositories.hibernate;

import javax.persistence.EntityManagerFactory;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HibernateUtilConfig {

  @Autowired
  private final EntityManagerFactory entityManagerFactory = null;

  @Bean
  public SessionFactory getSessionFactory() {
    return entityManagerFactory.unwrap(SessionFactory.class);
  }
}
