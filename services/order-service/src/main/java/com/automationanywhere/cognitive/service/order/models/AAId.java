package com.automationanywhere.cognitive.service.order.models;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

/**
 * Represents the identification of the entity.
 */
@Embeddable
public class AAId implements Serializable{

  @Column(name = "Id", nullable = false)
  @GeneratedValue(generator = "UUID")
  @Type(type = "uuid-char")
  @GenericGenerator(
      name = "UUID",
      strategy = "org.hibernate.id.UUIDGenerator"
  )
  private UUID value;

  public AAId() {
    super();
  }

  /**
   * Constructs with a UUID instance.
   *
   * @param value the UUID instance
   */
  public AAId(UUID value) {
    this.value = value;
  }

  /**
   * Constructs with a UUID instance.
   *
   * @param value the UUID instance
   */
  public AAId(String value) {
    this.value = UUID.fromString(value);
  }


  /**
   * Returns the UUID value.
   *
   * @return the UUID value
   */
  public UUID get() {
    return value;
  }

  /**
   * Returns the string representation of the UUID instance.
   *
   * @return the string representation of the UUID
   */
  public String getValue() {
    return value == null ? null : value.toString();
  }

  public void setValue(UUID value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    AAId id = (AAId) o;

    return value != null ? value.equals(id.value) : id.value == null;
  }

  @Override
  public int hashCode() {
    return value != null ? value.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Id{"
        + "value=" + value
        + '}';
  }
}
