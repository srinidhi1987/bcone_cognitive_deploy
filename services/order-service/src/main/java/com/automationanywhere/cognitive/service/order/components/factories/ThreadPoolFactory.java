package com.automationanywhere.cognitive.service.order.components.factories;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;


/**
 * Factory to create thread pools.
 */
@Component
@SuppressWarnings("Duplicates")
public class ThreadPoolFactory {

    /**
     * Creates a new thread pool and initialize it.
     *
     * @param corePoolSize     the core pool size
     * @param maxPoolSize      the max pool size
     * @param keepAliveTimeout the keep alive timeout
     * @param queueCapacity    the queue capacity
     * @param queueName        the queue name
     * @return the new thread pool created.
     */
    public ThreadPoolTaskExecutor create(int corePoolSize, int maxPoolSize, int keepAliveTimeout,
                                         int queueCapacity, String queueName) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setTaskDecorator(new LogContextPropagatingDecorator());
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setKeepAliveSeconds(keepAliveTimeout);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(queueName);
        executor.initialize();
        return executor;
    }
}

