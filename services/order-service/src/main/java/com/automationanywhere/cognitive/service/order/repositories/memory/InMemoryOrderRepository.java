package com.automationanywhere.cognitive.service.order.repositories.memory;

import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;
import com.automationanywhere.cognitive.service.order.repositories.OrderRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import static com.automationanywhere.cognitive.service.order.repositories.DummyData.*;

/**
 * Order repository implementation.
 */
//@Repository
public class InMemoryOrderRepository extends InMemoryRepository<Order> implements OrderRepository {

    public static final int PROD_QTY_1 = 1;
    public static final int PROD_QTY_2 = 2;
    public static final int PROD_QTY_3 = 3;
    public static final int PROD_QTY_4 = 4;
    public static final int PROD_QTY_5 = 5;
    public static final int PROD_QTY_6 = 6;

    /**
     * Default constructor to initialize the dummy data.
     */
    public InMemoryOrderRepository() {
        ConcurrentMap<AAId, Order> recordsById = getRecordsById();
        Order order = new Order(ORDER_ID_1, CUSTOMER_ID, new Date());
        recordsById.put(ORDER_ID_1, order);
        order.setProducts(Lists.newArrayList(
                new Product(PRODUCT_ID_1, "iPhone X", PROD_QTY_1)
        ));

        order = new Order(ORDER_ID_2, CUSTOMER_ID, new Date());
        recordsById.put(ORDER_ID_2, order);
        order.setProducts(Lists.newArrayList(
                new Product(PRODUCT_ID_2, "iPhone 8 Plus", PROD_QTY_2),
                new Product(PRODUCT_ID_3, "iPhone 8", PROD_QTY_3)
        ));

        order = new Order(ORDER_ID_3, CUSTOMER_ID, new Date());
        recordsById.put(ORDER_ID_3, order);
        order.setProducts(Lists.newArrayList(
                new Product(PRODUCT_ID_4, "iPhone X", PROD_QTY_4),
                new Product(PRODUCT_ID_5, "iPhone 8 Plus", PROD_QTY_5),
                new Product(PRODUCT_ID_6, "iPhone 8", PROD_QTY_6)
        ));
    }

    @Override
    public List<Order> searchOrdersByCustomer(AAId customerId) {
        List<Order> orders = new ArrayList<>();
        for (Order order : listAll()) {
            if (order.getCustomerId().equals(customerId)) {
                orders.add(order);
            }
        }
        return orders;
    }
}
