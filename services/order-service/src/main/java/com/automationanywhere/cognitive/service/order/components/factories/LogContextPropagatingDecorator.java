package com.automationanywhere.cognitive.service.order.components.factories;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.core.task.TaskDecorator;

import java.util.HashMap;
import java.util.Map;

/**
 * Propagates log context: sets up a log context before a runnable execution
 * and restores it to the previous state after the runnable termination.
 */
public class LogContextPropagatingDecorator implements TaskDecorator {

    @Override
    public Runnable decorate(final Runnable runnable) {
        Map<String, String> context = new HashMap<>(ThreadContext.getContext());
        return () -> this.propagate(context, runnable);
    }

    void propagate(final Map<String, String> context, final Runnable runnable) {
        HashMap<String, String> oldContext = new HashMap<>(ThreadContext.getContext());
        ThreadContext.putAll(context);

        try {
            runnable.run();
        } finally {
            ThreadContext.clearMap();
            ThreadContext.putAll(oldContext);
        }
    }
}
