package com.automationanywhere.cognitive.service.order.controllers.mapper.v2;

import com.automationanywhere.cognitive.service.order.controllers.dto.v2.OrderDTO;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Responsible for convert the Order entity in DTO.
 */
@Component("OrderMapperV2")
public class OrderMapper {

    /**
     * Converts the Order entity and his products to OrderDTO.
     *
     * @param order the order entity
     * @return the order DTO
     */
    public OrderDTO getDto(Order order) {
        OrderDTO dto = new OrderDTO();
        dto.setOrderId(order.getId().getValue());
        dto.setCustomerId(order.getCustomerId().getValue());
        dto.setCreatedAt(order.getCreatedAt());
        dto.setProducts(order.getProducts().stream().collect(Collectors.toMap(Product::getName, Product::getQuantity)));
        return dto;
    }

    /**
     * Converts the list of order entities to a list of order DTOs.
     *
     * @param orders list of order entities to be converted
     * @return list of order DTOs converted
     */
    public List<OrderDTO> getDto(List<Order> orders) {
        return orders.stream().map(this::getDto).collect(Collectors.toList());
    }


    /**
     * Construct the Dao object from the Dto object to be passed to the ServiceLayer
     */
    public Order getEntityFromDto(OrderDTO orderDto) {
        Order order = new Order();
        order.setCustomerId(new AAId(orderDto.getCustomerId()));
        order.setId(new AAId(UUID.randomUUID()));
        order.setCreatedAt(orderDto.getCreatedAt());
        Map<String, Integer> products = orderDto.getProducts();
        List<Product> productList = products.entrySet().stream().map(p->new Product(new AAId(UUID.randomUUID()), p.getKey(), p.getValue(),order)).collect(Collectors.toList());
        order.setProducts(productList);
        return order;
    }


    /**
     * Converts the list of order Dtos to a list of order entities.
     *
     * @param orderDtoList list of order dto to be converted
     * @return list of order entities converted
     */
    public List<Order> getEntityFromDto(List<OrderDTO> orderDtoList) {
        return orderDtoList.stream().map(this::getEntityFromDto).collect(Collectors.toList());
    }
}
