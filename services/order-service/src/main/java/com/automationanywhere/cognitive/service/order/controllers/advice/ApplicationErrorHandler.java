package com.automationanywhere.cognitive.service.order.controllers.advice;

import com.automationanywhere.cognitive.service.order.controllers.dto.v1.ErrorDTO;
import com.automationanywhere.cognitive.service.order.exceptions.InvalidIdException;
import com.automationanywhere.cognitive.service.order.exceptions.OrderException;
import com.automationanywhere.cognitive.service.order.exceptions.OrderNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Responsible to handle all exceptions thrown by controllers.
 */
@ControllerAdvice
public class ApplicationErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationErrorHandler.class);

    /**
     * Handles a top level exception.
     *
     * @param e       the exception thrown
     * @param request the request instance
     * @return the error DTO instance with the exception information
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorDTO> handleTopLevelException(Exception e, WebRequest request) {
        LOGGER.error("Handling a top level exception", e);
        return new ResponseEntity<>(new ErrorDTO(INTERNAL_SERVER_ERROR.value(), e), INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles an exception to create ID.
     *
     * @param e       the exception thrown
     * @param request the request instance
     * @return the error DTO instance with the exception information
     */
    @ExceptionHandler({InvalidIdException.class})
    public ResponseEntity<ErrorDTO> handleInvalidIdException(Exception e, WebRequest request) {
        LOGGER.error("Handling an exception to create ID", e);
        return new ResponseEntity<>(new ErrorDTO(BAD_REQUEST.value(), e), BAD_REQUEST);
    }

    /**
     * Handles an exception while fetching order by customer ID or orderId
     *
     * @param e       the exception thrown
     * @param request the request instance
     * @return the error DTO instance with the exception information
     */
    @ExceptionHandler({OrderException.class})
    public ResponseEntity<ErrorDTO> handleOrderException(Exception e, WebRequest request) {
        LOGGER.error("Handling an exception while listing orders", e);
        return new ResponseEntity<>(new ErrorDTO(INTERNAL_SERVER_ERROR.value(), e), INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles an exception while fetching order by customer ID or orderId
     *
     * @param e       the exception thrown
     * @param request the request instance
     * @return the error DTO instance with the exception information
     */
    @ExceptionHandler({OrderNotFoundException.class})
    public ResponseEntity<ErrorDTO> handleOrderNotFoundException(Exception e, WebRequest request) {
        LOGGER.error("Handling an exception while listing orders", e);
        return new ResponseEntity<>(new ErrorDTO(NOT_FOUND.value(), e), NOT_FOUND);
    }
}
