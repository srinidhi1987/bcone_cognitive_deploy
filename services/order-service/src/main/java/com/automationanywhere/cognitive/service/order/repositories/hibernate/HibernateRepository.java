package com.automationanywhere.cognitive.service.order.repositories.hibernate;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.automationanywhere.cognitive.service.order.models.AAEntity;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.repositories.Repository;

public abstract class HibernateRepository<E extends AAEntity> implements Repository<E> {

  @Autowired
  private SessionFactory sessionFactory;

  @SuppressWarnings("unchecked")
  public HibernateRepository() {
    this.entityClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
  }

  private final Class<E> entityClass;

  @Override
  public List<E> listAll() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public E get(AAId id) {
    E entity = getCurrentSession().get(entityClass, id);
    return entity;
  }

  @Override
  public void create(E entity) {
    Session session = getCurrentSession();
    Transaction tx = null;
    try {
      tx = session.beginTransaction();
      session.save(entity);
      tx.commit();
    } catch (RuntimeException e) {
      if (tx != null) { tx.rollback(); }
      throw e;
    } finally {
      session.close();
    }
  }

  @Override
  public void update(E entity) {
    getCurrentSession().saveOrUpdate(entity);
  }

  @Override
  public E delete(AAId id) {
    // TODO Auto-generated method stub
    return null;
  }

  public void delete(E entity) {
    getCurrentSession().delete(entity);
  }

  protected final Session getCurrentSession() {
    Session session = null;
    try {
      session = sessionFactory.getCurrentSession();
    } catch (HibernateException e) {
      session = sessionFactory.openSession();
    }
    return session;
  }
}
