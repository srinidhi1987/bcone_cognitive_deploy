package com.automationanywhere.cognitive.service.order.repositories.memory;

import com.automationanywhere.cognitive.service.order.models.AAEntity;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.repositories.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Generic implementation of repository.
 *
 * @param <E> the entity class handled by this repository
 */
public abstract class InMemoryRepository<E extends AAEntity> implements Repository<E> {

    private ConcurrentMap<AAId, E> recordsById;
    private IdGenerator idGenerator;

    /**
     * Default constructor.
     */
    public InMemoryRepository() {
        idGenerator = new IdGenerator();
        recordsById = new ConcurrentHashMap<>();
    }

    @Override
    public List<E> listAll() {
        return new ArrayList<>(recordsById.values());
    }

    @Override
    public E get(AAId id) {
        return recordsById.get(id);
    }

    @Override
    public void create(E entity) {
        entity.setId(idGenerator.nextId());
        recordsById.put(entity.getId(), entity);
    }

    @Override
    public void update(E entity) {
        if (entity.getId() == null) {
            entity.setId(idGenerator.nextId());
        }
        recordsById.put(entity.getId(), entity);
    }

    @Override
    public E delete(AAId id) {
        return recordsById.remove(id);
    }

    /**
     * Returns the recordsById.
     *
     * @return the value of recordsById
     */
    ConcurrentMap<AAId, E> getRecordsById() {
        return recordsById;
    }
}
