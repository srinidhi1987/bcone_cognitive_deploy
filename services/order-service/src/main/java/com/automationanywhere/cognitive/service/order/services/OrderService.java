package com.automationanywhere.cognitive.service.order.services;

import com.automationanywhere.cognitive.service.order.exceptions.OrderException;
import com.automationanywhere.cognitive.service.order.exceptions.OrderNotFoundException;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Order service implementation.
 */
@Service
public class OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository orderRepository;

    /**
     * List all orders for a specific customer.
     *
     * @param customerId the customer ID
     * @return the list of all customer's order
     */
    @Async
    public CompletableFuture<List<Order>> listOrdersByCustomer(AAId customerId) {
      LOGGER.debug("Listing all orders by customer");
      List<Order> orders;
      try {
        orders = orderRepository.searchOrdersByCustomer(customerId);
        if (orders == null || orders.isEmpty() ) {
          throw new OrderNotFoundException(String.format("Order with customer Id not found %s", customerId));
        }
      } catch (Exception ex) {
        throw new OrderException(String
            .format("Some error occurred while fetching order for customer %s", customerId),
            ex);
      }
      return CompletableFuture.completedFuture(orders);
    }
    
    @Async
    public CompletableFuture<Order> listOrders(AAId orderId) {
      LOGGER.debug("Listing all orders");
      Order order = orderRepository.get(orderId);
      if (order == null) {
        throw new OrderNotFoundException(String.format("Order with Id not found %s", orderId));
      }
      return CompletableFuture.completedFuture(order);
    }
    
    @Async
    public CompletableFuture<Order> saveOrder(Order order) {
        LOGGER.debug("save order");
        orderRepository.create(order);
        return CompletableFuture.completedFuture(order);
    }
}
