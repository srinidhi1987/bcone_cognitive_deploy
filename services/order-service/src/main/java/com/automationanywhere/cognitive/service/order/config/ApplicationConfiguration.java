package com.automationanywhere.cognitive.service.order.config;

import com.automationanywhere.cognitive.common.configuration.ConfigurationProvider;
import com.automationanywhere.cognitive.common.configuration.exceptions.NoSuchKeyException;

public class ApplicationConfiguration {

    private static final String BASE_KEY = "aa.cognitive.micro-services.order.";

    private final ConfigurationProvider provider;

    /**
     * Default constructor.
     *
     * @param provider the <code>ConfigurationProvider</code> instance.
     */
    public ApplicationConfiguration(ConfigurationProvider provider) {
        this.provider = provider;
    }

    /**
     * Retrieves value from a specific key.
     *
     * @param key          the key of the property.
     * @param defaultValue the default value if the key was not found.
     * @param <T>          the class of the value.
     * @return the value of the property.
     */
    public <T> T getValue(String key, T defaultValue) {
        try {
            return this.provider.getValue(BASE_KEY + key);

        } catch (NoSuchKeyException e) {
            return defaultValue;
        }
    }


    public long serviceExecutorCorePoolSize(long value) {
        return getValue(BASE_KEY + "services.thread-pool.core-pool-size", value);
    }

    public long serviceExecutorMaxPoolSize(long value) {
        return getValue(BASE_KEY + "services.thread-pool.max-pool-size", value);
    }

    public long serviceExecutorKeepAliveTimeout(long value) {
        return getValue(BASE_KEY + "services.thread-pool.keep-alive-timeout", value);
    }

    public long serviceExecutorQueueCapacity(long value) {
        return getValue(BASE_KEY + "services.thread-pool.queue-capacity", value);
    }

    public String serviceExecutorQueueName(String value) {
        return getValue(BASE_KEY + "services.thread-pool.queue-name", value);
    }
}
