package com.automationanywhere.cognitive.service.order.controllers.dto.v2;

import com.automationanywhere.cognitive.service.order.controllers.json.MapStringIntFlatDeserializer;
import com.automationanywhere.cognitive.service.order.controllers.json.MapStringIntFlatSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.Map;

/**
 * DTO class to keep the order data.
 */
public class OrderDTO {

    private String orderId;
    private String customerId;
    private Date createdAt;

    @JsonSerialize(using = MapStringIntFlatSerializer.class)
    @JsonDeserialize(using = MapStringIntFlatDeserializer.class)
    private Map<String, Integer> products;

    /**
     * Returns the order ID.
     *
     * @return the order ID
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the order ID.
     *
     * @param orderId the order ID to be set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Returns the customer ID.
     *
     * @return the customer ID
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customer ID.
     *
     * @param customerId the customer ID to be set
     */

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * Returns the createdAt.
     *
     * @return the value of createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt.
     *
     * @param createdAt the createdAt to be set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Returns a map of quantity by product.
     *
     * @return the map of quantity by product
     */
    public Map<String, Integer> getProducts() {
        return products;
    }

    /**
     * Sets a map of quantity by product.
     *
     * @param products map of quantity by product to be set
     */
    public void setProducts(Map<String, Integer> products) {
        this.products = products;
    }
}
