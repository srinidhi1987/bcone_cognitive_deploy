package com.automationanywhere.cognitive.service.order.models;

/**
 * Base entity, this class should be inherited for all entities implementation.
 */
public abstract class Entity {

    private Id id;

    /**
     * Default constructor.
     */
    public Entity() {
    }

    /**
     * Constructs with an ID of the entity.
     *
     * @param id the ID
     */
    public Entity(Id id) {
        this.id = id;
    }

    /**
     * Returns the entity ID.
     *
     * @return the entity ID
     */
    public Id getId() {
        return id;
    }

    /**
     * Sets the entity ID.
     *
     * @param id the entity ID to be set
     */
    public void setId(Id id) {
        this.id = id;
    }
}
