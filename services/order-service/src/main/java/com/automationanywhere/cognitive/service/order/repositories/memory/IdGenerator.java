package com.automationanywhere.cognitive.service.order.repositories.memory;

import com.automationanywhere.cognitive.service.order.models.AAId;

import java.util.UUID;

/**
 * ID generator class used by in memory repositories to generate IDs.
 */
public final class IdGenerator {

    /**
     * Generates a new ID.
     *
     * @return the instance of ID
     */
    public AAId nextId() {
        return new AAId(UUID.randomUUID());
    }
}
