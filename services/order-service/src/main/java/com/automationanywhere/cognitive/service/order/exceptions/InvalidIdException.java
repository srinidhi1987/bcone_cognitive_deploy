package com.automationanywhere.cognitive.service.order.exceptions;

public class InvalidIdException extends RuntimeException {

    public InvalidIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
