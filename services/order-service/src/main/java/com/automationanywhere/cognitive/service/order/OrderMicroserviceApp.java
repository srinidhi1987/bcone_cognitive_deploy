package com.automationanywhere.cognitive.service.order;

import org.springframework.boot.SpringApplication;

/**
 * Main application class.
 */
public final class OrderMicroserviceApp {

    private OrderMicroserviceApp() {
    }

    /**
     * Entry point.
     *
     * @param args - app args
     */
    public static void main(String[] args) {
        System.setProperty("log4j2.isThreadContextMapInheritable", "true");

        SpringApplication.run(SpringBootConfiguration.class, args);
    }
}
