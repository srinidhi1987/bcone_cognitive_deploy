package com.automationanywhere.cognitive.service.order.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Sets up the configuration for all controllers.
 */
@Configuration
@ComponentScan(basePackages = {
        "com.automationanywhere.cognitive.service.order.controllers",
        "com.automationanywhere.cognitive.common.healthcheck"
})
public class ControllerConfig {
}


