package com.automationanywhere.cognitive.service.order.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The product representation.
 */
@Table
@Entity
public class Product extends AAEntity {

  @Column
  private String name;
  @Column
  private int quantity;
  @ManyToOne
  @JoinColumn(name = "order_id", nullable = false)
  private Order order;

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  /**
   * Default constructor.
   */
  public Product() {
  }

  /**
   * Full constructor.
   *
   * @param id the ID of product
   * @param name the name of product
   * @param quantity the quantity of product
   */
  public Product(AAId id, String name, int quantity) {
    super(id);
    this.name = name;
    this.quantity = quantity;
  }

  /**
   * Full constructor.
   *
   * @param id the ID of product
   * @param name the name of product
   * @param quantity the quantity of product
   * @param order associated with the product
   */
  public Product(AAId id, String name, int quantity, Order order) {
    super(id);
    this.name = name;
    this.quantity = quantity;
    this.order = order;
  }

  /**
   * Returns the name of the product.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of the product.
   *
   * @param name the name to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Returns the quantity.
   *
   * @return the value of quantity
   */
  public int getQuantity() {
    return quantity;
  }

  /**
   * Sets the quantity.
   *
   * @param quantity the quantity to be set
   */
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
}
