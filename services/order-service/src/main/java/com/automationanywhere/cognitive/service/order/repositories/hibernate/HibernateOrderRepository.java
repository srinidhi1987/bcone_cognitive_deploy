package com.automationanywhere.cognitive.service.order.repositories.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;
import com.automationanywhere.cognitive.service.order.repositories.OrderRepository;

@Repository
public class HibernateOrderRepository extends HibernateRepository<Order> implements
    OrderRepository {

  @Autowired
  private HibernateObjectToRepositoryObjectMapper hibernateObjectToRepoObjectMapper;

  @Override
  public List<Order> searchOrdersByCustomer(AAId customerId) {
    Session session = super.getCurrentSession();
    SQLQuery sqlQuery = session.createSQLQuery(
        "select o.id as {o.id.value}, customer_id as {o.customerId.value}, date_created as {o.createdAt}, name as {p.name}, p.id as {p.id.value}, quantity as {p.quantity}, p.order_id as {p.order}  from order1 o, product p where p.order_id = o.id and o.customer_id = :customerID")
        .addEntity("o", Order.class).addEntity("p", Product.class);
    Query query = sqlQuery.setParameter("customerID", customerId.getValue());
    @SuppressWarnings("unchecked")
    List<Object[]> results = query.list();
    return hibernateObjectToRepoObjectMapper.getListOfOrder(results);
  }
}
