package com.automationanywhere.cognitive.service.order.repositories.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;

@Component("hibernateObjectToRepoObjectMapper")
public class HibernateObjectToRepositoryObjectMapper {

  public HibernateObjectToRepositoryObjectMapper() {
    // TODO Auto-generated constructor stub
  }

  public List<Order> getListOfOrder(List<Object[]> listOfProducts) {
    //Get the list of Products
    List<Product> products = listOfProducts.stream().map(result -> {
      Object[] temp = (Object[]) result;
      return (Product) temp[1];
    })
        .collect(Collectors.toList());

    //Get the list of Order
    Set<Order> orders = listOfProducts.stream().map(result -> {
      Object[] temp = (Object[]) result;
      Order order = (Order) temp[0];
      List<Product> tempProducts = products.stream()
          .filter(p -> p.getOrder().getId().equals(order.getId())).collect(Collectors.toList());
      order.setProducts(tempProducts);
      return order;
    })
        .collect(Collectors.toSet());
    return new ArrayList<Order>(orders);
  }
}
