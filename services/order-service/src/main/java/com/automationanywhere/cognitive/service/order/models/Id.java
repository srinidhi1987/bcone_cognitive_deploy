package com.automationanywhere.cognitive.service.order.models;

import java.util.UUID;

/**
 * Represents the identification of the entity.
 */
public class Id {

    private final UUID value;

    /**
     * Constructs with a UUID instance.
     *
     * @param value the UUID instance
     */
    public Id(UUID value) {
        this.value = value;
    }

    /**
     * Constructs with a string that represents the UUID, the string will be converted to UUID instance.
     *
     * @param value the string representation of UUID
     */
    public Id(String value) {
        if (value != null) {
            this.value = UUID.fromString(value);
        } else {
            this.value = null;
        }
    }

    /**
     * Returns the UUID value.
     *
     * @return the UUID value
     */
    public UUID get() {
        return value;
    }

    /**
     * Returns the string representation of the UUID instance.
     *
     * @return the string representation of the UUID
     */
    public String getValue() {
        return value == null ? null : value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Id id = (Id) o;

        return value != null ? value.equals(id.value) : id.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Id{"
                + "value=" + value
                + '}';
    }
}
