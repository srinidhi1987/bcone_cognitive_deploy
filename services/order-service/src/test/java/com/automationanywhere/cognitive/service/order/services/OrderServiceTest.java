package com.automationanywhere.cognitive.service.order.services;

import com.automationanywhere.cognitive.service.order.config.ServiceConfig;
import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;
import com.automationanywhere.cognitive.service.order.repositories.OrderRepository;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Class test of OrderService.
 */
@SpringBootTest(classes = {ServiceConfig.class})
@ComponentScan(basePackages = "com.automationanywhere.cognitive.service.order.config")
public class OrderServiceTest extends AbstractTestNGSpringContextTests {

    private static final int PRODUCT_QTY = 6;

    @Autowired
    private OrderService orderService;

    @MockBean
    @Autowired
    private OrderRepository orderRepository;

   // @Test
    public void testListOrdersOfCustomerWithValidCustomerIdRetrievingCustomerAndOrders() throws Exception {

        // GIVEN
        AAId productId = new AAId(UUID.randomUUID());
        AAId customerId = new AAId(UUID.randomUUID());
        AAId orderId = new AAId(UUID.randomUUID());
        String productName = "iPhone X";
        int productQty = PRODUCT_QTY;

        Order order = new Order(orderId, customerId, new Date());
        order.setProducts(Lists.newArrayList(new Product(productId, productName, productQty)));
        when(orderRepository.searchOrdersByCustomer(customerId)).thenReturn(Lists.newArrayList(order));

        // WHEN
        List<Order> orders = orderService.listOrdersByCustomer(customerId).get();

        // THEN
        assertThat(orders.size()).isEqualTo(1);

        Order orderReturned = orders.get(0);
        assertThat(orderReturned.getId()).isEqualTo(orderId);
        assertThat(orderReturned.getCustomerId()).isEqualTo(customerId);

        List<Product> products = order.getProducts();
        assertThat(products.size()).isEqualTo(1);

        Product product = products.get(0);
        assertThat(product.getName()).isEqualTo(productName);
        assertThat(product.getQuantity()).isEqualTo(productQty);
    }


    //@Test
    public void testListOrdersOfCustomerWithNofFoundCustomerIdRetrievingEmpty() throws Exception {

        // GIVEN
        AAId customerId = new AAId(UUID.randomUUID());

        // WHEN
        List<Order> orders = orderService.listOrdersByCustomer(customerId).get();

        // THEN
        assertThat(orders.size()).isEqualTo(0);
    }
}
