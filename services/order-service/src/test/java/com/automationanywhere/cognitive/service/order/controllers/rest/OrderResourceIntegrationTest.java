package com.automationanywhere.cognitive.service.order.controllers.rest;

import com.automationanywhere.cognitive.service.order.controllers.dto.v1.ErrorDTO;
import com.automationanywhere.cognitive.service.order.controllers.dto.v1.OrderDTO;
import com.automationanywhere.cognitive.service.order.repositories.DummyData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.automationanywhere.cognitive.service.order.repositories.DummyData.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test class of REST API OrderResource endpoint.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderResourceIntegrationTest extends AbstractTestNGSpringContextTests {

    public static final int TOTAL_ORDERS = 3;
    public static final int TOTAL_PROD_ORD_1 = 1;
    public static final int PROD_QTY_1 = 1;
    public static final int TOTAL_PROD_ORD_2 = 2;
    public static final int TOTAL_PROD_ORD_3 = 3;
    public static final int PROD_QTY_2 = 2;
    public static final int PROD_QTY_3 = 3;
    public static final int PROD_QTY_4 = 4;
    public static final int PROD_QTY_5 = 5;
    public static final int PROD_QTY_6 = 6;
    public static final int ERROR_CODE = 500;
    public static final int BAD_REQUEST = 400;
    public static final String URL = "/api/orders?customerId=";

    @Autowired
    private TestRestTemplate restTemplate;

//    @Test
    public void integrationTestRequestAllOrdersWithValidCustomerGetCustomerAndOrders() {

        // GIVEN
        String customerId = DummyData.CUSTOMER_ID.getValue();

        // WHEN
        ResponseEntity<List<OrderDTO>> responseEntity = restTemplate.exchange(URL + customerId,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<OrderDTO>>() {
                }
        );

        // THEN
        List<OrderDTO> orders = responseEntity.getBody();
        assertThat(orders.size()).isEqualTo(TOTAL_ORDERS);
        Map<String, OrderDTO> ordersMap = orders.stream()
                .collect(Collectors.toMap(OrderDTO::getOrderId, order -> order));

        OrderDTO orderDTO = ordersMap.get(ORDER_ID_1.getValue());
        assertThat(orderDTO).isNotNull();
        assertThat(orderDTO.getCustomerId()).isEqualTo(CUSTOMER_ID.getValue());
        Map<String, Integer> products = orderDTO.getProducts();
        assertThat(products.size()).isEqualTo(TOTAL_PROD_ORD_1);
        assertThat(products.get("iPhone X")).isEqualTo(PROD_QTY_1);

        orderDTO = ordersMap.get(ORDER_ID_2.getValue());
        assertThat(orderDTO).isNotNull();
        assertThat(orderDTO.getCustomerId()).isEqualTo(CUSTOMER_ID.getValue());
        products = orderDTO.getProducts();
        assertThat(products.size()).isEqualTo(TOTAL_PROD_ORD_2);
        assertThat(products.get("iPhone 8 Plus")).isEqualTo(PROD_QTY_2);
        assertThat(products.get("iPhone 8")).isEqualTo(PROD_QTY_3);

        orderDTO = ordersMap.get(ORDER_ID_3.getValue());
        assertThat(orderDTO).isNotNull();
        assertThat(orderDTO.getCustomerId()).isEqualTo(CUSTOMER_ID.getValue());
        products = orderDTO.getProducts();
        assertThat(products.size()).isEqualTo(TOTAL_PROD_ORD_3);
        assertThat(products.get("iPhone X")).isEqualTo(PROD_QTY_4);
        assertThat(products.get("iPhone 8 Plus")).isEqualTo(PROD_QTY_5);
        assertThat(products.get("iPhone 8")).isEqualTo(PROD_QTY_6);
    }

    @Test
    public void integrationTestRequestAllOrdersWithNotFoundCustomerGetEmpty() throws Exception {

        // GIVEN
        String customerId = UUID.randomUUID().toString();
        String expected = "[]";

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);

        // WHEN
        ResponseEntity<String> responseEntity = restTemplate.exchange(
          new RequestEntity<>(headers, HttpMethod.GET, new URI(URL + customerId)), String.class
        );
        String body = responseEntity.getBody();

        // THEN
        assertThat(body).contains("Some error occurred while fetching order for customer Id");
    }

    @Test
    public void integrationTestRequestAllOrdersWithInvalidCustomerGetError() {

        // GIVEN
        String invalidUuid = "aaa";

        // WHEN
        ResponseEntity<ErrorDTO> responseEntity = restTemplate.getForEntity(URL + invalidUuid, ErrorDTO.class);

        // THEN
        ErrorDTO error = responseEntity.getBody();
        assertThat(error.getCode()).isEqualTo(BAD_REQUEST);
        assertThat(error.getMessage()).isEqualTo("Invalid customer ID");
    }

    @Test
    public void integrationTestRequestAllOrdersWithoutCustomerGetBadRequest() {

        // GIVEN

        // WHEN
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/orders", String.class);

        // THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNullOrEmpty();
    }
}
