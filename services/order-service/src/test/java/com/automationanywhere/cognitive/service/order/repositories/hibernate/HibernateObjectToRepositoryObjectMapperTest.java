package com.automationanywhere.cognitive.service.order.repositories.hibernate;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.models.Product;

public class HibernateObjectToRepositoryObjectMapperTest {

  public HibernateObjectToRepositoryObjectMapperTest() {
    // TODO Auto-generated constructor stub
  }

  @Test
  public void testOfGetListOfOrderWhenListOfOrdersFetchedFromQuery() {

    //given
    Order o1 = new Order(new AAId("8c53e636-a239-4073-9e03-3df28815a90e"),
        new AAId("2c076351-774b-49e4-8e55-ce0ab4351ceb"), new Date());
    Product p1 = new Product(new AAId(), "P1", 1);
    p1.setOrder(o1);
    Product p2 = new Product(new AAId(), "P2", 2);
    p2.setOrder(o1);
    /*Product[] products = {p1, p2};
		Order[] orders = {o1};
		*/
    Object[] obj1 = {o1, p1};
    Object[] obj2 = {o1, p2};
    List<Object[]> objects = new ArrayList<Object[]>();
    objects.add(obj1);
    objects.add(obj2);
    HibernateObjectToRepositoryObjectMapper hibernateObjectToRepositoryObjectMapper = new HibernateObjectToRepositoryObjectMapper();

    //when
    List<Order> ordersList = hibernateObjectToRepositoryObjectMapper.getListOfOrder(objects);

    //then
    Order o3 = ordersList.get(0);
    assertThat(o3.getId()).isEqualTo(o1.getId());
    assertThat(o3.getProducts()).isEqualTo(o1.getProducts());
  }
}
