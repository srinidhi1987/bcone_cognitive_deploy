package com.automationanywhere.cognitive.service.order.models;

import org.testng.annotations.Test;

import java.util.Date;
import java.util.UUID;

import static com.automationanywhere.cognitive.service.order.repositories.DummyData.CUSTOMER_ID;
import static com.automationanywhere.cognitive.service.order.repositories.DummyData.ORDER_ID_1;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class of Order.
 */
public class OrderTest {

    @Test
    public void testEmptyConstructorGetterAndSetter() {
        // GIVEN
        AAId orderId = ORDER_ID_1;
        AAId customerId = CUSTOMER_ID;

        // WHEN
        Order order = new Order();
        order.setId(orderId);
        order.setCustomerId(customerId);

        // THEN
        assertThat(order.getId()).isEqualTo(orderId);
        assertThat(order.getCustomerId()).isEqualTo(customerId);
    }

    @Test
    public void testFullConstructorAndGetter() {
        // GIVEN
        AAId orderId = new AAId(UUID.randomUUID());
        AAId customerId = new AAId(UUID.randomUUID());

        // WHEN
        Order order = new Order(orderId, customerId, new Date());

        // THEN
        assertThat(order.getId()).isEqualTo(orderId);
        assertThat(order.getCustomerId()).isEqualTo(customerId);
    }
}
