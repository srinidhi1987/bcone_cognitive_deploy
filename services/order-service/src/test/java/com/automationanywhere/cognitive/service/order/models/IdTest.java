package com.automationanywhere.cognitive.service.order.models;

import org.testng.annotations.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class of Id.
 */
public class IdTest {

    @Test
    public void testStringConstructorAndGet() {

        // GIVEN
        String value = "2c076351-774b-49e4-8e55-ce0ab4251ceb";

        // WHEN
        AAId id = new AAId(UUID.fromString(value));

        // THEN
        assertThat(id.get()).isEqualTo(UUID.fromString(value));
    }

    @Test
    public void testEqualsAndHash() {

        // GIVEN
        UUID value = UUID.fromString("2c076351-774b-49e4-8e55-ce0ab4251ceb");
        String value2 = "b0c60f97-eeaa-46f7-8ef8-2d34fd2c244f";

        // WHEN
        Id id = new Id(value);
        Id id2 = new Id(value2);
        Id id3 = new Id(value2);

        // THEN
        assertThat(id.hashCode()).isEqualTo(value.hashCode());
        assertThat(id2.hashCode()).isNotEqualTo(value.hashCode());
        assertThat(id.equals(id2)).isFalse();
        assertThat(id2.equals(id3)).isTrue();
    }

    @Test
    public void testWithNullValue() {

        // WHEN
        Id id = new Id((UUID) null);
        Id id2 = new Id((String) null);

        // THEN
        assertThat(id.hashCode()).isEqualTo(0);
        assertThat(id2.hashCode()).isEqualTo(0);
        assertThat(id.equals(id2)).isTrue();
        assertThat(id.getValue()).isNull();
    }
}
