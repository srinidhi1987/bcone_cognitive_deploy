package com.automationanywhere.cognitive.service.order.repositories.memory;

import com.automationanywhere.cognitive.service.order.models.AAId;
import com.automationanywhere.cognitive.service.order.models.Order;
import com.automationanywhere.cognitive.service.order.repositories.DummyData;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.automationanywhere.cognitive.service.order.repositories.DummyData.CUSTOMER_ID;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class of InMemoryOrderRepository.
 */
public class InMemoryOrderRepositoryTest {

    public static final int MINUTE_IN_MILLISECONS = 60000;
    private static final int TOTAL_ORDERS = 3;

    @Test
    public void testListAllOrders() {

        // GIVEN
        InMemoryOrderRepository repository = new InMemoryOrderRepository();

        // WHEN
        List<Order> orders = repository.listAll();

        // THEN
        assertThat(orders.size()).isEqualTo(TOTAL_ORDERS);
    }

    @Test
    public void testCreateReadOrderWithAllAttributes() {

        // GIVEN
        Date date = new Date();

        Order order = new Order();
        order.setCustomerId(CUSTOMER_ID);

        // WHEN
        InMemoryOrderRepository repository = new InMemoryOrderRepository();
        repository.create(order);

        // THEN
        Order expectedOrder = repository.get(order.getId());
        assertThat(order.getId()).isEqualTo(expectedOrder.getId());
        assertThat(order.getCustomerId()).isEqualTo(CUSTOMER_ID);
    }

    @Test
    public void testUpdateReadOrderWithAllAttributes() {

        // GIVEN
        AAId orderId = DummyData.ORDER_ID_1;
        Date date = new Date();

        InMemoryOrderRepository repository = new InMemoryOrderRepository();
        Order order = repository.get(orderId);

        // WHEN
        order.setCustomerId(CUSTOMER_ID);
        repository.update(order);

        // THEN
        Order expectedOrder = repository.get(order.getId());
        assertThat(order.getId()).isEqualTo(expectedOrder.getId());
        assertThat(order.getCustomerId()).isEqualTo(CUSTOMER_ID);
    }

    @Test
    public void testUpdateReadOrderWithAllAttributesAndWithoutId() {

        // GIVEN
        AAId customerId = new AAId(UUID.randomUUID());
        Date date = new Date(System.currentTimeMillis() - MINUTE_IN_MILLISECONS);
        InMemoryOrderRepository repository = new InMemoryOrderRepository();

        // WHEN
        Order order = new Order();
        order.setCustomerId(customerId);
        repository.update(order);

        // THEN
        Order expectedOrder = repository.get(order.getId());
        assertThat(order.getId()).isEqualTo(expectedOrder.getId());
        assertThat(order.getCustomerId()).isEqualTo(customerId);
    }

    @Test
    public void testDeleteOrderWithValidId() {

        // GIVEN
        InMemoryOrderRepository repository = new InMemoryOrderRepository();
        AAId orderId = DummyData.ORDER_ID_1;
        assertThat(repository.get(orderId)).isNotNull();

        // WHEN
        repository.delete(orderId);

        // THEN
        assertThat(repository.get(orderId)).isNull();
    }

    @Test
    public void testSearchOrdersByCustomerWithValidId() {

        // GIVEN
        InMemoryOrderRepository repository = new InMemoryOrderRepository();

        // WHEN
        List<Order> orders = repository.searchOrdersByCustomer(CUSTOMER_ID);

        // THEN
        assertThat(orders).isNotNull();
        assertThat(orders).size().isEqualTo(TOTAL_ORDERS);
    }

    @Test
    public void testSearchOrdersByCustomerWithInvalidId() {

        // GIVEN
        AAId customerId = new AAId(UUID.randomUUID());
        InMemoryOrderRepository repository = new InMemoryOrderRepository();

        // WHEN
        List<Order> orders = repository.searchOrdersByCustomer(customerId);

        // THEN
        assertThat(orders).isNotNull();
        assertThat(orders).size().isEqualTo(0);
    }
}
