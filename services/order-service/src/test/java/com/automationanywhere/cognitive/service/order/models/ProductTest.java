package com.automationanywhere.cognitive.service.order.models;

import org.testng.annotations.Test;

import java.util.UUID;

import static com.automationanywhere.cognitive.service.order.repositories.DummyData.PRODUCT_ID_1;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class of Product.
 */
public class ProductTest {

    private static final int QUANTITY = 1;

    @Test
    public void testEmptyConstructorGetterAndSetter() {
        // GIVEN
        AAId productId = PRODUCT_ID_1;
        String name = "iPhone X";

        // WHEN
        Product product = new Product();
        product.setId(productId);
        product.setName(name);
        product.setQuantity(QUANTITY);

        // THEN
        assertThat(product.getId()).isEqualTo(productId);
        assertThat(product.getName()).isEqualTo(name);
        assertThat(product.getQuantity()).isEqualTo(QUANTITY);
    }

    @Test
    public void testFullConstructorAndGetter() {
        // GIVEN
        AAId productId = new AAId(UUID.randomUUID());
        String name = "iPhone X";

        // WHEN
        Product product = new Product(productId, name, QUANTITY);

        // THEN
        assertThat(product.getId()).isEqualTo(productId);
        assertThat(product.getName()).isEqualTo(name);
        assertThat(product.getQuantity()).isEqualTo(QUANTITY);
    }
}
