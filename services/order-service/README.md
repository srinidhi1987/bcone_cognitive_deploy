# MICROSERVICE
The Order Service is a reference implementation for all microservices. 
The microservice should not depend on other modules or projects, the only exception is the common module and only infrastructure codes should be included there. 
It is a server created using the Spring Boot Framework. 

## Layers and Packages
Microservice will have 3 layers and 6 main packages. The main layer is the service layer

Layers:
* Controllers - Responsible to receive the request and sent response, could be REST API, Web Service, Message Queues, 
etc.
* Services - Keeps all the business rules of the microservice and the main model
* Repositories - Contains all classes responsible for access data used by the microservice, I.E. databases, webservices, 
Amazon S3, etc.

Packages:
* components - Contains all components shared by layers
* config - Any configuration class or set up of instances
* controllers - Everything related to controllers should be here, including DTOs, converters, mappers, etc.
* models - Keeps the core model used by services.
* repositories - All classes used to access/store data and convert the service model to storage model
* services - The main package, keeps all services class with business rules and related features

## Architecture
The microservice was constructed based on a hexagonal architecture, it means the **service** class is our **core** and 
must be isolated with not external dependence, the input and output will occurs through ports. The controllers will be 
our primary ports and the repository the secondary ports. You can read more about it 
<a href="http://java-design-patterns.com/blog/build-maintainable-systems-with-hexagonal-architecture/" target="_blank">here</a>. 

### Controllers

#### REST API
REST API are the endpoints, the interface with microservice clients, they are created as 
<a href="https://spring.io/guides/gs/rest-service/" target="_blank">Spring REST controllers</a>.
The controllers mapped methods should execute asynchronous, not blocking the caller thread. So it should return always a 
CompletableFuture instance and the Spring will handle and convert it automatically to DeferredResult keeping the 
connection opened and releasing the thread to handle new incoming requests. 

```java
package com.automationanywhere.cognitive.service.order.controllers.rest.v1;
 
@RestController("OrderResourceV1")
public class OrderResource {
    
    @RequestMapping(value = "/api/orders", method = RequestMethod.GET,
            headers = {"accept=application/vnd.cognitive.v1+json"},
            consumes = {"application/vnd.cognitive.v1+json"}
    )
    public CompletableFuture<List<OrderDTO>> listOrdersByCustomer(@RequestParam String customerId) {
    }
}
```

The controllers will use mappers to convert the service models to DTOs, this DTOs will be returned within 
CompletableFuture and converted to JSON by the Spring, to permit an evolution without breaks any microservice client, 
the JSON returned must have strictly controlled changes, for this, all endpoints and DTOs should be versioned.
All versioned classes should have as the last package the version as in 
``com.automationanywhere.cognitive.service.order.controllers.rest.v1`` grouping classes with same version.
The controllers should use the content-type approach to versioning the mapping and the last version should accept 
default content-type. The response headers always will have the version of the content-type, as in the example below.

```java 
@RequestMapping(value = "/api/orders", method = RequestMethod.GET,
        headers = {
            "accept=application/vnd.cognitive.v1+json",
            "accept=application/vnd.cognitive+json",
            "accept=application/json"
        },
        consumes = {"application/vnd.cognitive.v1+json"}
)
```

### Services


## Infrastructure

### Health check and Heartbeat

### Build

### Check style, PMD, Code Formatter