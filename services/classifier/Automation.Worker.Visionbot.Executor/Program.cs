/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Services.Client;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.NewCode;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using CommandLine;
using System.Text;
using Automation.Cognitive.VisionBotEngine.Model.ExecutorOperations;
using Automation.Services.Client.Common;
using Automation.IQBot.Logger;

namespace Automation.Worker.Visionbot
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var operationParam = new OperationParam();
                Parser.Default.ParseArguments(args, operationParam);
                if (string.IsNullOrEmpty(operationParam.Name))
                {
                    WriteError("operation not specified");
                    IQBotLogger.Error("operation not specified");
                    Environment.Exit(1);
                    return;
                }
              
                Operation currentOperationParam = OperationHelper.GetOperation(operationParam, args);

                if (currentOperationParam == null)
                {
                    WriteError($"arguments for operation \"{operationParam.Name}\" not correctly specified");
                    IQBotLogger.Error($"arguments for operation \"{operationParam.Name}\" not correctly specified");
                    Environment.Exit(2);
                    return;
                }
                initializingLoggingMechanism(operationParam, args);
                initializingCognitiveSecurePropertiesProvider();
                currentOperationParam.Execute();
            }
            catch (Exception ex)
            {
                WriteError(ex.Message);
                IQBotLogger.Fatal(ex.Message, ex);
                Environment.Exit(2);
            }
        }

        public static void WriteError(string errorMsg)
        {
            Console.Write($"ERROR: {errorMsg}");
        }

        private static void initializingLoggingMechanism(OperationParam operationParam, string[] args)
        {
            string logDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\Automation Anywhere IQBot Platform\\Logs\\";
            string servicConfigurationPath = "CognitiveServiceConfiguration.json";
            string path = Path.Combine(
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
            string json = File.ReadAllText(path);

            CognitiveServicesConfiguration _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);

            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }
            var dt = DateTime.Now;
            var dtStamp = $"{dt.Year}.{dt.Month}.{dt.Day}-{dt.Hour}.{dt.Minute}.{dt.Second}.{dt.Millisecond}";
            if (operationParam.Name.Equals(GenerateAndExtractVbotDataOperationParams.OperationName))
            {
                var param = new GenerateAndExtractVbotDataOperationParams();
                Parser.Default.ParseArguments(args, param);
                logDir = Path.Combine(logDir, $"VisionBotWorkerExecutor_{dtStamp}_{operationParam.Name}_{param.DocumentId}.log");
            }
            else
            {
                logDir = Path.Combine(logDir, $"VisionBotWorkerExecutor_{dtStamp}_{operationParam.Name}.log");
            }
            VBotLogger.Initlization(logDir, _cognitiveServicesConfiguration.LoggerSettings);
        }

        private static void initializingCognitiveSecurePropertiesProvider()
        {
            try
            {
                string relativePathToPropertyFile = "";
                string basedir = AppDomain.CurrentDomain.BaseDirectory;
                IQBotLogger.Trace($"[basedir] {basedir}");

                var fileInfo = new FileInfo(basedir);
                var parentDir = fileInfo.Directory == null ? null : fileInfo.Directory.Parent;
                if (parentDir != null)
                {
                    var resourceInfo = new FileInfo(parentDir.FullName);
                    relativePathToPropertyFile = resourceInfo.DirectoryName + "\\Configurations";
                }
                CognitiveSecurePropertiesProvider.Init(relativePathToPropertyFile, "Cognitive.properties", "private.key");
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Initializing failed {ex.Message}", ex);
            }
        }
    }

    public static class SerializeHelper
    {
        public static string SerializeData(Object data)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
            return JsonConvert.SerializeObject(data, settings);
        }

        public static T DeserializeData<T>(string jsonObject)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
            return JsonConvert.DeserializeObject<T>(jsonObject, settings);
        }
    }

    #region Operation

    public static class OperationHelper
    {
        public static Operation GetOperation(OperationParam operationParam, string[] args)
        {
            if (string.IsNullOrWhiteSpace(operationParam.Name)) return null;

            operationParam.Name = operationParam.Name.ToLower();

            if (operationParam.Name.Equals(CreateVisionBotOperationParams.OperationName))
            {
                var param = new CreateVisionBotOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new CreateVisionBotOperation(param) : null;
            }
            if (operationParam.Name.Equals(AddNewFieldsOperationParams.OperationName))
            {
                var param = new AddNewFieldsOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new AddNewFieldsOperation(param) : null;
            }
            if (operationParam.Name.Equals(GenerateAndExtractVbotDataOperationParams.OperationName))
            {
                var param = new GenerateAndExtractVbotDataOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new GenerateAndExtractVbotDataOperation(param) : null;
            }
            if (operationParam.Name.Equals(ExtractVBotDataOperationParams.OperationName))
            {
                var param = new ExtractVBotDataOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new ExtractVBotDataOperation(param) : null;
            }
            if (operationParam.Name.Equals(GetValueFieldOperationParams.OperationName))
            {
                var param = new GetValueFieldOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new GetValueFieldOperation(param) : null;
            }
            if (operationParam.Name.Equals(GetImageOperationParams.OperationName))
            {
                var param = new GetImageOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new GetImageOperation(param) : null;
            }
            if (operationParam.Name.Equals(ExportDataOperationParams.OperationName))
            {
                var param = new ExportDataOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new ExportDataOperation(param) : null;
            }
            if (operationParam.Name.Equals(ProcessDocumentOperationParams.OperationName))
            {
                var param = new ProcessDocumentOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new ProcessDocumentOperation(param) : null;
            }
            if (operationParam.Name.Equals(ProcessStagingDocumentOperationParams.OperationName))
            {
                var param = new ProcessStagingDocumentOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new ProcessStagingDocumentOperation(param) : null;
            }
            if (operationParam.Name.Equals(GetVBotReportDataOperationParams.OperationName))
            {
                var param = new GetVBotReportDataOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new GetVBotReportDataOperation(param) : null;
            }

            return null;
        }
    }

    public abstract class Operation
    {
        protected abstract string ExecuteAndGetResult();

        public virtual void Execute()
        {
            var result = ExecuteAndGetResult();
            ReturnResult(result);
        }

        protected void ReturnResult(string result)
        {
            var filePath = Path.Combine(System.IO.Path.GetTempPath(), $"{Guid.NewGuid().ToString()}.result");
            File.WriteAllText(filePath, result);
            Console.Write(filePath);
        }
    }

    public class CreateVisionBotOperation : Operation
    {
        public CreateVisionBotOperationParams Params { get; private set; }

        public CreateVisionBotOperation(CreateVisionBotOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var visionbotId = engineAPI.CreateBasicVisionBot(
                this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.VisionBotDetailId);

            return SerializeHelper.SerializeData(visionbotId);
        }
    }

    public class AddNewFieldsOperation : Operation
    {
        public AddNewFieldsOperationParams Params { get; private set; }

        public AddNewFieldsOperation(AddNewFieldsOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var result = engineAPI.AddFieldsInVisionbot(
                this.Params.OrganizationId, this.Params.ProjectId);

            return SerializeHelper.SerializeData(result);
        }
    }

    public class GenerateAndExtractVbotDataOperation : Operation
    {
        public GenerateAndExtractVbotDataOperationParams Params { get; private set; }

        public GenerateAndExtractVbotDataOperation(GenerateAndExtractVbotDataOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var vBotDocument = engineAPI.GetVBotDocument(
                this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.VisionBotId, this.Params.DocumentId);

            if (vBotDocument == null)
                vBotDocument = new VBotDocument();

            return SerializeHelper.SerializeData(vBotDocument);
        }
    }

    public class ExtractVBotDataOperation : Operation
    {
        public ExtractVBotDataOperationParams Params { get; private set; }

        public ExtractVBotDataOperation(ExtractVBotDataOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var vBotDocument = engineAPI.ExtractVBotData(
                this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.VisionBotId, this.Params.LayoutId);

            if (vBotDocument == null)
                vBotDocument = new VBotDocument();

            return SerializeHelper.SerializeData(vBotDocument);
        }
    }

    public class GetValueFieldOperation : Operation
    {
        public GetValueFieldOperationParams Params { get; private set; }

        public GetValueFieldOperation(GetValueFieldOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            FieldLayout fieldLayout = SerializeHelper.DeserializeData<FieldLayout>(this.Params.FieldLayout);

            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            Field valueField = engineAPI.GetValueField(
                this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.VisionBotId,
                this.Params.LayoutId, fieldLayout, this.Params.FieldDefId, this.Params.DocumentId);

            if (valueField == null)
                return string.Empty;

            return SerializeHelper.SerializeData(valueField);
        }
    }

    public class GetImageOperation : Operation
    {
        public GetImageOperationParams Params { get; private set; }

        public GetImageOperation(GetImageOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            string response = string.Empty;
            Bitmap image = null;
            string tempFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            try
            {
                VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
                image = engineAPI.GetImage(
                    this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.DocumentId, this.Params.PageIndex);
                if (image == null)
                    return string.Empty;

                image.Save(tempFilePath);

                byte[] fileBytes = File.ReadAllBytes(tempFilePath);
                response = SerializeHelper.SerializeData(fileBytes);
            }
            catch (Exception ex)
            {
                ex.Log("Program", "GetImage");
                IQBotLogger.Error(ex.Message, ex);
            }
            finally
            {
                if (image != null)
                {
                    image.Dispose();
                    image = null;
                }
                if (File.Exists(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
            }
            return response;
        }
    }

    public class ExportDataOperation : Operation
    {
        public ExportDataOperationParams Params { get; private set; }

        public ExportDataOperation(ExportDataOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            VBotData vbotData = getFileBasedParameterValue<VBotData>(this.Params.VbotData);

            VBotDocument vbotDocument = getFileBasedParameterValue<VBotDocument>(this.Params.VbotDocument);

            VBotData data = vbotData ?? vbotDocument?.Data;
            if (data == null) return string.Empty;

            string exportPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
            var projectMetaData = projectService.GetProjectMetaData(this.Params.OrganizationId, this.Params.ProjectId);
            var standardFieldsId = projectMetaData.fields.standard;

            List<string> fieldsIdsAsPerProject = new List<string>();
            fieldsIdsAsPerProject.AddRange(standardFieldsId);
            fieldsIdsAsPerProject.AddRange(projectMetaData.fields.custom.Select(x => x.id));

            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var result = engineAPI.ExportData(data, exportPath, fieldsIdsAsPerProject);
            IQBotLogger.Debug($"Exported data : {result}");

            string csvFileData = File.ReadAllText(exportPath);

            return SerializeHelper.SerializeData(csvFileData);
        }

        private static T getFileBasedParameterValue<T>(string value)
        {
            T result = default(T);
            if (!string.IsNullOrEmpty(value) && value.EndsWith(".input") && File.Exists(value))
            {
                var filePath = value;
                value = System.Text.Encoding.UTF8.GetString(File.ReadAllBytes(filePath));
                File.Delete(filePath);
            }

            try
            {
                result = string.IsNullOrWhiteSpace(value) ? default(T)
                    : SerializeHelper.DeserializeData<T>(value);
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"error deserializing {typeof(T)} data {value}", ex);
                throw;
            }
            return result;
        }
    }

    public class ProcessDocumentOperation : Operation
    {
        public ProcessDocumentOperationParams Params { get; private set; }

        public ProcessDocumentOperation(ProcessDocumentOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            IFileServiceEndPointConsumer fileService = new FileServiceEndPointConsumer(this.Params.OrganizationId, this.Params.ProjectId);
            FileDetails fileDetails = fileService.GetDocumentDetails(this.Params.DocumentId);
            if (!fileDetails.IsProduction)
            {
                return SerializeHelper.SerializeData(string.Empty);
            }

            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var validationStatus = engineAPI.ProcessDocument(
                this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.DocumentId);
            return SerializeHelper.SerializeData(validationStatus);
        }
    }

    public class ProcessStagingDocumentOperation : Operation
    {
        public ProcessStagingDocumentOperationParams Params { get; private set; }

        public ProcessStagingDocumentOperation(ProcessStagingDocumentOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var validationStatus = engineAPI.ProcessStagingDocument(
                this.Params.OrganizationId, this.Params.ProjectId, this.Params.CategoryId, this.Params.DocumentId,
                this.Params.VisionBotRunDetailsId, this.Params.DocumentProcessedDetailsId);

            return SerializeHelper.SerializeData(validationStatus);
        }
    }

    public class GetVBotReportDataOperation : Operation
    {
        public GetVBotReportDataOperationParams Params { get; private set; }

        public GetVBotReportDataOperation(GetVBotReportDataOperationParams param)
        {
            this.Params = param;
        }

        protected override string ExecuteAndGetResult()
        {
            if (string.IsNullOrWhiteSpace(this.Params.VBotDocument)) return string.Empty;
            var vbotDocument = SerializeHelper.DeserializeData<VBotDocument>(this.Params.VBotDocument);
            if (vbotDocument == null) return string.Empty;

            VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();
            var reportData = engineAPI.GetVBotReportData(vbotDocument.Data);
            return SerializeHelper.SerializeData(reportData);
        }
    }

    #endregion Operation
}