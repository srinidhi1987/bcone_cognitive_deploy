#include "stdafx.h"
#include "Logger.h"
#include"Utilities.h"
#include <io.h>   // For access().
#include <sys/types.h>  // For stat().
#include <sys/stat.h>   // For stat().
#include <experimental/filesystem>
#include <stdlib.h>
#include <ShlObj.h>

CLogger* CLogger::m_pThis = NULL;
ofstream CLogger::m_Logfile;

namespace fs = std::experimental::filesystem;
bool CLogger::IsDirectoryExist(string strPath)
{
	return 	fs::exists(strPath);

	//if (access(strPath.c_str(), 0) == 0)
	//{
	//	struct stat status;
	//	stat(strPath.c_str(), &status);
	//	if (status.st_mode & S_IFDIR)
	//	{
	//		return true;// cout << "The directory exists." << endl;
	//	}
	//	else
	//	{
	//		cout << "The path you entered is a file." << endl;
	//	}
	//}
	//else
	//{
	//	return false;// cout << "Path doesn't exist." << endl;
	//}
}
void CLogger::InitializeLogging()
{
	return;
	string logFolderName = "\\Automation Anywhere IQBot Platform\\Logs";
	string logDir = "";
	wchar_t path[1024];
	HRESULT hr = SHGetFolderPath(NULL, CSIDL_COMMON_DOCUMENTS, NULL,
		SHGFP_TYPE_CURRENT, path);

	if (FAILED(hr)) {
		logDir = logFolderName.c_str();
	}
	else
	{
		char str[1024];
		wcstombs(str, path, 1023);
		logDir = strcat(str, logFolderName.c_str());
	}
	if (!IsDirectoryExist(logDir))
	{
		fs::create_directories(logDir);
	}
	char *templogDir = (char*)logDir.c_str();
	char* logFileName = "\\ClassifierWorker.log";
	char* tempFileName = strcat(templogDir, logFileName);
	m_sFileName = tempFileName;
}
CLogger::CLogger()
{
	InitializeLogging();
}
CLogger* CLogger::GetLogger() {
	if (m_pThis == NULL) {
		m_pThis = new CLogger();
		//m_Logfile.open(m_pThis->m_sFileName, ios::out | ios::app);
	}
	return m_pThis;
}

void CLogger::Log(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);
	cout << sMessage << endl;
	return;
	//m_Logfile.open(m_sFileName, ios::out | ios::app);
	//m_Logfile.seekp(std::ios::end);
	//char* sMessage = NULL;
	//int nLength = 0;
	//va_list args;
	//va_start(args, format);
	////  Return the number of characters in the string referenced the list of arguments.
	//// _vscprintf doesn't count terminating '\0' (that's why +1)
	//nLength = _vscprintf(format, args) + 1;
	//sMessage = new char[nLength];
	//vsprintf_s(sMessage, nLength, format, args);
	////vsprintf(sMessage, format, args);
	//m_Logfile << Util::CurrentDateTime() << ":\t";
	//m_Logfile << sMessage << "\n";
	//va_end(args);
	//m_Logfile.close();
	//delete[] sMessage;
}

void CLogger::Log(const string& sMessage)
{
	cout << sMessage << endl;
	return;
	m_Logfile.open(m_sFileName, ios::out | ios::app);
	m_Logfile << Util::CurrentDateTime() << ":\t";
	m_Logfile << sMessage << "\n";

	m_Logfile.close();
}

CLogger& CLogger::operator<<(const string& sMessage)
{
	m_Logfile.open(m_sFileName, ios::out | ios::app);
	m_Logfile << "\n" << Util::CurrentDateTime() << ":\t";
	m_Logfile << sMessage << "\n";
	m_Logfile.close();
	return *this;
}