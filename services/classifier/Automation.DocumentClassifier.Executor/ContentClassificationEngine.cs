﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.DocumentClassifier;
using Automation.VisionBotEngine.Model;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    class ContentClassificationEngine
    {
        internal ContentClassificationResult classifyByContent(string[] featureWords
            , DocumentVectorDetails documentVectorDetails
            , int minNoOfFields)
        {
            return new ContentClassification()
                .GetClassificationID(featureWords
                , documentVectorDetails
                , minNoOfFields);
        }
    }
}
