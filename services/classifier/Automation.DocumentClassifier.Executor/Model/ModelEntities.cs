using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;

/**
* Copyright (c) 2017 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

namespace Automation.Model
{
    internal class FileBlobsEntity
    {
        public string fileId { get; set; }
        public byte[] fileBlob { get; set; }
    }

    public class ProjectDetailEntity
    {
        public string projectType;
        public FieldsEntity fields { get; set; }
        public string primaryLanguage;
        public string projectTypeId;

        //public List<string> fields { get; set; }
    }

    public class FieldsEntity
    {
        public List<string> standard { get; set; }

        public List<CustomField> custom { get; set; }
    }

    public class CustomField
    {
        public string type { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    //internal class FieldDetailBasedOnProjectType
    //{
    //    public List<AliasDetail> fields
    //    {
    //        get; set;
    //    }
    //}

    //internal class AliasDetail
    //{
    //    public string id { get; set; }
    //    public string name { get; set; }
    //    public string fieldType { get; set; }

    //    public List<string> aliases
    //    {
    //        get; set;
    //    }
    //}

    internal class FieldIdAndAliases
    {
        public string fieldId { get; set; }
        public List<string> aliases { get; set; }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal class classificationResult
    {
        public string fieldId { get; set; }
        public string aliasName { get; set; }
        public bool isFound { get; set; }
    }

    //public class AliasLanguageDetail
    //{
    //    [JsonProperty(PropertyName = "id")]
    //    public string Id { get; set; }

    //    [JsonProperty(PropertyName = "name")]
    //    public string Name;

    //    [JsonProperty(PropertyName = "code")]
    //    public string Code;
    //}
}