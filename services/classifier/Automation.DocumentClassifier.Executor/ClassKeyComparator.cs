using Automation.IQBot.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    internal class ClassKeyComparator
    {
        internal static double GetMatchPercentage(string sourceKeysOrg, string targetKeysOrg)
        {
            string sourceKeys = string.Empty;
            string targetKeys = string.Empty;

            initializeSourceAndTargetKeys(sourceKeysOrg
                , targetKeysOrg
                , out sourceKeys, out targetKeys);

            double newAlgorithmSimilarity = 0;

            string[] sourceKeyList = convertStringKeysToKeysArray(sourceKeys);
            string[] targetKeyList = convertStringKeysToKeysArray(targetKeys);

            try
            {
                List<KeyMappingInfo> sourceToTargetKeyMappingInfoList = getKeysMappingResult(sourceKeyList, targetKeyList);

                List<string> mappingFoundSourceKeysList = new List<string>();

                List<KeyMappingInfo> mappingFoundSourceKeyMappingInfoList = new List<KeyMappingInfo>();
                foreach (KeyMappingInfo keyMappingInfo in sourceToTargetKeyMappingInfoList)
                {
                    if (isThisKeyMappedWithAnyTargetKey(keyMappingInfo))
                    {
                        mappingFoundSourceKeysList.Add(keyMappingInfo.KeyObject.Value);
                        mappingFoundSourceKeyMappingInfoList.Add(keyMappingInfo);
                    }
                }

                List<string> targetKeysAfterRemove = getTargetKeysAfterRemovingNotMappedDuplicateKeys(targetKeyList, sourceToTargetKeyMappingInfoList);

                int twiceMatching = getTwoKeysCascadeMatchingCount(mappingFoundSourceKeyMappingInfoList);

                double sourceKeyRemovedRatio = ((double)mappingFoundSourceKeysList.Count) / sourceKeyList.Length;

                if (areAllCriteriaMatchToUseThisResult(targetKeysAfterRemove
                    , mappingFoundSourceKeysList
                    , twiceMatching
                    , sourceKeyRemovedRatio))
                {
                    string modifiedSourceKeys = convertKeysListToStringKeys(mappingFoundSourceKeysList);
                    string modifiedDestinationKeys = convertKeysListToStringKeys(targetKeysAfterRemove);

                    double distance = (double)LevenshteinDistance(modifiedSourceKeys, modifiedDestinationKeys);
                    newAlgorithmSimilarity = 1 - (distance / (double)Math.Max(mappingFoundSourceKeysList.Count, targetKeysAfterRemove.Count));

                }
            }
            catch (Exception ex)
            {
                IQBotLogger.Error(ex.Message, ex);
            }
            double distanceOfOrgkeys = (double)LevenshteinDistance(sourceKeys, targetKeys);
            double orginalKeysSimilarity = 1 - (distanceOfOrgkeys / (double)Math.Max(sourceKeyList.Length, targetKeyList.Length));

            double similarity = (double)Math.Max(orginalKeysSimilarity, newAlgorithmSimilarity);

            return similarity;
        }

        private static List<string> getTargetKeysAfterRemovingNotMappedDuplicateKeys(string[] targetKeyList, List<KeyMappingInfo> sourceToTargetKeyMappingInfoList)
        {
            List<string> targetKeysAfterRemove = targetKeyList.ToList();
            List<int> possibleRemovableIndexes = getDuplicateKeysIndexesBasedOnMappingInfo(targetKeysAfterRemove, sourceToTargetKeyMappingInfoList);

            for (int rmIndex = possibleRemovableIndexes.Count - 1; rmIndex > -1; rmIndex--)
            {
                targetKeysAfterRemove.RemoveAt(possibleRemovableIndexes[rmIndex]);
            }

            return targetKeysAfterRemove;
        }

        private static List<int> getDuplicateKeysIndexesBasedOnMappingInfo(List<string> modifiedDestinationStringList, List<KeyMappingInfo> sourceToTargetKeyMappingInfoList)
        {
            List<int> possibleRemovableIndexes = new List<int>();
            for (int index = 0; index < sourceToTargetKeyMappingInfoList.Count - 1; index++)
            {
                KeyMappingInfo currentKeyMappingInfo = sourceToTargetKeyMappingInfoList[index];
                KeyMappingInfo nextKeyMappingInfo = sourceToTargetKeyMappingInfoList[index + 1];

                List<int> indexsToBeRemove = getDuplicateKeysIndexesInBetweenGivenKeyMappingInfo(modifiedDestinationStringList,
                    currentKeyMappingInfo, nextKeyMappingInfo);

                if (indexsToBeRemove.Count > 0) { possibleRemovableIndexes.AddRange(indexsToBeRemove); }

            }

            return possibleRemovableIndexes;
        }

        private static List<int> getDuplicateKeysIndexesInBetweenGivenKeyMappingInfo(List<string> modifiedDestinationStringList, 
            KeyMappingInfo currentKeyMappingInfo, KeyMappingInfo nextKeyMappingInfo)
        {
            List<int> duplicateIndexes = new List<int>();
            for (int destinationKeysIndex = currentKeyMappingInfo.DestinationMatchedIndex + 1;
                                           destinationKeysIndex < nextKeyMappingInfo.DestinationMatchedIndex;
                                           destinationKeysIndex++)
            {
                string value = modifiedDestinationStringList[destinationKeysIndex];

                if (doesListContainThisValueMoreThanOnce(modifiedDestinationStringList, value))
                {
                    duplicateIndexes.Add(destinationKeysIndex);
                }
            }
            return duplicateIndexes;
        }

        private static bool doesListContainThisValueMoreThanOnce(List<string> modifiedDestinationStringList, string value)
        {
            return modifiedDestinationStringList.FindAll(x => x == value).Count > 1;
        }

        private static bool isThisKeyMappedWithAnyTargetKey(KeyMappingInfo currentValue)
        {
            return currentValue.DestinationMatchedIndex > -1;
        }

        private static string convertKeysListToStringKeys(List<string> keys)
        {
            string modifiedSourceKeys = string.Empty;
            foreach (string key in keys)
            {
                modifiedSourceKeys += key + ",";
            }

            return modifiedSourceKeys;
        }

        private static string[] convertStringKeysToKeysArray(string keyInFormOfString)
        {
            return keyInFormOfString.Split(',');
        }

        private static bool areAllCriteriaMatchToUseThisResult(List<string> modifiedDestinationStringList, List<string> modifiedSourceStringList, int twiceMatching, double sourceKeyRemovedRatio)
        {
            return sourceKeyRemovedRatio > 0.6
                                && (twiceMatching > modifiedDestinationStringList.Count / 2)
                                && modifiedSourceStringList.Count > 5
                                && modifiedDestinationStringList.Count > 5;
        }

        private static int getTwoKeysCascadeMatchingCount(List<KeyMappingInfo> comparisonResult)
        {
            int twiceMatching = 0;
            for (int i = 0; i < comparisonResult.Count - 1; i++)
            {
                if (comparisonResult[i].DestinationMatchedIndex != -1
                    && comparisonResult[i + 1].DestinationMatchedIndex != -1
                    && ((comparisonResult[i].DestinationMatchedIndex + 1) == (comparisonResult[i + 1].DestinationMatchedIndex)
                    || (i != 0 && (comparisonResult[i - 1].DestinationMatchedIndex + 1) == (comparisonResult[i].DestinationMatchedIndex))))
                {
                    twiceMatching = twiceMatching + 1;
                }
            }

            return twiceMatching;
        }

        private static List<KeyMappingInfo> getKeysMappingResult(string[] sourceKeyList
            , string[] targetKeyList)
        {
            List<KeyObject> sourceKeyObjects = getKeyObjects(sourceKeyList);

            List<KeyObject> targetKeyObjects = getKeyObjects(targetKeyList);

            List<KeyMappingInfo> foundMappingResult = getMappingResultBasedOnTwoCascadeKeyMatch(sourceKeyObjects, targetKeyObjects);

            tryToMapSingleMatchKeyWhichFallsBetweenTwoCascadeKeysMapping(targetKeyObjects, foundMappingResult);

            return foundMappingResult;
        }

        private static void tryToMapSingleMatchKeyWhichFallsBetweenTwoCascadeKeysMapping(List<KeyObject> orgTargetKeyObjects
            , List<KeyMappingInfo> foundMappingResult)
        {
            bool isModified = false;
            do
            {
                isModified = false;
                for (int foundMappingResultIndex = 0; foundMappingResultIndex < foundMappingResult.Count; foundMappingResultIndex++)
                {
                    KeyMappingInfo currentValue = foundMappingResult[foundMappingResultIndex];
                    if (currentValue.DestinationMatchedIndex == -1)
                    {
                        if (foundMappingResultIndex > 0 && foundMappingResultIndex != (foundMappingResult.Count))
                        {
                            int prevIndex = foundMappingResult.Count;
                            KeyMappingInfo previousValue = foundMappingResult.FindLast(x => { prevIndex--; return (x.DestinationMatchedIndex != -1 && foundMappingResultIndex > (prevIndex)); });

                            int nextIndex = 0;
                            KeyMappingInfo nextValue = foundMappingResult.Find(x => { nextIndex++; return (x.DestinationMatchedIndex != -1 && foundMappingResultIndex < (nextIndex)); }); ;

                            if (previousValue != null && nextValue != null && (nextValue.DestinationMatchedIndex - previousValue.DestinationMatchedIndex) > 0)
                            {
                                int index = orgTargetKeyObjects.FindIndex(previousValue.DestinationMatchedIndex
                                    , (nextValue.DestinationMatchedIndex - previousValue.DestinationMatchedIndex)
                                    , x => x.Value == currentValue.KeyObject.Value);
                                if (index > -1)
                                {
                                    isModified = true;
                                    foundMappingResult[foundMappingResultIndex].DestinationMatchedIndex = index;
                                }
                            }
                            else
                            {
                                int previousDestinationIndex = previousValue != null ? previousValue.DestinationMatchedIndex : 0;
                                int nextDestinationIndex = nextValue != null ? nextValue.DestinationMatchedIndex : orgTargetKeyObjects.Count - 1;
                                if ((nextDestinationIndex - previousDestinationIndex) > 0)
                                {
                                    int index = orgTargetKeyObjects.FindIndex(previousDestinationIndex
                                      , (nextDestinationIndex - previousDestinationIndex)
                                      , x => x.Value == currentValue.KeyObject.Value);
                                    if (index > -1)
                                    {
                                        isModified = true;
                                        foundMappingResult[foundMappingResultIndex].DestinationMatchedIndex = index;
                                    }
                                }
                            }
                        }
                    }
                    if (isModified)
                    {
                        break;
                    }
                }
            } while (isModified);
        }

        private static List<KeyMappingInfo> getMappingResultBasedOnTwoCascadeKeyMatch(List<KeyObject> orgSourceKeyObjects, List<KeyObject> orgTargetKeyObjects)
        {
            List<KeyMappingInfo> foundMappingResult = new List<KeyMappingInfo>();

            for (int sourceKeyObjectIndex = 0; sourceKeyObjectIndex < orgSourceKeyObjects.Count; sourceKeyObjectIndex++)
            {
                KeyObject currentValue = orgSourceKeyObjects[sourceKeyObjectIndex];
                int index = orgTargetKeyObjects.FindIndex(x => x.Value == currentValue.Value);
                if (index > -1)
                {
                    if (orgTargetKeyObjects.Count > (index + 1) &&
                        orgSourceKeyObjects.Count > (sourceKeyObjectIndex + 1) &&
                        orgTargetKeyObjects[index + 1].Value == orgSourceKeyObjects[sourceKeyObjectIndex + 1].Value)
                    {
                        foundMappingResult.Add(new KeyMappingInfo() { DestinationMatchedIndex = index, KeyObject = currentValue });
                    }
                    else if ((index - 1) > -1 &&
                        (sourceKeyObjectIndex - 1) > -1 &&
                        orgTargetKeyObjects[index - 1].Value == orgSourceKeyObjects[sourceKeyObjectIndex - 1].Value)
                    {
                        foundMappingResult.Add(new KeyMappingInfo() { DestinationMatchedIndex = index, KeyObject = currentValue });
                    }
                    else
                    {
                        foundMappingResult.Add(new KeyMappingInfo() { DestinationMatchedIndex = -1, KeyObject = currentValue });
                    }
                }
                else
                {
                    foundMappingResult.Add(new KeyMappingInfo() { DestinationMatchedIndex = -1, KeyObject = currentValue });
                }
            }

            return foundMappingResult;
        }

        private static List<KeyObject> getKeyObjects(string[] keysList)
        {
            List<KeyObject> keyObjects = new List<KeyObject>();
            for (int i = 0; i < keysList.Length; i++)
            {
                keyObjects.Add(new KeyObject(i, keysList[i]));
            }

            return keyObjects;
        }

        private static void initializeSourceAndTargetKeys(string sourceKeysOrg, string targetKeysOrg, out string sourceKeys, out string targetKeys)
        {
            if (sourceKeysLengthIsSmallerThanTargerKeysLength(sourceKeysOrg, targetKeysOrg))
            {
                sourceKeys = targetKeysOrg;
                targetKeys = sourceKeysOrg;
            }
            else
            {
                sourceKeys = sourceKeysOrg;
                targetKeys = targetKeysOrg;
            }
        }

        private static bool sourceKeysLengthIsSmallerThanTargerKeysLength(string sourceKeys, string targetKeys)
        {
            return convertStringKeysToKeysArray(sourceKeys)
                .Length > convertStringKeysToKeysArray(targetKeys)
                .Length;
        }

        private static int LevenshteinDistance(string source, string target)
        {
            if (String.IsNullOrEmpty(source))
            {
                if (String.IsNullOrEmpty(target)) return 0;
                return target.Length;
            }
            if (String.IsNullOrEmpty(target)) return source.Length;
            target = target.Replace(" ", string.Empty);
            source = source.Replace(" ", string.Empty);

            string[] targetList = target.Split(',');
            string[] sourceList = source.Split(',');
            if (sourceList.Length > targetList.Length)
            {
                var temp = targetList;
                targetList = sourceList;
                sourceList = temp;
            }
            var m = targetList.Length;
            var n = sourceList.Length;
            var distance = new int[2, m + 1];

            for (var j = 1; j <= m; j++) distance[0, j] = j;

            var currentRow = 0;
            for (var i = 1; i <= n; ++i)
            {
                currentRow = i & 1;
                distance[currentRow, 0] = i;
                var previousRow = currentRow ^ 1;
                for (var j = 1; j <= m; j++)
                {
                    var cost = (targetList[j - 1] == sourceList[i - 1] ? 0 : 1);
                    distance[currentRow, j] = Math.Min(Math.Min(
                                distance[previousRow, j] + 1,
                                distance[currentRow, j - 1] + 1),
                                distance[previousRow, j - 1] + cost);
                }
            }
            return distance[currentRow, m];
        }

        private class KeyMappingInfo
        {
            public int DestinationMatchedIndex { get; set; }

            public KeyObject KeyObject { get; set; }
        }

        private class KeyObject
        {
            internal int Index { get; private set; }
            internal string Value { get; private set; }

            public KeyObject(int index, string value)
            {
                Index = index;
                Value = value;
            }
        }
    }
}