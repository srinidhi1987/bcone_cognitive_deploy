﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine;
using Automation.IQBot.Logger;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    internal class LayoutClassificationEngine
    {
        //TODO: make DLL name configurable
        private const string DllName = @"Automation.LayoutClassifier.dll";

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetLayoutClassificationId(string strDocFolderPath, string strConnectionString);

        [HandleProcessCorruptedStateExceptions]
        internal string classifyByLayout(string tempfilepath, string connectionString)
        {
            string layoutClassID = "-1";
            try
            {
                bool isPDFFile = PDFHelper.IsPdfFile(tempfilepath);
                if (isPDFFile)
                    tempfilepath = PDFHelper.GetPdfToImage(tempfilepath);

                int layoutClassid = -1;
                layoutClassid = GetLayoutClassificationId(tempfilepath, connectionString);
                layoutClassID = layoutClassid.ToString();
                if (isPDFFile)
                    System.IO.File.Delete(tempfilepath);
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Layout classification task crashed {ex.Message}", ex);
            }

            return layoutClassID;
        }
    }
}