﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Cognitive.DocumentClassifier.Executor;
using Automation.Cognitive.VisionBotEngine;
using Automation.IQBot.Logger;
using Automation.Model;
using Automation.Services.Client;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Common;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.NewCode;
using Automation.VisionBotEngine.OcrEngine;
using Automation.VisionBotEngine.PdfEngine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Text.RegularExpressions;

namespace Automation.DocumentClassifier
{
    internal class ClassificationEngineAPI
    {
        public readonly static string LAYOUT_CLASSIFICATION_EXE = "Automation.LayoutClassifier.exe";
        private const string UNCLASSIFICATION_ID = "-1";

        [HandleProcessCorruptedStateExceptions]
        internal void Classify(string organizationId
            , string projectId
            , string fileId
            , string fileName
            , LoggingConfigurations logingConfigurations)
        {
            try
            {
                FileInformation currFileInfo = new FileInformation();
                currFileInfo.fileid = fileId;
                currFileInfo.projectid = projectId;
                currFileInfo.filename = fileName;
                currFileInfo.orgId = organizationId;
                string tempFilePath = Path.Combine(Path.GetTempPath(), currFileInfo.fileid + Path.GetExtension(currFileInfo.filename));
                IQBotLogger.Debug($"FilePath: {tempFilePath}");

                IFileServiceEndPointConsumer fileService = new FileServiceEndPointConsumer(currFileInfo.orgId, currFileInfo.projectid);
                FileDetails fileDetails = fileService.GetDocumentDetails(currFileInfo.fileid);

                if (!string.IsNullOrEmpty(fileDetails.ClassificationId))
                {
                    IQBotLogger.Debug($"Skipping already classified document, Fileid:{currFileInfo.fileid}");
                    return;
                }

                if (!GetFileBlobAndWriteOnDisk(currFileInfo, tempFilePath, fileService))
                    return;

                //Below Commented lines are useeful for doing classification based on only firstpage of multipage document
                //bool isPDFFile = IsPdfFile(tempfilepath);
                //if (isPDFFile == true)
                //    tempfilepath = GetPdfToImage(tempfilepath);

                IQBotLogger.Trace($"[classify] [START LAYOUT CLASSIFICATION]");
                try
                {
                    //Call layoutclassifier exe.
                    //Wait till execution is complete.
                    logingConfigurations.tansactionID = currFileInfo.filename;
                    var response = LaunchLayoutClassificationProcess(tempFilePath, ClassifierConfiguration.DbConnectionString, logingConfigurations);
                    currFileInfo.layoutClassificationID = response.LayoutId;
                    IQBotLogger.Debug($"LayoutId: {response.LayoutId} and ExitCode: {response.LayoutClassificationProcessExitCode}]");
                    IQBotLogger.Trace("Layout classification compelted.");
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Exception during layout classification : {ex.Message}", ex);
                    currFileInfo.layoutClassificationID = "-1";
                }

                fileService.UpdateLayoutClassificationStatus(currFileInfo.orgId, currFileInfo.projectid, currFileInfo.fileid, currFileInfo.layoutClassificationID);

                IQBotLogger.Trace("Content classification started.");
                currFileInfo.foundFieldIDs = "";
                currFileInfo.foundAliases = "";
                IQBotLogger.Trace("Getting project metadata");
                currFileInfo.projectDetail = new DataProvider().GetProjectDetail(currFileInfo.orgId, currFileInfo.projectid);
                IQBotLogger.Trace("Received project metadata");
                List<FieldIdAndAliases> selectedFieldNAliases = new List<FieldIdAndAliases>();
                try
                {
                    DocumentProperties docProperties = initializeDocumentPropeties(currFileInfo, tempFilePath);

                    IAliasLib aliasLib = IAliasLibFactory.GetAliasLibObject();
                    aliasLib.SetFileInfo(currFileInfo);
                    var featureWords = aliasLib.GetFeatureWords();

                    selectedFieldNAliases = new DataProvider().GetSelectedFieldIdsAndAliasesOfCurrentProj(currFileInfo.orgId, currFileInfo.projectid);
                    IQBotLogger.Debug($"No Of selected fields: {selectedFieldNAliases.Count}");

                    int minNoOfFields = GetNoOfFieldsThreshold(selectedFieldNAliases.Count, currFileInfo.projectDetail.ProjectTypeId);
                    IQBotLogger.Debug($"Minimum no. of fields required to  classify: {minNoOfFields} for instance : {currFileInfo.projectid}");

                    ContentClassificationResult contentClassificationResult = classifyByContent(organizationId, projectId, docProperties, featureWords, minNoOfFields, currFileInfo,  tempFilePath);

                    currFileInfo.contentClassificationID = contentClassificationResult.ClassId;
                    currFileInfo.foundAliases = contentClassificationResult.FoundAliases;
                    currFileInfo.foundFieldIDs = contentClassificationResult.FoundFieldIds;

                    IQBotLogger.Debug($"Found aliases: {currFileInfo.foundAliases}]");
                    IQBotLogger.Trace("Content classification compelted.");
                }
                catch (Exception exc)
                {
                    IQBotLogger.Error($"Exception during content classification, moving document {fileId} to unclassified. Error: {exc.Message}", exc);
                   currFileInfo.contentClassificationID = "-1";
                }
                string report = string.Empty;
                try
                {
                    if (currFileInfo.contentClassificationID != "-1")
                    {
                        report = generateAnalysisReport(currFileInfo, selectedFieldNAliases);
                    }
                    IQBotLogger.Debug(report);
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Exception generating classification report, passing NULL report with ContentClassID: {currFileInfo.contentClassificationID}] : {ex.Message}", ex);
                    report = null;
                }

                IQBotLogger.Trace("Updating Classification Report");
                IQBotLogger.Debug($"Classification report: {report}");
                fileService.UpdateClassificationReport(currFileInfo.orgId, currFileInfo.projectid, currFileInfo.fileid, currFileInfo.layoutClassificationID, currFileInfo.contentClassificationID, report);
                IQBotLogger.Trace("Classification Report uploaded");                
                File.Delete(tempFilePath);
                IQBotLogger.Trace("Classification End");
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Error classifying document: {ex.Message}", ex);
            }
        }

        private static bool GetFileBlobAndWriteOnDisk(FileInformation currFileInfo, string tempFilePath, IFileServiceEndPointConsumer fileService)
        {
            try
            {
                VBotLogger.Trace(() => string.Format($"[GetFileBlobAndWriteOnDisk] [Getting File blobs from File Servie]"));
                byte[] fileBlob = fileService.GetDocument(currFileInfo.fileid);
                File.WriteAllBytes(tempFilePath, fileBlob);
                VBotLogger.Trace(() => string.Format($"[GetFileBlobAndWriteOnDisk] [Getting FileBlobs Succeed]"));
            }
            catch (Exception exc)
            {
                VBotLogger.Error(() => $"Error in getting the file blob and writing it on local disk, Moving document [{currFileInfo.fileid}] to unclassified. Error: {exc.Message}");
                ExceptionLoggingExtensions.Log(exc, () => string.Format($"[GetFileBlobAndWriteOnDisk]"));
                currFileInfo.contentClassificationID = "-1";
                currFileInfo.layoutClassificationID = "-1";
                string blank_report = string.Empty;
                fileService.UpdateClassificationReport(currFileInfo.orgId, currFileInfo.projectid, currFileInfo.fileid, currFileInfo.layoutClassificationID, currFileInfo.contentClassificationID, blank_report);
                return false;
            }
            return true;
        }


        private ContentClassificationResult classifyByContent(string organizationId, string projectId, DocumentProperties docProperties, string[] featureWords, int minNoOfFields, FileInformation currFileInfo, String tempFilePath)
        {
            ProcessingMode processingMode = (docProperties.Type.Equals(DocType.Pdf)) ? ProcessingMode.Pdf : ProcessingMode.Image;

            SegmentedDocumentCacheConfiguration segmentedDocumentCacheConfiguration = new SegmentedDocumentCacheConfiguration();
            SegmentedDocument segDoc = null;

            ContentClassificationResult contentClassificationResult = classifyByContent(organizationId
                , projectId, docProperties, featureWords, minNoOfFields, processingMode, segmentedDocumentCacheConfiguration, out segDoc);

            if (String.Equals(contentClassificationResult.ClassId, UNCLASSIFICATION_ID, StringComparison.OrdinalIgnoreCase) && docProperties.Type.Equals(DocType.Pdf) && segDoc.processingMode.Equals(ProcessingMode.Pdf))
            {
                IQBotLogger.Debug($"PDF_RECLASSIFY: Document :'{docProperties.Name}' is failed to classify, sending it again for classification as a Image");
               DocumentProperties docPropertiesImage= initializeDocumentPropeties(currFileInfo, tempFilePath);

                processingMode = ProcessingMode.Image;
                segmentedDocumentCacheConfiguration.IsReadFromCache = false;

                contentClassificationResult = classifyByContent(organizationId
                , projectId, docPropertiesImage, featureWords, minNoOfFields, processingMode, segmentedDocumentCacheConfiguration, out segDoc);
            }
            return contentClassificationResult;
        }

        private ContentClassificationResult classifyByContent(string organizationId, string projectId, DocumentProperties docProperties, string[] featureWords, 
            int minNoOfFields, ProcessingMode processingMode, SegmentedDocumentCacheConfiguration segmentedDocumentCacheConfiguration, out SegmentedDocument segDoc)
        {
            IQBotLogger.Debug($"OCR Started, Document='{docProperties.Name}'; type='{docProperties.Type}'");
            VisionBotEngineAPI visionBotEngineAPI = new VisionBotEngineAPI();
            segDoc = visionBotEngineAPI.GetDocumentVectorDetails(organizationId
                , projectId, docProperties, processingMode, segmentedDocumentCacheConfiguration);
            IQBotLogger.Debug($"OCR Completed, Document='{docProperties.Name}'; type='{docProperties.Type}'");

            VBotLogger.Trace(() => string.Format($"[classifyByContent Start] [Document='{docProperties.Name}'; type='{docProperties.Type}'"));
            var contentClassificationResult = new ContentClassificationEngine()
                .classifyByContent(featureWords
                , new DocumentVectorDetails(docProperties, segDoc)
                , minNoOfFields);

            IQBotLogger.Debug($"Document='{docProperties.Name}'; type='{docProperties.Type}'; mode='{processingMode}'; classification result='{contentClassificationResult.ClassId}'");
            IQBotLogger.Trace("Completed");
            return contentClassificationResult;
        }
        private DocumentProperties initializeDocumentPropeties(FileInformation currFileInfo, string tempFilePath)
        {
            bool isPdfDocument = CommonMethods.IsPdfFile(tempFilePath);
            DocumentProperties docProperties = new DocumentProperties()
            {
                Id = currFileInfo.fileid,
                Name = currFileInfo.filename,
                Path = tempFilePath,
                Type = isPdfDocument ? DocType.Pdf : DocType.Image
            };
            return docProperties;
        }

        private const string CustomProjectTypeId = "0";
        private const int MinFieldThreshold = 4;
        private const int MaxFieldThreshold = 6;

        private int GetNoOfFieldsThreshold(int noOfSelectedFields, string projectTypeID)
        {
            if (string.Equals(projectTypeID, CustomProjectTypeId))
                return 1;
            else if (noOfSelectedFields <= MinFieldThreshold)
                return MinFieldThreshold;
            else if (noOfSelectedFields >= MaxFieldThreshold)
                return MaxFieldThreshold;
            else
                return noOfSelectedFields;
        }

        private string generateAnalysisReport(FileInformation currFileInfo, List<FieldIdAndAliases> selectedFieldNAliases)
        {
            IQBotLogger.Trace("Started");
            Dictionary<string, object> message = new Dictionary<string, object>();

            List<classificationResult> foundFieldDetail = new List<classificationResult>();

            message.Add("documentId", currFileInfo.fileid);

            string[] Aliases = currFileInfo.foundAliases.Split(',');
            foreach (FieldIdAndAliases fieldIDAliases in selectedFieldNAliases)
            {
                classificationResult currFieldResult = new classificationResult();
                currFieldResult.fieldId = fieldIDAliases.fieldId;
                currFieldResult.isFound = false;

                var aliasFound = fieldIDAliases.aliases.Intersect(Aliases);
                if (aliasFound.Count() > 0)
                {
                    currFieldResult.aliasName = aliasFound.ElementAt(0);
                    currFieldResult.isFound = true;
                }

                foundFieldDetail.Add(currFieldResult);
            }

            message.Add("fieldDetail", foundFieldDetail);

            IQBotLogger.Trace("Completed");
            return JsonConvert.SerializeObject(message);
        }

        public string getTessDataPath()
        {
            string tessDataPath = AppDomain.CurrentDomain.BaseDirectory + @"tessdata";
            return tessDataPath;
        }

        public LayoutClassificationResponse LaunchLayoutClassificationProcess(string filePath
                                                                            , string connectionString
                                                                            , LoggingConfigurations loggingConfigurations)
        {
            StringBuilder outputBuilder = new StringBuilder();
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.CreateNoWindow = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.UseShellExecute = false;
            bool isPDFFile = PDFHelper.IsPdfFile(filePath);
            if (isPDFFile)
                filePath = PDFHelper.GetPdfToImage(filePath);

            string logFilePath = Path.Combine(Path.GetDirectoryName(loggingConfigurations.logFilePath), "Layout-ClasssifierLog.log");
            processStartInfo.FileName = System.IO.Path.Combine(LAYOUT_CLASSIFICATION_EXE);
            string arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"",
                                            filePath
                                            , connectionString
                                            , logFilePath
                                            , loggingConfigurations.loggerSettings.level
                                            , loggingConfigurations.tansactionID);

            processStartInfo.Arguments = arguments;
            var process = new Process();
            process.StartInfo = processStartInfo;
            process.EnableRaisingEvents = true;
            process.OutputDataReceived += new DataReceivedEventHandler(
                                          delegate (object sender, DataReceivedEventArgs e)
                                          {
                                              outputBuilder.Append(e.Data);
                                          });
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
            process.CancelOutputRead();
            string response = outputBuilder.ToString();
            if (isPDFFile)
                System.IO.File.Delete(filePath);

            IQBotLogger.Debug($"{response}");
            return
                new LayoutClassificationResponse()
                {
                    LayoutClassificationProcessExitCode = process.ExitCode,
                    LayoutId = GetLayoutClassIDFromResponse(response)
                };
        }

        public string GetLayoutClassIDFromResponse(string response)
        {
            Match match = Regex.Match(response, @"LAYOUTCLASS:(.*):LAYOUTCLASS",
               RegexOptions.IgnoreCase);

            string layoutclassID = "-1";
            if (match.Success)
            {
                layoutclassID = match.Groups[1].Value;
            }
            else
            {
                IQBotLogger.Error($"Error in parsing the response : {response}, returning -1");
            }

            return layoutclassID;
        }
    }

    public class LayoutClassificationResponse
    {
        public string LayoutId { get; set; }
        public int LayoutClassificationProcessExitCode { get; set; }
    }
}