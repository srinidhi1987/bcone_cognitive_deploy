﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Cognitive.DocumentClassifier.Executor;
using Automation.IQBot.Logger;
using Automation.Cognitive.VisionBotEngine.Model.V2.Project;
using Automation.Model;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Automation.DocumentClassifier
{
    internal interface IAliasLib
    {
        void SetFileInfo(FileInformation fileInfo);

        string[] GetFeatureWords();
    }

    internal class IAliasLibFactory
    {
        public static IAliasLib GetAliasLibObject()
        {
            return new ServiceAlias();
        }
    }

    internal class CsvAlias : IAliasLib
    {
        public void SetFileInfo(FileInformation fileInfo)
        {
        }

        public string[] GetFeatureWords()
        {
            string featureWordFile = AppDomain.CurrentDomain.BaseDirectory + @"\FeatureWords.csv";
            var featureWords = File.ReadAllLines(featureWordFile);
            return featureWords;
        }
    }

    internal class ServiceAlias : IAliasLib
    {
        private FileInformation currfileInfo;

        public void SetFileInfo(FileInformation fileInfo)
        {
            currfileInfo = fileInfo;
        }

        public string[] GetFeatureWords()
        {
            IQBotLogger.Trace("Started");
            DataProvider fieldInfo = new DataProvider();
            List<FieldIdAndAliases> fieldIdAndAliseslist = new List<FieldIdAndAliases>();
            if (currfileInfo.projectDetail != null && !"0".Equals(currfileInfo.projectDetail.ProjectTypeId))
                fieldIdAndAliseslist = fieldInfo.GetAliasesForCurrentProj(currfileInfo.projectid, currfileInfo.projectDetail.PrimaryLanguage, currfileInfo.projectDetail.ProjectTypeId);
            else if (currfileInfo.projectDetail != null && "0".Equals(currfileInfo.projectDetail.ProjectTypeId))
            {
                IQBotLogger.Debug($"ProjectType is other domain for ProjectId:{currfileInfo.projectDetail.Id}");
            }
            else
                IQBotLogger.Error("Project details not found");

            Dictionary<string, string> featureWordsDict = new Dictionary<string, string>();
            foreach (FieldIdAndAliases idAndAliases in fieldIdAndAliseslist)
            {
                string guid = idAndAliases.fieldId;
                foreach (string alias in idAndAliases.aliases)
                    featureWordsDict[alias] = guid;
            }
            foreach (CustomFieldV2 curCustomField in currfileInfo.projectDetail?.Fields?.Custom.Where(c => c.IsAddedLater == false).ToList() ?? new List<CustomFieldV2>())
            {
                featureWordsDict[curCustomField.Name] = curCustomField.Id;
            }

            var featureWords = featureWordsDict.Select(item => string.Format("{0},{1}", item.Key, item.Value));
            IQBotLogger.Trace("Completed");
            return featureWords.ToArray();
        }
    }

    internal class ContentClassification
    {
        private string extractDataFromDocument(string[] featureWords
            , DocumentVectorDetails documentVectorDetails
            , ref string foundAliases)
        {
            if (featureWords.Count() <= 0)
            {
                IQBotLogger.Error($"Feature words not found, document cannot be classified");
                return "";
            }

            string extracteddata = new Automation.VisionBotEngine.DataExtraction()
              .ExtractDataFromDocument(featureWords
              , documentVectorDetails
              , ""
              , true
              , ref foundAliases);

            IQBotLogger.Trace("Completed");
            return extracteddata;

        }

        private const string UNCLASSIFICATION_ID = "-1";
        public ContentClassificationResult GetClassificationID(string[] featureWords
            , DocumentVectorDetails documentVectorDetails
            , int minNoOfFields)
        {
            string foundAliases = string.Empty;
            string classid = UNCLASSIFICATION_ID;
            IQBotLogger.Trace("Started");
            var foundFieldIDs = extractDataFromDocument(featureWords, documentVectorDetails, ref foundAliases);
            IQBotLogger.Debug($"Field ids: {foundFieldIDs}");

            if (string.IsNullOrEmpty(foundFieldIDs))
            {
                IQBotLogger.Error("Fields not found, document can not be classified");
                return new ContentClassificationResult(UNCLASSIFICATION_ID
                , foundAliases, foundFieldIDs);
            }
            else if (foundFieldIDs.Split(',').Length < minNoOfFields)
            {
                IQBotLogger.Error($"No. of fields found is les than {minNoOfFields}, document cannot be classified");
                return new ContentClassificationResult(UNCLASSIFICATION_ID
                , foundAliases, foundFieldIDs);
            }
            string noOfFieldsFound = foundFieldIDs;
            IQBotLogger.Debug($"Fields found: {noOfFieldsFound}");

            try
            {
                 classid = getClassId(foundFieldIDs);
            }
            catch (Exception ex)
            {
                classid = UNCLASSIFICATION_ID;
                IQBotLogger.Error($"[ContentClassification::GetClassificationID] Exception Occurred in Getting matching percentage with Existing Groups after OCR the document, so returning -1 {ex.Message}");
                ExceptionLoggingExtensions.Log(ex, () => string.Format($"[DbDataAccess::ReadTrainData]"));
            }
            IQBotLogger.Trace($"[ContentClassification::GetClassificationID] [End...]");
            ContentClassificationResult result = new ContentClassificationResult(classid.ToString()
                , foundAliases, foundFieldIDs);
            return result;
        }

        private static string getClassId(string foundFieldIDs)
        {
            string classid = UNCLASSIFICATION_ID;
            do
            {
                int noOfClassesInDB = 0;
                string classificationResult = Classification.ClassifyDocument(foundFieldIDs, ref noOfClassesInDB);
                if (classificationResult == "")
                {
                    if (foundFieldIDs != "")
                    {
                        int indexFile = 0;
                        classid = DataAccessFactory.GetDataAccessObject()
                            .AddNewTrainData(foundFieldIDs, ref indexFile, noOfClassesInDB)
                            .ToString();
                    }
                }
                else
                {
                    string[] CLASS = classificationResult.Split('-');
                    classid = CLASS[1];
                }

            } while (classid == "-2");

            return classid;
        }
    }
}