﻿

using Automation.Cognitive.VisionBotEngine.Model.V2.Project;
/**
* Copyright (c) 2017 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
using Automation.Model;
using Automation.Services.Client;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Automation.IQBot.Logger;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    internal class DataProvider
    {
        public List<FieldIdAndAliases> GetSelectedFieldIdsAndAliasesOfCurrentProj(
                        string orgId,
                        string projectId)
        {
            IQBotLogger.Trace("Started");
            ProjectDetailsV2 projectDetail = GetProjectDetail(orgId, projectId);
            List<FieldIdAndAliases> allFieldAndAliases = getSelectedFieldAliases(projectDetail);
            IQBotLogger.Trace("Completed");
            return allFieldAndAliases;
        }

        public List<FieldIdAndAliases> GetAliasesForCurrentProj(
            string projectId,
            string primaryLanguage,
            string projectType)
        {
            List<FieldIdAndAliases> fieldIdAndAliases = new List<FieldIdAndAliases>();
            try
            {
                IQBotLogger.Trace("Started");
                IAliasServiceEndPointConsumer aliasService = new AliasServiceEndPointConsumer();
                AliasLanguageDetail languageDetail = aliasService.GetLanguageDetail(primaryLanguage);
                string languageId = "1";
                if (languageDetail != null)
                    languageId = languageDetail.Id;
                else
                    IQBotLogger.Error($"Language details not found for language: {primaryLanguage}");
                if (!"0".Equals(projectType))
                {
                    AliasDetailBasedOnProjectTypeAndLanguage aliasesDetail = aliasService.GetAliasesBasedOnProjectTypeAndLanguageId(projectType, languageId);
                    if (aliasesDetail != null)
                    {
                        foreach (AliasDetail curaliasDetail in aliasesDetail?.fields)
                        {
                            FieldIdAndAliases currFieldIdAndAliases = new FieldIdAndAliases();
                            currFieldIdAndAliases.aliases = curaliasDetail.aliases;
                            currFieldIdAndAliases.fieldId = curaliasDetail.id;

                            fieldIdAndAliases.Add(currFieldIdAndAliases);
                        }
                    }
                    else
                        IQBotLogger.Error($"Alias detail not found for ProjectType: {projectType} And LabguageID: {languageId}]");
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception($"Error in FieldInfo::GetAliasesForCurrentProj {ex.Message}", ex);
            }
            IQBotLogger.Trace("Completed");
            return fieldIdAndAliases;
        }

        private List<FieldIdAndAliases> getSelectedFieldAliases(
            ProjectDetailsV2 projectDetail)
        {
            IQBotLogger.Trace("Started");
            List<FieldIdAndAliases> selectedFieldAndAliases = new List<FieldIdAndAliases>();
            List<FieldIdAndAliases> allFieldAndAliases = new List<FieldIdAndAliases>();
            if (projectDetail != null)
            {
                allFieldAndAliases = GetAliasesForCurrentProj(projectDetail.Id, projectDetail.PrimaryLanguage, projectDetail.ProjectTypeId);
                foreach (StandardFieldV2 standardField in projectDetail.Fields?.Standard.Where(f => f.IsAddedLater == false).ToList())
                {
                    try
                    {
                        FieldIdAndAliases currFieldIdAndAliases = new FieldIdAndAliases();
                        List<String> aliases = new List<string>();
                        IQBotLogger.Trace($"[FieldInfo::getSelectedFieldAliases] [Standard FieldID: {standardField.Id}]");
                        aliases = allFieldAndAliases.FirstOrDefault(x => x.fieldId == standardField.Id).aliases;
                        if (aliases == null || aliases.Count <= 0)
                            IQBotLogger.Error($"[FieldInfo::getSelectedFieldAliases] [Aliases Not found for Field: {standardField.Id}]");
                        currFieldIdAndAliases.aliases = aliases;// getAliasesForCurrentFieldId(fieldId, projectDetail.PrimaryLanguage);
                        currFieldIdAndAliases.fieldId = standardField.Id;
                        selectedFieldAndAliases.Add(currFieldIdAndAliases);
                        IQBotLogger.Debug($"Standard FieldID: {standardField.Id} added]");
                    }
                    catch (Exception ex)
                    {
                        throw new System.Exception($"Error getting feild alias {ex.Message}");
                    }
                }
                foreach (CustomFieldV2 customField in projectDetail.Fields?.Custom.Where(c => c.IsAddedLater == false).ToList())
                {
                    IQBotLogger.Trace($"[FieldInfo::getSelectedFieldAliases] [Custom FieldID: {customField.Id}]");
                    FieldIdAndAliases currFieldIdAndAliases = new FieldIdAndAliases();
                    currFieldIdAndAliases.aliases = new List<string>();
                    currFieldIdAndAliases.aliases.Add(customField.Name);
                    currFieldIdAndAliases.fieldId = customField.Id;
                    selectedFieldAndAliases.Add(currFieldIdAndAliases);
                    IQBotLogger.Debug($"Custom FieldID: {customField.Id}");
                }
            }
            else
                IQBotLogger.Error("Project detial not found, Aliases cannot be found");

            IQBotLogger.Trace("Completed");
            return selectedFieldAndAliases;
        }

        public ProjectDetailsV2 GetProjectDetail(string orgId, string projectId)
        {
            IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
            ProjectDetailsV2 projectDetail = projectService.GetProjectMetaDataV2(orgId, projectId);
            if (projectDetail == null)
                throw new System.Exception("Failed to retrive project metadata");
            return projectDetail;
        }

        public AliasLanguageDetail GetLanguageDetail(string languageId)
        {
            IAliasServiceEndPointConsumer aliasService = new AliasServiceEndPointConsumer();
            AliasLanguageDetail languageDetail = aliasService.GetLanguageDetail(languageId);
            if (languageDetail == null)
                throw new System.Exception("Failed to retrive languageDetial from alias service");
            return languageDetail;
        }
    }
}