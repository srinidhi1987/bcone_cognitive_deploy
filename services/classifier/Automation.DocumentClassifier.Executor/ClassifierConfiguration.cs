﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Generic.Configuration;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    internal class ClassifierConfiguration
    {
        private static readonly ConfigurationHelper _configHelper = new ConfigurationHelper(new JsonConfigManager());

        private const string _defaultDBConnectionString = "";
        private static string _dBConnectionString;

        public static string DbConnectionString
        {
            get
            {
                if (_dBConnectionString != "")
                {
                    _dBConnectionString = _configHelper.GetParsedValue(
                        nameof(DbConnectionString),
                        _defaultDBConnectionString,
                        ConfigurationHelper.ParseString);
                }
                return _dBConnectionString;
            }
        }

        private const string _defaultAliasServiceBaseURL = "http://localhost:9997/";
        private static string _aliasServiceBaseURL;

        public static string AliasServiceBaseURL
        {
            get
            {
                if (_aliasServiceBaseURL != "")
                {
                    _aliasServiceBaseURL = _configHelper.GetParsedValue(
                        nameof(AliasServiceBaseURL),
                        _defaultAliasServiceBaseURL,
                        ConfigurationHelper.ParseString);
                }
                return _aliasServiceBaseURL;
            }
        }

        private const string _defaultProjectServiceBaseURL = "http://localhost:9999/";
        private static string _projectServiceBaseURL;

        public static string ProjectServiceBaseURL
        {
            get
            {
                if (_projectServiceBaseURL != "")
                {
                    _projectServiceBaseURL = _configHelper.GetParsedValue(
                        nameof(ProjectServiceBaseURL),
                        _defaultProjectServiceBaseURL,
                        ConfigurationHelper.ParseString);
                }
                return _projectServiceBaseURL;
            }
        }

        private const string _defaultClassifiedMQName = "QUEUE_Visionbot_ProcessDocument";
        private static string _classifiedmqName;

        public static string ClassifiedMQName
        {
            get
            {
                if (_classifiedmqName != "")
                {
                    _classifiedmqName = _configHelper.GetParsedValue(
                        nameof(ClassifiedMQName),
                        _defaultClassifiedMQName,
                        ConfigurationHelper.ParseString);
                }
                return _classifiedmqName;
            }
        }

        private const string _defaultmqPassword = "";
        private static string _mqPassword;

        public static string Password
        {
            get
            {
                if (_mqPassword != "")
                {
                    _mqPassword = _configHelper.GetParsedValue(
                        nameof(Password),
                        _defaultmqPassword,
                        ConfigurationHelper.ParseString);
                }
                return _mqPassword;
            }
        }

        private const string _defaultmqUserName = "";
        private static string _mqUserName;

        public static string UserName
        {
            get
            {
                if (_mqUserName != "")
                {
                    _mqUserName = _configHelper.GetParsedValue(
                        nameof(UserName),
                        _defaultmqUserName,
                        ConfigurationHelper.ParseString);
                }
                return _mqUserName;
            }
        }

        private const string _defaultmqVirtualHost = "test";
        private static string _mqVirtualHost;

        public static string VirtualHost
        {
            get
            {
                if (_mqVirtualHost != "")
                {
                    _mqVirtualHost = _configHelper.GetParsedValue(
                        nameof(VirtualHost),
                        _defaultmqVirtualHost,
                        ConfigurationHelper.ParseString);
                }
                return _mqVirtualHost;
            }
        }

        private const string _defaultFileUploadedMQName = "topic_classification";
        private static string _fileUploadedMQName;

        public static string FileUploadedMQName
        {
            get
            {
                if (_fileUploadedMQName != "")
                {
                    _fileUploadedMQName = _configHelper.GetParsedValue(
                        nameof(FileUploadedMQName),
                        _defaultFileUploadedMQName,
                        ConfigurationHelper.ParseString);
                }
                return _fileUploadedMQName;
            }
        }

        private const string _defaultFileServiceBaseURL = "http://localhost:9996/";
        private static string _fileServiceBaseURL;

        public static string FileServiceBaseURL
        {
            get
            {
                if (_fileServiceBaseURL != "")
                {
                    _fileServiceBaseURL = _configHelper.GetParsedValue(
                        nameof(FileServiceBaseURL),
                        _defaultFileServiceBaseURL,
                        ConfigurationHelper.ParseString);
                }
                return _fileServiceBaseURL;
            }
        }

        private const string _defaultMQServer = "localhost";
        private static string _mqServer;

        public static string Uri
        {
            get
            {
                if (_mqServer != "")
                {
                    _mqServer = _configHelper.GetParsedValue(
                        nameof(Uri),
                        _defaultMQServer,
                        ConfigurationHelper.ParseString);
                }
                return _mqServer;
            }
        }
    }
}