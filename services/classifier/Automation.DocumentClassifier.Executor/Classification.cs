﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using Automation.Cognitive.DocumentClassifier.Executor;
using Automation.IQBot.Logger;
using Automation.VisionBotEngine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace Automation.DocumentClassifier
{
    internal interface IDataAccess
    {
        void ReadTrainData(List<string> trainings, List<string> classes);

        long AddNewTrainData(string output, ref int indexFile, int prevCount);
    }

    internal class DataAccessFactory
    {
        public static IDataAccess GetDataAccessObject()
        {
            return new DbDataAccess();
        }
    }

    internal class CsvDataAccess : IDataAccess
    {
        private string FileName
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory + "\\trainSet.csv";
            }
        }

        public void ReadTrainData( List<string> listA,  List<string> listB)
        {
            var reader = new StreamReader(File.Open(this.FileName, FileMode.Open));

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line != "")
                {
                    var values = line.Split(';');

                    listA.Add(values[0]);
                    listB.Add(values[1]);
                }
            }
            reader.Dispose();
        }

        public long AddNewTrainData(string output, ref int indexFile, int prevCount)
        {
            List<string> listTrainSetFeature = new List<string>();
            List<string> listTrainSetClass = new List<string>();
            ReadTrainData( listTrainSetFeature,  listTrainSetClass);
            string className = listTrainSetClass[listTrainSetClass.Count - 1];
            int className1 = Int32.Parse(className) + 1;
            output += ";" + className1.ToString();
            System.IO.StreamWriter file = new System.IO.StreamWriter(this.FileName, true);
            if (indexFile == 0)
            {
                file.WriteLine(Environment.NewLine);
                indexFile++;
            }
            file.WriteLine(output);
            file.Flush();
            file.Dispose();

            return className1;
        }
    }

    internal class DbDataAccess : IDataAccess
    {
        private int DB_ACCESS_RETRY_COUNT = 5;
        private int RETRY_INTERVAL = 1000;//in ms
        private string ConnectionString = null;

        public DbDataAccess()
        {
            this.ConnectionString = ClassifierConfiguration.DbConnectionString;
        }

        // trainset: id, training

        public void ReadTrainData( List<string> trainings,  List<string> classes)
        {
            DataTable result = new DataTable();
            IQBotLogger.Trace("Started");
            string CommandText = "SELECT id, training FROM ContentTrainset";
            try
            {
                handleActionWithMultipleTry(() =>
                {
                    using (SqlConnection myConnection = new SqlConnection(ConnectionString))
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter(CommandText, myConnection))
                        {
                            da.Fill(result);
                        }
                    }
                    foreach (DataRow dr in result.Rows)
                    {
                        classes.Add(dr[0].ToString());
                        trainings.Add(dr[1].ToString());
                    }
                    IQBotLogger.Trace("Completed");
                }, "DbDataAccess::ReadTrainData", DB_ACCESS_RETRY_COUNT, RETRY_INTERVAL);
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Tried {DB_ACCESS_RETRY_COUNT} times to execute command [{CommandText}], but it failed", ex);
                throw;
            }
        }

        void handleActionWithMultipleTry(Action action, string ActionName, int noOfTry, int intervalBetweenTryInMilliSeconds = 0)
        {
            int tryCount = 1;
            do
            {
                try
                {
                    action.Invoke();
                    return;
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Error processing [{ActionName}], try count:{tryCount}.", ex);
                    if (tryCount >= noOfTry)
                        throw;
                }
                tryCount++;
                if (intervalBetweenTryInMilliSeconds > 0)
                {
                    Thread.Sleep(intervalBetweenTryInMilliSeconds);
                }
            } while (tryCount <= noOfTry);
        }

        public long AddNewTrainData(string output, ref int indexFile, int prevCount)
        {
            IQBotLogger.Trace("Started");
            string CommandText = "Insert into ContentTrainset (training) Select (@training) FROM ContentTrainSet c HAVING COUNT(c.id) = @t0;SELECT SCOPE_IDENTITY()";
            try
            {
                IQBotLogger.Debug($"Command: {CommandText}");
                long result=-1;
                handleActionWithMultipleTry(() =>
                {
                    using (SqlConnection myConnection = new SqlConnection(ConnectionString))
                    {
                        myConnection.Open();

                        SqlCommand cmd = myConnection.CreateCommand();
                        cmd.CommandText = CommandText;
                        cmd.Parameters.AddWithValue("@training", output);
                        cmd.Parameters.AddWithValue("@t0", prevCount);
                        var insertSuccessful = cmd.ExecuteScalar();
                        if (string.IsNullOrEmpty(insertSuccessful.ToString()))
                        {
                            IQBotLogger.Info($"[DbDataAccess::AddNewTrainData] Inserting the training is failed, New training might be inserted need to fetch those first");
                            result = -2;
                        }
                        else
                        {
                            IQBotLogger.Trace($"[DbDataAccess::AddNewTrainData] [End...]");
                            result = Convert.ToInt64(insertSuccessful);
                        }
                    }
                }, "DbDataAccess::AddNewTrainData", DB_ACCESS_RETRY_COUNT, RETRY_INTERVAL);

                return result;
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"[DbDataAccess::AddNewTrainData]Tried {DB_ACCESS_RETRY_COUNT} times to execute command [{CommandText}], but it failed");
                IQBotLogger.Error(ex.Message);
                return -1;
            }
        }
    }

    internal class Classification
    {
        protected Classification() { }
        private const double CONTENT_BASED_CLASS_MATCH_THRESHOLD = 0.65;

        static public string ClassifyDocument(string data, ref int noOfClassesInDB)
        {
            IQBotLogger.Trace("Started");

            List<string> listTrainSetFeature = new List<string>();
            List<string> listTrainSetClass = new List<string>();

            DataAccessFactory.GetDataAccessObject().ReadTrainData(listTrainSetFeature, listTrainSetClass);
            noOfClassesInDB = listTrainSetClass.Count;
            string classificationResult = string.Empty;
            IQBotLogger.Trace("Calculating matching percenatge");
            List<double> matchingPercentage = new List<double>();
            foreach (string list1 in listTrainSetFeature)
            {
                double matchPercentageResult = ClassKeyComparator.GetMatchPercentage(data, list1);
                matchingPercentage.Add(matchPercentageResult);
                if (matchPercentageResult >= 0.999)
                {
                    break;
                }
            }

            int index = GetBestMatchClass(matchingPercentage);
            IQBotLogger.Debug($"Matching percentage: {CONTENT_BASED_CLASS_MATCH_THRESHOLD}");
            if (matchingPercentage.Count > 0
                && (matchingPercentage[index] > CONTENT_BASED_CLASS_MATCH_THRESHOLD))
            {
                IQBotLogger.Debug($"Best matching percentage: {matchingPercentage[index] * 100}");
                classificationResult += "Classified  -" + listTrainSetClass[index] + "-" + (matchingPercentage[index] * 100).ToString() + Environment.NewLine;
            }
            else
                IQBotLogger.Trace("Best matching class not found, adding new class");

            IQBotLogger.Debug($"Classification result: {classificationResult}]");
            IQBotLogger.Trace("Completed");
            return classificationResult;
        }

        static private int GetBestMatchClass(List<double> matchingPercentage)
        {
            double max = matchingPercentage.Count > 0 ? matchingPercentage[0] : 0;
            int Index = 0;

            for (int i = 1; i < matchingPercentage.Count; ++i)
            {
                if (matchingPercentage[i] > max)
                {
                    max = matchingPercentage[i];
                    Index = i;
                }
            }
            return Index;
        }
    }
}