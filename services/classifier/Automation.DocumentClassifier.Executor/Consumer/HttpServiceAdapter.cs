/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Consumer
{
    using Newtonsoft.Json;
    using System;
    using System.Net.Http;

    public class HttpServiceAdapter
    {
        private HttpClientService _client = null;

        public HttpServiceAdapter(string serviceUrl)
        {
            HttpServiceModel serviceModel = new HttpServiceModel()
            { /*ServiceUri = "http://192.168.2.59:9999" */
                ServiceUri = serviceUrl /*"http://172.16.16.125:9996"*/
            };

            _client = new HttpClientService(serviceModel);
        }

        public T Get<T>(string subUrl)
        {
            try
            {
                var response = _client.Get(subUrl);

                var jsonString = response.Content.ReadAsStringAsync().Result;
                VisionbotServiceResponse visionbotServiceResponse = JsonConvert.DeserializeObject(jsonString, typeof(VisionbotServiceResponse)) as VisionbotServiceResponse;
                if (visionbotServiceResponse == null)
                    return default(T);
                if (visionbotServiceResponse.data == null)
                    return default(T);
                else
                    return JsonConvert.DeserializeObject<T>(visionbotServiceResponse.data.ToString());
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public T Post<T, C>(string subUrl, C content)
        {
            try
            {
                HttpResponseMessage response;
                if (content != null)
                {
                    var jsonString = JsonConvert.SerializeObject(content);
                    response = _client.Post(subUrl, jsonString);
                }
                else
                    response = _client.Post(subUrl, "");
                var jsonResultString = response.Content.ReadAsStringAsync().Result;

                VisionbotServiceResponse visionbotServiceResponse = JsonConvert.DeserializeObject(jsonResultString, typeof(VisionbotServiceResponse)) as VisionbotServiceResponse;
                if (visionbotServiceResponse == null)
                    return default(T);
                if (visionbotServiceResponse.data == null)
                    return default(T);
                else
                    return JsonConvert.DeserializeObject<T>(visionbotServiceResponse.data.ToString());
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
    }
}