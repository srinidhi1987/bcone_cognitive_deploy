﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.IO;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    internal class PDFHelper
    {
        internal static bool IsPdfFile(string filepath)
        {
            return Path.GetExtension(filepath).ToLower().Equals(".pdf");
        }

        internal static string GetPdfToImage(string pdfFilePath)
        {
            return new Automation.VisionBotEngine.DataExtraction().GetPdfToImageFile(pdfFilePath);
        }
    }
}
