﻿/**
* Copyright (c) 2018 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
using Automation.VisionBotEngine.Model;
using Automation.Cognitive.VisionBotEngine.Model.V2.Project;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    public struct FileInformation
    {
        public string orgId;
        public string projectid;
        public string fileid;
        public string foundFieldIDs;
        public string foundAliases;
        public string layoutClassificationID;
        public string contentClassificationID;
        public string filename;
        public ProjectDetailsV2 projectDetail;
    }
}
