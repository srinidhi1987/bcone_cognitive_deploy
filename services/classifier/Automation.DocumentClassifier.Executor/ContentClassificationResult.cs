﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.DocumentClassifier.Executor
{
    internal class ContentClassificationResult
    {
        public string ClassId { get; private set; }
        public string FoundAliases { get; private set; }
        public string FoundFieldIds { get; private set; }
        public ContentClassificationResult(string classId
            , string foundAliases
            , string foundFieldIds)
        {
            ClassId = classId;
            FoundAliases = foundAliases;
            FoundFieldIds = foundFieldIds;
        }
    }
}
