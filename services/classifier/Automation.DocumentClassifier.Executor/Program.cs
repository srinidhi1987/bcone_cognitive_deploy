﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.IO;
using System.Reflection;
using Automation.Cognitive.VisionBotEngine.Model.ExecutorOperations;
using Automation.Services.Client;
using Automation.VisionBotEngine;
using CommandLine;
using System.Runtime.ExceptionServices;
using Automation.IQBot.Logger;

namespace Automation.DocumentClassifier
{
    public class LoggingConfigurations
    {
        public string logFilePath;
        public LoggerSettings loggerSettings;
        public string tansactionID;
    }

    internal class Program
    {
        private static void initializingLoggingMechanism(LoggingConfigurations loggingConfiguration)
        {
            VBotLogger.Initlization(loggingConfiguration.logFilePath, loggingConfiguration.loggerSettings);
        }

        private static LoggingConfigurations GetLoggingConfigurations(string[] args)
        {
            
            string logDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\Automation Anywhere IQBot Platform\\Logs\\";
            string servicConfigurationPath = "CognitiveServiceConfiguration.json";
            string path = Path.Combine(
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
            string json = File.ReadAllText(path);

            CognitiveServicesConfiguration _cognitiveServicesConfiguration = 
                Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }
            //Document Specific log.
            string logfilePath = string.Empty;
            var dt = DateTime.Now;
            var dtStamp = $"{dt.Year}.{dt.Month}.{dt.Day}-{dt.Hour}.{dt.Minute}.{dt.Second}.{dt.Millisecond}";

            var operationParam = new OperationParam();
            bool cmdParsingSuccess = Parser.Default.ParseArguments(args, operationParam);
            operationParam.Name = operationParam.Name.ToLower();
            if (operationParam.Name.Equals(ClassifyDocumentOperationParams.OperationName))
            {
                var param = new ClassifyDocumentOperationParams();
                Parser.Default.ParseArguments(args, param);
                logfilePath = Path.Combine(logDir, $"ClassifierWorker_Executor_{dtStamp}_{param.FileId}.log");
            }
            else
            {
                logfilePath = Path.Combine(logDir, $"ClassifierWorker_Executor_{dtStamp}.log");
            }
            LoggingConfigurations loggingConfigurations = new LoggingConfigurations();
            loggingConfigurations.logFilePath = logfilePath;
            loggingConfigurations.loggerSettings = _cognitiveServicesConfiguration.LoggerSettings;
            return loggingConfigurations;
        }

        [HandleProcessCorruptedStateExceptions]
        private static void Main(string[] args)
        {
            try
            {
                string correlationId = Guid.NewGuid().ToString();
                IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
                var operationParam = new OperationParam();
                bool cmdParsingSuccess = Parser.Default.ParseArguments(args, operationParam);
                if (string.IsNullOrEmpty(operationParam.Name))
                {
                    IQBotLogger.Error("Operation not specified");
                    Environment.Exit(1);
                    return;
                }
                Operation currentOperationParam = OperationHelper.GetOperation(operationParam, args);
                
                if (currentOperationParam == null)
                {
                    IQBotLogger.Error($"Arguments for operation \"{operationParam.Name}\" not correctly specified");
                    Environment.Exit(2);
                    return;
                }

                LoggingConfigurations loggingConfigurations = GetLoggingConfigurations(args);
                initializingLoggingMechanism(loggingConfigurations);
                currentOperationParam.Execute(loggingConfigurations);
                IQBotLogger.Trace("Classifier executor exited");
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Error in executor {ex.Message}", ex);
                Environment.Exit(2);
            }
        }

    }

    public static class OperationHelper
    {
        public static Operation GetOperation(OperationParam operationParam, string[] args)
        {
            if (string.IsNullOrWhiteSpace(operationParam.Name)) return null;

            operationParam.Name = operationParam.Name.ToLower();

            if (operationParam.Name.Equals(ClassifyDocumentOperationParams.OperationName))
            {
                var param = new ClassifyDocumentOperationParams();
                return Parser.Default.ParseArguments(args, param) ? new ClassifyDocumentOperation(param) : null;
            }

            return null;
        }
    }

    public abstract class Operation
    {
        protected abstract string ExecuteAndGetResult(LoggingConfigurations loggingConfigurations);

        public virtual void Execute(LoggingConfigurations loggingConfigurations)
        {
            var result = ExecuteAndGetResult(loggingConfigurations);
            ReturnResult(result);
        }

        protected void ReturnResult(string result)
        {
            var filePath = Path.Combine(System.IO.Path.GetTempPath(), $"{Guid.NewGuid().ToString()}.result");
            File.WriteAllText(filePath, result);
            IQBotLogger.Debug($"File path: {filePath}");
        }
    }

    public class ClassifyDocumentOperation : Operation
    {
        public ClassifyDocumentOperationParams Params { get; private set; }

        public ClassifyDocumentOperation(ClassifyDocumentOperationParams param)
        {
            this.Params = param;
        }

        [HandleProcessCorruptedStateExceptions]
        protected override string ExecuteAndGetResult(LoggingConfigurations loggingConfigurations)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, this.Params.FileId);   
            ClassificationEngineAPI engineAPI = new ClassificationEngineAPI();
            engineAPI.Classify(
               this.Params.OrganizationId, this.Params.ProjectId, this.Params.FileId, this.Params.FileName, loggingConfigurations);

            return "success";
        }
    }
}