﻿namespace Automation.CognitiveData.Validator.Model
{
    public class InvalidReason
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }
}
