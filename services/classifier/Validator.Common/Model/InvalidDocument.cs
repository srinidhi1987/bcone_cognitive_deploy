﻿using System.Collections.Generic;

namespace Automation.CognitiveData.Validator.Model
{
    public class InvalidDocument
    {
        public string OrganizationId { get; set; }

        public string ProjectId { get; set; }

        public string FileId { get; set; }

        public IList<InvalidReason> Reasons { get; set; }
    }
}
