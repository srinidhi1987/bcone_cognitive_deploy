﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */


using Automation.ValidatorLite.IntegratorCommon.Model;

namespace Automation.CognitiveData.Validator.Model
{
    public class CloseDocumentRequest : ServiceRequest<FailedVBotDocument>
    { }

    public class NextDocumentRequest : ServiceRequest<FailedVBotDocument>
    {
    }

    public class PrevDocumentRequest : ServiceRequest<FailedVBotDocument>
    {
    }

    public class CloseDocumentResponse : ServiceResponse<FailedVBotDocument>
    { }

    public class NextDocumentResponse : ServiceResponse<FailedVBotDocument>
    {
    }

    public class PrevDocumentResponse : ServiceResponse<FailedVBotDocument>
    {
    }

    public class SaveDocumentRequest : ServiceRequest<FailedVBotDocument>
    {
    }

    public class SaveDocumentResponse : ServiceResponse<FailedVBotDocument>
    {
    }
    public class InvalidDocumentRequest : ServiceRequest<InvalidDocument>
    {
    }
    public class InvalidDocumentResponse:ServiceResponse<InvalidDocument>
    {
    }
}