﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.ValidatorLite.IntegratorCommon.Model
{
    public class ServiceRequest<T>
    {
        public ServiceRequest() { }

        public Guid Id { get; set; }
        public T Model { get; set; }
        public IUser User { get; set; }
    }
    public class QSettingUpdateRequest : ServiceRequest<QSetting>
    { }
}
