﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.ValidatorLite.IntegratorCommon.Model
{
    public class ServiceResponse<T>
    {
        public ServiceResponse() { }

        public int ErrorCode { get; set; }
        public T Model { get; set; }
        public ServiceRequest<T> Request { get; set; }
        public int ResponseCode { get; set; }
    }
    public class QSettingUpdateResponse : ServiceResponse<IEnumerable<Model.QSetting>>
    { }
}
