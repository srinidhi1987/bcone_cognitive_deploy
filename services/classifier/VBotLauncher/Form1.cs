﻿//using Automation.CognitiveData.Validator.UI.Contract;
//using Automation.CognitiveData.Validator.UI.Service;
//using Automation.CognitiveData.Validator.UI.ViewModel;
using Automation.CognitiveData;
using Automation.CognitiveData.VisionBot.UI;
using Automation.CognitiveData.VisionBot.UI.ViewModels;
using Automation.VisionBotEngine.Model;
using System;
using System.IO;
using System.Windows.Forms;

namespace VBotLauncher
{
    public partial class Form1 : Form
    {
        private VisionBotUIManager visionBotInfo;

        public Form1()
        {
            System.Diagnostics.Debugger.Launch();
            InitializeComponent();

            ////listBox1.DataSource = DesignerServiceEndPointProvider.GetInstant().GetAllProjectsManifest("000");
            //listBox1.DisplayMember = "ProjectId";
            button1.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenVbot();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            listBox1.DataSource = DesignerServiceEndPointProvider.GetInstant().GetAllVisionBotsManifest(listBox1.SelectedItem as ProjectManifest);
            listBox1.DisplayMember = "Id";
            button1.Visible = true;
            //  OpenVbot();
        }

        private void OpenVbot()
        {
            try
            {
                VisionBotUIManager manager = new VisionBotUIManager(listBox1.SelectedItem as VisionBotManifest);
                manager.ShowModalDialog();
            }
            catch (Exception ex)
            {
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();

            string templateName = Path.GetFileNameWithoutExtension(listBox1.SelectedItem as string);
            string dataStoragePath = null;
            var vBotManifest = new VisionBotManifest()
            {
                Name = templateName,
                ParentPath = dataStoragePath
            };
            var visionBotInfo = new VisionBotUIManager(vBotManifest);
            visionBotInfo.ShowModalDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //string templateName = Path.GetFileNameWithoutExtension(listBox1.SelectedItem as string);
            //string fileName = string.Empty;
            //OpenFileDialog fileDlg = new OpenFileDialog();
            //if(fileDlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            //{
            //    fileName = fileDlg.FileName;
            //}

            //System.Windows.Window w = new System.Windows.Window();
            //w.WindowState = System.Windows.WindowState.Maximized;
            //w.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            //w.Title = "IQBot Validator";
            //Automation.CognitiveData.VisionBot.UI.Validator.ValidatorView view = new Automation.CognitiveData.VisionBot.UI.Validator.ValidatorView(templateName, fileName);
            //w.Content = view;
            //w.ShowDialog();
            //string path = textBoxValidatorQ.Text;
            //IValidatorUIService validatorUIService = new ValidatorUIService();
            //validatorUIService.OpenValidator(path);
            ProjectsInformationViewModel pp = new ProjectsInformationViewModel();

            ProjectsInformation info = new ProjectsInformation(pp);
            info.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // listBox1.DataSource = visionBotInfo.GetProjectList("000");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Image document = DesignerServiceEndPointProvider.GetInstant().GetDocument();
            //document.Save("D:\\serviceImage.png");
        }
    }
}