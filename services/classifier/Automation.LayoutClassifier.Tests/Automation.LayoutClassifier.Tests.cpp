// Automation.LayoutClassifier.Tests.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "gmock\gmock.h"
#include "gtest\gtest.h"
#include <math.h>
#include "../Automation.LayoutClassifier/LayoutClassification.h"
using namespace ::testing;
using namespace cv;

class LayoutClassificationTest : public testing::Test
{
public:
	cv::Rect *initialRect;
	LayoutClassification *layoutClassification;
	vector<cv::Rect> rectsInHeader;
	int totalRows;
	int totalCols;
	int noOfValidateRect;
	virtual void SetUp()
	{
		totalRows = 500;
		totalCols = 2000;
		noOfValidateRect = 2;
		layoutClassification = new LayoutClassification();
		initialRect = new cv::Rect(10, 10, 500, 300);
		rectsInHeader.push_back(cv::Rect(12, 10, 30, 10));
		rectsInHeader.push_back(cv::Rect(25, 12, 50, 10));
	}
	virtual void TearDown()
	{
		delete layoutClassification;
		delete initialRect;
		rectsInHeader.erase(rectsInHeader.begin(), rectsInHeader.begin() + rectsInHeader.size());
	}
};
TEST_F(LayoutClassificationTest, DoNotShiftROIifHeaderHasEnoughValidRects)
{
	//ARRANGE
	cv::Rect expected = *initialRect;

	//ACT
	cv::Rect actual = layoutClassification->ShiftROIifHeaderHasnotEnoughValidRects(*initialRect, rectsInHeader, totalRows, totalCols, noOfValidateRect);

	//Assert
	EXPECT_TRUE(expected == actual);
}
TEST_F(LayoutClassificationTest, ShiftROIifHeaderHasnotEnoughValidRects)
{
	//ARRANGE
	cv::Rect expected = *initialRect;
	noOfValidateRect = 4;

	//ACT
	cv::Rect actual = layoutClassification->ShiftROIifHeaderHasnotEnoughValidRects(*initialRect, rectsInHeader, totalRows, totalCols, noOfValidateRect);

	//Assert
	EXPECT_FALSE(expected == actual);
}
TEST_F(LayoutClassificationTest, AddPaddingAndGetValidatedROI)
{
	//ARRANGE
	cv::Rect expected = *initialRect;
	int padding = 5;
	expected.y = expected.y - padding;
	totalRows = 2200;
	totalCols = 1700;

	//ACT
	cv::Rect actual = layoutClassification->AddPaddingAndGetValidatedROI(*initialRect, padding, totalRows, totalCols);

	//Assert
	EXPECT_EQ(expected, actual);
}
TEST_F(LayoutClassificationTest, AddPaddingAndGetValidatedROI_ifPaddingIsBiggerThanY_ReturnOriginalRect)
{
	//ARRANGE
	cv::Rect expected = *initialRect;
	int padding = 5000;
	totalRows = 2200;
	totalCols = 1700;

	//ACT
	cv::Rect actual = layoutClassification->AddPaddingAndGetValidatedROI(*initialRect, padding, totalRows, totalCols);

	//Assert
	EXPECT_EQ(expected, actual);
}
TEST_F(LayoutClassificationTest, GetHeightOfMiddleRect)
{
	//ARRANGE
	rectsInHeader.erase(rectsInHeader.begin(), rectsInHeader.begin() + rectsInHeader.size());
	rectsInHeader.push_back(cv::Rect(5, 5, 20, 20));
	rectsInHeader.push_back(cv::Rect(5, 10, 25, 10));
	rectsInHeader.push_back(cv::Rect(10, 5, 36, 50));
	rectsInHeader.push_back(cv::Rect(3, 2, 50, 38));
	rectsInHeader.push_back(cv::Rect(1, 5, 50, 48));
	rectsInHeader.push_back(cv::Rect(5, 5, 12, 98));
	//rectsInHeader.push_back(cv::Rect(5, 5, 12, 75));
	int expected = 48;
	//ACT
	int actual = layoutClassification->GetHeightOfMiddleRect(rectsInHeader);

	//Assert
	EXPECT_EQ(expected, actual);
}

TEST_F(LayoutClassificationTest, AdjustRectHeightToMerge_IncreaseRectFromToAndBottom)
{
	//ARRANGE
	cv::Rect originalRect(5, 5, 100, 100);
	totalRows = 2000;
	int sensitivity = 3;
	cv::Rect expected(originalRect.x, originalRect.y - (sensitivity / 2.0f), originalRect.width, originalRect.height + sensitivity);
	//ACT
	cv::Rect actual = layoutClassification->AdjustRectHeightToMerge(originalRect, totalRows, sensitivity);

	//Assert
	EXPECT_EQ(expected, actual);
}
TEST_F(LayoutClassificationTest, AdjustRectHeightToMerge_AfterAddingSesitivityIfYGoesMinus_ReturnOriginalY)
{
	//ARRANGE
	cv::Rect originalRect(5, 5, 100, 100);
	totalRows = 2000;
	int sensitivity = 20;
	cv::Rect expected(originalRect.x, originalRect.y, originalRect.width, originalRect.height + sensitivity);
	//ACT
	cv::Rect actual = layoutClassification->AdjustRectHeightToMerge(originalRect, totalRows, sensitivity);

	//Assert
	EXPECT_EQ(expected, actual);
}
TEST_F(LayoutClassificationTest, AdjustRectHeightToMerge_AfterAddingSesitivityIfHeightGoesBeyondTotalHeight_ReturnTotalHeightMinusRectheight)
{
	//ARRANGE
	cv::Rect originalRect(5, 5, 100, 100);
	totalRows = 2000;
	int sensitivity = 2000;
	cv::Rect expected(originalRect.x, originalRect.y, originalRect.width, totalRows - originalRect.y);
	//ACT
	cv::Rect actual = layoutClassification->AdjustRectHeightToMerge(originalRect, totalRows, sensitivity);

	//Assert
	EXPECT_EQ(expected, actual);
}

TEST_F(LayoutClassificationTest, GetCenterPointOfTop3ValidateRect)
{
	//ARRANGE
	rectsInHeader.erase(rectsInHeader.begin(), rectsInHeader.begin() + rectsInHeader.size());
	rectsInHeader.push_back(cv::Rect(5, 5, 20, 20));
	rectsInHeader.push_back(cv::Rect(5, 20, 20, 10));
	rectsInHeader.push_back(cv::Rect(5, 5, 20, 50));
	rectsInHeader.push_back(cv::Rect(3, 2, 50, 38));
	rectsInHeader.push_back(cv::Rect(1, 5, 50, 48));
	rectsInHeader.push_back(cv::Rect(5, 5, 12, 98));
	cv::Point2f expectedCenters[3] = { {15,15}, {15,25}, {28, 21} };
	cv::Point2f actualCenters[3];
	//ACT
	layoutClassification->GetCenterPointOfTop3ValidateRect(rectsInHeader, actualCenters);

	//Assert
	EXPECT_EQ(expectedCenters[0], actualCenters[0]);
	EXPECT_EQ(expectedCenters[1], actualCenters[1]);
	EXPECT_EQ(expectedCenters[2], actualCenters[2]);
}

int main(int argc, char** argv)
{
	testing::InitGoogleMock(&argc, argv);

	RUN_ALL_TESTS();
	getchar();
	return 0;
}