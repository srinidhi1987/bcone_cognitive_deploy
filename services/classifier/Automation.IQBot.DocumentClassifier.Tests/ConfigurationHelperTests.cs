﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Automation.Generic.Configuration;

namespace Automation.IQBot.DocumentClassifier.Tests
{
    public class ConfigurationHelperTests
    {
        #region public TOutput GetParsedValue<TOutput>(string key, TOutput defaultValue, Func<string, TOutput, TOutput> parser)

        [Fact]
        public void GetParsedValue_ValueAvailableFromConfigManagerAndParsingSuccessful_ReturnsThatValue()
        {
            // ARRANGE

            var defaultValue = 4;
            var configuredValue = 7;
            var key = "RequiredKeysThreshold";

            var configManager = new Mock<IConfigurationManager>();
            configManager.Setup(x => x.GetValue(key)).Returns(7.ToString());

            ConfigurationHelper configHelper = new ConfigurationHelper(configManager.Object);

            // ACT

            var actualValue = configHelper.GetParsedValue(key, defaultValue, ConfigurationHelper.ParseInt);

            // ASSERT

            Assert.Equal(configuredValue, actualValue);
        }

        [Fact]
        public void GetParsedValue_NoValueAvailableFromConfigManager_ReturnsDefaultValue()
        {
            // ARRANGE

            var defaultValue = 4;
            var key = "SampleKey";

            var configManager = new Mock<IConfigurationManager>();
            configManager.Setup(x => x.GetValue(key)).Returns((string)null);

            ConfigurationHelper configHelper = new ConfigurationHelper(configManager.Object);

            // ACT

            var actualValue = configHelper.GetParsedValue(key, defaultValue, ConfigurationHelper.ParseInt);

            // ASSERT

            Assert.Equal(defaultValue, actualValue);
        }

        [Fact]
        public void GetParsedValue_ValueAvailableFromConfigManagerAndParsingUnsuccessful_ReturnsDefaultValue()
        {
            //ARRANGE
            int defaultvalue = 4;
            var key = "SampleKey";

            var configManager = new Mock<IConfigurationManager>();
            configManager.Setup(x => x.GetValue(key)).Returns("a12");

            ConfigurationHelper configHelper = new ConfigurationHelper(configManager.Object);

            // ACT
            int actualValue = configHelper.GetParsedValue(key, defaultvalue, ConfigurationHelper.ParseInt);

            //ASSERT
            Assert.Equal(defaultvalue, actualValue);
        }

        [Fact]
        public void GetParsedValue_BlankOrNullKeyPassed_ReturnsDefaultValue()
        {
            //ARRANGE
            int defaultvalue = 4;
            var key = "";

            var configManager = new Mock<IConfigurationManager>();
            configManager.Setup(x => x.GetValue(key)).Returns((string)null);

            ConfigurationHelper configHelper = new ConfigurationHelper(configManager.Object);

            // ACT
            int actualValue = configHelper.GetParsedValue(key, defaultvalue, ConfigurationHelper.ParseInt);

            //ASSERT
            Assert.Equal(defaultvalue, actualValue);
        }

        [Fact]
        public void GetParsedValue_NullConfigurationManager_ReturnsDefaultValue()
        {
            //ARRANGE
            int defaultvalue = 4;
            var key = "SampleKey";

            ConfigurationHelper configHelper = new ConfigurationHelper(null);

            // ACT
            int actualValue = configHelper.GetParsedValue(key, defaultvalue, ConfigurationHelper.ParseInt);

            //ASSERT
            Assert.Equal(defaultvalue, actualValue);
        }

        #endregion public TOutput GetParsedValue<TOutput>(string key, TOutput defaultValue, Func<string, TOutput, TOutput> parser)
    }
}