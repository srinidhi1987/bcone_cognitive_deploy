﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Cognitive.VisionBotEngine.Model.ExecutorOperations;
using Automation.IQBot.Logger;
using Automation.Services.Client;
using Automation.Services.Client.Common;
using Automation.VisionBotEngine;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Worker.Visionbot
{
    internal class Program
    {
        public static bool stopService = false;
        public const string ServiceName = "Automation Anywhere Cognitive VisionbotEngine Service";
        public static string ExecutorPath = string.Empty;
        internal static ArrayList MessagesInProgress = ArrayList.Synchronized(new ArrayList());
        private const int CleaningTimeIntervalInMilliSecond = 15 * 60 * 1000;
        internal static ICacheCleanUpWizard cacheCleanUpWizard;

        private static ReaderWriterLockSlim pauseLock = new ReaderWriterLockSlim();

        private static bool _pause = false;
        //TODO: try to lock with In BuiltIn Reader Writer lock
        internal static bool Pause
        {
            get
            {
                pauseLock.EnterReadLock();
                try
                {
                    return _pause;
                }
                finally
                {
                    pauseLock.ExitReadLock();
                }
            }
            set
            {
                pauseLock.EnterWriteLock();
                try
                {
                    _pause = value;
                }
                finally
                {
                    pauseLock.ExitWriteLock();
                }
            }
        }

        private static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                // running as console app
                Start();
                Console.WriteLine("Press any key to stop...");
                Console.ReadKey(true);
                Stop();
            }
            else
            {
                // running as service
                using (var service = new Service())
                {
                    ServiceBase.Run(service);
                }
            }
        }

        public static void Start()
        {
            string correlationId = Guid.NewGuid().ToString();
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            IQBotLogger.Debug("Correlation Id attached to current process: " + correlationId);
            initalizingFileCleanUpMechanism();

            string relativePathToPropertyFile = "";
            string basedir = AppDomain.CurrentDomain.BaseDirectory;
            IQBotLogger.Debug(string.Format($"basedir: {basedir}"));

            ExecutorPath = Path.Combine(basedir, "Automation.Cognitive.Worker.Visionbot.Executor.exe");
            IQBotLogger.Debug($"ExecutorPath: {ExecutorPath}");

            var fileInfo = new FileInfo(basedir);
            var parentDir = fileInfo.Directory == null ? null : fileInfo.Directory.Parent;
            if (parentDir != null)
            {
                var resourceInfo = new FileInfo(parentDir.FullName);
                relativePathToPropertyFile = resourceInfo.DirectoryName + "\\Configurations";
            }

            initializeConfigurations(relativePathToPropertyFile);

            Thread T1 = new Thread(new ParameterizedThreadStart(StartCreateVisionBotQueue));
            T1.Start(correlationId);
            Thread T2 = new Thread(new ParameterizedThreadStart(StartGenerateAndExtractVBotDataQueue));
            T2.Start(correlationId);
            Thread T3 = new Thread(new ParameterizedThreadStart(StartExtractVBotDataQueue));
            T3.Start(correlationId);
            Thread T4 = new Thread(new ParameterizedThreadStart(StartValueFieldQueue));
            T4.Start(correlationId);
            Thread T5 = new Thread(new ParameterizedThreadStart(StartGetImageQueue));
            T5.Start(correlationId);
            Thread T6 = new Thread(new ParameterizedThreadStart(StartExportDataQueue));
            T6.Start(correlationId);
            Thread T7 = new Thread(new ParameterizedThreadStart(StartProcessDocumentQueue));
            T7.Start(correlationId);
            Thread T8 = new Thread(new ParameterizedThreadStart(StartGetVboReportData));
            T8.Start(correlationId);
            Thread T9 = new Thread(new ParameterizedThreadStart(StartProcessStagingDocumentQueue));
            T9.Start(correlationId);
            Thread T10 = new Thread(new ParameterizedThreadStart(AddNewFieldsQueue));
            T10.Start(correlationId);

            T1.Join();
            T2.Join();
            T3.Join();
            T4.Join();
            T5.Join();
            T6.Join();
            T7.Join();
            T8.Join();
            T9.Join();
            T10.Join();

        }

        private static void initalizingFileCleanUpMechanism()
        {
            string rootCacheDirectory = Path.Combine(Path.GetTempPath(), "VisionBot");
            cacheCleanUpWizard = new DiskBasedCleanUpWizard(rootCacheDirectory
               , CleaningTimeIntervalInMilliSecond);

            cacheCleanUpWizard.BeforeCleanUpEvent += CacheCleanUpWizard_BeforeCleanUpEvent;
            cacheCleanUpWizard.AfterCleanUpEvent += CacheCleanUpWizard_AfterCleanUpEvent;
        }

        private static void CacheCleanUpWizard_AfterCleanUpEvent
            (object sender, EventArgs e)
        {
            Program.Pause = false;
        }


        private static void CacheCleanUpWizard_BeforeCleanUpEvent
            (object sender, CacheClenupEventArgs e)
        {
            DateTime startTime = DateTime.Now;
            Program.Pause = true;

            while (MessagesInProgress.Count != 0)
            {
                Thread.Sleep(1000);
                TimeSpan span = DateTime.Now - startTime;
                if (span.TotalMinutes > 2)
                {
                    e.DoesCleanupTheCache = false;
                    IQBotLogger.Trace("Skipping temp deletion as Current process taking more time.");
                    break;
                }
            }
        }

        private static void CleanTheCache()
        {
            try
            {
                var directoryPath = Path.Combine(
                            Path.GetTempPath(),
                            "VisionBot");
                if (Directory.Exists(directoryPath))
                {
                    Directory.Delete(directoryPath, true);
                    IQBotLogger.Trace(string.Format("[VisionBotMainWindow_OnClosed] Successfully deleted {0} directory.", directoryPath));
                }
            }
            catch (Exception ex)
            {
                IQBotLogger.Warn(ex.Message);
            }
        }

        private static void initializeConfigurations(string relativePath)
        {
            try
            {
                CognitiveSecurePropertiesProvider.Init(relativePath, "Cognitive.properties", "private.key");
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Exception: {ex.ToString()}");
            }
        }

        private static RabbitMQSetting getRabbitMQSetting()
        {
            try
            {
                var rabbitMQSetting = new ServiceLocationProvider().GetRabbitMQSetting();
                rabbitMQSetting.Password = CognitiveSecurePropertiesProvider.Instance().GetMessageQueuePassword();
                return rabbitMQSetting;
            }
            catch (Exception ex)
            {
                IQBotLogger.Error($"Exception: {ex.ToString()}");
                throw;
            }
        }

        private static void StartProcessStagingDocumentQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();

            var rpcProcessStagingDocument = new RpcMsgQ(
                      "QUEUE_Visionbot_ProcessStagingDocument"
                      , rabbitMQSetting);
            rpcProcessStagingDocument.RabbitQueueStart(callerProxy.ProcessStagingDocument);
        }

        private static void StartGetVboReportData(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcGetVBotReportData = new RpcMsgQ(
                        "QUEUE_Visionbot_GetVBotReportData"
                        , rabbitMQSetting);
            rpcGetVBotReportData.Start(callerProxy.GetVBotReportData);
        }

        private static void StartProcessDocumentQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcProcessDocument = new RpcMsgQ(
                           "QUEUE_Visionbot_ProcessDocument"
                           , rabbitMQSetting);
            rpcProcessDocument.RabbitQueueStart(callerProxy.ProcessDocument);
        }

        private static void StartExportDataQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcExportData = new RpcMsgQ(
                           "QUEUE_ExportData"
                           , rabbitMQSetting);
            rpcExportData.Start(callerProxy.ExportData);
        }

        private static void StartGetImageQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcGetImage = new RpcMsgQ(
                            "QUEUE_GetImage"
                            , rabbitMQSetting);
            rpcGetImage.Start(callerProxy.GetImage);
        }

        private static void StartValueFieldQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcGetValueField = new RpcMsgQ(
                            "QUEUE_Visionbot_GetValueField"
                            , rabbitMQSetting);
            rpcGetValueField.Start(callerProxy.GetValueField);
        }

        private static void StartExtractVBotDataQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcExtractVBotData = new RpcMsgQ(
                            "QUEUE_Visionbot_ExtractVBotData"
                            , rabbitMQSetting);
            rpcExtractVBotData.Start(callerProxy.ExtractVBotData);
        }

        private static void StartGenerateAndExtractVBotDataQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcGenerateAndExtractVBotData = new RpcMsgQ(
                            "QUEUE_Visionbot_GenerateAndExtractVBotData"
                            , rabbitMQSetting);
            rpcGenerateAndExtractVBotData.Start(callerProxy.GenerateAndExtractVBotData);
        }

        private static void StartCreateVisionBotQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcCreateVisionBot = new RpcMsgQ(
              "QUEUE_CreateVisionBot"
              , rabbitMQSetting);
            rpcCreateVisionBot.Start(callerProxy.CreateVisionBot);
        }

        private static void AddNewFieldsQueue(object correlationId)
        {
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            var callerProxy = new CallerProxy();

            var rabbitMQSetting = getRabbitMQSetting();
            var rpcAddNewFields = new RpcMsgQ(
              "QUEUE_AddNewFields"
              , rabbitMQSetting);

            rpcAddNewFields.Start(callerProxy.AddNewFields);
        }

        public static void Stop()
        {
            CleanTheCache();
            Program.stopService = true;

            File.AppendAllText(@"VBotEngineService.txt", String.Format("{0} stopped{1}", DateTime.Now, Environment.NewLine));
        }
    }

    internal class CallerProxy
    {
        private static string ExecutorPath
        {
            get
            {
                return Program.ExecutorPath;
            }
        }

        //This method is same as Execute with a difference that it returns the result even if any error/exception
        //occurs while executing any process
        private string ExecuteProcess(string args)
        {
            var instanceId = Guid.NewGuid().ToString();
            IQBotLogger.Trace($"Executor path {ExecutorPath}");
            IQBotLogger.Trace($"Args {args}");
            var t = ProcessInvokerUtil.RunProcessAsync(ExecutorPath, args);
            t.Wait();
            var output = t.Result;
            var outputResult = output.Result;
            if (!output.IsSuccess || outputResult.StartsWith("ERROR"))
            {
                IQBotLogger.Error($"Error: {output.Result}");
                return outputResult;
            }
            else if (outputResult.EndsWith(".result") && File.Exists(outputResult))
            {
                IQBotLogger.Debug($"Result: {output}");
                var result = File.ReadAllText(outputResult);
                IQBotLogger.Debug($"Result after reading file: {result}");
                File.Delete(outputResult);
                IQBotLogger.Debug($"Result after file delete: {result}");
                return result;
            }
            else
            {
                IQBotLogger.Error($"Error processing document: {output.IsSuccess}, Result {output.Result}");
            }
            return outputResult;
        }

        private string Execute(string args)
        {
            IQBotLogger.Trace($"Executor path {ExecutorPath}");
            IQBotLogger.Trace($"Args {args}");
            var t = ProcessInvokerUtil.RunProcessAsync(ExecutorPath, args);

            t.Wait();
            var output = t.Result;

            if (!output.IsSuccess || output.Result.StartsWith("ERROR"))
            {
                IQBotLogger.Error($"Error: {output.Result}");
                return string.Empty;
            }
            else if (output.Result.EndsWith(".result") && File.Exists(output.Result))
            {
                IQBotLogger.Debug($"Result: {output}");
                var result = File.ReadAllText(output.Result);
                IQBotLogger.Debug($"Result after reading file: {result}");
                File.Delete(output.Result);
                IQBotLogger.Debug($"Result after file delete: {result}");
                return result;
            }
            else
            {
                IQBotLogger.Error($"Error processing document: {output.IsSuccess}, Result {output.Result}");
            }
            return output.Result;
        }

		private string handleCreateVisionBotMessage(String processOutput)
        {
            //TODO currently the requirement is just to propogate this message to the UI, 
            //if the requirement extends to show all errors then this check could be safely removed
            var ERRORMESSAGE = "There are no training documents available for this Bot. Upload some training documents for the associated learning instance and try again.";
            if (processOutput.Contains(ERRORMESSAGE))
            {
                processOutput = ERRORMESSAGE;
            }
            else if (processOutput.StartsWith("ERROR"))
            {
                processOutput = string.Empty;
            }
            return processOutput;
        }
        public string CreateVisionBot(string inputData)
        {
            var param = new CreateVisionBotOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.VisionBotDetailId = inputParameters["visionBotDetailId"].ToString();

            var args = param.ToConsoleArgs();
            var processOutput = ExecuteProcess(args);
            return handleCreateVisionBotMessage(processOutput);
        }

        public string AddNewFields(string inputData)
        {
            var param = new AddNewFieldsOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string GenerateAndExtractVBotData(string inputData)
        {
            var param = new GenerateAndExtractVbotDataOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.VisionBotId = inputParameters["visionbotId"].ToString();
            param.DocumentId = inputParameters["documentId"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string ExtractVBotData(string inputData)
        {
            var param = new ExtractVBotDataOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.VisionBotId = inputParameters["visionbotId"].ToString();
            param.LayoutId = inputParameters["layoutId"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string GetValueField(string inputData)
        {
            var param = new GetValueFieldOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.VisionBotId = inputParameters["visionbotId"].ToString();
            param.LayoutId = inputParameters["layoutId"].ToString();
            param.FieldLayout = inputParameters["fieldLayout"].ToString();
            param.FieldDefId = inputParameters["fieldDefId"].ToString();
            param.DocumentId = inputParameters["documentId"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string GetImage(string inputData)
        {
            var param = new GetImageOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.DocumentId = inputParameters["documentId"].ToString();
            param.PageIndex = inputParameters["pageIndex"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string ExportData(string inputData)
        {
            var param = new ExportDataOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.VbotData = inputParameters.ContainsKey("vBotData") ? inputParameters["vBotData"].ToString() : string.Empty;
            if (!string.IsNullOrEmpty(param.VbotData))
            {
                var filePath = Path.Combine(System.IO.Path.GetTempPath(), $"{Guid.NewGuid().ToString()}.input");
                File.WriteAllBytes(filePath, System.Text.Encoding.UTF8.GetBytes(param.VbotData));
                param.VbotData = filePath;
            }
            param.VbotDocument = inputParameters.ContainsKey("vbotDocument") ? inputParameters["vbotDocument"].ToString() : string.Empty;
            if (!string.IsNullOrEmpty(param.VbotDocument))
            {
                var filePath = Path.Combine(System.IO.Path.GetTempPath(), $"{Guid.NewGuid().ToString()}.input");
                File.WriteAllBytes(filePath, System.Text.Encoding.UTF8.GetBytes(param.VbotDocument));
                param.VbotDocument = filePath;
            }

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string ProcessDocument(string inputData)
        {
            var param = new ProcessDocumentOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.DocumentId = inputParameters["documentId"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        public string ProcessStagingDocument(string inputData)
        {
            var param = new ProcessStagingDocumentOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = inputParameters["organizationId"].ToString();
            param.ProjectId = inputParameters["projectId"].ToString();
            param.CategoryId = inputParameters["categoryId"].ToString();
            param.VisionBotId = inputParameters["visionBotId"].ToString();
            param.DocumentId = inputParameters["documentId"].ToString();
            param.VisionBotRunDetailsId = inputParameters["visionBotRunDetailsId"].ToString();
            param.DocumentProcessedDetailsId = inputParameters["documentProcessedDetailsId"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        internal string GetVBotReportData(string inputData)
        {
            var param = new GetVBotReportDataOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.VBotDocument = inputParameters["vBotData"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        #region Private Member

        private Dictionary<string, object> convertToDictionary(string input)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(input);
        }

        #endregion Private Member
    }

    public class RpcMsgQ
    {
        public string HostName { get; private set; }
        public string VirtualHostName { get; private set; }

        public string QueueName { get; private set; }

        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string Port { get; private set; }

        public RpcMsgQ(string queueName, string hostName = "localhost")
        {
            this.HostName = hostName;
            this.QueueName = queueName;
        }

        public RpcMsgQ(string queueName, RabbitMQSetting setting)
        {
            this.HostName = setting.Uri;
            this.VirtualHostName = setting.VirtualHost;
            this.UserName = setting.UserName;
            this.Password = setting.Password;
            this.QueueName = queueName;
            this.Port = setting.Port;
        }

        public void Start(Func<string, string> functionality)
        {
            IQBotLogger.Trace($"{this.QueueName} Enter");
            var factory = new ConnectionFactory()
            {
                HostName = this.HostName,
                VirtualHost = this.VirtualHostName
                ,
                UserName = this.UserName
                ,
                Password = this.Password
                ,
                AutomaticRecoveryEnabled = true
                ,
                Port = Int32.Parse(this.Port)
            };

            int noOfContiniousTryForConnectionEstablishment = 0;
            do
            {
                try
                {
                    startQueue(functionality, factory);
                    noOfContiniousTryForConnectionEstablishment = 0;
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Exception: {ex.Message}, try count {noOfContiniousTryForConnectionEstablishment}");
                    Thread.Sleep(1000);
                    noOfContiniousTryForConnectionEstablishment++;
                }
            }
            while (!Program.stopService);
        }

        private void startQueue(Func<string, string> functionality, ConnectionFactory factory)
        {
            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: this.QueueName,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    channel.BasicQos(0, 1, false);

                    IQBotLogger.Debug($"Waiting for messages...");
                    QueueingBasicConsumer consumer = new QueueingBasicConsumer(channel);
                    channel.BasicConsume(queue: this.QueueName,
                            noAck: false,
                            consumer: consumer);

                    while (Program.stopService == false)
                    {
                        try
                        {
                            BasicDeliverEventArgs e = new BasicDeliverEventArgs();
                            if (!connection.IsOpen)
                            {
                                return;
                            }

                            waitIfPauseRequested();

                            consumer.Queue.Dequeue(1000, out e);
                            if (e != null)
                            {
                                string response = null;

                                var body = e.Body;
                                var props = e.BasicProperties;
                                var replyProps = channel.CreateBasicProperties();
                                replyProps.CorrelationId = props.CorrelationId;
                                replyProps.Persistent = true;

                                string messageId = Guid.NewGuid().ToString();
                                try
                                {
                                    Program.MessagesInProgress.Add(messageId);

                                    var message = Encoding.UTF8.GetString(body);
                                    //To avoid ExportData functionality to print the whole document data
                                    if (!functionality.Method.Name.Equals("ExportData"))
                                    {
                                        IQBotLogger.Debug($"Message: {message}");
                                    }
                                    else
                                    {
                                        IQBotLogger.Debug("DONE");                                 
        							}
                                    response = functionality(message);
                                }
                                catch (Exception ex)
                                {
                                    IQBotLogger.Error($"Exception Occured while executing functionality {ex.Message}");
                                    response = "";
                                }
                                finally
                                {
                                    Program.MessagesInProgress.Remove(messageId);

                                    try
                                    {
                                        if (!string.IsNullOrEmpty(props.ReplyTo))
                                        {
                                            if (response == null)
                                                response = string.Empty;
                                            var responseBytes = Encoding.UTF8.GetBytes(response);
                                            channel.BasicPublish(exchange: "", routingKey: props.ReplyTo,
                                              basicProperties: replyProps, body: responseBytes);

                                            channel.BasicAck(deliveryTag: e.DeliveryTag,
                                              multiple: false);
                                            IQBotLogger.Trace("Completed");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        IQBotLogger.Error($"Failed to send ack, Exception: {ex.Message}");
                                    }
                                }
                                IQBotLogger.Trace($"Waiting for next message ...");
                            }
                        }
                        catch (Exception ex)
                        {
                            IQBotLogger.Error($"Exception: {ex.Message}");
                        }
                    }
                    IQBotLogger.Trace("Completed");
                }
            }
        }

        private static void waitIfPauseRequested()
        {
            while (Program.Pause)
            {
                Thread.Sleep(1000);
            }
        }

        public void RabbitQueueStart(Func<string, string> functionality)
        {
            IQBotLogger.Trace($"{this.QueueName} Started");
            var factory = new ConnectionFactory()
            {
                HostName = this.HostName,
                VirtualHost = this.VirtualHostName,
                UserName = this.UserName,
                Password = this.Password,
                AutomaticRecoveryEnabled = true,
                Port = Int32.Parse(this.Port)
            };
            int noOfContiniousTryForConnectionEstablishment = 0;
            do
            {
                try
                {
                    startRabbitQueueStart(functionality, factory);
                    noOfContiniousTryForConnectionEstablishment = 0;
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Exception: {ex.Message} Try count {noOfContiniousTryForConnectionEstablishment}");
                    Thread.Sleep(1000);
                    noOfContiniousTryForConnectionEstablishment++;
                }
            }
            while (!Program.stopService);
            IQBotLogger.Trace($"{this.QueueName} Completed");
        }

        private void startRabbitQueueStart(Func<string, string> functionality, ConnectionFactory factory)
        {
            IQBotLogger.Trace($"{this.QueueName} Started");
            using (IConnection connection = factory.CreateConnection())
            using (IModel channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: this.QueueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

                channel.BasicQos(0, 1, false);
                QueueingBasicConsumer consumer = new QueueingBasicConsumer(channel);
                channel.BasicConsume(queue: this.QueueName,
                        noAck: false,
                        consumer: consumer);

                while (!Program.stopService)
                {
                    try
                    {
                        BasicDeliverEventArgs ea = new BasicDeliverEventArgs();
                        if (!connection.IsOpen)
                        {
                            return;
                        }
                        waitIfPauseRequested();
                        consumer.Queue.Dequeue(5000, out ea);
                        if (ea != null)
                        {
                            var body = ea.Body;
                            var message = Encoding.UTF8.GetString(body);
                            IQBotLogger.Debug($"Message: {message}");
                            string messageId = Guid.NewGuid().ToString();
                            try
                            {
                                Program.MessagesInProgress.Add(messageId);
                                functionality(message);
                            }
                            catch (Exception e)
                            {
                                IQBotLogger.Error($"Exception: {e.ToString()}");
                            }
                            finally
                            {
                                Program.MessagesInProgress.Remove(messageId);

                                try
                                {
                                    channel.BasicAck(ea.DeliveryTag, false);
                                }
                                catch (Exception ex)
                                {
                                    IQBotLogger.Error($"Failed to send ack, Excception: {ex.Message}");
                                }
                                IQBotLogger.Trace("Completed Message Processing");
                            }
                            IQBotLogger.Trace($"Waiting for next message ...");
                        }
                    }
                    catch (Exception ex)
                    {
                        IQBotLogger.Error($"Exception: {ex.Message}");
                    }
                }
                IQBotLogger.Trace($"Completed");
            }
        }
    }

    public class ProcessResult
    {
        public bool IsSuccess { get; set; }
        public string Result { get; set; }

        public override string ToString()
        {
            return $"success:{IsSuccess}, data: {Result}";
        }
    }

    internal class ProcessInvokerUtil
    {
        protected ProcessInvokerUtil() { }
        public static async Task<ProcessResult> RunProcessAsync(string fileName, string args)
        {
            using (var process = new Process
            {
                StartInfo =
                {
                    FileName = fileName, Arguments = args,
                    UseShellExecute = false, CreateNoWindow = true,
                    RedirectStandardOutput = true
                },
                EnableRaisingEvents = true
            })
            {
                return await RunProcessAsync(process).ConfigureAwait(false);
            }
        }

        private static Task<ProcessResult> RunProcessAsync(Process process)
        {
            var tcs = new TaskCompletionSource<ProcessResult>();
            var processResult = string.Empty;
            bool processExited = false;
            process.Exited += (s, ea) =>
            {
                IQBotLogger.Trace($"Exited");
                processExited = true;
            };
            process.OutputDataReceived += (s, ea) =>
            {
                if (ea.Data == null)
                {
                    if (!processExited)
                    {
                        while (!processExited) { Thread.Sleep(100); };
                    }
                    var result = new ProcessResult { IsSuccess = (process.ExitCode == 0), Result = processResult.ToString() };
                    IQBotLogger.Debug($"Result: {result.ToString()}");

                    tcs.SetResult(result);
                    return;
                }

                if (!string.IsNullOrWhiteSpace(ea.Data))
                {
                    processResult = ea.Data;
                }
                IQBotLogger.Debug($"Data: {ea.Data}");
            };
            bool started = process.Start();
            if (!started)
            {
                IQBotLogger.Trace($"executor util not started");
                //you may allow for the process to be re-used (started = false)
                //but I'm not sure about the guarantees of the Exited event in such a case
                throw new InvalidOperationException($"Could not start process: {process}");
            }
            IQBotLogger.Trace($"Completed");
            process.BeginOutputReadLine();
            return tcs.Task;
        }
    }
}