﻿using Automation.IQBot.Logger;
using Automation.VisionBotEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Worker.Visionbot
{
    public delegate void CacheClenupHandler(object sender, CacheClenupEventArgs e);

    public class CacheClenupEventArgs : EventArgs
    {
        public bool DoesCleanupTheCache{ get; set; }

        public CacheClenupEventArgs(bool doesCleanupTheCache)
        {
            DoesCleanupTheCache = doesCleanupTheCache;
        }
    }

    interface ICacheCleanUpWizard
    {
        event CacheClenupHandler BeforeCleanUpEvent;
        event EventHandler AfterCleanUpEvent;
    }

    internal abstract class BaseCacheCleanUpWizard : ICacheCleanUpWizard
    {
        public event EventHandler AfterCleanUpEvent;
        public event CacheClenupHandler BeforeCleanUpEvent;

        protected abstract void CleanTheOlderCache(object state);

        protected void raiseBeforeCleanUpEvent(CacheClenupEventArgs args)
        {
            if (BeforeCleanUpEvent != null)
            {
                BeforeCleanUpEvent(this, args);
            }
        }

        protected void raiseAfterCleanUpEvent()
        {
            if (AfterCleanUpEvent != null)
            {
                AfterCleanUpEvent(this, new EventArgs() { });
            }

        }
    }

    internal class DiskBasedCleanUpWizard : BaseCacheCleanUpWizard
    {
        private readonly int CleaningIntervalInMilliSecond;
        private static object _isCleaningInProgressLock = new object();
        private string _rootDirectory;
        Timer cacheCleaningTimer = null;

        public DiskBasedCleanUpWizard(string rootDirectory
            , int cleaninngIntervalInMilliSecond)
        {
            CleaningIntervalInMilliSecond = cleaninngIntervalInMilliSecond;
            _isCleaningInProgressLock = new object();
            _rootDirectory = rootDirectory;

            initalizingFileCleanUpMechanism();
        }

        private void initalizingFileCleanUpMechanism()
        {

            cacheCleaningTimer = new Timer(new TimerCallback(CleanTheOlderCache)
               , null, 0, CleaningIntervalInMilliSecond);
        }

        protected override void CleanTheOlderCache(object state)
        {
            IQBotLogger.Trace($"Started");
            if (Monitor.TryEnter(_isCleaningInProgressLock))
            {
                try
                {
                    List<string> filesToDelete = getFilesToDelete();

                    if (filesToDelete.Count > 0)
                    {
                        CacheClenupEventArgs args = new CacheClenupEventArgs(true);
                        raiseBeforeCleanUpEvent(args);

                        if (args.DoesCleanupTheCache)
                        {
                            deleteFile(filesToDelete);
                        }

                        raiseAfterCleanUpEvent();

                    }
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Error cleaning cache at {_rootDirectory}", ex);
                }
                Monitor.Exit(_isCleaningInProgressLock);
            }
            IQBotLogger.Trace($"Completed");
        }

        private static void deleteFile(List<string> filesToDelete)
        {
            foreach (string filePath in filesToDelete)
            {
                try
                {
                    File.Delete(filePath);
                    IQBotLogger.Debug($"Successful, FilePath:{filePath}");
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Error cleaning file {filePath}", ex);
                }
            }
        }

        private List<string> getFilesToDelete()
        {
            List<string> filesToDelete = new List<string>();

            if (Directory.Exists(_rootDirectory))
            {
                var files = Directory.GetFiles(_rootDirectory, "*", SearchOption.AllDirectories);
                foreach (string filePath in files)
                {
                    try
                    {
                        FileInfo fileInfo = new FileInfo(filePath);
                        if (fileInfo != null)
                        {
                            TimeSpan timeSpan = DateTime.Now - fileInfo.CreationTime;
                            if (timeSpan.TotalMilliseconds > CleaningIntervalInMilliSecond)
                            {
                                filesToDelete.Add(filePath);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        IQBotLogger.Error($"Error getting files to delete {filePath}", ex);
                    }
                }
            }
            return filesToDelete;
        }
    }
}
