﻿using System.ServiceProcess;
using System.Threading;

namespace Automation.Worker.Visionbot
{
    internal class Service : ServiceBase
    {
        private Thread m_thread = null;

        public Service()
        {
            ServiceName = Program.ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            // instantiate the thread
            m_thread = new Thread(new ThreadStart(ThreadProc));
            // start the thread
            m_thread.Start();
        }

        public void ThreadProc()
        {
            Program.Start();
            this.Stop();
        }

        protected override void OnStop()
        {
            Program.stopService = true;

            Program.Stop();
        }
    }
}