﻿namespace Automation.Cognitive.VisionBotEngine.Validation.Test.Validation.Validators.Confidence
{
    using Automation.Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using Xunit;

    public class ConfidenceValidatorTest
    {
        [Fact]
        public void Validate_FieldConfidenceMoreThenThreshold_Return_Null()
        {
            #region Arrange

            int input_ConfidenceThreshold = 60;
            int input_FieldConfidence = 70;

            FieldData input_FieldData = getFieldData(input_FieldConfidence);

            ConfidenceValidator validator = getConfidenceValidator(input_ConfidenceThreshold);

            #endregion

            //Act
            var result = validator.Validate(input_FieldData, null, null, null);

            //Assert
            Assert.Null(result);
        }
        
        [Fact]
        public void Validate_FieldConfidenceEqualToThreshold_Return_Null()
        {
            #region Arrange

            int input_ConfidenceThreshold = 70;
            int input_FieldConfidence = 70;
            FieldData input_FieldData = getFieldData(input_FieldConfidence);

            ConfidenceValidator validator = getConfidenceValidator(input_ConfidenceThreshold);

            #endregion

            //Act
            var result = validator.Validate(input_FieldData, null, null, null);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void Validate_FieldConfidenceLessThenThreshold_Return_ConfidenceIssue()
        {
            #region Arrange

            int input_ConfidenceThreshold = 80;
            int input_FieldConfidence = 60;

            FieldData input_FieldData = getFieldData(input_FieldConfidence);

            ConfidenceValidator validator = getConfidenceValidator(input_ConfidenceThreshold);

            #endregion

            //Act
            var result = validator.Validate(input_FieldData, null, null, null);

            //Assert
            Assert.IsType(typeof(ConfidenceIssue),result);
            Assert.Equal(input_FieldConfidence,((ConfidenceIssue)result).Confidence);
            Assert.Equal(input_ConfidenceThreshold, ((ConfidenceIssue)result).ThresholdConfidence);
        }

        private static FieldData getFieldData(int input_FieldConfidece)
        {
            return new FieldData()
            {
                Field = new FieldDef(),
                Value = new TextValue()
                {
                    Field = new Field()
                    {
                        Confidence = input_FieldConfidece
                    }
                }
            };
        }

        private static ConfidenceValidator getConfidenceValidator(int confideceThreshold)
        {
            return new ConfidenceValidator(new ValidatorAdvanceInfo()
            {
                ConfidenceThreshold = confideceThreshold
            });
        }
    }
}
