﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Automation.TesseractTestApp
{
    internal class Program
    {
        private const string DllName = "Automation.Tesseract.dll";
        private const string DllNameT4 = "Automation.Tesseract4.0.dll";
        private const string DllPatternRecog = "Automation.PatternRecognition.dll";
        private const string DllNameOCRService = "OcrService.dll";

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        //        public static extern string Scan(string imagePath, string searchString, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath);
        public static extern string Scan(string imagePath, string searchString, string languageCode, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath, string hOCROutputFile);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetPageOrientation(string imagePath, string trainingPath, string applicationPath);

        [DllImport(DllNameT4, CallingConvention = CallingConvention.Cdecl)]
        public static extern string ScanByLanguage(string imagePath, string searchString, string languageCode, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath, string hOCROutputFile);

        [DllImport(DllNameT4, CallingConvention = CallingConvention.Cdecl)]
        public static extern string ScanBySegmetation(string processedimagePath, string orgImagePath, string searchString, string languageCode, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath, string hOCROutputFile, int loglevel, string transactionID);

        [DllImport(DllPatternRecog, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool RemoveSpeckleNoise(string imagePath);

        [DllImport(DllPatternRecog, CallingConvention = CallingConvention.Cdecl)]
        public static extern int PredictSpeckleNoiseInDocument(string imagePath);

        [DllImport(DllPatternRecog, CallingConvention = CallingConvention.Cdecl)]
        public static extern void RemoveLines(string imagePath, string lineImagePath);

        [DllImport(DllPatternRecog, CallingConvention = CallingConvention.Cdecl)]
        public static extern int PatternNoiseDetectionAndRemoval(string imagePath, string tempIOmagePath);

        [DllImport(DllNameT4, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SegmentImage(string imagePath, string segmentFilePath);

        [DllImport(DllNameOCRService, CallingConvention = CallingConvention.Cdecl)]
        public static extern string ScanParallel(string imagePath, string srcImagePath, string searchString, string trainingDataPath, int pageSegmentationMode, int ocrEngineMode, string languageCode, string applicationPath);

        [HandleProcessCorruptedStateExceptions]
        private static void Main(string[] args)
        {
            string imagePath = @"D:\Test\ClassificationProblem\004996119-000000265.tif";
            string patterrnImagePath = @"D:\PatternImage.png";
            string processedImg = @"D:\SourceImage.png";
            string segFilePath = "segments.txt";
            string lineImagePath = @"D:\\LineImage.png"; ;

            File.Copy(imagePath, processedImg, true);

            string searchString = string.Empty;// "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 /.,#$@()*<>:-=%&_?~`!^[]{}\\+';";
            string tessdataPath = @"D:\GitClone\tesseract\tessdata";
            string hocrOutputFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            //int patternNoiseFound = PatternNoiseDetectionAndRemoval(imagePath, patterrnImagePath);
            //Console.ReadLine();
            //if (patternNoiseFound > 0)
            //    Console.WriteLine("Pattern Noise Found");

            int predictNoise = -3;
            predictNoise = PredictSpeckleNoiseInDocument(processedImg);
            if (predictNoise == 1)
            {
                Console.WriteLine("Noise is predicted");
                RemoveSpeckleNoise(processedImg);
            }
            RemoveLines(processedImg, lineImagePath);
            //SegmentImage(imagePath, segFilePath);
            Console.WriteLine("Segments are Saved");
            //string hocrText = ScanByLanguage(imagePath, searchString, "deu", tessdataPath, 3, 0, "", hocrOutputFile);
            //string hocrText = ScanParallel(imagePath, srcImagePrth, searchString, tessdataPath, 7, 2, "deu", "D:");
            try
            {
                ScanBySegmetation(processedImg, imagePath, searchString, "deu", tessdataPath, 7, 2, "D:\\Test.log", hocrOutputFile, 1, "Tesseract-App");
                string hocrText = File.ReadAllText(hocrOutputFile);
                Console.WriteLine(hocrText);
                //Console.WriteLine(hocrOutputFile);
                Console.WriteLine("Done1....");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            Console.ReadLine();
        }

        private static void reformString(string listT1, string list1)
        {
            string sourceKeys = listT1;
            string targetKeys = list1;

            string[] sourceList = sourceKeys.Split(',');
            string[] targetList = targetKeys.Split(',');

            List<KeyObject> orgSourceKeyObjects = new List<KeyObject>();
            List<KeyObject> orgTargetKeyObjects = new List<KeyObject>();

            List<string> modifiedDestinationString = new List<string>();

            for (int i = 0; i < sourceList.Length; i++)
            {
                orgSourceKeyObjects.Add(new KeyObject(i, sourceList[i]));
            }
            for (int j = 0; j < targetList.Length; j++)
            {
                orgTargetKeyObjects.Add(new KeyObject(j, targetList[j]));
                modifiedDestinationString.Add(targetList[j]);
            }

            int currentScore = 0;

            List<ComparizonObject> comaprizonResult = new List<ComparizonObject>();

            for (int i = 0; i < orgSourceKeyObjects.Count; i++)
            {
                KeyObject currentValue = orgSourceKeyObjects[i];
                int index = orgTargetKeyObjects.FindIndex(x => x.Value == currentValue.Value);
                if (index > -1)
                {
                    if (orgTargetKeyObjects.Count > (index + 1) &&
                        orgSourceKeyObjects.Count > (i + 1) &&
                        orgTargetKeyObjects[index + 1].Value == orgSourceKeyObjects[i + 1].Value)
                    {
                        comaprizonResult.Add(new ComparizonObject() { DestinationMatchedIndex = index, KeyObject = currentValue });
                    }
                    else if ((index - 1) > -1 &&
                        (i - 1) > -1 &&
                        orgTargetKeyObjects[index - 1].Value == orgSourceKeyObjects[i - 1].Value)
                    {
                        comaprizonResult.Add(new ComparizonObject() { DestinationMatchedIndex = index, KeyObject = currentValue });
                    }
                    else
                    {
                        comaprizonResult.Add(new ComparizonObject() { DestinationMatchedIndex = -1, KeyObject = currentValue });
                    }
                }
                else
                {
                    comaprizonResult.Add(new ComparizonObject() { DestinationMatchedIndex = -1, KeyObject = currentValue });
                }
            }
            List<string> modifiedSourceString = new List<string>();
            //List<string> modifiedDestinationString = new List<string>();
            List<ComparizonObject> comaprizonResultNew = new List<ComparizonObject>();
            List<int> possibilityOfRemovedIndex = new List<int>();
            for (int i = 0; i < comaprizonResult.Count; i++)
            {
                ComparizonObject currentValue = comaprizonResult[i];
                if (currentValue.DestinationMatchedIndex > -1)
                {
                    if (comaprizonResultNew.Count > 0)
                    {
                        for (int p = comaprizonResultNew[comaprizonResultNew.Count - 1].DestinationMatchedIndex + 1; p < currentValue.DestinationMatchedIndex; p++)
                        {
                            string value = modifiedDestinationString[p];

                            if (modifiedDestinationString.FindAll(x => x == value).Count > 1)
                            {
                                // modifiedDestinationString.RemoveAt(comaprizonResultNew.Count - 1);
                                possibilityOfRemovedIndex.Add(p);
                            }
                        }
                    }
                    modifiedSourceString.Add(currentValue.KeyObject.Value);
                    comaprizonResultNew.Add(currentValue);
                }
                else
                {
                    if (comaprizonResultNew.Count > 0)
                    {
                        string value = modifiedDestinationString[comaprizonResultNew.Count - 1];

                        if (modifiedDestinationString.FindAll(x => x == value).Count > 1)
                        {
                            //modifiedDestinationString.RemoveAt(comaprizonResultNew.Count - 1);
                            possibilityOfRemovedIndex.Add(comaprizonResultNew.Count - 1);
                        }
                    }
                }
            }
            for (int rmIndex = possibilityOfRemovedIndex.Count - 1; rmIndex > -1; rmIndex--)
            {
                modifiedDestinationString.RemoveAt(possibilityOfRemovedIndex[rmIndex]);
            }
        }

        private class ComparizonObject
        {
            public int DestinationMatchedIndex { get; set; }

            public KeyObject KeyObject { get; set; }
        }

        private class KeyObject
        {
            internal int Index { get; private set; }
            internal string Value { get; private set; }

            public KeyObject(int index, string value)
            {
                Index = index;
                Value = value;
            }
        }
    }
}