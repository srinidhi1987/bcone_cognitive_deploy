#pragma once
#include <list>
#include <vector>
#include <opencv2/opencv.hpp>

class __declspec(dllexport) FileOperation
{
public:
	FileOperation();
	virtual ~FileOperation();

	void SaveImage(std::string pcImgPath, cv::Mat imgSrc);
	cv::Mat ReadImage(std::string pcImagePath);
	std::string GetTempFilePath();

private:
	//bool IsFileExistInCurrentDirectory(string strFilePath);
	//string GetDirName(string strFilePath);
	//string GetFileExtension(string strFilePath);
	//bool IsDirectoryExist(string strFilePath);
	//bool IsFileExist(string strFilePath);
	//bool IsFileSupported(string strFilePath);
};
