#pragma once
#include "mysql_driver.h"
#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
using namespace std;
struct stTableInfo
{
	std::string strHaskey;
	std::string strClassName;
	std::string strValue;
};
class CMySQLdb
{
	bool ConnectTodb();
	sql::Driver *m_driver;
	sql::Connection *m_con;
	sql::Statement *m_stmt;

public:
	CMySQLdb();
	virtual ~CMySQLdb();
	bool ContainsKey(std::string strHasKey);
	stTableInfo Get(std::string strHasKey);
	bool Insert(stTableInfo &sTableInfo);
	int GetNoOfClass();
};
