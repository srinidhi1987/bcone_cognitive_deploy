#include "stdafx.h"
#include "OpenCVWrapper.h"
#include "LayoutClassification.h"
using namespace std;
using namespace cv;

OpenCVWrapper::OpenCVWrapper()
{
}

OpenCVWrapper::~OpenCVWrapper()
{
}
cv::Mat OpenCVWrapper::GetClippedImage(cv::Mat &srcImg, cv::Rect RoiToClip)
{
	LOGGER->Trace("[OpenCVWrapper::GetClippedImage] Started...");
	Mat headerimg = srcImg(RoiToClip).clone();
	if (headerimg.data == NULL)
		throw new std::exception("OpenCVWrapper::GetClippedImage [Image Data is not found");
	LOGGER->Trace("[OpenCVWrapper::GetClippedImage] End...");
	return headerimg;
}
cv::Mat OpenCVWrapper::ApplyBlur(cv::Mat srcImg)
{
	LOGGER->Trace("<Debug> : [OpenCVWrapper::ApplyBlur] Started...");
	Mat imgBlur;
	try
	{
		imgBlur = srcImg.clone();
		cv::blur(srcImg, imgBlur, Size(7, 7));
	}
	catch (std::exception exc)
	{
		throw exc;
	}
	if (imgBlur.data == NULL)
		throw new std::exception("OpenCVWrapper::GetClippedImage [Image Data is not found");

	LOGGER->Trace("<Debug> : [OpenCVWrapper::ApplyBlur] End...");
	return imgBlur;
}
cv::Mat OpenCVWrapper::ApplyGrayScalling(cv::Mat srcImg)
{
	LOGGER->Trace("[OpenCVWrapper::ApplyGrayScalling] Started...");
	cv::Mat imgGray;
	try
	{
		imgGray = srcImg.clone();
		if (srcImg.channels() >= 3)
			cv::cvtColor(srcImg, imgGray, cv::COLOR_BGR2GRAY);
	}
	catch (std::exception exc)
	{
		throw exc;
	}
	if (imgGray.data == NULL)
		throw new std::exception("OpenCVWrapper::ApplyGrayScalling [Image Data is not found");

	LOGGER->Trace("[OpenCVWrapper::ApplyGrayScalling] End...");
	return imgGray;
}
cv::Mat OpenCVWrapper::ApplyThresholding(
	cv::Mat srcImg,
	int thresholdValue,
	int maxValue,
	int thresholdingType)
{
	LOGGER->Trace("[OpenCVWrapper::ApplyThresholding] Started...");
	cv::Mat imgThreshold;
	try {
		imgThreshold = srcImg.clone();
		threshold(srcImg, imgThreshold, thresholdValue, maxValue, thresholdingType);
	}
	catch (std::exception exc)
	{
		throw exc;
	}
	if (imgThreshold.data == NULL)
		throw new std::exception("OpenCVWrapper::ApplyThresholding [Image Data is not found");

	LOGGER->Trace("[OpenCVWrapper::ApplyThresholding] End...");
	return imgThreshold;
}