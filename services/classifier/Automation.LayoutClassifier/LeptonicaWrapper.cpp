#include "stdafx.h"
#include "LeptonicaWrapper.h"

using namespace std;
LeptonicaWrapper::LeptonicaWrapper()
{
}

LeptonicaWrapper::~LeptonicaWrapper()
{
}

PIX* LeptonicaWrapper::ReadImage(std::string imagePath)
{
	return pixRead(imagePath.c_str());
}

PIX* LeptonicaWrapper::ApplyThresholding(
	PIX* imagePix,
	int threshold)
{
	return pixConvertTo1(imagePix, threshold);
}
Boxa* LeptonicaWrapper::GetConnectedComponentBoundingBox(
	PIX *imagePix,
	int connectivity)
{
	return pixConnCompBB(imagePix, connectivity);
}
BOX_PTR LeptonicaWrapper::GetBoxFromBoundingBox(BOUNDING_BOX_PTR boundingBox, int index, int accessFlag)
{
	return boxaGetBox(boundingBox, index, accessFlag);
}
void LeptonicaWrapper::DestroyImage(PIX** imageToDestroy)
{
	if (imageToDestroy)
		pixDestroy(imageToDestroy);
}
void LeptonicaWrapper::DestroyBoundingBox(BOUNDING_BOX_PTR* boundingBoxToDestroy)
{
	boxaDestroy(boundingBoxToDestroy);
}
void LeptonicaWrapper::DestroyBox(BOX_PTR* boxToDestroy)
{
	boxDestroy(boxToDestroy);
}