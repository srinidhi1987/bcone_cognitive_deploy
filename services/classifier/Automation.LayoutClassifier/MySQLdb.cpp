#include "stdafx.h"
#include "MySQLdb.h"
#include "mysql_error.h"
#include <sstream>
#include <cppconn/prepared_statement.h>

CMySQLdb::CMySQLdb()
{
	ConnectTodb();
}

CMySQLdb::~CMySQLdb()
{
	m_con->close();
	if (m_con)
		delete m_con;
	cout << "DB Connection Closed..." << endl;
}
bool CMySQLdb::ConnectTodb()
{
	/* Create a connection */
	cout << "Getting DB Connection..." << endl;

	m_driver = get_driver_instance();
	m_con = m_driver->connect("tcp://192.168.2.59:3306", "root", "root");
	/* Connect to the MySQL test database */
	m_con->setSchema("featureset");
	cout << "Getting DB Connection End..." << endl;

	return true;
}
stTableInfo CMySQLdb::Get(std::string strHasKey)
{
	stTableInfo sTableInfo;
	if (m_con)
	{
		cout << "Getting Existing Record.." << endl;

		m_stmt = m_con->createStatement();
		std::stringstream strQuery;
		string strMsg;
		strQuery << "SELECT classname from docfeature where hashkey=" << "\"" << strHasKey << "\"";
		sql::ResultSet *res = m_stmt->executeQuery(strQuery.str()); // replace with your statement

		while (res->next()) {
			sTableInfo.strClassName = res->getString("classname");
		}
	}
	cout << "Getting Existing Record End.." << endl;

	return sTableInfo;
}
bool CMySQLdb::Insert(stTableInfo &sTableInfo)
{
	if (m_con)
	{
		cout << "Inserting New Record.." << endl;
		m_stmt = m_con->createStatement();
		std::stringstream strQueryStream;
		string strmsg = "";
		strQueryStream << "INSERT into docfeature" << "(" << "hashkey" << "," << "classname" << "," << "value" << ")" << " VALUES(" << "\"" << sTableInfo.strHaskey << "\"" << "," << "\"" << sTableInfo.strClassName << "\"" << "," << "\"" << sTableInfo.strValue << "\"" << ")";
		//try
		{
			//m_stmt->executeQuery(strQueryStream.str()); // replace with your statement
			sql::PreparedStatement *pstmt = m_con->prepareStatement(strQueryStream.str());
			pstmt->executeUpdate();
			cout << "Inserting New Record End.." << endl;
		}
		/*catch (exception ex)
		{
			strmsg = ex.what();
		}*/
	}
	return true;
}
bool CMySQLdb::ContainsKey(std::string strHasKey)
{
	try
	{
		Get(strHasKey);
	}
	catch (std::invalid_argument ex)
	{
		return false;
	}
	return true;
}
int CMySQLdb::GetNoOfClass()
{
	if (m_con)
	{
		cout << "Getting No Of Classes..." << endl;

		m_stmt = m_con->createStatement();
		std::stringstream strQuery;
		strQuery << "SELECT * from docfeature;";
		sql::ResultSet *res = m_stmt->executeQuery(strQuery.str()); // replace with your statement

		if (res == NULL)
		{
			return 0;
		}
		cout << "Getting No Of Classes End: " << res->rowsCount() << endl;

		return res->rowsCount();
	}
}