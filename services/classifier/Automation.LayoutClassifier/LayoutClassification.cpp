#include "stdafx.h"
#include "LayoutClassification.h"
#include "AAMath.h"

using namespace std;
using namespace cv;

typedef int(*GetClassNameForCurrHashkey)(const char* cHashKey, const char* connectionString, CPPLogger* logger);
CPPLogger *LayoutClassification::_cppLogger = NULL;
bool SortRect(cv::Rect &r1, cv::Rect &r2)
{
	return (r1.area() > r2.area());
};
bool SortRectByY(cv::Rect &r1, cv::Rect &r2)
{
	return (r1.y < r2.y);
};
bool SortRectByHeight(cv::Rect &r1, cv::Rect &r2)
{
	return (r1.height < r2.height);
};
LayoutClassification::LayoutClassification(CPPLoggerConfiguration loggerConfiguration)
{
	_cppLogger = CPPLogger::GetCppLogger(loggerConfiguration);
}

LayoutClassification::~LayoutClassification()
{
}

int LayoutClassification::GetClassNameOfCurrFile(
	BYTE *bData,
	int iWidth,
	int iHeight,
	const char* connectionString)
{
	int classId = -1;
	try {
		LOGGER->Trace("GetClassNameOfCurrFile started...");

		Mat img = GetImageFromByteArray(bData, iWidth, iHeight);

		Mat headerImage = GetHeaderImg(img);
		if (headerImage.rows == 0 || headerImage.cols == 0)
		{
			LOGGER->Error("<Error> : Header Image is not valid, image size is zero byte so returning -1");
			return -1;
		}

		stTrianlgeInfo sTriangleInfo = GetTriangleInfo(headerImage);
		classId = GetClassNameOfTriFromSqlServerdb(sTriangleInfo, connectionString);

		LOGGER->Trace("GetClassNameOfCurrFile End Successfully...");
	}
	catch (std::exception exc)
	{
		LOGGER->Error("<Exception> : LayoutClassification::GetClassNameOfCurrFile : [%s]", exc.what());
		classId = -1;
	}
	return classId;
}

Mat LayoutClassification::GetImageFromByteArray(
	BYTE *bData,
	int iWidth,
	int iHeight)
{
	Mat img = Mat(iHeight, iWidth, CV_8UC3, bData);
	img.reshape(1, iHeight);
	if (img.data == NULL)
		throw new std::exception("LayoutClassification::GetImageFromByteArray [Image data is not found]");
	return img;
}

cv::Mat LayoutClassification::GetHeaderImg(
	Mat img,
	int iImagePercent)
{
	LOGGER->Trace("[LayoutClassification::GetHeaderImg] Started...");

	cv::Rect ROI = GetHeaderROI(img, iImagePercent);
	int padding = 5;
	ROI = AddPaddingAndGetValidatedROI(ROI, padding, img.rows, img.cols);
	Mat RetImg = _openCVWrapper.GetClippedImage(img, ROI);

	LOGGER->Trace("[LayoutClassification::GetHeaderImg] End...");
	return RetImg;
}
cv::Rect LayoutClassification::GetHeaderROI(
	Mat srcImg,
	int headerPercentage)
{
	int iPadding = 25;
	int iWidthPercet = 95;
	cv::Rect initialROI;
	initialROI.x = iPadding;
	initialROI.y = iPadding;
	initialROI.width = srcImg.cols*iWidthPercet / 100.0f;
	initialROI.height = srcImg.rows*headerPercentage / 100.0f;

	Mat headerimg = _openCVWrapper.GetClippedImage(srcImg, initialROI);

	headerimg = _openCVWrapper.ApplyBlur(headerimg);
	int thresholdValue = 0;
	int maxValue = 255;
	int thresholdingType = THRESH_BINARY + THRESH_OTSU;
	headerimg = _openCVWrapper.ApplyGrayScalling(headerimg);
	headerimg = _openCVWrapper.ApplyThresholding(headerimg, thresholdValue, maxValue, thresholdingType);

	std::vector<cv::Rect> segmentedRects = GetSegmentedRectFrom(headerimg);
	if (segmentedRects.size() <= 0)
		return initialROI;
	sort(segmentedRects.begin(), segmentedRects.end(), SortRectByY);

	cv::Rect firstRect = segmentedRects[0];
	firstRect.y = initialROI.y + firstRect.y;
	firstRect.x = initialROI.x + firstRect.x;
	int noOfValidRect = 4;
	cv::Rect finalROI = ShiftROIifHeaderHasnotEnoughValidRects(initialROI, segmentedRects, initialROI.width, initialROI.height, noOfValidRect);
	if (finalROI.y == initialROI.y)
		finalROI.y = firstRect.y;

	return finalROI;
}
cv::Rect LayoutClassification::ShiftROIifHeaderHasnotEnoughValidRects(
	cv::Rect initialROI,
	std::vector<cv::Rect> rectsInHeader,
	int initialROIWidth,
	int initialROIHeight,
	int noOfValidateRects)
{
	LOGGER->Trace("[LayoutClassification::ShiftROIifHeaderHasnotEnoughValidRect] Started...");

	int validRects = 0;
	for (int i = 0; i < rectsInHeader.size(); i++)
	{
		if (rectsInHeader[i].width < initialROIWidth*0.6 && rectsInHeader[i].width > initialROIWidth*0.05)
			validRects++;

		if (validRects > noOfValidateRects)
			break;
	}
	if (validRects < noOfValidateRects)
	{
		initialROI.y = initialROI.y + initialROIHeight;
		LOGGER->Trace("[LayoutClassification::ShiftROIifHeaderHasnotEnoughValidRect] ROI Top Changed...");
	}
	LOGGER->Trace("[LayoutClassification::ShiftROIifHeaderHasnotEnoughValidRect] End...");
	return initialROI;
}
cv::Rect LayoutClassification::ShiftROIToFirstRectTop(
	cv::Rect initialROI,
	cv::Rect firstRect)
{
	LOGGER->Trace("[LayoutClassification::ShiftROIToFirstRectTop] Started...");

	int shiftedY = firstRect.y;
	if (shiftedY <= 0 && firstRect.y > 0)
		initialROI.y = firstRect.y;
	else
		initialROI.y = shiftedY;

	LOGGER->Trace("[LayoutClassification::ShiftROIToFirstRectTop] End...");
	return initialROI;
}
cv::Rect LayoutClassification::AddPaddingAndGetValidatedROI(
	cv::Rect ROI,
	int padding,
	int imageRows,
	int imageCols)
{
	LOGGER->Trace("[LayoutClassification::AddPaddingAndGetValidatedROI] Started...");

	if (ROI.y > padding)
		ROI.y = ROI.y - padding;

	if ((ROI.y + ROI.height) > imageRows)
		ROI.height = (imageRows - ROI.y - (padding * 2));
	if ((ROI.x + ROI.width) > imageCols)
		ROI.width = imageCols - ROI.x - (padding * 2);

	LOGGER->Trace("[LayoutClassification::AddPaddingAndGetValidatedROI] End...");
	return ROI;
}
stTrianlgeInfo LayoutClassification::GetTriangleInfo(cv::Mat srcImage)
{
	try {
		LOGGER->Trace("LayoutClassification::GetTriangleInfo Started...");

		cv::Mat inputImage = srcImage.clone();
		stTrianlgeInfo sTriagnleInfo;

		std::vector<cv::Rect> segmentedRects = GetSegmentedRectFrom(srcImage);
		if (segmentedRects.size() == 0)
			return sTriagnleInfo;

		int middleRectHeight = GetHeightOfMiddleRect(segmentedRects);
		Mat kernelImage = GetKernelAccordingToHeight(middleRectHeight);

		Mat kernelAppliedImage = ApplyKernelOn(inputImage, kernelImage);

		int thresholdValue = 0;
		int maxValue = 255;
		int thresholdingType = CV_THRESH_BINARY | CV_THRESH_OTSU;
		kernelAppliedImage = _openCVWrapper.ApplyThresholding(kernelAppliedImage, thresholdValue, maxValue, thresholdingType);

		segmentedRects.erase(segmentedRects.begin(), segmentedRects.begin() + segmentedRects.size());
		segmentedRects = GetSegmentedRectFrom(kernelAppliedImage);
		if (segmentedRects.size() == 0)
			return sTriagnleInfo;

		int sensitivity = kernelImage.rows * 2;
		MergeOverlappedRects(segmentedRects, inputImage, sensitivity);

		sort(segmentedRects.begin(), segmentedRects.end(), SortRect);

		AAMath aaMath;
		GetCenterPointOfTop3ValidateRect(segmentedRects, sTriagnleInfo.ptVertex);

		sTriagnleInfo.ptVector[0] = aaMath.GetVectorFromPoints(sTriagnleInfo.ptVertex[0], sTriagnleInfo.ptVertex[1]);
		sTriagnleInfo.ptVector[1] = aaMath.GetVectorFromPoints(sTriagnleInfo.ptVertex[1], sTriagnleInfo.ptVertex[2]);
		sTriagnleInfo.ptVector[2] = aaMath.GetVectorFromPoints(sTriagnleInfo.ptVertex[2], sTriagnleInfo.ptVertex[0]);

		for (int vectorIndex = 0; vectorIndex < 3; vectorIndex++)
		{
			sTriagnleInfo.fDirection[vectorIndex] = aaMath.GetDirectionOf(sTriagnleInfo.ptVector[vectorIndex]);

			sTriagnleInfo.ptUnitVector[vectorIndex] = aaMath.GetUnitVectorOf(sTriagnleInfo.ptVector[vectorIndex]);
		}

		float fAngles[3];
		LOGGER->Trace("Getting Angle of Triangle...");
		aaMath.GetAnglesOfTriangleFromPoints(sTriagnleInfo.ptVertex, sTriagnleInfo.fAngles);
		LOGGER->Trace("Getting Angle of Triangle End...");

		//Below commented line is to use for debugging purpose
		//DrawRectAndTriangleOn(srcImage, segmentedRects, sTriagnleInfo);

		LOGGER->Trace("Generating HashKey...");
		sTriagnleInfo.strHasKey = GenerateHasKeyForCurrTriangle(sTriagnleInfo);
		LOGGER->Trace("Generating HashKey End...");

		LOGGER->Trace("LayoutClassification::GetTriangleInfo End...");
		return sTriagnleInfo;
	}
	catch (Exception exc)
	{
		LOGGER->Error("<Exception>LayoutClassification::GetTriangleInfo : [%s]", exc.msg);
	}
}

int LayoutClassification::GetHeightOfMiddleRect(std::vector<cv::Rect> segmentedRects)
{
	std::vector<int>height;
	sort(segmentedRects.begin(), segmentedRects.end(), SortRectByHeight);
	int middleRectHeight = segmentedRects[segmentedRects.size() / 2].height;

	return middleRectHeight;
}

cv::Mat LayoutClassification::GetKernelAccordingToHeight(int height)
{
	Mat kernelImage = Mat::zeros(3, 7, CV_8UC3);
	if (height > 10)
	{
		resize(kernelImage, kernelImage, Size(height*0.75, kernelImage.rows));
	}

	return kernelImage;
}

void LayoutClassification::MergeOverlappedRects(
	std::vector<cv::Rect> &inputBoxes,
	cv::Mat image,
	int sensitivity)
{
	LOGGER->Trace("LayoutClassification::MergeOverlappedRects Started...");

	for (int i = 0; i < inputBoxes.size(); i++)
	{
		inputBoxes[i] = AdjustRectHeightToMerge(inputBoxes[i], image.rows, sensitivity);
	}
	cv::Mat maskImage = GetMaskImageOfMergedRects(inputBoxes, image.rows, image.cols);

	inputBoxes.erase(inputBoxes.begin(), inputBoxes.begin() + inputBoxes.size());
	inputBoxes = GetBoundingRect(maskImage);

	LOGGER->Trace("LayoutClassification::MergeOverlappedRects End...");
}

cv::Rect LayoutClassification::AdjustRectHeightToMerge(
	cv::Rect inputBoxes,
	int imageRows,
	int sensitivity)
{
	cv::Rect box;
	box = inputBoxes;

	box.y = inputBoxes.y - sensitivity / 2.0f;
	if (box.y < 0)
		box.y = inputBoxes.y;
	box.height = inputBoxes.height + (sensitivity);
	if ((box.y + box.height) > imageRows)
		box.height = imageRows - box.y;

	return box;
}

cv::Mat LayoutClassification::GetMaskImageOfMergedRects(
	std::vector<cv::Rect> &inputBoxes,
	int imageRows,
	int imageCols)
{
	cv::Mat mask = GetBlackImage(imageRows, imageCols);

	for (int i = 0; i < inputBoxes.size(); i++)
	{
		if (inputBoxes[i].width < imageCols*0.6)
			DrawFilledRectangleOn(mask, inputBoxes[i]);
	}
	return mask;
}

cv::Mat LayoutClassification::GetBlackImage(
	int imageRows,
	int imageCols)
{
	return cv::Mat::zeros(imageRows, imageCols, CV_8UC1);
}

void LayoutClassification::DrawFilledRectangleOn(
	cv::Mat image,
	cv::Rect rect)
{
	int lineThickness = CV_FILLED;
	int lineType = 34;
	int shift = 0;
	cv::rectangle(image, rect, cv::Scalar(255), lineThickness, lineType, shift);
}

vector<cv::Rect> LayoutClassification::GetBoundingRect(cv::Mat maskImage)
{
	try {
		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Rect> mergedRects;

		cv::findContours(maskImage, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
		for (int j = 0; j < contours.size(); j++)
		{
			cv::Rect rect = cv::boundingRect(contours.at(j));
			if (rect.width < maskImage.cols*0.6)
				mergedRects.push_back(rect);
		}

		return mergedRects;
	}
	catch (cv::Exception ex)
	{
		//LOGGER->Error("LayoutClassification::GetBoundingRect: " + ex.msg);
	}
}

void LayoutClassification::GetCenterPointOfTop3ValidateRect(
	vector<cv::Rect> inputRect,
	cv::Point2f ptCenters[])
{
	LOGGER->Trace("LayoutClassification::GetCenterPointOfTop3ValidateRect Started...");

	bool bFound = false;
	int i = 0;
	int rectIndex = 0;
	AAMath aaMath;
	do
	{
		if (rectIndex >= inputRect.size() || i > 2)
			break;

		ptCenters[i].x = inputRect[rectIndex].x + inputRect[rectIndex].width / 2.0f;
		ptCenters[i].y = inputRect[rectIndex].y + inputRect[rectIndex].height / 2.0f;
		if (i == 2 && IsAllCentersAreOnOneLine(ptCenters) == false)
			break;
		else if (i < 2)
			i++;

		rectIndex++;
	} while (i);

	LOGGER->Trace("LayoutClassification::GetCenterPointOfTop3ValidateRect End...");
}

bool LayoutClassification::IsAllCentersAreOnOneLine(cv::Point2f centers[])
{
	float fAngles[3];
	AAMath aaMath;
	aaMath.GetAnglesOfTriangleFromPoints(centers, fAngles);

	if (fAngles[0] > 177.0f ||
		fAngles[1] > 177.0f ||
		fAngles[2] > 177.0f)
		return true;

	return false;
}

string LayoutClassification::GenerateHasKeyForCurrTriangle(stTrianlgeInfo &sCurrTrianle)
{
	//UnitVector Haskey
	try {
		string strUnitVectorKey = "";
		int iUnitKey[6] = { 0 };
		for (int i = 0; i < 3; i++)
		{
			int iKeyIndex = i * 2;
			iUnitKey[iKeyIndex] = abs(sCurrTrianle.ptUnitVector[i].x / 0.2f);
			if (sCurrTrianle.ptUnitVector[0].x > 0.0f)
			{
				iUnitKey[iKeyIndex] = iUnitKey[iKeyIndex] + 10;
			}

			iKeyIndex = iKeyIndex + 1;
			iUnitKey[iKeyIndex] = abs(sCurrTrianle.ptUnitVector[i].y / 0.2f);
			if (sCurrTrianle.ptUnitVector[0].y < 0.0f)
			{
				iUnitKey[iKeyIndex] = iUnitKey[iKeyIndex] + 10;
			}
		}

		int iAngDirKey[6] = { 0 };
		for (int n = 0; n < 3; n++)
		{
			int iKeyIndex = n * 2;
			iAngDirKey[iKeyIndex] = abs(sCurrTrianle.fAngles[n] / 10.0f);

			int oddIndex = iKeyIndex + 1;
			iAngDirKey[oddIndex] = abs(sCurrTrianle.fDirection[n] / 10.0f);
		}

		string strStream = "";
		for (int i = 0; i < 6; i++)
			strStream += std::to_string(iUnitKey[i]);
		stringstream strStreamANgleDir;
		for (int m = 0; m < 6; m++)
		{
			strStream += std::to_string(iAngDirKey[m]);
		}

		LOGGER->Trace("GenerateHasKeyForCurrTriangle()  End");
		//	LOGGER->Trace("GenerateHasKeyForCurrTriangle()  Key: [%s]", strStream);
		return strStream;
	}
	catch (Exception ex)
	{
		LOGGER->Error("<Exception> LayoutClassification::GenerateHasKeyForCurrTriangle [%s]", ex.msg);
	}
}
void LayoutClassification::DrawRectAndTriangleOn(
	cv::Mat srcImage,
	vector<cv::Rect> segmentedRects,
	stTrianlgeInfo sTriagnleInfo)
{
	if (true)
	{
		cv::Mat colorImg = srcImage.clone();
		if (colorImg.channels() < 3)
			cv::cvtColor(colorImg, colorImg, CV_GRAY2BGR);
		for (int i = 0; i < 3; i++)
		{
			cv::rectangle(colorImg, segmentedRects[i], cv::Scalar(255, 0, 0), 2);
		}
		cv::line(colorImg, sTriagnleInfo.ptVertex[0], sTriagnleInfo.ptVertex[1], cv::Scalar(255, 0, 0), 2);
		cv::line(colorImg, sTriagnleInfo.ptVertex[1], sTriagnleInfo.ptVertex[2], cv::Scalar(0, 255, 0), 2);
		cv::line(colorImg, sTriagnleInfo.ptVertex[2], sTriagnleInfo.ptVertex[0], cv::Scalar(0, 255, 255), 2);
		std::stringstream strstream;
		strstream << "TriAngleImage.png";
		imwrite(strstream.str(), colorImg);
	}
}
int LayoutClassification::GetClassNameOfTriFromSqlServerdb(stTrianlgeInfo &sTriInfo, const char* connectionString)
{
	LOGGER->Trace("LayoutClassification::GetClassNameOfTri Started...");

	HMODULE dbDll = LoadLibrary(_T("Automation.ClassifierDBManagerCpp.dll"));
	GetClassNameForCurrHashkey getClassname;
	int iClassName = -1;
	try {
		getClassname = (GetClassNameForCurrHashkey)GetProcAddress(dbDll, "GetLayoutClassName");
		if (getClassname)
		{
			iClassName = getClassname(sTriInfo.strHasKey.c_str(), connectionString, LOGGER);
		}
	}
	catch (Exception ex)
	{
		LOGGER->Error("<Exception>LayoutClassification::GetClassNameOfTri [%s]", ex.msg);
	}

	LOGGER->Trace("LayoutClassification::GetClassNameOfTri End...");

	return iClassName;
}
int LayoutClassification::GetClassNameFromMysqldb(stTrianlgeInfo &sTriInfo)
{
	/*
	CMySQLdb cMysqldb;
	stTableInfo sTableInfo;

	{
	sTableInfo.strHaskey = sTriInfo.strHasKey;
	std::stringstream strClassStream;
	strClassStream << cMysqldb.GetNoOfClass() + 1;
	sTableInfo.strClassName = strClassStream.str();
	strClassStream.str("");
	strClassStream << sTriInfo.ptUnitVector[0].x << "," << sTriInfo.ptUnitVector[0].y << ","
	<< sTriInfo.ptUnitVector[1].x << "," << sTriInfo.ptUnitVector[1].y << ","
	<< sTriInfo.ptUnitVector[2].x << "," << sTriInfo.ptUnitVector[2].y << ","
	<< sTriInfo.fAngles[0] << "," << sTriInfo.fAngles[1] << "," << sTriInfo.fAngles[2] << ","
	<< sTriInfo.fDirection[0] << "," << sTriInfo.fDirection[1] << "," << sTriInfo.fDirection[2];
	sTableInfo.strValue = strClassStream.str();
	try
	{
	cMysqldb.Insert(sTableInfo);
	}
	catch (sql::SQLException ex)
	{
	string strMsg = ex.what();
	sTableInfo = cMysqldb.Get(sTriInfo.strHasKey);
	}
	}
	return sTableInfo.strClassName;*/
	return 0;
}
vector<Rect> LayoutClassification::GetSegmentedRectFrom(cv::Mat image)
{
	LOGGER->Trace("CImageOPBase::segmentAndReturnRect Started...");

	PIX * orgImage = ConvertMat_TO_LeptonicaPix(image);

	int threshold = 250;
	PIX * thresholdImage = _leptonicaWrapper.ApplyThresholding(orgImage, threshold);

	int connectivity = 4;
	BOUNDING_BOX_PTR boundingBox = _leptonicaWrapper.GetConnectedComponentBoundingBox(thresholdImage, connectivity);

	vector<Rect> segmentedRect = GetRectFrom(boundingBox);

	_leptonicaWrapper.DestroyImage(&orgImage);
	_leptonicaWrapper.DestroyImage(&thresholdImage);
	_leptonicaWrapper.DestroyBoundingBox(&boundingBox);

	LOGGER->Trace("LayoutClassification::segmentAndReturnRect End...");

	return segmentedRect;
}
std::vector<cv::Rect> LayoutClassification::GetRectFrom(BOUNDING_BOX_PTR boundingBox)
{
	LOGGER->Trace("<Debug> : CImageOPBase::GetRectFromBoundingBox Started...");

	vector<Rect> segmentedRect;
	if (boundingBox == NULL)
	{
		LOGGER->Error(" : CImageOPBase::segmentAndReturnRect Error in getting bounding box...");
	}
	else
	{
		for (int i = 0; i < boundingBox->n; i++) {
			int accessFlag = L_CLONE;
			int index = i;
			BOX* box = _leptonicaWrapper.GetBoxFromBoundingBox(boundingBox, index, accessFlag);//TODO: make these box function in more like english naming
			Rect r(box->x, box->y, box->w, box->h);
			segmentedRect.push_back(r);

			_leptonicaWrapper.DestroyBox(&box);
		}
	}
	LOGGER->Trace("CImageOPBase::GetRectFromBoundingBox End...");

	return segmentedRect;
}
PIX* LayoutClassification::ConvertMat_TO_LeptonicaPix(cv::Mat image)
{
	LOGGER->Trace("CImageOPBase::ConvertMat_TO_LeptonicaPix Start...");
	try {
		FileOperation fileOperation;
		std::string tempFilepath = fileOperation.GetTempFilePath();
		fileOperation.SaveImage(tempFilepath, image);
		PIX *imagePix = _leptonicaWrapper.ReadImage(tempFilepath.c_str());
		std::remove(tempFilepath.c_str());
		LOGGER->Trace("CImageOPBase::ConvertMat_TO_LeptonicaPix End...");
		return imagePix;
	}
	catch (std::exception exc)
	{
		throw new std::exception(std::string("Error in LayoutClassification::ConvertMat_TO_LeptonicaPix(): ").append(exc.what()).c_str());
	}
}
cv::Mat LayoutClassification::ApplyKernelOn(
	cv::Mat srcImage,
	cv::Mat kernel)
{
	cv::Mat inputImage = srcImage.clone();

	if (inputImage.channels() >= 3)
		cv::cvtColor(inputImage, inputImage, cv::COLOR_BGR2GRAY);

	bitwise_not(inputImage, inputImage);

	if (kernel.channels() >= 3)
		cvtColor(kernel, kernel, CV_BGR2GRAY);
	threshold(kernel, kernel, 100, 255, 0);
	bitwise_not(kernel, kernel);
	dilate(inputImage, inputImage, kernel, cv::Point(-1, -1));
	bitwise_not(inputImage, inputImage);
	return inputImage;
}
CPPLogger*  LayoutClassification::GetLogger()
{
	if (_cppLogger)
		return _cppLogger;
	else
	{
		cout << "CPPLogger is NULL, possibly not initalizing properly" << endl;
		throw new std::exception("CPPLogger is NULL, possibly not initalizing properly");
	}
}