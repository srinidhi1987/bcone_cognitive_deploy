#include "stdafx.h"
#include "AAMath.h"

using namespace std;
using namespace cv;

AAMath::AAMath()
{
}

AAMath::~AAMath()
{
}
double AAMath::GetCosineSimilarity(std::vector<float> V1, std::vector<float> V2)
{
	double sim = 0.0f;
	int N = 0;
	N = ((V2.size() < V1.size()) ? V2.size() : V1.size());
	double dot = 0.0f;
	double mag1 = 0.0f;
	double mag2 = 0.0f;
	for (int n = 0; n < N; n++)
	{
		dot += V1[n] * V2[n];
		mag1 += pow(V1[n], 2);
		mag2 += pow(V2[n], 2);
	}

	double dCostheta = dot / (sqrt(mag1) * sqrt(mag2));
	return dCostheta;
}
float AAMath::GetCosineSimilarity(cv::Point2f UnitV1, cv::Point2f UnitV2)
{
	float fDotP = (UnitV1.x * UnitV2.x) + (UnitV1.y * UnitV2.y);
	float fa = sqrt((UnitV1.x*UnitV1.x) + (UnitV1.y*UnitV1.y));
	float fb = sqrt((UnitV2.x*UnitV2.x) + (UnitV2.y*UnitV2.y));

	float fCosTheta = ((fa*fb) / (fDotP));

	return fCosTheta;
}
cv::Point2f AAMath::GetUnitVectorFromPoints(cv::Point2f pt1, cv::Point2f pt2)
{
	cv::Point2f ptVector;
	float fL = sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
	ptVector.x = (pt1.x - pt2.x) / fL;
	ptVector.y = (pt1.y - pt2.y) / fL;
	return ptVector;
}
cv::Point2f AAMath::GetUnitVectorOf(cv::Point2f currentVector)
{
	cv::Point2f ptUnitVector;
	float fL = sqrt((currentVector.x*currentVector.x) + (currentVector.y*currentVector.y));
	ptUnitVector.x = currentVector.x / fL;
	ptUnitVector.y = currentVector.y / fL;

	return ptUnitVector;
}
float AAMath::GetDirectionOf(cv::Point2f currentVector)
{
	float fDirection = 0.0f;
	if (fabs(currentVector.x) <= 0.00001f)
		fDirection = 90.0f;
	else
		fDirection = RADTODEG(atan(currentVector.y / currentVector.x));

	if (fDirection < 0.0f)
		fDirection = 360.0f + fDirection;

	return fDirection;
}
cv::Point2f AAMath::GetVectorFromPoints(cv::Point2f pt1, cv::Point2f pt2)
{
	cv::Point2f ptVector;
	ptVector.x = (pt2.x - pt1.x);
	ptVector.y = (pt2.y - pt1.y);

	return ptVector;
}
void AAMath::GetAnglesOfTriangleFromPoints(cv::Point2f ptTriangle[3], float fAngle[])
{
	float a, b, c;
	a = sqrt((ptTriangle[1].x - ptTriangle[2].x)*(ptTriangle[1].x - ptTriangle[2].x) + (ptTriangle[1].y - ptTriangle[2].y)*(ptTriangle[1].y - ptTriangle[2].y));
	b = sqrt((ptTriangle[2].x - ptTriangle[0].x)*(ptTriangle[2].x - ptTriangle[0].x) + (ptTriangle[2].y - ptTriangle[0].y)*(ptTriangle[2].y - ptTriangle[0].y));
	c = sqrt((ptTriangle[0].x - ptTriangle[1].x)*(ptTriangle[0].x - ptTriangle[1].x) + (ptTriangle[0].y - ptTriangle[1].y)*(ptTriangle[0].y - ptTriangle[1].y));

	float A = (acos(((b*b) + (c*c) - (a*a)) / (2 * b*c)));
	float B = (acos(((a*a) + (c*c) - (b*b)) / (2 * a*c)));
	float C = (acos(((a*a) + (b*b) - (c*c)) / (2 * a*b)));

	//Trianlge Rule: A+B+C = 180
	fAngle[0] = RADTODEG(A);
	fAngle[1] = RADTODEG(B);
	fAngle[2] = RADTODEG(C);
}