//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Automation.LayoutClassifier.rc
//
#define IDB_BITMAP_CROSS                111
#define IDB_BITMAP_LEFT_T               112
#define IDB_BITMAP_RIGHT_T              113
#define IDB_BITMAP_REVERSE_T            114
#define IDB_BITMAP_T                    115
#define IDB_BITMAP_LEFTBOTTOM           116
#define IDB_BITMAP_RIGHTBOTTOM          117
#define IDB_BITMAP_LEFTTOP              118
#define IDB_BITMAP_LEFTTOP1             119
#define IDB_BITMAP_RIGHTTOP             119
#define IDB_BITMAP_CENTERL              120
#define IDB_BITMAP_CENTERL1             121
#define IDB_BITMAP_CENTERL2             122
#define IDB_BITMAP_CENTERL3             123

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
