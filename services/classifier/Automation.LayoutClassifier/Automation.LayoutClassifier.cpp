// OpenCVTestApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "resource.h"
#include "windows.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <list>
#include "ImageOPBase.h"
#include "LayoutClassification.h"
#include "FileOperation.h"

using namespace std;
using namespace cv;

extern "C"
{
	int _declspec(dllexport) GetLayoutClassificationId(const char *documentPath, const char* connectionString, CPPLoggerConfiguration loggerConfiguration)
	{
		int classId = -1;
		try {
			FileOperation fileOperation;
			Mat img = fileOperation.ReadImage(documentPath);

			LayoutClassification layoutClassification(loggerConfiguration);
			classId = layoutClassification.GetClassNameOfCurrFile(img.data, img.cols, img.rows, connectionString);
		}
		catch (Exception ex)
		{
			LOGGER->Error("<Exception> LayoutClassification::GetLayoutClassificationId [%s]", ex.msg);
			return classId;
		}
		return classId;
	}
}

eLogLevel GetLogLevel(std::string logLevel)
{
	std::transform(logLevel.begin(), logLevel.end(), logLevel.begin(), ::tolower);
	if (logLevel == "off")
		return eLogLevel::OFF_LEVEL;
	else if (logLevel == "error")
		return eLogLevel::ERROR_LEVEL;
	else if (logLevel == "info")
		return eLogLevel::INFO_LEVEL;
	else if (logLevel == "warn")
		return eLogLevel::WARNING_LEVEL;
	else if (logLevel == "debug")
		return eLogLevel::DEBUG_LEVEL;
	else if (logLevel == "trace")
		return eLogLevel::TRACE_LEVEL;
	else if (logLevel == "all")
		return eLogLevel::ALL_LEVEL;
	else
		return eLogLevel::ALL_LEVEL;
}

int main(int argc, char** argv)
{
	int exitCode = -1;
	int classId = -2;
	try {
		char* filePath = "";
		if (argc > 1)
			filePath = argv[1];

		char* connectionString = "";
		if (argc > 2)
			connectionString = argv[2];

		CPPLoggerConfiguration loggerConfiguration;
		loggerConfiguration.eLoggerType = eLoggerType::LOG4CPlus;
		loggerConfiguration.elogLevel = eLogLevel::ERROR_LEVEL;
		loggerConfiguration.logFilePath = "LayoutClassifierLog.txt";
		loggerConfiguration.transactionID = "Layout-Classifier";
		if (argc > 3)
			loggerConfiguration.logFilePath = argv[3];
		cout << " logfile Path:" << loggerConfiguration.logFilePath << endl;
		if (argc > 4)
		{
			loggerConfiguration.elogLevel = GetLogLevel(argv[4]);
			cout << " log Level:" << argv[4] << endl;
		}
		if (argc > 5)
			loggerConfiguration.transactionID = argv[5];
		cout << " Transaction ID:" << loggerConfiguration.transactionID << endl;
		LayoutClassification layoutClassification(loggerConfiguration);

		string sRet = "";
		LOGGER->Trace("Layout Classifier Started...");
		FileOperation fileOperation;
		Mat img = fileOperation.ReadImage(filePath);
		cout << " Reading file End" << endl;
		classId = layoutClassification.GetClassNameOfCurrFile(img.data, img.cols, img.rows, connectionString);
		LOGGER->Trace("Layout Classifier Exited...");
		exitCode = 0;
	}
	catch (std::exception ex)
	{
		LOGGER->Error("<Exception> : LayoutClassification::GetLayoutClassificationId [%s]", ex.what());
		classId = -2;
	}
	LOGGER->ShutDown();
	cout << "LAYOUTCLASS:" << classId << ":LAYOUTCLASS";
	return exitCode;
}