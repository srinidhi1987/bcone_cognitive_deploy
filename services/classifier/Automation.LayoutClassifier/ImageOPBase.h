#pragma once
#include "stdafx.h"
#include "resource.h"
#include "windows.h"
#include <iostream>
#include <list>
#include <vector>
#include "LeptonicaWrapper.h"
#include "OpenCVWrapper.h"
#include "FileOperation.h"

using namespace std;
using namespace cv;

class CImageOPBase
{
public:
	CImageOPBase();
	virtual ~CImageOPBase();
	FileOperation _fileOperation;
	LeptonicaWrapper _leptonicaWrapper;
	OpenCVWrapper _openCVWrapper;

	virtual int GetClassNameOfCurrFile(BYTE *bData, int iWidth, int iHeight);
protected:
	virtual vector<Rect> GetSegmentedRectFrom(cv::Mat image);
	virtual cv::Mat CImageOPBase::ApplyKernelOn(Mat srcImage, Mat kernel);
	PIX* ConvertMat_TO_LeptonicaPix(cv::Mat image);
	std::vector<cv::Rect> GetRectFrom(BOUNDING_BOX_PTR boundingBox);
};
