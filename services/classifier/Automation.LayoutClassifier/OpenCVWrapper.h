#pragma once
#include "opencv2\opencv.hpp"
#include "opencv2/highgui.hpp"

class __declspec(dllexport) OpenCVWrapper
{
public:
	OpenCVWrapper();
	virtual ~OpenCVWrapper();
	cv::Mat GetClippedImage(cv::Mat &srcImg, cv::Rect RoiToClip);
	cv::Mat ApplyBlur(cv::Mat srcImg);
	cv::Mat ApplyGrayScalling(cv::Mat srcImg);
	cv::Mat ApplyThresholding(cv::Mat srcImg, int thresholdValue, int maxValue, int thresholdingType);
};
