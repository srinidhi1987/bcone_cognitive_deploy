#pragma once
#include "opencv2\opencv.hpp"

#define DEGTORAD(d) (d*3.1452/180.0f)
#define RADTODEG(d) (d*180.0f/3.1452)
class AAMath
{
public:
	AAMath();
	~AAMath();
	float GetCosineSimilarity(cv::Point2f UnitV1, cv::Point2f UnitV2);
	double GetCosineSimilarity(std::vector<float> V1, std::vector<float> V2);
	cv::Point2f GetUnitVectorFromPoints(cv::Point2f pt1, cv::Point2f pt2);

	cv::Point2f GetVectorFromPoints(cv::Point2f pt1, cv::Point2f pt2);
	cv::Point2f GetUnitVectorOf(cv::Point2f currentVector);
	float GetDirectionOf(cv::Point2f currentVector);
	void GetAnglesOfTriangleFromPoints(cv::Point2f[3], float fAngle[]);
};
