#include "stdafx.h"
#include "ImageOPBase.h"
#include <algorithm>
#include <leptonica\allheaders.h>
#include "boost\filesystem.hpp"
#include "Logger.h"
#include "FileOperation.h"

using namespace boost::filesystem;

CImageOPBase::CImageOPBase()
{
}

CImageOPBase::~CImageOPBase()
{
}
vector<Rect> CImageOPBase::GetSegmentedRectFrom(cv::Mat image)
{
	LOGGER->Log("<Debug> : CImageOPBase::segmentAndReturnRect Started...");

	PIX * orgImage = ConvertMat_TO_LeptonicaPix(image);

	int threshold = 250;
	PIX * thresholdImage = _leptonicaWrapper.ApplyThresholding(orgImage, threshold);

	int connectivity = 4;
	BOUNDING_BOX_PTR boundingBox = _leptonicaWrapper.GetConnectedComponentBoundingBox(thresholdImage, connectivity);

	vector<Rect> segmentedRect = GetRectFrom(boundingBox);

	_leptonicaWrapper.DestroyImage(&orgImage);
	_leptonicaWrapper.DestroyImage(&thresholdImage);
	_leptonicaWrapper.DestroyBoundingBox(&boundingBox);

	LOGGER->Log("<Debug> : OpenCVLogoDetection::segmentAndReturnRect End...");

	return segmentedRect;
}
std::vector<cv::Rect> CImageOPBase::GetRectFrom(BOUNDING_BOX_PTR boundingBox)
{
	LOGGER->Log("<Debug> : CImageOPBase::GetRectFromBoundingBox Started...");

	vector<Rect> segmentedRect;
	if (boundingBox == NULL)
	{
		LOGGER->Log("<Error> : CImageOPBase::segmentAndReturnRect Error in getting bounding box...");
	}
	else
	{
		for (int i = 0; i < boundingBox->n; i++) {
			int accessFlag = L_CLONE;
			int index = i;
			BOX* box = _leptonicaWrapper.GetBoxFromBoundingBox(boundingBox, index, accessFlag);//TODO: make these box function in more like english naming
			Rect r(box->x, box->y, box->w, box->h);
			segmentedRect.push_back(r);

			_leptonicaWrapper.DestroyBox(&box);
		}
	}
	LOGGER->Log("<Debug> : CImageOPBase::GetRectFromBoundingBox End...");

	return segmentedRect;
}
PIX* CImageOPBase::ConvertMat_TO_LeptonicaPix(cv::Mat image)
{
	FileOperation fileOperation;
	string tempFilepath = fileOperation.GetTempFilePath();
	fileOperation.SaveImage(tempFilepath, image);

	return _leptonicaWrapper.ReadImage(tempFilepath.c_str());
}
cv::Mat CImageOPBase::ApplyKernelOn(
	Mat srcImage,
	Mat kernel)
{
	Mat inputImage = srcImage.clone();

	if (inputImage.channels() >= 3)
		cv::cvtColor(inputImage, inputImage, cv::COLOR_BGR2GRAY);

	bitwise_not(inputImage, inputImage);

	if (kernel.channels() >= 3)
		cvtColor(kernel, kernel, CV_BGR2GRAY);
	threshold(kernel, kernel, 100, 255, 0);
	bitwise_not(kernel, kernel);
	dilate(inputImage, inputImage, kernel, Point(-1, -1));
	bitwise_not(inputImage, inputImage);
	return inputImage;
}

int CImageOPBase::GetClassNameOfCurrFile(BYTE *bData, int iWidth, int iHeight)
{
	return -1;
}