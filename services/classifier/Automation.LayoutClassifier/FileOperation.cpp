#include "stdafx.h"
#include "FileOperation.h"
#include <algorithm>
#include "LayoutClassification.h"

using namespace std;
using namespace cv;

FileOperation::FileOperation()
{
}

FileOperation::~FileOperation()
{
}
//string FileOperation::GetDirName(string strFilePath)
//{
//	size_t found;
//	found = strFilePath.find_last_of("/\\");
//	if (found == -1)
//		return "";
//	return strFilePath.substr(0, found);
//}
//string FileOperation::GetFileExtension(string strFilePath)
//{
//	path p(strFilePath);
//
//	directory_iterator itr(p);
//	string strExtention = itr->path().extension().string();
//	return strExtention;
//	//size_t found;
//	//found = strFilePath.find_last_of(".");
//	//return strFilePath.substr(found + 1, strFilePath.length() - 1);
//}
//bool FileOperation::IsDirectoryExist(string strFilePath)
//{
//	path pDir(strFilePath);
//
//	if (strFilePath == "")
//		return true;
//
//	if (exists(pDir))
//		return true;
//
//	LOGGER->Error(" : Directory is not found: %s", strFilePath);
//	return false;
//}
//bool FileOperation::IsFileExist(string strFilePath)
//{
//	if (!boost::filesystem::exists(strFilePath))
//		return true;
//	if (IsFileExistInCurrentDirectory(strFilePath))
//		return true;
//
//	LOGGER->Error(" : File is not found : %s", strFilePath);
//	return false;
//}
//bool FileOperation::IsFileExistInCurrentDirectory(string strFilePath)
//{
//	string filePath = boost::filesystem::current_path().string() + strFilePath;
//	LOGGER->Trace("%s", filePath);
//	if (!boost::filesystem::exists(filePath))
//		return true;
//
//	LOGGER->Error(" : File is not found : %s", strFilePath);
//	return false;
//}
//bool FileOperation::IsFileSupported(string strFilePath)
//{
//	string strFileExtension = GetFileExtension(strFilePath);
//	std::transform(strFileExtension.begin(), strFileExtension.end(), strFileExtension.begin(), ::tolower);
//	if (strFileExtension == "png" || strFileExtension == "tiff" ||
//		strFileExtension == "tif" || strFileExtension == "jpeg" || strFileExtension == "jpg" ||
//		strFileExtension == "bmp")
//		return true;
//
//	LOGGER->Error(" : File Formate is not Supported : %s", strFileExtension);
//
//	return false;
//}
void FileOperation::SaveImage(
	std::string pcImgPath,
	cv::Mat imgSrc)
{
	try {
		LOGGER->Trace("FileOperation::SaveImage Started...");

		imwrite(pcImgPath, imgSrc);
		LOGGER->Trace("FileOperation::SaveImage End...");
	}
	catch (Exception exc)
	{
		LOGGER->Error("<Exception> : LayoutClassification::GetClassNameOfCurrFile : [%s]", exc.msg);
		throw exc;
	}
}
cv::Mat FileOperation::ReadImage(
	std::string pcImagePath)
{
	LOGGER->Trace("FileOperation::ReadImage Started...");
	Mat imageReturn;
	//if (IsFileExist(pcImagePath) == false && IsFileSupported(pcImagePath) == false)
	//{
	//	return imageReturn;
	//}

	bool readSuccess = true;
	try
	{
		imageReturn = cv::imread(pcImagePath);
	}
	catch (std::exception exc)
	{
		cout << "Exception in reading a file" << endl;
		readSuccess = false;
		LOGGER->Info("FileOperation::ReadImage [Exception: %s] [File cannot be reading in origin format, Reading in Grayscale Format]", exc.what());
	}
	try {
		if (readSuccess == false)
			imageReturn = cv::imread(pcImagePath, cv::IMREAD_GRAYSCALE);
	}
	catch (std::exception exc)
	{
		LOGGER->Info("FileOperation::ReadImage [Exception: %s]", exc.what());
	}
	if (imageReturn.data == NULL)
	{
		std::exception exct("Image data is not available");
		throw exct;
	}

	LOGGER->Trace("FileOperation::ReadImage End...");

	return imageReturn;
}
std::string FileOperation::GetTempFilePath()
{
	char *cTempFileName = NULL;
	cTempFileName = std::tmpnam(cTempFileName);
	strcat(cTempFileName, ".png");

	return cTempFileName;
}