#pragma once
#include <leptonica\allheaders.h>
#include <string>

typedef  Boxa* BOUNDING_BOX_PTR;
typedef  Box* BOX_PTR;

class __declspec(dllexport) LeptonicaWrapper
{
public:
	LeptonicaWrapper();
	virtual ~LeptonicaWrapper();

	PIX* ReadImage(std::string imagePath);
	PIX* ApplyThresholding(PIX* imagePix, int threshold);
	BOUNDING_BOX_PTR GetConnectedComponentBoundingBox(PIX *imagePix, int connectivity);
	BOX_PTR GetBoxFromBoundingBox(BOUNDING_BOX_PTR boundingBox, int index, int accessFlag);
	void DestroyImage(PIX** imageToDestroy);
	void DestroyBoundingBox(BOUNDING_BOX_PTR* boundingBoxToDestroy);
	void DestroyBox(BOX_PTR* boxToDestroy);
};
