#pragma once
#include "stdafx.h"
#include "resource.h"
#include "windows.h"
#include <iostream>
#include <list>
#include <vector>
#include "LeptonicaWrapper.h"
#include "OpenCVWrapper.h"
#include "FileOperation.h"
#include "../Automation.Cognitive.CppLogger/CPPLogger.h"

#define LOGGER LayoutClassification::GetLogger()

struct stTrianlgeInfo
{
	float fAngles[3];
	float fDirection[3];
	cv::Point2f ptVertex[3];
	cv::Point2f ptVector[3];
	cv::Point2f ptUnitVector[3];
	float fExteriorAngles[3];
	std::string strHasKey;
};
class __declspec(dllexport) LayoutClassification
{
	FileOperation _fileOperation;
	LeptonicaWrapper _leptonicaWrapper;
	OpenCVWrapper _openCVWrapper;
	void MergeOverlappedRects(std::vector<cv::Rect> &inputBoxes, cv::Mat image, int sensitivity);
	string GenerateHasKeyForCurrTriangle(stTrianlgeInfo &sCurrTrianle);
	int GetClassNameOfTriFromSqlServerdb(stTrianlgeInfo &sTriInfo, const char* connectionString);
	stTrianlgeInfo GetTriangleInfo(cv::Mat srcImage);
	cv::Mat GetHeaderImg(cv::Mat img, int iImagePercent = 15);
	cv::Mat GetImageFromByteArray(BYTE *bData, int iWidth, int iHeight);
	cv::Rect GetHeaderROI(cv::Mat srcImg, int headerPercentage);
	cv::Mat GetKernelAccordingToHeight(int height);
	cv::Mat GetMaskImageOfMergedRects(std::vector<cv::Rect> &inputBoxes, int imageRows, int imageCols);
	cv::Mat GetBlackImage(int imageRows, int imageCols);
	void DrawFilledRectangleOn(cv::Mat image, cv::Rect rect);
	bool IsAllCentersAreOnOneLine(cv::Point2f centers[]);
	std::vector<cv::Rect> GetBoundingRect(cv::Mat maskImage);
	void DrawRectAndTriangleOn(cv::Mat srcImage, std::vector<cv::Rect> segmentedRects, stTrianlgeInfo sTriagnleInfo);
	int GetClassNameFromMysqldb(stTrianlgeInfo &sTriInfo);
	std::vector<cv::Rect> GetSegmentedRectFrom(cv::Mat image);
	cv::Mat ApplyKernelOn(cv::Mat srcImage, cv::Mat kernel);
	PIX* ConvertMat_TO_LeptonicaPix(cv::Mat image);
	std::vector<cv::Rect> GetRectFrom(BOUNDING_BOX_PTR boundingBox);
public:
	static CPPLogger *_cppLogger;
	static CPPLogger*  GetLogger();
	LayoutClassification(CPPLoggerConfiguration loggerConfiguration);
	~LayoutClassification();

	void GetCenterPointOfTop3ValidateRect(vector<cv::Rect> inputRect, cv::Point2f ptCenters[]);
	cv::Rect AdjustRectHeightToMerge(cv::Rect inputBoxes, int imageRows, int sensitivity);
	int GetHeightOfMiddleRect(std::vector<cv::Rect> segmentedRects);
	cv::Rect AddPaddingAndGetValidatedROI(cv::Rect ROI, int padding, int imageRows, int imageCols);
	int GetClassNameOfCurrFile(BYTE *bData, int iWidth, int iHeight, const char* connectionString);
	cv::Rect ShiftROIToFirstRectTop(cv::Rect initialROI, cv::Rect firstRect);
	cv::Rect ShiftROIifHeaderHasnotEnoughValidRects(cv::Rect initialROI,
		std::vector<cv::Rect> rectsInHeader,
		int headerImgRows, int headerImgCols,
		int noOfValidateRects);
};
