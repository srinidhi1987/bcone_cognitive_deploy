/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal static class DateCorrectionHelper
    {
        private static Dictionary<string, char> alphabetToNumberCorrectionDictionary =
            new Dictionary<string, char>
                {
                    { "O", '0' },
                    { "o", '0' },

                    { "l", '1' },
                    { "i", '1' },
                    { "I", '1' },
                    { "J", '1' },
                    { "t", '1' },

                    { "S", '5' },
                    { "s", '5' },
                };

        public static string CorrectAlphabetToNumber(string value)
        {
            foreach (KeyValuePair<string, char> keyValuePair in alphabetToNumberCorrectionDictionary)
            {
                value = value.Replace(keyValuePair.Key, keyValuePair.Value.ToString());
            }

            return value;
        }

        public static string RemoveWhiteSpaces(string value)
        {
            return value.Trim().Replace(" ", string.Empty);
        }

        private static List<string> __threeDigitMonthNames;

        private static List<string> ThreeDigitMonthNames
        {
            get
            {
                if (__threeDigitMonthNames == null)
                {
                    __threeDigitMonthNames = GetAllCasingPossibility(new List<string>
                    {
                        "JAN",
                        "FEB",
                        "MAR",
                        "APR",
                        "MAY",
                        "JUN",
                        "JUL",
                        "AUG",
                        "SEP",
                        "OCT",
                        "NOV",
                        "DEC"
                    });
                }

                return __threeDigitMonthNames;
            }
        }

        private static List<string> GetAllCasingPossibility(IEnumerable<string> coreItems)
        {
            var result = new List<string>();

            foreach (var coreItem in coreItems)
            {
                result.Add(coreItem.ToUpper());
                result.Add(coreItem.ToCapital());
                result.Add(coreItem.ToLower());
            }

            return result;
        }

        public static string GetBestMatchThreeDigitMonthName(string value)
        {
            return GetBestMatch(value, ThreeDigitMonthNames);
        }

        private static List<string> __fullMonthNames;

        private static List<string> FullMonthNames
        {
            get
            {
                if (__fullMonthNames == null)
                {
                    __fullMonthNames = GetAllCasingPossibility(new List<string>
                    {
                        "JANUARY",
                        "FEBRUARY",
                        "MARCH",
                        "APRIL",
                        "MAY",
                        "JUNE",
                        "JULY",
                        "AUGUST",
                        "SEPTEMBER",
                        "OCTOBER",
                        "NOVEMBER",
                        "DECEMBER"
                    });
                }

                return __fullMonthNames;
            }
        }

        public static string GetBestMatchFullMonthName(string value)
        {
            return GetBestMatch(value, FullMonthNames);
        }

        private static string GetBestMatch(string value, IList<string> options)
        {
            if (options.Any(month => month.Equals(value)))
            {
                return value;
            }

            var allMatch = options.GetAllFuzzyMatch(value, 0.5);

            if (allMatch == null ||
                allMatch.Count <= 0)
            {
                return value;
            }

            if (allMatch.Count > 1)
            {
                if (!allMatch[0].MatchPercentage.Equals(allMatch[1].MatchPercentage))
                {
                    return allMatch[0].Value;
                }

                if (!allMatch[0].Value.Equals(allMatch[1].Value, System.StringComparison.InvariantCultureIgnoreCase))
                {
                    return value;
                }

                if (allMatch[0].Value.Equals(allMatch[1].Value, System.StringComparison.InvariantCultureIgnoreCase))
                {
                    //TODO: write logic to somehow select value with the right casing
                    //i.e. if input="Aeb" and result are "Feb" and "feb" then because input diff is in first letter which is capital so we need to select "Feb" as result
                }
            }

            return allMatch[0].Value;
        }
    }

    //TODO: remove this class and move function to proper generic location
    internal static class StringExtensions_ToCapital
    {
        public static string ToCapital(this string value)
        {
            var result = new StringBuilder();

            var capitalNext = true;
            foreach (char c in value)
            {
                if (capitalNext && char.IsLetter(c))
                {
                    result.Append(char.ToUpper(c));
                    capitalNext = false;
                    continue;
                }

                result.Append(char.IsLetter(c) ? char.ToLower(c) : c);

                if (char.IsWhiteSpace(c) || c == '.')
                {
                    capitalNext = true;
                }
            }

            return result.ToString();
        }
    }
}