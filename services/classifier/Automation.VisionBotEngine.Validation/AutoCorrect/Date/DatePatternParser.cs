/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal class DatePatternParser
    {
        public static IDatePatternComponent[] GetDatePatternComponents(string pattern)
        {
            pattern = pattern.ToLower().Trim();

            var result = new List<IDatePatternComponent>();

            var component = string.Empty;

            foreach (char c in pattern)
            {
                if (component == string.Empty ||
                    component[component.Length - 1] == c)
                {
                    component += c;
                    continue;
                }

                var objComponent = GetDatePatternComponent(component);
                if (objComponent != null)
                {
                    result.Add(objComponent);
                    component = c.ToString();
                    continue;
                }

                if (c == 'd' || c == 'm' || c == 'y')
                {
                    result.Add(new DateSeparatorComponent(component));
                    component = c.ToString();
                }
                else
                {
                    component += c;
                }
            }

            if (component != string.Empty)
            {
                var objComponent = GetDatePatternComponent(component);
                result.Add(objComponent ?? new DateSeparatorComponent(component));
            }

            return result.ToArray();
        }

        private static IDatePatternComponent GetDatePatternComponent(string patternComponent)
        {
            switch (patternComponent)
            {
                case "d": return new SingleOrDoubleDigitDayPatternComponent();
                case "dd": return new AlwaysDoubleDigitDayPatternComponent();
                case "m": return new SingleOrDoubleDigitMonthPatternComponent();
                case "mm": return new AlwaysDoubleDigitMonthPatternComponent();
                case "mmm": return new ThreeDigitMonthPatternComponent();
                case "mmmm": return new FullNameMonthPatternComponent();
                case "yy": return new DoubleDigitYearPatternComponent();
                case "yyyy": return new FourDigitYearPatternComponent();
                default: return null;
            }
        }

        public static string ConvertToPattern(IDatePatternComponent[] patternComponents)
        {
            var result = string.Empty;

            foreach (var datePatternComponent in patternComponents)
            {
                result += datePatternComponent.GetPatternComponent();
            }

            return result;
        }

        public static DatePatternComponentWithValue[] GetPatternComponentWithValues(string rawValue, IDatePatternComponent[] patternComponents)
        {
            var result = new List<DatePatternComponentWithValue>();

            var valueRemainingToProcess = rawValue;

            for (int i = 0; i < patternComponents.Count(); i++)
            {
                var datePatternComponent = patternComponents[i];

                if (valueRemainingToProcess.Length <= 0)
                {
                    var parsedInto = string.Empty;
                    foreach (var datePatternComponentWithValue in result)
                    {
                        parsedInto += string.Format(
                            " <\"{0}\"=\"{1}\"> ",
                            datePatternComponentWithValue.PatternComponent.GetType(),
                            datePatternComponentWithValue.ValueComponent);
                    }

                    var remainingComponents = string.Empty;
                    for (int j = i; j < patternComponents.Count(); j++)
                    {
                        remainingComponents += string.Format(
                            " <\"{0}\"> ",
                            patternComponents[j].GetPatternComponent().GetType());
                    }

                    throw new Exception(string.Format(
                        "insufficient data for parsed components in given value \"{0}\". Parsed info {1}. Remaining components {2}",
                        rawValue, parsedInto, remainingComponents));
                }

                var extractedComponentValue = datePatternComponent.ExtractComponentValue(valueRemainingToProcess);

                valueRemainingToProcess = valueRemainingToProcess.Substring(extractedComponentValue.Length);

                result.Add(new DatePatternComponentWithValue
                               {
                                   PatternComponent = datePatternComponent,
                                   ValueComponent = extractedComponentValue
                               });
            }

            if (valueRemainingToProcess.Length > 0)
            {
                var parsedInto = string.Empty;
                foreach (var datePatternComponentWithValue in result)
                {
                    parsedInto += string.Format(
                        " <\"{0}\"=\"{1}\"> ",
                        datePatternComponentWithValue.PatternComponent.GetType(),
                        datePatternComponentWithValue.ValueComponent);
                }

                throw new Exception(string.Format(
                    "could not parse full value \"{0}\" into given patternComponents. Left with parsing of trailing component \"{1}\". Parsed into {2}", 
                    rawValue, valueRemainingToProcess, parsedInto));
            }

            return result.ToArray();
        }
        
        public static string GetAutoCorrectedValue(DatePatternComponentWithValue[] patternComponentWithValues)
        {
            var result = string.Empty;

            foreach (var datePatternComponentWithValue in patternComponentWithValues)
            {
                result += datePatternComponentWithValue.PatternComponent.GetAutoCorrectedValue(datePatternComponentWithValue.ValueComponent);
            }

            return result;
        }
    }
}