/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    internal class DateSeparatorComponent : IDatePatternComponent
    {
        private string Separator { get; }

        public DateSeparatorComponent(string separator)
        {
            this.Separator = separator ?? string.Empty;
        }

        public string GetPatternComponent()
        {
            return this.Separator;
        }

        public string ExtractComponentValue(string value)
        {
            var result = string.Empty;

            var charCount = 0;
            foreach (char c in value)
            {
                if (c == ' ' || !char.IsLetterOrDigit(c))
                {
                    result += c;

                    if (c != ' ' || this.Separator == " ") charCount++;

                    if (charCount == this.Separator.Length)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return result;
        }
        public string GetAutoCorrectedValue(string value)
        {
            return this.Separator;
        }
    }
}