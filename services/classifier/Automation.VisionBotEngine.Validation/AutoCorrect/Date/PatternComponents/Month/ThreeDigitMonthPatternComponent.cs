/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    internal class ThreeDigitMonthPatternComponent : MonthPatternComponent
    {
        public override string GetPatternComponent()
        {
            return "MMM";
        }

        public override string ExtractComponentValue(string value)
        {
            return ExtractionHelper.Extract(value, 3);
        }
        public override string GetAutoCorrectedValue(string value)
        {
            return DateCorrectionHelper.GetBestMatchThreeDigitMonthName(DateCorrectionHelper.RemoveWhiteSpaces(value));
        }
    }
}