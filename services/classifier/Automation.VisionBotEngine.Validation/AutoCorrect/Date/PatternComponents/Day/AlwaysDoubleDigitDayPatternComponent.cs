/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    internal class AlwaysDoubleDigitDayPatternComponent : DayPatternComponent
    {
        public override string GetPatternComponent()
        {
            return "DD";
        }

        public override string ExtractComponentValue(string value)
        {
            return ExtractionHelper.Extract(value, 2);
        }

        public override string GetAutoCorrectedValue(string value)
        {
            return AutoCorrectSingleDigitNumber(
                AutoCorrectMisspells(value));
        }

        private string AutoCorrectMisspells(string value)
        {
            return DateCorrectionHelper.CorrectAlphabetToNumber(DateCorrectionHelper.RemoveWhiteSpaces(value));
        }

        private string AutoCorrectSingleDigitNumber(string value)
        {
            if (value.Length == 1)
            {
                var parsedValue = int.Parse(value);
                if (parsedValue >= 1 && parsedValue <= 9)
                    value = '0' + value;
            }

            return value;
        }
    }
}