/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    static class ExtractionHelper
    {
        public static string Extract(string value, uint maxExtractionCount)
        {
            var result = string.Empty;

            var charCount = 0;
            foreach (char c in value)
            {
                if (c == ' ' || char.IsLetterOrDigit(c))
                {
                    result += c;

                    if (c != ' ') charCount++;

                    if (charCount == maxExtractionCount)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return result;
        }
    }
}