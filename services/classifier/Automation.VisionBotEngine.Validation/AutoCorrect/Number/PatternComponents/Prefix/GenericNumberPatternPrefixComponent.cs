/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    using System.Linq;

    using Automation.VisionBotEngine.Validation;

    internal class GenericNumberPatternPrefixComponent : NumberPatternPrefixComponent
    {
        public override string Pattern => string.Empty;

        public string CorrectValue { get; private set; }

        private bool IsAutoCorrectionApplied = false;
        public override void ApplyAutoCorrection()
        {
            if (string.IsNullOrEmpty(this.CorrectValue) || this.IsAutoCorrectionApplied) return;

            var strToPad = string.Empty;
            for (int i = this.ExtractedValue.Length - 1; i >= 0; i--)
            {
                var c = this.ExtractedValue[i];
                if (c == ' ') strToPad += c;
                else break;
            }

            this.ExtractedValue = this.CorrectValue.Trim() + strToPad;
            this.IsAutoCorrectionApplied = true;
        }

        public override string GetOutputValue()
        {
            return string.Empty;
        }

        public override string ExtractYourValueAndReturnRemainingValue(string value)
        {
            this.IsAutoCorrectionApplied = false;

            var extractedValue = StringHelper.GetWordIfFound(
                value,
                StringHelper.ItemSearchOptions.Start,
                CurrencyHelper.ValidCurrencySymbols.ToArray());

            if (string.IsNullOrEmpty(extractedValue)) { return value; }

            string originalDetectedString;
            var result = StringHelper.RemoveFromStart(value, extractedValue, out originalDetectedString);
            this.ExtractedValue = originalDetectedString;
            this.CorrectValue = extractedValue;
            return result;
        }

        public override bool IsValid()
        {
            return true;
        }
    }
}