/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal class MillionSeparatedWholePartNumberComponent : WholePartNumberComponent
    {
        private const int MidSequenceLength = 3;

        public override string Pattern => string.Format("{0}{1}{0}{0}{0}{1}{0}{0}{0}", RepresentationCharacter, SeparatorCharacter);

        public override void ApplyAutoCorrection()
        {
            if (this.IsValid()) return;
            this.ExtractedValue = NumberPatternHelper.AutoCorrectWholePartNumber(this.ExtractedValue, MidSequenceLength);
        }

        public override string ExtractYourValueAndReturnRemainingValue(string value)
        {
            this.ExtractedValue = value;
            return string.Empty;
        }

        public override bool IsValid()
        {
            return NumberPatternHelper.IsValid(this.ExtractedValue, MidSequenceLength);
        }
    }
}