/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal class NoNumberPatternPostfixComponent : NumberPatternPostfixComponent
    {
        public override string ExtractYourValueAndReturnRemainingValue(string value)
        {
            return value;
        }

        public override void ApplyAutoCorrection()
        {

        }

        public override string GetOutputValue()
        {
            return string.Empty;
        }

        public override bool IsValid()
        {
            return true;
        }
    }
}