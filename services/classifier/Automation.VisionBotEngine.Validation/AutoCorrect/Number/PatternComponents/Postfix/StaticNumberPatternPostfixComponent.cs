/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal sealed class StaticNumberPatternPostfixComponent : NumberPatternPostfixComponent
    {
        public StaticNumberPatternPostfixComponent(string pattern)
        {
            this.Pattern = pattern;
        }

        public override void ApplyAutoCorrection()
        {
            this.ExtractedValue = NumberPatternHelper.ApplyAutoCorrectionToStaticString(this.ExtractedValue, this.Pattern);
        }

        public override string GetOutputValue()
        {
            return this.IsValid() ? string.Empty : this.ExtractedValue;
        }

        public override string ExtractYourValueAndReturnRemainingValue(string value)
        {
            if (string.IsNullOrWhiteSpace(this.Pattern)) { return value; }

            var patternCharLength = this.Pattern.Trim().Replace(" ", "").Length;
            var extractedValue = string.Empty;
            var extractedCharCount = 0;
            for (int i = value.Length - 1; i >= 0; i--)
            {
                var c = value[i];

                if (extractedCharCount == patternCharLength)
                {
                    if (c == ' ')
                    {
                        extractedValue = c + extractedValue;
                        continue;
                    }

                    this.ExtractedValue = extractedValue;
                    return value.Substring(0, i + 1);
                }

                extractedValue = c + extractedValue;
                if (c == ' ') continue;
                extractedCharCount++;
            }

            this.ExtractedValue = extractedValue;
            return string.Empty;
        }

        public override bool IsValid()
        {
            return this.Pattern == this.ExtractedValue;
        }
    }
}