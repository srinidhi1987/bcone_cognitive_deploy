/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal class PatternBasedNumber
    {
        public PatternBasedNumber(NumberPattern pattern, string number)
        {
            this.Pattern = pattern;
            this.OriginalNumber = number;
            this.ProcessedNumber = number;

            if (this.Pattern.IsNumberPatternValid())
            {
                this.ExtractComponentValues();
            }
        }

        public NumberPattern Pattern { get; private set; }

        private bool? __isPatternValid;
        private bool IsPatternValid
        {
            get
            {
                if (this.__isPatternValid == null)
                {
                    this.__isPatternValid = this.Pattern.IsNumberPatternValid();
                }
                return this.__isPatternValid.Value;
            }
        }

        public string OriginalNumber { get; }
        public string ProcessedNumber { get; private set; }

        private bool IsNumberAutoCorrected;
        public void ApplyAutoCorrection()
        {
            if (!this.IsPatternValid || this.IsNumberAutoCorrected) return;

            this.Pattern.ParsedPattern.PrefixComponent.ApplyAutoCorrection();
            this.Pattern.ParsedPattern.PatternValueComponent.ApplyAutoCorrection();
            this.Pattern.ParsedPattern.PostfixComponent.ApplyAutoCorrection();

            this.ProcessedNumber = string.Format("{0}{1}{2}",
                this.Pattern.ParsedPattern.PrefixComponent.ExtractedValue,
                this.Pattern.ParsedPattern.PatternValueComponent.ExtractedValue,
                this.Pattern.ParsedPattern.PostfixComponent.ExtractedValue);

            this.IsNumberAutoCorrected = true;
        }

        private string OutputValue;
        public string GetOutputValue()
        {
            if (!this.IsPatternValid) return this.OriginalNumber;

            if (!this.IsNumberValid()) return this.ProcessedNumber;

            return this.OutputValue ??
                   (this.OutputValue = string.Format("{0}{1}{2}",
                       this.Pattern.ParsedPattern.PrefixComponent.GetOutputValue(),
                       this.Pattern.ParsedPattern.PatternValueComponent.GetOutputValue(),
                       this.Pattern.ParsedPattern.PostfixComponent.GetOutputValue()));
        }

        public bool IsNumberValid()
        {
            if (!this.IsPatternValid) return false;

            return this.Pattern.ParsedPattern.PrefixComponent.IsValid()
                   && this.Pattern.ParsedPattern.PostfixComponent.IsValid()
                   && this.Pattern.ParsedPattern.PatternValueComponent.IsValid();
        }

        private void ExtractComponentValues()
        {
            if (!this.IsPatternValid) return;

            var value = this.ProcessedNumber;
            value = this.Pattern.ParsedPattern.PrefixComponent.ExtractYourValueAndReturnRemainingValue(value);
            value = this.Pattern.ParsedPattern.PostfixComponent.ExtractYourValueAndReturnRemainingValue(value);
            value = this.Pattern.ParsedPattern.PatternValueComponent.ExtractYourValueAndReturnRemainingValue(value);
        }
    }
}