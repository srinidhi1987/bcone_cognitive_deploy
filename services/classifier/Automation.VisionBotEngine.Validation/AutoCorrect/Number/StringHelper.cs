/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    using System;

    internal static class StringHelper
    {
        public enum ItemSearchOptions
        {
            Start,
            End,
            BothStartAndEnd,
            //AnyLocation
        }

        public static string GetWordIfFound(string strToSearchIn, ItemSearchOptions searchOption, params string[] wordsToSearchFor)
        {
            if (string.IsNullOrWhiteSpace(strToSearchIn))
            {
                return string.Empty;
            }

            var value = strToSearchIn.Trim().Replace(" ", "");

            foreach (var validCurrencySymbol in wordsToSearchFor)
            {
                if ((searchOption == ItemSearchOptions.Start || searchOption == ItemSearchOptions.BothStartAndEnd)
                    && value.StartsWith(validCurrencySymbol, StringComparison.InvariantCultureIgnoreCase))
                {
                    var currencySymbolInString = value.Substring(0, validCurrencySymbol.Length);
                    if (currencySymbolInString != validCurrencySymbol)
                    {
                        continue;
                    }
                    return validCurrencySymbol;
                }
                if ((searchOption == ItemSearchOptions.End || searchOption == ItemSearchOptions.BothStartAndEnd)
                    && value.EndsWith(validCurrencySymbol, StringComparison.InvariantCultureIgnoreCase))
                {
                    var currencySymbolInString = value.Substring(value.Length - validCurrencySymbol.Length, validCurrencySymbol.Length);
                    if (currencySymbolInString != validCurrencySymbol)
                    {
                        continue;
                    }
                    return validCurrencySymbol;
                }
            }

            return String.Empty;
        }

        public static string RemoveFromStart(string source, string toRemove, out string removedString)
        {
            toRemove = toRemove.Replace(" ", "");

            if (source.Equals(toRemove, StringComparison.InvariantCultureIgnoreCase))
            {
                removedString = source;
                return string.Empty;
            }

            var sourceWithoutWhiteSpace = source.Replace(" ", "");

            if (sourceWithoutWhiteSpace.Equals(toRemove, StringComparison.InvariantCultureIgnoreCase))
            {
                removedString = source;
                return String.Empty;
            }

            if (toRemove.Length > sourceWithoutWhiteSpace.Length)
            {
                removedString = string.Empty;
                return source;
            }

            var originalTraversedValue = String.Empty;
            var traversedValue = String.Empty;
            var traversedValueLength = 0;
            bool? found = null;
            for (int i = 0; i < source.Length; i++)
            {
                var c = source[i];

                if (toRemove.Length == traversedValueLength)
                {
                    if (found == null)
                    {
                        found = toRemove.Equals(traversedValue, StringComparison.InvariantCultureIgnoreCase);
                    }

                    if (found.Value)
                    {
                        if (c == ' ')
                        {
                            originalTraversedValue += c;
                            continue;
                        }
                        removedString = originalTraversedValue;
                        return source.Substring(i);
                    }
                    removedString = String.Empty;
                    return source;
                }

                originalTraversedValue += c;
                if (c != ' ')
                {
                    traversedValue += c;
                    traversedValueLength++;
                }
            }

            if (toRemove.Equals(traversedValue, StringComparison.InvariantCultureIgnoreCase))
            {
                removedString = source;
                return String.Empty;
            }
            else
            {
                removedString = String.Empty;
                return source;
            }
        }

        public static string RemoveFromEnd(string source, string toRemove, out string removedString)
        {
            toRemove = toRemove.Replace(" ", "");

            if (source.Equals(toRemove, StringComparison.InvariantCultureIgnoreCase))
            {
                removedString = source;
                return String.Empty;
            }

            var sourceWithoutWhiteSpace = source.Replace(" ", "");

            if (sourceWithoutWhiteSpace.Equals(toRemove, StringComparison.InvariantCultureIgnoreCase))
            {
                removedString = source;
                return String.Empty;
            }

            if (toRemove.Length > sourceWithoutWhiteSpace.Length)
            {
                removedString = String.Empty;
                return source;
            }

            var originalTraversedValue = String.Empty;
            var traversedValue = String.Empty;
            var traversedValueLength = 0;
            bool? found = null;
            for (int i = source.Length - 1; i >= 0; i--)
            {
                var c = source[i];

                if (toRemove.Length == traversedValueLength)
                {
                    if (found == null)
                    {
                        found = toRemove.Equals(traversedValue, StringComparison.InvariantCultureIgnoreCase);
                    }

                    if (found.Value)
                    {
                        if (c == ' ')
                        {
                            originalTraversedValue = c + originalTraversedValue;
                            continue;
                        }
                        removedString = originalTraversedValue;
                        return source.Substring(0, i + 1);
                    }
                    removedString = String.Empty;
                    return source;
                }

                originalTraversedValue = c + originalTraversedValue;
                if (c != ' ')
                {
                    traversedValue = c + traversedValue;
                    traversedValueLength++;
                }
            }

            if (toRemove.Equals(traversedValue, StringComparison.InvariantCultureIgnoreCase))
            {
                removedString = source;
                return String.Empty;
            }
            else
            {
                removedString = String.Empty;
                return source;
            }
        }
    }
}
