/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System.Collections.Generic;
    using System.Linq;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation.Helpers;

    internal class ListValidator : IFieldValidator
    {
        private const double ListAutoCorrectionMinimumSimilarityFactor = 0.66f;

        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vBotValidatorDataReader)
        {
            if (!fieldData.Field.IsListValidationAvailable() ||
                string.IsNullOrWhiteSpace(fieldData.Value.Field.Text))
            {
                return null;
            }

            var listProvider = vBotValidatorDataReader.Get<ListProvider>(fieldData.Field.Id);

            IEnumerable<string> items = listProvider?.GetListItems();

            if (items == null || !items.Any())
            {
                return null;
            }

            var detectedValue = fieldData.Value.Field.Text.Trim();
            if (items.Any(x => x.Trim().Equals(detectedValue, System.StringComparison.InvariantCultureIgnoreCase)))
            {
                return null;
            }

            var item = items.GetFirstBestFuzzyMatch(detectedValue, ListAutoCorrectionMinimumSimilarityFactor);
            if (!string.IsNullOrEmpty(item))
            {
                fieldData.Value.Field.Text = item;

                VBotLogger.Trace(
                    () =>
                        string.Format(
                            "[List Validator] [FieldId=\"{0}\"] [Value re-assignment] detected=\"{1}\", changed to=\"{2}\" ",
                            fieldData.Field.Id,
                            fieldData.Value.Field.Text,
                            item));

                return null;
            }

            return FieldDataValidationIssueCodes.ValueNotInList.ToValidationIssue();
        }
    }
}