/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = false)]
    public class FileBasedListProvider : ListProvider
    {
        [Obfuscation(Exclude = true, ApplyToMembers = false)]
        public string FilePath { get; set; }

        public override ICollection<string> GetListItems()
        {
            if (!IsValidListProviderFile(this.FilePath).Equals(ValidationInputErrorCode.None)) return new List<string>();

            var listItems = new List<string>();
            using (var file = new StreamReader(this.FilePath))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (!string.IsNullOrWhiteSpace(line)) listItems.Add(line.Trim());
                }

                file.Close();
            }
            return listItems;
        }

        private static ValidationInputErrorCode IsValidListProviderFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return ValidationInputErrorCode.FilePathIsEmpty;
            }

            if (!File.Exists(filePath))
            {
                return ValidationInputErrorCode.FileDoesNotExists;
            }

            var fileExtension = Path.GetExtension(filePath);

            return AllowedFileExtensions.Any(allowedExtension => fileExtension.Equals("." + allowedExtension, System.StringComparison.InvariantCultureIgnoreCase))
                       ? ValidationInputErrorCode.None
                       : ValidationInputErrorCode.InvalidFileFormat;
        }

        private static IEnumerable<string> AllowedFileExtensions => new[] { "txt", "csv" };

        private enum ValidationInputErrorCode
        {
            None,
            FilePathIsEmpty,
            FileDoesNotExists,
            InvalidFileFormat
        }
    }
}