/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class StaticListProvider : ListProvider
    {
        private ICollection<string> _staticListItems;
        public ICollection<string> StaticListItems
        {
            get { return _staticListItems ?? (_staticListItems = new List<string>()); }
            set { _staticListItems = value; }
        }

        public override ICollection<string> GetListItems()
        {
            return this.StaticListItems;
        }

        public void SetItemsFromSingleString(string itemString)
        {
            if (string.IsNullOrWhiteSpace(itemString))
            {
                this.StaticListItems = new string[] { };
                return;
            }

            var items = itemString.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            var result = items.Select(item => item.Trim()).Where(i => !string.IsNullOrWhiteSpace(i)).ToList();
            this.StaticListItems = result;
        }

        public string GetSingleStringForItems()
        {
            if (this.StaticListItems == null || this.StaticListItems.Count <= 0)
            {
                return string.Empty;
            }

            var result = new StringBuilder();

            foreach (var staticListItem in this.StaticListItems.Where(staticListItem => !string.IsNullOrWhiteSpace(staticListItem)))
            {
                if (result.Length > 0)
                {
                    result.Append(Environment.NewLine);
                }

                result.Append(staticListItem);
            }

            return result.ToString();
        }
    }
}