﻿/**
* Copyright (c) 2015 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System.Collections.Generic;
using System.Reflection;

namespace Automation.VisionBotEngine.Validation.JSFormulaValidation
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    class Table
    {
        public string Id { get; set; }

        public IList<string> Columns { get; set; } = new List<string>();

        public IList<IList<string>> Rows { get; set; } = new List<IList<string>>();
    }
}
