﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation.JSFormulaValidation
{
    using Newtonsoft.Json;
    using Noesis.Javascript;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    internal class FormulaValidationEngineCommunicator
    {
        private const string JavascriptEngineFileName = "FormulaEvaluator.js";

        public List<JavascriptRuleResult> GetValidationResults(VBotFormulaValidationData request)
        {
            try
            {
                var installationPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                if (installationPath == null)
                {
                    VBotLogger.Trace(() => $"[Formula Validation] [{nameof(this.GetValidationResults)}] {nameof(installationPath)} is null");
                    return new List<JavascriptRuleResult>();
                }

                var filePath = Path.Combine(installationPath, JavascriptEngineFileName);

                if (!File.Exists(filePath))
                {
                    VBotLogger.Trace(() => $"[Formula Validation] [{nameof(this.GetValidationResults)}] file '{filePath}' not found");
                    return new List<JavascriptRuleResult>();
                }

                var requestObject = JsonConvert.SerializeObject(request);
                VBotLogger.Trace(() => $"[Formula Validation] [{nameof(this.GetValidationResults)}] Request object -> {requestObject}");

                var finalJavascriptContent = $"var request={requestObject};{Environment.NewLine}{File.ReadAllText(filePath)}";

                string resultObject;
                using (var context = new JavascriptContext())
                {
                    resultObject = context.Run(finalJavascriptContent).ToString();
                }
                VBotLogger.Trace(() => $"[Formula Validation] [{nameof(this.GetValidationResults)}] Result object -> {resultObject}");

                return JsonConvert.DeserializeObject<List<JavascriptRuleResult>>(resultObject);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(FormulaValidationEngineCommunicator), nameof(GetValidationResults));
            }

            return new List<JavascriptRuleResult>();
        }
    }
}