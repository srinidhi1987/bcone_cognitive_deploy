/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
namespace Automation.VisionBotEngine.Validation
{
    using System.Reflection;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class FormulaValueMismatchIssue : ValidationIssue
    {
        public FormulaValueMismatchIssue(string formula)
            : this(formula, null)
        {

        }

        public FormulaValueMismatchIssue(string formula, decimal? calculatedValue)
            : base(FieldDataValidationIssueCodes.FormulaValueMismatch)
        {
            this.Formula = formula;
            this.CalculatedValue = calculatedValue;
        }

        public string Formula { get; private set; }

        public decimal? CalculatedValue { get; private set; }
    }
}