/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation.Helpers;
    using Automation.VisionBotEngine.Validation.JSFormulaValidation;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    internal class FormulaValidationManagerHelper
    {
        public IDictionary<FieldData, ValidationResult> GetValidationResult(VBotData vBotData, ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            return ConvertValidationResultToDictionary(vBotData, this.GetFormulaValidationData(vBotData, validatorAdvanceInfo));
        }

        private VBotFormulaValidationData GetFormulaValidationData(VBotData vBotData, ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            var result = new VBotFormulaValidationData();
            try
            {
                AddFieldData(vBotData, result, validatorAdvanceInfo);
                AddTableData(vBotData, result, validatorAdvanceInfo);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(FormulaValidationManagerHelper), nameof(GetFormulaValidationData));
            }
            return result;
        }

        private static void AddTableData(VBotData vBotData, VBotFormulaValidationData result, ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            foreach (TableValue tableRecord in vBotData.TableDataRecord)
            {
                var tableData = new Table { Id = tableRecord.TableDef.Name };

                foreach (FieldDef tableColumn in tableRecord.Headers)
                {
                    tableData.Columns.Add(tableColumn.Name);
                    AddRuleData(result, tableColumn);
                }

                var columnWiseDataRecord = ConvertRowsToColumn(tableRecord.Headers, tableRecord.Rows);

                foreach (var tableRow in columnWiseDataRecord)
                {
                    tableData.Rows.Add(
                        tableRow.Fields?.Select(f => getSanitizedValue(f, validatorAdvanceInfo))
                        .ToList()
                        ?? new List<string>());
                }

                result.Tables.Add(tableData);
            }
        }

        private static string getSanitizedValue(FieldData fieldData, ValidatorAdvanceInfo validatorAdvanceInfo)
        {

            var value = fieldData.Value.Field.Text;
            if (fieldData.Field.ValueType == FieldValueType.Number
                && (fieldData?.ValidationIssue == null
                || fieldData.ValidationIssue.IssueCode != FieldDataValidationIssueCodes.InvalidNumberTypeValue))
            {

                value = SanitizeNumericValue(value, validatorAdvanceInfo);
            }

            return value;
        }

        private static void AddFieldData(VBotData vBotData
            , VBotFormulaValidationData result
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            foreach (var fieldData in vBotData.FieldDataRecord.Fields)
            {
                var value = fieldData.Value.Field.Text;

                value = getSanitizedValue(fieldData, validatorAdvanceInfo);

                result.Fields.Add(new FieldsData { Id = fieldData.Field.Name, Value = value });

                AddRuleData(result, fieldData.Field);
            }
        }

        private static string SanitizeNumericValue(string rawNumericValue, ValidatorAdvanceInfo validatorAdvanceInfo)
        {

            try
            {
                return decimal.Parse(rawNumericValue
                    , validatorAdvanceInfo.CultureInfo)
                    .ToString(new CultureInfo("en"));
            }
            catch (Exception ex)
            {
                return rawNumericValue;
            }
        }

        private static void AddRuleData(VBotFormulaValidationData addTo, FieldDef fieldDef)
        {
            if (fieldDef.IsFormulaValidationAvailable() && !string.IsNullOrEmpty(fieldDef.Formula))
            {
                addTo.Rules.Add(new Rules { Id = fieldDef.Name, Rule = fieldDef.Formula });
            }
        }

        private static IEnumerable<DataRecord> ConvertRowsToColumn(IReadOnlyCollection<FieldDef> tableColumns, IReadOnlyCollection<DataRecord> tableRows)
        {
            var result = new List<DataRecord>();

            for (var colIndex = 0; colIndex < tableColumns.Count; colIndex++)
            {
                var record = new DataRecord();
                foreach (var row in tableRows)
                {
                    record.Fields.Add(row.Fields[colIndex]);
                }
                result.Add(record);
            }

            return result;
        }

        private static IDictionary<FieldData, ValidationResult> ConvertValidationResultToDictionary(VBotData vBotData, VBotFormulaValidationData vbotFormulaValidationData)
        {
            var validationResults = new Dictionary<FieldData, ValidationResult>();
            try
            {
                var parser = new FormulaValidationEngineCommunicator();

                var ruleResults = parser.GetValidationResults(vbotFormulaValidationData);
                foreach (var result in ruleResults)
                {
                    var fieldData = vBotData.FieldDataRecord.Fields.FirstOrDefault(e => e.Field.Name == result.Id);
                    if (fieldData != null)
                    {
                        validationResults.Add(fieldData, new ValidationResult(result.Results[0]));
                    }
                }

                foreach (var result in ruleResults)
                {
                    foreach (var table in vBotData.TableDataRecord)
                    {
                        var rowNumber = 0;
                        foreach (var row in table.Rows)
                        {
                            var fieldData = row.Fields.FirstOrDefault(e => e.Field.Name == result.Id);
                            if (fieldData != null)
                            {
                                validationResults.Add(fieldData, new ValidationResult(result.Results[rowNumber]));
                            }
                            rowNumber++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(FormulaValidationManagerHelper), nameof(ConvertValidationResultToDictionary));
            }
            return validationResults;
        }
    }
}