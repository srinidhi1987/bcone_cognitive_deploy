﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Configuration;

namespace Automation.Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence
{
    public static class ConfidenceIssueHelper
    {
        public static void IgnoreLowConfidenceIssue(this FieldValue fieldValue)
        {
            if (fieldValue?.Field != null)
            {
                fieldValue.Field.Confidence = Constants.CDC.MAX_CONFIDENCE_THRESHOLD;
            }
        }

        public static void IgnoreLowConfidenceIssue(this VBotData vbotData)
        {
            foreach (FieldData fieldData in vbotData.FieldDataRecord.Fields)
            {
                fieldData.IgnoreLowConfidenceIssue();
            }

            foreach (TableValue table in vbotData.TableDataRecord)
            {
                foreach (DataRecord row in table.Rows)
                {
                    foreach (FieldData fieldData in row.Fields)
                    {
                        fieldData.IgnoreLowConfidenceIssue();
                    }
                }
            }
        }

        public static void IgnoreLowConfidenceIssue(this FieldData fieldData)
        {
            fieldData?.Value?.IgnoreLowConfidenceIssue();

            if (fieldData?.ValidationIssue?.IssueCode == Automation.VisionBotEngine
                .FieldDataValidationIssueCodes
                .ConfidenceIssue)
            {
                fieldData.ValidationIssue = null;
            }
        }
    }
}
