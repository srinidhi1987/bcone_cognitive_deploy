﻿using Automation.Cognitive.VisionBotEngine.Model.Generic;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;
using System;

namespace Automation.Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence
{
    public class ConfidenceValidator : IFieldValidator
    {
        private ValidatorAdvanceInfo _validatorAdvanceInfo;

        public ConfidenceValidator()
        {
            _validatorAdvanceInfo = new ValidatorAdvanceInfo();
        }

        public ConfidenceValidator(ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            _validatorAdvanceInfo = validatorAdvanceInfo;
        }

        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vBotValidatorDataReader)
        {
            if (string.IsNullOrWhiteSpace(fieldData.Value.Field.Text))
            {
                return null;
            }

            int fieldLength = fieldData.Value.Field.Text.Length;
            int fieldConfidence = fieldData.Value.Field.Confidence;
            int characterLevelConfidence = (int)(new ConfidenceProvider().GetCharacterLevelConfidence((double)fieldData.Value.Field.Confidence/100, fieldLength) * 100);

            int characterLevelThreshold = _validatorAdvanceInfo.ConfidenceThreshold;
            int fieldThreshold = (int)new ConfidenceProvider().GetFieldLevelConfidence((double)_validatorAdvanceInfo.ConfidenceThreshold/100, fieldLength);
            

            if (fieldConfidence < fieldThreshold)
            {
                VBotLogger.Trace(() => string.Format(
                     "[ConfidenceValidator][Field Fail][Low Confidence] SIR[Curr={0}; Req={1}]; Char[Curr={2}; Req={3}]; Field[Id={4}; NoOfChar={5}]; ", fieldConfidence, fieldThreshold, characterLevelConfidence, characterLevelThreshold, fieldData.Field.Name, fieldLength));
                return new ConfidenceIssue(fieldData.Value.Field.Confidence
                   , fieldThreshold);
            }
            VBotLogger.Trace(() => string.Format(
                      "[ConfidenceValidator][Field Pass] SIR[Curr={0}; Req={1}]; Char[Curr={2}; Req={3}]; Field[Id={4}; NoOfChar={5}]; ", fieldConfidence, fieldThreshold, characterLevelConfidence, characterLevelThreshold, fieldData.Field.Name, fieldLength));

            return null;
        }
    }
}