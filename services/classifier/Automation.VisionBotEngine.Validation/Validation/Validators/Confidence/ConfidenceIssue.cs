﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Validation
{
    public class ConfidenceIssue : ValidationIssue
    {
        public int ThresholdConfidence { get; private set; }
        public int Confidence { get; private set; }
        public ConfidenceIssue(int confidence, int thresholdConfidence) 
            : base(FieldDataValidationIssueCodes.ConfidenceIssue)
        {
            Confidence = confidence;
            ThresholdConfidence = thresholdConfidence;
        }
    }
}
