/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Reflection;

    using Automation.VisionBotEngine.AutoCorrect.Date;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    internal class DateTypeValidator : IFieldValidator
    {
        private IParsableType<DateTime> Parser => new DateTypeParser();

        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vBotValidatorDataReader)
        {
            var pattern = fieldData.Field.GetApplicablePattern(layout);

            FieldDataValidationIssueCodes? issue;

            var isAutoCorrectionDone = false;

            while (true)
            {
                issue = fieldData.Field.ValueType != FieldValueType.Date
                        || string.IsNullOrWhiteSpace(fieldData.Value.Field.Text)
                        || this.Parser.Parse(fieldData.Value.Field.Text, pattern).HasValue
                            ? (FieldDataValidationIssueCodes?)null
                            : (string.IsNullOrEmpty(pattern)
                                   ? FieldDataValidationIssueCodes.InvalidDateTypeValue
                                   : FieldDataValidationIssueCodes.PatternMismatch);

                if (isAutoCorrectionDone || issue == null || string.IsNullOrWhiteSpace(pattern))
                {
                    break;
                }

                isAutoCorrectionDone = true;
                var autoCorrectedValue = new DateAutoCorrector().GetAutoCorrectedValue(
                    fieldData.Value.Field.Text,
                    pattern);

                if (fieldData.Value.Field.Text == autoCorrectedValue)
                {
                    break;
                }

                VBotLogger.Trace(() => $"[AutoCorrection] [Date] pattern \"{pattern}\" value \"{fieldData.Value.Field.Text}\" corrected to \"{autoCorrectedValue}\"");

                fieldData.Value.Field.Text = autoCorrectedValue;
            }

            return issue == FieldDataValidationIssueCodes.PatternMismatch 
                ? new PatternMismatchIssue(pattern) 
                : issue.ToValidationIssue();
        }
    }
}