/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine.Model;

    public static class FieldDefHelper
    {
        public static string GetApplicablePattern(this FieldDef fieldDef, Layout layout)
        {
            var fieldLayout = FieldFinder.GetLayoutField(fieldDef.Id, layout);

            return fieldDef.GetApplicablePattern(fieldLayout);
        }

        public static string GetApplicablePattern(this FieldDef fieldDef, FieldLayout fieldLayout)
        {
            if (!string.IsNullOrWhiteSpace(fieldLayout?.FormatExpression))
            {
                return fieldLayout.FormatExpression;
            }

            return string.IsNullOrWhiteSpace(fieldDef.FormatExpression)
                ? null
                : fieldDef.FormatExpression;
        }

        public static string GetApplicableStartWith(this FieldDef fieldDef, Layout layout)
        {
            var fieldLayout = FieldFinder.GetLayoutField(fieldDef.Id, layout);
            if (!string.IsNullOrWhiteSpace(fieldLayout?.StartsWith))
            {
                return fieldLayout.StartsWith;
            }

            return string.IsNullOrWhiteSpace(fieldDef.StartsWith)
                ? null
                : fieldDef.StartsWith;
        }

        public static string GetApplicableEndWith(this FieldDef fieldDef, Layout layout)
        {
            var fieldLayout = FieldFinder.GetLayoutField(fieldDef.Id, layout);
            if (!string.IsNullOrWhiteSpace(fieldLayout?.EndsWith))
            {
                return fieldLayout.EndsWith;
            }

            return string.IsNullOrWhiteSpace(fieldDef.EndsWith)
                ? null
                : fieldDef.EndsWith;
        }
    }
}