/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
namespace Automation.VisionBotEngine.Validation
{
    using System.Collections.Generic;
    using System.Globalization;
    using Automation.Services.Client;
    using Automation.VisionBotEngine.Model;
    public static class CultureHelper
    {
        /* For a list of culture codes refer https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx */
        /* For a list of date formats across different countries refer https://en.wikipedia.org/wiki/Date_format_by_country */
        /* Different number formats refer http://userguide.icu-project.org/formatparse */

        public static IEnumerable<CultureInfo> SupportedDateTimeCultures()
        {
            return new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("en-AU"),
                CultureInfo.CurrentCulture
            };
        }

        public static IEnumerable<CultureInfo> SupportedNumberCultures()
        {
            return new[]
            {
                new CultureInfo("en-US"),
                //new CultureInfo("de-DE"),
                //new CultureInfo("fr-FR"),
                CultureInfo.CurrentCulture
            };
        }
        public static CultureInfo GetCultureInfo(string language)
        {
            Dictionary<string, string> mapBetweenLanguageCodeAndCultureCode = getMappingBetweenLanguageCodeAndCultureCode();
            AliasServiceEndPointConsumer aliasService = new AliasServiceEndPointConsumer();
            AliasLanguageDetail languageDetail = aliasService.GetLanguageDetail(language);
            if (string.IsNullOrEmpty(languageDetail?.Name))
                language = "english";
            else
                language = languageDetail.Name?.ToLower();

            if (language != null && mapBetweenLanguageCodeAndCultureCode.ContainsKey(language))
            {
                return new CultureInfo(mapBetweenLanguageCodeAndCultureCode[language]);
            }
            return new CultureInfo("en");
        }

        private static Dictionary<string, string> getMappingBetweenLanguageCodeAndCultureCode()
        {
            Dictionary<string, string> mapBetweenLanguageAndCultureCode = new Dictionary<string, string>()
            {
                { "english","en" },
                { "german","de" },
                { "french","fr" },
                { "spanish","es" },
                { "italian","it" },

                { "afrikaans","af" },
                { "bulgarian","bg" },
                { "catalan","ca" },
                { "chinese-simplified","zh-Hans" },
                { "chinese-traditional","zh-Hant" },
                { "czech","cs" },
                { "danish","da" },
                { "dutch","nl" },
                { "flemish","nl" },
                { "greek","el" },
                { "hungarian","hu" },
                { "indonesian","id" },
                { "japanese","ja" },
                { "korean","ko" },
                { "malay","ms" },
                { "norwegian","nn" },//Two types are there
                { "polish","pl" },
                { "portuguese","pt" }, //Two types are there
                { "romanian","ro" },
                { "russian","ru" },
                { "serbian-latin","sr-Latn-RS" },
                { "slovak","sk" },
                { "swedish","sv" },
                { "turkish","tr" }
             };

            return mapBetweenLanguageAndCultureCode;
        }
    }
}