﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal static class StringOperations
    {
        public static bool PartiallyEquals(this string source, string target, double matchPercentage)
        {
            return GetSimilarity(source, target) >= matchPercentage;
        }

        public static double GetSimilarity(this string source, string target)
        {
            if ((source == null) || (target == null)) return 0.0;
            if ((source.Length == 0) || (target.Length == 0)) return 0.0;
            if (source == target) return 1.0;

            int distance = ComputeLevenshteinDistance(source, target);
            return (1.0 - ((double)distance / (double)Math.Max(source.Length, target.Length)));
        }

        public static int ComputeLevenshteinDistance(this string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if ((source.Length == 0) || (target.Length == 0)) return 0;
            if (source == target) return source.Length;

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }

        internal class StringComparisionResult
        {
            public string Value { get; private set; }
            public double MatchPercentage { get; private set; }

            public StringComparisionResult(string value, double matchPercentage)
            {
                Value = value;
                MatchPercentage = matchPercentage;
            }
        }

        public static string GetFirstBestFuzzyMatch(
            this IEnumerable<string> items,
            string source,
            double matchPercentage)
        {
            var matches = items.GetAllFuzzyMatch(source, matchPercentage);
            if ((matches?.Count ?? 0) > 1)
            {
                var firstMatch = matches[0];
                var secondMatch = matches[1];

                if (firstMatch.MatchPercentage.Equals(secondMatch.MatchPercentage)) return null;
            }

            return matches.FirstOrDefault()
                ?.Value;
        }

        public static IList<StringComparisionResult> GetAllFuzzyMatch(
            this IEnumerable<string> items,
            string source,
            double matchPercentage)
        {
            return (from item in items
                    let similarity = source.GetSimilarity(item)
                    where similarity >= matchPercentage
                    select new StringComparisionResult(item, similarity))
                .OrderByDescending(r => r.MatchPercentage)
                .ToList();
        }
    }
}