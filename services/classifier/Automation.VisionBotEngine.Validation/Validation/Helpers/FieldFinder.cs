/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System.Linq;

    using Automation.VisionBotEngine.Model;

    internal static class FieldFinder
    {
        public static FieldLayout GetLayoutField(string fieldId, Layout layout)
        {
            if (string.IsNullOrWhiteSpace(fieldId) || layout == null)
            {
                return null;
            }

            var result = layout.Fields.FirstOrDefault(field => fieldId == field.FieldId);
            if (result != null)
            {
                return result;
            }

            foreach (var tableLayout in layout.Tables)
            {
                result = tableLayout.Columns.FirstOrDefault(column => fieldId == column.FieldId);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }
    }
}