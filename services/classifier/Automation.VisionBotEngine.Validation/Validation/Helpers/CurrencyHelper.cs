﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System.Collections.Generic;

    internal static class CurrencyHelper
    {
        public static readonly string[] ValidCurrencySymbols = new[]
            {
                "$",
                "¥",
                "£",
                "₹",
                "€",
                "Rs",
                "USD",
                "EUR",
                "CAD",
                "AUD",
                "GBP",
                "INR",
            };

        public static readonly Dictionary<char, char[]> CurrencyAutoCorrectionEntries = new Dictionary<char, char[]>
            {
                { '$', new [] { 's', 'S' } },
                { '₹', new [] { 'R' } },
                { '¥', new [] { 'Y', 'y' } },
                { '€', new [] { 'E', 'e' } },
                { '£', new [] { 'E', 'S', 's' } },
                /*{ 'S', new [] { '5' } },
                { 's', new [] { '5' } },*/
            };

        public static string GetCurrencySymbol(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            value = value.Trim();

            foreach (var validCurrencySymbol in ValidCurrencySymbols)
            {
                if (value.StartsWith(validCurrencySymbol))
                {
                    var currencySymbolInString = value.Substring(0, validCurrencySymbol.Length);
                    if (currencySymbolInString != validCurrencySymbol)
                    {
                        continue;
                    }
                    return validCurrencySymbol;
                }

                if (value.EndsWith(validCurrencySymbol))
                {
                    var currencySymbolInString = value.Substring(value.Length - validCurrencySymbol.Length, validCurrencySymbol.Length);
                    if (currencySymbolInString != validCurrencySymbol)
                    {
                        continue;
                    }
                    return validCurrencySymbol;
                }
                if (value.Length > 1 && value.Length > validCurrencySymbol.Length)
                {
                    if (value.Substring(1, validCurrencySymbol.Length).Equals(validCurrencySymbol))
                    {
                        var currencySymbolInString = value.Substring(1, validCurrencySymbol.Length);
                        if (currencySymbolInString != validCurrencySymbol)
                        {
                            continue;
                        }
                        return validCurrencySymbol;
                    }
                }
            }

            return string.Empty;
        }

        public static string RemoveCurrencySymbol(string value)
        {
            var currentCurrencySymbol = GetCurrencySymbol(value);

            if (string.IsNullOrEmpty(currentCurrencySymbol))
            {
                return value;
            }

            var valueWithoutCurrencySymbol = value.StartsWith(currentCurrencySymbol) ? value.Substring(currentCurrencySymbol.Length)
                                            : value.Substring(1, currentCurrencySymbol.Length).Equals(currentCurrencySymbol) ? string.Concat(value.Substring(0, 1), value.Substring(currentCurrencySymbol.Length + 1, value.Length - currentCurrencySymbol.Length - 1))
                : value.Substring(0, value.Length - currentCurrencySymbol.Length);

            return valueWithoutCurrencySymbol.Trim();
        }
    }
}