﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Services.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;
using System.Drawing;

namespace Automation.VisionBotEngine.Validation.VisionbotServiceConsumer
{
    internal class VisionbotServiceEndPointConsumer : IVisionbotServiceEndPointConsumer
    {
        private HttpServiceAdapter _client = null;
        private ClassifiedFolderManifest _classifiedFolderManifest;
        public VisionbotServiceEndPointConsumer(ClassifiedFolderManifest classifiedFolderManifest)
        {
            _client = new HttpServiceAdapter(new VisionBotServiceClientProvider().GetClient());
            _classifiedFolderManifest = classifiedFolderManifest;
        }

        #region Interface Implementor

        public VbotValidatorData GetValidationData(string visionbotID)
        {
            string endPoint = getValidationDataAccessURI(visionbotID);

            var response = _client.Get<VbotValidatorData>(endPoint);
            return response;
        }

        public bool SetValidationData(string visionbotID, VbotValidatorData validationData)
        {
            string endPoint = getValidationDataAccessURI(visionbotID);
            var response = _client.Post<object, VbotValidatorData>(endPoint, validationData);
            return true;
        }

    
        #endregion Interface Implementor

        #region Private Methods

        private  string getVisionBotDataAccessURI(string visionbotID)
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}";
        }

        private  string getValidationDataAccessURI(string visionbotID)
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}/validationdata";
        }

        #endregion Private Methods
    }
}