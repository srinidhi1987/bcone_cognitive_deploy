﻿using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Validation.VisionbotServiceConsumer
{
    interface IVisionbotServiceEndPointConsumer
    {
        VbotValidatorData GetValidationData(string visionbotID);

        bool SetValidationData(string visionbotID, VbotValidatorData validationData);
    }
}
