

using System;
using System.Globalization;
/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
namespace Automation.VisionBotEngine.Validation
{
    public abstract class RegExPatternParser<T> : IParsableType<T> where T : struct
    {
        public abstract T? Parse(string value);

        public abstract T? Parse(string value, CultureInfo cultureInfo);

        public virtual T? Parse(string value, string pattern)
        {
            return string.IsNullOrWhiteSpace(pattern) || PatternHelper.IsMatch(value, pattern)
                ? this.Parse(value)
                : null;
        }

        public virtual T? Parse(string value, string pattern, CultureInfo cultureInfo)
        {
            return string.IsNullOrWhiteSpace(pattern) || PatternHelper.IsMatch(value, pattern)
               ? this.Parse(value,cultureInfo)
               : null;
        }
    }
}