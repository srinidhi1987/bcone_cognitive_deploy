///**
// * Copyright (c) 2016 Automation Anywhere.
// * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
// * All rights reserved.
// *
// * This software is the confidential and proprietary information of
// * Automation Anywhere.("Confidential Information").  You shall not
// * disclose such Confidential Information and shall use it only in
// * accordance with the terms of the license agreement you entered into
// * with Automation Anywhere.
// */

//namespace Automation.VisionBotEngine.Validation
//{
//    using System.IO;
//    using System.Reflection;

//    internal class VbotValidatorFileStorage : GenericVBotFileStorage<VbotValidatorData>
//    {
//        internal static string FileName = "validation.vdata";

//        public VbotValidatorFileStorage(string packageSourceFile)
//            : base(packageSourceFile)
//        {
//        }

//        public VbotValidatorFileStorage(Stream packageStream)
//            : base(packageStream)
//        {
//        }

//        protected override string GetMimeContentType()
//        {
//            return StandardMimeTypes.Json;
//        }

//        protected override string GetFileName()
//        {
//            return FileName;
//        }
//    }

//    [Obfuscation(Exclude = true, ApplyToMembers = true)]
//    public static class VBotValidatorDataCopier
//    {
//        public static void Copy(
//            Automation.Generic.Compression.ResourcePackage sourcePackage,
//            Automation.Generic.Compression.ResourcePackage destinationPackage)
//        {
//            // open source package
//            // check if file exists
//            // if exists, open destination
//            // copy contents

//            if (sourcePackage == null || destinationPackage == null) return;

//            var data = sourcePackage.GetResource(VbotValidatorFileStorage.FileName);
//            if (data == null || data.Content == null) return;

//            var destinationData = destinationPackage.GetResource(VbotValidatorFileStorage.FileName);
//            if (destinationData != null && destinationData.Content != null)
//                destinationPackage.DeleteFile(VbotValidatorFileStorage.FileName);

//            destinationPackage.AddResource(data);
//        }
//    }
//}