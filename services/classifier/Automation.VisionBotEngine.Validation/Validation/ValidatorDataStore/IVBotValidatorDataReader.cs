/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public interface IVBotValidatorDataReader
    {
        IDictionary<Type, ValidatorData> GetAll(string id);

        T Get<T>(string id) where T : ValidatorData;
    }
}