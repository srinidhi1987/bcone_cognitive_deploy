﻿using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;
using Automation.VisionBotEngine.Validation.VisionbotServiceConsumer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Validation
{
    internal class VBotValidatorServiceStorage : IValidatorDataStorage<VbotValidatorData>
    {

        private string _visionbotID = string.Empty;
        private VisionbotServiceConsumer.IVisionbotServiceEndPointConsumer _visionbotServiceEndPointConsumer = null;
        public VBotValidatorServiceStorage(ClassifiedFolderManifest classifiedFolderManifest,string visionbotID)
        {
            _visionbotID = visionbotID;
            _visionbotServiceEndPointConsumer = new VisionbotServiceConsumer.VisionbotServiceEndPointConsumer(classifiedFolderManifest);
        }
        public VbotValidatorData Load()
        {
           return _visionbotServiceEndPointConsumer.GetValidationData(_visionbotID);
        }
        public void Save(VbotValidatorData valdidationData)
        {
            _visionbotServiceEndPointConsumer.SetValidationData(_visionbotID,valdidationData);
        }
    }
}
