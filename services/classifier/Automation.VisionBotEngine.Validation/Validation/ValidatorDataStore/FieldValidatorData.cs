/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;

    using Newtonsoft.Json;
    using Generic.Serialization;

    internal class FieldValidatorData
    {
        public string ID { get; set; }

        private ValidatorData _validatorData;

        public ValidatorData ValidatorData
        {
            get
            {
                if (this._validatorData != null && this._validatorData.GetType() == typeof(ValidatorData))
                {
                    this._validatorData = JsonConvert.DeserializeObject(this.SerializedData, this.TypeOfData) as ValidatorData;
                }
                return this._validatorData;
            }
            set
            {
                this._validatorData = value;
            }
        }

        public Type TypeOfData { get; set; }
        public string SerializedData { get; set; }

        public void SetValidatorData(ValidatorData validatorData)
        {
            this.ValidatorData = validatorData;
            this.TypeOfData = validatorData.GetType();
            this.SerializedData = (new JSONSerialization()).SerializeToString(validatorData);
        }
    }
}