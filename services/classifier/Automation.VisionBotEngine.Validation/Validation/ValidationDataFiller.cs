/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Linq;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class ValidationDataFiller
    {
        private static IValidatorProvider __fieldValidatorProvider;

        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        public static IValidatorProvider FieldValidatorProvider
        {
            get
            {
                return __fieldValidatorProvider ?? (__fieldValidatorProvider = new GeneralValidatorProvider());
            }
            set
            {
                __fieldValidatorProvider = value;
            }
        }

        private static FormulaValidationManager formulaValidationManager;

        internal static FormulaValidationManager FormulaValidationManager
        {
            get
            {
                return formulaValidationManager ?? (formulaValidationManager = new FormulaValidationManager());
            }
            private set
            {
                formulaValidationManager = value;
            }
        }

        public static void EvaluateAndFillValidationDetails(this VBotDocument vBotDocument
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            if (vBotDocument == null)
            {
                return;
            }

            vBotDocument.Data.EvaluateAndFillValidationDetails(vBotDocument.Layout, vbotValidtorDataReader, validatorAdvanceInfo);
        }

        private static bool HasFirstValidationCheckDone = false;

        public static void EvaluateAndFillValidationDetails(this VBotData vBotData
            , Layout layout
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            try
            {
                HasFirstValidationCheckDone = false;

                evaluateAndFillValidationDetails(vBotData, layout, vbotValidtorDataReader, validatorAdvanceInfo);

                HasFirstValidationCheckDone = true;

                FieldValidatorProvider = new FormulaValidatorProvider();
                evaluateAndFillValidationDetails(vBotData, layout, vbotValidtorDataReader, validatorAdvanceInfo);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidationDataFiller), nameof(evaluateAndFillValidationDetails));
            }
            finally
            {
                if (FormulaValidationManager != null)
                {
                    FormulaValidationManager = null;
                }
                if (FieldValidatorProvider != null)
                {
                    FieldValidatorProvider = null;
                }
                HasFirstValidationCheckDone = false;
            }
        }

        private static void evaluateAndFillValidationDetails(VBotData vBotData
            , Layout layout
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            if (vBotData == null)
            {
                return;
            }

            vBotData.FieldDataRecord.EvaluateAndFillValidationDetails(layout, vBotData, vbotValidtorDataReader, validatorAdvanceInfo);

            foreach (var tableValue in vBotData.TableDataRecord)
            {
                tableValue.EvaluateAndFillValidationDetails(layout, vBotData, vbotValidtorDataReader, validatorAdvanceInfo);
            }
        }

        private static void EvaluateAndFillValidationDetails(this TableValue tableValue
            , Layout layout
            , VBotData vBotData
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            tableValue.ConditionEmptyTable();

            foreach (var dataRecord in tableValue.Rows)
            {
                dataRecord.EvaluateAndFillValidationDetails(layout, vBotData, vbotValidtorDataReader, validatorAdvanceInfo);
            }
        }

        private static void EvaluateAndFillValidationDetails(this DataRecord dataRecord
            , Layout layout
            , VBotData vBotData
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            foreach (var field in dataRecord.Fields)
            {
                field.EvaluateAndFillValidationDetails(layout, vBotData, vbotValidtorDataReader, validatorAdvanceInfo);
            }
        }

        private static void EvaluateAndFillValidationDetails(this FieldData fieldData
            , Layout layout
            , VBotData vBotData
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            try
            {
                fieldData.CheckAndPlaceDefaultValueIfApplicable();

                var fieldValidators = FieldValidatorProvider.GetAllFieldValidators(validatorAdvanceInfo);

                fieldData.ValidationIssue = HasFirstValidationCheckDone ? fieldData.ValidationIssue : null;
                foreach (var fieldValidator in fieldValidators)
                {
                    var validationIssue = fieldValidator.Validate(fieldData, layout, vBotData, vbotValidtorDataReader);
                    if (validationIssue.HasValue())
                    {
                        fieldData.ValidationIssue = validationIssue;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidationDataFiller), nameof(evaluateAndFillValidationDetails), $"validating field \"{fieldData.Field.Id}\" with value \"{fieldData.Value.Field.Text}\"");
            }
        }

        public static void EvaluateAndFillValidationDetailsExceptFormulaValidation(this FieldData fieldData
            , Layout layout
            , VBotData vBotData
            , IVBotValidatorDataReader vbotValidtorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            fieldData.EvaluateAndFillValidationDetails(layout, vBotData, vbotValidtorDataReader, validatorAdvanceInfo);
        }

        private static void CheckAndPlaceDefaultValueIfApplicable(this FieldData fieldData)
        {
            if (string.IsNullOrWhiteSpace(fieldData.Value.Field.Text) &&
                fieldData.Field.IsRequired &&
                !string.IsNullOrWhiteSpace(fieldData.Field.DefaultValue))
            {
                fieldData.Value.Field.Text = fieldData.Field.DefaultValue;
            }
        }
    }

    internal static class EmptyTableCatcher
    {
        public static void ConditionEmptyTable(this TableValue tableValue)
        {
            if (tableValue.HasRequiredColumn() && tableValue.IsEmpty())
            {
                tableValue.AddEmptyRow();
            }
        }

        private static bool HasRequiredColumn(this TableValue tableValue)
        {
            return tableValue != null &&
                tableValue.TableDef != null &&
                tableValue.TableDef.Columns != null &&
                tableValue.TableDef.Columns.FirstOrDefault(x => x.IsRequired) != null;
        }

        private static bool IsEmpty(this TableValue tableValue)
        {
            return tableValue == null ||
                tableValue.TableDef == null ||
                tableValue.TableDef.Columns == null ||
                tableValue.TableDef.Columns.Count <= 0 ||
                tableValue.Rows == null ||
                tableValue.Rows.Count <= 0;
        }

        private static void AddEmptyRow(this TableValue tableValue)
        {
            if (tableValue == null ||
                tableValue.TableDef == null ||
                tableValue.TableDef.Columns == null ||
                tableValue.TableDef.Columns.Count <= 0 ||
                tableValue.Rows == null)
                return;

            tableValue.Rows.Add(tableValue.CreateEmptyRow());
        }

        private static DataRecord CreateEmptyRow(this TableValue tableValue)
        {
            var result = new DataRecord();
            result.Fields = new System.Collections.Generic.List<FieldData>();

            foreach (var fieldDef in tableValue.TableDef.Columns.FindAll(c => c.IsDeleted == false))
            {
                var fieldData = new FieldData();

                fieldData.Field = fieldDef;
                fieldData.Value = new TextValue { Field = new Field() };

                result.Fields.Add(fieldData);
            }

            return result;
        }
    }
}