/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence;
    using System.Collections.Generic;
    using System.Globalization;

    public class GeneralValidatorProvider : IValidatorProvider
    {
        private static IList<IFieldValidator> Validators;

        public IList<IFieldValidator> GetAllFieldValidators()
        {
            return Validators ?? (Validators = this.GetValidators());
        }

        public IList<IFieldValidator> GetAllFieldValidators(ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            return Validators ?? (Validators = this.GetValidators(validatorAdvanceInfo));
        }

        private IList<IFieldValidator> GetValidators()
        {
            return new List<IFieldValidator>
            {
                new RequiredFieldValidator(),

                new TextTypeValidator(),
                new DateTypeValidator(),
                new NumberTypeValidator(),

                new ListValidator(),

                new StartsWithValidator(),
                new EndsWithValidator(),
                new ConfidenceValidator()
            };
        }

        private IList<IFieldValidator> GetValidators(ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            return new List<IFieldValidator>
            {
                new RequiredFieldValidator(),

                new TextTypeValidator(),
                new DateTypeValidator(),
                new NumberTypeValidator(validatorAdvanceInfo),

                new ListValidator(),

                new StartsWithValidator(),
                new EndsWithValidator(),
                new ConfidenceValidator(validatorAdvanceInfo)
            };
        }
    }

    public class ValidatorAdvanceInfo
    {
        public CultureInfo CultureInfo { get; set; } = new CultureInfo("en");

        public int ConfidenceThreshold { get; set; } = 0;
    }

}