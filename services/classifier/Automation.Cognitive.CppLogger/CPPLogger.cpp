#include "CPPLogger.h"
#include "Log4CPlusWrapper.h"

CPPLogger* CPPLogger::_cppLogger = NULL;

CPPLogger::CPPLogger()
{
}

CPPLogger::~CPPLogger()
{
	if (_cppLogger)
		_cppLogger->ShutDown();
	_cppLogger = NULL;
}
CPPLogger *CPPLogger::GetCppLogger(CPPLoggerConfiguration loggerConfig)
{
	if (loggerConfig.eLoggerType == eLoggerType::LOG4CPlus)
		_cppLogger = Log4CPlusWrapper::GetSingletoneInstance(loggerConfig);
	else
		_cppLogger = Log4CPlusWrapper::GetSingletoneInstance(loggerConfig);

	_cppLogger->SetLogLevel(loggerConfig.elogLevel);
	return _cppLogger;
}
void CPPLogger::Trace(const char * format, ...)
{
	if (GetLogLevel() > eLogLevel::TRACE_LEVEL)
		return;
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);

	Trace(std::string(sMessage));
}
void CPPLogger::Error(const char * format, ...)
{
	if (GetLogLevel() > eLogLevel::ERROR_LEVEL)
		return;
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);

	Error(std::string(sMessage));
}
void CPPLogger::Debug(const char * format, ...)
{
	if (GetLogLevel() > eLogLevel::DEBUG_LEVEL)
		return;
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);

	Debug(std::string(sMessage));
}
void CPPLogger::Info(const char * format, ...)
{
	if (GetLogLevel() > eLogLevel::INFO_LEVEL)
		return;
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);

	Info(std::string(sMessage));
}
void CPPLogger::Warning(const char * format, ...)
{
	if (GetLogLevel() > eLogLevel::WARNING_LEVEL)
		return;
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);

	Warning(std::string(sMessage));
}
void CPPLogger::Fatal(const char * format, ...)
{
	if (GetLogLevel() > eLogLevel::ERROR_LEVEL)
		return;
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);

	Fatal(std::string(sMessage));
}

std::string CPPLogger::ParseArgs(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	int nLength = _vscprintf(format, args) + 1;
	cout << "Eror Occured: " << nLength << endl;
	char* sMessage = new char[nLength];
	vsprintf_s(sMessage, nLength, format, args);
	cout << "Eror Occured: " << sMessage << endl;
	return sMessage;
}

eLogLevel CPPLogger::GetLogLevel()
{
	return _logLevel;
}