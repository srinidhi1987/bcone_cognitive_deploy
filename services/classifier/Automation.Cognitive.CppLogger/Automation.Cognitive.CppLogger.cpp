// Automation.Cognitive.CppLogger.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CPPLogger.h"

int main()
{
	CPPLoggerConfiguration loggerConfiguration;
	loggerConfiguration.eLoggerType = eLoggerType::LOG4CPlus;
	loggerConfiguration.elogLevel = eLogLevel::ALL_LEVEL;
	loggerConfiguration.logFilePath = "D:\\Log4Log.log";
	loggerConfiguration.transactionID = "Congnitive-Classifier";
	CPPLogger *cppLogger = CPPLogger::GetCppLogger(loggerConfiguration);
	int var = 111;
	printf("This is an Error Message [%d]\n", var);
	cppLogger->Error("This is an Error Message [%d]", var);
	cppLogger->Info("This is Info Message");
	cppLogger->Fatal("This is Fatal Message");
	cppLogger->Warning("This is Warning Message");
	cppLogger->Debug("This is Debug Message");
	cppLogger->Trace("This is Trace Message");

	getchar();
	return 0;
}