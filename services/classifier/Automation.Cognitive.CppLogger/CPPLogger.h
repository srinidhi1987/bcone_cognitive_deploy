#pragma once
#include <string>
#include <cstdarg>
#include <stdio.h>
using namespace std;

enum eLoggerType
{
	LOG4CPlus
};
enum eLogLevel
{
	ALL_LEVEL = 1,
	TRACE_LEVEL = 2,
	DEBUG_LEVEL = 3,
	INFO_LEVEL = 4,
	WARNING_LEVEL = 5,
	ERROR_LEVEL = 6,
	OFF_LEVEL = 7
};
struct CPPLoggerConfiguration
{
public:
	eLogLevel elogLevel;
	eLoggerType eLoggerType;
	std::string logFilePath;
	std::string transactionID;
};
class __declspec(dllexport) CPPLogger
{
public:
	static CPPLogger * GetCppLogger(CPPLoggerConfiguration loggerConfig);
	void Trace(const char * format, ...);
	void Error(const char * format, ...);
	void Debug(const char * format, ...);
	void Info(const char * format, ...);
	void Warning(const char * format, ...);
	void Fatal(const char * format, ...);
	virtual void ShutDown() = 0;
	virtual void SetLogLevel(eLogLevel loglevel) = 0;

	eLogLevel GetLogLevel();
protected:
	eLogLevel _logLevel;
	CPPLoggerConfiguration _loggerConfiguration;
	static CPPLogger *_cppLogger;
	virtual void Trace(const std::string& sMessage) = 0;
	virtual void Error(const std::string& sMessage) = 0;
	virtual void Debug(const std::string& sMessage) = 0;
	virtual void Info(const std::string& sMessage) = 0;
	virtual void Warning(const std::string& sMessage) = 0;
	virtual void Fatal(const std::string& sMessage) = 0;
	std::string  ParseArgs(const char * format, ...);
	CPPLogger();
	~CPPLogger();
};
