#pragma once

#include "CPPLogger.h"
#include <log4cplus/logger.h>
#include <log4cplus/layout.h>
#include <log4cplus/fileappender.h>
#include <log4cplus\mdc.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/thread/threads.h>
#include <log4cplus/helpers/sleep.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>
#include <string>

#define LOGGER CLogger::Getlog4CPlusInstance()

class Log4CPlusWrapper :public CPPLogger
{
public:
	virtual void ShutDown();
	void SetTransactionID(std::string id);
	static Log4CPlusWrapper* GetSingletoneInstance(CPPLoggerConfiguration loggerConfigration);

	virtual void Trace(const std::string& sMessage);
	virtual void Error(const std::string& sMessage);
	virtual void Debug(const std::string& sMessage);
	virtual void Info(const std::string& sMessage);
	virtual void Warning(const std::string& sMessage);
	virtual void Fatal(const std::string& sMessage);
	virtual void SetLogLevel(eLogLevel loglevel);
private:
	std::map<int, log4cplus::LogLevel> _log4CPlusLogLevel;
	log4cplus::tstring GetLog4String(std::string stringToConvert);
	static log4cplus::Logger Getlog4CPlusInstance();
	void InitializeLogging();
	static Log4CPlusWrapper *_loggerInstance;
	Log4CPlusWrapper();
	~Log4CPlusWrapper();
};
