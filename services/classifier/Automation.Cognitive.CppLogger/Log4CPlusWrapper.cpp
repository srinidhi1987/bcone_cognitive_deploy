#include "Log4CPlusWrapper.h"
#include "log4cplus\loggingmacros.h"

Log4CPlusWrapper* Log4CPlusWrapper::_loggerInstance = NULL;
#define TRANSACTIONID_KEY std::string("TransactionId")

using namespace std;
using namespace log4cplus;
using namespace log4cplus::helpers;

Log4CPlusWrapper::Log4CPlusWrapper()
{
	_log4CPlusLogLevel[eLogLevel::ALL_LEVEL] = log4cplus::ALL_LOG_LEVEL;
	_log4CPlusLogLevel[eLogLevel::TRACE_LEVEL] = log4cplus::TRACE_LOG_LEVEL;
	_log4CPlusLogLevel[eLogLevel::DEBUG_LEVEL] = log4cplus::DEBUG_LOG_LEVEL;
	_log4CPlusLogLevel[eLogLevel::INFO_LEVEL] = log4cplus::INFO_LOG_LEVEL;
	_log4CPlusLogLevel[eLogLevel::ERROR_LEVEL] = log4cplus::ERROR_LOG_LEVEL;
	_log4CPlusLogLevel[eLogLevel::WARNING_LEVEL] = log4cplus::WARN_LOG_LEVEL;
	_log4CPlusLogLevel[eLogLevel::OFF_LEVEL] = log4cplus::OFF_LOG_LEVEL;
}

Log4CPlusWrapper::~Log4CPlusWrapper()
{
}
void Log4CPlusWrapper::ShutDown()
{
	LOG4CPLUS_TRACE(Getlog4CPlusInstance(), "Log4CPlusWrapper::ShutDown() Called");
	Getlog4CPlusInstance().shutdown();
	_loggerInstance = NULL;
}

Log4CPlusWrapper* Log4CPlusWrapper::GetSingletoneInstance(CPPLoggerConfiguration loggerConfigration)
{
	try {
		if (_loggerInstance == NULL)
		{
			_loggerInstance = new Log4CPlusWrapper;
			_loggerInstance->_loggerConfiguration = loggerConfigration;;
			_loggerInstance->InitializeLogging();
		}
	}
	catch (std::exception exc)
	{
		throw new std::exception("Error in Initializing log4CPlus");
	}
	return _loggerInstance;
}
void Log4CPlusWrapper::InitializeLogging()
{
	log4cplus::initialize();

	SetTransactionID(_loggerConfiguration.transactionID);

	SharedAppenderPtr append_1(
		new RollingFileAppender(GetLog4String(_loggerConfiguration.logFilePath), 10 * 1024 * 1024, 5));

	std::string logPattern = std::string("<%p> %d[ThreadID:%t]  <%X{").append(TRANSACTIONID_KEY).append("}> %m %n");
	log4cplus::tstring pattern = GetLog4String(logPattern);// LOG4CPLUS_TEXT("<%p> %d [ThreadID: %t]  <%X{TransactionId}> %m %n");
	append_1->setLayout(std::auto_ptr<Layout>(new PatternLayout(pattern)));
	Logger::getRoot().addAppender(append_1);
}
log4cplus::tstring Log4CPlusWrapper::GetLog4String(std::string stringToConvert)
{
	return log4cplus::tstring(stringToConvert.begin(), stringToConvert.end());
}

void Log4CPlusWrapper::SetTransactionID(std::string id)
{
	log4cplus::getMDC().put(GetLog4String(TRANSACTIONID_KEY), GetLog4String(id));
}
log4cplus::Logger Log4CPlusWrapper::Getlog4CPlusInstance()
{
	return Logger::getInstance(LOG4CPLUS_TEXT("cognitive"));
}
void Log4CPlusWrapper::Trace(const std::string& sMessage)
{
	LOG4CPLUS_TRACE(Getlog4CPlusInstance(), sMessage.c_str());
}
void Log4CPlusWrapper::Error(const std::string& sMessage)
{
	LOG4CPLUS_ERROR(Getlog4CPlusInstance(), sMessage.c_str());
}
void Log4CPlusWrapper::Debug(const std::string& sMessage)
{
	LOG4CPLUS_DEBUG(Getlog4CPlusInstance(), sMessage.c_str());
}
void Log4CPlusWrapper::Info(const std::string& sMessage)
{
	LOG4CPLUS_INFO(Getlog4CPlusInstance(), sMessage.c_str());
}
void Log4CPlusWrapper::Warning(const std::string& sMessage)
{
	LOG4CPLUS_WARN(Getlog4CPlusInstance(), sMessage.c_str());
}
void Log4CPlusWrapper::Fatal(const std::string& sMessage)
{
	LOG4CPLUS_FATAL(Getlog4CPlusInstance(), sMessage.c_str());
}
void Log4CPlusWrapper::SetLogLevel(eLogLevel loglevel)
{
	_logLevel = loglevel;
	Getlog4CPlusInstance().setLogLevel(_log4CPlusLogLevel[_logLevel]);
}