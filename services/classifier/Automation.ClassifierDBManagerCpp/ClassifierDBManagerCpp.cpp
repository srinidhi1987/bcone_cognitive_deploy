// ClassifierDBManagerCpp.cpp : main project file.

#include "stdafx.h"
#include <string>
#include <sstream>
#include "../Automation.Cognitive.CppLogger/CPPLogger.h"
#include <msclr\marshal_cppstd.h>

using namespace System;
using namespace System::Data::SqlClient;
using namespace System::Data;
using namespace std;

class IDataAccess
{
public:

	virtual int Insert(String^ strHashKey, String ^strClassName, String ^connectionString, CPPLogger *loggerObj) = 0;

	virtual int GetClassName(String^ strHasKey, String ^connectionString, CPPLogger *loggerObj) = 0;
	virtual int GetNoOfClasses(String ^connectionString, CPPLogger *loggerObj) = 0;
};

class DbDataAccess : public IDataAccess
{
public:

	DbDataAccess()
	{
	}
	std::string GetStdString(String ^managedString)
	{
		std::string unmanaged = msclr::interop::marshal_as<std::string>(managedString);
		return unmanaged;
	}
	int Insert(String ^strHashKey, String ^strClassName, String ^connectionString, CPPLogger *loggerObj)
	{
		String ^CommandText = "Insert into LayoutTrainset (hashkey, classname) values (@hashkey, @classname);SELECT SCOPE_IDENTITY();";
		try
		{
			{
				loggerObj->Trace("Opening Connection...");
				SqlConnection myConnection(connectionString);
				myConnection.Open();

				loggerObj->Trace("Executing Insert...");
				SqlCommand ^cmd = myConnection.CreateCommand();
				cmd->CommandText = CommandText;
				cmd->Parameters->AddWithValue("@hashkey", strHashKey);
				cmd->Parameters->AddWithValue("@classname", strClassName);
				loggerObj->Trace("Getting ClassName...");
				Object ^insertSuccessful = cmd->ExecuteScalar();
				loggerObj->Trace("Returning ClassName...");
				return Convert::ToInt32(insertSuccessful);
			}
		}
		catch (Exception ^ex)
		{
			loggerObj->Trace(GetStdString(ex->Message).c_str());
			loggerObj->Trace("Same Class exists so taking ClassName");

			return GetClassName(strHashKey, connectionString, loggerObj);
		}
	}

	int GetClassName(String ^strHashKey, String ^connectionString, CPPLogger *loggerObj)
	{
		System::Data::DataTable ^result;
		String ^strCommand = String::Format("SELECT classname FROM LayoutTrainset where hashkey='{0}'", strHashKey);
		try
		{
			{
				loggerObj->Trace("GetClassName::Opening connection...");
				SqlConnection ^myConnection = gcnew SqlConnection(connectionString);
				myConnection->Open();
				loggerObj->Trace(GetStdString(strCommand).c_str());
				SqlCommand ^cmd = myConnection->CreateCommand();
				cmd->CommandText = strCommand;
				loggerObj->Trace("GetClassName::Executing SELECT query...");
				SqlDataReader ^reader = cmd->ExecuteReader();
				while (reader->Read())
				{
					return Convert::ToInt32(reader->GetValue(reader->GetOrdinal("classname")));
				}
			}
		}
		catch (Exception ^ex)
		{
			loggerObj->Error(GetStdString(ex->Message).c_str());
			loggerObj->Error("Error Occured during getting ClassName, so returning -1");
			return -1;
		}
	}
	int GetNoOfClasses(String^ connectionString, CPPLogger *loggerObj)
	{
		System::Data::DataTable ^result;
		String ^strCommand = String::Format("SELECT COUNT(LayoutTrainset.hashkey) FROM LayoutTrainset");
		try
		{
			{
				loggerObj->Trace("GetNoOfClasses::Opening connection...");
				SqlConnection ^myConnection = gcnew SqlConnection(connectionString);
				myConnection->Open();
				SqlCommand ^cmd = myConnection->CreateCommand();
				loggerObj->Trace(GetStdString(strCommand).c_str());
				cmd->CommandText = strCommand;
				SqlDataReader ^reader = cmd->ExecuteReader();
				while (reader->Read())
				{
					return Convert::ToInt32(reader->GetInt32(0));
				}
			}
		}
		catch (Exception ^ex)
		{
			loggerObj->Error(GetStdString(ex->Message).c_str());
			loggerObj->Error("Error Occured during getting No Of Classes, so returning 0");
			return 0;
		}
	}
};
class DataAccessFactory
{
public:
	static IDataAccess* GetDataAccessObject()
	{
		return new DbDataAccess();
	}
};
extern "C"
{
	int _declspec(dllexport)GetLayoutClassName(char *cHashKey, char * connectionString, CPPLogger *loggerObj)
	{
		String ^strKey = gcnew String(cHashKey);
		String ^strConnectionString = gcnew String(connectionString);
		int iNoOfClasses = DataAccessFactory::GetDataAccessObject()->GetNoOfClasses(strConnectionString, loggerObj) + 1;
		String ^strClassName = String::Format("{0}", iNoOfClasses);
		return DataAccessFactory::GetDataAccessObject()->Insert(strKey, strClassName, strConnectionString, loggerObj);
	}
};
/*
int main()
{
	String ^strDbConnection = System::Configuration::ConfigurationManager::AppSettings["DbConnectionString"];
	Console::WriteLine(System::Configuration::ConfigurationManager::AppSettings["DbConnectionString"]);
	Object ^section = System::Configuration::ConfigurationManager::GetSection("appSettings");
	Console::WriteLine(L"Hello World");
	Console::ReadLine();
	return 0;
}
*/