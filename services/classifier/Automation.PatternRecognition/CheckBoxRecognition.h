#pragma once
#include "includefiles.h"

using namespace std;
using namespace cv;

enum CheckBoxStatus
{
	UnChecked,
	Checked,
	NotFound = -1
};

class CheckBoxRecognition
{
private:
	CheckBoxStatus eReturnValue;
public:
	CheckBoxRecognition();
	~CheckBoxRecognition();
	CheckBoxStatus GetCheckBoxStatus(char* imagePath, int X, int Y, int width, int height);
};
