/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
#pragma once

#include "includefiles.h"
#include "ReadWriteImageOperation.h"
#include "LineRemover.h"

//#define SAVE_IMAGE_TO_DEBUG 1

using namespace std;
using namespace cv;

class ImageOperations
{
private:
	ReadWriteImageOperation oReadWriteImageOperation;

public:

	Mat GetCroppedImageOfRectangle(Mat targetImage, RotatedRect rectImage)
	{
		cv::Point2f corners[4];
		rectImage.points(corners);

		int minX = 10000;
		int minY = 10000;
		int maxX = 0;
		int maxY = 0;

		for (int i = 0; i < 4; i++)
		{
			if (corners[i].x < minX)
			{
				minX = corners[i].x;
			}
			if (corners[i].y < minY)
			{
				minY = corners[i].y;
			}
			if (corners[i].x > maxX)
			{
				maxX = corners[i].x;
			}
			if (corners[i].y > maxY)
			{
				maxY = corners[i].y;
			}
		}
		return CropSmallImageFromBigImage(targetImage, minX, minY, maxX - minX, maxY - minY);
	}

	Mat CropSmallImageFromBigImage(Mat mImage, int X, int Y, int width, int height)
	{
		cv::Mat cropped;

		if (mImage.cols <= width && mImage.rows <= height)
			return cropped;

		Rect rect(X, Y, width, height);

		Mat croppedRef(mImage, rect);

		croppedRef.copyTo(cropped);
		return cropped;
	}

	Mat MakeBorderOutsideImage(Mat src)
	{
		Mat dst = src;
		int top = (int)(0.01*src.rows); int bottom = (int)(0.01*src.rows);
		int left = (int)(0.01*src.cols); int right = (int)(0.01*src.cols);
		int padding = 2;
		copyMakeBorder(src, dst, padding, padding, padding, padding, 0, Scalar(255, 255, 255));
		return dst;
	}

	RotatedRect FindRectangle(cv::Mat input)
	{
		//convert to grayscale (you could load as grayscale instead)
		cv::Mat gray;
		cv::cvtColor(input, gray, CV_BGR2GRAY);

		int verticalsize = gray.rows / (gray.rows*.50);

		// Create structure element for extracting vertical lines through morphology operations
		Mat verticalStructure = getStructuringElement(MORPH_RECT, Size(1, verticalsize));

		// Apply morphology operations
		erode(gray, gray, verticalStructure, Point(-1, -1));
		dilate(gray, gray, verticalStructure, Point(-1, -1));

		// Inverse vertical image
		bitwise_not(gray, gray);

		// Step 1
		Mat edges;
		adaptiveThreshold(gray, edges, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 3, -2);

		// Step 2
		Mat kernel = Mat::ones(2, 2, CV_8UC1);
		dilate(edges, edges, kernel);

		// Step 3
		Mat smooth;
		gray.copyTo(smooth);
		// Step 4
		blur(smooth, smooth, Size(2, 2));
		// Step 5
		smooth.copyTo(gray, edges);

		//imwrite("D:\\image.png", gray);

		// find contours (if always so easy to segment as your image, you could just add the black/rect pixels to a vector)
		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Vec4i> hierarchy;
		cv::findContours(gray, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

		/// Draw contours and find biggest contour (if there are other contours in the image, we assume the biggest one is the desired rect)
		// drawing here is only for demonstration!
		int biggestContourIdx = -1;
		float biggestContourArea = 0;
		for (int i = 0; i < contours.size(); i++)
		{
			float ctArea = cv::contourArea(contours[i]);
			if (ctArea > biggestContourArea)
			{
				biggestContourArea = ctArea;
				biggestContourIdx = i;
			}
		}

		cv::RotatedRect EmptyboundingBox;
		// if no contour found
		if (biggestContourIdx < 0)
		{
			return EmptyboundingBox;
		}

		// compute the rotated bounding rect of the biggest contour! (this is the part that does what you want/need)
		RotatedRect boundingBox = cv::minAreaRect(contours[biggestContourIdx]);
		// one thing to remark: this will compute the OUTER boundary box, so maybe you have to erode/dilate if you want something between the ragged lines

		//cv::rectangle(input, boundingBox.boundingRect(), cv::Scalar(255, 0, 0), 1);

		//imwrite("D:\\image1.png", input);
		//	waitKey(0);
		int angle = boundingBox.angle;

		if ((abs(angle)) % 90 != 0)
			return EmptyboundingBox;

		return boundingBox;
	}

	bool ResizeImage(char* imageFilePath, int resizeRatio, int interpolationMethod)
	{
		Mat image = oReadWriteImageOperation.ReadImage(imageFilePath);
		if (interpolationMethod > 16)
		{
			interpolationMethod = 1;
		}
		if (image.data)
		{
			resize(image, image, Size(image.cols*resizeRatio, image.rows*resizeRatio), 0, 0, interpolationMethod);
			return oReadWriteImageOperation.WriteImage(image, imageFilePath);
		}
		return ReturnValue::Failure;
	}

	void RemoveVerticalAndHorizontalLines(char* imagePath, char* onlyLineImage)
	{
		//Sometime crashes, if we rewrite image then format will change and work fine
		Mat image = oReadWriteImageOperation.ReadImage(imagePath);
		if (image.data)
		{
			if (image.channels() >= 3)
				cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
			cv::threshold(image, image, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
			oReadWriteImageOperation.WriteImage(image, imagePath);
			image.release();
		}

		PIX* pixa = pixRead(imagePath);
		SavePIXToDebug("ImageTobeLineRemoved.tiff", pixa, IFF_TIFF);

		if (pixa != nullptr && pixa->w > 0 && pixa->h > 0)
		{
			PIX *pixb = pixConvertTo8(pixa, 1);
			if (pixb->w > 0 && pixb->h > 0)
			{
				LineRemover mLineRemover;
				PIX* imgLineRemoved = mLineRemover.RemoveHorizontalLines(mLineRemover.RemoveVerticalLines(pixb));
				SavePIXToDebug("LineRemovedByLeptonica.tiff", imgLineRemoved, IFF_TIFF);
				if (imgLineRemoved->w > 0 && imgLineRemoved->w < 10000)
				{
					pixWrite(imagePath, imgLineRemoved, 1);
					SaveLineImage(onlyLineImage, pixa, imgLineRemoved);
				}
				/*if (pixb->data && pixb->w > 0 && pixb->w < 10000)
					pixDestroy(&pixb);*/
			}
			/*if (pixa->data && pixa->w > 0 && pixa->w < 10000)
				pixDestroy(&pixa);*/
		}
	}
	void RemoveVerticalAndHorizontalLinesByFillHoles(char* imagePath, char* onlyLineImage)
	{
		PIX* pixFillHoles = pixRead(onlyLineImage);
		if (pixFillHoles != nullptr && pixFillHoles->w > 0 && pixFillHoles->h > 0)
		{
			PIX *pixFillHolesb = pixConvertTo8(pixFillHoles, 1);
			PIX* pixaSecondErode = pixErodeGray(pixFillHolesb, 3, 3);
			if (pixaSecondErode->w > 0 && pixaSecondErode->h > 0)
			{
				LineRemover mLineRemover;
				PIX* imgLineRemoved = mLineRemover.RemoveAllLines(pixaSecondErode);
				if (imgLineRemoved->w > 0 && imgLineRemoved->w < 10000)
				{
					SavePIXToDebug("LineRemoved.tiff", imgLineRemoved, IFF_TIFF);
					
					SaveLineImage(onlyLineImage, pixFillHoles, imgLineRemoved);
					
					cv::Mat imgOnlyLine = cv::imread(onlyLineImage);
					cv::Mat imgOrg = cv::imread(imagePath);
					if (imgOrg.channels() >= 3)
						cv::cvtColor(imgOrg, imgOrg, cv::COLOR_BGR2GRAY);
					cv::threshold(imgOrg, imgOrg, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
					
					if (imgOnlyLine.channels() >= 3)
						cv::cvtColor(imgOnlyLine, imgOnlyLine, cv::COLOR_BGR2GRAY);
					cv::threshold(imgOnlyLine, imgOnlyLine, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
					
					cv::Mat imgAddToOrg = imgOnlyLine.clone();
					cv::add(imgOrg, imgOnlyLine, imgAddToOrg);

					SaveMatToDebug("AddImage.png", imgAddToOrg);

					cv::Mat imgFinalLineRemoved = imgAddToOrg.clone();
					cv::subtract(imgAddToOrg, imgOrg, imgFinalLineRemoved);

					SaveMatToDebug("Subtract.png", imgFinalLineRemoved);

					cv::bitwise_not(imgFinalLineRemoved, imgFinalLineRemoved);

					SaveMatToDebug("FinalLineRemoved.png", imgFinalLineRemoved);
					
					cv::imwrite(imagePath, imgFinalLineRemoved);
				}
				if (imgLineRemoved && imgLineRemoved->data && imgLineRemoved->w > 0 && imgLineRemoved->w < 10000)
					pixDestroy(&imgLineRemoved);
			}
			if (pixFillHolesb && pixFillHolesb->data && pixFillHolesb->w > 0 && pixFillHolesb->w < 10000)
				pixDestroy(&pixFillHolesb);
			if (!pixaSecondErode && pixaSecondErode->data && pixaSecondErode->w > 0 && pixaSecondErode->w < 10000)
				pixDestroy(&pixaSecondErode);
		}
		if (pixFillHoles && pixFillHoles->data && pixFillHoles->w > 0 && pixFillHoles->w < 10000)
			pixDestroy(&pixFillHoles);


	}
	void SaveMatToDebug(std::string fileName, cv::Mat image)
	{
		
		#ifdef SAVE_IMAGE_TO_DEBUG
		
		std::string filePath = "C:\\Temp\\" + fileName;
		cv::imwrite(filePath, image);
		
		#endif
	}
	void SavePIXToDebug(std::string fileName, PIX* image, l_int32 format)
	{

		#ifdef SAVE_IMAGE_TO_DEBUG

		std::string filePath = "C:\\Temp\\" + fileName;
		pixWrite(filePath.c_str(), image, format);

		#endif
	}
	void SaveLineImage(char* lineImagePath, PIX* srcImage, PIX* lineRemovedImage)
	{
		PIX * imgOnlyLine = pixCreate(srcImage->w, srcImage->h, 1);
		if(srcImage->d != 1)
			srcImage = pixConvertTo1(srcImage, 128);
		if (lineRemovedImage->d != 1)
			lineRemovedImage = pixConvertTo1(lineRemovedImage, 128);
		pixXor(imgOnlyLine, lineRemovedImage, srcImage);
		pixWrite(lineImagePath, imgOnlyLine, 1);
	}
	void ApplySmoothingNSharpeningOnImage(char* imagePath)
	{
		cv::Mat erodImg, srcImg, blurimg, thrimg;
		srcImg = oReadWriteImageOperation.ReadImage(imagePath);
		if (srcImg.channels() >= 3)
			cv::cvtColor(srcImg, srcImg, cv::COLOR_BGR2GRAY);
		cv::GaussianBlur(srcImg, blurimg, cv::Size(5, 5), 1.0, 1.0, BORDER_CONSTANT);
		cv::threshold(blurimg, thrimg, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
		oReadWriteImageOperation.WriteImage(thrimg, imagePath);
	}
};