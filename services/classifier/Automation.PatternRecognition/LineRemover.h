/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
#pragma once
#include "includefiles.h"

class LineRemover
{
private:
	int vSize = 51;
	int hSize = 5;
public:
	PIX* RemoveAllLines(PIX* imageV1)
	{
		PIX* imageV2 = pixCloseGray(imageV1, L_HORIZ, vSize);

		PIX* imageV3 = pixErodeGray(imageV2, L_VERT, hSize);

		pixInvert(imageV3, imageV3);

		PIX* imageH2 = pixCloseGray(imageV1, vSize, L_HORIZ);

		PIX* imageH3 = pixErodeGray(imageH2, hSize, L_VERT);

		pixInvert(imageH3, imageH3);

		PIX*imageAllLines = pixOr(NULL, imageV3, imageH3);
		PIX* imageAllLineRemoved = pixAddGray(NULL, imageV1, imageAllLines);

		pixDestroy(&imageV1);
		pixDestroy(&imageV2);
		pixDestroy(&imageV3);
		pixDestroy(&imageH2);
		pixDestroy(&imageH3);
		pixDestroy(&imageAllLines);

		return imageAllLineRemoved;
	}
	PIX* RemoveVerticalLines(PIX* imageV1)
	{
		PIX* imageV2 = pixCloseGray(imageV1, L_HORIZ, vSize);

		PIX* imageV3 = pixErodeGray(imageV2, L_VERT, hSize);

		pixInvert(imageV3, imageV3);
		PIX* imageV4 = pixAddGray(NULL, imageV1, imageV3);

		pixDestroy(&imageV1);
		pixDestroy(&imageV2);
		pixDestroy(&imageV3);

		return imageV4;
	}

	PIX* RemoveHorizontalLines(PIX* imageH1)
	{
		PIX* imageH2 = pixCloseGray(imageH1, vSize, L_HORIZ);

		PIX* imageH3 = pixErodeGray(imageH2, hSize, L_VERT);

		pixInvert(imageH3, imageH3);
		PIX* imageH4 = pixAddGray(NULL, imageH1, imageH3);

		pixDestroy(&imageH1);
		pixDestroy(&imageH2);
		pixDestroy(&imageH3);

		return imageH4;
	}
};