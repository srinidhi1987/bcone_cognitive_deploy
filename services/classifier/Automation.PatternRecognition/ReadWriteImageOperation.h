/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

#pragma once

#include "includefiles.h"

enum ReturnValue
{
	Failure,
	Success
};

class ReadWriteImageOperation
{
private:
	string GetDirName(string strFilePath)
	{
		size_t found;
		found = strFilePath.find_last_of("/\\");
		return strFilePath.substr(0, found);
	}
	string GetFileExtension(string strFilePath)
	{
		size_t found;
		found = strFilePath.find_last_of(".");
		return strFilePath.substr(found + 1, strFilePath.length() - 1);
	}
	bool ValidateFilePath(string strFilePath)
	{
		return ReturnValue::Success;
		string strdir = GetDirName(strFilePath);
		DWORD ftyp = GetFileAttributesA(strdir.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return ReturnValue::Failure;  //something is wrong with your path!

		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return ReturnValue::Success;   // this is a directory!

		return ReturnValue::Success;    // this is not a directory!
	}
	bool IsFileExist(string strFilePath)
	{
		DWORD ftyp = GetFileAttributesA(strFilePath.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES || ftyp == INVALID_FILE_SIZE)
			return ReturnValue::Failure;

		return ReturnValue::Success;
	}
	bool IsFileSupported(string strFilePath)
	{
		string strFileExtension = GetFileExtension(strFilePath);
		std::transform(strFileExtension.begin(), strFileExtension.end(), strFileExtension.begin(), ::tolower);
		//	strFileExtension = (strFileExtension.c_str);
		if (strFileExtension == "png" || strFileExtension == "tiff" ||
			strFileExtension == "tif" || strFileExtension == "jpeg" || strFileExtension == "jpg" ||
			strFileExtension == "bmp" ||
			strFileExtension == "PNG" || strFileExtension == "TIFF" ||
			strFileExtension == "TIF" || strFileExtension == "JPEG" || strFileExtension == "JPG" ||
			strFileExtension == "BMP")
			return ReturnValue::Success;

		return ReturnValue::Failure;
	}
public:
	cv::Mat ReadImage(char* filePath)
	{
		cv::Mat mImage;
		if (IsFileSupported(filePath) == ReturnValue::Success)
			mImage = cv::imread(filePath);

		return mImage;
	}
	bool WriteImage(Mat image, char* filePath)
	{
		//if (ValidateFilePath(filePath) == ReturnValue::Success && image.data)
		{
			imwrite(filePath, image);
			return ReturnValue::Success;
		}
		return ReturnValue::Failure;
	}
};