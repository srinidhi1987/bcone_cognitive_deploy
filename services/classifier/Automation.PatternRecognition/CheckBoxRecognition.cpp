/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
#pragma once
#include "CheckBoxRecognition.h"
#include "MatchingTechniques.h"
#include "ImageOperations.h"

CheckBoxRecognition::CheckBoxRecognition()
{
	eReturnValue = NotFound;
}
CheckBoxRecognition::~CheckBoxRecognition()
{
}
CheckBoxStatus CheckBoxRecognition::GetCheckBoxStatus(char* imagePath, int X, int Y, int Width, int Height)
{
	ReadWriteImageOperation oReadWriteImageOperation;
	Mat image = oReadWriteImageOperation.ReadImage(imagePath);

	if (image.cols == 0 || image.rows == 0 || Width > image.cols || Height > image.rows || X < 0 || Y < 0)
	{
		return eReturnValue;
	}

	ImageOperations oImageOperation;
	Mat targetImage = oImageOperation.CropSmallImageFromBigImage(image, X, Y, Width, Height);

	RotatedRect rectImage = oImageOperation.FindRectangle(targetImage);

	if (rectImage.size.height > rectImage.size.width*1.5 || rectImage.size.width > rectImage.size.height*1.5)
		return eReturnValue;

	if ((abs((int)rectImage.angle) % 90 == 0 && rectImage.size.area() > 0 && (rectImage.size.height*rectImage.size.width) > ((targetImage.rows*targetImage.cols)*.4)))
	{
		targetImage = oImageOperation.GetCroppedImageOfRectangle(targetImage, rectImage);
		if (!targetImage.data)
			return eReturnValue;

		MatchingTechniques oMatchingTechniques;
		//targetImage= mImageOperations.ImageKernel(targetImage);

		bool className = oMatchingTechniques.InsideRectImagePixelBasedApproach(targetImage);

		if (className == 1)
		{
			eReturnValue = Checked;
		}
		else
		{
			eReturnValue = UnChecked;
		}
	}
	return eReturnValue;
}