/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

#pragma once

#ifdef Automation.PatternRecognition_EXPORTS
#define PatternRecognition_API __declspec(dllexport)
#else
#define PatternRecognition_API __declspec(dllimport)
#endif

extern "C"
{
	PatternRecognition_API int MatchAndRecognizeCheckBox(char* ImagePath, int x, int y, int width, int height);
	PatternRecognition_API bool ResizeImage(char* imagePath, int ResizeRatio, int interpolationMethod);
	PatternRecognition_API void RemoveLines(char* filePath, char* onlyLineImage);
	PatternRecognition_API void RemoveLinesByFillHoles(char * imagePath, char* onlyLineImage);
	PatternRecognition_API void ApplyPreprocessingForRotation(const char *cSrcImage);
	PatternRecognition_API int PredictSpeckleNoiseInDocument(char* imagePath);
	PatternRecognition_API void RemoveSpeckleNoise(char *cSrcImage);
}