#pragma once

#include "includefiles.h"

using namespace std;
using namespace cv;
class MatchingTechniques
{
private:
	Mat element1 = getStructuringElement(cv::MORPH_RECT, Size(2 * 1 + 1, 2 * 1 + 1), Point(0, 0));
public:
	bool CroppedLineBasedMatching(Mat targetImage)
	{
		cv::erode(targetImage, targetImage, element1);
		//	Mat targetImage=MakeBorderOutsideImage(targetImage1);

		Rect rect(targetImage.cols / 2, 0, 1, targetImage.rows);

		Mat croppedRef(targetImage, rect);

		cv::Mat cropped;
		croppedRef.copyTo(cropped);

		threshold(cropped, cropped, 60, 255, CV_THRESH_BINARY);

		int rept = 0;

		int count = 1;
		for (int i = 0; i < cropped.rows; i++)
		{
			int val = cropped.at<uchar>(i, 0);
			if (val == 0)
			{
				count++;
			}
			else
			{
				if (count > 2)
				{
					rept++;
				}
				count = 1;
			}
		}

		if (rept > 2)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	bool InsideRectImagePixelBasedApproach(Mat targetImage)
	{
		resize(targetImage, targetImage, Size(64, 64));

		Rect rect(targetImage.cols / 4, targetImage.rows / 4, targetImage.cols / 2, targetImage.rows / 2);

		Mat croppedRef(targetImage, rect);

		cv::Mat cropped;
		croppedRef.copyTo(cropped);
		/*
				imshow("original", targetImage);
				imshow("cropped", cropped);
				waitKey(0);*/
		threshold(cropped, cropped, 60, 255, CV_THRESH_BINARY);

		int count = 0;
		for (int i = 0; i < cropped.rows; i++)
		{
			for (int j = 0; j < cropped.cols; j++)
			{
				int val = cropped.at<uchar>(i, j);
				if (val == 0)
				{
					count++;
				}
			}
		}

		if (count > (cropped.rows*cropped.cols) / 15)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
};