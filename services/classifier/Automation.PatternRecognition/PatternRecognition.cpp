/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

#pragma once
#include "CheckBoxRecognition.h"
#include "PatternRecognition.h"
#include "ImageOperations.h"
#include <fstream>

void ApplyPreprocessingForRotation(const char *cSrcImage)
{
	ImageOperations *mImageOperations = new ImageOperations();
	mImageOperations->ApplySmoothingNSharpeningOnImage((char *)cSrcImage);
}

void RemoveSpeckleNoise(char *cSrcImage)
{
	cv::Mat orgImg, srcImg, blurimg, thrBlurImg, thrImgOrig;
	srcImg = cv::imread(cSrcImage);
	orgImg = srcImg.clone();
	if (srcImg.channels() >= 3)
		cv::cvtColor(srcImg, srcImg, cv::COLOR_BGR2GRAY);
	cv::GaussianBlur(srcImg, blurimg, cv::Size(3, 3), 0.0, 0.0, BORDER_CONSTANT);

	if (orgImg.channels() >= 3)
		cvtColor(orgImg, orgImg, cv::COLOR_BGR2GRAY);

	cv::Mat finalImage;
	if (blurimg.cols > 0 && orgImg.cols > 0)
	{
		cv::threshold(blurimg, thrBlurImg, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
		cv::threshold(orgImg, thrImgOrig, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
		add(thrBlurImg, orgImg, finalImage);
	}
	if (finalImage.data)
	{
		cv::imwrite(cSrcImage, finalImage);
	}
}

void RemoveLinesByFillHoles(char * imagePath, char* onlyLineImage)
{
	ImageOperations *mImageOperations = new ImageOperations();
	mImageOperations->RemoveVerticalAndHorizontalLinesByFillHoles(imagePath, onlyLineImage);

	delete mImageOperations;
}
void RemoveLines(char * imagePath, char* onlyLineImage)
{
	ImageOperations *mImageOperations = new ImageOperations();
	mImageOperations->RemoveVerticalAndHorizontalLines(imagePath, onlyLineImage);
	delete mImageOperations;
}

// --------- CheckBox Recognition --------- //
int MatchAndRecognizeCheckBox(char* ImagePath, int x, int y, int width, int height)
{
	int returnVal = -1;
	CheckBoxRecognition *mCheckBoxRecognition = new CheckBoxRecognition();
	returnVal = mCheckBoxRecognition->GetCheckBoxStatus(ImagePath, x, y, width, height);
	delete mCheckBoxRecognition;
	return returnVal;
}

bool ResizeImage(char* imagePath, int ResizeRatio = 1, int InterpolationMethod = 1)
{
	bool returnVal = false;
	ImageOperations *mImageOperations = new ImageOperations();
	returnVal = mImageOperations->ResizeImage(imagePath, ResizeRatio, InterpolationMethod);
	delete mImageOperations;
	return returnVal;
}

double Hypothesis(double *theta, int whiteCount, int blackCount)
{
	double theta0 = theta[0];
	double feature1 = 0.0;
	feature1 = double(whiteCount);
	double feature2 = 0.0;
	feature2 = double(blackCount);
	double x1theta1 = theta[1] * feature1;
	double x2theta2 = theta[2] * feature2;
	//calculate hypothesis
	double value = theta0 + x1theta1 + x2theta2;
	//return (theta[0] * input[0] + theta[1] * input[1] + theta[2] * input[2]);
	return value;
}

double Sigmoid(double value)
{
	double expValue = exp(-value);
	double logisticFunctionValue = (double)1 / ((double)1 + expValue);
	return logisticFunctionValue;
	//return ((double)1 / ((double)1 + exp(-value)));
}

int PredictSpeckleNoiseInDocument(char* imagePath)
{
	double theta[] = { 4.72641588, -2.30554079e-03, -2.14077178e-03 };
	ofstream myfile;
	Mat img = imread(imagePath);
	if (img.channels() >= 3)
		cvtColor(img, img, CV_BGR2GRAY);
	cv::threshold(img, img, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	int whiteCount = 0;
	int blackCount = 0;
	for (int j = 2; j < img.rows - 2; j++)
	{
		for (int i = 2; i < img.cols - 2; i++)
		{
			int count = 0;
			if (img.at<uchar>(j - 1, i) == 0)
				count++;
			if (img.at<uchar>(j + 1, i) == 0)
				count++;
			if (img.at<uchar>(j, i + 1) == 0)
				count++;
			if (img.at<uchar>(j, i - 1) == 0)
				count++;

			if ((img.at<uchar>(j, i) == 255 && count == 4))
			{
				whiteCount++;
			}
			else if ((count == 0 && img.at<uchar>(j, i) == 0))
			{
				blackCount++;
			}
		}
	}

	double value = Hypothesis(theta, whiteCount, blackCount);

	double logisticFunctionValue = Sigmoid(value);

	int isNoise = 2;
	if (logisticFunctionValue >= 0.50)
	{
		isNoise = 0;
	}
	else
	{
		isNoise = 1;
	}
	return isNoise;
}