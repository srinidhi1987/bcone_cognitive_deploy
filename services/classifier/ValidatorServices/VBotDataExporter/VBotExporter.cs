﻿///**
// * Copyright (c) 2016 Automation Anywhere.
// * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
// * All rights reserved.
// *
// * This software is the confidential and proprietary information of
// * Automation Anywhere.("Confidential Information").  You shall not
// * disclose such Confidential Information and shall use it only in
// * accordance with the terms of the license agreement you entered into
// * with Automation Anywhere.
// */

//using Automation.VisionBotEngine;

//namespace Automation.CognitiveData.VBotDataExporter
//{
//    using System.Collections.Generic;
//    using System.Data;
//    using System.Linq;

//    using Automation.CognitiveData.Converters;
//    using Automation.VisionBotEngine.Model;
//    using System;

//    using Automation.VisionBotEngine.Validation;

//    internal class VBotExporter : IVBotExporter
//    {
//        public VBotExporter()
//            : this(new ExportableVisionBotDataProvider(), new ExportableDataTableProvider())
//        {
//        }

//        public VBotExporter(
//            IExportableVisionBotDataProvider exportableDataProvider,
//            IExportableDataTableProvider exportableDataTableProvider)
//            : this(new ExportableVisionBotDataTableProvider(exportableDataProvider, exportableDataTableProvider))
//        {
//        }

//        public VBotExporter(IExportableVisionBotDataTableProvider exporter)
//        {
//            this.Exporter = exporter;
//        }

//        private IExportableVisionBotDataTableProvider Exporter;

//        public void ExportToCsv(DataTable dataTableToExport, CSVExportSettings settings)
//        {
//            VBotLogger.Trace(() => "[ExportToCsv] Start ....");
//            IDataTableSerializer dataTableSerializer = new CsvDataTableSerializer(
//                settings.CSVExportFilePath,
//                settings.Delimiter,
//                new CsvHelperReaderWriter(settings.CSVExportFilePath, settings.Delimiter));

//            dataTableSerializer.Serialize(dataTableToExport);

//            VBotLogger.Trace(() => "[ExportToCsv] End ....");
//        }

//        public DataTable ExportToDataTable(VBotDocument vbotDocument, IDictionary<string, string> postData)
//        {
//            VBotLogger.Trace(() => "[ExportToDataTable] Start ....");

//            VBotLogger.Trace(() => string.Format("[ExportToDataTable] Object created ...."));

//            return this.Exporter.GetExportableVisionBotDataTable(vbotDocument.Data, postData);
//        }
//    }

//    internal class VBotErrorExporter : VBotExporter
//    {
//        public VBotErrorExporter()
//            : base(new ExportableVisionBotErrorDataProvider(), new ExportableDataTableProvider())
//        {
//        }
//    }

//    internal class VBotCombinedErrorAndDataExporter : VBotExporter
//    {
//        public VBotCombinedErrorAndDataExporter()
//            : base(new ExportableVisionBotCombinedDataProvider(), new ExportableDataTableProvider())
//        {
//        }
//    }

//    //public enum FileWriteType
//    //{
//    //    Append,
//    //    Overwrite
//    //}

//    //public class CSVExportSettings
//    //{
//    //    private const string DefaultDelimiter = ",";

//    //    public CSVExportSettings(string filePath)
//    //    {
//    //        this.FilePath = filePath;
//    //        this.Delimiter = DefaultDelimiter;
//    //    }

//    //    public string FilePath { get; private set; }

//    //    public string Delimiter { get; set; }

//    //    public FileWriteType WriteType { get; set; }
//    //}

//    //public interface ICSVExporter
//    //{
//    //    void ExportToCSV(DataTable data, CSVExportSettings exportSettings);
//    //}

//    //public class CSVExporter : ICSVExporter
//    //{
//    //    public void ExportToCSV(DataTable data, CSVExportSettings exportSettings)
//    //    {
//    //        IDataTableSerializer dataTableSerializer = new CsvDataTableSerializer(
//    //            exportSettings.FilePath,
//    //            exportSettings.Delimiter,
//    //            new CsvHelperReaderWriter(exportSettings.FilePath, exportSettings.Delimiter));

//    //        dataTableSerializer.Serialize(data);
//    //    }
//    //}

//    internal class ExportableField
//    {
//        public string Title { get; private set; }

//        public string Value { get; private set; }

//        public ExportableField(string title, string value)
//        {
//            this.Title = title;
//            this.Value = value;
//        }
//    }

//    internal class ExportableRecord
//    {
//        public IList<ExportableField> Fields { get; private set; }

//        public ExportableRecord(IList<ExportableField> record)
//        {
//            this.Fields = record;
//        }
//    }

//    internal class ExportableTable
//    {
//        public IList<string> Headers { get; private set; }

//        public IList<ExportableRecord> Records { get; private set; }

//        public ExportableTable(IList<ExportableRecord> records, IList<string> headers)
//        {
//            this.Records = records;
//            this.Headers = headers;
//        }
//    }

//    internal class ExportableVisionBotData
//    {
//        public ExportableRecord NormalFields { get; private set; }

//        public IList<ExportableTable> Tables { get; private set; }

//        public ExportableVisionBotData(ExportableRecord normalFields, IList<ExportableTable> tables)
//        {
//            this.NormalFields = normalFields;
//            this.Tables = tables;
//        }
//    }

//    interface IExportableVisionBotDataProvider
//    {
//        ExportableVisionBotData GetExportableVisionBotData(VBotData processedData);
//    }

//    interface IExportableDataTableProvider
//    {
//        DataTable GetExportableDataTable(ExportableVisionBotData exportableVisionBotData, IDictionary<string, string> postExportableRecord);
//    }

//    interface IExportableVisionBotDataTableProvider
//    {
//        DataTable GetExportableVisionBotDataTable(VBotData processedData, IDictionary<string, string> postData);
//    }

//    internal class ExportableVisionBotDataTableProvider : IExportableVisionBotDataTableProvider
//    {
//        public ExportableVisionBotDataTableProvider(
//            IExportableVisionBotDataProvider exportableVisionBotDataProvider,
//            IExportableDataTableProvider exportableDataTableProvider)
//        {
//            this.ExportableVisionBotDataProvider = exportableVisionBotDataProvider;
//            this.ExportableDataTableProvider = exportableDataTableProvider;
//        }

//        private IExportableDataTableProvider ExportableDataTableProvider { get; set; }

//        private IExportableVisionBotDataProvider ExportableVisionBotDataProvider { get; set; }

//        public DataTable GetExportableVisionBotDataTable(VBotData processedData, IDictionary<string, string> postData)
//        {
//            VBotLogger.Trace(() => "[GetExportableVisionBotDataTable]  Export to visionbot DataTable");
//            var exportableVisionBotData = this.ExportableVisionBotDataProvider.GetExportableVisionBotData(processedData);
//            return this.ExportableDataTableProvider.GetExportableDataTable(exportableVisionBotData, postData);
//        }
//    }

//    internal class ExportableVisionBotDataProvider : IExportableVisionBotDataProvider
//    {
//        public ExportableVisionBotData GetExportableVisionBotData(VBotData processedData)
//        {
//            return Convert(processedData);
//        }

//        #region Convert

//        protected virtual ExportableVisionBotData Convert(VBotData data)
//        {
//            VBotLogger.Trace(() => "[ExportableVisionBotData]  Convert data");
//            return new ExportableVisionBotData(
//                Convert(data.FieldDataRecord),
//                data.TableDataRecord.Select(Convert).ToList());
//        }

//        protected virtual ExportableTable Convert(TableValue data)
//        {
//            return new ExportableTable(
//                data.Rows.Select(Convert).ToList(),
//                data.Headers.Select(x => x.Name).ToList());
//        }

//        protected virtual ExportableRecord Convert(DataRecord data)
//        {
//            return new ExportableRecord(data.Fields.Select(Convert).ToList());
//        }

//        protected virtual ExportableField Convert(FieldData data)
//        {
//            return new ExportableField(
//                data.Field.Name,
//                data.ValidationIssue.GetIssueType() != ValidationIssueCodeTypes.Error
//                    ? data.Value.Field.Text
//                    : string.Empty);
//        }

//        #endregion
//    }

//    internal class ExportableVisionBotErrorDataProvider : ExportableVisionBotDataProvider
//    {
//        protected override ExportableField Convert(FieldData data)
//        {
//            return new ExportableField(
//                data.Field.Name,
//                data.ValidationIssue.GetIssueType() != ValidationIssueCodeTypes.Error
//                    ? string.Empty
//                    : string.Format("<{1}> {0}", data.Value.Field.Text, data.ValidationIssue.IssueCode));
//        }
//    }

//    internal class ExportableVisionBotCombinedDataProvider : ExportableVisionBotDataProvider
//    {
//        protected override ExportableField Convert(FieldData data)
//        {
//            return new ExportableField(
//                data.Field.Name,
//                data.ValidationIssue.GetIssueType() != ValidationIssueCodeTypes.Error
//                     ? data.Value.Field.Text
//                    : string.Format("<{1}> {0}", data.Value.Field.Text, data.ValidationIssue.IssueCode));
//        }
//    }

//    internal class ExportableDataTableProvider : IExportableDataTableProvider
//    {
//        public DataTable GetExportableDataTable(ExportableVisionBotData exportableVisionBotData, IDictionary<string, string> postData)
//        {
//            VBotLogger.Trace(() => "[GetExportableDataTable]   Start creating DataTable..");
//            var result = new DataTable();

//            var postDataRecord = postData.GetExportableRecord();

//            CreateDataTableColumns(result, exportableVisionBotData, postDataRecord);
//            VBotLogger.Trace(() => "[GetExportableDataTable]   Start Fill Data..");

//            FillData(result, exportableVisionBotData, postDataRecord);
//            VBotLogger.Trace(() => "[GetExportableDataTable]   Done Fill Data..");

//            return result;
//        }

//        #region CreateDataTableColumns

//        private static void CreateDataTableColumns(DataTable dataTable, ExportableVisionBotData data, ExportableRecord postExportableRecord)
//        {
//            CreateDataTableColumns(dataTable, data.NormalFields);

//            foreach (var exportableTable in data.Tables)
//            {
//                CreateDataTableColumns(dataTable, exportableTable);
//            }

//            CreateDataTableColumns(dataTable, postExportableRecord);
//        }

//        private static void CreateDataTableColumns(DataTable dataTable, ExportableTable table)
//        {
//            if (table == null)
//            {
//                return;
//            }

//            foreach (var header in table.Headers)
//            {
//                dataTable.Columns.Add(header);
//            }
//        }

//        private static void CreateDataTableColumns(DataTable dataTable, ExportableRecord record)
//        {
//            if (record == null)
//            {
//                return;
//            }

//            foreach (var field in record.Fields)
//            {
//                string colName = field.Title;

//                //TODO : [Prakash] This is a temporary fix and needs to be corrected.
//                if (dataTable.Columns.Contains(colName))
//                {
//                    VBotLogger.Trace(() => string.Format("[CreateDataTableColumns]   Duplicate column found. Column '{0}' aleready exists...", colName));
//                    colName = field.Title + Guid.NewGuid().ToString();
//                }

//                dataTable.Columns.Add(colName);
//            }
//        }

//        #endregion

//        #region FillData

//        private static void FillData(DataTable dataTable, ExportableVisionBotData data, ExportableRecord postExportableRecord)
//        {
//            #region Preparation

//            var preExportableRecords = new List<ExportableRecord>();

//            var postExportableRecords = new List<ExportableRecord>();
//            if (data.Tables != null && data.Tables.Count > 0)
//            {
//                postExportableRecords.AddRange(data.Tables.Select(t => GenerateBlankRecord(t.Headers)));
//            }
//            postExportableRecords.Add(postExportableRecord);

//            #endregion

//            /* Only single row in cases: 
//             * 1. no tables
//             * 2. no records in any of the tables
//             */
//            if (data.Tables == null ||
//                data.Tables.Count <= 0 ||
//                !data.Tables.Any(table => table.Records.Count > 0))
//            {
//                FillData(dataTable, Combine(preExportableRecords, data.NormalFields, postExportableRecords).ToArray());
//                return;
//            }

//            preExportableRecords.Add(data.NormalFields);

//            foreach (var exportableTable in data.Tables)
//            {
//                var currentTableBlankRecord = postExportableRecords[0];
//                postExportableRecords.Remove(currentTableBlankRecord);

//                foreach (var exportableRecord in exportableTable.Records)
//                {
//                    FillData(dataTable, Combine(preExportableRecords, exportableRecord, postExportableRecords).ToArray());
//                }

//                preExportableRecords.Add(currentTableBlankRecord);
//            }
//        }

//        private static IEnumerable<ExportableRecord> Combine(
//            IList<ExportableRecord> preExportableRecords,
//            ExportableRecord currentExportableRecord,
//            IList<ExportableRecord> postExportableRecords)
//        {
//            var result = new List<ExportableRecord>();
//            if (preExportableRecords != null && preExportableRecords.Any())
//            {
//                result.AddRange(preExportableRecords);
//            }
//            if (currentExportableRecord != null)
//            {
//                result.Add(currentExportableRecord);
//            }
//            if (postExportableRecords != null && postExportableRecords.Any())
//            {
//                result.AddRange(postExportableRecords);
//            }
//            return result;
//        }

//        private static void FillData(DataTable dataTable, params ExportableRecord[] exportableRecords)
//        {
//            var dataRow = dataTable.NewRow();
//            FillData(dataRow, exportableRecords);
//            dataTable.Rows.Add(dataRow);
//        }

//        private static void FillData(DataRow dataRow, params ExportableRecord[] exportableRecords)
//        {
//            var columnIndex = 0;
//            var totalColumns = dataRow.Table.Columns.Count;

//            foreach (var record in exportableRecords)
//            {
//                if (record == null)
//                {
//                    continue;
//                }

//                foreach (var exportableField in record.Fields)
//                {
//                    if (columnIndex >= totalColumns)
//                    {
//                        return;
//                    }

//                    dataRow[columnIndex++] = exportableField.Value;
//                }
//            }

//            while (columnIndex < totalColumns)
//            {
//                dataRow[columnIndex++] = string.Empty;
//            }
//        }

//        private static ExportableRecord GenerateBlankRecord(IList<string> headers)
//        {
//            return new ExportableRecord(headers
//                      .Select(header => new ExportableField(header, string.Empty))
//                      .ToList());
//        }

//        #endregion
//    }

//    internal static class DictionaryHelper
//    {
//        public static ExportableRecord GetExportableRecord(this IDictionary<string, string> dictionaryData)
//        {
//            var result = new List<ExportableField>();

//            if (dictionaryData != null)
//            {
//                result.AddRange(dictionaryData.Select(keyValuePair => new ExportableField(keyValuePair.Key, keyValuePair.Value)));
//            }

//            return new ExportableRecord(result);
//        }
//    }
//}
