﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Generic
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;

    public class FileHelper : IFileHelper
    {
        [ExcludeFromCodeCoverage]
        public bool DoesExists(string filePath)
        {
            return File.Exists(filePath);
        }

        [ExcludeFromCodeCoverage]
        public string GetContents(string filePath)
        {
            return File.ReadAllText(filePath);
        }

        //[ExcludeFromCodeCoverage]
        //public string[] GetAllLines(string filePath)
        //{
        //    return File.ReadAllLines(filePath);
        //}

        [ExcludeFromCodeCoverage]
        public void Delete(string filePath)
        {
            File.Delete(filePath);
        }

        [ExcludeFromCodeCoverage]
        public void Move(string sourceFilePath, string destinationFilePath)
        {
            File.Move(sourceFilePath, destinationFilePath);
        }

        //[ExcludeFromCodeCoverage]
        //public void WriteAllText(string filePath, string contents)
        //{
        //    File.WriteAllText(filePath, contents);
        //}
    }
}
