﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Generic.Compression
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Packaging;
    using System.Runtime.Serialization;
    using System.Security.Permissions;
    using System.Text.RegularExpressions;

    public class ResourceData
    {
        public string UniqueId
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }

        public string FilePath
        {
            get;
            set;
        }

        public Stream Content
        {
            get;
            set;
        }
    }

    public interface IResourcePackage
    {
        void Create(string packageName);

        void CreateNew(string packageName);

        void Open(string packageName);

        void Close();

        ResourceData GetResource(string uniqueId);

        IList<ResourceData> GetAllResources();

        void AddFile(string uniqueId, string filepath);

        void AddResource(ResourceData resourceData);

        void CreateFromFolder(string packageName, string folderPath, bool includSubFolder);

        void ExtractToFolder(string packageName, string destinationPath);

        void DeleteFile(string uniqueId);
    }

    public class ResourcePackage : IResourcePackage, IDisposable
    {
        private CompressionOption _compressionOption = CompressionOption.Maximum;
        private Package _package;

        public void Create(string packageName)
        {
            validatePackageName(packageName);
            validateDuplicatePackage(packageName);

            _package = Package.Open(packageName, FileMode.OpenOrCreate);
        }

        public void CreateNew(string packageName)
        {
            validatePackageName(packageName);
            _package = Package.Open(packageName, FileMode.Create);
        }

        public void Open(string packageName)
        {
            validatePackageName(packageName);
            if (!File.Exists(packageName))
            {
                throwException(new FileNotFoundException("PackageNotFound", packageName));
            }

            _package = Package.Open(packageName, FileMode.Open, FileAccess.ReadWrite);
        }

        public void Close()
        {
            if (_package != null)
            {
                _package.Close();
            }
        }

        public ResourceData GetResource(string uniqueId)
        {
            validatePackageInitialize();
            ResourceData resourcedata = new ResourceData();
            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueId, UriKind.Relative));

            if (_package.PartExists(uri))
            {
                PackagePart packagePart = _package.GetPart(uri);
                resourcedata.UniqueId = packagePart.Uri.ToString();
                resourcedata.ContentType = packagePart.ContentType;
                resourcedata.Content = packagePart.GetStream();
                return resourcedata;
            }

            return resourcedata;
        }

        public IList<ResourceData> GetAllResources()
        {
            IList<ResourceData> resulList = new List<ResourceData>();
            validatePackageInitialize();

            foreach (var packagePart in _package.GetParts())
            {
                ResourceData resourceData = new ResourceData();
                resourceData.UniqueId = packagePart.Uri.ToString();
                resourceData.ContentType = packagePart.ContentType;
                resourceData.Content = packagePart.GetStream();

                resulList.Add(resourceData);
            }

            return resulList;
        }

        public void AddFile(string uniqueid, string filepath)
        {
            validatePackageInitialize();
            validateuniqueid(uniqueid);

            if (!File.Exists(filepath))
            {
                throwException(new FileNotFoundException("FileNotFound", filepath));
            }

            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueid, UriKind.Relative));

            PackagePart packagePart = _package.CreatePart(uri, string.Empty, _compressionOption);

            if (packagePart != null)
            {
                using (FileStream fileStream = new FileStream(
                    filepath,
                    FileMode.Open,
                    FileAccess.Read))
                {
                    fileStream.CopyTo(packagePart.GetStream());
                }
            }
        }

        public void AddResource(ResourceData resourceData)
        {
            validateResourceData(resourceData);
            validatePackageInitialize();

            Uri uri = PackUriHelper.CreatePartUri(new Uri(resourceData.UniqueId, UriKind.Relative));

            PackagePart packagePart = _package.CreatePart(uri, resourceData.ContentType, _compressionOption);

            if (packagePart != null)
            {
                resourceData.Content.CopyTo(packagePart.GetStream());
            }
        }

        public void CreateFromFolder(string packageName, string folderPath, bool includSubFolder)
        {
            if (!Directory.Exists(folderPath))
            {
                throwException(new DirectoryNotFoundException("DirectoryNotFound"));
            }

            if (File.Exists(packageName))
            {
                throw new PackageAlreadyExistsException(packageName);
            }

            Create(packageName);

            compressDirectory(folderPath, folderPath, includSubFolder);
        }

        public void ExtractToFolder(string packageName, string destinationPath)
        {
            Open(packageName);

            foreach (var part in _package.GetParts())
            {
                var partUri = part.Uri.OriginalString.Replace('/', Path.DirectorySeparatorChar);
                partUri = Decode(partUri);
                if (partUri.StartsWith(Path.DirectorySeparatorChar.ToString()))
                {
                    partUri = partUri.TrimStart(Path.DirectorySeparatorChar);
                }

                var partFilePath = Path.Combine(destinationPath, partUri);
                var partDirectoryPath = Path.GetDirectoryName(partFilePath);

                if (!Directory.Exists(partDirectoryPath))
                {
                    Directory.CreateDirectory(partDirectoryPath);
                }

                using (FileStream fileStream = File.Create(partFilePath))
                {
                    using (var compressedStream = part.GetStream())
                    {
                        compressedStream.CopyTo(fileStream);
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_package != null)
                {
                    _package.Close();
                    _package = null;
                }
            }
        }

        public void DeleteFile(string uniqueId)
        {
            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueId, UriKind.Relative));
            if (!_package.PartExists(uri))
            {
                throw new Exception("Unable to find item");
            }
            _package.DeletePart(uri);
        }

        private void validateDuplicatePackage(string packageName)
        {
            if (File.Exists(packageName))
            {
                throw new Exception("PackageExist");
            }
        }

        private void validatePackageName(string packageName)
        {
            if (string.IsNullOrEmpty(packageName))
            {
                throwException(new ArgumentNullException("packageName", "ArgumentNullOrBlank"));
            }
        }

        private void validatePackageInitialize()
        {
            if (_package == null)
            {
                throwException(new Exception("PackageNotInitialize"));
            }
        }

        private void validateResourceData(ResourceData resourceData)
        {
            if (resourceData == null)
            {
                throwException(new ArgumentNullException("resourceData", "ArgumentNullOrBlank"));
            }
        }

        private void validateuniqueid(string uniqueid)
        {
            if (string.IsNullOrEmpty(uniqueid))
            {
                throwException(new ArgumentNullException("uniqueid", "ArgumentNullOrBlank"));
            }

            if (isDuplicateUniqueId(uniqueid))
            {
                throwException(new Exception("DuplicateUniqueID"));
            }
        }

        private bool isDuplicateUniqueId(string uniqueid)
        {
            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueid, UriKind.Relative));
            return _package.PartExists(uri);
        }

        private void compressDirectory(string rootDirectory, string currentDirectory, bool includeSubFolder)
        {
            string searchDirectory = currentDirectory == string.Empty ? rootDirectory : currentDirectory;

            compressFile(rootDirectory, searchDirectory);

            if (includeSubFolder)
            {
                foreach (string dir in Directory.GetDirectories(searchDirectory))
                {
                    compressDirectory(rootDirectory, dir, true);
                }
            }
        }

        private void compressFile(string rootDirectory, string parentDirectory)
        {
            var searchDirectory = new DirectoryInfo((parentDirectory == string.Empty) ? rootDirectory : parentDirectory);
            foreach (var fileName in searchDirectory.GetFiles())
            {
                var relativePathInCompressedFile = parentDirectory.Replace(rootDirectory, string.Empty);
                Uri partUriDocument = PackUriHelper.CreatePartUri(
                    new Uri(
                        Encode(relativePathInCompressedFile + "\\" + fileName.Name),
                        UriKind.Relative));

                PackagePart file = _package.CreatePart(partUriDocument, string.Empty, this._compressionOption);

                using (var fileStream = fileName.OpenRead())
                {
                    fileStream.CopyTo(file.GetStream());
                }
            }
        }

        private void throwException(Exception exception)
        {
            throw exception;
        }

        private static string Encode(string value)
        {
            return Regex.Replace(UrlUtilities.Encode(value), "%5c", "\\");
        }

        private static string Decode(string value)
        {
            return UrlUtilities.Decode(value);
        }
    }

    [Serializable]
    public class PackageAlreadyExistsException : Exception
    {
        public string PackagePath { get; private set; }

        public PackageAlreadyExistsException(string packagePath)
        {
            this.PackagePath = packagePath;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}