﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Generic
{
    using System;
    using System.IO;
    using System.Xml.Serialization;

    public class XmlDataPersister<T> : IDataPersister<T> where T : class
    {
        public string XmlFilePath { get; private set; }

        public XmlDataPersister(string xmlFilePath)
        {
            XmlFilePath = xmlFilePath;
        }

        public bool Store(T dataToStore)
        {
            try
            {
                using (TextWriter writer = new StreamWriter(XmlFilePath))
                {
                    (new XmlSerializer(typeof(T))).Serialize(writer, dataToStore);
                }
                return true;
            }
            catch (Exception ex)
            {
                //Log.Write(Log.Modules.Player, Log.LogTypes.FATAL, GetLogFunctionName("Store"), string.Empty, ex);
            }
            return false;
        }

        public T Retrieve()
        {
            if (!File.Exists(XmlFilePath))
                return null;

            try
            {
                using (TextReader reader = new StreamReader(XmlFilePath))
                {
                    return (T)(new XmlSerializer(typeof(T))).Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                //Log.Write(Log.Modules.Player, Log.LogTypes.FATAL, GetLogFunctionName("Retrieve"), string.Empty, ex);
            }

            return null;
        }

        private static string GetLogFunctionName(string functionName)
        {
            return "XmlPersister::" + typeof(T) + "::" + functionName;
        }
    }
}