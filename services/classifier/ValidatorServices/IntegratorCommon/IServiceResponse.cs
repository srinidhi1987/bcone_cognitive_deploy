﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.ValidatorLite.IntegratorCommon.Model
{
    public class IServiceResponse<T>
    {
        public IServiceResponse() { }

        public int ErrorCode { get; set; }
        public T Model { get; set; }
        public IServiceRequest<T> Request { get; set; }
        public int ResponseCode { get; set; }
    }
    public class QSettingUpdateResponse : IServiceResponse<IEnumerable<Model.QSetting>>
    { }
}
