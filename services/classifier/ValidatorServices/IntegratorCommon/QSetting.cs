﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.ValidatorLite.IntegratorCommon.Model
{
    using System.Collections.Generic;
    using System.IO;

    public enum QSettingsState
    {
        Init = 0,
        Found = 1,
        Count = 2,
        ReadPermissionCheck = 3,
        WritePermissionCheck = 4,
        DeletePermissionCheck = 5,
        NetworkCheck = 6,
        Complete = 7
    }
    public enum QSettingsFileSystemEventType
    {
        Default = 0,
        Created = 1,
        Renamed = 2,
        Deleted = 3
    }
    public enum QSettingOperationType
    {
        Init = 0,
        Add = 1,
        Delete = 2,
        Refresh = 3,
        Open = 4
    }

    public class QSettingList
    {
        public IList<QSetting> qSettings { get; set; }
    }

    public class QSetting
    {
        public QPath QueuePath { get; set; }
        public QPermission QueuePermission { get; set; }
        public QStats QueueStats { get; set; }
        public int State { get; set; }
        public int OperationType { get; set; }
        public string StateDescription { get; set; }
        public QSettingsFileSystemEventType QSettingsFileSystemEventType { get; set; }
        public void CopyFrom(QSetting qSetting)
        {
            QueuePath = qSetting.QueuePath;
            QueuePermission = qSetting.QueuePermission;
            QueueStats = qSetting.QueueStats;
            State = qSetting.State;
            OperationType = qSetting.OperationType;
            StateDescription = qSetting.StateDescription;
            QSettingsFileSystemEventType = qSetting.QSettingsFileSystemEventType;
        }
    }

    public class QPermission
    {
        public bool CanReadQueue { get; set; }
        public bool CanWriteQueue { get; set; }
        public bool CanDeleteQueue { get; set; }
        public bool CanConnectQueue { get; set; }
    }

    public class QStats
    {
        public int? TotalNoOfDocumentsInQ { get; set; }
        public int NoOfDocumentsInProgress { get; set; }
        public int NoOfDocumentsCompleted { get; set; }
        public string BaudRate { get; set; }
        public long LastUpdatedUtcDateTimeAsTicks { get; set; }
    }

    public class QPath
    {
        public string ServerPath { get; set; }
        public string ServerScheme { get; set; }
        public string LocalMachinePath { get; set; }
        public string TaskName { get; set; }

        private string __queueName;

        public string QueueName
        {
            get
            {
                if (string.IsNullOrEmpty(__queueName) &&
                    !string.IsNullOrEmpty(this.ServerPath))
                {
                    __queueName = GetFolderName(this.ServerPath);
                }
                return __queueName;
            }
            set
            {
                __queueName = value;
            }
        }

        private static string GetFolderName(string path)
        {
            var result = Path.GetFileName(path);
            if (result.Equals("failq", System.StringComparison.InvariantCultureIgnoreCase))
                result = Path.GetFileName(Path.GetDirectoryName(path));
            return result;
        }

        public string DisplayPath
        {
            get { return Path.GetDirectoryName(ServerPath); }
        }
    }
}
