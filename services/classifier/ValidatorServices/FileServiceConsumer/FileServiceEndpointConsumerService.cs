﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */


namespace ValidatorServices.FileServiceConsumer
{
    using Automation.Services.Client;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System.Collections.Generic;
    using ValidatorServices.Validator.Model;
    class FileServiceEndpointConsumerService : IFileServiceEndPointConsumer
    {
        private HttpServiceAdapter _httpServiceAdapter = null;
        private static string baseUri = (new ServiceLocationProvider()).GetFileServiceConfiguration().ServiceUri;
        public FileServiceEndpointConsumerService(QSetting qsetting)
        {
            _httpServiceAdapter = new HttpServiceAdapter(baseUri, qsetting.UserName, qsetting.UserToken);
        }
        public List<FileBlobsEntity> getFileBlob(EndpointConsumerParameter parameter)
        {
            string endPoint = "/organizations/"+parameter.OrganizationId +"/projects/" + parameter.ProjectId+ "/fileblob/" + parameter.FileId;
            var response = _httpServiceAdapter.Get<List<FileBlobsEntity>>(endPoint);

            return response;
        }
    }
}
