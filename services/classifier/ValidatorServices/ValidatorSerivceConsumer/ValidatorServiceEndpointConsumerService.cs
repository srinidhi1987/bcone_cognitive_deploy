﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */


namespace ValidatorServices.ValidatorSerivceConsumer
{
    using System.Collections.Generic;
    using Automation.Services.Client;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.VisionBotEngine.Model;
    using Validator.Model;
    using Automation.CognitiveData.Validator.Model;
    using System;
    using Automation.VisionBotEngine;

    class DataResponseModel
    {
        public bool success;
    }
    public class ValidatorServiceEndpointConsumerService : IValidatorServiceEndpointConsumer
    {
        private HttpServiceAdapter _httpServiceAdapter = null;
        private static string baseUri = (new ServiceLocationProvider()).GetValidatorServiceConfiguration().ServiceUri;
        public ValidatorServiceEndpointConsumerService(QSetting qsetting)
        {
            _httpServiceAdapter = new HttpServiceAdapter(baseUri,qsetting.UserName,qsetting.UserToken);
        }
        public int getFailDocumentCountByProjectId(EndpointConsumerParameter parameter)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorServiceEndpointConsumerService -> getFailDocumentCountByProjectId] projectId:{0}", parameter.ProjectId));
            string endPoint = "/organizations/"+parameter.OrganizationId+ "/validator/projects/" + parameter.ProjectId + "";
            var response = _httpServiceAdapter.Get<int>(endPoint);
            return response;
        }

        public VisionBotDocumentEntity getFailVBotDocumentByProjectId(EndpointConsumerParameter parameter)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorServiceEndpointConsumerService -> getFailVBotDocumentByProjectId] projectId:{0}",parameter.ProjectId));
            string endPoint = "/organizations/"+parameter.OrganizationId+"/projects/" + parameter.ProjectId + "/visionbot/";
            var response = _httpServiceAdapter.Get<VisionBotDocumentEntity>(endPoint);
            return response;
            //return JsonConvert.DeserializeObject<VisionBotDocumentEntity>(response["data"].ToString());
        }

        public bool SaveCorrectedDocument(EndpointConsumerParameter parameter, Dictionary<string, Object> correctedVBotDocument)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorServiceEndpointConsumerService -> SaveCorrectedDocument] projectId:{0}  fileId:{1}", parameter.ProjectId,parameter.FileId));
            string endPoint = "/organizations/"+parameter.OrganizationId+"/projects/" + parameter.ProjectId+ "/visionbot/" + parameter.FileId;
            var response = _httpServiceAdapter.Post<DataResponseModel, Dictionary<string, Object>>(endPoint, correctedVBotDocument);
            if (response != null)
            return response.success;
            else
                return false;
        }

        public bool MarkDocumentInvalid(EndpointConsumerParameter parameter, IList<ReasonPostModel> reasonList)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorServiceEndpointConsumerService -> MarkDocumentInvalid] projectId:{0} fileId:{1}", parameter.ProjectId,parameter.FileId));
            string endPoint = "/organizations/" + parameter.OrganizationId + "/projects/" + parameter.ProjectId+ "/files/" + parameter.FileId + "/invalidate";
            var response = _httpServiceAdapter.Post<DataResponseModel, IList<ReasonPostModel>>(endPoint, reasonList);
            return response.success;
        }

        public bool UnlockDocuments(EndpointConsumerParameter parameter)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorServiceEndpointConsumerService -> UnlockDocuments] projectId:{0}", parameter.ProjectId));
            string endPoint = "/organizations/" + parameter.OrganizationId + "/projects/" + parameter.ProjectId + "/unlock/";
            var response = _httpServiceAdapter.Post<DataResponseModel,object>(endPoint,null);
            if (response == null)
                return true;
            return response.success;
        }

        public IList<InvalidReason> GetInvalidReasons(EndpointConsumerParameter parameter)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorServiceEndpointConsumerService -> GetInvalidReasons]"));
            string endPoint = "/validator/invalidreasons/";
            var responseModel = _httpServiceAdapter.Get<IList<ReasonGetModel>>(endPoint);
            IList<InvalidReason> invalidReasonList = new List<InvalidReason>();

            if (responseModel != null && responseModel.Count > 0)
            {
                foreach (ReasonGetModel reason in responseModel)
                {
                    invalidReasonList.Add(new InvalidReason { Id = reason.Id, Text = reason.Text });
                }

                return invalidReasonList;
            }

            return null;
        }
    }
}
