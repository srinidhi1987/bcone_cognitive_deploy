﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VBotExecutor
{
    using System.Collections.Generic;

    using Automation.CognitiveData.Analytics;
    using Automation.CognitiveData.VBotDataExporter;
    using Automation.CognitiveData.VisionBot;
    using Automation.Integrator.CognitiveDataAutomation;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    class VBotCognitiveExecutor : ICognitiveExecutor
    {

        public bool CreateNewTemplate(string templateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }

        public bool DeleteTemplate(string templateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }

        public bool EditTemplate(string templateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }

        public bool ExecuteDataProcessing(CognitiveData cognitiveData)
        {
            IVisionBotManager visionBotManager = new VisionBotManager();
            VisionBot visionBot = visionBotManager.LoadVisionBot(
                   new VisionBotManifest()
                       {
                           Name = cognitiveData.Template,
                           ParentPath = IQBotDataPathProvider.GetTemplatePath(cognitiveData.Template)
                       });

            VisionBotExecutor visionBotExecutor = new VisionBotExecutor();
           // visionBotExecutor.ProcessDocument(visionBot, cognitiveData.InputFile, cognitiveData.OutputFile);

            return true;
        }

        public IEnumerable<string> GetTemplates()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<string> GetValidationErrorsForCognitiveData(CognitiveData cognitiveData)
        {
            throw new System.NotImplementedException();
        }

        public bool IsDefaultTemplate(string templateName)
        {
            throw new System.NotImplementedException();
        }

        public bool RenameTemplate(string templateName, string newTemplateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }

        public bool ShowListConfiguration(string templateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }

        public bool ShowListToFieldMappingConfiguration(string templateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }

        public bool TrainTemplate(string templateName, string requesterUserId)
        {
            throw new System.NotImplementedException();
        }
    }
}
