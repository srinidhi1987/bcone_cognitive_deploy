﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace ValidatorServices
{
    using System;

    public class FileSystemBasedConnectivityService : IConnectivityService
    {
        public ConnectionStatus GetConnectionStatus(ConnectionEndPoint connectionEndPoint)
        {
            var endPoint = connectionEndPoint as FileSystemBasedConnectionEndPoint;
            if (endPoint == null) throw new ArgumentException($"{nameof(connectionEndPoint)} must be of type {nameof(FileSystemBasedConnectionEndPoint)}");

            return new NetworkStatus().CheckStatus(endPoint.FilePath)
                ? ConnectionStatus.Online : ConnectionStatus.Offline;
        }
    }
}