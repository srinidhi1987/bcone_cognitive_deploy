﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */


namespace ValidatorServices
{
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public delegate void ConnectivityStatusChangedHandler(ConnectionEndPoint connectionEndPoint, ConnectionStatus oldConnectionStatus, ConnectionStatus newConnectionStatus);

    public delegate void ConnectivitReconnecteddHandler(ConnectionEndPoint connectionEndPoint, ConnectionStatus oldConnectionStatus, ConnectionStatus newConnectionStatus);

    public class IterativelyCheckingConnectivityNotifierService : IConnectivityNotifierService, IDisposable
    {
        public ConnectionEndPoint ConnectionEndPoint { get; private set; }
        private BackgroundWorker _backgroundWorker_folderAvailability;

        public IterativelyCheckingConnectivityNotifierService(
            ConnectionEndPoint connectionEndPoint,
            IConnectivityService connectivityService)
        {
            this.ConnectionEndPoint = connectionEndPoint;
            this.ConnectivityService = connectivityService;
        }

        private bool IsNotifierStarted = false;
        private ConnectionStatus oldConnectivityStatus;

        public void StartNotification()
        {
            if (IsNotifierStarted) return;

            IsNotifierStarted = true;
            oldConnectivityStatus = ConnectionStatus.Offline;
            //TODO: call below in thread
            _backgroundWorker_folderAvailability = new BackgroundWorker();
            _backgroundWorker_folderAvailability.WorkerSupportsCancellation = true;
            _backgroundWorker_folderAvailability.DoWork += new DoWorkEventHandler(backgroundWorker_folderAvailability_DoWork);
            _backgroundWorker_folderAvailability.RunWorkerAsync();
            _backgroundWorker_folderAvailability.RunWorkerCompleted += _backgroundWorker_folderAvailability_RunWorkerCompleted;
        }

        private void backgroundWorker_folderAvailability_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = (BackgroundWorker)sender;
            while (!bw.CancellationPending)
            {
                Worker();
            }
            e.Cancel = true;
            //_resetEvent.Set();
        }

        private void _backgroundWorker_folderAvailability_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _backgroundWorker_folderAvailability.Dispose();
        }

        public event ConnectivityStatusChangedHandler ConnectivityStatusChanged;

        public event ConnectivitReconnecteddHandler ConnectivityReconnected;

        private IConnectivityService ConnectivityService { get; set; }

        private void Worker()
        {
            while (true)
            {
                try
                {
                    var newConnectivityStatus = ConnectivityService.GetConnectionStatus(this.ConnectionEndPoint);

                    if (oldConnectivityStatus != newConnectivityStatus)
                    {
                        if (this.ConnectivityStatusChanged != null)
                        {
                            ConnectivityStatusChanged(this.ConnectionEndPoint, oldConnectivityStatus, newConnectivityStatus);
                            if (oldConnectivityStatus == ConnectionStatus.Offline)
                            {
                                if (ConnectivityReconnected != null)
                                    ConnectivityReconnected(this.ConnectionEndPoint, oldConnectivityStatus, newConnectivityStatus);
                            }
                        }
                    }
                    oldConnectivityStatus = newConnectivityStatus;
                }
                finally
                {
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        public void Dispose()
        {
            if (_backgroundWorker_folderAvailability != null)
            {
                _backgroundWorker_folderAvailability.CancelAsync();
            }
        }

        ~IterativelyCheckingConnectivityNotifierService()
        {
            this.Dispose();
        }
    }

    public static class IterativelyCheckingConnectivityNotifierServiceProvider
    {
        private static IterativelyCheckingConnectivityNotifierService _service;

        public static IterativelyCheckingConnectivityNotifierService GetInstance(QSetting qseeting)
        {
            if (_service == null)
            {
                FileSystemBasedConnectionEndPoint fsEndPoint = new FileSystemBasedConnectionEndPoint(qseeting.QueuePath.ServerPath);

                FileSystemBasedConnectivityService fsConnectivityService = new FileSystemBasedConnectivityService();
                _service = new IterativelyCheckingConnectivityNotifierService(fsEndPoint, fsConnectivityService);
            }

            return _service;
        }
    }
}