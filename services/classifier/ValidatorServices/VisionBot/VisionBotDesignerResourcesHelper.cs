/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    //using Automation.CognitiveData.VisionBot.UI.Model;
    //using Automation.CognitiveData.VisionBot.UI.ViewModels;

    using Automation.VisionBotEngine;
    //using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    //using Automation.VisionBotEngine.PdfEngine;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media.Imaging;

    public enum ElementType
    {
        [Automation.Common.StringValue("Field")]
        Field,

        [Automation.Common.StringValue("Table Column")]
        Table,

        [Automation.Common.StringValue("Marker")]
        Marker,

        [Automation.Common.StringValue("SystemIdentifiedRegion")]
        SystemIdentifiedRegion,

        [Automation.Common.StringValue("SystemIdentifiedField")]
        SystemIdentifiedField
    }

    public enum UserDefinedRegionType
    {
        None,
        UserDefinedField,
        UserDefinedRegion
    }

    public class VisionBotDesignerResourcesHelper
    {
        internal BitmapImage GetImage(string path)
        {
            try
            {
                if (string.IsNullOrEmpty(path))
                {
                    return null;
                }

                string imagePath = File.Exists(path)
                                       ? path
                                       : Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + path;

                imagePath = new Uri(imagePath).LocalPath;

                if (!File.Exists(imagePath))
                {
                    return null;
                }

                using (var stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
                {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = stream;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();

                    stream.Close();
                    stream.Dispose();
                    return bitmapImage;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesignerResourcesHelper), nameof(GetImage));
                return null;
            }
        }
    }
}