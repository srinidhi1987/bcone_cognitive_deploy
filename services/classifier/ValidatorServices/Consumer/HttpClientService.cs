﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */


namespace ValidatorServices.Consumer
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    internal class HttpClientService
    {
        private readonly object instanceLocker = new Object();
        private HttpClient httpClient = null;
        private string baseAddress;
        private HttpServiceModel _httpServiceModel = null;

        public HttpClientService(HttpServiceModel visionbotServiceModel,string username,string tocken)
        {
            _httpServiceModel = visionbotServiceModel;
            httpClient = getClient(username,tocken);
        }

        public HttpResponseMessage Get(string subUrl)
        {
            var task = httpClient.GetAsync(subUrl);
            var test = task.Result;

            return test;

            // string result = "";

            // HttpResponse response = JsonConvert.DeserializeObject(result, typeof(HttpResponse)) as HttpResponse;

            // var project = JsonConvert.DeserializeObject(response.Data.ToString(), typeof(List<ProjectManifest>));
        }

        public HttpResponseMessage Post(string subUrl, string jsonString)
        {
            return httpClient.PostAsync(subUrl, getHttpContent(jsonString)).Result;

            //  string result = task.;

            //  HttpResponse response = JsonConvert.DeserializeObject(result, typeof(HttpResponse)) as HttpResponse;

            // var project = JsonConvert.DeserializeObject(response.Data.ToString(), typeof(List<ProjectManifest>));
        }

        private static HttpContent getHttpContent(string jsonString)
        {
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            return content;
        }

        private HttpClient getClient(string username, string tocken)
        {
            if (httpClient == null)
            {
                lock (instanceLocker)
                {
                    if (httpClient == null)
                    {
                        httpClient = new HttpClient();
                        baseAddress = _httpServiceModel.ServiceUri;
                        //var authUserName = _serviceModel.ServiceUserName;
                        //var authPassword = _serviceModel.ServiceAccessKey.GetSecureString();
                        // var authorization = new AuthenticationHeaderValue("AppToken", _serviceModel.ServiceAccessKey.GetString());
                        //httpClient.DefaultRequestHeaders.Authorization = authorization;
                        httpClient.BaseAddress = new Uri(baseAddress);
                        httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                        httpClient.DefaultRequestHeaders.Add("username", username);
                    }
                }
            }
            return httpClient;
        }
    }

    //public class ProjectManifest
    //{
    //    public string id { get; set; }
    //    public string name { get; set; }
    //}

    public class VisionbotServiceResponse
    {
        public bool success { get; set; }
        public object data { get; set; }
    }

    public class RequestData
    {
        public object data { get; set; }
    }
}