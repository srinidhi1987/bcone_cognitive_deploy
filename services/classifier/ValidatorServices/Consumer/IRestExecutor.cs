﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatorServices.Consumer
{
    internal interface IRestExecutor
    {
        T Execute<T>(IRequest request);

        IResponse Execute(IRequest request);
    }

    internal class RestExecutor : IRestExecutor
    {
        public IResponse Execute(IRequest request)
        {
            throw new NotImplementedException();
        }

        public T Execute<T>(IRequest request)
        {
            throw new NotImplementedException();
        }
    }
}