﻿using Automation.VisionBotEngine.Validation;
using Automation.VisionBotEngine.Model;
using Automation.Services.Client;

namespace ValidatorServices.VisionBotEngine.ValidationWrapper
{
    public static class VBotDataWraper
    {
        public static void EvaluateAndFillValidationDetails(VBotData vBotData, Layout layout, VBotValidatorDataManager vbotValidtorDataReader, string organizationID, string projectId)
        {
            ValidatorAdvanceInfo validatorAdvanceInfo = getValidatorAdvanceInfo(organizationID, projectId);
            vBotData.EvaluateAndFillValidationDetails(layout, vbotValidtorDataReader, validatorAdvanceInfo);
        }

        private static string _organizationID = string.Empty;
        private static string _projectId = string.Empty;
        private static ValidatorAdvanceInfo _validatorAdvanceInfo;
        private static ValidatorAdvanceInfo getValidatorAdvanceInfo(string organizationID, string projectId)
        {
            if (_organizationID != organizationID 
                || _projectId != projectId)
            {
                IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
                ProjectDetails projectDetails = projectService.GetProjectMetaData(organizationID, projectId);
                _validatorAdvanceInfo = new ValidatorAdvanceInfo()
                {
                    CultureInfo = CultureHelper.GetCultureInfo(projectDetails.PrimaryLanguage),
                    ConfidenceThreshold = projectDetails.ConfidenceThreshold
                };
                _organizationID = organizationID;
                _projectId = projectId;
            }
           
            return _validatorAdvanceInfo;
        }
    }
}
