﻿using Automation.Generic.Compression;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatorServices.VisionBotEngine.ValidationWrapper
{
    public interface IResourceService : IDisposable
    {
        //Stream fileStream { get; set; }
        void Create(string packageName);

        ResourceData GetResource(string resourceName);

        void AddResource(ResourceData resourceData);

        void DeleteResource(string resourceName);

        IList<ResourceData> GetAllResources();

        void AddFile(string uniqueId, string path);

        void DeleteFile(string uniqueId);

        void OpenResourceConnection(string packageName);

        void OpenResourceConnection(Stream fileStream);

        void CloseResourceConnection();
    }
}