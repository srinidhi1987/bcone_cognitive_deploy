﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{
    using Automation.CognitiveData.Validator.Engine;
    //using Automation.Integrator.CognitiveDataAutomation.Validator;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Validator.Service;
    using Validator.UI.Contract;
    using Validator.UI.Service;
    using Validator.UI.ViewModel;

    public sealed class VisionBotValidatorExecutor// : IVisionBotValidatorExecutor
    {
        private static ValidatorQSettingEngine validatorQSettingEngine;

        public static ValidatorQSettingEngine GetValidatorQSettingEngine
        {
            get
            {
                return validatorQSettingEngine;
            }
            private set { }
        }

        public VisionBotValidatorExecutor()
        {
            ValidatorViewModel.QueueReconnected += QSettingService.ValidatorViewModel_QueueReconnected;
            validatorQSettingEngine = new ValidatorQSettingEngine(null);
        }
        ~VisionBotValidatorExecutor()
        {
            ValidatorViewModel.QueueReconnected -= QSettingService.ValidatorViewModel_QueueReconnected;
        }
        //public Task<QSettingInitResponse> GetValidatorQueues(QSettingInitRequest initRequest)
        //{
        //    return validatorQSettingEngine.Init(initRequest);
        //}

        //public void AddObserversToEngine(IEnumerable<IObserver<QSetting>> listOfObservers)
        //{
        //    validatorQSettingEngine.SetupComponents(null, listOfObservers);
        //}

        //public Task<QSettingAddResponse> AddNewQueue(QSettingAddRequest addRequest)
        //{
        //    return validatorQSettingEngine.Add(addRequest);
        //}

        public bool OpenQueue(string name)
        {
            IValidatorUIService validatorUIService = new ValidatorUIService();
            validatorUIService.OpenValidator(name);
            return true;
        }

        //public Task<QSettingDeleteResponse> DeleteQueue(QSettingDeleteRequest deleteRequest)
        //{
        //    return validatorQSettingEngine.Delete(deleteRequest);
        //}

        //public System.Windows.Forms.Control GetValidatorStatusControl()
        //{
        //    IQBotStatusBoard board = new IQBotStatusBoard();

        //    ElementHost host = new ElementHost();
        //    host.Location = new System.Drawing.Point(18, 251);
        //    host.Size = new System.Drawing.Size(600, 200);
        //    host.Dock = System.Windows.Forms.DockStyle.Bottom;
        //    host.Child = board;
        //    host.BackColorTransparent = true;
        //    return host;
        //}
    }
}