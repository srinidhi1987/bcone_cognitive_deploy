﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Engine
{
    using Automation.CognitiveData.Validator.Service;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VisionBotEngine;

    public class ValidatorQSettingEngine
    {
        protected Service.IQSettingService qSettingService;

        public ValidatorQSettingEngine(QSetting qsetting)
        {
            //TODO: call service registry to get the instance of service.
            //TODO: call UI registry to get instance of UI Observer on the basis of the API & request type.

            SetupComponents(qsetting, null, null);
        }
        public IQSettingService GetQSettingService
        {
            get
            {
                return qSettingService;
            }
            private set { }
        }

        public virtual void SetupComponents(QSetting qsetting, Service.IQSettingService qService = null, IEnumerable<IObserver<QSetting>> listOfObservers = null)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorQSettingEngine -> SetupComponents]"));
            //this.qSettingService = new Service.QSettingService(listOfObservers);
            this.qSettingService = new Service.QSettingService(qsetting);
        }
    }
}