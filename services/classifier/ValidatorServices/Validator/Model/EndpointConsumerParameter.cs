﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatorServices.Validator.Model
{
    public class EndpointConsumerParameter
    {
        public string OrganizationId { get;}
        public string ProjectId { get;}
        public string FileId { get;}
        public EndpointConsumerParameter(string orgid,string projectid,string fileid)
        {
            OrganizationId = orgid;
            ProjectId = projectid;
            FileId = fileid;
        }
    }
}
