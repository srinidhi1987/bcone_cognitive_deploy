﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatorServices.Validator.Model
{
    public class ReasonPostModel
    {
        [JsonProperty("reasonId")]
        public string Id { get; set; }

        [JsonProperty("reasonText")]
        public string Text { get; set; }
    }
}
