﻿

using Newtonsoft.Json;
/**
* Copyright (c) 2017 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
namespace ValidatorServices.Validator.Model
{
    class FileBlobsEntity
    {
        [JsonProperty(PropertyName = "fileId")]
        public string FileId { get; set; }
        [JsonProperty(PropertyName = "fileBlob")]
        public byte[] FileBlob { get; set; }
    }
}
