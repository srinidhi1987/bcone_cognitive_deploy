﻿
using Newtonsoft.Json;
/**
* Copyright (c) 2017 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
using System;
namespace ValidatorServices.Validator.Model
{
    public class VisionBotDocumentEntity
    {
        [JsonProperty(PropertyName = "fileId")]
        public String FileId { get; set; }
        [JsonProperty(PropertyName = "projectId")]
        public String ProjectId { get; set; }
        [JsonProperty(PropertyName = "vBotDocument")]
        public string VBotDocument { get; set; }
        [JsonProperty(PropertyName = "validationStatus")]
        public String ValidationStatus { get; set; }
        [JsonProperty(PropertyName = "correctedData")]
        public string CorrectedData { get; set; }
        [JsonProperty(PropertyName = "updatedBy")]
        public String UpdatedBy { get;set; }
        [JsonProperty(PropertyName = "updatedAt")]
        public DateTime UpdatedAt { get; set; }
        [JsonProperty(PropertyName = "vBotInUse")]
        public bool VBotInUse { get; set; }
        [JsonProperty(PropertyName = "visionBotId")]
        public String VisionBotId { get; set; }
    }
}
