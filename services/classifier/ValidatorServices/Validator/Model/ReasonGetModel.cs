﻿using Newtonsoft.Json;

namespace ValidatorServices.Validator.Model
{
    public class ReasonGetModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("reason")]
        public string Text { get; set; }
    }
}
