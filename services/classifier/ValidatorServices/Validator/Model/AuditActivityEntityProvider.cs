﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
namespace Automation.CognitiveData.Validator.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class AuditActivityEntityProvider
    {
        //TODO: Abtract Activity Entity and have virtual properties.
        //Virtual properties will then be overriden as per functionality.
        //public static Activity CreateSuccessfulSaveActivityEntity()
        //{
        //    var successfulSaveActivity = new Activity
        //    {
        //        EventDescription = string.Format("Document validated successfully."),
        //        ActivityType = EventType.VisionBotValidation.ToString(),
        //        Domain = "Default", //TODO: Check if we need to send network domain?
        //        HostName = System.Environment.MachineName,
        //        UserName = Automation.Common.Routines.GetClientConfiguration().Username,
        //        Status = EventStatus.Success.ToString(),
        //        VerbosityLevel = VerbosityLevel.Information.ToString(),
        //        Detail = "Document Validation Session",
        //        Source = "Validator UI",
        //        ActivityAt = DateTime.UtcNow
        //    };
        //    //TODO: Ask if we need UTC datetime.
        //    //TODO: remove magic strings.
        //    return successfulSaveActivity;
        //}

        //public static Activity CreateFailedActivityEntity()
        //{
        //    var failedActivity = new Activity
        //    {
        //        EventDescription = string.Format("Document validation session failure."),
        //        ActivityType = EventType.VisionBotValidation.ToString(),
        //        Domain = "Default", //TODO: Check if we need to send network domain?
        //        HostName = System.Environment.MachineName,
        //        UserName = Automation.Common.Routines.GetClientConfiguration().Username,
        //        Status = EventStatus.Failed.ToString(),
        //        VerbosityLevel = VerbosityLevel.Information.ToString(),
        //        Detail = "Document Validation Session",
        //        Source = "Validator UI",
        //        ActivityAt = DateTime.UtcNow
        //    };
        //    //TODO: Ask if we need UTC datetime.
        //    //TODO: remove magic strings.
        //    return failedActivity;
        //}
    }
}
