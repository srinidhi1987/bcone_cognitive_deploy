﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service.QSettingWatcher
{
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Permissions;
    using System.Threading.Tasks;
    using VisionBot;
    using VisionBotEngine;

    //internal class QSettingWatcher
    //{
    //    private static IDictionary<string, FileSystemWatcher> qSettingWatchers;
    //    private static List<IObserver<QSetting>> _observers;
    //    private IQSettingService qSettingServiceReference;
    //    public QSettingWatcher(List<IObserver<QSetting>> observers, IQSettingService qs)
    //    {
    //        _observers = observers;
    //        qSettingServiceReference = qs;
    //        qSettingWatchers = new Dictionary<string, FileSystemWatcher>();
    //    }

    //    public void AddQueueInWatcher(QSetting queue)
    //    {
    //        if (!qSettingWatchers.ContainsKey(queue.QueuePath.ServerPath))
    //            initializeWatcher(queue.QueuePath.ServerPath);
    //    }

    //    private void initializeWatcher(string queuePath)
    //    {
    //        if (Directory.Exists(queuePath))
    //        {
    //            FileSystemWatcher qSettingWatcher = new FileSystemWatcher(queuePath);

    //            qSettingWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.DirectoryName;
    //            qSettingWatcher.Filter = "*" + Eyeball.FailedFileExtensionWithDot;

    //            qSettingWatcher.Created += new FileSystemEventHandler(QueueWatcher_Changed);
    //            qSettingWatcher.Deleted += new FileSystemEventHandler(QueueWatcher_Changed);
    //            qSettingWatcher.Renamed += new RenamedEventHandler(QueueWatcher_Changed);
    //            qSettingWatcher.EnableRaisingEvents = true;

    //            if (!qSettingWatchers.ContainsKey(queuePath))
    //            {
    //                qSettingWatchers.Add(queuePath, qSettingWatcher);
    //                VBotLogger.Trace(() => string.Format("[QSettingWatcher -> initializeWatcher] Added watcher  -> {0}", queuePath));
    //            }
    //        }
    //        else if (Directory.Exists(Path.GetDirectoryName(queuePath)))
    //        {
    //            FileSystemWatcher qFolderWatcher = new FileSystemWatcher(Path.GetDirectoryName(queuePath));
    //            qFolderWatcher.Created += new FileSystemEventHandler(Folder_Changed);
    //            //qFolderWatcher.Changed += new FileSystemEventHandler(Folder_Changed);
    //            // qFolderWatcher.Renamed += new RenamedEventHandler(Folder_Changed);

    //            qFolderWatcher.EnableRaisingEvents = true;
    //        }
    //    }

    //    private void Folder_Changed(object sender, FileSystemEventArgs e)
    //    {
    //        if (e.Name.Equals("FailQ"))
    //        {
    //            VBotLogger.Trace(() => $"{nameof(QSettingWatcher)}->{nameof(Folder_Changed)} FailQ Folder created and so reinitializing the qSetting.");
    //            var qSettingWatcher = sender as FileSystemWatcher;
    //            string path = Path.Combine(qSettingWatcher.Path, "FailQ");
    //            initializeWatcher(path);
    //        }
    //    }

    //    private void QueueWatcher_Changed(object sender, FileSystemEventArgs e)
    //    {
    //        var qSettingWatcher = sender as FileSystemWatcher;
    //        //IQSettingService qSettingservice = VisionBotValidatorExecutor.GetValidatorQSettingEngine.GetQSettingService;
    //        if (_observers != null)
    //            _observers.ForEach(x => qSettingServiceReference.Subscribe(x));

    //        var task = Task.Run(() =>
    //        {
    //            QSettingUpdateRequest updateRequest = new QSettingUpdateRequest();
    //            updateRequest.Model = new QSetting();
    //            updateRequest.Model.QueuePath = new QPath();
    //            updateRequest.Model.QueuePath.ServerPath = qSettingWatcher.Path;
    //            switch(e.ChangeType)
    //            {
    //                case WatcherChangeTypes.Created:
    //                    updateRequest.Model.QSettingsFileSystemEventType = QSettingsFileSystemEventType.Created;
    //                    break;
    //                case WatcherChangeTypes.Renamed:
    //                    if(Path.GetExtension(e.Name) == Eyeball.FailedFileExtensionWithDot)
    //                        updateRequest.Model.QSettingsFileSystemEventType = QSettingsFileSystemEventType.Created;
    //                    else
    //                        updateRequest.Model.QSettingsFileSystemEventType = QSettingsFileSystemEventType.Renamed;
    //                    break;
    //                case WatcherChangeTypes.Deleted:
    //                    updateRequest.Model.QSettingsFileSystemEventType = QSettingsFileSystemEventType.Deleted;
    //                    break;
    //                default:
    //                    updateRequest.Model.QSettingsFileSystemEventType = QSettingsFileSystemEventType.Default;
    //                    break;
    //            }
    //            //Call QsettingService Refresh method to update QSettting
    //            qSettingServiceReference.Refresh(updateRequest);
    //        });
    //    }

    //    public void RemoveQueueFromWatcher(string queuePath)
    //    {
    //        if (qSettingWatchers.ContainsKey(queuePath))
    //        {
    //            var watcher = qSettingWatchers[queuePath];
    //            watcher.EnableRaisingEvents = false;
    //            qSettingWatchers.Remove(queuePath);
    //            watcher.Dispose();
    //        }
    //    }
    //}
}