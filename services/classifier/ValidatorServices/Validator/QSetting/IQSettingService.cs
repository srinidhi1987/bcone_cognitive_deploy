﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service
{
    //using Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IQSettingService : IObservable<QSetting>
    {
        /// <summary>
        /// Initialize Q Settings.
        /// For every validator, QSettings are initialized and stored in local validator machine.
        /// </summary>
        /// <param name="user">IUser</param>
        /// <returns>QSetting</returns>
        //Task<IServiceResponse<IEnumerable<QSetting>>> Init(IServiceRequest<QSetting> initRequest);

        /// <summary>
        /// Get All Error Queues for IUser
        /// </summary>
        /// <param name="user">Users</param>
        /// <returns>QSettings</returns>
        //Task<IServiceResponse<IEnumerable<QSetting>>> Get(IServiceRequest<QSetting> getRequest);

        /// <summary>
        /// Add new Q Settings.
        /// </summary>
        /// <param name="qSettings">QSetting</param>
        /// <param name="user">IUser</param>
        /// <returns>Queue Settings</returns>
        //Task<IServiceResponse<IEnumerable<QSetting>>> Add(IServiceRequest<QSetting> addRequest);

        /// <summary>
        /// Update Settings.
        /// </summary>
        /// <param name="user">IUser</param>
        /// <param name="qSetting">QSetting</param>
        /// <returns></returns>
        Task<ServiceResponse<IEnumerable<QSetting>>> Refresh(ServiceRequest<QSetting> updateRequest);

        /// <summary>
        /// Add new Q Settings.
        /// </summary>
        /// <param name="qSettings">QSetting</param>
        /// <param name="user">IUser</param>
        /// <returns>Queue Settings</returns>
        //Task<IServiceResponse<IEnumerable<QSetting>>> Delete(IServiceRequest<QSetting> deleteRequest);
    }
}