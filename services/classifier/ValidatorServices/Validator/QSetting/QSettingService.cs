﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service
{
    using Analytics;
    //using Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    //using Properties;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.AccessControl;
    using System.Threading.Tasks;
    using ValidatorServices.Validator.Model;
    using ValidatorServices.ValidatorSerivceConsumer;
    //using UI.ViewModel;
    using VisionBot;
    using VisionBotEngine;
    using VisionBotEngine.Configuration;

    public class QSettingService : IObservable<QSetting>, IQSettingService
    {
        public static List<IObserver<QSetting>> observers;
        protected string initPathOfQSettings;
        protected string pathOfJSON;

        //TODO: Remove hard-coding.
        private string qSettingsFileName = ValidatorServices.Properties.Resources.qSettingsJSONFileName;

        //private static QSettingWatcher.QSettingWatcher qSettingWatcher;

        public static event Action<QSetting> OpenDocumentInValidatorRequest;
        private ValidatorServiceEndpointConsumerService validatorServiceEndpointConsumerService;
        //TODO: Have a service registry to resolve depedency of QSettingService.
        //TODO: Remove hard coupling of the code with System.IO.File interface.
        
        public QSettingService(QSetting qsetting )
        {
            validatorServiceEndpointConsumerService = new ValidatorServiceEndpointConsumerService(qsetting);
            Setup();

        }
        ~QSettingService()
        {
        }
        public static void ValidatorViewModel_QueueReconnected(QSetting qsetting)
        {
            if (OpenDocumentInValidatorRequest != null)
                Task.Run(() => { OpenDocumentInValidatorRequest(qsetting); });
        }



        protected virtual void Setup()
        {
            //TODO: Why if logic inside constructor?
            //It should be strategy driven.
            if (string.IsNullOrEmpty(initPathOfQSettings))
            {
                IQBotDataPathProvider provider = new IQBotDataPathProvider();
                pathOfJSON = provider.GetPath();
                initPathOfQSettings = Path.GetTempPath();
            }
        }
        internal void LongRunningTask(QSetting qSetting)
        {
            try
            {
                if (qSetting.QueueStats == null)
                    qSetting.QueueStats = new QStats();
                EndpointConsumerParameter parameter = new EndpointConsumerParameter(qSetting.OrganizationId, qSetting.ProjectId, "");
                qSetting.QueueStats.TotalNoOfDocumentsInQ = validatorServiceEndpointConsumerService.getFailDocumentCountByProjectId(parameter);
                VBotLogger.Trace(() => $"[{nameof(QSettingService)} -> {nameof(LongRunningTask)}]");
            }
            catch(Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(QSettingService)} -> {nameof(LongRunningTask)}] {ex.Message}");
            }
        }


        public IDisposable Subscribe(IObserver<QSetting> observer)
        {
            return null;
        }

        /// <summary>
        /// Updates QSettings.
        /// Pass null to refresh all error queues.
        /// </summary>
        /// <param name="updateRequest"></param>
        /// <returns></returns>
        public async Task<ServiceResponse<IEnumerable<QSetting>>> Refresh(ServiceRequest<QSetting> updateRequest)
        {
            //Send updated model.
            //if updateRequest is null.
            try
            {
                LongRunningTask(updateRequest.Model);
                //AddQueueInWatcher(updateRequest.Model);
                QSettingUpdateResponse response = new QSettingUpdateResponse();
                response.ResponseCode = 200;
                QSettingList qSettingList = new QSettingList() { qSettings = new List<QSetting>() };
                qSettingList.qSettings.Add(updateRequest.Model);
                response.Model =  qSettingList.qSettings;
                return response;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(QSettingService), nameof(Refresh));
                VBotLogger.Error(() => $"[{nameof(QSettingService)} -> {nameof(Refresh)}]");
                QSettingUpdateResponse response = new QSettingUpdateResponse();
                //TODO: Need to fix the type definition.
                //response.Request = initRequest;
                response.ResponseCode = 100;
                return response;
            }
            
        }
    }

    public static class QSettingsHelper
    {
        public static QSetting GetQSetting(string queuePath)
        {
            var result = new QSetting
            {
                QueuePath = new QPath
                {
                    ServerPath = queuePath
                }
            };
            result.Refresh();
            return result;
        }

        public static void Refresh(this QSetting qSetting)
        {
            if (qSetting == null) throw new ArgumentNullException(nameof(qSetting));
            var service = new QSettingService(qSetting);
            QSettingUpdateRequest updateRequest = null;
            IList<QSetting> updatedQSettings = null;
            VBotLogger.Trace(() => $"[{nameof(QSettingsHelper)}->{nameof(Refresh)}] This refresh is manual triggered.");
            var task = Task.Run(() =>
            {
                updateRequest = new QSettingUpdateRequest();
                updateRequest.Model = qSetting;
                //Call QsettingService Refresh method to update QSettting
                var response = service.Refresh(updateRequest);
                while (!response.IsCompleted)
                    System.Threading.Thread.Yield();
                updatedQSettings = response.Result.Model.ToList();
            });
            Task.WaitAll(task);
        }
    }

    internal static class QPathExtensions
    {
        public static void SetQueueName(this QPath qPath)
        {
            string directoryPath = Path.GetDirectoryName(qPath.ServerPath);
            qPath.QueueName = Path.GetFileNameWithoutExtension(directoryPath);
        }
    }
}