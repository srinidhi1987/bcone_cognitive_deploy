using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
    public class ServiceLocationProvider
    {
        private CognitiveServicesConfiguration _cognitiveServicesConfiguration = new CognitiveServicesConfiguration();

        private string servicConfigurationPath = "CognitiveServiceConfiguration.json";

        public ServiceLocationProvider()
        {
            if (File.Exists(servicConfigurationPath))
            {
                string path = Path.Combine(
        Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
                string json = File.ReadAllText(path);

                _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);
            }

            if (_cognitiveServicesConfiguration.ServiceConfiguration.Count == 0)
            {
                _cognitiveServicesConfiguration.ServiceConfiguration = new List<VisionbotServiceModel>();
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetFileServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetAliasServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetVisionBotServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetProjectServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetMLServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetValidatorServiceConfiguration());
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(_cognitiveServicesConfiguration);
                try
                {
                    File.WriteAllText(servicConfigurationPath, json);
                }
                catch (Exception ex)
                { }
            }
        }

        public VisionbotServiceModel GetVisionBotServiceConfiguration()
        {
            VisionbotServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "VisionBotService");
            if (serviceModel == null)
            {
                serviceModel = new VisionbotServiceModel() { Name = "VisionBotService", ServiceUri = "http://localhost:9998" };
            }

            return serviceModel;
        }

        public VisionbotServiceModel GetFileServiceConfiguration()
        {
            VisionbotServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "FileService");
            if (serviceModel == null)
            {
                serviceModel = new VisionbotServiceModel() { Name = "FileService", ServiceUri = "http://localhost:9996" };
            }

            return serviceModel;
        }

        public VisionbotServiceModel GetProjectServiceConfiguration()
        {
            VisionbotServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "ProjectService");
            if (serviceModel == null)
            {
                serviceModel = new VisionbotServiceModel() { Name = "ProjectService", ServiceUri = "http://localhost:9999" };
            }

            return serviceModel;
        }

        public VisionbotServiceModel GetAliasServiceConfiguration()
        {
            VisionbotServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "AliasService");
            if (serviceModel == null)
            {
                serviceModel = new VisionbotServiceModel() { Name = "AliasService", ServiceUri = "http://localhost:9997" };
            }

            return serviceModel;
        }

        public VisionbotServiceModel GetValidatorServiceConfiguration()
        {
            VisionbotServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "ValidatorService");
            if (serviceModel == null)
            {
                serviceModel = new VisionbotServiceModel() { Name = "ValidatorService", ServiceUri = "http://localhost:9995" };
            }

            return serviceModel;
        }
        public VisionbotServiceModel GetMLServiceConfiguration()
        {
            VisionbotServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "MachineLearningService");
            if (serviceModel == null)
            {
                serviceModel = new VisionbotServiceModel() { Name = "MachineLearningService", ServiceUri = "http://172.16.3.17:8080" };
            }
            return serviceModel;
        }

        public RabbitMQSetting GetRabbitMQSetting()
        {
            return _cognitiveServicesConfiguration.RabbitMQSettings;
        }
    }

    public class RabbitMQSetting
    {
        public string VirtualHost { get; set; }
        public string Uri { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }

        public RabbitMQSetting()
        {
            VirtualHost = "test";
            Uri = "localhost";
            UserName = "messagequeue";
            Password = "passmessage";
        }
    }

    public class CognitiveServicesConfiguration
    {
        public List<VisionbotServiceModel> ServiceConfiguration = new List<VisionbotServiceModel>();
        public RabbitMQSetting RabbitMQSettings = null;

        public CognitiveServicesConfiguration()
        {
            RabbitMQSettings = new RabbitMQSetting();
        }
    }
}