﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service.WorkFlow
{
    using Automation.CognitiveData.Validator.Model;
    using Automation.CognitiveData.VisionBot;
    using Automation.Generic.Compression;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.VisionBotEngine.Model;
    using Newtonsoft.Json;
    using Services.Client;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using ValidatorServices.MachineLearningServiceConsumer;
    using ValidatorServices.Validator.Model;
    using ValidatorServices.ValidatorSerivceConsumer;
    using VisionBotEngine;

    public class ValidatorWorkflowService : IWorkflowService
    {
        //public static List<IObserver<FailedVBotDocument>> observers;
        private IValidatorServiceEndpointConsumer validatorServiceEndpointConsumer;

        private IMachineLearningServiceEndpointConsumer machineLearningServiceEndpointConsumer;
        private IFileServiceEndPointConsumer fileServiceEndPointConsumer;
        private string _projectId;
        private string _organizationId;

        public ValidatorWorkflowService(QSetting qSetting)
        {
            //observers = new List<IObserver<FailedVBotDocument>>(); // Bring it from registry of UI
            _projectId = qSetting.ProjectId;
            _organizationId = qSetting.OrganizationId;
            validatorServiceEndpointConsumer = new ValidatorServiceEndpointConsumerService(qSetting);
            machineLearningServiceEndpointConsumer = new MachineLearningEndpointClientSideConsumerService(qSetting);
            fileServiceEndPointConsumer = new FileServiceEndPointConsumer(qSetting.OrganizationId, qSetting.ProjectId);

            Setup();
        }

        public IDisposable Subscribe(IObserver<FailedVBotDocument> observer)
        {
            //Return IDisposable unscriber back to the Service.
            //Service allows
            //if (!observers.Contains(observer))
            //    observers.Add(observer);
            //return new ValidatorUnsubscriber(observers, observer);
            return null;
        }

        public ServiceResponse<FailedVBotDocument> Init(ServiceRequest<FailedVBotDocument> initRequest)
        {
            throw new NotImplementedException();
        }

        public ServiceResponse<FailedVBotDocument> Close(ServiceRequest<FailedVBotDocument> closeRequest)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> Close]"));
            if (closeRequest == null)
                return null;

            //Is user just toying with next button?
            string failedFile = closeRequest.Model.FailedDocument != null
                ? closeRequest.Model.FailedDocument.DocumentProperties.Path
                : string.Empty;

            if (string.IsNullOrEmpty(failedFile))
            {
                var errorresponse = new CloseDocumentResponse()
                {
                    ErrorCode = 102 //File not found.
                };
                return errorresponse;
            }

            //UnLock existing failed document.
            //TODO: Move code to resilient state.

            var failedVBotDocumentLocalMachinePath = failedFile;

            //Delete local file.
            if (File.Exists(failedVBotDocumentLocalMachinePath))
            {
                FileInfo file = new FileInfo(failedVBotDocumentLocalMachinePath);
                while (IsFileLocked(file))
                    Thread.Sleep(1000);
                File.Delete(failedVBotDocumentLocalMachinePath);
            }
            VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Close)}] filestream has been closed and deleted successfully.");
            var response = new CloseDocumentResponse()
            {
                ResponseCode = 200
            };
            return response;
        }

        private bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)) { };
            }
            catch (IOException)
            {
                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(IsFileLocked)}] file is locked by another process. vbotpfilename={file.Name}");
                return true;
            }

            return false;
        }

        public ServiceResponse<FailedVBotDocument> Prev(ServiceRequest<FailedVBotDocument> prevRequest)
        {
            return null;
        }

        public ServiceResponse<FailedVBotDocument> Next(ServiceRequest<FailedVBotDocument> nextRequest)
        {
            try
            {
                VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> Next]"));
                //Get QSetting object from request.
                //Do sanity check of QSetting object.
                //Find next failed document available for eyeball.
                //Copy the available document to local machine path.
                //Create object and return back as response.
                var qsetting = nextRequest.Model.Queue;
                //var sanityCheckResponse = DoSanityCheck(ref qsetting);
                //if (sanityCheckResponse != null)
                //    return sanityCheckResponse;
                this.Close(nextRequest);
                qsetting.Refresh();
                EndpointConsumerParameter parameter = new EndpointConsumerParameter(_organizationId, _projectId, "");
                VisionBotDocumentEntity vBotDocumententity = validatorServiceEndpointConsumer.getFailVBotDocumentByProjectId(parameter);
                VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> Next] VisionBotDocumentEntity loaded."));
                if (vBotDocumententity != null)
                {
                    VBotDocument vBotDocument = JsonConvert.DeserializeObject<VBotDocument>(vBotDocumententity.VBotDocument);
                    VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> Next] VBotDocument deserialized."));
                    var validatorUtility = new WorkFlowServiceUtility(qsetting);
                    //string tempPath = validatorUtility.GetDocumentLocalMachinePath(qsetting.OrganizationId,qsetting.ProjectId,vBotDocumententity.Fileid, Path.GetFileName(vBotDocument.DocumentProperties.Path));
                    //if (tempPath != "")
                    {
                        FileDetails fileDetail = fileServiceEndPointConsumer.GetDocumentDetails(vBotDocumententity.FileId);
                        VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> Next] FileDetails loaded. FileId:{0}", vBotDocumententity.FileId));
                        vBotDocument.DocumentProperties.Id = fileDetail.FileId;
                        vBotDocument.DocumentProperties.Path = LocalMachineFileHelper.GetLocalFilePath(Path.GetFileName(vBotDocument.DocumentProperties.Path));
                        var failedDocument = new FailedVBotDocument()
                        {
                            FailedDocument = vBotDocument,
                            MetaData = new FailedVBotDocumentMetaData()
                            {
                                ClassificationId = fileDetail.ClassificationId,
                                VisionBotId = vBotDocumententity.VisionBotId
                            },
                            Queue = qsetting,
                            PackageFileStream = null,
                            State = 101,
                            StateDescription = "Package Document downloaded.",
                            fileid = vBotDocumententity.FileId
                        };
                        var response = new NextDocumentResponse()
                        {
                            Model = failedDocument,
                            Request = nextRequest,
                            ResponseCode = 200
                        };
                        return response;
                    }
                }
                else
                {
                    VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> Next] VisionBotDocument not available."));
                    var response = new NextDocumentResponse()
                    {
                        ResponseCode = 102,
                        ErrorCode = 103 //No files available.
                    };
                    return response;
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] {ex.Message}"));
                var response = new NextDocumentResponse()
                {
                    ResponseCode = 102,
                    ErrorCode = 103 //No files available.
                };
                return response;
            }
        }

        private Tuple<List<FieldValueLearningDataModel>, List<FieldAccuracyModel>> PrepareLearningData(String projectId, VBotData failData, VBotData correctedData)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> PrepareLearningData]"));
            LevenshteinSimilarityCalculator similarityCalculator = new LevenshteinSimilarityCalculator();
            List<FieldAccuracyModel> fieldAccuracylist = new List<FieldAccuracyModel>();
            List<FieldValueLearningDataModel> learningdatalist = new List<FieldValueLearningDataModel>();
            for (int i = 0; i < failData.FieldDataRecord.Fields.Count; i++)
            {
                FieldAccuracyModel fieldAccuracy = new FieldAccuracyModel();
                String oldvalue = failData.FieldDataRecord.Fields[i].Value.Field.Text,
                    newvalue = correctedData.FieldDataRecord.Fields[i].Value.Field.Text;
                fieldAccuracy.FieldId = failData.FieldDataRecord.Fields[i].Field.Id;
                fieldAccuracy.OldValue = oldvalue;
                fieldAccuracy.NewValue = newvalue;
                if (oldvalue == newvalue)
                {
                    //IF OLD AND NEW VALUE ARE SAME, MEANS INCREASE PASS BY ONE
                    fieldAccuracy.PassAccuracyValue = 1;
                }
                else if (oldvalue != ""
                    && newvalue != ""
                    && oldvalue != newvalue)
                {
                    //Apply levenshtein
                    fieldAccuracy.PassAccuracyValue = (float)similarityCalculator.GetSimilarity(oldvalue, newvalue);
                    FieldValueLearningDataModel learningModel = new FieldValueLearningDataModel();
                    learningModel.FieldId = projectId + "_" + failData.FieldDataRecord.Fields[i].Field.Id;
                    learningModel.OriginalValue = oldvalue;
                    learningModel.CorrectedValue = newvalue;
                    learningdatalist.Add(learningModel);
                }
                fieldAccuracylist.Add(fieldAccuracy);
            }
            for (int i = 0; i < failData.TableDataRecord.Count; i++)
            {
                for (int j = 0; j < failData.TableDataRecord[i].Rows.Count; j++)
                {
                    for (int k = 0; k < failData.TableDataRecord[i].Rows[j].Fields.Count; k++)
                    {
                        String oldvalue = failData.TableDataRecord[i].Rows.Count < j + 1 ? "" :
                            failData.TableDataRecord[i].Rows[j].Fields[k].Value.Field.Text,
                            newvalue = correctedData.TableDataRecord[i].Rows.Count < j + 1 ? "" :
                            correctedData.TableDataRecord[i].Rows[j].Fields[k].Value.Field.Text;

                        if (oldvalue != "" && oldvalue != "" &&
                                oldvalue != newvalue)
                        {
                            FieldValueLearningDataModel learningModel = new FieldValueLearningDataModel();
                            learningModel.FieldId = projectId + "_" + failData.TableDataRecord[i].Rows[j].Fields[k].Field.Id;
                            learningModel.OriginalValue = oldvalue;
                            learningModel.CorrectedValue = newvalue;
                            learningdatalist.Add(learningModel);
                        }
                    }
                }
                for (int j = 0; j < correctedData.TableDataRecord[i].Rows.Count; j++)
                {
                    for (int k = 0; k < correctedData.TableDataRecord[i].Rows[j].Fields.Count; k++)
                    {
                        FieldAccuracyModel fieldAccuracy = new FieldAccuracyModel();

                        String oldvalue = failData.TableDataRecord[i].Rows.Count < j + 1 ? "" :
                            failData.TableDataRecord[i].Rows[j].Fields[k].Value.Field.Text,
                            newvalue = correctedData.TableDataRecord[i].Rows[j].Fields[k].Value.Field.Text;

                        fieldAccuracy.FieldId = failData.TableDataRecord[i].Rows[0].Fields[k].Field.Id;
                        fieldAccuracy.OldValue = oldvalue;
                        fieldAccuracy.NewValue = newvalue;

                        if (oldvalue == newvalue)
                        {
                            fieldAccuracy.PassAccuracyValue = 1;
                        }
                        if (oldvalue != "" && oldvalue != "" &&
                                oldvalue != newvalue)
                        {
                            fieldAccuracy.PassAccuracyValue = (float)similarityCalculator.GetSimilarity(oldvalue, newvalue);
                        }
                        fieldAccuracylist.Add(fieldAccuracy);
                    }
                }
            }
            //return learningdatalist;
            return Tuple.Create(learningdatalist, fieldAccuracylist);
        }

        public ServiceResponse<FailedVBotDocument> Save(ServiceRequest<FailedVBotDocument> saveRequest)
        {
            try
            {
                VBotLogger.Trace(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}]"));
                var qSetting = saveRequest.Model.Queue;

                var tupleObject = PrepareLearningData(qSetting.ProjectId, saveRequest.Model.FailedDocument.Data, saveRequest.Model.CorrectedDocument.Data);
                Dictionary<string, Object> saveTovalidatorService = new Dictionary<string, object>();
                saveTovalidatorService.Add("vBotDocument", saveRequest.Model.CorrectedDocument);
                saveTovalidatorService.Add("fieldAccuracyList", tupleObject.Item2);

                EndpointConsumerParameter parameter = new EndpointConsumerParameter(qSetting.OrganizationId, qSetting.ProjectId, saveRequest.Model.fileid);
                bool result = validatorServiceEndpointConsumer.SaveCorrectedDocument(parameter, saveTovalidatorService);

                if (!result)
                {
                    return new SaveDocumentResponse
                    {
                        Model = saveRequest.Model,
                        Request = saveRequest,
                        ResponseCode = -1, //TODO: decide ResponseCode
                        ErrorCode = 1 //TODO: decide ErrorCode
                    };
                }

                VBotLogger.Trace(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}] document Saved."));
                if (tupleObject.Item1.Count > 0)
                {
                    bool success = machineLearningServiceEndpointConsumer.SaveLearningData(qSetting.ProjectId, saveRequest.Model.fileid, tupleObject.Item1);
                    VBotLogger.Trace(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}] Learning Data Saved."));
                }

                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}] file has been saved successfully. fileID={saveRequest.Model.fileid}");

                return new SaveDocumentResponse
                {
                    Model = saveRequest.Model,
                    Request = saveRequest,
                    ResponseCode = 1, //TODO: decide ResponseCode
                    ErrorCode = -1 //TODO: decide ErrorCode
                };
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}] {ex.Message}"));
                return new SaveDocumentResponse
                {
                    Model = saveRequest.Model,
                    Request = saveRequest,
                    ResponseCode = -1, //TODO: decide ResponseCode
                    ErrorCode = 1 //TODO: decide ErrorCode
                };
            }
        }

        protected virtual void Setup()
        {
            //TODO: Why if logic inside constructor?
            //It should be strategy driven.
            //Update: logic moved to lazy-loading property
            //if (string.IsNullOrEmpty(initPathOfQSettings))
            //{
            //    var provider = new IQBotDataPathProvider();
            //    initPathOfQSettings = provider.GetPath();
            //}
        }

        public ServiceResponse<InvalidDocument> Invalid(ServiceRequest<InvalidDocument> invalidRequest)
        {
            VBotLogger.Trace(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Invalid)}]"));
            EndpointConsumerParameter parameter = new EndpointConsumerParameter(
                invalidRequest.Model.OrganizationId,
                invalidRequest.Model.ProjectId,
                invalidRequest.Model.FileId);

            IList<ReasonPostModel> reasonList = new List<ReasonPostModel>();
            IList<InvalidReason> invalidReasonList = invalidRequest.Model.Reasons;
            if (invalidReasonList != null)
            {
                foreach (InvalidReason reason in invalidReasonList)
                {
                    reasonList.Add(new ReasonPostModel { Id = reason.Id, Text = reason.Text });
                }
            }

            bool result = validatorServiceEndpointConsumer.MarkDocumentInvalid(parameter, reasonList);
            if (result)
            {
                return new InvalidDocumentResponse
                {
                    Model = invalidRequest.Model,
                    Request = invalidRequest,
                    ResponseCode = 1, //TODO: decide ResponseCode
                    ErrorCode = -1 //TODO: decide ErrorCode
                };
            }
            else
                return new InvalidDocumentResponse
                {
                    Model = invalidRequest.Model,
                    Request = invalidRequest,
                    ResponseCode = -1, //TODO: decide ResponseCode
                    ErrorCode = 1 //TODO: decide ErrorCode
                };
        }

        public ServiceResponse<FailedVBotDocument> Unlock(ServiceRequest<FailedVBotDocument> closeRequest)
        {
            VBotLogger.Trace(() => string.Format($"[{nameof(ValidatorWorkflowService)}->{nameof(Unlock)}]"));
            EndpointConsumerParameter parameter = new EndpointConsumerParameter(closeRequest.Model.Queue.OrganizationId, closeRequest.Model.Queue.ProjectId, "");
            validatorServiceEndpointConsumer.UnlockDocuments(parameter);
            return null;
        }
    }

    internal class ValidatorUnsubscriber : IDisposable
    {
        private readonly IObserver<FailedVBotDocument> _observer;
        private readonly List<IObserver<FailedVBotDocument>> _observers;

        public ValidatorUnsubscriber(List<IObserver<FailedVBotDocument>> observers, IObserver<FailedVBotDocument> observer)
        {
            _observers = observers;
            _observer = observer;
        }

        public void Dispose()
        {
            if (_observer != null && _observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }

    internal class WorkFlowServiceUtility
    {
        //private readonly ValidatorEngine validatorEngine;
        private ValidatorServices.FileServiceConsumer.FileServiceEndpointConsumerService fileServiceEndpointConsumerService;

        public WorkFlowServiceUtility(QSetting qsetting)
        {
            //validatorEngine = new ValidatorEngine();
            fileServiceEndpointConsumerService = new ValidatorServices.FileServiceConsumer.FileServiceEndpointConsumerService(qsetting);
        }

        public string GetDocumentLocalMachinePath(string organizationid, string projectid, string fileid, string filename)
        {
            try
            {
                EndpointConsumerParameter parameter = new EndpointConsumerParameter(organizationid, projectid, fileid);
                List<FileBlobsEntity> filebloblist = fileServiceEndpointConsumerService.getFileBlob(parameter);
                String tempPath = LocalMachineFileHelper.GetLocalFilePath(filename);
                if (File.Exists(tempPath))
                {
                    File.Delete(tempPath);
                }
                FileBlobsEntity fileblob = filebloblist[0];
                using (FileStream fs = File.Create(tempPath))
                {
                    Stream stream = new MemoryStream(fileblob.FileBlob);
                    CopyStream(stream, fs);
                }
                return tempPath;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
    }
}