﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service
{
    using Automation.CognitiveData.Validator.Model;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IWorkflowService : IObservable<FailedVBotDocument>
    {
        ServiceResponse<FailedVBotDocument> Init(ServiceRequest<FailedVBotDocument> initRequest);

        ServiceResponse<FailedVBotDocument> Prev(ServiceRequest<FailedVBotDocument> prevRequest);

        ServiceResponse<FailedVBotDocument> Next(ServiceRequest<FailedVBotDocument> nextRequest);

        ServiceResponse<InvalidDocument> Invalid(ServiceRequest<InvalidDocument> invalidRequest);

        ServiceResponse<FailedVBotDocument> Save(ServiceRequest<FailedVBotDocument> saveRequest);

        ServiceResponse<FailedVBotDocument> Close(ServiceRequest<FailedVBotDocument> closeRequest);

        ServiceResponse<FailedVBotDocument> Unlock(ServiceRequest<FailedVBotDocument> closeRequest);
    }
}