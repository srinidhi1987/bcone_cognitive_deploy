﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{
    //using Automation.CognitiveData.Properties;
    using Automation.CognitiveData.Analytics;
    using Automation.CognitiveData.Generic;
    using System;
    using System.IO;
    using System.Runtime.Remoting.Metadata.W3cXsd2001;
    using VisionBotEngine.Model;
    using VisionBotEngine.Configuration;

    //using IQBotConstants = Automation.Util.IQBots.Constants;
    using global::VisionBotEngine.Model.Enum;

    public static class Constants
    {
        public static class CDC
        {
            internal static readonly int MinNoOfParallelExtractionsInEngine = 1;
            internal static readonly int MaxNoOfParallelExtractionsInEngine = 9;
            internal static readonly int DefaultNoOfParallelExtractionsInEngine = 2;
            internal static readonly ErrorExportTypes DefaultErrorExportType = ErrorExportTypes.CombinedDataAndErrorCsv;

            public static readonly string TitleSingular = "VisionBot";
            public static readonly string MainFolderName = "Main";
            public static readonly string ExtensionFilter = "*.vbot";
            public static readonly string EngineTitle = string.Format("{0} Engine", TitleSingular);
            public static readonly double Version_1_0 = 1.0;

            public static readonly string VALIDATOR_INVALID_QUEUE_MESSAGE = "Unable to open validation queue :" + "\n\n" + "{0}" + "\n\n" + "This could be due to any of the following reasons:" + "\n" +
                                               "- You do not have necessary read / write permissions." + "\n" +
                                               "- The queue is unreachable or does not exist.";

            public static readonly string VALIDATOR_MESSAGE_TITLE = "Validation Queue";

            private static string __applicationDataPath;

            public static string ApplicationDataPath
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(__applicationDataPath))
                    {
                        return __applicationDataPath;
                    }

                    __applicationDataPath = (new IQBotDataPathProvider()).GetPath();

                    return __applicationDataPath;
                }
            }

            public static string ProductInstallationPath
            {
                get
                {
                    UriBuilder uri = new UriBuilder(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                    return Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
                }
            }
        }
    }

    public static class Configurations
    {
        public static class Visionbot
        {
            public static bool UseVectorInfoInPdf
            {
                get
                {
                    return Get("usevector", "1") != "0";
                }
            }

            public static bool PreserveOriginalDpi
            {
                get
                {
                    return Get("originaldpi", "1") != "0";
                }
            }

            public static bool IsLineRemovalEnabled
            {
                get
                {
                    return Convert.ToInt32(Configurations.Get($"{Visionbot.SectionName}\\accuracylevel2", "removelines", "0")) > 0;
                }
            }

            public static bool ShowAllMappedFieldsInValidator
            {
                get
                {
                    return Convert.ToInt32(Configurations.Get($"{Visionbot.SectionName}\\validator", "regiondisplaymode", "0")) > 0;
                }
            }

            public static bool IsCheckBoxRecognitionFeatureEnabled
            {
                get
                {
                    return Convert.ToInt32(Get("ischeckboxrecognitionenabled", "0")) > 0;
                }
            }

            public static bool IsAccuracyLevelEnabled
            {
                get
                {
                    return Convert.ToInt32(Get("accuracylevel", "1")) > 0;
                }
            }

            public static string AbbyyDataPath
            {
                get
                {
                    return Get("enginepath", "");
                }
            }

            private static int? _noOfParallelExtractionsInEngine;

            public static int NoOfParallelExtractionsInEngine
            {
                get
                {
                    if (_noOfParallelExtractionsInEngine == null)
                    {
                        int noOfParallelExtractions = Constants.CDC.DefaultNoOfParallelExtractionsInEngine;

                        var noOfParallelExtractionsStr = Get("noofparallelextractions", "2");

                        Int32.TryParse(noOfParallelExtractionsStr, out noOfParallelExtractions);
                        if (noOfParallelExtractions > Constants.CDC.MaxNoOfParallelExtractionsInEngine)
                        {
                            noOfParallelExtractions = Constants.CDC.MaxNoOfParallelExtractionsInEngine;
                        }
                        else if (noOfParallelExtractions < Constants.CDC.MinNoOfParallelExtractionsInEngine)
                        {
                            noOfParallelExtractions = Constants.CDC.MinNoOfParallelExtractionsInEngine;
                        }

                        _noOfParallelExtractionsInEngine = noOfParallelExtractions;
                    }

                    return _noOfParallelExtractionsInEngine.Value;
                }
            }

            public static int ValidatorConfiguredMaxPageLimit
            {
                get
                {
                    const int _defaultPageLimit = 60;

                    int pageLimit = _defaultPageLimit;
                    string configuredMaxPagesAllowed = Get("maxpagesallowed", _defaultPageLimit.ToString());
                    int.TryParse(configuredMaxPagesAllowed, out pageLimit);

                    return pageLimit;
                }
            }

            internal static ErrorExportTypes? __currentErrorExportType;

            internal static ErrorExportTypes ErrorExportType
            {
                get
                {
                    const string key = "errorexporttype";

                    if (__currentErrorExportType == null)
                    {
                        var errorExportType = Get(key, "-1");

                        if (errorExportType == "-1")
                        {
                            errorExportType = ((int)Constants.CDC.DefaultErrorExportType).ToString();

                            //Common.Configuration.ProductConfiguration.SetValue(
                            //       Common.Configuration.ProductComponents.Main,
                            //       SectionName,
                            //       key,
                            //       errorExportType);

                            __currentErrorExportType = Constants.CDC.DefaultErrorExportType;
                        }
                        else
                        {
                            ErrorExportTypes selectedExportType;

                            __currentErrorExportType = Enum.TryParse<ErrorExportTypes>(errorExportType, out selectedExportType)
                                                        ? selectedExportType
                                                        : Constants.CDC.DefaultErrorExportType;
                        }
                    }

                    //  VBotLogger.Trace(() => string.Format("[ProcessDocument] Error export type = {0}...", __currentErrorExportType.Value));
                    return __currentErrorExportType.Value;
                }
            }

            public static bool IsIQBotSettingsVisible
            {
                get
                {
                    return Get("visionbotsettings", "0") == "1";
                }
            }

            private static string SectionName = "visionbot";

            private static string Get(string key, string defaultValue)
            {
                return Configurations.Get(Visionbot.SectionName, key, defaultValue);
            }
        }

        private static string Get(string section, string key, string defaultValue)
        {
            return defaultValue;
        }
    }

    public static class ImageProcessingConfigExtensions
    {
        public static ImageProcessingConfig GetExtendedVersion(this ImageProcessingConfig config)
        {
            config.UseVectorInfoInPdf = Configurations.Visionbot.UseVectorInfoInPdf;
            config.PreserveOriginalDpi = Configurations.Visionbot.PreserveOriginalDpi;
            config.EnableRemoveLines = Configurations.Visionbot.IsLineRemovalEnabled;

            return config;
        }
    }
}