﻿
//namespace Automation.CognitiveData.Converters
//{
//    using System;
//    using System.Collections.Generic;
//    using System.IO;
//    using System.Linq;

//    using Automation.CognitiveData.Generic;

//    public class CsvHelperReaderWriter : ICsvReaderWriter
//    {
//        private string filePath;
//        private string delimiter;

//        public CsvHelperReaderWriter(string filePath, string delimiter)
//        {
//            this.filePath = filePath;
//            this.delimiter = delimiter;
//        }

//        public List<string[]> ReadContents()
//        {

//            TextReader textReader = File.OpenText(this.filePath);
//            CsvParser parser = new CsvParser(textReader);
//            parser.Configuration.Delimiter = this.delimiter;

//            List<string[]> csvContentList = new List<string[]>();

//            while (true)
//            {
//                string[] row = parser.Read();

//                if (row == null)
//                {
//                    break;
//                }

//                csvContentList.Add(row);
//            }

//            textReader.Close();

//            return csvContentList;
//        }

//        public void WriteContents(List<object[]> contentList)
//        {
//            using (TextWriter textWriter = new StreamWriter(this.filePath, true, System.Text.Encoding.UTF8))
//            {
//                CsvWriter csvWriter = new CsvWriter(textWriter);
//                csvWriter.Configuration.Encoding = System.Text.Encoding.UTF8;

//                csvWriter.Configuration.Delimiter = this.delimiter;

//                foreach (var contents in contentList)
//                {
//                    foreach (var field in contents)
//                    {
//                        csvWriter.WriteField(field);
//                    }

//                    csvWriter.NextRecord();
//                }

//                textWriter.Close();
//            }
//        }
//    }
//}
