﻿
namespace Automation.CognitiveData.Converters
{
    using System.Collections.Generic;

    public interface ICsvReaderWriter
    {
        List<string[]> ReadContents();
        void WriteContents(List<object[]> contentList);
    }
}
