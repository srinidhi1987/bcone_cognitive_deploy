﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Analytics
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Automation.CognitiveData.Generic;
    using Automation.Integrator.CognitiveDataAutomation.Analytics;
    using Automation.Integrator.CognitiveDataAutomation.Analytics.Model;
    using Automation.Common;
    using Automation.Common.Configuration;
    using System.Threading;
    using System.Threading.Tasks;

    //public class AnalyticsManager : IAnalyticsManager
    //{
    //    #region Constructor

    //    public AnalyticsManager()
    //    {
    //        _dataPersister = new XmlDataPersister<List<TemplateAnalytics>>(FilePath);
    //        this.InitializeWatcher();
    //    }

    //    public AnalyticsManager(IDataPersister<List<TemplateAnalytics>> dataPersister)
    //    {
    //        if (dataPersister == null) throw new ArgumentNullException("dataPersister");

    //        _dataPersister = dataPersister;
    //        this.InitializeWatcher();
    //    }

    //    #endregion

    //    #region Variables & Properties

    //    private readonly IDataPersister<List<TemplateAnalytics>> _dataPersister;

    //    private List<TemplateAnalytics> _propertyObjectAnalytics;

    //    private List<TemplateAnalytics> Analytics
    //    {
    //        get
    //        {
    //            return _propertyObjectAnalytics
    //                   ?? (_propertyObjectAnalytics = (_dataPersister.Retrieve() ?? new List<TemplateAnalytics>()));
    //        }
    //    }

    //    public static readonly string FileName = "CognitiveDataAnalytics.xml";

    //    private string FolderPath
    //    {
    //        get
    //        {
    //            return CognitiveData.Constants.CDC.ApplicationDataPath;
    //        }
    //    }

    //    private string _propertyObjectFilePath;

    //    public string FilePath
    //    {
    //        get
    //        {
    //            if (string.IsNullOrWhiteSpace(_propertyObjectFilePath)) _propertyObjectFilePath = Path.Combine(this.FolderPath, FileName);

    //            return _propertyObjectFilePath;
    //        }
    //    }

    //    #endregion

    //    #region Public Functionality

    //    public TemplateAnalytics GetTemplateAnalytics(string templateName)
    //    {
    //        return this.Analytics.Find(x => x.Name.Equals(templateName));
    //    }

    //    public void UpdateTemplateRunCounter(string templateName, CounterType counterType)
    //    {
    //        var templateAnalytics = GetTemplateAnalytics(templateName);
    //        if (templateAnalytics == null)
    //        {
    //            templateAnalytics = CreateAnalytics(templateName);
    //            Analytics.Add(templateAnalytics);
    //        }
    //        UpdateCounter(templateAnalytics, counterType);
    //        _dataPersister.Store(Analytics);
    //    }

    //    public void DeleteTemplateAnalytics(string templateName)
    //    {
    //        var visionBotAnalytics = GetTemplateAnalytics(templateName);
    //        if (visionBotAnalytics == null) return;
    //        Analytics.Remove(visionBotAnalytics);
    //        _dataPersister.Store(Analytics);
    //    }

    //    public void UpdateTemplateAnalyticsName(string oldTemplateName, string newTemplateName)
    //    {
    //        var visionBotAnalytics = GetTemplateAnalytics(oldTemplateName);
    //        if (visionBotAnalytics == null) return;

    //        visionBotAnalytics.Name = newTemplateName;
    //        _dataPersister.Store(Analytics);
    //    }

    //    public event EventHandler OnAnalyticsChanged;

    //    #endregion

    //    #region Internal Functionality

    //    private static TemplateAnalytics CreateAnalytics(string templateName)
    //    {
    //        return new TemplateAnalytics { Name = templateName, ID = 0, Fail = 0, Pass = 0 };
    //    }

    //    private static void UpdateCounter(TemplateAnalytics visionBotAnalytics, CounterType counterType)
    //    {
    //        if (visionBotAnalytics == null) return;
    //        switch (counterType)
    //        {
    //            case CounterType.Pass:
    //                visionBotAnalytics.Pass++;
    //                break;

    //            case CounterType.Fail:
    //                visionBotAnalytics.Fail++;
    //                break;

    //            default:
    //                throw new NotImplementedException("UpdateCounter not implemented for counterType " + counterType);
    //        }
    //    }

    //    private FileSystemWatcher _visionBotWatcher;

    //    private void InitializeWatcher()
    //    {
    //        _visionBotWatcher = new FileSystemWatcher();
    //        _visionBotWatcher.Path = this.FolderPath;

    //        _visionBotWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName
    //                                         | NotifyFilters.DirectoryName;

    //        // Only watch text files.
    //        _visionBotWatcher.Filter = FileName;

    //        _visionBotWatcher.Changed += StatisticsDataChanged;
    //        _visionBotWatcher.Created += StatisticsDataChanged;
    //        _visionBotWatcher.Deleted += StatisticsDataChanged;
    //        _visionBotWatcher.Renamed += StatisticsDataChanged;
    //        _visionBotWatcher.EnableRaisingEvents = true;
    //    }

    //    private void StatisticsDataChanged(object sender, EventArgs renamedEventArgs)
    //    {
    //        if (this.OnAnalyticsChanged == null)
    //        {
    //            return;
    //        }

    //        this._visionBotWatcher.EnableRaisingEvents = false;
    //        this.OnAnalyticsChanged(this, null);
    //        this._visionBotWatcher.EnableRaisingEvents = true;
    //    }

    //    #endregion
    //}
}