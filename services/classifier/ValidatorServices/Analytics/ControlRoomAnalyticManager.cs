﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Analytics
{
    using Automation.CognitiveData.Generic;
    //using Automation.Integrator.CognitiveDataAutomation.Analytics;
    //using Automation.Integrator.CognitiveDataAutomation.Analytics.Model;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    //[ObfuscationAttribute(Exclude = true, ApplyToMembers = false)]
    //public class ControlRoomAnalyticManager : IAnalyticsManager
    //{
    //    #region Variables & Properties

    //    private List<TemplateAnalytics> GetAnalytics(IDataPersister<List<TemplateAnalytics>> dataPersister)
    //    {
    //        return dataPersister.Retrieve() ?? new List<TemplateAnalytics>();
    //    }

    //    private const string FileName = "CognitiveDataAnalytics.xml";

    //    private string FolderPath
    //    {
    //        get
    //        {
    //            return new DataPathProvider(
    //                        new DataPathConfigurationProvider(),
    //                        new FileHelper(),
    //                        new FolderHelper())
    //                      .GetPath();
    //        }
    //    }

    //    #endregion Variables & Properties

    //    #region Public Functionality

    //    public TemplateAnalytics GetTemplateAnalytics(string templateName)
    //    {
    //        string filePath = GetFilePath(templateName);
    //        var dataPersister = new XmlDataPersister<List<TemplateAnalytics>>(filePath);
    //        this.InitializeWatcher();
    //        return this.GetAnalytics(dataPersister).Find(x => x.Name.Equals(templateName));
    //    }

    //    public void UpdateTemplateRunCounter(string templateName, CounterType counterType)
    //    {
    //        string filePath = GetFilePath(templateName);
    //        var templateAnalytics = GetTemplateAnalytics(templateName);
    //        var dataPersister = new XmlDataPersister<List<TemplateAnalytics>>(filePath);
    //        if (templateAnalytics == null)
    //        {
    //            templateAnalytics = CreateAnalytics(templateName);
    //        }

    //        UpdateCounter(templateAnalytics, counterType);

    //        List<TemplateAnalytics> templateAnalyticsList = new List<TemplateAnalytics>();
    //        templateAnalyticsList.Add(templateAnalytics);
    //        dataPersister.Store(templateAnalyticsList);
    //    }

    //    public void DeleteTemplateAnalytics(string templateName)
    //    {
    //        /*
    //        var visionBotAnalytics = GetTemplateAnalytics(templateName);
    //        if (visionBotAnalytics == null)
    //        {
    //            return;
    //        }

    //        string filePath = GetFilePath(templateName);
    //        var dataPersister = new XmlDataPersister<List<TemplateAnalytics>>(filePath);

    //        List<TemplateAnalytics> templateAnalyticsList = new List<TemplateAnalytics>();
    //        templateAnalyticsList.Remove(visionBotAnalytics);
    //        dataPersister.Store(templateAnalyticsList);
    //        */
    //    }

    //    public TemplateAnalytics GetUpdateTemplateAnalyticsName(string oldtemplateName, string newtemplateName)
    //    {
    //        var dataPersister = new XmlDataPersister<List<TemplateAnalytics>>(GetFilePath(newtemplateName));
    //        this.InitializeWatcher();
    //        return this.GetAnalytics(dataPersister).Find(x => x.Name.Equals(oldtemplateName));
    //    }

    //    public void UpdateTemplateAnalyticsName(string oldTemplateName, string newTemplateName)
    //    {
    //        var visionBotAnalytics = GetUpdateTemplateAnalyticsName(oldTemplateName, newTemplateName);
    //        if (visionBotAnalytics == null)
    //            return;

    //        var dataPersister = new XmlDataPersister<List<TemplateAnalytics>>(GetFilePath(newTemplateName));
    //        visionBotAnalytics.Name = newTemplateName;
    //        List<TemplateAnalytics> templateAnalyticsList = new List<TemplateAnalytics>();
    //        templateAnalyticsList.Add(visionBotAnalytics);
    //        dataPersister.Store(templateAnalyticsList);
    //    }

    //    public event EventHandler OnAnalyticsChanged;

    //    #endregion Public Functionality

    //    #region Internal Functionality

    //    private string GetFilePath(string templateName)
    //    {
    //        return string.IsNullOrEmpty(this.FolderPath)
    //            ? null
    //            : Path.Combine(this.FolderPath, templateName, FileName);
    //    }

    //    private static TemplateAnalytics CreateAnalytics(string templateName)
    //    {
    //        return new TemplateAnalytics
    //        {
    //            Name = templateName,
    //            ID = 0,
    //            Fail = 0,
    //            Pass = 0
    //        };
    //    }

    //    private static void UpdateCounter(TemplateAnalytics visionBotAnalytics, CounterType counterType)
    //    {
    //        if (visionBotAnalytics == null) return;
    //        switch (counterType)
    //        {
    //            case CounterType.Pass:
    //                visionBotAnalytics.Pass++;
    //                break;

    //            case CounterType.Fail:
    //                visionBotAnalytics.Fail++;
    //                break;

    //            default:
    //                throw new NotImplementedException("UpdateCounter not implemented for counterType " + counterType);
    //        }
    //    }

    //    private FileSystemWatcher _visionBotWatcher;

    //    private void InitializeWatcher()
    //    {
    //        if (_visionBotWatcher != null) return;

    //        _visionBotWatcher = new FileSystemWatcher();
    //        _visionBotWatcher.Path = this.FolderPath;
    //        _visionBotWatcher.IncludeSubdirectories = true;

    //        _visionBotWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

    //        // Only watch text files.
    //        _visionBotWatcher.Filter = FileName;

    //        _visionBotWatcher.Changed += StatisticsDataChanged;
    //        _visionBotWatcher.Created += StatisticsDataChanged;
    //        _visionBotWatcher.Deleted += StatisticsDataChanged;
    //        _visionBotWatcher.Renamed += StatisticsDataChanged;
    //        if (this.FolderPath != null)
    //            _visionBotWatcher.EnableRaisingEvents = true;
    //    }

    //    private void StatisticsDataChanged(object sender, EventArgs renamedEventArgs)
    //    {
    //        if (this.OnAnalyticsChanged == null)
    //        {
    //            return;
    //        }

    //        this._visionBotWatcher.EnableRaisingEvents = false;
    //        this.OnAnalyticsChanged(this, null);
    //        this._visionBotWatcher.EnableRaisingEvents = true;
    //    }

    //    #endregion Internal Functionality
    //}
}