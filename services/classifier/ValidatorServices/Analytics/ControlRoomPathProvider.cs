﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Analytics
{
    using System;
    using System.IO;

    using Automation.CognitiveData.Generic;
    using Automation.Common.Configuration;

    //public class ControlRoomPathProvider : IDataPathProvider
    //{
    //    #region Constructor

    //    public ControlRoomPathProvider(
    //        IDataPathConfigurationProvider configurationProvider,
    //        IFileHelper fileHelper,
    //        IFolderHelper folderHelper)
    //    {
    //        if (configurationProvider == null)
    //        {
    //            throw new ArgumentNullException("configurationProvider");
    //        }

    //        if (fileHelper == null)
    //        {
    //            throw new ArgumentNullException("fileHelper");
    //        }

    //        if (folderHelper == null)
    //        {
    //            throw new ArgumentNullException("folderHelper");
    //        }

    //        this.ConfigurationProvider = configurationProvider;
    //        this.FolderHelper = folderHelper;
    //        this.FileHelper = fileHelper;
    //    }

    //    #endregion

    //    #region Variables & Properties

    //    private readonly IFileHelper FileHelper;

    //    private readonly IFolderHelper FolderHelper;

    //    private readonly IDataPathConfigurationProvider ConfigurationProvider;

    //    #endregion

    //    #region Public Functionality

    //    public string GetPath()
    //    {
    //        // if hostname is available return it and
    //        var controlRoomHostName = ControlRoomHostName;
    //        if (!string.IsNullOrWhiteSpace(controlRoomHostName) &&
    //            this.FolderHelper.DoesExists(ConstructPath(controlRoomHostName)))
    //        {
    //            return ConstructPath(controlRoomHostName);
    //        }

    //        // else if ipaddress is available and it exists return it

    //        var controlRoomIPAddress = ControlRoomIPAddress;
    //        if (!string.IsNullOrWhiteSpace(controlRoomIPAddress) &&
    //            this.FolderHelper.DoesExists(ConstructPath(controlRoomIPAddress)))
    //        {
    //            return ConstructPath(controlRoomIPAddress);
    //        }

    //        // else return null

    //        return null;
    //    }

    //    private string ConstructPath(string rootLocation)
    //    {
    //        return Path.Combine(
    //            rootLocation,
    //            "Automation Anywhere",
    //            Automation.CognitiveData.Constants.CDC.EngineTitle);
    //    }

    //    private string ControlRoomIPAddress
    //    {
    //        get { return @"\\" + ProductConfiguration.GetValue(ProductComponents.Main, @"Options\ServerSettings", "IPAddress", ""); }
    //    }
    //    private string ControlRoomHostName
    //    {
    //        get { return @"\\" + ProductConfiguration.GetValue(ProductComponents.Main, @"Options\ServerSettings", "ServerName", ""); }
    //    }
    //    #endregion
    //}
}