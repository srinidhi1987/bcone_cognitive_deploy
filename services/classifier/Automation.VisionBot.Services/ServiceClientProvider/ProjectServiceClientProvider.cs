﻿

using System;
/**
* Copyright (c) 2017 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
namespace Automation.Services.Client
{
    public class ProjectServiceClientProvider : IServiceClientProvider
    {
        private static HttpClientService _client=null;
        public HttpClientService GetClient()
        {
            if (_client == null)
            {
                HttpServiceModel serviceModel = new ServiceLocationProvider().GetProjectServiceConfiguration();

                _client =new HttpClientService(serviceModel,"", "");
            }

            return _client;
        }
    }
}
