﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client.ServiceClientProvider
{
    public class ValidatorServiceClientProvider
    {
        private static HttpClientService _client = null;

        public HttpClientService GetClient()
        {
            if (_client == null)
            {
                HttpServiceModel serviceModel = new ServiceLocationProvider().GetValidatorServiceConfiguration();
                _client = new HttpClientService(serviceModel,"", "");
            }

            return _client;
        }
    }
}