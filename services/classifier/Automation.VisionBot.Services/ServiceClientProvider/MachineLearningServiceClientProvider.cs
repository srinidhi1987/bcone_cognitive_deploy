﻿using Automation.Services.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Services.Client.ServiceClientProvider
{
    internal class MachineLearningServiceClientProvider : IServiceClientProvider
    {
        private static HttpClientService _client = null;

        public HttpClientService GetClient()
        {
            if (_client == null)
            {
                HttpServiceModel serviceModel = new ServiceLocationProvider().GetMLServiceConfiguration();

                _client = new HttpClientService(serviceModel, "", "");
            }

            return _client;
        }
    }
}