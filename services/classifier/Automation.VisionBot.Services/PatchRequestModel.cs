﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
  public  class PatchRequestModel
    {
        [JsonProperty(PropertyName = "op")]
        public string Operations { get; set; }

        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }

    public enum PatchOperations
    {
        replace
    }
}
