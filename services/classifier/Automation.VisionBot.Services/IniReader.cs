﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Automation.Services.Client
{
    public class IniReader
    {
        private const string PROPERTY_TOKEN = "token";

        private static string token;

        public IniReader(string filename)
        {
            string initFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), filename);

            if (File.Exists(initFilePath))
            {

                string jsonData = File.ReadAllText(initFilePath);

                var inputData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonData);
                if (inputData == null || inputData.Count == 0)
                    return;

                if(inputData.ContainsKey(PROPERTY_TOKEN))  token = inputData[PROPERTY_TOKEN];
            }
        }

        internal static string GetToken()
        {
            return token;
        }

    }
}
