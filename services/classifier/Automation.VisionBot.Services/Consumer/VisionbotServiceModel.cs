﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
    public class HttpServiceModel
    {
        public string Name { get; set; }
        public string ServiceUri { get; set; }

        public int MaximumBufferSize { get; set; }

        public int DefaultConnectionLimit { get; set; }
    }
}