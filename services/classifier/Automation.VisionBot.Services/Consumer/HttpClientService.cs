﻿using Automation.Cognitive.Services.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
    public class HttpClientService
    {
        private const string HEADER_AUTHORIZATION = "Authorization";

        private readonly object instanceLocker = new Object();
        private HttpClient httpClient = null;
        private string baseAddress;
        private bool isGatewayUrl = false;
        private static string ROUTE_PREFIX = "gateway";


        private string baseRestPath = "/rest/api";
        private HttpServiceModel _httpServiceModel = null;

        public HttpClientService(HttpServiceModel visionbotServiceModel, string username, string tocken)
        {
            _httpServiceModel = visionbotServiceModel;
            httpClient = getClient(username, tocken);
        }

        public HttpResponseMessage Get(string subUrl)
        {
            HttpResponseMessage result = null;
            try
            {
                if (isGatewayUrl)
                    subUrl = ROUTE_PREFIX + subUrl;
                var task = httpClient.GetAsync(subUrl);
                result = task.Result;

            }
            catch (AggregateException ae)
            {
                throw new Exception("Unable to communicate with Server.",ae);
            }


            return result;

        }

        public HttpResponseMessage Post(string subUrl, string jsonString)
        {
            HttpResponseMessage result = null;
            try
            {
                if (isGatewayUrl)
                    subUrl = ROUTE_PREFIX + subUrl;
                result = httpClient.PostAsync(subUrl, getHttpContent(jsonString)).Result;
            }
            catch (AggregateException ae)
            {
                throw new Exception("Unable to communicate with Server.",ae);
            }


            return result;
        }

        public HttpResponseMessage Delete(string subUrl)
        {
            HttpResponseMessage result = null;
            try
            {
                if (isGatewayUrl)
                    subUrl = ROUTE_PREFIX + subUrl;
                result = httpClient.DeleteAsync(subUrl).Result;
            }
            catch (AggregateException ae)
            {
                throw new Exception("Unable to communicate with Server.",ae);
            }


            return result;
        }

        private static HttpContent getHttpContent(string jsonString)
        {
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            return content;
        }

        private HttpClient getClient(string userName, string token)
        {
            if (httpClient == null)
            {
                lock (instanceLocker)
                {
                    if (httpClient == null)
                    {
                        if (string.IsNullOrWhiteSpace(RestConfigurationProvider.BaseUrl))
                        {
                            baseAddress = _httpServiceModel.ServiceUri;
                            userName = "Unknown";
                            try
                            {
                                userName = Environment.UserName;
                            }
                            catch (Exception ex)
                            { }
                            token = IniReader.GetToken();
                        }
                        else
                        {
                            baseAddress = RestConfigurationProvider.BaseUrl;
                            userName = RestConfigurationProvider.UserName;
                            token = RestConfigurationProvider.Token;
                            isGatewayUrl = RestConfigurationProvider.IsGatewayUrl;
                        }

                        httpClient = new HttpClient();
                        httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                        httpClient.BaseAddress = new Uri(baseAddress);
                        httpClient.DefaultRequestHeaders.Add("username", userName);
                        if (!string.IsNullOrEmpty(token))
                        {
                            httpClient.DefaultRequestHeaders.Add(HEADER_AUTHORIZATION, token);
                        }
                        httpClient.Timeout = new TimeSpan(0, 10, 00);
                    }
                }
            }
            return httpClient;
        }

        internal HttpResponseMessage Patch(string subUrl, string jsonString)
        {
            HttpResponseMessage result = null;
            var method = new HttpMethod("PATCH");
            if (isGatewayUrl)
                subUrl = ROUTE_PREFIX + subUrl;
            var request = new HttpRequestMessage(method, subUrl)
            {
                Content = getHttpContent(jsonString)
            };
            try
            {
                result = httpClient.SendAsync(request).Result;
            }
            catch (AggregateException ae)
            {
                throw new Exception("Unable to communicate with Server.",ae);
            }


            return result;
        }
    }

    public class VisionbotServiceResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public Error Errors { get; set; }
    }

    public class VisionbotServiceResponse_Old
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public string [] Errors { get; set; }
    }

    public class VisionbotServiceResponse_Old_1
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public string Errors { get; set; }
    }

    public class Error
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}