﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Services.Client
{
    using Automation.VisionBotEngine;
    using Newtonsoft.Json;
    using System;
    using System.Globalization;
    using System.Net.Http;

    public class HttpServiceAdapter
    {
        private HttpClientService _client = null;
        private JsonSerializerSettings _jsonSerializationSettings;

        public HttpServiceAdapter(string serviceUrl, string username, string tockenId)
        {
            HttpServiceModel serviceModel = new HttpServiceModel()
            {
                ServiceUri = serviceUrl /*"http://172.16.16.125:9996"*/
            };

            _client = new HttpClientService(serviceModel, username, tockenId);
            _jsonSerializationSettings = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
        }

        public HttpServiceAdapter(HttpClientService clientService)
        {
            _client = clientService;
        }

        public T Get<T>(string subUrl)
        {
            VBotLogger.Trace(() => $"[HttpServiceAdapter::Get] url: {subUrl}");
            var response = _client.Get(subUrl);

            var jsonString = response.Content.ReadAsStringAsync().Result;

            VBotLogger.Trace(() => $"[HttpServiceAdapter::Get] url {subUrl}");
            VisionbotServiceResponse visionbotServiceResponse;
            visionbotServiceResponse = converJsonResponseToVisionbotServiceResponse(jsonString);

            throwExceptionIfErrorPersist(visionbotServiceResponse);

            if (visionbotServiceResponse.Data == null)
            {
                return default(T);
            }

            if (typeof(T) == typeof(string))
            {
                return (T)Convert.ChangeType(visionbotServiceResponse.Data.ToString(), typeof(T), CultureInfo.InvariantCulture);
            }

            return JsonConvert.DeserializeObject<T>(visionbotServiceResponse.Data.ToString(), _jsonSerializationSettings);
        }

        private static VisionbotServiceResponse converJsonResponseToVisionbotServiceResponse(string jsonString)
        {
            VisionbotServiceResponse visionbotServiceResponse = new VisionbotServiceResponse();
            bool parsingErrorOccured = false;
            try
            {
                visionbotServiceResponse = JsonConvert.DeserializeObject(jsonString, typeof(VisionbotServiceResponse)) as VisionbotServiceResponse;
            }
            catch (Exception ex)
            {
                parsingErrorOccured = true;
            }

            if (parsingErrorOccured)
            {
                parsingErrorOccured = false;
                try
                {
                    VisionbotServiceResponse_Old visionbotServiceResponse_Old = JsonConvert.DeserializeObject(jsonString, typeof(VisionbotServiceResponse_Old)) as VisionbotServiceResponse_Old;
                    visionbotServiceResponse.Data = visionbotServiceResponse_Old.Data;
                    visionbotServiceResponse.Success = visionbotServiceResponse_Old.Success;
                    if (visionbotServiceResponse_Old.Errors != null && visionbotServiceResponse_Old.Errors.Length > 0)
                    {
                        visionbotServiceResponse.Errors = new Error() { Message = visionbotServiceResponse_Old.Errors[0] };
                    }
                }
                catch (Exception ex)
                {
                    parsingErrorOccured = true;
                }
            }

            if (parsingErrorOccured)
            {
                parsingErrorOccured = false;
                try
                {
                    VisionbotServiceResponse_Old_1 visionbotServiceResponse_Old_1 = JsonConvert.DeserializeObject(jsonString, typeof(VisionbotServiceResponse_Old_1)) as VisionbotServiceResponse_Old_1;
                    visionbotServiceResponse.Data = visionbotServiceResponse_Old_1.Data;
                    visionbotServiceResponse.Success = visionbotServiceResponse_Old_1.Success;
                    if (visionbotServiceResponse_Old_1.Errors != null && visionbotServiceResponse_Old_1.Errors.Length > 0)
                    {
                        visionbotServiceResponse.Errors = new Error() { Message = visionbotServiceResponse_Old_1.Errors };
                    }
                }
                catch (Exception ex)
                {
                    parsingErrorOccured = true;
                }
            }

            return visionbotServiceResponse;
        }

        public T Post<T, C>(string subUrl, C content)
        {
            VBotLogger.Trace(() => $"[HttpServiceAdapter::Post] url: {subUrl}");

            HttpResponseMessage response;
            if (content != null)
            {
                var jsonString = JsonConvert.SerializeObject(content);
                response = _client.Post(subUrl, jsonString);
            }
            else
                response = _client.Post(subUrl, "");

            var jsonResultString = response.Content.ReadAsStringAsync().Result;

            VBotLogger.Trace(() => $"[HttpServiceAdapter::Post] url {subUrl}");

            VisionbotServiceResponse visionbotServiceResponse;
            visionbotServiceResponse = converJsonResponseToVisionbotServiceResponse(jsonResultString);

            throwExceptionIfErrorPersist(visionbotServiceResponse);

            if (visionbotServiceResponse == null)
                return default(T);
            if (visionbotServiceResponse.Data == null)
                return default(T);
            else
                return JsonConvert.DeserializeObject<T>(visionbotServiceResponse.Data.ToString(), _jsonSerializationSettings);
        }

        public T Patch<T, C>(string subUrl, C content)
        {
            VBotLogger.Trace(() => $"[HttpServiceAdapter::Patch] url: {subUrl}");
            try
            {
                HttpResponseMessage response;
                if (content != null)
                {
                    var jsonString = JsonConvert.SerializeObject(content);
                    response = _client.Patch(subUrl, jsonString);
                }
                else
                    response = _client.Patch(subUrl, "");

                var jsonResultString = response.Content.ReadAsStringAsync().Result;

                VBotLogger.Trace(() => $"[HttpServiceAdapter::Patch] url {subUrl}");

                VisionbotServiceResponse visionbotServiceResponse;
                visionbotServiceResponse = converJsonResponseToVisionbotServiceResponse(jsonResultString);

                throwExceptionIfErrorPersist(visionbotServiceResponse);

                if (visionbotServiceResponse == null)
                    return default(T);
                if (visionbotServiceResponse.Data == null)
                    return default(T);
                else
                    return JsonConvert.DeserializeObject<T>(visionbotServiceResponse.Data.ToString(), _jsonSerializationSettings);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public T Delete<T>(string subUrl)
        {
            VBotLogger.Trace(() => $"[HttpServiceAdapter::Delete] url: {subUrl}");

            var response = _client.Delete(subUrl);

            var jsonResultString = response.Content.ReadAsStringAsync().Result;

            VisionbotServiceResponse visionbotServiceResponse;
            visionbotServiceResponse = converJsonResponseToVisionbotServiceResponse(jsonResultString);

            throwExceptionIfErrorPersist(visionbotServiceResponse);

            if (visionbotServiceResponse.Data == null)
                return default(T);
            else 
                return JsonConvert.DeserializeObject<T>(visionbotServiceResponse.Data.ToString(), _jsonSerializationSettings);
        }

        private static void throwExceptionIfErrorPersist(VisionbotServiceResponse visionbotServiceResponse)
        {
            if (visionbotServiceResponse != null
                && visionbotServiceResponse.Errors != null)
            {
                throw new WebServiceException(visionbotServiceResponse.Errors);
            }
        }
    }
}