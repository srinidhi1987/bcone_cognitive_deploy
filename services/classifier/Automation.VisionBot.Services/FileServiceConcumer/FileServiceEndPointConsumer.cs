﻿using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using System.Collections.Generic;
using System.IO;

namespace Automation.Services.Client
{
    public class FileServiceEndPointConsumer : IFileServiceEndPointConsumer
    {
        private HttpServiceAdapter _client = null;
        private string _organizationId = string.Empty;
        private string _projectId = string.Empty;

        public FileServiceEndPointConsumer(string organizationId, string projectId)
        {
            _client = new HttpServiceAdapter(
                new FileServiceClientProvider()
                .GetClient());
            _organizationId = organizationId;
            _projectId = projectId;
        }

        public bool DownloadDocuments(string categoryId, string directoryPath)
        {
            if (!File.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
                var response = GetStagingDocumentsDetailsByCategory(categoryId);
                foreach (FileDetails file in response)
                {
                    byte[] data = GetDocument(file.FileId);

                    string fileName = Path.GetFileNameWithoutExtension(file.FileName) + "_" + file.FileId + "." + file.Format;
                    string filePath = Path.Combine(directoryPath, fileName);

                    FileStream stream = new FileStream(filePath, FileMode.Create);
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                }
            }

            return true;
        }

        public FileDetails GetDocumentDetails(string documentId)
        {
            var response = _client.Get<List<VisionBotEngine.Model.FileDetails>>($"/organizations/{_organizationId}/projects/{_projectId}/files/{documentId}");

            return response.Count > 0 ? response[0] : null;
        }

        public byte[] GetDocument(string documentId)
        {
            var servieResponse = _client.Get<List<Dictionary<string, byte[]>>>($"/organizations/{_organizationId}/projects/{_projectId}/fileblob/{documentId}");

            if (servieResponse == null || servieResponse.Count == 0)
            {
                throw new System.Exception($"Failed to retrive file from server with file id {documentId}.");
            }
            Dictionary<string, byte[]> fileData = servieResponse[0];

            if (fileData == null || !fileData.ContainsKey("fileBlob"))
            {
                throw new System.Exception($"Failed to retrive file from server with file id {documentId}.");
            }

            byte[] data = fileData["fileBlob"];

            if (data == null || data.Length == 0)
            {
                throw new System.Exception($"Failed to retrive file from server with file id {documentId}.");
            }

            return data;
        }

        public List<FileDetails> GetStagingDocumentsDetailsByCategory(string categoryId)
        {
            var response = _client.Get<List<VisionBotEngine.Model.FileDetails>>($"/organizations/{_organizationId}/projects/{_projectId}/categories/{categoryId}/files/staging");
            return response;
        }

        public void UpdateDocumentProcessingStatus(string documentId)
        {
            PatchRequestModel patchRequest = new PatchRequestModel();
            patchRequest.Operations = PatchOperations.replace.ToString();
            patchRequest.Path = "processed";
            patchRequest.Value = true.ToString();

            string endPoint = $"/organizations/{_organizationId}/projects/{_projectId}/files/{documentId}";

            var response = _client.Patch<object, List<PatchRequestModel>>(endPoint, new List<PatchRequestModel>() { patchRequest });
        }

        public void UpdateLayoutClassificationStatus(string orgId, string projectId, string documentId, string layoutId)
        {
            string endPoint = $"/organizations/{orgId}/projects/{projectId}/files/{documentId}/categories/{layoutId}";
            _client.Post<object, string>(endPoint, null);
        }

        public void UpdateClassificationReport(string orgId, string projectId, string documentId, string layoutId, string contentId, string report)
        {
            string endPoint = $"/organizations/{orgId}/projects/{projectId}/files/{documentId}/categories/{layoutId}/{contentId}";
            _client.Post<object, string>(endPoint, report);
        }

        public void SaveSegmentedDocument(string documentId, DocumentVectorDetails segDoc)
        {
            string endPoint = $"/segmenteddocument/{documentId}";
            _client.Post<object, DocumentVectorDetails>(endPoint, segDoc);
        }

        public DocumentVectorDetails GetSegmentedDocument(string documentId)
        {
            var response = _client.Get<DocumentVectorDetails>($"/segmenteddocument/{documentId}");

            return response;
        }

        public string DeleteSegmentedDocument(string documentId)
        {
            return _client.Delete<string>($"/organizations/{_organizationId}/files/{documentId }/segmenteddocument");
        }
    }
}