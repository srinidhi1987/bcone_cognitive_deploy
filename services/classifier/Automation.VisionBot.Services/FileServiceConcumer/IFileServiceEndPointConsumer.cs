﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
    public interface IFileServiceEndPointConsumer
    {
        bool DownloadDocuments(string categoryId, string directoryPath);

        byte[] GetDocument(string documentId);

        FileDetails GetDocumentDetails(string documentId);

        List<FileDetails> GetStagingDocumentsDetailsByCategory(string categoryId);

        void UpdateDocumentProcessingStatus(string documentId);

        void UpdateLayoutClassificationStatus(string orgId, string projectId, string documentId, string layoutId);

        void UpdateClassificationReport(string orgId, string projectId, string documentId, string layoutId, string contentId, string report);

        void SaveSegmentedDocument(string documentId, DocumentVectorDetails segDoc);

        DocumentVectorDetails GetSegmentedDocument(string documentId);

        string DeleteSegmentedDocument(string documentId);
    }
}