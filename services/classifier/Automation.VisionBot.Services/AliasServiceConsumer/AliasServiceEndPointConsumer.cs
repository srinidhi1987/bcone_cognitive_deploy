﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;

namespace Automation.Services.Client
{
    public class AliasServiceEndPointConsumer : IAliasServiceEndPointConsumer
    {
        private HttpServiceAdapter _client = null;

        public AliasServiceEndPointConsumer()
        {
            _client = new HttpServiceAdapter(
                new AliasServiceClientProvider()
                .GetClient());
        }

        public ProjectStandardField GetProjectStandardField(string fieldId)
        {
            return _client.Get<ProjectStandardField>($"/fields/{fieldId}");
        }

        public AliasLanguageDetail GetLanguageDetail(string languageId)
        {
            return _client.Get<AliasLanguageDetail>($"/languages/{languageId}");
        }

        public AliasDetailBasedOnProjectTypeAndLanguage GetAliasesBasedOnProjectTypeAndLanguageId(string projectType, string languageId)
        {
            return _client.Get<AliasDetailBasedOnProjectTypeAndLanguage>($"/projecttypes/{projectType}?languageid={languageId}");
        }

        public AliasDetail GetAliasDetailBasedOnFieldId(string fieldId)
        {
            return _client.Get<AliasDetail>($"fields/{fieldId}");
        }

        public AliasDetail GetAliasDetails(string fieldId, string languageId)
        {
            return _client.Get<AliasDetail>($"fields/{fieldId}?languageid={languageId}");
        }
    }
}