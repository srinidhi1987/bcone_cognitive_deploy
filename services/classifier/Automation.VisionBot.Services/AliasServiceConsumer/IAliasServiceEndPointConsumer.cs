﻿using Automation.VisionBotEngine.Model;

namespace Automation.Services.Client
{
    public interface IAliasServiceEndPointConsumer
    {
        ProjectStandardField GetProjectStandardField(string fieldId);

        AliasLanguageDetail GetLanguageDetail(string languageId);

        AliasDetailBasedOnProjectTypeAndLanguage GetAliasesBasedOnProjectTypeAndLanguageId(string projectType, string languageId);

        AliasDetail GetAliasDetailBasedOnFieldId(string fieldId);

        AliasDetail GetAliasDetails(string fieldId, string languageId);
    }
}