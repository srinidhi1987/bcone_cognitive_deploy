﻿using System.Collections.Generic;
using Automation.VisionBotEngine.Model;
using Automation.Cognitive.VisionBotEngine.Model.V2.Project;
namespace Automation.Services.Client
{
    public class ProjectServiceEndPointConsumer : IProjectServiceEndPointConsumer
    {
        private HttpServiceAdapter _client = null;

        public ProjectServiceEndPointConsumer()
        {
            _client = new HttpServiceAdapter(
                new ProjectServiceClientProvider()
                .GetClient());
        }

        public List<ProjectManifest> GetAllProjectsManifest(string organizationID)
        {
            var response = _client.Get<List<VisionBotEngine.Model.ProjectManifest>>($"/organizations/{organizationID}/projects");
            return response;
        }

        public ProjectDetails GetProjectDetails(string organizationID, string projectId)
        {
            var response = _client.Get<VisionBotEngine.Model.ProjectDetails>($"/organizations/{organizationID}/projects/{projectId}");
            return response;
        }

        public ProjectDetails GetProjectMetaData(string organizationID, string projectId)
        {
            var response = _client.Get<VisionBotEngine.Model.ProjectDetails>($"/organizations/{organizationID}/projects/{projectId}/metadata");
            return response;
        }

        public ProjectDetailsV2 GetProjectMetaDataV2(string organizationId, string projectId)
        {
            var response = _client.Get<ProjectDetailsV2>($"v2/organizations/{organizationId}/projects/{projectId}/metadata");
            return response;
        }
    }
}