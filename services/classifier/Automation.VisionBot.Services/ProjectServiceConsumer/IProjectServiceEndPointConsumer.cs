﻿using Automation.VisionBotEngine.Model;
using Automation.Cognitive.VisionBotEngine.Model.V2.Project;
using System.Collections.Generic;

namespace Automation.Services.Client
{
    public interface IProjectServiceEndPointConsumer
    {
        List<ProjectManifest> GetAllProjectsManifest(string organizationID);

        ProjectDetails GetProjectDetails(string organizationID, string projectId);

        ProjectDetails GetProjectMetaData(string organizationID, string projectId);

        ProjectDetailsV2 GetProjectMetaDataV2(string organizationId, string projectId);
    }
}