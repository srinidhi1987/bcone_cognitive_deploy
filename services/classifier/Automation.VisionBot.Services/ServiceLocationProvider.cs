﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Automation.Cognitive.VisionBotEngine.Model;
using Automation.VisionBotEngine;

namespace Automation.Services.Client
{
    public class Tesseract4SettingProvider
    {
        private readonly Tesseract4Settings _tesseract4Configuration;
        private string tesseract4SettingPath = "Tesseract4Settings.json";

        public Tesseract4SettingProvider()
        {
            string path = Path.Combine(
                           Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), tesseract4SettingPath);
            if (File.Exists(path))
            {
                try
                {
                    string json = File.ReadAllText(path);
                    VBotLogger.Trace(() => $"Tesseract4 Configuration File Path: {path}");
                    _tesseract4Configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Tesseract4Settings>(json);
                }
                catch (Exception exc)
                {
                    VBotLogger.Error(() => "Exception occured in reading Tesseract4 Setting File, So taking Default Settings");
                    exc.Log(nameof(Tesseract4SettingProvider), nameof(Tesseract4SettingProvider));
                    _tesseract4Configuration = new Tesseract4Settings();
                }
            }
            else
            {
                _tesseract4Configuration = new Tesseract4Settings();
                VBotLogger.Error(() => $"Tesseract4 Configuration file is not found: {path}, So taking Default Settings");
            }
        }

        public Tesseract4Settings GetTesseract4Settings()
        {
            return _tesseract4Configuration;
        }
    };

    public class Tesseract4Settings
    {
        public bool UseTesseract4 { get; set; }
        public bool UseSegmentation { get; set; }
        public int OcrEngineMode { get; set; }

        public int PageSegmentationMode { get; set; }
        public int NoOfDocsProcessInParallel { get; set; }

        public Tesseract4Settings()
        {
            UseTesseract4 = true;
            UseSegmentation = true;
            OcrEngineMode = 2;
            PageSegmentationMode = 7;
            NoOfDocsProcessInParallel = 2;
        }
    }

    public class ServiceLocationProvider
    {
        private readonly CognitiveServicesConfiguration _cognitiveServicesConfiguration;

        private string servicConfigurationPath = "CognitiveServiceConfiguration.json";

        public ServiceLocationProvider()
        {
            string path = Path.Combine(
                            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);

                _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);
            }
            else
            {
                _cognitiveServicesConfiguration = new CognitiveServicesConfiguration();
                VBotLogger.Error(() => $"Configuration file is not found: {path},  so taking default settings");
            }
            if (_cognitiveServicesConfiguration.ServiceConfiguration.Count == 0)
            {
                _cognitiveServicesConfiguration.ServiceConfiguration = new List<HttpServiceModel>();
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetFileServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetAliasServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetVisionBotServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetProjectServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetValidatorServiceConfiguration());
                _cognitiveServicesConfiguration.ServiceConfiguration.Add(GetMLServiceConfiguration());

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(_cognitiveServicesConfiguration);
                try
                {
                    File.WriteAllText(servicConfigurationPath, json);
                }
                catch (Exception ex)
                { }
            }
        }

        public HttpServiceModel GetVisionBotServiceConfiguration()
        {
            HttpServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "VisionBotService");
            if (serviceModel == null)
            {
                serviceModel = new HttpServiceModel() { Name = "VisionBotService", ServiceUri = "http://localhost:9998" };
            }

            return serviceModel;
        }

        public HttpServiceModel GetFileServiceConfiguration()
        {
            HttpServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "FileService");
            if (serviceModel == null)
            {
                serviceModel = new HttpServiceModel() { Name = "FileService", ServiceUri = "http://localhost:9996" };
            }

            return serviceModel;
        }

        public HttpServiceModel GetProjectServiceConfiguration()
        {
            HttpServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "ProjectService");
            if (serviceModel == null)
            {
                serviceModel = new HttpServiceModel() { Name = "ProjectService", ServiceUri = "http://localhost:9999" };
            }

            return serviceModel;
        }

        public HttpServiceModel GetAliasServiceConfiguration()
        {
            HttpServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "AliasService");
            if (serviceModel == null)
            {
                serviceModel = new HttpServiceModel() { Name = "AliasService", ServiceUri = "http://localhost:9997" };
            }

            return serviceModel;
        }

        public HttpServiceModel GetValidatorServiceConfiguration()
        {
            HttpServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "ValidatorService");
            if (serviceModel == null)
            {
                serviceModel = new HttpServiceModel() { Name = "ValidatorService", ServiceUri = "http://localhost:9995" };
            }

            return serviceModel;
        }

        public HttpServiceModel GetMLServiceConfiguration()
        {
            HttpServiceModel serviceModel = _cognitiveServicesConfiguration.ServiceConfiguration.FirstOrDefault(x => x.Name == "MachineLearningService");
            if (serviceModel == null)
            {
                serviceModel = new HttpServiceModel() { Name = "MachineLearningService", ServiceUri = "http://localhost:9991" };
            }

            return serviceModel;
        }

        public RabbitMQSetting GetRabbitMQSetting()
        {
            return _cognitiveServicesConfiguration.RabbitMQSettings;
        }

        public MLAutoCorrectionSettings GetMLAutoCorrectionSetting()
        {
            return _cognitiveServicesConfiguration.MLAutoCorrectionSettings;
        }

        public ClassifierMQSetting GetClassifierMQSettings()
        {
            return _cognitiveServicesConfiguration.ClassifierMQSettings;
        }

        public ClassifierDBSetting GetClassifierDBSettings()
        {
            return _cognitiveServicesConfiguration.ClassifierdbSettings;
        }
    }

    public class RabbitMQSetting
    {
        public string VirtualHost { get; set; }
        public string Uri { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Port { get; set; }

        public RabbitMQSetting()
        {
            VirtualHost = "test";
            Uri = "localhost";
            UserName = "messagequeue";
            Password = "passmessage";
            Port = "5673";
        }
    }

    public class ClassifierMQSetting
    {
        public string FileUploadedMQName { get; set; }
    }

    public class ClassifierDBSetting
    {
        public string DbConnectionString { get; set; }
    }

    public class MLAutoCorrectionSettings
    {
        private decimal _threshold;

        public bool IsEnable { get; set; }

        /// <summary>
        /// ML gives confidence values in the range of 0 to 1 , so to compare with confidence value we need to convert percentage threshold values inbetween of 0 to 1.
        /// </summary>
        public decimal Threshold
        {
            get { return _threshold / 100; }
        }

        public decimal ThresholdInPercentage
        {
            set
            {
                if (value < 0)
                    value = 0;
                else if (value > 100)
                    value = 100;
                _threshold = value;
            }
        }

        public MLAutoCorrectionSettings()
        {
            IsEnable = true;
            ThresholdInPercentage = 95M;
        }
    }

    public class CognitiveServicesConfiguration
    {
        public List<HttpServiceModel> ServiceConfiguration = new List<HttpServiceModel>();
        public RabbitMQSetting RabbitMQSettings = null;
        public ClassifierDBSetting ClassifierdbSettings = null;
        public ClassifierMQSetting ClassifierMQSettings = null;
        public LoggerSettings LoggerSettings = null;
        public MLAutoCorrectionSettings MLAutoCorrectionSettings = null;

        public CognitiveServicesConfiguration()
        {
            RabbitMQSettings = new RabbitMQSetting();
            ClassifierdbSettings = new ClassifierDBSetting();
            ClassifierMQSettings = new ClassifierMQSetting();
            LoggerSettings = new LoggerSettings();
            MLAutoCorrectionSettings = new MLAutoCorrectionSettings();
        }
    }
}