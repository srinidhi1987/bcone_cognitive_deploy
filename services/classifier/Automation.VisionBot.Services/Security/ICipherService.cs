﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Services.Client.Security
{
    public interface ICipherService
    {
        string Encrypt(string plainText, byte[] iv);

        string decrypt(string cipherText);
    }
}
