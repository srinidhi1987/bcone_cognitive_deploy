﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Automation.Cognitive.Services.Client.Security
{
    internal class CipherService : ICipherService
    {
        private static byte[] signature = new byte[] { 0x7a, 0xc6, 0xf4, 0x30, 0x52, 0xd9, 0x14, 0xfb, 0xb9, 0xb7 };
        private ISecretKeyProvider secretKeyProvider;

        public CipherService(ISecretKeyProvider secretKeyProvider)
        {
            this.secretKeyProvider = secretKeyProvider;
        }

        public string Encrypt(string plainText, byte[] iv)
        {
            return AesEncrypt(plainText, secretKeyProvider.GetKey(), iv);
        }

        public string decrypt(string cipherText)
        {
            return AesDecrypt(cipherText, secretKeyProvider.GetKey());
        }

        private string AesEncrypt(string plainText, byte[] key, byte[] iv, int keysize = 256, int blocksize = 128, CipherMode cipher = CipherMode.CBC, PaddingMode padding = PaddingMode.ISO10126)
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = blocksize;
            aes.KeySize = keysize;
            aes.Mode = cipher;
            aes.Padding = padding;

            if(iv.Length != 16)
            {
                throw new IVException("IV must be 16 bytes long");
            }

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] plainTextWithSignatureBytes = signature.Concat(plainTextBytes).ToArray();

            try
            {
                using (ICryptoTransform encrypt = aes.CreateEncryptor(key, iv))
                {
                    byte[] dest = encrypt.TransformFinalBlock(plainTextWithSignatureBytes, 0, plainTextWithSignatureBytes.Length);
                    byte[] encryptedBytesWithIV = iv.Concat(dest).ToArray();
                    encrypt.Dispose();
                    return Convert.ToBase64String(encryptedBytesWithIV);
                }
            }
            catch(Exception ex)
            {
                throw new SecurityException(ex.Message, ex);
            }
        }

        private string AesDecrypt(string cipherText, byte[] key, int keysize = 256, int blocksize = 128, CipherMode cipher = CipherMode.CBC, PaddingMode padding = PaddingMode.ISO10126)
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = blocksize;
            aes.KeySize = keysize;
            aes.Mode = cipher;
            aes.Padding = padding;

            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            if (cipherTextBytes.Length < 16)
            {
                throw new IVException("IV size is too short");
            }

            byte[] iv = cipherTextBytes.Take(16).ToArray();
            cipherTextBytes = cipherTextBytes.Skip(16).ToArray();

            try
            {
                using (ICryptoTransform decrypt = aes.CreateDecryptor(key, iv))
                {
                    byte[] decryptedBytes = decrypt.TransformFinalBlock(cipherTextBytes, 0, cipherTextBytes.Length);
                    decrypt.Dispose();

                    byte[] decryptedSignature = decryptedBytes.Take(10).ToArray();
                    byte[] decryptedMessage = decryptedBytes.Skip(10).ToArray();

                    if (!decryptedSignature.SequenceEqual(signature))
                    {
                        throw new SignatureMismatchException("Unable to verify signature");
                    }

                    string decodedString = Encoding.UTF8.GetString(decryptedMessage);

                    return decodedString;
                }
            }
            catch(Exception ex)
            {
                throw new SecurityException(ex.Message, ex);
            }
        }
    }
}
