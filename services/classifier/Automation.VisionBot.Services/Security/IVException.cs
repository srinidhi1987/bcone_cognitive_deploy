﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Services.Client.Security
{
    public class IVException : Exception
    {
        public IVException() : base()
        {
        }

        public IVException(string message) : base(message)
        {
        }

        public IVException(string message, Exception e) : base(message, e)
        {
        }
    }
}
