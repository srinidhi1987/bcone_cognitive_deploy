﻿using System.IO;
using System.Text;

namespace Automation.Cognitive.Services.Client.Security
{
    internal class SecretKeyProvider : ISecretKeyProvider
    {
        byte[] key;

        public SecretKeyProvider(string filepath)
        {
            if(!File.Exists(filepath))
            {
                throw new FileNotFoundException($"File - {filepath} - is not found.");
            }

            string keyString;
            using (StreamReader reader = new StreamReader(filepath))
            {
                keyString = reader.ReadToEnd();
                reader.Close();
            }

            key = Encoding.UTF8.GetBytes(keyString);
        }

        public byte[] GetKey()
        {
            return key;
        }
    }
}
