﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Services.Client.Security
{
    interface ISecretKeyProvider
    {
        byte[] GetKey();
    }
}
