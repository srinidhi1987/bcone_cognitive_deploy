﻿using Automation.Services.Client.ServiceClientProvider;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client.ValidatorServiceConsumer
{
    public class ValidatorServiceEndPointConsumer : IValidatorServiceEndPointConsumer
    {
        private HttpServiceAdapter _client = null;
        private ClassifiedFolderManifest _classifiedFolderManifest;

        public ValidatorServiceEndPointConsumer(ClassifiedFolderManifest manifest)
        {
            _client = new HttpServiceAdapter(
               new ValidatorServiceClientProvider()
               .GetClient());
            _classifiedFolderManifest = manifest;
        }

        public void SaveVBotDocument(string visionBotId, string docuementId, string validationStatus, Dictionary<string, object> validatorDataModel)
        {
            var response = _client.Post<Dictionary<string, object>, Dictionary<string, object>>($"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/visionbot/{visionBotId}/files/{docuementId}/validationstatus/{validationStatus}", validatorDataModel);
        }
    }
}