﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client.ValidatorServiceConsumer
{
    public interface IValidatorServiceEndPointConsumer
    {
        void SaveVBotDocument(string visionBotId, string docuementId, string validationStatus, Dictionary<string, object> validatorDataModel);
    }
}