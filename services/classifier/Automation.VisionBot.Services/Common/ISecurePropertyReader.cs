﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client.Common
{
    internal interface ISecurePropertyReader
    {
        Dictionary<string, string> Retrieve();
    }
}
