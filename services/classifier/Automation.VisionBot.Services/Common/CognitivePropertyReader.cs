﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace Automation.Services.Client.Common
{
    internal class CognitivePropertyReader : ISecurePropertyReader
    {
        private string FILENAME_COGNITIVE_PROPERTIES;
        private string _PropertyFileToRead;
        public CognitivePropertyReader(string relativePath,string propertyFileName)
        {
            FILENAME_COGNITIVE_PROPERTIES = propertyFileName;
            _PropertyFileToRead = Path.Combine(relativePath, FILENAME_COGNITIVE_PROPERTIES);
            if (!File.Exists(_PropertyFileToRead))
                throw new FileNotFoundException("Propery file does not exist");
        }
        public Dictionary<string, string> Retrieve()
        {
            return Retrieve(_PropertyFileToRead);
        }

        private Dictionary<string, string> Retrieve(string configFile)
        {

            try
            {
                return _Retrieve(configFile);
            }
            catch (ArgumentException ex)
            {
                string message = "Key already exists: check " + configFile + " for duplicate settings. You may have accidentally commented/uncommented one so it appears twice";
                throw new ArgumentException(message);
            }
        }

        private Dictionary<string, string> _Retrieve(string configFile)
        {
            //NOTE that if we need to use different config files at once this will need to be updated to 
            //check it's the same config file
            var lines = File.ReadAllLines(configFile)
                .Where(line => !line.TrimStart().StartsWith("#"))
                .ToList();

            var temp = new Dictionary<string, string>();
            string n = null;
            string v = null;

            const string nameValueSettingLinePattern = @"^[^=\s]+\s*=\s*[^\s]+";
            for (int i = 0; i < lines.Count; i++)
            {
                string line = lines[i].Replace("\t", "    ");

                if (Regex.IsMatch(line, nameValueSettingLinePattern))
                {
                    var s = line.Split('=');
                    n = s[0].Trim();
                    v = line.Substring(line.IndexOf('=') + 1);

                    //allow for comments on the same line following the value like name=value #this is a comment
                    //2015-10-01: don't allow comments on the same line because it interferes with 
                    //cases like colors.one = #FF0000;
                    //v = Regex.Replace(v, @"\s+#.*$", "");
                }
                else if (n != null && v != null && Regex.IsMatch(line, @"^\s{3,}[^ ]"))//multi-liners must indent at least 3 spaces
                {
                    /*  a multi-line value like:
                     * name=hello there
                     *      how are you today ?
                     */
                    //v += Environment.NewLine + line.Trim(); //preserve line breaks!
                    v += " " + line.Trim(); //do NOT preserve line breaks!
                }

                if ((n != null && v != null) &&
                    (i == lines.Count - 1  //we're at the end
                        || Regex.IsMatch(lines[i + 1], nameValueSettingLinePattern) //or the next line marks a new pair 
                        || String.IsNullOrWhiteSpace(lines[i + 1]) //or the next line is empty 
                    ))
                {
                    if (!temp.ContainsKey(n))
                    {
                        temp.Add(n, v);
                    }
                    else
                    {
                        throw new ArgumentException("Your config file contains multiple entires for " + n);
                    }

                    n = null;
                    v = null;
                }

            }

            var nameValuePairs = new Dictionary<string, string>();

            foreach (var pair in temp)
            {
                string name = pair.Key;
                string value = pair.Value;

                if (value.Contains("$"))
                {
                    //do we have some setting that can resolve this ?
                    foreach (var pair2 in temp)
                    {
                        if (value.Contains("$" + pair2.Key))
                        {
                            value = value.Replace("$" + pair2.Key, pair2.Value);
                        }
                    };
                }

                //if (value.Contains("$"))
                //{
                //    value = value.Replace("$PATH", GetAssemblyDirectory())
                //                 .Replace("$TIMESTAMP", DateTime.Now.ToString("yyyyMMdd"));
                //}

                //cleanup multiline stuff
                value = Regex.Replace(value, "[\n\r\t]", " ");

                nameValuePairs.Add(name, value.Trim());
            }

            
            return nameValuePairs;
        }
    }
}
