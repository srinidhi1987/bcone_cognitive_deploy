﻿using Automation.Cognitive.Services.Client.Security;
using System;
using System.Collections.Generic;
using System.IO;

namespace Automation.Services.Client.Common
{
    public class CognitiveSecurePropertiesProvider
    {
        private static CognitiveSecurePropertiesProvider securePropertyProvider;
        private static string relativePath = "";
        private static string propertyFile = "";
        private static string privateKeyFile = string.Empty;
        private static ICipherService cipherService;
        private static ISecretKeyProvider secretKeyProvider;

        public static void Init(string relativePath, string propertiesFilename, string privateKeyFilename)
        {
            if (string.IsNullOrWhiteSpace(relativePath))
            {
                throw new ArgumentException("RelativePath can not be empty.");
            }
            if (string.IsNullOrWhiteSpace(propertiesFilename))
            {
                throw new ArgumentException("FileName can not be empty.");
            }
            if (!string.IsNullOrWhiteSpace(CognitiveSecurePropertiesProvider.relativePath) && !string.IsNullOrWhiteSpace(propertyFile))
            {
                throw new InvalidOperationException("CognitiveSecurePropertiesProvider can not be reinitialized.");
            }

            CognitiveSecurePropertiesProvider.relativePath = relativePath;
            propertyFile = propertiesFilename;
            privateKeyFile = privateKeyFilename;
            secretKeyProvider = new SecretKeyProvider(Path.Combine(relativePath, privateKeyFilename));
            cipherService = new CipherService(secretKeyProvider);
        }

        public static CognitiveSecurePropertiesProvider Instance()
        {
            if (string.IsNullOrWhiteSpace(relativePath) || string.IsNullOrWhiteSpace(propertyFile) || string.IsNullOrWhiteSpace(privateKeyFile))
            {
                throw new InvalidOperationException("CognitiveSecurePropertiesProvider can not return instance without init mathod called.");
            }
            if (securePropertyProvider == null)
            {
                securePropertyProvider = new CognitiveSecurePropertiesProvider(relativePath, propertyFile);
            }

            return securePropertyProvider;
        }

        private ISecurePropertyReader _SecurePropertyReader;
        private Dictionary<string, string> properties;

        private CognitiveSecurePropertiesProvider(string relativePath, string propertyFileName)
        {
            _SecurePropertyReader = new CognitivePropertyReader(relativePath, propertyFileName);
            properties = _SecurePropertyReader.Retrieve();
        }

        private string messageQueuePassword = "";

        public string GetMessageQueuePassword()
        {
            if (messageQueuePassword == "")
                properties.TryGetValue("rabbitmq.password", out messageQueuePassword);

            if (string.IsNullOrWhiteSpace(messageQueuePassword))
            {
                return messageQueuePassword;
            }

            return cipherService.decrypt(messageQueuePassword);
        }

        private string dbPassword = "";

        public string GetDataBasePassword()
        {
            if (dbPassword == "")
                properties.TryGetValue("database.password", out dbPassword);

            if (string.IsNullOrWhiteSpace(dbPassword))
            {
                return dbPassword;
            }

            return cipherService.decrypt(dbPassword);
        }

        private string certificateKey = "";

        public string GetCertificateKey()
        {
            if (certificateKey == "")
                properties.TryGetValue("certificatekey", out certificateKey);

            if (string.IsNullOrWhiteSpace(certificateKey))
            {
                return certificateKey;
            }

            return cipherService.decrypt(certificateKey);
        }

        private string abbyyLicenseKey = "";

        public string GetAbbyyLicenseKey()
        {
            if (abbyyLicenseKey == "")
                properties.TryGetValue("abbyy.license.key", out abbyyLicenseKey);
           
            return abbyyLicenseKey;
        }
    }
}