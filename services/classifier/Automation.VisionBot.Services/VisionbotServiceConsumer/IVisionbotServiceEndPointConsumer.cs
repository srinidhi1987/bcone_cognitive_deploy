﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
    public interface IVisionbotServiceEndPointConsumer
    {
        bool UnLockVisionBot(string visionbotID);

        VisionBot LoadVisionBotForEdit(ClassifiedFolderManifest classifiedFolderManifest);
        VisionBot LoadVisionBot(ClassifiedFolderManifest classifiedFolderManifest);

        VisionBot LoadVisionBot(VisionBotManifest visionbotManifest);

        VisionBotMetadata GetVisionBotMetadata(ClassifiedFolderManifest classifiedFolderManifest);

        List<VisionBotMetadata> GetVisionBotMetadata(string organizationId);

        bool UpdateVisionBot(
            ClassifiedFolderManifest classifiedFolderManifest,
            VisionBotManifest visionBotManifest,
            VisionBot visionBot);

        bool SaveVisionBot(VisionBot visionBot);

        void GetImage(string documentID, int pageIndex, string imagePath);

        VBotDocument ExtractVBotData(string visionbotID, string LayoutID);

        VBotDocument GenerateAndExtractVBotDocument(string visionbotID, string documentID);

        Field GetValueField(
            string visionbotID,
            FieldLayout fieldLayout,
            FieldDef fieldDef,
            VBotDocument vbotDoc,
            DocumentProperties docProps);

        void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName);

        VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName);

        void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName);

        string ExportData(VBotData vbotData);

        void UpdateProcessedDocumentDetails(string visionbotId
            , string visionbotRunDetailsId
            , string docId
            , Dictionary<string, object> docReportData);
    }
}