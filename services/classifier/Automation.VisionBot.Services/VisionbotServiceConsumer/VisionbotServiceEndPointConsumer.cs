﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Services.Client
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Automation.VisionBotEngine.Model;
    using Newtonsoft.Json;

    public class VisionbotServiceEndPointConsumer : IVisionbotServiceEndPointConsumer
    {
        private HttpServiceAdapter _client = null;

        private ClassifiedFolderManifest _classifiedFolderManifest;

        public VisionbotServiceEndPointConsumer(ClassifiedFolderManifest classifiedFolderManifest)
        {
            _client = new HttpServiceAdapter(
                new VisionBotServiceClientProvider()
                .GetClient());
            _classifiedFolderManifest = classifiedFolderManifest;
        }

        #region Interface Implementor


        public bool UnLockVisionBot(string visionbotID)
        {
            string endPoint = $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}/Unlock";

            var response = _client.Get<string>(endPoint);

            if (response == visionbotID)
                return true;
            else
                return false;
        }

        public VisionBot LoadVisionBotForEdit(ClassifiedFolderManifest classifiedFolderManifest)
        {
            string endPoint = getVisionBotDataAccessUrlForEdit(classifiedFolderManifest);

            var response = _client.Get<Dictionary<string, object>>(endPoint);

            var visionBot = JsonConvert.DeserializeObject<VisionBot>(response["visionBotData"].ToString());
            visionBot.Id = response["id"].ToString();
            makeVisionBotCompatible(visionBot);
            return visionBot;
        }

        public VisionBot LoadVisionBot(ClassifiedFolderManifest classifiedFolderManifest)
        {
            string endPoint = getVisionBotDataAccessUrl(classifiedFolderManifest);

            var response = _client.Get<Dictionary<string, object>>(endPoint);

            var visionBot = JsonConvert.DeserializeObject<VisionBot>(response["visionBotData"].ToString());
            visionBot.Id = response["id"].ToString();
            makeVisionBotCompatible(visionBot);
            return visionBot;
        }

        public VisionBot LoadVisionBot(VisionBotManifest visionbotManifest)
        {
            string endPoint = getVisionBotDataAccessUrl(visionbotManifest);

            var response = _client.Get<Dictionary<string, object>>(endPoint);

            var visionBot = Newtonsoft.Json.JsonConvert.DeserializeObject<VisionBotEngine.Model.VisionBot>(response["visionBotData"].ToString());
            visionBot.Id = response["id"].ToString();
            makeVisionBotCompatible(visionBot);
            return visionBot;
        }

        private void makeVisionBotCompatible(VisionBot visionBot)
        {
            var layouts = visionBot?.Layouts;
            if (layouts != null && layouts.Count > 0)
            {
                foreach (var layout in layouts)
                {
                    var tables = layout?.Tables;
                    if (tables != null && tables.Count > 0)
                    {
                        foreach (var table in tables)
                        {
                            var columns = table?.Columns;
                            if (columns != null && columns.Count > 0)
                            {
                                foreach (var column in columns)
                                {
                                    if (column.ValueBounds == System.Drawing.Rectangle.Empty)
                                    {
                                        column.ValueBounds = new System.Drawing.Rectangle(
                                           0,
                                           0,
                                           column.Bounds.Width
                                           , column.Bounds.Height);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        public VisionBotMetadata GetVisionBotMetadata(ClassifiedFolderManifest classifiedFolderManifest)
        {
            var response = _client.Get<object>($"/organizations/{classifiedFolderManifest.organizationId}/projects/{classifiedFolderManifest.projectId}/categories/{classifiedFolderManifest.id}/visionbotmetadata");

            var visionBotMetadatas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VisionBotEngine.Model.VisionBotMetadata>>(response.ToString());
            if (visionBotMetadatas.Count > 0)
            {
                return visionBotMetadatas[0];
            }
            return null;
        }

        public List<VisionBotMetadata> GetVisionBotMetadata(string organizationId)
        {
            var response = _client.Get<object>($"/organizations/{organizationId}/visionbotmetadata");

            var visionBotMetadatas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VisionBotEngine.Model.VisionBotMetadata>>(response.ToString());
            if (visionBotMetadatas.Count > 0)
            {
                return visionBotMetadatas;
            }
            return null;
        }

        public bool UpdateVisionBot(ClassifiedFolderManifest classifiedFolderManifest, VisionBotManifest visionBotManifest, VisionBot visionBot)
        {
            string endPoint = getUpdateVisionBotDataAccessUrl(classifiedFolderManifest, visionBotManifest.Id);

            var response = _client.Post<object, VisionBotEngine.Model.VisionBot>(endPoint, visionBot);

            return true;
        }

        public void GetImage(string documentID, int pageIndex, string imagePath)
        {
            var response = _client.Get<byte[]>(getImageUrl(documentID, pageIndex));
            if (response != null)
            {
                File.WriteAllBytes(imagePath, response);
            }
        }

        public VBotDocument ExtractVBotData(string visionbotID, string layoutID)
        {
            var response = _client.Get<VBotDocument>($"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}/layout/{layoutID}/vbotdocument");

            return response;
        }

        public VBotDocument GenerateAndExtractVBotDocument(string visionbotID, string documentID)
        {
            var response = _client.Get<VBotDocument>(
                $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}/documents/{documentID}/vbotdocument");

            return response;
        }

        public Field GetValueField(string visionbotID, FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc, DocumentProperties docProps)
        {
            var response = _client.Post<Field, FieldLayout>(getValueFieldUrl(visionbotID, fieldDef, vbotDoc, docProps), fieldLayout);

            return response;
        }

        public void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName)
        {
            var response = _client.Post<object, VBotDocSetEntity>($"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionBotManifest.Id}/layout/{layoutName}/testset", new VBotDocSetEntity(docSet));
        }

        public VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            var response = _client.Get<VBotDocSetEntity>($"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionBotManifest.Id}/layout/{layoutName}/testset");
            if (response == null)
                response = new VBotDocSetEntity(new VBotDocSet());
            return response.GetDocSet();
        }

        public void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            var response = _client.Delete<VBotDocSetEntity>($"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionBotManifest.Id}/layout/{layoutName}/testset");
        }

        public string ExportData(VBotData vbotData)
        {
            return _client.Post<string, VBotData>($"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/vbotdata", vbotData);
        }

        #endregion Interface Implementor

        #region Private Methods

        private string getVisionBotDataAccessUrlForEdit(ClassifiedFolderManifest classifiedFolderManifest)
        {
            return $"/organizations/{classifiedFolderManifest.organizationId}/projects/{classifiedFolderManifest.projectId}/categories/{classifiedFolderManifest.id}/visionbots/Edit";
        }

        private string getVisionBotDataAccessUrl(ClassifiedFolderManifest classifiedFolderManifest)
        {
            return $"/organizations/{classifiedFolderManifest.organizationId}/projects/{classifiedFolderManifest.projectId}/categories/{classifiedFolderManifest.id}/visionbots";
        }

        private string getVisionBotDataAccessUrl(VisionBotManifest visionbotMenifest)
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotMenifest.Id}";
        }

        private string getUpdateVisionBotDataAccessUrl(ClassifiedFolderManifest classifiedFolderManifest, string visionBotId)
        {
            return $"/organizations/{classifiedFolderManifest.organizationId}/projects/{ classifiedFolderManifest.projectId}/categories/{ classifiedFolderManifest.id}/visionbots/{visionBotId}";
        }

        private string getSaveVisionBotDataAccessUrl()
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots";
        }

        private string getImageUrl(string documentID, int pageIndex)
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/documents/{documentID}/pages/{pageIndex}/image";
        }

        private string getValueFieldUrl(string visionbotID, FieldDef fieldDef, VBotDocument vbotDoc, DocumentProperties docProps)
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}/layout/{vbotDoc.Layout.Id}/fielddef/{fieldDef.Id}/documents/{docProps.Id}/valuefield";
        }

        private string getValidationDataAccessUrl(string visionbotID)
        {
            return $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotID}/validationdata";
        }

        public bool SaveVisionBot(VisionBot visionBot)
        {
            string endPoint = getSaveVisionBotDataAccessUrl();

            var response = _client.Post<object, VisionBotEngine.Model.VisionBot>(endPoint, visionBot);

            return true;
        }

        public void UpdateProcessedDocumentDetails(string visionbotId
            , string visionbotRunDetailsId
            , string docId
            , Dictionary<string, object> docReportData)
        {
            string endPoint = $"/organizations/{_classifiedFolderManifest.organizationId}/projects/{_classifiedFolderManifest.projectId}/categories/{_classifiedFolderManifest.id}/visionbots/{visionbotId }/visionbotrundetails/{visionbotRunDetailsId}/documents/{docId}"; ;

            var response = _client.Post<object, Dictionary<string, object>>(endPoint, docReportData);

        }

        #endregion Private Methods
    }

    public class VBotDocSetEntity
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "docItems")]
        public string DocItems { get; set; }

        public VBotDocSetEntity()
        {
        }

        public VBotDocSetEntity(VBotDocSet docset)
        {
            Id = docset.Id;
            Name = docset.Name;
            if (docset.DocItems == null)
            {
                docset.DocItems = new List<VBotDocItem>();
            }
            DocItems = JsonConvert.SerializeObject(docset.DocItems);
        }

        public VBotDocSet GetDocSet()
        {
            var docSet = new VBotDocSet();
            docSet.Id = this.Id;
            docSet.Name = this.Name;
            if (!string.IsNullOrWhiteSpace(this.DocItems))
            {
               var jsonSerializationSettings = new JsonSerializerSettings() { DateParseHandling=DateParseHandling.None };
                docSet.DocItems = JsonConvert.DeserializeObject<List<VBotDocItem>>(this.DocItems, jsonSerializationSettings);
            }
            else
                docSet.DocItems = new List<VBotDocItem>();

            return docSet;
        }
    }
}