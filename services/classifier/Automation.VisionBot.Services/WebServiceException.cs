﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services.Client
{
   public class WebServiceException:Exception
    {
        private Error _error = null;
        
        public Error Error { get { return _error; } }

        public WebServiceException(Error error):base(error?.Message)
        {
            _error = error;
        }
    }
}
