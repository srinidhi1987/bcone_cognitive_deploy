﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Services.Client
{
    public class RestConfigurationProvider
    {
        public static string BaseUrl { get; set; }

        public static string Token { get; set; }

        public static string UserName { get; set; }

        public static bool IsGatewayUrl { get; set; }
    }
}
