﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Services.Client
{
    using Automation.VisionBotEngine.Model;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public interface IMachineLearningServiceEndpointConsumer
    {
        string GetSuggestion(string projectid, string fieldid, string originalvalue);

        bool SaveLearningData(string projectid, string fileid, List<FieldValueLearningDataModel> correctedVBotDocument);

        ObservableCollection<RecommendationModel> GetSuggestionWithConfidence(string projectid, string fieldid, string originalvalue);
        List<ValueBasedTableLayout> GetValueBasedTables(object inputJsonObject);
    }
}