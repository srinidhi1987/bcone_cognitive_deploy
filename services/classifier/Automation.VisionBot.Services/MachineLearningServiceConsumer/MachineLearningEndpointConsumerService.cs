﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Services.Client
{
    using Automation.Cognitive.Services.Client.ServiceClientProvider;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    internal class DataResponseModel
    {
        public bool success;
    }

    public class RecommendationModel
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "confidence")]
        public float Confidence { get; set; }
    }

    public class MachineLearningEndpointConsumerService : IMachineLearningServiceEndpointConsumer
    {
        protected HttpServiceAdapter _httpServiceAdapter = null;

        public MachineLearningEndpointConsumerService()
        {
            _httpServiceAdapter = new HttpServiceAdapter(
               new MachineLearningServiceClientProvider()
               .GetClient());
        }

        public bool SaveLearningData(string projectid, string fileid, List<FieldValueLearningDataModel> LearningDataList)
        {
            VBotLogger.Trace(() => string.Format($"[{nameof(MachineLearningEndpointConsumerService)}->{nameof(SaveLearningData)}]"));
            //string endPoint = "projects/" + projectid + "/visionbot/" + fileid;
            string endPoint = "/value";
            var response = _httpServiceAdapter.Post<DataResponseModel, List<FieldValueLearningDataModel>>(endPoint, LearningDataList);
            if (response == null)
                return true;
            return response.success;
        }

        public string GetSuggestion(string projectid, string fieldid, string originalvalue)
        {
            VBotLogger.Trace(() => string.Format($"[{nameof(MachineLearningEndpointConsumerService)}->{nameof(GetSuggestion)}]"));
            string endPoint = "/predict?fieldid=" + projectid + "_" + fieldid + "&originalvalue=" + originalvalue;
            var suggestion = _httpServiceAdapter.Get<string>(endPoint);
            return suggestion;
        }

        public ObservableCollection<RecommendationModel> GetSuggestionWithConfidence(string projectid, string fieldid, string originalvalue)
        {
            VBotLogger.Trace(() => string.Format($"[{nameof(MachineLearningEndpointConsumerService)}->{nameof(GetSuggestionWithConfidence)}]"));
            string endPoint = "/predict?fieldid=" + projectid + "_" + fieldid + "&originalvalue=" + originalvalue;
            var suggestion = _httpServiceAdapter.Get<List<RecommendationModel>>(endPoint);
            return suggestion != null
                ? new ObservableCollection<RecommendationModel>(suggestion)
                : new ObservableCollection<RecommendationModel>();
        }

        public List<ValueBasedTableLayout> GetValueBasedTables(object inputJsonObject)
        {
            VBotLogger.Trace(() => string.Format($"[{nameof(MachineLearningEndpointConsumerService)}->{nameof(GetValueBasedTables)}]"));
            string endPoint = string.Format($"alt_tables");
            Dictionary<string, List<ValueBasedTableLayout>> tables = new Dictionary<string, List<ValueBasedTableLayout>>();
            tables = _httpServiceAdapter.Post<Dictionary<string, List<ValueBasedTableLayout>>, object>(endPoint, inputJsonObject);
            // var data = System.IO.File.ReadAllText("D:\\sample.json");
            //tables = JsonConvert.DeserializeObject<Dictionary<string, List<SSITable>>>(data);
            if (tables == null)
            {
                VBotLogger.Error(() => string.Format($"[{nameof(MachineLearningEndpointConsumerService)}->{nameof(GetValueBasedTables)}] Not Found any table from Webservice"));

                return new List<ValueBasedTableLayout>();
            }

            return tables["Tables"];
        }
    }
}