﻿using System;
using LightInject;
using Automation.Services.Client;

namespace Automation.CognitiveData.VisionBot
{
    public class AssemblyCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<IProjectServiceEndPointConsumer, ProjectServiceEndPointConsumer>();
        }
    }
}
