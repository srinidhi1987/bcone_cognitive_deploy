﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Designer
{
    internal class DecryptionProvider
    {
        internal static string Decrypt(string encryptedString)
        {
            var bytesToDecrypt = Convert.FromBase64String(encryptedString);

            AsymmetricCipherKeyPair keyPair;

            string filepath = Path.Combine(
                Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), 
                ConfigurationManager.AppSettings["PEM"]);
            
            if(!File.Exists(filepath))
            {
                throw new Exception("pem file not found");
            }

            using (var reader = File.OpenText(filepath))
                keyPair = (AsymmetricCipherKeyPair)new PemReader(reader).ReadObject();

            var decryptEngine = new Pkcs1Encoding(new RsaEngine());
            decryptEngine.Init(false, keyPair.Private);

            return Encoding.UTF8.GetString(decryptEngine.ProcessBlock(bytesToDecrypt, 0, bytesToDecrypt.Length));
        }
    }
}
