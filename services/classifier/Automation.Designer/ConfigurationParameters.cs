﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Designer
{
    internal class ConfigurationParameters
    {
        public string ProjectId { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string OrganizationId { get; set; }
        public string BaseUrl { get; set; }

        public string CategoryId { get; set; }
    }
}
