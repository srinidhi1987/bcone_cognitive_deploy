﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;

namespace Automation.CognitiveData.VisionBot
{
    internal interface ILayoutManager
    {
        IList<Layout> GetAllLayouts();

        Layout GetLayout(string id);

        void AddLayout(Layout layout);

        void UpdateLayout(Layout layout);

        void RemoveLayout(string id);

        event EventHandler LayoutCollectionModified;
    }
}