﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot
{
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IVisionBotManager
    {
      

        void SaveVisionBot(ClassifiedFolderManifest classifiedFolderManifest, VisionBotManifest visionBotManifest, VisionBotEngine.Model.VisionBot visionBot);

        VisionBotEngine.Model.VisionBot LoadVisionBot(VisionBotManifest visionBotManifest);

        VisionBotEngine.Model.VisionBot LoadVisionBot(ClassifiedFolderManifest classifiedFolderManifest);

        IList<VisionBotManifest> GetAllVisionBotManifests(string parentPath);
    }
}