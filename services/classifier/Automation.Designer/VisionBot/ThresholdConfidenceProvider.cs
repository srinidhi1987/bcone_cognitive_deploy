﻿using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot
{
    internal class ThresholdConfidenceProvider
    {
        private int confidenceThresold = -1;
        private bool isInitialized = false;
        ClassifiedFolderManifest _classifiedFolderManifest = null;
        IDesignerServiceEndPoints _designerServiceEndPoint = null;
        public ThresholdConfidenceProvider(ClassifiedFolderManifest classifiedFolderManifest, IDesignerServiceEndPoints designerServiceEndPoint)
        {
            _classifiedFolderManifest = classifiedFolderManifest;
            _designerServiceEndPoint = designerServiceEndPoint;
        }

        public int GetConfidenceThreshold()
        {
            if (isInitialized)
            {
                return confidenceThresold;
            }

            initializeConfidenceThreshold();
            
            return confidenceThresold;
        }

        private void initializeConfidenceThreshold()
        {
            var projectMetadata = _designerServiceEndPoint.GetProjectMetaData(_classifiedFolderManifest.organizationId
                , _classifiedFolderManifest.projectId);
            confidenceThresold = projectMetadata.ConfidenceThreshold;
            isInitialized = true;
        }
    }
}
