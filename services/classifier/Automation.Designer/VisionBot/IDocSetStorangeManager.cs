﻿namespace Automation.CognitiveData.VisionBot
{
    using Automation.CognitiveData.VBotExecutor;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System;

    internal interface IDocSetManager
    {
        void SaveDocSet(VisionBotManifest visionBotManifest, VBotDocSet docSet);

        void SaveDocSet(string VBotFilePath, VBotDocSet docSet);

        VBotDocSet LoadDocSet(VisionBotManifest visionBotManifest);

        VBotDocSet LoadDocSet(string VBotFilePath);
    }

    internal class DocSetManager : IDocSetManager
    {
        IDesignerServiceEndPoints _designerServiceEndPoint;
        public DocSetManager(IDesignerServiceEndPoints designerServiceEndPoint)
        {
            _designerServiceEndPoint = designerServiceEndPoint;
        }

        public void SaveDocSet(VisionBotManifest visionBotManifest, VBotDocSet docSet)
        {
            _designerServiceEndPoint.SaveDocSet(visionBotManifest, docSet);
        }

        public void SaveDocSet(string VBotFilePath, VBotDocSet docSet)
        {
            _designerServiceEndPoint.SaveDocSet(VBotFilePath, docSet);
        }

        public VBotDocSet LoadDocSet(VisionBotManifest visionBotManifest)
        {
            return _designerServiceEndPoint.LoadLayoutDocSet(visionBotManifest);
        }

        public VBotDocSet LoadDocSet(string VBotFilePath)
        {
            return _designerServiceEndPoint.LoadDocSet(VBotFilePath);
        }
    }

    internal interface ITestDocSetManager
    {
        void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName);

        void SaveTestDocSet(VBotDocSet docSet, string VBotFilePath, string layoutName);

        VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName);

        VBotDocSet LoadTestDocSet(string VBotFilePath, string layoutName);

        void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName);
    }

    internal class TestDocSetManager : ITestDocSetManager
    {
        IDesignerServiceEndPoints _designerServiceEndPoint;
        public TestDocSetManager(IDesignerServiceEndPoints designerServiceEndPoint)
        {
            _designerServiceEndPoint = designerServiceEndPoint;
        }

        public void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName)
        {
            _designerServiceEndPoint.SaveTestDocSet(docSet, visionBotManifest, layoutName);
        }

        public void SaveTestDocSet(VBotDocSet docSet, string VBotFilePath, string layoutName)
        {
            _designerServiceEndPoint.SaveTestDocSet(docSet, VBotFilePath, layoutName);
        }

        public VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            return _designerServiceEndPoint.LoadTestDocSet(visionBotManifest, layoutName);
        }

        public VBotDocSet LoadTestDocSet(string VBotFilePath, string layoutName)
        {
            return _designerServiceEndPoint.LoadTestDocSet(VBotFilePath, layoutName);
        }

        public void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            _designerServiceEndPoint.DeleteTestDocSet(visionBotManifest, layoutName);
        }
    }
}