﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class LayoutManager : ILayoutManager
    {
        private Automation.VisionBotEngine.Model.VisionBot visionBot;

        public event EventHandler LayoutCollectionModified;

        public LayoutManager(Automation.VisionBotEngine.Model.VisionBot vBot)
        {
            visionBot = vBot;
        }

        public IList<Layout> GetAllLayouts()
        {
            return visionBot.Layouts;
        }

        public Layout GetLayout(string id)
        {
            var layout = visionBot.Layouts.Find(layoutElement => layoutElement.Id == id);
            if (layout == null)
            {
                throw new ArgumentException("Invalid layout identifier");
            }

            return layout;
        }

        public void AddLayout(Layout layout)
        {
            throwExceptionIfLayoutIdisEmpty(layout);

            if (visionBot.Layouts.Find(layoutElement => layoutElement.Name == layout.Name) != null)
            {
                throw new LayoutException(layout, string.Format("Layout '{0}' already exists.", layout.Name));
            }

            visionBot.Layouts.Add(layout);
            raiseLayoutCollectionModified(EventArgs.Empty);
        }

        private void raiseLayoutCollectionModified(EventArgs e)
        {
            if (LayoutCollectionModified != null)
            {
                LayoutCollectionModified(this, e);
            }
        }

        public void UpdateLayout(Layout updatedLayout)
        {
            throwExceptionIfLayoutIdisEmpty(updatedLayout);

            int layoutIndex = visionBot.Layouts.FindIndex(layoutElement => layoutElement.Id == updatedLayout.Id);

            if (layoutIndex > -1)
            {
                visionBot.Layouts[layoutIndex] = updatedLayout;
                raiseLayoutCollectionModified(EventArgs.Empty);
            }
        }

        public void RemoveLayout(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("Layout Id is not Empty. It should have some value");
            }

            int layoutIndex = visionBot.Layouts.FindIndex(layoutElement => layoutElement.Id == id);

            if (layoutIndex > -1)
            {
                visionBot.Layouts.RemoveAt(layoutIndex);
                raiseLayoutCollectionModified(EventArgs.Empty);
            }
        }

        private void throwExceptionIfLayoutIdisEmpty(Layout layout)
        {
            if (string.IsNullOrWhiteSpace(layout.Name))
            {
                throw new LayoutException(layout, string.Format("Layout name can't be blank.", layout.Id));
            }
        }
    }

    public class LayoutException : Exception
    {
        public Layout Layout { get; private set; }

        //public LayoutException(Layout layout)
        //    : base()
        //{
        //    Layout = layout;
        //}

        public LayoutException(Layout layout, string message)
            : base(message)
        {
            Layout = layout;
        }

        //public LayoutException(Layout layout, string message, Exception innerException)
        //    : base(message, innerException)
        //{
        //    Layout = layout;
        //}
    }
}