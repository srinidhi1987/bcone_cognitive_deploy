/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Threading;
using Automation.Services.Client;

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.Model;
    using Automation.VisionBotEngine.Model;
    using System.IO;
    using System.Reflection;
    using System.Runtime.ExceptionServices;

    public class VisionBotUIManager
    {
        private IqBot _iqBotViewModel;
        private VisionBotMainWindow _wpfWindow;
        private VisionBotEngineCommunicator _vm;
        private BackgroundWorker _worker;
        private LoadingWindow _loadingWindow;
        private bool _workerStarted;

        private IWarningManager __warningManager;

        private IWarningManager WarningManager
        {
            get
            {
                return __warningManager ?? (__warningManager = new WarningManager());
            }
        }

        public VisionBotUIManager(ClassifiedFolderManifest classifiedFolderManifest)
        {
            _iqBotViewModel = new IqBot() { Name = classifiedFolderManifest.name };

            _vm = new VisionBotEngineCommunicator(classifiedFolderManifest);
            initializingLoggingMechanism();
        }

        private void setVBotEngineCommunicator()
        {
            _wpfWindow.SaveRequested += _vm.visionBotDesigner_SaveRequested;
            _wpfWindow.TestRequested += _vm.visionBotDesigner_TestRequested;
            _wpfWindow.PreviewRequested += _vm.visionBotDesigner_PreviewRequested;
            _wpfWindow.LayoutCreated += _vm.visionBotDesigner_LayoutCreated;
            _wpfWindow.TestDocSetModificationRequested += _vm.visionBotDesigner_ModifyTestDocSetItemRequested;
            _wpfWindow.ExecuteTestRequested += _vm.VisionBotDesignerView_ExecuteTestRequested;
            _wpfWindow.LayoutDeleted += _vm.VisionBotDesignerView_LayoutDeleted;
            _wpfWindow.LayoutFieldValueRequested += _vm.VisionBotDesignerView_LayoutFieldValueRequested;
            _wpfWindow.LayoutRescanRequested += _vm.visionBotDesigner_LayoutRescanRequested;
            _wpfWindow.ReArrageDataModelFieldRequested += _vm.visionBotDesigner_ReArrageDataModelFieldRequested;
            _wpfWindow.BenchMarkPanelActionRequested += _vm.VisionBotDesigner_BenchMarkPanelActionRequested;
            _wpfWindow.UpdateTestSetRequested += _vm.VisionBotDesigner_UpdateTestSetRequested;
            _wpfWindow.ExportPreviewDataEventRequested += _vm.visionBotDesigner_ExportPreviewDataRequested;
            _wpfWindow.DocumentsSelectionRequested += _vm.visionBotDesigner_DocumentsSelectionRequested;
            _wpfWindow.DocumentsDownloadRequested += _vm.visionBotDesigner_DocumentsDownloadRequested;
            _wpfWindow.DocumentPageImageRequested += _vm.VisionBotDesignerView_DocumentPageImageRequested;
            _wpfWindow.CloseRequested += _vm.VisionBotDesignerView_CloseRequested;
        }


        public void TrainVisionBot()
        {
            //_iqBotViewModel.Name = "Unknown";
            //_iqBotViewModel.Id = "Unknown";

            ShowModalDialog();
        }

        //public List<ProjectManifest> GetProjectList(string organizationID)
        //{
        //    return DesignerServiceEndPointProvider.GetInstant().GetAllProjectsManifest(organizationID);
        //}

        //public List<VisionBotManifest> GetVisionList(string projectManifestID)
        //{
        //    List<VisionBotManifest> visionBotList = new DesignerServiceEndPoints().GetAllVisionBotsManifest(new ProjectManifest { id = projectManifestID });
        //    return visionBotList;
        //}

        [HandleProcessCorruptedStateExceptions]
        public void ShowModalDialog()
        {
            try
            {
                _wpfWindow = new VisionBotMainWindow();
                setVBotEngineCommunicator();

                const string AAEMainFormType = "AA.Main.frmMain";
                //const string AAEMainFormType = "VBotLauncher.Form1";

                Form mainForm = null;
                foreach (Form form in Application.OpenForms)
                {
                    if (form.GetType().ToString().Equals(AAEMainFormType))
                    {
                        mainForm = form;
                        break;
                    }
                }

                if (mainForm == null)
                {
                    VBotLogger.Error(() =>
                    $"[ShowModalDialog] Main Form Type \"{AAEMainFormType}\" is not found. Now VisionBot Designer will not be assigned owner.");
                }
                else
                {
                    _wpfWindow.Left = mainForm.Left + (mainForm.Width - ((int)_wpfWindow.Width)) / 2;
                    _wpfWindow.Top = mainForm.Top + (mainForm.Height - ((int)_wpfWindow.Height)) / 2;

                    WindowInteropHelper helper = new WindowInteropHelper(_wpfWindow);
                    helper.Owner = mainForm.Handle;
                }

                _worker = new BackgroundWorker();
                _worker.DoWork += worker_DoWork;

                _wpfWindow.SetIqBot(_iqBotViewModel);
                _wpfWindow.ContentRendered += _wpfWindow_ContentRendered;
                _wpfWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(ShowModalDialog));
            }
            finally
            {
                if (_worker != null)
                {
                    _worker.Dispose();
                }
                _worker = null;

                removeBindings(_wpfWindow);

                _wpfWindow = null;
            }
        }

        private void removeBindings(System.Windows.DependencyObject parent)
        {
            for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = System.Windows.Media.VisualTreeHelper.GetChild(parent, i);
                if (child != null)
                {
                    removeBindings(child);
                    System.Windows.Data.BindingOperations.ClearAllBindings(child);
                }
            }
            System.Windows.Data.BindingOperations.ClearAllBindings(parent);
        }

        private void _wpfWindow_ContentRendered(object sender, EventArgs e)
        {
            if (!_workerStarted)
            {
                _workerStarted = true;
                _wpfWindow.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => showProgressWindowAndStartWorker()));
            }
        }

        private void LoadingWindow_ContentRendered(object sender, EventArgs e)
        {
            if (_worker != null)
            {
                _worker.RunWorkerAsync();
            }
        }

        private void showProgressWindowAndStartWorker()
        {
            _loadingWindow = new LoadingWindow();
            _loadingWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            _loadingWindow.ContentRendered += LoadingWindow_ContentRendered;
            _loadingWindow.Owner = _wpfWindow;
            _loadingWindow.ShowDialog();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int initialTabIndex = VisionBotDesigner.BlueprintTabIndex;

                try
                {
                    setIqBotProperties();
                    VBotLogger.Trace(() => "[RibbonTab_OnTabChanged] IQ Bot properties set...");
                    loadBlueprint();
                    VBotLogger.Trace(() => "[RibbonTab_OnTabChanged] Blueprint loaded...");
                    loadLayouts();
                    VBotLogger.Trace(() => "[RibbonTab_OnTabChanged] Layouts loaded...");
                    List<Model.LayoutTestSet> testSets = _vm.GetLayoutTestSet();

                    VBotLogger.Trace(() => "[RibbonTab_OnTabChanged] Test doc set loaded...");

                    this.setTestDocSet(testSets);

                    initialTabIndex = new TabHelper().GetDecidedTabViewIndex(_iqBotViewModel, testSets);
                }
                finally
                {
                    _wpfWindow.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => { _loadingWindow?.Close(); _loadingWindow = null; }));
                }

                selectTabOnUI(initialTabIndex);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(worker_DoWork));
                _wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal,
                  new Action(() =>
                  {
                      _wpfWindow.showErrorMessage(ex.Message);
                      _wpfWindow.Close();
                  }));
            }
        }

        private void selectTabOnUI(int tabIndex)
        {
            _wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => _wpfWindow.SelectTab(tabIndex)));
        }

        private void setTestDocSet(List<LayoutTestSet> testSets)
        {
            try
            {
                if (this._wpfWindow.Dispatcher.CheckAccess())
                {
                    this._wpfWindow.SetTestSet(testSets);
                    VBotLogger.Trace(() => "[worker_DoWork] _wpfWindow.Dispatcher.CheckAccess().");
                }
                else
                {
                    this._wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                              {
                                  this._wpfWindow.SetTestSet(testSets);
                                  VBotLogger.Trace(() => "[worker_DoWork] _wpfWindow.Dispatcher.Invoke.");
                              }));
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(setTestDocSet));
            }
        }

        private void selectFirstLayoutOnUI()
        {
            try
            {
                if (this._wpfWindow.Dispatcher.CheckAccess())
                {
                    selectFirstLayout();
                    VBotLogger.Trace(() => "[RibbonTab_OnTabChanged] First layout set...");
                }
                else
                {
                    this._wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(selectFirstLayout));
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(selectFirstLayoutOnUI));
            }
        }

        private void selectFirstLayout()
        {
            try
            {
                if (_iqBotViewModel.Layouts.Count > 0)
                {
                    _iqBotViewModel.Layouts[0].IsSelected = true;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(selectFirstLayout));
            }
        }

        private void setIqBotProperties()
        {
            var iqBot = _vm.LoadVisionBot();

            if (_wpfWindow.Dispatcher.CheckAccess())
            {
                setIqBotProperties(iqBot);
            }
            else
            {
                _wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => setIqBotProperties(iqBot)));
            }
        }

        private void setIqBotProperties(IqBot iqBot)
        {
            _iqBotViewModel.Name = iqBot.Name;
            _iqBotViewModel.Id = iqBot.Id;
            _iqBotViewModel.Version = iqBot.Version;
            _iqBotViewModel.Settings = iqBot.Settings;
            this.setDataModelIdStructure(iqBot);
        }

        private void setDataModelIdStructure(IqBot iqBot)
        {
            if (_iqBotViewModel.DataModelIdStructure == null)
            {
                _iqBotViewModel.DataModelIdStructure = new BlueprintIdSnap();
            }
            _iqBotViewModel.DataModelIdStructure.FieldId = iqBot.DataModelIdStructure.FieldId;
            _iqBotViewModel.DataModelIdStructure.TableDefId = iqBot.DataModelIdStructure.TableDefId;
            _iqBotViewModel.DataModelIdStructure.ColumnId = iqBot.DataModelIdStructure.ColumnId;
        }

        private void loadBlueprint()
        {
            try
            {
                VBotLogger.Trace(() => "[loadBlueprint] loading blueprint start ... ");
                addFieldDefinitionToBlueprint();
                addTableDefinitionToBlueprint();
                VBotLogger.Trace(() => "[loadBlueprint] loading blueprint end ... ");
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(loadBlueprint));
            }
        }

        private void addTableDefinitionToBlueprint()
        {
            var tableDefinitions = _vm.GetBlueprintTableDefinitions();
            if (_wpfWindow.Dispatcher.CheckAccess())
            {
                addTableDefinitions(tableDefinitions);
            }
            else
            {
                _wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() => { addTableDefinitions(tableDefinitions); }));
            }
        }

        private void addTableDefinitions(IList<TableDef> tableDefinitions)
        {
            foreach (var tableDefinition in tableDefinitions)
            {
                _iqBotViewModel.DataModel.Tables.Add(tableDefinition);
            }
        }

        private void addFieldDefinitionToBlueprint()
        {
            var fieldDefinitions = _vm.GetBlueprintFieldDefinitions();
            if (_wpfWindow.Dispatcher.CheckAccess())
            {
                addFieldDefinitions(fieldDefinitions);
            }
            else
            {
                _wpfWindow.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(() => { addFieldDefinitions(fieldDefinitions); }));
            }
        }

        private void addFieldDefinitions(IList<FieldDef> fieldDefinitions)
        {
            foreach (var fieldDefinition in fieldDefinitions)
            {
                _iqBotViewModel.DataModel.Fields.Add(fieldDefinition);
            }
        }

        private void loadLayouts()
        {
            try
            {
                VBotLogger.Trace(() => "[loadLayouts] Loading layouts start ... ");
                for (int index = 0; index < _vm.LayoutCount; index++)
                {
                    var layout = _vm.GetLayoutAt(index, _iqBotViewModel.DataModel);
                    layout.ArrangeLayoutItems(_iqBotViewModel.DataModel);
                    if (_wpfWindow.Dispatcher.CheckAccess())
                    {
                        FillLayouts(layout);
                    }
                    else
                    {
                        _wpfWindow.Dispatcher.Invoke(
                            DispatcherPriority.Normal,
                            new Action(
                                () =>
                                {
                                    FillLayouts(layout);
                                }));
                    }
                }
                VBotLogger.Trace(() => "[loadBlueprint] loading layouts end ... ");
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotUIManager), nameof(loadLayouts));
            }
        }

        private void FillLayouts(Layout layout)
        {
            this._iqBotViewModel.Layouts.Add(layout);
            WarningManager.UpdateLayoutWarning(layout, _iqBotViewModel.DataModel);
        }

        private static void initializingLoggingMechanism()
        {
            string servicConfigurationPath = "CognitiveServiceConfiguration.json";
            string path = Path.Combine(
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
            string json = File.ReadAllText(path);

            CognitiveServicesConfiguration _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);
            string logDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\Automation Anywhere IQBot Platform\\Logs\\";
            //if (Directory.Exists("D:\\"))
            //{
            //    logDir = "D:\\" + logFolderName;
            //}
            //else
            //{
            //    logDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), logFolderName);
            //}

            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }
            logDir = Path.Combine(logDir, "IQBotDesigner.log");
            VBotLogger.Initlization(logDir, _cognitiveServicesConfiguration.LoggerSettings);
        }
    }
}