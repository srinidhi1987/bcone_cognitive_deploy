﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.ConfidenceScore;
    using Automation.CognitiveData.VBotExecutor;
    using Automation.CognitiveData.VBotTestExecutor;
    using Automation.CognitiveData.VisionBot.UI.Extensions;
    using Automation.CognitiveData.VisionBot.UI.Model;
    using Automation.CognitiveData.VisionBot.UI.ModelConverters;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Cognitive.VisionBotEngine.Model.Generic;
    using Generic;
    using global::VisionBotEngine.Model.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Windows.Media;
    using System.Windows.Threading;
    using UI;
    using VisionBotEngine.Configuration;
    using VisionBotEngine.Validation;
    using VisionBot = Automation.VisionBotEngine.Model.VisionBot;

    internal class VisionBotEngineCommunicator
    {
        private const string EXTENSION_PDF = ".pdf";
        //public string DataStoragePath { get; set; }

        //public string VisionBotName { get; set; }

        public VisionBotManifest VisionBotManifest
        {
            //get
            //{
            //    //return new VisionBotManifest { Name = this.VisionBotName, ParentPath = this.DataStoragePath };
            //}
            get; set;
        }

        private ClassifiedFolderManifest ClassifiedFolderManifest { get; set; }

        private VisionBot _visionBot;
        private VBotDocSet _docSet;

        private List<LayoutTestSet> _testSet;
        private VBotDocSet _testDocSet;

        private List<VBotDocSet> _vBotTestSet;

        private VBotDocument _vBotDocumentForPreview;

        private IDesignerServiceEndPoints _designerServiceEndPoint = null;

        private ThresholdConfidenceProvider _thresholdConfidenceProvider = null;

        public VisionBotEngineCommunicator(ClassifiedFolderManifest classifiedFolderManifest)
        {
            ClassifiedFolderManifest = classifiedFolderManifest;

            _designerServiceEndPoint = new DesignerServiceEndPoints(classifiedFolderManifest);
            _thresholdConfidenceProvider = new ThresholdConfidenceProvider(classifiedFolderManifest, _designerServiceEndPoint);
        }

        public void VisionBotDesigner_ExportTestBenchmarkDataRequested(object sender, TestDocSetEventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                var selectedLayout = _visionBot.Layouts.Find(l => l.Id.Equals(e.LayoutId));

                ITestDataEngine testDataEngine = new TestDataEngine();
                BenchmarkData benchmarkData = testDataEngine.GenerateBenchmarkData(e.UpdatedData);

                VBotDocument vBotDocument = getValidatedVBotData(selectedLayout, benchmarkData.Fields, benchmarkData.TableSet);

                var csvFilePath = getCsvFilePath(e.DocSetItem.Name, Properties.Resources.IQTestDataExportFilenamePostfix);

                _designerServiceEndPoint.ExportData(vBotDocument,
                    new ExportConfiguration
                    {
                        ExportFilePath = csvFilePath,
                        ProcessedDocumnetPath = e.DocSetItem.DocumentName
                    });

                System.Diagnostics.Process.Start(csvFilePath);
            }));
        }

        public void VisionBotDesigner_BenchMarkPanelActionRequested(object sender, TestDocSetEventArgs e)
        {
            switch (e.Action)
            {
                case Operation.Refresh:
                    VisionBotDesigner_RefreshTestRequested(sender, e);
                    break;

                case Operation.Export:
                    VisionBotDesigner_ExportTestBenchmarkDataRequested(sender, e);
                    break;
            }
        }

        public void VisionBotDesigner_RefreshTestRequested(object sender, TestDocSetEventArgs e)
        {
            //VisionBotManifest vBotManifest = new VisionBotManifest()
            //{
            //    Name = this.VisionBotName,
            //    ParentPath = this.DataStoragePath
            //};

            _visionBot = ConvertIQBotToVisionBot(e.IqBot);

            var selectedLayout = _visionBot.Layouts.Find(l => l.Id.Equals(e.LayoutId));

            ITestDocSetManager testDocSetManager = new TestDocSetManager(_designerServiceEndPoint);
            _testDocSet = testDocSetManager.LoadTestDocSet(VisionBotManifest, selectedLayout.Name);
            _testDocSet.Name = selectedLayout.Name;

            var testDocSetItem = _testDocSet.DocItems.Find(d => d.Id.Equals(e.DocSetItem.Id));

            if (testDocSetItem != null)
            {
                // string docPath = testDocSetItem.DocumentProperties.Path;

                // var newFilePath = TestExecution.GetPathBasedOnDocType(testDocSetItem.DocumentProperties.Type, docPath);

                // if (!string.IsNullOrWhiteSpace(newFilePath) && File.Exists(newFilePath))
                {
                    bool isPreProcessed = testDocSetItem.DocumentProperties.Version >= Convert.ToDouble(Constants.CDC.Version_1_0)
                                    ? false
                                    : testDocSetItem.DocumentProperties.Type == DocType.Image && !((_designerServiceEndPoint.GetImagePageCount(testDocSetItem.DocumentProperties.Path) > 1));

                    DocumentProcessor documentProcessor = new DocumentProcessor(_designerServiceEndPoint);
                    VBotEngineSettings engineSettings = documentProcessor.GetEngineSettings(isPreProcessed, false);
                    _visionBot.Settings = _visionBot.Settings.GetExtendedVersion();
                    var vBotDocument = documentProcessor.GetVBotDocument(_visionBot, testDocSetItem.DocumentProperties, engineSettings);

                    if (string.IsNullOrWhiteSpace(vBotDocument.Layout.Name) || string.IsNullOrEmpty(vBotDocument.Layout.VBotTestDocSetId) ||
            (!string.IsNullOrWhiteSpace(vBotDocument.Layout.VBotTestDocSetId) && selectedLayout.VBotTestDocSetId.Equals(vBotDocument.Layout.VBotTestDocSetId)))
                    {
                        VBotLogger.Trace(() => "[ReScan] Layout classification failed");
                        ITestDataEngine testDataEngine = new TestDataEngine();
                        BenchmarkData currentBenchmarkData = testDataEngine.GenerateBenchmarkData(e.UpdatedData);

                        testDataEngine = new TestDataEngine() { VBotDocument = vBotDocument };
                        BenchmarkData newBenchmarkData = testDataEngine.GenerateValidatedBanchMarkData();

                        updateFields(currentBenchmarkData, newBenchmarkData, e.IsKeepCurrentContents);
                        updateTables(currentBenchmarkData, newBenchmarkData, e.IsKeepCurrentContents);

                        BenchmarkData validatedCurrentBenchmarkData = getValidatedBenchmarkData(
                        selectedLayout,
                        currentBenchmarkData.Fields,
                        currentBenchmarkData.TableSet);

                        e.DocSetItem.PreviewFields = validatedCurrentBenchmarkData.Fields;
                        e.DocSetItem.PreviewTableSet = validatedCurrentBenchmarkData.TableSet;
                        e.DocSetItem.IsBenchmarkStructureMismatchFound = areTestAndBlueprintStructuresDifferent(e.DocSetItem, e.IqBot);
                    }
                }
            }
        }

        private bool areTestAndBlueprintStructuresDifferent(DocSetItem docSetItem, IqBot iqBot)
        {
            IStructureVerifier structureVerifier = new StructureVerifier();
            ViewModels.IWarning warning = structureVerifier.Verify(iqBot, docSetItem.PreviewFields, docSetItem.PreviewTableSet);
            return warning.HasWarning;
        }

        private VisionBot ConvertIQBotToVisionBot(IqBot iqBot)
        {
            var visionBot = getVisionBot();

            IVisionBotConverter visionBotConverter = new VisionBotConverter();

            return visionBotConverter.ConvertTo(iqBot, visionBot);
        }

        public void VisionBotDesigner_UpdateTestSetRequested(object sender, UpdateTestSetWarningEventArgs e)
        {
            //TODO : This Code was not called Mukesh Methaniya .Make Sure

            VBotLogger.Trace(() => "[UpdateTestSetRequested] Update Update TestSet Requesting Started");
            // IVBotDocumentStorage docSetItemStorage;
            ITestDocSetManager testDocSetManager = new TestDocSetManager(_designerServiceEndPoint);
            // VisionBotManifest vBotManifest = new VisionBotManifest() { Name = this.VisionBotName, ParentPath = this.DataStoragePath };
            foreach (var benchmarkStructureStatu in e.BenchmarkStructureStatus)
            {
                _testDocSet = testDocSetManager.LoadTestDocSet(VisionBotManifest, benchmarkStructureStatu.Item1);
                // docSetItemStorage = new VBotDocumentStorage(this._testDocSet.DocItems);
                // docSetItemStorage.UpdateTestSetBenchmarkMismatchStatus(benchmarkStructureStatu.Item2, benchmarkStructureStatu.Item3);
                new NewCode().UpdateTestSetBenchmarkMismatchStatus(this._testDocSet.DocItems, benchmarkStructureStatu.Item2, benchmarkStructureStatu.Item3);
                testDocSetManager.SaveTestDocSet(_testDocSet, VisionBotManifest, benchmarkStructureStatu.Item1);
            }
            VBotLogger.Trace(() => "[UpdateTestSetRequested] Update Update TestSet Request Completed");
        }

        private static void updateFields(BenchmarkData validatedCurrentBenchmarkData, BenchmarkData newBenchmarkData, bool KeepCurrentData = true)
        {
            for (int columnIndex = 0; columnIndex < newBenchmarkData.Fields.Columns.Count; columnIndex++)
            {
                var fieldId = newBenchmarkData.Fields.Columns[columnIndex].ColumnName;
                var fieldValue = (ValidatedFieldValue)newBenchmarkData.Fields.Rows[0][columnIndex];

                if (!validatedCurrentBenchmarkData.Fields.Columns.Contains(fieldId))
                {
                    validatedCurrentBenchmarkData.Fields.Columns.Add(fieldId);
                    if (validatedCurrentBenchmarkData.Fields.Rows.Count == 0)
                    {
                        validatedCurrentBenchmarkData.Fields.Rows.Add();
                    }
                    validatedCurrentBenchmarkData.Fields.Rows[0][fieldId] = fieldValue.Value;
                }
                else if (!KeepCurrentData)
                {
                    if (validatedCurrentBenchmarkData.Fields.Rows.Count == 0)
                    {
                        validatedCurrentBenchmarkData.Fields.Rows.Add();
                    }
                    validatedCurrentBenchmarkData.Fields.Rows[0][fieldId] = fieldValue.Value;
                }
            }

            for (int columnIndex = 0; columnIndex < validatedCurrentBenchmarkData.Fields.Columns.Count; columnIndex++)
            {
                var fieldId = validatedCurrentBenchmarkData.Fields.Columns[columnIndex].ColumnName;

                if (!newBenchmarkData.Fields.Columns.Contains(fieldId))
                {
                    validatedCurrentBenchmarkData.Fields.Columns.Remove(fieldId);
                    columnIndex--;
                }
            }

            IList<string> desiredSortOrder = newBenchmarkData.Fields.GetOrderedColumnNames();
            validatedCurrentBenchmarkData.Fields.SetColumnsOrder(desiredSortOrder);
        }

        private static void updateTables(BenchmarkData validatedCurrentBenchmarkData, BenchmarkData newBenchmarkData, bool KeepCurrentData = true)
        {
            updateTableDefinTestData(validatedCurrentBenchmarkData, newBenchmarkData);

            foreach (DataTable table in newBenchmarkData.TableSet.Tables)
            {
                List<string> newColumns = new List<string>();
                updateColumnsInTestData(validatedCurrentBenchmarkData, newBenchmarkData, table, newColumns);

                updateRowsInTestData(validatedCurrentBenchmarkData, newBenchmarkData, KeepCurrentData, table, newColumns);
            }
        }

        private static void updateTableDefinTestData(BenchmarkData validatedCurrentBenchmarkData, BenchmarkData newBenchmarkData)
        {
            for (int tableIndex = 0; tableIndex < newBenchmarkData.TableSet.Tables.Count; tableIndex++)
            {
                var tableId = newBenchmarkData.TableSet.Tables[tableIndex].TableName;

                if (!validatedCurrentBenchmarkData.TableSet.Tables.Contains(tableId))
                {
                    DataTable dataTable = newBenchmarkData.TableSet.Tables[tableId].Copy();
                    validatedCurrentBenchmarkData.TableSet.Tables.Add(dataTable.TableName);
                    validatedCurrentBenchmarkData.TableSet.Tables[dataTable.TableName].ExtendedProperties.Add("TableId", tableId);
                }
            }

            for (int tableIndex = 0; tableIndex < validatedCurrentBenchmarkData.TableSet.Tables.Count; tableIndex++)
            {
                var tableId = validatedCurrentBenchmarkData.TableSet.Tables[tableIndex].TableName;

                if (!newBenchmarkData.TableSet.Tables.Contains(tableId))
                {
                    validatedCurrentBenchmarkData.TableSet.Tables.Remove(tableId);
                    tableIndex--;
                }
            }

            IList<string> desiredSortOrder = newBenchmarkData.TableSet.GetOrderedTableNames();
            validatedCurrentBenchmarkData.TableSet.SetTableOrder(desiredSortOrder);
        }

        private static void updateColumnsInTestData(BenchmarkData validatedCurrentBenchmarkData, BenchmarkData newBenchmarkData,
          DataTable table, List<string> newColumns)
        {
            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
            {
                string columnId = table.Columns[columnIndex].ColumnName;
                if (!validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Columns.Contains(columnId))
                {
                    validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Columns.Add(columnId);
                    newColumns.Add(columnId);
                }
            }

            for (int columnIndex = 0; columnIndex < validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Columns.Count; columnIndex++)
            {
                var columnId = validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Columns[columnIndex].ColumnName;

                if (!newBenchmarkData.TableSet.Tables[table.TableName].Columns.Contains(columnId))
                {
                    validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Columns.Remove(columnId);
                    columnIndex--;
                }
            }

            IList<string> desiredSortOrder = table.GetOrderedColumnNames();
            DataTable tableToReOrder = validatedCurrentBenchmarkData?.TableSet?.Tables != null && validatedCurrentBenchmarkData.TableSet.Tables.Contains(table.TableName)
                ? validatedCurrentBenchmarkData.TableSet.Tables[table.TableName] : null;
            if (tableToReOrder != null)
            {
                tableToReOrder.SetColumnsOrder(desiredSortOrder);
            }
            else
            {
                VBotLogger.Error(() => $"[updateColumnsInTestData] tableToReOrder is null.");
            }
        }

        private static void updateRowsInTestData(BenchmarkData validatedCurrentBenchmarkData, BenchmarkData newBenchmarkData,
            bool KeepCurrentData, DataTable table, List<string> newColumns)
        {
            for (int rowIndex = 0; rowIndex < newBenchmarkData.TableSet.Tables[table.TableName].Rows.Count; rowIndex++)
            {
                bool isAddedNewRow = false;
                if (validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Rows.Count - 1 < rowIndex)
                {
                    isAddedNewRow = true;
                    validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Rows.Add();
                }

                for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                {
                    string columnId = table.Columns[columnIndex].ColumnName;
                    if (isAddedNewRow || !KeepCurrentData || newColumns.Contains(columnId))
                    {
                        var rowValue = (ValidatedFieldValue)newBenchmarkData.TableSet.Tables[table.TableName].Rows[rowIndex][columnIndex];
                        validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Rows[rowIndex][columnId] = rowValue.Value;
                    }
                }
            }

            for (int rowIndex = 0; rowIndex < validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Rows.Count; rowIndex++)
            {
                if (newBenchmarkData.TableSet.Tables[table.TableName].Rows.Count - 1 < rowIndex)
                {
                    validatedCurrentBenchmarkData.TableSet.Tables[table.TableName].Rows.RemoveAt(rowIndex);
                    rowIndex--;
                }
            }
        }

        public void visionBotDesigner_LayoutRescanRequested(object sender, PreviewEventArgs e)
        {
            VBotLogger.Trace(() => "[visionBotDesigner_LayoutRescanRequested] Layout Rescan started.");
            if (e.LayoutToUpdate != null)
            {
                var layout = getConvertedLayout(e.LayoutToUpdate);

                VBotLogger.Trace(() => string.Format("[visionBotDesigner_LayoutRescanRequested] Converted the {0} Layout.", layout.Name));

                _visionBot = ConvertIQBotToVisionBot(e.IqBot);

                VBotLogger.Trace(() => "[visionBotDesigner_LayoutRescanRequested] Converted the IQBot to VisionBot ");

                VBotLogger.Trace(() => string.Format("[visionBotDesigner_LayoutRescanRequested] Find layout for {0} DocSet", layout.VBotDocSetId));
                VisionBotEngine.Model.Layout vBotLayout = _visionBot.Layouts.Find(x => x.VBotDocSetId.Equals(layout.VBotDocSetId));

                double docSetVersion = vBotLayout.DocProperties.Version;
                DocumentProperties docProperties = vBotLayout.DocProperties.Clone();
                if (docProperties != null && docProperties.PageProperties != null) docProperties.PageProperties.Clear();

                if (cleanSegmentedDocumentCache(docProperties.Id))
                {
                    VBotDocument vBotDocument = getVBotDocument(docProperties, _visionBot.Settings.GetExtendedVersion());
                    if (vBotDocument != null)
                    {
                        vBotDocument.DocumentProperties.Version = docSetVersion;
                        vBotLayout.DocProperties = vBotDocument.DocumentProperties;

                        _visionBot.Layouts.Find(x => x.VBotDocSetId.Equals(layout.VBotDocSetId)).SirFields = vBotDocument.SirFields;

                        SaveVisionBot();
                        SaveVBotDocSet(vBotLayout);
                        UpdateLayoutSirFields(e.LayoutToUpdate, layout.VBotDocSetId);
                        VBotLogger.Trace(() => string.Format("[visionBotDesigner_LayoutRescanRequested][Rescan][Success][Document:{0}]", docProperties.Id));
                    }
                }
            }
        }

        private bool cleanSegmentedDocumentCache(string documentId)
        {
            try
            {
                _designerServiceEndPoint.DeleteSegmentedDocument(documentId);
            }
            catch(Exception ex)
            {
                VBotLogger.Error(() => string.Format("[cleanSegmentedDocumentCache][Fail][Document:{0}][Exception:{1}",documentId,ex.ToString()));
                return false;
            }
            VBotLogger.Trace(() => string.Format("[cleanSegmentedDocumentCache][Success][Document:{0}]",documentId));
            return true;
            
        }

        private ProjectDetails _projectDetails = null;
        public void visionBotDesigner_ReArrageDataModelFieldRequested(IqBot iqBot)
        {
            if (_projectDetails == null)
            {
                _projectDetails = _designerServiceEndPoint.GetProjectMetaData(ClassifiedFolderManifest.organizationId, ClassifiedFolderManifest.projectId);
            }
            var standardFieldsId = _projectDetails.fields.standard;

            List<string> fieldsIdsAsPerProject = new List<string>();
            fieldsIdsAsPerProject.AddRange(standardFieldsId);
            fieldsIdsAsPerProject.AddRange(_projectDetails.fields.custom.Select(x => x.id));

            foreach (TableDef table in iqBot.DataModel.Tables)
            {
                bool isSwapped = false;
                do
                {
                    int lastProjectOrder = -1;
                    int lastSwapIndex = 0;
                    isSwapped = false;
                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        FieldDef fieldDef = table.Columns[i];
                        int fieldIndexInProject = fieldsIdsAsPerProject.IndexOf(fieldDef.Id);
                        if (lastProjectOrder == -1)
                        {
                            lastProjectOrder = fieldIndexInProject;
                            continue;
                        }
                        if (lastProjectOrder > fieldIndexInProject)
                        {
                            table.Columns.Move(i, i - 1);
                            isSwapped = true;
                        }
                        lastProjectOrder = fieldIndexInProject;
                    }

                } while (isSwapped);
            }
        }

        private VisionBot getVisionBot()
        {
            //VisionBotManifest vBotManifest = new VisionBotManifest()
            //{
            //    Name = this.VisionBotName,
            //    ParentPath = this.DataStoragePath
            //};

            var visionBot = getVisionBot(VisionBotManifest);

            return visionBot;
        }

        private void SaveVBotDocSet(VisionBotEngine.Model.Layout vBotLayout)
        {
            IVisionBotManager visionBotManager = new VisionBotManager(_designerServiceEndPoint);

            if (File.Exists(this.VisionBotManifest.GetFilePath()))
            {
                //var vBotManifest = new VisionBotManifest()
                //{
                //    Name = this.VisionBotName,
                //    ParentPath = this.DataStoragePath
                //};

                var docSetStorage = new DocSetManager(_designerServiceEndPoint);
                _docSet = docSetStorage.LoadDocSet(VisionBotManifest);

                VBotLogger.Trace(() => string.Format("[SaveVBotDocSet] SaveVBotDocSet docSet is {0}", _docSet.Name));

                UpdateDocSetProperties(_docSet, vBotLayout);
                docSetStorage.SaveDocSet(VisionBotManifest, _docSet);
            }
        }

        private void UpdateDocSetProperties(VBotDocSet docSet, VisionBotEngine.Model.Layout vBotLayout)
        {
            VBotDocItem vbDocItem = docSet.DocItems.Find(doc => doc.Id.Equals(vBotLayout.VBotDocSetId));
            if (vbDocItem != null)
            {
                vbDocItem.DocumentProperties = vBotLayout.DocProperties;
            }

            VBotLogger.Trace(() => "[UpdateDocSetProperties] Layout properties updated successfully");
        }

        private void UpdateLayoutSirFields(Layout layoutToUpdate, string docSetId)
        {
            _visionBot = loadVisionBot();
            var layout = _visionBot.Layouts.FirstOrDefault(l => l.VBotDocSetId.Equals(docSetId));
            if (layout != null)
            {
                new LayoutConverter().ConvertSIRFields(layout, layoutToUpdate);
            }
        }

        private VBotDocument getVBotDocument(DocumentProperties docProperties, ImageProcessingConfig settings)
        {
            VBotDocument vBotDocument = null;

            //string filePath = docProperties.Path + ".pdf";
            //string originalPath = docProperties.Path;
            //if (File.Exists(docProperties.Path) && docProperties.Type == DocType.Pdf)
            //{
            //    File.Copy(docProperties.Path, filePath);
            //    docProperties = docProperties.Clone();
            //    docProperties.Path = filePath;
            //}
            //  IVBotEngine vBotEngine = new VBotEngine(Configurations.Visionbot.OcrEngineType);
            //  var vbotValidationDataManager = new VBotValidatorDataManager(VisionBotManifest.GetFilePath());
            bool isPreProcessed = docProperties.Version > 0
                                    ? false
                                    : docProperties.Type == DocType.Image && !((_designerServiceEndPoint.GetImagePageCount(docProperties.Path) > 1));

            DocumentProcessor docProcessor = new DocumentProcessor(_designerServiceEndPoint);
            VBotEngineSettings engineSettings = docProcessor.GetEngineSettings(isPreProcessed, false);

            //  var vBotDocument = vBotEngine.GetVBotDocument(this._visionBot, docProperties, vbotValidationDataManager, engineSettings, settings.GetExtendedVersion());
            vBotDocument = docProcessor.GetVBotDocument(this._visionBot, docProperties, engineSettings);
            //  if (File.Exists(filePath))
            //      File.Delete(filePath);
            //  vBotDocument.DocumentProperties.Path = originalPath;

            return vBotDocument;
        }

        public void visionBotDesigner_PreviewRequested(object sender, PreviewEventArgs e)
        {
            VBotLogger.Trace(() => "[visionBotDesigner_PreviewRequested] Preview started.");
            if (e.LayoutToUpdate != null)
            {
                var layout = getConvertedLayout(e.LayoutToUpdate);

                VBotLogger.Trace(() => string.Format("[visionBotDesigner_PreviewRequested] Converted the {0} Layout.", layout.Name));

                _visionBot = ConvertIQBotToVisionBot(e.IqBot);

                VBotLogger.Trace(() => "[visionBotDesigner_PreviewRequested] Converted the IQBot to VisionBot ");

                VBotLogger.Trace(() => string.Format("[visionBotDesigner_PreviewRequested] Find layout for {0} DocSet", layout.VBotDocSetId));
                VisionBotEngine.Model.Layout vBotLayout = _visionBot.Layouts.Find(x => x.VBotDocSetId.Equals(layout.VBotDocSetId));

                VBotDocument vBotDocument = new VBotDocument() { SirFields = vBotLayout.SirFields, DocumentProperties = vBotLayout.DocProperties };
                vBotDocument.Layout.Id = vBotLayout.Id;
                vBotDocument.Layout.DocProperties = vBotLayout.DocProperties;

                VBotLogger.Trace(() => "[visionBotDesigner_PreviewRequested] Extract visionBot data.");
                ExtractVBotData(_visionBot, ref vBotDocument);

                if (vBotDocument.Layout.Id == null || !vBotDocument.Layout.Id.Equals(layout.Id))
                    e.IsLayoutClassifiedSuccessFully = false;

                _vBotDocumentForPreview = vBotDocument;

                VBotLogger.Trace(() => "[visionBotDesigner_PreviewRequested] Generate preview for Layout.");
                e.BenchmarkData = GeneratePreviewForLayout(layout, vBotDocument);
            }
        }

        public void visionBotDesigner_ExportPreviewDataRequested(object sender, PreviewEventArgs e)
        {
            var csvFilePath = getCsvFilePath(e.LayoutToUpdate.Name, Properties.Resources.TrainDataExportFilenamePostfix);
            _designerServiceEndPoint.ExportData(_vBotDocumentForPreview,
                new ExportConfiguration
                {
                    ExportFilePath = csvFilePath,
                    ProcessedDocumnetPath = e.LayoutToUpdate.DocumentName
                });
            System.Diagnostics.Process.Start(csvFilePath);
        }

        public void visionBotDesigner_DocumentsDownloadRequested(object sender, EventArgs ex)
        {
            string documentsLocalPath = getSampleDocumentLoacalPath();

            if (!Directory.Exists(documentsLocalPath))
            {
                if (!_designerServiceEndPoint.DownloadDocuments(ClassifiedFolderManifest.id, documentsLocalPath))
                {
                    //showErrorMessage(string.Format("Error while fetching documents"));
                    return;
                }
            }
        }

        private string getSampleDocumentLoacalPath()
        {
            return Path.Combine(
                Path.GetTempPath(),
                Properties.Resources.VisionBotTempDirectory,
                _visionBot.Id, 
                Properties.Resources.SampleDocumentTempDirectory);
        }

        public void visionBotDesigner_DocumentsSelectionRequested(object sender, DocumentsSelectionEventArgs<DocumentProperties> e)
        {
            string documentsLocalPath = getSampleDocumentLoacalPath();

            try
            {
                var dlg = new Microsoft.Win32.OpenFileDialog
                {
                    CheckFileExists = true,
                    Multiselect = false,
                    InitialDirectory = documentsLocalPath,
                    Filter = FileSelectionDialog.Filter
                };
                IEnumerable<string> filePaths = Enumerable.Empty<string>();
                do
                {
                    filePaths = (true == dlg.ShowDialog()) ?
                                                       (new string[] { dlg.FileName }).AsEnumerable<string>()
                                                       : Enumerable.Empty<string>();
                } while (!validateSelectedFile(filePaths) && showErrorMessage(string.Format("Only Select appropriate file.")));

                e.SelectedDocuments = converFilePathsToDocumentProperties(filePaths);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(visionBotDesigner_PreviewRequested));
            }
        }

        private IEnumerable<DocumentProperties> converFilePathsToDocumentProperties(IEnumerable<string> filePaths)
        {
            List<DocumentProperties> documents = new List<DocumentProperties>();

            foreach (string file in filePaths)
            {
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file);
                string[] fileNameParts = fileNameWithoutExtension.Split('_');
                if (fileNameParts.Length < 2)
                {
                    continue;
                }

                string fileId = fileNameParts[fileNameParts.Length - 1];
                string fileName = string.Join("_", fileNameParts.Take(fileNameParts.Length - 1));
                string fileExtension = Path.GetExtension(file);

                DocumentProperties docProperties = new DocumentProperties()
                {
                    Id = fileId,
                    Name = fileName + fileExtension,
                    Path = file
                };
                documents.Add(docProperties);
            }

            return documents.AsEnumerable();
        }

        private bool validateSelectedFile(IEnumerable<string> SelectedDocuments)
        {
            foreach (string file in SelectedDocuments)
            {
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file);
                string[] fileNameParts = fileNameWithoutExtension.Split('_');
                if (fileNameParts.Length < 2)
                {
                    return false;
                }
                string fileId = fileNameParts[fileNameParts.Length - 1];

                try
                {
                    var fileDetails = _designerServiceEndPoint.GetFileDetails(fileId);
                    if (!(fileDetails != null && fileDetails.ClassificationId == ClassifiedFolderManifest.id))
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return true;
        }

        public void VisionBotDesignerView_DocumentPageImageRequested(object sender, VisionBotEngine.Model.DocumentProperties documentProperties, int pageIndex, string imagePath, VisionBotEngine.Model.ImageProcessingConfig settings)
        {
            _designerServiceEndPoint.SaveBitmapToImagePath(documentProperties, pageIndex, imagePath, settings);
        }

        public void VisionBotDesignerView_CloseRequested(object sender, EventArgs e)
        {
            if (_visionBot != null && !string.IsNullOrWhiteSpace(_visionBot.Id))
            {
                _designerServiceEndPoint.UnLockVisionBot(new VisionBotManifest() { Id = _visionBot.Id });
            }
        }

        public bool showErrorMessage(string errorMessage)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = errorMessage,
                    Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "OK",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                        }
                    },
                }
            };

            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        private IFolderHelper __folderHelper;

        private IFolderHelper FolderHelper
        {
            get
            {
                return __folderHelper ?? (__folderHelper = new FolderHelper());
            }
        }

        private string getCsvFilePath(string layoutName, string fileNamePostFix)
        {
            var vBotTempPath = Path.Combine(Path.GetTempPath(), Properties.Resources.VisionBotTempDirectory);
            FolderHelper.Create(vBotTempPath);

            string csvFilePathWithoutExtension = Path.Combine(vBotTempPath, layoutName);
            const string CsvExtension = ".csv";

            string csvFilePath = csvFilePathWithoutExtension + fileNamePostFix + CsvExtension;

            if (!File.Exists(csvFilePath))
            {
                return csvFilePath;
            }

            int i = 0;
            do
            {
                i++;
                csvFilePath = csvFilePathWithoutExtension + fileNamePostFix + "(" + i + ")" + CsvExtension;
            } while (File.Exists(csvFilePath));

            return csvFilePath;
        }

        public Field GetValueField(Layout layout, LayoutField layoutField, IqBot iqBot)
        {
            Field field = null;
            VBotLogger.Trace(() => "[GetValueField] GetValueField Started");
            VisionBot visionBot = ConvertIQBotToVisionBot(iqBot);
            VBotLogger.Trace(() => "[GetValueField] Converted the IQBot to VisionBot ");

            VisionBotEngine.Model.Layout vBotLayout = visionBot.Layouts.Find(x => x.Name.Equals(layout.Name));

            FieldLayout fieldLayout = new LayoutFieldConverter().ConvertTo(layoutField);
            Automation.VisionBotEngine.Model.FieldDef fieldDef = getFieldDef(fieldLayout.FieldId, visionBot);
            VBotDocument vBotDocument = new VBotDocument() { SirFields = vBotLayout.SirFields, DocumentProperties = vBotLayout.DocProperties };
            vBotDocument.Layout.DocProperties = vBotLayout.DocProperties;
            vBotDocument.Layout.Id = vBotLayout.Id;

            field = _designerServiceEndPoint.GetValueField(iqBot.Id, fieldLayout, fieldDef, vBotDocument, layout.DocumentProperties.Clone(),
                new DocumentProcessor(_designerServiceEndPoint).GetEngineSettings(false, false), iqBot.Settings.Clone());
            if (field != null && string.IsNullOrWhiteSpace(field.Text))
            {
                fieldLayout.Label = string.Empty;
                //field = vBotEngine.GetValueField(fieldLayout, fieldDef, vBotDocument);
                field = _designerServiceEndPoint.GetValueField(iqBot.Id, fieldLayout, fieldDef, vBotDocument, layout.DocumentProperties.Clone(),
                    new DocumentProcessor(_designerServiceEndPoint).GetEngineSettings(false, false), iqBot.Settings.Clone());
            }

            return field;
        }

        private Automation.VisionBotEngine.Model.FieldDef getFieldDef(string fieldDefID, VisionBot visionBot)
        {
            Automation.VisionBotEngine.Model.FieldDef fieldDef = null;
            if (visionBot != null && visionBot.DataModel != null && visionBot.DataModel.Fields != null)
            {
                fieldDef = (from q in visionBot.DataModel.Fields where q.Id != null && q.Id == fieldDefID select q).FirstOrDefault();
            }

            if (fieldDef == null)
            {
                foreach (var tableDef in visionBot.DataModel.Tables)
                {
                    fieldDef = (from q in tableDef.Columns where q.Id != null && q.Id == fieldDefID select q).FirstOrDefault();
                    if (fieldDef != null)
                        break;
                }
            }
            return fieldDef;
        }

        private BenchmarkData GeneratePreviewForLayout(VisionBotEngine.Model.Layout layout, VBotDocument vBotDocument)
        {
            //   var validationDataManager = new VBotValidatorDataManager(this.VisionBotManifest.GetFilePath());
            //   vBotDocument.Data.EvaluateAndFillValidationDetails(layout, validationDataManager);
            BenchmarkData benchMarkData = null;

            ValidatorAdvanceInfo validatorAdvanceInfo = getValidatorAdvanceInfo(_visionBot);

            _designerServiceEndPoint.EvaluateAndFillValidationDetails(vBotDocument
                , layout
                , this.VisionBotManifest
                , validatorAdvanceInfo);
            VBotLogger.Trace(() => "[GeneratePreviewForLayout] Fill validation data Successfull");

            ITestDataEngine previewEngine = new TestDataEngine() { VBotDocument = vBotDocument };
            benchMarkData = previewEngine.GenerateValidatedBanchMarkData();

            return benchMarkData;
        }

        private ValidatorAdvanceInfo getValidatorAdvanceInfo(VisionBot visionBot)
        {
            return new ValidatorAdvanceInfo()
            {
                CultureInfo = Cognitive.VisionBotEngine.Model.Generic.CultureHelper.GetCultureInfo(visionBot.Language),
                ConfidenceThreshold = _thresholdConfidenceProvider.GetConfidenceThreshold()
            };
        }

        public void visionBotDesigner_LayoutCreated(object sender, FieldTree.LayoutCreationEventArgs e)
        {
            VBotLogger.Trace(() => "[visionBotDesigner_LayoutCreated] Layout creation started...");
            var layout = getConvertedLayout(e.NewLayout);

            if (_docSet.Name == null)
            {
                _docSet.Name = e.NewLayout.DocumentName;
            }

            string filePath = e.NewLayout.DocumentPath;
            VBotLogger.Trace(() => string.Format("[visionBotDesigner_LayoutCreated] Layout Document file path {0}", filePath));

            var docItem = CreateDocItem(e.NewLayout.DocumentProperties);
            _docSet.DocItems.Add(docItem);
            VBotLogger.Trace(() => "[visionBotDesigner_LayoutCreated] Doc Set Item added successfully");

            var vBotDocument = this.getVBotDocument(_visionBot, docItem.DocumentProperties, false, true);

            UpdateDocItemProperties(docItem, vBotDocument);

            if (!string.IsNullOrEmpty(vBotDocument.Layout.Name))
            {
                e.LayoutCreationOperation = duplicateLayoutReporter(sender, vBotDocument.Layout);

                if (e.LayoutCreationOperation == LayoutCreationOperation.Edit)
                {
                    e.NewLayout.Name = vBotDocument.Layout.Name;
                }
                return;
            }

            layout.DocProperties = docItem.DocumentProperties;

            this.setVisionBotLayout(layout, docItem, vBotDocument);
            e.NewLayout.DocumentPath = docItem.DocumentProperties.Path;
            e.NewLayout.DocumentProperties = layout.DocProperties;

            LayoutConverter layoutConverter1 = new LayoutConverter();
            layoutConverter1.ConvertSIRFields(layout, e.NewLayout);

            IDocSetManager docSetStorageManager = new DocSetManager(_designerServiceEndPoint);
            docSetStorageManager.SaveDocSet(this.VisionBotManifest, _docSet);
            SaveVisionBot();

            VBotLogger.Trace(() => "[visionBotDesigner_LayoutCreated] Layout created successfully.");
        }

        private void handleExceptionWithMessage(Action action, [CallerMemberName]string memberName = "")
        {
            //try
            //{
            action.Invoke();
            //}
            //catch (Exception ex)
            //{
            //    ex.Log(nameof(VisionBotEngineCommunicator), memberName, ex.ToString());
            //    //showErrorMessage(ex.Message);
            //}
        }

        private static void UpdateDocItemProperties(VBotDocItem docItem, VBotDocument vBotDocument)
        {
            //ToDo : Need to rethink of assigning the data to
            double version = docItem.DocumentProperties.Version;
            string documentName = docItem.DocumentProperties.Name;
            string documentId = docItem.DocumentProperties.Id;
            vBotDocument.DocumentProperties.Path = docItem.DocumentProperties.Path;
            docItem.DocumentProperties = vBotDocument.DocumentProperties;
            docItem.DocumentProperties.Version = version;
            docItem.DocumentProperties.Name = documentName;
            docItem.DocumentProperties.Id = documentId;
            docItem.SirFields = vBotDocument.SirFields;
        }

        private VBotDocItem CreateDocItem(DocumentProperties docProperties)
        {
            var docItem = new VBotDocItem()
            {
                DocumentProperties = new DocumentProperties() { Id = docProperties.Id },
                DocumentName = docProperties.Name
            };

            // docItem.DocumentProperties.Id = Path.GetFileNameWithoutExtension(filePath);
            docItem.DocumentProperties.Name = docProperties.Name;

            string path = Path.Combine(
                                        Path.GetTempPath(),
                                        Properties.Resources.VisionBotTempDirectory,
                                        _visionBot.Id);

            string docItemPath = Path.Combine(path, docProperties.Id);
            if (!Directory.Exists(docItemPath))
                Directory.CreateDirectory(docItemPath);

            docItem.DocumentProperties.Path = Path.Combine(docItemPath, docItem.Id); //getImagePath(docItem);
            docItem.DocumentProperties.Version = Convert.ToDouble(Properties.Resources.CurrentVersion, CultureInfo.InvariantCulture);

            VBotLogger.Trace(() => string.Format("[CreateDocItem] Document Version is {0}", docItem.DocumentProperties.Version));

            if (IsPdfDocument(docProperties.Path))
            {
                VBotLogger.Trace(() => string.Format("[CreateDocItem] Document {0} is PDF", docProperties.Path));
                docItem.DocumentProperties.Type = DocType.Pdf;
                docItem.DocumentProperties.Path = docItem.DocumentProperties.Path + EXTENSION_PDF;
                File.Copy(docProperties.Path, docItem.DocumentProperties.Path, true);
            }
            else
            {
                VBotLogger.Trace(() => string.Format("[CreateDocItem] Document {0} is Image", docProperties.Path));
                docItem.DocumentProperties.Type = DocType.Image;
                File.Copy(docProperties.Path, docItem.DocumentProperties.Path, true);
                //ImageOperations imgOperations = new ImageOperations();
                //imgOperations.UpdateImageProperties(docItem.DocumentProperties);
                _designerServiceEndPoint.UpdateImageProperties(docItem.DocumentProperties);
            }

            return docItem;
        }

        private bool IsPdfDocument(string filePath)
        {
            var extension = Path.GetExtension(filePath);
            if (extension != null && extension.ToLower().Equals(EXTENSION_PDF))
            {
                return true;
            }
            return false;
        }

        private LayoutCreationOperation duplicateLayoutReporter(object sender, VisionBotEngine.Model.Layout layout)
        {
            VBotLogger.Trace(() => string.Format("[duplicateLayoutReporter] Duplication is found for the layout {0}", layout.Name));

            LayoutCreationOperation layoutCreationOperation = LayoutCreationOperation.Cancel;
            try
            {
                VisionBotMainWindow wnd = sender as VisionBotMainWindow;
                if (wnd.Dispatcher.CheckAccess())
                {
                    layoutCreationOperation = promptForDuplicateLayout(sender, layout);
                }
                else
                {
                    var manualReset = new ManualResetEvent(false);
                    wnd.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        layoutCreationOperation = promptForDuplicateLayout(sender, layout);
                        manualReset.Set();
                    }));
                    manualReset.WaitOne();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(duplicateLayoutReporter));
            }

            return layoutCreationOperation;
        }

        private static LayoutCreationOperation promptForDuplicateLayout(object sender, VisionBotEngine.Model.Layout layout)
        {
            VBotLogger.Trace(() => "[promptForDuplicateLayout] Prompt layout duplication message...");

            LayoutCreationOperation layoutCreationOperation;
            var duplicateLayout = new DuplicateLayoutDialog();
            duplicateLayout.DataContext = layout;
            duplicateLayout.Owner = sender as VisionBotMainWindow;
            duplicateLayout.ShowDialog();
            layoutCreationOperation = duplicateLayout.DuplicateLayoutOperation;
            return layoutCreationOperation;
        }

        private static VisionBotEngine.Model.Layout getConvertedLayout(Layout layout)
        {
            VBotLogger.Trace(() => string.Format("[getConvertedLayout] Converting the Layout {0}", layout.Name));

            ILayoutConverter layoutConverter = new LayoutConverter();
            var convertedLayout = layoutConverter.ConvertTo(layout);
            return convertedLayout;
        }

        private void SaveVisionBot()
        {
            try
            {
                VBotLogger.Trace(() => "[SaveVisionBot] Save VisionBot processing start....");
                //VisionBotManifest vBotManifest = new VisionBotManifest
                //{
                //    Name = this.VisionBotName,
                //    ParentPath = this.DataStoragePath
                //};
                //updateBluePrint(vBotManifest);
                foreach (var layout in _visionBot.Layouts)
                {
                    if (layout != null)
                    {
                        VBotLogger.Trace(() => string.Format(
                                "[SaveVisionBot] layout.Name=\"{0}\", VBotTestDocSetId=\"{1}\"....",
                                layout.Name,
                                string.IsNullOrWhiteSpace(layout.VBotTestDocSetId) ? string.Empty : layout.VBotTestDocSetId));
                    }
                }

                IVisionBotManager visionBotManager = new VisionBotManager(_designerServiceEndPoint);
                visionBotManager.SaveVisionBot(ClassifiedFolderManifest, VisionBotManifest, this._visionBot);
                VBotLogger.Trace(() => "[SaveVisionBot] Saved VisionBot successfully.");
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(SaveVisionBot));
            }
        }

        private void updateBluePrint(VisionBotManifest vBotManifest)
        {
            var visionBot = getVisionBot(vBotManifest);

            foreach (var feildDef in visionBot.DataModel.Fields)
            {
                if (_visionBot.DataModel.Fields.Find(f => f.Id.Equals(feildDef.Id)) == null)
                {
                    feildDef.IsDeleted = true;
                    _visionBot.DataModel.Fields.Add(feildDef);
                }
            }

            foreach (var tableDef in visionBot.DataModel.Tables)
            {
                if (_visionBot.DataModel.Tables.Find(f => f.Id.Equals(tableDef.Id)) == null)
                {
                    tableDef.IsDeleted = true;
                    _visionBot.DataModel.Tables.Add(tableDef.Clone());
                }

                foreach (var columnDef in tableDef.Columns)
                {
                    var table = _visionBot.DataModel.Tables.Find(t => t.Id.Equals(tableDef.Id));
                    if (table.Columns.Find(c => c.Id.Equals(columnDef.Id)) == null)
                    {
                        columnDef.IsDeleted = true;
                        _visionBot.DataModel.Tables.Find(t => t.Id.Equals(tableDef.Id)).Columns.Add(columnDef);
                    }
                }
            }
        }

        private VisionBot getVisionBot(VisionBotManifest vBotManifest)
        {
            IVisionBotManager visionBotManager = new VisionBotManager(_designerServiceEndPoint);

            var visionBot = visionBotManager.LoadVisionBot(vBotManifest);
            return visionBot;
        }

        private void ExtractVBotData(VisionBot visionbot, ref VBotDocument vBotDocument)
        {
            DocumentProcessor docProcessor = new DocumentProcessor(_designerServiceEndPoint);
            docProcessor.ExtractVBotData(visionbot, ref vBotDocument);

            VBotLogger.Trace(() => "[ExtractVBotData] Extracted VisionBot data successfully.");
        }

        private VBotDocument getVBotDocument(VisionBot visionbot, DocumentProperties docProperties, bool isPreProcessed, bool preserveGrayscaledPages)
        {
            DocumentProcessor documentProcessor = new DocumentProcessor(_designerServiceEndPoint);
            VBotEngineSettings engineSettings = documentProcessor.GetEngineSettings(isPreProcessed, preserveGrayscaledPages);
            return documentProcessor.GetVBotDocument(visionbot, docProperties, engineSettings);
        }

        private void setVisionBotLayout(VisionBotEngine.Model.Layout layout, VBotDocItem docItem, VBotDocument vBotDocument)
        {
            layout.Id = Guid.NewGuid().ToString();
            layout.VBotDocSetId = docItem.Id;
            layout.SirFields = vBotDocument.SirFields;
            layout.Settings = vBotDocument.Layout.Settings;
            this._visionBot.Layouts.Add(layout);
        }

        public void visionBotDesigner_TestRequested(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void visionBotDesigner_SaveRequested(object sender, SaveRequestEventArgs e)
        {
            try
            {
                VBotLogger.Trace(() => "[visionBotDesigner_SaveRequested] VisionBot save requested.");
                _visionBot = ConvertIQBotToVisionBot(e.IqBot);
                SaveVisionBot();
                SaveValidationData(e.IqBot);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(visionBotDesigner_SaveRequested));
            }
        }

        //#region Dynamic Validators

        private IVBotValidatorDataManager __validatorDataManager;

        private IVBotValidatorDataManager ValidatorDataManager
        {
            get
            {
                return __validatorDataManager
                       ?? (__validatorDataManager = new VBotValidatorDataManager(ClassifiedFolderManifest, this.VisionBotManifest.Id));
            }
        }

        private void SaveValidationData(IqBot iqBot)
        {
            IVBotValidatorDataWriter manager = this.ValidatorDataManager;

            foreach (var fieldDef in iqBot.DataModel.Fields)
            {
                this.SaveValidationData_FieldDef(manager, fieldDef);
            }

            foreach (var tableDef in iqBot.DataModel.Tables)
            {
                foreach (var fieldDef in tableDef.Columns)
                {
                    this.SaveValidationData_FieldDef(manager, fieldDef);
                }
            }
        }

        private void SaveValidationData_FieldDef(IVBotValidatorDataWriter validatorDataManager, FieldDef fieldDef)
        {
            try
            {
                if (fieldDef == null || validatorDataManager == null)
                {
                    return;
                }

                if ((fieldDef.StaticListProvider == null || fieldDef.StaticListProvider.GetListItems().Count == 0)
                    && string.IsNullOrEmpty(fieldDef.FilePathForList))
                {
                    validatorDataManager.Remove<ListProvider>(fieldDef.Id);
                    return;
                }

                ListProvider listProvider = (string.IsNullOrEmpty(fieldDef.FilePathForList)
                                                 ? fieldDef.StaticListProvider as ListProvider
                                                 : new FileBasedListProvider { FilePath = fieldDef.FilePathForList });

                validatorDataManager.Set(fieldDef.Id, listProvider);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(SaveValidationData_FieldDef));
            }
        }

        private void SetDynamicValidatonData(IqBot iqBot)
        {
            IVBotValidatorDataReader manager = this.ValidatorDataManager;

            foreach (var fieldDef in iqBot.DataModel.Fields)
            {
                this.SetDynamicValidationData_FieldDef(manager, fieldDef);
            }

            foreach (var tableDef in iqBot.DataModel.Tables)
            {
                foreach (var fieldDef in tableDef.Columns)
                {
                    this.SetDynamicValidationData_FieldDef(manager, fieldDef);
                }
            }
        }

        private void SetDynamicValidationData_FieldDef(IVBotValidatorDataReader validatorDataManager, FieldDef fieldDef)
        {
            if (fieldDef == null || validatorDataManager == null)
            {
                return;
            }

            fieldDef.StaticListProvider = validatorDataManager.Get<StaticListProvider>(fieldDef.Id);

            fieldDef.FilePathForList = validatorDataManager.Get<FileBasedListProvider>(fieldDef.Id)?.FilePath;
        }

        private FieldDef SetDynamicValidatonData(FieldDef fieldDef)
        {
            this.SetDynamicValidationData_FieldDef(this.ValidatorDataManager, fieldDef);
            return fieldDef;
        }

        private TableDef SetDynamicValidatonData(TableDef tableDef)
        {
            foreach (var fieldDef in tableDef.Columns)
            {
                this.SetDynamicValidatonData(fieldDef);
            }
            return tableDef;
        }

        //#endregion Dynamic Validators

        public void visionBotDesigner_ModifyTestDocSetItemRequested(object sender, TestDocSetEventArgs e)
        {
            this.modifyTestDocSetItem(e);
        }

        private void modifyTestDocSetItem(TestDocSetEventArgs e)
        {
            try
            {
                if (e.LayoutId != null)
                {
                    var layout = _visionBot.Layouts.Find(x => x.Id.Equals(e.LayoutId));
                    string layoutName = layout.Name;

                    //   VisionBotManifest vBotManifest = new VisionBotManifest() { Name = this.VisionBotName, ParentPath = this.DataStoragePath };

                    ITestDocSetManager testDocSetManager = new TestDocSetManager(_designerServiceEndPoint);
                    this._testDocSet = testDocSetManager.LoadTestDocSet(VisionBotManifest, layoutName);
                    this._testDocSet.Name = layoutName;

                    if (_vBotTestSet == null)
                        _vBotTestSet = new List<VBotDocSet>();

                    if (_vBotTestSet.FirstOrDefault(x => x.Id == _testDocSet.Id) == null)
                    {
                        _vBotTestSet.Add(_testDocSet);
                    }

                    string filePath = e.DocSetItem.DocumentPath;
                    if (e.Action.Equals(Operation.Add))
                    {
                        this.AddDocSetItem(e);
                    }
                    else if (e.Action.Equals(Operation.Update))
                    {
                        UpdatedBenchmarkData(e);
                    }
                    else if (e.Action.Equals(Operation.Remove))
                    {
                        this.RemoveDocSetItem(e);
                    }
                    _visionBot.Layouts.Find(x => x.Id.Equals(e.LayoutId)).VBotTestDocSetId = e.TestDocSetId = this._testDocSet.Id;
                    Layout layoutOfTest = (from q in e.IqBot.Layouts where q.Id.Equals(e.LayoutId) select q).FirstOrDefault();
                    if (layoutOfTest != null)
                    {
                        layoutOfTest.TestDocSetId = this._testDocSet.Id;
                    }

                    // this.SaveVisionBot();
                    testDocSetManager.SaveTestDocSet(this._testDocSet, VisionBotManifest, layoutName);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(modifyTestDocSetItem));
            }
        }

        private void AddDocSetItem(TestDocSetEventArgs e)
        {
            try
            {
                var docItem = CreateDocItem(e.DocSetItem.DocumentProperties);
                VBotLogger.Trace(() => string.Format("[AddDocSetItem] Created DocItem successfully for file {0}", e.DocSetItem.DocumentProperties.Path));

                docItem.Name = e.DocSetItem.Name;
                docItem.DocumentName = e.DocSetItem.DocumentName;
                docItem.IsBenchMarkDataValidated = false;
                VisionBot visionBot = ConvertIQBotToVisionBot(e.IqBot);

                VBotLogger.Trace(() => "[AddDocSetItem] IQBot Converted successfully to VisionBot");
                bool isSameLayoutIdentified = true;

                var vBotDocument = this.getVBotDocument(visionBot, docItem.DocumentProperties, false, true);
                UpdateDocItemProperties(docItem, vBotDocument);

                VisionBotEngine.Model.Layout layout = _visionBot.Layouts.Find(x => x.Id.Equals(e.LayoutId));
                if (string.IsNullOrWhiteSpace(vBotDocument.Layout.Name) ||
                    (layout != null && !layout.Id.Equals(vBotDocument.Layout.Id)))
                {
                    isSameLayoutIdentified = false;
                }

                ITestDataEngine testDataEngine = new TestDataEngine() { VBotDocument = vBotDocument };
                docItem.BenchmarkData = testDataEngine.GenerateBanchMarkData(!isSameLayoutIdentified);
                VBotLogger.Trace(() => "[AddDocSetItem] Generated Benchmark data successfully.");

                e.DocSetItem = new DocSetConverter().ConvertFrom(docItem);

                var benchmarkData = isSameLayoutIdentified
                                    ? testDataEngine.GenerateValidatedBanchMarkData()
                                    : this.getValidatedBenchmarkData(layout, docItem.BenchmarkData.Fields, docItem.BenchmarkData.TableSet);

                VBotLogger.Trace(() => "[AddDocSetItem] Generated validated Benchmark data successfully.");

                e.DocSetItem.Fields = benchmarkData.Fields;
                e.DocSetItem.TableSet = benchmarkData.TableSet;
                VBotLogger.Trace(() => "[AddDocSetItem] Set Benchmark data to DocSetItem.");

                e.DocSetItem.SystemIdentifiedFields = new LayoutConverter().ConvertSIRFields(vBotDocument.SirFields);
                if (isSameLayoutIdentified)
                {
                    e.DocSetItem.FillRegions();
                }

                VBotLogger.Trace(() => "[AddDocSetItem] Set SystemIdentifiedFields and Regions to DocSetItem.");

                this._testDocSet.DocItems.Add(docItem);

                VBotLogger.Trace(() => string.Format("[AddDocSetItem] Addded DocItem {0} to DocSet successfully", docItem.Name));
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(modifyTestDocSetItem));
            }
        }

        private void RemoveDocSetItem(TestDocSetEventArgs e)
        {
            //IVBotDocumentStorage docSetItemStorage = new VBotDocumentStorage(this._testDocSet.DocItems);
            //docSetItemStorage.RemoveDocItem(new DocSetConverter().ConvertTo(e.DocSetItem));

            VBotDocItem vbotDocItem = new DocSetConverter().ConvertTo(e.DocSetItem);

            VBotDocItem deleteRequestItem = _testDocSet.DocItems.FirstOrDefault(x => x.Id == vbotDocItem.Id);

            if (deleteRequestItem != null)
            {
                _testDocSet.DocItems.Remove(deleteRequestItem);
            }

            // DesignerServiceEndPointProvider.GetInstant().RemoveDocItem(_testDocSet,);
            VBotLogger.Trace(() => string.Format("[RemoveDocSetItem] Removed DocSetItem {0} successfully", e.DocSetItem.Name));
        }

        private void UpdatedBenchmarkData(TestDocSetEventArgs e)
        {
            try
            {
                VBotLogger.Trace(() => "[UpdatedBenchmarkData] Update Benchmark data");
                //   IVBotDocumentStorage docSetItemStorage = new VBotDocumentStorage(this._testDocSet.DocItems);
                ITestDataEngine testDataEngine = new TestDataEngine();
                BenchmarkData benchmarkData = testDataEngine.GenerateBenchmarkData(e.UpdatedData);
                VBotLogger.Trace(() => "[UpdatedBenchmarkData] Generated Benchmark data successfully.");
                VisionBotEngine.Model.Layout selectedLayout = _visionBot.Layouts.Find(l => l.Id.Equals(e.LayoutId));
                BenchmarkData validatedBenchmarkData = getValidatedBenchmarkData(selectedLayout, benchmarkData.Fields, benchmarkData.TableSet);
                VBotLogger.Trace(() => "[UpdatedBenchmarkData] Generated Validated Benchmark data successfully.");
                e.DocSetItem.PreviewFields = validatedBenchmarkData.Fields;
                e.DocSetItem.PreviewTableSet = validatedBenchmarkData.TableSet;
                BenchmarkData dataToSave = testDataEngine.GenerateBanchMarkData(validatedBenchmarkData.Fields, validatedBenchmarkData.TableSet);
                //docSetItemStorage.UpdateData(e.DocSetItem.Id, dataToSave, e.DocSetItem.IsBenchMarkDataSaved, e.DocSetItem.IsBenchmarkStructureMismatchFound);
                new NewCode().UpdateData(this._testDocSet.DocItems, e.DocSetItem.Id, dataToSave, e.DocSetItem.IsBenchMarkDataSaved, e.DocSetItem.IsBenchmarkStructureMismatchFound);
                e.DocSetItem.OriginalIsBenchmarchStructureFound = e.DocSetItem.IsBenchmarkStructureMismatchFound;
                VBotLogger.Trace(() => "[UpdatedBenchmarkData] Benchmark data updated successfully.");
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(UpdatedBenchmarkData));
            }
        }

        //TODO :Change Hardcore Value
        public IqBot LoadVisionBot()
        {
            var visionBot = this.loadVisionBot();
            _visionBot = visionBot;
            IVisionBotConverter visionBotConverter = new VisionBotConverter();
            IDocSetManager docSetStorageManager = new DocSetManager(_designerServiceEndPoint);
            // var visionBotMenifest = new VisionBotManifest() { Name = VisionBotName, ParentPath = DataStoragePath };
            var result = visionBotConverter.ConvertFrom(visionBot, docSetStorageManager.LoadDocSet(VisionBotManifest/*VisionBotManifest.GetFilePath()*/));
            //SetDynamicValidatonData(result);
            return result;
        }

        public Layout GetLayoutAt(int index, DataModel bluePrint)
        {
            ILayoutConverter layoutConverter = new LayoutConverter();
            string imagePath = string.Empty;
            string documentName = string.Empty;

            foreach (var docItem in _docSet.DocItems)
            {
                if (docItem.Id.Equals(_visionBot.Layouts[index].VBotDocSetId))
                {
                    documentName = docItem.DocumentName;
                    imagePath = getImagePath(docItem);
                    break;
                }
            }

            return layoutConverter.ConvertFrom(_visionBot.Layouts[index], imagePath, _visionBot, bluePrint, documentName);
        }

        private static string getImagePath(VBotDocItem docItem)
        {
            return Path.Combine(createTempVisionBotDirectory(docItem), docItem.Id);
            //return docItem.DocumentProperties.Path;
        }

        private static string createTempVisionBotDirectory(VBotDocItem docItem)
        {
            var directoryPath = Path.Combine(
                Path.GetTempPath(),
                Properties.Resources.VisionBotTempDirectory,
                docItem.DocumentName);
            Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }

        internal IList<FieldDef> GetBlueprintFieldDefinitions()
        {
            IFieldDefConverter fieldDefConverter = new FieldDefConverter();
            IList<FieldDef> fieldDefs = new List<FieldDef>();
            foreach (var fieldDefinitions in _visionBot.DataModel.Fields)
            {
                if (!fieldDefinitions.IsDeleted)
                    fieldDefs.Add(this.SetDynamicValidatonData(fieldDefConverter.ConvertFrom(fieldDefinitions)));
            }

            return fieldDefs;
        }

        internal IList<TableDef> GetBlueprintTableDefinitions()
        {
            ITableDefConverter tableDefConverter = new TableDefConverter();
            IList<TableDef> tableDefinitions = new List<TableDef>();

            foreach (var tableDefinition in _visionBot.DataModel.Tables)
            {
                if (!tableDefinition.IsDeleted)
                    tableDefinitions.Add(this.SetDynamicValidatonData(tableDefConverter.ConvertFrom(tableDefinition)));
            }
            return tableDefinitions;
        }

        public int LayoutCount
        {
            get
            {
                if (_visionBot == null || _visionBot.Layouts == null)
                {
                    return 0;
                }

                return _visionBot.Layouts.Count;
            }
        }

        private VisionBot loadVisionBot()
        {
            VBotLogger.Trace(() => string.Format("[loadVisionBot] Loading VisionBot name : {0}.", VisionBotManifest.Name));

            IVisionBotManager visionBotManager = new VisionBotManager(_designerServiceEndPoint);
            VisionBot visionBot;
            // if (File.Exists(this.VisionBotManifest.GetFilePath()))

            _visionBot = visionBot = visionBotManager.LoadVisionBot(ClassifiedFolderManifest);
            if (_visionBot != null)
            {
                VisionBotManifest = new VisionBotManifest()
                {
                    Id = visionBot.Id,
                    ParentPath = string.Empty
                };

                //if (string.IsNullOrEmpty(visionBot.Name))
                //{
                //    visionBot.Name = visionBot.Id;
                //}

                var docSetStorage = new DocSetManager(_designerServiceEndPoint);
                _docSet = docSetStorage.LoadDocSet(VisionBotManifest);

                VBotLogger.Trace(() => string.Format("[loadVisionBot] Loaded docSet is {0}", _docSet.Name));

                UpdateLayoutProperties(_docSet, _visionBot);
                var testDocSetStorage = new TestDocSetManager(_designerServiceEndPoint);

                this._testDocSet = visionBot.Layouts.Count > 0 && !string.IsNullOrWhiteSpace(visionBot.Layouts[0].VBotTestDocSetId)
                    ? testDocSetStorage.LoadTestDocSet(VisionBotManifest, visionBot.Layouts[0].Name)
                    : new VBotDocSet();
                _testSet = this.GetLayoutTestSet();
            }
            else
            {
                visionBot = new VisionBot();
                _docSet = new VBotDocSet();
                _testDocSet = new VBotDocSet();
                _testSet = new List<LayoutTestSet>();
            }

            return visionBot;
        }

        private void UpdateLayoutProperties(VBotDocSet docSet, VisionBot visionBot)
        {
            for (int i = 0; i < visionBot.Layouts.Count; i++)
            {
                VBotDocItem vbDocItem = docSet.DocItems.Find(doc => doc.Id.Equals(visionBot.Layouts[i].VBotDocSetId));
                if (vbDocItem != null)
                {
                    visionBot.Layouts[i].DocProperties.Path = vbDocItem.DocumentProperties.Path;
                }
            }
            VBotLogger.Trace(() => "[UpdateLayoutProperties] Layout properties updated successfully");
        }

        public List<LayoutTestSet> GetLayoutTestSet()
        {
            // var vBotManifest = new VisionBotManifest() { Name = this.VisionBotName, ParentPath = this.DataStoragePath };
            var testDocSetStorage = new TestDocSetManager(_designerServiceEndPoint);
            var docSetConverter = new DocSetConverter();
            var testSet = new List<LayoutTestSet>();
            _vBotTestSet = new List<VBotDocSet>();

            foreach (var layout in _visionBot.Layouts)
            {
                if (string.IsNullOrWhiteSpace(layout.Id))
                    layout.Id = Guid.NewGuid().ToString();

                var vBotDocset = (!string.IsNullOrWhiteSpace(layout.Id))
                                     ? testDocSetStorage.LoadTestDocSet(VisionBotManifest, layout.Name)
                                     : new VBotDocSet();
                _vBotTestSet.Add(vBotDocset);
                ValidateBenchmarkData(layout, vBotDocset);
                var layoutTestSet = new LayoutTestSet()
                {
                    LayoutId = layout.Id,
                    LayoutName = layout.Name,
                    DocSetItems = docSetConverter.ConvertFrom(vBotDocset)
                };

                testSet.Add(layoutTestSet);
            }

            VBotLogger.Trace(() => "[GetLayoutTestSet] Get Layout's test set.");
            return testSet;
        }

        private void ValidateBenchmarkData(VisionBotEngine.Model.Layout layout, VBotDocSet vBotDocset)
        {
            foreach (var vBotDocItem in vBotDocset.DocItems)
            {
                DataTable fieldsTable = vBotDocItem.BenchmarkData.Fields;
                DataSet tableSet = vBotDocItem.BenchmarkData.TableSet;

                var benchmarkData = getValidatedBenchmarkData(layout, fieldsTable, tableSet);
                vBotDocItem.BenchmarkData = benchmarkData;
            }
            VBotLogger.Trace(() => "[ValidateBenchmarkData] Benchmark data validated successfully.");
        }

        private BenchmarkData getValidatedBenchmarkData(VisionBotEngine.Model.Layout layout, DataTable fieldsTable, DataSet tableSet)
        {
            VBotDocument vBotDocument = getValidatedVBotData(layout, fieldsTable, tableSet);
            var testDataEngine = new TestDataEngine() { VBotDocument = vBotDocument };
            BenchmarkData benchmarkData = testDataEngine.GenerateValidatedBanchMarkDataEngineModel();
            return benchmarkData;
        }

        private VBotDocument getValidatedVBotData(VisionBotEngine.Model.Layout layout, DataTable fieldsTable, DataSet tableSet)
        {
            var vBotData = getVBotDataFromBenchmark(layout, fieldsTable, tableSet);
            VBotDocument vBotDocument = new VBotDocument()
            {
                Data = vBotData,
                Layout = layout
            };

            ValidatorAdvanceInfo validatorAdvanceInfo = getValidatorAdvanceInfo(_visionBot);
            //  var validationDataManager = new VBotValidatorDataManager(this.VisionBotManifest.GetFilePath());
            // vBotDocument.Data.EvaluateAndFillValidationDetails(layout, validationDataManager);
            vBotDocument.Data = _designerServiceEndPoint.EvaluateAndFillValidationDetails(vBotDocument
                , layout
                , this.VisionBotManifest
                , validatorAdvanceInfo);
            return vBotDocument;
        }

        private VBotData getVBotDataFromBenchmark(VisionBotEngine.Model.Layout layout, DataTable fieldsTable, DataSet tableSet)
        {
            VBotLogger.Trace(() => "[getVBotDataFromBenchmark] Get VisionBot data from Benchmark data.");
            BecnchmarkDataConverter dataConverter = new BecnchmarkDataConverter();
            DataRecord fields = dataConverter.ExtractFieldValues(fieldsTable, _visionBot.DataModel, layout);
            List<TableValue> tables = dataConverter.ExtractTableValues(tableSet, _visionBot.DataModel, layout);
            VBotData vBotData = new VBotData();
            vBotData.FieldDataRecord = fields;
            vBotData.TableDataRecord = tables;
            return vBotData;
        }

        public void VisionBotDesignerView_ExecuteTestRequested(object sender, TestExecutionEventArgs e)
        {
            try
            {
                VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - Execution Started.");

                ITestExecution testExecution = new TestExecution(_designerServiceEndPoint);
                var visionBot = ConvertIQBotToVisionBot(e.IqBot);

                List<LayoutTestResult> layoutTestResults = new List<LayoutTestResult>();
                foreach (var layoutTestSetViewModel in e.LayoutTestSetViewModels)
                {
                    VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_ExecuteTestRequested] - Execution Started for layout -> {0}.", layoutTestSetViewModel.Name));
                    DateTime startTime = DateTime.Now;
                    var layout = visionBot.Layouts.FirstOrDefault(l => l.Id.Equals(layoutTestSetViewModel.Id));

                    VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_ExecuteTestRequested] - Execution Started for DocSetId -> {0}.", layout.VBotTestDocSetId));

                    TestLayoutResultManager layoutResultManager = new TestLayoutResultManager(layout.Id, layout.VBotTestDocSetId);

                    if (layout != null)
                    {
                        foreach (var layoutTestSetItem in layoutTestSetViewModel.LayoutTestSetItems)
                        {
                            IDocSetConverter docSetConverter = new DocSetConverter();
                            VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - Get DocSetItem.");

                            var docSetItem = docSetConverter.ConvertTo(layoutTestSetItem.GetDocSetItem());

                            if (docSetItem != null)
                            {
                                VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_ExecuteTestRequested] - Execution Started for Test -> {0} with Document {1}.", docSetItem.Name, docSetItem.DocumentName));

                                if (docSetItem.IsBenchMarkDataValidated && !layoutTestSetItem.Warning.HasWarning)
                                {
                                    VBotLogger.Trace(
                                        () => "[VisionBotDesignerView_ExecuteTestRequested] - Benchmark data are validated.");
                                    ITestDocSetManager testDocSetManager = new TestDocSetManager(_designerServiceEndPoint);
                                    VBotDocSet testDocSet =
                                        testDocSetManager.LoadTestDocSet(
                                            /*new VisionBotManifest() { Name = VisionBotName, ParentPath = DataStoragePath }*/VisionBotManifest,
                                            layout.Name);

                                    VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_ExecuteTestRequested] - load all test docs from layout  {0}", layout.Name));

                                    if (testDocSet.DocItems.Find(x => x.Id.Equals(docSetItem.Id)) != null)
                                    {
                                        docSetItem.BenchmarkData =
                                            testDocSet.DocItems.Find(x => x.Id.Equals(docSetItem.Id)).BenchmarkData;

                                        VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - Test execution started.");
                                        TestDocItemResult docItemResult = testExecution.RunTest(visionBot, docSetItem, layout.Id);
                                        docItemResult.TestName = docSetItem.Name;
                                        layoutResultManager.TestDocItemResults.Add(docItemResult);
                                        VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - Test execution completed.");
                                    }
                                    else
                                    {
                                        VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_ExecuteTestRequested] - Test Documents count is {0} ", testDocSet.DocItems.Count));
                                    }
                                }
                                else
                                {
                                    VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - Benchmark data are invalid.");
                                    layoutResultManager.ExcludedTests.Add(
                                        new ExcludedTestInfo()
                                        {
                                            DocItemId = docSetItem.Id,
                                            ErrorCode = TestExcludeErrorCode.BenchmarkDataNotValidated
                                        });
                                }
                            }
                            else
                            {
                                VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - No Test available");
                            }
                        }

                        layoutResultManager.LayoutName = layout.Name;
                    }

                    layoutTestResults.Add(layoutResultManager.GetTestDocSetResult());
                }

                e.LayoutTestResults = layoutTestResults;
                VBotLogger.Trace(() => "[VisionBotDesignerView_ExecuteTestRequested] - Return result.");
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(VisionBotDesignerView_ExecuteTestRequested));
            }
        }

        public void VisionBotDesignerView_LayoutDeleted(object sender, LayoutDeletedEventArgs e)
        {
            //VisionBotManifest vBotManifest = new VisionBotManifest()
            //{
            //    Name = this.VisionBotName,
            //    ParentPath = this.DataStoragePath
            //};

            ITestDocSetManager testDocSetManager = new TestDocSetManager(_designerServiceEndPoint);
            IDocSetManager docSetStorageManager = new DocSetManager(_designerServiceEndPoint);
            try
            {
                string vbotDocSetID = Path.GetFileNameWithoutExtension(e.Layout.DocumentPath);
                //foreach (var docItem in _docSet.DocItems.ToArray())
                //{
                //    if (!string.IsNullOrWhiteSpace(vbotDocSetID) && docItem.Id.Equals(vbotDocSetID))
                //    {
                //        _docSet.DocItems.Remove(docItem);
                //        VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_LayoutDeleted] Removed docSet Item {0}", docItem.Name));

                VBotDocSet testDocSet = _vBotTestSet.Find(t => t.Id.Equals(e.Layout.TestDocSetId));
                if (!string.IsNullOrWhiteSpace(e.Layout.TestDocSetId)
                    && testDocSet != null)
                {
                    // TestDocSetId = layoutToDelete.VBotTestDocSetId;

                    _vBotTestSet.Remove(testDocSet);
                    testDocSetManager.DeleteTestDocSet(VisionBotManifest, e.Layout.Name);
                    VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_LayoutDeleted] Removed TestdocSet for layout {0}", e.Layout.Name));
                }

                //        break;
                //    }
                //}
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineCommunicator), nameof(VisionBotDesignerView_LayoutDeleted));
            }

            docSetStorageManager.SaveDocSet(VisionBotManifest, _docSet);
            VBotLogger.Trace(() => string.Format("[VisionBotDesignerView_LayoutDeleted] Save updated docSet."));
        }

        public void VisionBotDesignerView_LayoutFieldValueRequested(object sender, FieldValueRequestEventArgs e)
        {
            Automation.VisionBotEngine.Model.Field field = GetValueField(e.Layout, e.LayoutField, e.IqBot);
            e.ValueField = field;
        }
    }
}