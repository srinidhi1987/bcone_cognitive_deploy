﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels.Validators;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Interaction logic for FieldTree.xaml
    /// </summary>
    public partial class FieldTree : UserControl
    {
        private const string Filter = "Documents(*.pdf;*.jpg;*.jpeg;*.png;*.tif;*.tiff)|*.pdf;*.jpg;*.jpeg;*.png;*.tif;*.tiff";
        private RibbonTabItem _toggleImage;
        private ObservableCollection<MarkerType> markerTypes = new ObservableCollection<MarkerType>();
        public static FileSelectionDialog fileSelectionDialog = null;
        private Layout layout = null;
        public static SolidColorBrush NormalColorBrush = new SolidColorBrush(Color.FromArgb(255, 51, 51, 51));

        public FieldTree()
        {
            fillMarkerTypes();
            _toggleImage = new RibbonTabItem
            {
                ActiveImageSource = "/Images/Expanded.png",
                Caption = "Layouts",
                InactiveImageSource = "/Images/Collapsed.png",
                IsSelected = true
            };

            InitializeComponent();
            ToggleButton.DataContext = _toggleImage;
            checkedPopup.DataContext = markerTypes;
        }

        public RibbonTabItem ToggleImage
        {
            get { return _toggleImage; }
            set { _toggleImage = value; }
        }

        private void LayoutToggle_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainTreeView.Visibility == Visibility.Visible)
            {
                MainTreeView.Visibility = Visibility.Collapsed;
                container.RowDefinitions[1] = new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) };
                _toggleImage.IsSelected = false;
            }
            else
            {
                MainTreeView.Visibility = Visibility.Visible;
                container.RowDefinitions[1] = new RowDefinition { Height = new GridLength(1, GridUnitType.Star) };
                _toggleImage.IsSelected = true;
            }
        }

        private void CreateLayout_OnClick(object sender, RoutedEventArgs e)
        {
            layout = new Layout();
            while (true)
            {
                var isLayoutCreated = false;
                fileSelectionDialog = new FileSelectionDialog(layout)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                var fieldTreeVM = this.DataContext as FieldTreeVM;

                if (fieldTreeVM != null)
                {
                    fileSelectionDialog.Layouts = fieldTreeVM.GetLayouts();
                }
                fileSelectionDialog.Owner = Window.GetWindow(this);
                isLayoutCreated = Convert.ToBoolean(fileSelectionDialog.ShowDialog());

                if (!isLayoutCreated)
                {
                    return;
                }

                layout = fileSelectionDialog.Layout;
                DocumentValidator documentValidator = new DocumentValidator { dedpendencyObject = this };
                if (documentValidator.IsValidDocument(fileSelectionDialog.Layout.DocumentPath))
                {
                    break;
                }
            }

            if (fileSelectionDialog != null)
            {
                CreateNewLayout();
            }
        }

        private void CreateNewLayout()
        {
            var fieldTreeVM = this.DataContext as FieldTreeVM;

            if (fieldTreeVM != null)
            {
                bool addNewLayout = true;
                Layout prevLayout = null;
                if (LayoutCreated != null)
                {
                    if (IsNewLayoutCreationReportDuplicate(out prevLayout, out addNewLayout))
                    {
                        while (true)
                        {
                            var keyEvent = DuplicateLayoutHandler();
                            if (keyEvent == null) return;

                            if (keyEvent.LayoutCreationOperation != LayoutCreationOperation.Open)
                            {
                                addNewLayout = false;
                                if (keyEvent.LayoutCreationOperation == LayoutCreationOperation.New)
                                {
                                    addNewLayout = true;
                                }
                                prevLayout = keyEvent.NewLayout;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (!addNewLayout)
                        {
                            addNewLayout = false;
                            fieldTreeVM.SelectLayout(prevLayout);
                        }
                    }
                }
                if (addNewLayout)
                {
                    fieldTreeVM.AddLayout(fileSelectionDialog.Layout);
                }
                else
                {
                    fieldTreeVM.SelectLayout(prevLayout);
                }
            }
        }

        private bool IsNewLayoutCreationReportDuplicate(out Layout prevLayout, out Boolean isNewLayout)
        {
            var eventArgs = new LayoutCreationEventArgs(fileSelectionDialog.Layout);
            LayoutCreated(this, eventArgs);
            isNewLayout = false;
            prevLayout = eventArgs.NewLayout;
            if (IsLayoutNew(eventArgs))
            {
                isNewLayout = true;
                return false;
            }
            else if (IsDuplicateLayoutDialogOperationCancelEdit(eventArgs))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsDuplicateLayoutDialogOperationCancelEdit(LayoutCreationEventArgs e)
        {
            if (e.LayoutCreationOperation == LayoutCreationOperation.Cancel ||
                e.LayoutCreationOperation == LayoutCreationOperation.Edit)
            {
                return true;
            }
            return false;
        }

        private bool IsLayoutNew(LayoutCreationEventArgs e)
        {
            return e.LayoutCreationOperation == LayoutCreationOperation.New;
        }

        public LayoutCreationEventArgs DuplicateLayoutHandler()
        {
            while (true)
            {
                var fieldTreeVM = this.DataContext as FieldTreeVM;

                if (fieldTreeVM != null)
                {
                    fileSelectionDialog.Layouts = fieldTreeVM.GetLayouts();
                }
                string filePath = SelectFile();
                fileSelectionDialog.Layout.DocumentPath = filePath;
                fileSelectionDialog.Layout.DocumentName = Path.GetFileName(filePath);
                layout = fileSelectionDialog.Layout;
                DocumentValidator documentValidator = new DocumentValidator { dedpendencyObject = this };
                if (documentValidator.IsValidDocument(fileSelectionDialog.Layout.DocumentPath))
                {
                    break;
                }
            }

            LayoutCreationEventArgs ev = null;
            if (fileSelectionDialog != null && !string.IsNullOrEmpty(fileSelectionDialog.Layout.DocumentPath))
            {
                var fieldTreeVM = this.DataContext as FieldTreeVM;

                if (fieldTreeVM != null)
                {
                    if (LayoutCreated != null)
                    {
                        ev = new LayoutCreationEventArgs(fileSelectionDialog.Layout);
                        LayoutCreated(this, ev);
                    }
                }
            }
            return ev;
        }

        private string SelectFile()
        {
            var dlg = new System.Windows.Forms.OpenFileDialog { CheckFileExists = true, Multiselect = false, Filter = Filter };

            return dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK ? dlg.FileName : string.Empty;
        }

        private bool showErrorMessage(string errorMessage)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = errorMessage,
                    Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "OK",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                        }
                    },
                }
            };
            errorMessageDialog.Owner = Window.GetWindow(this);
            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        public event LayoutCreateHander LayoutCreated;

        public delegate void LayoutCreateHander(object sender, LayoutCreationEventArgs e);

        public class LayoutCreationEventArgs : EventArgs
        {
            public Layout NewLayout { get; private set; }
            public LayoutCreationOperation LayoutCreationOperation { get; set; }

            public LayoutCreationEventArgs(Layout newLayout)
            {
                NewLayout = newLayout;
            }
        }

        private void fillMarkerTypes()
        {
            markerTypes.Add(new MarkerType { Name = "System Identified", Show = true, ColorCode = "#FF00AEDF" });
            markerTypes.Add(new MarkerType { Name = "Markers", Show = true, ColorCode = "#FFF73A3A" });
            markerTypes.Add(new MarkerType { Name = "Fields", Show = true, ColorCode = "#FF8CC636" });
            markerTypes.Add(new MarkerType { Name = "Tables", Show = true, ColorCode = "#FFFFCC00" });
        }

        public event LayoutFieldSelectionHandler LayoutFieldSelectionChanged;

        public event LayoutSelectionHandler LayoutSelectionChanged;

        // public event LayoutItemSelectionHandle LayoutItemSelectionChanged;
        public event LayoutTableSelectionHandle LayoutTableSelectionChanged;

        private void MainTreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var treeView = sender as TreeView;
            if (treeView != null)
            {
                var layout = treeView.SelectedItem as Layout;
                if (layout != null && LayoutSelectionChanged != null)
                {
                    LayoutSelectionChanged(this, new LayoutSelectionEventArgs(layout));
                    // return;
                }
                var layoutField = treeView.SelectedItem as LayoutField;
                if (layoutField != null && LayoutFieldSelectionChanged != null)
                {
                    // findLayoutOfCurrentField( layoutfield);
                    LayoutFieldSelectionChanged(this, new LayoutFieldSelectionEventArgs(layoutField));
                }

                // var layoutItem = treeView.SelectedItem as LayoutItem;
                //if (layoutField != null && LayoutFieldSelectionChanged != null)
                //{
                //   // findLayoutOfCurrentField( layoutfield);
                //    LayoutItemSelectionChanged(this, new LayoutItemSelectionEventArgs(layoutItem));
                //}

                // var layoutTable= treeView.SelectedItem as LayoutTable;
                //if (layoutField != null && LayoutFieldSelectionChanged != null)
                //{
                //   // findLayoutOfCurrentField( layoutfield);
                //    LayoutTableSelectionChanged(this, new LayoutTableSelectionEventArgs(layoutTable));
                //}

                FieldTreeVM fieldTreeVM = this.DataContext as FieldTreeVM;

                if (fieldTreeVM != null)
                {
                    fieldTreeVM.SelectedItem = treeView.SelectedItem;
                }
            }
        }

        public event LayoutItemDeleteHandler LayoutDeleteRequested;

        private void LayoutItemDeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var layoutDeleteButton = sender as Button;
            if (layoutDeleteButton != null)
            {
                LayoutDeleteRequested(this, new LayoutDeleteEventArgs(layoutDeleteButton.DataContext));
            }
        }

        public event DataModelItemDeleteHandler DataModelDeleteRequested;

        private void DataModelItemDeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var dataModelDeleteButton = sender as Button;
            if (dataModelDeleteButton != null)
            {
                DataModelDeleteRequested(this, new DataModelDeleteEventArgs(dataModelDeleteButton.DataContext));
            }
        }

        public event TableDefAddHandler TableDefAddRequested;

        private void TableDefAddRequested_OnClick(object sender, RoutedEventArgs e)
        {
            if (TableDefAddRequested != null)
            {
                TableDefAddRequested(this, EventArgs.Empty);
            }
        }
    }

    public delegate void LayoutItemDeleteHandler(object sender, LayoutDeleteEventArgs e);

    public class LayoutDeleteEventArgs : EventArgs
    {
        public object LayoutItemDeleteRequested { get; set; }

        public LayoutDeleteEventArgs(object layoutItemDeleteRequested)
        {
            LayoutItemDeleteRequested = layoutItemDeleteRequested;
        }
    }

    public delegate void DataModelItemDeleteHandler(object sender, DataModelDeleteEventArgs e);

    public class DataModelDeleteEventArgs : EventArgs
    {
        public object DataModelItemDeleteRequested { get; set; }

        public DataModelDeleteEventArgs(object dataModelItemDeleteRequested)
        {
            DataModelItemDeleteRequested = dataModelItemDeleteRequested;
        }
    }

    public delegate void TableDefAddHandler(object sender, EventArgs e);

    public delegate void LayoutFieldSelectionHandler(object sender, LayoutFieldSelectionEventArgs e);

    public class LayoutFieldSelectionEventArgs : EventArgs
    {
        public LayoutField LayoutField { get; set; }

        public LayoutFieldSelectionEventArgs(LayoutField layoutField)
        {
            LayoutField = layoutField;
        }
    }

    public delegate void LayoutSelectionHandler(object sender, LayoutSelectionEventArgs e);

    public class LayoutSelectionEventArgs : EventArgs
    {
        public Layout Layout { get; set; }

        public LayoutSelectionEventArgs(Layout layout)
        {
            Layout = layout;
        }
    }

    //public delegate void LayoutItemSelectionHandle(object sender, LayoutItemSelectionEventArgs e);

    //public class LayoutItemSelectionEventArgs : EventArgs
    //{
    //    public LayoutItem LayoutItem { get; set; }

    //    public LayoutItemSelectionEventArgs(LayoutItem layoutItem)
    //    {
    //        LayoutItem = layoutItem;
    //    }
    //}

    public delegate void LayoutTableSelectionHandle(object sender, LayoutTableSelectionEventArgs e);

    public class LayoutTableSelectionEventArgs : EventArgs
    {
        public LayoutTable LayoutTable { get; set; }

        public LayoutTableSelectionEventArgs(LayoutTable layoutTable)
        {
            LayoutTable = layoutTable;
        }
    }

    public enum LayoutCreationOperation
    {
        New,
        Edit,
        Open,
        Cancel
    }
}