/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Collections;
using System.ComponentModel;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class LayoutItem : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public IEnumerable Items { get; set; }
        public object Color { get; set; }
        public SolidColorBrush ItemBackground { get; set; }
        public bool IsRequired { get { return false; } }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                onNotifyPropertyChanged("IsSelected");
            }
        }

        private void onNotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        
        public LayoutItem()
        {
            ItemBackground = FieldTree.NormalColorBrush;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class DataModelParentItem : LayoutItem
    {

    }
}