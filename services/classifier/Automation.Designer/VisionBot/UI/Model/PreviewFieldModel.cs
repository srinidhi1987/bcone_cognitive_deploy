﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.ComponentModel;

    using Automation.CognitiveData.ConfidenceScore;

    public class PreviewFieldModel : INotifyPropertyChanged
    {
        public FieldDef FieldDef { get; set; }
        
        public ValidatedFieldValue Value { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
