/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Media;
    using VisionBotEngine;

    public class Layout : INotifyPropertyChanged
    {
        internal UIImageProvider ImageProvider { get; set; }

        public string Id { get; set; }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public event LayoutItemSelectionHandler SelectionChanged;

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                    if (IsSelected)
                        onSelectionChanged(this, new LayoutItemSelectionEventArgs(this) { Layout = this });
                }
            }
        }

        public event EventHandler ExpandChanged;

        private bool _isExpanded;

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    OnPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private string _testDocSetId;

        public string TestDocSetId
        {
            get { return _testDocSetId; }
            set { _testDocSetId = value; }
        }

        public DocumentProperties DocumentProperties { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }

        private ObservableCollection<ViewModelBase> _children = new ObservableCollection<ViewModelBase>();

        public ObservableCollection<ViewModelBase> Children
        {
            get
            {
                _children.Clear();
                _children.Add(_markers);
                _children.Add(_layoutFields);
                _children.Add(_layoutTables);
                return _children;
            }
        }

        private LayoutFieldsViewModel _layoutFields;

        internal LayoutFieldsViewModel LayoutFields
        {
            get { return _layoutFields; }
            set
            {
                if (_layoutFields != value)
                {
                    _layoutFields = value;
                    if (_layoutFields != null)
                    {
                        _layoutFields.SelectionChanged += layoutItem_SelectionChanged;
                        _layoutFields.ExpandChanged += field_ExpandChanged;
                        _layoutFields.LayoutSaveRequested += LayoutItemSaveRequested;
                        _layoutFields.DeletedHandler += layoutItem_DeletedHandler;
                    }
                }
            }
        }

        public event EventHandler LayoutSaveRequested;

        private void LayoutItemSaveRequested(object sender, EventArgs e)
        {
            OnSaveRequeted();
        }

        //private ObservableCollection<LayoutTable> _tables;
        //public ObservableCollection<LayoutTable> Tables
        //{
        //    get { return _tables; }
        //    set
        //    {
        //        _tables = value;
        //        if (_tables != null)
        //        {
        //            bindEventWithTables();
        //        }
        //    }
        //}

        //private ObservableCollection<LayoutField> _markers;
        //public ObservableCollection<LayoutField> Markers
        //{
        //    get { return _markers; }
        //    set
        //    {
        //        _markers = value;
        //        if (_markers != null)
        //        {
        //            bindEventWithMarkers();
        //        }
        //    }
        //}

        private LayoutTablesViewModel _layoutTables;

        internal LayoutTablesViewModel LayoutTables
        {
            get { return _layoutTables; }
            private set
            {
                if (_layoutTables != value)
                {
                    _layoutTables = value;
                    if (_layoutTables != null)
                    {
                        _layoutTables.SelectionChanged += layoutItem_SelectionChanged;
                        _layoutTables.ExpandChanged += table_ExpandChanged;
                        _layoutTables.LayoutSaveRequested += LayoutItemSaveRequested;
                        _layoutTables.DeletedHandler += layoutItem_DeletedHandler;
                    }
                }
            }
        }

        private void layoutItem_DeletedHandler(object sender, LayoutItemDeletedEventArgs e)
        {
            e.Layout = this;
            OnDeleted(sender, e);
        }

        private LayoutFieldsViewModel _markers;

        internal LayoutFieldsViewModel Markers
        {
            get { return _markers; }
            private set
            {
                if (_markers != value)
                {
                    _markers = value;
                    _markers.SelectionChanged += layoutItem_SelectionChanged;
                    _markers.ExpandChanged += marker_ExpandChanged;
                    _markers.LayoutSaveRequested += LayoutItemSaveRequested;
                    _markers.DeletedHandler += layoutItem_DeletedHandler;
                }
            }
        }

        public ObservableCollection<LayoutField> SystemIdentifiedFields { get; set; }
        public ObservableCollection<Line> Lines { get; set; }

        public event EventHandler DeleteRequested;

        //   public event EventHandler SelectionChanged;
        public RelayCommand Delete { get; set; }

        public ImageProcessingConfig Settings { get; set; }

        private IWarning _warning;

        public IWarning Warning
        {
            get
            {
                return _warning == null ? new GenericWarning(false, string.Empty, WarningType.NoWarning) : _warning;
            }
            set
            {
                if (_warning != value)
                {
                    _warning = value;
                    OnPropertyChanged(nameof(Warning));
                }
            }
        }

        public Layout()
        {
            LayoutFields = new LayoutFieldsViewModel("Fields");

            LayoutTables = new LayoutTablesViewModel("Tables");

            Markers = new LayoutFieldsViewModel("Markers");

            SystemIdentifiedFields = new ObservableCollection<LayoutField>();
            Lines = new ObservableCollection<Line>();
            DocumentProperties = new DocumentProperties();

            Delete = new RelayCommand(onDeleteRequested);

            _children = new ObservableCollection<ViewModelBase>();
            _children.Add(_markers);
            _children.Add(_layoutFields);
            _children.Add(_layoutTables);
        }

        public Layout(LayoutFieldsViewModel fieldModel, LayoutTablesViewModel tableModel, LayoutFieldsViewModel markerModel)
        {
            //Fields = new ObservableCollection<LayoutField>();
            LayoutFields = fieldModel ?? new LayoutFieldsViewModel("Fields");

            //  Tables = new ObservableCollection<LayoutTable>();
            LayoutTables = tableModel ?? new LayoutTablesViewModel("Tables");

            //  Markers = new ObservableCollection<LayoutField>();
            Markers = markerModel ?? new LayoutFieldsViewModel("Markers");

            SystemIdentifiedFields = new ObservableCollection<LayoutField>();
            Lines = new ObservableCollection<Line>();
            DocumentProperties = new DocumentProperties();

            Delete = new RelayCommand(onDeleteRequested);

            _children = new ObservableCollection<ViewModelBase>();
            _children.Add(_markers);
            _children.Add(_layoutFields);
            _children.Add(_layoutTables);
        }

        //private void bindEventWithMarkers()
        //{
        //    foreach (LayoutField marker in Markers.Fields)
        //    {
        //        marker.DeleteRequested += marker_DeleteRequested;
        //        marker.SelectionChanged += marker_SelectionChanged;
        //        marker.ExpandChanged += marker_ExpandChanged;
        //    }
        //}

        //private void bindEventWithTables()
        //{
        //    foreach (LayoutTable table in LayoutTables.Tables)
        //    {
        //        table.DeleteRequested += table_DeleteRequested;
        //        table.SelectionChanged += table_SelectionChanged;
        //        table.ExpandChanged += table_ExpandChanged;
        //    }
        //}

        //private void bindEventWithFields()
        //{
        //    foreach (LayoutField field in LayoutFields.Fields)
        //    {
        //        field.DeleteRequested += field_DeleteRequested;
        //        field.SelectionChanged += field_SelectionChanged;
        //        field.ExpandChanged += field_ExpandChanged;
        //    }
        //}

        //private void marker_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (SelectionChanged != null)
        //        SelectionChanged(sender, e);
        //}

        private void layoutItem_SelectionChanged(object sender, EventArgs e)
        {
            onSelectionChanged(sender, new LayoutItemSelectionEventArgs(sender) { Layout = this });
        }

        private void marker_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        //private void table_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (SelectionChanged != null)
        //        SelectionChanged(sender, e);
        //}

        private void table_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void field_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        //private void table_DeleteRequested(object sender, EventArgs e)
        //{
        //    LayoutTables.Tables.Remove(sender as LayoutTable);
        //    OnDeleted(new )
        //    OnSaveRequeted();

        //}

        //void Fields_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.NewItems != null)
        //    {
        //        foreach (LayoutField Item in e.NewItems)
        //        {
        //            Item.DeleteRequested += field_DeleteRequested;

        //        }
        //    }
        //}

        //private void field_DeleteRequested(object sender, EventArgs e)
        //{
        //    // Fields.Remove(sender as LayoutField);
        //}

        //void marker_DeleteRequested(object sender, EventArgs e)
        //{
        //    // Markers.Remove(sender as LayoutField);
        //}

        private void onDeleteRequested(object obj)
        {
            if (null != DeleteRequested)
            {
                DeleteRequested(this, EventArgs.Empty);
            }
        }

        private void OnSaveRequeted()
        {
            if (LayoutSaveRequested != null)
            {
                LayoutSaveRequested(this, EventArgs.Empty);
            }
        }

        #region DeletedEvent

        public event LayoutItemDeletedEventHandler DeletedHandler;

        private void OnDeleted(object sender, LayoutItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }

        #endregion DeletedEvent

        public SolidColorBrush ItemBackground { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void onSelectionChanged(object sender, LayoutItemSelectionEventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(sender, e);
            }
        }

        public SolidColorBrush GetColor(ElementType elementType)
        {
            switch (elementType)
            {
                case ElementType.Field:
                    return new SolidColorBrush(Color.FromArgb(255, 140, 198, 54));

                case ElementType.Table:
                    return new SolidColorBrush(Color.FromArgb(255, 255, 204, 0));

                case ElementType.Marker:
                    return new SolidColorBrush(Color.FromArgb(255, 247, 58, 58));

                default:
                    return new SolidColorBrush(Color.FromArgb(255, 159, 216, 241));
            }
        }

        public void ArrangeLayoutItems(DataModel iqBotDataModel)
        {
            try
            {
                _layoutFields.ArrangeFields(iqBotDataModel.Fields);
                _layoutTables.ArrangeTables(iqBotDataModel.Tables);
                foreach (TableDef table in iqBotDataModel.Tables)
                    _layoutTables.ArrangeTableColumns(table.Id, table.Columns);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Layout), nameof(ArrangeLayoutItems));
            }
        }

        public void ArrangeLayoutFields(IList<FieldDef> fields)
        {
            try
            {
                _layoutFields.ArrangeFields(fields);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Layout), nameof(ArrangeLayoutFields));
            }
        }

        public void ArrangeLayoutTables(IList<TableDef> tables)
        {
            try
            {
                _layoutTables.ArrangeTables(tables);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Layout), nameof(ArrangeLayoutTables));
            }
        }

        public void ArrangLayoutTableColums(string tableID, IList<FieldDef> tableColumns)
        {
            try
            {
                _layoutTables.ArrangeTableColumns(tableID, tableColumns);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Layout), nameof(ArrangLayoutTableColums));
            }
        }
    }
}