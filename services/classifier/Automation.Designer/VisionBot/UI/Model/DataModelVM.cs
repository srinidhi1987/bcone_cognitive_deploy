﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Automation.CognitiveData.VisionBot.UI.EventArguments;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class DataModelVM : INotifyPropertyChanged
    {
        
 		private IqBot _iqBot;
        private bool _shouldDisplayTableName;

        public bool ShouldDisplayTableName
        {
            get { return _shouldDisplayTableName; }
            set
            {
                if (_shouldDisplayTableName != value)
                {
                    _shouldDisplayTableName = value;
                    NotifyPropertyChanged("ShouldDisplayTableName");
                }
            }
        }

        private TableDef _tableDef;

        public TableDef TableDef
        {
            get { return _tableDef; }
            set
            {
                if (_tableDef != value)
                {
                    _tableDef = value;
                    NotifyPropertyChanged("TableDef");
                    DiplayTableNameOnListViewHandler();
                }
            }
        }


        private ObservableCollection<FieldDef> _fields;

        public ObservableCollection<FieldDef> Fields
        {
            get { return _fields; }
            set
            {
                if (_fields != value)
                {
                    _fields = value;
                    NotifyPropertyChanged("Fields");
                }
            }
        }

        private ObservableCollection<TableDef> _tables;

        public ObservableCollection<TableDef> Tables
        {
            get { return _tables; }
            set
            {
                if (_tables != value)
                {
                    SelectedTable = null;
                    _tables = value;
                    NotifyPropertyChanged("Tables");
                }
            }
        }

        private bool _tablesVisible = false;

        public bool TablesVisible
        {
            get { return _tablesVisible; }
            set
            {
                if (_tablesVisible != value)
                {
                    _tablesVisible = value;
                    NotifyPropertyChanged("TablesVisible");
                }
            }
        }

       
        private RelayCommand _addField;

        public RelayCommand AddField
        {
            get { return _addField; }
            set { _addField = value; }
        }

        private object _selectedTable;

        public object SelectedTable
        {
            get { return _selectedTable; }
            set
            {
                if (_selectedTable != value)
                {
                    _selectedTable = value;
                    NotifyPropertyChanged("SelectedTable");
                    
                }
            }
        }

        private object _selectedField;

        public object SelectedField
        {
            get { return _selectedField; }
            set
            {
                if (_selectedField != value)
                {
                    _selectedField = value;
                    NotifyPropertyChanged("SelectedField");
                    if (_selectedField != null)
                    {
                        DiplayTableNameOnListViewHandler();
                        if (_selectedField is FieldDef)
                        {
                            if (_tableDef != null)
                                OnUpdateTableCollumnRequested(_tableDef as TableDef, _selectedField as FieldDef);
                            else
                                OnUpdateFieldRequested(_selectedField as FieldDef);
                        }
                    }
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<FieldDef> AddFieldRequested;
        public event Action<TableDef, FieldDef> AddTableCollumnRequested;
        public event Action<FieldDef> UpdateFieldRequested;
        public event Action<TableDef> UpdateTableRequested;
        public event Action<TableDef, FieldDef> UpdateTableCollumnRequested;
        public event EventHandler FieldsOrderChanged;
        public event EventHandler TablesOrderChanged;
        public event EventHandler<ColumnOrderChangeArgs> TableColumnsOrderChanged;

        public DataModelVM(IqBot iqBot)
        {
            _iqBot = iqBot;
            _addField = new RelayCommand(AddFieldHandler);
        }

        //TODO :Make differ function for Tablefield And FieldDef
        public void AddFieldHandler(object param)
        {
            FieldDef fieldDef = new FieldDef();
            if (_tableDef == null)
            {
                fieldDef.Id = new IqBotIDGenerationHelper().GetNewFieldId(_iqBot);
                OnAddFieldRequested(fieldDef);
            }
            else
            {
                fieldDef.Id = new IqBotIDGenerationHelper().GetNewColumnId(_iqBot);
                OnAddTableCollumnRequested(_tableDef, fieldDef);
            }
        }
                
        public void MoveField(FieldDef field, int moveToIndex)
        {
            moveItem<FieldDef>(Fields, field, moveToIndex);
            if (_tableDef == null)
            {
                OnFieldsOrderChanged();
            }
            else
            {
                OnTableColumnsOrderChanged(_tableDef);
            }
        }
                
        public void MoveTable(TableDef table, int moveToIndex)
        {
            moveItem<TableDef>(Tables, table, moveToIndex);
            OnTablesOrderChanged();
        }
                
        public void SelectTableInLeftPane(TableDef table)
        {
            if (table != null)
            {
                SelectedTable = table;
                OnUpdateTableRequested(table);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
        private void OnAddFieldRequested(FieldDef fieldDef)
        {
            SelectedField = null;
            if (AddFieldRequested != null)
            {
                AddFieldRequested(fieldDef);
            }
        }

        private void OnAddTableCollumnRequested(TableDef tableDef, FieldDef fieldDef)
        {
            SelectedField = null;
            if (AddTableCollumnRequested != null)
            {
                AddTableCollumnRequested(tableDef, fieldDef);
            }
        }

        private void OnUpdateFieldRequested(FieldDef fieldDef)
        {
            if (UpdateFieldRequested != null)
            {
                UpdateFieldRequested(fieldDef);
            }
        }

        private void OnUpdateTableRequested(TableDef tableDef)
        {
            if (UpdateTableRequested != null)
            {
                UpdateTableRequested(tableDef);
            }
        }

        private void OnUpdateTableCollumnRequested(TableDef tableDef, FieldDef fieldDef)
        {
            if (UpdateTableCollumnRequested != null)
            {
                UpdateTableCollumnRequested(tableDef, fieldDef);
            }
        }

        private void DiplayTableNameOnListViewHandler()
        {
            if (TableDef != null)
            {
                //DisplayTableName = ((TableDef)TableDef).Name;
                //DisplayTableID = ((TableDef)TableDef).Id;
                ShouldDisplayTableName = true;
            }
            else
            {
                //DisplayTableName = string.Empty;
                //DisplayTableID = string.Empty;
                ShouldDisplayTableName = false;
            }
        }
                
        private void moveItem<T>(ObservableCollection<T> collection, T item, int moveToIndex)
        {
            if (collection != null && item != null && collection.Contains(item) && moveToIndex < collection.Count)
            {
                int oldIndex = collection.IndexOf(item);
                collection.Move(oldIndex, moveToIndex);
            }
        }     

		public void OnFieldsOrderChanged()
        {
            if (FieldsOrderChanged != null)
            {
                FieldsOrderChanged(this, new EventArgs());
            }
        }

        public void OnTablesOrderChanged()
        {
            if (TablesOrderChanged != null)
            {
                TablesOrderChanged(this, new EventArgs());
            }
        }

        public void OnTableColumnsOrderChanged(TableDef table)
        {
            if (TableColumnsOrderChanged != null)
            {
                TableColumnsOrderChanged(this, new ColumnOrderChangeArgs(table));
            }
        }              
    }
    public class ColumnOrderChangeArgs : EventArgs
    {
        public TableDef TableDef { get; private set; }   
		
		public ColumnOrderChangeArgs(TableDef tableDef)
        {
            TableDef = tableDef;
        }
    }
}
