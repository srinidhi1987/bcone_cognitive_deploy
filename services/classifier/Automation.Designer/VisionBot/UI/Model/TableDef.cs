﻿using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class TableDef : INotifyPropertyChanged
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
                IsDirty = true;
            }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
                IsDirty = true;
            }
        }

        private bool _isSelected = false;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }

        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {

                _isExpanded = value;
                if (_isExpanded)
                {
                    NotifyPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }

        private bool _isDirty;

        public bool IsDirty
        {
            get { return _isDirty; }
            private set
            {
                if (_isDirty != value)
                {
                    _isDirty = value;
                    NotifyPropertyChanged("IsDirty");
                }
            }
        }

        private ObservableCollection<FieldDef> _columns;

        public ObservableCollection<FieldDef> Columns
        {
            get { return _columns; }
            set
            {
                if (_columns != value)
                {
                    _columns = value;
                    if (_columns != null)
                    {
                        bindEventWithFields();
                        _columns.CollectionChanged += _columns_CollectionChanged;
                    }
                }
            }
        }

        private void _columns_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.OldStartingIndex == -1)
            {
                foreach (FieldDef column in e.NewItems)
                {
                    column.DeleteRequested += column_DeleteRequested;
                    column.SelectionChanged += column_SelectionChanged;
                    column.ExpandChanged += column_ExpandChanged;
                }
            }
        }

        public event EventHandler SelectionChanged;
        public event EventHandler ExpandChanged;
        public event EventHandler SaveRequested;
        public event BluePrintItemDeletedEventHandler DeletedHandler;
        public event EventHandler DeleteRequested;
        public event PropertyChangedEventHandler PropertyChanged;

        public RelayCommand Delete { get; set; }

        public IDeleteOparationWarnings DeleteWarning;

        public TableDef()
        {
            Columns = new ObservableCollection<FieldDef>();
            Delete = new RelayCommand(onDeleteRequested);
            DeleteWarning = new BluePrintDeleteWarning();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ResetDirtyFlag()
        {
            IsDirty = false;
        }

        private void bindEventWithFields()
        {
            foreach (FieldDef column in Columns)
            {
                column.DeleteRequested += column_DeleteRequested;
                column.SelectionChanged += column_SelectionChanged;
                column.ExpandChanged += column_ExpandChanged;
            }
        }

        private void column_DeleteRequested(object sender, EventArgs e)
        {
            FieldDef fieldDef = sender as FieldDef;
            if (DeleteWarning.Delete(string.Format(Properties.Resources.DataModelTableCollumnRemoveWarning, fieldDef.Name,
                Properties.Resources.LayoutLabel)))
            {
                Columns.Remove(sender as FieldDef);
                DeletedHandler(this, new BluePrintItemDeletedEventArgs(BluePrintItemDeleteMode.TableField) { Field = fieldDef });
                OnSaveRequeted();
            }
        }

        private void column_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void column_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, e);
        }

        #region On Event

        private void onDeleteRequested(object obj)
        {
            if (null != DeleteRequested)
            {
                DeleteRequested(this, EventArgs.Empty);
            }
        }
        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private void OnSaveRequeted()
        {
            if (SaveRequested != null)
            {
                SaveRequested(this, EventArgs.Empty);
            }
        }

        #endregion



    }
}
