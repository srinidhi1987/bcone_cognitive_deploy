﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;
using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class LayoutTable : INotifyPropertyChanged
    {
        IDeleteOparationWarnings deletPrimaryColumn;

        public string Id { get; set; }
        public TableDef TableDef { get; set; }
        private LayoutField _primaryField;

        public LayoutField PrimaryField
        {
            get { return _primaryField; }
            set
            {
                if (_primaryField != value)
                {
                    if (_primaryField != null)
                    {
                        _primaryField.IsPrimary = false;
                    }
                    _primaryField = value;

                    if (_primaryField != null)
                    {
                        _primaryField.IsPrimary = true;
                    }
                }
            }
        }

        private ObservableCollection<LayoutField> _fields;
        public ObservableCollection<LayoutField> Fields
        {
            get { return _fields; }
            set
            {
                if (_fields != value)
                {
                    _fields = value;
                    if (_fields != null)
                    {
                        bindEventWithFields(_fields);
                        _fields.CollectionChanged += _fields_CollectionChanged;
                    }
                }
            }
        }

        private void bindEventWithFields(ObservableCollection<LayoutField> fields)
        {
            foreach (LayoutField field in fields)
            {
                field.DeleteRequested += field_DeleteRequested;
                field.SelectionChanged += field_SelectionChanged;
                field.ExpandChanged += field_ExpandChanged;
            }
        }

        void _fields_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (LayoutField field in e.NewItems)
                {
                    field.DeleteRequested += field_DeleteRequested;
                    field.SelectionChanged += field_SelectionChanged;
                    field.ExpandChanged += field_ExpandChanged;
                }
            }
        }

        public LayoutField Footer { get; set; }

        public event EventHandler DeleteRequested;
        public RelayCommand Delete { get; set; }

        public event EventHandler LayoutSaveRequested;

        private bool _isValueMappingEnabled = false;
        public bool IsValueMappingEnabled
        {
            get { return _isValueMappingEnabled; }
            set { _isValueMappingEnabled = value; }
        }
        
        public LayoutTable()
        {
            Fields = new ObservableCollection<LayoutField>();
            ItemBackground = FieldTree.NormalColorBrush;
            Footer = new LayoutField();
            Footer.SimilarityFactor = 0.7f;
            Footer.MergeRatio = 1f;
            IsValueMappingEnabled = false;
            Delete = new RelayCommand(onDeleteRequested);
            deletPrimaryColumn = new LayoutPrimaryFieldDeleteWarning();

            foreach (LayoutField field in Fields)
            {
                field.DeleteRequested += field_DeleteRequested;
            }
        }


        private void field_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, e);
        }

        private void field_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void field_DeleteRequested(object sender, EventArgs e)
        {
            LayoutField layoutField = sender as LayoutField;

            if (PrimaryField == layoutField)
            {
                if (Fields.Count > 1)
                {
                    deletPrimaryColumn.Delete(Properties.Resources.LayoutTablePrimaryColumnRemoveMessage);
                    return;
                }
                else
                {
                    removeFieldFromTable(layoutField);
                    DeleteRequested(this, EventArgs.Empty);

                }
            }
            else
            {

                removeFieldFromTable(layoutField);
                if (Fields.Count == 0)
                {
                    DeleteRequested(this, EventArgs.Empty);
                }
                else
                    OnSaveRequeted();
            }
        }

        private void removeFieldFromTable(LayoutField layoutField)
        {
            Fields.Remove(layoutField);
            OnDeleted(this, new LayoutItemDeletedEventArgs(LayoutItemDeleteMode.TableField) { Field = layoutField, Table = this });
        }

        private void onDeleteRequested(object obj)
        {
            if (DeleteRequested != null)
            {
                DeleteRequested(this, EventArgs.Empty);
            }
        }

        private void OnSaveRequeted()
        {
            if (LayoutSaveRequested != null)
            {
                LayoutSaveRequested(this, EventArgs.Empty);
            }
        }

        public SolidColorBrush ItemBackground { get; set; }

        public int Height { get; set; }

        public int Left { get; set; }

        public int Top { get; set; }

        public ElementType Type { get; set; }

        public int Width { get; set; }

        public event EventHandler SelectionChanged;
        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    onPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }

        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        public event EventHandler ExpandChanged;
        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {

                _isExpanded = value;
                if (_isExpanded)
                {
                    onPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region DeletedEvent
        public event LayoutItemDeletedEventHandler DeletedHandler;
        private void OnDeleted(object sender, LayoutItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }
        #endregion
    }
}