﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Automation.VisionBotEngine;

namespace Automation.CognitiveData.VisionBot.UI.Extensions
{
    internal static class DataSetExtensions
    {
        public static void SetTableOrder(this DataSet dataSet, IList<string> tableNames)
        {
            if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && tableNames != null)
            {
                IList<DataTable> dataTables = new List<DataTable>();
                for (int i = 0; i < dataSet.Tables.Count; i++)
                {
                    dataTables.Add(dataSet.Tables[i]);
                }

                dataSet.Tables.Clear();

                for (int i = 0; i < tableNames.Count; i++)
                {
                    DataTable dataTable = (from q in dataTables where q.TableName == tableNames[i] select q).FirstOrDefault();
                    if (dataTable != null)
                    {
                        dataTables.Remove(dataTable);
                        dataSet.Tables.Add(dataTable);
                    }
                    else
                    {
                        VBotLogger.Error(() => $"[SetTableOrder] Could not find table name: {tableNames[i]} in dataTables.");
                    }
                }

                for (int i = 0; i < dataTables.Count; i++)
                {
                    dataSet.Tables.Add(dataTables[i]);
                }
            }
            else
            {
                VBotLogger.Error(() => $"[SetTableOrder] dataSet or dataSet.Tables or tableNames not present.");
            }
        }

        public static IList<string> GetOrderedTableNames(this DataSet dataSet)
        {
            IList<string> orderedTableNames = new List<string>();
            if (dataSet?.Tables != null)
            {
                for (int i = 0; i < dataSet.Tables.Count; i++)
                {
                    orderedTableNames.Add(dataSet.Tables[i].TableName);
                }
            }
            return orderedTableNames;
        }
    }
}
