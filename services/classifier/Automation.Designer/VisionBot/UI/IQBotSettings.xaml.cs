﻿using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for IQBotSettings.xaml
    /// </summary>
    public partial class IQBotSettings : Window
    {
        public IQBotSettings(IQBotSettingsViewModel settingsViewModel)
        {
            InitializeComponent();
            settingsViewModel.SaveRequested += SaveSettings;
            settingsViewModel.CancelRequested += CancelSettings;
        }

        void SaveSettings(object sender, EventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        void CancelSettings(object sender, EventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void BinarizationThresholdTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                TextBox binarizationThresholdValue = sender as TextBox;
                string text = binarizationThresholdValue.Text + e.Text;
                int value = Convert.ToInt32(text);
                if (value < 0 || value > 255)
                    e.Handled = true;
            }
            catch
            {
                e.Handled = true;
            }
        }

        private void BinarizationThresholdTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            validateBinarizationThresoldText();
        }

        //ToDo: Need to move this in viewModel
        private bool validate()
        {
            bool result = true;
            result = validateBinarizationThresoldText();
            return result;
        }

        private bool validateBinarizationThresoldText()
        {
            bool result = true;
            int value = 147;
            bool isParsed = Int32.TryParse(BinarizationThresholdTextBox.Text, out value);
            if (!isParsed || value.ToString().Length == 0 || value < 0 || value > 255)
            {
                result = false;
                BinarizationThresholdTextBox.Text = "147";
                BinarizationThresholdTextBox.Focusable = true;
                BinarizationThresholdTextBox.Focus();
            }
            return result;

        }

    }
}
