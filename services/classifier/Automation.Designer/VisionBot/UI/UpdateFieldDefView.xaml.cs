﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for UpdateFieldDefView.xaml
    /// </summary>
    public partial class UpdateFieldDefView : UserControl
    {
        public UpdateFieldDefView()
        {
            InitializeComponent();
        }
        
        private void UserControl_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            UpdateFieldDef dataContext = e.NewValue as UpdateFieldDef;
            if(dataContext != null && dataContext.FieldDef != null && string.IsNullOrWhiteSpace(dataContext.FieldDef.Name))
            {
                txtNameValue.Focus();
            }
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNameValue.Text))
            {
                txtNameValue.Focus();
            }
        }
    }
}
