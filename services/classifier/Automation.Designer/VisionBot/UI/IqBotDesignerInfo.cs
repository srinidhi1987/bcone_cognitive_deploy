/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class IqBotDesignerInfo : INotifyPropertyChanged
    {
        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("FileName"));
                }
            }
        }

        private string title;

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Title"));
                }
            }
        }

        //public string Title { get; set; }
        private string layoutName;

        public string LayoutName
        {
            get { return layoutName; }
            set
            {
                layoutName = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("LayoutName"));
                }
            }
        }

        private string documentName;

        public string DocumentName
        {
            get { return documentName; }
            set
            {
                documentName = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("DocumentName"));
                }
            }
        }

        public ObservableCollection<RibbonTabItem> RibbonTabItems { get; set; }

        public ObservableCollection<RibbonTabButton> RibbonTabButtons { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}