﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI
{
    public delegate void OnTabChangeHandler(object sender, RoutedEventArgs e);

    public delegate void SaveRequestHandler(object sender, SaveRequestEventArgs e);

    public class SaveRequestEventArgs : EventArgs
    {
        public IqBot IqBot { get; private set; }

        public SaveRequestEventArgs(IqBot iqBot)
        {
            IqBot = iqBot;
        }
    }
}
