﻿using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for ErrorMessageDialog.xaml
    /// </summary>
    public partial class ErrorMessageDialog : Window
    {

        public ErrorMessageDialog()
        {
            InitializeComponent();
        }

        private void Button_OnClick(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                if (button.IsCancel)
                    DialogResult = false;
                else if (button.IsDefault)
                    DialogResult = true;
                Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Left = Left - 10;
        }

        private void Button1_OnClick(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                if (button.IsCancel)
                    DialogResult = false;
                else if (button.IsDefault)
                    DialogResult = true;

                setSelectedAction(button);
                Close();
            }
        }

        private void setSelectedAction(Button button)
        {
            var errorModel = DataContext as ErrorModel;
            var buttonModel = button.DataContext as ButtonModel;
            if (errorModel != null && buttonModel != null)
            {
                errorModel.SelectedAction = buttonModel;
            }
        }
    }
}
