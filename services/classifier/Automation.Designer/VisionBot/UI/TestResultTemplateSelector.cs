﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows;
using System.Windows.Controls;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    internal class TestResultTemplateSelector : DataTemplateSelector
    {

        private const string SingleTestViewKey = "SingleTestView";
        private const string MultiTestViewKey = "MultiTestView";

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var contentControl = container as ContentPresenter;
            if (contentControl != null)
            {
                if (item is TestResultViewModel)
                {
                    return contentControl.FindResource(SingleTestViewKey) as DataTemplate;
                }

                if (item is SummaryViewModel)
                {
                    return contentControl.FindResource(MultiTestViewKey) as DataTemplate;
                }
            }

            return null;
        }
    }
}