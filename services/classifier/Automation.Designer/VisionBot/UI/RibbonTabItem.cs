﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.ComponentModel;
using System.Windows.Media.Imaging;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class RibbonTabItem : ViewModelBase
    {
        private bool _isSelected;

        private bool _isEnabled;

        public string Caption { get; set; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(ImageSource));
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                OnPropertyChanged(nameof(IsEnabled));
            }
        }

        public BitmapImage ImageSource
        {
            get
            {
                VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();

                if (_isSelected)
                {
                    return vbotResourceHelper.GetImage(ActiveImageSource);
                }

                return vbotResourceHelper.GetImage(InactiveImageSource);
            }
        }

        public string ActiveImageSource { get; set; }

        public string InactiveImageSource { get; set; }

      }
}
