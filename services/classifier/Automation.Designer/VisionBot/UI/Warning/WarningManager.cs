﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal interface IWarningManager
    {
       void UpdateLayoutWarning(Layout layout, DataModel dataModel);
    }

    internal class WarningManager : IWarningManager
    {
        #region Public Methods

        public void UpdateLayoutWarning(Layout layout, DataModel dataModel)
        {
            IVBotWarning layoutWarning = new LayoutWarning(layout, dataModel);

            List<Warning> warnings = layoutWarning.GetWarnings();
            setLayoutWarning(layout, warnings);
        }

        #endregion Public Methods

        #region Private Methods

        private void setLayoutWarning(Layout layout, List<Warning> Warnings)
        {
            layout.Warning = ConvertToGenericWarning(Warnings);
        }

        private IWarning ConvertToGenericWarning(List<Warning> warningList)
        {
            IWarning layoutwarning = new GenericWarning(false, string.Empty, WarningType.NoWarning);

            var warningMessage = getWarningMessageForMapping(warningList);
            string tablePropertiesWarningMessage = getWarningMessageForMissingProperties(warningList);

            if (string.IsNullOrWhiteSpace(warningMessage))
            {
                warningMessage = tablePropertiesWarningMessage;
            }
            else if (!string.IsNullOrWhiteSpace(tablePropertiesWarningMessage))
            {
                warningMessage += Environment.NewLine + tablePropertiesWarningMessage;
            }

            if (!string.IsNullOrWhiteSpace(warningMessage))
                layoutwarning = new GenericWarning(true, warningMessage, WarningType.Layout);

            return layoutwarning;
        }

        private string getWarningMessageForMapping(List<Warning> warningList)
        {
            var filterWarningList = warningList.Where(w => w.WarningType.Equals(WarningTypes.FieldsNotMapped) || w.WarningType.Equals(WarningTypes.ColumnsNotMapped)).ToList();

            if (filterWarningList.Count > 0)
            {
                return GetWarningMessageFromType(WarningTypes.FieldsNotMapped);
            }

            return string.Empty;
        }

        private string getWarningMessageForMissingProperties(List<Warning> warningList)
        {
            var filterWarningList = warningList.Where(w => w.WarningType.Equals(WarningTypes.ReferenceColumnMissing)
                   || w.WarningType.Equals(WarningTypes.TableFooterMissing)).ToList();

            if (filterWarningList.Count > 0)
            {
                return GetWarningMessageFromType(WarningTypes.ReferenceColumnMissing);
            }

            return string.Empty;
        }

        private string GetWarningMessageFromType(WarningTypes? warningType)
        {
            switch (warningType)
            {
                case WarningTypes.FieldsNotMapped:
                case WarningTypes.ColumnsNotMapped:
                    return Properties.Resources.MandatoryFieldNotMappedWarningMessage;

                case WarningTypes.ReferenceColumnMissing:
                case WarningTypes.TableFooterMissing:
                    return Properties.Resources.ReferenceColumnFooterNotDefinedWarningMessage;

                default:
                    return string.Empty;
            }
        }

        #endregion Private Methods
    }
}