﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;
    using System.Linq;

    internal class TableColumnMappingVerifier : IVerifier
    {
        private LayoutTable _layoutTable;

        private TableDef _blueprintTable;

        public TableColumnMappingVerifier(LayoutTable layoutTable, TableDef blueprintTable)
        {
            _layoutTable = layoutTable;
            _blueprintTable = blueprintTable;
        }

        public Warning Verify()
        {
            List<object> NotMappedColumns = new List<object>();

            foreach (var blueprintColumn in _blueprintTable.Columns)
            {
                if (blueprintColumn.IsRequired)
                {
                    if (_layoutTable == null)
                    {
                        NotMappedColumns.Add(blueprintColumn);
                    }
                    else
                    {
                        LayoutField layoutField =
                            _layoutTable.Fields.ToList().Find(f => f.FieldDef.Id.Equals(blueprintColumn.Id));
                        if (layoutField == null) NotMappedColumns.Add(blueprintColumn);
                    }
                }
            }

            return NotMappedColumns.Count > 0 ? new Warning(WarningTypes.ColumnsNotMapped, NotMappedColumns) : new Warning();
        }

    }
}
