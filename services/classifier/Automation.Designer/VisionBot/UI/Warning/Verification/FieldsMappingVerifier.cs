﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;

    internal class FieldsMappingVerifier : IVerifier
    {
        private List<LayoutField> _layoutFields;

        private List<FieldDef> _blueprintFields;

        public FieldsMappingVerifier(List<LayoutField> layoutFields, List<FieldDef> blueprintFields)
        {
            _layoutFields = layoutFields;
            _blueprintFields = blueprintFields;
        }

        public Warning Verify()
        {
            List<object> NotMappedFields = new List<object>();

            foreach (var blueprintField in _blueprintFields)
            {
                if (blueprintField.IsRequired)
                {
                    LayoutField layoutField = _layoutFields.Find(f => f.FieldDef.Id.Equals(blueprintField.Id));
                    if (layoutField == null)
                    {
                        NotMappedFields.Add(blueprintField);
                    }
                }
            }


            return NotMappedFields.Count > 0 ? new Warning(WarningTypes.FieldsNotMapped, NotMappedFields) : new Warning();
        }
    }
}
