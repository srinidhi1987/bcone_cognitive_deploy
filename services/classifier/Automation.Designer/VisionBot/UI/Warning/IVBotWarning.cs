﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;
    using System.Linq;
    
    internal interface IVBotWarning
    {
        List<Warning> GetWarnings();
    }

    internal abstract class VBotWarning :IVBotWarning
    {
        public abstract List<Warning> GetWarnings();
      
        public List<Warning> mergeWarnings(List<Warning> warningList)
        {
            List<Warning> filterWarningList = new List<Warning>();

            foreach (var warning in warningList)
            {
                if (warning == null || warning.WarningType == null)
                {
                    continue;
                }

                bool isExists = false;
                foreach (var filterWarning in filterWarningList.Where(filterWarning => filterWarning.WarningType == warning.WarningType))
                {
                    isExists = true;
                    filterWarning.WarningObjectList.Add(warning.WarningObjectList);
                }

                if (!isExists)
                {
                    filterWarningList.Add(warning);
                }
            }

            return filterWarningList;
        }
    }
    
}
