﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

//namespace Automation.CognitiveData.VisionBot.UI.Behaviors
//{
//    using Automation.CognitiveData.Validator.UI.Control;
//    using Automation.CognitiveData.VisionBot.UI.Adorners;
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;
//    using System.Threading.Tasks;
//    using System.Windows;
//    using System.Windows.Controls;
//    using System.Windows.Documents;
//    using System.Windows.Media;

//    public class BusyBehavior : DependencyObject
//    {
//        public static DependencyProperty IsBusyProperty =
//            DependencyProperty.RegisterAttached("IsBusy",
//                                                typeof(bool),
//                                                typeof(BusyBehavior),
//                                                new PropertyMetadata(false, new PropertyChangedCallback(OnPropertyChangedCallback)));

//        public static void SetIsBusy(DependencyObject obj, bool value)
//        {
//            obj.SetValue(IsBusyProperty, value);
//        }

//        public static bool GetIsBusy(DependencyObject obj)
//        {
//            return (bool)obj.GetValue(IsBusyProperty);
//        }

//        private static void OnPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
//        {
//            if (e.NewValue != null && e.NewValue is bool)
//            {
//                bool value = (bool)e.NewValue;
//                if (value)
//                {
//                    if ((d as UserControl).IsLoaded)
//                    {
//                        AddAdorner(d as UIElement);
//                    }
//                    else
//                    {
//                        (d as UserControl).Loaded += BusyBehavior_Loaded;
//                    }
//                }
//                else
//                {
//                    RemoveAdorner(d as UIElement);
//                }
//            }
//        }

//        private static void BusyBehavior_Loaded(object sender, RoutedEventArgs e)
//        {
//            (sender as UserControl).Loaded -= BusyBehavior_Loaded;
//            if (BusyBehavior.GetIsBusy((sender as UserControl)))
//            {
//                AddAdorner(sender as UserControl);
//            }
//        }

//        private static ControlHostAdorner GetAdorner(UIElement uiElement)
//        {
//            ControlHostAdorner ca = null;
//            AdornerLayer al = AdornerLayer.GetAdornerLayer(uiElement);
//            if (al != null)
//            {
//                Adorner[] adorners = al.GetAdorners(uiElement);
//                if (adorners != null && adorners.Length > 0)
//                {
//                    ca = (from q in adorners where q is ControlHostAdorner select q as ControlHostAdorner).FirstOrDefault();
//                }
//            }

//            return ca;
//        }

//        private static void AddAdorner(UIElement uiElement)
//        {
//            ControlHostAdorner ca = GetAdorner(uiElement);
//            if (ca == null)
//            {
//                AdornerLayer al = AdornerLayer.GetAdornerLayer(uiElement);
//                if (al != null)
//                {
//                    al.Add(new ControlHostAdorner(uiElement, new LoadingControl()));
//                }
//            }
//        }

//        private static void RemoveAdorner(UIElement uiElement)
//        {
//            ControlHostAdorner ca = GetAdorner(uiElement);
//            if (ca != null)
//            {
//                AdornerLayer al = AdornerLayer.GetAdornerLayer(uiElement);
//                if (al != null)
//                {
//                    al.Remove(ca);
//                }
//            }
//        }
//    }
//}