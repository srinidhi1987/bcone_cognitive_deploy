﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.Helpers;
using Automation.CognitiveData.VisionBot.UI.Adorners;
using Automation.CognitiveData.VisionBot.UI.EventArguments;
using Automation.CognitiveData.VisionBot.UI.Extensions;
using Automation.VisionBotEngine;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.Behaviors
{
    //Credit for DataGridDragDropBehavior class : Majority code was taken from http://www.codeproject.com/Articles/17266/Drag-and-Drop-Items-in-a-WPF-ListView
    //It is a Drag Drop sample implemented by Josh Smith in CODE PROJECT
    internal class DataGridDragDropBehavior<ItemType> where ItemType : class
    {
        private const double _dropTargetIndicatorMargin = 30.0;

        private DataGrid _dataGrid;
        private DataGridRowInfo _dragStartInfo;
        private DataGridRow _lastAnimatedRowInfo;
        private Thickness _originalMarginForRow;
        private bool _isDragging;
        private DragAdorner _dragAdorner;
        private int? _dropTargetIndex;

        public DataGrid DataGrid
        {
            get
            {
                return _dataGrid;
            }
        }

        public bool IsDragging
        {
            get
            {
                return _isDragging;
            }
        }

        public event EventHandler<DropArgs> Drop;

        public void Attach(DataGrid dataGrid)
        {
            if (dataGrid == null)
            {
                throw new ArgumentNullException("dataGrid");
            }

            if (_dataGrid != null && _dataGrid != dataGrid)
            {
                throw new Exception("DataGridDragDropBehavior is already attached with some other DataGrid.");
            }

            _dataGrid = dataGrid;
        }

        public void StartDrag(object dragData)
        {
            try
            {
                throwIfNotAttached();
                StartDrag(_dataGrid.SelectedItem as ItemType, dragData);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(StartDrag), "Exception in StartDrag(object)");
            }
        }

        public void StartDrag(ItemType item, object dragData)
        {
            try
            {
                throwIfNotAttached();
                DataGridRow dataGridRow = this._dataGrid.GetDataGridRow(item);
                StartDrag(dataGridRow, dragData);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(StartDrag), "Exception in StartDrag(ItemType, object)");
            }
        }

        public void StartDrag(int itemIndex, object dragData)
        {
            try
            {
                throwIfNotAttached();
                DataGridRow dataGridRow = this._dataGrid.GetDataGridRow(itemIndex);
                DataGridRowInfo dataGridRowInfo = new DataGridRowInfo(itemIndex, dataGridRow);
                startDragInternal(dataGridRowInfo, dragData);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(StartDrag), "Exception in StartDrag(int, object)");
            }
        }

        public void StartDrag(DataGridRow dataGridRow, object dragData)
        {
            try
            {
                throwIfNotAttached();
                int? itemIndex = this._dataGrid.GetDataGridRowIndex(dataGridRow);
                DataGridRowInfo dataGridRowInfo = new DataGridRowInfo(itemIndex, dataGridRow);
                startDragInternal(dataGridRowInfo, dragData);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(StartDrag), "Exception in StartDrag(DataGridRow, object)");
            }
        }

        private void startDragInternal(DataGridRowInfo dataGridRowInfo, object dragData)
        {
            _dragStartInfo = dataGridRowInfo;
            if (_dragStartInfo != null)
            {
                try
                {
                    initializeDragDrop();
                    DragDropEffects de = DragDrop.DoDragDrop(_dataGrid, dragData, DragDropEffects.Move);
                }
                finally
                {
                    cleanupDragDrop();
                }
            }
        }

        private void cleanupDragDrop()
        {
            undoRowMarginChange();
            clearAdornerLayer();
            unsubscribeDataGridDragEvents();
            _isDragging = false;
            _dragStartInfo = null;
            _lastAnimatedRowInfo = null;
            _dropTargetIndex = null;
            _originalMarginForRow = new Thickness(0, 0, 0, 0);
        }

        private void initializeDragDrop()
        {
            subscribeDataGridDragEvents();
            initializeAdornerLayer(_dragStartInfo.DataGridRow);
            _dropTargetIndex = null;
            _isDragging = true;
            _lastAnimatedRowInfo = null;
            _originalMarginForRow = _dragStartInfo.DataGridRow != null ? _dragStartInfo.DataGridRow.Margin : new Thickness(0, 0, 0, 0);
        }

        private bool isAttachedToDataGrid()
        {
            return _dataGrid != null;
        }

        private void throwIfNotAttached()
        {
            if (!isAttachedToDataGrid())
            {
                throw new Exception("Behavior is not attached to any DataGrid.");
            }
        }

        private void initializeAdornerLayer(DataGridRow itemToDrag)
        {
            VisualBrush brush = new VisualBrush(itemToDrag);
            this._dragAdorner = new DragAdorner(_dataGrid, itemToDrag.RenderSize, brush);
            this._dragAdorner.Opacity = 0.8;
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(_dataGrid);
            layer.Add(this._dragAdorner);
        }

        private void clearAdornerLayer()
        {
            if (_dragAdorner != null)
            {
                AdornerLayer layer = AdornerLayer.GetAdornerLayer(_dataGrid);
                layer.Remove(_dragAdorner);
                _dragAdorner = null;
            }
        }

        private void subscribeDataGridDragEvents()
        {
            _dataGrid.GiveFeedback += dataGrid_GiveFeedback;
            _dataGrid.PreviewDragOver += dataGrid_DragOver;
            _dataGrid.DragLeave += dataGrid_DragLeave;
            _dataGrid.PreviewDragEnter += dataGrid_DragEnter;
            _dataGrid.Drop += dataGrid_Drop;
        }

        private void unsubscribeDataGridDragEvents()
        {
            _dataGrid.GiveFeedback -= dataGrid_GiveFeedback;
            _dataGrid.PreviewDragOver -= dataGrid_DragOver;
            _dataGrid.DragLeave -= dataGrid_DragLeave;
            _dataGrid.PreviewDragEnter -= dataGrid_DragEnter;
            _dataGrid.Drop -= dataGrid_Drop;
        }

        private void dataGrid_DragOver(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                Point p = MouseHelper.GetMousePosition(_dataGrid);
                _dragAdorner.SetOffsets(p.X, p.Y);
                identifyDropTargetForPoint(p);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(dataGrid_DragOver));
            }
            finally
            {
                e.Handled = true;
            }
        }

        private void identifyDropTargetForPoint(Point mousePoint)
        {
            DataGridRow rowUnderDragCursor = this._dataGrid.GetDataGridRow(mousePoint);
            int? rowUnderDragCursorIndex = this._dataGrid.GetDataGridRowIndex(rowUnderDragCursor);
            if (rowUnderDragCursor != null && rowUnderDragCursorIndex.HasValue)
            {
                DataGridRowInfo rowUnderDragCursorInfo = new DataGridRowInfo(rowUnderDragCursorIndex, rowUnderDragCursor);
                _dropTargetIndex = calculateDropTargetIndexAndAnimate(mousePoint, rowUnderDragCursorInfo);
            }
        }

        private int? calculateDropTargetIndexAndAnimate(Point mousePoint, DataGridRowInfo rowUnderDragCursorInfo)
        {
            int? dropTargetIndex = rowUnderDragCursorInfo?.Index != null ? rowUnderDragCursorInfo.Index.Value : (int?)null;
            bool isMouseInUpperHalfOfRow = isMouseCursorInUpperHalfOfRow(mousePoint, rowUnderDragCursorInfo.DataGridRow);
            if (rowUnderDragCursorInfo.Index == _dragStartInfo.Index)
            {
                dropTargetIndex = null;
                undoRowMarginChange();
            }
            else if (rowUnderDragCursorInfo.Index > _dragStartInfo.Index)
            {
                bool changeUpperMargin = false;
                if (rowUnderDragCursorInfo.Index != _dragStartInfo.Index + 1 && isMouseInUpperHalfOfRow)
                {
                    dropTargetIndex = dropTargetIndex - 1;
                    changeUpperMargin = true;
                }

                changeRowMarginForAnimatedEffect(rowUnderDragCursorInfo.DataGridRow, changeUpperMargin);
            }
            else if (rowUnderDragCursorInfo.Index < _dragStartInfo.Index)
            {
                bool changeUpperMargin = true;
                if (rowUnderDragCursorInfo.Index != _dragStartInfo.Index - 1 && !isMouseInUpperHalfOfRow)
                {
                    dropTargetIndex = dropTargetIndex + 1;
                    changeUpperMargin = false;
                }

                changeRowMarginForAnimatedEffect(rowUnderDragCursorInfo.DataGridRow, changeUpperMargin);
            }

            return dropTargetIndex;
        }

        private bool isMouseCursorInUpperHalfOfRow(Point mousePoint, DataGridRow dataGridRow)
        {
            bool result = true;
            if (dataGridRow != null)
            {
                Point pointRelativeToRow = _dataGrid.TranslatePoint(mousePoint, dataGridRow);
                if (pointRelativeToRow.Y > (dataGridRow.ActualHeight / 2.0))
                {
                    result = false;
                }
            }

            return result;
        }

        private void changeRowMarginForAnimatedEffect(DataGridRow dataGridRow, bool changeUpperMargin)
        {
            Thickness marginToApply = changeUpperMargin ? new Thickness(_originalMarginForRow.Left, _originalMarginForRow.Top + _dropTargetIndicatorMargin, _originalMarginForRow.Right, _originalMarginForRow.Bottom)
                : new Thickness(_originalMarginForRow.Left, _originalMarginForRow.Top, _originalMarginForRow.Right, _dropTargetIndicatorMargin);
            if (_lastAnimatedRowInfo != dataGridRow)
            {
                undoRowMarginChange();
            }

            dataGridRow.Margin = marginToApply;
            _lastAnimatedRowInfo = dataGridRow;
        }

        private void undoRowMarginChange()
        {
            if (_lastAnimatedRowInfo != null)
            {
                _lastAnimatedRowInfo.Margin = _originalMarginForRow;
            }
        }

        private void dataGrid_Drop(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                if (this._dragAdorner != null)
                {
                    _dragAdorner.Visibility = Visibility.Collapsed;
                }

                if (_dropTargetIndex.HasValue && Drop != null)
                {
                    DropArgs dropArgs = new DropArgs(e, _dropTargetIndex.Value);
                    Drop(this, dropArgs);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(dataGrid_Drop));
            }
        }

        private void dataGrid_DragEnter(object sender, System.Windows.DragEventArgs e)
        {
            if (this._dragAdorner != null)
            {
                _dragAdorner.Visibility = Visibility.Visible;
            }

            e.Handled = true;
        }

        private void dataGrid_DragLeave(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                Point p = MouseHelper.GetMousePosition(_dataGrid);
                if (!MouseHelper.IsMouseOverItem(_dataGrid, p))
                {
                    //System.Diagnostics.Debug.WriteLine("############Leave################# : " + e.OriginalSource.ToString() + " : " + e.Source.ToString());
                    undoRowMarginChange();
                    if (_dragAdorner != null)
                    {
                        _dragAdorner.Visibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DataGridDragDropBehavior<ItemType>), nameof(dataGrid_DragLeave));
            }
        }

        private void dataGrid_GiveFeedback(object sender, System.Windows.GiveFeedbackEventArgs e)
        {
            if ((e.Effects & DragDropEffects.Move) == DragDropEffects.Move && e.Source == _dataGrid)
            {
                Mouse.SetCursor(Cursors.SizeNS);
                e.UseDefaultCursors = false;
                e.Handled = true;
            }
        }

        private class DataGridRowInfo
        {
            public int? Index { get; private set; }

            public DataGridRow DataGridRow { get; private set; }

            public DataGridRowInfo(int? index, DataGridRow dataGridRow)
            {
                Index = index;
                DataGridRow = dataGridRow;
            }
        }
    }
}