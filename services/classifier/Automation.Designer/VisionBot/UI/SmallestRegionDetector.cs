/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    internal class SmallestRegionDetector : IRegionDetector
    {
        private readonly IList<IRegion> _regions;

        public SmallestRegionDetector(IList<IRegion> regions)
        {
            _regions = regions;
        }

        public IRegion DetectRegionFromPoint(Point detectionLocation)
        {
            var regions = _regions.Where(r => isPointWithinBounds(detectionLocation, r)).OrderBy(r => r.Bounds.Height).ThenBy(r => r.Bounds.Width).ToList();
            if (regions.Count == 0)
            {
                return null;
            }

            return getSmallestRegion(regions);
        }

        private IRegion getSmallestRegion(IList<IRegion> regions)
        {
            var smallestRegion = regions[0];
            foreach (var region in regions)
            {
                if (region.Bounds.Height < smallestRegion.Bounds.Height)
                {
                    smallestRegion = region;
                }
            }

            return getObjectToSelect(regions, smallestRegion);
        }

        private IRegion getObjectToSelect(IList<IRegion> regions, IRegion smallestRegion)
        {
            if (isNotSir(smallestRegion.Type))
            {
                return smallestRegion;
            }

            for (int index = regions.Count - 1; index >= 0; index--)// (var region in regions)
            {
                //TODO: Replace with delta comparer
                if (regions[index].Bounds.Equals(smallestRegion.Bounds))
                {
                    return regions[index];
                }
            }

            return smallestRegion;
        }

        private bool isNotSir(ElementType elementType)
        {
            return elementType != ElementType.SystemIdentifiedField &&
                   elementType != ElementType.SystemIdentifiedRegion;
        }

        private bool isPointWithinBounds(Point detectionLocation, IRegion r)
        {
            return r.Bounds.Left <= detectionLocation.X && r.Bounds.Right >= detectionLocation.X &&
                   r.Bounds.Top <= detectionLocation.Y && r.Bounds.Bottom >= detectionLocation.Y;
        }
    }
}