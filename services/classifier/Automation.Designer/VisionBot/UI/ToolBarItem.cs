﻿using System;
using System.ComponentModel;
/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows.Media.Imaging;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class ToolBarItem : INotifyPropertyChanged
    {
        public BitmapImage ImageSource { get; set; }
        private string _text;
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Text"));
                }
            }
        }
        public RelayCommand ItemCommand { get; private set; }
        public Action<object> Action { get; set; }

        public ToolBarItem(BitmapImage imageSource, string text)
        {
            ImageSource = imageSource;
            Text = text;
            ItemCommand = new RelayCommand(Execute);
        }

        public void Execute(object param)
        {
            if (Action != null)
            {
                Action(param);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}