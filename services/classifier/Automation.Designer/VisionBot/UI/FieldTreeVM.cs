﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class FieldTreeVM : INotifyPropertyChanged
    {      
        private object _items = new ObservableCollection<object>();
        public readonly SolidColorBrush LayoutColorBrush = new SolidColorBrush(Color.FromArgb(255, 71, 71, 71));
        private Visibility _layoutVisibility;

        public Visibility LayoutVisibility
        {
            get { return _layoutVisibility; }
            set
            {
                _layoutVisibility = value;
                onPropertyChanged("LayoutVisibility");
            }
        }
        
        public object Items
        {
            get { return _items; }
            set
            {
                if (value is DataModel)
                {
                    ObservableCollection<object> items = new ObservableCollection<object>();
                    LayoutItem fieldItems = new LayoutItem() { Name = "Fields", Items = ((DataModel)value).Fields, Color = "Transparent" };
                    LayoutItem tablesItems = new DataModelParentItem() { Name = "Tables", Items = ((DataModel)value).Tables, Color = "Transparent" };
                    items.Add(fieldItems);
                    items.Add(tablesItems);
                    _items = items;   
                    LayoutVisibility = Visibility.Collapsed;               
                }
                else
                {
                    _items = value;
                    LayoutVisibility = Visibility.Visible;
                }

                onPropertyChanged("Items");
            }
        }

        private void onPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private object _selectedItem;

        public object SelectedItem
        {
            get { return _selectedItem; }
            set 
            {
                if (_selectedItem != value)
                {
                    if(value==null)
                    {
                        clearSelectionForSelectItemIsNull();
                    }

                    _selectedItem = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("SelectedItem"));
                    }

                    if(SelectionChanged != null)
                    {
                        SelectionChanged(_selectedItem);
                    }
                }
            }
        }

        private void clearSelectionForSelectItemIsNull()
        {
            if (_selectedItem is FieldDef)
            {
                ((FieldDef)_selectedItem).IsSelected = false;
            }
            else if (_selectedItem is TableDef)
            {
                ((TableDef)_selectedItem).IsSelected = false;
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<object> SelectionChanged;

        internal void AddLayout(Layout layout)
        {
            var items = _items as ObservableCollection<Layout>;
            if (items != null)
            {
                var oldIndex = items.Count;
                items.Add(layout);
                var newIndex = items.OrderBy(l => l.Name).ToList().IndexOf(layout);
                items.Move(oldIndex, newIndex);
            }
        }
        
        public ObservableCollection<Layout> GetLayouts()
        {
             return _items as ObservableCollection<Layout>;
        }

        public void SelectLayout(Layout layout)
        {
            var items = _items as ObservableCollection<Layout>;
            if (items != null)
            {
                var itemToSelect = items.FirstOrDefault(l => l.Name.Equals(layout.Name));
                if (itemToSelect != null)
                {
                    itemToSelect.IsSelected = true;
                }
            }
        }
    }
}
