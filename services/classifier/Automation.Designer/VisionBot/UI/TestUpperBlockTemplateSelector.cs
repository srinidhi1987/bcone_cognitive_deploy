﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    internal class TestUpperBlockTemplateSelector : DataTemplateSelector
    {

        private const string TestImageViewTemplateKey = "TestImageViewTemplate";
        private const string DocSetItemPreviewKey = "DocSetItemPreview";
        private const string EmptyTestSetKey = "EmptyTestSet";

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var contentControl = container as ContentPresenter;
            if (contentControl != null)
            {
                if (item is CanvasImageViewModel)
                {
                    return contentControl.FindResource(TestImageViewTemplateKey) as DataTemplate;
                }

                if (item is PreviewViewModel)
                {
                    return contentControl.FindResource(DocSetItemPreviewKey) as DataTemplate;
                }

                if (item is string)
                {
                    return contentControl.FindResource(EmptyTestSetKey) as DataTemplate;
                }
            }

            return null;
        }
    }
}