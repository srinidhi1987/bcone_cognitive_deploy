﻿

using System.Collections.ObjectModel;
/**
* Copyright (c) 2015 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for FieldTree.xaml
    /// </summary>
    public partial class LeftPanel : UserControl
    {
        // TODO: Move FieldTree.xaml View here. Intentionally created duplicate copy of FieldTree.xaml.
        public LeftPanel()
        {
            InitializeComponent();
        }

        private void MainTreeView_OnSelectedItemChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
        {
            //if (MainTreeView.SelectedItem != null)
            //{
            //    MainTreeView.BringIntoView(MainTreeView.SelectedItem);
            //}
        }

        private void TreeViewItemSelected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                item.BringIntoView();
                e.Handled = true;
            }
        }

        private void ContextMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {

            TextBlock textBlock = (sender as TextBlock);

            if (this.DataContext is Automation.CognitiveData.VisionBot.UI.ViewModels.BluePrintViewModel)
            {
                var bluePrintViewModel = this.DataContext as Automation.CognitiveData.VisionBot.UI.ViewModels.BluePrintViewModel;
                var tableColumnTreeViewItem = (textBlock as DependencyObject).GetParentOfType<TreeViewItem>();
                var tableViewItem = (tableColumnTreeViewItem as DependencyObject).GetParentOfType<TreeViewItem>();

                var parentTable = (tableViewItem.DataContext as TableDef);
                if (parentTable == null)
                {
                    e.Handled = true;
                    return;
                }
                if (bluePrintViewModel.BluePrintTables?.Tables == null || bluePrintViewModel.BluePrintTables.Tables.Count == 1)
                {
                    e.Handled = true;
                    return;
                }
                var mainMenu = bluePrintViewModel.BluePrintTables.GetContextMenu(parentTable, tableColumnTreeViewItem.DataContext as FieldDef);

                if (mainMenu == null)
                {
                    e.Handled = true;
                    return;
                }
                textBlock.ContextMenu.DataContext = mainMenu;
            }
        }

        private void TextBlock_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            TextBlock textBlock = (sender as TextBlock);
            textBlock.ContextMenu.DataContext = null;
        }
    }
    internal static class DependencyObjectExtensions
    {
        public static T GetParentOfType<T>(this DependencyObject element) where T : DependencyObject
        {
            DependencyObject parent = null;
            if (element != null)
            {
                parent = VisualTreeHelper.GetParent(element);
                if (parent != null && !(parent is T))
                {
                    parent = parent.GetParentOfType<T>();
                }

            }

            return parent as T;
        }
    }
    public class MainMenu
    {
        public ObservableCollection<MenuItem> MenuItems { get; set; } = new ObservableCollection<MenuItem>();
    }

    public class MenuItem
    {
        public string Name { get; set; }

        public RelayCommand Command { get; set; }
        public ObservableCollection<MenuItem> MenuItems { get; set; } = new ObservableCollection<MenuItem>();

        public object SourceInfo { get; private set; }

        public MenuItem(object sourceInfo)
        {
            SourceInfo = sourceInfo;
          //  Command = new RelayCommand(CommandExecute);
        }
       
    }

    public class TableColumnMoveMenuItemSourceInfo
    {
        public TableDef SourceTable { get; private set; }
        public FieldDef ColumnToMove { get; private set; }

        public TableColumnMoveMenuItemSourceInfo(TableDef sourceTable, FieldDef columnToMove)
        {
            SourceTable = sourceTable;
            ColumnToMove = columnToMove;
        }
    }

}
