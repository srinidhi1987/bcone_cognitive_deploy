﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.ConfidenceScore;
    using Automation.CognitiveData.VisionBot.UI.EventArguments;
    using Automation.CognitiveData.VisionBot.UI.Model;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Services.Client;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using VisionBotEngine.Configuration;

    /// <summary>
    /// Interaction logic for VisionBotDesigner.xaml
    /// </summary>
    public partial class VisionBotDesigner : UserControl
    {
        private IqBot _iqBot;
        private TestInspectionViewModel _testInspectionViewModel;
        public const int BlueprintTabIndex = 0;
        public const int TestTabIndex = 2;
        public const int LayoutTabIndex = 1;

        public IqBot IqBot
        {
            get { return _iqBot; }
            set
            {
                _iqBot = value;
                if (_iqBot != null)
                {
                    loadScreenResources();
                }
            }
        }

        public void SelectTab(int tabIndex)
        {
            screenResources.DesignerInfo.RibbonTabItems[tabIndex].IsSelected = true;
        }

        public bool IsLoadingCompleted { get; set; }

        private List<LayoutTestSet> _testDocSets;

        public List<LayoutTestSet> TestDocSets
        {
            get { return _testDocSets; }
            set
            {
                _testDocSets = value;
                screenResources.DesignerInfo.RibbonTabItems[BlueprintTabIndex].IsEnabled = true;
                if (_testDocSets != null)
                {
                    initTestSet();
                }
            }
        }

        private void initTestSet()
        {
            if (_testSetViewModel == null || _testSetViewModel.Items == null || _testSetViewModel.Items.Count == 0)
            {
                VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();
                _testSetViewModel = vbotResourceHelper.GetTestSets();

                _testSetViewModel.AddLayoutTestSets(TestDocSets);
                updateWarningVisibility();

                _testSetViewModel.TestDocSetItemAddRequested += _testSetViewModel_TestDocSetItemModificationRequested;
                _testSetViewModel.ItemSelected += testSetViewModel_SelectionChanged;
                _testSetViewModel.DocumentsSelectionRequested += testSetViewModel_DocumentsSelectionRequested;

                _testSetViewModel.IsSelected = true;

                _testInspectionViewModel = new TestInspectionViewModel { Run = new RelayCommand(RunTests) };

                screenResources.InspectionViewModel = _testInspectionViewModel;
                screenResources.DesignerInfo.RibbonTabItems[TestTabIndex].IsEnabled = true;
            }
        }

        private void testSetViewModel_DocumentsSelectionRequested(object sender, DocumentsSelectionEventArgs<DocumentProperties> e)
        {
            handleExceptionWithMessage(() =>
            {
                if (DocumentsDownloadRequested != null)
                {
                    handleActionWithProgressBar(() =>
                    {
                        DocumentsDownloadRequested(sender, e);
                    }, "Downloading Documents... Please wait...");
                }

                if (DocumentsSelectionRequested != null)
                {
                    DocumentsSelectionRequested(sender, e);
                }
            });
        }

        private VisionBotDesignerResources screenResources;

        public VisionBotDesigner()
        {
            InitializeComponent();
        }

        private void screenResources_SaveVisionBotHander(object sender, EventArgs e)
        {
            onSaveRequested(new SaveRequestEventArgs(_iqBot));
            updateTestSetViewModel();
        }

        // TODO: Remove below function to ViewModel so that switching of view is managed.
        private void RibbonTab_OnTabChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                int tabIndex = ((RibbonTab)sender).SelectedTabIndex;
                this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => handleRibbonTabSelectionChange(tabIndex)));
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(RibbonTab_OnTabChanged));
            }
        }

        private void handleRibbonTabSelectionChange(int tabIndex)
        {
            try
            {
                screenResources.RightPanelVM.UpdateContext(UpdateMode.Add, null, null);
                ctrlTestBar.Visibility = Visibility.Collapsed;
                screenResources.MessageProvider = null;
                ResetPreview();
                if (tabIndex == VisionBotDesigner.LayoutTabIndex)
                {
                    fieldTree.Visibility = Visibility.Collapsed;
                    leftPanel.Visibility = Visibility.Visible;
                    if (_layoutViewModel == null)
                    {
                        loadLayoutViewModel();
                    }
                    leftPanel.DataContext = null;
                    leftPanel.DataContext = _layoutViewModel;
                    //  screenResources.FieldTreeVM.Items = screenResources.IqBot.Layouts;
                    ctrlDataModel.Visibility = Visibility.Collapsed;
                    screenResources.SelectedTabName = "Layout";
                    makeSureLayoutItemSelection();

                    if (_layoutViewModel.SelectedItem != null)
                    {
                        if (_layoutViewModel.SelectedItem is LayoutField)
                        {
                            screenResources.RightPanelVM.UpdateLayoutFieldContext(
                                screenResources.SelectedLayout,
                                _layoutViewModel.SelectedItem as LayoutField);
                        }
                        else if (_layoutViewModel.SelectedItem is LayoutTable)
                        {
                            screenResources.RightPanelVM.UpdateLayoutTableContext(
                                _layoutViewModel.SelectedItem as LayoutTable);
                        }
                        else if (_layoutViewModel.SelectedItem is Layout)
                        {
                            screenResources.RightPanelVM.UpdateLayoutContext(
                                _layoutViewModel.SelectedItem as Layout);
                        }
                    }
                    if (screenResources.IqBot.Layouts.Count > 0)
                    {
                        screenResources.HeaderPanelViewModel.IsLayoutSelected = true;
                        screenResources.HeaderPanelViewModel.IsTestSetSelected = false;
                        screenResources.HeaderPanelViewModel.IsTestSetItemSelected = false;
                        screenResources.HeaderPanelViewModel.IsDocumentSelected = true;
                    }
                }
                else if (tabIndex == VisionBotDesigner.BlueprintTabIndex)
                {
                    fieldTree.Visibility = Visibility.Collapsed;
                    leftPanel.Visibility = Visibility.Visible;
                    if (_bluePrintViewModel == null)
                    {
                        loadBluePrintViewModel();
                    }
                    leftPanel.DataContext = null;
                    leftPanel.DataContext = _bluePrintViewModel;
                    // screenResources.FieldTreeVM.Items = screenResources.IqBot.DataModel;
                    ctrlDataModel.Visibility = Visibility.Visible;
                    //   ((LayoutItem)((screenResources.FieldTreeVM.Items as ObservableCollection<object>)[0])).IsSelected = true;
                    screenResources.SelectedTabName = "Blueprint";
                    if (_bluePrintViewModel.SelectedItem != null)
                    {
                        BluePrintViewModel_SelectionChanged(_bluePrintViewModel.SelectedItem, EventArgs.Empty);
                    }
                    else _bluePrintViewModel.BluePrintFields.IsSelected = true;
                    screenResources.HeaderPanelViewModel.IsLayoutSelected = false;
                    screenResources.HeaderPanelViewModel.IsTestSetSelected = false;
                    screenResources.HeaderPanelViewModel.IsTestSetItemSelected = false;
                    screenResources.HeaderPanelViewModel.IsDocumentSelected = false;
                }
                else
                {
                    fieldTree.Visibility = Visibility.Collapsed;
                    leftPanel.Visibility = Visibility.Visible;
                    leftPanel.DataContext = null;
                    leftPanel.DataContext = _testSetViewModel;
                    ctrlDataModel.Visibility = Visibility.Collapsed;
                    ctrlTestBar.Visibility = Visibility.Visible;
                    screenResources.SelectedTabName = "Test";
                    setTestSetHeaderPanelVisiblity(_testSetViewModel);
                    setTestSetHeaderPanelData(_testSetViewModel);
                    screenResources.MessageProvider = _testSetViewModel;

                    if (screenResources.IsResultsOn)
                        screenResources.ResultsTabSelected.Execute(null);
                    else
                        screenResources.EditorTabSelected.Execute(null);

                    updateTestSetUI();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(handleRibbonTabSelectionChange));
            }
        }

        private void updateTestSetUI()
        {
            if (screenResources.RightPanelVM.Content is TestInspectionViewModel)
            {
                LayoutTestSetItem selectedTestSet = _testSetViewModel.SelectedTestSetItem;
                if (selectedTestSet.Warning.HasWarning)
                {
                    if (selectedTestSet.Warning.WarningType.Equals(WarningType.Blueprint))
                    {
                        (screenResources.RightPanelVM.Content as TestInspectionViewModel).Edit.Execute(null);
                    }
                }

                if (gridTestImageView.DataContext != null)
                {
                    PreviewViewModel previewVM = (gridTestImageView.DataContext as TestResultViewModel).LowerContent as PreviewViewModel;
                    if (previewVM != null)
                    {
                        if (previewVM.IsEditable
                        && selectedTestSet.Warning.WarningType.Equals(WarningType.Layout))
                        {
                            previewVM.IsEnabled = false;
                        }
                        else
                        {
                            previewVM.IsEnabled = true;
                        }
                    }
                }
            }
        }

        private void makeSureLayoutItemSelection()
        {
            if (_layoutViewModel?.SelectedItem == null &&
                screenResources?.IqBot?.Layouts?.Count > 0)
            {
                screenResources.IqBot.Layouts[0].IsSelected = true;
            }
        }

        private void updateTestSetViewModel()
        {
            if (_testSetViewModel != null)
            {
                if (_testSetViewModel.Items.Count < _iqBot.Layouts.Count)
                {
                    addNewLayoutToTestSet();
                }
                else
                {
                    removeLayoutFromTestSet();
                }
                _testSetViewModel.UpdateTestSetSelection();
            }
        }

        private void removeLayoutFromTestSet()
        {
            for (int index = 0; index < _testSetViewModel.Items.Count; index++)
            {
                var layout = _testSetViewModel.Items[index];
                var layoutTestSet = _iqBot.Layouts.FirstOrDefault(l => l.Id.Equals(layout.Id));
                if (layoutTestSet == null)
                {
                    //Before deletion of selected layout from test default selection should move to Test Set.
                    if (_testSetViewModel.Items[index].IsSelected || _testSetViewModel.Items[index].IsExpanded)
                        _testSetViewModel.IsSelected = true;
                    _testSetViewModel.Items.RemoveAt(index);
                    if (layout.LayoutTestSetItems != null)
                    {
                        foreach (var doc in layout.LayoutTestSetItems)
                        {
                            var imageProvider = doc.GetDocSetItem()?.ImageProvider;
                            if (imageProvider != null)
                            {
                                Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                                {
                                    imageProvider.Dispose();
                                }));
                            }
                        }
                    }
                    break;
                }
            }
            if (_testSetViewModel.Items.Count == 0)
                _testSetViewModel.IsSelected = true;
        }

        private void addNewLayoutToTestSet()
        {
            for (int index = 0; index < _iqBot.Layouts.Count; index++)
            {
                var layout = _iqBot.Layouts[index];
                var layoutTestSet = _testSetViewModel.Items.FirstOrDefault(l => l.Id.Equals(layout.Id));
                if (layoutTestSet == null)
                {
                    _testSetViewModel.AddLayoutTestSet(new LayoutTestSet
                    {
                        DocSetItems = new List<DocSetItem>(),
                        LayoutId = layout.Id,
                        LayoutName = layout.Name,
                    });
                    break;
                }
            }
        }

        internal event ExecuteTestHandler ExecuteTestRequested;

        internal event ExportPreviewDataEventHandler ExportPreviewDataRequested;

        internal event DocumentsDownloadRequestHandler DocumentsDownloadRequested;

        internal event DocumentSelectionRequestHandler DocumentsSelectionRequested;

        internal event DocumentPageImageRequestHandler DocumentPageImageRequested;

        internal event Action<IqBot> ReArrageDataModelFieldRequested;

        private void testInspectionViewModel_SaveClicked(object sender, SaveTestEventArgs e)
        {
            SaveTest(e);
        }

        private void SaveTest(SaveTestEventArgs saveArguments)
        {
            if (TestDocSetModificationRequested != null && gridTestImageView.DataContext != null)
            {
                handleExceptionWithMessage(new Action(() =>
                {
                    DocSetItem docSetItem = _testSetViewModel.SelectedTestSetItem.GetDocSetItem();
                    docSetItem.IsBenchMarkDataSaved = true;
                    PreviewViewModel previewVM = (gridTestImageView.DataContext as TestResultViewModel).LowerContent as PreviewViewModel;

                    TestDocSetEventArgs args = new TestDocSetEventArgs(_testSetViewModel.SelectedTestSets[0].LayoutTestSet.LayoutId, docSetItem, Operation.Update, previewVM);
                    args.IqBot = _iqBot;
                    onTestDocSetModificationRequested(this, args);

                    docSetItem.AssignModifiedPreviewDataToOriginalData();

                    PreviewViewModel previewWithModifiedData = getPreviewData(docSetItem);
                    previewVM.IsEditable = false;
                    previewVM.PreviewFields = previewWithModifiedData.PreviewFields;
                    previewVM.PreviewTables = previewWithModifiedData.PreviewTables;

                    saveArguments.IsSaveSuccessful = true;
                }));
            }
        }

        private void RunTests(object obj)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                VBotLogger.Trace(() => "[ VisionBotDesigner.xaml - RunTests] - Execution Started");
                if (ExecuteTestRequested != null)
                {
                    var testExecutionEventArgs = new TestExecutionEventArgs(this._iqBot, this._testSetViewModel.SelectedTestSets);

                    handleActionWithDocumentProcessingProgressBar(new Action(() =>
                    {
                        this.ExecuteTestRequested(this, testExecutionEventArgs);

                    }));

                    this.displayTestResult(testExecutionEventArgs);
                }
            }));
        }

        private void resetRunResult()
        {
            try
            {
                foreach (var TestViewModel in screenResources.TestViewModels)
                {
                    TestViewModel.Value.TestExecutionData = null;
                    TestViewModel.Value.TestDocumentSummary = null;
                }
                screenResources.SummaryViewModels.Clear();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(resetRunResult));
            }
        }

        private void displayTestResult(TestExecutionEventArgs testExecutionEventArgs)
        {
            try
            {
                VBotLogger.Trace(() => "[displayTestResult] -  Start Displaying result");

                resetRunResult();
                if (isSingleDocSetItemSelected(this._testSetViewModel))
                {
                    VBotLogger.Trace(() => "[displayTestResult] -  isSingleDocSetItemSelected - true");
                    var testItemPreviewViewModel = this.createPreviewViewModel();
                    var banchmarkPreviewViewModel = this.createPreviewViewModel();
                    if (testExecutionEventArgs.LayoutTestResults.Count > 0
                        && testExecutionEventArgs.LayoutTestResults[0].TestDocItemResults.Count > 0)
                        this.exeucteSingleTest(
                            testItemPreviewViewModel,
                            banchmarkPreviewViewModel,
                            testExecutionEventArgs.LayoutTestResults[0].TestDocItemResults[0]);
                }
                else
                {
                    VBotLogger.Trace(() => "[displayTestResult] -  isSingleDocSetItemSelected - false");

                    foreach (var testLayoutResult in testExecutionEventArgs.LayoutTestResults)
                    {
                        foreach (var testDocItemResult in testLayoutResult.TestDocItemResults)
                        {
                            var testItemPreviewViewModel = this.createPreviewViewModel();
                            var banchmarkPreviewViewModel = this.createPreviewViewModel();
                            this.setResult(testItemPreviewViewModel, banchmarkPreviewViewModel, testDocItemResult);

                            var testItemSummary = getTestItemRunSummary(testDocItemResult);
                            if (screenResources.TestViewModels.ContainsKey(testDocItemResult.TestDocItemId))
                            {
                                screenResources.TestViewModels[testDocItemResult.TestDocItemId].TestExecutionData =
                                    testItemPreviewViewModel;
                                screenResources.TestViewModels[testDocItemResult.TestDocItemId].TestDocumentSummary =
                                    testItemSummary;
                            }
                            else
                            {
                                TestItemViewModel testItemVM = new TestItemViewModel();
                                testItemVM.TestExecutionData = testItemPreviewViewModel;
                                testItemVM.TestDocumentSummary = testItemSummary;
                                screenResources.TestViewModels.Add(testDocItemResult.TestDocItemId, testItemVM);
                            }
                        }
                    }

                    string selectedItem = string.Empty;

                    VBotLogger.Trace(() => string.Format("[displayTestResult] -  SelectedTestSetMode -> {0}", _testSetViewModel.SelectedTestSetMode));

                    if (_testSetViewModel.SelectedTestSetMode == TestItemTypes.Layout)
                    {
                        selectedItem = testExecutionEventArgs.LayoutTestResults[0].LayoutId;
                    }
                    bool isTestSet = (_testSetViewModel.SelectedTestSetMode == TestItemTypes.TestSet);
                    if (screenResources.SummaryViewModels.ContainsKey(selectedItem))
                    {
                        SummaryViewModel summaryVM = new SummaryViewModel(
                            testExecutionEventArgs.LayoutTestResults,
                            isTestSet);
                        summaryVM.ViewTestRequested += viewRequestedHandler;
                        screenResources.SummaryViewModels[selectedItem] = summaryVM;
                    }
                    else
                    {
                        SummaryViewModel summaryVM = new SummaryViewModel(
                            testExecutionEventArgs.LayoutTestResults,
                            isTestSet);
                        summaryVM.ViewTestRequested += viewRequestedHandler;
                        screenResources.SummaryViewModels.Add(selectedItem, summaryVM);
                    }
                }
                screenResources.ResultsTabSelected.Execute(null);

                VBotLogger.Trace(() => "[displayTestResult] -  displayTestResult completed");
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(displayTestResult));
            }
        }

        private void viewRequestedHandler(object sender, EventArgs e)
        {
            if (sender != null && sender is ITestItemResult)
            {
                screenResources.SelectionType = TestItemTypes.TesSetItem;
                _testSetViewModel.SelectTestSetItem(((ITestItemResult)sender).Id);
                screenResources.IsEditorOn = false;
                screenResources.ResultsTabSelected.Execute(null);
            }
        }

        private void exeucteSingleTest(PreviewViewModel testItemPreviewViewModel, PreviewViewModel banchmarkPreviewViewModel, TestDocItemResult testDocItemResult)
        {
            try
            {
                setResult(testItemPreviewViewModel, banchmarkPreviewViewModel, testDocItemResult);

                var testItemSummary = getTestItemRunSummary(testDocItemResult);
                if (screenResources.TestViewModels.ContainsKey(testDocItemResult.TestDocItemId))
                {
                    screenResources.TestViewModels[testDocItemResult.TestDocItemId].TestExecutionData =
                        testItemPreviewViewModel;
                    screenResources.TestViewModels[testDocItemResult.TestDocItemId].TestDocumentSummary =
                        testItemSummary;
                }

                screenResources.IsEditorOn = false;
                screenResources.RightPanelVM.UpdateTestDocResultSummaryContext(testItemSummary);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(exeucteSingleTest));
            }
        }

        private PreviewViewModel createPreviewViewModel()
        {
            var testItemPreviewViewModel = new PreviewViewModel();

            testItemPreviewViewModel.EditChanged += PreviewViewModel_EditPreview;
            testItemPreviewViewModel.IsEditable = false;
            return testItemPreviewViewModel;
        }

        private void setResult(PreviewViewModel testItemPreviewViewModel, PreviewViewModel banchmarkPreviewViewModel, TestDocItemResult testDocItemResult)
        {
            testItemPreviewViewModel.PreviewFields = getValidatedFields(testDocItemResult.FieldComparisionResult.ResultDataTable);
            banchmarkPreviewViewModel.PreviewFields = getFields(testDocItemResult.FieldComparisionResult.BenchmarkDataTable);

            IList<PreviewTableModel> previewTables = new List<PreviewTableModel>();
            IList<PreviewTableModel> benchmarkPreviewTables = new List<PreviewTableModel>();
            foreach (var table in testDocItemResult.TableComparisionResults)
            {
                getValidatedTables(previewTables, table.ResultDataTable);
                getTables(benchmarkPreviewTables, table.BenchmarkDataTable);
            }
            testItemPreviewViewModel.PreviewTables = new ObservableCollection<PreviewTableModel>(previewTables);
            banchmarkPreviewViewModel.PreviewTables = new ObservableCollection<PreviewTableModel>(benchmarkPreviewTables);
        }

        private void CancelTestEditing(object obj)
        {
            MessageBox.Show("Not Available");
        }

        private void testSetViewModel_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var testSetViewModel = sender as TestSetViewModel;
                this.changeSelection(testSetViewModel);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(testSetViewModel_SelectionChanged));
            }
        }

        private void changeSelection(TestSetViewModel testSetViewModel)
        {
            if (testSetViewModel != null)
            {
                this.screenResources.SelectionType = testSetViewModel.SelectedTestSetMode;
                if (this.screenResources.SelectedTabName == "Test")
                {
                    this.setTestSetHeaderPanelVisiblity(testSetViewModel);
                    this.setTestSetHeaderPanelData(testSetViewModel);
                }
                if (isSingleDocSetItemSelected(testSetViewModel))
                {
                    LayoutTestSetItem selectedTestSet = testSetViewModel.SelectedTestSets[0].LayoutTestSetItems[0];
                    if (selectedTestSet != null)
                    {
                        DocSetItem docset = selectedTestSet.GetDocSetItem();
                        if (docset.ImageProvider == null)
                        {
                            docset.ImageProvider = new UIImageProvider(docset.DocumentProperties, _iqBot.Settings);
                            docset.ImageProvider.DocumentPageImageRequested += ImageProvider_DocumentPageImageRequested;
                        }

                        docset.ImageProvider.StartOrResumeProcessing();
                        var imageViewInfo = getImageViewInfoForDocSetItem(selectedTestSet.GetDocSetItem());

                        CanvasImageViewModel canvasImageVM = new CanvasImageViewModel(this.gridTestImageView)
                        {
                            ImageViewInfo = imageViewInfo,
                            HasItems = true
                        };

                        canvasImageVM.SetInitializeZoomLevel();

                        TestItemViewModel testItemViewModel;
                        if (this.screenResources.TestViewModels.ContainsKey(selectedTestSet.GetDocSetItem().Id))
                        {
                            testItemViewModel = this.screenResources.TestViewModels[selectedTestSet.GetDocSetItem().Id];
                        }
                        else
                        {
                            testItemViewModel = new TestItemViewModel();
                            this.screenResources.TestViewModels.Add(selectedTestSet.GetDocSetItem().Id, testItemViewModel);
                        }

                        testItemViewModel.CanvasImageViewModel = canvasImageVM;
                        DocSetItem docsetItem = selectedTestSet.GetDocSetItem();
                        testItemViewModel.BenchmarkData = this.getPreviewData(docsetItem);
                        updateSelectedTestWarningStatus();
                        this.screenResources.SelectedTestItem = selectedTestSet.GetDocSetItem().Id;

                        fillImageViewInfoForTest(canvasImageVM, docsetItem);

                        testItemViewModel.State = this.createTestInspectionViewModel(testSetViewModel);
                        this.screenResources.RightPanelVM.UpdateTestSetContext(testItemViewModel.State);

                        if (selectedTestSet.Warning.HasWarning && selectedTestSet.Warning.WarningType.Equals(WarningType.Blueprint))
                        {
                            testItemViewModel.State.Edit.Execute(null);
                        }
                        //To avoid memory consumption clear CanvasImageInfo from instances present in dictionary
                        var testItemViewModelToExclude = testItemViewModel;
                        clearImagesFromDictionary(testItemViewModelToExclude);
                    }
                }
                else
                {
                    if (testSetViewModel.SelectedTestSetMode == TestItemTypes.Layout)
                    {
                        this.screenResources.SelectedTestItem = testSetViewModel.SelectedTestSets[0].Id;
                    }
                    else
                    {
                        this.screenResources.SelectedTestItem = string.Empty;
                    }
                    this.screenResources.RightPanelVM.UpdateTestSetContext(this._testInspectionViewModel);
                }
            }
        }

        private void ImageProvider_DocumentPageImageRequested(object sender, DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            if (DocumentPageImageRequested != null)
            {
                DocumentPageImageRequested(sender, documentProperties, pageIndex, imagePath, settings);
            }
        }

        private void fillImageViewInfoForTest(CanvasImageViewModel canvasImageVM, DocSetItem docSetItem)
        {
            try
            {
                LoadingWindow window = new LoadingWindow
                {
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    Owner = Window.GetWindow(this)
                };

                VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();

                var worker = new Thread(() =>
                {
                    IList<RegionViewModel> allRegions = vbotResourceHelper.GetAllRegions(docSetItem);
                    assignImagesAndRegionsToImageViewInfoForTestItem(docSetItem, docSetItem.DocumentProperties, canvasImageVM.ImageViewInfo, allRegions, _iqBot.Settings, false);
                    var formClose = new LoadingWindow.FromClose(window.Close);
                    window.Dispatcher.Invoke(DispatcherPriority.Background, formClose);
                });

                worker.Start();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(fillImageViewInfoForTest));
            }
        }

        private void clearImagesFromDictionary(TestItemViewModel testItemViewModelToExclude)
        {
            if (this.screenResources.TestViewModels != null && this.screenResources.TestViewModels.Keys != null)
            {
                foreach (string key in this.screenResources.TestViewModels.Keys)
                {
                    TestItemViewModel testItemVMToClear = this.screenResources.TestViewModels[key];
                    if (testItemVMToClear != null && testItemVMToClear != testItemViewModelToExclude && testItemVMToClear.CanvasImageViewModel != null)
                    {
                        testItemVMToClear.CanvasImageViewModel.ImageViewInfo = null;
                        testItemVMToClear.CanvasImageViewModel = null;
                    }
                }
            }
        }

        private ImageViewInfo getImageViewInfoForDocSetItem(DocSetItem docSetItem)
        {
            ImageViewInfo imageViewInfo = null;
            VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();
            imageViewInfo = vbotResourceHelper.CreateImageViewInfoWithoutImagesAndRegions(docSetItem);
            return imageViewInfo;
        }

        private TestInspectionViewModel createTestInspectionViewModel(TestSetViewModel testSetViewModel)
        {
            TestInspectionViewModel testInspectionViewModel = new TestInspectionViewModel(testSetViewModel.SelectedTestSets)
            {
                Run = new RelayCommand(RunTests),
            };
            testInspectionViewModel.SaveClicked += testInspectionViewModel_SaveClicked;
            testInspectionViewModel.CancelClicked += testInspectionViewModel_CancelClicked;
            testInspectionViewModel.EditClicked += testInspectionViewModel_EditClicked;
            testInspectionViewModel.TrainClicked += TestInspectionViewModel_TrainClicked;
            testInspectionViewModel.RescanClicked += TestRefresh_OnClick;
            return testInspectionViewModel;
        }

        private void TestInspectionViewModel_TrainClicked(object sender, EventArgs e)
        {
            SelectTab(LayoutTabIndex);

            string layoutID = _testSetViewModel.SelectedTestSets[0].Id;
            if (screenResources.SelectedLayout == null || screenResources.SelectedLayout.Id != layoutID)
            {
                Layout layoutToSelect =
                    (from q in screenResources.IqBot.Layouts where q.Id == layoutID select q).FirstOrDefault();
                if (layoutToSelect != null)
                {
                    layoutToSelect.IsSelected = true;
                }
            }
        }

        private TestDocSummaryViewModel getTestItemRunSummary(TestDocItemResult testDocItemResult)
        {
            TestDocSummaryViewModel testInspectionViewModel =
                new TestDocSummaryViewModel
                {
                    Correctness = testDocItemResult.ConfidenceScore + "%",
                    FailedFields = testDocItemResult.FailedFieldsCount.ToString(),
                    TotalFields = testDocItemResult.TotalFieldsCount.ToString(),
                    ExecutionTime = string.Format("{0:hh\\:mm\\:ss}", testDocItemResult.ExecutionTime),
                    HasLayoutClassificationError = !testDocItemResult.LayoutClassificationError.IsSameLayoutClassified,
                    LayoutName = _testSetViewModel.SelectedTestSets.Count > 0 ? _testSetViewModel.SelectedTestSets[0].Name : string.Empty
                };
            int counter = 1;
            foreach (var failedField in testDocItemResult.FailedFieldsInfo.OrderByDescending(item => item.Value.ErrorCount))
            {
                testInspectionViewModel.TestDocFailedFieldsSummary.Add(
                    new FailedObject
                    {
                        Id = (counter < 10 ? counter.ToString("00") : counter.ToString()) + ". ",
                        Name = failedField.Value.Name,
                        ErrorCount = failedField.Value.ErrorCount
                    });
                counter++;
            }

            testInspectionViewModel.HasFailedFields = testInspectionViewModel.TestDocFailedFieldsSummary.Count > 0;
            testInspectionViewModel.IsCorrectnessNotZero = testDocItemResult.FailedFieldsInfo.Count != 0;
            return testInspectionViewModel;
        }

        private void testInspectionViewModel_EditClicked(object sender, EventArgs e)
        {
            try
            {
                if (gridTestImageView.DataContext != null)
                {
                    PreviewViewModel previewVM =
                        (gridTestImageView.DataContext as TestResultViewModel).LowerContent as PreviewViewModel;
                    previewVM.IsEditable = true;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(testInspectionViewModel_EditClicked));
            }
        }

        private void testInspectionViewModel_CancelClicked(object sender, EventArgs e)
        {
            try
            {
                if (gridTestImageView.DataContext != null)
                {
                    PreviewViewModel previewVM =
                        (gridTestImageView.DataContext as TestResultViewModel).LowerContent as PreviewViewModel;

                    DocSetItem docSetItem = _testSetViewModel.SelectedTestSetItem.GetDocSetItem();

                    PreviewViewModel previewVMTemp = getPreviewData(docSetItem);
                    previewVMTemp.ArrangePreview(IqBot.DataModel);

                    previewVM.PreviewFields = previewVMTemp.PreviewFields;
                    previewVM.PreviewTables = previewVMTemp.PreviewTables;
                    docSetItem.IsBenchmarkStructureMismatchFound = docSetItem.OriginalIsBenchmarchStructureFound;
                    updateSelectedTestWarningStatus();

                    if (_testSetViewModel.SelectedTestSetItem != null
                        && _testSetViewModel.SelectedTestSetItem.Warning.HasWarning
                       && _testSetViewModel.SelectedTestSetItem.Warning.WarningType.Equals(WarningType.Blueprint))
                    {
                        (sender as TestInspectionViewModel).Edit.Execute(null);
                    }
                    else
                    {
                        previewVM.IsEditable = (sender as TestInspectionViewModel).IsInEditMode = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(testInspectionViewModel_CancelClicked));
            }
        }

        private void setTestSetHeaderPanelVisiblity(TestSetViewModel testSetmodel)
        {
            if (testSetmodel.IsSelected)
            {
                //If Test Set selected
                screenResources.HeaderPanelViewModel.IsTestSetSelected = true;
                screenResources.HeaderPanelViewModel.IsLayoutSelected = false;
                screenResources.HeaderPanelViewModel.IsTestSetItemSelected = false;
                screenResources.HeaderPanelViewModel.IsDocumentSelected = false;
            }
            else
            {
                //If Layout selected
                screenResources.HeaderPanelViewModel.IsTestSetSelected = true;
                screenResources.HeaderPanelViewModel.IsLayoutSelected = true;
                if (testSetmodel.SelectedTestSets[0].IsSelected)
                {
                    //If no any Test set Item in the layout or Layout itselef selected
                    screenResources.HeaderPanelViewModel.IsTestSetItemSelected = false;
                    screenResources.HeaderPanelViewModel.IsDocumentSelected = false;
                }
                else if (testSetmodel.SelectedTestSets[0].LayoutTestSetItems[0].IsSelected)
                {
                    //If test set item selected
                    screenResources.HeaderPanelViewModel.IsTestSetItemSelected = true;
                    screenResources.HeaderPanelViewModel.IsDocumentSelected = true;
                }
            }
        }

        private void setTestSetHeaderPanelData(TestSetViewModel testSetmodel)
        {
            if (testSetmodel.SelectedTestSets.Count == 1)
            {
                screenResources.HeaderPanelViewModel.TestSetLayoutName = testSetmodel.SelectedTestSets[0].Name;
                if (testSetmodel.SelectedTestSets[0].LayoutTestSetItems.Count > 0)
                {
                    foreach (LayoutTestSetItem item in testSetmodel.SelectedTestSets[0].LayoutTestSetItems)
                    {
                        if (item.IsSelected)
                        {
                            screenResources.HeaderPanelViewModel.TestSetItemName = item.Name;
                            DocSetItem docItem = item.GetDocSetItem();
                            screenResources.HeaderPanelViewModel.TestSetDocumentName = docItem.DocumentName;
                        }
                    }
                }
            }
        }

        private static bool isSingleDocSetItemSelected(TestSetViewModel testSetViewModel)
        {
            return testSetViewModel.SelectedTestSets.Count == 1 &&
                   testSetViewModel.SelectedTestSets[0].LayoutTestSetItems.Count == 1 && testSetViewModel.SelectedTestSets[0].LayoutTestSetItems[0].IsSelected;
        }

        private void _testSetViewModel_TestDocSetItemModificationRequested(object sender, TestDocSetEventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                if (TestDocSetModificationRequested != null)
                {
                    if (e.Action.Equals(Operation.Add))
                    {
                        handleActionWithDocumentProcessingProgressBar(new Action(() =>
                        {
                            e.IqBot = _iqBot;
                            onTestDocSetModificationRequested(sender, e);

                        }));

                        leftPanel.MainTreeView.Focus();
                    }
                    else
                    {
                        e.IqBot = _iqBot;
                        onTestDocSetModificationRequested(sender, e);
                    }
                }
            }));
        }

        private void onTestDocSetModificationRequested(object sender, TestDocSetEventArgs e)
        {
            if (TestDocSetModificationRequested != null)
            {
                TestDocSetModificationRequested.Invoke(sender, e);
                //if (_testSetViewModel.SelectedTestSetItem != null && !string.IsNullOrEmpty(e.DocSetItem.Id) &&
                //    e.DocSetItem.Id.Equals(_testSetViewModel.SelectedTestSetItem.GetDocSetItem().Id))
                //{
                //    _testSetViewModel.SelectedTestSetItem.Warning = GenericWarning.NoWarning;
                //}
            }
        }

        private PreviewViewModel getPreviewData(DocSetItem docSetItem, bool isResetRequired = true)
        {
            if (isResetRequired)
            {
                docSetItem.ResetPreviewData();
            }
            var previewViewModel = new PreviewViewModel();
            previewViewModel.EditChanged += PreviewViewModel_EditPreview;
            previewViewModel.IsEditable = !docSetItem.IsBenchMarkDataSaved;
            previewViewModel.PreviewFields = getValidatedFields(docSetItem.PreviewFields);

            clearConfidenceIssue(previewViewModel);

            IList<PreviewTableModel> previewTables = new List<PreviewTableModel>();
            if (docSetItem.PreviewTableSet != null && docSetItem.PreviewTableSet.Tables != null)
            {
                foreach (DataTable table in docSetItem.PreviewTableSet.Tables)
                {
                    getValidatedTables(previewTables, table);
                }
            }
            clearConfidenceIssue(previewTables);
            previewViewModel.PreviewTables = new ObservableCollection<PreviewTableModel>(previewTables);

            return previewViewModel;
        }

        private static void clearConfidenceIssue(IList<PreviewTableModel> previewTables)
        {
            foreach (PreviewTableModel previewTable in previewTables)
            {
                DataTable dataTable = previewTable.DataTable;
                foreach (DataRow row in dataTable.Rows)
                {
                    for (int index = 0; index < row.ItemArray.Length; index++)
                    {
                        var validatedFieldValue = (row.ItemArray[index] as ValidatedFieldValue);

                        clearConfidenceIssue(validatedFieldValue);
                    }
                }
            }
        }

        private static void clearConfidenceIssue(PreviewViewModel previewViewModel)
        {
            foreach (PreviewFieldModel previewField in previewViewModel.PreviewFields)
            {
                ValidatedFieldValue validatedFieldValue = previewField?.Value;
                clearConfidenceIssue(validatedFieldValue);
            }
        }

        private static void clearConfidenceIssue(ValidatedFieldValue validatedFieldValue)
        {
            if (validatedFieldValue?.FieldDataValidationIssue != null
                && validatedFieldValue.FieldDataValidationIssue.IssueCode == FieldDataValidationIssueCodes.ConfidenceIssue)
            {
                validatedFieldValue.FieldDataValidationIssue = null;
            }
        }

        private void PreviewViewModel_EditPreview(object sender, EventArgs arg)
        {
            PreviewViewModel previewVM = sender as PreviewViewModel;
            LayoutTestSetItem selectedTestSet = _testSetViewModel.SelectedTestSetItem;
            if (selectedTestSet != null && selectedTestSet.Warning.HasWarning)
            {
                previewVM.IsEnabled = !(previewVM.IsEditable && selectedTestSet.Warning.WarningType.Equals(WarningType.Layout));
            }
        }

        private void getValidatedTables(IList<PreviewTableModel> previewTables, DataTable table)
        {
            var dataTable = getValidatedPreviewTable(table);
            if (dataTable != null)
            {
                previewTables.Add(new PreviewTableModel
                {
                    DataTable = dataTable
                });
            }
        }

        private void getTables(IList<PreviewTableModel> previewTables, DataTable table)
        {
            var dataTable = getPreviewTable(table);
            if (dataTable != null)
            {
                previewTables.Add(new PreviewTableModel
                {
                    DataTable = dataTable
                });
            }
        }

        private DataTable getValidatedPreviewTable(DataTable table)
        {
            int tableIndex = getTableIndex(table);
            if (tableIndex == -1)
            {
                table.ExtendedProperties.Add("TableId", table.TableName);
                return table;
            }

            var dataTable = new DataTable("DataTable");
            dataTable.TableName = _iqBot.DataModel.Tables[tableIndex].Name;
            dataTable.ExtendedProperties.Add("TableId", table.TableName);

            if (table.Rows.Count == 0)
            {
                for (int i = 0; i < _iqBot.DataModel.Tables[tableIndex].Columns.Count; i++)
                {
                    DataColumn column = new DataColumn(_iqBot.DataModel.Tables[tableIndex].Columns[i].Id, typeof(ValidatedFieldValue));
                    column.Caption = _iqBot.DataModel.Tables[tableIndex].Columns[i].Name;
                    dataTable.Columns.Add(column);
                }
                return dataTable;
            }

            for (int i = 0; i < table.Rows[0].ItemArray.Length; i++)
            {
                if (table.Rows[0].ItemArray[i] is ValidatedFieldValue)
                {
                    ValidatedFieldValue validatedFieldValue = (ValidatedFieldValue)(table.Rows[0].ItemArray[i]);
                    DataColumn column = new DataColumn(validatedFieldValue.FieldDefId, typeof(ValidatedFieldValue));
                    column.Caption = getColumnCaption(validatedFieldValue.FieldDefId, tableIndex);

                    if (string.IsNullOrEmpty(column.Caption))
                    {
                        column.Caption = validatedFieldValue.FieldName;
                    }
                    dataTable.Columns.Add(column);
                }
            }

            foreach (DataRow row in table.Rows)
            {
                var newRow = dataTable.NewRow();
                for (int index = 0; index < newRow.ItemArray.Length; index++)
                {
                    var cell = row.ItemArray[index];
                    if (cell != null)
                    {
                        if (cell is ValidatedFieldValue)
                        {
                            newRow[index] = cell;
                        }
                    }
                }
                dataTable.Rows.Add(newRow);
            }

            return dataTable;
        }

        private DataTable getPreviewTable(DataTable table)
        {
            int tableIndex = getTableIndex(table);
            if (tableIndex == -1)
            {
                return null;
            }

            var dataTable = new DataTable("DataTable");
            dataTable.TableName = _iqBot.DataModel.Tables[tableIndex].Name;
            dataTable.ExtendedProperties.Add("TableId", table.TableName);

            if (table.Rows.Count == 0)
            {
                return dataTable;
            }

            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
            {
                dataTable.Columns.Add(table.Columns[columnIndex].ColumnName);
                dataTable.Columns[columnIndex].Caption = getColumnCaption(table.Columns[columnIndex].ColumnName, tableIndex);
            }

            foreach (DataRow row in table.Rows)
            {
                var newRow = dataTable.NewRow();
                for (int index = 0; index < newRow.ItemArray.Length; index++)
                {
                    var cellValue = row.ItemArray[index];
                    if (cellValue != null)
                    {
                        newRow[index] = cellValue;
                    }
                    else
                    {
                        newRow[index] = string.Empty;
                    }
                }
                dataTable.Rows.Add(newRow);
            }

            return dataTable;
        }

        private string getColumnCaption(string columnId, int tableIndex)
        {
            var column = _iqBot.DataModel.Tables[tableIndex].Columns.FirstOrDefault(c => c.Id.Equals(columnId));
            if (column != null)
            {
                return column.Name;
            }

            return string.Empty;
        }

        private int getTableIndex(DataTable table)
        {
            for (int index = 0; index < _iqBot.DataModel.Tables.Count; index++)
            {
                if (_iqBot.DataModel.Tables[index].Id.Equals(table.TableName))
                {
                    return index;
                }
            }
            return -1;
        }

        private DataColumn getColumnName(string columnId, int tableIndex)
        {
            var column = _iqBot.DataModel.Tables[tableIndex].Columns.FirstOrDefault(c => c.Id.Equals(columnId));
            if (column != null)
            {
                return new DataColumn(column.Name);
            }

            return new DataColumn();
        }

        private ObservableCollection<PreviewFieldModel> getValidatedFields(DataTable dataTable)
        {
            IList<PreviewFieldModel> fields = new List<PreviewFieldModel>();
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    PreviewFieldModel field = null;
                    var validatedFieldValue = row.ItemArray[i];
                    if (validatedFieldValue is ValidatedFieldValue)
                    {
                        ValidatedFieldValue validatedField = (ValidatedFieldValue)validatedFieldValue;
                        field = new PreviewFieldModel
                        {
                            FieldDef = new FieldDef { Id = validatedField.FieldDefId, Name = getFieldName(validatedField.FieldDefId) },
                            Value = validatedField
                        };

                        if (string.IsNullOrEmpty(field.FieldDef.Name))
                        {
                            field.FieldDef.Name = validatedField.FieldName;
                        }
                    }
                    if (field != null)
                    {
                        fields.Add(field);
                    }
                }
            }
            return new ObservableCollection<PreviewFieldModel>(fields);
        }

        private ObservableCollection<PreviewFieldModel> getFields(DataTable dataTable)
        {
            IList<PreviewFieldModel> fields = new List<PreviewFieldModel>();
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                for (int columnIndex = 0; columnIndex < row.ItemArray.Length; columnIndex++)
                {
                    var field = new PreviewFieldModel
                    {
                        FieldDef = new FieldDef { Id = dataTable.Columns[columnIndex].ColumnName, Name = getFieldName(dataTable.Columns[columnIndex].ColumnName) },
                        Value = dataTable.Rows[0][columnIndex] as ValidatedFieldValue //dataTable.Rows[0][columnIndex].ToString()
                    };
                    fields.Add(field);
                }
            }
            return new ObservableCollection<PreviewFieldModel>(fields);
        }

        private string getFieldName(string fieldId)
        {
            var field = _iqBot.DataModel.Fields.FirstOrDefault(f => f.Id.Equals(fieldId));
            if (field != null)
            {
                return field.Name;
            }

            return string.Empty;
        }

        private TestSetViewModel _testSetViewModel;
        private LayoutViewModel _layoutViewModel;
        private BluePrintViewModel _bluePrintViewModel;

        public event SaveRequestHandler SaveRequested;

        public event EventHandler TestRequested;

        public event FieldValueRequestHandler LayoutFieldValueRequested;

        public event TestDocSetItemAddHandler TestDocSetModificationRequested;

        private void FieldTree_OnLayoutFieldSelectionChanged(object sender, LayoutFieldSelectionEventArgs e)
        {
            try
            {
                screenResources.RightPanelVM.UpdateLayoutFieldContext(screenResources.SelectedLayout, e.LayoutField);

                if (e.LayoutField != null
                    && (!e.LayoutField.IsSelected
                        || e.LayoutField != screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField))
                {
                    if (screenResources?.CanvasImageViewModel?.ImageViewInfo?.SelectedField != null)
                    {
                        screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField.IsSelected = false;
                    }
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField = e.LayoutField;
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField.IsSelected = true;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(FieldTree_OnLayoutFieldSelectionChanged));
            }
        }

        private Layout findLayoutOfCurrentField(LayoutField layoutfield)
        {
            foreach (Layout layout in screenResources.IqBot.Layouts)
            {
                if (layoutfield.Type == ElementType.Field)
                {
                    if (layout.LayoutFields.Fields.Contains(layoutfield))
                        return layout;
                }
                else if (layoutfield.Type == ElementType.Table)
                {
                    foreach (LayoutTable table in layout.LayoutTables.Tables)
                    {
                        if (table.Fields.Contains(layoutfield))
                            return layout;
                    }
                }
                else if (layoutfield.Type == ElementType.Marker)
                {
                    if (layout.Markers.Fields.Contains(layoutfield))
                        return layout;
                }
            }
            return null;
        }

        private LayoutTable findLayoutTableOfCurrentField(LayoutField layoutfield)
        {
            foreach (Layout layout in screenResources.IqBot.Layouts)
            {
                foreach (LayoutTable table in layout.LayoutTables.Tables)
                {
                    if (table.Fields.Contains(layoutfield))
                        return table;
                }
            }
            return null;
        }

        private void rightPanelVM_SaveRequested(object sender, EventArgs e)
        {
            bool verifyStructure = e is SaveEventArgs && ((SaveEventArgs)e).VerifyStructureMismatch;
            bool useDispatcher = e is SaveEventArgs && ((SaveEventArgs)e).SaveUsingDispatcher;
            if (useDispatcher)
            {
                this.Dispatcher.BeginInvoke(new Action(() => saveVisionBot(verifyStructure)), DispatcherPriority.Background);
            }
            else
            {
                saveVisionBot(verifyStructure);
            }
        }

        private void saveVisionBot(bool verifyStructure)
        {
            try
            {
                if (verifyStructure)
                {
                    updateWarningVisibility();
                }
                onSaveRequested(new SaveRequestEventArgs(_iqBot));
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(saveVisionBot));
            }
        }

        private void updateIQBotSetting(ImageProcessingConfig settings)
        {
            _iqBot.Settings = settings;
            //If setting changed in one layout should change in all layout
            foreach (Layout l in _iqBot.Layouts)
            {
                l.Settings = settings;
            }
        }

        internal event LayoutRescanEventHandler LayoutRescanRequested;

        private void rightPanelVM_LayoutRescanRequested(object sender, EventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                if (LayoutRescanRequested != null)
                {
                    if (showLayoutRescanConfirmationMessage(Properties.Resources.LayoutRescanWarning))
                    {
                        PreviewEventArgs previewEventArgs = new PreviewEventArgs(screenResources.SelectedLayout, _iqBot);
                        handleActionWithDocumentProcessingProgressBar(new Action(() =>
                        {
                            LayoutRescanRequested(sender, previewEventArgs);

                        }));

                        updateCanvasDetails(screenResources.SelectedLayout, screenResources.DesignerInfo.RibbonTabButtons[0].IsSettingsVisible);
                    }
                }
            }));
        }

        public event FieldTree.LayoutCreateHander LayoutCreated;

        public event ToolBar.PreviewEventHandler PreviewRequested;

        private void FieldTree_OnLayoutCreated(object sender, FieldTree.LayoutCreationEventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                if (LayoutCreated != null)
                {
                    handleActionWithDocumentProcessingProgressBar(new Action(() =>
                    {
                        LayoutCreated(IqBot, e);

                    }));

                    if (e.LayoutCreationOperation == LayoutCreationOperation.New)
                    {
                        if (e.NewLayout != null && string.IsNullOrWhiteSpace(e.NewLayout.Id))
                        {
                            e.NewLayout.Id = new IqBotIDGenerationHelper().GetNewLayoutID(screenResources.IqBot);
                        }
                        e.NewLayout.IsSelected = true;

                        UpdateLayoutAndTestWarning(e.NewLayout, _testSetViewModel);
                    }
                }
            }));
        }

        private void UpdateLayoutAndTestWarning(Layout layout, TestSetViewModel testSets)
        {
            screenResources.UpdateLayoutWarning(layout);
            LayoutTestSetViewModel TestSet = testSets.Items.FirstOrDefault(d => d.LayoutTestSet.LayoutId.Equals(layout.Id));
            UpdateTestSetWarningVisibility(layout, TestSet);
        }

        private void FieldTree_OnLayoutSelectionChanged(object sender, LayoutSelectionEventArgs e)
        {
            try
            {
                screenResources.DesignerInfo.FileName = _iqBot.Name;
                screenResources.DesignerInfo.LayoutName = e.Layout.Name;
                screenResources.DesignerInfo.DocumentName = e.Layout.DocumentName;
                screenResources.LayoutVisible = true;

                screenResources.HeaderPanelViewModel.IsLayoutSelected = true;
                screenResources.HeaderPanelViewModel.LayoutName = e.Layout.Name;
                screenResources.HeaderPanelViewModel.IsDocumentSelected = true;
                screenResources.HeaderPanelViewModel.LayoutDocumentName = e.Layout.DocumentProperties.Name;
                ResetPreview();
                if (screenResources.SelectedLayout != e.Layout)
                {
                    if (e.Layout.ImageProvider == null)
                    {
                        e.Layout.ImageProvider = new UIImageProvider(e.Layout.DocumentProperties, e.Layout.Settings);
                        e.Layout.ImageProvider.DocumentPageImageRequested += ImageProvider_DocumentPageImageRequested;
                    }
                    screenResources?.SelectedLayout?.ImageProvider?.PauseProcess();
                    screenResources.SelectedLayout = e.Layout;

                    updateCanvasDetails(e.Layout, false);
                    screenResources?.SelectedLayout?.ImageProvider?.StartOrResumeProcessing();
                }

                //TODO : Need to optimize this code by removing duplication
                //if (IsShowLayoutSettings())
                //{
                //    screenResources.UpdateLayoutContext(e.Layout, screenResources.CanvasImageViewModel);
                //}
                //else
                screenResources.UpdateLayoutContext(e.Layout);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(FieldTree_OnLayoutSelectionChanged));
            }
        }

        private static bool IsShowLayoutSettings()
        {
            bool isShowLayoutSettings = false;

            //TODO: Need to read this setting from setting file and set.

            return isShowLayoutSettings;
        }

        private void updateCanvasDetails(Layout layout, bool doesRequireToCreateNewImage)
        {
            try
            {
                if (screenResources.CanvasImageViewModel != null)
                {
                    screenResources.CanvasImageViewModel.ImageViewInfo.LayoutFieldSelectionChanged -=
                        FieldTree_OnLayoutFieldSelectionChanged;
                }

                LoadingWindow window = new LoadingWindow
                {
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    Owner = Window.GetWindow(this)
                };

                ImageViewInfo imageViewInfo = null;

                var worker = new Thread(() =>
                {
                    window.Dispatcher.Invoke(new Action(() => clearFieldsFromCanvasImageViewModel(screenResources.CanvasImageViewModel)), DispatcherPriority.Background);
                    window.Dispatcher.Invoke(new Action(() => clearImageInfoFromCanvasImageViewModel(screenResources.CanvasImageViewModel)), DispatcherPriority.Background);

                    VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();
                    imageViewInfo = vbotResourceHelper.CreateImageViewInfoWithoutImagesAndRegions(layout, _iqBot.Layouts.Count);

                    window.Dispatcher.Invoke(new Action(() => createCanvasImageInfo(imageViewInfo)), DispatcherPriority.Background);
                    IList<RegionViewModel> allRegions = vbotResourceHelper.GetAllFields(layout);

                    if (layout.ImageProvider == null)
                    {
                        layout.ImageProvider = new UIImageProvider(layout.DocumentProperties, layout.Settings);
                        layout.ImageProvider.DocumentPageImageRequested += ImageProvider_DocumentPageImageRequested;
                    }
                    assignImagesAndRegionsToImageViewInfoForLayout(layout, layout.DocumentProperties, imageViewInfo, allRegions, _iqBot.Settings, doesRequireToCreateNewImage);

                    var formClose = new LoadingWindow.FromClose(window.Close);

                    window.Dispatcher.Invoke(DispatcherPriority.Background, formClose);
                });

                worker.Start();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(updateCanvasDetails));
            }
        }

        private void clearFieldsFromCanvasImageViewModel(CanvasImageViewModel canvasImageViewModel)
        {
            try
            {
                if (canvasImageViewModel != null && canvasImageViewModel.ImageViewInfo != null)
                {
                    if (canvasImageViewModel.ImageViewInfo.Fields != null)
                    {
                        canvasImageViewModel.ImageViewInfo.Fields.Clear();
                    }
                    //canvasImageViewModel.ImageViewInfo.SelectedValueBounds = Rect.Empty;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(clearFieldsFromCanvasImageViewModel));
            }
        }

        private void clearImageInfoFromCanvasImageViewModel(CanvasImageViewModel canvasImageViewModel)
        {
            try
            {
                if (canvasImageViewModel != null && canvasImageViewModel.ImageViewInfo != null)
                {
                    canvasImageViewModel.ImageViewInfo.ImageSource = null;
                    canvasImageViewModel.ImageViewInfo = null;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(clearFieldsFromCanvasImageViewModel));
            }
        }

        private void createCanvasImageInfo(ImageViewInfo imageViewInfo)
        {
            try
            {
                screenResources.CanvasImageViewModel = new CanvasImageViewModel(ctrlImage)
                {
                    ImageViewInfo = imageViewInfo,
                    InspectionViewModel = screenResources.RightPanelVM,
                    HasItems = _iqBot.Layouts.Count > 0
                };
                screenResources.CanvasImageViewModel.Save += CanvasImageViewModel_Save;
                screenResources.CanvasImageViewModel.LayoutFieldValueRequested += CanvasImageViewModel_LayoutFieldValueRequested;
                screenResources.CanvasImageViewModel.ImageViewInfo.DeleteUserDefinedRegion += ImageViewInfo_DeleteUserDefinedRegion;
                screenResources.CanvasImageViewModel.ImageViewInfo.LayoutFieldSelectionChanged +=
                    FieldTree_OnLayoutFieldSelectionChanged;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(createCanvasImageInfo));
            }
        }

        private void assignImagesAndRegionsToImageViewInfoForTestItem(DocSetItem docSetItem, VisionBotEngine.Model.DocumentProperties documentProperties, ImageViewInfo imageViewInfo, IList<RegionViewModel> allRegions, ImageProcessingConfig settings, bool doesRequireToCreateNewImage)
        {
            try
            {
                IList<PageRegions> pagewiseRegions = new PagewiseRegionDivider().Divide(documentProperties, allRegions);
                List<Action> actions = new List<Action>();

                for (int i = 0; i < imageViewInfo.ImageSource.Count; i++)
                {
                    PageImageSource pageImageSource = imageViewInfo.ImageSource[i];

                    PageRegions pageRegionsForPageIndex = (from q in pagewiseRegions where q.PageIndex == pageImageSource.PageIndex select q).FirstOrDefault();
                    if (i == 0)
                    {
                        VisionBotDesignerResourcesHelper resourceHelper = new VisionBotDesignerResourcesHelper();

                        if (docSetItem.ImageProvider != null)
                        {
                            docSetItem.ImageProvider.CurrentViewIndex = i;
                            docSetItem.ImageProvider.StartOrResumeProcessing();
                        }
                        else
                        {
                            docSetItem.ImageProvider = new UIImageProvider(docSetItem.DocumentProperties, settings.Clone());
                            docSetItem.ImageProvider.DocumentPageImageRequested += ImageProvider_DocumentPageImageRequested;
                            docSetItem.ImageProvider.StartOrResumeProcessing();
                        }
                    }

                    assignPageRegionsToImageViewInfo(imageViewInfo, pageImageSource.PageIndex, pageRegionsForPageIndex);
                    imageViewInfo.PageRegions.Add(pageRegionsForPageIndex);
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[assignImagesAndRegionsToImageViewInfo] - Exception occured {0}.", ex.Message));
            }
        }

        private void assignImagesAndRegionsToImageViewInfoForLayout(Layout layout, VisionBotEngine.Model.DocumentProperties documentProperties, ImageViewInfo imageViewInfo, IList<RegionViewModel> allRegions, ImageProcessingConfig settings, bool doesRequireToCreateNewImage)
        {
            try
            {
                IList<PageRegions> pagewiseRegions = new PagewiseRegionDivider().Divide(documentProperties, allRegions);
                List<Action> actions = new List<Action>();

                for (int i = 0; i < imageViewInfo.ImageSource.Count; i++)
                {
                    PageImageSource pageImageSource = imageViewInfo.ImageSource[i];

                    PageRegions pageRegionsForPageIndex = (from q in pagewiseRegions where q.PageIndex == pageImageSource.PageIndex select q).FirstOrDefault();
                    if (i == 0)
                    {
                        VisionBotDesignerResourcesHelper resourceHelper = new VisionBotDesignerResourcesHelper();
                        if (layout.ImageProvider != null)
                        {
                            layout.ImageProvider.CurrentViewIndex = i;
                            layout.ImageProvider.StartOrResumeProcessing();
                        }
                    }

                    assignPageRegionsToImageViewInfo(imageViewInfo, pageImageSource.PageIndex, pageRegionsForPageIndex);
                    imageViewInfo.PageRegions.Add(pageRegionsForPageIndex);
                }

                this.Dispatcher.BeginInvoke(new Action(() => SetCurrentPage()), DispatcherPriority.Background);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(assignImagesAndRegionsToImageViewInfoForLayout));
            }
        }

        private void assignPageRegionsToImageViewInfo(ImageViewInfo imageViewInfo, int pageIndex, PageRegions pageRegionsForPageIndex)
        {
            try
            {
                if (pageRegionsForPageIndex != null)
                {
                    for (int j = 0; j < pageRegionsForPageIndex.RegionsInChunks.Count; j++)
                    {
                        IList<RegionViewModel> pagewiseRegionsToConsider = pageRegionsForPageIndex.RegionsInChunks[j];
                        VBotLogger.Trace(() => string.Format("[assignPageRegionsToImageViewInfo] Queuing call for assignRegionsToImageViewInfo. PageIndex: {0} ChunkIndex: {1} ChunkCount: {2}", pageRegionsForPageIndex.PageIndex, j, pagewiseRegionsToConsider.Count));
                        this.Dispatcher.BeginInvoke(new Action(() => assignRegionsToImageViewInfo(imageViewInfo, pagewiseRegionsToConsider, pageIndex)), DispatcherPriority.Background);
                    }
                }
                else
                {
                    VBotLogger.Error(() => string.Format("[assignPageRegionsToImageViewInfo] Could not found PageRegions for PageIndex: {0}", pageIndex));
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(assignPageRegionsToImageViewInfo));
            }
        }

        private void assignImageToPageImageSource(PageImageSource pageImageSource, System.Windows.Media.Imaging.BitmapImage bitmapImage)
        {
            try
            {
                pageImageSource.PageImage = bitmapImage;
                pageImageSource.DpiX = bitmapImage != null ? Convert.ToInt32(bitmapImage.DpiX) : 96;
                pageImageSource.DpiY = bitmapImage != null ? Convert.ToInt32(bitmapImage.DpiY) : 96;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(assignImageToPageImageSource));
            }
        }

        private void assignRegionsToImageViewInfo(ImageViewInfo imageViewInfo, IList<RegionViewModel> regions, int index)
        {
            try
            {
                for (int i = 0; i < regions.Count; i++)
                {
                    imageViewInfo.Fields.Add(regions[i]);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(assignRegionsToImageViewInfo));
            }
        }

        private void CanvasImageViewModel_LayoutFieldValueRequested(object sender, LayoutFieldValueRequestedEventArgs e)
        {
            if (LayoutFieldValueRequested != null)
            {
                LoadingWindow window = new LoadingWindow
                {
                    WindowStartupLocation =
                   WindowStartupLocation.CenterOwner,
                    Owner = Window.GetWindow(this)
                };

                System.Threading.Tasks.Task task = new System.Threading.Tasks.Task(() =>
                {
                    FieldValueRequestEventArgs args = new FieldValueRequestEventArgs(screenResources.IqBot, screenResources.SelectedLayout, e.LayoutField);
                    handleExceptionWithMessage(new Action(() =>
                    {
                        LayoutFieldValueRequested(sender, args);
                        e.ValueField = args.ValueField;
                    }));
                    window.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => window.Close()));
                });

                window.InitWork(task);
                window.ShowDialog();
            }
        }

        private void ImageViewInfo_DeleteUserDefinedRegion(object sender, DeleteUserDefinedRegionEventArgs e)
        {
            var layoutField = e.RegionViewModel.Region as LayoutField;
            if (layoutField != null)
            {
                screenResources.SelectedLayout.SystemIdentifiedFields.Remove(layoutField);
                onSaveRequested(new SaveRequestEventArgs(_iqBot));
            }
        }

        private void CanvasImageViewModel_Save(object sender, EventArgs e)
        {
            if (screenResources.CanvasImageViewModel.AddedField != null)
            {
                screenResources.SelectedLayout.SystemIdentifiedFields.Add(screenResources.CanvasImageViewModel.AddedField);
                onSaveRequested(new SaveRequestEventArgs(_iqBot));
            }
        }

        private void onSaveRequested(SaveRequestEventArgs e)
        {
            if (SaveRequested != null)
            {
                handleExceptionWithMessage(new Action(() =>
                {
                    SaveRequested(this, e);
                }));
            }
        }

        private void loadScreenResources()
        {
            VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();

            screenResources = vbotResourceHelper.CreateVisionBotDesignerResources(_iqBot, ctrlImage);

            this.DataContext = screenResources;

            loadLeftPanel();
            //LayoutViewModel instance is created here in loadLeftPanel method. Created one property in VisionBotDesignerResources.
            //ans Assigning same _layoutViewModel instance to that property. So we can accesss LayoutViewModel instance from
            //VisionBotDesignerResources.
            if (_layoutViewModel != null)
            {
                screenResources.LayoutViewModel = _layoutViewModel;
            }
            else
            {
                VBotLogger.Error(() => "[loadScreenResources] _layoutViewModel instance is null. It should not be null.");
            }
            screenResources.RightPanelVM.CancelFieldDefRequested += RightPanelVM_CancelFieldDefRequested;

            screenResources.SaveVisionBotHander += screenResources_SaveVisionBotHander;
            screenResources.SaveRequested += rightPanelVM_SaveRequested;
            screenResources.LayoutRescanRequested += rightPanelVM_LayoutRescanRequested;

            screenResources.FieldTreeVM.SelectionChanged += FieldTreeVM_SelectionChanged;

            screenResources.DesignerInfo.RibbonTabButtons[0].IsSettingsVisible = Configurations.Visionbot.IsIQBotSettingsVisible;
        }

        private void RightPanelVM_CancelFieldDefRequested(FieldDef obj)
        {
            try
            {
                //if (!((LayoutItem)((FieldTreeVM.Items as ObservableCollection<object>)[0])).IsSelected)
                //    ((LayoutItem)((FieldTreeVM.Items as ObservableCollection<object>)[0])).IsSelected = true;
                if (!_bluePrintViewModel.BluePrintFields.IsSelected) _bluePrintViewModel.BluePrintFields.IsSelected = true;
                else screenResources.RightPanelVM.UpdateContext(UpdateMode.Add, null, null);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(RightPanelVM_CancelFieldDefRequested));
            }
        }

        private void loadLeftPanel()
        {
            loadLayoutViewModel();

            loadBluePrintViewModel();
        }

        private void loadBluePrintViewModel()
        {
            VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();
            _bluePrintViewModel = vbotResourceHelper.GetBluePrintViewModel(IqBot);

            _bluePrintViewModel.SelectionChanged += BluePrintViewModel_SelectionChanged;
            _bluePrintViewModel.SaveRequested += screenResources_SaveVisionBotHander;

            _bluePrintViewModel.DeletedHandler += BluePrintViewModelItem_DeletedHandler;
            _bluePrintViewModel.BluePrintTables.CreateTableHandler += BluePrintTables_CreateTableHandler;
            _bluePrintViewModel.OnMoveColumnToAnotherTable += BluePrintTables_OnMoveColumnToAnotherTable;
        }

        private void BluePrintTables_OnMoveColumnToAnotherTable(TableDef arg1, FieldDef arg2, TableDef arg3)
        {
            if (ReArrageDataModelFieldRequested != null)
            {
                ReArrageDataModelFieldRequested(IqBot);
            }
            removeTableFieldFromAllLayout(arg2);
        }

        private void loadLayoutViewModel()
        {
            VBotLogger.Trace(() => "[Load Layouts] Started... ");

            VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();

            _layoutViewModel = vbotResourceHelper.GetLayotViewModel(IqBot);
            _layoutViewModel.DocumentsSelectionRequested += testSetViewModel_DocumentsSelectionRequested;
            _layoutViewModel.SelectionChanged += LayoutViewModel_SelectionChanged;
            _layoutViewModel.LayoutCreated += FieldTree_OnLayoutCreated;

            _layoutViewModel.LayoutSaveRequested += screenResources_SaveVisionBotHander;
            _layoutViewModel.DeletedHandler += layoutViewModelItem_DeletedHandler;

            VBotLogger.Trace(() => "[Load Layouts] Sucessfully Loaded... ");
        }

        private void BluePrintViewModelItem_DeletedHandler(object sender, BluePrintItemDeletedEventArgs e)
        {
            try
            {
                bool verifyStructure = false;
                if (e.Mode == BluePrintItemDeleteMode.Field)
                {
                    removeFieldFromAllAlyout(e.Field);
                    verifyStructure = true;
                }
                else if (e.Mode == BluePrintItemDeleteMode.TableField)
                {
                    removeTableFieldFromAllLayout(e.Field);
                    verifyStructure = true;
                }
                else if (e.Mode == BluePrintItemDeleteMode.Table)
                {
                    removeTableFromAllAlyout(e.Table);
                    verifyStructure = true;
                }

                screenResources.UpdateLayoutWarning();
                if (verifyStructure)
                {
                    updateWarningVisibility();
                }
                screenResources_SaveVisionBotHander(sender, e);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(BluePrintViewModelItem_DeletedHandler));
            }
        }

        private void BluePrintViewModel_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var obj = sender;

                if (obj != null)
                {
                    if ((obj is BluePrintFieldsViewModel && ((BluePrintFieldsViewModel)obj).Name == "Fields"))
                    {
                        screenResources.DataModelVM.TablesVisible = false;
                        screenResources.DataModelVM.Fields = IqBot.DataModel.Fields;

                        screenResources.DataModelVM.Tables = null;
                        screenResources.DataModelVM.TableDef = null;

                        screenResources.DataModelVM.SelectedField = null;
                        screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                    }
                    else if (obj is BluePrintTablesViewModel && ((BluePrintTablesViewModel)obj).Name == "Tables")
                    {
                        screenResources.DataModelVM.TablesVisible = true;
                        screenResources.DataModelVM.Fields = null;
                        screenResources.DataModelVM.Tables = IqBot.DataModel.Tables;
                        screenResources.DataModelVM.TableDef = null;
                        screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                    }
                    else if (obj is TableDef)
                    {
                        screenResources.DataModelVM.TablesVisible = false;
                        screenResources.DataModelVM.Fields = ((TableDef)obj).Columns;
                        screenResources.DataModelVM.Tables = null;
                        screenResources.DataModelVM.SelectedField = null;
                        screenResources.DataModelVM.TableDef = ((TableDef)obj);
                        screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                    }
                    else if (obj is FieldDef)
                    {
                        screenResources.DataModelVM.TablesVisible = false;
                        FieldDef selectedFieldDef = obj as FieldDef;
                        if (IqBot.DataModel.Fields.Contains(selectedFieldDef))
                        {
                            screenResources.DataModelVM.Fields = IqBot.DataModel.Fields;
                            screenResources.DataModelVM.Tables = null;
                            screenResources.DataModelVM.TableDef = null;
                            screenResources.DataModelVM.SelectedField = selectedFieldDef;
                            screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                        }
                        else
                        {
                            foreach (TableDef table in IqBot.DataModel.Tables)
                            {
                                if (table.Columns.Contains(selectedFieldDef))
                                {
                                    screenResources.DataModelVM.Fields = table.Columns;
                                    screenResources.DataModelVM.Tables = null;
                                    screenResources.DataModelVM.TableDef = table;
                                    screenResources.DataModelVM.SelectedField = selectedFieldDef;
                                    screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, table, obj);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[BluePrintViewModel_SelectionChanged] Exception occured {0}.", ex.Message));
            }
        }

        internal event LayoutDeletedHandler LayoutDeleted;

        private void layoutViewModelItem_DeletedHandler(object sender, LayoutItemDeletedEventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                //  screenResources.CanvasImageViewModel.ImageViewInfo.
                if (e.Mode == LayoutItemDeleteMode.Field)
                {
                    screenResources.RemoveFieldFromImageView(e.Field);
                    screenResources.ResetValueSelection();
                    UpdateLayoutAndTestWarning(e.Layout, _testSetViewModel);
                }
                else if (e.Mode == LayoutItemDeleteMode.TableField)
                {
                    screenResources.RemoveFieldFromImageView(e.Field);
                    UpdateLayoutAndTestWarning(e.Layout, _testSetViewModel);
                }
                else if (e.Mode == LayoutItemDeleteMode.Marker)
                {
                    screenResources.RemoveFieldFromImageView(e.Field);
                }
                else if (e.Mode == LayoutItemDeleteMode.Table)
                {
                    foreach (var field in e.Table.Fields)
                    {
                        screenResources.RemoveFieldFromImageView(field);
                    }
                    UpdateLayoutAndTestWarning(e.Layout, _testSetViewModel);
                }
                else if (e.Mode == LayoutItemDeleteMode.Layout)
                {
                    if (LayoutDeleted != null)
                    {
                        LayoutDeleted(sender, new LayoutDeletedEventArgs(screenResources.IqBot, e.Layout));
                    }
                }

                if (_iqBot.Layouts.Count == 0)
                {
                    screenResources.CanvasImageViewModel.HasItems = false;
                    screenResources.CanvasImageViewModel.LayoutName = string.Empty;
                    screenResources.CanvasImageViewModel.DocumentPath = string.Empty;
                    screenResources.DesignerInfo.LayoutName = string.Empty;
                    screenResources.DesignerInfo.DocumentName = string.Empty;
                    screenResources.LayoutVisible = false;
                    screenResources.HeaderPanelViewModel.IsLayoutSelected = false;
                    screenResources.HeaderPanelViewModel.IsDocumentSelected = false;
                    screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, null);
                }
                ResetPreview();
            }));
        }

        private void LayoutViewModel_SelectionChanged(object sender, LayoutItemSelectionEventArgs e)
        {
            try
            {
                var selectedLayoutItem = e.LayoutItem;

                if (selectedLayoutItem is Layout ||
                    (e.Layout != null && e.Layout != screenResources.SelectedLayout))
                {
                    FieldTree_OnLayoutSelectionChanged(sender, new LayoutSelectionEventArgs(e.Layout));
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField = null;
                }

                if (selectedLayoutItem is Layout)
                {
                    return;
                }

                if ((selectedLayoutItem is LayoutFieldsViewModel && ((LayoutFieldsViewModel)selectedLayoutItem).Name == "Fields"))
                {
                    screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, null);
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField = null;
                }
                else if (selectedLayoutItem is LayoutTablesViewModel && ((LayoutTablesViewModel)selectedLayoutItem).Name == "Tables")
                {
                    screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, null);
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField = null;
                }
                else if (selectedLayoutItem is LayoutFieldsViewModel && ((LayoutFieldsViewModel)selectedLayoutItem).Name == "Markers")
                {
                    screenResources.RightPanelVM.UpdateContext(UpdateMode.Edit, null, null);
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField = null;
                }
                else if (selectedLayoutItem is LayoutTable)
                {
                    screenResources.RightPanelVM.UpdateLayoutTableContext(sender as LayoutTable);
                    screenResources.CanvasImageViewModel.ImageViewInfo.SelectedField = null;
                }
                else
                {
                    var layoutField = sender as LayoutField;
                    if (null != layoutField)
                    {
                        screenResources.UpdateCanvasSelectedValue(layoutField);
                        FieldTree_OnLayoutFieldSelectionChanged(sender, new LayoutFieldSelectionEventArgs(layoutField));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(LayoutViewModel_SelectionChanged));
            }
        }

        private void ResetPreview()
        {
            ctrlToolBar.ResetPreview();
            screenResources.RightPanelVM.IsPreviewOn = false;
        }

        //TODO :Remove This code after removing treeview
        private void FieldTreeVM_SelectionChanged(object obj)
        {
        }

        //TODO :Remove This code after removing treeview
        private void FieldTree_OnLayoutDeleteRequested(object sender, LayoutDeleteEventArgs e)
        {
            try
            {
                if (e.LayoutItemDeleteRequested is Layout)
                {
                    var layout = (Layout)e.LayoutItemDeleteRequested;
                    if (showDeleteConfirmation(string.Format(Properties.Resources.LayoutRemoveWarning, layout.Name, Properties.Resources.LayoutLabel)))
                    {
                        _iqBot.Layouts.Remove(layout);
                        layout.ImageProvider.Dispose();
                        layout.ImageProvider = null;
                    }
                }
                else if (e.LayoutItemDeleteRequested is LayoutTable)
                {
                    var layoutTable = (LayoutTable)e.LayoutItemDeleteRequested;
                    if (
                        showDeleteConfirmation(
                            string.Format(Properties.Resources.TableRemoveWarning, layoutTable.TableDef.Name)))
                    {
                        removeTableFromLayout(layoutTable);
                    }
                }
                else if (e.LayoutItemDeleteRequested is LayoutField)
                {
                    deleteLayoutField(e.LayoutItemDeleteRequested as LayoutField);
                }
                onSaveRequested(new SaveRequestEventArgs(_iqBot));

                if (_iqBot.Layouts.Count == 0)
                {
                    screenResources.CanvasImageViewModel.HasItems = false;
                    screenResources.CanvasImageViewModel.LayoutName = string.Empty;
                    screenResources.CanvasImageViewModel.DocumentPath = string.Empty;
                    screenResources.DesignerInfo.LayoutName = string.Empty;
                    screenResources.DesignerInfo.DocumentName = string.Empty;
                    screenResources.LayoutVisible = false;
                    screenResources.HeaderPanelViewModel.IsLayoutSelected = false;
                    screenResources.HeaderPanelViewModel.IsDocumentSelected = false;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(FieldTree_OnLayoutDeleteRequested));
            }
        }

        //TODO :Remove This code after removing treeview
        private void deleteLayoutField(LayoutField layoutField)
        {
            if (layoutField != null)
            {
                if (layoutField.Type == ElementType.Field || layoutField.Type == ElementType.Marker)
                {
                    bool shouldContinue = confirmDeletion(layoutField);
                    if (shouldContinue)
                    {
                        removeFieldFromLayout(layoutField);
                    }
                }
                else if (layoutField.Type == ElementType.Table)
                {
                    LayoutTable table = findLayoutTableOfCurrentField(layoutField);
                    if (table != null)
                    {
                        if (table.PrimaryField == layoutField)
                        {
                            if (table.Fields.Count > 1)
                            {
                                showRestrictActionMessage(Properties.Resources.LayoutTablePrimaryColumnRemoveMessage);
                            }
                            else
                            {
                                removeTableFromLayout(table);
                            }
                        }
                        else
                        {
                            if (table.Fields.Count > 1)
                            {
                                removeFieldFromLayout(layoutField);
                            }
                            else
                            {
                                removeTableFromLayout(table);
                            }
                        }
                    }
                }
            }
        }

        //TODO :Remove This code after removing treeview
        private bool confirmDeletion(LayoutField layoutField)
        {
            bool shouldContinue = false;
            if (layoutField.Type == ElementType.Marker)
            {
                string confirmationMessage = getDeleteConfirmationMessage(ElementType.Marker, layoutField.MarkerName);
                shouldContinue = showDeleteConfirmation(confirmationMessage);
            }
            else if (layoutField.Type == ElementType.Field)
            {
                shouldContinue = true;
            }
            return shouldContinue;
        }

        //TODO :Remove This code after removing treeview
        private string getDeleteConfirmationMessage(ElementType elementType, string name)
        {
            string type = ElementType.Field.ToString().ToLower();
            if (elementType == ElementType.Marker)
            {
                type = ElementType.Marker.ToString().ToLower();
            }
            else if (elementType == ElementType.Table)
            {
                type = "column";
            }

            return string.Format(Properties.Resources.FieldMarkerRemoveWarning, type, name, Properties.Resources.LayoutLabel);
        }

        //TODO :Remove This code after removing treeview
        private void FieldTree_OnDataModelDeleteRequested(object sender, DataModelDeleteEventArgs e)
        {
            try
            {
                if (e.DataModelItemDeleteRequested is TableDef)
                {
                    var table = (TableDef)e.DataModelItemDeleteRequested;
                    if (
                        showDeleteConfirmation(
                            string.Format(Properties.Resources.DataModelTableRemoveWarning, table.Name, Properties.Resources.LayoutLabel)))
                    {
                        removeTableFromDataModel(table);
                    }
                }
                else if (e.DataModelItemDeleteRequested is FieldDef)
                {
                    var fieldDef = (FieldDef)e.DataModelItemDeleteRequested;
                    if (screenResources.DataModelVM.TableDef != null)
                    {
                        if (
                            showDeleteConfirmation(
                                string.Format(Properties.Resources.DataModelTableCollumnRemoveWarning, fieldDef.Name, Properties.Resources.LayoutLabel)))
                        {
                            removeTableFieldFromDataModel(screenResources.DataModelVM.TableDef, fieldDef);
                        }
                    }
                    else
                    {
                        if (
                            showDeleteConfirmation(
                                string.Format(Properties.Resources.DataModelFieldRemoveWarning, fieldDef.Name, Properties.Resources.LayoutLabel)))
                        {
                            removeFieldFromDataModel(fieldDef);
                        }
                    }
                }
                onSaveRequested(new SaveRequestEventArgs(_iqBot));
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(FieldTree_OnDataModelDeleteRequested));
            }
        }

        private void updateWarningVisibility()
        {
            IModificationSetter modificationSetter = new BlueprintModificationSetter(_testSetViewModel);
            modificationSetter.Setter();
            foreach (var layoutTestSetViewModel in _testSetViewModel.Items)
            {
                Layout layout = _layoutViewModel.Items.FirstOrDefault(d => d.Id.Equals(layoutTestSetViewModel.Id));
                UpdateTestSetWarningVisibility(layout, layoutTestSetViewModel);
            }
        }

        private void UpdateTestSetWarningVisibility(Layout layout, LayoutTestSetViewModel layoutTestSetViewModel)
        {
            if (layoutTestSetViewModel == null)
            {
                return;
            }
            foreach (var layoutTestSetItem in layoutTestSetViewModel.LayoutTestSetItems)
            {
                UpdateWarning(layout, layoutTestSetItem);
            }
        }

        private void UpdateWarning(Layout layout, LayoutTestSetItem layoutTestSetItem)
        {
            if (layout.Warning.HasWarning && layout.Warning.Message.Contains(Properties.Resources.MandatoryFieldNotMappedWarningMessage))
            {
                //Currently we are not giving Refernece column or footer missing warning as mandatory warning. So excluding that warning from test.
                assignLayoutWarning(layout, layoutTestSetItem);
            }
            else
            {
                verifyStructures(layoutTestSetItem);
            }
        }

        private void assignLayoutWarning(Layout layout, LayoutTestSetItem layoutTestSetItem)
        {
            IWarning warning = new GenericWarning(true, Properties.Resources.MandatoryFieldNotMappedWarningMessage, WarningType.Layout);
            var docSetItem = layoutTestSetItem.GetDocSetItem();
            layoutTestSetItem.Warning = warning;
            docSetItem.OriginalIsBenchmarchStructureFound = docSetItem.IsBenchmarkStructureMismatchFound;
        }

        private void verifyStructures(LayoutTestSetItem layoutTestSetItem)
        {
            var docSetItem = layoutTestSetItem.GetDocSetItem();
            IStructureVerifier structureVerifier = new StructureVerifier();
            layoutTestSetItem.Warning = structureVerifier.Verify(_iqBot, docSetItem.PreviewFields, docSetItem.PreviewTableSet);

            docSetItem.OriginalIsBenchmarchStructureFound = docSetItem.IsBenchmarkStructureMismatchFound;
            if (layoutTestSetItem.IsSelected)
            {
                IWarning selectedTestsetwarning = structureVerifier.Verify(_iqBot, docSetItem.Fields, docSetItem.TableSet);
                docSetItem.OriginalIsBenchmarchStructureFound = selectedTestsetwarning.HasWarning;
            }
        }

        //TODO :Remove This code after removing treeview
        private void FieldTree_OnTableDefAddRequested(object sender, EventArgs e)
        {
            try
            {
                screenResources.RequestAddTable();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(FieldTree_OnTableDefAddRequested));
            }
        }

        private void BluePrintTables_CreateTableHandler(object sender, EventArgs e)
        {
            try
            {
                screenResources.RequestAddTable();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(BluePrintTables_CreateTableHandler));
            }
        }

        //TODO :Remove This code after removing treeview
        private void removeFieldFromDataModel(FieldDef fieldDef)
        {
            if (_iqBot.DataModel.Fields.Contains(fieldDef))
            {
                _iqBot.DataModel.Fields.Remove(fieldDef);
                removeFieldFromAllAlyout(fieldDef);
                return;
            }
        }

        //TODO :Remove This code after removing treeview
        private void removeTableFieldFromDataModel(TableDef table, FieldDef fieldDef)
        {
            //foreach (var table in _iqBot.DataModel.Tables)
            //{
            if (table.Columns.Contains(fieldDef))
            {
                table.Columns.Remove(fieldDef);
                removeTableFieldFromAllLayout(fieldDef);
                return;
            }
            //}
        }

        //TODO :Remove This code after removing treeview
        private void removeTableFromDataModel(TableDef table)
        {
            if (_iqBot.DataModel.Tables.Contains(table))
            {
                _iqBot.DataModel.Tables.Remove(table);
                removeTableFromAllAlyout(table);
                return;
            }
        }

        private void removeFieldFromAllAlyout(FieldDef fieldDef)
        {
            foreach (var layout in _iqBot.Layouts)
            {
                //LayoutField existingField = (from q in layout.Fields where q.Name.Equals(fieldDef.Name, StringComparison.InvariantCultureIgnoreCase) select q).FirstOrDefault();
                LayoutField existingField = (from q in layout.LayoutFields.Fields where q.FieldDef.Id.Equals(fieldDef.Id, StringComparison.InvariantCultureIgnoreCase) select q).FirstOrDefault();
                if (existingField != null)
                {
                    layout.LayoutFields.Fields.Remove(existingField);
                    removeLayoutFieldFromImageView(layout, existingField);
                }
            }
        }

        private void removeTableFieldFromAllLayout(FieldDef fieldDef)
        {
            foreach (var layout in _iqBot.Layouts)
            {
                removeTableFieldFromSpecificLayout(layout, fieldDef);
            }
        }

        private void removeTableFieldFromSpecificLayout(Layout layout, FieldDef fieldDef)
        {
            for (int i = 0; i < layout.LayoutTables.Tables.Count; i++)
            {
                var table = layout.LayoutTables.Tables[i];
                LayoutField existingField = (from q in table.Fields where q.FieldDef.Id.Equals(fieldDef.Id, StringComparison.InvariantCultureIgnoreCase) select q).FirstOrDefault();
                if (existingField != null)
                {
                    table.Fields.Remove(existingField);
                    removeLayoutFieldFromImageView(layout, existingField);

                    if (existingField.Equals(table.PrimaryField))
                    {
                        table.PrimaryField = null;
                    }
                    if (table.Fields.Count == 0)
                    {
                        layout.LayoutTables.Tables.Remove(table);
                        i--;
                    }
                }
            }
        }

        private void removeTableFromAllAlyout(TableDef table)
        {
            foreach (var layout in _iqBot.Layouts)
            {
                LayoutTable existingTable = (from q in layout.LayoutTables.Tables where q.TableDef.Id.Equals(table.Id, StringComparison.InvariantCultureIgnoreCase) select q).FirstOrDefault();
                if (existingTable != null)
                {
                    if (layout == screenResources.SelectedLayout && existingTable.Fields != null)
                    {
                        for (int i = 0; i < existingTable.Fields.Count; i++)
                        {
                            removeLayoutFieldFromImageView(layout, existingTable.Fields[i]);
                        }
                    }
                    layout.LayoutTables.Tables.Remove(existingTable);
                }
            }
        }

        private void removeLayoutFieldFromImageView(Layout layout, LayoutField existingField)
        {
            if (screenResources.SelectedLayout == layout)
            {
                screenResources.RemoveFieldFromImageView(existingField);
            }
        }

        private bool showDeleteConfirmation(string message)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                DataContext = new ErrorModel
                {
                    ErrorMessage = message,
                    Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                    {
                        Caption = "Cancel",
                        IsCancel = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                    },
                        new ButtonModel
                    {
                        Caption = "Delete Anyway",
                        IsDefault = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 247, 58, 58))
                    }
                    }
                },
            };
            errorMessageDialog.Owner = Window.GetWindow(this);
            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        private bool showRestrictActionMessage(string message)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                DataContext = new ErrorModel
                {
                    ErrorMessage = message,
                    Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                    {
                        Caption = "Okay",
                        IsDefault = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 247, 58, 58))
                    }
                    }
                },
            };
            errorMessageDialog.Owner = Window.GetWindow(this);
            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        private bool showLayoutRescanConfirmationMessage(string message)
        {
            var warningMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                DataContext = new ErrorModel
                {
                    ErrorMessage = message,
                    Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                    {
                        Caption = "Yes",
                        IsDefault = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 247, 58, 58))
                    },
                        new ButtonModel
                    {
                        Caption = "No",
                        IsCancel = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                    }
                    }
                },
            };
            warningMessageDialog.Owner = Window.GetWindow(this);
            return Convert.ToBoolean(warningMessageDialog.ShowDialog());
        }

        private void removeFieldFromLayout(LayoutField layoutField)
        {
            foreach (var layout in _iqBot.Layouts)
            {
                if (layout.LayoutFields.Fields.Contains(layoutField))
                {
                    layout.LayoutFields.Fields.Remove(layoutField);
                    screenResources.RemoveFieldFromImageView(layoutField);
                    return;
                }

                if (layout.Markers.Fields.Contains(layoutField))
                {
                    layout.Markers.Fields.Remove(layoutField);
                    screenResources.RemoveFieldFromImageView(layoutField);
                    return;
                }

                foreach (var table in layout.LayoutTables.Tables)
                {
                    if (table.Fields.Contains(layoutField))
                    {
                        table.Fields.Remove(layoutField);
                        screenResources.RemoveFieldFromImageView(layoutField);
                        return;
                    }
                }
            }
        }

        private void removeTableFromLayout(LayoutTable layoutTable)
        {
            foreach (var layout in _iqBot.Layouts)
            {
                if (layout.LayoutTables.Tables.Contains(layoutTable))
                {
                    foreach (var field in layoutTable.Fields)
                    {
                        screenResources.RemoveFieldFromImageView(field);
                    }
                    layout.LayoutTables.Tables.Remove(layoutTable);
                    return;
                }
            }
        }

        private void ToolBar_OnPreviewCloseRequested(object sender, EventArgs e)
        {
            try
            {
                gridImageView.RowDefinitions[2].Height = new GridLength(0);
                imagePreviewSplitter.IsEnabled = false;
                ResetPreview();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(ToolBar_OnPreviewCloseRequested));
            }
        }

        private void VisionBotDesigner_OnMyCustom(object sender, RoutedEventArgs e)
        {
            try
            {
                gridImageView.RowDefinitions[2].Height = new GridLength(0);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), nameof(VisionBotDesigner_OnMyCustom));
            }
        }

        internal event BenchMarkPanelActionHandler BenchMarkPanelActionRequested;

        private void updateSelectedTestWarningStatus()
        {
            if (_testSetViewModel.SelectedTestSetItem != null)
            {
                Layout layout = _layoutViewModel.Items.FirstOrDefault(d => d.Id.Equals((_testSetViewModel.SelectedTestSetItem.Parent as LayoutTestSetViewModel).Id));
                UpdateWarning(layout, _testSetViewModel.SelectedTestSetItem);
            }
        }

        public DialogResult RefreshAllContent()
        {
            var messageModel = new ErrorModel
            {
                ErrorMessage = string.Format(Properties.Resources.RefreshAllContents, Environment.NewLine),
                Buttons = new ObservableCollection<ButtonModel>
                            {
                                new ButtonModel
                                    {
                                        Caption = "Yes",
                                        IsDefault = true,
                                        Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                                    },
                                new ButtonModel
                                    {
                                        Caption = "No",
                                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                                    },
                                new ButtonModel
                                    {
                                        Caption = "Cancel",
                                        IsCancel = true,
                                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                                    }
                            }
            };
            var errorMessageDialog = new ErrorMessageDialog
            {
                DataContext = messageModel,
                Owner = Window.GetWindow(this)
            };
            errorMessageDialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            errorMessageDialog.ShowDialog();
            if (messageModel.SelectedAction.Equals(messageModel.Buttons[0]))
            {
                return DialogResult.Yes;
            }
            if (messageModel.SelectedAction.Equals(messageModel.Buttons[1]))
            {
                return DialogResult.No;
            }

            return DialogResult.Cancel;
        }

        internal event UpdateTestSetWarningHandler UpdateTestSetRequested;

        private void ToolBar_OnPreviewRequested(object sender, EventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                if (PreviewRequested != null)
                {
                    PreviewEventArgs previewArgs = new PreviewEventArgs(screenResources.SelectedLayout, _iqBot);

                    handleActionWithDocumentProcessingProgressBar(new Action(() =>
                    {
                        PreviewRequested(sender, previewArgs);

                    }));

                    gridImageView.RowDefinitions[2].Height = new GridLength(gridImageView.ActualHeight / 2);
                    imagePreviewSplitter.IsEnabled = true;

                    if (previewArgs.IsLayoutClassifiedSuccessFully)
                    {
                        PreviewViewModel PreviewViewModel = new PreviewViewModel();
                        PreviewViewModel.PreviewFields = getValidatedFields(previewArgs.BenchmarkData.Fields);
                        IList<PreviewTableModel> previewTables = new List<PreviewTableModel>();
                        foreach (DataTable table in previewArgs.BenchmarkData.TableSet.Tables)
                        {
                            getValidatedTables(previewTables, table);
                        }
                        PreviewViewModel.PreviewTables = new ObservableCollection<PreviewTableModel>(previewTables);
                        screenResources.PreviewDataView.PreviewContent = PreviewViewModel;
                    }
                    else
                    {
                        screenResources.PreviewDataView.PreviewContent = Properties.Resources.InformationMessage_LayoutNotClassified;
                    }

                    screenResources.PreviewDataView.IsLayoutClassified = previewArgs.IsLayoutClassifiedSuccessFully;
                    screenResources.RightPanelVM.IsPreviewOn = true;
                }
            }));
        }

        private void TestRefresh_OnClick(object sender, EventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                if (_testSetViewModel.SelectedTestSets.Count > 0)
                {
                    DialogResult result = RefreshAllContent();
                    if (result == DialogResult.Cancel) return;

                    DocSetItem docSetItem = _testSetViewModel.SelectedTestSets[0].LayoutTestSetItems[0].GetDocSetItem();
                    docSetItem.IsDataExtractedSuccessfully = true;
                    PreviewViewModel previewVM = (gridTestImageView.DataContext as TestResultViewModel).LowerContent as PreviewViewModel;
                    TestDocSetEventArgs testDocSetEventArgs = new TestDocSetEventArgs(
                            _testSetViewModel.SelectedTestSets[0].LayoutTestSet.LayoutId,
                            docSetItem,
                            Operation.Refresh, previewVM);

                    testDocSetEventArgs.IqBot = _iqBot;
                    testDocSetEventArgs.IsKeepCurrentContents = result == DialogResult.Yes;



                    handleActionWithDocumentProcessingProgressBar(new Action(() =>
                    {
                        BenchMarkPanelActionRequested(this, testDocSetEventArgs);
                    }));

                    PreviewViewModel previewWithModifiedData = getPreviewData(docSetItem, false);
                    previewVM.PreviewFields = previewWithModifiedData.PreviewFields;
                    previewVM.PreviewTables = previewWithModifiedData.PreviewTables;

                    docSetItem.IsDataExtractedSuccessfully = previewVM.IsDataExtractedSuccessfully;
                    updateSelectedTestWarningStatus();

                    if (this.screenResources.TestViewModels.ContainsKey(this.screenResources.SelectedTestItem))
                    {
                        TestItemViewModel testItemViewModel = this.screenResources.TestViewModels[this.screenResources.SelectedTestItem];

                        testItemViewModel.State.IsScanTheDocument = true;
                    }
                }
            }));
        }

        private void onUpdateTestSetReqeusted(UpdateTestSetWarningEventArgs e)
        {
            handleExceptionWithMessage(new Action(() =>
            {
                handleActionWithDocumentProcessingProgressBar(new Action(() =>
            {
                UpdateTestSetRequested?.Invoke(this, e);
            }));
            }));
        }

        private void ExportPreview_OnClick(object sender, RoutedEventArgs e)
        {
            if (ExportPreviewDataRequested != null)
            {
                PreviewEventArgs previewEventArgs = new PreviewEventArgs(screenResources.SelectedLayout, _iqBot);
                ExportPreviewDataRequested(sender, previewEventArgs);
            }
        }

        private void RibbonTab_SettingsClickedHandler(object sender, EventArgs e)
        {
            ShowAndSaveIQBotSettings();
        }

        private void ShowAndSaveIQBotSettings()
        {
            ImageProcessingConfig settings = _iqBot.Settings.Clone();
            IQBotSettingsViewModel settingsViewModel = new IQBotSettingsViewModel(settings);
            IQBotSettings settingsView = new IQBotSettings(settingsViewModel);
            settingsView.SizeToContent = SizeToContent.WidthAndHeight;
            settingsView.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settingsView.DataContext = settingsViewModel;
            settingsView.Owner = Window.GetWindow(this);
            bool? result = settingsView.ShowDialog();
            if (result.HasValue && result.Value)
            {
                updateIQBotSetting(settings);
                saveVisionBot(false);
            }
        }

        private void TestExportPreview_OnClick(object sender, RoutedEventArgs e)
        {
            DocSetItem docSetItem = _testSetViewModel.SelectedTestSets[0].LayoutTestSetItems[0].GetDocSetItem();

            PreviewViewModel previewVM = (gridTestImageView.DataContext as TestResultViewModel).LowerContent as PreviewViewModel;
            TestDocSetEventArgs testDocSetEventArgs = new TestDocSetEventArgs(
                    _testSetViewModel.SelectedTestSets[0].LayoutTestSet.LayoutId,
                    docSetItem,
                    Operation.Export, previewVM);

            BenchMarkPanelActionRequested(sender, testDocSetEventArgs);
        }

        private void ctrlImage_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange != 0 || e.ExtentHeightChange != 0)
            {
                SetCurrentPage();

                CommandManager.InvalidateRequerySuggested();
            }
        }

        private void SetCurrentPage()
        {
            ImageViewInfo inf = (imageView.DataContext as CanvasImageViewModel)?.ImageViewInfo;

            if (inf?.ViewRect != null)
            {
                double zoom = inf.Parent.ZoomPercentage;
                inf.ViewRect = new Rect(ctrlImage.HorizontalOffset / zoom
                    , ctrlImage.VerticalOffset / zoom
                    , (ctrlImage.ActualWidth / zoom)
                    , (ctrlImage.ActualHeight / zoom));
            }

            inf?.SetCurrentPage(ctrlImage.VerticalOffset + (ctrlImage.ActualHeight / 2));
        }

        private void ctrlTestImage_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange != 0 || e.ExtentHeightChange != 0)
            {
                SetCurrentPageForTest(sender as ScrollViewer);

                CommandManager.InvalidateRequerySuggested();
            }
        }

        private void SetCurrentPageForTest(ScrollViewer viewer)
        {
            var imageView = (viewer.FindName("testImageView") as ImageView);
            if (imageView != null)
            {
                ImageViewInfo inf = (imageView.DataContext as CanvasImageViewModel)?.ImageViewInfo;
                DocSetItem temp = _testSetViewModel?.SelectedTestSets[0]?.LayoutTestSetItems[0]?.GetDocSetItem();
                if (temp != null)
                {
                    if (inf?.ViewRect != null)
                    {
                        double zoom = inf.Parent.ZoomPercentage;
                        inf.ViewRect = new Rect(viewer.HorizontalOffset / zoom
                            , viewer.VerticalOffset / zoom
                            , (viewer.ActualWidth / zoom)
                            , (viewer.ActualHeight / zoom));
                    }
                    inf?.SetCurrentPage(viewer.VerticalOffset + (viewer.ActualHeight / 2));
                }
            }
        }

        private void ctrlTestImage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(() => SetCurrentPageForTest(sender as ScrollViewer)), DispatcherPriority.Background);
        }

        private void ctrlTestImage_Loaded(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(() => SetCurrentPageForTest(sender as ScrollViewer)), DispatcherPriority.Background);
        }

        private void handleExceptionWithMessage(Action action, [CallerMemberName]string memberName = "")
        {
            try
            {
                action.Invoke();
            }
            catch (WebServiceException ex)
            {
                ex.Log(nameof(VisionBotDesigner), memberName, ex.ToString());

                if (ex.Error.Code == "404")
                {
                    showErrorMessage(Properties.Resources.ProjectDeletedMessage);
                    this.Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action(() =>
                        {
                            Window.GetWindow(this).Close();
                        }));
                    return;
                }

                showErrorMessage(ex.Message);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesigner), memberName, ex.ToString());
                showErrorMessage(ex.Message);
            }

        }

        public void showErrorMessage(string errorMessage)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal,
                 new Action(() =>
                 {
                     var errorMessageDialog = new ErrorMessageDialog
                     {
                         WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen,
                         DataContext = new ErrorModel
                         {
                             ErrorMessage = errorMessage,
                             Buttons = new ObservableCollection<ButtonModel>
                            {
                                new ButtonModel
                                {
                                    Caption = "OK",
                                    IsDefault = true,
                                    Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                                }
                            },
                         }
                     };

                     errorMessageDialog.ShowDialog();
                 }));
        }

        public void handleActionWithDocumentProcessingProgressBar(Action action, [CallerMemberName]string memberName = "")
        {
            handleActionWithProgressBar(action, "Processing... Please wait...", memberName);
        }

        public void handleActionWithProgressBar(Action action, string progressWindowMessage, [CallerMemberName]string memberName = "")
        {
            ProgressWindow window = new ProgressWindow(progressWindowMessage)
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                Owner = Window.GetWindow(this)
            };
            Exception workerException = null;
            var worker = new Thread(() =>
            {

                try
                {
                    action.Invoke();
                }
                catch (Exception ex)
                {
                    workerException = ex;
                }
                finally
                {
                    var formClose = new ProgressWindow.FromClose(window.Close);
                    if (window.Dispatcher.CheckAccess())
                    {
                        window.Close();
                    }
                    else
                    {
                        window.Dispatcher.Invoke(DispatcherPriority.Normal, formClose);
                    }
                }
            });

            worker.Start();
            window.ShowDialog();
            if (workerException != null)
            {
                throw workerException;
            }
        }
    }

    internal delegate void BenchMarkPanelActionHandler(object sender, TestDocSetEventArgs e);

    internal delegate void LayoutRescanEventHandler(object sender, PreviewEventArgs e);

    internal delegate void ExecuteTestHandler(object sender, TestExecutionEventArgs e);

    internal delegate void ExportPreviewDataEventHandler(object sender, PreviewEventArgs e);

    internal delegate void DocumentsDownloadRequestHandler(object sender, EventArgs e);

    internal delegate void DocumentSelectionRequestHandler(object sender, DocumentsSelectionEventArgs<DocumentProperties> e);

    internal delegate void DocumentPageImageRequestHandler(object sender, DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings);

    internal class TestExecutionEventArgs : EventArgs
    {
        public IqBot IqBot { get; private set; }
        public IList<LayoutTestSetViewModel> LayoutTestSetViewModels { get; private set; }
        public IList<LayoutTestResult> LayoutTestResults { get; set; }

        public TestExecutionEventArgs(IqBot iqBot, IList<LayoutTestSetViewModel> layoutTestSetViewModels)
        {
            IqBot = iqBot;
            LayoutTestSetViewModels = layoutTestSetViewModels;
        }
    }

    internal delegate void LayoutDeletedHandler(object sender, LayoutDeletedEventArgs e);

    internal class LayoutDeletedEventArgs : EventArgs
    {
        public IqBot IqBot { get; private set; }
        public Layout Layout { get; private set; }

        public LayoutDeletedEventArgs(IqBot iqBot, Layout layout)
        {
            IqBot = IqBot;
            Layout = layout;
        }
    }

    public delegate void FieldValueRequestHandler(object sender, FieldValueRequestEventArgs e);

    public class FieldValueRequestEventArgs : EventArgs
    {
        public IqBot IqBot { get; private set; }
        public Layout Layout { get; private set; }
        public LayoutField LayoutField { get; private set; }
        public VisionBotEngine.Model.Field ValueField { get; set; }

        public FieldValueRequestEventArgs(IqBot iqBot, Layout layout, LayoutField layoutField)
        {
            IqBot = iqBot;
            Layout = layout;
            LayoutField = layoutField;
        }
    }

    public delegate void UpdateTestSetWarningHandler(object sender, UpdateTestSetWarningEventArgs e);

    public class UpdateTestSetWarningEventArgs : EventArgs
    {
        public IList<Tuple<string, string, bool>> BenchmarkStructureStatus { get; set; }

        public UpdateTestSetWarningEventArgs(IList<Tuple<string, string, bool>> benchmarkStatus)
        {
            BenchmarkStructureStatus = benchmarkStatus;
        }
    }
}