﻿using Automation.CognitiveData.VisionBot.UI.Model;
using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for ProjectsInformation.xaml
    /// </summary>
    public partial class ProjectsInformation : Window
    {
        public ProjectsInformation()
        {
            InitializeComponent();
            ProjectsInformationViewModel projectsInformationViewModel = new ProjectsInformationViewModel();
            DataContext = projectsInformationViewModel;
        }

        public ProjectsInformation(ProjectsInformationViewModel projectsInformationViewModel)
        {
            InitializeComponent();
            DataContext = projectsInformationViewModel;
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
           
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (DataContext is ProjectsInformationViewModel)
            //{
            //    string startupFilePath = "Designer.ini";
            //    if (File.Exists(startupFilePath))
            //    {
            //        string jsonData = File.ReadAllText(startupFilePath);

            //        var inputData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonData);

            //        var projectViewModel = DataContext as ProjectsInformationViewModel;
            //        projectViewModel.OpenVisionBot(inputData["projectId"], inputData["categoryId"]);
            //    }

            //}
        }

        //private void Item_DoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    ClassifiedFolder folderDetails = dataGrid.SelectedItem as ClassifiedFolder;
        //    VisionBotUIManager manager = new VisionBotUIManager(folderDetails.Folder);
        //    manager.ShowModalDialog();
        //}

        //private void Btn_TrainClick(object sender, RoutedEventArgs e)
        //{
        //    ClassifiedFolder folderDetails = dataGrid.SelectedItem as ClassifiedFolder;
        //    if (folderDetails != null)
        //    {
        //        VisionBotUIManager manager = new VisionBotUIManager(folderDetails.Folder);
        //        manager.ShowModalDialog();
        //    }
        //}
    }
}