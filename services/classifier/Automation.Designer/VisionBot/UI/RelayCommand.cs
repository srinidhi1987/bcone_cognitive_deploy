﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class RelayCommand : ICommand 
    { 
        #region Fields 
        readonly Action<object> _execute; 
        readonly Predicate<object> _canExecute; 
        #endregion // Fields 
        
        #region Constructors 
        public RelayCommand(Action<object> execute) : this(execute, null) 
        { } 
        
        public RelayCommand(Action<object> execute, Predicate<object> canExecute) 
        { 
            if (execute == null) 
                throw new ArgumentNullException("execute"); 
            _execute = execute; 
            _canExecute = canExecute; 
        } 
        
        #endregion // Constructors 
        
        #region ICommand Members 
        
        [DebuggerStepThrough] 
        public bool CanExecute(object parameter) 
        { 
            return _canExecute == null ? true : _canExecute(parameter); 
        } 
        
        public event EventHandler CanExecuteChanged 
        { 
            add { CommandManager.RequerySuggested += value; } 
            remove { CommandManager.RequerySuggested -= value; } 
        } 
        
        public void Execute(object parameter) 
        { _execute(parameter); } 
        
        #endregion // ICommand Members 
    }   
}
