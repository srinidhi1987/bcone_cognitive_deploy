﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
namespace Automation.CognitiveData.VisionBot.UI
{
    class FieldDefConverter : IFieldDefConverter
    {
        public FieldDef ConvertFrom(VisionBotEngine.Model.FieldDef fieldDef)
        {
            return new FieldDef
            {
                Id = fieldDef.Id,
                Name = fieldDef.Name,
                ValueType = fieldDef.ValueType,
                Formula = fieldDef.Formula,
                ValidationID = fieldDef.ValidationID,
                IsRequired = fieldDef.IsRequired,
                DefaultValue=fieldDef.DefaultValue,
                StartsWith = fieldDef.StartsWith,
                EndsWith = fieldDef.EndsWith,
                FormatExpression = fieldDef.FormatExpression
            };
        }

        public VisionBotEngine.Model.FieldDef ConvertTo(FieldDef fieldDef)
        {
            return new VisionBotEngine.Model.FieldDef
            {
                Id = fieldDef.Id,
                Name = fieldDef.Name,
                ValueType = fieldDef.ValueType,
                Formula = fieldDef.Formula,
                ValidationID = fieldDef.ValidationID,
                IsRequired = fieldDef.IsRequired,
                DefaultValue = fieldDef.DefaultValue,
                StartsWith = fieldDef.StartsWith,
                EndsWith = fieldDef.EndsWith,
                FormatExpression = fieldDef.FormatExpression
            };
        }
    }
}
