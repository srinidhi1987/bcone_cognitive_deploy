/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Drawing;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI
{
    public static class RectangleExtensions
    {
        public static Rect ConvertToRect(this Rectangle source)
        {
            return new Rect(source.X, source.Y, source.Width, source.Height);
        }

        public static Rectangle ConvertToRectangle(this Rect source)
        {
            return new Rectangle
            {
                X = Convert.ToInt32(source.X),
                Y = Convert.ToInt32(source.Y),
                Width = Convert.ToInt32(source.Width),
                Height = Convert.ToInt32(source.Height)
            };
        }

        public static Rect Subtract(this Rect bounds, Rect boundsToSubtract)
        {
            return new Rect
            {
                X = bounds.X - boundsToSubtract.X,
                Y = bounds.Y - boundsToSubtract.Y,
                Width = bounds.Width,
                Height = bounds.Height
            };
        }

        public static Rect Add(this Rect bounds, Rect boundsToAdd)
        {
            return new Rect
            {
                X = bounds.X + boundsToAdd.X,
                Y = bounds.Y + boundsToAdd.Y,
                Width = !(boundsToAdd.Width.Equals(0)) ? boundsToAdd.Width : bounds.Width,
                Height = !(boundsToAdd.Height.Equals(0)) ? boundsToAdd.Height : bounds.Height
            };
        }

        public static Rectangle Add(this Rectangle bounds, Rectangle boundsToAdd)
        {
            return new Rectangle
            {
                X = bounds.X + boundsToAdd.X,
                Y = bounds.Y + boundsToAdd.Y,
                Width = boundsToAdd.Width != 0? boundsToAdd.Width : bounds.Width,
                Height = boundsToAdd.Height != 0 ? boundsToAdd.Height : bounds.Height
            };
        }

        public static Rect AddOffset(this Rect bounds, double xOffset, double yOffset, double widthOffset, double heightOffset)
        {
            return new Rect
            {
                X = bounds.X + xOffset,
                Y = bounds.Y + yOffset,
                Width = bounds.Width + widthOffset,
                Height = bounds.Height + heightOffset
            };
        }
    }
}