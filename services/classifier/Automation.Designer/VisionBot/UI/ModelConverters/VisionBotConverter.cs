using Automation.CognitiveData.Properties;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;

/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace Automation.CognitiveData.VisionBot.UI
{
    internal class VisionBotConverter : IVisionBotConverter
    {
        public IqBot ConvertFrom(VisionBotEngine.Model.VisionBot visionBot, VBotDocSet vBotDocSet)
        {
            var iqBot = new IqBot
            {
                Name = visionBot.Name,
                Id = visionBot.Id,
                Version = visionBot.Version
            };
            iqBot.Settings = visionBot.Settings;
            copyDataModelIdStructureFromVisionBot(visionBot, iqBot);
            copyDataModelFromVisionBot(visionBot, iqBot);
            copyLayoutInformationFromVisionBot(visionBot, iqBot, vBotDocSet);
            VBotLogger.Trace(() => "[ConvertFrom] VisionBot Convert to IQBot successfully.");
            return iqBot;
        }

        private void copyLayoutInformationFromVisionBot(VisionBotEngine.Model.VisionBot visionBot, IqBot iqBot, VBotDocSet vBotDocSet)
        {
            string documentName = string.Empty;
            ILayoutConverter layoutConverter = new LayoutConverter();
            IList<Layout> layouts = new List<Layout>();
            foreach (var layout in visionBot.Layouts)
            {
                string imagePath = string.Empty;
                foreach (var docItem in vBotDocSet.DocItems)
                {
                    if (docItem.Id.Equals(layout.VBotDocSetId))
                    {
                        documentName = docItem.DocumentName;
                        imagePath = Path.Combine(Path.GetTempPath(), Resources.VisionBotTempDirectory,
                            docItem.DocumentName,
                            docItem.Id);
                        break;
                    }
                }

                layouts.Add(layoutConverter.ConvertFrom(layout, imagePath, visionBot, iqBot.DataModel, documentName));
            }
            iqBot.Layouts = new ObservableCollection<Layout>(layouts.OrderBy(l => l.Name));
        }

        private void copyDataModelIdStructureFromVisionBot(VisionBotEngine.Model.VisionBot visionBot, IqBot iqBot)
        {
            if (visionBot.DataModelIdStructure != null)
            {
                iqBot.DataModelIdStructure.FieldId = visionBot.DataModelIdStructure.FieldId;
                iqBot.DataModelIdStructure.TableDefId = visionBot.DataModelIdStructure.TableDefId;
                iqBot.DataModelIdStructure.ColumnId = visionBot.DataModelIdStructure.ColumnId;
            }
        }

        private void copyDataModelFromVisionBot(VisionBotEngine.Model.VisionBot visionBot, IqBot iqBot)
        {
            IDataModelConverter dataModelConverter = new DataModelConverter();
            iqBot.DataModel = dataModelConverter.ConvertFrom(visionBot.DataModel);
        }

        private void copyDocSetFromVisionBot(VisionBotEngine.Model.VisionBot visionBot, IqBot iqBot)
        {
            IDataModelConverter dataModelConverter = new DataModelConverter();
            iqBot.DataModel = dataModelConverter.ConvertFrom(visionBot.DataModel);
        }

        public VisionBotEngine.Model.VisionBot ConvertTo(IqBot iqBot, VisionBotEngine.Model.VisionBot savedVisionBot)
        {
            var visionBot = new VisionBotEngine.Model.VisionBot();
            visionBot.Id = iqBot.Id;
            visionBot.Name = iqBot.Name;
            visionBot.Version = iqBot.Version;
            visionBot.Settings = iqBot.Settings;
            visionBot.Language = savedVisionBot.Language;
            copyDataModelIdStructureToVisionBot(iqBot, visionBot);
            copyLayoutInformationToVisionBot(iqBot, visionBot);
            copyDataModelToVisionBot(iqBot, visionBot, savedVisionBot);
            return visionBot;
        }

        private void copyLayoutInformationToVisionBot(IqBot iqBot, VisionBotEngine.Model.VisionBot visionBot)
        {
            ILayoutConverter layoutConverter = new LayoutConverter();
            foreach (var layout in iqBot.Layouts)
            {
                visionBot.Layouts.Add(layoutConverter.ConvertTo(layout));
            }
        }

        private void copyDataModelIdStructureToVisionBot(IqBot iqBot, VisionBotEngine.Model.VisionBot visionBot)
        {
            if (visionBot.DataModelIdStructure == null)
                visionBot.DataModelIdStructure = new BlueprintIdSnap();

            if (iqBot.DataModelIdStructure != null)
            {
                visionBot.DataModelIdStructure.FieldId = iqBot.DataModelIdStructure.FieldId;
                visionBot.DataModelIdStructure.TableDefId = iqBot.DataModelIdStructure.TableDefId;
                visionBot.DataModelIdStructure.ColumnId = iqBot.DataModelIdStructure.ColumnId;
            }
        }

        private void copyDataModelToVisionBot(IqBot iqBot, VisionBotEngine.Model.VisionBot visionBot, VisionBotEngine.Model.VisionBot savedVisionBot)
        {
            IDataModelConverter dataModelConverter = new DataModelConverter();
            visionBot.DataModel = dataModelConverter.ConvertTo(iqBot.DataModel, savedVisionBot.DataModel);
        }
    }
}