﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.IO;
    using System.Linq;

    internal class LayoutConverter : ILayoutConverter
    {
        public Layout ConvertFrom(VisionBotEngine.Model.Layout layout, string documentPath, VisionBotEngine.Model.VisionBot visionBot, DataModel bluePrint, string documentName)
        {
            Layout uiLayout = new Layout(
                                    new ViewModels.LayoutFieldsViewModel("Fields", convertFromFields(layout.Fields, bluePrint)),
                                    new ViewModels.LayoutTablesViewModel("Tables", convertFromTables(layout.Tables, visionBot, bluePrint)),
                                    new ViewModels.LayoutFieldsViewModel("Markers", convertFromIdentifiers(layout.Identifiers))
                                        )
            {
                Id = layout.Id,
                DocumentPath = documentPath,
                Name = layout.Name,
                TestDocSetId = layout.VBotTestDocSetId,
                DocumentProperties = layout.DocProperties,
                DocumentName = documentName,
                Settings = layout.Settings
            };

            ConvertSIRFields(layout, uiLayout);

            return uiLayout;
        }

        public DocumentProperties setLayoutDocProperty(VisionBotEngine.Model.Layout layout)
        {
            DocumentProperties layoutDocProperties = new DocumentProperties();
            if (layout.DocProperties != null)
            {
                layoutDocProperties.Width = layout.DocProperties.Width;
                layoutDocProperties.Height = layout.DocProperties.Height;
            }
            return layoutDocProperties;
        }

        public ObservableCollection<LayoutField> ConvertSIRFields(VisionBotEngine.Model.SIRFields sirFieldsToConvert)
        {
            ObservableCollection<LayoutField> sirFieldsToReturn = new ObservableCollection<LayoutField>();
            if (sirFieldsToConvert != null)
            {
                if (sirFieldsToConvert.Regions != null)
                {
                    ObservableCollection<LayoutField> sirRegions = convertFromSirFieldsWithType(
                         sirFieldsToConvert.Regions,
                         ElementType.SystemIdentifiedRegion);
                    for (int i = 0; i < sirRegions.Count; i++)
                    {
                        sirFieldsToReturn.Add(sirRegions[i]);
                    }
                }

                if (sirFieldsToConvert.Fields != null)
                {
                    ObservableCollection<LayoutField> sirFields = convertFromSirFieldsWithType(
                        sirFieldsToConvert.Fields,
                        ElementType.SystemIdentifiedField);
                    for (int i = 0; i < sirFields.Count; i++)
                    {
                        sirFieldsToReturn.Add(sirFields[i]);
                    }
                }
            }

            return sirFieldsToReturn;
        }

        public void ConvertSIRFields(VisionBotEngine.Model.Layout layout, Layout uiLayout)
        {
            VBotLogger.Trace(() => "[ConvertSIRFields] Convert SIR Fields...");

            uiLayout.SystemIdentifiedFields = new ObservableCollection<LayoutField>();
            uiLayout.Lines = new ObservableCollection<Line>();

            ObservableCollection<LayoutField> sirRegions = convertFromSirFieldsWithType(
                 layout.SirFields.Regions,
                 ElementType.SystemIdentifiedRegion);
            for (int i = 0; i < sirRegions.Count; i++)
            {
                uiLayout.SystemIdentifiedFields.Add(sirRegions[i]);
            }

            ObservableCollection<LayoutField> sirFields = convertFromSirFieldsWithType(
                layout.SirFields.Fields,
                ElementType.SystemIdentifiedField);
            for (int i = 0; i < sirFields.Count; i++)
            {
                uiLayout.SystemIdentifiedFields.Add(sirFields[i]);
            }

            if (layout.SirFields != null)
            {
                uiLayout.Lines = convertFromLines(layout.SirFields.Lines);
            }
        }

        //private ObservableCollection<LayoutField> convertFromFields(List<VisionBotEngine.Model.FieldLayout> fields, VisionBotEngine.Model.VisionBot visionBot)
        private ObservableCollection<LayoutField> convertFromFields(List<VisionBotEngine.Model.FieldLayout> fields, DataModel dataModel)
        {
            var bulkFieldConversionHelper = new BulkFieldConversionHelper();
            return bulkFieldConversionHelper.ConvertFromFieldsWithType(fields, ElementType.Field, dataModel.Fields.ToList());
        }

        private ObservableCollection<LayoutTable> convertFromTables(List<VisionBotEngine.Model.TableLayout> tables, VisionBotEngine.Model.VisionBot visionBot, DataModel bluePrint)
        {
            var layoutTables = new ObservableCollection<LayoutTable>();
            ILayoutTableConverter layoutTableConverter = new LayoutTableConverter();
            foreach (var table in tables)
            {
                TableDef tableDef = null;
                if (visionBot.DataModel.Tables != null)
                {
                    tableDef = (from q in bluePrint.Tables where q.Id == table.TableId select q).FirstOrDefault();
                    //TODO: Remove following if once all old visionbots start working
                    if (tableDef == null)
                    {
                        tableDef = bluePrint.Tables[0];
                    }
                }
                layoutTables.Add(layoutTableConverter.ConvertFrom(table, tableDef));
            }
            return layoutTables;
        }

        private ObservableCollection<LayoutField> convertFromIdentifiers(List<VisionBotEngine.Model.MarkerLayout> identifiers)
        {
            var bulkFieldConversionHelper = new BulkFieldConversionHelper();
            return bulkFieldConversionHelper.ConvertFromFieldsWithType(identifiers);
        }

        private ObservableCollection<LayoutField> convertFromSirFieldsWithType(List<Field> fields, ElementType elementType)
        {
            VBotLogger.Trace(() => "[convertFromSirFieldsWithType] Started conversion of SIR fields with type...");
            var layoutFields = new ObservableCollection<LayoutField>();
            foreach (var field in fields)
            {
                layoutFields.Add(new LayoutField
                {
                    Bounds = field.Bounds.ConvertToRect(),
                    OriginalBounds = field.Bounds.ConvertToRect(),
                    Id = field.Id,
                    SystemIdentifiedFieldParentId = field.ParentId,
                    Confidence = field.Confidence,
                    //FieldID = field.Id,
                    Label = field.Text,
                    //Name = field.Id,
                    Type = elementType,
                    FieldDirection = VisionBotEngine.FieldDirection.Right,
                    MergeRatio = 1.0f,
                    SimilarityFactor = 0.7f,
                    UserDefinedRegionType = GetUDRRegionType(field.Type),
                    Angle = field.Angle
                });
            }
            return layoutFields;
        }

        private ObservableCollection<Line> convertFromLines(List<VisionBotEngine.Model.Line> lines)
        {
            VBotLogger.Trace(() => "[convertFromLines] Started conversion of SIR fields from lines...");
            ObservableCollection<Line> convertedLines = new ObservableCollection<Line>();
            if (lines != null)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    Line line = new Line
                    {
                        X1 = lines[i].X1,
                        X2 = lines[i].X2,
                        Y1 = lines[i].Y1,
                        Y2 = lines[i].Y2
                    };

                    convertedLines.Add(line);
                }
            }
            return convertedLines;
        }

        public VisionBotEngine.Model.Layout ConvertTo(Layout layout)
        {
            return new VisionBotEngine.Model.Layout
            {
                Id = layout.Id,
                Name = layout.Name,
                VBotTestDocSetId = layout.TestDocSetId,
                DocProperties = layout.DocumentProperties,
                Fields = convertToFields(layout.LayoutFields.Fields),
                Tables = convertToTables(layout.LayoutTables.Tables),
                Identifiers = convertToIdentifiers(layout.Markers.Fields),
                Settings = layout.Settings,
                SirFields = convertToSirFields(layout.SystemIdentifiedFields, layout.Lines),
                VBotDocSetId = Path.GetFileNameWithoutExtension(layout.DocumentPath)
            };
        }

        public DocumentProperties getLayoutDocProperty(Layout layout)
        {
            DocumentProperties layoutDocProperties;
            layoutDocProperties = layout.DocumentProperties;
            return layoutDocProperties;
        }

        private List<FieldLayout> convertToFields(ObservableCollection<LayoutField> fields)
        {
            var bulkFieldConversionHelper = new BulkFieldConversionHelper();
            return bulkFieldConversionHelper.ConvertFieldsTo(fields);
        }

        private List<TableLayout> convertToTables(ObservableCollection<LayoutTable> tables)
        {
            var layoutTables = new List<TableLayout>();
            ILayoutTableConverter layoutTableConverter = new LayoutTableConverter();
            foreach (var table in tables)
            {
                layoutTables.Add(layoutTableConverter.ConvertTo(table));
            }
            return layoutTables;
        }

        private List<MarkerLayout> convertToIdentifiers(ObservableCollection<LayoutField> markers)
        {
            var bulkFieldConversionHelper = new BulkFieldConversionHelper();
            return bulkFieldConversionHelper.ConvertFieldsToMarkers(markers);
        }

        private SIRFields convertToSirFields(ObservableCollection<LayoutField> fields, ObservableCollection<Line> lines)
        {
            SIRFields sirFields = new SIRFields();
            foreach (var field in fields)
            {
                Field sirItem = new Field();
                sirItem.Bounds = new Rectangle
                {
                    X = (int)(field.Bounds.X),
                    Y = (int)(field.Bounds.Y),
                    Width = (int)(field.Bounds.Width),
                    Height = (int)(field.Bounds.Height)
                };
                //TODO: consider Confidence in convert to and convert from
                //sirItem.Confidence =
                //sirItem.Id = field.FieldID;
                sirItem.Id = field.Id;
                sirItem.ParentId = field.SystemIdentifiedFieldParentId;
                sirItem.Confidence = field.Confidence;
                //TODO: consider ParentID in convert to and convert from
                //sirItem.ParentId
                sirItem.Text = field.Label;
                sirItem.Type = GetFieldType(field.UserDefinedRegionType);
                sirItem.Angle = field.Angle;
                //TODO : xonsider Type
                //sirItem.Type
                if (field.Type == ElementType.SystemIdentifiedField)
                {
                    sirFields.Fields.Add(sirItem);
                }
                else if (field.Type == ElementType.SystemIdentifiedRegion)
                {
                    sirItem.Type = FieldType.SystemRegion;
                    sirFields.Regions.Add(sirItem);
                }
                //TODO: What should we do for lines?
            }

            sirFields.Lines = convertToLines(lines);

            return sirFields;
        }

        private List<VisionBotEngine.Model.Line> convertToLines(ObservableCollection<Line> lines)
        {
            List<VisionBotEngine.Model.Line> convertedLines = new List<VisionBotEngine.Model.Line>();
            if (lines != null)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    VisionBotEngine.Model.Line line = new VisionBotEngine.Model.Line
                    {
                        X1 = lines[i].X1,
                        X2 = lines[i].X2,
                        Y1 = lines[i].Y1,
                        Y2 = lines[i].Y2
                    };

                    convertedLines.Add(line);
                }
            }

            return convertedLines;
        }

        public static UserDefinedRegionType GetUDRRegionType(FieldType fieldType)
        {
            UserDefinedRegionType udrRegionType = UserDefinedRegionType.None;
            switch (fieldType)
            {
                case FieldType.UserDefinedField:
                    udrRegionType = UserDefinedRegionType.UserDefinedField;
                    break;

                case FieldType.UserDefinedRegion:
                    udrRegionType = UserDefinedRegionType.UserDefinedRegion;
                    break;

                default:
                    udrRegionType = UserDefinedRegionType.None;
                    break;
            }
            return udrRegionType;
        }

        public static FieldType GetFieldType(UserDefinedRegionType udrRegionType)
        {
            FieldType fieldType = FieldType.SystemField;
            switch (udrRegionType)
            {
                case UserDefinedRegionType.UserDefinedField:
                    fieldType = FieldType.UserDefinedField;
                    break;

                case UserDefinedRegionType.UserDefinedRegion:
                    fieldType = FieldType.UserDefinedRegion;
                    break;

                default:
                    fieldType = FieldType.SystemField;
                    break;
            }
            return fieldType;
        }
    }
}