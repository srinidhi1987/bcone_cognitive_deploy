﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Controls;
    using System.Windows.Media;

    using Automation.VisionBotEngine.Model;
    /// <summary>
    /// Interaction logic for LayoutMatchWarningMessage.xaml
    /// </summary>
    public partial class DuplicateLayoutDialog : Window
    {
              
        public DuplicateLayoutDialog()
        {
            InitializeComponent();
        }
        public LayoutCreationOperation DuplicateLayoutOperation { get; set; }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {           
            this.DuplicateLayoutOperation = LayoutCreationOperation.Open;
            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            this.DuplicateLayoutOperation = LayoutCreationOperation.Edit;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DuplicateLayoutOperation = LayoutCreationOperation.Cancel;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Left = Left - 10;
        }
    }
}
