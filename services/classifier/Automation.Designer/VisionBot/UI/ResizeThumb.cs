﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class ResizeThumb : Thumb
    {
        private const string AncestorName = "MainCanvas";
        internal const int MinRegionWidth = 10;
        internal const int MinRegionHeight = 10;
        public event RegionBoxResizeHandler Committed;
        private Rect _resizedBounds;

        public ResizeThumb()
        {
            DragDelta += ResizeThumb_DragDelta;
        }

        void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            RegionBox item = this.FindAncestor<RegionBox>(string.Empty);

            if (item != null)
            {
                var canvas = item.FindAncestor<Canvas>(AncestorName);
                if (canvas != null)
                {
                    //var canvasImageViewModel = canvas.DataContext as CanvasImageViewModel;
                    //if(canvasImageViewModel != null)
                    //{
                    _resizedBounds = new Rect
                    {
                        X = Canvas.GetLeft(item),
                        Y = Canvas.GetTop(item),
                        Width = item.ActualWidth,
                        Height = item.ActualHeight
                    };

                    setTopBottomFromVerticalDelta(e, item);
                    setLeftRightFromHorizontalDelta(e, item);

                    setRegionBoxBounds(item);
                    //}
                }
            }
            e.Handled = true;
        }

        private void setRegionBoxBounds(RegionBox item)
        {
            Canvas.SetLeft(item, _resizedBounds.X);
            Canvas.SetTop(item, _resizedBounds.Y);
            item.Width = _resizedBounds.Width;
            item.Height = _resizedBounds.Height;
        }

        private void setLeftRightFromHorizontalDelta(DragDeltaEventArgs e, RegionBox item)
        {
            switch (HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    calculateLeftFromDelta(e, item);
                    break;

                case HorizontalAlignment.Right:
                    calculateWidthFromDelta(e, item);
                    break;
            }
        }

        private void calculateLeftFromDelta(DragDeltaEventArgs e, RegionBox item)
        {
            var deltaHorizontal = Math.Min(e.HorizontalChange, item.ActualWidth - item.MinWidth);
            if ((_resizedBounds.X + deltaHorizontal) >= 0)
            {
                if ((_resizedBounds.Right - (_resizedBounds.X + deltaHorizontal)) > MinRegionWidth)
                {
                    _resizedBounds.X += deltaHorizontal;
                    _resizedBounds.Width -= deltaHorizontal;
                }
            }
            else
            {
                _resizedBounds.X = 0;
            }
        }

        private void calculateWidthFromDelta(DragDeltaEventArgs e, RegionBox item)
        {
            var deltaHorizontal = Math.Min(-e.HorizontalChange, item.ActualWidth - item.MinWidth);
            if ((_resizedBounds.Width - deltaHorizontal) > MinRegionWidth)
            {
                _resizedBounds.Width -= deltaHorizontal;
            }
        }

        private void setTopBottomFromVerticalDelta(DragDeltaEventArgs e, RegionBox item)
        {
            switch (VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    setTopFromDelta(e, item);
                    break;

                case VerticalAlignment.Bottom:
                    setHeightFromDelta(e, item);
                    break;
            }
        }

        private void setTopFromDelta(DragDeltaEventArgs e, RegionBox item)
        {
            var deltaVertical = Math.Min(e.VerticalChange, item.ActualHeight - item.MinHeight);
            if ((_resizedBounds.Y + deltaVertical) >= 0)
            {
                if ((_resizedBounds.Bottom - (_resizedBounds.Y + deltaVertical)) > MinRegionHeight)
                {
                    _resizedBounds.Y += deltaVertical;
                    _resizedBounds.Height -= deltaVertical;
                }
            }
            else
            {
                _resizedBounds.Y = 0;
            }
        }

        private void setHeightFromDelta(DragDeltaEventArgs e, RegionBox item)
        {
            var deltaVertical = Math.Min(-e.VerticalChange, item.ActualHeight - item.MinHeight);
            if ((_resizedBounds.Height - deltaVertical) > MinRegionHeight)
            {
                _resizedBounds.Height -= deltaVertical;
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            if (_resizedBounds != Rect.Empty
                &&(_resizedBounds.Width>0&&_resizedBounds.Height>0))
            {
                Committed?.Invoke(this, new ResizeEventArgs(_resizedBounds));
            }
        }
    }

    public delegate void RegionBoxResizeHandler(object sender, ResizeEventArgs e);

    public class ResizeEventArgs : EventArgs
    {
        public Rect NewRect { get; }

        public ResizeEventArgs(Rect newRect)
        {
            NewRect = newRect;
        }
    }
}