﻿using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ProgressWindow : Window
    {
        public ProgressWindow()
        {
            this.DataContext = "Processing... Please wait...";
            InitializeComponent();
        }

        public ProgressWindow(string message)
        {
            this.DataContext = message;
            InitializeComponent();
        }

        public delegate void FromClose();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = this.Left - 10;
        }
    }
}
