﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Threading.Tasks;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window
    {
        private Task _task = null;

        public LoadingWindow()
        {
            InitializeComponent();
        }

        public void InitWork(Task task)
        {
            _task = task;
        }
               
        public delegate void FromClose();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = this.Left - 10;
            if(_task != null)
            {
                _task.Start();
            }
        }
    }
}
