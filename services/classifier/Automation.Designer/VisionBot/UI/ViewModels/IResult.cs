﻿using System;
using System.Collections.Generic;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    //Master Interface for result object
    public interface IResult
    {
        string Id { get; }
    }

    //Interface for individual Test result
    public interface ITestResult : IResult
    {
        IList<FieldViewModel> Fields { get; set; }
        IList<TableViewModel> Tables { get; set; }
    }

    //Interface for Individual test item/field/table result
    public interface ITestItemResult : IResult
    {
        string Name { get; }
        int ErrorCount { get; }
    }

    //Container for multiple item results.
    public interface ITestItemResults : ITestItemResult
    {
        IList<ITestItemResult> Children { get; }
    }

    //Summary result interface.
    public interface ISummaryResult : IResult
    {
        int ErrorCount { get; }
        string ItemName { get; }
        IList<ITestItemResult> Children { get; }
    }

    //Multiple type result generator
    public interface IResultGenerator
    {
        ITestItemResults Generate(EventHandler viewHandler);
    }
}