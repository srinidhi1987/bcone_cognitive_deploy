/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels.Validators;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;

    internal class DocSetItemOperator : IDocSetItemOperator
    {
        public LayoutTestSetItem CreateLayoutTestSetItem(ISelectableItem parent, IList<string> existingDocSetNames)
        {
          

            DocSetItemViewModel docSetItemViewModel = new DocSetItemViewModel(parent, existingDocSetNames);

            while (true)
            {
                var createTestSetDialog = new CreateTestSetDialog(docSetItemViewModel)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                createTestSetDialog.ShowDialog();

                if (
                    new DocumentValidator().IsValidDocument(
                        createTestSetDialog.LayoutTestSetItem.GetDocSetItem().DocumentPath))
                {
                    return createTestSetDialog.LayoutTestSetItem;
                }
                else
                {
                    docSetItemViewModel.Name = createTestSetDialog.LayoutTestSetItem.Name;
                }
            }
        }

        public bool RemoveDocSetItem(LayoutTestSetItem layoutTestSetItemToRemove)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                DataContext = new ErrorModel
                {
                    ErrorMessage = string.Format(Properties.Resources.DeleteTestDocWarning, layoutTestSetItemToRemove.Name),
                    Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "Cancel",
                            IsCancel = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                        },
                        new ButtonModel
                        {
                            Caption = "Delete Anyway",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 247, 58, 58))
                        }
                     }
                }
            };
            errorMessageDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            return errorMessageDialog.ShowDialog() == true;
        }
    }
}