﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    public class TestInspectionViewModel : ViewModelBase
    {
        public ICommand Run { get; set; }
        public ICommand Edit { get; private set; }
        public ICommand Save { get; private set; }
        public ICommand Cancel { get; private set; }
        public ICommand Train { get; private set; }

        public ICommand Rescan { get; private set; }

        private bool _isRunAllowed;

        public bool IsRunAllowed
        {
            get { return _isRunAllowed; }
            set
            {
                if (_isRunAllowed != value)
                {
                    _isRunAllowed = value;
                    OnPropertyChanged("IsRunAllowed");
                }
            }
        }

        private bool _isSaveAllowed;

        public bool IsSaveAllowed
        {
            get { return _isSaveAllowed; }
            set
            {
                if (_isSaveAllowed != value)
                {
                    _isSaveAllowed = value;
                    OnPropertyChanged("IsSaveAllowed");
                }
            }
        }

        private bool _isScanTheDocument;

        public bool IsScanTheDocument
        {
            get
            {
                return this._isScanTheDocument;
            }
            set
            {
                this._isScanTheDocument = value;
                OnPropertyChanged(nameof(IsScanTheDocument));
                OnPropertyChanged(nameof(MessageAsPerExtractionResult));
                OnPropertyChanged(nameof(ImageAsPerDataExtractionResult));
                OnPropertyChanged(nameof(MessageColourAsPerExtractionResult));
                OnPropertyChanged(nameof(IsDataExtractedSuccessfully));
            }
        }

        public bool IsDataExtractedSuccessfully
        {
            get
            {
                if (_selectedTestSetItem != null)
                    return _selectedTestSetItem.GetDocSetItem().IsDataExtractedSuccessfully;

                return true;
            }
        }

        private bool _isMappingRequired = false;

        public bool IsMappingRequired
        {
            get { return _isMappingRequired; }
            set
            {
                if (_isMappingRequired != value)
                {
                    _isMappingRequired = value;
                    OnPropertyChanged(nameof(IsMappingRequired));
                }
            }
        }

        public bool IsBenchMarkDataSaved
        {
            get
            {
                var docsetItem = _selectedTestSetItem?.GetDocSetItem();
                if (docsetItem != null)
                {
                    return _selectedTestSetItem.GetDocSetItem().IsBenchMarkDataSaved;
                }

                return true;
            }
        }

        private bool _isInEditMode = false;

        public bool IsInEditMode
        {
            get { return _isInEditMode; }
            set
            {
                if (_isInEditMode != value)
                {
                    _isInEditMode = value;
                    OnPropertyChanged(nameof(IsInEditMode));
                }
            }
        }

        public BitmapImage ImageAsPerDataExtractionResult
        {
            get
            {
                VisionBotDesignerResourcesHelper visionBotDesignerResourcesHelper =
                    new VisionBotDesignerResourcesHelper();

                String imagePath = string.Empty;
                if (_selectedTestSetItem != null)
                {
                    if (_selectedTestSetItem.Warning.HasWarning)
                    {
                        imagePath = @"/Images/IQBot_Warning.png";
                    }
                    else if (_selectedTestSetItem.GetDocSetItem().IsDataExtractedSuccessfully)
                        imagePath = @"/Images/IQ_ThumbsUp.png";
                    else if (!_selectedTestSetItem.GetDocSetItem().IsDataExtractedSuccessfully)
                        imagePath = @"/Images/IQBot_Warning.png";
                }

                return visionBotDesignerResourcesHelper.GetImage(imagePath);
            }
        }

        public string MessageColourAsPerExtractionResult
        {
            get
            {
                if (_selectedTestSetItem?.Warning?.HasWarning == true || !_selectedTestSetItem.GetDocSetItem().IsDataExtractedSuccessfully)
                {
                    return "#FFFF0000";
                }

                return "#FF3E9D00";
            }
        }

        public string MessageColorForRescanWarningDetails
        {
            get
            {
                return "#979797";
            }
        }

        public string RescanWarningDetailsMessage
        {
            get
            {
                return Properties.Resources.RescanWarningDetailsMessage;
            }
        }

        public string MessageAsPerExtractionResult
        {
            get
            {
                if (_selectedTestSetItem != null)
                {
                    if (_selectedTestSetItem.Warning.HasWarning)
                    {
                        if (_selectedTestSetItem.Warning.WarningType.Equals(WarningType.Blueprint))
                        {
                            return Properties.Resources.ForcefullyRescanMessage;
                        }
                        else if (_selectedTestSetItem.Warning.WarningType.Equals(WarningType.Layout))
                        {
                            return Properties.Resources.FailureMessageOnFieldExtraction;
                        }
                    }
                    else if (_selectedTestSetItem.GetDocSetItem().IsDataExtractedSuccessfully)
                        return Properties.Resources.SuccessMessageOnFieldExtraction;
                    else if (!_selectedTestSetItem.GetDocSetItem().IsDataExtractedSuccessfully)
                        return Properties.Resources.FailureMessageOnFieldExtraction;
                }

                return string.Empty;
            }
        }

        private bool _isEditAllowed;

        public bool IsEditAllowed
        {
            get { return _isEditAllowed; }
            set
            {
                if (_isEditAllowed != value)
                {
                    _isEditAllowed = value;
                    OnPropertyChanged("IsEditAllowed");
                }
            }
        }

        private bool _isCancelAllowed;

        public bool IsCancelAllowed
        {
            get { return _isCancelAllowed; }
            set
            {
                if (_isCancelAllowed != value)
                {
                    _isCancelAllowed = value;
                    OnPropertyChanged("IsCancelAllowed");
                }
            }
        }

        private IList<LayoutTestSetViewModel> _selectedTestSets;

        public IList<LayoutTestSetViewModel> SelectedTestSets
        {
            get { return _selectedTestSets; }
        }

        private LayoutTestSetItem _selectedTestSetItem;

        public LayoutTestSetItem SelectedTestSetItem
        {
            get { return _selectedTestSetItem; }
            private set
            {
                _selectedTestSetItem = value;
                OnPropertyChanged(nameof(SelectedTestSetItem));
            }
        }

        public event EventHandler EditClicked;

        public event EventHandler CancelClicked;

        public event EventHandler TrainClicked;

        public event EventHandler RescanClicked;

        public event EventHandler<SaveTestEventArgs> SaveClicked;

        public TestInspectionViewModel()
        {
            _selectedTestSets = new List<LayoutTestSetViewModel>();
            Save = new RelayCommand(saveCommandHandler);
        }

        public TestInspectionViewModel(IList<LayoutTestSetViewModel> selectedTestSets)
        {
            _selectedTestSets = selectedTestSets;
            Edit = new RelayCommand(editCommandHandler);
            Cancel = new RelayCommand(cancelCommandHandler);
            Save = new RelayCommand(saveCommandHandler);
            Train = new RelayCommand(TrainCommandHandler);
            Rescan = new RelayCommand(RescanCommandHandler);
            initialize();
        }

        private void RescanCommandHandler(object param)
        {
            onRescanClicked();
        }
        private void TrainCommandHandler(object param)
        {
            onTrainClicked();
        }

        private void editCommandHandler(object param)
        {
            onEditClicked();
            IsInEditMode = true;
        }

        private void cancelCommandHandler(object param)
        {
            onCancelClicked();
        }

        private void saveCommandHandler(object param)
        {
            SaveTestEventArgs args = new SaveTestEventArgs();
            onSaveClicked(args);
            if (args.IsSaveSuccessful)
            {
                OnPropertyChanged(nameof(IsBenchMarkDataSaved));
                IsInEditMode = false;
            }
            else
            {
                //TODO: We display/notify user that save was unsuccessful.
            }
        }

        private void onTrainClicked()
        {
            if (TrainClicked != null)
            {
                TrainClicked(this, EventArgs.Empty);
            }
        }

        private void onEditClicked()
        {
            if (EditClicked != null)
            {
                EditClicked(this, EventArgs.Empty);
            }
        }

        private void onCancelClicked()
        {
            if (CancelClicked != null)
            {
                CancelClicked(this, EventArgs.Empty);
                IsScanTheDocument = false;
            }
        }

        private void onSaveClicked(SaveTestEventArgs args)
        {
            if (SaveClicked != null)
            {
                SaveClicked(this, args);
                IsScanTheDocument = false;
            }
        }

        private void onRescanClicked()
        {
            if (RescanClicked != null)
            {
                RescanClicked(this, EventArgs.Empty);
            }
        }

        private void initializeSelectedTestSetItem()
        {
            if (isSingleTestSetItemSelected())
            {
                SelectedTestSetItem = _selectedTestSets[0].LayoutTestSetItems[0];
            }
        }

        private bool isSingleTestSetItemSelected()
        {
            return _selectedTestSets != null
                && _selectedTestSets.Count == 1
                && _selectedTestSets[0].LayoutTestSetItems != null
                && _selectedTestSets[0].LayoutTestSetItems.Count == 1
                && _selectedTestSets[0].LayoutTestSetItems[0] != null
                && _selectedTestSets[0].LayoutTestSetItems[0].IsSelected;
        }

        private void initialize()
        {
            if (_selectedTestSets != null && _selectedTestSets.Count > 0)
            {
                initializeSelectedTestSetItem();
                if (_selectedTestSetItem != null)
                {
                    if (_selectedTestSetItem.GetDocSetItem().IsBenchMarkDataSaved)
                    {
                        IsInEditMode = false;
                    }
                    else
                    {
                        IsInEditMode = true;
                        IsScanTheDocument = true;
                    }
                }
            }
        }
    }

    public class SaveTestEventArgs : EventArgs
    {
        public bool IsSaveSuccessful { get; set; }
        public string ErrorMessage { get; set; }
    }
}