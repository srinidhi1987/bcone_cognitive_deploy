﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class LayoutFieldsViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public ElementType ChildElementType { get; set; }

        public object Color { get; set; }
        public SolidColorBrush ItemBackground { get; set; }

      
      
        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }

        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    OnPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }

        public event EventHandler SelectionChanged;
        public event EventHandler ExpandChanged;
        public event EventHandler LayoutSaveRequested;
        public event LayoutItemDeletedEventHandler DeletedHandler;
        public IDeleteOparationWarnings FieldDeleteWarning;
        #region Items
        private ObservableCollection<LayoutField> _fields;
        public ObservableCollection<LayoutField> Fields
        {
            get { return _fields; }
            private set
            {


                if (_fields != value)
                {
                    _fields = value;
                    if (_fields != null)
                    {
                        bindEventWithFields(_fields);
                        _fields.CollectionChanged += _fields_CollectionChanged;
                    }
                }

            }
        }

        void _fields_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.OldStartingIndex == -1)
            {
                foreach (LayoutField field in e.NewItems)
                {
                    field.DeleteRequested += field_DeleteRequested;
                    field.SelectionChanged += field_SelectionChanged;
                    field.ExpandChanged += field_ExpandChanged;
                }
            }
        }
        #endregion

        #region Constructor
        public LayoutFieldsViewModel(string name, ObservableCollection<LayoutField> fields)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            if (name == "Markers")
            {
                ChildElementType = ElementType.Marker;
            }
            else
            {
                ChildElementType = ElementType.Field;
            }
            Fields = fields ?? new ObservableCollection<LayoutField>();
            FieldDeleteWarning = new LayoutTableDeleteWarning();
        }
        public LayoutFieldsViewModel(string name)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            if (name == "Markers")
            {
                ChildElementType = ElementType.Marker;
            }
            else
            {
                ChildElementType = ElementType.Field;
            }
            Fields = new ObservableCollection<LayoutField>();
            FieldDeleteWarning = new LayoutTableDeleteWarning();
        }
        #endregion

        #region Events

        private void field_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, e);
        }

        private void field_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void field_DeleteRequested(object sender, EventArgs e)
        { 
            LayoutField layoutField=sender as LayoutField;
            if (layoutField.Type == ElementType.Marker)
            {
                if(!FieldDeleteWarning.Delete(string.Format(Properties.Resources.FieldMarkerRemoveWarning, ElementType.Marker.ToString().ToLower(), layoutField.MarkerName, 
                    Properties.Resources.LayoutLabel)))
                {
                    return;
                }
            }
         
                Fields.Remove(layoutField);
            LayoutItemDeleteMode deletedMode=layoutField.Type!=ElementType.Marker? LayoutItemDeleteMode.Field:LayoutItemDeleteMode.Marker;
            OnDeleted(this, new LayoutItemDeletedEventArgs(deletedMode) { Field = layoutField });
            OnSaveRequeted();
        }

        #endregion

        private void bindEventWithFields(ObservableCollection<LayoutField> fields)
        {
            foreach (LayoutField field in fields)
            {
                field.DeleteRequested += field_DeleteRequested;
                field.SelectionChanged += field_SelectionChanged;
                field.ExpandChanged += field_ExpandChanged;
            }
        }

        #region On Event

        private void OnSaveRequeted()
        {
            if (LayoutSaveRequested != null)
            {
                LayoutSaveRequested(this, EventArgs.Empty);
            }
        }

        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private void OnDeleted(object sender, LayoutItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }
       
        #endregion
		public void ArrangeFields(IList<FieldDef> blueprintFieldList)
        {
            int indexToMove = 0;
            for (int blueprintIndex = 0; blueprintIndex < blueprintFieldList.Count; blueprintIndex++)
            {
                for (int layoutIndex = 0; layoutIndex < Fields.Count; layoutIndex++)
                {
                    if (blueprintFieldList[blueprintIndex].Id == Fields[layoutIndex].FieldDef.Id)
                    {
                        if (layoutIndex != indexToMove)
                        {
                            Fields.Move(layoutIndex, indexToMove);
                        }
                        indexToMove++;
                        break;
                    }
                }
            }
        }
    }
}
