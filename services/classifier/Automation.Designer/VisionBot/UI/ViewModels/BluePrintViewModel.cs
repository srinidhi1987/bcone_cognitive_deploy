﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    class BluePrintViewModel
    {
        public object SelectedItem { get; set; }

        public bool IsAddAvailable
        { get { return true; } }

        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {

                    onPropertyChanged("IsExpanded");
                    // onExpandChanged();
                }
            }
        }

        public string Caption { get; set; }

        public bool HasTitle { get; set; }



        public IDeleteOparationWarnings LayoutDeleteWarning;

        public event EventHandler SelectionChanged;

        //public event LayoutCreateHander LayoutCreated;
        //public delegate void LayoutCreateHander(object sender, Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs e);

        //public event LayoutSelectionHandler SelectedLayoutChanged;
        //public delegate void LayoutSelectionHandler(object sender, LayoutSelectionEventArgs e);

        public event EventHandler SaveRequested;

        public event BluePrintItemDeletedEventHandler DeletedHandler;

        private ObservableCollection<ViewModelBase> _children = new ObservableCollection<ViewModelBase>();

        public ObservableCollection<ViewModelBase> Items
        {
            get
            {
                _children.Clear();
                _children.Add(_bluePrintFields);
                _children.Add(_bluePrintTables);
                return _children;
            }
            set { }
        }

        BluePrintFieldsViewModel _bluePrintFields;

        internal BluePrintFieldsViewModel BluePrintFields
        {
            get { return _bluePrintFields; }
            set
            {
                if (_bluePrintFields != value)
                {
                    _bluePrintFields = value;
                    if (_bluePrintFields != null)
                    {
                        _bluePrintFields.SelectionChanged += BluePrintItem_OnSelectionChanged;
                        _bluePrintFields.ExpandChanged += BluePrintItem_OnExpandChanged;
                        _bluePrintFields.SaveRequested += BluePrintItem_SaveRequested;
                        _bluePrintFields.DeletedHandler += BluePrintItemDeletedHandle;
                    }
                }
            }
        }

        private BluePrintTablesViewModel _bluePrintTables;

        internal BluePrintTablesViewModel BluePrintTables
        {
            get { return _bluePrintTables; }
            private set
            {
                if (_bluePrintTables != value)
                {
                    _bluePrintTables = value;
                    if (_bluePrintTables != null)
                    {
                        _bluePrintTables.SelectionChanged += BluePrintItem_OnSelectionChanged;
                        _bluePrintTables.ExpandChanged += BluePrintItem_OnExpandChanged;
                        _bluePrintTables.SaveRequested += BluePrintItem_SaveRequested;
                        _bluePrintTables.DeletedHandler += BluePrintItemDeletedHandle;
                    }
                }
            }
        }



        public BluePrintViewModel(DataModel dataModel)
        {
            BluePrintFields = new BluePrintFieldsViewModel("Fields", dataModel.Fields);

            BluePrintTables = new BluePrintTablesViewModel("Tables", dataModel.Tables);
            BluePrintTables.OnMoveColumnToAnotherTable += BluePrintTables_OnMoveColumnToAnotherTable;
            _children = new ObservableCollection<ViewModelBase>();


            _children.Add(_bluePrintFields);
            _children.Add(_bluePrintTables);

        }



        public BluePrintViewModel(BluePrintFieldsViewModel fieldModel, BluePrintTablesViewModel tableModel)
        {
            BluePrintFields = fieldModel ?? new BluePrintFieldsViewModel("Fields");
            BluePrintTables = tableModel ?? new BluePrintTablesViewModel("Tables");
            BluePrintTables.OnMoveColumnToAnotherTable += BluePrintTables_OnMoveColumnToAnotherTable;
            _children = new ObservableCollection<ViewModelBase>();
            _children.Add(_bluePrintFields);
            _children.Add(_bluePrintTables);
        }

        #region Events

        private void BluePrintTables_OnMoveColumnToAnotherTable(TableDef arg1, FieldDef arg2, TableDef arg3)
        {
            if (OnMoveColumnToAnotherTable != null)
            {
                OnMoveColumnToAnotherTable(arg1, arg2, arg3);
            }
        }

        private void BluePrintItemDeletedHandle(object sender, BluePrintItemDeletedEventArgs e)
        {
            OnDeleted(sender, e);
        }

        void BluePrintItem_SaveRequested(object sender, EventArgs e)
        {
            OnSaveRequeted();
        }

        private void BluePrintItem_OnSelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectedItem = sender;
                SelectionChanged(sender, EventArgs.Empty);
            }
        }

        private void BluePrintItem_OnExpandChanged(object sender, EventArgs e)
        {
            IsExpanded = true;
        }

     

        #endregion

        #region On Event

        private void OnDeleted(object sender, BluePrintItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }

        private void OnSaveRequeted()
        {
            if (SaveRequested != null)
            {
                SaveRequested(this, EventArgs.Empty);
            }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<TableDef, FieldDef, TableDef> OnMoveColumnToAnotherTable;

        private void onPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public enum BluePrintItemDeleteMode
    {
        Field,
        TableField,
        Table
    }
    public delegate void BluePrintItemDeletedEventHandler(object sender, BluePrintItemDeletedEventArgs e);
    public class BluePrintItemDeletedEventArgs : EventArgs
    {
        public BluePrintItemDeleteMode Mode;
        public TableDef Table { get; set; }
        public FieldDef Field { get; set; }
        public BluePrintItemDeletedEventArgs(BluePrintItemDeleteMode mode)
        {
            Mode = mode;
        }


    }
}