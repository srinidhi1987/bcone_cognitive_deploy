﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    
    public class TableDocItem
    {
        public List<FieldDef> Headers { get; set; }
        public List<DataDocItem> Rows { get; set; }

        public TableDocItem()
        {
            Headers = new List<FieldDef>();
            Rows = new List<DataDocItem>();
        }
    }

    public class DataDocItem
    {
        public List<PreviewFieldModel> Fields { get; set; }

        public DataDocItem()
        {
            Fields = new List<PreviewFieldModel>();
        }
    }
}
