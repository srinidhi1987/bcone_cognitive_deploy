﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    class BluePrintTablesViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public ElementType ChildElementType { get; set; }
        public object Color { get; set; }
        public SolidColorBrush ItemBackground { get; set; }

        public IDeleteOparationWarnings DeleteWarning;

        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }


        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    OnPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }



        public event EventHandler SelectionChanged;
        public event EventHandler ExpandChanged;
        public event EventHandler SaveRequested;
        public event BluePrintItemDeletedEventHandler DeletedHandler;
        public event EventHandler CreateTableHandler;
        public event Action<TableDef, FieldDef, TableDef> OnMoveColumnToAnotherTable;

        private ObservableCollection<TableDef> _tables;
        public ObservableCollection<TableDef> Tables
        {
            get { return _tables; }
            private set
            {
                if (_tables != value)
                {
                    _tables = value;
                    if (_tables != null)
                    {
                        bindEventWithTables(_tables);
                        _tables.CollectionChanged += tables_CollectionChanged;
                    }
                }
            }
        }

        public RelayCommand AddTable { get; set; }
        #region Constuctor

        public BluePrintTablesViewModel(string name)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            ChildElementType = ElementType.Table;
            Tables = new ObservableCollection<TableDef>();
            DeleteWarning = new BluePrintDeleteWarning();
            AddTable = new RelayCommand(addTable);
        }

        public BluePrintTablesViewModel(string name, ObservableCollection<TableDef> tables)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            ChildElementType = ElementType.Table;
            Tables = tables ?? new ObservableCollection<TableDef>();
            DeleteWarning = new BluePrintDeleteWarning();
            AddTable = new RelayCommand(addTable);
        }

        #endregion

        #region Events

        void tables_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.OldStartingIndex == -1)
            {
                foreach (TableDef table in e.NewItems)
                {
                    table.DeleteRequested += table_DeleteRequested;
                    table.SelectionChanged += table_SelectionChanged;
                    table.ExpandChanged += table_ExpandChanged;
                    table.DeletedHandler += tableField_DeletedHandler;
                }
            }
        }

        void tableField_DeletedHandler(object sender, BluePrintItemDeletedEventArgs e)
        {
            OnDeleted(sender, e);
        }

        void table_LayoutSaveRequested(object sender, EventArgs e)
        {
            OnSaveRequeted();
        }

        private void table_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, e);
        }

        private void table_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void table_DeleteRequested(object sender, EventArgs e)
        {
            TableDef tableDef = sender as TableDef;
            if (DeleteWarning.Delete(string.Format(Properties.Resources.DataModelTableRemoveWarning, tableDef.Name, Properties.Resources.LayoutLabel)))
            {
                removeTable(tableDef);
                OnSaveRequeted();
            }
        }

        #endregion

        #region Private

        public MainMenu GetContextMenu(TableDef tableDef, FieldDef fieldDef)
        {
            if (tableDef == null
                || tableDef.Columns == null
                || tableDef.Columns.Count == 0)
            {
                return null;
            }

            TableColumnMoveMenuItemSourceInfo sourceInfo = new TableColumnMoveMenuItemSourceInfo(
                tableDef
                , fieldDef);
            var menuItem = new MenuItem(sourceInfo);
            menuItem.Name = "Move To Table";

            foreach (TableDef table in this.Tables)
            {
                if (table.Name != tableDef.Name)
                {
                    var moveMenuItem = new MenuItem(sourceInfo) { Name = table.Name };
                    moveMenuItem.Command = new RelayCommand(requestedMoveTableColumn);
                    menuItem.MenuItems.Add(moveMenuItem);
                }
            }

            var mainManu = new MainMenu();

            mainManu.MenuItems.Add(menuItem);

            return mainManu;
        }

        private void requestedMoveTableColumn(object obj)
        {
            MenuItem menuItem = obj as MenuItem;
            TableColumnMoveMenuItemSourceInfo columnMoveInfo = menuItem.SourceInfo as TableColumnMoveMenuItemSourceInfo;

            moveColumnToAnotherTable(columnMoveInfo.SourceTable, columnMoveInfo.ColumnToMove, menuItem.Name);
            if (columnMoveInfo.ColumnToMove.IsSelected == false)
            {
                columnMoveInfo.ColumnToMove.IsSelected = true;
            }
            OnSaveRequeted();
        }

        private void moveColumnToAnotherTable(TableDef sourceTable, FieldDef columnToMove, string destinationTableName)
        {
            sourceTable.Columns.Remove(columnToMove);
            foreach (TableDef table in this.Tables)
            {
                if (table.Name == destinationTableName)
                {
                    table.Columns.Add(columnToMove);
                    if(table.IsExpanded==false)
                    {
                        table.IsExpanded = true;
                    }
                   
                    if (OnMoveColumnToAnotherTable != null)
                    {
                        OnMoveColumnToAnotherTable(sourceTable
                            , columnToMove
                            , table);
                        
                    }
                    break;
                }
            }

        }

        private void removeTable(TableDef table)
        {
            if (table.Columns != null && table.Columns.Count > 0)
            {
                foreach (FieldDef column in table.Columns.ToList())
                {
                    moveColumnToAnotherTable(table, column, VisionBotEngine.Configuration.Constants.CDC.DEFAULT_TABLE_NAME);
                }
            }

            Tables.Remove(table);
         
            OnDeleted(this, new BluePrintItemDeletedEventArgs(BluePrintItemDeleteMode.Table) { Table = table });
            OnSaveRequeted();
            var defaultTable = this.Tables.FirstOrDefault(x => x.Name == VisionBotEngine.Configuration.Constants.CDC.DEFAULT_TABLE_NAME);
            if (defaultTable != null)
            { defaultTable.IsSelected = true; }
        }

        private void bindEventWithTables(ObservableCollection<TableDef> tableDef)
        {
            foreach (TableDef table in tableDef)
            {
                table.DeleteRequested += table_DeleteRequested;
                table.SelectionChanged += table_SelectionChanged;
                table.ExpandChanged += table_ExpandChanged;
                table.SaveRequested += table_SaveRequested;
                table.DeletedHandler += tableField_DeletedHandler;
            }
        }

        private void table_SaveRequested(object sender, EventArgs e)
        {
            OnSaveRequeted();
        }

        private void addTable(object para)
        {
            if (CreateTableHandler != null)
                CreateTableHandler(this, EventArgs.Empty);
        }
        #endregion

        #region OnEvent

        private void OnDeleted(object sender, BluePrintItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }

        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private void OnSaveRequeted()
        {
            if (SaveRequested != null)
            {
                SaveRequested(this, EventArgs.Empty);
            }
        }
        #endregion




    }
}
