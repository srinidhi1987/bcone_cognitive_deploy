﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    class BluePrintFieldsViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public ElementType ChildElementType { get; set; }

        public object Color { get; set; }
        public SolidColorBrush ItemBackground { get; set; }

        public IDeleteOparationWarnings DeleteWarning;

        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }

        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    OnPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }

        public event EventHandler SelectionChanged;
        public event EventHandler ExpandChanged;
        public event EventHandler SaveRequested;
        public event BluePrintItemDeletedEventHandler DeletedHandler;

        #region Items
        private ObservableCollection<FieldDef> _fields;
        public ObservableCollection<FieldDef> Fields
        {
            get { return _fields; }
            private set
            {


                if (_fields != value)
                {
                    _fields = value;
                    if (_fields != null)
                    {
                        bindEventWithFields(_fields);
                        _fields.CollectionChanged += fields_CollectionChanged;
                    }
                }

            }
        }

        void fields_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.OldStartingIndex == -1)
            {
                foreach (FieldDef field in e.NewItems)
                {
                    field.DeleteRequested += field_DeleteRequested;
                    field.SelectionChanged += field_SelectionChanged;
                    field.ExpandChanged += field_ExpandChanged;
                }
            }
        }
        #endregion

        #region Constructor
        public BluePrintFieldsViewModel(string name, ObservableCollection<FieldDef> fields)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            Fields = fields ?? new ObservableCollection<FieldDef>();
            DeleteWarning = new BluePrintDeleteWarning();
        }
        public BluePrintFieldsViewModel(string name)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            Fields = new ObservableCollection<FieldDef>();
            DeleteWarning = new BluePrintDeleteWarning();
        }
        #endregion

        #region Events

        private void field_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, e);
        }

        private void field_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void field_DeleteRequested(object sender, EventArgs e)
        {
            FieldDef fieldDef = sender as FieldDef;
            if (DeleteWarning.Delete(string.Format(Properties.Resources.DataModelFieldRemoveWarning, fieldDef.Name, Properties.Resources.LayoutLabel)))
            {
                
                Fields.Remove(fieldDef);
                BluePrintItemDeleteMode deletedMode = BluePrintItemDeleteMode.Field;
                OnDeleted(this, new BluePrintItemDeletedEventArgs(deletedMode) { Field = fieldDef });
                OnSaveRequeted();
            }
        }

        #endregion

        private void bindEventWithFields(ObservableCollection<FieldDef> fields)
        {
            foreach (FieldDef field in fields)
            {
                field.DeleteRequested += field_DeleteRequested;
                field.SelectionChanged += field_SelectionChanged;
                field.ExpandChanged += field_ExpandChanged;
            }
        }

        #region On Event

        private void OnSaveRequeted()
        {
            if (SaveRequested != null)
            {
                SaveRequested(this, EventArgs.Empty);
            }
        }

        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private void OnDeleted(object sender, BluePrintItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }

        #endregion
    }

}
