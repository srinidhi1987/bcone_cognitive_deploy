﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class RowViewModel : ViewModelBase
    {
        public FieldViewModel[] Cells { get; private set; }
        private TableViewModel _table;

        public RowViewModel(FieldViewModel[] cells)
        {
            Cells = cells;
        }

        internal void SetTable(TableViewModel table)
        {
            _table = table;
        }

        public FieldViewModel this[int index]
        {
            get { return Cells[index]; }
            set { Cells[index] = value; }
        }

        public FieldViewModel this[string columnName]
        {
            get { return Cells[getIndex(columnName)]; }
            set { Cells[getIndex(columnName)] = value; }
        }

        private int getIndex(string columnName)
        {
            for (var index = 0; index < _table.Columns.Count; index++)
            {
                if (_table.Columns[index].Value.Equals(columnName))
                {
                    return index;
                }
            }

            return -1;
        }
    }
}