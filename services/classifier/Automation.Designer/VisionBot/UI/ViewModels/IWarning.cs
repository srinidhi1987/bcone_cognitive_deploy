﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public enum WarningType
    {
        Blueprint,
        Layout,
        NoWarning
    }

    public interface IWarning
    {
        bool HasWarning { get; }
        string Message { get; }
        WarningType WarningType { get; }
    }

    public class GenericWarning : IWarning
    {
        //internal const string BlueprintBenchmarkStructureMismatchWarningMessage = "Mismatch of Test fields/columns with the Blueprint detected. For correct test results, edit and re-scan the test to synchronize the Blueprint changes.";

        public static readonly IWarning NoWarning = new GenericWarning(false, string.Empty,WarningType.NoWarning);

        public bool HasWarning { get; }

        public string Message { get; }

        public WarningType WarningType { get; }

        public GenericWarning(bool hasWarning, string message, WarningType warningType)
        {
            HasWarning = hasWarning;
            Message = message;
            WarningType = warningType;
        }
    }

    internal interface IModificationSetter
    {
        void Setter();
    }

    internal class BlueprintModificationSetter : IModificationSetter
    {
        private TestSetViewModel _testSets;

        public BlueprintModificationSetter(TestSetViewModel testSets)
        {
            _testSets = testSets;
        }

        public void Setter()
        {
            foreach (var layoutTestSet in _testSets.Items)
            {
                foreach (var layoutTestSetItem in layoutTestSet.LayoutTestSetItems)
                {
                    if (!layoutTestSetItem.Warning.HasWarning)
                    {
                        layoutTestSetItem.Warning = new GenericWarning(true, string.Format(Properties.Resources.BlueprintBenchmarkStructureMismatchWarningMessage,Properties.Resources.BlueprintTabLabel), WarningType.Blueprint);
                    }
                }
            }
        }
    }
}