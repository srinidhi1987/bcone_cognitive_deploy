﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class HeaderPanelViewModel : ViewModelBase
    {
        private string _layoutName;
        public string LayoutName
        {
            get
            {
                return _layoutName;
            }
            set
            {
                _layoutName = value;
                OnPropertyChanged("LayoutName");
            }
        }

        private string _testSetLayoutName;
        public string TestSetLayoutName
        {
            get
            {
                return _testSetLayoutName;
            }
            set
            {
                _testSetLayoutName = value;
                OnPropertyChanged("TestSetLayoutName");
            }
        }
        

        private string _testSetItemName;
        public string TestSetItemName
        {
            get
            {
                return _testSetItemName;
            }
            set
            {
                _testSetItemName = value;
                OnPropertyChanged("TestSetItemName");
 
            }
        }

        private string _layoutDocumentName;
        public string LayoutDocumentName
        {
            get { return _layoutDocumentName; }
            set
            {
                _layoutDocumentName = value;
                OnPropertyChanged("LayoutDocumentName");
            }
        }
        
        private string _testSetDocumentName;
        public string TestSetDocumentName
        {
            get { return _testSetDocumentName; }
            set
            {
                _testSetDocumentName = value;
                OnPropertyChanged("TestSetDocumentName");
            }
        }

        private bool _isDocumentSelected;
        public bool IsDocumentSelected
        {
            get
            {
                return _isDocumentSelected;
            }
            set
            {
                _isDocumentSelected = value;
                OnPropertyChanged("IsDocumentSelected");
            }
        }

        private bool _isTestSetSelected;
        public bool IsTestSetSelected 
        {
            get
            {
                return _isTestSetSelected;
            }
            set
            {
                _isTestSetSelected = value;
                OnPropertyChanged("IsTestSetSelected");
            }
        }

        private bool _isLayoutSelected;
        public bool IsLayoutSelected
        {
            get
            {
                 return _isLayoutSelected;
            }
            set
            {
                 _isLayoutSelected = value;
                 OnPropertyChanged("IsLayoutSelected");
            }
        }

        private bool _isTestSetItemSelected;
        public bool IsTestSetItemSelected
        {
            get
            {
                return _isTestSetItemSelected;
            }
            set
            {
                _isTestSetItemSelected = value;
                OnPropertyChanged("IsTestSetItemSelected");
            }
        }

    }
}
