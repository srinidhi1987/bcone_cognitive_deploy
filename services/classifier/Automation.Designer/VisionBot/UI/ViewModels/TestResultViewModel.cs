/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using System;
    using System.Windows.Input;

    public class TestResultViewModel : ViewModelBase
    {
        public ICommand RefreshTestDataCommand { get; set; }

        private object _upperContent;
        public object UpperContent
        {
            get { return _upperContent; }
            set
            {
                _upperContent = value;
                OnPropertyChanged("UpperContent");
            }
        }

        private object _lowerContent;
        public object LowerContent
        {
            get { return _lowerContent; }
            set
            {
                _lowerContent = value;
                OnPropertyChanged("LowerContent");
            }
        }

        public event EventHandler RefreshTestDataRequested;

        public TestResultViewModel()
        {
            _upperContent = null;
            _lowerContent = null;
            RefreshTestDataCommand = new RelayCommand(OnRefreshTestData);

        }

        private void OnRefreshTestData(object obj)
        {
            if (RefreshTestDataRequested != null)
            {
                RefreshTestDataRequested(this, EventArgs.Empty);
            }
        }

    }

    public class TestItemViewModel:ViewModelBase
    {

        private CanvasImageViewModel _canvasImageViewModel;

        public CanvasImageViewModel CanvasImageViewModel
        {
            get { return _canvasImageViewModel; }
            set {
                if (_canvasImageViewModel != value)
                {
                    if (_canvasImageViewModel!=null)
                    {
                        if(_canvasImageViewModel.ImageViewInfo!=null)
                        {
                            _canvasImageViewModel.ImageViewInfo.ImageSource = null;
                            _canvasImageViewModel.ImageViewInfo = null;
                        }
                        _canvasImageViewModel = null;
                    }
                    _canvasImageViewModel = value;
                    OnPropertyChanged("CanvasImageViewModel");
                }
            }
        }
        //public CanvasImageViewModel CanvasImageViewModel { get; set; }
        public UI.PreviewViewModel BenchmarkData { get; set; }
        public UI.PreviewViewModel TestExecutionData { get; set; }
        public TestDocSummaryViewModel TestDocumentSummary { get; set; }
        public TestInspectionViewModel State { get; set; }
        
    }
}