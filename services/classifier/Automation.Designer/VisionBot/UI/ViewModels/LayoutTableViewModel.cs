﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class LayoutTablesViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public ElementType ChildElementType { get; set; }
        public object Color { get; set; }
        public SolidColorBrush ItemBackground { get; set; }

        public IDeleteOparationWarnings TableDeleteWarning;

        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }


        private bool _isExpanded = false;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    OnPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }



        public event EventHandler SelectionChanged;
        public event EventHandler ExpandChanged;
        public event EventHandler LayoutSaveRequested;
        public event LayoutItemDeletedEventHandler DeletedHandler;

        private ObservableCollection<LayoutTable> _tables;
        public ObservableCollection<LayoutTable> Tables
        {
            get { return _tables; }
            private set
            {
                if (_tables != value)
                {
                    _tables = value;
                    if (_tables != null)
                    {
                        bindEventWithTables(_tables);
                        _tables.CollectionChanged += tables_CollectionChanged;
                    }
                }
            }
        }


        #region Constuctor

        public LayoutTablesViewModel(string name)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            ChildElementType = ElementType.Table;
            Tables = new ObservableCollection<LayoutTable>();
            TableDeleteWarning = new LayoutTableDeleteWarning();
        }

        public LayoutTablesViewModel(string name, ObservableCollection<LayoutTable> tables)
        {
            Name = name;
            ItemBackground = FieldTree.NormalColorBrush;
            ChildElementType = ElementType.Table;
            Tables = tables ?? new ObservableCollection<LayoutTable>();
            TableDeleteWarning = new LayoutTableDeleteWarning();
        }

        #endregion

        #region Events

        void tables_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.OldStartingIndex == -1)
            {
                foreach (LayoutTable table in e.NewItems)
                {
                    table.DeleteRequested += table_DeleteRequested;
                    table.SelectionChanged += table_SelectionChanged;
                    table.ExpandChanged += table_ExpandChanged;
                    table.DeletedHandler += tableField_DeletedHandler;
                    table.LayoutSaveRequested += table_LayoutSaveRequested;
                }
            }
        }

        void tableField_DeletedHandler(object sender, LayoutItemDeletedEventArgs e)
        {
            OnDeleted(sender, e);
        }

        void table_LayoutSaveRequested(object sender, EventArgs e)
        {
            OnSaveRequeted();
        }

        private void table_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, e);
        }

        private void table_ExpandChanged(object sender, EventArgs e)
        {
            this.IsExpanded = true;
        }

        private void table_DeleteRequested(object sender, EventArgs e)
        {
            LayoutTable layoutTable = sender as LayoutTable;
            if (layoutTable.Fields.Count == 0 || TableDeleteWarning.Delete(string.Format(Properties.Resources.TableRemoveWarning, layoutTable.TableDef.Name)))
            {
                removeTable(layoutTable);
                OnSaveRequeted();
            }
        }

        #endregion

        #region Private 

        private void removeTable(LayoutTable layoutTable)
        {
            Tables.Remove(layoutTable);
            OnDeleted(this, new LayoutItemDeletedEventArgs(LayoutItemDeleteMode.Table) { Table = layoutTable });
        }

        private void bindEventWithTables(ObservableCollection<LayoutTable> layoutTable)
        {
            foreach (LayoutTable table in layoutTable)
            {
                table.DeleteRequested += table_DeleteRequested;
                table.SelectionChanged += table_SelectionChanged;
                table.ExpandChanged += table_ExpandChanged;
                table.LayoutSaveRequested += table_LayoutSaveRequested;
                table.DeletedHandler += tableField_DeletedHandler;
            }
        }

        #endregion

        #region OnEvent

        private void OnDeleted(object sender, LayoutItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }

        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private void OnSaveRequeted()
        {
            if (LayoutSaveRequested != null)
            {
                LayoutSaveRequested(this, EventArgs.Empty);
            }
        }
        #endregion

        public void ArrangeTables(IList<TableDef> blueprintTables)
        {
            int indexToMove = 0;
            for (int blueprintTableIndex = 0; blueprintTableIndex < blueprintTables.Count; blueprintTableIndex++)
            {
                for (int layoutTableIndex = 0; layoutTableIndex < Tables.Count; layoutTableIndex++)
                {
                    if (blueprintTables[blueprintTableIndex].Id == Tables[layoutTableIndex].TableDef.Id)
                    {
                        if (layoutTableIndex != indexToMove)
                        {
                            Tables.Move(layoutTableIndex, indexToMove);
                        }
                        indexToMove++;
                        break;
                    }
                }
            }
        }

        public void ArrangeTableColumns(string tableId, IList<FieldDef> blueprintTableColumnList)
        {
            LayoutTable selectedTable = Tables.FirstOrDefault(d => d.TableDef.Id.Equals(tableId));
            if (selectedTable == null)
                return;

            int indexToMove = 0;
            for (int blueprintTableColumnIndex = 0; blueprintTableColumnIndex < blueprintTableColumnList.Count; blueprintTableColumnIndex++)
            {
                for (int layoutTableColumnIndex = 0; layoutTableColumnIndex < selectedTable.Fields.Count; layoutTableColumnIndex++)
                {
                    if (blueprintTableColumnList[blueprintTableColumnIndex].Id == selectedTable.Fields[layoutTableColumnIndex].FieldDef.Id)
                    {
                        if (layoutTableColumnIndex != indexToMove)
                        {
                            selectedTable.Fields.Move(layoutTableColumnIndex, indexToMove);
                        }
                        indexToMove++;
                        break;
                    }
                }
            }
        }
    }
}
