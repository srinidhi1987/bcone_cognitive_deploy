/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using Automation.CognitiveData.ConfidenceScore;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TestSetSummaryViewModel
    {
        public TestSetSummaryViewModel(IList<LayoutTestResult> LayoutTestResults)
        {
            CalculateTestSetSummaryData(LayoutTestResults);
        }

        public float Percentages { get; private set; }

        public int TotalTestsRunCount { get; private set; }

        public int PassedTestsCount { get; private set; }

        public int ExcludedTestsCount { get; private set; }

        public int FailedTestsCount { get; private set; }

        public int TotalLayoutsRunCount { get; private set; }

        public string TotalTimeElapse { get; private set; }

        public string AverageTimePerTest { get; private set; }

        public string ExcludedTestMessage { get; private set; }
        
        public bool ExcludedTestMessageVisibility { get; private set; }
        public void CalculateTestSetSummaryData(IList<LayoutTestResult> layoutTestResults)
        {
            TotalTestsRunCount = 0;
            FailedTestsCount = 0;
            double totalMilisecond = 0;
            double averageMiliSecond = 0;
            float overallConfidenceScore = 0.0F;
            ExcludedTestsCount = 0;

            TotalLayoutsRunCount = layoutTestResults.Count;

            foreach (LayoutTestResult result in layoutTestResults)
            {
                TotalTestsRunCount += result.TotalTestCount;
                FailedTestsCount += result.FailedTestCount;
                totalMilisecond += result.ExecutionTime.TotalMilliseconds;
                ExcludedTestsCount += result.ExcludedTests.Count;
                IList<TestDocItemResult> docResult = result.TestDocItemResults;

                overallConfidenceScore += docResult.Sum(item => item.ConfidenceScore);
            }

            PassedTestsCount = TotalTestsRunCount - FailedTestsCount;

            this.Percentages = this.TotalTestsRunCount > 0 ? (float)(Math.Round(overallConfidenceScore / this.TotalTestsRunCount, 2)) : 0;

            TimeSpan time = TimeSpan.FromMilliseconds(totalMilisecond);
            TotalTimeElapse = string.Format("{0:hh\\:mm\\:ss}", time);

            averageMiliSecond = TotalTestsRunCount > 0 ? totalMilisecond / (TotalTestsRunCount) : 0;

            TimeSpan averageSpan = TimeSpan.FromMilliseconds(averageMiliSecond);
            AverageTimePerTest = string.Format("{0:hh\\:mm\\:ss}", averageSpan);

            ExcludedTestMessageVisibility = ExcludedTestsCount > 0 ? true : false;
            ExcludedTestMessage = Properties.Resources.TestResultExcludedMessage;
        }
    }
}