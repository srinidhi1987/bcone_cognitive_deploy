using Automation.CognitiveData.Properties;

/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Text.RegularExpressions;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels.Validators
{
    internal class InvalidCharacterValidator : IInputValueValidator
    {
        public const string InvalidContext = "Invalid Context.";

        public InputValidationResult Validate(ValidationContext validationContext, string inputValue)
        {
            if (validationContext != null && validationContext.Context != null)
            {
                var expression = validationContext.Context as string;
                if (!string.IsNullOrEmpty(expression))
                {
                    var invalidFileRegex = new Regex(string.Format("[{0}]", Regex.Escape(expression)));

                    return invalidFileRegex.IsMatch(inputValue)
                        ? new InputValidationResult(string.Format(Resources.ErrorMessageForInvalideCharacters, validationContext.FieldCaption), false)
                        : InputValidationResult.Success;
                }
                else
                {
                    return new InputValidationResult(InvalidContext, false);
                }
            }

            return InputValidationResult.Success;
        }
    }
}