﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels.Validators
{
    using Automation.CognitiveData.Properties;
    using Automation.VisionBotEngine;

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using VisionBotEngine.Configuration;
    using Color = System.Windows.Media.Color;

    public class DocumentValidator
    {
        private const string _okButtonCaption = "OK";

        internal const int RecommendedDpi = 150;

        public DependencyObject dedpendencyObject { get; set; }

        private static int? _pageLimit = null;

        public bool IsValidDocument(string filePath)
        {
            VBotLogger.Trace(() => string.Format("[ IsValidDocument ] - FilePath : {0}", filePath));
            if (string.IsNullOrEmpty(filePath)) return true;
            var extension = Path.GetExtension(filePath);

            if (extension != null && extension
                     .Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
            {
                return IsValidPageCountPDF(filePath);
            }

            if (extension != null && (extension
                .Equals(".tiff", StringComparison.InvariantCultureIgnoreCase) || extension
                    .Equals(".tif", StringComparison.InvariantCultureIgnoreCase)))
            {
                bool isValid = true;
                isValid = IsValidDPIDocument(filePath);
                return isValid ? IsValidPageCountTIFF(filePath) : isValid;
            }

            return IsValidDPIDocument(filePath);
        }

        private bool IsValidDPIDocument(string filePath)
        {
            VBotLogger.Trace(() => string.Format("[ IsValidDPIDocument ] - FilePath : {0}", filePath));

            using (var imageData = new Bitmap(filePath))
            {
                if (isBelowRecommendedDpi(imageData))
                {
                    return isContinueOnDocumentWarning(Resources.ErrorMessageForLowDPI);
                }

                return true;
            }
        }

        private bool isBelowRecommendedDpi(Image image)
        {
            return image.HorizontalResolution < RecommendedDpi || image.VerticalResolution < RecommendedDpi;
        }

        private bool IsValidPageCountPDF(string filePath)
        {
            VBotLogger.Trace(() => string.Format("[ IsValidPageCountPDF ] - FilePath : {0}", filePath));
            int pageLimit = getPageLimit();
            //IPdfEngine iPdfWrapper = PdfEngineStrategy.GetPdfEngine();
            //int pageCount = iPdfWrapper.PageCount(filePath);
            int pageCount = new DesignerServiceEndPoints(new VisionBotEngine.Model.ClassifiedFolderManifest()).GetPDFPageCount(filePath);
            if (isNoPagePresent(pageCount))
            {
                return this.OnDocumentWarning(Resources.ErrorMessageForNotSupportedPDF);
            }

            return isPageCountUnderLimit(pageLimit, pageCount);
        }

        private bool IsValidPageCountTIFF(string filePath)
        {
            VBotLogger.Trace(() => string.Format("[ IsValidPageCountTIFF ] - FilePath : {0}", filePath));
            int pageLimit = getPageLimit();

            //int count = new ImageOperations().GetPageCount(filePath);
            int count = new DesignerServiceEndPoints(new VisionBotEngine.Model.ClassifiedFolderManifest()).GetImagePageCount(filePath);
            return isPageCountUnderLimit(pageLimit, count);
        }

        private int getPageLimit()
        {
            if (_pageLimit == null)
            {
                _pageLimit = Configurations.Visionbot.ValidatorConfiguredMaxPageLimit;
            }

            return _pageLimit.Value;
        }

        private string getPageLimitMessage()
        {
            return string.Format(Resources.ErrorMessageForMultiplePage, getPageLimit());
        }

        private bool isPageCountUnderLimit(int pageLimit, int pageCount)
        {
            if (pageCount > pageLimit)
            {
                return this.OnDocumentWarning(getPageLimitMessage());
            }

            return true;
        }

        private static bool isNoPagePresent(int pageCount)
        {
            return pageCount <= 0;
        }

        private bool isContinueOnDocumentWarning(string infoMessage)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = infoMessage,
                    Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "Open Anyway",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                        },
                        new ButtonModel
                        {
                            Caption = "Cancel",
                            IsCancel = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                        }
                    },
                }
            };

            if (dedpendencyObject != null)
            {
                errorMessageDialog.Owner = Window.GetWindow(dedpendencyObject);
            }

            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        private bool OnDocumentWarning(string infoMessage)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = infoMessage,
                    Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = _okButtonCaption,
                            IsCancel = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                        }
                    },
                }
            };

            if (dedpendencyObject != null)
                errorMessageDialog.Owner = Window.GetWindow(dedpendencyObject);
            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }
    }
}