﻿using Automation.CognitiveData;
using Automation.CognitiveData.VisionBot.UI.Model;
using Automation.CognitiveData.VisionBot.UI.ViewModels;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class ProjectsInformationViewModel : ViewModelBase
    {
        private ProjectManifest _selectedProject;

        public ProjectManifest selectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                RefreshGrid();
            }
        }

        public string OrganizationID { get; set; }

        public GridModel SelectedClassifiedFolder { get; set; }

        private ObservableCollection<GridModel> _foldersDetails;

        public ObservableCollection<GridModel> FoldersDetails
        {
            get { return _foldersDetails; }
            set
            {
                _foldersDetails = value;
                OnPropertyChanged(nameof(FoldersDetails));
            }
        }

        private ObservableCollection<ProjectManifest> _projects;

        public ObservableCollection<ProjectManifest> Projects
        {
            get { return _projects; }
            private set
            {
                _projects = value;
                if (_projects != null)
                {
                    _projects.CollectionChanged += _projects_CollectionChanged;
                }
            }
        }

        private ObservableCollection<VisionBotManifest> _visionBots;

        public ObservableCollection<VisionBotManifest> VisionBots
        {
            get { return _visionBots; }
            private set
            {
                _visionBots = value;
                if (_visionBots != null)
                {
                    _visionBots.CollectionChanged += _visionBots_CollectionChanged;
                }
            }
        }

        public RelayCommand Train { get; private set; }

        public RelayCommand New { get; set; }

        public ProjectsInformationViewModel( string organizationId="1")
        {
            // TODO : From where to get organizationID?
            OrganizationID = organizationId;
        
            Projects = new ObservableCollection<ProjectManifest>(new DesignerServiceEndPoints(new VisionBotEngine.Model.ClassifiedFolderManifest()).GetAllProjectsManifest(OrganizationID));
            _foldersDetails = new ObservableCollection<GridModel>();

            Train = new RelayCommand(OnTrainClicked);
            New = new RelayCommand(OnNewClicked);
        }

        public void OpenVisionBot(string projectId, string categoryID)
        {
            var project = Projects.FirstOrDefault(x => x.id == projectId);
            if (project != null)
            {
                selectedProject = project;

                var category = FoldersDetails.FirstOrDefault(x => x.CategoryId == categoryID);

                if (category != null)
                {
                    SelectedClassifiedFolder = category;
                    Train.Execute(null);
                }
            }
        }

        private void _projects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //ToDo: Set selected project and call ,ethod to update grid to display visionbot list for selected project
            //selectedProject = e.NewItems.
        }

        private void _visionBots_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void GetAllProjectsManifest(string organizationID)
        {
        }

        private void RefreshGrid()
        {
            // List<ClassifiedFolder> temp = new List<ClassifiedFolder>();
            // _visionBots = new ObservableCollection<VisionBotManifest>(DesignerServiceEndPointProvider.GetInstant().GetAllVisionBotsManifest(_selectedProject));
            _foldersDetails.Clear();

            ProjectDetails list = new DesignerServiceEndPoints(new VisionBotEngine.Model.ClassifiedFolderManifest()).GetProjectDetails(OrganizationID,_selectedProject);

            foreach (ClassifiedFolderManifest folder in list.categories)
            {
                GridModel model = new GridModel();
                model.ProjectId = _selectedProject.id;
                model.CategoryId = folder.id;
                model.CategoryName = folder.name;
                model.LastUpdated = "26/10/2016 17:54:00";
                _foldersDetails.Add(model);
            }

            //foreach (VisionBotManifest visionBotManifest in _visionBots)
            //{
            //    ClassifiedFolder folder = new ClassifiedFolder();
            //    folder.projectId = _selectedProject.id;
            //    folder.Folder = visionBotManifest;
            //    folder.LastUpdated = "26/10/2016 17:54:00";
            //    _foldersDetails.Add(folder);
            //}
        }

        public void OnTrainClicked(object control)
        {
            if (SelectedClassifiedFolder != null)
            {
                ClassifiedFolderManifest manifest = new ClassifiedFolderManifest()
                {
                    organizationId = OrganizationID,
                    id = SelectedClassifiedFolder.CategoryId,
                    name = SelectedClassifiedFolder.CategoryName,
                    projectId = SelectedClassifiedFolder.ProjectId
                };

                VisionBotUIManager manager = new VisionBotUIManager(manifest);
                manager.TrainVisionBot();
            }
        }

        public void OnNewClicked(object control)
        {
            // DesignerServiceEndPointProvider.GetInstant().CreateVisionBot(selectedProject);
        }
    }

    public class GridModel
    {
        public string ProjectId { get; set; }

        public string CategoryId { get; set; }
        public string CategoryName { get; set; }

        public string LastUpdated { get; set; }
    }
}