/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using System.Collections.Generic;
    using System.Linq;

    using Automation.CognitiveData.ConfidenceScore;

    public class LayoutClassificationErrorResultViewModel
    {
        public int ErrorCount { get; set; }
        private Dictionary<string, List<FailedObject>> LayoutClassificationTestsDictionary;

        private Dictionary<string, FailedObject> LayoutClassificationLayoutDictionary;

        public List<FailedObject> FailedLayouts { get; set; }
        public List<FailedObject> FailedTests { get; set; }

        public LayoutClassificationErrorResultViewModel(IList<LayoutTestResult> layoutTestResults)
        {
            ErrorCount = 0;

            fillListOfFailedObjects(layoutTestResults);
        }

        private void fillListOfFailedObjects(IList<LayoutTestResult> layoutTestResults)
        {
            LayoutClassificationTestsDictionary = new Dictionary<string, List<FailedObject>>();
            LayoutClassificationLayoutDictionary = new Dictionary<string, FailedObject>();

            foreach (LayoutTestResult result in layoutTestResults)
            {
                IList<TestDocItemResult> docResult = result.TestDocItemResults;

                foreach (TestDocItemResult item in docResult)
                {
                    if (!item.LayoutClassificationError.IsSameLayoutClassified)
                    {
                        ErrorCount++;
                        AddLayoutClassificationErrorInTestDictionary(item, result.LayoutId);
                        AddLayoutClassificationErrorInLayoutDictionary(result);
                    }
                }
            }
        }

        private void AddLayoutClassificationErrorInTestDictionary(TestDocItemResult item, string layoutId)
        {
            FailedObject failedTestObject = new FailedObject
            {
                Id = item.TestDocItemId,
                ParentId = layoutId,
                Type = TestItemTypes.TesSetItem,
                Name = item.TestName,
                ErrorCount = 1
            };

            if (this.LayoutClassificationTestsDictionary.ContainsKey(layoutId))
            {
                List<FailedObject> failedTestObjects = this.LayoutClassificationTestsDictionary[layoutId];
                failedTestObjects.Add(failedTestObject);
            }
            else
            {
                this.LayoutClassificationTestsDictionary.Add(layoutId, new List<FailedObject>() { failedTestObject });
            }
        }

        private void AddLayoutClassificationErrorInLayoutDictionary(LayoutTestResult result)
        {
            FailedObject failedLayoutObject = new FailedObject
            {
                Id = result.LayoutId,
                Type = TestItemTypes.Layout,
                Name = result.LayoutName,
                ErrorCount = 1
            };

            if (this.LayoutClassificationLayoutDictionary.ContainsKey(result.LayoutId))
            {
                FailedObject layoutObject = this.LayoutClassificationLayoutDictionary[result.LayoutId];

                layoutObject.ErrorCount += 1;

            }
            else
            {
                this.LayoutClassificationLayoutDictionary.Add(result.LayoutId, failedLayoutObject);
            }
        }

        public List<FailedObject> GetListOfFailedLayoutClassificationObjects()
        {
            return this.LayoutClassificationLayoutDictionary.Select(failedobject => failedobject.Value).ToList();
        }

        public List<FailedObject> GetListOfFailedLayoutClassificationTestsObject(string layoutId)
        {
            List<FailedObject> testFailedObjects = new List<FailedObject>();
            foreach (var testObject in this.LayoutClassificationTestsDictionary.Where(testObject => testObject.Key.Equals(layoutId)))
            {
                testFailedObjects.AddRange(testObject.Value.Where(testObjectItem => testObjectItem.ParentId.Equals(layoutId)));
            }

            return testFailedObjects;
        }
    }
}