﻿/***
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class IQBotSettingsViewModel : ViewModelBase
    {
        private ImageProcessingConfig _imageProcessingConfig;
        private bool _isIQBotSettingsVisible = false;
        public IQBotSettingsViewModel(ImageProcessingConfig imageProcessingConfig)
        {
            _imageProcessingConfig = imageProcessingConfig == null ? new ImageProcessingConfig() : imageProcessingConfig;

            Save = new RelayCommand(onSaveRequeted);
            Cancel = new RelayCommand(onCancelRequested);
            reset = new RelayCommand(onResetRequested);
        }

        public bool IsAutoRotationOn
        {
            get { return _imageProcessingConfig.EnableAutoRotation; }
            set
            {
                _imageProcessingConfig.EnableAutoRotation = value;
                OnPropertyChanged("IsAutoRotationOn");
            }
        }

        public bool IsPageOrientationOn
        {
            get { return _imageProcessingConfig.EnablePageOrientation; }
            set
            {
                _imageProcessingConfig.EnablePageOrientation = value;
                OnPropertyChanged("IsPageOrientationOn");
            }
        }

        public bool IsBorderFilterOn
        {
            get { return _imageProcessingConfig.EnableBorderFilter; }
            set
            {
                _imageProcessingConfig.EnableBorderFilter = value;
                OnPropertyChanged("IsBorderFilterOn");
            }
        }

        public bool IsBinarizationOn
        {
            get { return _imageProcessingConfig.EnableBinarization; }
            set
            {
                _imageProcessingConfig.EnableBinarization = value;
                OnPropertyChanged("IsBinarizationOn");
            }
        }

        public bool IsGrayscaleConversionOn
        {
            get { return _imageProcessingConfig.EnableGrayscaleConversion; }
            set
            {
                _imageProcessingConfig.EnableGrayscaleConversion = value;
                OnPropertyChanged("IsGrayscaleConversionOn");
            }
        }

        public bool IsNoiseFilterOn
        {
            get { return _imageProcessingConfig.EnableNoiseFilter; }
            set
            {
                _imageProcessingConfig.EnableNoiseFilter = value;
                OnPropertyChanged("IsNoiseFilterOn");
            }
        }

        public bool IsContrastStretchOn
        {
            get { return _imageProcessingConfig.EnableContrastStretch; }
            set
            {
                _imageProcessingConfig.EnableContrastStretch = value;
                OnPropertyChanged("IsContrastStretchOn");
            }
        }

        public IList<BinarizationTypes> BinarizationTypes
        {
            get
            {
                return new List<BinarizationTypes>(Enum.GetValues(typeof(BinarizationTypes)).Cast<BinarizationTypes>());
            }
        }

        public BinarizationTypes BinarizationType
        {
            get { return _imageProcessingConfig.BinarizationType; }
            set
            {
                _imageProcessingConfig.BinarizationType = value;
                OnPropertyChanged("BinarizationType");
            }
        }

        public int NoiseThreshold
        {
            get { return _imageProcessingConfig.NoiseThreshold; }
            set
            {
                _imageProcessingConfig.NoiseThreshold = value;
                OnPropertyChanged("NoiseThreshold");
            }
        }

        public int BinarizationThreshold
        {
            get { return _imageProcessingConfig.BinarizationThreshold; }
            set
            {
                _imageProcessingConfig.BinarizationThreshold = value;
                OnPropertyChanged("BinarizationThreshold");
            }
        }

        public bool EnbleBackgroundRemoval
        {
            get { return _imageProcessingConfig.EnbleBackgroundRemoval; }
            set
            {
                _imageProcessingConfig.EnbleBackgroundRemoval = value;
                OnPropertyChanged("EnbleBackgroundRemoval");
            }
        }
        public bool IsIQBotSettingsVisible
        {
            get { return _isIQBotSettingsVisible; }
            set
            {
                _isIQBotSettingsVisible = value;
                OnPropertyChanged("IsLayoutSettingVisible");
            }
        }
        public ICommand reset { get; set; }
        public ICommand Save { get; set; }
        public ICommand Cancel { get; set; }

        public event EventHandler SaveRequested;
        public event EventHandler CancelRequested;

        private void onResetRequested(object obj)
        {
            copyDefaultSettings();
        }

        private void onSaveRequeted(object obj)
        {
            if (SaveRequested != null)
            {
                SaveRequested(this, EventArgs.Empty);
            }
        }

        private void onCancelRequested(object obj)
        {
            if (CancelRequested != null)
            {
                CancelRequested(this, EventArgs.Empty);
            }
        }
    
        private void copyDefaultSettings()
        {
            ImageProcessingConfig config = new ImageProcessingConfig();
            this.IsAutoRotationOn = config.EnableAutoRotation;
            this.IsPageOrientationOn = config.EnablePageOrientation;
            this.IsBorderFilterOn = config.EnableBorderFilter;
            this.IsBinarizationOn = config.EnableBinarization;
            this.IsGrayscaleConversionOn= config.EnableGrayscaleConversion;
            this.IsNoiseFilterOn = config.EnableNoiseFilter;
            this.IsContrastStretchOn = config.EnableContrastStretch;

            this.BinarizationType = config.BinarizationType;
            this.NoiseThreshold = config.NoiseThreshold;
            this.BinarizationThreshold = config.BinarizationThreshold;
            this.EnbleBackgroundRemoval = config.EnbleBackgroundRemoval;
        }

    }
}
