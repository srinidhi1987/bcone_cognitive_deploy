/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Windows.Input;
using System.Collections.Generic;

using Automation.CognitiveData.VisionBot.UI.Model;
using Automation.CognitiveData.VisionBot.UI.ViewModels.Validators;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class DocSetItemViewModel : ValidatorViewModelBase
    {
        private const string NameProperty = "Name";
        private const string ErrorMessageProperty = "ErrorMessage";
        private const string IsValidNameProperty = "IsValidName";
        private const string NamePropertyLabel = "Test";
        private const string InvalidExpression = @"<>:""/\|?*";

        private DocSetItem _docSetItem = new DocSetItem();

        public event EventHandler Validated;

        public string Name
        {
            get { return _docSetItem.Name; }
            set
            {
                _docSetItem.Name = value;
                OnPropertyChanged(NameProperty);
            }
        }

        public string Id
        {
            get { return _docSetItem.Id; }
            set { _docSetItem.Id = value; }
        }

        public string DocumentName
        {
            get { return _docSetItem.DocumentName; }
            set { _docSetItem.DocumentName = value; }
        }

        public string ImagePath
        {
            get { return _docSetItem.ImagePath; }
            set { _docSetItem.ImagePath = value; }
        }

        public string DocumentPath
        {
            get { return _docSetItem.DocumentPath; }
            set { _docSetItem.DocumentPath = value; }
        }

        public ICommand ValidateDocSetName { get; private set; }

        public DocSetItemViewModel(ISelectableItem parent, IList<string> docSetNames)
            : base(getValidators(docSetNames))
        {
            _isValidName = true;
            Parent = parent;
            ValidateDocSetName = new RelayCommand(validateDocSetName);
        }

        private void validateDocSetName(object obj)
        {
            Validate();
            if (_validationResults.Count == 0)
            {
                IsValidName = true;
                if (Validated != null)
                {
                    Validated(this, EventArgs.Empty);
                }
            }
            else
            {
                OnPropertyChanged(ErrorMessageProperty);
                IsValidName = false;
            }
        }

        private bool _isValidName;

        public bool IsValidName
        {
            get { return _isValidName; }
            set
            {
                _isValidName = value;
                OnPropertyChanged(IsValidNameProperty);
            }
        }

        public string ErrorMessage
        {
            get
            {
                if (_validationResults!= null && _validationResults.ContainsKey(NameProperty))
                    return _validationResults[NameProperty];

                return string.Empty;
            }
        }

        public ISelectableItem Parent { get; private set; }

        public DocSetItem GetDocSetItem()
        {
            return _docSetItem;
        }

        private static IDictionary<string, IList<ValidationContext>> getValidators(IList<string> docSetNames)
        {
            IDictionary<string, IList<ValidationContext>> validators = new Dictionary<string, IList<ValidationContext>>
            {
                {NameProperty,
                    new List<ValidationContext>()
                    {
                        new ValidationContext(null, BlankValueValidatorService, NamePropertyLabel),
                        new ValidationContext(InvalidExpression, InvalidCharacterValidatorService, NamePropertyLabel),
                        new ValidationContext(docSetNames, UniqueValueValidatorService, NamePropertyLabel)
                    }
                }
            };
            return validators;
        }
    }
}