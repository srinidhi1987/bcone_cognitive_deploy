﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.VisionBot.UI.ViewModels.Validators;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    internal class LayoutViewModel : INotifyPropertyChanged
    {
        private const string Filter = "Documents(*.pdf;*.jpg;*.jpeg;*.png;*.tif;*.tiff)|*.pdf;*.jpg;*.jpeg;*.png;*.tif;*.tiff";
        private IqBot _iqBot;
        private FileSelectionDialog fileSelectionDialog = null;
        private Layout layout = null;

        public object SelectedItem { get; set; }

        public bool IsAddAvailable
        {
            get
            {
                return Items.Count > 0 ? false : true;
            }
            set
            {
            }
        }

        private bool _isSelected = false;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isExpanded = false;

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    onPropertyChanged("IsExpanded");
                    // onExpandChanged();
                }
            }
        }

        public string Caption { get; set; }

        public bool HasTitle { get; set; }

        public RelayCommand LayoutCreatRequested { get; set; }

        public IDeleteOparationWarnings LayoutDeleteWarning;

        public event LayoutItemSelectionHandler SelectionChanged;

        public event LayoutCreateHander LayoutCreated;

        public delegate void LayoutCreateHander(object sender, Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs e);

        public delegate void LayoutSelectionHandler(object sender, LayoutSelectionEventArgs e);

        public event EventHandler LayoutSaveRequested;

        public event LayoutItemDeletedEventHandler DeletedHandler;

        private ObservableCollection<Layout> _items;

        public ObservableCollection<Layout> Items
        {
            get { return _items; }
            set
            {
                if (_items != value)
                {
                    _items = value;
                    if (_items != null)
                    {
                        bindEventWithLayout(_items);
                    }
                }
            }
        }

        public LayoutViewModel(IqBot iqBot)
        {
            _iqBot = iqBot;
            foreach (Layout layout in _iqBot.Layouts)
            {
                layout.DeleteRequested += layout_DeleteRequested;
            }
            Items = _iqBot.Layouts;
            iqBot.Layouts.CollectionChanged += Layouts_CollectionChanged;
            LayoutCreatRequested = new RelayCommand(onLayoutCreatRequested);
            LayoutDeleteWarning = new LayoutDeleteWarning();
        }

        public void ArrangeLayoutFields(IList<FieldDef> fields)
        {
            foreach (Layout layout in Items)
            {
                layout.ArrangeLayoutFields(fields);
            }
        }

        public void ArrangeLayoutTables(IList<TableDef> blueprintTables)
        {
            foreach (Layout layout in Items)
            {
                layout.ArrangeLayoutTables(blueprintTables);
            }
        }

        public void ArrangeLayoutTableColums(string tableID, IList<FieldDef> tableColumns)
        {
            foreach (Layout layout in Items)
            {
                layout.ArrangLayoutTableColums(tableID, tableColumns);
            }
        }

        #region Events

        private void Layouts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.OldStartingIndex == -1)
            {
                foreach (Layout layout in e.NewItems)
                {
                    layout.SelectionChanged += layout_OnSelectionChanged;
                    layout.ExpandChanged += layout_OnExpandChanged;
                    layout.DeleteRequested += layout_DeleteRequested;
                    layout.LayoutSaveRequested += layout_LayoutSaveRequested;
                    layout.DeletedHandler += layoutItemDeletedHandle;
                }
            }
        }

        private void layoutItemDeletedHandle(object sender, LayoutItemDeletedEventArgs e)
        {
            OnDeleted(sender, e);
        }

        private void layout_LayoutSaveRequested(object sender, EventArgs e)
        {
            OnSaveRequeted();
        }

        private void layout_OnSelectionChanged(object sender, LayoutItemSelectionEventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectedItem = e.LayoutItem;
                SelectionChanged(sender, e);
            }
        }

        private void layout_OnExpandChanged(object sender, EventArgs e)
        {
            IsExpanded = true;
        }

        private void layout_DeleteRequested(object sender, EventArgs e)
        {
            Layout layout = sender as Layout;
            if (LayoutDeleteWarning.Delete(string.Format(Properties.Resources.LayoutRemoveWarning, layout.Name, Properties.Resources.LayoutLabel)))
            {
                Items.Remove(layout);

                Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                {
                    if (layout?.ImageProvider != null)
                    {
                        layout.ImageProvider.Dispose();
                    }
                }));

                OnSaveRequeted();
                OnDeleted(this, new LayoutItemDeletedEventArgs(LayoutItemDeleteMode.Layout) { Layout = layout, });
                onPropertyChanged(nameof(IsAddAvailable));
            }
        }

        #endregion Events

        internal event DocumentSelectionRequestHandler DocumentsSelectionRequested;

        #region CreateNewLayout

        private bool showErrorMessage(string errorMessage)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = errorMessage,
                    Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "OK",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                        }
                    },
                }
            };

            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        private void onLayoutCreatRequested(object obj)
        {
            VBotLogger.Trace(() => "[ onLayoutCreatRequested - AddLayout] - Execution Started");
            try
            {
                string documentsLocalPath = Path.Combine(
                    Path.GetTempPath(),
                    Properties.Resources.VisionBotTempDirectory,
                    _iqBot.Id, 
                    Properties.Resources.SampleDocumentTempDirectory);

                //if (_iqBot.Layouts.Count == 0 || !Directory.Exists(documentsLocalPath))
                //{
                //    if (!DesignerServiceEndPointProvider.GetInstant().DownloadDocuments(_iqBot.Id, documentsLocalPath))
                //        showErrorMessage(string.Format("Error while fetching documents"));
                //}

                if (_iqBot.Layouts.Count == 1 && _iqBot.Layouts[0].Markers.Fields.Count == 0)
                {
                    showErrorMessage(string.Format(Properties.Resources.ErrorLayoutWithoutMarker, Properties.Resources.LayoutLabel));
                    return;
                }

                layout = new Layout();
                while (true)
                {
                    var isLayoutCreated = false;
                    fileSelectionDialog = null;
                    fileSelectionDialog = new FileSelectionDialog(layout, documentsLocalPath)
                    {
                        WindowStartupLocation = WindowStartupLocation.CenterScreen

                    };
                    fileSelectionDialog.DocumentsSelectionRequested += FileSelectionDialog_DocumentsSelectionRequested; ;
                    fileSelectionDialog.Layouts = Items;

                    //  fileSelectionDialog.Owner = Window.GetWindow(this);
                    isLayoutCreated = Convert.ToBoolean(fileSelectionDialog.ShowDialog());

                    if (!isLayoutCreated)
                    {
                        VBotLogger.Trace(() => "[ onLayoutCreatRequested ] - layout creation canceled");
                        return;
                    }

                    layout = fileSelectionDialog.Layout;
                    DocumentValidator documentValidator = new DocumentValidator();// { dedpendencyObject = this };
                    if (documentValidator.IsValidDocument(fileSelectionDialog.Layout.DocumentPath))
                    {
                        break;
                    }
                }

                if (fileSelectionDialog != null)
                {
                    CreateNewLayout();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(LayoutViewModel), nameof(onLayoutCreatRequested));
            }
        }

        private void FileSelectionDialog_DocumentsSelectionRequested(object sender, DocumentsSelectionEventArgs<DocumentProperties> e)
        {
            if (DocumentsSelectionRequested != null)
            {
                DocumentsSelectionRequested(sender, e);
            }
        }

        private void CreateNewLayout()
        {
            VBotLogger.Trace(() => string.Format("[ CreateNewLayout ] - Started"));

            bool addNewLayout = true;
            Layout prevLayout = null;
            if (LayoutCreated != null)
            {
                if (IsNewLayoutCreationReportDuplicate(out prevLayout, out addNewLayout))
                {
                    while (true)
                    {
                        var keyEvent = DuplicateLayoutHandler();
                        if (keyEvent == null) return;

                        if (keyEvent.LayoutCreationOperation != LayoutCreationOperation.Open)
                        {
                            addNewLayout = false;
                            if (keyEvent.LayoutCreationOperation == LayoutCreationOperation.New)
                            {
                                addNewLayout = true;
                            }
                            prevLayout = keyEvent.NewLayout;
                            break;
                        }
                    }
                }
                else
                {
                    if (!addNewLayout)
                    {
                        addNewLayout = false;
                        layout.IsSelected = true;//fieldTreeVM.SelectLayout(prevLayout);
                    }
                }
            }
            if (addNewLayout)
            {
                fileSelectionDialog.Layout.IsSelected = false;
                layout.Settings = _iqBot.Settings;
                _iqBot.Layouts.Add(layout);
                Sorting();
                fileSelectionDialog.Layout.IsExpanded = true;
                fileSelectionDialog.Layout.IsSelected = true;
            }
            else
            {
                Layout existingLayout = (from q in Items where q.Name.Equals(prevLayout.Name) select q).FirstOrDefault();
                if (existingLayout != null)
                {
                    existingLayout.IsSelected = true;
                }
            }
            OnSaveRequeted();
        }

        internal void Sorting()
        {
            if (Items != null)
            {
                var oldIndex = Items.Count - 1;
                var newIndex = Items.OrderBy(l => l.Name).ToList().IndexOf(layout);
                Items.Move(oldIndex, newIndex);
            }
        }

        private bool IsNewLayoutCreationReportDuplicate(out Layout prevLayout, out Boolean isNewLayout)
        {
            VBotLogger.Trace(() => "[ IsNewLayoutCreationReportDuplicate ] - Execution Started");

            var eventArgs = new Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs(fileSelectionDialog.Layout);
            LayoutCreated(this, eventArgs);
            isNewLayout = false;
            prevLayout = eventArgs.NewLayout;
            if (IsLayoutNew(eventArgs))
            {
                VBotLogger.Trace(() => "[ IsNewLayoutCreationReportDuplicate ] - New Layout");
                isNewLayout = true;
                return false;
            }
            else if (IsDuplicateLayoutDialogOperationCancelEdit(eventArgs))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsDuplicateLayoutDialogOperationCancelEdit(Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs e)
        {
            if (e.LayoutCreationOperation == LayoutCreationOperation.Cancel ||
                e.LayoutCreationOperation == LayoutCreationOperation.Edit)
            {
                return true;
            }
            return false;
        }

        private bool IsLayoutNew(Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs e)
        {
            return e.LayoutCreationOperation == LayoutCreationOperation.New;
        }

        public Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs DuplicateLayoutHandler()
        {
            while (true)
            {
                fileSelectionDialog.Layouts = Items;

                string filePath = SelectFile();
                fileSelectionDialog.Layout.DocumentPath = filePath;
                fileSelectionDialog.Layout.DocumentName = Path.GetFileName(filePath);
                layout = fileSelectionDialog.Layout;
                DocumentValidator documentValidator = new DocumentValidator();// { dedpendencyObject = this };
                if (documentValidator.IsValidDocument(fileSelectionDialog.Layout.DocumentPath))
                {
                    break;
                }
            }

            Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs ev = null;
            if (fileSelectionDialog != null && !string.IsNullOrEmpty(fileSelectionDialog.Layout.DocumentPath))
            {
                if (LayoutCreated != null)
                {
                    ev = new Automation.CognitiveData.VisionBot.UI.FieldTree.LayoutCreationEventArgs(fileSelectionDialog.Layout);
                    LayoutCreated(this, ev);
                }
            }
            return ev;
        }

        private string SelectFile()
        {
            var dlg = new System.Windows.Forms.OpenFileDialog { CheckFileExists = true, Multiselect = false, Filter = Filter };

            return dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK ? dlg.FileName : string.Empty;
        }

        #endregion CreateNewLayout

        private void bindEventWithLayout(ObservableCollection<Layout> layouts)
        {
            foreach (Layout layout in layouts)
            {
                layout.SelectionChanged += layout_OnSelectionChanged;
                layout.ExpandChanged += layout_OnExpandChanged;
                layout.DeleteRequested += layout_DeleteRequested;
                layout.LayoutSaveRequested += layout_LayoutSaveRequested;
                layout.DeletedHandler += layoutItemDeletedHandle;
            }
        }

        #region On Event

        private void OnDeleted(object sender, LayoutItemDeletedEventArgs e)
        {
            if (DeletedHandler != null)
            {
                DeletedHandler(sender, e);
            }
        }

        private void OnSaveRequeted()
        {
            VBotLogger.Trace(() => "[ LayoutViewModel.cs  OnSaveRequeted ] - Save Layout");
            if (LayoutSaveRequested != null)
            {
                LayoutSaveRequested(this, EventArgs.Empty);
            }
        }

        #endregion On Event

        public event PropertyChangedEventHandler PropertyChanged;

        private void onPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public enum LayoutItemDeleteMode
    {
        Field,
        Marker,
        TableField,
        Table,
        Layout
    }

    public delegate void LayoutItemDeletedEventHandler(object sender, LayoutItemDeletedEventArgs e);

    public class LayoutItemDeletedEventArgs : EventArgs
    {
        public LayoutItemDeleteMode Mode;
        public Layout Layout { get; set; }
        public LayoutTable Table { get; set; }
        public LayoutField Field { get; set; }

        public LayoutItemDeletedEventArgs(LayoutItemDeleteMode mode)
        {
            Mode = mode;
        }
    }

    public delegate void LayoutItemSelectionHandler(object sender, LayoutItemSelectionEventArgs e);

    public class LayoutItemSelectionEventArgs : EventArgs
    {
        public Layout Layout { get; set; }
        public object LayoutItem { get; set; }

        public LayoutItemSelectionEventArgs(object layoutItem)
        {
            LayoutItem = layoutItem;
        }
    }
}