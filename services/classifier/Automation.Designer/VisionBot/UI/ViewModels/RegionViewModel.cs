/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.ComponentModel;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class RegionViewModel : ViewModelBase
    {
        internal const string IsSelectedPropertyName = "IsSelected";
        private IRegion _region;

        public Rect Bounds
        {
            get { return _region.Bounds; }
            set
            {
                _region.Bounds = value;
                OnPropertyChanged("Bounds");
            }
        }

        private Rect _displayBounds;
        public Rect DisplayBounds
        {
            get { return _displayBounds; }
            set
            {
                _displayBounds = value;
                OnPropertyChanged("DisplayBounds");
            }
        }

        public ElementType Type
        {
            get { return _region.Type; }
            set
            {
                _region.Type = value;
                OnPropertyChanged("Type");
            }
        }

        public bool IsSelected
        {
            get { return _region.IsSelected; }
            set
            {
                _region.IsSelected = value;
                OnPropertyChanged(IsSelectedPropertyName);
            }
        }

        private Rect getBoldBorderRect(int thickness)
        {
            return new Rect
            {
                X = Bounds.X,
                Y = Bounds.Y,
                Height = Bounds.Height,
                Width = Bounds.Width
            };
        }

        public UserDefinedRegionType UserDefinedRegionType
        {
            get
            {
                var layoutField = _region as LayoutField;
                if (layoutField != null)
                {
                    return layoutField.UserDefinedRegionType;
                }
                return UserDefinedRegionType.None;
            }
            set
            {
                var layoutField = _region as LayoutField;
                if (layoutField != null)
                {
                    layoutField.UserDefinedRegionType = value;
                    OnPropertyChanged("UserDefinedRegionType");
                }
            }
        }

        public IRegion Region { get { return _region; } }

        public RegionViewModel(IRegion region)
        {
            _region = region;
            DisplayBounds = _region.Bounds;
            var propChanged = (_region as INotifyPropertyChanged);
            if (propChanged != null)
            {
                propChanged.PropertyChanged += propChanged_PropertyChanged;
            }
        }

        void propChanged_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(IsSelectedPropertyName))
            {
                OnPropertyChanged(IsSelectedPropertyName);
                DisplayBounds = _region.IsSelected ? getBoldBorderRect(10) : Bounds;
            }
            else if (e.PropertyName.Equals(nameof(_region.Bounds)))
            {
                OnPropertyChanged(nameof(Bounds));
            }
        }
    }
}