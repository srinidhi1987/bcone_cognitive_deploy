/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Validation;
    using Microsoft.Win32;
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using VisionBotEngine.Configuration;

    public class UpdateFieldDef : ViewModelBase
    {
        private enum SelectableValidationOptions
        {
            None,
            List,
            Formula,
            File
        }

        #region Fields

        private ObservableCollection<string> _validationOptions = new ObservableCollection<string>
        {
            SelectableValidationOptions
                .None
                .ToString()
        };

        private ObservableCollection<Automation.VisionBotEngine.FieldValueType> _fieldValueTypes = new ObservableCollection<Automation.VisionBotEngine.FieldValueType>();

        private string _selectedValidationOption;
        private string errorMessage = string.Empty;

        private bool _showFilePathSelection;
        private bool _showFormula;
        private bool _showStaticList;

        private FieldDef _originalFieldDef;
        private TableDef _originalTableDef;

        private RelayCommand _cancelRequested;
        private RelayCommand _fieldIsRequiredRequested;
        private RelayCommand _selectFileRequested;
        private RelayCommand _updateRequested;

        #endregion Fields

        #region Events

        public event Action<FieldDef> UpdateFieldDefRequested;

        public event Action<TableDef, FieldDef> UpdateTableFieldDefRequested;

        public event Action<FieldDef> ValidateFieldRequested;

        public event Action<TableDef, FieldDef> ValidateTableCollumnRequested;

        public event Action<FieldDef> CancelFieldDefRequested;

        public event Action<TableDef, FieldDef> CancelTableFieldDefRequested;

        #endregion Events

        #region Properties

        public RelayCommand CancelRequested
        {
            get { return _cancelRequested; }
            set { _cancelRequested = value; }
        }

        public FieldDef FieldDef { get; set; }

        public RelayCommand FieldIsRequiredRequested
        {
            get { return _fieldIsRequiredRequested; }
            set { _fieldIsRequiredRequested = value; }
        }

        public ObservableCollection<Automation.VisionBotEngine.FieldValueType> FieldValueTypes
        {
            get { return _fieldValueTypes; }
            set { _fieldValueTypes = value; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public bool IsFieldChangedToRequired { get; private set; }

        public UpdateMode Mode { get; private set; }

        public RelayCommand SelectFileRequested
        {
            get { return _selectFileRequested; }
            set { _selectFileRequested = value; }
        }

        public string SelectedValidationOption
        {
            get
            {
                return this._selectedValidationOption;
            }
            set
            {
                if (value != this._selectedValidationOption)
                {
                    this._selectedValidationOption = value ?? SelectableValidationOptions.None.ToString();

                    this.ShowFormula = value == SelectableValidationOptions.Formula.ToString();
                    this.ShowStaticList = value == SelectableValidationOptions.List.ToString();
                    this.ShowFilePathSelection = value == SelectableValidationOptions.File.ToString();

                    if (value == SelectableValidationOptions.None.ToString())
                    {
                        this.resetFormula();
                        this.resetStaticList();
                        this.resetFilePathOfList();
                    }
                    else if (value == SelectableValidationOptions.List.ToString())
                    {
                        this.resetFormula();
                        this.resetFilePathOfList();
                    }
                    else if (value == SelectableValidationOptions.File.ToString())
                    {
                        this.resetFormula();
                        this.resetStaticList();
                    }
                    OnPropertyChanged(nameof(SelectedValidationOption));
                }
            }
        }

        public bool ShowFormula
        {
            get
            {
                return this._showFormula;
            }
            set
            {
                if (this._showFormula != value)
                {
                    this._showFormula = value;
                    OnPropertyChanged(nameof(ShowFormula));
                }
            }
        }

        public bool ShowStaticList
        {
            get
            {
                return this._showStaticList;
            }
            set
            {
                if (this._showStaticList != value)
                {
                    this._showStaticList = value;
                    OnPropertyChanged(nameof(ShowStaticList));
                }
            }
        }

        public bool ShowFilePathSelection
        {
            get
            {
                return this._showFilePathSelection;
            }
            set
            {
                if (this._showFilePathSelection != value)
                {
                    this._showFilePathSelection = value;
                    OnPropertyChanged(nameof(ShowFilePathSelection));
                }
            }
        }

        public TableDef TableDef { get; set; }

        public RelayCommand UpdateRequested
        {
            get { return _updateRequested; }
            set { _updateRequested = value; }
        }

        public ObservableCollection<string> ValidationOptions
        {
            get
            {
                return _validationOptions;
            }
            set
            {
                _validationOptions = value;
            }
        }

        private bool IsFormulaAllowed
        {
            get
            {
                return this.FieldDef.ValueType == FieldValueType.Number;
            }
        }

        private bool IsListAllowed
        {
            get
            {
                return this.FieldDef.ValueType == FieldValueType.Text;
            }
        }

        #endregion Properties

        #region Constructor

        public UpdateFieldDef(UpdateMode updateMode, TableDef tableDef, FieldDef fieldDef)
        {
            _originalFieldDef = fieldDef;
            _originalTableDef = tableDef;
            Mode = updateMode;

            if (fieldDef != null)
            {
                FieldDef = new FieldDef();
                copyFieldDef(fieldDef, FieldDef);
            }

            if (tableDef != null)
            {
                TableDef = new TableDef();
                copyTableDef(tableDef, TableDef); //shallow copy
            }

            _updateRequested = new RelayCommand(UpdateRequestedHandler);
            _cancelRequested = new RelayCommand(CancelRequestedHandler);
            _fieldIsRequiredRequested = new RelayCommand(ClearDefaultValueHandler);
            _selectFileRequested = new RelayCommand(SelectFileForListHandler);
            fillFieldTypes();

            this.FieldDef.PropertyChanged += delegate (object sender, PropertyChangedEventArgs args)
            {
                if (args.PropertyName == "ValueType")
                {
                    this.UpdateValidationOptions();
                }
            };

            this.UpdateValidationOptions();

            _originalFieldDef.ResetDirtyFlag();
            FieldDef.ResetDirtyFlag();
        }

        #endregion Constructor

        #region Methods

        public void CancelRequestedHandler(object param)
        {
            if (Mode == UpdateMode.Add)
            {
                if (TableDef != null && FieldDef != null)
                {
                    if (CancelTableFieldDefRequested != null)
                    {
                        CancelTableFieldDefRequested(_originalTableDef, FieldDef);
                    }
                }
                else if (FieldDef != null)
                {
                    if (CancelFieldDefRequested != null)
                    {
                        CancelFieldDefRequested(FieldDef);
                    }
                }
            }
            copyFieldDef(_originalFieldDef, FieldDef);
            FieldDef.ResetDirtyFlag();
            ErrorMessage = string.Empty;

            UpdateValidationOptions();
        }

        public void ClearDefaultValueHandler(object param)
        {
            if (FieldDef.IsRequired)
                FieldDef.DefaultValue = string.Empty;
        }

        public void SelectFileForListHandler(object param)
        {
            var filePath = SelectFile();

            if (!string.IsNullOrWhiteSpace(filePath))
            {
                FieldDef.FilePathForList = filePath;
            }
        }

        public void UpdateRequestedHandler(object param)
        {
            ErrorMessage = string.Empty;
            try
            {
                onUpdateData();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }

        private void copyFieldDef(FieldDef copyFrom, FieldDef copyTo)
        {
            //copyTo.FieldId = copyFrom.FieldId;
            copyTo.Formula = copyFrom.Formula;
            copyTo.Id = copyFrom.Id;
            copyTo.IsRequired = copyFrom.IsRequired;
            copyTo.Name = copyFrom.Name;
            copyTo.ValidationID = copyFrom.ValidationID;
            copyTo.ValueType = copyFrom.ValueType;
            copyTo.DefaultValue = copyFrom.DefaultValue;
            copyTo.StartsWith = copyFrom.StartsWith;
            copyTo.EndsWith = copyFrom.EndsWith;
            copyTo.FormatExpression = copyFrom.FormatExpression;
            copyTo.FilePathForList = copyFrom.FilePathForList;

            var copyOfStaticListProvider = new StaticListProvider();
            if (copyFrom.StaticListProvider != null)
            {
                copyOfStaticListProvider.SetItemsFromSingleString(copyFrom.StaticListProvider.GetSingleStringForItems());
            }
            copyTo.StaticListProvider = copyOfStaticListProvider;
        }

        private void copyTableDef(TableDef copyFrom, TableDef copyTo)
        {
            //copyTo.Columns = copyFrom.Columns;
            copyTo.Id = copyFrom.Id;
            copyTo.Name = copyFrom.Name;
        }

        private void fillFieldTypes()
        {
            _fieldValueTypes.Add(Automation.VisionBotEngine.FieldValueType.Text);
            _fieldValueTypes.Add(Automation.VisionBotEngine.FieldValueType.Number);
            //_fieldValueTypes.Add(Automation.VisionBotEngine.FieldValueType.Decimal);
            //_fieldValueTypes.Add(Automation.VisionBotEngine.FieldValueType.Currency);
            _fieldValueTypes.Add(Automation.VisionBotEngine.FieldValueType.Date);
            if (Configurations.Visionbot.IsCheckBoxRecognitionFeatureEnabled)
                _fieldValueTypes.Add(Automation.VisionBotEngine.FieldValueType.CheckBox);
        }

        private bool isInvalidListOrFormulaSelection()
        {
            if ((this.SelectedValidationOption == SelectableValidationOptions.List.ToString() && !this.IsListAllowed)
                || (this.SelectedValidationOption == SelectableValidationOptions.Formula.ToString()
                    && !this.IsFormulaAllowed))
            {
                this.SelectedValidationOption = SelectableValidationOptions.None.ToString();
                return true;
            }
            return false;
        }

        private void onUpdateData()
        {
            if (TableDef != null && FieldDef != null)
            {
                if (ValidateTableCollumnRequested != null)
                {
                    ValidateTableCollumnRequested(_originalTableDef, FieldDef);
                }

                if (IsFormulaAllowed && SelectedValidationOption == SelectableValidationOptions.Formula.ToString()
                    && string.IsNullOrWhiteSpace(FieldDef.Formula))
                {
                    throw new Exception("Formula cannot be left empty.");
                }

                if (IsListAllowed && SelectedValidationOption == SelectableValidationOptions.List.ToString()
                    && (FieldDef.StaticListProvider == null || !FieldDef.StaticListProvider.GetListItems().Any()))
                {
                    throw new Exception("At least one list item must be specified.");
                }

                if (IsListAllowed && SelectedValidationOption == SelectableValidationOptions.File.ToString()
                    && (!System.IO.File.Exists(FieldDef.FilePathForList)))
                {
                    throw new Exception("File not selected.");
                }

                if (Mode != UpdateMode.Add)
                {
                    IsFieldChangedToRequired = (_originalFieldDef.IsRequired != FieldDef.IsRequired && FieldDef.IsRequired);
                }
                else
                {
                    IsFieldChangedToRequired = FieldDef.IsRequired;
                }

                copyFieldDef(FieldDef, _originalFieldDef);

                if (UpdateTableFieldDefRequested != null)
                {
                    UpdateTableFieldDefRequested(_originalTableDef, _originalFieldDef);
                    FieldDef.ResetDirtyFlag();
                }
            }
            else if (FieldDef != null)
            {
                if (ValidateFieldRequested != null)
                {
                    ValidateFieldRequested(FieldDef);
                }

                if (IsFormulaAllowed && SelectedValidationOption == SelectableValidationOptions.Formula.ToString()
                    && string.IsNullOrWhiteSpace(FieldDef.Formula))
                {
                    throw new Exception("Formula cannot be left empty.");
                }

                if (IsListAllowed && SelectedValidationOption == SelectableValidationOptions.List.ToString()
                    && (FieldDef.StaticListProvider == null || !FieldDef.StaticListProvider.GetListItems().Any()))
                {
                    throw new Exception("At least one list item must be specified.");
                }

                if (IsListAllowed && SelectedValidationOption == SelectableValidationOptions.File.ToString()
                    && (!System.IO.File.Exists(FieldDef.FilePathForList)))
                {
                    throw new Exception("File not selected.");
                }

                if (Mode != UpdateMode.Add)
                {
                    IsFieldChangedToRequired = (_originalFieldDef.IsRequired != FieldDef.IsRequired && FieldDef.IsRequired);
                }
                else
                {
                    IsFieldChangedToRequired = FieldDef.IsRequired;
                }

                copyFieldDef(FieldDef, _originalFieldDef);

                if (UpdateFieldDefRequested != null)
                {
                    UpdateFieldDefRequested(_originalFieldDef);
                    FieldDef.ResetDirtyFlag();
                }
            }
        }

        private void resetFilePathOfList()
        {
            if (!string.IsNullOrWhiteSpace(this.FieldDef.FilePathForList))
            {
                this.FieldDef.FilePathForList = string.Empty;
            }
        }

        private void resetFormula()
        {
            if (!string.IsNullOrWhiteSpace(this.FieldDef.Formula))
            {
                this.FieldDef.Formula = string.Empty;
            }
        }

        private void resetStaticList()
        {
            if (!string.IsNullOrWhiteSpace(this.FieldDef.StaticListItems))
            {
                this.FieldDef.StaticListItems = string.Empty;
            }
        }

        private void UpdateValidationOptions()
        {
            ValidationOptions.Clear();
            ValidationOptions.Add(SelectableValidationOptions.None.ToString());

            if (IsListAllowed)
            {
                ValidationOptions.Add(SelectableValidationOptions.List.ToString());
                ValidationOptions.Add(SelectableValidationOptions.File.ToString());
            }

            if (IsFormulaAllowed)
            {
                ValidationOptions.Add(SelectableValidationOptions.Formula.ToString());
            }

            if (this.isInvalidListOrFormulaSelection())
            {
                return;
            }

            SelectableValidationOptions optionToSelect = SelectableValidationOptions.None;
            if (this.IsFormulaAllowed && !string.IsNullOrWhiteSpace(this.FieldDef.Formula))
            {
                optionToSelect = SelectableValidationOptions.Formula;
            }
            else if (this.IsListAllowed)
            {
                if (!string.IsNullOrWhiteSpace(this.FieldDef.StaticListItems))
                {
                    optionToSelect = SelectableValidationOptions.List;
                }
                else if (!string.IsNullOrWhiteSpace(this.FieldDef.FilePathForList))
                {
                    optionToSelect = SelectableValidationOptions.File;
                }
            }

            this.SelectedValidationOption = optionToSelect.ToString();
        }

        private static string SelectFile()
        {
            const string Filter = "File (*.txt;*.csv)|*.txt;*.csv";

            var dlg = new OpenFileDialog { CheckFileExists = true, Multiselect = false, Filter = Filter };

            return (true == dlg.ShowDialog()) ? dlg.FileName : string.Empty;
        }

        #endregion Methods
    }
}