﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public interface IDeleteOparationWarnings
    {
        bool Delete(string message);
    }

    public class LayoutDeleteWarning : IDeleteOparationWarnings
    {
        public bool Delete(string message)
        {
            return (new MessageHelper()).showDeleteConfirmation(message);
        }
    }

    public class LayoutTableDeleteWarning : IDeleteOparationWarnings
    {
        public bool Delete(string message)
        {
            return (new MessageHelper()).showDeleteConfirmation(message);
        }
    }

    public class LayoutPrimaryFieldDeleteWarning : IDeleteOparationWarnings
    {
        public bool Delete(string message)
        {
            return (new MessageHelper()).showRestrictActionMessage(message);
        }
    }

    public class BluePrintDeleteWarning : IDeleteOparationWarnings
    {
        public bool Delete(string message)
        {
            return (new MessageHelper()).showDeleteConfirmation(message);
        }
    }

    public class MessageHelper
    {
        public bool showDeleteConfirmation(string message)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = message,
                    Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "Cancel",
                            IsCancel = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                        },
                        new ButtonModel
                        {
                            Caption = "Delete Anyway",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 247, 58, 58))
                        }
                    }
                },
            };
            //  errorMessageDialog.Owner = Window.GetWindow(this);
            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }

        public bool showRestrictActionMessage(string message)
        {
            var errorMessageDialog = new ErrorMessageDialog
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                DataContext = new ErrorModel
                {
                    ErrorMessage = message,
                    Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "Okay",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 247, 58, 58))
                        }
                    }
                },
            };
            //   errorMessageDialog.Owner = Window.GetWindow(this);
            return Convert.ToBoolean(errorMessageDialog.ShowDialog());
        }
    }
}