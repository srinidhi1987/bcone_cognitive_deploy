/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.ConfidenceScore;
using System.Collections.Generic;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using System;
    using System.Linq;
    using System.Windows.Input;

    public class DataRecognitionErrorResultViewModel : ViewModelBase
    {
        public int ErrorCount { get; set; }

        public List<FailedObject> FailedFields
        {
            get
            {
                return this._failedFields;
            }
            set
            {
                if (this._failedFields != value)
                {
                    this._failedFields = value;
                    OnPropertyChanged("FailedFields");
                }
            }
        }

        public List<FailedObject> FailedLayouts
        {
            get
            {
                return this._failedLayouts;
            }
            set
            {
                if (this._failedLayouts != value)
                {
                    this._failedLayouts = value;
                    OnPropertyChanged("FailedLayouts");
                }
            }
        }

        public List<FailedObject> FailedTests
        {
            get
            {
                return this._failedTests;
            }
            set
            {
                if (this._failedTests != value)
                {
                    this._failedTests = value;
                    OnPropertyChanged("FailedTests");
                }
            }
        }

        public FailedObject SelectedFailedField
        {
            get
            {
                return this._selectedFailedField;
            }
            set
            {
                if (this._selectedFailedField != value)
                {
                    this._selectedFailedField = value;
                    OnPropertyChanged("SelectedFailedField");
                    FailedLayouts = GetListOfFailedlayouts(_selectedFailedField.Id);
                }
            }
        }

        public FailedObject SelectedFailedLayout
        {
            get
            {
                return this._selectedFailedLayout;
            }
            set
            {
                if (this._selectedFailedLayout != value)
                {
                    this._selectedFailedLayout = value;
                    OnPropertyChanged("SelectedFailedLayout");
                    FailedTests = GetListOfFailedTests(_selectedFailedField.Id, _selectedFailedLayout.Id);
                }
            }
        }

        public ICommand ViewCommand
        {
            get
            {
                return this._viewCommand;
            }
            set
            {
                this._viewCommand = value;
            }
        }

        public FailedObject SelectedTestDoc
        {
            get
            {
                return this._selectedTestDoc;
            }
            set
            {
                if (this._selectedTestDoc != value)
                {
                    this._selectedTestDoc = value;
                    OnPropertyChanged("SelectedTestDoc");
                }
            }
        }

        public Action<string> ViewTestRequested;

        private ICommand _viewCommand;

        private List<FailedObject> _failedFields;

        private List<FailedObject> _failedLayouts;

        private List<FailedObject> _failedTests;

        private FailedObject _selectedFailedField;

        private FailedObject _selectedFailedLayout;

        private FailedObject _selectedTestDoc;

        private Dictionary<string, FailedObject> FailedFieldsDictionary;

        private Dictionary<string, List<FailedObject>> FailedTestsDictionary;

        private Dictionary<string, List<FailedObject>> FailedLayoutDictionary;

        public DataRecognitionErrorResultViewModel(IList<LayoutTestResult> layoutTestResults)
        {
            ErrorCount = 0;
            ViewCommand = new RelayCommand(viewTestSetHandler);
            fillListOfFailedObjects(layoutTestResults);
            FailedFields = GetListOfFailedFields();
        }

        private void viewTestSetHandler(object testSetID)
        {
            if (SelectedTestDoc != null)
            {
                if (ViewTestRequested != null)
                {
                    ViewTestRequested(SelectedTestDoc.Id);
                }
            }
        }

        private void fillListOfFailedObjects(IList<LayoutTestResult> layoutTestResults)
        {
            FailedFieldsDictionary = new Dictionary<string, FailedObject>();
            FailedTestsDictionary = new Dictionary<string, List<FailedObject>>();
            FailedLayoutDictionary = new Dictionary<string, List<FailedObject>>();

            foreach (LayoutTestResult result in layoutTestResults)
            {
                IList<TestDocItemResult> docResult = result.TestDocItemResults;

                foreach (TestDocItemResult item in docResult)
                {
                    if (item.LayoutClassificationError.IsSameLayoutClassified)
                    {
                        foreach (KeyValuePair<string, FailedObject> failedobject in item.FailedFieldsInfo)
                        {
                            this.ErrorCount += failedobject.Value.ErrorCount;
                            this.AddInFailedFieldDictionary(failedobject, item.TestDocItemId);

                            this.AddInFailedTestDictionary(failedobject, item, result.LayoutId);

                            this.AddInFailedLayoutDictionary(failedobject, result, failedobject.Value.ErrorCount);
                        }
                    }
                }
            }
        }

        private void AddInFailedFieldDictionary(KeyValuePair<string, FailedObject> failedobject, string TestDocItemId)
        {
            if (this.FailedFieldsDictionary.ContainsKey(failedobject.Key))
            {
                FailedObject failedField = this.FailedFieldsDictionary[failedobject.Key];
                failedField.ErrorCount += failedobject.Value.ErrorCount;
            }
            else
            {
                failedobject.Value.ParentId = TestDocItemId;
                failedobject.Value.Type = TestItemTypes.Field;
                this.FailedFieldsDictionary.Add(failedobject.Key, failedobject.Value);
            }
        }

        private void AddInFailedTestDictionary(KeyValuePair<string, FailedObject> failedobject, TestDocItemResult item, string layoutId)
        {
            FailedObject failedTestObject = new FailedObject
            {
                Id = item.TestDocItemId,
                ParentId = layoutId,
                Type = TestItemTypes.TesSetItem,
                Name = item.TestName,
                ErrorCount = failedobject.Value.ErrorCount
            };

            if (this.FailedTestsDictionary.ContainsKey(failedobject.Key))
            {
                List<FailedObject> failedTestObjects = this.FailedTestsDictionary[failedobject.Key];
                failedTestObjects.Add(failedTestObject);
            }
            else
            {
                this.FailedTestsDictionary.Add(failedobject.Key, new List<FailedObject>() { failedTestObject });
            }
        }

        private void AddInFailedLayoutDictionary(KeyValuePair<string, FailedObject> failedobject, LayoutTestResult result, int TestFieldErroCount)
        {
            FailedObject failedLayoutObject = new FailedObject
            {
                Id = result.LayoutId,
                Type = TestItemTypes.Layout,
                Name = result.LayoutName,
                ErrorCount = TestFieldErroCount
            };

            if (this.FailedLayoutDictionary.ContainsKey(failedobject.Key))
            {
                List<FailedObject> failedLayoutObjects = this.FailedLayoutDictionary[failedobject.Key];
                var layoutObject = failedLayoutObjects.Find(l => l.Id.Equals(result.LayoutId));
                if (layoutObject == null)
                {
                    failedLayoutObjects.Add(failedLayoutObject);
                }
                else
                {
                    layoutObject.ErrorCount += TestFieldErroCount;
                }
            }
            else
            {
                this.FailedLayoutDictionary.Add(failedobject.Key, new List<FailedObject>() { failedLayoutObject });
            }
        }

        private List<FailedObject> GetListOfFailedFields()
        {
            return this.FailedFieldsDictionary.Select(failedobject => failedobject.Value).ToList();
        }

        private List<FailedObject> GetListOfFailedlayouts(string failedFieldId)
        {
            foreach (var layoutObject in FailedLayoutDictionary)
            {
                if (layoutObject.Key.Equals(failedFieldId))
                {
                    return layoutObject.Value;
                }
            }

            return new List<FailedObject>();
        }

        private List<FailedObject> GetListOfFailedTests(string failedFieldId, string layoutId)
        {
            List<FailedObject> testFailedObjects = new List<FailedObject>();
            foreach (var testObject in FailedTestsDictionary)
            {
                if (testObject.Key.Equals(failedFieldId))
                {
                    foreach (var testObjectItem in testObject.Value)
                    {
                        if (testObjectItem.ParentId.Equals(layoutId))
                        {
                            testFailedObjects.Add(testObjectItem);
                        }
                    }

                }
            }

            return testFailedObjects;
        }
    }

    class DataRecognitionErrorViewModel : ViewModelBase, ISummaryResult
    {
        public string Header { get; set; } = "Field";

        public int ErrorCount { get; private set; }

        public string ItemName { get; private set; }

        public IList<ITestItemResult> Children { get; private set; }

        public string Id { get; private set; }

        private ITestItemResult _selectedChild;

        public ITestItemResult SelectedChild
        {
            get { return _selectedChild; }
            set
            {
                if (_selectedChild != value)
                {
                    _selectedChild = value;
                    OnPropertyChanged("SelectedChild");
                }
            }
        }

        private ITestItemResult _selectedSecondLevelChild;

        public ITestItemResult SelectedSecondLevelChild
        {
            get { return _selectedSecondLevelChild; }
            set
            {
                if (_selectedSecondLevelChild != value)
                {
                    _selectedSecondLevelChild = value;
                    OnPropertyChanged("SelectedSecondLevelChild");
                }
            }
        }

        public event EventHandler ViewRequested;

        public DataRecognitionErrorViewModel(IResultGenerator resultGenerator)
        {
            var result = resultGenerator.Generate(viewHandler);
            ErrorCount = result.ErrorCount;
            ItemName = result.Name;
            Children = result.Children != null ? result.Children.OrderByDescending(element => element.ErrorCount).ToList() : result.Children;
            Id = result.Id;
        }

        private void viewHandler(object sender, EventArgs e)
        {
            if (ViewRequested != null)
            {
                ViewRequested(sender, e);
            }
        }
    }

    class DataRecognitionResultGenerator : IResultGenerator
    {
        private const string ItemName = "Data Recognition Error";

        private IList<LayoutTestResult> _layoutTestResults;
        private bool _isTestSet;
        private EventHandler _viewHandler;

        public DataRecognitionResultGenerator(IList<LayoutTestResult> layoutTestResults, bool isTestSet)
        {
            _layoutTestResults = layoutTestResults;
            _isTestSet = isTestSet;
        }

        public ITestItemResults Generate(EventHandler viewHandler)
        {
            _viewHandler = viewHandler;
            return fillListOfFailedObjects();
        }

        private class Entity
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public IDictionary<string, IList<TestRow>> Layouts { get; set; }

            public Entity()
            {
                Layouts = new Dictionary<string, IList<TestRow>>();
            }
        }

        private class TestRow
        {
            public string LayoutName { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
            public int ErrorCount { get; set; }
        }

        private ITestItemResults fillListOfFailedObjects()
        {
            string fieldId = string.Empty;
            IDictionary<string, Entity> entities = new Dictionary<string, Entity>();
            int totalErrorCount = 0;
            IList<ITestItemResult> fields = new List<ITestItemResult>();
            if (_isTestSet)
            {
                foreach (var layoutTestResult in _layoutTestResults)
                {
                    fieldId = layoutTestResult.LayoutId;
                    getTestSetResult(layoutTestResult.TestDocItemResults, entities,
                        new Tuple<string, string>(layoutTestResult.LayoutId, layoutTestResult.LayoutName));
                }
                foreach (var entity in entities)
                {
                    int fieldErrorCount = 0;
                    IList<ITestItemResult> layoutResults = new List<ITestItemResult>();
                    foreach (var testRow in entity.Value.Layouts)
                    {
                        var layoutErrorCount = 0;
                        IList<ITestItemResult> testResults = new List<ITestItemResult>();
                        string layoutName = string.Empty;
                        foreach (var row in testRow.Value)
                        {
                            TestItemResult testItemResultToAdd = new TestItemResult(row.Id, row.Name, row.ErrorCount);
                            testItemResultToAdd.ViewRequested += _viewHandler;
                            testResults.Add(testItemResultToAdd);
                            layoutName = row.LayoutName;
                            layoutErrorCount += row.ErrorCount;
                        }
                        layoutResults.Add(new TestItemResults(testRow.Key, layoutName, testResults, layoutErrorCount));
                        fieldErrorCount += layoutErrorCount;
                    }

                    fields.Add(new LayoutItemResults(entity.Value.Id, entity.Value.Name, layoutResults, fieldErrorCount));
                    totalErrorCount += fieldErrorCount;
                }
            }
            else
            {
                if (_layoutTestResults.Count > 0)
                {
                    getTestSetResult(_layoutTestResults[0].TestDocItemResults, entities,
                        new Tuple<string, string>(_layoutTestResults[0].LayoutId, _layoutTestResults[0].LayoutName));

                    foreach (var entity in entities)
                    {
                        int fieldErrorCount = 0;
                        IList<ITestItemResult> testResults = new List<ITestItemResult>();
                        foreach (var testRow in entity.Value.Layouts)
                        {
                            foreach (var row in testRow.Value)
                            {
                                TestItemResult testItemResultToAdd = new TestItemResult(row.Id, row.Name, row.ErrorCount);
                                testItemResultToAdd.ViewRequested += _viewHandler;
                                testResults.Add(testItemResultToAdd);
                                fieldErrorCount += row.ErrorCount;
                            }
                            break;
                        }

                        fields.Add(new TestItemResults(entity.Value.Id, entity.Value.Name, testResults, fieldErrorCount));
                        totalErrorCount += fieldErrorCount;
                    }
                }
            }
            return new TestItemResults(fieldId, ItemName, fields, totalErrorCount);
        }

        private IList<ITestItemResult> getTestSetResult(IList<TestDocItemResult> testDocItemResults,
            IDictionary<string, Entity> entities, Tuple<string, string> layoutInfo)
        {
            IList<ITestItemResult> tests = new List<ITestItemResult>();
            foreach (var testDocItemResult in testDocItemResults)
            {
                if (testDocItemResult.LayoutClassificationError.IsSameLayoutClassified)
                {
                    foreach (KeyValuePair<string, FailedObject> failedobject in testDocItemResult.FailedFieldsInfo)
                    {
                        Entity entity = getEntity(entities, failedobject);
                        updateTestRow(entity, testDocItemResult, failedobject, layoutInfo);
                    }
                }
            }
            return tests;
        }

        private void updateTestRow(Entity entity, TestDocItemResult testDocItemResult, KeyValuePair<string, FailedObject> failedobject,
            Tuple<string, string> layoutInfo)
        {
            if (entity.Layouts.ContainsKey(layoutInfo.Item1))
            {
                var testRows = entity.Layouts[layoutInfo.Item1];
                var testRow = testRows.FirstOrDefault(t => t.Id.Equals(testDocItemResult.TestDocItemId));
                if (testRow == null)
                {
                    testRow = new TestRow
                    {
                        ErrorCount = failedobject.Value.ErrorCount,
                        Id = testDocItemResult.TestDocItemId,
                        LayoutName = layoutInfo.Item2,
                        Name = testDocItemResult.TestName
                    };
                    entity.Layouts[layoutInfo.Item1].Add(testRow);
                }
                else
                {
                    testRow.ErrorCount += failedobject.Value.ErrorCount;
                }
            }
            else
            {
                var testRow = new TestRow
                {
                    ErrorCount = failedobject.Value.ErrorCount,
                    Id = testDocItemResult.TestDocItemId,
                    LayoutName = layoutInfo.Item2,
                    Name = testDocItemResult.TestName
                };
                entity.Layouts.Add(layoutInfo.Item1, new List<TestRow> { testRow });
            }
        }

        private Entity getEntity(IDictionary<string, Entity> entities, KeyValuePair<string, FailedObject> failedobject)
        {
            if (entities.ContainsKey(failedobject.Value.Id))
            {
                return entities[failedobject.Value.Id];
            }

            var entity = new Entity { Id = failedobject.Value.Id, Name = failedobject.Value.Name };
            entities.Add(entity.Id, entity);
            return entity;
        }
    }
}