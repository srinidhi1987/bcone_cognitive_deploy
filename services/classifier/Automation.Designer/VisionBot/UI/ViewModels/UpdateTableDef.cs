/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class UpdateTableDef : ViewModelBase
    {
        #region Fields
        private string errorMessage = string.Empty;

        private TableDef _originalTableDef;

        private RelayCommand _cancelRequested;
        private RelayCommand _updateRequested;
        #endregion

        #region Events
        public event Action<TableDef> CancelTableDefRequested;
        public event Action<TableDef> UpdateTableDefRequested;
        public event Action<TableDef> ValidateTableRequested;
        #endregion

        #region Properties
        public RelayCommand CancelRequested
        {
            get { return _cancelRequested; }
            set { _cancelRequested = value; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public UpdateMode Mode { get; private set; }

        public TableDef TableDef { get; set; }

        public RelayCommand UpdateRequested
        {
            get { return _updateRequested; }
            set { _updateRequested = value; }
        }
        #endregion

        #region Constructor
        public UpdateTableDef(UpdateMode updateMode, TableDef tableDef)
        {
            _originalTableDef = tableDef;
            Mode = updateMode;

            if (tableDef != null)
            {
                TableDef = new TableDef();
                copyTableDef(tableDef, TableDef); //shallow copy
            }

            _originalTableDef.ResetDirtyFlag();
            TableDef.ResetDirtyFlag();

            _updateRequested = new RelayCommand(UpdateRequestedHandler);
            _cancelRequested = new RelayCommand(CancelRequestedHandler);
        }
        #endregion

        #region Methods
        public void CancelRequestedHandler(object param)
        {
            if (Mode == UpdateMode.Add)
            {
                if (CancelTableDefRequested != null)
                {
                    CancelTableDefRequested(TableDef);
                }
            }
            else
                copyTableDef(_originalTableDef, TableDef);
            TableDef.ResetDirtyFlag();
        }

        public void UpdateRequestedHandler(object param)
        {
            ErrorMessage = string.Empty;
            try
            {

                onUpdateData();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }

        private void copyTableDef(TableDef copyFrom, TableDef copyTo)
        {
            copyTo.Id = copyFrom.Id;
            copyTo.Name = copyFrom.Name;
        }

        private void onUpdateData()
        {
            if (TableDef != null)
            {
                if (ValidateTableRequested != null)
                {
                    ValidateTableRequested(TableDef);
                }

                copyTableDef(TableDef, _originalTableDef);

                if (UpdateTableDefRequested != null)
                {
                    UpdateTableDefRequested(_originalTableDef);
                    TableDef.ResetDirtyFlag();
                }
            }
        }
        #endregion
    }
}