﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.TemplateSelector
{
    using System.Windows;
    using System.Windows.Controls;

    internal class PreviewTemplateSelector : DataTemplateSelector
    {
        private const string PreviewDataTemplate = "PreviewDataTemplate";

        private const string ErrorMessageDataTemplate = "ErrorMessageDataTemplate";

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var contentControl = container as ContentPresenter;

            if (item is PreviewViewModel)
                return contentControl.FindResource(PreviewDataTemplate) as DataTemplate;
            if (item is string)
                return contentControl.FindResource(ErrorMessageDataTemplate) as DataTemplate;

            return null;
        }
    }
}
