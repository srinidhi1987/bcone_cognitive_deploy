/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI
{
    public static class VisualTreeWalker
    {
        public static T FindAncestor<T>(this DependencyObject child, string ancestorName) where T : DependencyObject
        {
            var dependencyParent = VisualTreeHelper.GetParent(child);
            if (dependencyParent == null)
            {
                return null;
            }

            var parent = dependencyParent as T;
            if (parent != null)
            {
                if (string.IsNullOrEmpty(ancestorName))
                {
                    return parent;
                }

                if (ancestorName.Equals(parent.GetValue(FrameworkElement.NameProperty)))
                {
                    return parent;
                }
            }

            return FindAncestor<T>(dependencyParent, ancestorName);
        }
    }
}