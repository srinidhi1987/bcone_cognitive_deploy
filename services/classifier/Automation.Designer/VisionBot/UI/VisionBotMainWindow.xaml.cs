﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.Model;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;

    //using Automation.Common;
    using Automation.VisionBotEngine;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Windows.Media;
    using System.Windows.Threading;
    using VisionBotEngine.Model;

    /// <summary>
    /// Interaction logic for VisionBotMainWindow.xaml
    /// </summary>
    public partial class VisionBotMainWindow : Window
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button

        public event SaveRequestHandler SaveRequested;

        public event EventHandler TestRequested;

        public event EventHandler CloseRequested;

        public event FieldTree.LayoutCreateHander LayoutCreated;

        public event ToolBar.PreviewEventHandler PreviewRequested;

        public event TestDocSetItemAddHandler TestDocSetModificationRequested;

        internal event ExecuteTestHandler ExecuteTestRequested;

        internal event LayoutDeletedHandler LayoutDeleted;

        internal event FieldValueRequestHandler LayoutFieldValueRequested;

        internal event LayoutRescanEventHandler LayoutRescanRequested;

        internal event BenchMarkPanelActionHandler BenchMarkPanelActionRequested;

        internal event UpdateTestSetWarningHandler UpdateTestSetRequested;

        internal event ExportPreviewDataEventHandler ExportPreviewDataEventRequested;

        internal event DocumentsDownloadRequestHandler DocumentsDownloadRequested;
        internal event DocumentSelectionRequestHandler DocumentsSelectionRequested;
        internal event DocumentPageImageRequestHandler DocumentPageImageRequested;
        internal event Action<IqBot> ReArrageDataModelFieldRequested;

        public VisionBotMainWindow()
        {
            InitializeComponent();
            VisionBotDesignerView.SaveRequested += VisionBotDesignerView_SaveRequested;
            VisionBotDesignerView.TestRequested += VisionBotDesignerView_TestRequested;
            VisionBotDesignerView.LayoutCreated += VisionBotDesignerView_LayoutCreated;
            VisionBotDesignerView.PreviewRequested += VisionBotDesignerView_PreviewRequested;
            VisionBotDesignerView.TestDocSetModificationRequested += VisionBotDesignerView_TestDocSetModificationRequested;
            VisionBotDesignerView.ExecuteTestRequested += VisionBotDesignerView_ExecuteTestRequested;
            VisionBotDesignerView.LayoutDeleted += VisionBotDesignerView_LayoutDeleted;
            VisionBotDesignerView.LayoutFieldValueRequested += VisionBotDesignerView_LayoutFieldValueRequested;
            VisionBotDesignerView.LayoutRescanRequested += VisionBotDesignerView_LayoutRescanRequested;
            VisionBotDesignerView.BenchMarkPanelActionRequested += VisionBotDesignerView_BenchMarkPanelActionRequested;
            VisionBotDesignerView.UpdateTestSetRequested += VisionBotDesignerView_UpdateTestSetRequested;
            VisionBotDesignerView.ExportPreviewDataRequested += VisionBotDesignerView_ExportPreviewDataRequested;
            VisionBotDesignerView.DocumentsDownloadRequested += VisionBotDesignerView_DocumentsDownloadRequested;
            VisionBotDesignerView.DocumentsSelectionRequested += VisionBotDesignerView_DocumentsSelectionRequested;
            VisionBotDesignerView.DocumentPageImageRequested += VisionBotDesignerView_DocumentPageImageRequested;
            VisionBotDesignerView.ReArrageDataModelFieldRequested += VisionBotDesignerView_ReArrageDataModelFieldRequested;
            SourceInitialized += MainWindow_SourceInitialized;
        }

        private void VisionBotDesignerView_ReArrageDataModelFieldRequested(IqBot obj)
        {
            if(ReArrageDataModelFieldRequested!=null)
            {
                ReArrageDataModelFieldRequested(obj);
            }
        }

        private void VisionBotDesignerView_DocumentsDownloadRequested(object sender, EventArgs e)
        {
           if(DocumentsDownloadRequested!=null)
            {
                DocumentsDownloadRequested(sender, e);
            }
        }

        private void VisionBotDesignerView_DocumentPageImageRequested(object sender, VisionBotEngine.Model.DocumentProperties documentProperties, int pageIndex, string imagePath, VisionBotEngine.Model.ImageProcessingConfig settings)
        {
            if (DocumentPageImageRequested != null)
            {
                    DocumentPageImageRequested(sender, documentProperties, pageIndex, imagePath, settings);
            }
        }

        

        private void VisionBotDesignerView_DocumentsSelectionRequested(object sender, DocumentsSelectionEventArgs<DocumentProperties> e)
        {
            if (DocumentsSelectionRequested != null)
            {
                    DocumentsSelectionRequested(sender, e);
            }
        }

        private void VisionBotDesignerView_ExportPreviewDataRequested(object sender, PreviewEventArgs e)
        {
            if (ExportPreviewDataEventRequested != null)
            {
                ExportPreviewDataEventRequested(sender, e);
            }
        }

        private void VisionBotDesignerView_BenchMarkPanelActionRequested(object sender, TestDocSetEventArgs e)
        {
            if (BenchMarkPanelActionRequested != null)
            {
                BenchMarkPanelActionRequested(sender, e);
            }
        }

        private void VisionBotDesignerView_LayoutFieldValueRequested(object sender, FieldValueRequestEventArgs e)
        {
            if (LayoutFieldValueRequested != null)
            {
                    LayoutFieldValueRequested(sender, e);
            }
        }

        private void VisionBotDesignerView_LayoutDeleted(object sender, LayoutDeletedEventArgs e)
        {
            if (LayoutDeleted != null)
            {
                    LayoutDeleted(sender, e);
            }
        }

        private void VisionBotDesignerView_ExecuteTestRequested(object sender, TestExecutionEventArgs e)
        {
            if (ExecuteTestRequested != null)
            {
               
                    ExecuteTestRequested(this, e);
            }
        }

        private void VisionBotDesignerView_TestDocSetModificationRequested(object sender, TestDocSetEventArgs e)
        {
              TestDocSetModificationRequested(sender, e);
        }

        private void VisionBotDesignerView_LayoutRescanRequested(object sender, PreviewEventArgs e)
        {
            if (LayoutRescanRequested != null)
            {
                    LayoutRescanRequested(this, e);
            }
        }

        private IntPtr _windowHandle;

        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;

            //disable minimize button
            DisableMinimizeButton();
        }

        protected void DisableMinimizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");

            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MINIMIZEBOX);
        }

        public void SetIqBot(IqBot iqBot)
        {
            VisionBotDesignerView.IqBot = iqBot;
        }

        public void SelectTab(int tabindex)
        {
            VisionBotDesignerView.SelectTab(tabindex);
        }

        private void VisionBotDesignerView_UpdateTestSetRequested(object sender, UpdateTestSetWarningEventArgs e)
        {
           
                UpdateTestSetRequested?.Invoke(this, e);
        }

        private void VisionBotDesignerView_SaveRequested(object sender, SaveRequestEventArgs e)
        {
            if (SaveRequested != null)
            {
                    SaveRequested(this, e);
            }
        }

        private void VisionBotDesignerView_TestRequested(object sender, EventArgs e)
        {
            if (TestRequested != null)
            {
               TestRequested(this, e);
            }
        }

        private void VisionBotDesignerView_LayoutCreated(object sender, FieldTree.LayoutCreationEventArgs e)
        {
            if (LayoutCreated != null)
            {
                    LayoutCreated(this, e);
            }
        }

        private void VisionBotDesignerView_PreviewRequested(object sender, PreviewEventArgs e)
        {
            if (PreviewRequested != null)
            {
                    PreviewRequested(this, e);
            }
        }

        private void VisionBotMainWindow_OnContentRendered(object sender, EventArgs e)
        {
            Visibility = Visibility.Visible;
        }

        public void SetTestSet(List<LayoutTestSet> testDocSets)
        {
            VisionBotDesignerView.TestDocSets = testDocSets;
            VisionBotDesignerView.IsLoadingCompleted = true;
        }

        private bool isPlayerRunning()
        {
            //ToDo : Need to check if this method require or not. If require then need to think different appraoch to check AAPlayer running status.

            //var process = Routines.GetProcessByName(System.Windows.Forms.Application.StartupPath, "AAPlayer");
            //if (process != null)
            //{
            //    //Player is running.
            //    return true;
            //}
            return false;
        }

        private void VisionBotMainWindow_OnClosed(object sender, EventArgs e)
        {
            try
            {
                VBotLogger.Trace(() => "[VisionBotMainWindow_OnClosed] Closed IQBot Designer.");

                if (CloseRequested != null)
                {
                    CloseRequested(this, EventArgs.Empty);
                }

                ABortBackgroundThread();

                if (!isPlayerRunning())
                {
                    var directoryPath = Path.Combine(
                          Path.GetTempPath(),
                          Properties.Resources.VisionBotTempDirectory);
                    if (Directory.Exists(directoryPath))
                    {
                        Directory.Delete(directoryPath, true);
                        VBotLogger.Trace(() => string.Format("[VisionBotMainWindow_OnClosed] Successfully deleted {0} directory.", directoryPath));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotMainWindow), nameof(VisionBotMainWindow_OnClosed));
            }
        }

        private void ABortBackgroundThread()
        {
            AbortLayoutImageProviderThread();

            AbortTestDocSetItemImageProviderThread();
        }

        private void AbortLayoutImageProviderThread()
        {
            if (VisionBotDesignerView?.IqBot?.Layouts != null)
            {
                foreach (var layout in VisionBotDesignerView.IqBot.Layouts)
                {
                    if (layout?.ImageProvider != null)
                    {
                        layout.ImageProvider.Dispose();
                    }
                }
            }
        }

        private void AbortTestDocSetItemImageProviderThread()
        {
            if (VisionBotDesignerView?.TestDocSets != null)
            {
                foreach (var docsets in VisionBotDesignerView.TestDocSets)
                {
                    if (docsets.DocSetItems != null)
                    {
                        foreach (var document in docsets.DocSetItems)
                        {
                            if (document?.ImageProvider != null)
                            {
                                document.ImageProvider.Dispose();
                            }
                        }
                    }
                }
            }
        }

        private void handleExceptionWithMessage(Action action, [CallerMemberName]string memberName = "")
        {
            //try
            //{
                action.Invoke();
            //}
            //catch (Exception ex)
            //{
            //    ex.Log(nameof(VisionBotEngineCommunicator), memberName, ex.ToString());
            //    showErrorMessage(ex.Message);
            //}

        }

        public void showErrorMessage(string errorMessage)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal,
                 new Action(() =>
                 {
                     var errorMessageDialog = new ErrorMessageDialog
                     {
                         WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen,
                         DataContext = new ErrorModel
                         {
                             ErrorMessage = errorMessage,
                             Buttons = new ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = "OK",
                            IsDefault = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                        }
                    },
                         }
                     };

                     errorMessageDialog.ShowDialog();
                 }));
        }
    }
}