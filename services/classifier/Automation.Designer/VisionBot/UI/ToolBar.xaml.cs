﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    using System;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// Interaction logic for ToolBar.xaml
    /// </summary>
    public partial class ToolBar : UserControl
    {
        public ToolBar()
        {
            InitializeComponent();
        }

        public delegate void PreviewHandler(object sender, EventArgs e);
        public delegate void PreviewEventHandler(object sender, PreviewEventArgs e);
        public event PreviewHandler PreviewRequested;
        public event PreviewHandler PreviewCloseRequested;
      
        //TODO Make it proper way and try to involve viewmodel here
        private void Preview_OnClick(object sender, EventArgs e)
        {
            ToggleButton toggleButton = (ToggleButton)sender;
            if (toggleButton.IsChecked.HasValue && toggleButton.IsChecked.Value)
            {
                if (PreviewRequested != null)
                {
                    PreviewRequested(this, new EventArgs());
                    previewTB.IsChecked = true;
                }
            }
            else
            {
                if (PreviewCloseRequested != null)
                {
                    PreviewCloseRequested(this, new EventArgs());
                }
            }
        }
        //TODO Make it proper way and try to involve viewmodel here
        public void ResetPreview()
        {
            if (previewTB.IsChecked.HasValue && previewTB.IsChecked.Value)
            {
                previewTB.IsChecked = false;
                if (PreviewCloseRequested != null)
                {
                    PreviewCloseRequested(this, new EventArgs());
                }
            }
        }


    }
}
