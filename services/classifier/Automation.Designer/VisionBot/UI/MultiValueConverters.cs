/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.ConfidenceScore.Validation;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Media;
    using VisionBotEngine.Configuration;
    using Brush = System.Windows.Media.Brush;
    using Brushes = System.Windows.Media.Brushes;
    using Color = System.Windows.Media.Color;
    using ColorConverter = System.Windows.Media.ColorConverter;

    public class UserDefinedTypeToVisibilityValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue || values[2] == DependencyProperty.UnsetValue)
            {
                return Visibility.Collapsed;
            }
            var userDefinedType = (UserDefinedRegionType)values[0];
            var isSelected = (bool)values[1];
            var type = (ElementType)values[2];
            if (userDefinedType != UserDefinedRegionType.None && isSelected && isSystemIdentifiedRegion(type))
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        private bool isSystemIdentifiedRegion(ElementType type)
        {
            return (type == ElementType.SystemIdentifiedField || type == ElementType.SystemIdentifiedRegion);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PageOffsetAndZoomMultiConverter : IMultiValueConverter
    {
        private double zoomPercentage = 1.0;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
            {
                return zoomPercentage;
            }

            int pageTopOffset = 0;
            if (values.Length > 2 && values[2] is int)
                pageTopOffset = (int)values[2];

            if (values[1] is double)
                zoomPercentage = Math.Round((double)values[1], 2);

            var parameterValue = System.Convert.ToDouble(parameter);

            var presentValue = 0.0;
            if (!double.IsInfinity((int)values[0]))
            {
                presentValue = System.Convert.ToDouble(values[0]) - pageTopOffset;
            }
            //var isSelected = (values.Length == 2) || (bool)values[3];

            var value = (presentValue + parameterValue) * zoomPercentage;
            //if (!isSelected)
            //{
            //    return value;
            //}

            return (parameterValue < 0) ? value - 1 : value + 1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            var presentValue = DependencyProperty.UnsetValue;

            if (value != DependencyProperty.UnsetValue)
            {
                double value1 = System.Convert.ToDouble(value);
                var parameterValue = System.Convert.ToDouble(parameter);
                presentValue = (value1 / zoomPercentage) - parameterValue;
            }
            //System.Diagnostics.Debug.WriteLine("Convert");
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing };
        }
    }

    public class ZoomMultiConverter : IMultiValueConverter
    {
        private double zoomPercentage = 1.0;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
            {
                return zoomPercentage;
            }

            if (values[1] is double)
                zoomPercentage = Math.Round((double)values[1], 2);

            var parameterValue = System.Convert.ToDouble(parameter);

            var presentValue = 0.0;
            if (!double.IsInfinity((double)values[0]))
            {
                presentValue = System.Convert.ToDouble(values[0]);
            }
            var isSelected = (values.Length == 2) || (bool)values[2];

            var value = (presentValue + parameterValue) * zoomPercentage;
            if (!isSelected)
            {
                return value;
            }

            return (parameterValue < 0) ? value - 1 : value + 1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            var presentValue = DependencyProperty.UnsetValue;

            if (value != DependencyProperty.UnsetValue)
            {
                double value1 = System.Convert.ToDouble(value);
                var parameterValue = System.Convert.ToDouble(parameter);
                presentValue = (value1 / zoomPercentage) - parameterValue;
            }
            //System.Diagnostics.Debug.WriteLine("Convert");
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing };
        }
    }

    public class ZoomMultiConverterNew : IMultiValueConverter
    {
        private double zoomPercentage = 1.0;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
            {
                return zoomPercentage;
            }

            if (values[1] is double)
                zoomPercentage = Math.Round((double)values[1], 3);

            var parameterValue = System.Convert.ToDouble(parameter);

            var presentValue = 0.0;
            if (!double.IsInfinity((double)values[0]))
            {
                presentValue = System.Convert.ToDouble(values[0]);
            }
            var isSelected = (values.Length == 2) || (bool)values[2];

            var value = (presentValue + parameterValue) * zoomPercentage;
            if (!isSelected)
            {
                return value;
            }

            return (parameterValue < 0) ? value - 1 : value + 1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            var presentValue = DependencyProperty.UnsetValue;

            if (value != DependencyProperty.UnsetValue)
            {
                double value1 = System.Convert.ToDouble(value);
                var parameterValue = System.Convert.ToDouble(parameter);
                presentValue = (value1 / zoomPercentage) - parameterValue;
            }
            System.Diagnostics.Debug.WriteLine("Convert");
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing };
        }
    }

    public class LayoutFieldSaveCaptionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string caption = "Save";

            if (values != null && values.Length == 3)
            {
                Nullable<ElementType> elementType = values[0] != null && values[0] is Nullable<ElementType> ? (Nullable<ElementType>)values[0] : null;
                bool isAddMode = values[1] != null && values[1] is bool ? (bool)values[1] : false;
                bool isDirty = values[2] != null && values[2] is bool ? (bool)values[2] : false;

                if (!isAddMode && isDirty)
                {
                    caption = caption + "*";
                }

                if (isAddMode && elementType != null && elementType.Value == ElementType.Marker)
                {
                    caption = "Define";
                }
            }

            return caption;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValueOptionMultiLineVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            if (values != null && values.Length == 2)
            {
                TableDef tableDef = values[0] as TableDef;
                FieldDef fieldDef = values[1] as FieldDef;

                if (tableDef == null && fieldDef != null)
                //if (fieldDef != null)
                {
                    visibility = Visibility.Visible;
                }
            }

            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FieldVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            if (values != null)
            {
                int length = values.Length;
                if (length > 0)
                {
                    Nullable<ElementType> elementType = values[0] is Nullable<ElementType> ? (Nullable<ElementType>)values[0] : null;
                    TableDef tableDef = null;
                    if (length > 1)
                    {
                        tableDef = values[1] as TableDef;
                    }

                    if (elementType != null)
                    {
                        if (elementType.Value == ElementType.Field || (elementType.Value == ElementType.Table && tableDef != null))
                        {
                            visibility = Visibility.Visible;
                        }
                    }
                }
            }

            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LayoutFieldExpanderVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            if (values != null)
            {
                int length = values.Length;
                if (length > 0)
                {
                    Nullable<ElementType> elementType = values[0] is Nullable<ElementType> ? (Nullable<ElementType>)values[0] : null;
                    if (elementType != null && elementType.Value == ElementType.Marker)
                    {
                        visibility = Visibility.Visible;
                    }
                    FieldDef fieldDef = null;
                    if (length > 1)
                    {
                        fieldDef = values[1] as FieldDef;
                        if (fieldDef != null)
                        {
                            visibility = Visibility.Visible;
                        }
                    }
                }
            }

            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class UpdateModeToCaptionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string caption = string.Empty;
            if (values != null && values.Length > 0 && values[0] is UpdateMode)
            {
                UpdateMode mode = (UpdateMode)values[0];
                if (mode == UpdateMode.Edit)
                {
                    caption = "Save";
                    if (values.Length > 1 && values[1] is bool)
                    {
                        if (((bool)values[1]))
                        {
                            caption = "Save*";
                        }
                    }
                }
                else
                {
                    caption = "Define";
                }
            }

            return caption;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class EmptyStringToVisibilityValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is int && values[1] is bool)
            {
                int length = System.Convert.ToInt32(values[0]);
                bool isFocused = System.Convert.ToBoolean(values[1]);
                if (length > 0 || isFocused)
                {
                    return Visibility.Collapsed;
                }
            }

            return Visibility.Visible;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { };
        }
    }

    public class LayoutFieldsConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //get folder name listing...
            string folder = parameter as string ?? "";
            var folders = folder.Split(',').Select(f => f.Trim()).ToList();
            //...and make sure there are no missing entries
            while (values.Length > folders.Count) folders.Add(String.Empty);

            //this is the collection that gets all top level items
            List<object> items = new List<object>();

            for (int i = 0; i < values.Length; i++)
            {
                //make sure were working with collections from here...
                IEnumerable childs = values[i] as IEnumerable ?? new List<object> { values[i] };

                string folderName = folders[i];
                if (folderName != String.Empty)
                {
                    //create folder item and assign childs

                    folderName = folderName.Replace("Layout", string.Empty);
                    var folderItem = new LayoutItem
                    {
                        Name = folderName,
                        Items = childs,
                        Color = GetColor(GetElementType(folderName))
                    };
                    items.Add(folderItem);
                }
                else
                {
                    //if no folder name was specified, move the item directly to the root item
                    foreach (var child in childs)
                    {
                        items.Add(child);
                    }
                }
            }

            return items;
        }

        public static object GetColor(ElementType elementType)
        {
            switch (elementType)
            {
                case ElementType.Field:
                    return new SolidColorBrush(Color.FromArgb(255, 140, 198, 54));

                case ElementType.Table:
                    return new SolidColorBrush(Color.FromArgb(255, 255, 204, 0));

                case ElementType.Marker:
                    return new SolidColorBrush(Color.FromArgb(255, 247, 58, 58));

                default:
                    return new SolidColorBrush(Color.FromArgb(255, 159, 216, 241));
            }
        }

        public static ElementType GetElementType(string folderName)
        {
            return (ElementType)Enum.Parse(typeof(ElementType), folderName.Substring(0, folderName.Length - 1));
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeColorMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue)
            {
                return ElementType.Field;
            }
            ElementType elementType = (ElementType)values[0];
            return LayoutFieldsConverter.GetColor(elementType);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidationIndicatorMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Brush brush = null;
            bool validationFailed = false;
            bool comparisonFailed = false;
            bool confidenceValidationFailed = false;
            if (values != null && values.Length >= 2)
            {
                FieldDataValidationIssueCodes? fieldDataValidationIssueCodes =
                    values[0] != null &&
                    values[0] is ValidationIssue
                        ? (values[0] as ValidationIssue).IssueCode
                        : (FieldDataValidationIssueCodes?)null;

                ComparisionErrorCode? fieldComparisionErrorCode =
                    values[1] != null &&
                    values[1] is Nullable<ComparisionErrorCode>
                        ? (Nullable<ComparisionErrorCode>)values[1]
                        : null;
                if(fieldComparisionErrorCode != null)
                {
                    comparisonFailed = true;
                }
                else if (fieldDataValidationIssueCodes != null)
                {
                   if(fieldDataValidationIssueCodes == FieldDataValidationIssueCodes.ConfidenceIssue)
                    {
                        confidenceValidationFailed = true;
                    }
                    else
                    {
                        validationFailed = true;
                    }
                }
            }

            if (validationFailed|| comparisonFailed)
            {
                brush = Brushes.Red;
            }
            else if (confidenceValidationFailed)
            {
                brush =new SolidColorBrush(Color.FromArgb(255, 255, 165, 0));
            }
            else
            {
                string parameterValue = parameter != null && parameter is string ? (string)parameter : "Transparent";
                brush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(parameterValue));
            }

            return brush;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MarkerEditDefinationVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length > 1 && values[0] is bool && values[1] is bool)
            {
                bool isInEditMode = System.Convert.ToBoolean(values[0]);
                bool isPanelEnabled = System.Convert.ToBoolean(values[1]);
                if (!isInEditMode && isPanelEnabled)
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LayoutFieldNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string name = string.Empty;
            if (values != null && values.Length == 3)
            {
                ElementType elementType = values[0] is ElementType ? (ElementType)values[0] : ElementType.SystemIdentifiedField;
                string markerName = values[1] as string;
                string fieldDefName = values[2] as string;
                if (elementType == ElementType.Marker)
                {
                    name = markerName;
                }
                else
                {
                    name = fieldDefName;
                }
            }
            return name;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RegionToVisibilityValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var type = (ElementType)values[0];
            var isSelected = System.Convert.ToBoolean(values[1]);
            var bounds = (Rect)values[2];

            var regionBox = parameter as RegionBox;

            var region = values.Length >= 4 ? values[4] as RegionViewModel : null;
            if (regionBox != null)
            {
                if (type == ElementType.Table)
                {
                    Rect valueBounds = new Rect();
                    if (values[3] is Rect)
                    {
                        valueBounds = (Rect)values[3];
                    }
                    if (isSelected)
                    {
                        var scaleFactor = (regionBox.DataContext as CanvasImageViewModel)?.ZoomPercentage ?? 1.0;
                        var grid = (regionBox.Child as Grid);
                        var rectangle = grid?.Children[0] as System.Windows.Shapes.Rectangle;
                        if (rectangle != null)
                        {
                            rectangle.Stroke = (SolidColorBrush)LayoutFieldsConverter.GetColor(type);
                            regionBox.EnableResize = false;
                            scaleFactor = Math.Round(scaleFactor, 2);
                            Canvas.SetLeft(regionBox, getScaledValue(bounds.X, 0, ImageView.RegionLeftOffset, scaleFactor));
                            Canvas.SetTop(regionBox, getScaledValue(bounds.Y, 0, ImageView.RegionTopOffset, scaleFactor));
                            regionBox.Width = getScaledValue(bounds.Width, 0, ImageView.RegionRightOffset, scaleFactor);
                            regionBox.Height = getScaledValue(bounds.Height, 0, ImageView.RegionBottomOffset, scaleFactor);
                            regionBox.Visibility = Visibility.Visible;
                            regionBox.Tag = region;
                            return Visibility.Collapsed;
                        }
                    }
                }
                regionBox.EnableResize = false;
                if (region != null)
                {
                    if (region == (regionBox.Tag as RegionViewModel))
                    {
                        regionBox.Visibility = Visibility.Collapsed;
                    }
                }
            }
            return Visibility.Visible;
        }

        private double getScaledValue(double value, double offset, double padding, double scaleFactor)
        {
            var scaledValue = (value + padding) * scaleFactor;
            return (padding < 0) ? scaledValue - 1 : scaledValue + 1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TextBlockWidthMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var width = System.Convert.ToDouble(values[0]);
            var offset = 0;
            var grid = (values[1] as Grid);
            if (grid != null)
            {
                offset = System.Convert.ToInt32(grid.ColumnDefinitions[1].ActualWidth);
            }
            return width - (offset + 28);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SelectedValueBoundAppearanceConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Object returnObj = null;
            string parameterStr = parameter.ToString();
            if (parameterStr == "Fill")
            {
                returnObj = getFillBrush(values);
            }
            else if (parameterStr == "StrokeDashArray")
            {
                returnObj = getStrokeDashArray(values);
            }
            else if (parameterStr == "Opacity")
            {
                returnObj = getOpacity(values);
            }

            return returnObj;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private void getInputParameters(object[] values, out Nullable<ElementType> elementType, out Nullable<bool> isValueBoundAuto)
        {
            elementType = null;
            isValueBoundAuto = null;
            if (values != null && values.Length > 1 && values[0] != null && values[0] is ElementType && values[1] != null && values[1] is bool)
            {
                elementType = (ElementType)values[0];
                isValueBoundAuto = (bool)values[1];
            }
        }

        private object getFillBrush(object[] values)
        {
            Nullable<ElementType> elementType = null;
            Nullable<bool> isValueBoundAuto = null;
            getInputParameters(values, out elementType, out isValueBoundAuto);
            Brush brush = elementType.HasValue && isValueBoundAuto.HasValue && isValueBoundAuto.Value ?
                            (LayoutFieldsConverter.GetColor(elementType.Value) as SolidColorBrush) : Brushes.Transparent;
            return brush;
        }

        private object getStrokeDashArray(object[] values)
        {
            Nullable<ElementType> elementType = null;
            Nullable<bool> isValueBoundAuto = null;
            getInputParameters(values, out elementType, out isValueBoundAuto);
            DoubleCollection doubleCollection = isValueBoundAuto.HasValue && isValueBoundAuto.Value ?
                            new DoubleCollection() { 4, 0 } : new DoubleCollection() { 4, 1.5 };
            return doubleCollection;
        }

        private object getOpacity(object[] values)
        {
            Nullable<ElementType> elementType = null;
            Nullable<bool> isValueBoundAuto = null;
            getInputParameters(values, out elementType, out isValueBoundAuto);
            double opacity = isValueBoundAuto.HasValue && isValueBoundAuto.Value ? 0.5 : 1;
            return opacity;
        }
    }

    public class SelectedHeaderResizeEnableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var layoutField = value as LayoutField;

            if (layoutField == null)
                return false;

            if (layoutField.UserDefinedRegionType == UserDefinedRegionType.UserDefinedField)
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BinarizationThresholdVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (System.Convert.ToBoolean(values[0]) && System.Convert.ToString(values[1]) == BinarizationTypes.Manual.ToString())
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TrainButtonVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 7)
            {
                bool isInEditMode = values[0] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[0]) : false;
                LayoutTestSetItem testsetItem = values[1] != DependencyProperty.UnsetValue ? values[1] as LayoutTestSetItem : null;

                GenericWarning warning = values[2] != DependencyProperty.UnsetValue ? values[2] as GenericWarning : GenericWarning.NoWarning as GenericWarning;

                bool IsDataExtractedSuccessfully = values[3] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[3]) : true;
                bool IsBenchMarkDataSaved = values[4] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[4]) : true;
                bool IsMappingRequired = values[5] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[5]) : false;
                bool IsScanTheDocument = values[6] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[6]) : false;
                return btnVisibility(isInEditMode, testsetItem, warning, IsBenchMarkDataSaved, IsDataExtractedSuccessfully, IsMappingRequired, IsScanTheDocument);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Visibility btnVisibility(bool isInEditMode,
            LayoutTestSetItem testsetItem,
            GenericWarning warning,
            bool IsBenchMarkDataSaved,
             bool IsDataExtractedSuccessfully,
              bool IsMappingRequired,
              bool IsScanTheDocument)
        {
            if (testsetItem != null)
            {
                //This is test set ites
                if (testsetItem.Warning.HasWarning)
                {
                    if (testsetItem.Warning.WarningType.Equals(WarningType.Layout))
                    {
                        return Visibility.Visible;
                    }
                }
                else if (!IsDataExtractedSuccessfully && IsScanTheDocument)
                {
                    return Visibility.Visible;
                }
            }
            return Visibility.Collapsed;
        }
    }

    public class RunButtonVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 6)
            {
                bool isInEditMode = values[0] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[0]) : false;
                LayoutTestSetItem testsetItem = values[1] != DependencyProperty.UnsetValue ? values[1] as LayoutTestSetItem : null;

                GenericWarning warning = values[2] != DependencyProperty.UnsetValue ? values[2] as GenericWarning : GenericWarning.NoWarning as GenericWarning;

                bool IsDataExtractedSuccessfully = values[3] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[3]) : true;
                bool IsBenchMarkDataSaved = values[4] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[4]) : true;
                bool IsMappingRequired = values[5] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[5]) : false;

                return btnVisibility(isInEditMode, testsetItem, warning, IsBenchMarkDataSaved, IsDataExtractedSuccessfully, IsMappingRequired);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Visibility btnVisibility(bool isInEditMode,
            LayoutTestSetItem testsetItem,
            GenericWarning warning,
            bool IsBenchMarkDataSaved,
             bool IsDataExtractedSuccessfully,
              bool IsMappingRequired)
        {
            if (testsetItem != null)
            {
                if (testsetItem.Warning.HasWarning)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    if (IsBenchMarkDataSaved)
                    {
                        if (isInEditMode)
                        {
                            return Visibility.Collapsed;
                        }
                        else
                        {
                            return Visibility.Visible;
                        }
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                }
            }
            else
            {
                return Visibility.Visible;
            }
        }
    }

    public class SaveButtonVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 6)
            {
                bool isInEditMode = values[0] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[0]) : false;
                LayoutTestSetItem testsetItem = values[1] != DependencyProperty.UnsetValue ? values[1] as LayoutTestSetItem : null;

                GenericWarning warning = values[2] != DependencyProperty.UnsetValue ? values[2] as GenericWarning : GenericWarning.NoWarning as GenericWarning;

                bool IsDataExtractedSuccessfully = values[3] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[3]) : true;
                bool IsBenchMarkDataSaved = values[4] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[4]) : true;
                bool IsMappingRequired = values[5] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[5]) : false;

                return btnVisibility(isInEditMode, testsetItem, warning, IsBenchMarkDataSaved, IsDataExtractedSuccessfully, IsMappingRequired);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Visibility btnVisibility(bool isInEditMode,
           LayoutTestSetItem testsetItem,
           GenericWarning warning,
           bool IsBenchMarkDataSaved,
            bool IsDataExtractedSuccessfully,
             bool IsMappingRequired)
        {
            if (testsetItem != null)
            {
                if (testsetItem.Warning.HasWarning)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    if (!isInEditMode)
                    {
                        return Visibility.Collapsed;
                    }
                    else
                    {
                        return Visibility.Visible;
                    }
                }
            }
            else
            {
                return Visibility.Collapsed;
            }
        }
    }

    public class CancelButtonVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 6)
            {
                bool isInEditMode = values[0] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[0]) : false;
                LayoutTestSetItem testsetItem = values[1] != DependencyProperty.UnsetValue ? values[1] as LayoutTestSetItem : null;

                GenericWarning warning = values[2] != DependencyProperty.UnsetValue ? values[2] as GenericWarning : GenericWarning.NoWarning as GenericWarning;

                bool IsDataExtractedSuccessfully = values[3] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[3]) : true;
                bool IsBenchMarkDataSaved = values[4] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[4]) : true;
                bool IsMappingRequired = values[5] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[5]) : false;

                return btnVisibility(isInEditMode, testsetItem, warning, IsBenchMarkDataSaved, IsDataExtractedSuccessfully, IsMappingRequired);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Visibility btnVisibility(bool isInEditMode,
           LayoutTestSetItem testsetItem,
           GenericWarning warning,
           bool IsBenchMarkDataSaved,
            bool IsDataExtractedSuccessfully,
             bool IsMappingRequired)
        {
            if (testsetItem != null)
            {
                if (testsetItem.Warning.HasWarning || !IsBenchMarkDataSaved)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    if (!isInEditMode)
                    {
                        return Visibility.Collapsed;
                    }
                    else
                    {
                        return Visibility.Visible;
                    }
                }
            }
            else
            {
                return Visibility.Collapsed;
            }
        }
    }

    public class WarningTextBlockVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 7)
            {
                bool isInEditMode = values[0] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[0]) : false;
                LayoutTestSetItem testsetItem = values[1] != DependencyProperty.UnsetValue ? values[1] as LayoutTestSetItem : null;

                GenericWarning warning = values[2] != DependencyProperty.UnsetValue ? values[2] as GenericWarning : GenericWarning.NoWarning as GenericWarning;

                bool IsDataExtractedSuccessfully = values[3] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[3]) : true;
                bool IsBenchMarkDataSaved = values[4] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[4]) : true;
                bool IsMappingRequired = values[5] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[5]) : false;
                bool IsScanTheDocument = values[6] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[6]) : false;

                return btnVisibility(isInEditMode, testsetItem, warning, IsBenchMarkDataSaved, IsDataExtractedSuccessfully, IsMappingRequired, IsScanTheDocument);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Visibility btnVisibility(bool isInEditMode,
           LayoutTestSetItem testsetItem,
           GenericWarning warning,
           bool IsBenchMarkDataSaved,
            bool IsDataExtractedSuccessfully,
             bool IsMappingRequired, bool IsScanTheDocument)
        {
            if (testsetItem != null)
            {
                if (testsetItem.Warning.HasWarning)
                {
                    return Visibility.Visible;
                }
                else
                {
                    if (IsScanTheDocument)
                        return Visibility.Visible;
                }
            }
            else
            {
                return Visibility.Collapsed;
            }

            return Visibility.Collapsed;
        }
    }

    public class EditButtonVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 6)
            {
                bool isInEditMode = values[0] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[0]) : false;
                LayoutTestSetItem testsetItem = values[1] != DependencyProperty.UnsetValue ? values[1] as LayoutTestSetItem : null;

                GenericWarning warning = values[2] != DependencyProperty.UnsetValue ? values[2] as GenericWarning : GenericWarning.NoWarning as GenericWarning;

                bool IsBenchMarkDataSaved = values[3] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[3]) : true;

                bool IsDataExtractedSuccessfully = values[4] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[4]) : true;
                bool IsMappingRequired = values[5] != DependencyProperty.UnsetValue ? System.Convert.ToBoolean(values[5]) : false;

                return btnVisibility(isInEditMode, testsetItem, warning, IsBenchMarkDataSaved, IsDataExtractedSuccessfully, IsMappingRequired);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Visibility btnVisibility(bool isInEditMode,
           LayoutTestSetItem testsetItem,
           GenericWarning warning,
           bool IsBenchMarkDataSaved,
            bool IsDataExtractedSuccessfully,
             bool IsMappingRequired)
        {
            if (testsetItem != null)
            {
                if (testsetItem.Warning.HasWarning)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    if (!isInEditMode)
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                }
            }
            else
            {
                return Visibility.Collapsed;
            }
        }
    }

    public class ValidationIssueToTabStopConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool isTabStop = true;
            if (values != null && values.Length == 2)
            {
                ValidationIssue issue = values[0] as ValidationIssue;
                int totalErrorCount = values[1] is Int32 ? (int)values[1] : 0;
                isTabStop = (totalErrorCount == 0) || (issue != null);
            }

            return isTabStop;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidatorRegionVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool isSelected = false;
            bool showAllMappedFields = false;
            Visibility visibility = Visibility.Collapsed;

            if (values != null && values.Length >= 2)
            {
                isSelected = values[0] != null && values[0] is bool ? (bool)values[0] : false;
                showAllMappedFields = values[1] != null && values[1] is bool ? (bool)values[1] : false;
                if (isSelected || showAllMappedFields)
                {
                    visibility = Visibility.Visible;
                }
            }

            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BluePrintTableDeleteButtonVisibility : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 2
                && values[0] is bool
                && values[1] is TableDef)
            {
                bool elementIsSelected = (bool)values[0];
                if (elementIsSelected)
                {
                    var tableDef = values[1] as TableDef;
                    if (tableDef != null
                    && tableDef.IsSelected
                    && tableDef.Name != Constants.CDC.DEFAULT_TABLE_NAME)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CurrencyVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            return visibility;
            if (values != null)
            {
                int length = values.Length;
                if (length > 0)
                {
                    Nullable<ElementType> elementType = values[0] is Nullable<ElementType> ? (Nullable<ElementType>)values[0] : null;
                    TableDef tableDef = null;
                    FieldDef fieldDef = null;
                    if (length > 1)
                    {
                        tableDef = values[1] as TableDef;
                        fieldDef = values[2] as FieldDef;
                    }
                    if (elementType != null)
                    {
                        if (elementType.Value == ElementType.Table && tableDef != null && fieldDef != null && fieldDef.ValueType == FieldValueType.Number)
                        {
                            visibility = Visibility.Visible;
                        }
                    }
                }
            }

            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}