﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.Model;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class UIEventArgs : EventArgs
    {
        public Layout NewLayout { get; private set; }

        public UIEventArgs(Layout newLayout)
        {
            NewLayout = newLayout;
        }
    }

    public class PreviewEventArgs : EventArgs
    {
        public Layout LayoutToUpdate { get; set; }

        public IqBot IqBot { get; private set; }

        public BenchmarkData BenchmarkData { get; set; }

        public PreviewEventArgs(Layout layoutToUpdate, IqBot iqBot)
        {
            LayoutToUpdate = layoutToUpdate;
            IqBot = iqBot;
        }

        public bool IsLayoutClassifiedSuccessFully { get; set; } = true;
    }


    public class DocumentsSelectionEventArgs<T> : EventArgs
    {
        public IEnumerable<T> SelectedDocuments { get;  set; }

        public DocumentsSelectionEventArgs()
        {

        }
    }
    public enum Operation
    {
        Add,
        Update,
        Remove,
        Refresh,
        Export
    }

    public class TestDocSetEventArgs : EventArgs
    {
        public string LayoutId { get; set; }

        public string TestDocSetId { get; set; }

        public DocSetItem DocSetItem { get; set; }

        public Operation Action { get; set; }

        public bool IsKeepCurrentContents { get; set; }

        public PreviewViewModel UpdatedData { get; set; }

        public IqBot IqBot { get; set; }

        public TestDocSetEventArgs(string layoutId, DocSetItem docSetItem, Operation action)
        {
            LayoutId = layoutId;
            DocSetItem = docSetItem;
            Action = action;
        }

        public TestDocSetEventArgs(string layoutId, DocSetItem docSetItem, Operation action, PreviewViewModel updatedData)
            : this(layoutId, docSetItem, action)
        {
            UpdatedData = updatedData;
        }
    }
}