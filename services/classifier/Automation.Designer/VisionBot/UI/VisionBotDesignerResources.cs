/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.EventArguments;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    public class VisionBotDesignerResources : INotifyPropertyChanged
    {
        #region Variables and Properties

        public BitmapImage Logo { get; set; }
        public IqBotDesignerInfo DesignerInfo { get; set; }
        private CanvasImageViewModel _canvasImageViewModel;

        public HeaderPanelViewModel HeaderPanelViewModel { get; set; }
        public TestItemTypes SelectionType { get; set; }

        public TestInspectionViewModel InspectionViewModel { get; set; }

        private bool _isEditorOn;

        public bool IsEditorOn
        {
            get { return _isEditorOn; }
            set
            {
                _isEditorOn = value;
                IsResultsOn = !_isEditorOn;
                onNotifyPropertyChanged("IsEditorOn");
                onNotifyPropertyChanged("IsResultsOn");
            }
        }

        public bool IsResultsOn { get; set; }

        private string _selectedTestItem;

        public string SelectedTestItem
        {
            get { return _selectedTestItem; }
            set
            {
                if (_selectedTestItem != value)
                {
                    _selectedTestItem = value;
                    onNotifyPropertyChanged("SelectedTestItem");
                    setTestContent();
                }
            }
        }

        private object _testContent;

        public object TestContent
        {
            get { return _testContent; }
            set
            {
                _testContent = value;
                onNotifyPropertyChanged("TestContent");
            }
        }

        public IDictionary<string, TestItemViewModel> TestViewModels { get; private set; }
        public IDictionary<string, SummaryViewModel> SummaryViewModels { get; private set; }

        public ICommand EditorTabSelected { get; set; }
        public ICommand ResultsTabSelected { get; set; }

        public CanvasImageViewModel CanvasImageViewModel
        {
            get { return _canvasImageViewModel; }
            set
            {
                _canvasImageViewModel = value;
                onNotifyPropertyChanged("CanvasImageViewModel");
                _canvasImageViewModel.SetInitializeZoomLevel();
            }
        }

        public IqBot IqBot { get; set; }
        public FieldTreeVM FieldTreeVM { get; private set; }
        public DataModelVM DataModelVM { get; private set; }
        public RightPanelVM RightPanelVM { get; private set; }

        private PreviewDataViewModel _previewDataView;

        public PreviewDataViewModel PreviewDataView
        {
            get
            {
                return _previewDataView;
            }
            set
            {
                _previewDataView = value;
                onNotifyPropertyChanged("PreviewDataView");
            }
        }

        public Layout SelectedLayout { get; set; }

        public bool IsMandatoryFieldDefChanged
        {
            get
            {
                if (RightPanelVM == null)
                {
                    return false;
                }

                return RightPanelVM.IsMandatoryFieldDefChanged;
            }
        }

        private LayoutField _selectedLayoutField;

        public LayoutField SelectedLayoutField
        {
            get { return _selectedLayoutField; }
            set
            {
                _selectedLayoutField = value;
                // onNotifyPropertyChanged("SelectedLayoutField");
            }
        }

        internal LayoutViewModel LayoutViewModel { get; set; }

        public event EventHandler SaveVisionBotHander;

        private bool _layoutVisible = false;

        public bool LayoutVisible
        {
            get { return _layoutVisible; }
            set
            {
                if (_layoutVisible != value)
                {
                    _layoutVisible = value && (IqBot.Layouts.Count > 0);
                    onNotifyPropertyChanged("LayoutVisible");
                }
            }
        }

        private bool _testVisible = false;

        public bool TestVisible
        {
            get { return _testVisible; }
            set
            {
                if (_testVisible != value)
                {
                    _testVisible = value;
                    onNotifyPropertyChanged("TestVisible");
                }
            }
        }

        private string _selectedTabName = string.Empty;

        public string SelectedTabName
        {
            get
            {
                return _selectedTabName;
            }
            set
            {
                if (_selectedTabName != value)
                {
                    _selectedTabName = value;
                    onNotifyPropertyChanged("SelectedTabName");
                }
            }
        }

        private IWarningManager __warningManager;

        private IWarningManager WarningManager
        {
            get
            {
                return __warningManager ?? (__warningManager = new WarningManager());
            }
        }

        internal IMessageProvider MessageProvider { get; set; }

        #endregion Variables and Properties

        public event PropertyChangedEventHandler PropertyChanged;

        private void onNotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public VisionBotDesignerResources(IqBot iqBot, CanvasImageViewModel canvasImageViewModel)
        {
            _canvasImageViewModel = canvasImageViewModel;
            IqBot = iqBot;
            _canvasImageViewModel.SetInitializeZoomLevel();
            HeaderPanelViewModel = new HeaderPanelViewModel();

            FieldTreeVM = new FieldTreeVM();
            FieldTreeVM.SelectionChanged += FieldTreeVM_SelectionChanged;
            FieldTreeVM.Items = IqBot.Layouts;
            DataModelVM = new DataModelVM(IqBot);
            DataModelVM.AddFieldRequested += DataModelVM_AddFieldRequested;
            DataModelVM.AddTableCollumnRequested += DataModelVM_AddTableCollumnRequested;

            DataModelVM.UpdateFieldRequested += DataModelVM_UpdateFieldRequested;
            DataModelVM.UpdateTableRequested += DataModelVM_UpdateTableRequested;
            DataModelVM.UpdateTableCollumnRequested += DataModelVM_UpdateTableCollumnRequested;
            DataModelVM.FieldsOrderChanged += DataModelVM_FieldsOrderChangedRequested;
            DataModelVM.TablesOrderChanged += DataModelVM_TablesOrderChangedRequested;
            DataModelVM.TableColumnsOrderChanged += DataModelVM_TableColumnOrderChangedRequested;
           

            //RightPanelVM = new RightPanelVM(iqBot);
            RightPanelVM = new RightPanelVM(this);
            canvasImageViewModel.InspectionViewModel = RightPanelVM;
            RightPanelVM.UpdateFieldDefRequested += RightPanelVM_UpdateFieldDefRequested;
            RightPanelVM.UpdateTableFieldDefRequested += RightPanelVM_UpdateTableFieldDefRequested;
            RightPanelVM.UpdateTableDefRequested += RightPanelVM_UpdateTableDefRequested;

            RightPanelVM.UpdateLayoutData += RightPanelVM_UpdateLayoutData;
            RightPanelVM.UpdateMarker += RightPanelVM_UpdateMarker;
            RightPanelVM.UpdateTableLayoutField += RightPanelVM_UpdateTableLayoutField;
            RightPanelVM.UpdateLayoutTableRequested += RightPanelVM_UpdateLayoutTableRequested;

            //  RightPanelVM.CancelFieldDefRequested += RightPanelVM_CancelFieldDefRequested;
            RightPanelVM.CancelTableDefRequested += RightPanelVM_CancelTableDefRequested;
            RightPanelVM.CancelTableFieldDefRequested += RightPanelVM_CancelTableFieldDefRequested;

            RightPanelVM.SettingsSaveRequested += RightPanelVM_SaveRequested;

            RightPanelVM.LayoutRescanRequested += RightPanelVM_LayoutRescanRequested;
            RightPanelVM.SelectionStatusChanged += RightPanelVm_SelectionStatusChanged;
            RightPanelVM.AutoValueBoundRequested += RightPanelVM_AutoValueBoundRequested;
            RightPanelVM.TurnValueSelectionOffRequested += RightPanelVM_TurnValueSelectionOffRequested;

            PreviewDataView = new PreviewDataViewModel();

            EditorTabSelected = new RelayCommand(editorTabSelected);
            ResultsTabSelected = new RelayCommand(resultsTabSelected);

            IsEditorOn = true;

            TestContent = new TestResultViewModel();
            TestViewModels = new Dictionary<string, TestItemViewModel>();
            SummaryViewModels = new Dictionary<string, SummaryViewModel>();
        }

        private void DataModelVM_FieldsOrderChangedRequested(Object sender, EventArgs e)
        {
            ArrangeLayoutFields();
            SaveEventArgs saveArgs = new SaveEventArgs(true, true);
            onSaveRequested(saveArgs);
        }

        private void DataModelVM_TablesOrderChangedRequested(Object sender, EventArgs e)
        {
            ArrangeLayoutTables();
            SaveEventArgs saveArgs = new SaveEventArgs(true, true);
            onSaveRequested(saveArgs);
        }

        private void DataModelVM_TableColumnOrderChangedRequested(Object sender, ColumnOrderChangeArgs e)
        {
            ArrangeLayoutTableColums(e.TableDef);
            SaveEventArgs saveArgs = new SaveEventArgs(true, true);
            onSaveRequested(saveArgs);
        }

        private void ArrangeLayoutTables()
        {
            LayoutViewModel.ArrangeLayoutTables(IqBot.DataModel.Tables);
        }

        private void ArrangeLayoutFields()
        {
            LayoutViewModel.ArrangeLayoutFields(IqBot.DataModel.Fields);
        }

        private void ArrangeLayoutTableColums(TableDef table)
        {
            LayoutViewModel.ArrangeLayoutTableColums(table.Id, table.Columns);
        }



        private void RightPanelVm_SelectionStatusChanged(object sender, SelectionEventArgs e)
        {
            CanvasImageViewModel.ImageViewInfo.IsValuePreviewOn = e.IsValuePreviewOn;
            CanvasImageViewModel.ImageViewInfo.IsValueExtractionAllowed = e.IsValueExtractionAllowed;
            if (e.SelectionState == SelectionState.On)
            {
                var layoutFieldViewModel = sender as UpdateLayoutFieldViewModel;
                if (null != layoutFieldViewModel)
                {
                    CanvasImageViewModel.ImageViewInfo.SelectedHeaderField = layoutFieldViewModel.LayoutField;
                }
            }
            else if (e.SelectionState == SelectionState.Cancel)
            {
                ResetValueSelection();
            }
            else
            {
                var layoutFieldViewModel = sender as UpdateLayoutFieldViewModel;
                if (null != layoutFieldViewModel)
                {
                    CanvasImageViewModel.ImageViewInfo.SelectedElementType = layoutFieldViewModel.LayoutField.Type;
                    CanvasImageViewModel.ImageViewInfo.IsSelectedValueBoundAuto = layoutFieldViewModel.LayoutField.IsValueBoundAuto;
                }
            }
        }

        internal void UpdateCanvasSelectedValue(LayoutField layoutField)
        {
            CanvasImageViewModel.ImageViewInfo.IsValuePreviewOn = false;
            CanvasImageViewModel.ImageViewInfo.SelectedElementType = layoutField.Type;
            CanvasImageViewModel.ImageViewInfo.IsSelectedValueBoundAuto = layoutField.IsValueBoundAuto;
            //if (CanvasImageViewModel.ImageViewInfo.SelectedElementType == ElementType.Table)
            //{
            //    CanvasImageViewModel.ImageViewInfo.SelectedValueBounds = Rect.Empty;
            //}
            //else
            {
                CanvasImageViewModel.ImageViewInfo.SelectedValueBounds = CanvasImageViewModel.ImageViewInfo.GetAbsoluteBounds(layoutField);
            }
        }

        internal void ResetValueSelection()
        {
            CanvasImageViewModel.ImageViewInfo.SelectedValueBounds = Rect.Empty;
            CanvasImageViewModel.ImageViewInfo.SelectedHeaderField = null;
            //if (CanvasImageViewModel?.ImageViewInfo?.SelectedField!=null
            //    &&CanvasImageViewModel.ImageViewInfo.SelectedField.Type == ElementType.Table)
            //{
            //    CanvasImageViewModel.ImageViewInfo.SelectedField.Bounds = new Rect();
            //    CanvasImageViewModel.ImageViewInfo.SelectedField.Bounds =
            //        CanvasImageViewModel.ImageViewInfo.SelectedField.OriginalBounds.Add(
            //            CanvasImageViewModel.ImageViewInfo.SelectedField.ValueBounds);
            //}
        }

        private void RightPanelVM_AutoValueBoundRequested(object sender, LayoutFieldValueRequestedEventArgs e)
        {
            try
            {
                VisionBotEngine.Model.Field field = CanvasImageViewModel.GetLayoutFieldValue(e.LayoutField);
                if (field != null)
                {
                    e.ValueField = field;
                    CanvasImageViewModel.ImageViewInfo.SelectedElementType = e.LayoutField.Type;
                    CanvasImageViewModel.ImageViewInfo.IsSelectedValueBoundAuto = e.LayoutField.IsValueBoundAuto;
                    CanvasImageViewModel.ImageViewInfo.SelectedValueBounds = e.ValueField.Bounds.Height > 0 && e.ValueField.Bounds.Width > 0 ?
                         e.ValueField.Bounds.ConvertToRect() : System.Drawing.Rectangle.Empty.ConvertToRect();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesignerResources), nameof(RightPanelVM_AutoValueBoundRequested));
            }
        }

        private void RightPanelVM_TurnValueSelectionOffRequested(object sender, EventArgs e)
        {
            CanvasImageViewModel.ImageViewInfo.TurnValueSelectionOff();
        }

        private void RightPanelVM_SaveRequested(LayoutSettingsViewModel obj)
        {
            onSaveRequested(EventArgs.Empty);
        }

        private void RightPanelVM_LayoutRescanRequested(LayoutSettingsViewModel obj)
        {
            onLayoutRescanRequested(new LayoutSettingsEventArgs(obj));
        }

        private void editorTabSelected(object obj)
        {
            setTestContent();
        }

        private void setTestContent()
        {
            var testSetViewModel = (MessageProvider as TestSetViewModel);
            IsEditorOn = true;
            switch (SelectionType)
            {
                case TestItemTypes.TesSetItem:
                    setTestItemContent();
                    break;

                case TestItemTypes.Layout:
                    testSetViewModel?.UpdateTestSetSelection();
                    RightPanelVM.UpdateTestSetContext(InspectionViewModel);
                    break;

                case TestItemTypes.TestSet:
                    testSetViewModel?.UpdateTestSetSelection();
                    RightPanelVM.UpdateTestSetContext(InspectionViewModel);
                    break;
            }
        }

        private void setSummaryContent(string message)
        {
            if (SummaryViewModels.ContainsKey(SelectedTestItem))
            {
                setMessage(string.Empty);
                TestContent = SummaryViewModels[SelectedTestItem];
            }
            else
            {
                setMessage(message);
            }
            RightPanelVM.ClearInspectionPanel();
        }

        private void setTestItemContent()
        {
            if (!string.IsNullOrEmpty(SelectedTestItem))
            {
                setMessage(string.Empty);
                TestViewModels[SelectedTestItem].BenchmarkData.ArrangePreview(IqBot.DataModel);
                var testResultViewModel = new TestResultViewModel
                {
                    UpperContent = TestViewModels[SelectedTestItem].CanvasImageViewModel,
                    LowerContent = TestViewModels[SelectedTestItem].BenchmarkData
                };
                TestContent = testResultViewModel;
                RightPanelVM.UpdateTestSetContext(TestViewModels[SelectedTestItem].State);
            }
            else
            {
                RightPanelVM.ClearInspectionPanel();
            }
        }

        public string TrainDocumentMessage
        {
            get
            {
                return string.Format(Properties.Resources.TrainDocumentMessage, Properties.Resources.LayoutLabel);
            }
        }

        private void setMessage(string message)
        {
            if (MessageProvider != null)
            {
                MessageProvider.InformationMessage = message;
            }
        }

        private void resultsTabSelected(object obj)
        {
            IsEditorOn = false;
            switch (SelectionType)
            {
                case TestItemTypes.TesSetItem:
                    setResultTab();
                    break;

                case TestItemTypes.Layout:
                    setSummaryContent(string.Format(Properties.Resources.NoResultForLayoutMessage, Properties.Resources.LayoutLabel));
                    break;

                case TestItemTypes.TestSet:
                    setSummaryContent(Properties.Resources.NoResultForTestSetMessage);
                    break;
            }
        }

        private void setResultTab()
        {
            if (!string.IsNullOrEmpty(SelectedTestItem) &&
                TestViewModels.ContainsKey(SelectedTestItem) &&
                null != TestViewModels[SelectedTestItem].TestExecutionData)
            {
                var testResultViewModel = new TestResultViewModel();
                if (TestViewModels[SelectedTestItem].TestDocumentSummary.HasLayoutClassificationError)
                {
                    testResultViewModel.UpperContent = string.Format(Properties.Resources.LayoutClassificationFailed,
                                                                        TestViewModels[SelectedTestItem].TestDocumentSummary.LayoutName,
                                                                        Properties.Resources.LayoutLabel);
                }
                else
                {
                    TestViewModels[SelectedTestItem].TestExecutionData.ArrangePreview(IqBot.DataModel);
                    testResultViewModel.UpperContent = TestViewModels[SelectedTestItem].TestExecutionData;
                }

                if (TestViewModels[SelectedTestItem].BenchmarkData != null)
                {
                    TestViewModels[SelectedTestItem].BenchmarkData.ArrangePreview(IqBot.DataModel);
                }

                testResultViewModel.LowerContent = TestViewModels[SelectedTestItem].BenchmarkData;
                TestContent = testResultViewModel;
                RightPanelVM.UpdateTestDocResultSummaryContext(TestViewModels[SelectedTestItem].TestDocumentSummary);
            }
            else
            {
                RightPanelVM.ClearInspectionPanel();
                setMessage(Properties.Resources.ClickOnRunMessage);
            }
        }

        private void RightPanelVM_CancelTableFieldDefRequested(TableDef arg1, FieldDef arg2)
        {
            if (arg1 != null)
            {
                arg1.IsSelected = true;
                FieldTreeVM.SelectedItem = arg1;
            }
        }

        private void RightPanelVM_CancelTableDefRequested(TableDef obj)
        {
            RightPanelVM.UpdateContext(UpdateMode.Add, null, null);
        }

        //void RightPanelVM_CancelFieldDefRequested(FieldDef obj)
        //{
        //    //if (!((LayoutItem)((FieldTreeVM.Items as ObservableCollection<object>)[0])).IsSelected)
        //    //    ((LayoutItem)((FieldTreeVM.Items as ObservableCollection<object>)[0])).IsSelected = true;
        //    if (!blupr .IsSelected)
        //        ((LayoutItem)((FieldTreeVM.Items as ObservableCollection<object>)[0])).IsSelected = true;
        //    else
        //        RightPanelVM.UpdateContext(UpdateMode.Add, null, null);

        //}

        private void DataModelVM_AddFieldRequested(FieldDef obj)
        {
            FieldTreeVM.SelectedItem = null;
            RightPanelVM.UpdateContext(UpdateMode.Add, null, obj);
        }

        public void RequestAddTable()
        {
            TableDef tableDef = new TableDef();
            tableDef.Id = new IqBotIDGenerationHelper().GetNewTableId(IqBot);
            tableDef.Columns = new ObservableCollection<FieldDef>();
            RightPanelVM.UpdateContext(UpdateMode.Add, null, tableDef);
        }

        private void DataModelVM_AddTableCollumnRequested(TableDef table, FieldDef field)
        {
            FieldTreeVM.SelectedItem = null;
            RightPanelVM.UpdateContext(UpdateMode.Add, table, field);
        }

        private void DataModelVM_UpdateFieldRequested(FieldDef obj)
        {
            if (obj != null)
            {
                obj.IsSelected = true;
                FieldTreeVM.SelectedItem = obj;
            }
            else
                FieldTreeVM.SelectedItem = obj;
        }

        private void DataModelVM_UpdateTableRequested(TableDef obj)
        {
            if (obj != null)
            {
                obj.IsSelected = true;
                FieldTreeVM.SelectedItem = obj;
            }
            else
                FieldTreeVM.SelectedItem = obj;
        }

        private void DataModelVM_UpdateTableCollumnRequested(TableDef table, FieldDef field)
        {
            if (field != null)
            {
                field.IsSelected = true;
                FieldTreeVM.SelectedItem = field;
            }
            else
                FieldTreeVM.SelectedItem = field;
        }

        private void RightPanelVM_UpdateLayoutTableRequested(LayoutTable obj)
        {
            SaveEventArgs saveArgs = new SaveEventArgs(true, false);
            onSaveRequested(saveArgs);
        }

        private void RightPanelVM_UpdateTableFieldDefRequested(TableDef arg1, FieldDef arg2)
        {
            if (!arg1.Columns.Contains(arg2))
            {
                arg1.Columns.Add(arg2);

                string currentId = IqBot.DataModelIdStructure.ColumnId;

                IqBot.DataModelIdStructure.ColumnId = arg2.Id;

                checkForValidId(currentId, arg2.Id);
            }

            SaveEventArgs saveArgs = new SaveEventArgs(true, false);
            onSaveRequested(saveArgs);
            if (!arg2.IsSelected)
            {
                arg2.IsSelected = true;
                FieldTreeVM.SelectedItem = arg2;
            }
        }

        private static void checkForValidId(string currentId, string newId)
        {
            currentId = currentId.Substring(currentId.Length - 3);
            newId = newId.Substring(newId.Length - 3);

            if (Convert.ToInt32(currentId) > Convert.ToInt32(newId))
            {
                VBotLogger.Error(() => string.Format("[RightPanelVM_UpdateTableFieldDefRequested] Current Id {0} and New Id {1}.", currentId, newId));
            }
        }

        private void RightPanelVM_UpdateFieldDefRequested(FieldDef obj)
        {
            if (!IqBot.DataModel.Fields.Contains(obj))
            {
                IqBot.DataModel.Fields.Add(obj);

                string currentId = IqBot.DataModelIdStructure.FieldId;
                IqBot.DataModelIdStructure.FieldId = obj.Id;
                checkForValidId(currentId, obj.Id);
            }

            SaveEventArgs saveArgs = new SaveEventArgs(true, false);
            onSaveRequested(saveArgs);
            if (!obj.IsSelected)
            {
                obj.IsSelected = true;
                FieldTreeVM.SelectedItem = obj;
            }
        }

        private void RightPanelVM_UpdateTableDefRequested(TableDef obj)
        {
            if (!IqBot.DataModel.Tables.Contains(obj))
            {
                IqBot.DataModel.Tables.Add(obj);
                obj.IsSelected = true;
                FieldTreeVM.SelectedItem = obj;

                string currentId = IqBot.DataModelIdStructure.TableDefId;
                IqBot.DataModelIdStructure.TableDefId = obj.Id;

              //  checkForValidId(currentId, obj.Id);
            }

            SaveEventArgs saveArgs = new SaveEventArgs(true, false);
            onSaveRequested(saveArgs);
            //RightPanelVM.UpdateContext(null, null);
        }

        public void UpdateLayoutWarning()
        {
            foreach (var layout in IqBot.Layouts)
            {
                UpdateLayoutWarning(layout);
            }
        }

        public void UpdateLayoutWarning(Layout layout)
        {
            WarningManager.UpdateLayoutWarning(layout, IqBot.DataModel);
        }

        private void RightPanelVM_UpdateTableLayoutField(TableDef arg1, LayoutField arg2)
        {
            //if (arg1 != null && arg2 != null && IqBot.Layouts != null && IqBot.Layouts.Count > 0 && IqBot.Layouts[0].Fields != null)
            //{
            //    //TODO: Decide where to and how to change type.
            //    if (arg2.Type == ElementType.SystemIdentifiedRegion || arg2.Type == ElementType.SystemIdentifiedField)
            //    {
            //        arg2.Type = ElementType.Table;
            //        LayoutTable matchingTable = null;
            //        matchingTable = (from q in IqBot.Layouts[0].Tables where string.Compare(q.Name, arg1.Name, true) == 0 select q).FirstOrDefault();

            //        if (matchingTable == null)
            //        {
            //            matchingTable = new LayoutTable();
            //            matchingTable.Name = arg1.Name;
            //            IqBot.Layouts[0].Tables.Add(matchingTable);
            //        }
            //        matchingTable.Fields.Add(arg2);
            //    }
            //}

            if (arg1 != null && arg2 != null && IqBot.Layouts != null && SelectedLayout != null && IqBot.Layouts.Count > 0 && IqBot.Layouts.Contains(SelectedLayout) && SelectedLayout.LayoutTables.Tables != null)
            {
                //TODO: Decide where to and how to change type.
                if (arg2.Type == ElementType.SystemIdentifiedRegion || arg2.Type == ElementType.SystemIdentifiedField)
                {
                    arg2.Type = ElementType.Table;
                    arg2.Id = new IqBotIDGenerationHelper().GetNewLayoutColumnID(IqBot, SelectedLayout.Id);
                    LayoutTable matchingTable = null;
                    matchingTable = (from q in SelectedLayout.LayoutTables.Tables where string.Compare(q.TableDef.Id, arg1.Id, true) == 0 select q).FirstOrDefault();

                    if (matchingTable == null)
                    {
                        matchingTable = new LayoutTable();
                        matchingTable.Id = new IqBotIDGenerationHelper().GetNewLayoutTableID(IqBot, SelectedLayout.Id);
                        matchingTable.TableDef = arg1;
                        SelectedLayout.LayoutTables.Tables.Add(matchingTable);
                        ArrangeLayoutTables();
                    }
                    matchingTable.Fields.Add(arg2);
                    ArrangeLayoutTableColums(matchingTable.TableDef);
                    AddFieldToImageView(arg2);
                    arg2.IsSelected = true;
                    RightPanelVM.UpdateLayoutFieldContext(SelectedLayout, arg2);
                }
            }
            SaveEventArgs saveArgs = new SaveEventArgs(true, false);
            onSaveRequested(saveArgs);

            //RightPanelVM.UpdateContext(null, null);
        }

        private void RightPanelVM_UpdateMarker(LayoutField obj)
        {
            //if (obj != null && IqBot.Layouts != null && IqBot.Layouts.Count > 0 && IqBot.Layouts[0].Markers != null)
            if (obj != null && IqBot.Layouts != null && SelectedLayout != null && IqBot.Layouts.Count > 0 && IqBot.Layouts.Contains(SelectedLayout) && SelectedLayout.Markers != null)
            {
                //TODO: Decide where to and how to change type.
                if (obj.Type == ElementType.SystemIdentifiedRegion || obj.Type == ElementType.SystemIdentifiedField)
                {
                    obj.Type = ElementType.Marker;
                    obj.Id = new IqBotIDGenerationHelper().GetNewLayoutMarkerID(IqBot, SelectedLayout.Id);
                    SelectedLayout.Markers.Fields.Add(obj);
                    AddFieldToImageView(obj);
                    obj.IsSelected = true;
                    RightPanelVM.UpdateLayoutFieldContext(SelectedLayout, obj);
                }
            }

            onSaveRequested(EventArgs.Empty);
            //RightPanelVM.UpdateContext(null, null);
        }

        public void AddFieldToImageView(IRegion region)
        {
            if (CanvasImageViewModel != null && CanvasImageViewModel.ImageViewInfo != null && CanvasImageViewModel.ImageViewInfo.Fields != null)
                CanvasImageViewModel.ImageViewInfo.Fields.Add(new RegionViewModel(region));
        }

        public void RemoveFieldFromImageView(IRegion obj)
        {
            if (CanvasImageViewModel != null && CanvasImageViewModel.ImageViewInfo != null &&
                CanvasImageViewModel.ImageViewInfo.Fields != null)
            {
                for (int index = 0; index < CanvasImageViewModel.ImageViewInfo.Fields.Count; index++)
                {
                    if (CanvasImageViewModel.ImageViewInfo.Fields[index].Region.Equals(obj))
                    {
                        if (CanvasImageViewModel.ImageViewInfo.Fields[index].Region.IsSelected)
                        {
                            CanvasImageViewModel.ImageViewInfo.SelectedHeaderField = null;
                            CanvasImageViewModel.ImageViewInfo.Fields[index].Region.IsSelected = false;
                        }
                        CanvasImageViewModel.ImageViewInfo.Fields.RemoveAt(index);
                        break;
                    }
                }
            }
        }

        private void RightPanelVM_UpdateLayoutData(LayoutField obj)
        {
            //if (obj != null && IqBot.Layouts != null && IqBot.Layouts.Count > 0 && IqBot.Layouts[0].Fields != null)
            if (obj != null && IqBot.Layouts != null && SelectedLayout != null && IqBot.Layouts.Count > 0 && IqBot.Layouts.Contains(SelectedLayout) && SelectedLayout.LayoutFields != null)
            {
                //TODO: Decide where to and how to change type.
                if (obj.Type == ElementType.SystemIdentifiedRegion || obj.Type == ElementType.SystemIdentifiedField)
                {
                    obj.Type = ElementType.Field;
                    obj.Id = new IqBotIDGenerationHelper().GetNewLayoutFieldID(IqBot, SelectedLayout.Id);
                    SelectedLayout.LayoutFields.Fields.Add(obj);
                    ArrangeLayoutFields();
                    AddFieldToImageView(obj);
                    obj.IsSelected = true;
                    RightPanelVM.UpdateLayoutFieldContext(SelectedLayout, obj);
                }
            }

            SaveEventArgs saveArgs = new SaveEventArgs(true, false);
            onSaveRequested(saveArgs);
        }

        public event EventHandler SaveRequested;

        private void onSaveRequested(EventArgs e)
        {
            UpdateLayoutWarning();

            SaveRequested?.Invoke(this, e);
        }

        public event EventHandler LayoutRescanRequested;

        private void onLayoutRescanRequested(EventArgs e)
        {
            if (LayoutRescanRequested != null)
            {
                LayoutRescanRequested(this, e);
            }
        }

        private void FieldTreeVM_SelectionChanged(object obj)
        {
            if (obj != null)
            {
                if ((obj is LayoutItem && ((LayoutItem)obj).Name == "Fields"))
                {
                    DataModelVM.TablesVisible = false;
                    DataModelVM.Fields = IqBot.DataModel.Fields;
                    DataModelVM.Tables = null;
                    DataModelVM.TableDef = null;
                    DataModelVM.SelectedField = null;
                    RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                }
                else if (obj is LayoutItem && ((LayoutItem)obj).Name == "Tables")
                {
                    DataModelVM.TablesVisible = true;
                    DataModelVM.Fields = null;
                    DataModelVM.Tables = IqBot.DataModel.Tables;
                    DataModelVM.TableDef = null;
                    RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                }
                else if (obj is LayoutItem && ((LayoutItem)obj).Name == "Markers")
                {
                    RightPanelVM.UpdateContext(UpdateMode.Edit, null, null);
                }
                else if (obj is TableDef)
                {
                    DataModelVM.TablesVisible = false;
                    DataModelVM.Fields = ((TableDef)obj).Columns;
                    DataModelVM.Tables = null;
                    DataModelVM.SelectedField = null;
                    DataModelVM.TableDef = ((TableDef)obj);
                    RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                }
                else if (obj is FieldDef)
                {
                    DataModelVM.TablesVisible = false;
                    FieldDef selectedFieldDef = obj as FieldDef;
                    if (IqBot.DataModel.Fields.Contains(selectedFieldDef))
                    {
                        DataModelVM.Fields = IqBot.DataModel.Fields;
                        DataModelVM.Tables = null;
                        DataModelVM.TableDef = null;
                        DataModelVM.SelectedField = selectedFieldDef;
                        RightPanelVM.UpdateContext(UpdateMode.Edit, null, obj);
                    }
                    else
                    {
                        foreach (TableDef table in IqBot.DataModel.Tables)
                        {
                            if (table.Columns.Contains(selectedFieldDef))
                            {
                                DataModelVM.Fields = table.Columns;
                                DataModelVM.Tables = null;
                                DataModelVM.TableDef = table;
                                DataModelVM.SelectedField = selectedFieldDef;
                                RightPanelVM.UpdateContext(UpdateMode.Edit, table, obj);
                                break;
                            }
                        }
                    }
                }
                //else if (obj is LayoutTable)
                //{
                //    RightPanelVM.UpdateLayoutTableContext(SelectedLayout, obj as LayoutTable);
                //}
                else if (obj is Layout)
                {
                    RightPanelVM.UpdateContext(UpdateMode.Edit, null, null);
                }
            }
        }

        public void SaveIqBot(object param)
        {
            SaveVisionBotHander?.Invoke(this, EventArgs.Empty);
        }

        public void TestIqBot(object param)
        {
            MessageBox.Show("Test IQBot");
        }

        public void PreviewIqBot(object param)
        {
            MessageBox.Show("Preview IQBot");
        }

        internal void UpdateLayoutContext(Layout layout)
        {
            RightPanelVM.UpdateLayoutContext(layout);
        }
    }

    public class LayoutSettingsEventArgs : EventArgs
    {
        public LayoutSettingsViewModel layoutSettingsViewModel { get; private set; }

        public LayoutSettingsEventArgs(LayoutSettingsViewModel layoutSetting)
        {
            layoutSettingsViewModel = layoutSetting;
        }
    }

    public class IQBotSettingsEventArgs : EventArgs
    {
        public IQBotSettingsViewModel iqBotSettingsViewModel { get; private set; }

        public IQBotSettingsEventArgs(IQBotSettingsViewModel iqBotSettings)
        {
            iqBotSettingsViewModel = iqBotSettings;
        }
    }
}