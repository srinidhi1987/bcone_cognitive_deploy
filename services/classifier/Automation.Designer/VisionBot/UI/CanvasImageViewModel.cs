/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;

    public class CanvasImageViewModel : INotifyPropertyChanged
    {
        private FrameworkElement _frameworkElement;
        internal const double ZoomStep = 0.1f;

        public event PropertyChangedEventHandler PropertyChanged;

        private ImageViewInfo _imageViewInfo;

        public ImageViewInfo ImageViewInfo
        {
            get { return _imageViewInfo; }
            set
            {
                if (_imageViewInfo != value)
                {
                    ImageViewInfo oldImageViewInfo = _imageViewInfo;

                    _imageViewInfo = value;
                    if (_imageViewInfo != null)
                    {
                        _imageViewInfo.Parent = this;
                    }

                    if (oldImageViewInfo != null)
                    {
                        oldImageViewInfo.Dispose();
                    }
                }
            }
        }

        private double _imageHeight;

        public double ImageHeight
        {
            get { return _imageHeight; }
            set
            {
                if (_imageHeight != value)
                {
                    _imageHeight = value;
                    OnPropertyChanged("ImageHeight");
                }
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private double _imageWidth;

        public double ImageWidth
        {
            get { return _imageWidth; }
            set
            {
                if (_imageWidth != value)
                {
                    _imageWidth = value;
                    OnPropertyChanged("ImageWidth");
                }
            }
        }

        private bool _hasItems;

        public bool HasItems
        {
            get { return _hasItems; }
            set
            {
                if (_hasItems != value)
                {
                    _hasItems = value;
                    OnPropertyChanged("HasItems");
                }
            }
        }

        public string LayoutName { get; set; }

        public double OriginalImageWidth
        {
            get
            {
                if (_imageViewInfo != null)
                {
                    return _imageViewInfo.GetOriginalImageWidth();
                }

                return 0;
            }
        }

        public double OriginalImageHeight
        {
            get
            {
                if (_imageViewInfo != null)
                {
                    return _imageViewInfo.GetOriginalImageHeight();
                }

                return 0;
            }
        }

        private double _zoomPercentage = 1.0f;

        public double ZoomPercentage
        {
            get { return _zoomPercentage; }
            set
            {
                if (_zoomPercentage != value)
                {
                    _zoomPercentage = value;
                    OnPropertyChanged("ZoomPercentage");
                }
            }
        }

        private string documentPath;

        public string DocumentPath
        {
            get
            {
                return documentPath;
            }
            set
            {
                documentPath = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("DocumentPath"));
                }
            }
        }

        private string _zoomLevel;

        public string ZoomLevel
        {
            get { return _zoomLevel; }
            set
            {
                _zoomLevel = value;
                OnPropertyChanged("ZoomLevel");
            }
        }

        public LayoutField AddedField { get; private set; }

        public RelayCommand ZoomInCommand { get; private set; }
        public RelayCommand ZoomResetCommand { get; private set; }
        public RelayCommand ZoomOutCommand { get; private set; }
        public RelayCommand PreviewCommand { get; private set; }
        public ICommand OnRegionCreated { get; private set; }
        public ICommand OnRegionUpdated { get; private set; }
        public RightPanelVM InspectionViewModel { get; set; }

        public event EventHandler Save;

        public event LayoutFieldValueRequestedHandler LayoutFieldValueRequested;

        public event EventHandler<double> ZoomPercentageChanged;

        public CanvasImageViewModel(FrameworkElement frameworkElement)
        {
            _frameworkElement = frameworkElement;
            ZoomInCommand = new RelayCommand(ZoomInIqBot);
            ZoomResetCommand = new RelayCommand(ResetZoomIqBot);
            ZoomOutCommand = new RelayCommand(ZoomOutIqBot);
            PreviewCommand = new RelayCommand(PreviewIQBot);
            OnRegionCreated = new RelayCommand(onRegionCreated);
            OnRegionUpdated = new RelayCommand(onRegionUpdated);
        }

        private void onRegionUpdated(object obj)
        {
            if (obj is Rect && InspectionViewModel != null)
            {
                InspectionViewModel.UpdateTableColumnBounds((Rect)obj);
            }
        }

        private void onRegionCreated(object obj)
        {
            if (obj is Rect)
            {
                if (ImageViewInfo.SelectedHeaderField != null)
                {
                    ImageViewInfo.MapUserDefinedRegion((Rect)obj);
                }
                else
                {
                    AddedField = ImageViewInfo.CreateUserDefinedField((Rect)obj);
                    if (Save != null)
                    {
                        Save(this, EventArgs.Empty);
                    }
                    AddedField = null;
                }
            }
        }

        public void MakeFieldValueBoundSelectionOn()
        {
            InspectionViewModel.MakeFieldValueBoundSelectionOn();
        }

        private void ZoomInIqBot(object obj)
        {
            if (ZoomPercentage <= 2)
            {
                var existsingZoomPercentage = ZoomPercentage;
                if ((ZoomPercentage + ZoomStep) > 1 && (ZoomPercentage < 1))
                {
                    ZoomPercentage = 1;
                }
                else
                {
                    ZoomPercentage += ZoomStep;
                }
                setZoomText();
                setDocumentZoom(existsingZoomPercentage);
            }
        }

        public void ResetZoomIqBot(object param)
        {
            var existsingZoomPercentage = ZoomPercentage;
            SetInitializeZoomLevel();
            setZoomText();
            setDocumentZoom(existsingZoomPercentage);
        }

        private void ZoomOutIqBot(object obj)
        {
            if (ZoomPercentage > 0.1)
            {
                var existsingZoomPercentage = ZoomPercentage;
                ZoomPercentage -= ZoomStep;
                setZoomText();
                setDocumentZoom(existsingZoomPercentage);
            }
        }

        private double getZoomLevel()
        {
            if (OriginalImageWidth != 0 && OriginalImageWidth > _frameworkElement.ActualWidth && _frameworkElement.ActualWidth > 19)
            {
                return Math.Round((_frameworkElement.ActualWidth - 19) / OriginalImageWidth, 2);
            }
            return 1;
        }

        public void SetInitializeZoomLevel()
        {
            double widthDiff = 1;
            if (OriginalImageWidth > _frameworkElement.ActualWidth)
            {
                widthDiff = OriginalImageWidth - _frameworkElement.ActualWidth;
            }
            var zoomLevel = getZoomLevel();
            ZoomPercentage = zoomLevel;
            setZoomText();
            setDocumentZoom(ZoomPercentage);
        }

        public Field GetLayoutFieldValue(LayoutField layoutField)
        {
            LayoutFieldValueRequestedEventArgs args = new LayoutFieldValueRequestedEventArgs(layoutField);
            onLayoutFieldValueRequested(args);
            return args.ValueField;
        }

        private void setDocumentZoom(double oldZoomValue)
        {
            ImageHeight = OriginalImageHeight * ZoomPercentage;
            ImageWidth = OriginalImageWidth * ZoomPercentage;
            if (_imageViewInfo != null)
            {
                _imageViewInfo.SetZoomHeightWidth(ZoomPercentage);
            }
            ZoomPercentageChanged?.Invoke(this, oldZoomValue);
        }

        private void setZoomText()
        {
            ZoomLevel = (Convert.ToInt32(ZoomPercentage * 100) + "%").PadLeft(5, ' ');
        }

        private void PreviewIQBot(object obj)
        {
            // MessageBox.Show("Preview");
        }

        private void onLayoutFieldValueRequested(LayoutFieldValueRequestedEventArgs e)
        {
            if (e.LayoutField != null && LayoutFieldValueRequested != null)
            {
                LayoutFieldValueRequested(this, e);
            }
        }
    }

    public delegate void LayoutFieldValueRequestedHandler(object sender, LayoutFieldValueRequestedEventArgs e);

    public class LayoutFieldValueRequestedEventArgs : EventArgs
    {
        public LayoutField LayoutField { get; private set; }
        public Field ValueField { get; set; }

        public LayoutFieldValueRequestedEventArgs(LayoutField layoutField)
        {
            LayoutField = layoutField;
        }
    }
}