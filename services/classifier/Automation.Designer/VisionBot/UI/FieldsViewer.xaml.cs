﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for EditableFieldBox.xaml
    /// </summary>
    public partial class FieldsViewer : UserControl
    {
        public FieldsViewer()
        {
            InitializeComponent();
        }
        
        public DataTemplate DataItemTemplate
        {
            get { return (DataTemplate)GetValue(DataItemTemplateProperty); }
            set { SetValue(DataItemTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataItemTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataItemTemplateProperty =
            DependencyProperty.Register("DataItemTemplate", typeof(DataTemplate), typeof(FieldsViewer), null);

    
    }
}
