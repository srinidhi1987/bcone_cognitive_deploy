﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System.Collections.Generic;
using System.Linq;

using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    internal class PagewiseRegionDivider
    {
        private int _chunksize = 25;

        public int Chunksize
        {
            get
            {
                return this._chunksize;
            }
        }

        public IList<PageRegions> Divide(VisionBotEngine.Model.DocumentProperties documentProperties, IList<RegionViewModel> regions)
        {
            IList<PageRegions> pagewiseRegions = this.createPageRegionsFromDocProperties(documentProperties);
            if (regions != null && regions.Count > 0)
            {
                for (int i = 0; i < regions.Count; i++)
                {
                    RegionViewModel region = regions[i];
                    dividePagewiseRegions(region, pagewiseRegions);
                }
            }

            return pagewiseRegions;
        }

        private static void dividePagewiseRegions(RegionViewModel region, IList<PageRegions> pagewiseRegions)
        {
            for (int j = 0; j < pagewiseRegions.Count; j++)
            {
                if (region.Bounds.Y < pagewiseRegions[j].PageBottomY || j == (pagewiseRegions.Count - 1))
                {
                    pagewiseRegions[j].Add(region);
                    break;
                }
            }
        }

        private IList<PageRegions> createPageRegionsFromDocProperties(VisionBotEngine.Model.DocumentProperties documentProperties)
        {
            IList<PageRegions> pageRegionsList = new List<PageRegions>(10);

            List<VisionBotEngine.Model.PageProperties> pagePropertiesList = documentProperties.PageProperties;
            if (pagePropertiesList != null && pagePropertiesList.Count > 0)
            {
                pagePropertiesList = this.createPageRegionsFromPageProperties(pageRegionsList, pagePropertiesList);
            }
            else
            {
                PageRegions pageRegions = new PageRegions(0, documentProperties.Height, this._chunksize);                
                pageRegionsList.Add(pageRegions);
            }

            return pageRegionsList;
        }

        private List<VisionBotEngine.Model.PageProperties> createPageRegionsFromPageProperties(IList<PageRegions> pageRegionsList, List<VisionBotEngine.Model.PageProperties> pagePropertiesList)
        {
            int pageBottomY = 0;
            pagePropertiesList = pagePropertiesList.OrderBy(q => q.PageIndex).ToList();
            for (int i = 0; i < pagePropertiesList.Count; i++)
            {
                pageBottomY = pageBottomY + pagePropertiesList[i].Height;
                PageRegions pageRegions = new PageRegions(pagePropertiesList[i].PageIndex, pageBottomY, this._chunksize);
                pageRegionsList.Add(pageRegions);
            }

            return pagePropertiesList;
        }
    }
}
