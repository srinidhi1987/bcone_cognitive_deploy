﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Integrator.CognitiveDataAutomation
{
    using System;

    public class TemplateException : Exception
    {
        public string TemplateName { get; private set; }

        public TemplateException(string templateName)
        {
            this.TemplateName = templateName;
        }
    }

    public class TemplateCheckedOutToOtherUserException : TemplateException
    {
        public string RequestingUser { get; private set; }
        public string CheckedOutToUser { get; private set; }

        public TemplateCheckedOutToOtherUserException(
            string templateName,
            string requestingUser,
            string checkedOutToUser)
            : base(templateName)
        {
            this.RequestingUser = requestingUser;
            this.CheckedOutToUser = checkedOutToUser;
        }
    }

    public class TemplateNotFoundException : TemplateException
    {
        public TemplateNotFoundException(string templateName)
            : base(templateName)
        {

        }
    }
}
