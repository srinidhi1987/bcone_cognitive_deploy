﻿using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;
using System.Collections.Generic;
using VisionBotEngine.Model.Model;

namespace Automation.CognitiveData
{
    public interface IDesignerServiceEndPoints
    {
        VBotData EvaluateAndFillValidationDetails(VBotDocument vBotDocument, Layout layout, VisionBotManifest visionBotManifest, ValidatorAdvanceInfo validatorAdvanceInfo);

        void ExtractVBotData(VisionBotEngine.Model.VisionBot visionBot, ref VBotDocument vBotDocument, VBotEngineSettings vbotEngineSetting);

        VBotDocument GenerateAndExtractVBotData(VisionBotEngine.Model.VisionBot visionBot, DocumentProperties docProps, VBotEngineSettings engineSettings);

        IList<VisionBotManifest> GetAllVisionBotManifests(string parentPath);

        int GetImagePageCount(string filePath);

        int GetPDFPageCount(string filePath);

        Field GetValueField(string visionbotID, FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc, DocumentProperties docProps, VBotEngineSettings engineSettings, ImageProcessingConfig imageConfig);

        VisionBotEngine.Model.VisionBot LoadVisionBot(ClassifiedFolderManifest classifiedFolderManifest);
        bool UnLockVisionBot(VisionBotManifest visionBotManifest);

        void SaveBitmapToImagePath(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings);

        void SaveVisionBot(ClassifiedFolderManifest classifiedFolderManifest, VisionBotManifest visionBotManifest, VisionBotEngine.Model.VisionBot visionBot);

        void UpdateImageProperties(DocumentProperties properties);

        void SaveDocSet(VisionBotManifest visionBotManifest, VBotDocSet docSet);

        void SaveDocSet(string VBotFilePath, VBotDocSet docSet);

        VBotDocSet LoadLayoutDocSet(VisionBotManifest visionBotManifest);

        VBotDocSet LoadDocSet(string VBotFilePath);

        void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName);

        void SaveTestDocSet(VBotDocSet docSet, string VBotFilePath, string layoutName);

        VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName);

        VBotDocSet LoadTestDocSet(string VBotFilePath, string layoutName);

        void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName);

        void ExportData(VBotDocument vBotDocument, ExportConfiguration exportConfiguration);

        VisionBotEngine.Model.VisionBot LoadVisionBot(VisionBotManifest visionBotManifest);

        List<ProjectManifest> GetAllProjectsManifest(string organizationID);

        // List<VisionBotManifest> GetAllVisionBotsManifest(ProjectManifest projectManifest);

        ProjectDetails GetProjectDetails(string organizationID,ProjectManifest projectManifest);

        //  VisionBotEngine.Model.VisionBot CreateVisionBot(ClassifiedFolderManifest classifiedFolderManifest);
        ProjectDetails GetProjectMetaData(string organizationID, string projectId);

        bool DownloadDocuments(string categoryId, string directoryPath);

         FileDetails GetFileDetails(string documentID);

         string DeleteSegmentedDocument(string documentId);
    }
}