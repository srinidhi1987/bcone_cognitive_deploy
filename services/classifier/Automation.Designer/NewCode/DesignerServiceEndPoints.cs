﻿using Automation.CognitiveData.VisionBot;
using Automation.Services.Client;
using Automation.VisionBotEngine;

//using Automation.VisionBotEngine.Export;
//using Automation.VisionBotEngine.Imaging;
using Automation.VisionBotEngine.Model;

//using Automation.VisionBotEngine.PdfEngine;
using Automation.VisionBotEngine.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

//using System.ServiceModel;
using VisionBotEngine.Model.Model;

namespace Automation.CognitiveData
{
    public class DesignerServiceEndPoints : IDesignerServiceEndPoints
    {
        private bool isServiceView = true;
        //    private VisionBotEngineAPI engineAPI = new VisionBotEngineAPI();

        private ClassifiedFolderManifest _classifiedFolderManifest;
        private IVisionbotServiceEndPointConsumer _visionbotServiceEndPoint;
        private IFileServiceEndPointConsumer _fileService;

        public DesignerServiceEndPoints(ClassifiedFolderManifest classifiedFolderManifest)
        {
            _classifiedFolderManifest = classifiedFolderManifest;

            _visionbotServiceEndPoint = new VisionbotServiceEndPointConsumer(_classifiedFolderManifest);
            _fileService = new FileServiceEndPointConsumer(_classifiedFolderManifest.organizationId, _classifiedFolderManifest.projectId);
        }

        public void SaveBitmapToImagePath(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            _visionbotServiceEndPoint.GetImage(documentProperties.Id, pageIndex, imagePath);
        }

        public void ExtractVBotData(Automation.VisionBotEngine.Model.VisionBot visionBot, ref VBotDocument vBotDocument, VBotEngineSettings vbotEngineSetting)
        {
            var vbotDoc = _visionbotServiceEndPoint.ExtractVBotData(visionBot.Id, vBotDocument?.Layout?.Id);
            if (vbotDoc != null)
            {
                vBotDocument = vbotDoc;
            }
        }

        public VBotDocument GenerateAndExtractVBotData(Automation.VisionBotEngine.Model.VisionBot visionBot, DocumentProperties docProps, VBotEngineSettings engineSettings)
        {
            return _visionbotServiceEndPoint.GenerateAndExtractVBotDocument(visionBot.Id, docProps.Id);
        }

        public Field GetValueField(string visionbotID, FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc, DocumentProperties docProps, VBotEngineSettings engineSettings, ImageProcessingConfig imageConfig)
        {
            return _visionbotServiceEndPoint.GetValueField(visionbotID, fieldLayout, fieldDef, vbotDoc, docProps);
        }

        public int GetPDFPageCount(string filePath)
        {
            //IPdfEngine iPdfWrapper = PdfEngineStrategy.GetPdfEngine();
            //return iPdfWrapper.PageCount(filePath);
            return 1;
        }

        public int GetImagePageCount(string filePath)
        {
            //int count = new ImageOperations().GetPageCount(filePath);
            //return count;
            return 1;
        }

        public void UpdateImageProperties(DocumentProperties properties)
        {
            //ImageOperations imgOperations = new ImageOperations();
            //imgOperations.UpdateImageProperties(properties);
        }

        public VBotData EvaluateAndFillValidationDetails(VBotDocument vBotDocument, Layout layout, VisionBotManifest visionBotManifest,ValidatorAdvanceInfo validatorAdvanceInfo)
        {
            var validationDataManager = new VBotValidatorDataManager(_classifiedFolderManifest, visionBotManifest.Id);
            vBotDocument.Data.EvaluateAndFillValidationDetails(layout, validationDataManager, validatorAdvanceInfo);
            return vBotDocument.Data;
        }

        public void SaveVisionBot(ClassifiedFolderManifest classifiedFolderManifest, VisionBotManifest visionBotManifest, VisionBotEngine.Model.VisionBot visionBot)
        {
            _visionbotServiceEndPoint.UpdateVisionBot(classifiedFolderManifest, visionBotManifest, visionBot);
        }

        public VisionBotEngine.Model.VisionBot LoadVisionBot(VisionBotManifest visionBotManifest)
        {
            VBotLogger.Trace(() => "[VisionBotManager::LoadVisionBot] Loading VisionBot ...");

            return _visionbotServiceEndPoint.LoadVisionBot(visionBotManifest);
        }

        public VisionBotEngine.Model.VisionBot LoadVisionBot(ClassifiedFolderManifest classifiedFolderManifest)
        {
            return _visionbotServiceEndPoint.LoadVisionBotForEdit(classifiedFolderManifest);
        }

        public bool UnLockVisionBot(VisionBotManifest visionBotManifest)
        {
            return _visionbotServiceEndPoint.UnLockVisionBot(visionBotManifest.Id);
        }

        public IList<VisionBotManifest> GetAllVisionBotManifests(string parentPath)
        {
            if (isServiceView)
            {
                return new List<VisionBotManifest>();
            }
            else
            {
                // IVBotStorage vBotStorage = new VBotFileStorage();
                //  return vBotStorage.GetAllVisionBots(parentPath);
            }

            return new List<VisionBotManifest>();
        }

        public void SaveDocSet(VisionBotManifest visionBotManifest, VBotDocSet docSet)
        {
            VBotLogger.Trace(() => "[DocSetManager::SaveDocSet] Saving DocSet....");

            if (isServiceView)
            {
            }
            else
            {
                //  IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
                // docSetStorage.Save(docSet);
            }
            VBotLogger.Trace(() => "[DocSetManager::SaveDocSet] DocSet saved successfully.");
        }

        public void SaveDocSet(string VBotFilePath, VBotDocSet docSet)
        {
            if (isServiceView)
            {
            }
            else
            {
                //  IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
                // docSetStorage.Save(docSet);
            }
        }

        public VBotDocSet LoadLayoutDocSet(VisionBotManifest visionBotManifest)
        {
            VBotLogger.Trace(() => "[LoadDocSet] Loading DocSet...");

            if (isServiceView)
            {
                return new VBotDocSet();
            }
            else
            {
                // IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
                //  return docSetStorage.Load();
            }

            return new VBotDocSet();
        }

        public VBotDocSet LoadDocSet(string VBotFilePath)
        {
            if (isServiceView)
            {
                return new VBotDocSet();
            }
            else
            {
                // IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
                //   return docSetStorage.Load();
            }

            return new VBotDocSet();
        }

        public void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName)
        {
            _visionbotServiceEndPoint.SaveTestDocSet(docSet, visionBotManifest, layoutName);
        }

        public void SaveTestDocSet(VBotDocSet docSet, string VBotFilePath, string layoutName)
        {
            if (isServiceView)
            {
            }
            else
            {
                //  IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
                //   docSetStorage.Save(docSet, layoutName);
            }
        }

        public VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            try
            {
                return _visionbotServiceEndPoint.LoadTestDocSet(visionBotManifest, layoutName);
            }
            catch(Exception ex)
            { }
            return new VBotDocSet();
        }

        public VBotDocSet LoadTestDocSet(string VBotFilePath, string layoutName)
        {
            if (isServiceView)
            {
                return new VBotDocSet();
            }
            else
            {
                //   IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
                //    return docSetStorage.Load(layoutName);
            }

            return new VBotDocSet();
        }

        public void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            _visionbotServiceEndPoint.DeleteTestDocSet(visionBotManifest, layoutName);
        }

        public void ExportData(VBotDocument vBotDocument, ExportConfiguration exportConfiguration)
        {
            string exportData = _visionbotServiceEndPoint.ExportData(vBotDocument.Data);
            try
            {
                using (Stream stream = File.OpenWrite(exportConfiguration.ExportFilePath))
                {
                    using (var writer = new StreamWriter(stream, new UTF8Encoding(true)))
                    {
                        writer.Write(exportData);
                    }
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[ExportData] Error creating csv : {ex.ToString()}");
            }
        }

        //Project Service API's
        public List<ProjectManifest> GetAllProjectsManifest(string organizationID)
        {
            IProjectServiceEndPointConsumer projectServiceEndPoint = IocContainer.Instance.Container.GetInstance<IProjectServiceEndPointConsumer>();
            return projectServiceEndPoint.GetAllProjectsManifest(organizationID);
        }

        public ProjectDetails GetProjectDetails(string organizationID, ProjectManifest projectManifest)
        {
            IProjectServiceEndPointConsumer projectServiceEndPoint = IocContainer.Instance.Container.GetInstance<IProjectServiceEndPointConsumer>();
            return projectServiceEndPoint.GetProjectDetails(organizationID, projectManifest.id);
        }

        public ProjectDetails GetProjectMetaData(string organizationID, string  projectId)
        {
            IProjectServiceEndPointConsumer projectServiceEndPoint = IocContainer.Instance.Container.GetInstance<IProjectServiceEndPointConsumer>();
            return projectServiceEndPoint.GetProjectMetaData(organizationID, projectId);
        }

        public void SaveVisionBot(ProjectManifest projectManifest, VisionBotManifest visionBotManifest, VisionBotEngine.Model.VisionBot visionBot)
        {
        }

        public bool DownloadDocuments(string categoryId, string directoryPath)
        {
            return _fileService.DownloadDocuments(categoryId, directoryPath);
        }

        public FileDetails GetFileDetails(string documentID)
        {
            return _fileService.GetDocumentDetails(documentID);
        }

        public string DeleteSegmentedDocument(string documentId)
        {
            return _fileService.DeleteSegmentedDocument(documentId);
        }
    }
}