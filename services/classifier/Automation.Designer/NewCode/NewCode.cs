﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.VisionBot
{
  internal  class NewCode
    {
        public void UpdateData(List<VBotDocItem> VBotDocSetItems ,string id, BenchmarkData benchmarkData, bool isBenchmarkDataValidated, bool isBenchmarkStructureMismatchFound)
        {
            for (int index = 0; index < VBotDocSetItems.Count; index++)
            {
                if (VBotDocSetItems[index].Id.Equals(id))
                {
                    VBotDocSetItems[index].BenchmarkData = benchmarkData;
                    VBotDocSetItems[index].IsBenchMarkDataValidated = isBenchmarkDataValidated;
                    VBotDocSetItems[index].IsBenchmarkStructureMismatchFound = isBenchmarkStructureMismatchFound;
                    break;
                }
            }
        }

        public void UpdateTestSetBenchmarkMismatchStatus(List<VBotDocItem> VBotDocSetItems,string id, bool isBenchmarkStructureMismatchFound)
        {
            foreach (var docSetItem in VBotDocSetItems)
            {
                if (docSetItem.Id.Equals(id))
                {
                    docSetItem.IsBenchmarkStructureMismatchFound = isBenchmarkStructureMismatchFound;
                    break;
                }
            }
        }
    }
}
