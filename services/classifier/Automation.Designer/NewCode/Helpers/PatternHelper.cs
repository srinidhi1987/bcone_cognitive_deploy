﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Text.RegularExpressions;

    internal static class PatternHelper
    {
        public static bool IsMatch(string value, string pattern)
        {
            var sanitizedRegEx = GetSanitizeRegEx(pattern);

            try
            {
                return Regex.IsMatch(value, sanitizedRegEx);
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        private static string GetSanitizeRegEx(string pattern)
        {
            const string StartChar = "^";
            const string EndChar = "$";

            if (!pattern.StartsWith(StartChar))
            {
                pattern = StartChar + pattern;
            }

            if (!pattern.EndsWith(EndChar))
            {
                pattern += EndChar;
            }

            return pattern;
        }
    }
}