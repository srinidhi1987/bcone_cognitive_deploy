/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine;

    internal static class ValidationIssueCodeTypesIdentifier
    {
        const int WarningIssueCodeStartingValue = 1000;

        public static ValidationIssueCodeTypes? GetIssueType(this FieldDataValidationIssueCodes? issueCode)
        {
            return issueCode?.GetIssueType();
        }

        public static ValidationIssueCodeTypes GetIssueType(this FieldDataValidationIssueCodes issueCode)
        {
            // TODO: change this logic to be based on attribute and not on enum value
            return (int)issueCode >= WarningIssueCodeStartingValue ? ValidationIssueCodeTypes.Warning : ValidationIssueCodeTypes.Error;
        }
    }
}