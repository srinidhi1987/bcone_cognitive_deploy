﻿
namespace Automation.Integrator.CognitiveDataAutomation
{
    public interface ICognitiveForm
    {
        int ShowForm();
    }

    public interface ITemplateForms
    {
        void CreateNewTemplate();
        void EditTemplate(string templateName);
        void DeleteTemplate(string templateName);
        void TrainTemplate(string templateName);
    }

    public interface IConfigurationForms
    {
        void ShowListToFieldMappingConfiguration(string templateName);
        void ShowListConfiguration(string templateName);
    }
}
