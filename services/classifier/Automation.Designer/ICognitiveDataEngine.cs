﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{

    using Designer;
    using Designer.NewCode;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IVisionbotDesignerEngine : IChecksCompatibility
    {
        int CreateNewTemplate(string templateName, string dataStoragePath);

        int EditTemplate(string templateName, string dataStoragePath, bool isReadOnlyTemplate);
    }
}