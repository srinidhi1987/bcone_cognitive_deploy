﻿using Automation.CognitiveData;
using Automation.CognitiveData.VisionBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Designer.Integrator
{
    interface ICognitiveDataCommandExecuter
    {
        bool ExecuteDataProcessing(CognitiveData cognitiveData);
    }

    public class VisionBotCommandExecutor : ICognitiveDataCommandExecuter
    {
        public bool ExecuteDataProcessing(CognitiveData cognitiveData)
        {
            return ICognitiveDataEngine.ExecuteDataProcessing(
                cognitiveData.Template,
                cognitiveData.InputFile,
                cognitiveData.OutputFile,
                Constants.CDC.ApplicationDataPath,
                cognitiveData.TaskName,
                cognitiveData.EnableValidator)
                == ReturnCodes.General.Success;
        }

        public int ExecuteDataProcessing(string templateFullPath, string filePath, string csvFilePath, string dataStoragePath, string taskName, bool isValidatorEnabled)
        {
            try
            {
                VBotLogger.Trace(() => String.Format("[ExecuteDataProcessing] Execute {0}  vision bot ", templateFullPath));
                IVisionBotManager visionBotManager = new VisionBotManager();


                var manifest = new VisionBotManifest()
                {
                    Name = Path.GetFileNameWithoutExtension(templateFullPath),
                    ParentPath = Path.GetDirectoryName(templateFullPath)
                };

                VisionBot visionBot = visionBotManager.LoadVisionBot(manifest);

                IQBotDataPathProvider.SetSpecificDataPath(manifest.ParentPath);

                VisionBotExecutor visionBotExecutor = new VisionBotExecutor();
                return visionBotExecutor.ProcessDocument(visionBot, filePath, csvFilePath, taskName, isValidatorEnabled, templateFullPath) ? 1 : 0;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotEngineWrapper), nameof(ExecuteDataProcessing), "Execute " + templateFullPath + "for vision bot for file " + filePath);
            }
            finally
            {
                IQBotDataPathProvider.ResetSpecificDataPath();
            }
            return 0;
        }


    }
}
