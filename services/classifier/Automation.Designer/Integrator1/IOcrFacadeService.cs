﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Integrator.CognitiveDataAutomation
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// The Ocr Service interface.
    /// </summary>
    public interface IOcrFacadeService
    {
        /// <summary>
        /// Gets the templates async.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<CognitiveDataResponse<IEnumerable<string>>> GetTemplatesAsync(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Invokes the scan bot UI.
        /// </summary>
        /// <returns></returns>
        //  CognitiveDataResponse<bool> InvokeScanBot();

        /// <summary>
        /// Processes the document async.
        /// </summary>
        /// <param name="cognitiveData">The cognitive data.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<CognitiveDataResponse<bool>> ProcessDocumentAsync(CognitiveData cognitiveData, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Gets the validation errors for cognitive data.
        /// </summary>
        /// <param name="cognitiveData">The cognitive data.</param>
        /// <returns></returns>
        IEnumerable<string> GetValidationErrorsForCognitiveData(CognitiveData cognitiveData);
    }
}
