﻿/** 
* Copyright (c) 2016 Automation Anywhere. 
* All rights reserved. 
* 
* This software is the proprietary information of Automation Anywhere. 
* You shall use it only in accordance with the terms of the license agreement 
* you entered into with Automation Anywhere. 
*/

using LightInject;

namespace Automation.CognitiveData.VisionBot
{
    internal class IocContainer
    {
        #region Variables

        private static IocContainer instance;
        private ServiceContainer container;
        private static object syncLock = new object();
        #endregion

        #region Constructor

        protected IocContainer()
        {
            container = new ServiceContainer();
            container.RegisterFrom<AssemblyCompositionRoot>();
        }

        #endregion

        #region Properties

        public static IocContainer Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        return instance ?? (instance = new IocContainer());
                    }
                }
                return instance;
            }
        }

        public ServiceContainer Container
        {
            get
            {
                return container;
            }
        }

        #endregion
    }
}
