﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CognitiveDataResponse.cs" company="Automation Anywhere">
//   All rights reserved 2015
// </copyright>
// <summary>
//   Defines the CognitiveDataResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Automation.Integrator.CognitiveDataAutomation
{
    using System;

    /// <summary>
    /// The cognitive data response.
    /// </summary>
    /// <typeparam name="T"> The  Response Type </typeparam>
    public class CognitiveDataResponse<T>
    {
        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        public T Response
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the application exception.
        /// </summary>
        public ApplicationException ApplicationException
        {
            get;
            set;
        }
    }
}
