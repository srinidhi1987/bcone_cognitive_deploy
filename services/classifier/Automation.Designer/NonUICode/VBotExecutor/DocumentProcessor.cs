﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VBotExecutor
{
    using Automation.CognitiveData.VisionBot;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using System;
    using System.IO;
    using VisionBotEngine.Configuration;

    internal class DocumentProcessor
    {
        private IDesignerServiceEndPoints _designerServiceEndPoint;

        public DocumentProcessor(IDesignerServiceEndPoints designerServiceEndPoint)
        {
            _designerServiceEndPoint = designerServiceEndPoint;
        }

        public VBotDocument GetVBotDocument(VisionBot visionBot, DocumentProperties docProperties, VBotEngineSettings engineSettings)
        {
            try
            {
                VBotDocument vBotDocument = getProcessedVBotDocument(visionBot, docProperties, engineSettings);

                VBotLogger.Trace(() => string.Format("[DocumentProcessor -> GetVBotDocument] Document processed. SIR fields count -> {0}", vBotDocument.SirFields.Fields.Count));

                return vBotDocument;
            }
            catch (Exception exception)
            {
                exception.Log(nameof(DocumentProcessor), nameof(GetVBotDocument));
            }
            return new VBotDocument();
        }

        public void ExtractVBotData(VisionBot visionBot, ref VBotDocument vBotDocument)
        {
            VBotLogger.Trace(() => "[DocumentProcessor::ExtractVBotData] Extracting VisionBot data...");

            //IVBotEngine vBotEngine = new VBotEngine(Configurations.Visionbot.OcrEngineType);
            //Automation.VisionBotEngine.Common.VBotSettings.ApplicationPath = RoutineProvider.Routine.GetApplicationPath();
            //vBotEngine.ExtractVBotData(visionBot, vBotDocument, Constants.CDC.OcrEnginePath, GetEngineSettings(false, false));
            _designerServiceEndPoint.ExtractVBotData(visionBot, ref vBotDocument, GetEngineSettings(false, false));
        }

        internal VBotEngineSettings GetEngineSettings(bool isPreProcessed, bool preserveGrayscaledPages)
        {
            VBotEngineSettings engineSettings = new VBotEngineSettings();
            engineSettings.MaxDegreeOfParallelism = Configurations.Visionbot.NoOfParallelExtractionsInEngine;
            engineSettings.IsCheckBoxFeatureEnabled = Configurations.Visionbot.IsCheckBoxRecognitionFeatureEnabled;
            engineSettings.IsAccuracyLevelEnabled = Configurations.Visionbot.IsAccuracyLevelEnabled;
            return engineSettings;
        }

        private VBotDocument getProcessedVBotDocument(VisionBot visionBot, DocumentProperties docProps, VBotEngineSettings engineSettings)
        {
            //IVBotEngine vBotEngine = new VBotEngine(Configurations.Visionbot.OcrEngineType);

            //var visionBotManifest = new VisionBotManifest
            //{
            //    Name = visionBot.Name,
            //    ParentPath = (new IQBotDataPathProvider()).GetPath()
            //};

            //var vbotValidationDataManager = new VBotValidatorDataManager(visionBotManifest.GetFilePath());
            //return vBotEngine.GetVBotDocument(visionBot, docProps, vbotValidationDataManager, engineSettings, visionBot.Settings.GetExtendedVersion());
            return _designerServiceEndPoint.GenerateAndExtractVBotData(visionBot, docProps, engineSettings);
        }
    }
}