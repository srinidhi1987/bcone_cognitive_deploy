﻿
namespace Automation.CognitiveData.VBotTestExecutor
{
    using System.Collections.Generic;
    using System.Data;
    using Automation.VisionBotEngine.Model;

    using DataModel = Automation.VisionBotEngine.Model.DataModel;
    using FieldDef = Automation.VisionBotEngine.Model.FieldDef;
    using FieldValue = Automation.VisionBotEngine.Model.FieldValue;
    using Layout = Automation.VisionBotEngine.Model.Layout;
    using Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence;

    public class BecnchmarkDataConverter
    {
        public DataRecord ExtractFieldValues(DataTable dataTable, DataModel dataModel, Layout layout)
        {
            DataRecord dataRecord = new DataRecord();

            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                FieldDef fieldDef = dataModel.Fields.Find(f => f.Id.Equals(dataTable.Columns[i].ColumnName));

                if (fieldDef == null) continue;

                FieldLayout fieldLayout = layout.Fields.Find(f => f.FieldId.Equals(dataTable.Columns[i].ColumnName));

                FieldValue fieldValue = new TextValue();
                fieldValue.Field = new Field();
                fieldValue.Field.Text = dataTable.Rows[0][i].ToString();
                fieldValue.Field.Confidence = 100;
                if (fieldLayout != null)
                {
                    fieldValue.Field.Bounds = fieldLayout.Bounds;
                    fieldValue.Field.Id = fieldLayout.Id;
                }

                var fieldData = new FieldData() { Field = fieldDef, Value = fieldValue };
                dataRecord.Fields.Add(fieldData);

            }
            return dataRecord;
        }

        public List<TableValue> ExtractTableValues(DataSet dataSet, DataModel dataModel, Layout layout)
        {
            List<TableValue> Tables = new List<TableValue>();

            if (dataModel != null)
            {
                for (int tableIndex = 0; tableIndex < dataSet.Tables.Count; tableIndex++)
                {
                    DataTable dataTable = dataSet.Tables[tableIndex];

                    TableDef tableDef = dataModel.Tables.Find(t => t.Id.Equals(dataTable.TableName));

                    //TableLayout tableLayout = layout.Tables.FirstOrDefault(tablelayout => tablelayout.TableId.Equals(dataTable.TableName));
                    if (tableDef != null)
                    {
                        TableDef TableDef = tableDef.Clone();
                        List<FieldDef> Headers = tableDef.Columns;
                        List<DataRecord> Rows = new List<DataRecord>();
                        List<FieldDef> columnHeaders = new List<FieldDef>();


                        if (dataTable.Columns.Count > 0)
                        {
                            for (int i = 0; i < dataTable.Columns.Count; i++)
                            {
                                var column = Headers.Find(h => h.Id.Equals(dataTable.Columns[i].ColumnName));
                                if (column != null)
                                {
                                    columnHeaders.Add(column);
                                }
                            }
                            TableDef.Columns = columnHeaders;
                        }
                        else
                        {
                            TableDef.Columns = columnHeaders = tableDef.Columns.FindAll(c => !c.IsDeleted);
                        }

                        if (dataTable.Rows.Count > 0)
                        {

                            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
                            {
                                DataRecord dataRecord = new DataRecord();
                                for (int i = 0; i < dataTable.Columns.Count; i++)
                                {
                                    var header = Headers.Find(h => h.Id.Equals(dataTable.Columns[i].ColumnName));
                                    if (header != null)
                                    {
                                        FieldData fieldData = new FieldData();

                                        fieldData.Field = new FieldDef();
                                        fieldData.Field = header;

                                        fieldData.Value = new TextValue();
                                        fieldData.Value.Field.Text = dataTable.Rows[rowIndex][i].ToString();

                                        fieldData?.IgnoreLowConfidenceIssue();
                                        dataRecord.Fields.Add(fieldData);
                                    }
                                }


                                Rows.Add(dataRecord);
                            }
                        }
                        TableValue tableValue = new TableValue() { Headers = columnHeaders, Rows = Rows, TableDef = TableDef };

                        Tables.Add(tableValue);
                    }

                }

            }

            return Tables;
        }
    }
}
