﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows.Input;

namespace Automation.CognitiveData.ConfidenceScore
{
    using Automation.CognitiveData.ConfidenceScore.Validation;
    using Automation.CognitiveData.VisionBot;
    using Automation.CognitiveData.VisionBot.UI;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data;
    using System.Windows;

    internal interface ITestDataEngine
    {
        BenchmarkData GenerateBanchMarkData(bool isAddBlankValue = false);

        BenchmarkData GenerateBanchMarkData(DataTable fieldData, DataSet tableSet);

        BenchmarkData GenerateBenchmarkData(CognitiveData.VisionBot.UI.PreviewViewModel previewViewModel);

        BenchmarkData GenerateValidatedBanchMarkData();
    }

    public class TestDataEngine : ITestDataEngine
    {
        public VBotDocument VBotDocument { get; set; }

        #region BenchmarkData

        public BenchmarkData GenerateBanchMarkData(bool isAddBlankValue = false)
        {
            BenchmarkData banchmarkData = new BenchmarkData()
            {
                Fields = this.GetFields(VBotDocument, isAddBlankValue),
                TableSet = this.GetTables(VBotDocument, isAddBlankValue)
            };

            return banchmarkData;
        }

        private DataTable GetFields(VBotDocument vBotDocument, bool isAddBlankValue = false)
        {
            DataTable fields = new DataTable("Fields");
            for (var i = 0; i < vBotDocument.Data.FieldDataRecord.Fields.Count; i++)
            {
                var fieldData = vBotDocument.Data.FieldDataRecord.Fields[i];
                fields.Columns.Add(fieldData.Field.Id);
                fields.Columns[i].Caption = fieldData.Field.Name;
            }

            DataRow dataRow = fields.NewRow();
            for (var i = 0; i < vBotDocument.Data.FieldDataRecord.Fields.Count; i++)
            {
                var fieldData = vBotDocument.Data.FieldDataRecord.Fields[i];
                dataRow[i] = isAddBlankValue ? string.Empty : fieldData.Value.Field.Text;
            }

            if (fields.Columns.Count > 0)
            {
                fields.Rows.Add(dataRow);
            }

            return fields;
        }

        private DataSet GetTables(VBotDocument vBotDocument, bool isAddBlankValue = false)
        {
            DataSet TableSet = new DataSet("Tables");

            for (int i = 0; i < vBotDocument.Data.TableDataRecord.Count; i++)
            {
                var tableData = vBotDocument.Data.TableDataRecord[i];
                DataTable dataTable = this.convertTableValueToBenchmarkDataTable(tableData, isAddBlankValue);
                dataTable.TableName = tableData.TableDef.Id;
                TableSet.Tables.Add(dataTable);
            }
            TableSet.AcceptChanges();
            return TableSet;
        }

        private DataTable convertTableValueToBenchmarkDataTable(TableValue tableValue, bool isAddBlankValue = false)
        {
            DataTable table = new DataTable("Table");

            for (int i = 0; i < tableValue.Headers.Count; i++)
            {
                var fieldDef = tableValue.Headers[i];
                table.Columns.Add(fieldDef.Id);
                table.Columns[i].Caption = fieldDef.Name;
            }

            foreach (var dataRecord in tableValue.Rows)
            {
                DataRow row = table.NewRow();
                for (int i = 0; i < dataRecord.Fields.Count; i++)
                {
                    var fieldData = dataRecord.Fields[i];
                    row[i] = isAddBlankValue ? string.Empty : (fieldData.Value.Field.Text);
                }

                table.Rows.Add(row);
            }
            return table;
        }

        public BenchmarkData GenerateBanchMarkData(DataTable fieldData, DataSet tableSet)
        {
            BenchmarkData banchmarkData = new BenchmarkData()
            {
                Fields = this.convertToStringValueFields(fieldData),
                TableSet = this.convertToStringValueTableSet(tableSet)
            };

            VBotLogger.Trace(() => "[GenerateBanchMarkData] Generated BenchMarkData successfully");
            return banchmarkData;
        }

        private DataTable convertToStringValueFields(DataTable validateDataTable)
        {
            DataTable dataTable = new DataTable("Table");

            for (int i = 0; i < validateDataTable.Columns.Count; i++)
            {
                dataTable.Columns.Add(validateDataTable.Columns[i].ColumnName, typeof(string));
                dataTable.Columns[validateDataTable.Columns[i].ColumnName].Caption = validateDataTable.Columns[i].Caption;
            }

            for (int i = 0; i < validateDataTable.Rows.Count; i++)
            {
                DataRow dataRow = dataTable.NewRow();
                for (int j = 0; j < validateDataTable.Columns.Count; j++)
                {
                    var cellData = validateDataTable.Rows[i][j] as ValidatedFieldValue;
                    if (cellData != null)
                    {
                        dataRow[j] = cellData.Value != null ? cellData.Value : string.Empty;
                    }
                }

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        private DataSet convertToStringValueTableSet(DataSet validatedTableSet)
        {
            DataSet TableSet = new DataSet("Tables");

            for (int i = 0; i < validatedTableSet.Tables.Count; i++)
            {
                var tableData = validatedTableSet.Tables[i];
                DataTable dataTable = this.convertToStringValueFields(tableData);
                if (tableData.ExtendedProperties.ContainsKey("TableId"))
                {
                    dataTable.TableName = tableData.ExtendedProperties["TableId"].ToString();
                }
                else
                {
                    dataTable.TableName = validatedTableSet.Tables[i].TableName;
                }

                TableSet.Tables.Add(dataTable);
            }
            TableSet.AcceptChanges();
            return TableSet;
        }

        public BenchmarkData GenerateBenchmarkData(CognitiveData.VisionBot.UI.PreviewViewModel previewViewModel)
        {
            BenchmarkData banchmarkData = new BenchmarkData()
            {
                Fields = this.convertToStringValueFields(previewViewModel.PreviewFields),
                TableSet = this.convertToStringValueTableSet(previewViewModel.PreviewTables)
            };

            return banchmarkData;
        }

        private DataTable convertToStringValueFields(ObservableCollection<PreviewFieldModel> PreviewFields)
        {
            DataTable dataTable = new DataTable("Table");

            for (int i = 0; i < PreviewFields.Count; i++)
            {
                dataTable.Columns.Add(PreviewFields[i].FieldDef.Id, typeof(string));
                dataTable.Columns[i].Caption = PreviewFields[i].FieldDef.Name;
            }

            DataRow dataRow = dataTable.NewRow();

            for (int i = 0; i < PreviewFields.Count; i++)
            {
                var cellData = PreviewFields[i].Value as ValidatedFieldValue;
                if (cellData != null)
                {
                    dataRow[i] = cellData.Value != null ? cellData.Value : string.Empty;
                }
            }
            dataTable.Rows.Add(dataRow);

            return dataTable;
        }

        private DataSet convertToStringValueTableSet(ObservableCollection<CognitiveData.VisionBot.UI.Model.PreviewTableModel> PreviewTables)
        {
            DataSet TableSet = new DataSet("Tables");
            for (int i = 0; i < PreviewTables.Count; i++)
            {
                var tableData = PreviewTables[i].DataTable;
                DataTable dataTable = this.convertToStringValueFields(tableData);
                dataTable.TableName = tableData.ExtendedProperties["TableId"].ToString();

                TableSet.Tables.Add(dataTable);
            }
            TableSet.AcceptChanges();
            return TableSet;
        }

        private void updateColumnName(DataTable dataTable)
        {
            for (int columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
            {
                dataTable.Columns[columnIndex].ColumnName = dataTable.Columns[columnIndex].Caption;
            }
        }

        #endregion BenchmarkData

        #region ValidatedBenchmarkData

        public BenchmarkData GenerateValidatedBanchMarkDataEngineModel()
        {
            BenchmarkData banchmarkData = new BenchmarkData()
            {
                Fields = this.GetValidatedFields(VBotDocument),
                TableSet = this.GetValidatedTableSet(VBotDocument)
            };
            VBotLogger.Trace(() => "[GenerateValidatedBanchMarkDataEngineModel] Validated benchmark data generated successfully.");
            return banchmarkData;
        }

        public BenchmarkData GenerateValidatedBanchMarkData()
        {
            BenchmarkData banchmarkData = new BenchmarkData()
            {
                Fields = this.GetValidatedFields(VBotDocument),
                TableSet = this.GetValidatedTableSet(VBotDocument)
            };

            VBotLogger.Trace(() => "[GenerateValidatedBanchMarkData] Validated benchmark data generated successfully.");
            return banchmarkData;
        }

        private DataSet GetValidatedTableSet(VBotDocument vBotDocument)
        {
            DataSet TableSet = new DataSet("Tables");

            for (int i = 0; i < vBotDocument.Data.TableDataRecord.Count; i++)
            {
                var tableData = vBotDocument.Data.TableDataRecord[i];
                DataTable dataTable = this.convertToValidatedDataTable(tableData);
                dataTable.TableName = tableData.TableDef.Id;
                TableSet.Tables.Add(dataTable);
            }
            TableSet.AcceptChanges();
            return TableSet;
        }

        private DataTable GetValidatedFields(VBotDocument vBotDocument)
        {
            DataTable Fields = new DataTable("Fields");

            for (var i = 0; i < vBotDocument.Data.FieldDataRecord.Fields.Count; i++)
            {
                var fieldData = vBotDocument.Data.FieldDataRecord.Fields[i];
                Fields.Columns.Add(fieldData.Field.Id, typeof(ValidatedFieldValue));
                Fields.Columns[i].Caption = fieldData.Field.Name;
            }

            DataRow dataRow = Fields.NewRow();
            for (var i = 0; i < vBotDocument.Data.FieldDataRecord.Fields.Count; i++)
            {
                var fieldData = vBotDocument.Data.FieldDataRecord.Fields[i];

                dataRow[i] = new ValidatedFieldValue()
                {
                    FieldDefId = fieldData.Field.Id,
                    FieldName = fieldData.Field.Name,
                    Value = fieldData.Value.Field.Text,
                    Bounds = new Rect(fieldData.Value.Field.Bounds.X, fieldData.Value.Field.Bounds.Y, fieldData.Value.Field.Bounds.Width, fieldData.Value.Field.Bounds.Height),
                    FieldDataValidationIssue = fieldData.ValidationIssue
                };
            }

            if (Fields.Columns.Count > 0)
                Fields.Rows.Add(dataRow);

            return Fields;
        }

        private DataTable convertToValidatedDataTable(TableValue tableValue)
        {
            DataTable table = new DataTable("Table");

            for (int i = 0; i < tableValue.Headers.Count; i++)
            {
                var fieldDef = tableValue.Headers[i];
                table.Columns.Add(fieldDef.Id, typeof(ValidatedFieldValue));
                table.Columns[i].Caption = fieldDef.Name;
            }

            foreach (var dataRecord in tableValue.Rows)
            {
                DataRow dataRow = table.NewRow();

                for (int i = 0; i < table.Columns.Count; i++) //TODO: Match ID with Column ID
                {
                    if (i < dataRecord.Fields.Count)
                    {
                        FieldData fieldData = dataRecord.Fields[i];
                        dataRow[i] = new ValidatedFieldValue()
                        {
                            FieldDefId = fieldData.Field.Id,
                            FieldName = fieldData.Field.Name,
                            Value = fieldData.Value.Field.Text,
                            Bounds = getRect(fieldData.Value.Field.Bounds),
                            //new Rect(fieldData.Value.Field.Bounds.X, fieldData.Value.Field.Bounds.Y, fieldData.Value.Field.Bounds.Width, fieldData.Value.Field.Bounds.Height),
                            FieldDataValidationIssue = fieldData.ValidationIssue
                        };
                    }
                }

                table.Rows.Add(dataRow);
            }
            return table;
        }

        private Rect getRect(System.Drawing.Rectangle rectangle)
        {
            Rect rect = new Rect();
            if (rectangle.Height >= 0 && rectangle.Width >= 0)
            {
                rect = new Rect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
            }

            return rect;
        }

        public BenchmarkData GenerateValidatedBanchMarkData(BenchmarkData engineBenchmarkData)
        {
            BenchmarkData benchmarkData = new BenchmarkData
            {
                Fields = this.GetValidatedFields(engineBenchmarkData.Fields),
                TableSet = this.GetValidatedTableSet(engineBenchmarkData.TableSet)
            };

            return benchmarkData;
        }

        private DataTable GetValidatedFields(DataTable engineFields)
        {
            DataTable Fields = new DataTable("Fields");

            for (var i = 0; i < engineFields.Columns.Count; i++)
            {
                Fields.Columns.Add(engineFields.Columns[i].ColumnName, typeof(ValidatedFieldValue));
            }

            for (var i = 0; i < engineFields.Rows.Count; i++)
            {
                DataRow dataRow = Fields.NewRow();

                for (var j = 0; j < engineFields.Columns.Count; j++)
                {
                    dataRow[j] = new ValidatedFieldValue()
                    {
                        FieldDefId = engineFields.Columns[j].ColumnName,
                        Value = engineFields.Rows[i][j].ToString()
                    };
                }

                Fields.Rows.Add(dataRow);
            }

            return Fields;
        }

        private DataSet GetValidatedTableSet(DataSet engineTableSet)
        {
            DataSet TableSet = new DataSet("Tables");

            for (int i = 0; i < engineTableSet.Tables.Count; i++)
            {
                var tableData = engineTableSet.Tables[i];
                DataTable dataTable = this.GetValidatedFields(tableData);
                dataTable.TableName = tableData.TableName;
                //dataTable.TableName = tableData.ExtendedProperties["TableId"].ToString();
                TableSet.Tables.Add(dataTable);
            }
            TableSet.AcceptChanges();
            return TableSet;
        }

        #endregion ValidatedBenchmarkData
    }

    public class ValidatedFieldValue : IRegion
    {
        private const bool _showValidationErrorTooltip = true;

        private string _value;

        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public string FieldDefId { get; set; }
        public string FieldName { get; set; }
        public ValidationIssue FieldDataValidationIssue { get; set; }
        public ComparisionErrorCode? FieldComparisionErrorCode { get; set; }

        private Rect _bounds;

        public Rect Bounds
        {
            get
            {
                return _bounds;
            }
            set
            {
                _bounds = value;
            }
        }

        private ElementType _type = ElementType.SystemIdentifiedField;

        public ElementType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public ICommand SetSelection { get; set; }

        public bool IsSelected { get; set; }

        public string Tooltip
        {
            get
            {
                if (_showValidationErrorTooltip)
                {
                    string error = null;
                    if (this.FieldDataValidationIssue != null)
                    {
                        error = ValidationIssueMessageProvider.ToUserFriendlyString(this.FieldDataValidationIssue);
                    }

                    if (this.FieldComparisionErrorCode.HasValue)
                    {
                        error = ErrorCodeHelper.ToUserFriendlyString(this.FieldComparisionErrorCode.Value);
                    }

                    if (!string.IsNullOrEmpty(error))
                    {
                        var msg = string.Format("Error: {0}", error);
                        if (!string.IsNullOrWhiteSpace(this.Value))
                        {
                            msg += Environment.NewLine + this.Value;
                        }

                        return msg;
                    }
                }

                return this.Value;
            }
        }

        //TODO : Should we use memberwise clone?
        public ValidatedFieldValue Clone()
        {
            return new ValidatedFieldValue
            {
                _bounds = this._bounds,
                _type = this._type,
                FieldComparisionErrorCode = this.FieldComparisionErrorCode,
                FieldDataValidationIssue = this.FieldDataValidationIssue,
                FieldDefId = this.FieldDefId,
                FieldName = this.FieldName,
                IsSelected = this.IsSelected,
                Value = this.Value
            };
        }
    }

    public static class ErrorCodeHelper
    {
        public static string ToUserFriendlyString(this ComparisionErrorCode issueCode)
        {
            switch (issueCode)
            {
                case ComparisionErrorCode.ValueNotMatch:
                    return "Value is not equal to the expected result.";

                case ComparisionErrorCode.ValueNotExtracted:
                    return "Unable to digitize the value.";
            }
            return issueCode.ToString();
        }
    }
}