﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.ConfidenceScore.Validation
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using System.Collections.Generic;
    using System.Data;

    public class DataTableComparisionResult
    {
        public DataTable ResultDataTable { get; set; }
        public DataTable BenchmarkDataTable { get; set; }
        public Dictionary<string, FailedObject> FailedFieldsInfo { get; set; }

        public float ConfidenceScore { get; private set; }
        public int FailedFieldsCount { get; private set; }
        public int TotalFieldsCount { get; private set; }

        public DataTableComparisionResult()
        {
            this.ResultDataTable = new DataTable("ResultDataTable");
            this.BenchmarkDataTable = new DataTable("BenchmarkDataTable");
            this.ConfidenceScore = 0;
            this.FailedFieldsCount = this.TotalFieldsCount = 0;
            FailedFieldsInfo = new Dictionary<string, FailedObject>();
        }

        public DataTableComparisionResult(float confidenceScore, int failedFieldsCount, int totalFieldsCount)
        {
            this.ConfidenceScore = confidenceScore;
            this.FailedFieldsCount = failedFieldsCount;
            this.TotalFieldsCount = totalFieldsCount;
        }
    }

    internal class Comparator : IComparator
    {
        public DataTableComparisionResult Compare(DataTable testDataTable, DataTable masterDataTable, ComparisonSettings comparisonSettings = null)
        {
            if (masterDataTable == null || testDataTable == null)
            {
                return new DataTableComparisionResult();
            }
            //Dictionary<string, int> failedFieldsInfo = new Dictionary<string, int>();
            Dictionary<string, FailedObject> failedFieldsInfo = new Dictionary<string, FailedObject>();
            var resultDataTable = this.CompareDataTables(testDataTable, masterDataTable, comparisonSettings);
            int failedFields = GetFailedFieldsCount(resultDataTable, failedFieldsInfo);
            int tolalFields = GetTolalFields(masterDataTable);
            float confidenceScore = GetConfidenceScore(failedFields, tolalFields);
            return new DataTableComparisionResult(confidenceScore, failedFields, tolalFields)
            {
                ResultDataTable = resultDataTable,
                BenchmarkDataTable = masterDataTable,
                FailedFieldsInfo = failedFieldsInfo
            };
        }

        private DataTable CompareDataTables(DataTable testDataTable, DataTable masterDataTable, ComparisonSettings comparisonSettings)
        {
            DataTable resultDataTable = testDataTable.Copy();
            Dictionary<int, int> mappedColumnDictionary = this.mapColumnsWithMasterDataTable(resultDataTable, masterDataTable);

            for (int rowIndex = 0; rowIndex < masterDataTable.Rows.Count; rowIndex++)
            {
                if (rowIndex >= resultDataTable.Rows.Count && resultDataTable.Columns.Count > 0)
                {
                    object[] extractedFieldValues = new object[masterDataTable.Columns.Count];

                    for (int columnIndex = 0; columnIndex < masterDataTable.Columns.Count; columnIndex++)
                    {
                        extractedFieldValues[columnIndex] = new ValidatedFieldValue()
                        {
                            FieldDefId = masterDataTable.Columns[columnIndex].ColumnName,
                            FieldName = masterDataTable.Columns[columnIndex].Caption,
                            FieldComparisionErrorCode = ComparisionErrorCode.ValueNotExtracted,
                            Value = string.Empty
                        };
                    }
                    resultDataTable.Rows.Add(extractedFieldValues);

                    continue;
                }

                for (int columnIndex = 0; columnIndex < masterDataTable.Columns.Count; columnIndex++)
                {
                    int testDataColumnIndex = mappedColumnDictionary[columnIndex];

                    if (testDataColumnIndex == -1)
                    {
                        if (rowIndex == 0)
                        {
                            resultDataTable.Columns.Add(masterDataTable.Columns[columnIndex].ColumnName, typeof(ValidatedFieldValue)).SetOrdinal(columnIndex);
                            mappedColumnDictionary = this.mapColumnsWithMasterDataTable(resultDataTable, masterDataTable);
                        }

                        if (resultDataTable.Rows.Count == rowIndex)
                        {
                            resultDataTable.Rows.Add();
                        }

                        resultDataTable.Rows[rowIndex][masterDataTable.Columns[columnIndex].ColumnName] =
                                new ValidatedFieldValue()
                                {
                                    FieldDefId = masterDataTable.Columns[columnIndex].ColumnName,
                                    FieldName = masterDataTable.Columns[columnIndex].Caption,
                                    FieldComparisionErrorCode =
                                            ComparisionErrorCode.ValueNotExtracted,
                                    Value = string.Empty
                                };

                        continue;
                    }

                    var testDataCellValue = resultDataTable.Rows[rowIndex][testDataColumnIndex] as ValidatedFieldValue;
                    var masterDataCellValue = masterDataTable.Rows[rowIndex][columnIndex].ToString();

                    if (testDataCellValue == null)
                    {
                        testDataCellValue = new ValidatedFieldValue()
                        {
                            FieldDefId = masterDataTable.Columns[columnIndex].ColumnName,
                            FieldName = masterDataTable.Columns[columnIndex].Caption,
                            FieldComparisionErrorCode = ComparisionErrorCode.ValueNotExtracted,
                            Value = string.Empty
                        };
                        resultDataTable.Rows[rowIndex][testDataColumnIndex] = testDataCellValue;
                    }
                    else if (!isCellValueMatch(testDataCellValue.Value, masterDataCellValue, comparisonSettings))
                    {
                        testDataCellValue.FieldName = masterDataTable.Columns[columnIndex].Caption;
                        testDataCellValue.FieldComparisionErrorCode = ComparisionErrorCode.ValueNotMatch;
                        resultDataTable.Rows[rowIndex][testDataColumnIndex] = testDataCellValue;
                    }
                }
            }
            return resultDataTable;
        }

        private static int GetTolalFields(DataTable masterDataTable)
        {
            return masterDataTable.Rows.Count * masterDataTable.Columns.Count;
        }

        private static bool isCellValueMatch(string testDataCellValue, string masterDataCellValue, ComparisonSettings comparisonSettings)
        {
            return testDataCellValue.Equals(masterDataCellValue);
        }

        private Dictionary<int, int> mapColumnsWithMasterDataTable(DataTable testDataTable, DataTable masterDataTable)
        {
            Dictionary<int, int> mappedColumnDictionary = new Dictionary<int, int>();

            for (int columnIndex = 0; columnIndex < masterDataTable.Columns.Count; columnIndex++)
            {
                int testDataColumnIndex = this.getMasterDataColumnIndex(testDataTable, masterDataTable.Columns[columnIndex].ColumnName);

                mappedColumnDictionary.Add(columnIndex, testDataColumnIndex);
            }

            return mappedColumnDictionary;
        }

        private int getMasterDataColumnIndex(DataTable testDataTable, string headerValue)
        {
            for (int columnIndex = 0; columnIndex < testDataTable.Columns.Count; columnIndex++)
            {
                if (testDataTable.Columns[columnIndex].ColumnName.Trim().Equals(headerValue.Trim()))
                {
                    return columnIndex;
                }
            }

            return -1;
        }

        private float GetConfidenceScore(float failedFields, float tolalFields)
        {
            return Operations.GetPercentage((tolalFields - failedFields), tolalFields);
        }

        private int GetFailedFieldsCount(DataTable resultDataTable, Dictionary<string, FailedObject> failedFieldsDictionary)
        {
            int failedFields = 0;
            foreach (DataRow row in resultDataTable.Rows)
            {
                for (var columnIndex = 0; columnIndex < row.ItemArray.Length; columnIndex++)
                {
                    var fieldValue = row[columnIndex] as ValidatedFieldValue;

                    if (fieldValue != null && (fieldValue.FieldDataValidationIssue != null || fieldValue.FieldComparisionErrorCode != null))
                    {
                        failedFields++;
                        if (failedFieldsDictionary.ContainsKey(resultDataTable.Columns[columnIndex].ColumnName))
                        {
                            FailedObject failedField = failedFieldsDictionary[resultDataTable.Columns[columnIndex].ColumnName];
                            failedField.ErrorCount++;
                        }
                        else
                        {
                            failedFieldsDictionary.Add(resultDataTable.Columns[columnIndex].ColumnName, new FailedObject()
                            {
                                Id = resultDataTable.Columns[columnIndex].ColumnName,
                                Name = fieldValue.FieldName,
                                ErrorCount = 1
                            });
                        }
                    }
                }
            }

            return failedFields;
        }
    }
}