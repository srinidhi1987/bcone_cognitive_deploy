﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.ConfidenceScore.Validation
{
    public enum ComparisionErrorCode
    {
        ValueNotMatch = 0,
        ValueNotExtracted = 1
    }

    public class ComparisonError
    {
        public ComparisionErrorCode Error { get; private set; }
        public int RowIndex { get; private set; }
        public int ColumnIndex { get; private set; }
        public string FieldName { get; private set; }

        public ComparisonError()
        {
            this.RowIndex = this.ColumnIndex = -1;
        }

        public ComparisonError(ComparisionErrorCode errorType)
            : this()
        {
            this.Error = errorType;
        }

        public ComparisonError(int rowIndex, int columnIndex, ComparisionErrorCode errorType)
        {
            this.RowIndex = rowIndex;
            this.ColumnIndex = columnIndex;
            this.Error = errorType;
        }

        public ComparisonError(int rowIndex, int columnIndex, string fieldName, ComparisionErrorCode errorType)
            : this(rowIndex, columnIndex, errorType)
        {
            this.FieldName = fieldName;
        }
    }
}
