﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;

namespace Automation.CognitiveData.ConfidenceScore
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Diagnostics;
    using System.Linq;

    using Automation.CognitiveData.ConfidenceScore.Validation;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;

    public class TestDocItemResult
    {
        public string TestDocItemId { get; private set; }
        public string TestName { get; set; }
        public float ConfidenceScore { get; set; }
        public int FailedFieldsCount { get; set; }
        public int TotalFieldsCount { get; set; }
        public TimeSpan ExecutionTime { get; set; }

        public Dictionary<string, FailedObject> FailedFieldsInfo { get; set; }
        public LayoutClassificationValidation LayoutClassificationError { get; set; }
        public DataTableComparisionResult FieldComparisionResult { get; set; }
        public List<DataTableComparisionResult> TableComparisionResults { get; set; }

        public TestDocItemResult(string testDocItemId)
        {
            TestDocItemId = testDocItemId;
            ConfidenceScore = 0;
            FailedFieldsCount = TotalFieldsCount = 0;
            FieldComparisionResult = new DataTableComparisionResult();
            TableComparisionResults = new List<DataTableComparisionResult>();
            LayoutClassificationError = new LayoutClassificationValidation(true);
            FailedFieldsInfo = new Dictionary<string, FailedObject>();
        }
    }

    public class LayoutTestResult
    {
        public string TestDocSetId { get; private set; }
        public string LayoutId { get; set; }
        public string LayoutName { get; set; }
        public List<ExcludedTestInfo> ExcludedTests { get; set; }
        public int FailedTestCount { get; set; }
        public int TotalTestCount { get; set; }
        public TimeSpan ExecutionTime { get; set; }
        public float ConfidenceScore { get; set; }
        public int FailedFieldsCount { get; set; }
        public int TotalFieldsCount { get; set; }

        public List<TestDocItemResult> TestDocItemResults { get; set; }
        public LayoutTestResult(string layoutId, string testDocSetId)
        {
            LayoutId = layoutId;
            TestDocSetId = testDocSetId;
            TestDocItemResults = new List<TestDocItemResult>();
            ExcludedTests = new List<ExcludedTestInfo>();
        }
    }

    public class TestDocItemResultManager
    {
        private DataTableComparisionResult _fieldComparisionResult;
        private List<DataTableComparisionResult> _tableComparisionResults;
        public string TestDocItemId { get; set; }
        public LayoutClassificationValidation LayoutClassificationError { get; set; }

        public TestDocItemResultManager(string testDocItemId)
        {
            TestDocItemId = testDocItemId;
        }

        public TestDocItemResultManager(string testDocItemId, DataTableComparisionResult fieldComparisionResult, List<DataTableComparisionResult> tableComparisionResults)
        {
            TestDocItemId = testDocItemId;
            _fieldComparisionResult = fieldComparisionResult;
            _tableComparisionResults = tableComparisionResults;
        }

        public TestDocItemResult GetTestDocSetResult()
        {
            return new TestDocItemResult(TestDocItemId)
            {
                FailedFieldsCount = this.GetFailedFieldCount(),
                TotalFieldsCount = this.GetTotalFieldCount(),
                ConfidenceScore = this.GetConfidenceScore(),
                FieldComparisionResult = this._fieldComparisionResult,
                TableComparisionResults = this._tableComparisionResults,
                FailedFieldsInfo = this.getFailedFieldInfoDictionary(),
                LayoutClassificationError = LayoutClassificationError == null ? new LayoutClassificationValidation(true) : LayoutClassificationError
            };
        }

        private int GetFailedFieldCount()
        {
            return this._fieldComparisionResult.FailedFieldsCount +
                   this._tableComparisionResults.Sum(tableComparisionResult => tableComparisionResult.FailedFieldsCount);
        }

        private int GetTotalFieldCount()
        {
            return this._fieldComparisionResult.TotalFieldsCount +
                this._tableComparisionResults.Sum(tableComparisionResult => tableComparisionResult.TotalFieldsCount);
        }

        private float GetConfidenceScore()
        {
            int totalFieldsCount = this.GetTotalFieldCount();
            int failedFieldsCount = this.GetFailedFieldCount();
            return (float)Math.Round(Operations.GetPercentage(totalFieldsCount - failedFieldsCount, totalFieldsCount), 2);
        }

        private Dictionary<string, FailedObject> getFailedFieldInfoDictionary()
        {
            Dictionary<string, FailedObject> failedFieldInfo = new Dictionary<string, FailedObject>(_fieldComparisionResult.FailedFieldsInfo);

            foreach (var tableComparisionResult in _tableComparisionResults)
            {
                foreach (KeyValuePair<string, FailedObject> failedFieldKeyValue in tableComparisionResult.FailedFieldsInfo)
                {
                    failedFieldInfo.Add(failedFieldKeyValue.Key, failedFieldKeyValue.Value);
                }
            }
            return failedFieldInfo;
        }

    }

    public class TestLayoutResultManager
    {
        public List<TestDocItemResult> TestDocItemResults { get; set; }
        public List<ExcludedTestInfo> ExcludedTests { get; set; }
        public string LayoutName { get; set; }
        private string _docSetId;

        private Stopwatch _stopwatch;

        private string _layoutId;

        public TestLayoutResultManager(string layoutId, string docSetId)
        {
            _layoutId = layoutId;
            _docSetId = docSetId;
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
            TestDocItemResults = new List<TestDocItemResult>();
            ExcludedTests = new List<ExcludedTestInfo>();
        }

        public LayoutTestResult GetTestDocSetResult()
        {
            return new LayoutTestResult(_layoutId, _docSetId)
                  {
                      LayoutName = LayoutName,
                      TestDocItemResults = TestDocItemResults,
                      ExcludedTests = ExcludedTests,
                      FailedFieldsCount = this.getFailedFieldCount(),
                      TotalFieldsCount = this.getTotalFieldCount(),
                      FailedTestCount = this.getFailedTestCount(),
                      TotalTestCount = this.getTotalTestCount(),
                      ConfidenceScore = this.getConfidenceScore(),
                      ExecutionTime = this.getExecutionTime()
                  };
        }

        private TimeSpan getExecutionTime()
        {
            _stopwatch.Stop();
            return _stopwatch.Elapsed;
        }

        private int getFailedFieldCount()
        {
            return this.TestDocItemResults.Sum(testDocItemResult => testDocItemResult.FailedFieldsCount);
        }

        private int getTotalFieldCount()
        {
            return this.TestDocItemResults.Sum(testDocItemResult => testDocItemResult.TotalFieldsCount);
        }

        private float getConfidenceScore()
        {
            float confidenceScore = this.TestDocItemResults.Sum(testDocItemResult => testDocItemResult.ConfidenceScore);

            return (float)Math.Round(confidenceScore / TestDocItemResults.Count, 2);
        }

        private int getFailedTestCount()
        {
            return this.TestDocItemResults.Count(testDocItemResult => testDocItemResult.FailedFieldsCount > 0 || 
                (testDocItemResult.LayoutClassificationError != null && !testDocItemResult.LayoutClassificationError.IsSameLayoutClassified));
        }

        private int getTotalTestCount()
        {
            return TestDocItemResults.Count;
        }

    }

    public class LayoutClassificationValidation
    {
        public bool IsSameLayoutClassified { get; set; }
        public Layout ClassifiedLayout { get; private set; }

        public LayoutClassificationValidation(bool isSameLayoutFound)
        {
            IsSameLayoutClassified = isSameLayoutFound;

        }

        public LayoutClassificationValidation(bool isSameLayoutFound, Layout classifiedLayout)
        {
            IsSameLayoutClassified = isSameLayoutFound;
            ClassifiedLayout = classifiedLayout;

        }
    }

    public enum TestExcludeErrorCode
    {
        BenchmarkDataNotValidated
    }

    public class ExcludedTestInfo
    {
        public string DocItemId;

        public TestExcludeErrorCode ErrorCode;
    }
}
