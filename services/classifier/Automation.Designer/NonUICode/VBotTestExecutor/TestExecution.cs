﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.ConfidenceScore
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using Automation.CognitiveData.ConfidenceScore.Validation;
    using Automation.CognitiveData.VBotExecutor;
    using Automation.VisionBotEngine.Model;
    using System.IO;
    using System.Diagnostics;
    using System.Linq;

    using Automation.VisionBotEngine;
    using VisionBotEngine.Configuration;

    internal interface ITestExecution
    {
        TestDocItemResult RunTest(VisionBot vbot, VBotDocItem testDocItem, string docSetId);
    }

    public class TestExecution : ITestExecution
    {
        private IDesignerServiceEndPoints _designerServiceEndPoint;

        public TestExecution(IDesignerServiceEndPoints designerServiceEndPoint)
        {
            _designerServiceEndPoint = designerServiceEndPoint;
        }

        public TestDocItemResult RunTest(VisionBot vbot, VBotDocItem testDocItem, string docSetId)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            VBotLogger.Trace(() => "[RunTest] Run Test Started...");

            var layoutClassificationError = new LayoutClassificationValidation(true);

            string docPath = testDocItem.DocumentProperties.Path;

            //var newFilePath = GetPathBasedOnDocType(testDocItem.DocumentProperties.Type, docPath);

            VBotLogger.Trace(() => string.Format("[RunTest] New file path {0}", docPath));

            var fieldsComparisionResult = new DataTableComparisionResult();
            var tableComparisionResults = new List<DataTableComparisionResult>();

            // if (!string.IsNullOrWhiteSpace(newFilePath) && File.Exists(newFilePath))
            {
                VBotLogger.Trace(() => string.Format("[RunTest] File '{0}' is exists", docPath));

                bool isPreProcessed = testDocItem.DocumentProperties.Version >= Convert.ToDouble(Constants.CDC.Version_1_0)
                                        ? false
                                        : testDocItem.DocumentProperties.Type == DocType.Image && !((_designerServiceEndPoint.GetImagePageCount(testDocItem.DocumentProperties.Path) > 1));

                DocumentProcessor documentProcessor = new DocumentProcessor(_designerServiceEndPoint);
                VBotEngineSettings engineSettings = documentProcessor.GetEngineSettings(isPreProcessed, false);
                VBotDocument vBotDocument = documentProcessor.GetVBotDocument(vbot, testDocItem.DocumentProperties, engineSettings);

                VBotLogger.Trace(() => "[RunTest] Got the vBotDocument");

                BenchmarkData benchmarkData = testDocItem.BenchmarkData;

                if (string.IsNullOrWhiteSpace(vBotDocument.Layout.Name) || string.IsNullOrEmpty(vBotDocument.Layout.Id) ||
                   (!string.IsNullOrWhiteSpace(vBotDocument.Layout.Id) && !docSetId.Equals(vBotDocument.Layout.Id)))
                {
                    VBotLogger.Trace(() => "[RunTest] Layout classification failed");

                    if (!string.IsNullOrWhiteSpace(vBotDocument.Layout.Name))
                    {
                        VBotLogger.Trace(() => string.Format("[RunTest] Layout classification failed. Classified layout -> '{0}'", vBotDocument.Layout.Name));
                    }

                    layoutClassificationError = new LayoutClassificationValidation(false, vBotDocument.Layout);
                    fieldsComparisionResult = new DataTableComparisionResult() { BenchmarkDataTable = benchmarkData.Fields.Copy() };
                    for (int i = 0; i < benchmarkData.TableSet.Tables.Count; i++)
                    {
                        DataTable masterDataTable = benchmarkData.TableSet.Tables[i].Copy();
                        updateTablesMasterDataBasedOnBlueprint(masterDataTable, vbot.DataModel);
                        tableComparisionResults.Add(new DataTableComparisionResult() { BenchmarkDataTable = masterDataTable });
                    }
                }
                else
                {
                    VBotLogger.Trace(() => "[RunTest] Layout classification Successfull.");

                    BenchmarkData testData = GetTestData(vBotDocument);

                    VBotLogger.Trace(() => "[RunTest] Got Test Data.");

                    fieldsComparisionResult = CompareFields(vbot.DataModel, benchmarkData, testData);

                    VBotLogger.Trace(() => "[RunTest] Field comparision done.");

                    tableComparisionResults = compareTables(vbot.DataModel, benchmarkData, testData);

                    VBotLogger.Trace(() => "[RunTest] Table comparision done.");
                }
            }

            var testDocItemResultManager = new TestDocItemResultManager(testDocItem.Id, fieldsComparisionResult, tableComparisionResults)
            {
                TestDocItemId = testDocItem.Id,
                LayoutClassificationError = layoutClassificationError
            };

            TestDocItemResult testDocItemResult = testDocItemResultManager.GetTestDocSetResult();
            stopwatch.Stop();
            testDocItemResult.ExecutionTime = stopwatch.Elapsed;
            return testDocItemResult;
        }

        private void updateTablesMasterDataBasedOnBlueprint(DataTable masterDataTable, DataModel dataModel)
        {
            TableDef tableDef = dataModel.Tables.Find(t => t.Id.Equals(masterDataTable.TableName));
            if (tableDef != null)
            {
                for (int columnIndex = 0; columnIndex < masterDataTable.Columns.Count; columnIndex++)
                {
                    var columnDef =
                        tableDef.Columns.Find(c => c.Id.Equals(masterDataTable.Columns[columnIndex].ColumnName));

                    if (columnDef != null)
                    {
                        masterDataTable.Columns[columnIndex].Caption = columnDef.Name;
                    }
                    else
                    {
                        masterDataTable.Columns.RemoveAt(columnIndex);
                        columnIndex--;
                    }
                }
            }
        }

        private void updateFieldsMasterDataBasedOnBlueprint(DataTable masterDataTable, DataModel dataModel)
        {
            for (int columnIndex = 0; columnIndex < masterDataTable.Columns.Count; columnIndex++)
            {
                FieldDef fieldDef = dataModel.Fields.Find(f => f.Id.Equals(masterDataTable.Columns[columnIndex].ColumnName));

                if (fieldDef != null)
                {
                    masterDataTable.Columns[columnIndex].Caption = fieldDef.Name;
                }
                else
                {
                    masterDataTable.Columns.RemoveAt(columnIndex);
                    columnIndex--;
                }
            }
        }

        private static BenchmarkData GetTestData(VBotDocument vBotDocument)
        {
            var testDataEngine = new TestDataEngine() { VBotDocument = vBotDocument };
            BenchmarkData testData = testDataEngine.GenerateValidatedBanchMarkDataEngineModel();
            return testData;
        }

        private List<DataTableComparisionResult> compareTables(DataModel dataModel, BenchmarkData benchmarkData, BenchmarkData testData)
        {
            IComparator dataTableComparator = new Comparator();

            var tableComparisionResults = new List<DataTableComparisionResult>();

            for (int i = 0; i < benchmarkData.TableSet.Tables.Count; i++)
            {
                DataTable masterDataTable = benchmarkData.TableSet.Tables[i].Copy();
                updateTablesMasterDataBasedOnBlueprint(masterDataTable, dataModel);

                DataTable testDataTable = testData.TableSet.Tables.Cast<DataTable>().FirstOrDefault(table => table.TableName.Equals(masterDataTable.TableName));

                if (testDataTable != null)
                    tableComparisionResults.Add(dataTableComparator.Compare(testDataTable, masterDataTable, new ComparisonSettings()));
            }

            return tableComparisionResults;
        }

        private DataTableComparisionResult CompareFields(DataModel dataModel, BenchmarkData benchmarkData, BenchmarkData testData)
        {
            IComparator dataTableComparator = new Comparator();
            DataTable masterDataTable = benchmarkData.Fields.Copy();
            updateFieldsMasterDataBasedOnBlueprint(masterDataTable, dataModel);
            DataTableComparisionResult fieldsComparisionResult = dataTableComparator.Compare(testData.Fields, masterDataTable, new ComparisonSettings());
            return fieldsComparisionResult;
        }
    }
}