﻿using Automation.Integrator.CognitiveDataAutomation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Designer.NewCode
{
    public interface ICognitiveDesignerExecutor
    {
        bool CreateNewTemplate(string templateName, string requesterUserId);
        bool EditTemplate(string templateName, string requesterUserId);

    }

    public interface IChecksCompatibility
    {
        /// <summary>
        /// Checks callers is compatible with current object
        /// </summary>
        /// <param name="key">compatibility key of caller</param>
        /// <param name="callerVersion">symentic version of caller</param>
        /// <returns>true if compatible; else false</returns>
        bool IsCompatible(string key, Version callerVersion);
    }


    public class IQBotCompatibilityChecker
    {
        public bool IsCompatible(Version clientVersion)
        {
            var clientKey = new ClientOwnedIQBotCompatibilityKeyProvider().GetKey();

            try
            {
                // IChecksCompatibility compatibilityChecker = DynamicInstanceGenerator.GetIChecksCompatibility();

                //   return compatibilityChecker.IsCompatible(clientKey, clientVersion);
            }
            catch (Exception)
            {
                throw new NonCompatibilityCheckableIQBotException();
            }
            return true;
        }
    }

    public class NonCompatibilityCheckableIQBotException : Exception
    { }

    internal class ClientOwnedIQBotCompatibilityKeyProvider
    {
        public string GetKey()
        {
            return "4.0.0-RC1-161007.06";
        }
    }

    public class CannotDeleteReadonlyTemplateException : TemplateException
    {
        public CannotDeleteReadonlyTemplateException(string templateName)
            : base(templateName)
        { }
    }

    public class CentralRepositoryUnavailableException : Exception
    {
        public CentralRepositoryUnavailableException()
        { }
    }

    public static class VersionExtensions
    {
        public static Version GetFileVersion(this FileVersionInfo fileVersionInfo)
        {
            return new Version(fileVersionInfo.FileMajorPart, fileVersionInfo.FileMinorPart, fileVersionInfo.FileBuildPart, fileVersionInfo.FilePrivatePart);
        }

        public static Version GetProductVersion(this FileVersionInfo fileVersionInfo)
        {
            return new Version(fileVersionInfo.ProductMajorPart, fileVersionInfo.ProductMinorPart, fileVersionInfo.ProductBuildPart, fileVersionInfo.ProductPrivatePart);
        }
    }

}
