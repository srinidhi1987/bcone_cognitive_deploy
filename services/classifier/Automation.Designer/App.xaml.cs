﻿using Automation.Cognitive.Designer;
using Automation.Cognitive.Services.Client;
using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string Protocol_Handler_Text = "aacp.designer:";

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ConfigurationParameters configurationParameters;
            try
            {
                configurationParameters = this.getConfigurationParameters(e);
            }
            catch(Exception ex)
            {
                //todo: write log
                MessageBox.Show(CognitiveData.Properties.Resources.ArgsFormattingException,
                    CognitiveData.Properties.Resources.MessageBoxCaption,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            if (configurationParameters != null)
            {
                RestConfigurationProvider.BaseUrl = configurationParameters.BaseUrl;
                RestConfigurationProvider.Token = configurationParameters.Token;
                RestConfigurationProvider.UserName = configurationParameters.Username;
                RestConfigurationProvider.IsGatewayUrl = true;

                ClassifiedFolderManifest manifest = new ClassifiedFolderManifest()
                {
                    organizationId = configurationParameters.OrganizationId,
                    id = configurationParameters.CategoryId,
                    name = configurationParameters.CategoryId,
                    projectId = configurationParameters.ProjectId
                };

                VisionBotUIManager manager = new VisionBotUIManager(manifest);
                manager.TrainVisionBot();
            }
            else
            {
                return;
            }
        }

        private ConfigurationParameters getConfigurationParameters(StartupEventArgs e)
        {
            if (e.Args == null || e.Args.Count() != 1)
            {
                MessageBox.Show(CognitiveData.Properties.Resources.InvalidArgs,
                   CognitiveData.Properties.Resources.MessageBoxCaption,
                   MessageBoxButton.OK,
                   MessageBoxImage.Error);
                return null;
            }

            string[] parameters = (e.Args[0]).Replace("%5C","\\").Split('\\');

            if (parameters.Length != 4)
            {
                MessageBox.Show(CognitiveData.Properties.Resources.InvalidArgs,
                  CognitiveData.Properties.Resources.MessageBoxCaption,
                  MessageBoxButton.OK,
                  MessageBoxImage.Error);
                return null;
            }

            string decryptedString = DecryptionProvider.Decrypt(parameters[0].Substring(Protocol_Handler_Text.Length));
            string[] decryptedParameters = decryptedString.Split('\\');

            ConfigurationParameters configurationParameters = new ConfigurationParameters
            {
                BaseUrl = decryptedParameters[0],
                Token = HttpUtility.UrlDecode(parameters[1]),
                Username = decryptedParameters[2],
                OrganizationId = decryptedParameters[1],
                ProjectId = parameters[2],
                CategoryId = parameters[3]
            };

            return configurationParameters;
        }
    }
}
