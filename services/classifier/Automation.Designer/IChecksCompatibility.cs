﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Designer
{
    public interface IChecksCompatibility
    {
        bool IsCompatible(string key, Version callerVersion);
    }
}
