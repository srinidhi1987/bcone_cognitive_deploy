﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{
    #region Usings

    //using Automation.CognitiveData.Analytics;
    using Automation.CognitiveData.VisionBot;
    using Automation.VisionBotEngine;
    using Designer.NewCode;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using VisionBotEngine.Configuration;
    using Automation.Services.Client;
    using System.IO;

    #endregion Usings

    [ObfuscationAttribute(Exclude = true, ApplyToMembers = false)]
    public sealed class VisionbotDesignerExecutor : Designer.NewCode.ICognitiveDesignerExecutor, IChecksCompatibility
    {
        #region Private Members

        private IVisionbotDesignerEngine CognitiveDataEngine;

        private static class ReturnCodes
        {
            public static class General
            {
                public static readonly int Success = 1;

                public static readonly int Failure = 0;
            }

            public static class DeleteTemplate
            {
                public static readonly int Success = 1;

                public static readonly int Failure = 0;

                public static readonly int CannotDeleteReadonlyTemplate = -1;
            }
        }

        #endregion Private Members

        #region Constructor

        public VisionbotDesignerExecutor()
        {
            this.CognitiveDataEngine = getDefaultEngine();
        }

        public VisionbotDesignerExecutor(IVisionbotDesignerEngine cognitiveEngine)
        {
            this.CognitiveDataEngine = cognitiveEngine;
        }

        #endregion Constructor

        #region Private Method

        //TODO: Temporary diversion for WPF VisionBot UI.
        private IVisionbotDesignerEngine getDefaultEngine()
        {
            var engineMode = ProductConfiguration.Get("Main", "visionbot", "enginetype", "1.1");
            string servicConfigurationPath = "CognitiveServiceConfiguration.json";
            string path = Path.Combine(
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
            string json = File.ReadAllText(path);

            CognitiveServicesConfiguration _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);
            /*Common.Configuration.ProductConfiguration.GetValue(Common.Configuration.ProductComponents.Main,
                "visionbot", "enginetype", "1.1");*/
            if (engineMode.StartsWith("1"))
            {
                VBotLogger.Initlization(RoutineProvider.Routine.GetApplicationPath(), _cognitiveServicesConfiguration.LoggerSettings);
                VBotLogger.Trace(() => "[Initlization] Visionbot logger ... ");
                return new VisionBotEngineWrapper(engineMode);
            }
            return null;
        }

        #endregion Private Method

        #region ICognitiveDesignerExecute Implementation

        /// <summary>
        /// Create new visionbot in IQBots root folder.
        /// </summary>
        /// <param name="templateName"></param>
        /// <param name="requesterUserId"></param>
        /// <returns>True</returns>
        public bool CreateNewTemplate(string templateName, string requesterUserId)
        {
            VBotLogger.Trace(() => "[CreateNewTemplate] New Visionbot ... ");

            return CognitiveDataEngine.CreateNewTemplate(templateName, Constants.CDC.ApplicationDataPath)
                        == ReturnCodes.General.Success;
        }

        /// <summary>
        /// Change Template Form fields and Line Items.
        /// </summary>
        /// <param name="templateName"></param>
        /// <param name="requesterUserId"></param>
        /// <returns>True</returns>
        public bool EditTemplate(string templateName, string requesterUserId)
        {
            VBotLogger.Trace(() => "[EditTemplate] Edit Visionbot ... ");

            return CognitiveDataEngine.EditTemplate(templateName, Constants.CDC.ApplicationDataPath, this.IsDefaultTemplate(templateName))
                      == ReturnCodes.General.Success;
        }

        #endregion ICognitiveDesignerExecute Implementation

        #region IChecksCompatibility Implementation

        public bool IsCompatible(string key, Version callerVersion)
        {
            return this.CognitiveDataEngine.IsCompatible(key, callerVersion);
        }

        #endregion IChecksCompatibility Implementation

        private bool IsDefaultTemplate(string templateName)
        {
            var defaultTemplates = new List<string>
                                       {
                                           "Demo Invoice Bot"
                                       };

            return defaultTemplates.Contains(templateName);
        }
    }
}