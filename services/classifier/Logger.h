#ifndef CUSTOM_CLogger_H
#define CUSTOM_CLogger_H
#include <fstream>
#include <iostream>
#include <cstdarg>
#include <string>
using namespace std;
#define LOGGER CLogger::GetLogger()

class CLogger
{
public:
	void Log(const std::string& sMessage);
	void Log(const char * format, ...);
	CLogger& operator<<(const string& sMessage);
	static CLogger* GetLogger();
private:
	CLogger();
	CLogger(const CLogger&) {};
	CLogger& operator=(const CLogger&) { return *this; };
	char* m_sFileName;
	static CLogger* m_pThis;
	static std::ofstream m_Logfile;
	void InitializeLogging();
	bool IsDirectoryExist(string strPath);
};
#endif