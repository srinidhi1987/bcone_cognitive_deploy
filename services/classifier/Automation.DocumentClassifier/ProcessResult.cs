﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.DocumentClassifier
{
    public class ProcessResult
    {
        public bool IsSuccess { get; set; }
        public string Result { get; set; }

        public override string ToString()
        {
            return $"success:{IsSuccess}, data: {Result}";
        }
    }
}
