﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Cognitive.VisionBotEngine.Model.ExecutorOperations;
using Automation.Generic.Configuration;
using Automation.IQBot.Logger;
using Automation.Services.Client;
using Automation.VisionBotEngine;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.DocumentClassifier
{
    internal class ClassifierConfiguration
    {
        private static readonly ConfigurationHelper _configHelper = new ConfigurationHelper(new JsonConfigManager());

        private const string _defaultDBConnectionString = "";
        private static string _dBConnectionString;

        public static string DbConnectionString
        {
            get
            {
                if (_dBConnectionString != "")
                {
                    _dBConnectionString = _configHelper.GetParsedValue(
                        nameof(DbConnectionString),
                        _defaultDBConnectionString,
                        ConfigurationHelper.ParseString);
                }
                return _dBConnectionString;
            }
        }

        private const string _defaultAliasServiceBaseURL = "http://localhost:9997/";
        private static string _aliasServiceBaseURL;

        public static string AliasServiceBaseURL
        {
            get
            {
                if (_aliasServiceBaseURL != "")
                {
                    _aliasServiceBaseURL = _configHelper.GetParsedValue(
                        nameof(AliasServiceBaseURL),
                        _defaultAliasServiceBaseURL,
                        ConfigurationHelper.ParseString);
                }
                return _aliasServiceBaseURL;
            }
        }

        private const string _defaultProjectServiceBaseURL = "http://localhost:9999/";
        private static string _projectServiceBaseURL;

        public static string ProjectServiceBaseURL
        {
            get
            {
                if (_projectServiceBaseURL != "")
                {
                    _projectServiceBaseURL = _configHelper.GetParsedValue(
                        nameof(ProjectServiceBaseURL),
                        _defaultProjectServiceBaseURL,
                        ConfigurationHelper.ParseString);
                }
                return _projectServiceBaseURL;
            }
        }

        private const string _defaultClassifiedMQName = "QUEUE_Visionbot_ProcessDocument";
        private static string _classifiedmqName;

        public static string ClassifiedMQName
        {
            get
            {
                if (_classifiedmqName != "")
                {
                    _classifiedmqName = _configHelper.GetParsedValue(
                        nameof(ClassifiedMQName),
                        _defaultClassifiedMQName,
                        ConfigurationHelper.ParseString);
                }
                return _classifiedmqName;
            }
        }

        private const string _defaultmqPassword = "";
        private static string _mqPassword;

        public static string Password
        {
            get
            {
                if (_mqPassword != "")
                {
                    _mqPassword = _configHelper.GetParsedValue(
                        nameof(Password),
                        _defaultmqPassword,
                        ConfigurationHelper.ParseString);
                }
                return _mqPassword;
            }
        }

        private const string _defaultmqUserName = "";
        private static string _mqUserName;

        public static string UserName
        {
            get
            {
                if (_mqUserName != "")
                {
                    _mqUserName = _configHelper.GetParsedValue(
                        nameof(UserName),
                        _defaultmqUserName,
                        ConfigurationHelper.ParseString);
                }
                return _mqUserName;
            }
        }

        private const string _defaultmqVirtualHost = "test";
        private static string _mqVirtualHost;

        public static string VirtualHost
        {
            get
            {
                if (_mqVirtualHost != "")
                {
                    _mqVirtualHost = _configHelper.GetParsedValue(
                        nameof(VirtualHost),
                        _defaultmqVirtualHost,
                        ConfigurationHelper.ParseString);
                }
                return _mqVirtualHost;
            }
        }

        private const string _defaultmqPort = "5673";
        private static string _mqPort;

        public static string Port
        {
            get
            {
                if (_mqPort != "")
                {
                    _mqPort = _configHelper.GetParsedValue(
                        nameof(Port),
                        _defaultmqPort,
                        ConfigurationHelper.ParseString);
                }
                return _mqPort;
            }
        }

        private const string _defaultFileUploadedMQName = "topic_classification";
        private static string _fileUploadedMQName;

        public static string FileUploadedMQName
        {
            get
            {
                if (_fileUploadedMQName != "")
                {
                    _fileUploadedMQName = _configHelper.GetParsedValue(
                        nameof(FileUploadedMQName),
                        _defaultFileUploadedMQName,
                        ConfigurationHelper.ParseString);
                }
                return _fileUploadedMQName;
            }
        }

        private const string _defaultFileServiceBaseURL = "http://localhost:9996/";
        private static string _fileServiceBaseURL;

        public static string FileServiceBaseURL
        {
            get
            {
                if (_fileServiceBaseURL != "")
                {
                    _fileServiceBaseURL = _configHelper.GetParsedValue(
                        nameof(FileServiceBaseURL),
                        _defaultFileServiceBaseURL,
                        ConfigurationHelper.ParseString);
                }
                return _fileServiceBaseURL;
            }
        }

        private const string _defaultMQServer = "localhost";
        private static string _mqServer;

        public static string Uri
        {
            get
            {
                if (_mqServer != "")
                {
                    _mqServer = _configHelper.GetParsedValue(
                        nameof(Uri),
                        _defaultMQServer,
                        ConfigurationHelper.ParseString);
                }
                return _mqServer;
            }
        }
    }

    internal class Program
    {
        public const string ServiceName = "Automation Anywhere Cognitive Classifier Service";

        private static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                var impl = new ClassifierServiceImpl();

                // running as console app
                impl.Start();

                Console.WriteLine("Press any key to stop...");
                Console.ReadKey(true);

                impl.Stop();
            }
            else
            {
                ServiceBase.Run(new Service());
            }
        }
    }

    internal class ClassifierServiceImpl
    {
        internal string ExecutorPath = string.Empty;
        public bool stopService = false;
        
        private EventingBasicConsumer consumer;
        private Tesseract4Settings Tesseract4Setting = new Tesseract4Settings();

        [HandleProcessCorruptedStateExceptions]
        private void classificationWoker()
        {
            string correlationId = Guid.NewGuid().ToString();
            IQBotLogger.setThreadContext(Constants.KEY_CORRELATION_ID, correlationId);
            IQBotLogger.Trace("Classification Woker started");
            IQBotLogger.Debug($"MQ Server: {ClassifierConfiguration.Uri}");
            Tesseract4Setting = new Tesseract4SettingProvider().GetTesseract4Settings();
            var factory = new ConnectionFactory() { HostName = ClassifierConfiguration.Uri };
            factory.VirtualHost = ClassifierConfiguration.VirtualHost;
            factory.UserName = ClassifierConfiguration.UserName;
            factory.Password = ClassifierConfiguration.Password;
            factory.AutomaticRecoveryEnabled = true;
            factory.Port = Int32.Parse(ClassifierConfiguration.Port);
            int noOfContiniousTryForConnectionEstablishment = 0;
            do
            {
                try
                {
                    startClassificationQueue(factory);
                    noOfContiniousTryForConnectionEstablishment = 0;
                }
                catch (Exception ex)
                {
                    IQBotLogger.Error($"Exception: {ex.Message}, try count {++noOfContiniousTryForConnectionEstablishment}", ex);
                }

                Thread.Sleep(30000);
            }
            while (!stopService);
        }

        #region MsgUnderProcess

        private int __msgUnderProcess = 0;
        private readonly object __msgUnderProcessLock = new object();

        private int MsgUnderProcess
        {
            get
            {
                lock (__msgUnderProcessLock)
                {
                    return __msgUnderProcess;
                }
            }
        }

        private int IncrementMsgUnderProcess()
        {
            lock (__msgUnderProcessLock)
            {
                return ++__msgUnderProcess;
            }
        }

        private int DecrementMsgUnderProcess()
        {
            lock (__msgUnderProcessLock)
            {
                return --__msgUnderProcess;
            }
        }

        #endregion MsgUnderProcess

        private void startClassificationQueue(ConnectionFactory factory)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(ClassifierConfiguration.FileUploadedMQName, true, false, false, null);

                Tesseract4Setting.NoOfDocsProcessInParallel = Environment.ProcessorCount > 1 ? (Environment.ProcessorCount - 1) : 1;
                channel.BasicQos(prefetchSize: 0, prefetchCount: (ushort)Tesseract4Setting.NoOfDocsProcessInParallel, global: false);

                IQBotLogger.Trace($"No. of workers {Tesseract4Setting.NoOfDocsProcessInParallel}");
                int noOfMsgCanProcess = Tesseract4Setting.NoOfDocsProcessInParallel;
                consumer = new EventingBasicConsumer(channel);
                channel.BasicConsume(queue: ClassifierConfiguration.FileUploadedMQName,
                                     noAck: false,
                                     consumer: consumer);
                consumer.Received += (model, ea) =>
                {
                    try
                    {
                        if (ea != null)
                        {
                            var message = Encoding.UTF8.GetString(ea.Body);
                            IQBotLogger.Debug($"Classification message arrived {message}");
                            try
                            {
                                CallerProxy caller = new CallerProxy();
                                IQBotLogger.Info("Starting document classification");
                                do
                                {
                                    Thread.Sleep(1000);
                                } while (MsgUnderProcess >= noOfMsgCanProcess);

                                Task t = Task.Run(new Action(() =>
                                {
                                    try
                                    {
                                        caller.ClassifyDocument(message);
                                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                                        IQBotLogger.Trace($"Classification end, basic acknowledgement completed");
                                    }
                                    finally
                                    {
                                        int msgCountAfterDecrement = DecrementMsgUnderProcess();
                                        IQBotLogger.Trace($"Message under process after decreasing: {msgCountAfterDecrement}");
                                    }
                                    return;
                                }));
                                int msgCountAfterIncrement = IncrementMsgUnderProcess();
                                IQBotLogger.Trace($"Message in queue process: {msgCountAfterIncrement}");
                            }
                            catch (Exception ex)
                            {
                                IQBotLogger.Error(ex.Message, ex);
                            }
                            IQBotLogger.Trace("Waiting for next message...");
                        }
                    }
                    catch (Exception ex)
                    {
                        IQBotLogger.Error(ex.Message, ex);
                    }
                };
                while (!stopService)
                {
                    Thread.Sleep(1000);
                }
                channel.Close();
                connection.Close();
            }
        }

        public void Start()
        {
            this.stopService = false;

            string basedir = AppDomain.CurrentDomain.BaseDirectory;
            IQBotLogger.Debug($"Base directory: {basedir}");

            ExecutorPath = Path.Combine(basedir, "Automation.Cognitive.DocumentClassifier.Executor.exe");
            CallerProxy.ExecutorPath = this.ExecutorPath;

            IQBotLogger.Debug($"Executor path: {ExecutorPath}");

            try
            {
                classificationWoker();
            }
            catch (Exception ex)
            {
                IQBotLogger.Error(ex.Message, ex);
            }
        }

        public void Stop()
        {
            this.stopService = true;
        }
    }

    internal class CallerProxy
    {
        public static string ExecutorPath { get; set; }

        private string Execute(string args)
        {
            IQBotLogger.Debug($"Executor path: {ExecutorPath}");
            IQBotLogger.Debug($"Arguments: {args}");
            var t = ProcessInvokerUtil.RunProcessAsync(ExecutorPath, args);

            t.Wait();
            var output = t.Result;

            if (!output.IsSuccess || output.Result.StartsWith("ERROR"))
            {
                IQBotLogger.Error($"Error: {output.Result}");
                return string.Empty;
            }
            if (output.Result.EndsWith(".result") && File.Exists(output.Result))
            {
                IQBotLogger.Debug($"Executor file result {output}");
                var result = File.ReadAllText(output.Result);
                File.Delete(output.Result);
                return result;
            }
            IQBotLogger.Debug($"Executor gave raw result {output.Result}");
            return output.Result;
        }

        public string ClassifyDocument(string inputData)
        {
            var param = new ClassifyDocumentOperationParams();

            var inputParameters = convertToDictionary(inputData);
            param.OrganizationId = "1";
            param.ProjectId = inputParameters["projectId"].ToString();
            param.FileId = inputParameters["fileId"].ToString();
            param.FileName = inputParameters["fileName"].ToString();

            var args = param.ToConsoleArgs();
            return Execute(args);
        }

        #region Private Member

        private Dictionary<string, object> convertToDictionary(string input)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(input);
        }

        #endregion Private Member
    }
}