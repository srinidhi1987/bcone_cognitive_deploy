﻿using System.ServiceProcess;
using System.Threading;

namespace Automation.DocumentClassifier
{
    internal class Service : ServiceBase
    {
        private Thread m_thread = null;
        private ClassifierServiceImpl impl = new ClassifierServiceImpl();

        public Service()
        {
            ServiceName = Program.ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            m_thread = new Thread(new ThreadStart(ThreadProc));
            m_thread.Start();
        }

        public void ThreadProc()
        {
            impl.Start();
            this.Stop();
        }

        protected override void OnStop()
        {
            impl.stopService = true;
            impl.Stop();
        }
    }
}