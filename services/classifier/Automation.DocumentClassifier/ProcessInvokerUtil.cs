﻿using Automation.Cognitive.DocumentClassifier;
using Automation.IQBot.Logger;
using Automation.VisionBotEngine;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Automation.DocumentClassifier
{
    internal class ProcessInvokerUtil
    {
        protected ProcessInvokerUtil() { }
        public static async Task<ProcessResult> RunProcessAsync(string fileName, string args)
        {
            using (var process = new Process
            {
                StartInfo =
                {
                    FileName = fileName, Arguments = args,
                    UseShellExecute = false, CreateNoWindow = true,
                    RedirectStandardOutput = true
                },
                EnableRaisingEvents = true
            })
            {
                return await RunProcessAsync(process).ConfigureAwait(false);
            }
        }

        private static Task<ProcessResult> RunProcessAsync(Process process)
        {
            IQBotLogger.Trace("Started");
            var tcs = new TaskCompletionSource<ProcessResult>();
            var processResult = string.Empty;

            process.Exited += (s, ea) =>
            {
                var result = new ProcessResult { IsSuccess = (process.ExitCode == 0), Result = processResult.ToString() };
                tcs.SetResult(result);
            };
            process.OutputDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrWhiteSpace(ea.Data))
                {
                    processResult = ea.Data;
                }
                IQBotLogger.Debug($"Data: {ea.Data}");
            };
            bool started = process.Start();
            if (!started)
            {
                IQBotLogger.Error($"Could not start executor process");
                //you may allow for the process to be re-used (started = false)
                //but I'm not sure about the guarantees of the Exited event in such a case
                throw new System.Exception("Could not start process: " + process);
            }
            IQBotLogger.Trace($"Completed");
            process.BeginOutputReadLine();
            return tcs.Task;
        }
    }
}