﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S3241:Methods should not return values that are never used", Justification = "<Pending>", Scope = "member", Target = "~M:Automation.IQBot.Logger.IQBotLogger.Log(System.String,log4net.Core.Level,System.Exception)~System.Threading.Tasks.Task")]

