﻿using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Automation.IQBot.Logger
{
    public class IQBotLogger
    {
        private static ILogger _logger;
        static IQBotLogger()
        {
            if (null == _logger)
            {
                _logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "IQBotLogger");
            }
        }

        protected IQBotLogger() { }

        public static void setThreadContext(string key, object value)
        {
            ThreadContext.Properties[key] = value;
        }

        public static object getThreadContext(string key)
        {
            return ThreadContext.Properties[key];
        }

        public static void Error(string message, Exception ex = null)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Log(message, Level.Error, ex);
        }
        public static void Fatal(string message, Exception ex = null)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Log(message, Level.Fatal, ex);
        }
        public static void Info(string message)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Log(message, Level.Info);
        }

        public static void Debug(string message)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Log(message, Level.Debug);
        }

        public static void Warn(string message)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Log(message, Level.Warn);
        }

        public static void Trace(string message)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Log(message, Level.Trace);
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        static async Task Log(string message, Level level, Exception ex = null)
        {
            _logger.Log(typeof(IQBotLogger), level, message, ex);
        }
    }
}
