﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Generic.Configuration
{
    public class ConfigurationHelper
    {
        private readonly IConfigurationManager _configManager;

        public ConfigurationHelper(IConfigurationManager configManager)
        {
            _configManager = configManager;
        }

        public static int ParseInt(string value, int defaultValue)
        {
            int result;
            return int.TryParse(value, out result) ? result : defaultValue;
        }

        public static float ParseFloat(string value, float defaultValue)
        {
            float result;
            return float.TryParse(value, out result) ? result : defaultValue;
        }

        public static string ParseString(string value, string defaultValue)
        {
            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        public TOutput GetParsedValue<TOutput>(
            string key,
            TOutput defaultValue,
            Func<string, TOutput, TOutput> parser)
        {
            TOutput result = defaultValue;
            if (_configManager == null) return result;

            var configValue = _configManager.GetValue(key);
            if (!string.IsNullOrEmpty(configValue))
            {
                result = parser(configValue, defaultValue);
            }

            return result;
        }
    }
}