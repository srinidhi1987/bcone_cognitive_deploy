﻿/**
* Copyright (c) 2018 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/


namespace Automation.Cognitive.Configuration.Util
{
    public static class Constants
    {
        public static class ConnectionString
        {
            public static readonly string KEY_USER_ID = "user id";
            public static readonly string VALUE_WIN_AUTH_USERNAME = "windows_auth";
            public static readonly string KEY_SECUREDATA = "Password";
            public static readonly string KEY_INTEGRATED_SECURITY = "Integrated Security";
            public static readonly string VALUE_INTEGRATED_SECURITY = "SSPI";
        }
        public static class Configuration
        {
            public static readonly string PATH_CONFIGURATIONS = "\\Configurations";
            public static readonly string FILENAME_CONFIGURATION_PROPERTIES = "Cognitive.properties";
            public static readonly string FILENAME_PRIVATE_KEY = "private.key";
            public static readonly string FILENAME_SETTINGS = "settings.txt";
            public static readonly string SETTINGS_KEY_SQLWINAUTH = "SQLWindowsAuthentication";
        }
    }
}
