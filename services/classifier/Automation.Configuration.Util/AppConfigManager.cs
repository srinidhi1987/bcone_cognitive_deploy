﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Generic.Configuration
{
    public class AppConfigManager : IConfigurationManager
    {
        public string GetValue(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }
    }
}