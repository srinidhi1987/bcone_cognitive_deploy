﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Generic.Configuration
{
    public interface IConfigurationManager
    {
        string GetValue(string key);
    }
}