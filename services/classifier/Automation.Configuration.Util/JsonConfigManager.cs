﻿/**
* Copyright (c) 2018 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/



namespace Automation.Generic.Configuration
{
    using System;
    using Automation.Services.Client;
    using Automation.Services.Client.Common;
    using Automation.VisionBotEngine;
    using System.IO;
    using System.Data.Common;
    using Automation.Cognitive.Configuration.Util;
    using System.Collections.Generic;

    public class JsonConfigManager : IConfigurationManager
    {
        string relativePathToPropertyFile = "";

        public JsonConfigManager()
        {
            string basedir = AppDomain.CurrentDomain.BaseDirectory;
            VBotLogger.Trace(() => string.Format($"[basedir] {basedir}"));
            var fileInfo = new FileInfo(basedir);
            var parentDir = fileInfo.Directory == null ? null : fileInfo.Directory.Parent;
            if (parentDir != null)
            {
                var resourceInfo = new FileInfo(parentDir.FullName);
                relativePathToPropertyFile = resourceInfo.DirectoryName + Constants.Configuration.PATH_CONFIGURATIONS;
            }
            CognitiveSecurePropertiesProvider.Init(relativePathToPropertyFile,
                Constants.Configuration.FILENAME_CONFIGURATION_PROPERTIES,
                Constants.Configuration.FILENAME_PRIVATE_KEY);
        }
        public string GetValue(string key)
        {
            RabbitMQSetting rabbitMQSettings = new ServiceLocationProvider().GetRabbitMQSetting();
            rabbitMQSettings.Password = CognitiveSecurePropertiesProvider.Instance().GetMessageQueuePassword();
            switch (key)
            {
                case "FileUploadedMQName":
                    ClassifierMQSetting classifierMQSetting = new ServiceLocationProvider().GetClassifierMQSettings();
                    return classifierMQSetting.FileUploadedMQName;

                case "DbConnectionString":
                    ClassifierDBSetting classifierDBSettings = new ServiceLocationProvider().GetClassifierDBSettings();
                    return UpdateConnectionString(classifierDBSettings.DbConnectionString);

                case "VirtualHost":
                    return rabbitMQSettings.VirtualHost;

                case "Uri":
                    return rabbitMQSettings.Uri;

                case "Password":
                    return rabbitMQSettings.Password;

                case "UserName":
                    return rabbitMQSettings.UserName;

                case "Port":
                    return rabbitMQSettings.Port;

                default:
                    return "";
            }
        }

        private string UpdateConnectionString(string connectionString)
        {
            DbConnectionStringBuilder builder = new DbConnectionStringBuilder();
            builder.ConnectionString = connectionString;
            if (!isWindowsAuthEnaled())
                builder.Add(Constants.ConnectionString.KEY_SECUREDATA, CognitiveSecurePropertiesProvider.Instance().GetDataBasePassword());
            else
            {
                builder.Add(Constants.ConnectionString.KEY_INTEGRATED_SECURITY, Constants.ConnectionString.VALUE_INTEGRATED_SECURITY);
                builder.Remove(Constants.ConnectionString.KEY_USER_ID);
            }
            return builder.ConnectionString;
        }

        private bool isWindowsAuthEnaled()
        {
            bool isWindowsAuthEnabled = false;
            try
            {
                Dictionary<string, string> settings = new CognitivePropertyReader(relativePathToPropertyFile,
                            Constants.Configuration.FILENAME_SETTINGS).Retrieve();
                if (settings.ContainsKey(Constants.Configuration.SETTINGS_KEY_SQLWINAUTH) && 
                    bool.TryParse(settings[Constants.Configuration.SETTINGS_KEY_SQLWINAUTH], out isWindowsAuthEnabled))
                {
                    isWindowsAuthEnabled = bool.Parse(settings[Constants.Configuration.SETTINGS_KEY_SQLWINAUTH]);
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => "Error reading settings file." + ex.StackTrace);
            }
            return isWindowsAuthEnabled;
        }
    }
}