﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    internal class ClasificationResult
    {
        public IList<LayoutConfidence> LayoutConfidences = new List<LayoutConfidence>();
    }

    internal class LayoutConfidence
    {
        public Layout Layout { get; set; }
        public decimal Confidence { get; set; }
        public double AspectRatioX { get; set; }
        public double AspectRatioY { get; set; }

        public LayoutConfidence()
        {
            Layout = new Layout();
            Confidence = 0;
        }
    }

    internal class VBotDocClassifier : IVBotDocClassifier
    {
        private IVisionBotPageImageProvider _visionBotPageImageProvider;
        private DocumentProperties _docProperties { get; set; }

        private bool _enableIRBasedMarker = true;
        private bool _isIRBasedMarkerEnable = false;

        private string _vbotName = string.Empty;
        private int _LayoutConfidenceThreshold = 0;

        public VBotDocClassifier(DocumentProperties docProps, IVisionBotPageImageProvider visionBotPageImageProvider)
        {
            _docProperties = docProps;
            _visionBotPageImageProvider = visionBotPageImageProvider;
        }

        public ClasificationResult Clasify(VisionBot vbot, List<Field> fields, int LayoutConfidenceThreshold)
        {
            _vbotName = vbot.Name;
            _LayoutConfidenceThreshold = LayoutConfidenceThreshold;
            ClasificationResult classificationResult = new ClasificationResult();
            bool IsValidConfidanceFound = false;
            bool IsLayoutConfidence100PercentFound = false;
            _isIRBasedMarkerEnable = false;

            for (int layoutNumber = 0; layoutNumber < vbot.Layouts.Count(); layoutNumber++)
            {
                VBotLogger.Trace(() => string.Format("[Layout Clasify]  Verify Layout '{0}'.", vbot.Layouts[layoutNumber].Name));

                if (!IsLayoutConfidence100PercentFound)
                {
                    LayoutConfidence layoutConfidance = CalculateLayoutConfidence(vbot.Layouts[layoutNumber], fields);

                    classificationResult.LayoutConfidences.Add(layoutConfidance);
                    if (layoutConfidance.Confidence >= _LayoutConfidenceThreshold)
                        IsValidConfidanceFound = true;

                    if (!IsValidConfidanceFound && vbot.Layouts.Count() - 1 == layoutNumber && !_isIRBasedMarkerEnable)
                    {
                        _isIRBasedMarkerEnable = true;
                        layoutNumber = -1;
                        classificationResult.LayoutConfidences.Clear();
                    }

                    if (layoutConfidance.Confidence == 100)
                    {
                        IsLayoutConfidence100PercentFound = true;
                    }
                }
                else
                {
                    LayoutConfidence layoutConfidance = new LayoutConfidence();
                    classificationResult.LayoutConfidences.Add(layoutConfidance);
                }
            }
            return classificationResult;
        }

        private LayoutConfidence CalculateLayoutConfidence(Layout layout, List<Field> fields)
        {
            if (fields.Count == 0)
            {
                return new LayoutConfidence();
            }
            FieldOperations fieldOperations = new FieldOperations();

            int matchCount = 0;
            // bool settingForIRBasedMarker = false;

            List<Point> markers = new List<Point>();
            List<Point> identifiers = new List<Point>();

            for (int i = 0; i < layout.Identifiers.Count; i++)
            {
                Field identifierField = fieldOperations.GetBestMatchingMarker(new SIRFields() { Fields = fields }, layout.Identifiers[i], layout.Identifiers[i].SimilarityFactor);

                int prevPageEndLocation = 0;
                int pageNumber = GetPageNumberBasedOnIdentifierLocation(layout, layout.Identifiers[i].Bounds.Y, ref prevPageEndLocation);

                if (identifierField != null && pageNumber < _docProperties.PageProperties.Count)
                {
                    if (/*settingForIRBasedMarker == false &&*/ identifierField.Bounds.Y <= (_docProperties.PageProperties[pageNumber].Height + prevPageEndLocation))
                    {
                        markers.Add(GetMarkerCenterPoint(layout.Identifiers[i]));
                        identifiers.Add(GetIdentifierCenterPoint(layout.DocProperties.PageProperties[pageNumber], _docProperties.PageProperties[pageNumber], identifierField.Bounds));

                        matchCount++;
                    }
                }
                else
                {
                    if (_enableIRBasedMarker == true && pageNumber == 0 && /*(settingForIRBasedMarker==true ||*/ _isIRBasedMarkerEnable == true)
                    {
                        Bitmap layoutImage = null;
                        Bitmap DocImage = null;
                        try
                        {
                            Page layoutPage = _visionBotPageImageProvider.GetImageFromLayout(layout, pageNumber);
                            Page DocPage = _visionBotPageImageProvider.GetImageFromProcessDocument(pageNumber);

                            if (layoutPage != null && DocPage != null)
                            {
                                layoutImage = AForge.Imaging.Image.Clone((Bitmap)layoutPage.ProcessedPageImage);
                                DocImage = AForge.Imaging.Image.Clone((Bitmap)DocPage.ProcessedPageImage);

                                if (DocImage.PixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed)
                                {
                                    DocImage = new Bitmap(DocPage.ProcessedPageImage);
                                    IImageConverter imageConverter = new Imaging.ImageConverter();
                                    imageConverter.ConvertImageTo8bpp(ref DocImage);
                                }

                                Rectangle MarkerBoundingRect = layout.Identifiers[i].Bounds;

                                //MarkerBoundingRect.Y = MarkerBoundingRect.Y - prevPageEndLocation;
                                Rectangle BoundingRectOfMatchingArea = ImageBasedMarkerMatching(MarkerBoundingRect, layoutImage, DocImage);
                                //BoundingRectOfMatchingArea.Y = BoundingRectOfMatchingArea.Y + prevPageEndLocation;

                                if (BoundingRectOfMatchingArea.Width != 0)
                                {
                                    markers.Add(GetMarkerCenterPoint(layout.Identifiers[i]));
                                    identifiers.Add(GetIdentifierCenterPoint(layout.DocProperties.PageProperties[pageNumber], _docProperties.PageProperties[pageNumber], BoundingRectOfMatchingArea));

                                    matchCount++;
                                    VBotLogger.Trace(() => string.Format("[CalculateLayoutConfidence]  Image Match Identifier = '{0}'.", layout.Identifiers[i].Label));
                                }
                            }
                            else
                            {
                                VBotLogger.Trace(() => string.Format("[CalculateLayoutConfidence]  Not Match Identifier = '{0}'.", layout.Identifiers[i].Label));
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Log(nameof(VBotDocClassifier), nameof(CalculateLayoutConfidence), "IRBasedMarker ImageIssue");
                        }
                        finally
                        {
                            if (layoutImage != null)
                                layoutImage.Dispose();
                            if (DocImage != null)
                                DocImage.Dispose();
                        }
                    }
                    else
                    {
                        VBotLogger.Trace(() => string.Format("[CalculateLayoutConfidence]  Not Match Identifier = '{0}'.", layout.Identifiers[i].Label));
                    }
                }
                /*   i++;
                   if (layout.Identifiers.Count == i && settingForIRBasedMarker == false)
                   {
                       LayoutConfidence layoutConfidence=GetLayoutConfidence(layout, matchCount, markers, identifiers);
                       if (layoutConfidence.Confidence >= _LayoutConfidenceThreshold)
                       {
                           return layoutConfidence;
                       }
                       settingForIRBasedMarker = true;
                       i = 0;
                   }      */
            }

            VBotLogger.Trace(() => string.Format("[CalculateLayoutConfidence]  Verify Layout Total Identifier = '{0}'.", matchCount));

            return GetLayoutConfidence(layout, matchCount, markers, identifiers);
        }

        private Point GetIdentifierCenterPoint(PageProperties layoutPage, PageProperties docPage, Rectangle identifierField)
        {
            RectangleOperations rectOperations = new RectangleOperations();
            return new Point(rectOperations.CalculateXLocation(identifierField, layoutPage.Width, docPage.Width),
                             rectOperations.CalculateYLocation(identifierField, layoutPage.Height, docPage.Height));
        }

        private static Point GetMarkerCenterPoint(MarkerLayout markerlayout)
        {
            return new Point(markerlayout.Bounds.X + (markerlayout.Bounds.Width / 2),
                             markerlayout.Bounds.Y + (markerlayout.Bounds.Height / 2));
        }

        private int GetPageNumberBasedOnIdentifierLocation(Layout layout, int identifierLocationY, ref int prevPageEndLocation)
        {
            for (int PageNumber = 0; PageNumber < layout.DocProperties.PageProperties.Count; PageNumber++)
            {
                if (identifierLocationY <= (layout.DocProperties.PageProperties[PageNumber].Height + prevPageEndLocation) && identifierLocationY > prevPageEndLocation)
                {
                    return PageNumber;
                }
                prevPageEndLocation += layout.DocProperties.PageProperties[PageNumber].Height;
            }
            return 0;
        }

        private LayoutConfidence GetLayoutConfidence(Layout layout, int matchCount, List<Point> markers, List<Point> identifiers)
        {
            LayoutConfidence layoutConfidence = new LayoutConfidence();
            RectangleOperations rectOperations = new RectangleOperations();
            const int DistanceTolerance = 300;

            layoutConfidence.Layout = layout;

            if (layout.Identifiers.Count == 0)
            {
                layoutConfidence.Confidence = 0;
            }
            else if (layout.Identifiers.Count == 1 && matchCount == 1)
            {
                int distance = rectOperations.DistanceBetweenTwoPoints(markers[0], identifiers[0]);

                if (distance < DistanceTolerance)
                {
                    layoutConfidence.Confidence = 100;
                    VBotLogger.Trace(() => "[DocumentClassification] Defined Single Marker Matched ...");
                }
                else
                {
                    VBotLogger.Trace(() => string.Format("[DocumentClassification] Layout Marker Point {0} , Identifier Point {1}", markers[0], identifiers[0]));
                    VBotLogger.Trace(() => "[DocumentClassification] Defined Single Marker Not Matched ...");
                }
            }
            else
            {
                if (layout.Identifiers.Count > 1 && matchCount > 1)
                {
                    if (AreDocumentWidthHeightAvailable(layout))
                    {
                        List<decimal> PageWiseMarkerConfidence = new List<decimal>();
                        int prevPageEndLocation = 0;
                        foreach (PageProperties page in layout.DocProperties.PageProperties)
                        {
                            List<Point> layoutPageMarker = new List<Point>();
                            List<Point> docPageMarker = new List<Point>();
                            for (int i = 0; i < markers.Count; i++)
                            {
                                if (markers[i].Y <= (page.Height + prevPageEndLocation) && markers[i].Y >= prevPageEndLocation)
                                {
                                    layoutPageMarker.Add(markers[i]);
                                    docPageMarker.Add(identifiers[i]);
                                }
                            }
                            PageWiseMarkerConfidence.Add(GetMarkerBasedConfidence(layout.Identifiers.Count, layoutPageMarker, docPageMarker, DistanceTolerance));
                            prevPageEndLocation += page.Height;
                        }

                        int totalPageWiseConfidence = 0;
                        foreach (decimal currentPageConfidence in PageWiseMarkerConfidence)
                        {
                            totalPageWiseConfidence += (int)currentPageConfidence;
                        }

                        layoutConfidence.Confidence = totalPageWiseConfidence;/// GetNumberOfPagesBasedOnMarkerLocations(layout);
                    }
                    else
                    {
                        layoutConfidence.Confidence = GetLayoutMatchConfidence(layout.Identifiers.Count, matchCount);
                    }
                }
            }

            VBotLogger.Trace(() => string.Format("[DocumentClassification] Layout Confidence = {0}", layoutConfidence.Confidence));

            UpdatetAspectLayoutConfidence(layoutConfidence, layout.DocProperties);

            //rectOperations.CalculateAspectRatio(layout.DocProperties.Width, layout.DocProperties.Height, _docProperties.Width, _docProperties.Height);

            //layoutConfidence.AspectRatioX = 1 / (rectOperations.GetXAspectRatio + 0.005);
            //layoutConfidence.AspectRatioY = 1 / (rectOperations.GetYAspectRatio + 0.005);

            return layoutConfidence;
        }

        public void UpdatetAspectLayoutConfidence(LayoutConfidence layoutConfidence, DocumentProperties layoutDoc)
        {
            RectangleOperations rectOperations = new RectangleOperations();
            var aspectRatio = rectOperations.GetAspectRatio(layoutDoc.PageProperties[0].Width, layoutDoc.PageProperties[0].Height, _docProperties.PageProperties[0].Width, _docProperties.PageProperties[0].Height);

            //layoutConfidence.AspectRatioX = 1 / (aspectRatio.XRatio + 0.005);
            //layoutConfidence.AspectRatioY = 1 / (aspectRatio.YRatio + 0.005);
            layoutConfidence.AspectRatioX = 1 / (aspectRatio.XRatio);
            layoutConfidence.AspectRatioY = 1 / (aspectRatio.YRatio);
        }

        private int GetNumberOfPagesBasedOnMarkerLocations(Layout layout)
        {
            List<int> maxPageCount = new List<int>();

            foreach (MarkerLayout marker in layout.Identifiers)
            {
                int prevPageEndLocation = 0;
                for (int PageNumber = 0; PageNumber < layout.DocProperties.PageProperties.Count; PageNumber++)
                {
                    if (marker.Bounds.Y <= (layout.DocProperties.PageProperties[PageNumber].Height + prevPageEndLocation) && marker.Bounds.Y > prevPageEndLocation)
                    {
                        maxPageCount.Add(PageNumber + 1);
                    }
                    prevPageEndLocation += layout.DocProperties.PageProperties[PageNumber].Height;
                }
            }
            return maxPageCount.Distinct().Count();
        }

        private decimal GetMarkerBasedConfidence(int layoutMarkerCount, List<Point> layoutPageMarker, List<Point> docPageMarker, int DistanceTolerance)
        {
            RectangleOperations rectOperations = new RectangleOperations();
            decimal markerBasedConfidence = 0;
            if (layoutPageMarker.Count == 1)
            {
                int distance = rectOperations.DistanceBetweenTwoPoints(layoutPageMarker[0], docPageMarker[0]);

                if (distance < DistanceTolerance)
                {
                    markerBasedConfidence = GetLayoutMatchConfidence(layoutMarkerCount, layoutPageMarker.Count);
                }
            }
            else
            {
                if (layoutPageMarker.Count > 1)
                {
                    List<Point> matchedMarkers = new List<Point>();
                    for (int i = 0; i < layoutPageMarker.Count; i++)
                    {
                        List<Point> layoutMarker = new List<Point>();
                        List<Point> docMarker = new List<Point>();
                        layoutMarker.Add(layoutPageMarker[i]);
                        docMarker.Add(docPageMarker[i]);
                        layoutMarker.Add(layoutPageMarker[(i + 1) % layoutPageMarker.Count]);
                        docMarker.Add(docPageMarker[(i + 1) % layoutPageMarker.Count]);

                        if (rectOperations.IsPatternMatch(layoutMarker, docMarker))
                        {
                            foreach (Point marker in docMarker)
                            {
                                if (!(matchedMarkers.Exists(x => x.X == marker.X)) && !(matchedMarkers.Exists(y => y.Y == marker.Y)))
                                    matchedMarkers.Add(marker);
                            }
                        }
                    }
                    markerBasedConfidence = GetLayoutMatchConfidence(layoutMarkerCount, matchedMarkers.Count);
                }
            }

            return markerBasedConfidence;
        }

        private static decimal GetLayoutMatchConfidence(int totalIdentifiers, int matchCount)
        {
            return (decimal)((decimal)matchCount / totalIdentifiers) * 100;
        }

        private bool AreDocumentWidthHeightAvailable(Layout layout)
        {
            return layout.DocProperties.Width > 0 &&
                   layout.DocProperties.Height > 0 &&
                   _docProperties.Width > 0 && _docProperties.Height > 0;
        }

        public Rectangle ImageBasedMarkerMatching(Rectangle MarkerBoundingRect, Bitmap layoutImage, Bitmap DocImage)
        {
            //Getting Marker Image as per bounding Rect
            IImageFilterOpetations imageFilterOperations = new ImageFilterOpetations();
            Bitmap template = imageFilterOperations.CroppedImage(layoutImage, MarkerBoundingRect);

            IImageConverter imageConverter = new Automation.VisionBotEngine.Imaging.ImageConverter();
            imageConverter.ConvertToGrayScale(ref template);

            //remove when actual logic comes
            IImageBinarizer imageBinarizer = new AForgeNetOtsu();
            imageBinarizer.AutoBinarize(ref template);

            //Getting Mapped Area from Document Image
            RectangleOperations rectOperations = new RectangleOperations();
            var aspectRatio = rectOperations.GetAspectRatio(DocImage.Width, DocImage.Height, layoutImage.Width, layoutImage.Height);

            Rectangle NewBoundingArea = rectOperations.GetBoundingRect(MarkerBoundingRect, DocImage.Width, DocImage.Height, aspectRatio);

            aspectRatio = rectOperations.GetAspectRatio(layoutImage.Width, layoutImage.Height, DocImage.Width, DocImage.Height);
            Bitmap sourceImage = imageFilterOperations.CroppedImage(DocImage, NewBoundingArea);

            imageFilterOperations.ResizeImage(ref sourceImage, aspectRatio.XRatio, aspectRatio.YRatio);

            imageConverter.ConvertImageTo8bpp(ref sourceImage);

            //remove when actual logic comes
            imageBinarizer.AutoBinarize(ref sourceImage);

            aspectRatio = rectOperations.GetAspectRatio(DocImage.Width, DocImage.Height, layoutImage.Width, layoutImage.Height);

            IImageFinder imageFinder = new ImageFinder();
            bool isImageFind = imageFinder.FindImage(template, sourceImage, "80");

            if (!isImageFind)
            {
                return rectOperations.ReturnEmptyRect();
            }
            return rectOperations.GetMappedBoundingRectangle(imageFinder.GetMatchedArea(), NewBoundingArea, aspectRatio);
        }

        //public string GetLayoutDocFilePath(string vBotName, string layoutVBotDocSetId)
        //{
        //    string vBotPath = CommonMethods.GetVBotPath(vBotName);

        //    IVBotDocSetStorage vBotDocSetStorage = new VBotDocSetStorage(vBotPath);
        //    VBotDocSet docStorage = vBotDocSetStorage.Load();

        //    foreach (VBotDocItem docItem in docStorage.DocItems)
        //    {
        //        if (docItem.Id == layoutVBotDocSetId)
        //        {
        //            return docItem.DocumentProperties.Path;
        //        }
        //    }
        //    return "";
        //}
    }
}