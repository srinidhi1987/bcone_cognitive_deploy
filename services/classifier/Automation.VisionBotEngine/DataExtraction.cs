﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using System.Reflection;

    public class DataExtraction
    {
        private const string ExclusionStartPattern = "-";
      
        private bool Flag = false;

        public DataExtraction()
        {
        }

        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        public string ExtractDataFromDocument(string[] featureWordsWithKey
            , DocumentVectorDetails documentVectorDetails
            , string folderName
            , bool isUniqueFeature
            , ref string FoundAliases)
        {
            string output = string.Empty;
            string output1 = string.Empty;

            List<string> featureWords = new List<string>();
            List<string> featureWordsKey = new List<string>();
            foreach (var featureWord in featureWordsWithKey)
            {
                var fWords = featureWord.Split(',');
                featureWords.Add(fWords[0]);
                featureWordsKey.Add(fWords[1]);
            }
            Dictionary<string, Field> sirs = GetSirs(documentVectorDetails, featureWords);

            List<Field> sirValue = new List<Field>();
            foreach (var sir in sirs)
            {
                sir.Value.Text = sir.Key;
                sirValue.Add(sir.Value);
            }
            List<Field> SortedField = new List<Field>();
            foreach (var field in sirValue)
            {
                if (SortedField.Count == 0)
                    SortedField.Add(field);

                bool flag = false;
                //foreach (var sortedField in SortedField)
                for (int i = 0; i < SortedField.Count; i++)
                {
                    Field sortedField = SortedField[i];
                    if (sortedField.Bounds.X == field.Bounds.X && sortedField.Bounds.Y == field.Bounds.Y)
                    {
                        if (sortedField.Confidence < field.Confidence)
                            SortedField[i] = field;
                        flag = true;
                    }
                }
                if (!flag)
                    SortedField.Add(field);
            }
            SortFieldsX(SortedField);
            SortFieldsY(SortedField);
            List<int> indexofSir = new List<int>();
            int index = 0;
            foreach (var value in SortedField)
            {
                for (int i = 0; i < featureWords.Count; i++)
                {
                    if (value.Text == featureWords[i])
                    {
                        //Console.WriteLine("FeatureWord Key:" + featureWordsKey[i]);
                        //Console.WriteLine("FeatureWord:" + featureWords[i]);
                        //Console.WriteLine("Found FeatureWord:" + value.Text);
                        if (isUniqueFeature && !output1.Contains(featureWords[i]) &&
                        !indexofSir.Contains(i))
                        {
                            indexofSir.Add(i);
                            output1 += featureWords[i].ToLower() + ",";
                        }
                        if (index == 0)
                        {
                            output += featureWordsKey[i].ToString();
                            FoundAliases += featureWords[i];
                        }
                        else
                        {
                            output += "," + featureWordsKey[i].ToString();
                            FoundAliases += "," + featureWords[i];
                        }
                        index++;
                    }
                }
            }
            if (Flag == true)
            {
                output += ";" + folderName;
            }
            return output;
        }

        public string GetPdfToImageFile(string pdfFilePath)
        {
            try
            {
                DocumentProperties docProperties = GetDocumentProperties(pdfFilePath);

                IDocumentPageImageProvider processingDocumentPageImageProvider =
                    new DocumentPageImageProvider(docProperties, new ImageProcessingConfig(), new VBotEngineSettings(), false);
                Image img = processingDocumentPageImageProvider.GetPage(0);
                if (img != null)
                {
                    string imgFilePath = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".png"); ;
                    img.Save(imgFilePath);
                    return imgFilePath;
                }
                else
                    return "";
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                return "";
            }
        }

        private bool DoExclude(string value)
        {
            return value.StartsWith(ExclusionStartPattern);
        }

        private DocumentProperties GetDocumentProperties(string filePath)
        {
            DocumentProperties docProps = new DocumentProperties();
            docProps.Path = filePath;
            return docProps;
        }

        private Dictionary<string, Field> GetSirs(
         DocumentVectorDetails documentVectorDetails,
         IEnumerable<string> featureWords)
        {
            var segmentedDocument = documentVectorDetails.SegmentedDocument;
            var lines = GetLines(segmentedDocument);
            var fields = GetFields(segmentedDocument, lines);

            return getFieldsFromKeys(fields, featureWords);
        }

        private Dictionary<string, Field> getFieldsFromKeys(List<Field> fields, IEnumerable<string> fieldKeys)
        {
            List<Field> fieldAsPerX = fields;
            List<Field> fieldAsPerY = fields;

            SortFieldsX(fieldAsPerX);

            SortFieldsY(fieldAsPerY);

            var trainFields = new Dictionary<string, Field>();
            foreach (string fieldKey in fieldKeys)
            {
                List<Field> matchFields = GetFieldFromKey(fields, fieldKey);
                if (matchFields?.Count > 0)
                {
                    trainFields.Add(fieldKey, matchFields.First().Clone());
                    //fields.Remove(matchFields.First());
                }
            }
            return trainFields;
        }

        private List<Field> GetFieldFromKey(List<Field> fields, string fieldKey)
        {
            IFieldOperations fieldOperation = new FieldOperations();
            return fieldOperation.GetAllBestMatchingFieldsForClassification(fields, fieldKey, 0.7);
        }

        private List<Field> GetFields(SegmentedDocument segDoc, List<Line> lines)
        {
            List<Field> fields = new List<Field>();

            for (int i = 0; i < segDoc.Regions.Count; i++)
            {
                Field field = CreateFieldFromRegion(segDoc.Regions[i]);

                if (IsValidWordField(field))
                {
                    fields.Add(field);
                }
            }

            fields = new SIROperations().MergeSegmentedFields(fields, lines);

            return fields;
        }

        private List<Line> GetLines(SegmentedDocument segDoc)
        {
            List<Line> lines = new List<Line>();

            for (int i = 0; i < segDoc.Regions.Count; i++)
            {
                Field field = CreateFieldFromRegion(segDoc.Regions[i]);

                if (IsValidLineField(field))
                {
                    Line line = CreateLineFromField(field);
                    lines.Add(line);
                }
            }

            return lines;
        }

        private void SortFieldsX(List<Field> fieldLists)
        {
            bool changes;
            int count = fieldLists.Count;
            do
            {
                changes = false;
                count--;
                for (int i = 0; i < count; i++)
                {
                    if (fieldLists[i].Bounds.X > fieldLists[i + 1].Bounds.X)
                    {
                        Field temp = fieldLists[i + 1];
                        fieldLists[i + 1] = fieldLists[i];
                        fieldLists[i] = temp;
                        changes = true;
                    }
                }
            } while (changes);
        }

        private void SortFieldsY(List<Field> fieldLists)
        {
            bool changes;
            int count = fieldLists.Count;
            do
            {
                changes = false;
                count--;
                for (int i = 0; i < count; i++)
                {
                    if (fieldLists[i].Bounds.Y > (fieldLists[i + 1].Bounds.Y + fieldLists[i + 1].Bounds.Height / 2) + 1
                        && fieldLists[i].Bounds.Y + fieldLists[i].Bounds.Width / 2 > fieldLists[i + 1].Bounds.Y + fieldLists[i + 1].Bounds.Height)
                    {
                        Field temp = fieldLists[i + 1];
                        fieldLists[i + 1] = fieldLists[i];
                        fieldLists[i] = temp;
                        changes = true;
                    }
                }
            } while (changes);
        }

        private Line CreateLineFromField(Field field)
        {
            Line newLine = new Line();

            int width = field.Bounds.Right - field.Bounds.Left;
            int height = field.Bounds.Bottom - field.Bounds.Top;

            int offset = 50;

            if (width <= offset && height >= offset)
            {
                newLine.X1 = newLine.X2 = (field.Bounds.Left + field.Bounds.Right) / 2;

                newLine.Y1 = field.Bounds.Top;
                newLine.Y2 = field.Bounds.Bottom;
            }
            else if (height <= offset && width >= offset)
            {
                newLine.Y1 = newLine.Y2 = (field.Bounds.Top + field.Bounds.Bottom) / 2;

                newLine.X1 = field.Bounds.Left;
                newLine.X2 = field.Bounds.Right;
            }

            return newLine;
        }

        private bool IsValidLineField(Field field)
        {
            if (field.SegmentationType != SegmentationType.Line)
                return false;

            int width = field.Bounds.Right - field.Bounds.Left;
            int height = field.Bounds.Bottom - field.Bounds.Top;

            int offset = 50;

            if (width <= offset && height >= offset)
                return true;

            if (height <= offset && width >= offset)
                return true;

            return false;
        }

        private Field CreateFieldFromRegion(SegmentedRegion segRegion)
        {
            Field field = new Field();

            field.Text = segRegion.Text;
            field.SegmentationType = segRegion.Type;
            field.Confidence = segRegion.Confidence;
            field.Bounds = segRegion.Bounds;
            field.Type = FieldType.SystemField;
            return field;
        }

        private bool IsValidWordField(Field field)
        {
            string fieldText = field.Text.Trim();

            if (field.SegmentationType != SegmentationType.Word || string.IsNullOrWhiteSpace(fieldText))
            {
                return false;
            }

            if (field.Bounds.Height <= 5 && field.Bounds.Width >= 10)
            {
                return false;
            }

            if (field.Text.Contains("___"))
            {
                return false;
            }

            if (field.Bounds.Width < 3 && field.Bounds.Height < 3)
            {
                return false;
            }
            return true;
        }
    }
}