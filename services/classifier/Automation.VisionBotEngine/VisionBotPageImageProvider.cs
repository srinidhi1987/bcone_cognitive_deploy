﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal class VisionBotPageImageProvider : IVisionBotPageImageProvider, IDisposable
    {

        private ImageProcessingConfig _config;
        private VisionBot _visionBot;
        private DocumentProperties _processingDocumentProperties;
        private IDocumentPageImageProvider _processingDocumentPageImageProvider;
        private IList<LayoutPageImageProvider> _layoutPageImageProviders = new List<LayoutPageImageProvider>();
        private VBotEngineSettings _engineSettings;

        private const int ImageCacheCount = 1;

        internal VisionBotPageImageProvider(VisionBot visionBot, DocumentProperties processingDocumentProperties, ImageProcessingConfig config, VBotEngineSettings engineSettings)
        {
            _visionBot = visionBot;
            _processingDocumentProperties = processingDocumentProperties;
            _config = config;

            _engineSettings = engineSettings;
            _processingDocumentPageImageProvider = new DocumentPageImageProvider(_processingDocumentProperties, _config, engineSettings, engineSettings.IsAccuracyLevelEnabled || engineSettings.IsCheckBoxFeatureEnabled);
        }

        public Page GetImageFromProcessDocument(int pageIndex)
        {
            VBotLogger.Trace(() => string.Format("[VisionBotPageImageProvider -> GetImageFromProcessDocument] Start for PageIndex: {0}", pageIndex));
            Page processedPage = _processingDocumentPageImageProvider.GetProcessedPageBitmap(pageIndex, 1);
            VBotLogger.Trace(() => string.Format("[VisionBotPageImageProvider -> GetImageFromProcessDocument] End for PageIndex: {0}  ProcessedPageReturned: {1}", pageIndex, processedPage != null));
            return processedPage;
        }

        public Page GetImageFromLayout(Layout layout, int pageIndex)
        {
            VBotLogger.Trace(() => string.Format("[VisionBotPageImageProvider -> GetImageFromLayout] Start for LayoutID: {0} Layout Name: {1} PageIndex: {2}", layout.Id, layout.Name, pageIndex));
            Page processedPage = null;
            LayoutPageImageProvider layoutPageImageProvider = getLayoutImageProviderAndCache(layout);

            processedPage = layoutPageImageProvider.DocumentPageImageProvider.GetProcessedPageBitmap(pageIndex, 0);
            VBotLogger.Trace(() => string.Format("[VisionBotPageImageProvider -> GetImageFromLayout] End for LayoutID: {0} Layout Name: {1} PageIndex: {2}  ProcessedPageReturned: {3}", layout.Id, layout.Name, pageIndex, processedPage != null));
            return processedPage;
        }

        private LayoutPageImageProvider getLayoutImageProviderAndCache(Layout layout)
        {
            LayoutPageImageProvider layoutPageImageProvider = getLayoutImageProvider(layout);
            if (layoutPageImageProvider == null)
            {
                layoutPageImageProvider = initializeLayoutPageImageProvider(layout);
                _layoutPageImageProviders.Add(layoutPageImageProvider);
            }

            return layoutPageImageProvider;
        }

        public void Dispose()
        {
            if (_processingDocumentPageImageProvider != null)
            {
                (_processingDocumentPageImageProvider as IDisposable).Dispose();
            }

            for (int i = 0; i < _layoutPageImageProviders.Count; i++)
            {
                _layoutPageImageProviders[i].Dispose();
            }

            _layoutPageImageProviders.Clear();
        }

        private LayoutPageImageProvider initializeLayoutPageImageProvider(Layout layout)
        {
            string layoutFilePath = GetLayoutDocFilePath(layout.VBotDocSetId);
            DocumentProperties clonedDocProperties = layout.DocProperties.Clone();
            clonedDocProperties.Path = layoutFilePath;
            VBotEngineSettings clonedEngineSettings = _engineSettings.Clone();

            IDocumentPageImageProvider layoutPageImageProvider = new DocumentPageImageProvider(clonedDocProperties, _config, clonedEngineSettings, false);
            LayoutPageImageProvider layoutImageProvider = new LayoutPageImageProvider(layout
                , layoutPageImageProvider);
            VBotLogger.Trace(() => string.Format("[VisionBotPageImageProvider -> initializeLayoutPageImageProvider] Initialized layoutPageImageProvider for LayoutID: {0} Layout Name: {1}", layout.Id, layout.Name));
            return layoutImageProvider;
        }

        private LayoutPageImageProvider getLayoutImageProvider(Layout layout)
        {
            return (from q in _layoutPageImageProviders where q.Layout.Id == layout.Id select q)
                .FirstOrDefault();
        }

        //TODO: Ned to optimize this. DocsetStorage.Load() should be called only once not for each layout.
        private string GetLayoutDocFilePath(string layoutVBotDocSetId)
        {
            string vBotPath = CommonMethods.GetVBotPath(_visionBot.Name);

            IVBotDocSetStorage vBotDocSetStorage = new VBotDocSetStorage(vBotPath);
            VBotDocSet docStorage = vBotDocSetStorage.Load();

            foreach (VBotDocItem docItem in docStorage.DocItems)
            {
                if (docItem.Id == layoutVBotDocSetId)
                {
                    return docItem.DocumentProperties.Path;
                }
            }
            return "";
        }

        public ImageProcessingConfig GetImageProcessingConfig()
        {
            return _config;
        }

        public CroppedImage GetCroppedImageFromProcessDocument(int x, int y, int w, int h)
        {
            return _processingDocumentPageImageProvider.GetCroppedImageFromAbsBounds(x, y, w, h);
        }

        public CroppedImage GetCroppedImageFromFromProcessDocument(int x
            , int y, int w, int h, int pageIndex)
        {
            return _processingDocumentPageImageProvider.GetCroppedImageFromRelativeBounds(x
                , y, w, h, pageIndex);
        }

        public CroppedImage GetCroppedImageFromLayout(Layout layout, int x, int y, int w, int h)
        {
            LayoutPageImageProvider layoutPageImageProvider = getLayoutImageProviderAndCache(layout);
            return layoutPageImageProvider.DocumentPageImageProvider.GetCroppedImageFromAbsBounds(x
                , y, w, h);
        }

        public CroppedImage GetCroppedImageFromFromLayout(Layout layout
            , int x, int y, int w, int h, int pageIndex)
        {
            LayoutPageImageProvider layoutPageImageProvider = getLayoutImageProviderAndCache(layout);
            return layoutPageImageProvider.DocumentPageImageProvider.GetCroppedImageFromRelativeBounds(x
                , y, w, h, pageIndex);
        }

        private class LayoutPageImageProvider
        {
            private Layout _layout;

            internal Layout Layout
            {
                get
                {
                    return _layout;
                }
            }

            private IDocumentPageImageProvider _documentPageImageProvider;

            internal IDocumentPageImageProvider DocumentPageImageProvider
            {
                get
                {
                    return _documentPageImageProvider;
                }
            }

            internal LayoutPageImageProvider(Layout layout
                , IDocumentPageImageProvider documentPageImageProvider)
            {
                _layout = layout;
                _documentPageImageProvider = documentPageImageProvider;
            }

            public void Dispose()
            {
                _layout = null;
                (_documentPageImageProvider as IDisposable).Dispose();
            }
        }
    }
}