﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 using Automation.VisionBotEngine.Model;
using System.Collections.Generic;

namespace Automation.VisionBotEngine
{
    internal class FieldKeyConverter
    {
        public List<FieldKey> ConvertLayoutToFieldKeys(List<FieldLayout> fieldLayouts)
        {
            List<FieldKey> fieldKeys = new List<FieldKey>();
            foreach (FieldLayout fieldLayout in fieldLayouts)
            {
                fieldKeys.AddRange(convertLayoutToFieldKey(fieldLayout));
            }
            return fieldKeys;
        }

        private List<FieldKey> convertLayoutToFieldKey(FieldLayout fieldLayout)
        {
            List<FieldKey> fieldKeys = new List<FieldKey>();
            if (string.IsNullOrWhiteSpace(fieldLayout.Label))
                return fieldKeys;

            string[] labelKeys = fieldLayout.Label.Split('|');

            foreach (string label in labelKeys)
            {
                FieldKey fieldKey = new FieldKey(label, Direction.Right, false);
                fieldKeys.Add(fieldKey);
            }

            return fieldKeys;
        }
    }
}
