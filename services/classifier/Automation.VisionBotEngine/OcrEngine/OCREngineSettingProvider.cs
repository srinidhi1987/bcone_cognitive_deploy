﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Automation.Cognitive.VisionBotEngine.Properties;


namespace Automation.VisionBotEngine.OcrEngine
{
    internal sealed class OCREngineSettingProvider
    {
        private static OCREngineSettingProvider _ocrEngineSettingProvider = null;

        #region Singleton OCREngineSettingProvider Instance

        public static OCREngineSettingProvider Instance
        {
            get
            {
                if (_ocrEngineSettingProvider == null)
                {
                    _ocrEngineSettingProvider = new OCREngineSettingProvider();
                }
                return _ocrEngineSettingProvider;
            }
        }

        #endregion Singleton OCREngineSettingProvider Instance

        #region Private Member

        private string _settingFilePath;
        private OCREngineSettings _ocrEngineSettings;

        #endregion Private Member

        #region Constructor

        private OCREngineSettingProvider()
        {
            // _settingFilePath = settingFilePath;
            _settingFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"../../Configurations/OCREngineSettings.json");
        }

        #endregion Constructor

        #region Public Methods

        public object GetSettings(string engineType)
        {
            if (_ocrEngineSettings == null)
            {
                initializeSettings();
            }

            if (engineType == Resources.Tesseract4OcrEngineName)
                return _ocrEngineSettings.Tesseract4_Settings;
            else if (engineType == Resources.Tesseract3OcrEngineName)
                return _ocrEngineSettings.Tesseract3_Settings;

            return null;
        }

        #endregion Public Methods

        #region Private Methods

        private void initializeSettings()
        {
            if (File.Exists(_settingFilePath))
            {
                try
                {
                    string json = File.ReadAllText(_settingFilePath);
                    VBotLogger.Trace(() => $"OCR Engine Setting File Path: {_settingFilePath}");
                    Dictionary<string, object> settings = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    _ocrEngineSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<OCREngineSettings>(settings[nameof(OCREngineSettings)].ToString());
                }
                catch (Exception exc)
                {
                    VBotLogger.Error(() => "Exception occured in reading OCR Engine Setting File, So taking Default Settings");
                    exc.Log(nameof(OCREngineSettingProvider), nameof(GetSettings));
                    _ocrEngineSettings = new OCREngineSettings();
                }
            }
            else
            {
                _ocrEngineSettings = new OCREngineSettings();
                VBotLogger.Error(() => $"OCR Engine Setting file is not found: {_settingFilePath}, So taking Default Settings");
            }
        }

        #endregion Private Methods
    }
}