﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.OcrEngine.Tesseract
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Drawing;
    using System.Xml.Linq;

    internal class HOcrToSegmentedDocConverter : IHOcrConverter
    {
        private const string HocrWord = "ocrx_word";
        private const string HocrLine = "ocr_line";
        private const string HocrBox = "bbox";
        private const string HocrClass = "class";
        private const string HocrTitle = "title";
        private const string BaseLine = "baseline";
        private const string HocrConfidence = "x_wconf";

        private SegmentedDocument _doc = null;

        public SegmentedDocument GetSegmentedDocument(string hOcrText)
        {
            _doc = new SegmentedDocument();

            if (string.IsNullOrEmpty(hOcrText))
            {
                VBotLogger.Trace(() => "[GetSegmentedDocument] Ocr text is empty.");
                return _doc;
            }

            try
            {
                XDocument xdoc = XDocument.Parse(hOcrText);
                ParseNodes(xdoc.Root);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(HOcrToSegmentedDocConverter), nameof(GetSegmentedDocument), $"Unable to parse ocr data \"{hOcrText}\"");
            }

            return _doc;
        }

        private void ParseNodes(XElement xelement)
        {
            foreach (XElement element in xelement.Elements())
            {
                SegmentedRegion eleRegion = ConvertXElementToRegion(element);

                if (eleRegion != null)
                {
                    _doc.Regions.Add(eleRegion);
                }

                ParseNodes(element);
            }
        }

        private SegmentedRegion ConvertXElementToRegion(XElement element)
        {
            SegmentedRegion region = null;

            string cls = GetAttributeValue(element, HocrClass);

            switch (cls)
            {
                case HocrLine:
                    region = ConvertXElementToSegmentedLine(element);
                    break;

                case HocrWord:
                    region = ConvertXElementToWordRegion(element);
                    break;

                default: return null;
            }

            if (region == null)
                return null;

            string title = GetAttributeValue(element, HocrTitle);
            Rectangle rect = GetRectangleFromTitle(title);
            region.Bounds = rect;

            if (cls.Equals(HocrWord))
            {
                region.Angle = getAngleFromBaseLine(element.Parent);
            }

            return region;
        }

        private Rectangle GetRectangleFromTitle(string title)
        {
            int bboxPos = title.IndexOf(HocrBox);

            if (bboxPos >= 0)
            {
                title = title.Substring(bboxPos + 4).Trim();

                string[] parameters = title.Split(new char[] { ';' });

                string rectdata = parameters[0];

                string[] data = rectdata.Split(new char[] { ' ' });

                int x1 = Convert.ToInt32(data[0]);
                int y1 = Convert.ToInt32(data[1]);
                int x2 = Convert.ToInt32(data[2]);
                int y2 = Convert.ToInt32(data[3]);

                return new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }

            return new Rectangle();
        }

        private SegmentedRegion ConvertXElementToSegmentedLine(XElement element)
        {
            foreach (XElement ele in element.Elements())
            {
                if (IsValidWordElement(ele))
                    return null;
            }

            SegmentedRegion region = new SegmentedRegion();
            region.Type = SegmentationType.Line;
            return region;
        }

        private static bool IsValidWordElement(XElement element)
        {
            return GetAttributeValue(element, HocrClass).Equals(HocrWord) && !string.IsNullOrWhiteSpace(element.Value);
        }

        private decimal getAngleFromBaseLine(XElement element)
        {
            try
            {
                string title = GetAttributeValue(element, HocrTitle);

                int startingIndex = title.IndexOf(BaseLine);

                if (startingIndex > -1)
                {
                    int endIndex = title.IndexOf(";", startingIndex);

                    if (endIndex == -1)
                    {
                        endIndex = title.Length;
                    }

                    string baseLineKeyValuePair = title.Substring(startingIndex, endIndex - startingIndex);
                    string[] baseLine = baseLineKeyValuePair.Split(new char[] { ' ' }, StringSplitOptions.None);
                    if (baseLine.Length > 1)
                    {
                        return Convert.ToDecimal(baseLine[1].Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(HOcrToSegmentedDocConverter), nameof(getAngleFromBaseLine), $"Unable to parse element \"{element.ToString()}\"");
            }

            return decimal.Zero;
        }

        private SegmentedRegion ConvertXElementToWordRegion(XElement element)
        {
            SegmentedRegion region = new SegmentedRegion();

            region.Type = SegmentationType.Word;
            region.Text = element.Value;

            string title = GetAttributeValue(element, HocrTitle);

            int xconfPos = title.IndexOf(HocrConfidence);

            if (xconfPos > 0)
            {
                region.Confidence = Convert.ToInt32(title.Substring(xconfPos + HocrConfidence.Length));
            }

            return region;
        }

        private static string GetAttributeValue(XElement element, string attributeName)
        {
            if (element.HasAttributes)
                return element.Attribute(attributeName).Value;
            else
                return string.Empty;
        }
    }
}