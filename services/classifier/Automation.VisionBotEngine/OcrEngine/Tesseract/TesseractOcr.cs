﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Automation.VisionBotEngine.OcrEngine.Tesseract
{
    internal static class Tesseract3_API
    {
        private const string DllName = "Automation.Tesseract.dll";

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern string Scan(string imagePath, string searchString, string languageCodes, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath, string hocrOutputFile);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetPageOrientation(string imagePath, string trainingPath, string applicationPath);
    }

    internal static class Tesseract4_API
    {
        private const string DllNameT4 = "Automation.Tesseract4.0.dll";

        [DllImport(DllNameT4, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetPageOrientation(string imagePath, string trainingPath, string applicationPath, int loglevel, string transactionID);

        [DllImport(DllNameT4, CallingConvention = CallingConvention.Cdecl)]
        public static extern string ScanByLanguage(string imagePath, string searchString, string languageCode, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath, string hOCROutputFile, int loglevel, string transactionID);

        [DllImport(DllNameT4, CallingConvention = CallingConvention.Cdecl)]
        public static extern string ScanBySegmetation(string processedimagePath, string orgImagePath, string fillHolesImagePath, string searchString, string languageCode, string trainingPath, int segmentationMode, int ocrEngineMode, string applicationPath, string hOCROutputFile, int loglevel, string transactionID);
    }
}