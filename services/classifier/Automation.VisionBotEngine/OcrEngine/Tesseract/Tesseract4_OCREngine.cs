﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Threading;
using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Imaging;

namespace Automation.VisionBotEngine.OcrEngine.Tesseract
{
    internal class Tesseract4_OCREngine : IOcrEngine, IPageOrientationProvider
    {
        internal enum OcrEngineMode
        {
            OEM_TESSERACT_ONLY,           // Run Tesseract only - fastest
            OEM_LSTM_ONLY,                // Run just the LSTM line recognizer.
            OEM_TESSERACT_LSTM_COMBINED,  // Run the LSTM recognizer, but allow fallback

            // to Tesseract when things get difficult.
            OEM_DEFAULT,                  // Specify this mode when calling init_*(),

            // to indicate that any of the above modes
            // should be automatically inferred from the
            // variables in the language-specific config,
            // command-line configs, or if not specified
            // in any of the above should be set to the
            // default OEM_TESSERACT_ONLY.
            OEM_CUBE_ONLY,                // Run Cube only - better accuracy, but slower

            OEM_TESSERACT_CUBE_COMBINED,  // Run both and combine results - best accuracy
        };

        private readonly SegmentationMode _segMode = SegmentationMode.PSM_AUTO;
        private readonly OcrEngineMode _engineMode = OcrEngineMode.OEM_TESSERACT_CUBE_COMBINED;
        private string _trainingDataPath { get { return _settings.TessdataPath; } }
        private const string TESSData_FOLDER_NAME = "tessdata_4";
        private readonly Tesseract4Settings _tesseract4Setting = new Tesseract4SettingProvider().GetTesseract4Settings();
        private readonly IHOcrConverter _hocrConvertor;
        private readonly Tesseract4_Settings _settings;

        public const string SKEW_AND_ORIENTATION_CORRECTED_IMAGE_PATH = "SKEW_AND_ORIENTATION_CORRECTED_IMAGE_PATH";

        public Tesseract4_OCREngine(Tesseract4_Settings settings, IHOcrConverter hocrConvertor)
        {
            _settings = GetValidatedSetting(settings);
            _hocrConvertor = hocrConvertor;
        }

        private Tesseract4_Settings GetValidatedSetting(Tesseract4_Settings settingToCheck)
        {
            if (!Directory.Exists(settingToCheck.TessdataPath))
            {
                if (!settingToCheck.TessdataPath.Equals(TESSData_FOLDER_NAME))
                    VBotLogger.Warning(() => $"Non-existent TessdataPath \"{settingToCheck.TessdataPath}\"; reverting to default settings");

                return new Tesseract4_Settings();
            }
            return settingToCheck;
        }

        [HandleProcessCorruptedStateExceptions]
        public SegmentedDocument ExtractRegions(ImageDetails imageDetails, string whiteListingPattern)
        {
            VBotLogger.Trace(() => string.Format("[ExtractRegions] Start region extraction for image '{0}' with data path {1} and language: {2}.", imageDetails.ImagePath, _trainingDataPath, imageDetails.Langugae));

            object skewAndOrientationCorrectedImage = string.Empty;
            if (!imageDetails.AdditionalParameters.TryGetValue(SKEW_AND_ORIENTATION_CORRECTED_IMAGE_PATH
                , out skewAndOrientationCorrectedImage))
            {
                throw new ArgumentException("SKEW_AND_ORIENTATION_CORRECTED_IMAGE_PATH is not provided in additional paramaeter in image details");
            }
            string hocrText = string.Empty;

            try
            {
                hocrText = Scan(imageDetails.ImagePath
                    , whiteListingPattern
                    , _trainingDataPath
                    , _segMode
                    , _engineMode
                    , imageDetails.Langugae
                    , (string)skewAndOrientationCorrectedImage);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Tesseract4_OCREngine), nameof(Scan));
                VBotLogger.Error(() =>
                string.Format("[ExtractRegions] Exception while scanning image '{0}' with data path {1}. Exception : {2}", imageDetails.ImagePath, _trainingDataPath, ex.Message));
            }

            SegmentedDocument segmentedDoc = _hocrConvertor.GetSegmentedDocument(hocrText);

            VBotLogger.Trace(() => string.Format("[ExtractRegions] Successfully extracted {0} system define regions from document {1}. ", segmentedDoc.Regions.Count, imageDetails.ImagePath));

            return segmentedDoc;
        }

        [HandleProcessCorruptedStateExceptions]
        public int GetPageOrientationAngle(string imagePath)
        {
            VBotLogger.Trace(() => string.Format("[PageOrientation] Start Page Orientation for image '{0}'.", imagePath));
            int angle = GetPageOrientation(imagePath);

            VBotLogger.Trace(() => string.Format("[PageOrientation] '{0}' Degree orientation successfull for document {1}. ", angle, imagePath));
            return angle;
        }

        [HandleProcessCorruptedStateExceptions]
        private string Scan(string imagePath, string searchString, string trainingPath, SegmentationMode segMode, OcrEngineMode engineMode, string language, string skewcorrectedFilePath)
        {
            trainingPath = _trainingDataPath;
            VBotLogger.Trace(() => string.Format("[Scan] Start scanning '{0}' with data path {1} and language: {2}.", imagePath, trainingPath, language));
            string appPath = VBotLogger.GetLogFilePath();//VBotLogger.IsLoggingEnabled(VisionBotEngine.LogTypes.Information) ? VBotLogger.GetLogFilePath() :
            int retryCount = 5;
            string hocrText = string.Empty;

            string hocrOutputFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            string languageCode = TesseractHelper.GetTesseractLangugaeCode(language);// "fra";
            searchString = string.Empty;
            string fillHolesImagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".tif");
            do
            {
                try
                {
                    string logFilePath = Path.Combine(Path.GetDirectoryName(appPath), "TesseractLog.log");
                    VBotLogger.Trace(() => "[Scan] Start scanning with tesseract4.0 ...");
                    engineMode = (OcrEngineMode)_tesseract4Setting.OcrEngineMode;
                    segMode = (SegmentationMode)_tesseract4Setting.PageSegmentationMode;
                    VBotLogger.Trace(() => $"[Scan] Log Path: {logFilePath} And LogLevel: {VBotLogger.GetLogLevel()}");

                    if (_tesseract4Setting.UseSegmentation == false
                        || !isLanguageSupportSegmentation(languageCode))
                    {
                        segMode = SegmentationMode.PSM_AUTO;
                        if (languageCode != "eng")
                            engineMode = OcrEngineMode.OEM_TESSERACT_ONLY;
                        hocrText = Tesseract4_API.ScanByLanguage(imagePath, searchString, languageCode, trainingPath, (int)segMode, (int)engineMode, logFilePath, hocrOutputFile, VBotLogger.GetLogLevel(), Path.GetFileName(imagePath));
                    }
                    else
                    {
                        string srcImagePath = skewcorrectedFilePath;

                        ImageOperations imageOperations = new ImageOperations();
                        imageOperations.DetectLines(imagePath, fillHolesImagePath);

                        hocrText = Tesseract4_API.ScanBySegmetation(imagePath, srcImagePath, fillHolesImagePath, searchString, languageCode, trainingPath, (int)segMode, (int)engineMode, logFilePath, hocrOutputFile, VBotLogger.GetLogLevel(), Path.GetFileName(imagePath));
                    }
                    if (File.Exists(hocrOutputFile))
                    {
                        hocrText = File.ReadAllText(hocrOutputFile, System.Text.Encoding.UTF8);
                    }

                    VBotLogger.Trace(() => "[Scan] End scanning...");

                    if (string.IsNullOrEmpty(hocrText))
                    {
                        VBotLogger.Trace(() => string.Format("[Scan] Retry counter -> '{0}' .", retryCount));
                        Thread.Sleep(100);
                        retryCount--;

                        if (retryCount <= 3) return hocrText;
                    }
                    else
                    {
                        return hocrText;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(Tesseract4_OCREngine), nameof(Scan), "Unable to scan image");
                    VBotLogger.Trace(() => string.Format("[Scan] Retry counter -> '{0}' .", retryCount));
                    Thread.Sleep(100);
                    retryCount--;
                }
                finally
                {
                    if (File.Exists(fillHolesImagePath))
                    {
                        File.Delete(fillHolesImagePath);
                    }
                }
            }
            while (retryCount > 0 && string.IsNullOrEmpty(hocrText));

            if (File.Exists(hocrOutputFile))
            {
                File.Delete(hocrOutputFile);
            }

            return hocrText;
        }

        [HandleProcessCorruptedStateExceptions]
        private int GetPageOrientation(string imagePath)
        {
            int retryCount = 1;
            string appPath = VBotLogger.GetLogFilePath();//VBotLogger.IsLoggingEnabled(VisionBotEngine.LogTypes.Information) ? VBotLogger.GetLogFilePath() :
            do
            {
                try
                {
                    string logFilePath = Path.Combine(Path.GetDirectoryName(appPath), "TesseractLog.log");
                    VBotLogger.Trace(() => $"[GetPageOrientation] Log Path: {logFilePath} And LogLevel: {VBotLogger.GetLogLevel()}");
                    return Tesseract4_API.GetPageOrientation(imagePath, _trainingDataPath, logFilePath, VBotLogger.GetLogLevel(), Path.GetFileName(imagePath));
                }
                catch
                {
                    retryCount--;
                    VBotLogger.Trace(() => string.Format("[Scan] Unable to find PageOrientation '{0}' with data path {1}.", imagePath, _trainingDataPath));
                    Thread.Sleep(100);
                }
            }
            while (retryCount > 0);

            return 0;
        }

        private bool isLanguageSupportSegmentation(string languageCode)
        {
            bool supportSegmentation = false;
            switch (languageCode?.ToLower())
            {
                case "eng":
                case "fra":
                case "spa":
                case "ita":
                case "deu":
                    supportSegmentation = true;
                    break;
            }
            return supportSegmentation;
        }
    }
}