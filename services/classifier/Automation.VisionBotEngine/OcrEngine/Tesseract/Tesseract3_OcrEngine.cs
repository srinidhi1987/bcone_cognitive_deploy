﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.OcrEngine.Tesseract
{
    using Automation.Services.Client;
    using Automation.VisionBotEngine.Model;
    using Configuration;
    using System;
    using System.IO;
    using System.Reflection;
    using System.Runtime.ExceptionServices;
    using System.Threading;

    internal class Tesseract3_OCREngine : IOcrEngine, IPageOrientationProvider
    {
        private enum OcrEngineMode
        {
            OEM_TESSERACT_ONLY,           // Run Tesseract only - fastest
            OEM_CUBE_ONLY,                // Run Cube only - better accuracy, but slower
            OEM_TESSERACT_CUBE_COMBINED,  // Run both and combine results - best accuracy
            OEM_DEFAULT                   // Specify this mode when calling init_*(),
                                          // to indicate that any of the above modes
                                          // should be automatically inferred from the
                                          // variables in the language-specific config,
                                          // command-line configs, or if not specified
                                          // in any of the above should be set to the
                                          // default OEM_TESSERACT_ONLY.
        };

        private SegmentationMode _segMode;
        private OcrEngineMode _engineMode;
        private readonly string _trainingDataPath;
        private const string TESSData_FOLDER_NAME = "tessdata";
        private const int DEFAULT_REGION_CONFIDENCE = 100;
        private IHOcrConverter _hocrConvertor;
        private Tesseract3_Settings _settings;

        public Tesseract3_OCREngine(Tesseract3_Settings settings, IHOcrConverter hocrConvertor)
        {
            _settings = settings;
            _trainingDataPath = _settings.TessdataPath;
            _segMode = SegmentationMode.PSM_AUTO;
            _engineMode = OcrEngineMode.OEM_TESSERACT_CUBE_COMBINED;
            _hocrConvertor = hocrConvertor;
        }

        public int GetPageOrientationAngle(string imagePath)
        {
            VBotLogger.Trace(() => string.Format("[PageOrientation] Start Page Orientation for image '{0}'.", imagePath));
            int angle = GetPageOrientation(imagePath, _trainingDataPath);

            VBotLogger.Trace(() => string.Format("[PageOrientation] '{0}' Degree orientation successfull for document {1}. ", angle, imagePath));
            return angle;
        }

        [HandleProcessCorruptedStateExceptions]
        public SegmentedDocument ExtractRegions(ImageDetails imageDetails, string whiteListingPattern = "")
        {
            VBotLogger.Trace(() => string.Format("[ExtractRegions] Start region extraction for image '{0}' with data path {1} and language: {2}.", imageDetails.ImagePath, _trainingDataPath, imageDetails.Langugae));
            string hocrText = string.Empty;

            try
            {
                hocrText = scan(imageDetails.ImagePath, whiteListingPattern, _trainingDataPath, _segMode, _engineMode, imageDetails.Langugae);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Tesseract3_OCREngine), nameof(scan));
                VBotLogger.Error(() => string.Format("[ExtractRegions] Exception while scanning image '{0}' with data path {1}. Exception : {2}", imageDetails.ImagePath, _trainingDataPath, ex.Message));
            }

            SegmentedDocument segmentedDoc = _hocrConvertor.GetSegmentedDocument(hocrText);

            resetRegionConfidenceToDefaultConfidence(segmentedDoc);

            VBotLogger.Trace(() => string.Format("[ExtractRegions] Successfully extracted {0} system define regions from document {1}. ", segmentedDoc.Regions.Count, imageDetails.ImagePath));

            return segmentedDoc;
        }

        private static void resetRegionConfidenceToDefaultConfidence(SegmentedDocument segmentedDoc)
        {
            segmentedDoc.Regions.ForEach(region => region.Confidence = DEFAULT_REGION_CONFIDENCE);
        }

        [HandleProcessCorruptedStateExceptions]
        private int GetPageOrientation(string imagePath, string trainingPath)
        {
            trainingPath = _trainingDataPath;
            int retryCount = 1;
            string appPath = VBotLogger.IsLoggingEnabled(VisionBotEngine.LogTypes.Information) ? VBotSettings.ApplicationPath : string.Empty;
            do
            {
                try
                {
                    return Tesseract3_API.GetPageOrientation(imagePath, trainingPath, appPath);
                }
                catch
                {
                    retryCount--;
                    VBotLogger.Trace(() => string.Format("[Scan] Unable to find PageOrientation '{0}' with data path {1}.", imagePath, trainingPath));
                    Thread.Sleep(100);
                }
            }
            while (retryCount > 0);

            return 0;
        }

        [HandleProcessCorruptedStateExceptions]
        private string scan(string imagePath, string searchString, string trainingPath, SegmentationMode segMode, OcrEngineMode engineMode, string language)
        {
            trainingPath = _trainingDataPath;
            VBotLogger.Trace(() => string.Format("[Scan] Start scanning '{0}' with data path {1} and language: {2}.", imagePath, trainingPath, language));
            string appPath = VBotLogger.IsLoggingEnabled(VisionBotEngine.LogTypes.Information) ? VBotLogger.GetLogFilePath() : string.Empty;// VBotSettings.ApplicationPath : string.Empty;

            string hocrOutputFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            string languageCode = TesseractHelper.GetTesseractLangugaeCode(language);

            string hocrText = string.Empty;

            int retryCount = 5;
            do
            {
                try
                {
                    VBotLogger.Trace(() => string.Format("[Scan] Start Scanning With Tesseract3.04"));

                    hocrText = Tesseract3_API.Scan(imagePath, searchString, languageCode, trainingPath, (int)segMode, (int)engineMode, appPath, hocrOutputFile);
                    if (File.Exists(hocrOutputFile))
                    {
                        hocrText = File.ReadAllText(hocrOutputFile, System.Text.Encoding.UTF8);
                    }

                    VBotLogger.Trace(() => "[Scan] End scanning...");

                    if (string.IsNullOrEmpty(hocrText))
                    {
                        VBotLogger.Trace(() => string.Format("[Scan] Retry counter -> '{0}' .", retryCount));
                        Thread.Sleep(100);
                        retryCount--;

                        if (retryCount <= 3) return hocrText;
                    }
                    else
                    {
                        return hocrText;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(Tesseract3_OCREngine), nameof(scan), "Unable to scan image");
                    VBotLogger.Trace(() => string.Format("[Scan] Retry counter -> '{0}' .", retryCount));
                    Thread.Sleep(100);
                    retryCount--;
                }
            }
            while (retryCount > 0 && string.IsNullOrEmpty(hocrText));
            if (File.Exists(hocrOutputFile))
            {
                File.Delete(hocrOutputFile);
            }

            return hocrText;
        }
    }
}