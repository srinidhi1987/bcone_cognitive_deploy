﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Automation.VisionBotEngine.OcrEngine.Tesseract
{
    internal class NextHOcrToSegmentedDocConverter : IHOcrConverter
    {
        private const string HocrWord = "ocrx_word";
        private const string HocrLine = "ocr_line";
        private const string HocrBox = "bbox";
        private const string HocrClass = "class";
        private const string HocrTitle = "title";
        private const string HocrConfidence = "x_wconf";

        private SegmentedDocument _doc = null;

        public SegmentedDocument GetSegmentedDocument(string hOcrText)
        {
            _doc = new SegmentedDocument();

            if (string.IsNullOrEmpty(hOcrText))
            {
                return _doc;
            }

            try
            {
                XDocument xdoc = XDocument.Parse(hOcrText);
                ParseNodesNew(xdoc.Root);

                handleCaseForFullyOverllapingRegions();
            }
            catch (Exception ex)
            {
            }

            return _doc;
        }

        private void handleCaseForFullyOverllapingRegions()
        {
            var regionsToBeRemoved = new List<SegmentedRegion>();
            foreach (var region in _doc.Regions)
            {
                if (regionsToBeRemoved.Any(x => x == region))
                    continue;

                var overllapingRegions = _doc.Regions.FindAll(x
                     => x.Bounds.Equals(region.Bounds) && x != region);
                if (overllapingRegions.Count > 0)
                {
                    overllapingRegions.ForEach(x => region.Text += " " + x.Text);

                    regionsToBeRemoved.AddRange(overllapingRegions);
                }
            }
            if (regionsToBeRemoved.Count > 0)
            {
                regionsToBeRemoved.ForEach(x => _doc.Regions.Remove(x));
            }
        }

        private int _x = 0;
        private int _y = 0;
        private double _scaleX = 1.0;
        private double _scaleY = 1.0;

        private void ParseNodesNew(XElement xelement)
        {
            foreach (XElement elementDoc in xelement.Elements())
            {
                UpdateBoxbound(elementDoc);

                foreach (XElement element in elementDoc.Elements())
                {
                    SegmentedRegion eleRegion = ConvertXElementToRegion(element);

                    if (eleRegion != null)
                    {
                        updatedRegion(eleRegion);
                        _doc.Regions.Add(eleRegion);
                    }

                    ParseNodesNew(element);
                }
            }
        }

        private void updatedRegion(SegmentedRegion seg)
        {
            seg.Bounds = new Rectangle((int)((seg.Bounds.X / _scaleX) + 0.5) + _x, (int)((seg.Bounds.Y / _scaleY) + 0.5) + _y, (int)((seg.Bounds.Width / _scaleX) + 0.5), (int)((seg.Bounds.Height / _scaleY) + 0.5));
        }

        private void UpdateBoxbound(XElement elementDoc)
        {
            var box_x = (from elementX in elementDoc.Elements()
                         where elementX.Name == "box-x"
                         select elementX).FirstOrDefault();
            if (box_x != null)
                _x = Convert.ToInt32(box_x.Value);
            var box_y = (from elementX in elementDoc.Elements()
                         where elementX.Name == "box-y"
                         select elementX).FirstOrDefault();
            if (box_y != null)
                _y = Convert.ToInt32(box_y.Value);

            var scale_x = (from elementX in elementDoc.Elements()
                           where elementX.Name == "scale-x"
                           select elementX).FirstOrDefault();
            if (scale_x != null)
                _scaleX = Convert.ToInt32(scale_x.Value);

            var scale_y = (from elementX in elementDoc.Elements()
                           where elementX.Name == "scale-y"
                           select elementX).FirstOrDefault();
            if (scale_y != null)
                _scaleY = Convert.ToInt32(scale_y.Value);
        }

        //void ParseNodes(XElement xelement)
        //{
        //    foreach (XElement element in xelement.Elements())
        //    {
        //        SegmentedRegion eleRegion = ConvertXElementToRegion(element);

        //        if (eleRegion != null)
        //        {
        //            updatedRegion(eleRegion);
        //            _doc.Regions.Add(eleRegion);
        //        }

        //        ParseNodes(element);
        //    }

        //}

        private SegmentedRegion ConvertXElementToRegion(XElement element)
        {
            SegmentedRegion region = null;

            string cls = GetAttributeValue(element, HocrClass);

            switch (cls)
            {
                case HocrLine:
                    region = ConvertXElementToSegmentedLine(element);
                    break;

                case HocrWord:
                    region = ConvertXElementToWordRegion(element);
                    break;

                default: return null;
            }

            if (region == null)
                return null;

            string title = GetAttributeValue(element, HocrTitle);
            Rectangle rect = GetRectangleFromTitle(title);

            region.Bounds = rect;
            return region;
        }

        private Rectangle GetRectangleFromTitle(string title)
        {
            int bboxPos = title.IndexOf(HocrBox);

            if (bboxPos >= 0)
            {
                title = title.Substring(bboxPos + 4).Trim();

                string[] parameters = title.Split(new char[] { ';' });

                string rectdata = parameters[0];

                string[] data = rectdata.Split(new char[] { ' ' });

                int x1 = Convert.ToInt32(data[0]);
                int y1 = Convert.ToInt32(data[1]);
                int x2 = Convert.ToInt32(data[2]);
                int y2 = Convert.ToInt32(data[3]);

                return new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }

            return new Rectangle();
        }

        private SegmentedRegion ConvertXElementToSegmentedLine(XElement element)
        {
            foreach (XElement ele in element.Elements())
            {
                if (IsValidWordElement(ele))
                    return null;
            }

            SegmentedRegion region = new SegmentedRegion();
            region.Type = SegmentationType.Line;
            return region;
        }

        private static bool IsValidWordElement(XElement element)
        {
            return GetAttributeValue(element, HocrClass).Equals(HocrWord) && !string.IsNullOrWhiteSpace(element.Value);
        }

        private SegmentedRegion ConvertXElementToWordRegion(XElement element)
        {
            SegmentedRegion region = new SegmentedRegion();

            region.Type = SegmentationType.Word;
            region.Text = element.Value;
            string title = GetAttributeValue(element, HocrTitle);

            int xconfPos = title.IndexOf(HocrConfidence);

            if (xconfPos > 0)
            {
                region.Confidence = Convert.ToInt32(title.Substring(xconfPos + HocrConfidence.Length));
            }

            return region;
        }

        private static string GetAttributeValue(XElement element, string attributeName)
        {
            if (element.HasAttributes)
                return element.Attribute(attributeName).Value;
            else
                return string.Empty;
        }
    }
}