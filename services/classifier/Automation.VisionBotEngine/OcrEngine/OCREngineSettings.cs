﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.OcrEngine
{
    internal class OCREngineSettings
    {
        public Tesseract3_Settings Tesseract3_Settings;
        public Tesseract4_Settings Tesseract4_Settings;
        public OCREngineSettings()
        {
            Tesseract3_Settings = new Tesseract3_Settings();
            Tesseract4_Settings = new Tesseract4_Settings();
        }
    }

    internal class OCRPreProcessingSettings
    {
        public bool IsLineRemoveEnable { get; set; }
        public bool IsBinarizationEnable { get; set; }
        public bool IsSpeckleNoiseRemovalEnable { get; set; }
        public bool IsPatternNoiseRemovalEnable { get; set; }
        public bool IsDarkerBackGroundRemovalEnable { get; set; }
        public bool IsGrayScalingEnable { get; set; }
    }

    internal class Tesseract3_Settings
    {
        public string TessdataPath { get; set; }

        public Tesseract3_Settings()
        {
            TessdataPath = Path.Combine(
                           Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).FullName, @"OCRTrainingData\tessdata");
        }
    }

    internal class Tesseract4_Settings
    {
        public string TessdataPath { get; set; }

        public Tesseract4_Settings()
        {
            TessdataPath = Path.Combine(
                           Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).FullName, @"OCRTrainingData\tessdata_4");
        }
    }
    internal class Abbyy_Settings
    {
        public string EnginePath { get; set; }

        public Abbyy_Settings()
        {
            EnginePath = string.Empty;
        }
    }

}