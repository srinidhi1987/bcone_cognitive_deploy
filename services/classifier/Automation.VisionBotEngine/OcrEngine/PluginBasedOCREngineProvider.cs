﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.OcrEngine
{
    using Automation.Cognitive.VisionBotEngine.Model.Generic;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using Automation.Generic;

    internal class PluginBasedOCREngineProvider
    {
        private readonly string _pluginDetailsFileName;

        private OCREnginePlugins _ocrEnginePlugins;

        public PluginBasedOCREngineProvider()
        {
            _pluginDetailsFileName = Path.Combine(
                               Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CognitiveServiceConfiguration.json");

            InitializeOcrEnginePlugins();
        }

        private void InitializeOcrEnginePlugins()
        {
            if (!File.Exists(_pluginDetailsFileName))
            {
                VBotLogger.Error(() => $"[{nameof(PluginBasedOCREngineProvider)}] configuration file '{_pluginDetailsFileName}' does not exist");
                return;
            }

            string fileContent = null;
            try
            {
                fileContent = File.ReadAllText(_pluginDetailsFileName);
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(PluginBasedOCREngineProvider)}] unable to read from configuration file '{_pluginDetailsFileName}'{Environment.NewLine}encountered exception: {ex.Message}");
                return;
            }

            if (string.IsNullOrWhiteSpace(fileContent))
            {
                VBotLogger.Error(() => $"[{nameof(PluginBasedOCREngineProvider)}] empty configuration file '{_pluginDetailsFileName}'");
                return;
            }

            VBotLogger.Trace(() => $"content of plug-in file {fileContent}");
            try
            {
                _ocrEnginePlugins = JsonConvert.DeserializeObject<OCREnginePlugins>(fileContent, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All });
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(PluginBasedOCREngineProvider)}] invalid content format of file '{_pluginDetailsFileName}'{Environment.NewLine}content: {fileContent}{Environment.NewLine}encountered exception: {ex.Message}");
                return;
            }

            if (_ocrEnginePlugins != null)
            {
                _ocrEnginePlugins.Plugins = _ocrEnginePlugins.Plugins.GetLowerCaseKeyDictionary();
            }
        }

        public IOcrEngine GetOCREngine(string engineType)
        {
            if (engineType == null)
                throw new ArgumentNullException(nameof(engineType));

            engineType = engineType.ToLower();

            VBotLogger.Debug(() => $"GetOCREngine {engineType}");

            if (_ocrEnginePlugins == null)
            {
                VBotLogger.Error(() => $"[{nameof(GetOCREngine)}] unable to initialize ocrEnginePlugins from configuration file '{_pluginDetailsFileName}'");
                return null;
            }

            if (!_ocrEnginePlugins.Plugins.ContainsKey(engineType))
            {
                VBotLogger.Error(() => $"[{nameof(GetOCREngine)}] set ocrEngineType='{engineType}' does not have settings in file '{_pluginDetailsFileName}'");
                return null;
            }

            var OCRPlugin = _ocrEnginePlugins.Plugins[engineType];

            IOcrEngine ocrEngine = null;
            try
            {
                ocrEngine = IntegrationFactory.CreateObject<IOcrEngine>(OCRPlugin.AssemblyName, OCRPlugin.NameSpace, OCRPlugin.ClassName);
            }
            catch (Exception ex)
            {
                throw new Exception($"unable to createObject for IOCREngine with config OCRPlugin.Assembly = '{OCRPlugin.AssemblyName}' OCRPlugin.NameSpace = '{OCRPlugin.NameSpace}' OCRPlugin.ClassName = '{OCRPlugin.ClassName}'",ex);
            }

            if (ocrEngine == null)
                throw new Exception($"Unable to createObject for IOCREngine with config OCRPlugin.Assembly='{OCRPlugin.AssemblyName}' OCRPlugin.NameSpace='{OCRPlugin.NameSpace}' OCRPlugin.ClassName='{OCRPlugin.ClassName}'");

            return ocrEngine;
        }
    }

    internal class OCREnginePlugin
    {
        [JsonProperty(PropertyName = "assembly")]
        public string AssemblyName { get; set; }

        [JsonProperty(PropertyName = "namespace")]
        public string NameSpace { get; set; }

        [JsonProperty(PropertyName = "classname")]
        public string ClassName { get; set; }
    }

    internal class OCREnginePlugins
    {
        [JsonProperty(PropertyName = "ocrengineplugins")]
        public IDictionary<string, OCREnginePlugin> Plugins = new Dictionary<string, OCREnginePlugin>();
    }
}