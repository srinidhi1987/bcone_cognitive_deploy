﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.OcrEngine
{
    using Automation.Cognitive.VisionBotEngine.Properties;
    using Automation.VisionBotEngine.OcrEngine.Tesseract;
    using System;

    public class OcrEngineStratagy
    {
        private PluginBasedOCREngineProvider _pluginBasedOCREngineProvider;

        public IOcrEngine GetOcrEngine(string engineType)
        {
            if (engineType == null) throw new ArgumentNullException(nameof(engineType));

            var lowerCasedEngineType = engineType.Trim().ToLower();

            if (lowerCasedEngineType.Equals(Resources.Tesseract4OcrEngineName.ToLower()))
                return createTesseract4_OCREngine();

            if (lowerCasedEngineType.Equals(Resources.Tesseract3OcrEngineName.ToLower()))
                return createTesseract3_OCREngine();

            if (_pluginBasedOCREngineProvider == null)
                _pluginBasedOCREngineProvider = new PluginBasedOCREngineProvider();

            return _pluginBasedOCREngineProvider.GetOCREngine(engineType);
        }

        private static Tesseract3_OCREngine createTesseract3_OCREngine()
        {
            return new Tesseract3_OCREngine(
                (OCREngineSettingProvider.Instance.GetSettings(Resources.Tesseract3OcrEngineName) as Tesseract3_Settings),
                new HOcrToSegmentedDocConverter());
        }

        private static Tesseract4_OCREngine createTesseract4_OCREngine()
        {
            return new Tesseract4_OCREngine(
                (OCREngineSettingProvider.Instance.GetSettings(Resources.Tesseract4OcrEngineName) as Tesseract4_Settings),
                new NextHOcrToSegmentedDocConverter());
        }
    }
}