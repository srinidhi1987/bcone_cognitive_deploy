﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.OcrEngine
{
    using System;
    using Automation.VisionBotEngine.Model;

    public class OCREngineProviderBasedOnProject
    {
        #region Private Member

        private OcrEngineStratagy _ocrEngineStrategy;

        #endregion Private Member

        #region Constructor

        public OCREngineProviderBasedOnProject(OcrEngineStratagy ocrEngineStrategy)
        {
            if (ocrEngineStrategy == null) throw new ArgumentNullException(nameof(ocrEngineStrategy));

            _ocrEngineStrategy = ocrEngineStrategy;
        }

        #endregion Constructor

        #region Public Member

        public IOcrEngine GetOcrEngine(ProjectDetails projectDetails)
        {
            if (projectDetails.OcrEngineDetails == null ||
                projectDetails.OcrEngineDetails.Length <= 0)
            {
                return null;
            }

            var engineType = projectDetails.OcrEngineDetails[0].EngineType;
            return _ocrEngineStrategy.GetOcrEngine(projectDetails
                                                   .OcrEngineDetails[0]
                                                   .EngineType);
        }

        #endregion Public Member
    }
}