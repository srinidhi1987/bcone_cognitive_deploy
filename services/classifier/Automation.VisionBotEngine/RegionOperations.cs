﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Automation.VisionBotEngine.Model;
using Automation.Cognitive.VisionBotEngine.Common;

namespace Automation.VisionBotEngine
{
    internal class RegionOperations : IRegionOperations
    {
        private List<Field> _scanedFields;
        private List<Field> _findRegionFields;
        private bool _isMatchRegion;

        public RegionOperations(List<Field> fields)
        {
            _scanedFields = fields;
            _findRegionFields = new List<Field>();
        }

        public List<Field> GetRegionsList()
        {
            new FieldOperations().SortFields(_scanedFields);
            if (_scanedFields == null || _scanedFields.Count <= 1)
            {
                return _scanedFields;
            }

            return GetAutoRegions();
        }

        public List<Field> GetAllNearestFields(Field field, FieldDirection fieldDirection = FieldDirection.None)
        {
            List<Field> regionList = new List<Field>();
            _findRegionFields = new List<Field>();
            FindAllNearestFields(field);
            _findRegionFields.Add(field);
            //InsertRegionField(regionList, _findRegionFields);
            return _findRegionFields;
        }

        private List<Field> GetAutoRegions()
        {
            List<Field> regionList = new List<Field>();

            for (int i = 0; i < _scanedFields.Count; i++)
            {
                if (!string.IsNullOrEmpty(_scanedFields[i].ParentId))
                {
                    continue;
                }

                if (VarifyBoundIntersectsAndUpdateBounds(regionList, _scanedFields[i]))
                {
                    continue;
                }

                _findRegionFields = new List<Field>();
                FindAllNearestFields(_scanedFields[i]);
                _findRegionFields.Add(_scanedFields[i]);
                InsertRegionField(regionList, _findRegionFields);
            }

            MeargeOverlapPageRegion(regionList);
            return regionList;
        }

        private void FindingIntersectsRegion(List<Field> regionList, int index)
        {
            for (int j = index + 1; j < regionList.Count; j++)
            {
                if (j == regionList.Count)
                {
                    break;
                }

                if (regionList[index].Bounds.IntersectsWith(regionList[j].Bounds))
                {
                    MergeMatchRegion(regionList, index, regionList[j]);
                }
            }
        }

        private void MergeMatchRegion(List<Field> sourcePageRegion, int index, Field matchPageRegion)
        {
            foreach (Field sr in _scanedFields.FindAll(field => field.ParentId == matchPageRegion.Id))
            {
                sr.ParentId = sourcePageRegion[index].Id;
                sourcePageRegion[index] = AppenedFieldBounds(sourcePageRegion[index], sr);
            }

            sourcePageRegion.Remove(matchPageRegion);
        }

        private void InsertRegionField(List<Field> regionList, List<Field> findFields)
        {
            if (regionList != null && findFields.Count > 0)
            {
                string id = Guid.NewGuid().ToString();
                Field createField = new Field();
                for (int j = 0; j < findFields.Count; j++)
                {
                    findFields[j].ParentId = id;
                    createField = AppenedFieldBounds(createField, findFields[j]);
                }

                createField.Id = id;
                regionList.Add(createField);
            }
        }

        private Field AppenedFieldBounds(Field sourceField, Field meargefield)
        {
            if (sourceField.Bounds.X == 0)
            {
                sourceField.Text = meargefield.Text;
                sourceField.Bounds = new Rectangle(meargefield.Bounds.X,
                                                   meargefield.Bounds.Y,
                                                   meargefield.Bounds.Width,
                                                   meargefield.Bounds.Height);
            }

            {
                int x = sourceField.Bounds.X, y = sourceField.Bounds.Y;
                int width = sourceField.Bounds.Width, height = sourceField.Bounds.Height;
                if (sourceField.Bounds.X > meargefield.Bounds.X)
                {
                    width = width + (x - meargefield.Bounds.X);
                    x = meargefield.Bounds.X;
                }

                if (y > meargefield.Bounds.Y)
                {
                    height = height + y - meargefield.Bounds.Y;
                    y = meargefield.Bounds.Y;
                }

                width = width > ((meargefield.Bounds.X + meargefield.Bounds.Width) - x) ?
                                    width : (meargefield.Bounds.X + meargefield.Bounds.Width) - x;
                height = height > ((meargefield.Bounds.Y + meargefield.Bounds.Height) - y) ?
                                    height : (meargefield.Bounds.Y + meargefield.Bounds.Height) - y;

                sourceField.Bounds = new Rectangle(x, y, width, height);
            }

            return sourceField;
        }

        private void FindAllNearestFields(Field field)
        {
            // if (_findRegionFields.Count > 0 && _findRegionFields.Contains(field)) return;

            FindBelowNearestFields(field);

            FindRightNearestFields(field);
        }

        private void FindRightNearestFields(Field field)
        {
            var foundRegion = GetRightField(field);
            if (foundRegion == null) return;
            foreach (Field sr in foundRegion)
            {
                if (_findRegionFields.Contains(sr)) continue;
                if (sr.Bounds.Y >= field.Bounds.Y && sr.Bounds.Y <= field.Bounds.Y + field.Bounds.Height)
                {
                    _findRegionFields.Add(sr);
                    FindAllNearestFields(sr);
                }
                else if (sr.Bounds.Y + sr.Bounds.Height >= field.Bounds.Y && sr.Bounds.Y <= field.Bounds.Y)
                {
                    _findRegionFields.Add(sr);
                    FindAllNearestFields(sr);
                }
            }
        }

        private void FindBelowNearestFields(Field field)
        {
            List<Field> foundRegion = GetBelowField(field);
            if (foundRegion != null)
            {
                foreach (Field sr in foundRegion)
                {
                    if (_findRegionFields.Contains(sr)) continue;
                    if (sr.Bounds.X >= field.Bounds.X && sr.Bounds.X <= (field.Bounds.X + field.Bounds.Width))
                    {
                        _findRegionFields.Add(sr);
                        FindAllNearestFields(sr);
                    }
                    else if (sr.Bounds.X + sr.Bounds.Width >= field.Bounds.X && sr.Bounds.X <= field.Bounds.X)
                    {
                        _findRegionFields.Add(sr);
                        FindAllNearestFields(sr);
                    }
                }
            }
        }

        private List<Field> GetRightField(Field currentfield)
        {
            List<Field> nearestFields = new List<Field>();

            int charWidth = 0;
            if (!string.IsNullOrEmpty(currentfield.Text) && currentfield.Text.Length > 0)
            {
                charWidth = 3 * (currentfield.Bounds.Width / currentfield.Text.Length) + 1;
            }

            int length = currentfield.Bounds.X + currentfield.Bounds.Width + charWidth;

            foreach (Field sr in _scanedFields)
            {
                if (string.IsNullOrEmpty(sr.ParentId))
                {
                    if (sr.Id != currentfield.Id && sr.Bounds.X >= currentfield.Bounds.X && sr.Bounds.X <= length)
                    {
                        nearestFields.Add(sr);
                    }
                }
            }

            return nearestFields;
        }

        private List<Field> GetBelowField(Field currentfield)
        {
            List<Field> listScanRegion = new List<Field>();
            int yPosition = (currentfield.Bounds.Height * 2) + currentfield.Bounds.Y + 10;

            foreach (Field field in _scanedFields)
            {
                if (string.IsNullOrEmpty(field.ParentId))
                {
                    if (field.Id != currentfield.Id && field.Bounds.Y <= yPosition && field.Bounds.Y >= currentfield.Bounds.Y)
                    {
                        listScanRegion.Add(field);
                    }
                }
            }

            return listScanRegion;
        }

        private bool VarifyBoundIntersectsAndUpdateBounds(List<Field> fields, Field currentfield)
        {
            if (fields == null || fields.Count == 0)
            {
                return false;
            }

            Field pageRegion = fields.Find(region => string.IsNullOrEmpty(region.ParentId)
                                                        && region.Bounds.X <= currentfield.Bounds.X
                                                        && region.Bounds.Y <= currentfield.Bounds.Y
                                                        && region.Bounds.X + region.Bounds.Width >= currentfield.Bounds.X
                                                        && region.Bounds.Y + region.Bounds.Height >= currentfield.Bounds.Y);
            if (pageRegion != null)
            {
                currentfield.ParentId = pageRegion.Id;
                AppenedFieldBounds(pageRegion, currentfield);

                return true;
            }

            return false;
        }

        private void MeargeOverlapPageRegion(List<Field> regionList)
        {
            _isMatchRegion = true;
            while (_isMatchRegion)
            {
                _isMatchRegion = false;
                for (int i = 0; i < regionList.Count; i++)
                {
                    if (i == regionList.Count)
                    {
                        break;
                    }

                    FindingIntersectsRegion(regionList, i);
                }
            }
        }
    }

    internal class NearestMaxRegionBound : IRegionOperations
    {
        private List<Field> _scanedFields = null;
        private List<Field> _findRegionFields;
        private bool _isDirectionMove = false;
        private const double RectWidthFactor = 1.25;
        private List<Line> _lines;

        public NearestMaxRegionBound(List<Field> field, List<Line> lines)
        {
            _lines = lines;
            _scanedFields = new FieldOperations().SortFields(field);
        }

        public List<Field> GetRegionsList()
        {
            return FindNearestFields();
        }

        public List<Field> GetAllNearestFields(Field field, FieldDirection fieldDirection = FieldDirection.None)
        {
            _findRegionFields = new List<Field>();
            FindConnectedFields(field, fieldDirection);

            return _findRegionFields;
        }

        private List<Field> FindNearestFields()
        {
            List<Field> findNearestFields = new List<Field>();
            for (int i = 0; i < _scanedFields.Count; i++)
            {
                _findRegionFields = new List<Field>();
                FindConnectedFields(_scanedFields[i], FieldDirection.None);
                InsertRegionField(findNearestFields, _findRegionFields);
            }
            UpdateRegionFieldText(findNearestFields, _scanedFields);
            return findNearestFields;
        }

        private void InsertRegionField(List<Field> regionList, List<Field> findFields)
        {
            if (regionList != null && findFields.Count > 0)
            {
                string id = Guid.NewGuid().ToString();
                Field createField = new Field();
                for (int j = 0; j < findFields.Count; j++)
                {
                    findFields[j].ParentId = id;
                    createField = AppenedFieldBounds(createField, findFields[j]);
                }
                createField.Confidence = new ConfidenceHelper().GetMergedFieldConfidence(findFields);
                createField.Id = id;
                createField.Type = FieldType.SystemRegion;
                regionList.Add(createField);
            }
        }

        private void UpdateRegionFieldText(List<Field> regionField, List<Field> fieldList)
        {
            for (int i = 0; i < regionField.Count; i++)
            {
                List<Field> fields = fieldList.FindAll(fi => fi.ParentId == regionField[i].Id);

                regionField[i].Text = new FieldOperations().GetTextFromFields(fields);
            }
        }

        private Field AppenedFieldBounds(Field sourceField, Field meargefield)
        {
            if (sourceField.Bounds.X == 0)
            {
                sourceField.Text = meargefield.Text;
                sourceField.Bounds = new Rectangle(meargefield.Bounds.X,
                                                   meargefield.Bounds.Y,
                                                   meargefield.Bounds.Width,
                                                   meargefield.Bounds.Height);
            }

            {
                int x = sourceField.Bounds.X, y = sourceField.Bounds.Y;
                int width = sourceField.Bounds.Width, height = sourceField.Bounds.Height;
                if (sourceField.Bounds.X > meargefield.Bounds.X)
                {
                    width = width + (x - meargefield.Bounds.X);
                    x = meargefield.Bounds.X;
                }

                if (y > meargefield.Bounds.Y)
                {
                    height = height + y - meargefield.Bounds.Y;
                    y = meargefield.Bounds.Y;
                }

                width = width > ((meargefield.Bounds.X + meargefield.Bounds.Width) - x) ?
                                    width : (meargefield.Bounds.X + meargefield.Bounds.Width) - x;
                height = height > ((meargefield.Bounds.Y + meargefield.Bounds.Height) - y) ?
                                    height : (meargefield.Bounds.Y + meargefield.Bounds.Height) - y;

                sourceField.Bounds = new Rectangle(x, y, width, height);
            }

            return sourceField;
        }

        private void FindConnectedFields(Field currentField, FieldDirection fieldDirection)
        {
            if (string.IsNullOrEmpty(currentField.ParentId))
            {
                _findRegionFields.Add(currentField);
                List<Field> findFields = new List<Field>();

                var rectBounds = GetFieldNearestBounds(currentField, fieldDirection);
                RectangleOperations rectOps = new RectangleOperations();

                for (int i = 0; i < _scanedFields.Count; i++)
                {
                    if (!_findRegionFields.Contains(_scanedFields[i])
                        && string.IsNullOrEmpty(_scanedFields[i].ParentId)
                        && rectBounds.IntersectsWith(_scanedFields[i].Bounds)
                        && !rectOps.AreRectsSeperatedByAnyHorizontalLine(currentField.Bounds, _scanedFields[i].Bounds, _lines)
                        && !rectOps.AreRectsSeperatedByAnyVerticalLine(currentField.Bounds, _scanedFields[i].Bounds, _lines))

                    {
                        if (IsValidForRegionMerge(currentField.Bounds.Height, _scanedFields[i].Bounds.Height))
                            findFields.Add(_scanedFields[i]);
                    }
                }

                for (int i = 0; i < findFields.Count; i++)
                {
                    FindConnectedFields(findFields[i], fieldDirection);
                }
            }
        }

        private bool IsValidForRegionMerge(int currentFieldHeight, int meargeFieldHeight)
        {
            return new RectangleOperations().IsHeightToleranceMatch(currentFieldHeight, meargeFieldHeight, 0.35);
        }

        private Rectangle GetFieldNearestBounds(Field field, FieldDirection fieldDirection)
        {
            Rectangle rect = field.Bounds;
            Rectangle returnValue;
            int charWidth = (int)((field.Bounds.Width / field.Text.Length) * RectWidthFactor);
            int charHeight = field.Bounds.Height + (int)(field.Bounds.Height * 0.10);

            if (_isDirectionMove)
            {
                switch (fieldDirection)
                {
                    case FieldDirection.Bottom:
                        returnValue = new Rectangle(rect.X - charWidth, rect.Y, rect.Width + charWidth * 2, rect.Height);
                        break;

                    case FieldDirection.Right:
                        returnValue = new Rectangle(rect.X, rect.Y - charHeight, rect.Width, rect.Height + charHeight * 2);
                        break;

                    default:
                        returnValue = new Rectangle(rect.X - charWidth, rect.Y - charHeight, rect.Width + charWidth * 2, rect.Height + charHeight * 2);
                        break;
                }
            }
            else
            {
                _isDirectionMove = true;
                switch (fieldDirection)
                {
                    case FieldDirection.Bottom:
                        returnValue = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height + charHeight);
                        break;

                    case FieldDirection.Right:
                        returnValue = new Rectangle(rect.X, rect.Y, rect.Width + charWidth, rect.Height);
                        break;

                    default:
                        returnValue = new Rectangle(rect.X - charWidth, rect.Y - charHeight, rect.Width + charWidth * 2, rect.Height + charHeight * 2);
                        break;
                }
            }

            return returnValue;
        }
    }
}