﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using AForge.Imaging.Filters;
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.PdfEngine;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;

    //TODO : Refactoring is pending
    internal class DocumentPageImageProvider : IDocumentPageImageProvider, IDisposable
    {
        private ImageProcessingConfig _config;
        private bool _isCachingOn;

        //TODO: Can we replace this collection with thread safe collection provided by .net?
        private IList<Page> _processedPages = new List<Page>();

        private object _cacheLock = new object();

        private DocumentProperties _documentProperties;

        public DocumentProperties DocumentProperties
        {
            get
            {
                return _documentProperties;
            }
        }

        public DocumentPageImageProvider(DocumentProperties documentProperties, ImageProcessingConfig config, VBotEngineSettings engineSettings, bool cachingOn)
        {
            _documentProperties = documentProperties;
            _config = config;
            _isCachingOn = cachingOn;

            VBotLogger.Trace(() => $"[DocumentPageImageProvider -> Constructor] {nameof(_isCachingOn)} value: {cachingOn.ToString()} ");
        }

        private static object lockObject = new object();

        public Page GetProcessedPageBitmap(int pageIndex, int imageCacheCount)
        {
            VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] Start. Document Properties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
            Page processedPage = null;

            if (!isPagePresent(pageIndex))
            {
                VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] PageIndex not found in cache. DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
                Bitmap pageImage = null;
                try
                {
                    lock (lockObject)
                    {
                        pageImage = GetPage(pageIndex) as Bitmap;
                    }
                    if (pageImage != null)
                    {
                        ImageOperations imageOperations = new ImageOperations();
                        string grayScaledImagePath = _documentProperties.Path + "_Page" + pageIndex.ToString() + ".vimg";
                        DocumentProperties pageDocProperties = imageOperations.GetProcessedImage(pageImage
                            , _config
                            , grayScaledImagePath);

                        VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] imageOperations.GetProcessedImage returned. pageDocProperties is not null: {0} Path: {1}", pageDocProperties != null, pageDocProperties != null ? pageDocProperties.Path : string.Empty));
                        PageProperties pageProperties = createPageProperty(pageIndex, pageDocProperties);
                        Image processedPageImage = imageOperations.CloneImage(pageDocProperties.Path);
                        deleteFile(pageDocProperties.Path);
                        if (processedPageImage != null)
                        {
                            //removeImageCache(imageCacheCount);
                            Page newProcessedPage = new Page(pageProperties, processedPageImage, true);
                            VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] processedPageImage instance created for DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
                            if (_isCachingOn)
                            {
                                addPage(newProcessedPage);
                            }
                            else
                            {
                                processedPage = newProcessedPage;
                            }
                            VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] Page for PageIndex added to cache. DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
                        }
                        else
                        {
                            VBotLogger.Error(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] imageOperations.CloneImage returned null. DocumentProperties Path: {0} PageIndex: {1} ProcessedDocument Path: {2}", _documentProperties.Path, pageIndex, pageDocProperties != null && pageDocProperties.Path != null ? pageDocProperties.Path : string.Empty));
                        }
                    }
                    else
                    {
                        VBotLogger.Error(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] getPage returned null. DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
                    }
                }
                finally
                {
                    if (pageImage != null)
                    {
                        pageImage.Dispose();
                        pageImage = null;
                    }
                }
            }
            else
            {
                VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] PageIndex found in cache. DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
            }

            if (_isCachingOn)
            {
                processedPage = getPageFromCacheAndClone(pageIndex);
            }

            if (processedPage == null)
            {
                VBotLogger.Error(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] Returning null processedPage. DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));
            }
            VBotLogger.Trace(() => string.Format("[DocumentPageImageProvider -> GetProcessedPageBitmap] End. DocumentProperties Path: {0} PageIndex: {1}", _documentProperties.Path, pageIndex));

            return processedPage;
        }

        //private void removeImageCache(int imageCacheCount)
        //{
        //    if (_processedPages == null || _processedPages.Count == 0)
        //        return;

        //    if(imageCacheCount == 0)
        //    {
        //        DisposeProcessPages();
        //    }

        //    if (_processedPages.Count >= imageCacheCount)
        //    {
        //        Page p = _processedPages[0];
        //        p.Dispose();
        //        _processedPages.Remove(p);
        //    }
        //}

        private void DisposeProcessPages()
        {
            foreach (Page p in _processedPages)
            {
                p.Dispose();
            }
            _processedPages.Clear();
        }

        public void Dispose()
        {
            DisposeProcessPages();
        }

        public Bitmap GetPage(int pageIndex)
        {
            Bitmap pageBitmap = null;
            try
            {
                if (isPDFFile(_documentProperties.Path))
                {
                    //TODO: DPI pass through argument
                    PDFBoxEngine pdfEngine = getPdfEngine() as PDFBoxEngine;
                    pageBitmap = pdfEngine.GetPagewiseBitmaps(_documentProperties.Path, 200, pageIndex, 1).FirstOrDefault();
                }
                else
                {
                    ImageOperations imageOperations = new ImageOperations();
                    pageBitmap = imageOperations.GetPagesFromImage(_documentProperties.Path, pageIndex, 1).FirstOrDefault();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            //TODO: Check for null
            return pageBitmap;
        }

        private bool isPDFFile(string path)
        {
            bool isPDFFile = CommonMethods.IsPdfFile(_documentProperties.Path);
            if (!isPDFFile)
            {
                isPDFFile = _documentProperties.Type == DocType.Pdf;
            }

            return isPDFFile;
        }

        private IPdfEngine getPdfEngine()
        {
            return PdfEngineStrategy.GetPdfEngine();
        }

        private static PageProperties createPageProperty(int pageIndex, DocumentProperties pageDocProperties)
        {
            PageProperties pageProperty = new PageProperties();
            pageProperty.PageIndex = pageIndex;
            pageProperty.Height = pageDocProperties.Height;
            pageProperty.Width = pageDocProperties.Width;
            pageProperty.BorderRect = pageDocProperties.BorderRect;
            return pageProperty;
        }

        private bool isPagePresent(int pageIndex)
        {
            bool isPresent = false;
            if (_isCachingOn)
            {
                Page page = null;
                lock (_cacheLock)
                {
                    page = (from q in _processedPages where q.PageProperties.PageIndex == pageIndex select q).FirstOrDefault();
                }

                isPresent = page != null;
            }
            return isPresent;
        }

        private void addPage(Page newPage)
        {
            lock (_cacheLock)
            {
                Page page = (from q in _processedPages where q.PageProperties.PageIndex == newPage.PageProperties.PageIndex select q).FirstOrDefault();
                if (page == null)
                {
                    _processedPages.Add(newPage);
                }
            }
        }

        private Page getPageFromCacheAndClone(int pageIndex)
        {
            Page page = null;
            lock (_cacheLock)
            {
                page = (from q in _processedPages where q.PageProperties.PageIndex == pageIndex select q).FirstOrDefault();
                if (page != null && page.ProcessedPageImage != null)
                {
                    page = page.Clone();
                }
            }
            return page;
        }

        private void deleteFile(string filePath)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DocumentPageImageProvider), nameof(deleteFile));
            }
        }

        public CroppedImage GetCroppedImageFromAbsBounds(int x, int y, int w, int h)
        {
            int relativeY = 0;
            int pageindex = getPageAndPageRelativeHeightFromBounds(x, y, w, h, out relativeY);

            return GetCroppedImageFromRelativeBounds(x, relativeY, w, h, pageindex);
        }

        private int getPageAndPageRelativeHeightFromBounds(int x, int y, int w, int h, out int relativeHeight)
        {
            int i = 0;
            double currentHeight = 0;
            double previousHeight = 0;
            relativeHeight = 0;

            if (_documentProperties != null && _documentProperties.PageProperties != null && _documentProperties.PageProperties.Count > 0)
            {
                while (i < _documentProperties.PageProperties.Count)
                {
                    currentHeight += _documentProperties.PageProperties[i].Height;
                    if (y < currentHeight)
                    {
                        break;
                    }

                    previousHeight = currentHeight;
                    i++;
                }

                relativeHeight = y - Convert.ToInt32(previousHeight);
            }

            return i;
        }

        public CroppedImage GetCroppedImageFromRelativeBounds(int x, int y, int w, int h, int pageIndex)
        {
            CroppedImage croppedImage = null;
            using (Page page = GetProcessedPageBitmap(pageIndex, 1))
            {
                Rectangle fieldRect = new Rectangle(x, y, w, h);
                int borderOffset = 30;
                fieldRect.Inflate(borderOffset, borderOffset);

                Bitmap fieldImage = new Crop(fieldRect).Apply((Bitmap)(page.ProcessedPageImage));
                //double resizeFactorValue = Math.Ceiling(CalculateResizeFactor(fieldImage));
                //VBotLogger.Debug(() => "####################### Resize factor : PSM_AUTO: OEM_TESSERACT_ONLY: " + resizeFactorValue);
                // fieldImage.Save(imgPath + "beforecrop");
                new CanvasCrop(new Rectangle(borderOffset
                    , borderOffset
                    , fieldRect.Width - 2 * borderOffset
                    , fieldRect.Height - 2 * borderOffset))
                    .ApplyInPlace(fieldImage);

                croppedImage = new CroppedImage(fieldImage, fieldRect);
            }

            return croppedImage;
        }
    }
}