﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    public interface ISIROperations
    {
        List<Field> MergeSegmentedFields(List<Field> fields, List<Line> lines);
    }

    internal class SIROperations : ISIROperations
    {
        private const float _mergeRatio = 1.0f;
        private const double _heightTolerance = 0.5f;

        private RectangleOperations _rectOperations = new RectangleOperations();

        public List<Field> MergeSegmentedFields(List<Field> fields, List<Line> lines)
        {
            List<Field> updatetedFields = new List<Field>();

            fields = new FieldOperations().SortFields(fields);

            foreach (Field field in fields)
            {
                if (!string.IsNullOrEmpty(field.Id))
                {
                    continue;
                }

                Field newField = GetRightFieldList(field, fields, lines);

                if (newField != null)
                {
                    updatetedFields.Add(newField);
                }
            }

            updatetedFields = new FieldOperations().SortFields(updatetedFields);
            return updatetedFields;
        }

        private Field GetRightFieldList(Field field, List<Field> fields, List<Line> lines)
        {
            List<Field> allRightFields = fields.FindAll(fi => Math.Abs(fi.Bounds.Top - field.Bounds.Top) < field.Bounds.Height
                                                        && fi.Bounds.Left > (field.Bounds.Left)
                                                        && string.IsNullOrEmpty(fi.Id));

            allRightFields = allRightFields.OrderBy(fi => _rectOperations.GetCenterDistanceBetweenRects(fi.Bounds, field.Bounds)).ToList();
            field.Id = Guid.NewGuid().ToString();
            if (allRightFields.Count > 0)
            {
                Field rightField = field.Clone();

                for (int i = 0; i < allRightFields.Count; i++)
                {
                    if (!IsMergeField(rightField, allRightFields[i], lines))
                    {
                        break;
                    }
                }

                rightField.Id = Guid.NewGuid().ToString();

                if (rightField.Text.Contains("- -"))
                {
                    VBotLogger.Trace(() => string.Format("[GetRightFieldList] Field '{0}' is invalid due to ambiguous pattern. Bounds = {1}", rightField.Text, rightField.Bounds));
                    return null;
                }
                return rightField;
            }

            return field;
        }

        private bool IsRightMeargingField(Field sourceField, Field destField, double mergeRatio)
        {
            int charWidth = sourceField.Text.Length > 0 ? sourceField.Bounds.Width / sourceField.Text.Length : (int)mergeRatio;
            return (sourceField.Bounds.X + sourceField.Bounds.Width + mergeRatio * charWidth) > destField.Bounds.X;
        }

        private void MeargeFieldText(Field field1, Field field2)
        {
            if (field1 != null && field2 != null)
            {
                field1.Text = field1.Text + " " + field2.Text;
                field2.Id = Guid.NewGuid().ToString();
            }
        }

        private bool IsMergeField(Field previousField, Field field, List<Line> lines)
        {
            Field prevField = previousField;

            FieldOperations fieldOps = new FieldOperations();
            RectangleOperations rectOps = new RectangleOperations();

            if (!rectOps.AreRectsSeperatedByAnyVerticalLine(field.Bounds, prevField.Bounds, lines))
            {
                //TODO: [prakash] handle the case where value start with # or :
                if (!fieldOps.IsFieldLable(prevField) &&
                    fieldOps.IsFieldValidForMerge(field) &&
                    field.Bounds.Left > prevField.Bounds.Left &&
                    Math.Abs(field.Bounds.Top - prevField.Bounds.Top) < 20)
                {
                    int xoffset = rectOps.GetXDistanceBetweenRects(field.Bounds, prevField.Bounds);

                    int xmin = fieldOps.GetMinAllowedDistanceToMergeFields(field, prevField) + 1;
                    //xmin = xmin > 4 ? xmin : 4;
                    if (xoffset <= (xmin * _mergeRatio) && xoffset > -5
                        && new RectangleOperations().IsHeightToleranceMatch(prevField.Bounds.Height, field.Bounds.Height, _heightTolerance))
                    {
                        field.Id = Guid.NewGuid().ToString();
                        MergeFieldWithPreviousField(prevField, field, xoffset);
                        return true;
                    }
                }
            }

            return false;
        }

        private void MergeFieldWithPreviousField(Field prevField, Field currField, int xoffset)
        {
            int newWidth = currField.Bounds.Width + (xoffset + prevField.Bounds.Width);
            int newHeight = prevField.Bounds.Height;

            prevField.Bounds = new Rectangle(prevField.Bounds.X, prevField.Bounds.Y, newWidth, newHeight);

            prevField.Text = prevField.Text + " " + currField.Text;

            prevField.Confidence = (prevField.Confidence + currField.Confidence) / 2;
        }
    }
}