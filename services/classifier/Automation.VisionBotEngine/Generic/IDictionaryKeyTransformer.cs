﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Generic
{
    using System;
    using System.Collections.Generic;

    public static class IDictionaryKeyTransformer
    {
        public static IDictionary<string, T> GetLowerCaseKeyDictionary<T>(this IDictionary<string, T> source)
        {
            return source.GetTransformedKeyDictionary((x) => x.ToLower());
        }

        public static IDictionary<string, T> GetUpperCaseKeyDictionary<T>(this IDictionary<string, T> source)
        {
            return source.GetTransformedKeyDictionary((x) => x.ToUpper());
        }

        public static IDictionary<string, T> GetTransformedKeyDictionary<T>(this IDictionary<string, T> source, Func<string, string> transformation, bool removeDuplicate = true)
        {
            var result = new Dictionary<string, T>();

            foreach (KeyValuePair<string, T> sourceItem in source)
            {
                var newKey = transformation(sourceItem.Key);

                if (result.ContainsKey(newKey) && removeDuplicate)
                {
                    continue;
                }

                result.Add(newKey, sourceItem.Value);
            }

            return result;
        }
    }
}