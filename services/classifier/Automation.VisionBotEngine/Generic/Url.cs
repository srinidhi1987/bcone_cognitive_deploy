﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Generic
{
    using System;
    using System.Text;

    public static class UrlUtilities
    {
        public static string Encode(string str)
        {
            if (str == null)
                return null;
            return Encode(str, Encoding.UTF8);
        }

        public static string Encode(string str, Encoding e)
        {
            if (str == null)
                return null;
            return Encoding.ASCII.GetString(EncodeToBytes(str, e));
        }

        public static byte[] EncodeToBytes(string str, Encoding e)
        {
            if (str == null)
                return null;
            byte[] bytes = e.GetBytes(str);
            return Encoder.UrlEncode(bytes, 0, bytes.Length, false /* alwaysCreateNewReturnValue */);
        }

        public static string Decode(string str)
        {
            if (str == null)
                return null;
            return Decode(str, Encoding.UTF8);
        }

        public static string Decode(string str, Encoding e)
        {
            return Encoder.UrlDecode(str, e);
        }

        private class Encoder
        {
            internal static byte[] UrlEncode(byte[] bytes, int offset, int count, bool alwaysCreateNewReturnValue)
            {
                byte[] encoded = UrlEncode(bytes, offset, count);

                return (alwaysCreateNewReturnValue && (encoded != null) && (encoded == bytes))
                    ? (byte[])encoded.Clone()
                    : encoded;
            }

            protected internal static byte[] UrlEncode(byte[] bytes, int offset, int count)
            {
                if (!ValidateUrlEncodingParameters(bytes, offset, count))
                {
                    return null;
                }

                int cSpaces = 0;
                int cUnsafe = 0;

                // count them first
                for (int i = 0; i < count; i++)
                {
                    char ch = (char)bytes[offset + i];

                    if (ch == ' ')
                        cSpaces++;
                    else if (!EncoderUtility.IsUrlSafeChar(ch))
                        cUnsafe++;
                }

                // nothing to expand?
                if (cSpaces == 0 && cUnsafe == 0)
                    return bytes;

                // expand not 'safe' characters into %XX, spaces to +s
                byte[] expandedBytes = new byte[count + cUnsafe * 2];
                int pos = 0;

                for (int i = 0; i < count; i++)
                {
                    byte b = bytes[offset + i];
                    char ch = (char)b;

                    if (EncoderUtility.IsUrlSafeChar(ch))
                    {
                        expandedBytes[pos++] = b;
                    }
                    else if (ch == ' ')
                    {
                        expandedBytes[pos++] = (byte)'+';
                    }
                    else
                    {
                        expandedBytes[pos++] = (byte)'%';
                        expandedBytes[pos++] = (byte)EncoderUtility.IntToHex((b >> 4) & 0xf);
                        expandedBytes[pos++] = (byte)EncoderUtility.IntToHex(b & 0x0f);
                    }
                }

                return expandedBytes;
            }

            internal static bool ValidateUrlEncodingParameters(byte[] bytes, int offset, int count)
            {
                if (bytes == null && count == 0)
                    return false;
                if (bytes == null)
                {
                    throw new ArgumentNullException("bytes");
                }
                if (offset < 0 || offset > bytes.Length)
                {
                    throw new ArgumentOutOfRangeException("offset");
                }
                if (count < 0 || offset + count > bytes.Length)
                {
                    throw new ArgumentOutOfRangeException("count");
                }

                return true;
            }

            internal static string UrlDecode(string value, Encoding encoding)
            {
                if (value == null)
                {
                    return null;
                }

                int count = value.Length;
                UrlDecoder helper = new UrlDecoder(count, encoding);

                // go through the string's chars collapsing %XX and %uXXXX and
                // appending each char as char, with exception of %XX constructs
                // that are appended as bytes

                for (int pos = 0; pos < count; pos++)
                {
                    char ch = value[pos];

                    if (ch == '+')
                    {
                        ch = ' ';
                    }
                    else if (ch == '%' && pos < count - 2)
                    {
                        if (value[pos + 1] == 'u' && pos < count - 5)
                        {
                            int h1 = EncoderUtility.HexToInt(value[pos + 2]);
                            int h2 = EncoderUtility.HexToInt(value[pos + 3]);
                            int h3 = EncoderUtility.HexToInt(value[pos + 4]);
                            int h4 = EncoderUtility.HexToInt(value[pos + 5]);

                            if (h1 >= 0 && h2 >= 0 && h3 >= 0 && h4 >= 0)
                            {   // valid 4 hex chars
                                ch = (char)((h1 << 12) | (h2 << 8) | (h3 << 4) | h4);
                                pos += 5;

                                // only add as char
                                helper.AddChar(ch);
                                continue;
                            }
                        }
                        else
                        {
                            int h1 = EncoderUtility.HexToInt(value[pos + 1]);
                            int h2 = EncoderUtility.HexToInt(value[pos + 2]);

                            if (h1 >= 0 && h2 >= 0)
                            {     // valid 2 hex chars
                                byte b = (byte)((h1 << 4) | h2);
                                pos += 2;

                                // don't add as char
                                helper.AddByte(b);
                                continue;
                            }
                        }
                    }

                    if ((ch & 0xFF80) == 0)
                        helper.AddByte((byte)ch); // 7 bit have to go as bytes because of Unicode
                    else
                        helper.AddChar(ch);
                }

                return Utf16StringValidator.ValidateString(helper.GetString());
            }

            // Internal class to facilitate URL decoding -- keeps char buffer and byte buffer, allows appending of either chars or bytes
            private class UrlDecoder
            {
                private int _bufferSize;

                // Accumulate characters in a special array
                private int _numChars;

                private char[] _charBuffer;

                // Accumulate bytes for decoding into characters in a special array
                private int _numBytes;

                private byte[] _byteBuffer;

                // Encoding to convert chars to bytes
                private Encoding _encoding;

                private void FlushBytes()
                {
                    if (_numBytes > 0)
                    {
                        _numChars += _encoding.GetChars(_byteBuffer, 0, _numBytes, _charBuffer, _numChars);
                        _numBytes = 0;
                    }
                }

                internal UrlDecoder(int bufferSize, Encoding encoding)
                {
                    _bufferSize = bufferSize;
                    _encoding = encoding;

                    _charBuffer = new char[bufferSize];
                    // byte buffer created on demand
                }

                internal void AddChar(char ch)
                {
                    if (_numBytes > 0)
                        FlushBytes();

                    _charBuffer[_numChars++] = ch;
                }

                internal void AddByte(byte b)
                {
                    // if there are no pending bytes treat 7 bit bytes as characters
                    // this optimization is temp disable as it doesn't work for some encodings
                    /*
                                    if (_numBytes == 0 && ((b & 0x80) == 0)) {
                                        AddChar((char)b);
                                    }
                                    else
                    */
                    {
                        if (_byteBuffer == null)
                            _byteBuffer = new byte[_bufferSize];

                        _byteBuffer[_numBytes++] = b;
                    }
                }

                internal String GetString()
                {
                    if (_numBytes > 0)
                        FlushBytes();

                    if (_numChars > 0)
                        return new String(_charBuffer, 0, _numChars);
                    else
                        return String.Empty;
                }
            }
        }

        private static class EncoderUtility
        {
            public static int HexToInt(char h)
            {
                return (h >= '0' && h <= '9') ? h - '0' :
                (h >= 'a' && h <= 'f') ? h - 'a' + 10 :
                (h >= 'A' && h <= 'F') ? h - 'A' + 10 :
                -1;
            }

            public static char IntToHex(int n)
            {
                if (n <= 9)
                    return (char)(n + (int)'0');
                else
                    return (char)(n - 10 + (int)'a');
            }

            // Set of safe chars, from RFC 1738.4 minus '+'
            public static bool IsUrlSafeChar(char ch)
            {
                if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9'))
                    return true;

                switch (ch)
                {
                    case '-':
                    case '_':
                    case '.':
                    case '!':
                    case '*':
                    case '(':
                    case ')':
                        return true;
                }

                return false;
            }

            //  Helper to encode spaces only
            internal static String UrlEncodeSpaces(string str)
            {
                if (str != null && str.IndexOf(' ') >= 0)
                    str = str.Replace(" ", "%20");
                return str;
            }
        }

        private static class Utf16StringValidator
        {
            private const char UNICODE_NULL_CHAR = '\0';
            private const char UNICODE_REPLACEMENT_CHAR = '\uFFFD';

            private static readonly bool _skipUtf16Validation = false;

            public static string ValidateString(string input)
            {
                return ValidateString(input, _skipUtf16Validation);
            }

            // only internal for unit testing
            internal static string ValidateString(string input, bool skipUtf16Validation)
            {
                if (skipUtf16Validation || String.IsNullOrEmpty(input))
                {
                    return input;
                }

                // locate the first surrogate character
                int idxOfFirstSurrogate = -1;
                for (int i = 0; i < input.Length; i++)
                {
                    if (Char.IsSurrogate(input[i]))
                    {
                        idxOfFirstSurrogate = i;
                        break;
                    }
                }

                // fast case: no surrogates = return input string
                if (idxOfFirstSurrogate < 0)
                {
                    return input;
                }

                // slow case: surrogates exist, so we need to validate them
                char[] chars = input.ToCharArray();
                for (int i = idxOfFirstSurrogate; i < chars.Length; i++)
                {
                    char thisChar = chars[i];

                    // If this character is a low surrogate, then it was not preceded by
                    // a high surrogate, so we'll replace it.
                    if (Char.IsLowSurrogate(thisChar))
                    {
                        chars[i] = UNICODE_REPLACEMENT_CHAR;
                        continue;
                    }

                    if (Char.IsHighSurrogate(thisChar))
                    {
                        // If this character is a high surrogate and it is followed by a
                        // low surrogate, allow both to remain.
                        if (i + 1 < chars.Length && Char.IsLowSurrogate(chars[i + 1]))
                        {
                            i++; // skip the low surrogate also
                            continue;
                        }

                        // If this character is a high surrogate and it is not followed
                        // by a low surrogate, replace it.
                        chars[i] = UNICODE_REPLACEMENT_CHAR;
                        continue;
                    }

                    // Otherwise, this is a non-surrogate character and just move to the
                    // next character.
                }
                return new String(chars);
            }
        }
    }
}