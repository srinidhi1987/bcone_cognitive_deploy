﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Generic
{
    using Automation.Generic;
    using Automation.Generic.Compression;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.IO.Packaging;
    using System.Reflection;
    using System.Text.RegularExpressions;

    internal class DiagonalMover
    {
        public enum StartFrom
        {
            X,
            Y
        }

        public static void Move(int width, int height, StartFrom startFrom, Action<Point> logic)
        {
            if (width <= 0 || height <= 0 || logic == null) return;

            int zeroBasedWidth = (startFrom == StartFrom.X ? width : height) - 1;
            int zeroBasedHeight = (startFrom == StartFrom.X ? height : width) - 1;

            var result = new List<int>();

            for (int distance = 0; distance <= zeroBasedWidth + zeroBasedHeight; distance++)
            {
                var widthCrossingValue = distance - zeroBasedWidth;
                var hasDistanceCrossedWidth = widthCrossingValue > 0;

                var x = hasDistanceCrossedWidth ? zeroBasedWidth : distance;
                var y = hasDistanceCrossedWidth ? widthCrossingValue : 0;

                do
                {
                    logic(startFrom == StartFrom.X ? new Point(x, y) : new Point(y, x));

                    x--;
                    y++;
                } while (x >= 0 && y < height);
            }
        }
    }

    internal static class AAConvert
    {
        public static int ToInt32(string value)
        {
            var numberFormats = new List<NumberFormatInfo>
            {
                CultureInfo.CurrentCulture.NumberFormat,
                NumberFormatInfo.InvariantInfo
            };

            int? result = null;
            Exception exception = null;

            foreach (var numberFormat in numberFormats)
            {
                try
                {
                    result = Convert.ToInt32(value, NumberFormatInfo.InvariantInfo);
                    break;
                }
                catch (FormatException)
                {
                    continue;
                }
                catch (Exception ex)
                {
                    exception = ex;
                    break;
                }
            }

            if (result.HasValue)
                return result.Value;

            throw exception;
        }
    }

    public interface IResourcePackage
    {
        void AddFile(string uniqueId, string filepath);

        void AddResource(ResourceData resourceData);

        void Close();

        void Create(string packageName);

        void CreateFromFolder(string packageName, string folderPath, bool includSubFolder);

        void CreateNew(string packageName);

        void DeleteFile(string uniqueId);

        void ExtractToFolder(string packageName, string destinationPath);

        IList<ResourceData> GetAllResources();

        ResourceData GetResource(string uniqueId);

        void Open(string packageName);

        void Open(string packageName, FileAccess fileaccess, FileShare fileshare);
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class VResourcePackage : IResourcePackage, IDisposable
    {
        private CompressionOption _compressionOption = CompressionOption.Maximum;
        private Package _package;

        public void Create(string packageName)
        {
            validatePackageName(packageName);
            validateDuplicatePackage(packageName);

            _package = Package.Open(packageName, FileMode.OpenOrCreate);
        }

        public void CreateNew(string packageName)
        {
            validatePackageName(packageName);
            _package = Package.Open(packageName, FileMode.Create);
        }

        public void Open(string packageName)
        {
            validatePackageName(packageName);
            if (!File.Exists(packageName))
            {
                //throwException(new FileNotFoundException(Properties.Resources.PackageNotFound, packageName));
            }

            _package = Package.Open(packageName, FileMode.Open, FileAccess.ReadWrite);
        }

        public void Open(string packageName, FileAccess fileaccess, FileShare fileshare)
        {
            validatePackageName(packageName);
            if (!File.Exists(packageName))
            {
                // throwException(new FileNotFoundException(Properties.Resources.PackageNotFound, packageName));
            }

            _package = Package.Open(packageName, FileMode.Open, fileaccess, fileshare);
        }

        public void Open(Stream fileStream)
        {
            _package = Package.Open(fileStream, FileMode.Open, FileAccess.ReadWrite);
        }

        public void Close()
        {
            if (_package != null)
            {
                _package.Close();
            }
        }

        public ResourceData GetResource(string uniqueId)
        {
            validatePackageInitialize();
            ResourceData resourcedata = new ResourceData();
            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueId, UriKind.Relative));

            if (_package.PartExists(uri))
            {
                PackagePart packagePart = _package.GetPart(uri);
                resourcedata.UniqueId = packagePart.Uri.ToString();
                resourcedata.ContentType = packagePart.ContentType;
                resourcedata.Content = packagePart.GetStream();
                return resourcedata;
            }

            return resourcedata;
        }

        public IList<ResourceData> GetAllResources()
        {
            IList<ResourceData> resulList = new List<ResourceData>();
            validatePackageInitialize();

            foreach (var packagePart in _package.GetParts())
            {
                ResourceData resourceData = new ResourceData();
                resourceData.UniqueId = packagePart.Uri.ToString();
                resourceData.ContentType = packagePart.ContentType;
                resourceData.Content = packagePart.GetStream();

                resulList.Add(resourceData);
            }

            return resulList;
        }

        public void AddFile(string uniqueid, string filepath)
        {
            validatePackageInitialize();
            validateuniqueid(uniqueid);

            if (!File.Exists(filepath))
            {
                //throwException(new FileNotFoundException(Properties.Resources.FileNotFound, filepath));
            }

            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueid, UriKind.Relative));

            PackagePart packagePart = _package.CreatePart(uri, string.Empty, _compressionOption);

            if (packagePart != null)
            {
                using (FileStream fileStream = new FileStream(
                    filepath,
                    FileMode.Open,
                    FileAccess.Read))
                {
                    fileStream.CopyTo(packagePart.GetStream());
                }
            }
        }

        public void AddResource(ResourceData resourceData)
        {
            validateResourceData(resourceData);
            validatePackageInitialize();

            Uri uri = PackUriHelper.CreatePartUri(new Uri(resourceData.UniqueId, UriKind.Relative));

            PackagePart packagePart = _package.CreatePart(uri, resourceData.ContentType, _compressionOption);

            if (packagePart != null)
            {
                resourceData.Content.CopyTo(packagePart.GetStream());
            }
        }

        public void CreateFromFolder(string packageName, string folderPath, bool includSubFolder)
        {
            if (!Directory.Exists(folderPath))
            {
                //throwException(new DirectoryNotFoundException(Properties.Resources.DirectoryNotFound));
            }

            if (File.Exists(packageName))
            {
                throw new PackageAlreadyExistsException(packageName);
            }

            Create(packageName);

            compressDirectory(folderPath, folderPath, includSubFolder);
        }

        public void ExtractToFolder(string packageName, string destinationPath)
        {
            Open(packageName);

            foreach (var part in _package.GetParts())
            {
                var partUri = part.Uri.OriginalString.Replace('/', Path.DirectorySeparatorChar);
                partUri = Decode(partUri);
                if (partUri.StartsWith(Path.DirectorySeparatorChar.ToString()))
                {
                    partUri = partUri.TrimStart(Path.DirectorySeparatorChar);
                }

                var partFilePath = Path.Combine(destinationPath, partUri);
                var partDirectoryPath = Path.GetDirectoryName(partFilePath);

                if (!Directory.Exists(partDirectoryPath))
                {
                    Directory.CreateDirectory(partDirectoryPath);
                }

                using (FileStream fileStream = File.Create(partFilePath))
                {
                    using (var compressedStream = part.GetStream())
                    {
                        compressedStream.CopyTo(fileStream);
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_package != null)
                {
                    _package.Close();
                    _package = null;
                }
            }
        }

        public void DeleteFile(string uniqueId)
        {
            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueId, UriKind.Relative));
            if (!_package.PartExists(uri))
            {
                throw new Exception("Unable to find item");
            }
            _package.DeletePart(uri);
        }

        private void validateDuplicatePackage(string packageName)
        {
            if (File.Exists(packageName))
            {
                //throw new Exception(Properties.Resources.PackageExist);
            }
        }

        private void validatePackageName(string packageName)
        {
            if (string.IsNullOrEmpty(packageName))
            {
                ////throwException(new ArgumentNullException("packageName", Properties.Resources.ArgumentNullOrBlank));
            }
        }

        private void validatePackageInitialize()
        {
            if (_package == null)
            {
                //throwException(new Exception(Properties.Resources.PackageNotInitialize));
            }
        }

        private void validateResourceData(ResourceData resourceData)
        {
            if (resourceData == null)
            {
                ////throwException(new ArgumentNullException("resourceData", Properties.Resources.ArgumentNullOrBlank));
            }
        }

        private void validateuniqueid(string uniqueid)
        {
            if (string.IsNullOrEmpty(uniqueid))
            {
                //throwException(new ArgumentNullException("uniqueid", Properties.Resources.ArgumentNullOrBlank));
            }

            if (isDuplicateUniqueId(uniqueid))
            {
                //throwException(new Exception(Properties.Resources.DuplicateUniqueID));
            }
        }

        private bool isDuplicateUniqueId(string uniqueid)
        {
            Uri uri = PackUriHelper.CreatePartUri(new Uri(uniqueid, UriKind.Relative));
            return _package.PartExists(uri);
        }

        private void compressDirectory(string rootDirectory, string currentDirectory, bool includeSubFolder)
        {
            string searchDirectory = currentDirectory == string.Empty ? rootDirectory : currentDirectory;

            compressFile(rootDirectory, searchDirectory);

            if (includeSubFolder)
            {
                foreach (string dir in Directory.GetDirectories(searchDirectory))
                {
                    compressDirectory(rootDirectory, dir, true);
                }
            }
        }

        private void compressFile(string rootDirectory, string parentDirectory)
        {
            var searchDirectory = new DirectoryInfo((parentDirectory == string.Empty) ? rootDirectory : parentDirectory);
            foreach (var fileName in searchDirectory.GetFiles())
            {
                var relativePathInCompressedFile = parentDirectory.Replace(rootDirectory, string.Empty);
                Uri partUriDocument = PackUriHelper.CreatePartUri(
                    new Uri(
                        Encode(relativePathInCompressedFile + "\\" + fileName.Name),
                        UriKind.Relative));

                PackagePart file = _package.CreatePart(partUriDocument, string.Empty, this._compressionOption);

                using (var fileStream = fileName.OpenRead())
                {
                    fileStream.CopyTo(file.GetStream());
                }
            }
        }

        private void throwException(Exception exception)
        {
            throw exception;
        }

        private static string Encode(string value)
        {
            return Regex.Replace(UrlUtilities.Encode(value), "%5c", "\\");
        }

        private static string Decode(string value)
        {
            return UrlUtilities.Decode(value);
        }
    }
}