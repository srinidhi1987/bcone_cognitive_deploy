﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine
{
    public interface IRotateRegion
    {
        List<SegmentedRegion> GetRotateRegion(List<SegmentedRegion> segmentedRegions, int angle, int width, int height);
        Rectangle GetRotateRegion(Rectangle fieldRectangle, int angle, int width, int height);
    }
}
