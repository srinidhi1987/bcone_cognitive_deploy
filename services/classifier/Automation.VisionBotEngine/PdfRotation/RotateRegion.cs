﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;
using System.Drawing;

namespace Automation.VisionBotEngine.PdfRotation
{
    internal class RotateRegion : IRotateRegion
    {
        int _width, _height;

        public List<SegmentedRegion> GetRotateRegion(List<SegmentedRegion> segmentedRegions, int angle, int width, int height)
        {
            _width = width;
            _height = height;
            for (int i=0; i< segmentedRegions.Count;i++)
            {
                segmentedRegions[i] = GetRotateField(segmentedRegions[i],angle);
            }

            return segmentedRegions;
        }


        public Rectangle GetRotateRegion(Rectangle fieldRectangle, int angle, int width, int height)
        {
              
            _width = width;
            _height = height;
            return GetRotateRectangle(fieldRectangle, angle);
        }

        private SegmentedRegion GetRotateField(SegmentedRegion segmentedRegion, int angle)
        {
            SegmentedRegion rotateSegmentedRegion = segmentedRegion.Clone();

            rotateSegmentedRegion.Bounds = GetRotateRectangle(segmentedRegion.Bounds, angle);

            return rotateSegmentedRegion;
        }

        private Rectangle GetRotateRectangle(Rectangle bound, int angle)
        {
            Point[] points = new Point[4];

            points[0] = new Point(bound.X, bound.Y);
            points[1] = new Point(bound.Right, bound.Y);
            points[2] = new Point(bound.X, bound.Bottom);
            points[3] = new Point(bound.Right, bound.Bottom);

            return CreateRotateRectangle(points, angle);

        }


        private Rectangle CreateRotateRectangle(Point[] points, int angle)
        {
            switch (angle)
            {
                case 90:
                    return Create90RotateRectangle(points);
                case 180:
                    return Create180RotateRectangle(points);
                case 270:
                    return Create270RotateRectangle(points);
                case 0:
                default:
                    return Create0RotateRectangle(points);
            }
        }

        private Rectangle Create0RotateRectangle(Point[] points)
        {
            return new Rectangle();
        }
        private Rectangle Create90RotateRectangle(Point[] points)
        {
            return new Rectangle(_height-points[2].Y, points[0].X, points[2].Y- points[0].Y,points[1].X -points[0].X);
        }
        private Rectangle Create180RotateRectangle(Point[] points)
        {
            return new Rectangle(_width- points[3].X, _height - points[3].Y, points[3].X- points[2].X, points[3].Y - points[1].Y );
        }
        private Rectangle Create270RotateRectangle(Point[] points)
        {
            //return new Rectangle(points[1], new Size(points[3].X - points[1].X, points[0].Y - points[1].Y));
            return new Rectangle(points[0].Y , _width - points[1].X, points[2].Y -points[0].Y, points[1].X-points[0].X );
        }

        private Point GetRotatePoint(Point oldPoint, int angle)
        {
            int x = oldPoint.X, y = oldPoint.Y;

            int xNew, yNew;

            xNew = Convert.ToInt32(x * Math.Cos(angle) - y * Math.Sin(angle));
            yNew = Convert.ToInt32(x * Math.Sin(angle) + y * Math.Cos(angle));


            return new Point(xNew, yNew);

        }

      
    }
}
