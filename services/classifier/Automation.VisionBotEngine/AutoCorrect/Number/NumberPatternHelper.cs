/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    using Automation.VisionBotEngine.AutoCorrect.Date;

    using Automation.VisionBotEngine.AutoCorrect.Date;

    using Automation.VisionBotEngine.Validation;

    using Automation.VisionBotEngine.Validation;

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal class NumberPatternHelper
    {
        public static bool HasProperWholePartSeparators(string value, int midSequenceLength)
        {
            if (midSequenceLength < 0)
                throw new Exception($"{nameof(midSequenceLength)} cannot be less then zero");

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var groups = value.Split('.', ',');

            if (midSequenceLength == 0)
            {
                return groups.Length <= 1;
            }

            for (int i = groups.Length - 1; i >= 0; i--)
            {
                var group = groups[i];
                var groupLength = group.Length;

                // first group
                if (i == groups.Length - 1)
                {
                    if (i == 0) { return groupLength <= 3; }
                    if (groupLength != 3) { return false; }
                    continue;
                }

                if (i == 0) { return groupLength <= midSequenceLength; }

                if (groupLength != midSequenceLength) { return false; }
            }
            return true;
        }

        public static bool IsValid(string value, int midSequenceLength)
        {
            if (midSequenceLength < 0) { throw new Exception($"{nameof(midSequenceLength)} cannot be less then zero"); }

            if (value == null) { throw new ArgumentNullException(nameof(value)); }

            if (value == string.Empty) { return false; }

            var groups = value.Split(',');

            if (midSequenceLength == 0)
            {
                return groups.Length == 1 && AreAllNumberCharactersOnly(value);
            }

            for (int i = groups.Length - 1; i >= 0; i--)
            {
                var group = groups[i];

                if (!AreAllNumberCharactersOnly(group)) { return false; }

                if (i == 0)
                {
                    var isNegativeNumber = CheckForNegativeNumber(group);
                    if (isNegativeNumber)
                    {
                        group = group.Substring(1);
                    }
                }

                var groupLength = group.Length;

                // first group
                if (i == groups.Length - 1)
                {
                    if (i == 0) { return groupLength <= 3; }
                    if (groupLength != 3) { return false; }
                    continue;
                }

                if (i == 0)
                {
                    return groupLength <= midSequenceLength;
                }

                if (groupLength != midSequenceLength) { return false; }
            }
            return true;
        }

        public static string AutoCorrectWholePartNumber(string value, int midSequenceLength)
        {
            var result = value;
            result = NumberPatternHelper.ApplyCharacterToNumberAutoCorrection(result);
            result = NumberPatternHelper.AutoCorrectWholePartSeparators(result, midSequenceLength);
            return result;
        }

        private static string AutoCorrectWholePartSeparators(string value, int midSequenceLength)
        {
            if (midSequenceLength < 0)
                throw new Exception($"{nameof(midSequenceLength)} cannot be less then zero");

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (midSequenceLength == 0)
            {
                value = value.Replace(" ", "");
                return value;
            }

            var groups = new List<string>();
            var group = string.Empty;
            var maxGroupSize = 3;
            char? foundSeparator = null;
            for (int i = value.Length - 1; i >= 0; i--)
            {
                var c = value[i];

                if (c == ' ')
                {
                    if (group.Length > 0 && group.Length < maxGroupSize) continue;

                    /* group.Length == 0 */
                    foundSeparator = c;
                    continue;
                }
                if (c == '.' || c == ',')
                {
                    if (group.Length == 0)
                    {
                        if (foundSeparator.HasValue && foundSeparator.Value != ' ')
                        {
                            // [confusion] second separator found => dont change anything
                            return value;
                        }

                        foundSeparator = c;
                        continue;
                    }
                    // [confusion] separator not in proper place => dont change anything
                    return value;
                }

                if (groups.Count > 0 && !foundSeparator.HasValue)
                {
                    // [confusion] no separtor available => dont change anything
                    return value;
                }

                group = c + group;

                if (group.Length == maxGroupSize)
                {
                    groups.Add(group);
                    group = string.Empty;
                    maxGroupSize = midSequenceLength;
                    foundSeparator = null;
                }
            }
            if (!string.IsNullOrEmpty(group))
            {
                groups.Add(group);
            }

            if (groups.Count == 1) return groups[0];

            var result = string.Empty;
            for (int i = groups.Count - 1; i >= 0; i--)
            {
                result += groups[i];

                if (i > 0)
                {
                    result += ',';
                }
            }
            return result;
        }

        public static bool AreAllNumberCharactersOnly(string value)
        {
            bool isNegativeNumber = CheckForNegativeNumber(value);
            if (isNegativeNumber)
            {
                if (value.StartsWith("-"))
                {
                    value = value.Substring(1);
                }
                else
                {
                    value = value.Substring(0, value.Length - 1);
                }
            }

            return value.All(char.IsNumber);
        }

        private static bool CheckForNegativeNumber(string value)
        {
            if (value.StartsWith("-") && value.EndsWith("-"))
                return false;
            return (value.StartsWith("-")
                || (value.EndsWith("-") && value.Length > 1 && Char.IsNumber(value[value.Length - 2])));
        }

        public static string ApplyAutoCorrectionToStaticString(string value, string pattern)
        {
            if (AreStaticStringsEqual(value, pattern)) return pattern;

            value = AutoCorrectStaticString(value, pattern);

            return AreStaticStringsEqual(value, pattern) ? pattern : value;
        }

        private static string AutoCorrectStaticString(string value, string pattern)
        {
            foreach (KeyValuePair<char, char[]> currencyAutoCorrectionEntry in CurrencyHelper.CurrencyAutoCorrectionEntries)
            {
                foreach (char possibleToReplace in currencyAutoCorrectionEntry.Value)
                {
                    value = AutoCorrectChar(
                        value,
                        pattern,
                        possibleToReplace,
                        currencyAutoCorrectionEntry.Key);
                }
            }

            return value;
        }

        private static string AutoCorrectChar(string rawValue, string pattern, char toReplace, char replaceWith)
        {
            if (!rawValue.Contains(toReplace) || !pattern.Contains(replaceWith)) return rawValue;

            var result = new StringBuilder();

            var charCount = 0;
            foreach (char charInValue in rawValue)
            {
                if (charInValue == ' ')
                {
                    result.Append(charInValue);
                    continue;
                }

                charCount++;
                result.Append(
                    charInValue == toReplace && HasThisCharAtLocation(pattern, replaceWith, charCount)
                        ? replaceWith
                        : charInValue);
            }

            return result.ToString();
        }

        private static bool HasThisCharAtLocation(string source, char charToCheck, int oneBasedLocation)
        {
            var charCount = 0;
            foreach (char charInSource in source)
            {
                if (charInSource == ' ') continue;

                charCount++;
                if (charCount == oneBasedLocation)
                {
                    return charInSource == charToCheck;
                }
            }
            return false;
        }

        public static bool AreStaticStringsEqual(string value1, string value2)
        {
            return value1.Trim().Replace(" ", "").Equals(value2.Trim().Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase);
        }

        public static string ApplyCharacterToNumberAutoCorrection(string value)
        {
            return DateCorrectionHelper.CorrectAlphabetToNumber(value);
        }
    }
}