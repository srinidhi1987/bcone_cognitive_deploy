/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    using System;
    using System.Linq;

    internal class NumberPattern
    {
        public NumberPattern(string pattern)
        {
            this.Pattern = pattern;
        }

        private NumberPattern(ParsedNumberPattern parsedPattern)
        {
            this.ParsedPattern = parsedPattern;
        }

        public string Pattern { get; private set; }

        private bool IsPatternParsingDone;
        private ParsedNumberPattern __parsedPattern;

        public ParsedNumberPattern ParsedPattern
        {
            get
            {
                if (!this.IsPatternParsingDone)
                {
                    try
                    {
                        this.__parsedPattern = this.GetParsedNumberPattern(this.Pattern);
                    }
                    catch
                    {
                        this.__parsedPattern = null;
                    }

                    this.IsPatternParsingDone = true;
                }

                return this.__parsedPattern;
            }
            private set
            {
                this.__parsedPattern = value;
            }
        }

        public bool IsNumberPatternValid()
        {
            return this.ParsedPattern != null;
        }

        private ParsedNumberPattern GetParsedNumberPattern(string pattern)
        {
            if (string.IsNullOrWhiteSpace(this.Pattern))
                throw new Exception("no pattern specified");

            pattern = pattern.Trim();

            var numberStartIndex = pattern.IndexOf(WholePartNumberComponent.RepresentationCharacter);

            if (numberStartIndex < 0)
                throw new Exception("number pattern missing");

            var result = new ParsedNumberPattern();
            result.OriginalPattern = pattern;

            /* PREFIX */

            if (numberStartIndex == 0)
            {
                result.PrefixComponent = new GenericNumberPatternPrefixComponent();
            }
            else
            {
                result.PrefixComponent = new StaticNumberPatternPrefixComponent(pattern.Substring(0, numberStartIndex));
                pattern = pattern.Substring(numberStartIndex + 1);
            }

            /* VALUE */

            var valueComponent = new NumberPatternValueComponent();
            valueComponent.FractionalPartComponent = new FractionalPartNumberComponent();

            var numberEndIndex = pattern.LastIndexOf(WholePartNumberComponent.RepresentationCharacter);
            var valuePattern = pattern.Substring(0, numberEndIndex + 1);
            pattern = pattern.Substring(numberEndIndex + 1);

            //TODO: check if valuePattern only contains '9' and ',' else throw exception

            if (valuePattern.Contains(WholePartNumberComponent.SeparatorCharacter))
            {
                var splitGroups = valuePattern.Split(WholePartNumberComponent.SeparatorCharacter);

                if (splitGroups.Length < 3)
                    throw new Exception($"invalid whole part pattern \"{valuePattern}\"");

                var groupSize = 0;
                for (int i = splitGroups.Length - 1; i >= 0; i--)
                {
                    var splitGroup = splitGroups[i];//.Replace(" ", "");

                    if (!string.IsNullOrEmpty(splitGroup.Replace("9", "")))
                        throw new Exception($"invalid whole part pattern \"{valuePattern}\"");

                    if (i == splitGroups.Length - 1)
                        if (splitGroup.Length != 3)
                            throw new Exception($"invalid whole part pattern \"{valuePattern}\"");
                        else
                            continue;

                    if (i == splitGroups.Length - 2)
                    {
                        groupSize = splitGroup.Length;
                        if (groupSize != 2 && groupSize != 3)
                            throw new Exception($"invalid whole part pattern \"{valuePattern}\"");
                        continue;
                    }

                    if ((i == 0 && splitGroup.Length > groupSize) ||
                        (i > 0 && splitGroup.Length != groupSize))
                        throw new Exception($"invalid whole part pattern \"{valuePattern}\"");
                }

                if (groupSize == 2)
                    valueComponent.WholePartComponent = new LackSeparatedWholePartNumberComponent();
                else if (groupSize == 3)
                    valueComponent.WholePartComponent = new MillionSeparatedWholePartNumberComponent();
                else
                    throw new Exception($"invalid whole part pattern \"{valuePattern}\"");
            }
            else
            {
                valueComponent.WholePartComponent = new NoSeparatorWholePartNumberComponent();
            }

            var fractionalPattern = string.Empty;
            var fractionalCharIndex = 0;
            if (pattern.Length > 1 && pattern[0] == '.')
            {
                fractionalCharIndex = 1;
                bool hitPostFixChars = false;
                for (; fractionalCharIndex < pattern.Length; fractionalCharIndex++)
                {
                    var patternChar = pattern[fractionalCharIndex];
                    if (patternChar != FractionalPartNumberComponent.RepresentationCharacter)
                    {
                        hitPostFixChars = true;
                        pattern = pattern.Substring(fractionalCharIndex);
                        break;
                    }
                    fractionalPattern += patternChar;
                }
                if (!hitPostFixChars)
                {
                    pattern = string.Empty;
                }
            }
            valueComponent.FractionalPartComponent.AllowedDecimalDigits = fractionalPattern.Length;

            result.PatternValueComponent = valueComponent;

            /* POSTFIX */

            if (string.IsNullOrEmpty(pattern))
            {
                result.PostfixComponent = new GenericNumberPatternPostfixComponent();
            }
            else
            {
                result.PostfixComponent = new StaticNumberPatternPostfixComponent(pattern);
                pattern = string.Empty;
            }

            return result;
        }

        public static bool ShouldPatternBeConsideredAsRegEx(string pattern)
        {
            return !pattern.Contains("999");
        }

        public string GetOnlyNumberPattern()
        {
            if (!this.IsNumberPatternValid()) return string.Empty;

            return this.ParsedPattern.PatternValueComponent.WholePartComponent.Pattern.Replace(",", "") +
                   this.ParsedPattern.PatternValueComponent.FractionalPartComponent.Pattern;
        }
    }
}