/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal abstract class WholePartNumberComponent : INumberComponent
    {
        public static readonly char RepresentationCharacter = '9';

        public static readonly char SeparatorCharacter = ',';

        public abstract string Pattern { get; }

        public abstract void ApplyAutoCorrection();

        public string GetOutputValue()
        {
            return this.IsValid()
                       ? this.ExtractedValue.Replace(SeparatorCharacter + "", "")
                       : this.ExtractedValue;
        }

        public string ExtractedValue { get; protected set; }

        public abstract string ExtractYourValueAndReturnRemainingValue(string value);

        public abstract bool IsValid();
    }
}