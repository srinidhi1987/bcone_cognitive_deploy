/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal abstract class NumberPatternPostfixComponent : INumberComponent
    {
        public string ExtractedValue { get; protected set; } = string.Empty;

        public virtual string Pattern { get; protected set; } = string.Empty;

        public abstract string ExtractYourValueAndReturnRemainingValue(string value);

        public abstract void ApplyAutoCorrection();

        public abstract string GetOutputValue();

        public abstract bool IsValid();
    }
}