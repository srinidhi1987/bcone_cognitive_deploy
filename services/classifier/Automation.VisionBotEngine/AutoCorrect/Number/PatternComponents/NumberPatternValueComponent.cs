/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal class NumberPatternValueComponent : INumberComponent
    {
        public string Pattern => $"{this.WholePartComponent.Pattern}{this.FractionalPartComponent.Pattern}";

        public WholePartNumberComponent WholePartComponent { get; set; }

        public FractionalPartNumberComponent FractionalPartComponent { get; set; }

        public string ExtractedValue => this.WholePartComponent.ExtractedValue + this.FractionalPartComponent.ExtractedValue;

        public void ApplyAutoCorrection()
        {
            this.WholePartComponent.ApplyAutoCorrection();
            this.FractionalPartComponent.ApplyAutoCorrection();
        }

        public string GetOutputValue()
        {
            return this.WholePartComponent.GetOutputValue() + this.FractionalPartComponent.GetOutputValue();
        }

        public string ExtractYourValueAndReturnRemainingValue(string value)
        {
            value = this.FractionalPartComponent.ExtractYourValueAndReturnRemainingValue(value);
            value = this.WholePartComponent.ExtractYourValueAndReturnRemainingValue(value);
            return value;
        }

        public bool IsValid()
        {
            return this.WholePartComponent.IsValid() && this.FractionalPartComponent.IsValid();
        }
    }
}