/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Number
{
    internal class FractionalPartNumberComponent : INumberComponent
    {
        public static readonly char RepresentationCharacter = '0';

        public char FractionalPartSeparator { get; set; } = '.';

        public int AllowedDecimalDigits { get; set; }

        public string ExtractedValue { get; private set; }

        public string Pattern
        {
            get
            {
                var result = string.Empty;

                for (int i = 1; i <= this.AllowedDecimalDigits; i++) result += RepresentationCharacter;

                return this.FractionalPartSeparator + result;
            }
        }

        public string ExtractYourValueAndReturnRemainingValue(string value)
        {
            if (this.AllowedDecimalDigits <= 0) return value;

            var fractionalValue = string.Empty;
            var extractedCharLength = 0;
            for (int i = value.Length - 1; i >= 0; i--)
            {
                var c = value[i];

                if (c == '.' || c == ',')
                {
                    fractionalValue = c + fractionalValue;
                    this.ExtractedValue = fractionalValue;
                    return value.Substring(0, i);
                }

                if (extractedCharLength == this.AllowedDecimalDigits)
                {
                    if (c == ' ')
                    {
                        fractionalValue = c + fractionalValue;
                        continue;
                    }

                    if (fractionalValue[0] == ' ')
                    {
                        this.ExtractedValue = fractionalValue;
                        return value.Substring(0, i + 1);
                    }

                    // no decimal point found
                    this.ExtractedValue = string.Empty;
                    return value;
                }

                fractionalValue = c + fractionalValue;
                if (c == ' ') continue;
                extractedCharLength++;
            }

            this.ExtractedValue = fractionalValue;
            return string.Empty;
        }

        public void ApplyAutoCorrection()
        {
            if (this.IsValid() || string.IsNullOrEmpty(this.ExtractedValue)) return;

            var result = this.ExtractedValue;
            result = NumberPatternHelper.ApplyCharacterToNumberAutoCorrection(result);

            if (result[0] != '.')
            {
                result = '.' + result.Substring(1);
            }

            result = result.Replace(" ", "");
            

            this.ExtractedValue = result;
        }

        public string GetOutputValue()
        {
            return this.ExtractedValue;
        }

        public bool IsValid()
        {
            // fractional part no applicable
            if (this.AllowedDecimalDigits <= 0) { return true; }

            // no extractedValue through pattern is not empty
            if (string.IsNullOrWhiteSpace(this.ExtractedValue)) { return false; }

            // fractional separator character not matching
            if (this.Pattern[0] != this.ExtractedValue[0]) { return false; }

            // only separator character available in extacted value
            if (this.ExtractedValue.Length <= 1) { return false; }

            return NumberPatternHelper.AreAllNumberCharactersOnly(this.ExtractedValue.Substring(1));
        }
    }
}