/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    using System;

    internal class DateAutoCorrector
    {
        public string GetAutoCorrectedValue(string rawValue, string pattern)
        {
            try
            {
                var patternComponents = DatePatternParser.GetDatePatternComponents(pattern);

                var patternComponentsWithValue = DatePatternParser.GetPatternComponentWithValues(
                    rawValue,
                    patternComponents);

                return DatePatternParser.GetAutoCorrectedValue(patternComponentsWithValue);
            }
            catch (Exception ex)
            {
                VBotLogger.Trace(() => string.Format("[AutoCorrection] [Date] [Not possible due to] {0}", ex.Message));
                return rawValue;
            }
        }
    }
}