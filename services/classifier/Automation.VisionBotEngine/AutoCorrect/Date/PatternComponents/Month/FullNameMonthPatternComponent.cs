/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.AutoCorrect.Date
{
    internal class FullNameMonthPatternComponent : MonthPatternComponent
    {
        public override string GetPatternComponent()
        {
            return "MMMM";
        }

        public override string ExtractComponentValue(string value)
        {
            return Extract(value);
        }
        public override string GetAutoCorrectedValue(string value)
        {
            return DateCorrectionHelper.GetBestMatchFullMonthName(DateCorrectionHelper.RemoveWhiteSpaces(value));
        }
        private static string Extract(string value)
        {
            var result = string.Empty;

            foreach (char c in value)
            {
                if (char.IsLetterOrDigit(c))
                {
                    result += c;
                }
                else
                {
                    break;
                }
            }

            return result;
        }
    }
}