﻿using Automation.VisionBotEngine.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;

namespace Automation.VisionBotEngine.Imaging
{
    internal class ImageBlend: IImageBlender
    {
        private BitmapFilterData filterData = new BitmapFilterData();
        private Bitmap bitmapResult = null;

        private string path = null;
        public string imagePath
        {
            get { return path; }
            set { path = value; }
        }

        private Bitmap BlendImage(Bitmap baseImage, Bitmap overlayImage, BitmapFilterData filterData)
        {
            BitmapData baseImageData = baseImage.LockBits(new Rectangle(0, 0, baseImage.Width, baseImage.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            byte[] baseImageBuffer = new byte[baseImageData.Stride * baseImageData.Height];

            Marshal.Copy(baseImageData.Scan0, baseImageBuffer, 0, baseImageBuffer.Length);

            BitmapData overlayImageData = baseImageData; // overlayImage.LockBits(new Rectangle(0, 0, overlayImage.Width, overlayImage.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            byte[] overlayImageBuffer = new byte[overlayImageData.Stride * overlayImageData.Height];

            Marshal.Copy(overlayImageData.Scan0, overlayImageBuffer, 0, overlayImageBuffer.Length);

            float sourceBlue = 0;
            float sourceGreen = 0;
            float sourceRed = 0;

            float overlayBlue = 0;
            float overlayGreen = 0;
            float overlayRed = 0;

            for (int k = 0; k < baseImageBuffer.Length && k < overlayImageBuffer.Length; k += 4)
            {
                sourceBlue = (filterData.SourceBlueEnabled ? baseImageBuffer[k] * filterData.SourceBlueLevel : 0);
                sourceGreen = (filterData.SourceGreenEnabled ? baseImageBuffer[k + 1] * filterData.SourceGreenLevel : 0);
                sourceRed = (filterData.SourceRedEnabled ? baseImageBuffer[k + 2] * filterData.SourceRedLevel : 0);

                overlayBlue = (filterData.OverlayBlueEnabled ? overlayImageBuffer[k] * filterData.OverlayBlueLevel : 0);
                overlayGreen = (filterData.OverlayGreenEnabled ? overlayImageBuffer[k + 1] * filterData.OverlayGreenLevel : 0);
                overlayRed = (filterData.OverlayRedEnabled ? overlayImageBuffer[k + 2] * filterData.OverlayRedLevel : 0);

                baseImageBuffer[k] = CalculateColorComponentBlendValue(sourceBlue, overlayBlue, filterData.BlendTypeBlue);
                baseImageBuffer[k + 1] = CalculateColorComponentBlendValue(sourceGreen, overlayGreen, filterData.BlendTypeGreen);
                baseImageBuffer[k + 2] = CalculateColorComponentBlendValue(sourceRed, overlayRed, filterData.BlendTypeRed);
            }

            Bitmap bitmapResult = new Bitmap(baseImage.Width, baseImage.Height, PixelFormat.Format32bppArgb);
            BitmapData resultImageData = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            Marshal.Copy(baseImageBuffer, 0, resultImageData.Scan0, baseImageBuffer.Length);

            bitmapResult.UnlockBits(resultImageData);
            baseImage.UnlockBits(baseImageData);
            //overlayImage.UnlockBits(overlayImageData);

            return bitmapResult;
        }


        public Bitmap RenderOverlayBitmap(Bitmap srcImg)
        {

            if (srcImg == null)
            {
                return null;
            }

            filterData.BlendTypeBlue = ColorComponentBlendType.Add;
            filterData.BlendTypeGreen = ColorComponentBlendType.Add;
            filterData.BlendTypeRed = ColorComponentBlendType.Add;

            filterData.OverlayBlueEnabled = true;
            filterData.OverlayGreenEnabled = true;
            filterData.OverlayRedEnabled = true;

            filterData.OverlayBlueLevel = (float)25 / 100.0f;
            filterData.OverlayGreenLevel = (float)25 / 100.0f;
            filterData.OverlayRedLevel = (float)25 / 100.0f;

            filterData.SourceBlueEnabled = true;
            filterData.SourceGreenEnabled = true;
            filterData.SourceRedEnabled = true;

            filterData.SourceBlueLevel = (float)100 / 100.0f;
            filterData.SourceGreenLevel = (float)100 / 100.0f;
            filterData.SourceRedLevel = (float)100 / 100.0f;
            Bitmap destImg = srcImg;

            using (Bitmap bmpPictureBoxSource = srcImg)
            {
                using (Bitmap bmpPictureBoxOverlay = destImg)
                {
                    bitmapResult = BlendImage(bmpPictureBoxSource, bmpPictureBoxOverlay, filterData);

                }
            }

            return bitmapResult;
        }

        private byte CalculateColorComponentBlendValue(float source, float overlay, ColorComponentBlendType blendType)
        {
            float resultValue = 0;
            byte resultByte = 0;

            if (blendType == ColorComponentBlendType.Add)
            {
                resultValue = source + overlay;
            }
            else if (blendType == ColorComponentBlendType.Subtract)
            {
                resultValue = source - overlay;
            }
            else if (blendType == ColorComponentBlendType.Average)
            {
                resultValue = (source + overlay) / 2.0f;
            }
            else if (blendType == ColorComponentBlendType.AscendingOrder)
            {
                resultValue = (source > overlay ? overlay : source);
            }
            else if (blendType == ColorComponentBlendType.DescendingOrder)
            {
                resultValue = (source < overlay ? overlay : source);
            }

            if (resultValue > 255)
            {
                resultByte = 255;
            }
            else if (resultValue < 0)
            {
                resultByte = 0;
            }
            else
            {
                resultByte = (byte)resultValue;
            }

            return resultByte;
        }
    }


    internal class BitmapFilterData
    {
        private bool sourceBlueEnabled = false;
        public bool SourceBlueEnabled { get { return sourceBlueEnabled; } set { sourceBlueEnabled = value; } }

        private bool sourceGreenEnabled = false;
        public bool SourceGreenEnabled { get { return sourceGreenEnabled; } set { sourceGreenEnabled = value; } }

        private bool sourceRedEnabled = false;
        public bool SourceRedEnabled { get { return sourceRedEnabled; } set { sourceRedEnabled = value; } }

        private bool overlayBlueEnabled = false;
        public bool OverlayBlueEnabled { get { return overlayBlueEnabled; } set { overlayBlueEnabled = value; } }

        private bool overlayGreenEnabled = false;
        public bool OverlayGreenEnabled { get { return overlayGreenEnabled; } set { overlayGreenEnabled = value; } }

        private bool overlayRedEnabled = false;
        public bool OverlayRedEnabled { get { return overlayRedEnabled; } set { overlayRedEnabled = value; } }

        private float sourceBlueLevel = 1.0f;
        public float SourceBlueLevel { get { return sourceBlueLevel; } set { sourceBlueLevel = value; } }

        private float sourceGreenLevel = 1.0f;
        public float SourceGreenLevel { get { return sourceGreenLevel; } set { sourceGreenLevel = value; } }

        private float sourceRedLevel = 1.0f;
        public float SourceRedLevel { get { return sourceRedLevel; } set { sourceRedLevel = value; } }

        private float overlayBlueLevel = 0.0f;
        public float OverlayBlueLevel { get { return overlayBlueLevel; } set { overlayBlueLevel = value; } }

        private float overlayGreenLevel = 0.0f;
        public float OverlayGreenLevel { get { return overlayGreenLevel; } set { overlayGreenLevel = value; } }

        private float overlayRedLevel = 0.0f;
        public float OverlayRedLevel { get { return overlayRedLevel; } set { overlayRedLevel = value; } }

        private ColorComponentBlendType blendTypeBlue = ColorComponentBlendType.Add;
        public ColorComponentBlendType BlendTypeBlue { get { return blendTypeBlue; } set { blendTypeBlue = value; } }

        private ColorComponentBlendType blendTypeGreen = ColorComponentBlendType.Add;
        public ColorComponentBlendType BlendTypeGreen { get { return blendTypeGreen; } set { blendTypeGreen = value; } }

        private ColorComponentBlendType blendTypeRed = ColorComponentBlendType.Add;
        public ColorComponentBlendType BlendTypeRed { get { return blendTypeRed; } set { blendTypeRed = value; } }       
       
    }

    public enum ColorComponentBlendType
    {
        Add,
        Subtract,
        Average,
        DescendingOrder,
        AscendingOrder
    }
}
