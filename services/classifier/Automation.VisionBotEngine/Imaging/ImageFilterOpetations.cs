﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Imaging
{
    using AForge.Imaging.Filters;

    using System;
    using System.Drawing;
    using System.Drawing.Imaging;

    internal class ImageFilterOpetations : IImageFilterOpetations
    {
        private IImageBlender ib = new ImageBlend();

        public void ConservativeSmoothingFilter(ref Bitmap inputImage)
        {
            const int RedToleranceLevel = 150;
            const int GreenToleranceLevel = 92;
            const int BlueToleranceLevel = 92;
            try
            {
                var smoothingFilter = new ConservativeSmoothing();
                if (smoothingFilter != null && smoothingFilter.FormatTranslations.ContainsKey(inputImage.PixelFormat))
                {
                    smoothingFilter.ApplyInPlace(inputImage);
                    var floodFillFilter = new PointedColorFloodFill();
                    if (floodFillFilter != null && floodFillFilter.FormatTranslations.ContainsKey(inputImage.PixelFormat))
                    {
                        floodFillFilter.Tolerance = Color.FromArgb(RedToleranceLevel, GreenToleranceLevel, BlueToleranceLevel); //Change tolerance level for smoothing image accordingly
                        floodFillFilter.FillColor = Color.FromArgb(255, 255, 255);
                        floodFillFilter.StartingPoint = new AForge.IntPoint(5, 5);
                        floodFillFilter.ApplyInPlace(inputImage);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageFilterOpetations), nameof(ConservativeSmoothingFilter));
            }
        }

        public void RemoveNoise(ref Bitmap noisyImage)
        {
            try
            {
                AForge.Imaging.Filters.FillHoles fillHolesFilter = new AForge.Imaging.Filters.FillHoles();

                fillHolesFilter.MaxHoleWidth = 2;
                fillHolesFilter.MaxHoleHeight = 2;
                fillHolesFilter.CoupledSizeFiltering = true;

                fillHolesFilter.ApplyInPlace(noisyImage);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageFilterOpetations), nameof(RemoveNoise));
            }
        }

        public void RemoveBorder(ref Bitmap inputImage, int borderThickness)
        {
            if (inputImage == null || borderThickness <= 0)
                return;

            if (borderThickness >= inputImage.Width || borderThickness >= inputImage.Height)
                return;

            if (inputImage.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
                return;

            using (Graphics grp = Graphics.FromImage(inputImage))
            {
                using (Pen pen = new Pen(Brushes.White, borderThickness))
                {
                    Rectangle cropRect = new Rectangle(0, 0, inputImage.Width, inputImage.Height);
                    grp.DrawRectangle(pen, cropRect);
                }
            }
        }

        public void CropBorder(ref Bitmap image, Rectangle borderRect, byte backColor = 255)
        {
            AForge.Imaging.Filters.CanvasCrop canvasCrop = new AForge.Imaging.Filters.CanvasCrop(borderRect, backColor);
            image = (Bitmap)canvasCrop.Apply(image);
        }

        public Rectangle GetBorder(Bitmap bmp8bppIndex, int threshold = 128, int offset = 5)
        {
            BitmapData bmpData = bmp8bppIndex.LockBits(new Rectangle(0, 0, bmp8bppIndex.Width, bmp8bppIndex.Height),
                                                        ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            int cropTop = 0;
            int cropBottom = 0;
            int cropRight = 0;
            int cropLeft = 0;

            unsafe
            {
                byte* p = (byte*)(void*)bmpData.Scan0.ToPointer();

                int h = bmp8bppIndex.Height;
                int w = bmp8bppIndex.Width;
                int ws = bmpData.Stride;

                IPageBorderDetector pageBorderDetector = new PageBorderDetector(threshold, offset);

                for (int i = offset; i < h / 2; i++)
                {
                    byte* prevTopRow = &p[(i - 1) * ws];
                    byte* currTopRow = &p[i * ws];
                    byte* nextTopRow = &p[(i + 1) * ws];

                    byte* prevBottomRow = &p[(h - i - 1) * ws];
                    byte* currBottomRow = &p[(h - i) * ws];
                    byte* nextBottomRow = &p[(h - i + 1) * ws];

                    for (int j = offset; j < (w - offset); j++)
                    {
                        if (cropTop == 0 && pageBorderDetector.IsPageBorderFound(currTopRow, prevTopRow, nextTopRow, j))
                        {
                            cropTop = i;
                        }

                        if (cropBottom == 0 && pageBorderDetector.IsPageBorderFound(currBottomRow, prevBottomRow, nextBottomRow, j))
                        {
                            cropBottom = h - i;
                        }

                        if (cropTop > 0 && cropBottom > 0)
                        {
                            break;
                        }
                    }

                    if (cropTop > 0 && cropBottom > 0)
                    {
                        break;
                    }
                }

                for (int j = offset; j < (w / 2); j++)
                {
                    for (int i = offset; i < h - offset; i++)
                    {
                        byte* prevRow = &p[(i - 1) * ws];
                        byte* currRow = &p[i * ws];
                        byte* nextRow = &p[(i + 1) * ws];

                        if (cropLeft == 0 && pageBorderDetector.IsPageBorderFound(currRow, prevRow, nextRow, j))
                        {
                            cropLeft = j;
                        }

                        if (cropRight == 0 && pageBorderDetector.IsPageBorderFound(currRow, prevRow, nextRow, w - j))
                        {
                            cropRight = w - j;
                        }

                        if (cropLeft > 0 && cropRight > 0)
                        {
                            break;
                        }
                    }

                    if (cropLeft > 0 && cropRight > 0)
                    {
                        break;
                    }
                }
            }

            bmp8bppIndex.UnlockBits(bmpData);

            if (cropLeft == 0)
            {
                cropLeft = offset;
            }

            if (cropTop == 0)
            {
                cropTop = offset;
            }

            if (cropBottom == 0)
            {
                cropBottom = bmp8bppIndex.Height - offset;
            }

            if (cropRight == 0)
            {
                cropRight = bmp8bppIndex.Width - offset;
            }

            Rectangle pageRect = new Rectangle(cropLeft, cropTop, cropRight - cropLeft, cropBottom - cropTop);
            pageRect.Inflate(2, 2);

            return pageRect;
        }

        public Bitmap CroppedImage(Bitmap image, Rectangle markerBound)
        {
            Crop filter = new Crop(markerBound);
            return filter.Apply(image);
        }

        public void ResizeImage(ref Bitmap image, double resizeXRatio, double resizeYRatio)
        {
            ResizeNearestNeighbor resizeFilter = new ResizeNearestNeighbor((int)(((double)image.Width) * resizeXRatio),
                                                                     (int)(((double)image.Height) * resizeYRatio));

            image = resizeFilter.Apply(image);
        }
    }
}