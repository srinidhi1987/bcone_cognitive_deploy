﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Imaging
{
    internal class PageBorderDetector : IPageBorderDetector
    {
        public int Threshold { get; }

        public int Offset { get; }

        public PageBorderDetector(int threshold, int offset)
        {
            Threshold = threshold;
            Offset = offset;
        }

        public unsafe bool IsPageBorderFound(byte* currentRow, byte* prevRow, byte* nextRow, int index)
        {
            if (isPageBorderFoundWithTop(currentRow, prevRow, index))
            {
                return true;
            }

            if (isPageBorderFoundWithBottom(currentRow, nextRow, index))
            {
                return true;
            }

            if (isPageBorderFoundWithLeft(currentRow, prevRow, nextRow, index))
            {
                return true;
            }

            if (isPageBorderFoundWithRight(currentRow, prevRow, nextRow, index))
            {
                return true;
            }

            return false;
        }

        private unsafe bool isPageBorderFoundWithBottom(byte* currentRow, byte* nextRow, int index)
        {
            return currentRow[index] < Threshold && currentRow[index + 1] < Threshold && currentRow[index - 1] < Threshold &&
                   nextRow[index] < Threshold && nextRow[index + 1] < Threshold && nextRow[index - 1] < Threshold;
        }

        private unsafe bool isPageBorderFoundWithTop(byte* currentRow, byte* prevRow, int index)
        {
            return (prevRow[index] < Threshold && prevRow[index + 1] < Threshold && prevRow[index - 1] < Threshold &&
                    currentRow[index] < Threshold && currentRow[index + 1] < Threshold && currentRow[index - 1] < Threshold);
        }

        private unsafe bool isPageBorderFoundWithLeft(byte* currentRow, byte* prevRow, byte* nextRow, int index)
        {
            return prevRow[index] < Threshold && prevRow[index - 1] < Threshold && 
                   currentRow[index] < Threshold && currentRow[index - 1] < Threshold &&
                   nextRow[index] < Threshold && nextRow[index - 1] < Threshold;
        }

        private unsafe bool isPageBorderFoundWithRight(byte* currentRow, byte* prevRow, byte* nextRow, int index)
        {
            return prevRow[index] < Threshold && prevRow[index + 1] < Threshold &&
                   currentRow[index] < Threshold && currentRow[index + 1] < Threshold &&
                   nextRow[index] < Threshold && nextRow[index + 1] < Threshold;
        }
    }
}