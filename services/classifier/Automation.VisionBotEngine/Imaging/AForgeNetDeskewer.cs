﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using AForge.Imaging;
using AForge.Imaging.Filters;

using System.Drawing;
using System.Drawing.Imaging;

namespace Automation.VisionBotEngine.Imaging
{
    internal class AForgeNetDeskewer : IImageDeskewer
    {
        public void Deskew(ref Bitmap sourceImage, double angle)
        {
            if (angle > -0.0001 && angle < 0.0001)
            {
                VBotLogger.Trace(() => string.Format("[AForgeNetDeskewer :: Deskew] Skew angle is very small. angle = {0}", angle));
                return;
            }

            if ((angle < -30) || (angle > 30))
            {
                VBotLogger.Trace(() => string.Format("[AForgeNetDeskewer :: Deskew] Unable to rotate image. Unsupported deskew angle = {0}", angle));
                return;
            }

            RotateBilinear rotationFilter = new RotateBilinear(-angle);
            rotationFilter.FillColor = Color.White;

            Bitmap rotatedImage;

            if (sourceImage.PixelFormat != PixelFormat.Format24bppRgb)
            {
                Bitmap img24Bpp = AForge.Imaging.Image.Clone(sourceImage, PixelFormat.Format24bppRgb);
                rotatedImage = rotationFilter.Apply(img24Bpp);
                img24Bpp.Dispose();
            }
            else
            {
                rotatedImage = rotationFilter.Apply(sourceImage);
            }

            sourceImage.Dispose();
            sourceImage = rotatedImage;
        }

        public double GetSkewAngle(Bitmap sourceImage)
        {
            VBotLogger.Trace(() => "[AForgeNetDeskewer :: GetSkewAngle] Get image deskew angle.");

            Bitmap bmp24BppImage = AForge.Imaging.Image.Clone(sourceImage, PixelFormat.Format24bppRgb);
            Bitmap grayImage = Grayscale.CommonAlgorithms.BT709.Apply(bmp24BppImage);

            OtsuThreshold threshold = new OtsuThreshold();
            threshold.ApplyInPlace(grayImage);

            DocumentSkewChecker skewChecker = new DocumentSkewChecker();
            double angle = skewChecker.GetSkewAngle(grayImage);

            bmp24BppImage.Dispose();
            grayImage.Dispose();

            VBotLogger.Trace(() => string.Format("[AForgeNetDeskewer :: GetSkewAngle] Image deskew angle = {0}", angle));

            return angle;
        }

        public void Deskew(ref Bitmap sourceImage)
        {
            VBotLogger.Trace(() => "[AForgeNetDeskewer :: AutoRotate] Start image auto rotation...");

            double angle = GetSkewAngle(sourceImage);

            Deskew(ref sourceImage, angle);

            VBotLogger.Trace(() => "[AForgeNetDeskewer :: AutoRotate] End image auto rotation...");
        }
    }
}