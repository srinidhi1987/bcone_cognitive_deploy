﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Automation.VisionBotEngine.Imaging
{
    public class HoughLineDeskewer : IImageDeskewer
    {
        private class HougLine
        {
            public int Count;
            public int Index;
            public double Alpha;
        }

        const int MAX_LUMINANCE = 140;
        const int MAX_LINE_COUNT = 20;

        const double ALPHA_START = -20;
        const double ALPHA_STEP = 0.2;

        const int STEPS = 40 * 5;
        const double STEP = 1;

        double[] _sinA;
        double[] _cosA;

        double _min;

        int _count;
        int[] _hMatrix;

        public void Deskew(ref Bitmap sourceImage)
        {
            try
            {
                double deskewAngle = GetSkewAngle(sourceImage);
                Deskew(ref sourceImage, -deskewAngle);
            }
            catch (Exception ex)
            {
                VBotLogger.Trace(() => string.Format("[HoughLineDeskewer :: Deskew] Unable to deskew the image. Exception occured -> {0}", ex.Message));
            }
        }

        public void Deskew(ref Bitmap sourceImage, double angle)
        {
            try
            {
                Bitmap rotatedImage = new Bitmap(sourceImage.Width, sourceImage.Height, PixelFormat.Format24bppRgb);
                rotatedImage.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);

                using (Graphics grp = Graphics.FromImage(rotatedImage))
                {
                    grp.FillRectangle(Brushes.White, 0, 0, sourceImage.Width, sourceImage.Height);
                    grp.RotateTransform((float)angle);
                    grp.DrawImage(sourceImage, 0, 0);
                }

                sourceImage.Dispose();               
                sourceImage = rotatedImage;
            }
            catch(Exception ex)
            {
                VBotLogger.Trace(() => string.Format("[HoughLineDeskewer :: Deskew] Unable to deskew the image for given angle. Exception occured -> {0}", ex.Message));
            }
        }

        public double GetSkewAngle(Bitmap sourceImage)
        {
            GenerateHoughLines(sourceImage);

            HougLine[] hougLines = GetTopHoughLines(MAX_LINE_COUNT);

            double sum = 0;
            int count = 0;

            for (int i = 0; i < MAX_LINE_COUNT; i++)
            {
                sum += hougLines[i].Alpha;
                count += 1;
            }

            return sum / count;
        }

        private HougLine[] GetTopHoughLines(int count)
        {
            HougLine[] topHouhLines = new HougLine[count];

            for (int i = 0; i <= count - 1; i++)
            {
                topHouhLines[i] = new HougLine();
            }

            for (int i = 0; i <= _hMatrix.Length - 1; i++)
            {
                if (_hMatrix[i] > topHouhLines[count - 1].Count)
                {
                    topHouhLines[count - 1].Count = _hMatrix[i];
                    topHouhLines[count - 1].Index = i;

                    int j = count - 1;

                    while (j > 0 && topHouhLines[j].Count > topHouhLines[j - 1].Count)
                    {
                        HougLine tmp = topHouhLines[j];

                        topHouhLines[j] = topHouhLines[j - 1];
                        topHouhLines[j - 1] = tmp;
                        j -= 1;
                    }
                }
            }

            for (int i = 0; i <= count - 1; i++)
            {
                int dIndex = topHouhLines[i].Index / STEPS;
                int alphaIndex = topHouhLines[i].Index - dIndex * STEPS;

                topHouhLines[i].Alpha = GetAlpha(alphaIndex);
            }

            return topHouhLines;
        }

        private void GenerateHoughLines(Bitmap sourceImage)
        {
            try
            {
                if (sourceImage != null)
                {
                    int hMin = sourceImage.Height / 4;
                    int hMax = sourceImage.Height * 3 / 4;

                    InitAngles(sourceImage);

                    BitmapData bmpData = sourceImage.LockBits(
                        new Rectangle(0, 0, sourceImage.Width, sourceImage.Height),
                        ImageLockMode.ReadOnly,
                        PixelFormat.Format24bppRgb);

                    unsafe
                    {
                        byte* pData = (byte*)(void*)bmpData.Scan0.ToPointer();

                        for (int y = hMin; y <= hMax; y++)
                        {
                            int currRow = y * bmpData.Stride;
                            int nextRow = (y + 1) * bmpData.Stride;

                            for (int x = 0; x < sourceImage.Width * 3; x += 3)
                            {
                                byte[] pixel1 = { pData[currRow + x], pData[currRow + x + 1], pData[currRow + x + 2] };

                                if (IsBlack(pixel1))
                                {
                                    byte[] pixel2 =
                                        {
                                            pData[nextRow + x], pData[nextRow + x + 1], pData[nextRow + x + 2]
                                        };

                                    if (!IsBlack(pixel2))
                                    {
                                        CalculateHoughLine(x / 3, y);
                                    }
                                }
                            }
                        }
                    }

                    sourceImage.UnlockBits(bmpData);
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Trace(() => string.Format("[GenerateHoughLines] Exception occured -> {0}.", ex.ToString()));
            }
        }

        private void CalculateHoughLine(int x, int y)
        {
            for (int alpha = 0; alpha <= STEPS - 1; alpha++)
            {
                double diff = y * _cosA[alpha] - x * _sinA[alpha];

                int calculatedIndex = (int)CalculateDiffIndex(diff);
                int index = calculatedIndex * STEPS + alpha;

                _hMatrix[index] += 1;
            }
        }

        private double CalculateDiffIndex(double diff)
        {
            return Convert.ToInt32(diff - _min);
        }

        private bool IsBlack(byte[] pixel)
        {
            double luminance = (pixel[0] * 0.299f) + (pixel[1] * 0.587f) + (pixel[2] * 0.114f);
            return luminance < MAX_LUMINANCE;
        }

        private void InitAngles(Bitmap sourceImage)
        {
            _cosA = new double[STEPS];
            _sinA = new double[STEPS];

            for (int i = 0; i < STEPS; i++)
            {
                double angle = GetAlpha(i) * Math.PI / 180.0;

                _sinA[i] = Math.Sin(angle);
                _cosA[i] = Math.Cos(angle);
            }

            _min = -sourceImage.Width;

            _count = (int)(2 * (sourceImage.Width + sourceImage.Height) / STEP);
            _hMatrix = new int[_count * STEPS];
        }

        private static double GetAlpha(int index)
        {
            return ALPHA_START + index * ALPHA_STEP;
        }
    }
}
