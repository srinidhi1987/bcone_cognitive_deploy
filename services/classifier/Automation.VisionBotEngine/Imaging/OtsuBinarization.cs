﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;

namespace Automation.Generic.Imaging
{
    class OtsuBinarization : IImageBinarization
    {
        public void AutoBinarize(string inputImagePath, string outputImagePath)
        {
            Bitmap outputImage = null;
            
            using (Bitmap inputImage = (Bitmap)Image.FromFile(inputImagePath))
            {
                StandardBinarization standardBinarization = new StandardBinarization();

                Bitmap grayScalledImage = standardBinarization.Binarize(inputImage, 0);

                Otsu otsu = new Otsu();
                int threshold = otsu.GetThreshold(grayScalledImage);

                grayScalledImage.Dispose();

                outputImage = standardBinarization.Binarize(inputImage, threshold);
            }
                
            outputImage.Save(outputImagePath);
            outputImage.Dispose();
        }        
    }
}
