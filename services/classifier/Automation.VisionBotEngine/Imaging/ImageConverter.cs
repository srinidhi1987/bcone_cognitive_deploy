﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Drawing;
using System.Drawing.Imaging;

using AForge.Imaging.Filters;

namespace Automation.VisionBotEngine.Imaging
{
    internal class ImageConverter : IImageConverter
    {
        public void ConvertToGrayScale(ref Bitmap sourceImage)
        {
            if (!IsImageValidForGrayScaleConversion(sourceImage))
            {
                if (sourceImage.PixelFormat == PixelFormat.Format8bppIndexed
                    && sourceImage.Palette?.Entries != null
                    && sourceImage.Palette.Entries.Length > 0)
                {
                    if (!(sourceImage.Palette.Entries[0].R == 0
                        && sourceImage.Palette.Entries[0].G == 0
                        && sourceImage.Palette.Entries[0].B == 0))
                    {
                        ReGrayScaleBasedOnFactor(ref sourceImage);
                    }
                }
                return;
            }

            Bitmap outputImage = new Bitmap(sourceImage.Width, sourceImage.Height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = Get256ColorPalette(outputImage);
            int bytesPerPixel = GetBytesPerPixel(sourceImage.PixelFormat);

            BitmapData sourceBmpData = sourceImage.LockBits(new Rectangle(0, 0, sourceImage.Width, sourceImage.Height), ImageLockMode.ReadOnly, sourceImage.PixelFormat);
            BitmapData outputBmpData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            int sourceBmpStride = sourceBmpData.Stride;
            int outputBmpStride = outputBmpData.Stride;

            unsafe
            {
                int row, inputCol, outputCol;

                double rFactor = 0.30f, gFactor = 0.59f, bFactor = 0.11f;

                byte* bmpPtr = (byte*)sourceBmpData.Scan0.ToPointer(),
                outputPtr = (byte*)outputBmpData.Scan0.ToPointer();

                if (bytesPerPixel == 3)
                {
                    //Convert the pixel to it's luminance using the formula:
                    // L = .30*R + .59*G + .11*B
                    //Note that ic is the input column and oc is the output column
                    for (row = 0; row < sourceImage.Height; row++)
                    {
                        for (inputCol = outputCol = 0; outputCol < sourceImage.Width; inputCol += 3, ++outputCol)
                        {
                            outputPtr[row * outputBmpStride + outputCol] = (byte)(int)
                               (rFactor * bmpPtr[row * sourceBmpStride + inputCol] +
                                gFactor * bmpPtr[row * sourceBmpStride + inputCol + 1] +
                                bFactor * bmpPtr[row * sourceBmpStride + inputCol + 2]);
                        }
                    }
                }
                else //bytesPerPixel == 4
                {
                    //Convert the pixel to it's luminance using the formula:
                    // L = alpha * (.3*R + .59*G + .11*B)
                    //Note that ic is the input column and oc is the output column
                    for (row = 0; row < sourceImage.Height; row++)
                    {
                        for (inputCol = outputCol = 0; outputCol < sourceImage.Width; inputCol += 4, ++outputCol)
                        {
                            outputPtr[row * outputBmpStride + outputCol] = (byte)(int)
                                ((bmpPtr[row * sourceBmpStride + inputCol] / 255.0f) *
                                (rFactor * bmpPtr[row * sourceBmpStride + inputCol + 1] +
                                 gFactor * bmpPtr[row * sourceBmpStride + inputCol + 2] +
                                 bFactor * bmpPtr[row * sourceBmpStride + inputCol + 3]));
                        }
                    }
                }
            }

            sourceImage.UnlockBits(sourceBmpData);
            outputImage.UnlockBits(outputBmpData);

            sourceImage.Dispose();
            sourceImage = outputImage;
        }

        public static void ReGrayScaleBasedOnFactor(ref Bitmap sourceImage)
        {
            //if (!IsImageValidForGrayScaleConversion(sourceImage))
            //{
            //    return;
            //}

            Bitmap outputImage = new Bitmap(sourceImage.Width, sourceImage.Height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = Get256ColorPalette(outputImage);
            int bytesPerPixel = 1;// GetBytesPerPixel(sourceImage.PixelFormat);

            Color[] paletteEntries = sourceImage.Palette.Entries;

            BitmapData sourceBmpData = sourceImage.LockBits(new Rectangle(0, 0, sourceImage.Width, sourceImage.Height), ImageLockMode.ReadOnly, sourceImage.PixelFormat);
            BitmapData outputBmpData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            int sourceBmpStride = sourceBmpData.Stride;
            int outputBmpStride = outputBmpData.Stride;

            unsafe
            {
                int row, inputCol, outputCol;

                double rFactor = 0.30f, gFactor = 0.59f, bFactor = 0.11f;

                byte* bmpPtr = (byte*)sourceBmpData.Scan0.ToPointer(),
                outputPtr = (byte*)outputBmpData.Scan0.ToPointer();
                for (row = 0; row < sourceImage.Height; row++)
                {
                    for (inputCol = outputCol = 0; outputCol < sourceImage.Width; inputCol += 1, ++outputCol)
                    {
                        int temp = (int)bmpPtr[row * sourceBmpStride + inputCol];
                        Color color = paletteEntries[temp];
                        // Color color = Color.Black;// sourceImage.Palette.Entries[(int)bmpPtr[row * sourceBmpStride + inputCol]];

                        outputPtr[row * outputBmpStride + outputCol] = (byte)(rFactor * ((int)color.R) +
                        gFactor * ((int)color.G) + bFactor * ((int)color.B));
                    }
                }
            }

            sourceImage.UnlockBits(sourceBmpData);
            outputImage.UnlockBits(outputBmpData);

            sourceImage.Dispose();
            sourceImage = outputImage;
        }

        private static bool IsImageValidForGrayScaleConversion(Bitmap sourceImage)
        {
            return !(sourceImage == null ||
                     AForge.Imaging.Image.IsGrayscale(sourceImage) ||
                     sourceImage.PixelFormat == PixelFormat.Format8bppIndexed ||
                     sourceImage.PixelFormat == PixelFormat.Format1bppIndexed);
        }

        private static int GetBytesPerPixel(PixelFormat pxlFormat)
        {
            int bytesPerPixel;

            switch (pxlFormat)
            {
                case PixelFormat.Format24bppRgb: bytesPerPixel = 3; break;
                case PixelFormat.Format32bppArgb: bytesPerPixel = 4; break;
                case PixelFormat.Format32bppRgb: bytesPerPixel = 4; break;

                default: throw new InvalidOperationException("Image format not supported");
            }

            return bytesPerPixel;
        }

        private static ColorPalette Get256ColorPalette(Bitmap output)
        {
            ColorPalette palette;
            palette = output.Palette;

            for (int i = 0; i < 256; i++)
            {
                Color tmp = Color.FromArgb(255, i, i, i);
                palette.Entries[i] = Color.FromArgb(255, i, i, i);
            }

            output.Palette = palette;
            return palette;
        }

        public void ConvertImageTo8bpp(ref Bitmap SourceImage)
        {
            Grayscale filter = new Grayscale(0.2125, 0.7154, 0.0721);
            if (SourceImage.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                SourceImage = filter.Apply(SourceImage);
            }
        }
    }
}