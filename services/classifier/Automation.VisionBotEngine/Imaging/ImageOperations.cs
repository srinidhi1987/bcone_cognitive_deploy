﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Imaging
{
    using AForge.Imaging.Filters;
    using Automation.VisionBotEngine.Model;
    using OcrEngine.Tesseract;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.ExceptionServices;
    using System.Runtime.InteropServices;
    using Automation.Services.Client;
    using OcrEngine;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class ImageOperations
    {
        internal class PreProcessParmas
        {
            public string convertFile { get; set; } = null;
            public string skewAndOrientationCorrectedImage { get; set; } = null;
        }
        public delegate void GrayscaleFilterApplied(Bitmap filteredImage);

        public delegate void BorderFilterApplied(Bitmap filteredImage, DocumentProperties docProps);

        public event BorderFilterApplied OnBorderFilterApplied;

        public DocumentProperties GetProcessedImage(string imageFilePath
            , ImageProcessingConfig config
            , string convertImagePath)
        {
            VBotLogger.Trace(() => "[GetProcessedImage] Load image for processing...");

            DocumentProperties docProps = null;
            Bitmap image = null;

            try
            {
                image = (Bitmap)CloneImage(imageFilePath);
                docProps = GetProcessedImage(image, config, convertImagePath);

                VBotLogger.Trace(() => "[GetProcessedImage] End image improvement...");
            }
            finally
            {
                if (image != null)
                {
                    image.Dispose();
                    image = null;
                }
            }

            return docProps;
        }

        public DocumentProperties GetProcessedImage(Bitmap sourceImage
            , ImageProcessingConfig config
            , string convertImagePath)
        {
            VBotLogger.Trace(() => "[GetProcessedImageOverloadWithBitmap] Load image for processing...");
            if (Is1BppImageFormat(sourceImage))
            {
                ImageHelper.PreserveImageMetadataSize(ref sourceImage, () =>
                {
                    var oldImage = sourceImage;

                    try
                    {
                        sourceImage = new Bitmap(sourceImage);
                    }
                    finally
                    {
                        oldImage.Dispose();
                    }
                }, !config.PreserveOriginalDpi);
            }
            return this.getDocumentPropertiesForProcessedImage(sourceImage
                , config, convertImagePath);
        }

        internal string MergeImagesintoSingleBitmap(IList<CroppedImage> _cropImageList, double _resizeFactor)
        {
            string imagePath = string.Empty;
            Bitmap image = null;
            try
            {
                int widthMax = 0;
                int heightMax = 0;
                int distance = 20;
                int currentLeft = 0;
                int maxAllowedWidth = 2000;

                int currentRowHeight = 0;
                int currntRowSegment = 0;
                for (int k = 0; k < _cropImageList.Count; k++) //taking 21 seconds
                {
                    CroppedImage CI = _cropImageList[k];
                    int width = (int)(CI.Bounds.Width);
                    int height = (int)(CI.Bounds.Height);

                    if (currentRowHeight < height)
                    {
                        currentRowHeight = height;
                    }
                    if (currentLeft + width > maxAllowedWidth || currntRowSegment > 4)
                    {
                        currentLeft = 0;
                        heightMax = heightMax + currentRowHeight + distance;
                        currentRowHeight = height;
                        currntRowSegment = 0;
                    }

                    CI.DestinationImageBound = new Rectangle(currentLeft, heightMax, width, height);
                    currentLeft = currentLeft + width + 10;
                    if (currentLeft > widthMax)
                    {
                        widthMax = currentLeft;
                    }
                    currntRowSegment++;
                }

                try
                {
                    image = new Bitmap(widthMax + 200, heightMax + 200 + currentRowHeight);
                    if (_cropImageList.Count > 0)
                    {
                        image.SetResolution(_cropImageList[0].GetCroppedImage().HorizontalResolution, _cropImageList[0].GetCroppedImage().VerticalResolution);
                    }
                }
                catch (Exception ex)
                {
                    //  image = new Bitmap(1024, 1024); //TODO: We need to handle this scenario in better way for bitmap creation exception
                    VBotLogger.Warning(() => "[MergesIntoSingleBitmap] Creating default bitmap of fixed size...widthMax: " + widthMax + " heightMax: " + heightMax + " currentRowHeight: " + currentRowHeight);
                    throw ex;
                }
                using (Graphics gs = Graphics.FromImage(image))
                {
                    gs.PageUnit = GraphicsUnit.Pixel;

                    gs.FillRectangle(Brushes.White, new Rectangle(0, 0, image.Width, image.Height));
                    for (int k = 0; k < _cropImageList.Count; k++)
                    {
                        CroppedImage CI = _cropImageList[k];
                        gs.DrawImage(CI.GetCroppedImage(), CI.DestinationImageBound.Location);
                    }
                    gs.Save();
                }

                imagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                image.Save(imagePath);
            }
            finally
            {
                if (_cropImageList != null && _cropImageList.Count > 0)
                {
                    for (int i = 0; i < _cropImageList.Count; i++)
                    {
                        _cropImageList[i]?.Dispose();
                    }
                }

                if (image != null)
                {
                    image.Dispose();
                    image = null;
                }
            }
            return imagePath;
        }

        private DocumentProperties getDocumentPropertiesForProcessedImage(Bitmap sourceImage
            , ImageProcessingConfig config
            , string convertImagePath)
        {
            DocumentProperties docProps = null;
            string acl2ImagePath = null;
            try
            {
                ImageHelper.PreserveImageMetadataSize(ref sourceImage, () =>
                {
                    VBotLogger.Trace(() => "[getDocumentPropertiesForProcessedImage] Load image for processing and return document properties");

                    docProps = new DocumentProperties();
                    PreProcessParmas preProcessParams = new PreProcessParmas();
                    preProcessParams.convertFile = convertImagePath;
                    preProcessParams.skewAndOrientationCorrectedImage = "";
                    if (config.IsTesseract4Engine)
                        preProcessParams.skewAndOrientationCorrectedImage = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                    PreProcessImage(config, ref sourceImage, preProcessParams);

                    if(config.IsTesseract4Engine 
                    && !string.IsNullOrEmpty(preProcessParams.skewAndOrientationCorrectedImage)
                    && File.Exists(preProcessParams.skewAndOrientationCorrectedImage))
                    {
                        sourceImage = this.GetImage(preProcessParams.skewAndOrientationCorrectedImage)?.Bitmap;
                    }

                    using (var grayScaleImage = CloneImage(sourceImage) as Bitmap)
                    {
                        if (config.EnableNoiseFilter)
                        {
                            VBotLogger.Trace(() => $"[{nameof(getDocumentPropertiesForProcessedImage)}] Noise Removal Filter is enabled");

                            var noiseRemovalFacade = new NoiseRemovalFacade();
                            acl2ImagePath = noiseRemovalFacade.RemoveSpacleNoise(sourceImage);
                            if (!string.IsNullOrEmpty(acl2ImagePath))
                            {
                                Bitmap temp = sourceImage;
                                sourceImage = CloneImage(acl2ImagePath) as Bitmap;
                                if (temp != null)
                                {
                                    temp.Dispose();
                                    temp = null;
                                }
                            }
                        }

                        if (sourceImage.PixelFormat != PixelFormat.Format8bppIndexed 
                            && config.IsTesseract4Engine == false)
                            ApplyGrayscaleFilter(config, ref sourceImage);

                        ApplyLineRemoval(config, ref sourceImage);

                        ApplyContrastStrech(config, ref sourceImage);

                        ApplyBinarizationFilter(config, ref sourceImage);

                        ApplyBackgroundRemovalFilter(config, ref sourceImage, grayScaleImage);
                    }

                    //ApplyNoiseFilter(config, ref sourceImage);

                    ApplyBorderFilter(config, ref sourceImage, ref docProps);

                    docProps.Path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                    if (File.Exists(docProps.Path))
                    {
                        File.Delete(docProps.Path);
                    }

                    docProps.Width = sourceImage.Width;
                    docProps.Height = sourceImage.Height;
                }, !config.PreserveOriginalDpi);

                sourceImage.Save(docProps.Path);

                VBotLogger.Trace(() => "[getDocumentPropertiesForProcessedImage] End image improvement...");
            }
            finally
            {
                sourceImage.Dispose();
                sourceImage = null;
                if (File.Exists(acl2ImagePath))
                {
                    File.Delete(acl2ImagePath);
                }
            }

            return docProps;
        }
        public void DetectLines(string sourceImagePath, string detectedLinesImgaePath)
        {
            Bitmap tempImg = new Bitmap(sourceImagePath);
            try
            {
                FillHoles(tempImg, detectedLinesImgaePath);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(DetectLines));
            }
            finally
            {
                if (tempImg != null)
                    tempImg.Dispose();
            }
        }
        internal void ApplyLineRemoval(ImageProcessingConfig config, ref Bitmap sourceImage)
        {
            if (config.EnableRemoveLines)
            {
                Bitmap inputImage = sourceImage;

                string acl2ImagePath = "";

                try
                {
                    acl2ImagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".png");
                    inputImage.Save(acl2ImagePath);
                    VBotLogger.Debug(() => "[Starting]: Line removal preprocessing on image..");
                    PatternRecognizer.RemoveLines(acl2ImagePath);
                    VBotLogger.Debug(() => "[Ending]: Line removal preprocessing on image..");
                    sourceImage = CloneImage(acl2ImagePath) as Bitmap;
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplyLineRemoval));
                }
                finally
                {
                    if (sourceImage != inputImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                    if (File.Exists(acl2ImagePath))
                    {
                        File.Delete(acl2ImagePath);
                    }
                }
            }
        }

        internal void ApplySaltAndPepperFilterForRGB(ImageProcessingConfig config, ref Bitmap sourceImage)
        {
            // if (config.EnableNoiseFilter)
            {
                VBotLogger.Trace(() => $"[{nameof(ApplySaltAndPepperFilterForRGB)}] Start smoothing and noise removal...");
                Bitmap inputImage = null;
                //Bitmap firstPassImage = null;
                //Bitmap secondPassImage = null;
                Bitmap finalimage = null;
                IImageBlender imageBlend = new ImageBlend();
                try
                {
                    inputImage = CloneImage(sourceImage) as Bitmap;
                    IImageFilterOpetations imgSaltAndPepper = new ImageFilterOpetations();
                    imgSaltAndPepper.ConservativeSmoothingFilter(ref inputImage);
                    Sharpen sh = new Sharpen();
                    if (inputImage != null)
                    {
                        finalimage = imageBlend.RenderOverlayBitmap(inputImage);//First pass

                        if (sh != null && sh.FormatTranslations.ContainsKey(finalimage.PixelFormat))
                        {
                            sh.ApplyInPlace(finalimage);
                        }
                        sourceImage.Dispose();

                        sourceImage = finalimage;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplySaltAndPepperFilterForRGB));

                    if (finalimage != null)
                    {
                        finalimage.Dispose();
                    }
                }
                finally
                {
                    if (inputImage != null)
                    {
                        inputImage.Dispose();
                    }
                }
            }
        }

        internal int GetPageOrientation(string docImagePath)
        {
            //TODO: Pass page orientation provider as dependancy
            PageOrientationProviderStratagy pageOrientationProviderStratagy = new PageOrientationProviderStratagy();
            IPageOrientationProvider pageOrientationProvide = pageOrientationProviderStratagy.GetPageOrientationProvider();

            string imagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".png");
            try
            {
                bool exceptionOccuredInCustomPreprocessing = false;
                try
                {
                    File.Copy(docImagePath, imagePath, true);
                    PatternRecognizer.ApplyPreprocessingForRotation(imagePath);
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(VBotEngine), nameof(GetPageOrientation));
                    exceptionOccuredInCustomPreprocessing = true;
                }

                return pageOrientationProvide.GetPageOrientationAngle(
                    exceptionOccuredInCustomPreprocessing ? docImagePath : imagePath);
            }
            finally
            {
                if (File.Exists(imagePath))
                {
                    File.Delete(imagePath);
                }
            }
        }

        private void PreProcessImage(ImageProcessingConfig config
            , ref Bitmap sourceImage
            ,PreProcessParmas preProcessParams)
        {
            try
            {
                ApplyAutoRotation(config, ref sourceImage);

                ApplyOrientation(config, ref sourceImage);

                SavePreProcessImage(sourceImage, preProcessParams.skewAndOrientationCorrectedImage);

                ApplyGrayscaleFilter(config, ref sourceImage);

                SavePreProcessImage(sourceImage, preProcessParams.convertFile);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(PreProcessImage));
            }
        }

        private void ApplyBackgroundRemovalFilter(ImageProcessingConfig config, ref Bitmap sourceImage, Bitmap withoutBinarizeImage)
        {
            VBotLogger.Trace(() => $"[{nameof(ApplyBackgroundRemovalFilter)}] BackgroundRemovelEnbled : {config.EnbleBackgroundRemoval}");

            if (config.EnbleBackgroundRemoval && sourceImage != null)
            {
                Bitmap inputImage = sourceImage;
                Bitmap cloneImage = null;
                try
                {
                    var blackBackgroundBoxes = new BackgroundDetector().DetectColorBackgroundBoxes(sourceImage);

                    if (blackBackgroundBoxes != null && blackBackgroundBoxes.Count > 0)
                    {
                        VBotLogger.Trace(() => $"[{nameof(ApplyBackgroundRemovalFilter)}] DetectedBoxes : {blackBackgroundBoxes.Count}");

                        cloneImage = CloneImage(sourceImage) as Bitmap;

                        AForge.Imaging.Filters.Invert filterINV = new AForge.Imaging.Filters.Invert();
                        foreach (BackgroundBox box in blackBackgroundBoxes)
                        {
                            var cropRectangle = new Rectangle(box.Dimensions.Left + 2, box.Dimensions.Top + 2, box.Dimensions.Width - 2, box.Dimensions.Height - 4);

                            AForge.Imaging.Filters.Crop filterCrop = new AForge.Imaging.Filters.Crop(cropRectangle);

                            Bitmap croppedImage = filterCrop.Apply(withoutBinarizeImage);

                            ImageProcessingConfig configForBackgroundRemove = new ImageProcessingConfig();
                            configForBackgroundRemove.EnableGrayscaleConversion = true;

                            ApplyGrayscaleFilter(configForBackgroundRemove, ref croppedImage);

                            IImageBinarizer binarizer = GetImageBinarizer(config);
                            binarizer.AutoBinarize(ref croppedImage);

                            if (hasDarkerBackground(croppedImage))
                            {
                                filterINV.ApplyInPlace(croppedImage);
                            }

                            for (int i = 0; i < croppedImage.Width; i++)
                            {
                                for (int j = 0; j < croppedImage.Height; j++)
                                {
                                    if (cloneImage.PixelFormat == PixelFormat.Format8bppIndexed)
                                    {
                                        set8bppIndexedPixel(ref cloneImage, cropRectangle.Left + i, cropRectangle.Top + j, croppedImage.GetPixel(i, j));
                                    }
                                    else
                                    {
                                        cloneImage.SetPixel(cropRectangle.Left + i, cropRectangle.Top + j, croppedImage.GetPixel(i, j));
                                    }
                                }
                            }

                            croppedImage.Dispose();
                        }

                        sourceImage = cloneImage;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplyBackgroundRemovalFilter));
                    if (cloneImage != null)
                    {
                        cloneImage.Dispose();
                        cloneImage = null;
                    }
                }
                finally
                {
                    if (inputImage != null && inputImage != sourceImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                }
            }
        }

        private static void set8bppIndexedPixel(ref Bitmap bmp, int x, int y, Color c)
        {
            BitmapData bmpData = bmp.LockBits(new Rectangle(x, y, 1, 1),
                                              ImageLockMode.ReadOnly,
                                              bmp.PixelFormat);
            Marshal.WriteByte(bmpData.Scan0, (byte)c.R);
            bmp.UnlockBits(bmpData);
        }

        private static bool hasDarkerBackground(Bitmap bitMap)
        {
            double TotalPixel = bitMap.Height * bitMap.Width;
            double currentGrayScalePixel = 0;
            for (int rowIndex = 0; rowIndex < bitMap.Height; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < bitMap.Width; columnIndex++)
                {
                    if (bitMap.GetPixel(columnIndex, rowIndex).R == 0)
                    {
                        currentGrayScalePixel++;
                    }
                }
            }

            return currentGrayScalePixel / TotalPixel > 0.6;
        }

        private void SavePreProcessImage(Bitmap sourceImage, string convertFile)
        {
            if (!string.IsNullOrEmpty(convertFile))
                sourceImage.Save(convertFile);
        }

        private void ApplyContrastStrech(ImageProcessingConfig config, ref Bitmap outputImage)
        {
            if (config.EnableContrastStretch)
            {
                VBotLogger.Trace(() => "[ApplyContrastStrech] Apply contrast strech filter ....");
                Bitmap inputImage = outputImage;
                try
                {
                    AForge.Imaging.Filters.ContrastStretch cs = new AForge.Imaging.Filters.ContrastStretch();
                    outputImage = cs.Apply(outputImage);
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplyContrastStrech));
                }
                finally
                {
                    if (inputImage != null && inputImage != outputImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                }

                VBotLogger.Trace(() => "[ApplyContrastStrech] Contrast strech filter applied...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyContrastStrech] Contrast strech filter is disabled...");
            }
        }

        private void ApplyAutoRotation(ImageProcessingConfig config, ref Bitmap outputImage)
        {
            if (config.EnableAutoRotation)
            {
                VBotLogger.Trace(() => "[ApplyAutoRotation] Auto rotate image ....");
                Bitmap inputImage = outputImage;
                try
                {
                    IImageDeskewer imgDeskewer = GetImageDeskewer(outputImage);
                    imgDeskewer.Deskew(ref outputImage);
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplyAutoRotation));
                }
                finally
                {
                    if (inputImage != null && inputImage != outputImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                }

                VBotLogger.Trace(() => "[ApplyAutoRotation] Auto rotation completed...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyAutoRotation] Auto rotation is disabled...");
            }
        }

        private static IImageDeskewer GetImageDeskewer(Bitmap sourceImage)
        {
            return new AForgeNetDeskewer();
        }

        [HandleProcessCorruptedStateExceptions]
        private void ApplyOrientation(ImageProcessingConfig config, ref Bitmap OutputImage)
        {
            if (config.EnablePageOrientation)
            {
                VBotLogger.Trace(() => "[ApplyOrientation] Load image for orientation ....");

                string imagePath = null;

                try
                {
                    imagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                    ((Image)OutputImage).Save(imagePath);


                    int orientationAngle = GetPageOrientation(imagePath);

                    if (orientationAngle > 0)
                    {
                        switch (orientationAngle)
                        {
                            case 90: OutputImage.RotateFlip(RotateFlipType.Rotate270FlipNone); break;
                            case 270: OutputImage.RotateFlip(RotateFlipType.Rotate90FlipNone); break;
                            case 180: OutputImage.RotateFlip(RotateFlipType.Rotate180FlipNone); break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplyOrientation));
                }
                finally
                {
                    if (File.Exists(imagePath))
                    {
                        File.Delete(imagePath);
                    }
                }

                VBotLogger.Trace(() => "[ApplyOrientation] Orientation completed...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyOrientation] Orientation is disabled...");
            }
        }

        private void ApplyBinarizationFilter(ImageProcessingConfig config, ref Bitmap outputImage)
        {
            if (config.EnableBinarization)
            {
                VBotLogger.Trace(() => "[ApplyBinarizationFilter] Binarize image...");
                outputImage = DoBinarization(config, outputImage);

                VBotLogger.Trace(() => "[ApplyBinarizationFilter] Image binarization completed...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyBinarizationFilter] Binarize is disabled...");
            }
        }

        private Bitmap DoBinarization(ImageProcessingConfig config, Bitmap outputImage)
        {
            Bitmap inputImage = outputImage;
            try
            {
                IImageBinarizer imageBinarizer = GetImageBinarizer(config);
                imageBinarizer.AutoBinarize(ref outputImage);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(ApplyBinarizationFilter));
            }
            finally
            {
                if (inputImage != null && inputImage != outputImage)
                {
                    inputImage.Dispose();
                }
            }

            return outputImage;
        }
        private void FillHoles(Bitmap inputImage, string fillHoleImgToSave)
        {
            VBotLogger.Trace(() => string.Format("ImageOperations::FillHoles Start"));
            List<Bitmap> bitmapToDispose = new List<Bitmap>();
            if (inputImage.PixelFormat == PixelFormat.Format1bppIndexed)
            {

                inputImage = new Bitmap(inputImage);
                bitmapToDispose.Add(inputImage);
            }
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed &&
                    inputImage.PixelFormat != PixelFormat.Format1bppIndexed)
                {
                    inputImage = Grayscale.CommonAlgorithms.BT709.Apply(inputImage);

                    bitmapToDispose.Add(inputImage);
                }
                if (inputImage.PixelFormat != PixelFormat.Format1bppIndexed)
                {
                    inputImage = DoBinarization(new ImageProcessingConfig(), inputImage);
                    bitmapToDispose.Add(inputImage);
                }

                FillHoles filter = new FillHoles();
                filter.MaxHoleWidth = (int)(inputImage.Width * 0.2);
                filter.MaxHoleHeight = (int)(inputImage.Height * 0.15);
                filter.CoupledSizeFiltering = true;
                // apply the filter
                bitmapToDispose.Add(inputImage);

                Bitmap result = filter.Apply(inputImage);
                bitmapToDispose.Add(result);

                result.Save(fillHoleImgToSave);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(FillHoles));
            }
            finally
            {
                for (int i = 0; i < bitmapToDispose.Count; i++)
                {
                    Bitmap bitmap = bitmapToDispose[i];
                    if (bitmap != null)
                    {
                        bitmap.Dispose();
                        bitmap = null;
                    }
                }
            }
            VBotLogger.Trace(() => string.Format("ImageOperations::FillHoles End"));

        }
        private IImageBinarizer GetImageBinarizer(ImageProcessingConfig config)
        {
            return new AForgeNetOtsu();
        }

        private void ApplyGrayscaleFilter(ImageProcessingConfig config, ref Bitmap sourceImage)
        {
            if (config.EnableGrayscaleConversion)
            {
                VBotLogger.Trace(() => "[ApplyGrayscaleFilter] Start 8bpp grayscale conversion...");
                Bitmap inputImage = sourceImage;
                try
                {
                    try
                    {
                        IImageConverter standardConverter = new ImageConverter();
                        standardConverter.ConvertToGrayScale(ref sourceImage);
                    }
                    catch (Exception ex)
                    {
                        ex.Log(nameof(ImageOperations), nameof(ApplyGrayscaleFilter));
                    }
                }
                finally
                {
                    if (inputImage != null && inputImage != sourceImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                }

                VBotLogger.Trace(() => "[ApplyGrayscaleFilter] End 8bpp grayscale conversion...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyGrayscaleFilter] Grayscale conversion is disabled....");
            }
        }

        private void ApplyNoiseFilter(ImageProcessingConfig config, ref Bitmap sourceImage)
        {
            if (config.EnableNoiseFilter)
            {
                VBotLogger.Trace(() => "[ApplyNoiseFilter] Start remove noise...");
                Bitmap inputImage = sourceImage;
                try
                {
                    IImageFilterOpetations imgEnhancer = new ImageFilterOpetations();
                    imgEnhancer.RemoveNoise(ref sourceImage);
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(ApplyNoiseFilter));
                }
                finally
                {
                    if (inputImage != null && inputImage != sourceImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                }

                VBotLogger.Trace(() => "[ApplyNoiseFilter] End remove noise...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyNoiseFilter] Noise filter is disabled....");
            }
        }

        private void ApplyBorderFilter(ImageProcessingConfig config, ref Bitmap sourceImage, ref DocumentProperties docProps)
        {
            if (config.EnableBorderFilter)
            {
                VBotLogger.Trace(() => "[ApplyBorderFilter] Start remove border...");
                Bitmap inputImage = sourceImage;
                try
                {
                    try
                    {
                        IImageFilterOpetations imgFilterOps = new ImageFilterOpetations();

                        docProps.BorderRect = imgFilterOps.GetBorder(sourceImage, 128, 25);
                        imgFilterOps.CropBorder(ref sourceImage, docProps.BorderRect);
                    }
                    catch (Exception ex)
                    {
                        ex.Log(nameof(ImageOperations), nameof(ApplyBorderFilter));
                    }

                    if (OnBorderFilterApplied != null)
                    {
                        OnBorderFilterApplied(sourceImage.Clone() as Bitmap, docProps);
                    }
                }
                finally
                {
                    if (inputImage != null && inputImage != sourceImage)
                    {
                        inputImage.Dispose();
                        inputImage = null;
                    }
                }

                VBotLogger.Trace(() => "[ApplyBorderFilter] End remove border...");
            }
            else
            {
                VBotLogger.Trace(() => "[ApplyBorderFilter] Border filter is disabled....");
            }
        }

        private int getPageFrameCountOfImage(Image image)
        {
            int pageCount = 1;
            try
            {
                if (image?.FrameDimensionsList != null && image.FrameDimensionsList.Contains(FrameDimension.Page.Guid))
                {
                    pageCount = image.GetFrameCount(FrameDimension.Page);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(getPageFrameCountOfImage));
                throw;
            }
            return pageCount;
        }

        public ImageData GetImage(string imagePath)
        {
            ImageData imageData = new ImageData();
            int count = 0;

            do
            {
                count++;
                try
                {
                    if (File.Exists(imagePath) && imageData.Bitmap == null)
                    {
                        string tempFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        File.Copy(imagePath, tempFile, true);
                        Bitmap temp = null;
                        try
                        {
                            temp = AForge.Imaging.Image.FromFile(tempFile);
                            imageData.PageCount = getPageFrameCountOfImage(temp);
                            imageData.Bitmap = CloneImage(temp) as Bitmap;
                        }
                        finally
                        {
                            if (temp != null)
                            {
                                temp.Dispose();
                            }

                            if (File.Exists(tempFile))
                            {
                                File.Delete(tempFile);
                            }
                        }
                    }
                    else
                    {
                        VBotLogger.Trace(() => string.Format("[GetImage] File does not exist at '{0}'.", imagePath));
                    }
                    if (imageData.Bitmap != null)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(GetImage), $"Reading image \"{imagePath}\"");
                }
            } while (count < 2);

            return imageData;
        }

        public Image CloneImage(string imagePath)
        {
            Image image = null;
            int count = 0;

            do
            {
                count++;
                try
                {
                    if (File.Exists(imagePath))
                    {
                        using (Stream stream = File.OpenRead(imagePath))
                        {
                            using (Image imageFromStream = Image.FromStream(stream))
                            {
                                image = CloneImage(imageFromStream);
                            }
                        }
                    }
                    else
                    {
                        VBotLogger.Trace(() => string.Format("[CloneImage]  OverloadWithString File does not exist at '{0}'.", imagePath));
                    }
                    break;
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(CloneImage), $"Reading Image \"{imagePath}\"");

                    if (image != null)
                    {
                        image.Dispose();
                        image = null;
                    }
                }
            } while (count < 2);

            return image;
        }

        public Image CloneImage(Image imageToClone)
        {
            Bitmap bitmap = null;
            try
            {
                bitmap = AForge.Imaging.Image.Clone((Bitmap)imageToClone);
                bitmap.SetResolution(imageToClone.HorizontalResolution, imageToClone.VerticalResolution);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(CloneImage));
                throw;
            }
            return bitmap;
        }

        public Bitmap[] GetPagesFromImage(string imagePath, int pageIndex, int? noOfPages)
        {
            Bitmap[] pageBitmaps = new Bitmap[0];
            int count = 0;
            do
            {
                count++;
                VBotLogger.Trace(() => string.Format("[GetPagesFromImage] Starting Retry No: {3} for ImagePath: {0} PageIndex: {1} NoOfPages: {2}", imagePath != null ? imagePath : string.Empty, pageIndex, noOfPages != null ? noOfPages.Value : 1000, count));
                try
                {
                    if (File.Exists(imagePath))
                    {
                        int pageCount = 0;
                        using (Stream stream = File.OpenRead(imagePath))
                        {
                            //TODO: Refactor this code in new class.
                            using (Image sourceImage = Image.FromStream(stream))
                            {
                                pageCount = getPageFrameCountOfImage(sourceImage);
                                int noOfPagesToFetch = noOfPages != null ? noOfPages.Value : pageCount;
                                pageBitmaps = new Bitmap[noOfPagesToFetch];
                                for (int i = pageIndex; i < (pageIndex + noOfPagesToFetch); i++)
                                {
                                    if (i > 0)
                                    {
                                        sourceImage.SelectActiveFrame(FrameDimension.Page, i);
                                    }
                                    Bitmap pageBitmap = CloneImage(sourceImage) as Bitmap;
                                    pageBitmaps[i - pageIndex] = pageBitmap;
                                }
                            }
                        }
                    }
                    else
                    {
                        VBotLogger.Trace(() => string.Format("[GetPagesFromImage] File does not exist at '{0}'.", imagePath));
                    }

                    if (pageBitmaps != null && pageBitmaps.Count() > 0 && pageBitmaps[0] != null)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ImageOperations), nameof(GetPagesFromImage), $"Reading image \"{imagePath}\"");
                    //TODO: Dispose pageBitmaps
                }
            } while (count < 2);

            if (pageBitmaps != null && pageBitmaps.Count() > 0 && pageBitmaps[0] != null)
            {
                VBotLogger.Trace(() => string.Format("[GetPagesFromImage] Got Bitmap for ImagePath: {0} PageIndex: {1} NoOfPages: {2}", imagePath != null ? imagePath : string.Empty, pageIndex, noOfPages != null ? noOfPages.Value : 1000));
            }
            else
            {
                VBotLogger.Error(() => string.Format("[GetPagesFromImage] Could not get Bitmap for ImagePath: {0} PageIndex: {1} NoOfPages: {2}", imagePath != null ? imagePath : string.Empty, pageIndex, noOfPages != null ? noOfPages.Value : 1000));
            }

            return pageBitmaps;
        }

        public int GetPageCount(string imagePath)
        {
            int pageCount = 0;
            using (ImageData image = GetImage(imagePath))
            {
                if (image != null)
                {
                    pageCount = image.PageCount;
                }
            }
            return pageCount;
        }

        public bool IsImageMultiPage(string imagePath)
        {
            return (GetPageCount(imagePath) > 1);
        }

        public bool Is1BppImageFormat(Bitmap image)
        {
            if (image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed)
            {
                return true;
            }
            return false;
        }

        public void UpdateImageProperties(DocumentProperties docProperties)
        {
            using (ImageData img = GetImage(docProperties.Path))
            {
                if (img != null && img.Bitmap != null)
                {
                    docProperties.Height = img.Bitmap.Height;
                    docProperties.Width = img.Bitmap.Width;
                }
            }
        }

        public void ConvertImageToGrayscale(string orgFile, string convertFile, ImageProcessingConfig settings)
        {
            VBotLogger.Trace(() => "[ConvertImageToGrayscale] Load image for GrayscaleFilter processing...");

            Bitmap[] pagewiseBitmaps = GetPagesFromImage(orgFile, 0, null);
            if (pagewiseBitmaps == null || pagewiseBitmaps.Length == 0)
            {
                VBotLogger.Trace(() => "[ConvertImageToGrayscale] Image load issue, Copy orignal file......");
                File.Copy(orgFile, convertFile, true);
                return;
            }

            ConvertImageToGrayscale(pagewiseBitmaps, convertFile, settings);

            VBotLogger.Trace(() => "[ConvertImageToGrayscale] End image for GrayscaleFilter processing......");
        }

        public void ConvertImageToGrayscale(Bitmap[] imageList, string convertFile, ImageProcessingConfig settings)
        {
            Bitmap outputImage = null;
            try
            {
                int count = 0;
                do
                {
                    count++;
                    try
                    {
                        VBotLogger.Trace(() => "[ConvertImageToGrayscale] Overload With Bitmap Array: Load image for GrayscaleFilter processing...");
                        if (settings == null)
                            settings = new ImageProcessingConfig();

                        PreProcessParmas preProcessParams = new PreProcessParmas();
                        preProcessParams.convertFile = convertFile;

                        for (int i = 0; i < imageList.Length; i++)
                        {
                            PreProcessImage(settings, ref imageList[i], preProcessParams);
                        }

                        outputImage = imageList[0];

                        if (outputImage != null && File.Exists(convertFile))
                        {
                            VBotLogger.Trace(() => "[ConvertImageToGrayscale] Overload With Bitmap Array: End image for GrayscaleFilter processing......");
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Log(nameof(ImageOperations), nameof(ConvertImageToGrayscale), "Overload With Bitmap Array");
                    }
                } while (count < 2);
            }
            finally
            {
                if (outputImage != null)
                {
                    outputImage.Dispose();
                }

                if (imageList != null)
                {
                    for (int i = 0; i < imageList.Length; i++)
                    {
                        if (imageList[i] != null)
                        {
                            imageList[i].Dispose();
                            imageList[i] = null;
                        }
                    }
                }
            }
        }

        public Image GetMirrorImage(Image imageToMirror, bool mirrorX, bool mirrorY)
        {
            Bitmap bitmap = null;
            try
            {
                var mirrorFilter = new AForge.Imaging.Filters.Mirror(mirrorX, mirrorY);
                bitmap = mirrorFilter.Apply((Bitmap)imageToMirror);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(GetMirrorImage));
                throw;
            }
            return bitmap;
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class ImageData : IDisposable
    {
        public Bitmap Bitmap { get; set; }
        public int PageCount = 0;

        public void Dispose()
        {
            if (Bitmap != null)
            {
                Bitmap.Dispose();
                Bitmap = null;
            }
        }
    }

    public class NoiseRemovalFacade
    {
        private string imageExtension = ".png";

        [HandleProcessCorruptedStateExceptions]
        public string RemoveSpacleNoise(Bitmap sourceImage)
        {
            try
            {
                string imagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + imageExtension);
                sourceImage.Save(imagePath);
                int predictNoise = -3;
                VBotLogger.Trace(() => $"[RemoveSpacleNoise] Check for Noise at Image Path: {imagePath}");
                predictNoise = PatternRecognizer.PredictSpeckleNoiseInDocument(imagePath);
                VBotLogger.Trace(() => $"[RemoveSpacleNoise] Image Path: {imagePath}, Noise Predicted: {predictNoise}");
                if (predictNoise == 1)
                {
                    VBotLogger.Trace(() => $"[RemoveSpacleNoise] Image Path: {imagePath}, Noise Predicted : {predictNoise}, Removing Noise in Progress");
                    PatternRecognizer.RemoveSpeckleNoise(imagePath);
                    VBotLogger.Trace(() => $"[RemoveSpacleNoise] Image Path: {imagePath}, Noise Predicted: {predictNoise}, Removing Noise : done");
                }

                if (predictNoise == 1)
                {
                    VBotLogger.Trace(() => $"[RemoveSpacleNoise] Return Image Path: {imagePath}");
                    return imagePath;
                }
                else
                {
                    if (File.Exists(imagePath))
                    {
                        File.Delete(imagePath);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageOperations), nameof(RemoveSpacleNoise), "Removing Noise Crashed");
            }
            return string.Empty;
        }
    }

    internal static class ImageHelper
    {
        public static void PreserveImageMetadataSize(ref Bitmap image, Action functionality, bool forceByPass = false)
        {
            if (image != null)
            {
                float horizontalResolution = image.HorizontalResolution;
                float verticalResolution = image.VerticalResolution;

                functionality();

                if (image != null && !forceByPass)
                {
                    image.SetResolution(horizontalResolution, verticalResolution);
                }
            }
            else
            {
                functionality();
            }
        }
    }
}