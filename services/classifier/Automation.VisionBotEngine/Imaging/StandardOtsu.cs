﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;
using System.Drawing.Imaging;

namespace Automation.Generic.Imaging
{
    class StandardOtsu
    {     
        private float getPixelSum(int init, int end, int[] hist)
        {
            int sum = 0;

            for (int i = init; i <= end; i++)
            {
                sum += hist[i];
            }

            return (float)sum;
        }
               
        private float getPixelMean(int init, int end, int[] hist)
        {
            int sum = 0;
       
            for (int i = init; i <= end; i++)
            {
                sum += i * hist[i];
            }

            return (float)sum;
        }
              
        private int findMax(float[] vec, int n)
        {
            float maxVec = 0;
            int idx=0;
       
            for (int i = 1; i < n - 1; i++)
            {
                if (vec[i] > maxVec)
                {
                    maxVec = vec[i];
                    idx = i;
                }
            }

            return idx;
        }
             
        unsafe private void getHistogram(byte* p, int w, int h, int ws, int[] hist)
        {
            hist.Initialize();
            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w*3; j+=3)
                {
                    int index=i*ws+j;

                    hist[p[index]]++;
                }
            }
        }
          
        public int GetThreshold(Bitmap bmp)
        {
            byte threshold = 0;
	        float[] vet=new float[256];
            int[] hist=new int[256];

            vet.Initialize();

	        float p1,p2,p12;
	        int k;

            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* pixels = (byte*)(void*)bmData.Scan0.ToPointer();

                getHistogram(pixels,bmp.Width,bmp.Height,bmData.Stride, hist);
                               
                for (k = 1; k != 255; k++)
                {
                    p1 = getPixelSum(0, k, hist);
                    p2 = getPixelSum(k + 1, 255, hist);

                    p12 = p1 * p2;

                    if (p12 == 0)
                    {
                        p12 = 1;
                    }

                    float diff=(getPixelMean(0, k, hist) * p2) - (getPixelMean(k + 1, 255, hist) * p1);
                    vet[k] = (float)diff * diff / p12;                    
                }
            }

            bmp.UnlockBits(bmData);

            threshold = (byte)findMax(vet, 256);

            return threshold;
        }     
    }
}

