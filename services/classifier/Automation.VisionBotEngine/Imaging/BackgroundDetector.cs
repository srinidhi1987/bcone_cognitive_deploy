﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Automation.VisionBotEngine.Generic;

namespace Automation.VisionBotEngine.Imaging
{

    public interface IImageColorBasedTransformationHelper
    {
        // public 
    }

    internal abstract class ImagePixelCollection<T>
    {
        public T this[int rowIndex, int columnIndex]
        {
            get
            {
                return pixels[rowIndex][columnIndex];
            }
        }
        private T[][] pixels;

        private int _height;
        public int Height
        {
            get
            {
                return _height;
            }
        }

        private int _width;
        public int Width
        {
            get
            {
                return _width;
            }
        }

        protected void CopyPixels(ImagePixelCollection<Color> image, Func<Color, T> getPixelValue)
        {
            _height = image.Height;
            _width = image.Width;

            pixels = new T[Height][];

            for (int j = 0; j < pixels.Length; j++)
            {
                pixels[j] = new T[Width];
            }

            for (int rowIndex = 0; rowIndex < image.Height; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < image.Width; columnIndex++)
                {
                    Color color = image[rowIndex, columnIndex];
                    pixels[rowIndex][columnIndex] = getPixelValue(color);
                }
            }
        }

        protected void FillPixels(Bitmap image, Func<Color, T> getPixelValue)
        {
            _height = image.Height;
            _width = image.Width;

            pixels = new T[Height][];

            for (int j = 0; j < pixels.Length; j++)
            {
                pixels[j] = new T[Width];
            }
            for (int rowIndex = 0; rowIndex < image.Height; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < image.Width; columnIndex++)
                {
                    //TODO: reverse indexes
                    Color color = image.GetPixel(columnIndex, rowIndex);
                    pixels[rowIndex][columnIndex] = getPixelValue(color);
                 
                }
            }
        }
    }

    internal class ImageGreyScalePixelCollection : ImagePixelCollection<byte>
    {
        public ImageGreyScalePixelCollection(Bitmap image)
        {
            // if bitmap is color
            // convert to greyscale
            // if bitmap is greyscale
            // do nothing

            this.FillPixels(image, color => color.R);
        }

        public ImageGreyScalePixelCollection(ImagePixelCollection<Color> image)
        {

        }
    }

    internal class BinarizedImagePixelCollection : ImagePixelCollection<bool>
    {
        public BinarizedImagePixelCollection(Bitmap image)
        {
            // if bitmap is color
            // convert to greyscale
            // if bitmap is greyscale
            // binarize
            // if bitmap is binarized
            // do nothing

            this.FillPixels(image, color => color.R == byte.MaxValue);
        }

        public BinarizedImagePixelCollection(ImagePixelCollection<Color> image)
        {

        }
    }

    internal interface IBackgroundDetector
    {
        IEnumerable<BackgroundBox> GetBackgroundBoxes(Bitmap sourceImage);
    }

    internal class PixelDetails
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public int Distance { get; set; }
    }


    internal class BackgroundBox
    {
        public Rectangle Dimensions { get; set; }

        public Byte GrayScaleValue { get; set; }

        public bool NeedsToInvert { get; set; }
    }

    internal struct GrayScalePixel
    {
        public byte GrayScaleValue;
        public Point Location;
    }

    internal class BackgroundDetector
    {
        private ImageConverter imageConverter;

        private const int GrayScaleValueTolerance = 5;
        private const int StartPointGrayScaleValueTolerance = 2;

        private const int SpanToleranceForFindingWidth = 5;
        private const int SpanToleranceForFindingHeight = 2;

        private const int MaxGrayScaleValueToConsider = 245;
        private const int MinHeightNeedToConsiderRect = 10;
        private const int MinWidthNeedToConsiderRect = 150;

        private const int MatrixToConsiderEdge = 3;

        public BackgroundDetector()
        {
            imageConverter = new ImageConverter();
        }

        public List<BackgroundBox> DetectColorBackgroundBoxes(Bitmap sourceImage)
        {
            var imagePixels = new ImageGreyScalePixelCollection(sourceImage);

            var foundBoxes = new List<BackgroundBox>();
            
            DiagonalMover.Move(imagePixels.Width-5, imagePixels.Height-5, DiagonalMover.StartFrom.X, (Point pixel) =>
            {
                int rowIndex = pixel.Y;
                int columnIndex = pixel.X;

                byte currentPixelGrayScaleValue = imagePixels[rowIndex, columnIndex];

                if (currentPixelGrayScaleValue > MaxGrayScaleValueToConsider)
                {
                    return;
                }

                var isPixelAlreadyCovered = foundBoxes.Any(backgroundBox => backgroundBox.Dimensions.Contains(columnIndex, rowIndex));
                if (isPixelAlreadyCovered)
                {
                    return;
                }

                var CurrentPixel = new GrayScalePixel() { GrayScaleValue = currentPixelGrayScaleValue, Location = new Point(columnIndex, rowIndex) };

                if (CanThisBeBackgroundStartingPoint(CurrentPixel, imagePixels))
                {


                    int width = findWidthOfBackgroundRect(imagePixels, foundBoxes, ref CurrentPixel);
                    if (!doesWidthConditionMatch(width))
                    {
                        return;
                    }

                    int height = findHeightOfBackgroundRect(imagePixels, foundBoxes, CurrentPixel);
                    if (!doesHeightConditionMatch(height))
                    {
                        return;
                    }

                    if (doesBackgroundConditionMatch(imagePixels, CurrentPixel, height, width, GrayScaleValueTolerance))
                    {
                        foundBoxes.Add(new BackgroundBox() { Dimensions = new Rectangle(CurrentPixel.Location, new Size(width, height)), GrayScaleValue = CurrentPixel.GrayScaleValue });
                    }
                }
            });

            return foundBoxes;
        }

        private static bool doesBackgroundConditionMatch(ImageGreyScalePixelCollection imagePixels, GrayScalePixel pixel, int height, int width, int allowedDifferance)
        {
            double TotalPixel = height * width;
            double currentGrayScalePixel = 0;
            for (int rowIndex = pixel.Location.Y; rowIndex < pixel.Location.Y + height; rowIndex++)
            {
                for (int columnIndex = pixel.Location.X; columnIndex < pixel.Location.X + width; columnIndex++)
                {
                    if (Math.Abs(pixel.GrayScaleValue - imagePixels[rowIndex, columnIndex]) < allowedDifferance)
                    {
                        currentGrayScalePixel++;
                    }
                }
            }

            return currentGrayScalePixel / TotalPixel > 0.6;
        }

        private static bool doesHeightConditionMatch(int height)
        {
            return height > MinHeightNeedToConsiderRect;
        }
        private static bool doesWidthConditionMatch(int width)
        {
            return width > MinWidthNeedToConsiderRect;
        }

        private int findWidthOfBackgroundRect(ImageGreyScalePixelCollection imagePixels, List<BackgroundBox> foundBoxes, ref GrayScalePixel pixel)
        {
            const int DepthToSeePixels = 8;
            const double MinDepthIntoleranceInPercentage = 0.6;

            var remainingSpanTolerance = SpanToleranceForFindingWidth;
            var latestSuccessPixel = pixel.Location.X;

            var movedPixels = 0;
            // start from given fixel position and go till last pixel at same level in imagePixels
            for (int columnIndex = pixel.Location.X; columnIndex < imagePixels.Width; columnIndex++)
            {
                int y = pixel.Location.Y;
                // check if this pixel is part of some already found boxes
                var isPixelAlreadyCovered = foundBoxes.Any(backgroundBox => backgroundBox.Dimensions.Contains(columnIndex, y));
                if (isPixelAlreadyCovered)
                {
                    break;
                }

                //TODO : need to decide outof bound condition
                // for all depth pixels find how many pixels match given pixel value
                double matchedPixelCount = 0;
                var endDepthIndex = (pixel.Location.Y + DepthToSeePixels + movedPixels) < imagePixels.Height ? (pixel.Location.Y + DepthToSeePixels + movedPixels) : imagePixels.Height;
                bool isFirstPixelHorixontal = true;
                for (int depthIndex = pixel.Location.Y; depthIndex < endDepthIndex; depthIndex++)
                {
                    var pixelInConsideration = imagePixels[depthIndex, columnIndex];
                    var greyScaleDifference = Math.Abs(pixel.GrayScaleValue - pixelInConsideration);
                    var isPixelMatched = greyScaleDifference < GrayScaleValueTolerance;
                    if (isPixelMatched)
                    {
                        if (isFirstPixelHorixontal)
                        {
                            isFirstPixelHorixontal = false;
                            movedPixels = depthIndex - pixel.Location.Y;
                        }
                        matchedPixelCount++;
                    }
                }

                // if min depth intolerance succeeds )
                if ((matchedPixelCount / (endDepthIndex - pixel.Location.Y)) > MinDepthIntoleranceInPercentage)
                {
                    // this is latest success pixel
                    latestSuccessPixel = columnIndex;
                    // move to next pixel location
                    // reassign remainingSpanTolerance
                    remainingSpanTolerance = SpanToleranceForFindingWidth;
                }
                // min depth tolerance fail
                else
                {
                    //if there is span tolerance
                    if (remainingSpanTolerance > 0)
                    {
                        // reduce span tolerance value and continue with next pixel locatin
                        remainingSpanTolerance--;
                    }
                    // else no remaining span tolerance
                    else
                    {
                        break;
                    }
                }
            }

            if ((latestSuccessPixel - pixel.Location.X) < 150)
            {
                return latestSuccessPixel - pixel.Location.X;
            }

            var width = latestSuccessPixel - pixel.Location.X;
            return width;
        }

        private int findHeightOfBackgroundRect(ImageGreyScalePixelCollection imagePixels, List<BackgroundBox> foundBoxes, GrayScalePixel pixel)
        {
            const int DepthToSeePixels = 5;
            const double MinDepthIntoleranceInPercentage = 0.6; /* MinDepthIntolerance = 3;*/

            var remainingSpanTolerance = SpanToleranceForFindingHeight;
            var latestSuccessPixel = pixel.Location.Y;
            var movedPixels = 0;
            // start from given fixel position and go till last pixel at same level in imagePixels
            for (int rowIndex = pixel.Location.Y; rowIndex < imagePixels.Height; rowIndex++)
            {
                // check if this pixel is part of some already found boxes
                var isPixelAlreadyCovered = foundBoxes.Any(backgroundBox => backgroundBox.Dimensions.Contains(pixel.Location.X, rowIndex));
                if (isPixelAlreadyCovered)
                {
                    break;
                }

                // for all depth pixels find how many pixels match given pixel value
                double matchedPixelCount = 0;
                var endDepthIndex = (pixel.Location.X + DepthToSeePixels + movedPixels) < imagePixels.Width ? (pixel.Location.X + DepthToSeePixels + movedPixels) : imagePixels.Width;
                bool isFirstPixelHorixontal = true;
                for (int depthIndex = pixel.Location.X; depthIndex < endDepthIndex; depthIndex++)
                {
                    var pixelInConsideration = imagePixels[rowIndex, depthIndex];
                    var greyScaleDifference = Math.Abs(pixel.GrayScaleValue - pixelInConsideration);
                    var isPixelMatched = greyScaleDifference < GrayScaleValueTolerance;
                    if (isPixelMatched)
                    {
                        if (isFirstPixelHorixontal)
                        {
                            isFirstPixelHorixontal = false;
                            movedPixels = depthIndex - pixel.Location.X;
                        }
                        matchedPixelCount++;
                    }
                }

                // if min depth intolerance succeeds
                if ((matchedPixelCount / (endDepthIndex - pixel.Location.X)) >= MinDepthIntoleranceInPercentage)
                {
                    // this is latest success pixel
                    latestSuccessPixel = rowIndex;
                    // move to next pixel location
                    // reassign remainingSpanTolerance
                    remainingSpanTolerance = SpanToleranceForFindingHeight;
                }
                // min depth tolerance fail
                else
                {
                    //if there is span tolerance
                    if (remainingSpanTolerance > 0)
                    {
                        // reduce span tolerance value and continue with next pixel locatin
                        remainingSpanTolerance--;
                    }
                    // else no remaining span tolerance
                    else
                    {
                        break;
                    }
                }
            }

            // calculate width based on parameter pixel and lastest success pixel
            var height = latestSuccessPixel - pixel.Location.Y;
            // return calculated width
            return height;
        }

        private bool CanThisBeBackgroundStartingPoint(
            GrayScalePixel pixel,
            ImageGreyScalePixelCollection imagePixels)
        {
            const int heightToCover = 6;
            const int widthToCover = 6;

            for (int widthLocation = pixel.Location.X; widthLocation < pixel.Location.X + widthToCover; widthLocation++)
            {
                for (int heightLocation = pixel.Location.Y; heightLocation < pixel.Location.Y + heightToCover; heightLocation++)
                {
                    var pixelInConsideration = imagePixels[heightLocation, widthLocation];
                    var greyScaleDifference = Math.Abs(pixel.GrayScaleValue - pixelInConsideration);

                    if (greyScaleDifference > StartPointGrayScaleValueTolerance)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
       


    }
}





