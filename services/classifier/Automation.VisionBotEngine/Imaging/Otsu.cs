﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace Automation.VisionBotEngine.Imaging
{
    //TODO : //TODO: [Prakash] Move this class with interface in Automation.Generic.Imaging
    public class LeptonicaOtsu : IImageBinarizer
    {
        public void AutoBinarize(ref Bitmap inputImage)
        {
            //TODO: [Prakash] Need to optimize this code by removing the dependecy of saving and reading the image to/from file.

            string imageFilePath = Path.GetTempFileName();
            
            inputImage.Save(imageFilePath);
            inputImage.Dispose();

            BinarizeImage(imageFilePath);

            inputImage = GetBinarizedImageFromFile(imageFilePath);
            DeleteTempImage(imageFilePath);
        }

        private void BinarizeImage(string imageFilePath)
        {
            IntPtr thresholdedImage;
            IntPtr imgPtr;

            IntPtr thresholdValue = IntPtr.Zero;
            thresholdedImage = IntPtr.Zero;

            imgPtr = LeptonicaWrapper.pixRead(imageFilePath);
            HandleRef imgHandle = new HandleRef(this, imgPtr);

            int width = LeptonicaWrapper.pixGetWidth(imgHandle);
            int height = LeptonicaWrapper.pixGetHeight(imgHandle);

            int num = LeptonicaWrapper.pixOtsuAdaptiveThreshold(imgHandle, width, height, 0, 0, 0.1f, out thresholdValue, out thresholdedImage);

            HandleRef thrImgHandle = new HandleRef(this, thresholdedImage);
            LeptonicaWrapper.pixWrite(imageFilePath, thrImgHandle, 3);

            LeptonicaWrapper.pixDestroy(ref imgPtr);
            LeptonicaWrapper.pixDestroy(ref thresholdedImage);
        }

        private Bitmap GetBinarizedImageFromFile(string imageFilePath)
        {           
            using (Stream imageStream = File.OpenRead(imageFilePath))
            {
                return (Bitmap)Image.FromStream(imageStream);
            }
        }

        private void DeleteTempImage(string imageFilePath)
        {
            try
            {     
                if (File.Exists(imageFilePath))
                {
                    File.Delete(imageFilePath);
                }
            }
            catch 
            {
                //TODO : log the exception. its not done as we need to move this class in generic.
            }
        }
    }
}
