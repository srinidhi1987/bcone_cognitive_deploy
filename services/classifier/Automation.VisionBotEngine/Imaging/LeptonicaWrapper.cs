﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
using System;
using System.Runtime.InteropServices;

namespace Automation.VisionBotEngine.Imaging
{
    static class LeptonicaWrapper
    {
        public const string LeptonicaDllName = "liblept171.dll";

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr pixRead(string filename);

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int pixOtsuAdaptiveThreshold(HandleRef pix, int sx, int sy, int smoothx, int smoothy, float scorefract, out IntPtr ppixth, out IntPtr ppixd);

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int pixSauvolaBinarize(HandleRef pix, int whsize, float factor, int addborder, out IntPtr ppixm, out IntPtr ppixsd, out IntPtr ppixth, out IntPtr ppixd);

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void pixDestroy(ref IntPtr pix);

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int pixGetWidth(HandleRef pix);

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int pixGetHeight(HandleRef pix);

        [DllImport(LeptonicaDllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int pixWrite(string filename, HandleRef handle, int format);
    }
}
