﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Imaging
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;

    internal class ImageFinder : IImageFinder
    {
        private string matchPercentage = string.Empty;

        private Rectangle MatchedArea = new Rectangle(0, 0, 0, 0);

        public bool FindImage(Bitmap _checkImage, Bitmap _sourceImage, string _matchPercentage)
        {
            return PixelBasedImageFinder(_checkImage, _sourceImage, _matchPercentage);
        }

        #region pixelBasedImageFinder

        private bool PixelBasedImageFinder(Bitmap _checkImage, Bitmap _sourceImage, string _matchPercentage)
        {
            bool retValue = false;
            matchPercentage = _matchPercentage;

            try
            {
                BitmapData smallData = _checkImage.LockBits(new Rectangle(0, 0, _checkImage.Width, _checkImage.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

                BitmapData bigData = _sourceImage.LockBits(new Rectangle(0, 0, _sourceImage.Width, _sourceImage.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

                bool matchFound = false;
                int tolerance = Convert.ToInt16("0");

                int xStart = 0;
                int yStart = 0;

                int xres = 1;
                int yres = 1;

                GetResolution(_checkImage, out xres, out yres);

                while (true)
                {
                    matchFound = FindImageEx(bigData, smallData, ref xStart, ref yStart, xres, yres, true);

                    if (matchFound && (xres > 1 || yres > 1))
                    {
                        int lastx = xStart;
                        int lasty = yStart;

                        matchFound = FindImageEx(bigData, smallData, ref xStart, ref yStart, 1, 1, false);

                        if (matchFound)
                        {
                            retValue = true;
                            break;
                        }
                        else
                        {
                            if (lastx < bigData.Width - smallData.Width)
                                xStart = lastx + 1;
                            else if (lasty < bigData.Height - smallData.Height)
                                yStart = lasty + 1;
                            else
                                break;
                        }
                    }
                    else
                        break;
                }

                _sourceImage.UnlockBits(bigData);
                _checkImage.UnlockBits(smallData);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageFinder), nameof(PixelBasedImageFinder));
                retValue = false;
            }
            return retValue;
        }

        private bool FindImageEx(BitmapData bigData, BitmapData smallData, ref int xStart, ref int yStart, int xres, int yres, bool recursive)
        {
            int smallWidth = smallData.Width;
            int smallHeight = smallData.Height;

            int bigWidth = bigData.Width;
            int bigHeight = bigData.Height;

            bool matchFound = false;

            try
            {
                int smallStride = smallData.Stride;
                int bigStride = bigData.Stride;

                int bigOffset = bigStride - bigWidth * 3;

                int margin = Convert.ToInt32(255f * Convert.ToDouble(Convert.ToSingle("0") / 100f));

                bool match100Perc = matchPercentage.Equals("100");

                int totalMatchPixel = 0;

                if (!match100Perc)
                {
                    for (int i = 0; i < smallHeight; i += yres)
                        for (int j = 0; j < smallWidth; j += xres)
                            totalMatchPixel++;
                }

                int minMatchPixel = Convert.ToInt32(totalMatchPixel * Convert.ToSingle(Convert.ToSingle(matchPercentage) / 100f));
                //int bypassIfNotMatchedPixels = totalMatchPixel - minMatchPixel;
                int bypassIfNotMatchedPixels = Convert.ToInt32(minMatchPixel * (100 - Convert.ToSingle(matchPercentage)) / 100f);

                long matchPixels = 0;

                unsafe
                {
                    byte* pSmall = (byte*)(void*)smallData.Scan0;
                    byte* pBig = (byte*)(void*)bigData.Scan0;

                    pBig += ((bigStride * yStart) + (xStart * 3));

                    for (int y = yStart; y < bigHeight - smallHeight + 1; y++)
                    {
                        for (int x = xStart; x < bigWidth; x++)
                        {
                            if (x > bigWidth - smallWidth)
                            {
                                pBig += ((smallWidth - 1) * 3);
                                break;
                            }

                            matchPixels = 0;
                            int mismatchPixels = 0;

                            byte* pBigBackup = pBig;
                            byte* pSmallBackup = pSmall;

                            matchFound = true;

                            #region scan vertical

                            for (int i = 0; i < smallHeight; i += yres)
                            {
                                int j = 0;
                                matchFound = true;

                                #region scan horizontal

                                for (j = 0; j < smallWidth; j += xres)
                                {
                                    matchFound = true;

                                    #region match pixel

                                    if (margin.Equals(0))
                                    {
                                        if (pBig[0] != pSmall[0] || pBig[1] != pSmall[1] || pBig[2] != pSmall[2])
                                            matchFound = false;
                                    }
                                    else
                                    {
                                        for (int k = 0; k < 1; k++)
                                        {
                                            int inf = pBig[k] - margin;
                                            int sup = pBig[k] + margin;

                                            if (sup < pSmall[k] || inf > pSmall[k])
                                                matchFound = false;
                                        }
                                    }

                                    pBig += 3;
                                    pSmall += 3;

                                    #endregion match pixel

                                    if (match100Perc)
                                    {
                                        if (!matchFound)
                                            break;
                                    }
                                    else
                                    {
                                        if (matchFound)
                                            matchPixels++;
                                        else
                                            mismatchPixels++;

                                        if (mismatchPixels > bypassIfNotMatchedPixels)
                                            break;
                                    }

                                    if (xres > 1)
                                    {
                                        pBig += ((xres * 3) - 3);
                                        pSmall += ((xres * 3) - 3);
                                    }
                                }

                                #endregion scan horizontal

                                if ((!matchFound && match100Perc) || (mismatchPixels > bypassIfNotMatchedPixels))
                                    break;

                                pSmall = pSmallBackup;
                                pBig = pBigBackup;

                                pSmall += smallStride * (yres + i);
                                pBig += bigStride * (yres + i);
                            }

                            #endregion scan vertical

                            #region match

                            #region verify match found

                            if (match100Perc)
                            {
                                if (matchFound)
                                {
                                    xStart = x;
                                    yStart = y;
                                    matchFound = true;
                                    MatchedArea.X = x;
                                    MatchedArea.Y = y;
                                    MatchedArea.Width = smallWidth;
                                    MatchedArea.Height = smallHeight;
                                    break;
                                }
                            }
                            else if (matchPixels >= minMatchPixel)
                            {
                                xStart = x;
                                yStart = y;
                                matchFound = true;
                                MatchedArea.X = x;
                                MatchedArea.Y = y;
                                MatchedArea.Width = smallWidth;
                                MatchedArea.Height = smallHeight;
                                break;
                            }

                            #endregion verify match found

                            #endregion match

                            if (!recursive)
                                break;

                            pBig = pBigBackup;
                            pSmall = pSmallBackup;

                            pBig += 3;
                        }

                        if (matchFound || !recursive)
                            break;

                        xStart = 0;
                        pBig += bigOffset;
                    }
                }

                return matchFound;
            }
            catch
            {
                return false;
            }
        }

        public Rectangle GetMatchedArea()
        {
            return MatchedArea;
        }

        private void GetResolution(Bitmap _checkImage, out int xres, out int yres)
        {
            xres = _checkImage.Width / 10;

            if (xres < 1)
                xres = 1;

            if (xres > 20)
                xres = 20;

            yres = _checkImage.Height / 10;

            if (yres < 1)
                yres = 1;

            if (yres > 20)
                yres = 20;
        }

        #endregion pixelBasedImageFinder
    }
}