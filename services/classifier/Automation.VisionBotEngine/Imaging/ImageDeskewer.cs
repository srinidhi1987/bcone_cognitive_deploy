﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Drawing;

namespace Automation.VisionBotEngine.Imaging
{
    public class ImageDeskewer : IImageDeskewer
    {
        private double AdjacentTopLocation {get; set;}
        private double OppositeTopLocation  {get; set;}
        private double AdjacentBottomLocation  {get; set;}
        private double OppositeBottomLocation  {get; set;}
       
        private int Width  {get; set;}
        private int Height  {get; set;}

        public void Deskew(ref Bitmap sourceImage)
        {
            throw new NotImplementedException("Method 'AutoRotate' is not implemented in class 'ImageDeskewer'.");
        }

        public double GetSkewAngle(Bitmap sourceImage)
        {
            throw new NotImplementedException("Method 'GetSkewAngle' is not implemented in class 'ImageDeskewer'.");
        }
        
        public void Deskew(ref Bitmap sourceImage, double angle)
        {            
            double Theta = ConvertAngleToRadians(-angle);
            
            GetWidthAndHeightOfRotatedImage(Theta, sourceImage.Width, sourceImage.Height);

            sourceImage = GetImageAfterRotation(sourceImage, Theta);
        }

        private Bitmap GetImageAfterRotation(Bitmap sourceImage, double Theta)
        {           
            Bitmap rotatedImage = new Bitmap(Width, Height);

            using (Graphics g = Graphics.FromImage(rotatedImage))
            {
                Point[] points;

                if (Theta >= 0.0 && Theta < (Math.PI / 2.0))
                {
                    points = new Point[] { 
                                             new Point( (int) OppositeBottomLocation, 0 ), 
                                             new Point( Width, (int) OppositeTopLocation ),
                                             new Point( 0, (int) AdjacentBottomLocation )
                                         };

                }
                else if (Theta >= (Math.PI / 2.0) && Theta < Math.PI)
                {
                    points = new Point[] { 
                                             new Point( Width, (int) OppositeTopLocation ),
                                             new Point( (int) AdjacentTopLocation, Height ),
                                             new Point( (int) OppositeBottomLocation, 0 )                        
                                         };
                }
                else if (Theta >= Math.PI && Theta < (Math.PI + (Math.PI / 2.0)))
                {
                    points = new Point[] { 
                                             new Point( (int) AdjacentTopLocation, Height ), 
                                             new Point( 0, (int) AdjacentBottomLocation ),
                                             new Point( Width, (int) OppositeTopLocation )
                                         };
                }
                else
                {
                    points = new Point[] { 
                                             new Point( 0, (int) AdjacentBottomLocation ), 
                                             new Point( (int) OppositeBottomLocation, 0 ),
                                             new Point( (int) AdjacentTopLocation, Height )        
                                         };
                }

                g.DrawImage(sourceImage, points);
                sourceImage.Dispose();
            }

            sourceImage.Dispose();
            return rotatedImage;
        }

        private void GetWidthAndHeightOfRotatedImage(double Theta, double imageWidth, double imageHeight)
        {
            if ((Theta >= 0.0 && Theta < (Math.PI / 2.0)) || (Theta >= Math.PI && Theta < (Math.PI + (Math.PI / 2.0))))
            {
                this.AdjacentTopLocation = ApplyCosThetaToGetLocation(Theta, imageWidth);
                this.OppositeTopLocation = ApplySinThetaToGetLocation(Theta, imageWidth);

                this.AdjacentBottomLocation = ApplyCosThetaToGetLocation(Theta, imageHeight);
                this.OppositeBottomLocation = ApplySinThetaToGetLocation(Theta, imageHeight);
            }
            else
            {
                this.AdjacentTopLocation = ApplySinThetaToGetLocation(Theta, imageHeight);
                this.OppositeTopLocation = ApplyCosThetaToGetLocation(Theta, imageHeight);

                this.AdjacentBottomLocation = ApplySinThetaToGetLocation(Theta, imageWidth);
                this.OppositeBottomLocation = ApplyCosThetaToGetLocation(Theta, imageWidth);
            }

            int newWidth = (int)(AdjacentTopLocation + OppositeBottomLocation);
            double newImageHeight = AdjacentBottomLocation + OppositeTopLocation;

            this.Width = (int)Math.Ceiling((double)newWidth);
            this.Height = (int)Math.Ceiling(newImageHeight);
        }

        private double ApplyCosThetaToGetLocation(double Theta, double value)
        {
            return Math.Abs(Math.Cos(Theta)) * value;
        }

        private double ApplySinThetaToGetLocation(double Theta, double value)
        {
            return Math.Abs(Math.Sin(Theta)) * value;
        }

        private double ConvertAngleToRadians(double angle)
        {
            double Theta= angle * Math.PI / 180.0;

            // Ensure theta is now [0, 2pi)
            while (Theta < 0.0)
                Theta += 2 * Math.PI;

            return Theta;
        }        
    }
}
