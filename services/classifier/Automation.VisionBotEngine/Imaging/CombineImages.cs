﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Imaging
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;

    internal class CombineImages
    {
        public Bitmap CombineVertically(Bitmap[] sourceBitmaps)
        {
            //read all images into memory
            var images = new List<Bitmap>();
            Bitmap finallmage = null;
            try
            {
                int width = 0; int height = 0;
                foreach (Bitmap image in sourceBitmaps)
                {
                    //create a Bitmap from the file and add it to the list
                    Bitmap bitmap = image;
                    //update the size of the final bitmap
                    width = bitmap.Width > width ? bitmap.Width : width;
                    height += bitmap.Height; images.Add(bitmap);
                }
                //create a bitmap to hold the combined image finallmage = new Bitmap(width, height);
                //get a graphics object from the image so we can draw on it
                finallmage = new Bitmap(width, height);
                using (Graphics g = Graphics.FromImage(finallmage))
                {
                    //set background color
                    g.Clear(Color.Black);
                    //go through each image and draw it on the final image
                    int offset = 0;
                    foreach (Bitmap image in images)
                    {
                        g.DrawImage(image, new Rectangle(0, offset > 0 ? offset + 1 : offset, image.Width, image.Height));
                        offset += image.Height;
                    }
                    return finallmage;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(CombineImages), nameof(CombineVertically));
            }
            return null;
        }

        private void disposeBitmap(Bitmap bitmap)
        {
            try
            {
                if (bitmap != null)
                {
                    bitmap.Dispose();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(CombineImages), nameof(disposeBitmap));
            }
        }
    }
}