﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;

namespace Automation.VisionBotEngine.Imaging
{
    public interface IImageFilterOpetations
    {
        void RemoveNoise(ref Bitmap IntensityImage);
        void RemoveBorder(ref Bitmap image, int borderThickness);
        void CropBorder(ref Bitmap image, Rectangle borderRect, byte backColor = 255);
        Rectangle GetBorder(Bitmap bmp8bppIndex, int threshold = 128, int offset = 5);                
        Bitmap CroppedImage(Bitmap image, Rectangle markerBound);
        void ResizeImage(ref Bitmap image, double resizeXRatio, double resizeYRatio);
        void ConservativeSmoothingFilter(ref Bitmap image);
    }
}
