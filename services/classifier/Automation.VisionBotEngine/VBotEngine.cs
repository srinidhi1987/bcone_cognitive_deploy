﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using AForge.Imaging.Filters;
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.OcrEngine;
    using Automation.VisionBotEngine.PdfEngine;
    using Automation.VisionBotEngine.Validation;
    using Cognitive.VisionBotEngine;
    using Interfaces;
    using ResizeRescan;
    using ResizeRescan.Interface;
    using Services.Client;
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = false)]
    internal class VBotEngine : IVBotEngine
    {
        private IOcrEngine _ocrEngine = null;
        private IPdfEngine _pdfEngine = null;
        private IDocumentVectorDetailsCacheManager _documentVectorDetailsCatchManager = null;

        public VBotEngine(IOcrEngine ocrEngine
            , IPdfEngine pdfEngine
            , IDocumentVectorDetailsCacheManager documentVectorDetailsCatchManager)
        {
            _ocrEngine = ocrEngine;
            _pdfEngine = pdfEngine;
            _documentVectorDetailsCatchManager = documentVectorDetailsCatchManager;
        }

        public VBotDocument GetVBotDocument(VisionBot vbot
            , DocumentProperties docProperties
            , IVBotValidatorDataManager validatorDataManager
            , VBotEngineSettings engineSettings
            , ImageProcessingConfig settings
            , string documentId
            , int confidenceThreshold)
        {
            VBotDocument vBotDocument = null;
            IVisionBotPageImageProvider visionBotPageImageProvider = null;
            try
            {
                VBotLogger.Trace(() => string.Format("[VBotEngine -> GetVBotDocument] Settings Parameter: {0}", engineSettings.ToString()));
                bool isPdfDocument = CommonMethods.IsPdfFile(docProperties.Path);
                ProcessingMode processingMode = isPdfDocument ? ProcessingMode.Pdf : ProcessingMode.Image;
                docProperties.Type = isPdfDocument ? DocType.Pdf : DocType.Image;

                if (settings == null)
                    settings = new ImageProcessingConfig();
                visionBotPageImageProvider = new VisionBotPageImageProvider(vbot, docProperties, settings, engineSettings);

                vBotDocument = getVBotDocumentInternal(vbot
                    , docProperties
                    , validatorDataManager
                    , processingMode
                    , visionBotPageImageProvider
                    , engineSettings
                    , documentId
                    , confidenceThreshold);

                IDisposable visionBotPageImageProviderDisposable = (visionBotPageImageProvider as IDisposable);
                if (visionBotPageImageProviderDisposable != null)
                {
                    visionBotPageImageProviderDisposable.Dispose();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VBotEngine), nameof(GetVBotDocument));
            }
            finally
            {
                if (visionBotPageImageProvider != null)
                {
                    (visionBotPageImageProvider as IDisposable).Dispose();
                }
            }

            return vBotDocument;
        }

        public Field GetValueField(FieldLayout fieldLayout, VBotDocument vbotDoc)
        {
            return GetValueField(fieldLayout, null, vbotDoc);
        }

        public Field GetValueField(FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc)
        {
            return GetValueField(fieldLayout, fieldDef, vbotDoc, null, null, null);
        }

        public Field GetValueField(FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc, DocumentProperties docProps, VBotEngineSettings engineSettings, ImageProcessingConfig imageConfig)
        {
            IVisionBotPageImageProvider visionBotPageImageProvider = null;
            try
            {
                FieldOperations fieldOps = new FieldOperations();
                FieldLayout fieldLayoutClone = fieldLayout.Clone();

                //TODO: Add checkboxrecognition settings flag -> UI project should send engine settings as part of parameters.

                if (docProps != null)
                {
                    visionBotPageImageProvider = new VisionBotPageImageProvider(null
                        , docProps
                        , imageConfig
                        , engineSettings);
                }
                VBotDocumentGenerator vbotGenerator = new VBotDocumentGenerator(null, null
                    , engineSettings.IsCheckBoxFeatureEnabled
                    , null
                    , visionBotPageImageProvider);

                fieldLayoutClone = vbotGenerator.UpdateFieldLayout(vbotDoc, fieldLayout);

                IRescan rescan = getRescanInstance(docProps.Type == DocType.Image ? ProcessingMode.Image : ProcessingMode.Pdf, visionBotPageImageProvider, engineSettings);

                if (fieldLayout.IsValueBoundAuto)
                {
                    Field field = vbotGenerator.FindLayoutMarkerField(vbotDoc, fieldLayoutClone);
                    if (field != null && !string.IsNullOrEmpty(field.Id))
                    {
                        fieldLayoutClone.Bounds = field.Bounds;
                    }
                }

                if (fieldDef == null)
                {
                    fieldDef = new FieldDef() { Name = fieldLayout.Label };
                }

                Field valueField = vbotGenerator.GetFieldValue(vbotDoc, fieldOps, fieldDef, fieldLayoutClone);
                if (fieldDef.ValueType == FieldValueType.CheckBox)
                    return valueField;
                if (rescan != null)
                {
                    Field resultField = rescan.GetRescanValue(valueField, fieldDef);
                    if (resultField != null && !string.IsNullOrWhiteSpace(resultField.Text))
                    {
                        valueField.Text = resultField.Text;
                    }
                }

                return valueField;
            }
            finally
            {
                if (visionBotPageImageProvider != null)
                {
                    (visionBotPageImageProvider as IDisposable).Dispose();
                }
            }
        }

        public void ExtractVBotData(VisionBot vbot
            , VBotDocument vbotDoc
            , string dataPath
            , VBotEngineSettings engineSettings
            , ImageProcessingConfig imageProcessingSettings)
        {
            IVisionBotPageImageProvider visionBotPageImageProvider = null;
            try
            {
                VBotLogger.Trace(() => "[VBotEngine::ExtractVBotData] Extracting VisionBot data...");
                //TODO: Single Responsibility Interface to retrieve VBotEngineSettings.
                //Setting should be immutable once created for engine.
                //Move this to constructor. Hence, for every instance of Engine, to have only related settings.
                if (engineSettings == null)
                {
                    throw new ArgumentException("visionbot engine setting should not null");
                }

                visionBotPageImageProvider = new VisionBotPageImageProvider(vbot
                    , vbotDoc.Layout.DocProperties
                    , imageProcessingSettings
                    , engineSettings);

                IRescan rescan = getRescanInstance(vbotDoc.Layout.DocProperties.Type == DocType.Image ? ProcessingMode.Image : ProcessingMode.Pdf, visionBotPageImageProvider, engineSettings);
                IVBotDocClassifier docClassifier = new VBotDocClassifier(vbotDoc.Layout.DocProperties, visionBotPageImageProvider);
                IVBotDocumentGenerator docProcessor = new VBotDocumentGenerator(docClassifier
                    , null
                    , engineSettings.IsCheckBoxFeatureEnabled
                    , rescan, visionBotPageImageProvider);

                FieldOperations filedOperations = new FieldOperations();

                VBotLogger.Trace(() => "[VBotEngine::ExtractVBotData] Sort the Fields.");

                vbotDoc.SirFields.Fields = filedOperations.SortFields(vbotDoc.SirFields.Fields);
                docProcessor.ExtractVBotData(vbot, vbotDoc);
                VBotLogger.Trace(() => "[VBotEngine::ExtractVBotData] Extraction complete for VisionBot.");
            }
            finally
            {
                if (visionBotPageImageProvider != null)
                {
                    (visionBotPageImageProvider as IDisposable).Dispose();
                }
            }
        }

        private VBotDocument getVBotDocumentInternal(VisionBot vbot
            , DocumentProperties docProperties
            , IVBotValidatorDataManager validatorDataManager
            , ProcessingMode processingMode
            , IVisionBotPageImageProvider visionBotPageImageProvider
            , VBotEngineSettings engineSettings
            , string documentId
            , int confidenceThreshold)
        {
            StatisticsLogger.Log($"Document Name : {docProperties.Name}");
            StatisticsLogger.Log($"No of Pages : {docProperties.PageProperties.Count}");

            IRescan rescan = getRescanInstance(processingMode, visionBotPageImageProvider, engineSettings);
            VBotLogger.Trace(() => string.Format("[GetVBotDocument] Get VBot Document for '{0}'.", docProperties.Path));

            StatisticsLogger.Log($"Creating segmented document");
            SegmentedDocument segDoc = getSegmentedDocument(docProperties, processingMode, visionBotPageImageProvider, engineSettings, documentId);
            StatisticsLogger.Log($"Segmented document generated");

            VBotLogger.Trace(() => string.Format("[GetVBotDocument] Generate VBot Document from segmented document with {0} regions.", segDoc.Regions.Count));

            IVBotDocClassifier docClassifier = GetDocumentClassifier(docProperties
                , visionBotPageImageProvider);
            VBotLogger.Trace(() => "[GetVBotDocument] GtDocumentClassifier object created.");

            StatisticsLogger.Log($"VBotDocument document generation start");
            IVBotDocumentGenerator docProcessor = new VBotDocumentGenerator(docClassifier
                , docProperties
                , engineSettings.IsCheckBoxFeatureEnabled
                , rescan, visionBotPageImageProvider);

            VBotLogger.Trace(() => "[GetVBotDocument] VBotDocumentGenerator object created.");

            VBotDocument vBotDocument = docProcessor.Generate(vbot, segDoc, validatorDataManager, confidenceThreshold);
            StatisticsLogger.Log($"VBotDocument generated");

            VBotLogger.Trace(() => "[GetVBotDocument] vBotDocument genereated.");

            return vBotDocument;
        }

        public int GetPageOrientation(string docImagePath, string dataPath)
        {
            return new ImageOperations().GetPageOrientation(docImagePath);
        }

        private SegmentedDocument getSegmentedDocument(DocumentProperties docProperties
            , ProcessingMode processingMode
            , IVisionBotPageImageProvider visionBotPageImageProvider
            , VBotEngineSettings engineSettings
            , string documentId)
        {
            SegmentedDocumentProvider segmentedDocumentProvider = new SegmentedDocumentProvider(_ocrEngine
                , _pdfEngine
                , _documentVectorDetailsCatchManager);

            return segmentedDocumentProvider.GetSegmentedDocument(docProperties
                 , processingMode
                 , visionBotPageImageProvider
                 , engineSettings, documentId);
        }

        private VBotDocClassifier GetDocumentClassifier(DocumentProperties docProperties
            , IVisionBotPageImageProvider visionBotPageImageProvider)
        {
            VBotLogger.Trace(() => "[GtDocumentClassifier] Document Classification started.");
            VBotLogger.Trace(() => string.Format("[GtDocumentClassifier] docHeight -> '{0}',  docWidth -> '{1}'.", docProperties.Height, docProperties.Width));
            return new VBotDocClassifier(docProperties
                , visionBotPageImageProvider);
        }

        private IRescan getRescanInstance(ProcessingMode processingMode, IVisionBotPageImageProvider visionBotPageImageProvider, VBotEngineSettings engineSettings)
        {
            IRescan rescanInstance = null;
            if (engineSettings != null
                && engineSettings.IsAccuracyLevelEnabled
                && visionBotPageImageProvider != null
                && processingMode == ProcessingMode.Image)
            {
                rescanInstance = new MultipleRescan(_ocrEngine, visionBotPageImageProvider);
            }
            return rescanInstance;
        }
    }
}