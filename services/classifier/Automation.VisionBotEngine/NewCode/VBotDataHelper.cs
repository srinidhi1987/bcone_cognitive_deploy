﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.NewCode
{
    internal static class VBotDataHelper
    {
        public static int GetTotalFieldCount(this VBotData vbotdata)
        {
            if (vbotdata == null)
                throw new ArgumentNullException(nameof(vbotdata));

            var result = 0;

            if (vbotdata.FieldDataRecord != null)
            {
                result += vbotdata.FieldDataRecord.GetTotalFieldCount();
            }

            if (vbotdata.TableDataRecord != null)
            {
                foreach (var tableValue in vbotdata.TableDataRecord)
                {
                    result += tableValue.GetTotalFieldCount();
                }
            }

            return result;
        }

        public static int GetTotalFieldCount(this TableValue tableValue)
        {
            if (tableValue == null)
                throw new ArgumentNullException(nameof(tableValue));

            var result = 0;
            if (tableValue.Rows != null)
            {
                foreach (var rowData in tableValue.Rows)
                {
                    result += rowData.GetTotalFieldCount();
                }
            }
            return result;
        }

        public static int GetTotalFieldCount(this DataRecord dataRecord)
        {
            if (dataRecord == null)
                throw new ArgumentNullException(nameof(dataRecord));

            return dataRecord.Fields.Count;
        }
    }
}
