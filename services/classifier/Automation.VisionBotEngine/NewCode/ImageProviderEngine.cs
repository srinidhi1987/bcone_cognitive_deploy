﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Configuration;
using Automation.VisionBotEngine.Imaging;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.PdfEngine;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.NewCode
{
    public class ImageProviderEngine : IImageProviderEngine
    {
        private ImageOperations imageOperations;
        private DocumentProvider documentProvider;

        public ImageProviderEngine()
        {
            imageOperations = new ImageOperations();
            documentProvider = new DocumentProvider();
        }

        public Bitmap GetImage(
            string organizationId,
            string projectId,
            string categoryId,
            DocumentProperties documentProperties,
            int pageIndex,
            ImageProcessingConfig settings)
        {
            //string documentPath = GetDocumentLocalPath(documentProperties);
            documentProperties.Path = documentProvider.GetDocument(organizationId, projectId, categoryId, documentProperties.Id)?.Path;
            if(string.IsNullOrEmpty(documentProperties.Path))
            {
                return null;
            }

            if (Path.GetExtension(documentProperties.Path).ToLower() == ".pdf")
            {
                documentProperties.Type = DocType.Pdf;
            }
            Bitmap[] pages = convertDocumentToImage(documentProperties, pageIndex);
            return performImageOperations(pages, settings);
        }

        //TODO: write logic here for fetching original document and save it locally and return that path
        private void GetDocumentLocalPath(DocumentProperties documentProperties)
        {
            //  return documentProperties.Path;
        }

        private Bitmap[] convertDocumentToImage(DocumentProperties documentProperties, int pageIndex)
        {
            Bitmap[] pages = null;

            if (documentProperties.Type == DocType.Image)
            {
                pages = imageOperations.GetPagesFromImage(documentProperties.Path, pageIndex, 1);
            }
            else
            {
                IPdfEngine pdfWrapper = PdfEngineStrategy.GetPdfEngine();
                IList<Bitmap> pageWiseBitmap = pdfWrapper.GetPagewiseBitmaps(documentProperties.Path, 200, pageIndex, 1);
                pages = pageWiseBitmap.ToArray();
            }
            return pages;
        }

        private Bitmap performImageOperations(Bitmap[] pages, ImageProcessingConfig settings)
        {
            Bitmap bitmap = null;
            string imagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            try
            {
                imageOperations.ConvertImageToGrayscale(pages, imagePath, settings);
                bitmap = new ImageOperations().GetImage(imagePath).Bitmap;
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(ImageProviderEngine)}][{nameof(performImageOperations)}][Image operation failed][{ex.Message}]");
            }
            finally
            {
                if (pages != null)
                {
                    foreach (var page in pages)
                    {
                        if (page != null)
                        {
                            page.Dispose();
                        }
                    }
                    pages = null;
                }
                if (File.Exists(imagePath))
                {
                    File.Delete(imagePath);
                }
            }
            return bitmap;
        }
    }
}