﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Cognitive.VisionBotEngine;
using Automation.Cognitive.VisionBotEngine.Common;

//using Automation.Cognitive.VisionBotEngine.NewCode;
using Automation.Services.Client;
using Automation.Services.Client.Common;
using Automation.Services.Client.ValidatorServiceConsumer;
using Automation.VisionBotEngine.Configuration;
using Automation.VisionBotEngine.Export;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.OcrEngine;
using Automation.VisionBotEngine.PdfEngine;
using Automation.VisionBotEngine.RabbitMQ;
using Automation.VisionBotEngine.Validation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using VisionBotEngine.Model.Model;
using Automation.Cognitive.VisionBotEngine.Model.Generic;
using Automation.VisionBotEngine.MLAutoCorrection;
using Automation.Cognitive.VisionBotEngine.Model.V2.Project;

namespace Automation.VisionBotEngine.NewCode
{
    public class VisionBotEngineAPI
    {
        private DocumentProvider _documentProvider;

        private IImageProviderEngine _imageProvider;
        private OcrEngineStratagy _ocrEngineStratagy;
        private string _datapath = string.Empty;
        private OCREngineProviderBasedOnProject _ocrEngineProvider;
        private IPdfEngine _pdfEngine;
        private IDocumentVectorDetailsCacheManager _documentVectorDetailsCacheManger;

        #region Public Methods

        public VisionBotEngineAPI()
        {
            _documentProvider = new DocumentProvider();
            _imageProvider = new ImageProviderEngine();
            _ocrEngineStratagy = new OcrEngineStratagy();
            _ocrEngineProvider = new OCREngineProviderBasedOnProject(_ocrEngineStratagy);
            _pdfEngine = PdfEngineStrategy.GetPdfEngine();
            _documentVectorDetailsCacheManger = new DocumentVectorDetailsCacheManager();
        }

        public string CreateBasicVisionBot(string organizationId
            , string projectId
            , string categoryId
            , string visionBotDetailId)
        {
            VBotEngine engine = getVBotEngine(organizationId, projectId);

            ProjectServiceEndPointConsumer projectConsumer = new ProjectServiceEndPointConsumer();
            ProjectDetailsV2 projectDetails = projectConsumer
                .GetProjectMetaDataV2(organizationId, projectId);

            DocumentProperties documentProperties = downloadDefaultLayoutDocument(organizationId
              , projectId, categoryId);

            VisionBotCreater visionBotCreater = new VisionBotCreater(engine,new AliasServiceEndPointConsumer());
            var visionBot = visionBotCreater.CreateBasicVisionBot(visionBotDetailId
                , "Group_" + categoryId
                , projectDetails
                , documentProperties);

            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest { id = categoryId, organizationId = organizationId, projectId = projectId };

            IVisionbotServiceEndPointConsumer visionBotServiceEndPoint = new VisionbotServiceEndPointConsumer(classifiedFolderManifest);
            visionBotServiceEndPoint.UpdateVisionBot(classifiedFolderManifest, new VisionBotManifest() { Id = visionBotDetailId }, visionBot);

            return visionBot.Id;
        }

        public bool AddFieldsInVisionbot(string organizationId, string projectId)
        {
            
            try
            {
                IVisionbotServiceEndPointConsumer visionBotService = getVisionbotService(organizationId, projectId);
                List<VisionBotMetadata> visionBotMetadataList = null;

                try
                {
                    visionBotMetadataList = visionBotService.GetVisionBotMetadata(organizationId);
                }
                catch (Exception ex)
                {
                    VBotLogger.Error(() => string.Format($"[ValidatorWorkflowService -> AddFieldsInVisionbot] {ex.ToString()}"));
                }

                if (visionBotMetadataList == null)
                {
                    VBotLogger.Info(() => string.Format($"[ValidatorWorkflowService -> AddFieldsInVisionbot] No visionBot found for Organization Id : {organizationId}; Project Id : {projectId}"));
                    return true;
                }

                var filteredVisionbots = visionBotMetadataList.Where(v => v.ProjectId.Equals(projectId, StringComparison.OrdinalIgnoreCase));

                var projectDetails = getProjectDetails(organizationId, projectId);

                addNewFields(organizationId, visionBotService, filteredVisionbots, projectDetails);

                return true;
            }
            catch(Exception ex)
            {
                VBotLogger.Error(() => string.Format($"[ValidatorWorkflowService -> AddFieldsInVisionbot] Exception Occurred : {ex.ToString()}"));
                return false;
            }
        }

        private void addNewFields(string organizationId, IVisionbotServiceEndPointConsumer visionBotServiceEndpointConsumer, IEnumerable<VisionBotMetadata> visionBotMetadataList, ProjectDetailsV2 projectDetails)
        {
            if(visionBotMetadataList.Count() == 0)
            {
                VBotLogger.Info(() => string.Format($"[ValidatorWorkflowService -> addNewFields] No visionbot found."));
                return;
            }

            if(projectDetails == null)
            {
                VBotLogger.Warning(() => string.Format($"[ValidatorWorkflowService -> addNewFields] Project detail not found."));
                return;
            }

            foreach (VisionBotMetadata visionBotMetadata in visionBotMetadataList)
            {
                ClassifiedFolderManifest classifierManifest = new ClassifiedFolderManifest
                {
                    id = visionBotMetadata.CategoryId,
                    organizationId = organizationId,
                    projectId = projectDetails.Id
                };

                var visionBot = visionBotServiceEndpointConsumer.LoadVisionBot(classifierManifest);
                VBotEngine engine = getVBotEngine(organizationId, projectDetails.Id);
                VisionBotCreater visionBotCreater = new VisionBotCreater(engine, new AliasServiceEndPointConsumer());

                if (visionBot != null)
                {
                    visionBotCreater.AddNewFields(visionBot, projectDetails);
                    visionBotServiceEndpointConsumer.UpdateVisionBot(classifierManifest, new VisionBotManifest() { Id = visionBot.Id }, visionBot);
                }
            }
        }

        private IVisionbotServiceEndPointConsumer getVisionbotService(string organizationId, string projectId)
        {
            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest { organizationId = organizationId, projectId = projectId };
            return new VisionbotServiceEndPointConsumer(classifiedFolderManifest);
        }

        private ProjectDetailsV2 getProjectDetails(string organizationId, string projectId)
        {
            VBotLogger.Debug(() => string.Format($"[ValidatorWorkflowService -> getProjectDetails] Fetching project details for Organization Id - {organizationId}; ProjectId - {projectId}"));
            IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
            return projectService.GetProjectMetaDataV2(organizationId, projectId);
        }

        public Bitmap GetImage(string organizationId
            , string projectId
            , string categoryId
            , string documentId
            , string pageIndex)
        {
            DocumentProperties docProperties = new DocumentProperties() { Id = documentId };

            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            var imageProcessingsettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);

            return _imageProvider.GetImage(organizationId
                , projectId
                , categoryId
                , docProperties
                , Convert.ToInt32(pageIndex)
                , imageProcessingsettings);
        }

        public VBotDocument GetVBotDocument(string organizationId
            , string projectId
            , string categoryId
            , string visionBotID
            , string documentID)
        {
            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest()
            {
                id = categoryId,
                organizationId = organizationId,
                projectId = projectId
            };

            IVBotValidatorDataManager validatorDataManager = new VBotValidatorDataManager(classifiedFolderManifest, visionBotID);

            VisionBot visionBot = getVisionBot(organizationId
                , projectId
                , categoryId
                , visionBotID);

            VBotEngineSettings engineSettings = new VBotEngineSettings();

            DocumentProperties docProperties = _documentProvider.GetDocument(organizationId
                , projectId
                , categoryId
                , documentID);

            docProperties.LanguageCode = LanguageHelper.GetLanguageCode(visionBot.Language);

            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            IVBotEngine engine = getVBotEngine(projectDetails);

            var imageProcessingsettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);

            VBotDocument vBotDocument = engine.GetVBotDocument(visionBot
                , docProperties
                , validatorDataManager
                , engineSettings
                , imageProcessingsettings
                , documentID
                , projectDetails.ConfidenceThreshold);

            validateAndTryToCorrectDocumentUsingLearningData(projectId, projectDetails.ConfidenceThreshold, visionBot, classifiedFolderManifest, vBotDocument);

            return vBotDocument;
        }

        public VBotDocument ExtractVBotData(string organizationId
            , string projectId
            , string categoryId
            , string visionBotID
            , string layoutID)
        {
            VisionBot visionBot = getVisionBot(organizationId, projectId, categoryId, visionBotID);

            VisionBotEngine.Model.Layout vBotLayout = visionBot.Layouts.Find(x => x.Id.Equals(layoutID));

            VBotDocument vBotDocument = new VBotDocument() { SirFields = vBotLayout.SirFields, DocumentProperties = vBotLayout.DocProperties };
            vBotDocument.Layout.DocProperties = vBotLayout.DocProperties;
            vBotDocument.Layout.DocProperties.Path = _documentProvider.GetDocument(organizationId
                , projectId
                , categoryId
                , vBotLayout.DocProperties.Id)?.Path;
            vBotDocument
                .Layout
                .DocProperties
                .LanguageCode = LanguageHelper.GetLanguageCode(visionBot.Language);

            VBotEngineSettings engineSettings = new VBotEngineSettings();

            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            IVBotEngine engine = getVBotEngine(projectDetails);

            var imageProcessingsettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);

            engine.ExtractVBotData(visionBot
                , vBotDocument
                , _datapath
                , engineSettings
                , imageProcessingsettings);

            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest()
            {
                id = categoryId,
                organizationId = organizationId,
                projectId = projectId
            };

            IVBotValidatorDataReader validatorDataReader = new VBotValidatorDataManager(classifiedFolderManifest, visionBot.Id);
            ValidatorAdvanceInfo validatorAdvanceInfo = getValidatorAdvanceInfo(visionBot, projectDetails.ConfidenceThreshold);

            vBotDocument.Data.EvaluateAndFillValidationDetails(vBotDocument.Layout
                , validatorDataReader
                , validatorAdvanceInfo);
            validateAndTryToCorrectDocumentUsingLearningData(projectId, projectDetails.ConfidenceThreshold, visionBot, classifiedFolderManifest, vBotDocument);

            return vBotDocument;
        }

        public Field GetValueField(string organizationId
            , string projectId
            , string categoryId
            , string visionBotID
            , string layoutID
            , FieldLayout fieldLayout,
            string fieldDefID
            , string docPropsID)
        {
            VisionBot visionBot = getVisionBot(organizationId, projectId, categoryId, visionBotID);

            VisionBotEngine.Model.Layout vBotLayout = visionBot.Layouts.Find(x => x.Id.Equals(layoutID));

            VBotDocument vBotDocument = new VBotDocument() { SirFields = vBotLayout.SirFields, DocumentProperties = vBotLayout.DocProperties };
            vBotDocument.Layout.DocProperties = vBotLayout.DocProperties;
            vBotDocument.Layout.DocProperties.Path = _documentProvider.GetDocument(organizationId
                , projectId
                , categoryId
                , vBotLayout.DocProperties.Id)?.Path;
            vBotDocument
                .Layout
                .DocProperties
                .LanguageCode = LanguageHelper.GetLanguageCode(visionBot.Language);

            FieldDef fieldDef = visionBot?.DataModel?.Fields.FirstOrDefault(x => x.Id == fieldDefID);

            VBotEngineSettings engineSettings = new VBotEngineSettings();

            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            IVBotEngine engine = getVBotEngine(projectDetails);

            var imageProcessingsettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);

            return engine.GetValueField(fieldLayout
                , fieldDef
                , vBotDocument
                , vBotDocument.Layout.DocProperties
                , engineSettings
                , imageProcessingsettings);
        }

        public bool ExportData(VBotData vBotData
            , string exportCSVPath
            , List<string> outputFieldsOrder)
        {
            IExportDataUtility exporter = new ExportDataUtility();
            ExportConfiguration exportConfiguration = new ExportConfiguration() { ExportFilePath = exportCSVPath };

            VBotDocument vBotDocument = new VBotDocument() { Data = vBotData };

            return exporter.ExportData(vBotDocument, exportConfiguration, outputFieldsOrder);
        }

        public bool ProcessDocument(string organizationId
            , string projectId
            , string categoryId
            , string documentId)
        {
            StatisticsLogger.Log(string.Empty);
            StatisticsLogger.Log($"Document processing start");
            StatisticsLogger.Log($"Document Id: {documentId}");
            StatisticsLogger.Log($"Project Id: {projectId}");
            StatisticsLogger.Log($"Category Id: {categoryId}");
            VisionBotMetadata visionBotMetadata = null;
            try
            {
                visionBotMetadata = getVisionbotMetaData(organizationId
               , projectId
               , categoryId);
            }
            catch (WebServiceException ex)
            {
                if (isResourceNotAvailable(ex))
                {
                    sendMessageForDocumentNotProcessedDueToVisionbotNotPresent(organizationId, projectId, documentId);
                }

                throw ex;
            }

            if (visionBotMetadata == null)
            {
                sendMessageForDocumentNotProcessedDueToVisionbotNotPresent(organizationId, projectId, documentId);
                throw new Exception("VisionBot is not available");
            }

            throwExceptionIfVisionBotIsNotInProductionMode(visionBotMetadata);

            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectIsNullOrIsNotInProductionMode(projectDetails);

            VisionBot visionBot = null;
            try
            {
                StatisticsLogger.Log("Fetching Visionbot");

                visionBot = getVisionBot(organizationId
                , projectId
                , categoryId
                , visionBotMetadata.Id);

                StatisticsLogger.Log("Visionbot fetching complete");
            }
            catch (WebServiceException ex)
            {
                if (isResourceNotAvailable(ex))
                {
                    sendMessageForDocumentNotProcessedDueToVisionbotNotPresent(organizationId, projectId, documentId);
                }
            }

            if (visionBot == null)
                throw new Exception("VisionBot is not ready for Production");

            StatisticsLogger.Log("Fetching document");
            DocumentProperties docProperties = _documentProvider.GetDocument(organizationId, projectId, categoryId, documentId);
            docProperties.LanguageCode = LanguageHelper.GetLanguageCode(visionBot.Language);
            StatisticsLogger.Log("Document fetching complete");

            VBotEngineSettings engineSettings = new VBotEngineSettings();

            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest() { id = categoryId, organizationId = organizationId, projectId = projectId };

            IVBotEngine vbotEngine = getVBotEngine(projectDetails);

            var imageProcessingSettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);
            StatisticsLogger.Log("Processing document");
            VBotDocument vBotDocument = generateAndExtractVBotData(vbotEngine
                , visionBot
                , docProperties
                , engineSettings
                , classifiedFolderManifest
                , imageProcessingSettings
                , projectDetails.ConfidenceThreshold);
            StatisticsLogger.Log("Document processing complete");

            validateAndTryToCorrectDocumentUsingLearningData(projectId, projectDetails.ConfidenceThreshold, visionBot, classifiedFolderManifest, vBotDocument);

            var isValidationSuccess = new ExportDataUtility().ValidateVBotData(vBotDocument);

            //
            StatisticsLogger.Log("Saving VBotDocument");
            //ToDo :
            VBotDocumentValidationStatus status = VBotDocumentValidationStatus.Fail;
            if (isValidationSuccess)
                status = VBotDocumentValidationStatus.Pass;

            var reportDictionary = GetVBotReportData(vBotDocument.Data);

            var tupleObject = PrepareLearningData(vBotDocument.Data, vBotDocument.Data);

            Dictionary<string, object> validatorDataModel = new Dictionary<string, object>();
            validatorDataModel.Add("vBotDocument", vBotDocument);
            validatorDataModel.Add("documentProcessingDetails", reportDictionary);
            validatorDataModel.Add("fieldAccuracyList", tupleObject.Item2); ;

            IValidatorServiceEndPointConsumer validatorConsumer = new ValidatorServiceEndPointConsumer(classifiedFolderManifest);
            validatorConsumer.SaveVBotDocument(visionBot.Id, documentId, status.ToString(), validatorDataModel);

            StatisticsLogger.Log("VBotDocument saved");

            StatisticsLogger.Log("Marking document as processed");
            IFileServiceEndPointConsumer fileServices = new FileServiceEndPointConsumer(organizationId, projectId);
            fileServices.UpdateDocumentProcessingStatus(documentId);
            StatisticsLogger.Log("Document marked as processed");

            StatisticsLogger.Log($"Validation Status : {isValidationSuccess.ToString()}");
            StatisticsLogger.Log("Document processing complete");

            return isValidationSuccess;
        }

        public bool ProcessStagingDocument(string organizationId
            , string projectId
            , string categoryId
            , string documentId
            , string visionBotrundetailsid
            , string documentProcessedDetailsId)
        {
            VBotLogger.Trace(() => $"[VisionBotEngineAPI::ProcessStagingDocument] for {documentId}");

            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            FileDetails fileDetails = getFileDetails(organizationId, projectId, documentId);

            throwExceptionIfFileDetailsIsNullOrIsInProduction(fileDetails);

            VisionBotMetadata visionBotMetadata = getVisionbotMetaData(organizationId
               , projectId
               , categoryId);

            throwExceptionIfVisionbotMetaDataIsNullOrNotInStagingMode(visionBotMetadata);

            VisionBot visionBot = getVisionBot(organizationId
                , projectId, categoryId, visionBotMetadata.Id);

            throwExceptionIfVisionBotIsNull(visionBot);

            DocumentProperties docProperties = _documentProvider.GetDocument(organizationId
                , projectId, categoryId, documentId);
            docProperties.LanguageCode = LanguageHelper.GetLanguageCode(visionBot.Language);

            VBotEngineSettings engineSettings = new VBotEngineSettings();

            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest()
            { id = categoryId, organizationId = organizationId, projectId = projectId };

            var imageProcessingSettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);

            IVBotEngine vbotEngine = getVBotEngine(projectDetails);

            VBotDocument vBotDocument = generateAndExtractVBotData(vbotEngine
                , visionBot
                , docProperties
                , engineSettings
                , classifiedFolderManifest
                , imageProcessingSettings
                , projectDetails.ConfidenceThreshold);

            validateAndTryToCorrectDocumentUsingLearningData(projectId, projectDetails.ConfidenceThreshold, visionBot, classifiedFolderManifest, vBotDocument);

            var reportDictionary = GetVBotReportData(vBotDocument.Data);
            reportDictionary.Add("visionBotRunDetailsId", visionBotrundetailsid);
            reportDictionary.Add("id", documentProcessedDetailsId);

            IVisionbotServiceEndPointConsumer visionBotService = new VisionbotServiceEndPointConsumer(classifiedFolderManifest);
            visionBotService.UpdateProcessedDocumentDetails(visionBot.Id
               , visionBotrundetailsid
               , documentId
               , reportDictionary
               );

            IFileServiceEndPointConsumer fileServices = new FileServiceEndPointConsumer(organizationId, projectId);
            fileServices.UpdateDocumentProcessingStatus(documentId);

            return true;
        }

        public SegmentedDocument GetDocumentVectorDetails(string organizationId
             , string projectId
             , DocumentProperties docProperties
             , ProcessingMode mode
             , SegmentedDocumentCacheConfiguration segmentedDocumentCacheConfiguration)
        {
            var projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            docProperties.LanguageCode = LanguageHelper.GetLanguageCode(projectDetails.PrimaryLanguage);

            OCREngineProviderBasedOnProject ocrEngineProvider = new OCREngineProviderBasedOnProject(new OcrEngineStratagy());
            IOcrEngine ocrEngine = ocrEngineProvider.GetOcrEngine(projectDetails);

            IPdfEngine pdfEngine = PdfEngineStrategy.GetPdfEngine();

            IDocumentVectorDetailsCacheManager documentVectorDetailsCacheManager = DocumentVectorDetailsCacheManagerStrategy
                .GetDocumentVectorDetailsCacheManager();

            var imageProcessingsettings = Common.ImageProcessingConfigHelper
                .GetImageProcessingSettings(projectDetails);

            VBotEngineSettings engineSettings = new VBotEngineSettings();

            IVisionBotPageImageProvider visionBotPageImageProvider =
                 new VisionBotPageImageProvider(null, docProperties, imageProcessingsettings, engineSettings);

            SegmentedDocumentProvider segmentedDocumentProvider = new SegmentedDocumentProvider(ocrEngine
                , pdfEngine, documentVectorDetailsCacheManager);

            return segmentedDocumentProvider.GetSegmentedDocument(docProperties
                 , mode
                 , visionBotPageImageProvider
                 , engineSettings
                 , docProperties.Id
                 , segmentedDocumentCacheConfiguration);
        }

        public Dictionary<string, object> GetVBotReportData(VBotData vBotData)
        {
            int totalField = vBotData.GetTotalFieldCount();
            int failedField = vBotData.GetAllInvalidFieldData().Count;
            Dictionary<string, object> reportData = new Dictionary<string, object>();
            reportData.Add("totalFieldCount", totalField);
            reportData.Add("passedFieldCount", totalField - failedField);
            reportData.Add("failedFieldCount", failedField);
            reportData.Add("processingStatus", failedField == 0 ? VBotDocumentValidationStatus.Pass.ToString() : VBotDocumentValidationStatus.Fail.ToString());

            return reportData;
        }

        #endregion Public Methods

        #region Private Methods

        private VBotDocument generateAndExtractVBotData(IVBotEngine vBotEngine
            , VisionBot visionBot
            , DocumentProperties docProps
            , VBotEngineSettings engineSettings
            , ClassifiedFolderManifest manifest
            , ImageProcessingConfig imageProcessingSettings
            , int confidenceThreshold)
        {
            IVBotValidatorDataManager vbotValidationDataManager = new VBotValidatorDataManager(manifest
                , visionBot.Id);

            return vBotEngine.GetVBotDocument(visionBot
                , docProps, vbotValidationDataManager
                , engineSettings
                , imageProcessingSettings
                , docProps.Id
                , confidenceThreshold);
        }

        private VisionBotMetadata getVisionbotMetaData(string organizationId
            , string projectId
            , string categoryId)
        {
            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest { organizationId = organizationId, projectId = projectId, id = categoryId };
            IVisionbotServiceEndPointConsumer visionBotService = new VisionbotServiceEndPointConsumer(classifiedFolderManifest);
            Console.WriteLine($"{classifiedFolderManifest.id},{classifiedFolderManifest.name}, {classifiedFolderManifest.organizationId}, {classifiedFolderManifest.projectId}");
            VisionBotMetadata visionBotMetadata = visionBotService.GetVisionBotMetadata(classifiedFolderManifest);
            return visionBotMetadata;
        }

        private VisionBotEngine.Model.VisionBot getVisionBot(string organizationId
            , string projectId
            , string categoryId
            , string visionBotID)
        {
            VBotLogger.Trace(() => "[VisionBotManager::getVisionBot] getting VisionBot ...");

            ClassifiedFolderManifest classifiedFolderManifest = new ClassifiedFolderManifest { organizationId = organizationId, projectId = projectId, id = categoryId };
            IVisionbotServiceEndPointConsumer visionBotService = new VisionbotServiceEndPointConsumer(classifiedFolderManifest);
            return visionBotService.LoadVisionBot(new VisionBotManifest() { Id = visionBotID });
        }

        private static FileDetails getFileDetails(string organizationId
            , string projectId
            , string documentId)
        {
            IFileServiceEndPointConsumer fileService = new FileServiceEndPointConsumer(organizationId, projectId);
            FileDetails fileDetails = fileService.GetDocumentDetails(documentId);
            return fileDetails;
        }

        private static ProjectDetails getProjectMetaDetails(string organizationId, string projectId)
        {
            IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
            ProjectDetails projectDetails = projectService.GetProjectMetaData(organizationId, projectId);

            return projectDetails;
        }

        private static void throwExceptionIfProjectDetailsIsNull(ProjectDetails projectDetails)
        {
            if (projectDetails == null)
            {
                throw new Exception("project is not available");
            }
        }

        private static void throwExceptionIfProjectIsNullOrIsNotInProductionMode(ProjectDetails projectDetails)
        {
            throwExceptionIfProjectDetailsIsNull(projectDetails);

            if (projectDetails.Environment != "production")
            {
                throw new Exception("project is not ready for Production");
            }
        }

        private static void throwExceptionIfFileDetailsIsNullOrIsInProduction(FileDetails fileDetails)
        {
            if (fileDetails == null || fileDetails.IsProduction)
            {
                throw new Exception("Document came for processing is not staging document.");
            }
        }

        private static void throwExceptionIfVisionBotIsNull(VisionBot visionBot)
        {
            if (visionBot == null)
                throw new Exception("VisionBot is not created");
        }

        private static void throwExceptionIfVisionbotMetaDataIsNull(VisionBotMetadata visionBotMetadata)
        {
            if (visionBotMetadata == null)
                throw new Exception("VisionBot is not available");
        }

        private static void throwExceptionIfVisionbotMetaDataIsNullOrNotInStagingMode(VisionBotMetadata visionBotMetadata)
        {
            throwExceptionIfVisionbotMetaDataIsNull(visionBotMetadata);

            if (visionBotMetadata.Environment != VisionBotEnvironment.staging.ToString())
                throw new Exception("VisionBot is not in staging mode");
        }

        private static void throwExceptionIfVisionBotIsNotInProductionMode(VisionBotMetadata visionBotMetadata)
        {
            if (visionBotMetadata.Environment != VisionBotEnvironment.production.ToString())
                throw new Exception("VisionBot is not ready for Production");
        }

        private static bool isResourceNotAvailable(WebServiceException ex)
        {
            return ex.Error.Code == "404";
        }

        private static void sendMessageForDocumentNotProcessedDueToVisionbotNotPresent(string organizationId
            , string projectId
            , string documentId)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("documentId", documentId);
            data.Add("projectId", projectId);
            data.Add("organizationId", organizationId);

            try
            {
                TopicExchangePublisher publisher = new TopicExchangePublisher(Constants.CDC.IQBOT_MQ_EXCHANGE, getRabbitMQSetting());
                publisher.PublishMessage(TopicExchangePublisher.TOPIC_FILE_PROCESSING_UNPROCESSED_VISIONBOT_NOT_FOUND, Newtonsoft.Json.JsonConvert.SerializeObject(data));
            }
            catch (Exception ex)
            {
                ex.Log("VisionBotEngineAPI", "sendMessageForDocumentNotProcessedDueToVisionbotNotPresent");
            }
        }

        private VBotEngine getVBotEngine(string organizationId
           , string projectId)
        {
            ProjectDetails projectDetails = getProjectMetaDetails(organizationId, projectId);

            throwExceptionIfProjectDetailsIsNull(projectDetails);

            VBotEngine engine = getVBotEngine(projectDetails);
            return engine;
        }

        private VBotEngine getVBotEngine(ProjectDetails projectDetails)
        {
            return new VBotEngine(_ocrEngineProvider.GetOcrEngine(projectDetails)
                , _pdfEngine
                , _documentVectorDetailsCacheManger);
        }

        internal VBotEngineSettings GetEngineSettings(bool isPreProcessed, bool preserveGrayscaledPages)
        {
            VBotEngineSettings engineSettings = new VBotEngineSettings();
            engineSettings.MaxDegreeOfParallelism = Configurations.Visionbot.NoOfParallelExtractionsInEngine;
            engineSettings.IsCheckBoxFeatureEnabled = Configurations.Visionbot.IsCheckBoxRecognitionFeatureEnabled;
            engineSettings.IsAccuracyLevelEnabled = Configurations.Visionbot.IsAccuracyLevelEnabled;
            return engineSettings;
        }

        private static void validateAndTryToCorrectDocumentUsingLearningData(string projectId, int confidenceThreshold, VisionBot visionBot, ClassifiedFolderManifest classifiedFolderManifest, VBotDocument vBotDocument)
        {
            MLAutoCorrectionSettings mlAutoCorrectionSettings = new ServiceLocationProvider().GetMLAutoCorrectionSetting();
            if (mlAutoCorrectionSettings.IsEnable)
            {
                StatisticsLogger.Log("Validating document");
                bool isValidationSuccess = new ExportDataUtility().ValidateVBotData(vBotDocument);
                StatisticsLogger.Log("Validating document complete");

                if (!isValidationSuccess)
                {
                    tryToCorrectDocumentUsingLearningData(projectId, classifiedFolderManifest, visionBot, confidenceThreshold, vBotDocument, mlAutoCorrectionSettings.Threshold);
                }
            }
        }

        private static void tryToCorrectDocumentUsingLearningData(string projectId,
                                                                  ClassifiedFolderManifest classifiedFolderManifest,
                                                                  VisionBot visionBot,
                                                                  int confidenceThreshold,
                                                                  VBotDocument vBotDocument,
                                                                  decimal mlAutoCorrectionThreshold
                                                                 )
        {
            VBotLogger.Trace(() => string.Format("[VisionBotEngineAPI -> tryToCorrectDocumentUsingLearningData]"));

            IVBotValidatorDataReader validatorDataReader = new VBotValidatorDataManager(classifiedFolderManifest, visionBot.Id);
            ValidatorAdvanceInfo validatorAdvanceInfo = getValidatorAdvanceInfo(visionBot, confidenceThreshold);
            MLAutoCorrectionEngine learningModelValueCorrection =
                new MLAutoCorrectionEngine(new MachineLearningEndpointConsumerService(), mlAutoCorrectionThreshold);

            learningModelValueCorrection.FieldAutoCorrectionUsingLearningModel(projectId, vBotDocument, validatorDataReader, validatorAdvanceInfo);
            /* We need to call validation once again to revalidate fields against Formula Validation (FieldAutoCorrectionUsingLearningModel skips formula validation as it works on field level.)
             */
            vBotDocument.EvaluateAndFillValidationDetails(validatorDataReader, validatorAdvanceInfo);
        }

        private static ValidatorAdvanceInfo getValidatorAdvanceInfo(VisionBot visionBot, int confidenceThreshold)
        {
            return new ValidatorAdvanceInfo()
            {
                CultureInfo = Cognitive.VisionBotEngine.Model.Generic.CultureHelper.GetCultureInfo(visionBot.Language),
                ConfidenceThreshold = confidenceThreshold
            };
        }

        private DocumentProperties downloadDefaultLayoutDocument(string organizationId, string projectId, string categoryId)
        {
            IFileServiceEndPointConsumer fileService = new FileServiceEndPointConsumer(organizationId, projectId);
            var files = fileService.GetStagingDocumentsDetailsByCategory(categoryId);

            if (files != null && files.Count > 0)
            {
                return _documentProvider.GetDocument(organizationId, projectId, categoryId, files[0].FileId);
            }

            return null;
        }

        #endregion Private Methods

        //Move it to proper place
        private Tuple<List<FieldValueLearningDataModel>, List<FieldAccuracyModel>> PrepareLearningData(VBotData failData, VBotData correctedData)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorWorkflowService -> PrepareLearningData]"));
            LevenshteinSimilarityCalculator similarityCalculator = new LevenshteinSimilarityCalculator();
            List<FieldAccuracyModel> fieldAccuracylist = new List<FieldAccuracyModel>();
            List<FieldValueLearningDataModel> learningdatalist = new List<FieldValueLearningDataModel>();
            for (int i = 0; i < failData.FieldDataRecord.Fields.Count; i++)
            {
                FieldAccuracyModel fieldAccuracy = new FieldAccuracyModel();
                String oldvalue = failData.FieldDataRecord.Fields[i].Value.Field.Text;
                string newvalue = string.Empty;
                if (failData.FieldDataRecord.Fields[i].ValidationIssue == null)
                {
                    newvalue = correctedData.FieldDataRecord.Fields[i].Value.Field.Text;
                }

                fieldAccuracy.FieldId = failData.FieldDataRecord.Fields[i].Field.Id;
                fieldAccuracy.OldValue = oldvalue;

                fieldAccuracy.NewValue = newvalue;
                if (oldvalue == newvalue && correctedData.FieldDataRecord.Fields[i].ValidationIssue == null)
                {
                    //IF OLD AND NEW VALUE ARE SAME, MEANS INCREASE PASS BY ONE
                    fieldAccuracy.PassAccuracyValue = 1;
                }

                fieldAccuracylist.Add(fieldAccuracy);
            }
            for (int i = 0; i < failData.TableDataRecord.Count; i++)
            {
                for (int j = 0; j < correctedData.TableDataRecord[i].Rows.Count; j++)
                {
                    for (int k = 0; k < correctedData.TableDataRecord[i].Rows[j].Fields.Count; k++)
                    {
                        FieldAccuracyModel fieldAccuracy = new FieldAccuracyModel();

                        String oldvalue = failData.TableDataRecord[i].Rows.Count < j + 1 ? "" :
                            failData.TableDataRecord[i].Rows[j].Fields[k].Value.Field.Text;
                        string newvalue = string.Empty;
                        if (correctedData.TableDataRecord[i].Rows[j].Fields[k].ValidationIssue == null)
                        {
                            newvalue = correctedData.TableDataRecord[i].Rows[j].Fields[k].Value.Field.Text;
                        }

                        fieldAccuracy.FieldId = failData.TableDataRecord[i].Rows[0].Fields[k].Field.Id;
                        fieldAccuracy.OldValue = oldvalue;
                        fieldAccuracy.NewValue = newvalue;

                        if (oldvalue == newvalue && correctedData.TableDataRecord[i].Rows[j].Fields[k].ValidationIssue == null)
                        {
                            fieldAccuracy.PassAccuracyValue = 1;
                        }
                        if (oldvalue != "" && oldvalue != "" &&
                                oldvalue != newvalue)
                        {
                            fieldAccuracy.PassAccuracyValue = (float)similarityCalculator.GetSimilarity(oldvalue, newvalue);
                        }
                        fieldAccuracylist.Add(fieldAccuracy);
                    }
                }
            }
            //return learningdatalist;
            return Tuple.Create(learningdatalist, fieldAccuracylist);
        }

        private static RabbitMQSetting getRabbitMQSetting()
        {
            var rabbitMQSetting = new ServiceLocationProvider().GetRabbitMQSetting();
            rabbitMQSetting.Password = CognitiveSecurePropertiesProvider.Instance().GetMessageQueuePassword();
            return rabbitMQSetting;
        }
    }
}