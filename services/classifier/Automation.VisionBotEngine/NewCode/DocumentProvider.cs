﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.NewCode
{
    internal class DocumentProvider
    {
        private string documentRootDir;

        public DocumentProvider()
        {
            documentRootDir = Path.Combine(Path.GetTempPath(), "VisionBot", "OriginalDocument");
            if (!Directory.Exists(documentRootDir))
                Directory.CreateDirectory(documentRootDir);
        }

        public DocumentProperties GetDocument(string organizationId, string projectId, string categoryId, string documentId)
        {
            VBotLogger.Trace(() => $"[DocumentProvider::GetDocument] documentId: {documentId}");
            DocumentProperties documentProperties = new DocumentProperties();

            SetTrainedFieldKeysFromAliasService(organizationId, projectId);

            IFileServiceEndPointConsumer fileService = new FileServiceEndPointConsumer(organizationId, projectId);

            FileDetails fileDetails = fileService.GetDocumentDetails(documentId);

            if (fileDetails == null)
            {
                VBotLogger.Trace(() => $"[DocumentProvider::GetDocument] Unable to retrive filedetails: {documentId}");
                throw new Exception("Unable to retrive file details.");
            }

            string fileExtension = fileDetails?.Format;
            if (!Directory.Exists(documentRootDir))
                Directory.CreateDirectory(documentRootDir);

            documentProperties.Id = fileDetails.FileId;
            documentProperties.Name = fileDetails.FileName;

            string filePath = Path.Combine(documentRootDir, documentId + "." + fileExtension);

            if (File.Exists(filePath))
            {
                string newFilePath = Path.Combine(documentRootDir, documentId + "_" + Guid.NewGuid() + "." + fileExtension);
                try
                {
                    File.Copy(filePath, newFilePath);
                    filePath = newFilePath;
                }
                catch (Exception ex)
                { }

                VBotLogger.Trace(() => $"[DocumentProvider::GetDocument] using existing file : {filePath}");
            }
            else
            {
                byte[] data = fileService.GetDocument(documentId);

                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    stream.Write(data, 0, data.Length);
                }
                VBotLogger.Trace(() => $"[DocumentProvider::GetDocument] created new file : {filePath}");
            }

            documentProperties.Path = filePath;

            return documentProperties;
            //return filePath;
        }

        private void SetTrainedFieldKeysFromAliasService(string organizationId, string projectId)
        {
            VBotLogger.Trace(() => $"[DocumentProvider::SetTrainedFieldKeysFromAliasService]");
            IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
            ProjectDetails projectDetail = projectService.GetProjectMetaData(organizationId, projectId);
            try
            {
                new AliasServiceFieldKeyStorage().SetFieldKeys(projectDetail.ProjectTypeId, projectDetail.PrimaryLanguage);
            }
            catch (Exception ex)
            {
                VBotLogger.Trace(() => $"[DocumentProvider::SetTrainedFieldKeysFromAliasService]: Project Detail not found");
            }
        }
    }
}