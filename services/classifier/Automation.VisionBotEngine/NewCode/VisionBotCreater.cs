﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Cognitive.VisionBotEngine.Common;
using Automation.Cognitive.VisionBotEngine.Model.V2.Project;
using Automation.Services.Client;
using Automation.VisionBotEngine.Configuration;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Automation.VisionBotEngine.NewCode
{
    internal class VisionBotCreater
    {
        private DocumentProvider _documentProvider = new DocumentProvider();
        private IAliasServiceEndPointConsumer _aliasService;
        private VBotEngine _engine;

        #region public methods

        public VisionBotCreater(VBotEngine engine
            , IAliasServiceEndPointConsumer aliasService)
        {
            _engine = engine;
            _aliasService = aliasService;
        }

        public VisionBot CreateBasicVisionBot(string visionBotDetailId
            , string name
            , ProjectDetailsV2 projectDetails
            , DocumentProperties documentProperties = null)
        {
            VBotLogger.Trace(() => $"[VisionBotCreator] [Started] [VisionBotId:{visionBotDetailId},Group Name:{name}].");
            List<ProjectStandardField> standardFields;
            List<ProjectCustomField> customFields;
            List<ProjectStandardField> tableStandardFields;
            List<ProjectCustomField> tableCustomFields;

            segregateAndFillFieldsV2(projectDetails
                , out standardFields
                , out customFields
                , out tableStandardFields
                , out tableCustomFields);

            VisionBot visionBot = new VisionBot()
            {
                Id = visionBotDetailId,
                Name = name
            };

            visionBot.Language = projectDetails.PrimaryLanguage;
            
            addNewStandardField(visionBot, standardFields);
            addNewCustomField(visionBot, customFields);
            addNewStandardTableField(visionBot, tableStandardFields);
            addNewCustomTableField(visionBot, tableCustomFields);

            if (documentProperties != null)
                fillLayoutInformation(visionBot, documentProperties, projectDetails);

            return visionBot;
        }

        public VisionBot AddNewFields(VisionBot visionBot, ProjectDetailsV2 projectDetails)
        {
            List<ProjectStandardField> standardFields;
            List<ProjectCustomField> customFields;
            List<ProjectStandardField> tableStandardFields;
            List<ProjectCustomField> tableCustomFields;

            segregateAndFillFieldsV2(projectDetails
                , out standardFields
                , out customFields
                , out tableStandardFields
                , out tableCustomFields);
            
            addNewStandardField(visionBot, standardFields.Where(f => f.IsAddedLater == true).ToList());
            addNewCustomField(visionBot, customFields.Where(f => f.IsAddedLater == true).ToList());
            addNewStandardTableField(visionBot, tableStandardFields.Where(f => f.IsAddedLater == true).ToList());
            addNewCustomTableField(visionBot, tableCustomFields.Where(f => f.IsAddedLater == true).ToList());

            return visionBot;
        }

        private static string ConvertToFieldName(string fieldName)
        {
            return fieldName.Replace(" ", "_");
        }

        private void addNewStandardField(VisionBot visionBot, List<ProjectStandardField> fieldList)
        {
            foreach (ProjectStandardField standardField in fieldList)
            {
                if (visionBot.DataModel.Fields.Exists(f => f.Name.Equals(ConvertToFieldName(standardField.FieldName))))
                {
                    continue;
                }

                VBotLogger.Debug(() => $"[VisionBotCreator] [addNewStandardField] Adding field {standardField.FieldName} to visionBot id {visionBot.Id}.");
                visionBot.DataModel.Fields.Add(toFieldDef(standardField));
            }
        }

        private void addNewCustomField(VisionBot visionBot, List<ProjectCustomField> fieldList)
        {
            foreach (ProjectCustomField customField in fieldList)
            {
                if (visionBot.DataModel.Fields.Exists(f => f.Name.Equals(ConvertToFieldName(customField.name))))
                    continue;

                VBotLogger.Debug(() => $"[VisionBotCreator] [addNewCustomField] Adding field {customField.name} to visionBot id {visionBot.Id}.");
                visionBot.DataModel.Fields.Add(toFieldDef(customField));
            }
        }

        private void addNewStandardTableField(VisionBot visionBot, List<ProjectStandardField> fieldList)
        {
            if (visionBot.DataModel.Tables.Count == 0)
            {
                VBotLogger.Debug(() => $"[VisionBotCreator] [addNewStandardTableField] No table found. Adding all fields to new table in visionBot id {visionBot.Id}.");
                TableDef tableField = new TableDef() { Name = Constants.CDC.DEFAULT_TABLE_NAME };

                tableField.Columns.AddRange(convertStandProjectFieldsToFieldDefs(fieldList));
                visionBot.DataModel.Tables.Add(tableField);
                return;
            }

            foreach (ProjectStandardField standardField in fieldList)
            {
                bool isFieldFound = false;

                foreach (TableDef table in visionBot.DataModel.Tables)
                {
                    if (table.Columns.Exists(f => f.Name.Equals(ConvertToFieldName(standardField.FieldName))))
                    {
                        isFieldFound = true;
                        break;
                    }
                }

                if (!isFieldFound)
                {
                    VBotLogger.Debug(() => $"[VisionBotCreator] [addNewStandardTableField] Adding field {standardField.FieldName} to visionBot id {visionBot.Id}.");
                    visionBot.DataModel.Tables[0].Columns.Add(toFieldDef(standardField));
                }
            }
        }

        private void addNewCustomTableField(VisionBot visionBot, List<ProjectCustomField> fieldList)
        {
            if (visionBot.DataModel.Tables.Count == 0)
            {
                VBotLogger.Debug(() => $"[VisionBotCreator] [addNewCustomTableField] No table found. Adding all fields to new table in visionBot id {visionBot.Id}.");
                TableDef tableField = new TableDef() { Name = Constants.CDC.DEFAULT_TABLE_NAME };

                tableField.Columns.AddRange(convertCustomProjectFieldsToFieldDefs(fieldList));
                visionBot.DataModel.Tables.Add(tableField);
                return;
            }

            foreach (ProjectCustomField customField in fieldList)
            {
                bool isFieldFound = false;

                foreach (TableDef table in visionBot.DataModel.Tables)
                {
                    if (table.Columns.Exists(f => f.Name.Equals(ConvertToFieldName(customField.name))))
                    {
                        isFieldFound = true;
                        break;
                    }
                }

                if (!isFieldFound)
                {
                    VBotLogger.Debug(() => $"[VisionBotCreator] [addNewCustomTableField] Adding field {customField.name} to visionBot id {visionBot.Id}.");
                    visionBot.DataModel.Tables[0].Columns.Add(toFieldDef(customField));
                }
            }
        }

        private void segregateAndFillFieldsV2(ProjectDetailsV2 projectDetails
            , out List<ProjectStandardField> standardFields
            , out List<ProjectCustomField> customFields
            , out List<ProjectStandardField> tableStandardFields
            , out List<ProjectCustomField> tableCustomFields)
        {
            standardFields = new List<ProjectStandardField>();
            tableStandardFields = new List<ProjectStandardField>();
            foreach (StandardFieldV2 field in projectDetails.Fields.Standard)
            {
                ProjectStandardField standardField = _aliasService.GetProjectStandardField(field.Id);
                standardField.IsAddedLater = field.IsAddedLater;

                if (standardField.FieldType == ProjectFieldType.FormField)
                {
                    standardFields.Add(standardField);
                }
                else
                {
                    tableStandardFields.Add(standardField);
                }
            }

            customFields = new List<ProjectCustomField>();
            tableCustomFields = new List<ProjectCustomField>();
            foreach (CustomFieldV2 customField in projectDetails.Fields.Custom)
            {

                if (customField.Type == ProjectFieldType.FormField)
                {
                    customFields.Add(convertCustomFieldV2ToProjectCustomField(customField));
                }
                else
                {
                    tableCustomFields.Add(convertCustomFieldV2ToProjectCustomField(customField));
                }
            }
        }

        #endregion public methods

        #region private methods

        private ProjectCustomField convertCustomFieldV2ToProjectCustomField(CustomFieldV2 customFieldV2)
        {
            return new ProjectCustomField {
                id = customFieldV2.Id,
                name = customFieldV2.Name,
                FieldValueType = customFieldV2.FieldValueType,
                type = customFieldV2.Type,
                IsAddedLater = customFieldV2.IsAddedLater
            };
        }

        private List<FieldDef> convertStandProjectFieldsToFieldDefs(List<ProjectStandardField> standardFields)
        {
            var result = new List<FieldDef>();

            foreach (var fields in standardFields)
            {
                result.Add(toFieldDef(fields));
            }

            return result;
        }

        private List<FieldDef> convertCustomProjectFieldsToFieldDefs(List<ProjectCustomField> customFields)
        {
            var result = new List<FieldDef>();

            foreach (ProjectCustomField field in customFields)
            {
                result.Add(toFieldDef(field));
            }

            return result;
        }

        private static FieldDef toFieldDef(ProjectStandardField field)
        {
            return createFieldDef(field.FieldId
                , field.FieldName
                , field.FieldValueType
                , !field.IsAddedLater);
        }

        private static FieldDef toFieldDef(ProjectCustomField field)
        {
            return createFieldDef(field.id
                 , field.name
                 , field.FieldValueType
                 , !field.IsAddedLater);
        }

        private static FieldDef createFieldDef(string id
            , string name
            , FieldValueType valueType
            , bool isRequired)
        {
            return new FieldDef()
            {
                Id = id,
                Name = ConvertToFieldName(name),
                ValueType = valueType,
                IsRequired = isRequired
            };
        }


        private void fillLayoutInformation(VisionBot visionBot
             , DocumentProperties documentProperties
            , ProjectDetailsV2 projectDetails)
        {
            buildDefaultLayout(visionBot, documentProperties, projectDetails);
        }

        private void buildDefaultLayout(VisionBot visionBot
            , DocumentProperties documentProperties
            , ProjectDetailsV2 projectDetails = null)
        {
            VBotLogger.Trace(() => $"[VisionBotCreator] [BuildDefaultLayout] [ProjectId:{projectDetails?.Id},VisionBotName:{visionBot.Name}].");
            if (string.IsNullOrWhiteSpace(documentProperties?.Path))
                return;

            Layout layout = new Layout
            {
                Name = "Layout1",
                DocProperties = documentProperties
            };

            var docProperties = createlocalDocumentProperties(documentProperties);

            docProperties.LanguageCode = LanguageHelper
                .GetLanguageCode(visionBot.Language);

            VBotEngineSettings engineSettings = new VBotEngineSettings();
            ImageProcessingConfig settings = new ImageProcessingConfig();
            IVBotValidatorDataManager validatorDataManager = null;

            var vBotDocument = _engine.GetVBotDocument(visionBot
                , docProperties
                , validatorDataManager
                , engineSettings
                , settings
                , docProperties.Id
                , projectDetails.ConfidenceThreshold);

            layout.DocProperties = vBotDocument.DocumentProperties;

            setVisionBotLayout(layout, vBotDocument, visionBot);

            var visionBotLayout = visionBot.Layouts[0];

            fillDefinationMappingInfoInToLayout(visionBot
                , projectDetails
                , vBotDocument
                , visionBotLayout);
        }

        private void fillDefinationMappingInfoInToLayout(VisionBot visionBot
            , ProjectDetailsV2 projectDetails
            , VBotDocument vBotDocument
            , Layout visionBotLayout)
        {
            VBotLogger.Trace(() => $"[VisionBotCreator] [AutoMapping] [VisionBotName:{visionBot?.Name},Layout:{visionBotLayout.Name}].");

            fillFieldMappingInfo(visionBot
                , projectDetails
                , vBotDocument
                , visionBotLayout);

            fillTableMappingInfo(visionBot
                , projectDetails
                , vBotDocument
                , visionBotLayout);
        }

        private void fillTableMappingInfo(VisionBot visionBot
            , ProjectDetailsV2 projectDetails
            , VBotDocument vBotDocument
            , Layout visionBotLayout)
        {
            VBotLogger.Trace(() => $"[VisionBotCreator] [TableMapping] [FieldMapping] [VisionBotName:{visionBot?.Name},Layout:{visionBotLayout.Name}].");
            foreach (TableDef tableDefination in visionBot.DataModel.Tables)
            {
                TableLayout tableLayout = new TableLayout()
                {
                    TableId = tableDefination.Id
                };

                foreach (FieldDef fieldDefination in tableDefination.Columns)
                {
                    FieldLayout fieldLayout;

                    if (tryToCreateFieldLayout(projectDetails
                        , vBotDocument
                        , visionBotLayout
                        , fieldDefination
                        , out fieldLayout, true))
                    {
                        if ((tableLayout.PrimaryColumn == null || (tableLayout.PrimaryColumn != null && string.IsNullOrEmpty(tableLayout.PrimaryColumn.FieldId)))
                            && fieldDefination.ValueType == FieldValueType.Number)
                        {
                            tableLayout.PrimaryColumn = fieldLayout;
                        }
                        tableLayout.Columns.Add(fieldLayout);
                    }
                }

                if (tableLayout.Columns.Count > 0)
                {
                    visionBotLayout.Tables.Add(tableLayout);
                }
            }
        }

        private void fillFieldMappingInfo(VisionBot visionBot
            , ProjectDetailsV2 projectDetails
            , VBotDocument vBotDocument
            , Layout visionBotLayout)
        {
            VBotLogger.Trace(() => $"[VisionBotCreator] [AutoMapping] [FieldMapping] [VisionBotName:{visionBot?.Name},Layout:{visionBotLayout.Name}].");
            foreach (FieldDef fieldDefination in visionBot.DataModel.Fields)
            {

                FieldLayout fieldLayout;

                if (tryToCreateFieldLayout(projectDetails
                    , vBotDocument
                    , visionBotLayout
                    , fieldDefination
                    , out fieldLayout
                    , false))
                {
                    visionBotLayout.Fields.Add(fieldLayout);
                }
            }
        }

        private bool tryToCreateFieldLayout(ProjectDetailsV2 projectDetails
            , VBotDocument vBotDocument
            , Layout visionBotLayout
            , FieldDef fieldDefination
            , out FieldLayout fieldLayout
            , bool isTableFieldDef)
        {
            bool isFound = false;

            fieldLayout = new FieldLayout()
            {
                Id = Guid.NewGuid().ToString(),
                FieldId = fieldDefination.Id,
                ValueType = FieldType.SystemField,
                IsValueBoundAuto = true,
                Type = FieldType.SystemField
            };

            VBotLogger.Trace(() => $"[Automapping] [Entered] [FieldId:{fieldDefination.Id}]");
            try
            {
                string[] aliases = null;

                CustomFieldV2 customField = null;
                if (projectDetails?.Fields?.Custom != null)
                {
                    customField = projectDetails.Fields.Custom.Find(x => x.Id == fieldDefination.Id);
                }

                if (customField == null)
                {
                    AliasDetail aliasDetail = _aliasService.GetAliasDetails(fieldLayout.FieldId, projectDetails.PrimaryLanguage);

                    if (aliasDetail != null)
                    {
                        aliases = aliasDetail.aliases.ToArray();
                    }
                }
                else
                {
                    aliases = new string[] { customField.Name };
                }

                if (aliases == null || aliases.Length == 0)
                {
                    return isFound;
                }

                aliases = aliases.OrderByDescending(x => x.Length).ToArray();

                double orgSimilarityFactor = fieldLayout.SimilarityFactor;
                foreach (string label in aliases)
                {
                    VBotLogger.Trace(() => $"[Automapping] [StartForAlias] [Alias:{label}].");
                    fieldLayout.IsValueBoundAuto = true;
                    fieldLayout.Label = label;
                    fieldLayout.FieldId = fieldDefination.Id;
                    fieldLayout.ValueType = FieldType.SystemField;
                    fieldLayout.IsValueBoundAuto = true;
                    fieldLayout.Type = FieldType.SystemField;
                    fieldLayout.SimilarityFactor = 0.85f;
                    fieldLayout = new VBotDocumentGenerator(null, visionBotLayout.DocProperties).UpdateFieldLayout(vBotDocument, fieldLayout);

                    if (!fieldLayout.Label.IsNullOrEmpty())
                    {
                        Field valueField = null;
                        if (!isTableFieldDef)
                            valueField = new VBotDocumentGenerator(null, visionBotLayout.DocProperties)
                              .GetFieldValue(vBotDocument, new FieldOperations(), fieldDefination, fieldLayout);
                        if (fieldLayout != null && !string.IsNullOrEmpty(fieldLayout.Id))
                        {
                            fieldLayout.Label = fieldLayout.DisplayValue;
                            fieldLayout.SimilarityFactor = orgSimilarityFactor;
                            if (!isTableFieldDef)
                            {
                                fieldLayout.ValueBounds = new System.Drawing.Rectangle()
                                {
                                    X = valueField.Bounds.X - fieldLayout.Bounds.X,
                                    Y = valueField.Bounds.Y - fieldLayout.Bounds.Y,
                                    Width = valueField.Bounds.Width,
                                    Height = valueField.Bounds.Height
                                };
                                fieldLayout.DisplayValue = valueField.Text;
                            }
                            else
                            {
                                fieldLayout.ValueBounds = new System.Drawing.Rectangle()
                                {
                                    X = 0,
                                    Y = 0,
                                    Width = fieldLayout.Bounds.Width,
                                    Height = fieldLayout.Bounds.Height
                                };
                            }

                            isFound = true;
                            VBotLogger.Trace(() => $"[Automapping] [Successful] key found based on aliase :{label}.");

                            break;
                        }
                    }
                    VBotLogger.Trace(() => $"[Automapping] [Unsuccessful] key not found based on aliase :{label}.");
                }

            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => $"[Automapping Unsuccessful] [Exception] [Project:{projectDetails.Id},DocId:{visionBotLayout.DocProperties.Id},FieldId:{fieldDefination.Id},FieldName:{fieldDefination.Name},] [msg:{ex.Message}]");
            }

            return isFound;
        }



        private DocumentProperties createlocalDocumentProperties(DocumentProperties docProperties)
        {
            DocumentProperties documentProperties = new DocumentProperties()
            {
                Id = docProperties.Id,
                Name = docProperties.Name,
                //Path = docPath
            };

            string path = Path.Combine(Path.GetDirectoryName(docProperties.Path), Path.GetFileNameWithoutExtension(docProperties.Path));
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            documentProperties.Path = Path.Combine(path, Guid.NewGuid().ToString());

            if (Path.GetExtension(docProperties.Path).ToLower().Equals(".pdf"))
            {
                documentProperties.Type = DocType.Pdf;
                documentProperties.Path = documentProperties.Path + ".Pdf";
                File.Copy(docProperties.Path, documentProperties.Path, true);
            }
            else
            {
                documentProperties.Type = DocType.Image;
                File.Copy(docProperties.Path, documentProperties.Path, true);
            }
            return documentProperties;
        }

        private void setVisionBotLayout(VisionBotEngine.Model.Layout layout, VBotDocument vBotDocument, VisionBot visionBot)
        {
            layout.Id = Guid.NewGuid().ToString();
            layout.VBotDocSetId = layout.DocProperties.Id;
            layout.SirFields = vBotDocument.SirFields;
            layout.Settings = vBotDocument.Layout.Settings;
            visionBot.Layouts.Add(layout);
        }

        #endregion private methods
    }
}