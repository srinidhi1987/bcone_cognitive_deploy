﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Converters
{
    //using Automation.CognitiveData.Generic;
    using global::CsvHelper;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class CsvHelperReaderWriter : ICsvReaderWriter
    {
        private string filePath;
        private string delimiter;

        public CsvHelperReaderWriter(string filePath, string delimiter)
        {
            this.filePath = filePath;
            this.delimiter = delimiter;
        }

        public List<string[]> ReadContents()
        {
            TextReader textReader = File.OpenText(this.filePath);
            CsvParser parser = new CsvParser(textReader);
            parser.Configuration.Delimiter = this.delimiter;

            List<string[]> csvContentList = new List<string[]>();

            while (true)
            {
                string[] row = parser.Read();

                if (row == null)
                {
                    break;
                }

                csvContentList.Add(row);
            }

            textReader.Close();

            return csvContentList;
        }

        public void WriteContents(List<object[]> contentList)
        {
            using (TextWriter textWriter = new StreamWriter(this.filePath, true, System.Text.Encoding.UTF8))
            {
                CsvWriter csvWriter = new CsvWriter(textWriter);
                csvWriter.Configuration.Encoding = System.Text.Encoding.UTF8;

                csvWriter.Configuration.Delimiter = this.delimiter;

                foreach (var contents in contentList)
                {
                    foreach (var field in contents)
                    {
                        csvWriter.WriteField(field);
                    }

                    csvWriter.NextRecord();
                }

                textWriter.Close();
            }
        }
    }
}