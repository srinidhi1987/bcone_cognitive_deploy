﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine;

namespace Automation.CognitiveData.Converters
{
    //using Automation.CognitiveData.Generic;
    using LightInject;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class CsvDataTableSerializer : IDataTableSerializer
    {
        private readonly string filePath;
        private readonly string delimiter;

        private ICsvReaderWriter csvReader;

        public CsvDataTableSerializer(string filePath, string delimiter, ICsvReaderWriter csvReader)
        {
            this.filePath = filePath;
            this.delimiter = delimiter;
            this.csvReader = csvReader;
        }

        public void Serialize(DataTable dataTable)
        {
            VBotLogger.Trace(() => "[Serialize] Data table serialize....");
            List<object[]> dataList = new List<object[]>();

            IEnumerable<object> columnNames = dataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName);

            dataList.Add(columnNames.ToArray());

            dataList.AddRange(from DataRow row in dataTable.Rows select row.ItemArray);

            this.csvReader.WriteContents(dataList);
            VBotLogger.Trace(() => "[Serialize] Sucessfully save CSV ....");
        }

        public DataTable Deserialize()
        {
            return this.convertToDataTable(this.filePath);
        }

        private DataTable convertToDataTable(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return new DataTable();
            }

            DataTable dataTable = new DataTable();
            DataRow dataRow;

            List<string[]> csvContentList = this.csvReader.ReadContents();

            for (int index = 0; index < csvContentList.Count; index++)
            {
                string[] contents = csvContentList[index];

                dataRow = dataTable.NewRow();

                for (int columnIndex = 0; columnIndex < contents.Length; columnIndex++)
                {
                    if (index == 0)
                    {
                        dataTable.Columns.Add(contents[columnIndex]);
                    }
                    else
                    {
                        dataRow[columnIndex] = contents[columnIndex];
                    }
                }

                if (index != 0)
                {
                    dataTable.Rows.Add(dataRow);
                }
            }

            return dataTable;
        }
    }
}