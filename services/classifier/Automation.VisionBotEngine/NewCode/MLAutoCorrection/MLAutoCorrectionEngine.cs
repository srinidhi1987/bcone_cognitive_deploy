﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;
using Automation.VisionBotEngine;

namespace Automation.VisionBotEngine.MLAutoCorrection
{
    public class MLAutoCorrectionEngine
    {
        private const int Max_Parallel_Task_Threshold = 3;
        private readonly IMachineLearningServiceEndpointConsumer _machineLearningConsumer;
        private readonly decimal _mlAutoCorrectionConfidenceThreshold;

        public MLAutoCorrectionEngine(MachineLearningEndpointConsumerService machineLearningConsumer, decimal mlAutoCorrectionThreshold)
        {
            _machineLearningConsumer = machineLearningConsumer;
            _mlAutoCorrectionConfidenceThreshold = mlAutoCorrectionThreshold;
        }

        public void FieldAutoCorrectionUsingLearningModel(string projectId
           , VBotDocument vBotDocument
           , IVBotValidatorDataReader vBotValidatorDataReader
           , ValidatorAdvanceInfo validatorAdvanceInfo
           )
        {
            VBotLogger.Trace(() => string.Format("[MLAutoCorrectionEngine -> FieldAutoCorrectionUsingLearningModel] Field correction using learning model started....."));
            List<Task> listOfTask = new List<Task>();

            try
            {
                for (int index = 0; index < vBotDocument.Data.FieldDataRecord.Fields.Count; index++)
                {
                    if (vBotDocument.Data.FieldDataRecord.Fields[index].ValidationIssue != null
                        && !string.IsNullOrWhiteSpace(vBotDocument.Data.FieldDataRecord.Fields[index]?.Value?.Field?.Text))
                    {
                        try
                        {
                            Action action = fieldAutoCorrectionUsingLearningModel(projectId
                                , vBotDocument
                                , vBotValidatorDataReader
                                , validatorAdvanceInfo
                                , index
                                , vBotDocument.Data.FieldDataRecord.Fields);

                            listOfTask.Add(Task.Run(action));

                            if (areRunningTasksThresholdReached(listOfTask))
                            {
                                waitForFinishAndClearTasks(listOfTask);
                            }
                        }
                        catch (Exception ex)
                        {
                            VBotLogger.Error(() => string.Format("[MLAutoCorrectionEngine -> FieldAutoCorrectionUsingLearningModel] Field value correction using machine learning model for field is failed with exception {0}", ex.Message));
                        }
                    }
                }

                List<TableValue> tableRecords = vBotDocument.Data.TableDataRecord;
                tableRecords.ForEach(tableValue =>
                {
                    List<DataRecord> rows = tableValue.Rows;
                    rows.ForEach(dataRecord =>
                    {
                        List<FieldData> tableFields = dataRecord.Fields;
                        for (int index = 0; index < tableFields.Count; index++)
                        {
                            if (tableFields[index].ValidationIssue != null
                            && !string.IsNullOrWhiteSpace(tableFields[index]?.Value?.Field?.Text))
                            {
                                try
                                {
                                    Action action = fieldAutoCorrectionUsingLearningModel(projectId
                                        , vBotDocument
                                        , vBotValidatorDataReader
                                        , validatorAdvanceInfo
                                        , index
                                        , tableFields);

                                    listOfTask.Add(Task.Run(action));
                                    if (areRunningTasksThresholdReached(listOfTask))
                                    {
                                        waitForFinishAndClearTasks(listOfTask);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    VBotLogger.Error(() => string.Format("[MLAutoCorrectionEngine -> FieldAutoCorrectionUsingLearningModel] Field value correction using machine learning model for table field is failed with exception {0}", ex.Message));
                                }
                            }
                        }
                    });
                });
                if (listOfTask.Count > 0)
                    Task.WaitAll(listOfTask.ToArray());
                VBotLogger.Trace(() => string.Format("[MLAutoCorrectionEngine -> FieldAutoCorrectionUsingLearningModel] Field correction using learning model finish....."));
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[MLAutoCorrectionEngine -> FieldAutoCorrectionUsingLearningModel] Field value correction using machine learning model is failed with exception {0}", ex.Message));
            }
        }

        private static void waitForFinishAndClearTasks(List<Task> listOfTask)
        {
            Task.WaitAll(listOfTask.ToArray());
            listOfTask.Clear();
        }

        private static bool areRunningTasksThresholdReached(List<Task> listOfTask)
        {
            return listOfTask.Count >= Max_Parallel_Task_Threshold;
        }

        private Action fieldAutoCorrectionUsingLearningModel(string projectId
            , VBotDocument vBotDocument
            , IVBotValidatorDataReader vBotValidatorDataReader
            , ValidatorAdvanceInfo validatorAdvanceInfo,
            int index,
            List<FieldData> fieldsData)
        {
            const string logFunctionName = "MLAutoCorrectionEngine -> fieldCorrectionUsingLearningModel";
            return new Action(() =>
            {
                FieldData failedFieldOrg = fieldsData[index];

                VBotLogger.Trace(() => string.Format("[{0}] Validation of Field {1} is failed ",
                    logFunctionName,
                    failedFieldOrg.Field.Name));

                bool autoCorrect = false;
                FieldData failedFieldClone = failedFieldOrg.Clone();
                List<RecommendationModel> suggestion = _machineLearningConsumer.GetSuggestionWithConfidence(projectId, failedFieldClone.Field.Id, failedFieldClone.Value.Field.Text).OrderByDescending(s => s.Confidence).ToList();
                VBotLogger.Trace(() => string.Format("[{0}] Received learning model suggestion; field={1}; count={2}",
                    logFunctionName,
                    failedFieldOrg.Field.Name,
                    suggestion.Count));
                foreach (RecommendationModel m in suggestion)
                {
                    if (m.Confidence >= (float)_mlAutoCorrectionConfidenceThreshold)
                    {
                        VBotLogger.Trace(() => string.Format("[{0}] Checking learning model suggestion; field={1}; confidence={2} [required={3}]",
                            logFunctionName,
                            failedFieldOrg.Field.Name,
                            m.Confidence,
                            _mlAutoCorrectionConfidenceThreshold));

                        failedFieldClone.Value.Field.Text = m.Value;
                        failedFieldClone.Value.Field.Confidence = Configuration.Constants.CDC.MAX_CONFIDENCE_THRESHOLD;

                        failedFieldClone.EvaluateAndFillValidationDetailsExceptFormulaValidation(vBotDocument.Layout
                            , vBotDocument.Data
                            , vBotValidatorDataReader
                            , validatorAdvanceInfo);

                        if (failedFieldClone.ValidationIssue == null)
                        {
                            autoCorrect = true;
                            break;
                        }
                        VBotLogger.Trace(() => string.Format("[{0}] Suggestion not used; field={1}; validation issue={2}",
                            logFunctionName,
                            failedFieldClone.Field.Name,
                            failedFieldClone.ValidationIssue.IssueCode));
                    }
                    else
                    {
                        VBotLogger.Trace(() => string.Format("[{0}] Skipping low confidence suggestions; field={1}; threshold={2}",
                            logFunctionName,
                            failedFieldOrg.Field.Name,
                            _mlAutoCorrectionConfidenceThreshold));
                        break;
                    }
                }
                if (autoCorrect)
                {
                    fieldsData[index] = failedFieldClone;
                    VBotLogger.Trace(() => string.Format("[{0}]Auto Corrected; field={1}", logFunctionName, failedFieldOrg.Field.Name));
                }
            });
        }
    }
}