﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class BottomValueExtraction : IValueExtraction
    {
        private const int BottomValueHeightOffsetFactor = 3;

        private FieldLayout _labelField;
        private FieldDef _fieldDef;
        private List<FieldKey> LayoutFieldKeys;

        public BottomValueExtraction(List<FieldKey> layoutFieldKeys)
        {
            LayoutFieldKeys = layoutFieldKeys;
        }

        public Field GetFieldValue(FieldLayout labelField, SIRFields sirFields, FieldDef fieldDef)
        {
            VBotLogger.Trace(() => "[GetFieldValue] BValue Extraction");

            _labelField = labelField;

            _fieldDef = fieldDef;
            Field field = new Field() { Bounds = labelField.Bounds };
            List<Field> fields = sirFields.Fields;
            List<Line> lines = sirFields.Lines;
            double meargRatio = labelField.MergeRatio;

            Rectangle targetRect = field.Bounds;

            targetRect.Y = field.Bounds.Bottom + 1;
            targetRect.Height = BottomValueHeightOffsetFactor * field.Bounds.Height;

            Field bottomField = fields.FirstOrDefault(fi => targetRect.Contains(fi.Bounds) || targetRect.IntersectsWith(fi.Bounds));
            if (bottomField != null)
            {
                if (!isValidValueField(bottomField, LayoutFieldKeys))
                {
                    bottomField = null;
                }
                else
                {
                    bottomField = bottomField.Clone();

                    Field bottomNearestField = GetRightValueFieldList(bottomField.Clone(), sirFields, meargRatio);

                    if (bottomNearestField != null && bottomNearestField.Id != bottomField.Id && new FieldOperations().IsRightMeargingField(bottomField, bottomNearestField, meargRatio))
                    {
                        bottomField = (new FieldOperations()).GetSingleFieldFromFields(new List<Field>() { bottomField, bottomNearestField });
                    }
                }
            }

            return bottomField;
        }

        private bool isValidValueField(Field field, List<FieldKey> layoutFieldKeys)
        {
            IValidateFieldValue validFieldValue = new ValidateFieldValue();
            return validFieldValue.IsValidFieldForValue(field, layoutFieldKeys);
        }

        private Field GetRightValueFieldList(Field field, SIRFields sirFields, double meargRatio)
        {
            RightValueExtraction valueExtraction = new RightValueExtraction(LayoutFieldKeys);
            return valueExtraction.GetFieldValue(field, _labelField, _fieldDef, sirFields, meargRatio);
        }

        public Field FindFieldAndFieldValue(FieldLayout labelField, SIRFields sirFields, FieldDef fieldDef)
        {
            return null;
        }
    }
}