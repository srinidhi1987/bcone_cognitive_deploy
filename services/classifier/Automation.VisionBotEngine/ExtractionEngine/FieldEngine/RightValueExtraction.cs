﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;

    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal class RightValueExtraction : IValueExtraction
    {
        private FieldLayout _fieldLayout;
        private FieldDef _fieldDef;

        private List<FieldKey> LayoutFieldKeys;

        public RightValueExtraction(List<FieldKey> layoutFieldKeys)
        {
            LayoutFieldKeys = layoutFieldKeys;
        }

        public Field GetFieldValue(FieldLayout fieldLayout, SIRFields sirFields, FieldDef fieldDef)
        {
            Field field = new Field() { Bounds = fieldLayout.Bounds };

            Field valueField = GetFieldValue(field, fieldLayout, fieldDef, sirFields, fieldLayout.MergeRatio);

            if (valueField == null)
            {
                VBotLogger.Trace(() => "[GetFieldValue] more RValue Extraction");

                valueField = SearchSimilarFieldValue(fieldLayout, sirFields, fieldDef);
            }

            return valueField;
        }

        private Field SearchSimilarFieldValue(FieldLayout fieldLayout, SIRFields sirFields, FieldDef fieldDef)
        {
            List<Field> searchFields = GetBestMatchFields(sirFields.Fields, fieldLayout.Id);

            foreach (Field fi in searchFields)
            {
                Field field = new Field() { Bounds = fi.Bounds };

                Field valueField = GetFieldValue(field, fieldLayout, fieldDef, sirFields, fieldLayout.MergeRatio);

                if (valueField != null)
                {
                    return valueField;
                }
            }

            return null;
        }

        public Field GetFieldValue(Field field, FieldLayout labelField, FieldDef fieldDef, SIRFields sirFields, double meargRatio)
        {
            VBotLogger.Trace(() => "[GetFieldValue] RValue Extraction");

            _fieldLayout = labelField;
            _fieldDef = fieldDef;
            List<Field> fields = sirFields.Fields;
            List<Line> lines = sirFields.Lines;

            Field selfField = GetSelfValueFromField(field, sirFields.Fields);

            if (selfField != null && !selfField.Text.EndsWith("#") && !selfField.Text.EndsWith(":"))
            {
                return selfField;
            }

            if (!field.Bounds.IsEmpty)
            {
                List<Field> allRightFields = fields.FindAll(fi => Math.Abs(fi.Bounds.Top - field.Bounds.Top) < field.Bounds.Height &&
                                                                            fi.Bounds.Left > (field.Bounds.Left + field.Bounds.Width - 5));

                if (allRightFields.Count > 0)
                {
                    allRightFields = GetNearestFields(field, allRightFields);
                }

                if (allRightFields.Count == 0)
                {
                    allRightFields = fields.FindAll(fi => fi.Bounds.Left > (field.Bounds.Left + field.Bounds.Width - 5));

                    allRightFields = allRightFields.FindAll(fi => (((fi.Bounds.Top + fi.Bounds.Height) >= field.Bounds.Top &&
                                          Math.Abs(fi.Bounds.Top + fi.Bounds.Height) <= (field.Bounds.Top + field.Bounds.Height)) ||
                                          ((Math.Abs(fi.Bounds.Top) >= field.Bounds.Top &&
                                          fi.Bounds.Top <= (field.Bounds.Top + field.Bounds.Height)))));
                }

                allRightFields = allRightFields.OrderBy(fi => new RectangleOperations().GetCenterDistanceBetweenRects(fi.Bounds, field.Bounds)).ToList();

                if (allRightFields.Count > 0)
                {
                    List<Field> rightFields = this.MeargingFields(meargRatio, allRightFields);

                    return this.GetSingleFieldFromList(rightFields);
                }
            }

            return null;
        }

        private List<Field> GetBestMatchFields(List<Field> fields, string ignorID)
        {
            FieldOperations fieldOperation = new FieldOperations();

            List<Field> searchFields = fieldOperation.GetAllBestMatchingFields(fields, _fieldLayout.Label, _fieldLayout.SimilarityFactor);
      
            return searchFields;
        }

        ////private List<Field> GetNearestFields(Field field, List<Field> rightFields)
        ////{
        ////    List<Field> fields = rightFields.FindAll(fi => Math.Abs(fi.Bounds.Top - field.Bounds.Top) < field.Bounds.Height / 2 &&
        ////                                                               fi.Bounds.Left > (field.Bounds.Left + field.Bounds.Width - 5));

        ////    if (fields.Count == 0)
        ////        return rightFields;

        ////    return fields;
        ////}
        private List<Field> GetNearestFields(Field field, List<Field> rightFields)
        {
            List<Field> fields = rightFields.FindAll(fi => Math.Abs(fi.Bounds.Top - field.Bounds.Top) < field.Bounds.Height / 2 &&
                                                                       fi.Bounds.Left > (field.Bounds.Left + field.Bounds.Width - 5));

            if (fields.Count == 0)
                return rightFields;

            List<Field> fields1 = fields.FindAll(fi => (fi.Bounds.Top + fi.Bounds.Height - field.Bounds.Top) > 0 &&
                                                                       fi.Bounds.Left > (field.Bounds.Left + field.Bounds.Width - 5));

            if (fields1.Count == 0)
                return fields;

            return fields1;
        }

        private bool isValidValueField(Field field, List<FieldKey> layoutFieldKeys)
        {
            IValidateFieldValue validFieldValue = new ValidateFieldValue();
            return validFieldValue.IsValidFieldForValue(field, layoutFieldKeys);
        }

        private Field GetSelfValueFromField(Field field, List<Field> fields)
        {
            Field currentfield = GetCurrentField(field, fields);
            if (currentfield != null && !string.IsNullOrWhiteSpace(currentfield.Text))
                currentfield = currentfield.Clone();
            else
                return null;

            if (!isValidValueField(currentfield, LayoutFieldKeys))
                return null;

            List<string> labels = _fieldLayout.Label.Split('|').OrderByDescending(l => l.Trim().Length).ToList();
            currentfield.Text = getBestMatchLabel(labels, currentfield.Text);

            currentfield.Text = removeSpecialCharacters(currentfield.Text);

            IValueTypeValidator valueValidator = new ValueTypeValidator();

            if (!string.IsNullOrEmpty(currentfield.Text) && valueValidator.IsValidType(currentfield.Text, _fieldLayout, _fieldDef))
                return currentfield;
            else
                return null;
        }

        private string removeSpecialCharacters(string textValue)
        {
            textValue = textValue.Trim();
            Dictionary<string, int> removeStrings = new Dictionary<string, int>();
            removeStrings.Add("#", 1);
            removeStrings.Add(":", 1);
            // removeStrings.Add("-", 1);
            removeStrings.Add(",", 1);
            removeStrings.Add(".", 1);
            removeStrings.Add(";", 1);
            removeStrings.Add("\"", 1);
            removeStrings.Add("`", 1);
            removeStrings.Add("'", 1);
            removeStrings.Add("#:", 2);

            foreach (var key in removeStrings.Keys)
            {
                if (textValue.StartsWith(key))
                    textValue = textValue.Remove(0, removeStrings[key]);
            }

            return textValue;
        }

        private string getBestMatchLabel(List<string> labels, string actualString)
        {
            string resultString = actualString.Trim().ToLower();

            //if (resultString.IndexOf(_fieldLayout.DisplayValue) >= 0)
            //{
            //    resultString = resultString.Replace(_fieldLayout.DisplayValue, string.Empty);
            //}

            foreach (var labelString in labels)
            {
                if (!string.IsNullOrWhiteSpace(labelString))
                {
                    string stringToReplace = labelString.Trim().ToLower();

                    int removeTextIndex = resultString.IndexOf(stringToReplace);
                    if (removeTextIndex >= 0)
                    {
                        resultString = actualString.Remove(0, removeTextIndex + stringToReplace.Length);
                        return resultString;
                    }
                }
            }

            return string.Empty;
        }

        private Field GetCurrentField(Field field, List<Field> fields)
        {
            return fields.FirstOrDefault(fi => fi.Bounds.Equals(field.Bounds));
        }

        private List<Field> MeargingFields(double mergeRatio, List<Field> allRightFields)
        {
            List<Field> rightFields = new List<Field>();
            rightFields.Add(allRightFields[0].Clone());

            for (int i = 1; i < allRightFields.Count; i++)
            {
                if (IsRightMeargingField(rightFields[i - 1], allRightFields[i], mergeRatio))
                {
                    rightFields.Add(allRightFields[i]);
                }
                else if (IsMeargingField(rightFields[i - 1], allRightFields[i], mergeRatio))
                {
                    rightFields.Add(allRightFields[i]);
                }
                else
                {
                    break;
                }
            }

            return rightFields;
        }

        private bool IsRightMeargingField(Field sourceField, Field destField, double mergeRatio)
        {
            int charWidth = sourceField.Text.Length > 0 ? sourceField.Bounds.Width / sourceField.Text.Length : (int)mergeRatio;
            return (sourceField.Bounds.X + sourceField.Bounds.Width + mergeRatio * charWidth + 10) >=
                   destField.Bounds.X;
        }

        private bool IsMeargingField(Field sourceField, Field destField, double mergeRatio)
        {
            if (IsSpecialString(sourceField.Text))
                return true;

            if (sourceField.Text.Length < 2)
                return true;

            if (sourceField.Text.EndsWith(")"))
                return true;

            return false;
        }

        private bool IsSpecialString(string textValue)
        {
            switch (textValue.ToLower())
            {
                case "$":
                case ":":
                case "#":
                case "#:":
                case "-":
                case "l":
                case "i":
                case ".":
                case "'":
                case "`":
                case ",":
                case "\"":
                case ";":
                    return true;
            }

            return false;
        }

        private Field GetSingleFieldFromList(List<Field> fields)
        {
            IValueTypeValidator valueValidator = new ValueTypeValidator();

            Field field = new FieldOperations().GetSingleFieldFromFields(fields);
            field.Text = removeSpecialCharacters(field.Text);

            if (valueValidator.IsValidType(field.Text, _fieldLayout, _fieldDef))
                return field;

            foreach (Field singlefield in fields)
            {
                if (!string.IsNullOrEmpty(singlefield.Text) && valueValidator.IsValidType(singlefield.Text, _fieldLayout, _fieldDef))
                    return singlefield;
            }

            return field;
        }

        public Field FindFieldAndFieldValue(FieldLayout labelField, SIRFields sirFields, FieldDef fieldDef)
        {
            return SearchSimilarFieldValue(labelField, sirFields, fieldDef);
        }
    }
}