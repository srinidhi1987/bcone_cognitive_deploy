﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;

    internal class ValueFactory
    {
        public IValueExtraction CreateValueExtraction(FieldDirection fieldDirection, List<FieldKey> layoutFieldKeys)
        {
            if (fieldDirection == FieldDirection.Right)
                return new RightValueExtraction(layoutFieldKeys);
            if (fieldDirection == FieldDirection.Bottom)
                return new BottomValueExtraction(layoutFieldKeys);
            if (fieldDirection == FieldDirection.Left)
                return new LeftValueExtraction();
            if (fieldDirection == FieldDirection.Top)
                return new BottomValueExtraction(layoutFieldKeys);

            return null; 
        }
    }
}
