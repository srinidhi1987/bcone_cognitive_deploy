﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Configuration;

namespace Automation.VisionBotEngine
{
    internal class ValidateFieldValue : IValidateFieldValue
    {
        private IFieldKeyStorage _fieldKeyStorage;
        private double _similarityFactor = 0.7;

        public ValidateFieldValue()
        {
            string storageFile = System.IO.Path.Combine(VBotSettings.ApplicationPath, Automation.Cognitive.VisionBotEngine.Properties.Resources.TrainedDataFileName);
            _fieldKeyStorage = new AliasServiceFieldKeyStorage(); //new CsvFieldKeyStorage(storageFile);
        }

        public bool IsValidFieldForValue(Field field)
        {
            if (string.IsNullOrEmpty(field?.Text))
                return false;

            return isValidFieldValue(field.Text);
        }

        public bool IsValidFieldForValue(Field field, List<FieldKey> includeMapFieldKey)
        {
            if (isValidFieldValue(field.Text, includeMapFieldKey) &&
                isValidFieldValue(field.Text))
            {
                return true;
            }
            return false;
        }

        private bool isValidFieldValue(string fieldValue)
        {
            List<FieldKey> fieldKeys = _fieldKeyStorage.GetFieldKeys().ToList();
            return isValidFieldValue(fieldValue, fieldKeys);
        }

        private bool isValidFieldValue(string fieldValue, List<FieldKey> fieldKeys)
        {
            List<FieldKey> findKeys = fieldKeys.FindAll(fi => fi.Name.ToLower().GetSimilarity(fieldValue.ToLower().Trim()) > _similarityFactor);

            if (findKeys == null || findKeys.Count == 0)
            {
                return true;
            }
            return false;
        }
    }
}