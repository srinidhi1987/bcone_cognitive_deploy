﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;

namespace Automation.VisionBotEngine
{
    internal class FieldValueExtraction : IFieldValueExtraction
    {
        FieldOperations _fieldOperation = new FieldOperations();
        List<FieldKey> _layoutFieldKeys;
        FieldDef _fieldDef;
        Field _lastValueField = null;

        bool _isRightFieldSearched = false;
        bool _isBottomFieldSearched = false;
        bool _isAllAreaSearched = false;
        public List<FieldKey> LayoutFieldKeys { set { _layoutFieldKeys = value; } }

        public Field GetValueField(FieldLayout labelField, SIRFields sirFields, FieldDef fieldDef)
        {
            VBotLogger.Trace(() => "[GetValueField] Auto Field extraction");

            _fieldDef = fieldDef;

            if (sirFields.Fields.Count == 0) return new Field();

            Field fieldValue = GetSingleLineValueField(labelField, sirFields);

            if (IsFieldNotFound(fieldValue))
                return fieldValue;

            if (labelField.IsMultiline && labelField.FieldDirection != FieldDirection.None)
                return GetMultilineValueField(fieldValue, sirFields.Fields, sirFields.Lines);
            else
                return _fieldOperation.GetFilteredFieldValue(fieldValue);
        }

        private IValueExtraction GetValueExtraction(List<FieldKey> layoutFieldKeys)
        {
            ValueFactory valueFactory = new ValueFactory();
            FieldDirection direction = FieldDirection.Right;
            if (!_isRightFieldSearched)
            {
                _isRightFieldSearched = true;
                direction = FieldDirection.Right;
            }
            else if (!_isBottomFieldSearched)
            {
                _isBottomFieldSearched = true;
                direction = FieldDirection.Bottom;
            }
            else
            {
                _isAllAreaSearched = true;

            }


            return valueFactory.CreateValueExtraction(direction, layoutFieldKeys); //labelField.FieldDirection
        }

        private Field GetSingleLineValueField(FieldLayout labelField, SIRFields sirFields)
        {

            Field valueField = null;

            _lastValueField = null;

            IValueExtraction valueExtraction;
            do
            {
                valueExtraction = GetValueExtraction(_layoutFieldKeys); //labelField.FieldDirection

                if (_isAllAreaSearched)
                    break;

                valueField = valueExtraction.GetFieldValue(labelField, sirFields, _fieldDef);

                valueField = ValidFieldDirectionAndValue(valueExtraction, valueField, labelField, sirFields);

            } while (!_isAllAreaSearched && valueField == null);


            if (valueField == null && _lastValueField != null)
                valueField = _lastValueField;
            else if (valueField == null)
                valueField = new Field();

            return _fieldOperation.GetValidatedField(valueField);
        }



        private Field ValidFieldDirectionAndValue(IValueExtraction valueExtraction, Field valueField, FieldLayout labelField, SIRFields sirFields)
        {
            bool validValueField = false;
            bool validTypeeField = false;

            Field findField = null;

            if (valueField != null)
            {
                VBotLogger.Trace(() => "[ValidFieldDirectionAndValue] Validate Value");

                validValueField = isValidValueField(valueField);

                if (validValueField && _lastValueField == null)
                { _lastValueField = valueField; }

                VBotLogger.Trace(() => "[ValidFieldDirectionAndValue] Validate Type");

                validTypeeField = isValidTypeField(valueField, labelField);
            }

            if (validValueField && validTypeeField)
            {
                return valueField;
            }

            if (!validValueField || !validTypeeField)
            {
                VBotLogger.Trace(() => string.Format("[ValidFieldDirectionAndValue] Value Validation : {0}, Type Validation : {1} ", validValueField, validTypeeField));

                SIRFields sirFieldsClone = sirFields.Clone();
           
                findField = valueExtraction.FindFieldAndFieldValue(labelField, sirFieldsClone, _fieldDef);
            }


            if (findField != null)
            {
                validValueField = isValidValueField(findField);

                validTypeeField = isValidTypeField(findField, labelField);
            }

            if (!validValueField || !validTypeeField)
            {
                return null;
            }

            return findField;

        }

        private static bool IsFieldNotFound(Field fieldValue)
        {
            return fieldValue == null || string.IsNullOrWhiteSpace(fieldValue.Text);
        }

        public Field GetMultilineValueField(Field valueField, List<Field> fields, List<Line> lines)
        {
            List<Field> belowFields = fields.FindAll(field => (field.ParentId != null) && field.ParentId.Equals(valueField.ParentId) && field.Bounds.Top >= valueField.Bounds.Top);

            Field multilineValueField = _fieldOperation.GetSingleFieldFromFields(belowFields);

            return multilineValueField;
        }

        private bool isValidValueField(Field field)
        {
            IValidateFieldValue validFieldValue = new ValidateFieldValue();
            return validFieldValue.IsValidFieldForValue(field, _layoutFieldKeys);
        }

        private bool isValidTypeField(Field field, FieldLayout labelField)
        {
            IValueTypeValidator validFieldValue = new ValueTypeValidator();
            return validFieldValue.IsValidType(field.Text, labelField, _fieldDef);
        }

    }
}
