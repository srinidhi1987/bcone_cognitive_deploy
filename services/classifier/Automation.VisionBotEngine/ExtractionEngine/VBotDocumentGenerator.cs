﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using AForge.Imaging.Filters;
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using Cognitive.VisionBotEngine;
    using Cognitive.VisionBotEngine.Model.Generic;
    using OcrEngine;
    using ResizeRescan.Interface;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;

    internal class VBotDocumentGenerator : IVBotDocumentGenerator
    {
        private IVBotDocClassifier _docClassifier;
        private LayoutConfidence _layoutConfidence;
        private bool _isCheckBoxRecognitionFeatureEnabled;
        private DocumentProperties _docProps;
        private IRescan _rescan;
        private Image _image;
        private IVisionBotPageImageProvider _visionBotPageImageProvider;
        private const int LayoutConfidenceThreshold = 50;

        public VBotDocumentGenerator(IVBotDocClassifier docClassifier, DocumentProperties docProps
            , bool isCheckBoxRecognitionFeatureEnabled = false, IRescan rescan = null, IVisionBotPageImageProvider visionBotPageImageProvider = null)
        {
            _docClassifier = docClassifier;
            _docProps = docProps;
            _isCheckBoxRecognitionFeatureEnabled = isCheckBoxRecognitionFeatureEnabled;
            _rescan = rescan;
            _visionBotPageImageProvider = visionBotPageImageProvider;
        }

        public VBotDocument Generate(VisionBot vbot, SegmentedDocument segmentedDoc, IVBotValidatorDataReader validatorDataReader, int confidenceThreshold)
        {
            VBotLogger.Trace(() => "[Generate] Convert Segmented data to visionbot Doc...");

            VBotDocument vbotDoc = new VBotDocument();
            vbotDoc.DocumentProperties = _docProps;
            vbotDoc.SegmentedDoc = segmentedDoc;
            vbotDoc.SirFields.Lines = GetLines(segmentedDoc);
            VBotLogger.Trace(() => "[Generate] Convert Segmented data to line Sucessfully...");

            vbotDoc.SirFields.Fields = GetFields(segmentedDoc, vbotDoc.SirFields.Lines);
            VBotLogger.Trace(() => "[Generate] Convert Segmented data to Field Sucessfully...");

            vbotDoc.SirFields.Regions = GetRegions(vbotDoc.SirFields.Fields, vbotDoc.SirFields.Lines);
            VBotLogger.Trace(() => "[Generate] Convert Segmented data to Region Sucessfully...");

            this.ExtractVBotData(vbot, vbotDoc);

            ValidatorAdvanceInfo validatorAdvanceInfo = getValidatorAdvanceInfo(vbot, confidenceThreshold);

            vbotDoc.Data.EvaluateAndFillValidationDetails(vbotDoc.Layout
                , validatorDataReader
                , validatorAdvanceInfo);
            VBotLogger.Trace(() => "[Generate] Fill validation data Successfull");

            logValidationStatistics(vbotDoc, confidenceThreshold);

            return vbotDoc;
        }

        private void logValidationStatistics(VBotDocument vbotDocument, int confidenceThreshold)
        {
            try
            {
                foreach (FieldData fieldData in vbotDocument.Data.FieldDataRecord.Fields)
                {
                    if (fieldData.ValidationIssue != null)
                    {
                        string fieldName = fieldData.Field.Name;
                        int fieldConfidence = fieldData.Value.Field.Confidence;
                        string validationIssue = fieldData.ValidationIssue.IssueCode.ToString();
                        VBotLogger.Info(() => $"[ValidationStat - Field] Name: {fieldName}, Confidence: {fieldConfidence}, Issue: {validationIssue}, ConfidenceThreshold: {confidenceThreshold}");
                    }
                }

                foreach (TableValue tableDataRecord in vbotDocument.Data.TableDataRecord)
                {
                    for (int rowCount = 0; rowCount < tableDataRecord.Rows.Count; rowCount++)
                    {
                        for (int fieldCount = 0; fieldCount < tableDataRecord.Rows[rowCount].Fields.Count; fieldCount++)
                        {
                            var fieldData = tableDataRecord.Rows[rowCount].Fields[fieldCount];
                            if (fieldData.ValidationIssue != null)
                            {
                                string fieldName = fieldData.Field.Name;
                                int fieldConfidence = fieldData.Value.Field.Confidence;
                                string validationIssue = fieldData.ValidationIssue.IssueCode.ToString();
                                VBotLogger.Info(() => $"[ValidationStat - TableField({rowCount},{fieldCount})] Name: {fieldName}, Confidence: {fieldConfidence}, Issue: {validationIssue}, ConfidenceThreshold: {confidenceThreshold}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => $"[VBotDocumentGenerator][logValidationStatistics] Warning Details :{ex.Message}");
            }
        }

        public void ExtractVBotData(VisionBot vbot, VBotDocument vbotDoc)
        {
            vbotDoc.Layout = GetLayout(vbot, vbotDoc.SirFields.Fields);
            VBotLogger.Trace(() => string.Format("[Generate] Layout sucessfully mapped with '" + vbotDoc.Layout.Name + "'"));

            UpdateFieldLayouts(vbotDoc);

            StatisticsLogger.Log($"Starting field extraction");
            VBotLogger.Trace(() => "[Generate]  Start field data extraction ...");
            ExtractFieldValues(vbot, vbotDoc);
            VBotLogger.Trace(() => "[Generate]  Start table data extraction ...");
            StatisticsLogger.Log($"Field extraction complete");
          
            StatisticsLogger.Log($"Starting table extraction");
            ExtractTableValues(vbot, vbotDoc);
            VBotLogger.Trace(() => "[Generate]  Document generation completed ...");
            StatisticsLogger.Log($"Table extraction complete");
        }

        public void ExtractTableValues(VisionBot vbot, VBotDocument vbotDoc)
        {
            vbotDoc.Data.TableDataRecord = new List<TableValue>();

            VBotLogger.Trace(() => string.Format("[ExtractTableValues] No of table(s) to extract = {0}.", vbot.DataModel.Tables.Count));
            try
            {
                new ValueBasedTableExtractor().ExtractTableValue(vbot, vbotDoc);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VBotDocumentGenerator), nameof(ExtractTableValues), "Unable to Extract Tables mapped based on Value");
                VBotLogger.Error(() => string.Format($"[Exception Occured in [ValueBasedTableExtractor::ExtractTableValue] So Tables based on Vlaue Mapping cannot be extracted. Exception: [{ex.Message}]"));
            }

            TableOperations tableOps = new TableOperations(_rescan);


            for (int i = 0; i < vbot.DataModel.Tables.Count; i++)
            {
                StatisticsLogger.Log($"Extracting Table start : Table-{i}");
                TableDef tableDef = vbot.DataModel.Tables[i].Clone();

                if (tableDef.IsDeleted)
                    continue;

                TableLayout tableLayout = vbotDoc.Layout.Tables.FirstOrDefault(tablelayout => tablelayout.TableId.Equals(tableDef.Id));

                if (tableLayout != null)
                    tableLayout = tableLayout.Clone();

                if (tableLayout != null && tableLayout.IsValueMappingEnabled == true)
                {
                    if (!vbotDoc.Data.TableDataRecord.Any(x => x?.TableDef?.Id == tableDef.Id))
                    {
                        TableValue tableValue1 = new TableValue();
                        tableValue1.Headers = vbot.DataModel.Tables[i].Columns.FindAll(c => c.IsDeleted == false);
                        tableValue1.TableDef = tableDef;
                        vbotDoc.Data.TableDataRecord.Add(tableValue1);
                    }
                    continue;
                }


                VBotLogger.Trace(() => string.Format("[ExtractTableValues] Extrating table without Value based mapping'{0}'...", tableDef.Name));

                TableValue tableValue = new TableValue();

                if (tableLayout != null && tableLayout.Columns.Count > 0)
                {
                    FieldLayout masterColumnLayout = tableLayout.PrimaryColumn;

                    VBotLogger.Trace(() => string.Format("[ExtractTableValues] Master column for table '{0}' is '{1}'...", tableDef.Name, masterColumnLayout.Label));
                    UpdateTableAspectBounds(tableLayout);
                    TableInfo tableInfo = new TableInfo(tableDef, tableLayout, vbotDoc.SirFields);
                    tableOps.path = vbotDoc.DocumentProperties.Path;
                    tableValue = tableOps.GetTableValue(tableInfo.Clone(), vbotDoc.DocumentProperties.PageProperties);

                    VBotLogger.Trace(() => string.Format("[ExtractTableValues] Table '{0}' extraction completed...", tableDef.Name));
                }
                else
                {
                    tableValue.Headers = vbot.DataModel.Tables[i].Columns.FindAll(c => c.IsDeleted == false);
                    VBotLogger.Trace(() => string.Format("[ExtractTableValues] There are no table columns defined for table '{0}'.", tableDef.Name));
                }

                tableValue.TableDef = tableDef;

                vbotDoc.Data.TableDataRecord.Add(tableValue);

                VBotLogger.Trace(() => string.Format("[ExtractTableValues] Table values (Rows = {0} , Cols = {1} added to vbot document data...", tableValue.Rows.Count, tableValue.Headers.Count));
                StatisticsLogger.Log($"Extracting Table end : Table-{i}");
            }
            if (vbot?.DataModel?.Tables?.Count > 0)
            {
                List<TableValue> orderedTempValue = new List<TableValue>();
                for (int i = 0; i < vbot.DataModel.Tables.Count; i++)
                {
                    var table = vbotDoc.Data.TableDataRecord.Find(x => x.TableDef.IsDeleted == false && x.TableDef.Id == vbot.DataModel.Tables[i].Id);
                    if (table != null)
                        orderedTempValue.Add(table);
                }
                vbotDoc.Data.TableDataRecord.Clear();
                vbotDoc.Data.TableDataRecord.AddRange(orderedTempValue);
            }
        }

        private List<Field> GetRegions(List<Field> fields, List<Line> lines)
        {
            List<Field> searchField = new NearestMaxRegionBound(fields, lines).GetRegionsList();

            return searchField;
        }

        public void ExtractFieldValues(VisionBot vbot, VBotDocument vbotDoc)
        {
            FieldOperations fieldOp = new FieldOperations();

            vbotDoc.Data.FieldDataRecord = new DataRecord();
            foreach (FieldDef fieldDef in vbot.DataModel.Fields)
            {
                if (fieldDef.IsDeleted)
                    continue;

                StatisticsLogger.Log($"Extracting Field start : {fieldDef.Name}");

                FieldLayout fieldLayout = vbotDoc.Layout.Fields.FirstOrDefault(field => field != null && field.FieldId != null && field.FieldId.Equals(fieldDef.Id));

                FieldLayout fieldClone = fieldLayout != null ? fieldLayout.Clone() : new FieldLayout();

                FieldData fieldData = new FieldData();

                StatisticsLogger.Log($"Extracting Field start ~ First Try");

                Field valueField = GetFieldValue(vbotDoc, fieldOp, fieldDef, fieldClone);

                StatisticsLogger.Log($"Confidence : {valueField.Confidence}");
                StatisticsLogger.Log($"Extracting Field end ~ First Try");

                string firstpassvalueconfidence = valueField.Text + "(" + valueField.Confidence + "%)";

                VBotLogger.Debug(() => "[ExtractFieldValues] First Pass Value: " + valueField.Text + " with confidence value " + valueField.Confidence);

                if (_rescan != null)
                {
                    Field rescanField = _rescan.GetRescanValue(valueField, fieldDef);
                    if (rescanField != null && !string.IsNullOrWhiteSpace(rescanField.Text))
                    {
                        valueField.Text = rescanField.Text;
                    }
                }

                fieldData = GetFieldData(fieldDef, valueField);
                vbotDoc.Data.FieldDataRecord.Fields.Add(fieldData);
            }

            if (_image != null)
            {
                _image.Dispose();
            }

            StatisticsLogger.Log($"Extracting Field end");
        }

        public Field GetFieldValue(VBotDocument vbotDoc, FieldOperations fieldOp, FieldDef fieldDef, FieldLayout fieldClone)
        {
            VBotLogger.Trace(() => string.Format("[GetFieldValue] Field def name '{0}' and field layout label {1}.", fieldDef.Name, fieldClone.Label));
            Field valueField = new Field();
            if (fieldClone != null)
            {
                if (Rectangle.Empty == fieldClone.ValueBounds || fieldClone.IsValueBoundAuto)
                {
                    valueField = extractFloatingField(vbotDoc, fieldClone, fieldDef);
                }
                else
                {
                    UpdateAspectBound(fieldClone);

                    if (fieldClone.ValueType == FieldType.SystemRegion)
                    {
                        valueField = fieldOp.GetSystemDefinedValueRegion(fieldClone, vbotDoc.SirFields);
                    }
                    else
                    {
                        //entry point for checkbox recognition
                        if (fieldDef.ValueType == FieldValueType.CheckBox)
                        {
                            valueField = fieldOp.GetUserDefinedCheckBoxValue(vbotDoc.DocumentProperties.Path, fieldClone, fieldDef, _isCheckBoxRecognitionFeatureEnabled, _visionBotPageImageProvider);
                        }
                        else
                        {
                            valueField = fieldOp.GetUserDefinedValueField(fieldClone, vbotDoc.SirFields.Fields);
                        }
                    }
                }

                VBotLogger.Trace(() => string.Format("[ExtractFieldValues] Field Layout '{0}' found with value {1}.", fieldDef.Name, valueField.Text));
            }
            else
            {
                VBotLogger.Trace(() => string.Format("[ExtractFieldValues] Field Layout '{0}' not found.", fieldDef.Name));
            }

            return valueField;
        }

        private Field extractFloatingField(VBotDocument vbotDoc, FieldLayout fieldClone, FieldDef fieldDef)
        {
            Field valueField;
            FieldKeyConverter fieldCoverter = new FieldKeyConverter();
            IFieldValueExtraction fieldValueExtraction = new FieldValueExtraction();
            fieldValueExtraction.LayoutFieldKeys = fieldCoverter.ConvertLayoutToFieldKeys(vbotDoc.Layout.Fields);

            valueField = fieldValueExtraction.GetValueField(fieldClone, vbotDoc.SirFields, fieldDef);
            return valueField;
        }

        private void UpdateAspectBound(FieldLayout fieldLayout)
        {
            if (fieldLayout == null || _layoutConfidence == null)
            {
                return;
            }
            VBotLogger.Trace(() => $"AspectRatioX:{_layoutConfidence.AspectRatioX} And AspectRatioY:{_layoutConfidence.AspectRatioY}");
            FieldOperations fieldOp = new FieldOperations();
            fieldLayout.XDistant = (int)(fieldLayout.XDistant * _layoutConfidence.AspectRatioX);
            fieldLayout.ValueBounds = fieldOp.FindAspectRect(fieldLayout.ValueBounds, _layoutConfidence.AspectRatioX, _layoutConfidence.AspectRatioY);
            fieldLayout.Bounds = fieldOp.FindAspectRect(fieldLayout.Bounds, _layoutConfidence.AspectRatioX, _layoutConfidence.AspectRatioY);
        }

        private void UpdateTableAspectBounds(TableLayout tableLayout)
        {
            foreach (FieldLayout fieldLayout in tableLayout.Columns)
            {
                UpdateAspectBound(fieldLayout);
            }
            if (tableLayout?.PrimaryColumn != null && tableLayout?.Columns != null && tableLayout.Columns.Count > 0)
            {
                var primaryColumn = tableLayout.Columns.FirstOrDefault(x => x.Id == tableLayout.PrimaryColumn.Id);

                if (primaryColumn != null)
                {
                    tableLayout.PrimaryColumn = primaryColumn;
                }
            }
        }

        private FieldData GetFieldData(FieldDef fieldDef, Field valueField)
        {
            FieldOperations fieldOp = new FieldOperations();

            FieldData fieldData = new FieldData();

            fieldData.Field = fieldDef;

            fieldData.Value = new TextValue();
            fieldData.Value.Field = fieldOp.GetFilteredFieldValue(valueField);

            if (string.IsNullOrWhiteSpace(valueField.Text))
            {
                if (fieldData.Field.IsRequired)
                {
                    VBotLogger.Trace(() => string.Format("[ExtractFieldValues] Unable to extract field '{0}'.", fieldDef.Name));
                }
                else
                {
                    fieldData.Value.Field.Text = fieldData.Field.DefaultValue;
                }
            }
            else
            {
                fieldData.Value.Field.Text = fieldData.Value.Field.Text.Trim();
            }

            return fieldData;
        }

        private void UpdateXdistant(FieldLayout fieldLayout, Field field)
        {
            if (fieldLayout.ValueBounds.Left > field.Bounds.Left + field.Bounds.Width)
            {
                fieldLayout.XDistant = fieldLayout.ValueBounds.Left - (field.Bounds.Left + field.Bounds.Width);
                fieldLayout.ValueBounds = new Rectangle(fieldLayout.ValueBounds.Left - field.Bounds.Width - fieldLayout.XDistant,
                    fieldLayout.ValueBounds.Top, fieldLayout.ValueBounds.Width, fieldLayout.ValueBounds.Height);
            }
            else if (fieldLayout.Bounds.Left + fieldLayout.ValueBounds.Left > fieldLayout.Bounds.Left + fieldLayout.Bounds.Width)
            {
                fieldLayout.XDistant = fieldLayout.Bounds.Left + fieldLayout.ValueBounds.Left - (fieldLayout.Bounds.Left + fieldLayout.Bounds.Width);
                fieldLayout.ValueBounds = new Rectangle(fieldLayout.ValueBounds.Left - fieldLayout.Bounds.Width - fieldLayout.XDistant,
                    fieldLayout.ValueBounds.Top, fieldLayout.ValueBounds.Width, fieldLayout.ValueBounds.Height);
            }
        }

        private void UpdateFieldLayouts(VBotDocument vbotDoc)
        {
            VBotLogger.Trace(() => "[UpdateFieldLayouts]  Mapping Layout fields to SIR data...");

            for (int i = 0; i < vbotDoc.Layout.Fields.Count; i++)
            {
                vbotDoc.Layout.Fields[i] = UpdateFieldLayout(vbotDoc, vbotDoc.Layout.Fields[i]);
            }

            VBotLogger.Trace(() => "[UpdateFieldLayouts]  Mapping Layout fields done...");
        }

        public FieldLayout UpdateFieldLayout(VBotDocument vbotDoc, FieldLayout fieldLayout)
        {
            FieldLayout updatedFieldLayout = fieldLayout;
            Field field = FindLayoutMarkerField(vbotDoc, fieldLayout);

            if (field != null && !string.IsNullOrEmpty(field.Id))
            {
                UpdateXdistant(updatedFieldLayout, field);
                updatedFieldLayout.Bounds = field.Bounds;
             
                updatedFieldLayout.DisplayValue = field.Text;
            }
            else if (updatedFieldLayout.IsValueBoundAuto
                || !string.IsNullOrWhiteSpace(updatedFieldLayout.Label))
            {
                updatedFieldLayout = new FieldLayout();
                updatedFieldLayout.Label = string.Empty;
            }
            else
            {
                VBotLogger.Trace(
                    () =>
                        string.Format(
                            "[UpdateFieldLayouts]  Field ID '{0}' without lable not found in current doc ...",
                            updatedFieldLayout.FieldId, updatedFieldLayout.Label));
            }
            return updatedFieldLayout;
        }

        public Field FindLayoutMarkerField(VBotDocument vbotDoc, FieldLayout fieldLayout)
        {
            FieldOperations fieldOperations = new FieldOperations();

            Field layoutField = fieldOperations.GetBestMatchingField(vbotDoc.SirFields, fieldLayout, _layoutConfidence);

            if (layoutField == null && !string.IsNullOrEmpty(fieldLayout.Label))
            {
                VBotLogger.Trace(() => string.Format("[FindLayoutMarkerField] Unable to find exact lable '{0}'. Try similar one.", fieldLayout.Label));
                layoutField = fieldOperations.GetBestSimilarField(vbotDoc.SirFields, fieldLayout);
            }

            if (layoutField == null && !string.IsNullOrWhiteSpace(fieldLayout.Label))
            {
                VBotLogger.Trace(() => string.Format("[FindLayoutMarkerField] Unable to find similar field for label '{0}'. Try Start With Option.", fieldLayout.Label));
                layoutField = vbotDoc.SirFields.Fields.FirstOrDefault(field => field.Text.ToLower().StartsWith(fieldLayout.Label.ToLower()));

                if (layoutField != null)
                {
                    fieldLayout.FieldDirection = FieldDirection.None;
                }
            }

            if (layoutField.IsNull())
            {
                layoutField = fieldOperations.FindBestMatchingFieldFromCombiningMultipleSIR(fieldLayout.Label
                    , fieldLayout.SimilarityFactor
                    , vbotDoc.SirFields.Fields, null, vbotDoc.SirFields.Lines);

                if (!layoutField.IsNull() && layoutField.Id.IsNullOrEmpty())
                {
                    layoutField.Id = Guid.NewGuid().ToString();
                    //TODO: add/show this new region on screen
                }
            }

            if (layoutField == null)
            {
                VBotLogger.Trace(() => string.Format("[FindLayoutMarkerField] Unable to find any field for label '{0}'.", fieldLayout.Label));
            }
            else
            {
                VBotLogger.Trace(() => string.Format("[FindLayoutMarkerField] Field layout '{0}' found with text '{1}'.", fieldLayout.Label, layoutField.Text));
            }

            return layoutField;
        }

        private List<Field> GetFields(SegmentedDocument segDoc, List<Line> lines)
        {
            List<Field> fields = new List<Field>();

            for (int i = 0; i < segDoc.Regions.Count; i++)
            {
                Field field = CreateFieldFromRegion(segDoc.Regions[i]);

                if (IsValidWordField(field))
                {
                    fields.Add(field);
                }
            }

            VBotLogger.Trace(() => "[GetFields]  Merging nearest fields ...");
            fields = new SIROperations().MergeSegmentedFields(fields, lines);

            return fields;
        }

        private List<Line> GetLines(SegmentedDocument segDoc)
        {
            List<Line> lines = new List<Line>();

            for (int i = 0; i < segDoc.Regions.Count; i++)
            {
                Field field = CreateFieldFromRegion(segDoc.Regions[i]);

                if (IsValidLineField(field))
                {
                    Line line = CreateLineFromField(field);
                    lines.Add(line);
                }
            }

            return lines;
        }

        private Layout GetLayout(VisionBot vbot, List<Field> fields)
        {
            VBotLogger.Trace(() => "[GetLayout]  Get Layout by classification ...");
            LayoutConfidence layoutConfidence = null;
            if (vbot.Layouts.Count == 0)
            {
                VBotLogger.Trace(() => string.Format("[GetLayout]  This is first new layout."));

                return new Layout();
            }
            else if (vbot.Layouts.Count == 1 && vbot.Layouts[0].Identifiers.Count == 0)
            {
                VBotLogger.Trace(() => string.Format("[GetLayout]  This is a single layout without markers."));
                _layoutConfidence = new LayoutConfidence() { AspectRatioX = 1, AspectRatioY = 1 };
                _docClassifier.UpdatetAspectLayoutConfidence(_layoutConfidence, vbot.Layouts[0].DocProperties);

                return vbot.Layouts[0].Clone();
            }

            var layoutClassifications = _docClassifier.Clasify(vbot, fields, LayoutConfidenceThreshold).LayoutConfidences.OrderByDescending(lc => lc.Confidence).ToList();

            if (layoutClassifications.Count > 1)
            {
                VBotLogger.Trace(() => string.Format("[GetLayout]  Multiple layout detected ... No of layouts = {0}", layoutClassifications.Count));

                decimal maxConfidence = layoutClassifications[0].Confidence;
                layoutClassifications = layoutClassifications.FindAll(lc => lc.Confidence == maxConfidence);

                if (layoutClassifications.Count > 1)
                {
                    VBotLogger.Trace(() => string.Format("[GetLayout]  Multiple layout detected with same confidence ... No of similar layouts = {0}", layoutClassifications.Count));
                    layoutConfidence = layoutClassifications.OrderByDescending(lc => lc.Layout.Identifiers.Count).First();
                }
                else
                {
                    layoutConfidence = layoutClassifications[0];
                }
            }
            else if (layoutClassifications.Count == 1)
            {
                layoutConfidence = layoutClassifications[0];
            }

            if (layoutConfidence != null && layoutConfidence.Confidence >= LayoutConfidenceThreshold)
            {
                VBotLogger.Trace(() => string.Format("[GetLayout]  Document is classified with Layout '{0}'.", layoutConfidence.Layout.Name));
                _layoutConfidence = layoutConfidence;
                return layoutConfidence.Layout.Clone();
            }

            VBotLogger.Trace(() => string.Format("[GetLayout]  No layout mapped. This may be a new layout."));

            return new Layout();
        }

        private Line CreateLineFromField(Field field)
        {
            Line newLine = new Line();

            int width = field.Bounds.Right - field.Bounds.Left;
            int height = field.Bounds.Bottom - field.Bounds.Top;

            int offset = 50;

            if (width <= offset && height >= offset)
            {
                newLine.X1 = newLine.X2 = (field.Bounds.Left + field.Bounds.Right) / 2;

                newLine.Y1 = field.Bounds.Top;
                newLine.Y2 = field.Bounds.Bottom;
            }
            else if (height <= offset && width >= offset)
            {
                newLine.Y1 = newLine.Y2 = (field.Bounds.Top + field.Bounds.Bottom) / 2;

                newLine.X1 = field.Bounds.Left;
                newLine.X2 = field.Bounds.Right;
            }

            return newLine;
        }

        private bool IsValidWordField(Field field)
        {
            string fieldText = field.Text.Trim();

            if (field.SegmentationType != SegmentationType.Word || string.IsNullOrWhiteSpace(fieldText))
            {
                return false;
            }

            if (field.Bounds.Height <= 5 && field.Bounds.Width >= 10)
            {
                VBotLogger.Trace(() => string.Format("[IsValidWordField] Field '{0}' is too small in height. Bounds = {1}", field.Text, field.Bounds));
                return false;
            }

            if (field.Text.Contains("___") || field.Text.Contains("---"))
            {
                VBotLogger.Trace(() => string.Format("[IsValidWordField] Field '{0}' is invalid due to ambiguous pattern. Bounds = {1}", field.Text, field.Bounds));
                return false;
            }

            if (field.Bounds.Width < 3 && field.Bounds.Height < 3)
            {
                VBotLogger.Trace(() => string.Format("[IsValidWordField] Field '{0}' is too small in width and height. Bounds = {1}", field.Text, field.Bounds));
                return false;
            }

            return true;
        }

        private bool IsValidLineField(Field field)
        {
            if (field.SegmentationType != SegmentationType.Line)
                return false;

            int width = field.Bounds.Right - field.Bounds.Left;
            int height = field.Bounds.Bottom - field.Bounds.Top;

            int offset = 50;

            if (width <= offset && height >= offset)
                return true;

            if (height <= offset && width >= offset)
                return true;

            return false;
        }

        private ValidatorAdvanceInfo getValidatorAdvanceInfo(VisionBot visionBot, int confidenceThreshold)
        {
            return new ValidatorAdvanceInfo()
            {
                CultureInfo = Cognitive.VisionBotEngine.Model.Generic.CultureHelper.GetCultureInfo(visionBot.Language),
                ConfidenceThreshold = confidenceThreshold
            };
        }

        internal static Field CreateFieldFromRegion(SegmentedRegion segRegion)
        {
            Field field = new Field();

            field.Text = segRegion.Text;
            field.SegmentationType = segRegion.Type;
            field.Confidence = segRegion.Confidence;
            field.Bounds = segRegion.Bounds;
            field.Type = FieldType.SystemField;
            field.Angle = segRegion.Angle;
            return field;
        }
    }

    internal static class TemperoryExtensionMethodsToBeMovedToBetterLocation
    {
        public static bool IsNullOrEmpty(this string subject)
        {
            return string.IsNullOrEmpty(subject);
        }

        public static bool IsNull(this Field field)
        {
            return field == null ||
                field.Bounds.Equals(new Rectangle());
        }
    }
}