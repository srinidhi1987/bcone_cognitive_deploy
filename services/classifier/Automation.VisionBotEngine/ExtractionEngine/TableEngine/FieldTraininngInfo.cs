﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Automation.VisionBotEngine.TableValueExtractor;

namespace Automation.Cognitive.VisionBotEngine.ExtractionEngine.TableEngine
{
    class FieldTraininngInfo
    {
        public FieldDef FieldDef;
        public FieldLayout FieldLayout;

        public Rectangle RespectToTableValueRect;

        public Rectangle AbsoluteValueRect;

        public DataTypes valueDataType;

        public FieldTraininngInfo(FieldDef fieldDef, FieldLayout fieldLayout
            , Rectangle absoluteValueRect
            , Rectangle respectToTableValueRect)
        {
            this.FieldDef = fieldDef;
            this.FieldLayout = fieldLayout;
            this.AbsoluteValueRect = absoluteValueRect;
            this.RespectToTableValueRect = respectToTableValueRect;
        }
    }
}
