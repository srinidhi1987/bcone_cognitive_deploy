﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Newtonsoft.Json;
using System.IO;
using Automation.Services.Client;

namespace Automation.VisionBotEngine
{
    class ValueBasedTableExtractor
    {
        private List<string> filesTobeDeleted;
        public void ExtractTableValue(VisionBot vbot, VBotDocument vbotDoc)
        {
            int tableIndex = vbotDoc.Layout.Tables.FindIndex(x =>
            {
                if (x.IsValueMappingEnabled == true)
                {
                    var tableDef = vbot.DataModel.Tables
                    .FirstOrDefault(y => y.Id == x.TableId);

                    if (tableDef != null && tableDef.IsDeleted == false)
                    {
                        return true;
                    }
                    return false;
                }
                return false;
            });

            if (tableIndex > -1)
            {
                var inputJsonObject = prepareInputJsonObject(vbot, vbotDoc);
                string inputJsonString = JsonConvert.SerializeObject(inputJsonObject);
                File.WriteAllText("C:\\Input.json", inputJsonString);
                GetTableValues(inputJsonObject, vbot, vbotDoc);
                DeleteSavedFile();
            }
            else
                VBotLogger.Trace(() => string.Format("[ExtractTableValues] No Tables are mapped based on Value"));

        }

        private void DeleteSavedFile()
        {
            if (VBotLogger.GetLogLevel() < (int)LogTypes.ERROR)
            {
                foreach (string filePath in filesTobeDeleted)
                {
                    try
                    {
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Log(nameof(ValueBasedTableExtractor), nameof(ExtractTableValue), $"Unable to Delete the File:[{filePath}]");
                        VBotLogger.Error(() => $"Unable to dlete the File: [{filePath}. Exception Occured: [{ex.Message}]");
                    }
                }
            }
        }

        private void GetTableValues(object inputJsonObject, VisionBot vbot, VBotDocument vbotDoc)
        {
            VBotLogger.Trace(() => string.Format("[ExtractTableValues] Extraction Using Alternative Algorithm (ML Python) Started."));

            List<ValueBasedTableLayout> tables = GetTablesViaEndpoint(inputJsonObject);

            for (int i = 0; i < tables.Count; i++)
            {
                ValueBasedTableLayout table = tables[i];
                TableValue tableValue = new TableValue();
                TableLayout currTableLayout = vbotDoc.Layout.Tables.FirstOrDefault(x => x.Id == table.Id);
                if (currTableLayout == null)
                {
                    VBotLogger.Info(() => string.Format($"Table Layout is not found for the Table ID: [{table.Id}]"));
                    continue;
                }

                TableDef tableDef = vbot.DataModel.Tables.FirstOrDefault(x => x.Id == currTableLayout.TableId);
                if (tableDef == null)
                {
                    VBotLogger.Info(() => string.Format($"Table Definition is not found for the TableDef ID: [{currTableLayout.TableId}]"));
                    continue;
                }

                if (tableDef.IsDeleted)
                    continue;

                tableValue.Headers = tableDef.Columns.FindAll(c => c.IsDeleted == false);
                VBotLogger.Trace(() => string.Format("[ValueBasedTableExtractor::GetTableValues] Extrating table with Value based mapping'{0}'...", tableDef.Name));

                foreach (var currrow in table.Rows)
                {
                    var row = new DataRecord();

                    foreach (var column in tableDef.Columns.Where(x => x.IsDeleted != true))
                    {
                        var cell = new FieldData();
                        cell.Field = column;// tableDef.Columns.FirstOrDefault(x=>x.Id == column.defid);
                        var cellValue = currrow.Columns.FirstOrDefault(x => x.defid == column.Id);
                        cell.Value = new TextValue();

                        if (cellValue != null)
                        {
                            cell.Value.Field = new Field()
                            {
                                Text = cellValue.text,
                                Bounds = cellValue.Bound.ToRectangle(),
                                Confidence = 100,
                                SegmentationType = SegmentationType.Word,
                                Type = FieldType.SystemField,
                            };

                            (cell.Value as TextValue).Value = cellValue.text;
                        }
                        else
                        {
                        }
                        if (string.IsNullOrWhiteSpace(cell?.Value?.Field?.Text))
                        {
                            if (cell.Field.IsRequired)
                            {
                                VBotLogger.Trace(() => $"[ExtractFieldValues] Unable to extract column {cell?.Field?.Name}.");
                            }
                            else
                            {
                                cell.Value.Field.Text = cell.Field.DefaultValue;
                            }
                        }


                        row.Fields.Add(cell);
                    }
                    tableValue.Rows.Add(row);
                }
                tableValue.TableDef = tableDef;
                vbotDoc.Data.TableDataRecord.Add(tableValue);

            }
            VBotLogger.Trace(() => string.Format("[ExtractTableValues] Extraction Using Alternative Algorithm (ML Python) End."));
        }

        private List<ValueBasedTableLayout> GetTablesViaEndpoint(object inputJsonObject)
        {
            MachineLearningEndpointConsumerService mlEndpointconsumerService = new MachineLearningEndpointConsumerService();
            List<ValueBasedTableLayout> tables = mlEndpointconsumerService.GetValueBasedTables(inputJsonObject);
            return tables;
        }
        private object prepareInputJsonObject(VisionBot vbot, VBotDocument vbotDoc)
        {
            Dictionary<string, object> input = new Dictionary<string, object>();
            VBotLogger.Trace(() => "[ValueBasedTableExtractor::prepareInputJsonObject] Preparing inputs for Table Extraction Algorithm");
            input.Add("segmenteddocument", getSegmentedDocument(vbotDoc));
            filesTobeDeleted = new List<string>();
            input.Add("documentproperties", getDocumentProperties(vbotDoc.DocumentProperties, true));
            input.Add("botdesigninfo", getbotDesignInfo(vbot, vbotDoc));
            VBotLogger.Trace(() => "[ValueBasedTableExtractor::prepareInputJsonObject] Inputs for the Table Extraction algorithm are ready");

            return input;
        }

        private object getbotDesignInfo(VisionBot vbot, VBotDocument vbotDoc)
        {
            Dictionary<string, object> botDesignInfo = new Dictionary<string, object>();
            botDesignInfo.Add("tables", getTables(vbot, vbotDoc));
            botDesignInfo.Add("layoutdocumentproperties", getDocumentProperties(vbotDoc.Layout.DocProperties, false));
            return botDesignInfo;
        }

        private object getTables(VisionBot vbot, VBotDocument vbotDoc)
        {
            List<Dictionary<string, object>> allTables = new List<Dictionary<string, object>>();
            for (int i = 0; i < vbotDoc.Layout.Tables.Count; i++)
            {
                TableLayout curTable = vbotDoc.Layout.Tables[i];
                var tableDef = vbot.DataModel.Tables.FirstOrDefault(x => x.Id == curTable.TableId);
                if (curTable.IsValueMappingEnabled == false || tableDef.IsDeleted)
                    continue;


                Dictionary<string, object> curtableObj = new Dictionary<string, object>();
                curtableObj.Add("id", curTable.Id);
                curtableObj.Add("primarycolumnid", curTable?.PrimaryColumn?.Id);
                curtableObj.Add("footer", curTable.Footer.Label);
                curtableObj.Add("columns", getColumns(curTable.Columns, vbot.DataModel.Tables.FirstOrDefault(x => x.Id == curTable.TableId)));
                allTables.Add(curtableObj);
            }
            return allTables;
        }

        private object getColumns(List<FieldLayout> columns, TableDef tableDef)
        {
            List<Dictionary<string, object>> allColumnsObj = new List<Dictionary<string, object>>();
            for (int i = 0; i < columns.Count; i++)
            {
                FieldLayout curColumn = columns[i];
                FieldDef columnDef = tableDef.Columns.FirstOrDefault(x => x.Id == curColumn.FieldId);
                Dictionary<string, object> curColumnObj = new Dictionary<string, object>();
                curColumnObj.Add("columnid", curColumn.Id);
                curColumnObj.Add("defid", curColumn.FieldId);
                curColumnObj.Add("label", curColumn.Label);
                curColumnObj.Add("type", columnDef.ValueType.ToString());
                curColumnObj.Add("keybounds", getBounds(curColumn.Bounds));
                curColumnObj.Add("valuebounds", getBounds(curColumn.ValueBounds));
                curColumnObj.Add("isrequired", columnDef.IsRequired);
                curColumnObj.Add("isdollarcurrency", curColumn.IsDollarCurrency);
                allColumnsObj.Add(curColumnObj);
            }
            return allColumnsObj;
        }
        private object getDocumentProperties(DocumentProperties documentProperties, bool saveAllPageImages)
        {
            Dictionary<string, object> docProperties = new Dictionary<string, object>();
            docProperties.Add("id", documentProperties.Id);
            docProperties.Add("name", documentProperties.Name);
            docProperties.Add("pageproperties", getPageProperties(documentProperties, saveAllPageImages));
            return docProperties;
        }

        private object getPageProperties(DocumentProperties documentProperties, bool saveAllPageImages)
        {
            List<Dictionary<string, object>> allPages = new List<Dictionary<string, object>>();
            for (int i = 0; i < documentProperties.PageProperties.Count; i++)
            {
                Dictionary<string, object> currpagePropertiesObj = new Dictionary<string, object>();

                PageProperties currPageProperties = documentProperties.PageProperties[i];
                currpagePropertiesObj.Add("pageindex", currPageProperties.PageIndex);
                currpagePropertiesObj.Add("width", currPageProperties.Width);
                currpagePropertiesObj.Add("height", currPageProperties.Height);
                string imgFilePath = "";
                if (saveAllPageImages == true && VBotLogger.GetLogLevel() < (int)LogTypes.ERROR)
                {
                    imgFilePath = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".png");
                    SaveCurrentPageImage(imgFilePath, documentProperties.Path, i);
                    filesTobeDeleted.Add(imgFilePath);
                }
                currpagePropertiesObj.Add("path", imgFilePath);

                allPages.Add(currpagePropertiesObj);
            }
            return allPages;
        }

        private object getSegmentedDocument(VBotDocument vbotDoc)
        {
            Dictionary<string, object> segmenteddocument = new Dictionary<string, object>();
            segmenteddocument.Add("fields", getFields(vbotDoc.SirFields.Fields));
            segmenteddocument.Add("lines", getLines(vbotDoc.SirFields.Lines));
            return segmenteddocument;
        }
        private object getFields(List<Field> regions)
        {
            List<Dictionary<string, object>> allFiledsRegion = new List<Dictionary<string, object>>();
            for (int i = 0; i < regions.Count; i++)
            {
                Field fieldRegion = regions[i];
                Dictionary<string, object> currLine = new Dictionary<string, object>();
                currLine.Add("id", Guid.NewGuid());
                currLine.Add("text", fieldRegion.Text);
                currLine.Add("angle", fieldRegion.Angle);
                currLine.Add("confidence", fieldRegion.Confidence);
                currLine.Add("bounds", getBounds(fieldRegion.Bounds));
                allFiledsRegion.Add(currLine);
            }

            return allFiledsRegion;
        }

        private object getBounds(Rectangle bounds)
        {
            Dictionary<string, object> regionBound = new Dictionary<string, object>();
            regionBound.Add("x", bounds.X);
            regionBound.Add("y", bounds.Y);
            regionBound.Add("width", bounds.Width);
            regionBound.Add("height", bounds.Height);
            return regionBound;
        }

        private object getLines(List<Line> regions)
        {
            List<Dictionary<string, object>> allLineObj = new List<Dictionary<string, object>>();
            for (int i = 0; i < regions.Count; i++)
            {
                Line lineRegion = regions[i];
                Dictionary<string, object> currLine = new Dictionary<string, object>();
                currLine.Add("id", Guid.NewGuid());
                currLine.Add("x1", lineRegion.X1);
                currLine.Add("y1", lineRegion.Y1);
                currLine.Add("x2", lineRegion.X2);
                currLine.Add("y2", lineRegion.Y2);
                allLineObj.Add(currLine);
            }

            return allLineObj;
        }
        void SaveCurrentPageImage(string pathToSaveCurrPage, string docPath, int pageIndex)
        {
            DocumentProperties docProps = new DocumentProperties();
            docProps.Path = docPath;

            IDocumentPageImageProvider processingDocumentPageImageProvider =
                new DocumentPageImageProvider(docProps, new ImageProcessingConfig(), new VBotEngineSettings(), false);
            Image img = processingDocumentPageImageProvider.GetPage(pageIndex);
            if (img != null)
            {
                img.Save(pathToSaveCurrPage);
            }
        }
    }
}
