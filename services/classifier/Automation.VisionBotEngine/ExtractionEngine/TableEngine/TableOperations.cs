﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using Cognitive.VisionBotEngine;
    using Cognitive.VisionBotEngine.Common;
    using ResizeRescan.Interface;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    internal interface ITableOperations
    {
        TableValue GetTableValue(TableInfo tableInfo, List<PageProperties> pageProperties);
    }

    // TODO: use interface instead of table in all references
    internal class TableOperations : ITableOperations
    {
        public TableOperations()
            : this(new RectangleOperations(), new FieldOperations())
        { }

        public TableOperations(IRescan rescan) : this(new RectangleOperations(), new FieldOperations())
        {
            _rescan = rescan;
        }

        public TableOperations(IRectangleOperations rectangleOperations, IFieldOperations fieldOperations)
        {
            _rectOperations = rectangleOperations;
            _fieldOperations = fieldOperations;
        }

        private IFieldOperations _fieldOperations;
        private IRectangleOperations _rectOperations;
        private IRescan _rescan;
        public string path;
        public TableValue GetTableValue(TableInfo tableInfo, List<PageProperties> pageProperties)
        {
            TableValue result = new TableValue();
            fillTableHeaders(result, tableInfo);
            fillTableRows(result, tableInfo, pageProperties);
            return result;
        }

        public FieldValue GetCellValue(List<Field> fields, Rectangle cellRect)
        {
            FieldValue fieldValue = new TextValue();
            fieldValue.Field.Bounds = cellRect;
            if (!_rectOperations.IsRectangleEmpty(cellRect))
            {
                List<Field> cellfields = fields.FindAll(field => cellRect.Contains(field.Bounds.X + (field.Bounds.Width / 2), field.Bounds.Y + (field.Bounds.Height / 2)));
                cellfields = cellfields.OrderBy(field => field.Bounds.Top).ToList();

                fieldValue.Field.Text = _fieldOperations.GetTextFromFields(cellfields);


                if (cellfields.Count > 0)
                {
                    fieldValue.Field.Confidence = new ConfidenceHelper().GetMergedFieldConfidence(cellfields);
                    fieldValue.Field.Bounds = _fieldOperations.GetBoundsFromFields(cellfields);
                }
            }

            return fieldValue;
        }

        public Rectangle GetTableRect(TableInfo tableInfo
            , FieldLayout primaryColumnLayout
            , Field bottomTableRegion
            , List<Field> priamryColumnValues = null)
        {
            Rectangle rect = new Rectangle();

            Field leftMostColumn = tableInfo.Headers[0];
            Field rightMostColumn = tableInfo.Headers[tableInfo.Headers.Count - 1];

            rect.X = GetTableLeft(leftMostColumn, tableInfo.Lines);
            rect.Y = primaryColumnLayout.Bounds.Top;

            // TODO : Header with the lowest top should be the table top
            if (Math.Abs(leftMostColumn.Bounds.Y - rect.Y) > 2 * leftMostColumn.Bounds.Height)
            {
                rect.Y = leftMostColumn.Bounds.Top;
            }

            int tableRight = GetTableRight(rightMostColumn, tableInfo.Lines);

            rect.Width = tableRight - rect.X;

            int tableBottom = getTableBottomFromFooter(tableInfo
                , leftMostColumn.Bounds.Bottom + 1);

            if (tableBottom == 0)
            {
                tableBottom = getTableBottomFromPrimaryColumnValues(tableInfo
                , bottomTableRegion
                , priamryColumnValues
                , leftMostColumn.Bounds.X
                , rightMostColumn.Bounds.X);
            }

            if (tableBottom == 0)
            {
                tableBottom = bottomTableRegion.Bounds.Bottom + (bottomTableRegion.Bounds.Height * 5);

                int lastRowTop = bottomTableRegion.Bounds.Top;

                foreach (Field header in tableInfo.Headers)
                {
                    Rectangle cell = new Rectangle(header.Bounds.X, lastRowTop, header.Bounds.Width, bottomTableRegion.Bounds.Height * 2);
                    var foundFields = _fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, cell);
                    if (foundFields != null && foundFields.Count > 0)
                    {
                        Field parent = tableInfo.Regions.FirstOrDefault(element => element.Id == foundFields[0].ParentId);
                        if (parent != null && parent.Bounds.Bottom > tableBottom)
                        {
                            tableBottom = parent.Bounds.Bottom;
                        }
                    }
                }
            }

            rect.Height = tableBottom - rect.Top;

            return rect;
        }

        private int getTableBottomFromPrimaryColumnValues(TableInfo tableInfo, Field bottomTableRegion, List<Field> priamryColumnValues, int tableLeft, int tableRight)
        {
            int tableBottom = 0;
            if (priamryColumnValues != null
                   && priamryColumnValues.Count > 1)
            {
                Rectangle secondLastRowRect = new Rectangle(tableLeft
                        , priamryColumnValues[priamryColumnValues.Count - 2].Bounds.Top
                        , tableRight - tableLeft
                        , priamryColumnValues[priamryColumnValues.Count - 1].Bounds.Top - priamryColumnValues[priamryColumnValues.Count - 2].Bounds.Top);


                List<Field> secondLastRowField = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(tableInfo.Fields, secondLastRowRect);

                Field bottomFieldOfSecondLastRow = null;
                if (secondLastRowField != null && secondLastRowField.Count > 0)
                {
                    bottomFieldOfSecondLastRow = secondLastRowField.OrderByDescending(x => x.Bounds.Bottom).ToList()[0];
                }

                if (bottomFieldOfSecondLastRow == null)
                {
                    return tableBottom;
                }

                int maxBottomOfSecondLastRow = bottomFieldOfSecondLastRow.Bounds.Bottom + 10;
                int predictedRowBottomOfSecondLastRow = getRowBottomFromVerticalLines(tableInfo
                , priamryColumnValues[priamryColumnValues.Count - 2]
                , tableLeft
                , tableRight
                , maxBottomOfSecondLastRow);

                int actualBottomOfTheSecondLastRow = bottomFieldOfSecondLastRow.Bounds.Bottom;
                bool isVericalLinesAreValidToFindRowBottom = (predictedRowBottomOfSecondLastRow - actualBottomOfTheSecondLastRow + 3) > 0;

                if (isVericalLinesAreValidToFindRowBottom)
                {
                    int maxRowBottomofLastRow = bottomTableRegion.Bounds.Bottom + (bottomTableRegion.Bounds.Height * 5);

                    tableBottom = getRowBottomFromVerticalLines(tableInfo
                        , priamryColumnValues[priamryColumnValues.Count - 1]
                        , tableLeft
                        , tableRight
                        , maxRowBottomofLastRow);
                }
            }

            return tableBottom;
        }

        private int getRowBottomFromVerticalLines(TableInfo tableInfo
            , Field priamryColumnCell
            , int tableLeft, int tableRight
            , int maxRowBottom)
        {
            int offset = 3;
            int rowBottom = 0;
            bool isLoopInitalized = false;
            List<Line> possibleLines = new List<Line>();

            do
            {
                Rectangle leftRectAtLastRow = new Rectangle(tableLeft
                    , priamryColumnCell.Bounds.Top + offset
                    , priamryColumnCell.Bounds.Width
                    , priamryColumnCell.Bounds.Height);

                Rectangle rightRectAtLastRow = new Rectangle(tableRight
                   , priamryColumnCell.Bounds.Top + offset
                   , priamryColumnCell.Bounds.Width
                   , priamryColumnCell.Bounds.Height);

                List<Line> lines = _rectOperations.FindAllVerticalLinesInBetweenRects(leftRectAtLastRow
                     , rightRectAtLastRow
                     , tableInfo.Lines);


                bool isLinesMatched = possibleLines.TrueForAll(
                    x => lines.FindIndex(line => Math.Abs(x.X1 - line.X1) > 5) > -1);

                if (isLoopInitalized
                    && (!isLinesMatched
                    || (leftRectAtLastRow.Bottom > (maxRowBottom + leftRectAtLastRow.Height))))
                {
                    break;
                }

                if (!isLoopInitalized)
                {
                    if (lines.Count == 0)
                    {
                        break;
                    }

                    isLoopInitalized = true;
                    possibleLines = lines;
                }
                if (leftRectAtLastRow.Top > priamryColumnCell.Bounds.Bottom)
                {
                    rowBottom = leftRectAtLastRow.Top;
                }

                offset += 3;
            }
            while (true);

            return rowBottom;
        }

        public Line GetRowLine(int tableLeft, int tableRight, int rowTop)
        {
            Line rowLine = new Line();

            rowLine.X1 = tableLeft;
            rowLine.X2 = tableRight;

            rowLine.Y1 = rowLine.Y2 = rowTop;

            return rowLine;
        }

        public int GetTableRight(Field rightColumnRegion, List<Line> lines)
        {
            return rightColumnRegion.Bounds.X + rightColumnRegion.Bounds.Width;
        }

        public int GetTableLeft(Field leftColumnRegion, List<Line> lines)
        {
            return leftColumnRegion.Bounds.X;
        }

        public List<Field> GetPrimaryColumnValues(FieldLayout primaryColumnLayout
            , TableInfo tableInfo
            , TableValue tableValue
            , bool isThisHeaderLessTable)
        {
            List<Field> masterColumnRows = new List<Field>();

            int masterColumnIndex = GetColumnIndex(tableInfo.Headers, primaryColumnLayout);

            if (masterColumnIndex < 0)
            {
                return masterColumnRows;
            }

            tableInfo.Layout.PrimaryColumn.Bounds = tableInfo.Headers[masterColumnIndex].Bounds;

            int colValueStartHeight = tableInfo.Layout.PrimaryColumn.Bounds.Bottom + 1;

            int tableBottom = getTableBottomFromFooter(tableInfo, colValueStartHeight);

            if (tableBottom == 0)
            {
                tableBottom = getTableBottomFromFields(tableInfo
                    , masterColumnIndex
                    , tableValue
                    , isThisHeaderLessTable);
            }

            int colHeight = tableBottom - colValueStartHeight + tableInfo.Layout.PrimaryColumn.Bounds.Height;

            Rectangle colRect = Rectangle.Empty;
            if (!isThisHeaderLessTable)
            {
                colRect = GetColumnValueRect(tableInfo, primaryColumnLayout, colHeight);
            }
            else
            {
                colRect = tableInfo.Headers[masterColumnIndex].Bounds.Clone();
            }

            colRect.Y = colValueStartHeight;
            colRect.Height = tableBottom - colRect.Y;

            //  masterColumnRows = tableInfo.Fields.FindAll(field => field.Type == FieldType.SystemField && colRect.Contains(field.Bounds) || colRect.IntersectsWith(field.Bounds));
            masterColumnRows = tableInfo.Fields.FindAll(field => field.Type == FieldType.SystemField && colRect.Contains(field.Bounds));
            masterColumnRows = masterColumnRows.OrderBy(field => field.Bounds.Top).ToList();

            bool areColumnsAtSameHeightExists = false;
            int minRowHeight = 10;

            do
            {
                areColumnsAtSameHeightExists = false;

                for (int i = 1; i < masterColumnRows.Count; i++)
                {
                    if ((Math.Abs(masterColumnRows[i - 1].Bounds.Top - masterColumnRows[i].Bounds.Top) < 10) ||
                        masterColumnRows[i - 1].Bounds.Height < minRowHeight || masterColumnRows[i].Bounds.Height < minRowHeight)
                    {
                        areColumnsAtSameHeightExists = true;
                        masterColumnRows.RemoveAt(i);
                        break;
                    }
                }
            }
            while (areColumnsAtSameHeightExists);

            return masterColumnRows;
        }

        public List<Field> GetCombinedFieldsWithGivenFieldWhichMatchWithGivenText(Field field, List<Field> tempFields, string text, double similarity)
        {
            Rectangle colRect = new Rectangle(field.Bounds.Left, field.Bounds.Top, field.Bounds.Width + 50, field.Bounds.Height);
            List<Field> horizontallyfieldsStorage = _fieldOperations.GetFieldsIntersectingWithRect(tempFields, colRect);
            horizontallyfieldsStorage = horizontallyfieldsStorage.FindAll(element => element.Text.StartsWith(text));
            List<Field> verticallyfieldsStorage = new List<Field>();
            List<Field> mergedFieldStorage = new List<Field>();
            foreach (Field elemnt in horizontallyfieldsStorage)
            {
                tempFields.Remove(elemnt);
            }

            for (int j = 2; j < 5; j++)
            {
                colRect = new Rectangle(field.Bounds.Left, field.Bounds.Top, field.Bounds.Width, field.Bounds.Height * j);
                List<Field> verticallyfields = _fieldOperations.GetFieldsIntersectingWithRect(tempFields, colRect);
                verticallyfields = verticallyfields.FindAll(element => element.Text.StartsWith(text));
                foreach (Field elemnt in verticallyfields)
                {
                    tempFields.Remove(elemnt);
                }
                verticallyfieldsStorage.AddRange(verticallyfields);
            }

            foreach (Field elemnt in horizontallyfieldsStorage)
            {
                mergedFieldStorage.Add(_fieldOperations.GetSingleFieldFromFields(new List<Field>() { field, elemnt }));
            }

            foreach (Field elemnt in verticallyfieldsStorage)
            {
                mergedFieldStorage.Add(_fieldOperations.GetSingleFieldFromFields(new List<Field>() { field, elemnt }));
            }
            return mergedFieldStorage;
        }

        private static Field getPreviousColumnHeader(TableInfo tableInfo, int columnHeaderIndex)
        {
            Field columnHeader = tableInfo.Headers[columnHeaderIndex];

            Field prviousColumnHeader = tableInfo.Headers.LastOrDefault(field => field.Bounds.Right <= columnHeader.Bounds.Left);

            return prviousColumnHeader;
        }

        private static Field getNextColumnHeader(TableInfo tableInfo, int columnHeaderIndex)
        {
            Field columnHeader = tableInfo.Headers[columnHeaderIndex];

            Field nextColumnHeader = tableInfo.Headers.FirstOrDefault(field => field.Bounds.Left >= columnHeader.Bounds.Right);

            return nextColumnHeader;
        }

        private static bool isValidColumnRect(Field nextColumnHeader, Field prevColumnHeader, int minLeft, int maxRight, Rectangle tempColRect)
        {
            if (prevColumnHeader != null && tempColRect.Left < minLeft)
            {
                return false;
            }

            if (nextColumnHeader != null && tempColRect.Right > maxRight)
            {
                return false;
            }

            return true;
        }

        private static bool hasColumns(TableInfo tableInfo)
        {
            return (tableInfo?.Layout?.Columns?.Count ?? 0) > 0;
        }

        private void fillTableHeaders(TableValue tableValue, TableInfo tableInfo)
        {
            VBotLogger.Trace(() => string.Format("[fillTableHeaders] Entered."));
            tableValue.Headers = tableInfo.ColumnDefs.Columns.FindAll(c => c.IsDeleted == false);
        }

        private void fillTableRows(TableValue tableValue, TableInfo tableInfo, List<PageProperties> pages)
        {
            VBotLogger.Trace(() => string.Format("[fillTableRows] Entered."));
            if (!IsPrimaryColumnDefined(tableInfo.Layout.PrimaryColumn))
            {
                VBotLogger.Trace(() => $"[{nameof(fillTableRows)}] Primary column is not defined in table \"{tableInfo.Layout.TableId}\"");
                return;
            }

            foreach (var fieldLayout in tableInfo.Layout.Columns)
            {
                fieldLayout.IsValueBoundAuto = false;
                fieldLayout.IsValueBoundAuto = isSelfValueMapped(fieldLayout);
            }

            TableInfo clonedTableInfo = tableInfo.Clone();
            List<Field> allPagesField = new List<Field>(clonedTableInfo.Fields);
            List<Field> allPagesRegions = new List<Field>(clonedTableInfo.Regions);
            List<Line> allPagesLine = new List<Line>(clonedTableInfo.Lines);
            int currentPageTopOffSet = 0;
            bool isTableStartFound = false;

            List<FieldLayout> lastPageTableColumns = new List<FieldLayout>();
            List<Field> lastPageTableHeaderFields = new List<Field>();
            for (int pageNo = 0; pageNo < pages.Count; pageNo++)
            {
                ReAssignTableColumnLabels(tableInfo, clonedTableInfo);
                FieldLayout primaryColumnLayout = clonedTableInfo.Layout.PrimaryColumn;

                Rectangle currentPageRect = new Rectangle(0, currentPageTopOffSet, pages[pageNo].Width, pages[pageNo].Height);

                clonedTableInfo.Fields = GetCurrentPageFieldExceptPageFooter(currentPageRect, allPagesField);

                clonedTableInfo.Lines = getCurrentPageLines(currentPageRect, allPagesLine);

                clonedTableInfo.Regions = GetCurrentPageFieldExceptPageFooter(currentPageRect, allPagesRegions);

                currentPageTopOffSet += pages[pageNo].Height;

                if (clonedTableInfo.Fields?.Count > 0)
                {
                    bool isThisHeaderLessTable = false;

                    Point centerPointOfDocument = _rectOperations.GetCenterPoint(currentPageRect);

                    rotateFieldsAsPerTheirAngle(clonedTableInfo.Fields, centerPointOfDocument);

                    Field primaryColField = GetPrimaryColumnField(primaryColumnLayout, clonedTableInfo);

                    if (primaryColField == null || string.IsNullOrWhiteSpace(primaryColField.Text))
                    {
                        VBotLogger.Trace(() => $"[{nameof(fillTableRows)}] Primary column is not identified with label '{primaryColumnLayout.Label}' in Page Number '{pageNo}'");

                        if (isTableStartFound)
                        {
                            clonedTableInfo.Headers = lastPageTableHeaderFields;
                            int top = GetContinuedHeaderlessTableStartPosition(clonedTableInfo);
                            if (top < 0)
                            {
                                break;
                            }

                            isThisHeaderLessTable = true;

                            foreach (Field header in clonedTableInfo.Headers)
                            {
                                header.Bounds = new Rectangle(header.Bounds.Left
                                    , top - header.Bounds.Height
                                    , header.Bounds.Width
                                    , header.Bounds.Height);
                            }

                            clonedTableInfo.Layout.Columns = lastPageTableColumns;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {

                        if (!(isSelfValueMapped(primaryColumnLayout)))
                        {
                            primaryColField.Bounds = new Rectangle(primaryColField.Bounds.Left
                                + primaryColumnLayout.ValueBounds.Left
                                //, primaryColField.Bounds.Top + primaryColumnLayout.ValueBounds.Top
                                , primaryColField.Bounds.Top
                                , primaryColumnLayout.ValueBounds.Width
                                , primaryColumnLayout.ValueBounds.Height);
                        }

                        isTableStartFound = true;
                        clonedTableInfo.Headers = GetTableHeaders(clonedTableInfo, primaryColField);
                    }

                    appendTableValueForGivenTableInfo(primaryColumnLayout
                        , clonedTableInfo
                        , tableValue
                        , isThisHeaderLessTable);

                    if (hasColumns(clonedTableInfo))
                    {
                        lastPageTableColumns = clonedTableInfo.Layout.Columns.Select(column => column.Clone()).ToList();
                        lastPageTableHeaderFields = clonedTableInfo.Headers;
                    }
                }
            }
        }

        private static List<Line> getCurrentPageLines(Rectangle currentPageRect, List<Line> lines)
        {
            List<Line> result = new List<Line>();

            if (lines == null || lines.Count == 0)
            { return result; }

            foreach (Line line in lines)
            {
                if (currentPageRect.Contains(line))
                {
                    result.Add(line);
                }
            }

            return result;
        }

        private void rotateFieldsAsPerTheirAngle(List<Field> fields, Point referencePoint)
        {
            foreach (Field field in fields)
            {
                if (field.Angle == decimal.Zero)
                {
                    continue;
                }

                field.Bounds = new Rectangle(rotatePoint(field.Bounds.Location, referencePoint, -(double)field.Angle), field.Bounds.Size);
            }
        }

        private void ReAssignTableColumnLabels(TableInfo source, TableInfo destination)
        {
            if (source == null || destination == null || source.Layout == null || destination.Layout == null)
            {
                return;
            }

            var sourceColumns = source.Layout.Columns.ToDictionary(x => x.Id);

            foreach (FieldLayout column in destination.Layout.Columns)
            {
                if (!sourceColumns.ContainsKey(column.Id))
                {
                    continue;
                }

                column.Label = sourceColumns[column.Id].Label;
            }
        }

        private List<Field> GetCurrentPageFieldExceptPageFooter(Rectangle pageBoundry, List<Field> allPagesField)
        {
            List<Field> pageFields = _fieldOperations.GetAllSystemFieldsWithinRect(allPagesField, pageBoundry);

            int possibleFooterHeight = 200;
            Rectangle possiblePagefooterBoarder = new Rectangle(
                                                         0,
                                                         pageBoundry.Bottom - possibleFooterHeight,
                                                         pageBoundry.Width,
                                                         possibleFooterHeight);

            List<Field> possibleFooterFields = _fieldOperations.GetAllSystemFieldsWithinRect(pageFields, possiblePagefooterBoarder);

            List<Field> actualFooterFields = GetActualFooterFields(possibleFooterFields, possiblePagefooterBoarder);

            foreach (Field field in actualFooterFields)
            {
                pageFields.Remove(field);
            }

            return pageFields;
        }

        private List<Field> GetActualFooterFields(List<Field> possibleFooterField, Rectangle pagefooterRect)
        {
            List<string> footerRegexes = new List<string>()
                                            {
                                                @"(page)[ ]+\d*[ ]+of[ ]+\d*",
                                                @"(page)[ ]*\d*[ ]*/[ ]*\d*",
                                                @"(page)[ ]*\d*"
                                            };

            List<Field> actualFooterFields = new List<Field>();
            foreach (Field field in possibleFooterField)
            {
                foreach (string pattern in footerRegexes)
                {
                    if (Regex.IsMatch(field.Text.ToLower(), pattern))
                    {
                        actualFooterFields.Add(field);
                        break;
                    }
                }
            }

            Rectangle actualFooterBoarder = _fieldOperations.GetBoundsFromFields(actualFooterFields);
            List<Field> fieldsToBeRemoved = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(possibleFooterField, new Rectangle(0, actualFooterBoarder.Top, pagefooterRect.Width, actualFooterBoarder.Height));

            return fieldsToBeRemoved;
        }

        private int GetContinuedHeaderlessTableStartPosition(TableInfo pageSpecificTableInfo)
        {
            int top = pageSpecificTableInfo.Fields.Min(field => field.Bounds.Top);

            int bottom = pageSpecificTableInfo.Fields.Max(field => field.Bounds.Bottom);
            bool isNotIntersected = true;

            if (pageSpecificTableInfo.Headers.Count == 0)
            {
                return -1;
            }

            int minimumNumberOfColumnNeededToDecidePossibiltyOfTable = (int)((pageSpecificTableInfo.Headers.Count / 2) + 1);
            for (int i = 0; i < bottom; i++)
            {
                isNotIntersected = true;
                Rectangle rectLeft = new Rectangle();
                Rectangle rectRight = new Rectangle();

                foreach (Field header in pageSpecificTableInfo.Headers)
                {
                    rectLeft = new Rectangle(header.Bounds.X - 1, top - 2 + i, 1, header.Bounds.Height);
                    rectRight = new Rectangle(header.Bounds.X + header.Bounds.Width + 1, top - 2 + i, 1, header.Bounds.Height);
                    if (_fieldOperations.GetFieldsIntersectingWithRect(pageSpecificTableInfo.Fields, rectLeft).Count > 0 || _fieldOperations.GetFieldsIntersectingWithRect(pageSpecificTableInfo.Fields, rectRight).Count > 0)
                    {
                        isNotIntersected = false;
                        break;
                    }
                }
                if (isNotIntersected)
                {
                    bool isNotEmptySpace = false;
                    int numberOfCellContainData = 0;
                    foreach (Field header in pageSpecificTableInfo.Headers)
                    {
                        Rectangle rect = new Rectangle(header.Bounds.X, top + i, header.Bounds.Width, header.Bounds.Height);
                        if (_fieldOperations.GetFieldsWithinBoundField(pageSpecificTableInfo.Fields, rect).Count > 0)
                        {
                            numberOfCellContainData++;
                            if (numberOfCellContainData >= minimumNumberOfColumnNeededToDecidePossibiltyOfTable)
                            {
                                isNotEmptySpace = true;
                                break;
                            }
                        }
                    }
                    if (isNotEmptySpace)
                    {
                        top = rectLeft.Top;
                        return top;
                    }
                }
            }

            return -1;
        }

        private void appendTableValueForGivenTableInfo(FieldLayout primaryColumnLayout
            , TableInfo tableInfo
            , TableValue tableValue
            , bool isThisHeaderLessTable)
        {
            if (tableInfo.Headers.Count > 0)
            {
                List<Field> primaryColumnRowValues = GetPrimaryColumnValues(primaryColumnLayout
                    , tableInfo
                    , tableValue
                    , isThisHeaderLessTable);

                if (primaryColumnRowValues.Count > 0)
                {
                    var lastCellValueOfPrimaryColumn = primaryColumnRowValues[primaryColumnRowValues.Count - 1];

                    Rectangle tableRect = GetTableRect(tableInfo
                        , primaryColumnLayout
                        , lastCellValueOfPrimaryColumn
                        , primaryColumnRowValues);

                    List<int> rowTop = GetRowTops(primaryColumnRowValues
                        , primaryColumnLayout
                        , tableRect
                        , tableInfo.Lines);

                    List<Line> headerLeftLines = new List<Line>();
                    List<Line> headerRightLines = new List<Line>();
                    List<Rectangle> headerBounds = new List<Rectangle>();

                    for (int i = 0; i < tableValue.Headers.Count; i++)
                    {
                        FieldLayout colLayout = GetColumnLayout(tableInfo, tableValue.Headers[i].Id);

                        if (!isThisHeaderLessTable)
                        {
                            updateHeaderBoundForGivenFieldLayout(tableInfo
                              , colLayout
                              , tableRect.Height);
                        }

                        Line leftLine = new Line();
                        Line rightLine = new Line();
                        Rectangle headerBound = new Rectangle();
                        int columnIndex = GetColumnIndex(tableInfo.Headers, colLayout);

                        if (columnIndex > -1)
                        {
                            headerBound = tableInfo.Headers[columnIndex].Bounds.Clone();
                            leftLine = new Line(headerBound.Left, headerBound.Left
                                , headerBound.Top, headerBound.Bottom);
                            rightLine = new Line(headerBound.Right, headerBound.Right
                                , headerBound.Top, headerBound.Bottom);
                        }

                        headerLeftLines.Add(leftLine);
                        headerRightLines.Add(rightLine);
                        headerBounds.Add(headerBound);
                    }
                    int noOfRowInTableValue = tableValue.Rows.Count;
                    for (int j = 0; j < rowTop.Count; j++)
                    {
                        tableValue.Rows.Add(new DataRecord());
                        tableValue.Rows[noOfRowInTableValue + j].Fields = new List<FieldData>(tableInfo.Headers.Count - 1);

                        for (int i = 0; i < tableValue.Headers.Count; i++)
                        {
                            int offset = 1;

                            int cellTop = rowTop[j] - offset;

                            if (j == 0 && !headerBounds[i].IsEmpty)
                            {
                                cellTop = headerBounds[i].Bottom + offset;
                            }

                            int cellBottom = j < rowTop.Count - 1 ? rowTop[j + 1] - offset : tableRect.Bottom;

                            Rectangle cellRect = new Rectangle(headerLeftLines[i].X1, cellTop, headerRightLines[i].X1 - headerLeftLines[i].X1, cellBottom - cellTop);

                            FieldData fieldData = GetFieldDataForCell(tableInfo, tableValue.Headers[i], cellRect);

                            if (string.IsNullOrWhiteSpace(fieldData.Value.Field.Text))
                            {
                                if (fieldData.Field.IsRequired)
                                {
                                    VBotLogger.Trace(() => $"[ExtractFieldValues] Unable to extract column {fieldData?.Field?.Name} cell R{j}C{i}.");
                                }
                                else
                                {
                                    fieldData.Value.Field.Text = fieldData.Field.DefaultValue;
                                }
                            }

                            tableValue.Rows[noOfRowInTableValue + j].Fields.Add(fieldData);
                        }
                    }
                }
                else
                {
                    VBotLogger.Trace(() => "[ExtractFieldValues] Unable to identify primary row values.");
                }
            }
            else
            {
                VBotLogger.Trace(() => "[ExtractFieldValues] Unable to identify table headers.");
            }
        }

        private void updateHeaderBoundForGivenFieldLayout(TableInfo tableInfo, FieldLayout colLayout, int tableRectHeight)
        {
            int columnIndex = GetColumnIndex(tableInfo.Headers, colLayout);

            if (columnIndex > -1)
            {
                Field header = tableInfo.Headers[columnIndex];

                if (isSelfValueMapped(colLayout))
                {
                    Rectangle colRect = GetColumnValueRect(tableInfo, colLayout, tableRectHeight);

                    int mostLeft = Math.Min(colRect.Left, header.Bounds.Left);
                    int mostRight = Math.Max(colRect.Right, header.Bounds.Right);

                    header.Bounds = new Rectangle(mostLeft
                        , header.Bounds.Top
                        , mostRight - mostLeft
                        , header.Bounds.Height);
                }
            }
        }

        private void updateHeaderBoundOfGivenTable(TableInfo tableInfo, TableValue tableValue, Field bottomIdentifiedField, bool isThisHeaderLessTable)
        {
            Rectangle tableRect = GetTableRect(tableInfo, tableInfo.Layout.PrimaryColumn, bottomIdentifiedField);

            foreach (var header in tableValue.Headers)
            {
                FieldLayout colLayout = GetColumnLayout(tableInfo, header.Id);

                if (!isThisHeaderLessTable)
                {
                    updateHeaderBoundForGivenFieldLayout(tableInfo, colLayout, tableRect.Height);
                }
            }
        }

        private FieldData GetFieldDataForCell(TableInfo tableInfo, FieldDef headerFieldDef, Rectangle cellRect)
        {
            StatisticsLogger.Log($"Extracting cell start : {headerFieldDef.Name}");
            FieldData fieldData = new FieldData();

            fieldData.Field = new FieldDef();
            fieldData.Field = headerFieldDef;

            StatisticsLogger.Log($"Extracting cell start ~ First Try");
            fieldData.Value = GetCellValue(tableInfo.Fields, cellRect);
            StatisticsLogger.Log($"Extracting cell end ~ First Try");

            if (_rescan != null)
            {
                Field valueField = _rescan.GetRescanValue(fieldData.Value.Field, fieldData.Field);
                if (valueField != null && !string.IsNullOrEmpty(valueField.Text))
                {
                    fieldData.Value.Field = valueField;
                }
            }

            StatisticsLogger.Log($"Extracting cell end : {headerFieldDef.Name}");

            return fieldData;
        }

        private static FieldLayout GetColumnLayout(TableInfo tableInfo, string colId)
        {
            FieldLayout colLayout = tableInfo.Layout.Columns.FirstOrDefault(col => col.FieldId == colId);

            if (colLayout == null)
            {
                colLayout = new FieldLayout();
            }

            return colLayout;
        }

        private static bool IsPrimaryColumnDefined(FieldLayout primaryColumnLayout)
        {
            return primaryColumnLayout != null && !string.IsNullOrWhiteSpace(primaryColumnLayout.Label);
        }

        private Field GetPrimaryColumnField(FieldLayout primaryColumnLayout, TableInfo tableInfo)
        {
            Field masterColField = null;

            List<Field> masterColFields = _fieldOperations.GetAllBestMatchingFields(tableInfo.Fields
                 , primaryColumnLayout.Label
                 , primaryColumnLayout.SimilarityFactor);

            if (masterColFields.Count > 0)
            {
                masterColField = masterColFields.OrderBy(
                   field =>
                       _rectOperations.GetCenterDistanceBetweenRects(field.Bounds
                           , primaryColumnLayout.Bounds))
                   .ToList()[0];
            }

            if (masterColField == null)
            {
                masterColField = _fieldOperations.FindBestMatchingFieldFromCombiningMultipleSIR(primaryColumnLayout.Label
                , primaryColumnLayout.SimilarityFactor
                , tableInfo.Fields
                , null);
            }

            if (masterColField != null)
            {
                if (!string.IsNullOrEmpty(masterColField.Text)
                    && !string.IsNullOrEmpty(primaryColumnLayout.Label)
                    && primaryColumnLayout.Label.Contains("|"))
                {
                    primaryColumnLayout.Label = masterColField.Text;
                }
                return masterColField;
            }

            return null;
        }

        private List<int> GetRowTops(List<Field> masterColumnRowValues
            , FieldLayout primaryColumnLayout
            , Rectangle tableRect
            , List<Line> lines)
        {
            List<int> rowTop = new List<int>();

            for (int i = 0; i < masterColumnRowValues.Count; i++)
            {

                var currentRowBounds = masterColumnRowValues[i].Bounds;
                int rowTopValue = currentRowBounds.Top;
                Rectangle previousRowRect = Rectangle.Empty;
                if (i == 0)
                {
                    previousRowRect = new Rectangle(currentRowBounds.Left
                        , tableRect.Top
                        , currentRowBounds.Width, 1);
                }
                else
                {
                    previousRowRect = masterColumnRowValues[i - 1].Bounds;
                }

                List<Line> foundLines = _rectOperations.FindAllHorizontalLinesInBetweenRects(previousRowRect
                      , currentRowBounds
                      , lines);

                if (foundLines.Count > 0)
                {
                    rowTopValue = foundLines.Max(line => line.Y1);
                }

                Line rowLine = GetRowLine(tableRect.Left
                    , tableRect.Right
                    , rowTopValue - 1);

                rowTop.Add(rowLine.Y1);
            }

            return rowTop;
        }

        private Rectangle GetColumnValueRect(TableInfo tableInfo, FieldLayout columnLayout, int columnHeight)
        {
            int columnIndex = GetColumnIndex(tableInfo.Headers, columnLayout);

            if (columnIndex < 0)
            {
                return new Rectangle();
            }

            Field columnHeader = tableInfo.Headers[columnIndex];
            Field nextColumnHeader = getNextColumnHeader(tableInfo, columnIndex);
            Field prevColumnHeader = getPreviousColumnHeader(tableInfo, columnIndex);

            Rectangle colRect = columnHeader.Bounds;

            colRect.Y = colRect.Top + colRect.Height + 5;
            colRect.Height = columnHeight - colRect.Height - 5;

            List<Field> colFields = new List<Field>();

            if (prevColumnHeader == null)
            {
                List<Field> allLeftFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - columnHeader.Bounds.Top) < columnHeader.Bounds.Height && fi.Bounds.Right < (columnHeader.Bounds.Left));

                //decide any field or line between header field and left side field and assign thier right value
                if (allLeftFields != null && allLeftFields.Count > 0)
                {
                    allLeftFields = allLeftFields.OrderByDescending(fi => fi.Bounds.Right).ToList();

                    prevColumnHeader = allLeftFields[0];
                }
            }

            int minLeft = getMinLeftForGivenHeaderUsingPreviousHeaderField(tableInfo, columnHeader, prevColumnHeader);

            if (nextColumnHeader == null)
            {
                //get right side fields

                List<Field> allRightFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - columnHeader.Bounds.Top) < columnHeader.Bounds.Height && fi.Bounds.Left > (columnHeader.Bounds.Left + columnHeader.Bounds.Width - 5));

                allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();


                if (allRightFields != null && allRightFields.Count > 0)
                {
                    allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();

                    nextColumnHeader = allRightFields[0];
                }
            }

            int maxRight = getMaxRightForGivenHeaderUsingNextHeaderField(tableInfo, columnHeader, nextColumnHeader);

            Rectangle tempColRect = colRect;

            do
            {
                tempColRect.Inflate(1, 0);

                colFields = _fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, tempColRect);
                List<Field> colFields_regions = new List<Field>();
                colFields_regions = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(tableInfo.Regions, tempColRect);
                if (colFields_regions.Count > 0)
                {
                    colFields.AddRange(colFields_regions);
                }

                if (colFields.Count > 0)
                {
                    tempColRect = getColumnRectFromIntersectedFields(tableInfo, colFields, tempColRect);

                    if (isValidColumnRect(nextColumnHeader, prevColumnHeader, minLeft, maxRight, tempColRect))
                    {
                        return tempColRect;
                    }
                }
            }
            while (tempColRect.Left > minLeft && tempColRect.Right < maxRight);

            return colRect;
        }

        private int getMaxRightForGivenHeaderUsingNextHeaderField(TableInfo tableInfo, Field columnHeader, Field nextColumnHeader)
        {
            int maxRight = 3 * columnHeader.Bounds.Right;



            if (nextColumnHeader != null)
            {
                int top = _rectOperations.GetMostTopOfTwoRectangle(nextColumnHeader.Bounds, columnHeader.Bounds);
                int bottom = _rectOperations.GetMostBottomOfTwoRectangle(nextColumnHeader.Bounds, columnHeader.Bounds);

                Rectangle emptySpaceRectBetweenHeader = new Rectangle(columnHeader.Bounds.Right + 5, top, nextColumnHeader.Bounds.Left - columnHeader.Bounds.Right - 5, bottom - top);

                List<Field> extraHeader = _fieldOperations.GetFieldsWithinBoundField(tableInfo.Fields, emptySpaceRectBetweenHeader);
                extraHeader.RemoveAll(field => field.Text.Length <= 1);

                if (extraHeader.Count > 0)
                {
                    Field mostRightField = extraHeader.OrderByDescending(field => field.Bounds.Left).ToList()[0];
                    maxRight = mostRightField.Bounds.Left;
                }
                else
                {
                    maxRight = nextColumnHeader.Bounds.Left;
                }
            }

            return maxRight;
        }

        private int getMinLeftForGivenHeaderUsingPreviousHeaderField(TableInfo tableInfo, Field columnHeader, Field prevColumnHeader)
        {
            int minLeft = 0;



            if (prevColumnHeader != null)
            {
                int top = _rectOperations.GetMostTopOfTwoRectangle(prevColumnHeader.Bounds, columnHeader.Bounds);
                int bottom = _rectOperations.GetMostBottomOfTwoRectangle(prevColumnHeader.Bounds, columnHeader.Bounds);

                Rectangle emptySpaceRectBetweenHeader = new Rectangle(prevColumnHeader.Bounds.Right + 5, top, columnHeader.Bounds.Left - prevColumnHeader.Bounds.Right - 5, bottom - top);

                List<Field> extraHeader = _fieldOperations.GetFieldsWithinBoundField(tableInfo.Fields, emptySpaceRectBetweenHeader);
                extraHeader.RemoveAll(field => field.Text.Length <= 1);

                if (extraHeader.Count > 0)
                {
                    Field mostRightField = extraHeader.OrderByDescending(field => field.Bounds.Right).ToList()[0];
                    minLeft = mostRightField.Bounds.Right;
                }
                else
                {
                    minLeft = prevColumnHeader.Bounds.Right;
                }
            }

            return minLeft;
        }

        private Rectangle getColumnRectFromIntersectedFields(TableInfo tableInfo, List<Field> colFields, Rectangle originalColRect)
        {
            Rectangle colRect = _fieldOperations.GetBoundsFromFields(colFields);

            colRect = new Rectangle(colRect.Left, originalColRect.Top, colRect.Width, originalColRect.Height);

            int nextCount = colFields.Count;
            int prevCount = 0;

            do
            {
                prevCount = nextCount;

                colFields = _fieldOperations.GetAllSystemFieldsIntersectingWithRect(tableInfo.Fields, colRect);

                Rectangle tempRect = _fieldOperations.GetBoundsFromFields(colFields);
                Rectangle mergedRect = new Rectangle(tempRect.Left, originalColRect.Top, tempRect.Width, originalColRect.Height);

                colRect = mergedRect;
                nextCount = colFields.Count;
            }
            while (nextCount > prevCount);

            return colRect;
        }

        private int getTableBottomFromFields(TableInfo tableInfo, int masterColumnIndex, TableValue tableValue, bool isThisHeaderLessTable)
        {
            int maxBottom = tableInfo.Fields.Max(x => x.Bounds.Bottom);

            int tableBottom = tableInfo.Headers[masterColumnIndex].Bounds.Bottom;

            Field prevRowField = tableInfo.Headers[masterColumnIndex].Clone();

            int maxLeft = prevRowField.Bounds.Left;
            int maxRight = prevRowField.Bounds.Right;
            //get left side line
            List<Field> allLeftFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - prevRowField.Bounds.Top) < prevRowField.Bounds.Height && fi.Bounds.Right < (prevRowField.Bounds.Left));

            //decide any field or line between header field and left side field and assign thier right value
            if (allLeftFields != null && allLeftFields.Count > 0)
            {
                allLeftFields = allLeftFields.OrderByDescending(fi => fi.Bounds.Right).ToList();

                var leftSideField = allLeftFields[0];
                List<Line> foundLines = _rectOperations.FindAllVerticalLinesInBetweenRects(prevRowField.Bounds
                        , leftSideField.Bounds
                        , tableInfo.Lines);

                //decide min left
                if (foundLines.Count > 0)
                {
                    maxLeft = foundLines.OrderByDescending(x => x.X2).ToList()[0].X2;
                }
                else
                {
                    maxLeft = leftSideField.Bounds.Right;
                }
            }
            //get right side line

            List<Field> allRightFields = tableInfo.Fields.FindAll(fi => Math.Abs(fi.Bounds.Top - prevRowField.Bounds.Top) < prevRowField.Bounds.Height && fi.Bounds.Left > (prevRowField.Bounds.Left + prevRowField.Bounds.Width - 5));

            allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();

            //decide any field or line between header field and right side field and assign thier left value
            if (allRightFields != null && allRightFields.Count > 0)
            {
                allRightFields = allRightFields.OrderBy(fi => fi.Bounds.Left).ToList();

                var rightSideField = allRightFields[0];
                List<Line> foundLinesRight = _rectOperations.FindAllVerticalLinesInBetweenRects(prevRowField.Bounds
                        , rightSideField.Bounds
                        , tableInfo.Lines);

                //decide max right
                if (foundLinesRight.Count > 0)
                {
                    maxRight = foundLinesRight.OrderBy(x => x.X1).ToList()[0].X1;
                }
                else
                {
                    maxRight = rightSideField.Bounds.Left;
                }
            }
            prevRowField.Bounds = new Rectangle(maxLeft, prevRowField.Bounds.Top, maxRight - maxLeft, prevRowField.Bounds.Height);

            Field nextRowField = _fieldOperations.GetNearestBottomField(prevRowField, tableInfo.Fields, 5);

            while (nextRowField != null)
            {
                updateHeaderBoundOfGivenTable(tableInfo, tableValue, nextRowField, isThisHeaderLessTable);

                tableBottom = nextRowField.Bounds.Bottom;

                prevRowField = nextRowField;
                nextRowField = _fieldOperations.GetNearestBottomField(prevRowField, tableInfo.Fields, 10);

                if (nextRowField == null)
                {
                    const int stepToMoveDown = 10;
                    int currentIncrementedStep = 0;
                    do
                    {
                        currentIncrementedStep += stepToMoveDown;
                        nextRowField = _fieldOperations.GetNearestBottomField(prevRowField, tableInfo.Fields, currentIncrementedStep);
                    } while (nextRowField == null && (prevRowField.Bounds.Bottom + currentIncrementedStep) < maxBottom);

                    if (nextRowField != null)
                    {
                        int top = nextRowField.Bounds.Top;

                        /**
                         * Below Line is For number of Column value need to qualify possibility of row/table present So  it is based on all column.
                         * Currently Code does not care whether header (table column) is  optional or not.
                         * TODO: It is left to be decided what modification need to be made for optional column.
                         */
                        int minimumNumberOfColumnNeededToDecidePossibiltyOfTable = (int)((tableInfo.Headers.Count / 2) + 1);
                        int numberOfCellNotIntersected = 0;
                        foreach (Field header in tableInfo.Headers)
                        {
                            var rectLeft = new Rectangle(header.Bounds.X - 1, top - 2, 1, header.Bounds.Height);
                            var rectRight = new Rectangle(header.Bounds.X + header.Bounds.Width + 1, top - 2, 1, header.Bounds.Height);
                            if (!(_fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, rectLeft).Count > 0 || _fieldOperations.GetFieldsIntersectingWithRect(tableInfo.Fields, rectRight).Count > 0))
                            {
                                numberOfCellNotIntersected++;
                            }
                        }
                        if (numberOfCellNotIntersected > minimumNumberOfColumnNeededToDecidePossibiltyOfTable)
                        {
                            bool isNotEmptySpace = false;
                            int numberOfCellContainData = 0;
                            foreach (Field header in tableInfo.Headers)
                            {
                                Rectangle rect = new Rectangle(header.Bounds.X, top, header.Bounds.Width, header.Bounds.Height);
                                if (_fieldOperations.GetFieldsWithinBoundField(tableInfo.Fields, rect).Count > 0)
                                {
                                    numberOfCellContainData++;
                                    if (numberOfCellContainData >= minimumNumberOfColumnNeededToDecidePossibiltyOfTable)
                                    {
                                        isNotEmptySpace = true;
                                        break;
                                    }
                                }
                            }
                            if (!isNotEmptySpace)
                            {
                                nextRowField = null;
                            }
                        }
                        else
                        {
                            nextRowField = null;
                        }
                    }
                }
            }

            return tableBottom;
        }

        private int getTableBottomFromFooter(TableInfo tableInfo, int tableRowStartPosition)
        {
            int tableBottom = 0;

            if (string.IsNullOrWhiteSpace(tableInfo.Layout.Footer.Label))
            {
                return tableBottom;
            }

            List<Field> footerFields = _fieldOperations.GetAllBestMatchingFields(tableInfo.Fields
                , tableInfo.Layout.Footer.Label
                , tableInfo.Layout.Footer.SimilarityFactor);

            footerFields.RemoveAll(field => field.Bounds.Top <= tableRowStartPosition);

            Field bottomField = null;

            if (footerFields.Count > 0)
            {
                footerFields = footerFields.OrderBy(field => field.Bounds.Top).ToList();
                bottomField = footerFields[0];
            }

            if (bottomField != null)
            {
                tableBottom = bottomField.Bounds.Top - 10;
            }
            else
            {
                bottomField = tableInfo.Fields.FirstOrDefault(field => field.Bounds.Top > tableRowStartPosition &&
                                                            field.Text.ToLower().StartsWith(tableInfo.Layout.Footer.Label.ToLower()));

                if (bottomField != null)
                {
                    tableBottom = bottomField.Bounds.Top - 10;
                }
            }

            return tableBottom;
        }

        public int GetColumnIndex(List<Field> headers, FieldLayout columnLayout)
        {
            if (string.IsNullOrEmpty(columnLayout.Label))
            {
                return -1;
            }

            List<Field> headerFields = headers.FindAll(field => field.Text.PartiallyEquals(columnLayout.Label, columnLayout.SimilarityFactor));
            headerFields = headerFields.OrderByDescending(field => field.Text.GetSimilarity(columnLayout.Label)).ToList();

            if (headerFields.Count > 0)
            {
                return headers.FindIndex(field => field.Text.Equals(headerFields[0].Text));
            }
            else
            {
                return -1;
            }
        }

        public List<Field> GetTableHeaders(TableInfo tableInfo, Field primaryColField)
        {
            List<Field> tableHeaders = new List<Field>();

            int headerBottom = 0;

            foreach (FieldLayout fieldLayout in tableInfo.Layout.Columns)
            {
                Field columnField = getColumnFieldFromLayout(fieldLayout, primaryColField, tableInfo.Fields);

                if (columnField.Text.Length > 1)
                {
                    if (columnField.Bounds.Bottom > headerBottom)
                    {
                        headerBottom = columnField.Bounds.Bottom;
                    }

                    fieldLayout.Bounds = columnField.Bounds;
                    if ((!isSelfValueMapped(fieldLayout))
                    && fieldLayout.Id != tableInfo.Layout.PrimaryColumn.Id)
                    {
                        // columnField.Bounds = _fieldOperations.FindRelativeBound(fieldLayout);
                        columnField.Bounds = new Rectangle(fieldLayout.Bounds.Left
                                 + fieldLayout.ValueBounds.Left
                                 //, primaryColField.Bounds.Top + primaryColumnLayout.ValueBounds.Top
                                 , fieldLayout.Bounds.Top
                                 , fieldLayout.ValueBounds.Width
                                 , fieldLayout.ValueBounds.Height);
                    }

                    tableHeaders.Add(columnField);
                }
            }

            for (int i = 0; i < tableHeaders.Count; i++)
            {
                Rectangle headerRect = tableHeaders[i].Bounds;
                headerRect.Height = headerBottom - headerRect.Top;

                tableHeaders[i].Bounds = headerRect;
            }

            tableHeaders = tableHeaders.OrderBy(field => field.Bounds.Left).ToList();

            return tableHeaders;
        }

        private bool isSelfValueMapped(FieldLayout fieldLayout)
        {
            return fieldLayout.IsValueBoundAuto || (0 == fieldLayout.ValueBounds.X
                 && 0 == fieldLayout.ValueBounds.Y
                 && Math.Abs(fieldLayout.Bounds.Width - fieldLayout.ValueBounds.Width) <= 2
                 && Math.Abs(fieldLayout.Bounds.Height - fieldLayout.ValueBounds.Height) <= 2) || fieldLayout.ValueBounds == Rectangle.Empty;
        }

        private Field getColumnFieldFromLayout(FieldLayout columnLayout, Field primaryColField, List<Field> fields)
        {
            List<Field> similarColFields = _fieldOperations.GetAllBestMatchingFields(fields, columnLayout.Label, columnLayout.SimilarityFactor);
            Field columnField = new Field();

            if (similarColFields.Count > 0)
            {
                Field colFieldAtSameHeight = similarColFields.FirstOrDefault(field => _rectOperations.GetYDistanceBetweenRects(field.Bounds, primaryColField.Bounds) < primaryColField.Bounds.Height);

                if (colFieldAtSameHeight != null)
                {
                    columnField = colFieldAtSameHeight;
                }
            }

            if (string.IsNullOrEmpty(columnField.Text) || columnField.Text.Length <= 1)
            {
                columnField = _fieldOperations
                    .FindBestMatchingFieldFromCombiningMultipleSIR(columnLayout.Label
                        , columnLayout.SimilarityFactor
                        , fields, primaryColField);
            }

            if (!string.IsNullOrEmpty(columnField.Text) && !string.IsNullOrEmpty(columnLayout.Label) && columnLayout.Label.Contains("|"))
            {
                columnLayout.Label = columnField.Text;
            }

            return columnField;
        }

        private Point rotatePoint(Point pointToRotate, Point centerPoint, double angleInDegrees)
        {
            double angleInRadians = Math.Atan(angleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new Point
            {
                X =
                    (int)
                    (cosTheta * (pointToRotate.X - centerPoint.X) -
                    sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y =
                    (int)
                    (sinTheta * (pointToRotate.X - centerPoint.X) +
                    cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
        }
    }
}