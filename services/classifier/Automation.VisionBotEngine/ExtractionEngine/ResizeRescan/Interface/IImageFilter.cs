﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Drawing;

namespace Automation.VisionBotEngine.ResizeRescan.Interface
{
    interface IImageFilter
    {
        Bitmap Apply(System.Drawing.Image input, IList<IImageFilter> chainedFilters);
    }
}
