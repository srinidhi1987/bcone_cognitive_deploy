﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Drawing;

using AForge.Imaging.Filters;
using Automation.VisionBotEngine.ResizeRescan.Interface;

namespace Automation.VisionBotEngine.ResizeRescan
{
    class ResizeImageFilter : IImageFilter
    {
        double _resizeFactor;

        public ResizeImageFilter(double resizeFactor)
        {
            _resizeFactor = resizeFactor;
        }

        //TODO: Implementation is pending for chainedFilters
        public Bitmap Apply(System.Drawing.Image input, IList<IImageFilter> chainedFilters)
        {
            Bitmap reSizedImage = input as Bitmap;
            ResizeBilinear filter = new ResizeBilinear((int)(reSizedImage.Width * _resizeFactor), (int)(reSizedImage.Height * _resizeFactor));
            //   apply the filter
            reSizedImage = filter.Apply(reSizedImage);

            return reSizedImage;
        }
    }
}
