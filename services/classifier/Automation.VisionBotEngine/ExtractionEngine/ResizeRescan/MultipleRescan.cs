﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.ResizeRescan
{
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.OcrEngine;
    using Automation.VisionBotEngine.OcrEngine.Tesseract;
    using Automation.VisionBotEngine.ResizeRescan.Interface;
    using Automation.VisionBotEngine.Validation;
    using Cognitive.VisionBotEngine;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    internal class MultipleRescan : IRescan
    {
        private IOcrEngine _ocrEngine;
        private IVisionBotPageImageProvider _pageImageProvider;


        public MultipleRescan(IOcrEngine ocrEngine
            , IVisionBotPageImageProvider pageImageProvider)
        {
            _ocrEngine = ocrEngine;
            _pageImageProvider = pageImageProvider;
        }

        public Field GetRescanValue(Field fieldToRescan, FieldDef fieldDef)
        {
            List<CroppedImage> cropImageList = new List<CroppedImage>();
            CroppedImage cropImage = null;
            string imagePath = null;
            try
            {
                VBotLogger.Trace(() => string.Format("Starting ACL1.."));
                fieldToRescan = fieldToRescan.Clone();
                /* if (fieldDef.ValueType == FieldValueType.Text && fieldToRescan.Confidence < 85)
                 {
                     cropImage = _pageImageProvider.GetCroppedImageFromProcessDocument(fieldToRescan.Bounds.X, fieldToRescan.Bounds.Y, fieldToRescan.Bounds.Width, fieldToRescan.Bounds.Height);

                     cropImageList.Add(cropImage);
                     imagePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                     cropImage.GetCroppedImage().Save(imagePath);

                     //bool result = PatternRecognizer.ResizeImage(imagePath, 3);

                     //string resultFieldText = "";
                     //Field resultField = fieldToRescan;
                     //List<Field> scannedFields = GetScannedFields(imagePath);
                     //if (scannedFields.Count > 0)
                     //{
                     //    for (int i = 0; i < scannedFields.Count; i++)
                     //    {
                     //        //Append the text from all scanned fields into one single text
                     //        if (i < scannedFields.Count - 1)
                     //            resultFieldText += scannedFields[i].Text + " ";
                     //        else
                     //            resultFieldText += scannedFields[i].Text;

                     //        VBotLogger.Debug(() => "[MultipleRescan::GetRescanValue], SecondPass, Field Def Name: " + fieldDef.Name + " with Text value: " + scannedFields[i].Text + " with Confidence: " + scannedFields[i].Confidence);
                     //    }
                     //    resultField.Text = resultFieldText;
                     //    return resultField;
                     //}
                     //else
                     //{
                     //    return fieldToRescan;
                     //}
                 }
                 else */
                if ((fieldDef.ValueType == FieldValueType.Date || fieldDef.ValueType == FieldValueType.Number) && !string.IsNullOrEmpty(fieldToRescan.Text) && fieldToRescan.Confidence < 90)
                {
                    StatisticsLogger.Log($"Extracting Field start ~ Retry");
                    cropImage = _pageImageProvider.GetCroppedImageFromProcessDocument(fieldToRescan.Bounds.X, fieldToRescan.Bounds.Y, fieldToRescan.Bounds.Width, fieldToRescan.Bounds.Height);
                    //Decide resize factor
                    double resizeFactor = CalculateResizeFactor(fieldToRescan.Bounds.Width, fieldToRescan.Bounds.Height);
                    VBotLogger.Trace(() => string.Format("ACL1: Resize factor for {0} field type with first pass value {1} is {2} with tesseract confidence {3}", fieldDef.ValueType, fieldToRescan.Text, resizeFactor, fieldToRescan.Confidence));
                    //Loop through and prepare multi resized images
                    cropImageList = ConstructMultipleResizedImages(cropImage, resizeFactor);
                    ImageOperations imageOperation = new ImageOperations();
                    //Merge resized images and create single image
                    imagePath = imageOperation.MergeImagesintoSingleBitmap(cropImageList, resizeFactor);

                    //Scan that single image and return Field object

                    List<Field> scannedFields = GetScannedFields(imagePath);
                    if (scannedFields.Count > 0)
                    {
                        scannedFields = new SIROperations().MergeSegmentedFields(scannedFields, new List<Line>());
                        //Find the best field from the scanned fields
                        string resultFieldText = GetBestFieldFromScannedFields(scannedFields, fieldDef, cropImageList, fieldToRescan.Text);
                        Field resultField = fieldToRescan;
                        resultField.Text = resultFieldText;
                        VBotLogger.Trace(() => string.Format("ACL1: Field value after second pass is {0}", resultField.Text));
                        StatisticsLogger.Log($"Extracting Field end ~ Retry");
                        return resultField;
                    }
                    else
                    {
                        VBotLogger.Trace(() => string.Format("ACL1: Field value after second pass is {0}", fieldToRescan.Text));
                        StatisticsLogger.Log($"Extracting Field end ~ Retry");
                        return fieldToRescan;
                    }
                }
                VBotLogger.Trace(() => string.Format("Ending ACL1.."));
            }
            catch (Exception ex)
            {
                StatisticsLogger.Log($"Extracting Field start ~ Retry");
                VBotLogger.Warning(() => "[GetRescanValue] Unable to do second pass rescan and taking the default value");
            }
            finally
            {
                if (cropImage != null)
                    cropImage.Dispose();
                for (int i = 0; i < cropImageList.Count; i++)
                    cropImageList[i].Dispose();
                if (imagePath != null && System.IO.File.Exists(imagePath))
                    System.IO.File.Delete(imagePath);
            }
            return fieldToRescan;
        }

        private string GetBestFieldFromScannedFields(List<Field> scannedFields, FieldDef fieldDef, List<CroppedImage> cropImageList, string firstPassValue)
        {
            Dictionary<string, int> frequencies = new Dictionary<string, int>();
            DateTypeParser dp = new DateTypeParser();
            NumberTypeParser np = new NumberTypeParser();

            for (int k = 0; k < cropImageList.Count; k++)
            {
                CroppedImage croppedImage = cropImageList[k];
                List<Field> temp = new FieldOperations().GetFieldsWithinBoundField(scannedFields, croppedImage.DestinationImageBound);
                Field valueFieldSecondPassTemp = new Field();
                if (temp.Count > 0)
                {
                    valueFieldSecondPassTemp = new FieldOperations().GetSingleFieldFromFields(temp);
                    string scannedText = valueFieldSecondPassTemp.Text;
                    if ((fieldDef.ValueType == FieldValueType.Date && dp.Parse(scannedText) != null)
                        || (fieldDef.ValueType == FieldValueType.Number && np.Parse(scannedText) != null) || (fieldDef.ValueType == FieldValueType.Text))
                    {
                        if (frequencies.ContainsKey(scannedText))
                        {
                            frequencies[scannedText] = frequencies[scannedText] + 1;
                        }
                        else
                        {
                            frequencies.Add(scannedText, 1);
                        }
                    }
                }
            }

            if (frequencies.ContainsKey(firstPassValue))
            {
                frequencies[firstPassValue] = frequencies[firstPassValue] + 1;
            }
            else
            {
                frequencies.Add(firstPassValue, 1);
            }

            if (fieldDef.ValueType == FieldValueType.Number)
            {
                var DotContainedValue = frequencies.Keys.FirstOrDefault(x => x.Contains("."));

                if (!string.IsNullOrWhiteSpace(DotContainedValue))
                {
                    var keys = frequencies.Keys.ToList();
                    foreach (string key in keys)
                    {
                        if (!key.Contains("."))
                            frequencies.Remove(key);
                    }
                }
            }

            var orderedDict = frequencies.OrderByDescending(p => p.Value);
            var SameCountDict = orderedDict.Where(x => x.Value == orderedDict.ElementAt(0).Value);
            KeyValuePair<string, int> freqKeyVal = SameCountDict.FirstOrDefault(x => x.Key == firstPassValue);
            if (freqKeyVal.Key == null)
            {
                freqKeyVal = SameCountDict.OrderByDescending(x => x.Key.GetSimilarity(firstPassValue)).FirstOrDefault();
            }
            string resultField = "";
            if (freqKeyVal.Key != null)
            {
                resultField = freqKeyVal.Key.ToString();
            }
            else
            {
                resultField = orderedDict.ElementAt(0).Key.ToString();
            }

            VBotLogger.Debug(() => "[MultipleRescan::GetBestFieldFromScannedFields] obj key " + resultField);
            return resultField;
        }

        private List<CroppedImage> ConstructMultipleResizedImages(CroppedImage croppedImage, double resizeFactorValue)
        {
            List<CroppedImage> _cropImageList = new List<CroppedImage>();
            double fromResizeFactorValue = 1;

            for (int i = 0; i < (int)resizeFactorValue * 2 - 1; i++)
            {
                IImageFilter resizeFilter = new ResizeImageFilter(fromResizeFactorValue);
                Bitmap resizedImage = resizeFilter.Apply(croppedImage.GetCroppedImage(), null);

                CroppedImage ci = new CroppedImage(resizedImage, new Rectangle(0, 0, resizedImage.Width, resizedImage.Height));

                _cropImageList.Add(ci);

                fromResizeFactorValue = fromResizeFactorValue + 0.5;
            }
            return _cropImageList;
        }

        private List<Field> GetScannedFields(string imagePath)
        {
            List<Field> scannedFields = new List<Field>();

            SegmentedDocument segDoc = null;

            //TODO:5.1 remove hardcoded language
            segDoc = _ocrEngine.ExtractRegions(new ImageDetails(imagePath, "eng"), Constants.SearchString);

            for (int i = 0; i < segDoc.Regions.Count; i++)
            {
                scannedFields.Add(VBotDocumentGenerator.CreateFieldFromRegion(segDoc.Regions[i]));
            }

            return scannedFields;
        }

        private float CalculateResizeFactor(int originalWidth, int originalHeight)
        {
            float maxWidth = 543;
            float maxHeight = 150;
            float ratio = 1;
            if (originalWidth > 0 && originalHeight > 0)
            {
                // float ratioX = maxWidth / (float)originalWidth;
                float ratioY = maxHeight / (float)originalHeight;
                ratio = ratioY;// Math.Min(ratioX, ratioY);
            }
            return ratio;
        }
    }
}