﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.DocumentDataEngine
{
    interface IDocumentEngine
    {
        int GetPageCount(string documnetID);

        SegmentedDocument GetData(string documnetID);

        DocumentProperties GetMetaData(string documnetID);

        List<Bitmap> GetPageImage(string documnetID, int fromPageIndex = 0, int toPageIndex = 0);
    }
}
