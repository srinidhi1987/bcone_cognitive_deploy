﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
namespace Automation.VisionBotEngine
{
    using AForge.Imaging.Filters;
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Model;
    using Cognitive.VisionBotEngine.Common;
    using Configuration;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;

    internal static class PatternRecognizer
    {
        private const string DllName = @"Automation.PatternRecognition.dll";

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int MatchAndRecognizeCheckBox(string imagePath, int x, int y, int width, int height);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool ResizeImage(string imagePath, int ResizeRatio);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void RemoveLines(string imagePath);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ApplyPreprocessingForRotation(string cSrcImage);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool RemoveSpeckleNoise(string imagePath);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int PredictSpeckleNoiseInDocument(string imagePath);
    }

    internal interface IFieldOperations
    {
        List<Field> GetFieldsWithinRect(List<Field> allPagesField, Rectangle rect);
        List<Field> GetAllSystemFieldsWithinRect(List<Field> fields, Rectangle rect);

        Rectangle GetBoundsFromFields(List<Field> actualFooterFields);

        List<Field> GetFieldsIntersectingWithRect(List<Field> possibleFooterField, Rectangle rectangle);
        List<Field> GetAllSystemFieldsIntersectingWithRect(List<Field> fields, Rectangle colRect);

        List<Field> GetAllBestMatchingFieldsForClassification(List<Field> fields, string fieldText, double minSimilarity);

        List<Field> GetFieldsWithinBoundField(List<Field> fields, Rectangle rect);

        List<Field> GetAllBestMatchingFields(List<Field> fields, string label, double similarityFactor);

        Field FindBestMatchingFieldFromCombiningMultipleSIR(string labels
            , double similarityFactor
            , List<Field> fields
            , Field referenceField, List<Line> lines = null);

        string GetTextFromFields(List<Field> cellfields);

        Field GetNearestBottomField(Field prevRowField, List<Field> fields, int v);

        Rectangle FindRelativeBound(FieldLayout fieldLayout);

        Field GetSingleFieldFromFields(List<Field> list);
        List<Field> GetFieldsWithinBoundFieldByOverlapping(List<Field> fields, Rectangle cellRect, float overlapRatio);
    }

    // TODO : need interface
    internal class FieldOperations
    : IFieldOperations
    {
        private const int BottomValueHeightOffsetFactor = 3;
        private const int MaxDistanceOffset = 30;
        private const int MaxAlignementOffset = 100;

        private const double MultilineValueHeightOffsetFactor = 1.25;
        private const double MultilineValueWidthOffsetFactor = 1.25;
        private const double HeightInflate = 0.25f;

        private IRectangleOperations _rectOperations;

        public FieldOperations()
            : this(new RectangleOperations())
        {
        }

        public FieldOperations(IRectangleOperations rectangleOperations)
        {
            _rectOperations = rectangleOperations;
        }

        public List<Field> GetAllBestMatchingFieldsForClassification(List<Field> fields
          , string fieldText, double minSimilarity)
        {
            List<Field> allMatchingFields = new List<Field>();

            if (string.IsNullOrWhiteSpace(fieldText) || minSimilarity <= 0.0f)
            {
                return allMatchingFields;
            }

            List<string> labels = GetLabels(fieldText);
            foreach (var label in labels)
            {
                allMatchingFields = fields.FindAll(field
                    => field.Text.GetSimilarity(label) >= minSimilarity);

                if (allMatchingFields.Count > 0)
                {
                    break;
                }

                var lowerCaseLabel = label.ToLower();
                allMatchingFields = fields.FindAll(field
                    => field.Text.ToLower()
                    .GetSimilarity(lowerCaseLabel) >= minSimilarity);

                if (allMatchingFields.Count > 0)
                {
                    break;
                }
            }

            if (allMatchingFields.Count == 0)
            {
                allMatchingFields = fields.FindAll(field
                    => field.Text.StartsWith(fieldText
                    , StringComparison.OrdinalIgnoreCase));
            }

            if (allMatchingFields.Count == 0)
            {
                foreach (var label in labels)
                {
                    allMatchingFields = fields.FindAll(field
                        => field.Text.GetSimilarity(label) >= minSimilarity);

                    if (allMatchingFields.Count > 0) break;
                }
            }

            allMatchingFields = allMatchingFields.OrderByDescending(field
                => field.Text.GetSimilarity(fieldText)).ToList();

            foreach (Field field in allMatchingFields)
            {
                field.Confidence = (int)(field.Text.GetSimilarity(fieldText) * 100);
            }
            return allMatchingFields;
        }

        public List<Field> GetAllBestMatchingFields(List<Field> fields
            , string fieldText
            , double minSimilarity)
        {
            VBotLogger.Trace(
                           () => $"[GetAllBestMatchingFields] labels: {fieldText} similarity: {minSimilarity}");

            List<Field> allMatchingFields = new List<Field>();

            bool isThereAnyInvalidParameter = string.IsNullOrWhiteSpace(fieldText)
                || minSimilarity <= 0.0f
                || fields == null
                || fields.Count == 0;

            if (isThereAnyInvalidParameter)
            {
                return allMatchingFields;
            }

            List<string> labels = GetLabels(fieldText);

            foreach (var label in labels)
            {
                var lowerCaseLabel = label.ToLower();
                allMatchingFields = fields.FindAll(
                    field =>
                        field.Text.ToLower()
                            .GetSimilarity(lowerCaseLabel) >= 1.0);

                if (allMatchingFields.Count > 0)
                {
                    VBotLogger.Trace(
                          () => $"[GetAllBestMatchingFields] found {allMatchingFields.Count} fields which has label '{label} using 100% similarity'");
                    break;
                }
            }

            if (allMatchingFields.Count == 0)
            {
                foreach (var label in labels)
                {
                    allMatchingFields =
                        fields.FindAll(
                            field =>
                                field.Text
                                    .StartsWith(label.Trim(), StringComparison.OrdinalIgnoreCase));

                    if (allMatchingFields.Count > 0)
                    {
                        VBotLogger.Trace(
                         () => $"[GetAllBestMatchingFields] found {allMatchingFields.Count} fields which has label '{label}' using start with technique.");
                        break;
                    }
                }
            }

            if (allMatchingFields.Count == 0)
            {
                foreach (var label in labels)
                {
                    allMatchingFields = fields.FindAll(
                        field =>
                            field.Text
                                .GetSimilarity(label) >= minSimilarity);

                    if (allMatchingFields.Count > 0)
                    {
                        VBotLogger.Trace(
                         () => $"[GetAllBestMatchingFields] found {allMatchingFields.Count} fields which has label '{label}' using mimimum similarity.");
                        break;
                    }
                }
            }

            allMatchingFields = allMatchingFields.OrderByDescending(field
                => field.Text.GetSimilarity(fieldText))
                .ToList();

            return allMatchingFields;
        }

        public Field FindBestMatchingFieldFromCombiningMultipleSIR(string labels
            , double similarityFactor
            , List<Field> fields
            , Field referenceField, List<Line> lines = null)
        {
            Field colField = new Field();

            string[] splittedLabels = GetLabels(labels).ToArray();

            foreach (string lable in splittedLabels)
            {
                string[] words = getWords(lable);
                string searchLable = string.Empty;

                if (words.Length > 0)
                {

                    List<Field> allMergedFields = getAllMatchingFields(similarityFactor
                        , fields, words[0]);

                   List<Field> allFields = new List<Field>(fields);

                    foreach (var item in allMergedFields)
                    {
                        allFields.Remove(item);
                    }

                    for (int index = 1; index < words.Length; index++)
                    {

                        List<Field> tempAllMergedFields = new List<Field>(allMergedFields);
                        for (int i = 0; i < tempAllMergedFields.Count; i++)
                        {
                            Field colTextField = tempAllMergedFields[i];

                            List<Field> combinedFields = getCombinedFieldsWithGivenFieldWhichMatchWithGivenText(colTextField, allFields, words[index], similarityFactor, lines);

                            if (combinedFields.Count > 0)
                            {
                                allMergedFields.InsertRange(0, combinedFields);
                                allMergedFields.Remove(colTextField);
                            }
                        }
                    }

                    allMergedFields = allMergedFields.OrderByDescending(elememt
                        => elememt.Text.GetSimilarity(lable))
                        .ToList();

                    for (int i = 0; i < allMergedFields.Count; i++)
                    {
                        if (allMergedFields[i].Text.PartiallyEquals(lable, similarityFactor) &&
                                    (referenceField == null
                                    || referenceField.Bounds
                                    .IsHorizonatallyAlignedWithReferenceRect(allMergedFields[i].Bounds)))
                        {
                            return allMergedFields[i];
                        }
                    }
                }
            }
            return new Field();
        }

        private static List<Field> getAllMatchingFields(double similarityFactor, List<Field> fields, string word)
        {
            string processedWord = word?.Trim().ToLower();
            List<Field> allMatchingFields =
               fields.FindAll(
                   field =>
                   {
                       string text = field.Text.ToLower();
                       return (text.GetSimilarity(processedWord) >= similarityFactor ||
                         text.StartsWith(processedWord, StringComparison.OrdinalIgnoreCase));
                   });
            return allMatchingFields;
        }

        private static string[] getWords(string lable)
        {
            return lable.Split(new char[] { ' ' });
        }

        public Field GetColumnFieldForMultilineLayout(FieldLayout fieldLayout, List<Field> fields)
        {
            string labels = fieldLayout.Label;
            double similarityFactor = fieldLayout.SimilarityFactor;

            string[] splittedLabels = GetLabels(labels).ToArray();
            return FindBestMatchingFieldFromCombiningMultipleSIR(labels, similarityFactor, fields, null);
        }

        public List<Field> getCombinedFieldsWithGivenFieldWhichMatchWithGivenText(Field field
            , List<Field> tempFields
            , string text, double similarity
            , List<Line> lines = null)
        {
            Rectangle colRect = new Rectangle(field.Bounds.Left, field.Bounds.Top, field.Bounds.Width + 50, field.Bounds.Height);
            List<Field> horizontallyfieldsStorage = GetFieldsIntersectingWithRect(tempFields, colRect);
            horizontallyfieldsStorage = horizontallyfieldsStorage.FindAll(element => element.Text.StartsWith(text)
            && (!(new RectangleOperations()).AreRectsSeperatedByAnyVerticalLine(field.Bounds, element.Bounds, lines)));

            List<Field> verticallyfieldsStorage = new List<Field>();
            List<Field> mergedFieldStorage = new List<Field>();
            foreach (Field elemnt in horizontallyfieldsStorage)
            {
                tempFields.Remove(elemnt);
            }

            for (int j = 2; j < 5; j++)
            {
                colRect = new Rectangle(field.Bounds.Left, field.Bounds.Top, field.Bounds.Width, field.Bounds.Height * j);
                List<Field> verticallyfields = GetFieldsIntersectingWithRect(tempFields, colRect);
                verticallyfields = verticallyfields.FindAll(element => element.Text.StartsWith(text)
                && (!(new RectangleOperations()).AreRectsSeperatedByAnyHorizontalLine(field.Bounds, element.Bounds, lines)));

                foreach (Field elemnt in verticallyfields)
                {
                    tempFields.Remove(elemnt);
                }
                verticallyfieldsStorage.AddRange(verticallyfields);
            }

            foreach (Field elemnt in horizontallyfieldsStorage)
            {
                mergedFieldStorage.Add(GetSingleFieldFromFields(new List<Field>() { field, elemnt }));
            }

            foreach (Field elemnt in verticallyfieldsStorage)
            {
                mergedFieldStorage.Add(GetSingleFieldFromFields(new List<Field>() { field, elemnt }));
            }
            return mergedFieldStorage;
        }

        private List<string> GetLabels(string fieldText)
        {
            if (string.IsNullOrEmpty(fieldText))
                return new List<string>();

            string[] lables = fieldText.Split('|');

            for (int i = 0; i < lables.Length; i++)
            {
                lables[i] = lables[i].Trim();
            }

            return lables.OrderByDescending(l => l.Length).ToList();
        }

        public Field GetBestMatchingField(SIRFields sirFields, FieldLayout fieldLayout)
        {
            List<Field> allMatchingFields = GetAllBestMatchingFields(sirFields.Fields, fieldLayout.Label, fieldLayout.SimilarityFactor);

            if (allMatchingFields.Count > 0)
            {
                var firstField = allMatchingFields[0];
                allMatchingFields =
                    allMatchingFields.FindAll(
                        fi =>
                            fi.Text.GetSimilarity(fieldLayout.Label)
                                .Equals(firstField.Text.GetSimilarity(fieldLayout.Label)));
                Field field = GetNearestField(fieldLayout.Bounds, allMatchingFields);
                return field;
            }
            else
                return null;
        }

        public Field GetBestMatchingField(SIRFields sirFields, FieldLayout fieldLayout,
            LayoutConfidence layoutConfidence)
        {
            Field userField = GetUserDefineField(sirFields, fieldLayout.Clone(), layoutConfidence);

            if (userField == null)
            {
                List<Field> allMatchingFields;

                allMatchingFields = GetAllBestMatchingFields(sirFields.Fields, fieldLayout.Label,
                   fieldLayout.SimilarityFactor);

                if (allMatchingFields.Count > 0)
                {
                    var firstField = allMatchingFields[0];

                    allMatchingFields =
                        allMatchingFields.FindAll(
                            fi =>
                                fi.Text.GetSimilarity(fieldLayout.Label)
                                    .Equals(firstField.Text.GetSimilarity(fieldLayout.Label)));

                    Field field = GetNearestField(fieldLayout.Bounds, allMatchingFields);
                    return field;
                }
                else
                    return null;
            }
            else
                return userField;
        }

        private Field GetUserDefineField(SIRFields sirFields, FieldLayout fieldLayout, LayoutConfidence layoutConfidence)
        {
            IUdrOperations iUderOperations = new UdrOperations();

            return iUderOperations.FindLayoutField(fieldLayout, sirFields, layoutConfidence);
        }

        public Field GetBestMatchingMarker(SIRFields sirFields, MarkerLayout markerLayout, double minSimilarity)
        {
            if (string.IsNullOrWhiteSpace(markerLayout.Label))
                return null;

            List<Field> allMatchingFields = sirFields.Fields.FindAll(field => field.Text.GetSimilarity(markerLayout.Label) >= minSimilarity);
            allMatchingFields = allMatchingFields.OrderByDescending(field => field.Text.GetSimilarity(markerLayout.Label)).ToList();

            if (allMatchingFields.Count > 0)
            {
                allMatchingFields = allMatchingFields.FindAll(fi => fi.Text.GetSimilarity(markerLayout.Label).Equals(allMatchingFields[0].Text.GetSimilarity(markerLayout.Label)));
            }

            return GetNearestField(markerLayout.Bounds, allMatchingFields);
        }

        private Field GetBestMatchingSelfField(SIRFields sirFields, FieldLayout labelField)
        {
            List<Field> allMatchingFields = new List<Field>();

            if (labelField.IsMultiline)
            {
                Field fieldMatch = GetBestMatchingRegion(sirFields, labelField);
                return GetUpdatedSelfField(fieldMatch, labelField.Label);
            }
            else if (!string.IsNullOrWhiteSpace(labelField.Label))
            {
                allMatchingFields = sirFields.Fields.FindAll(fi => fi.Text.ToLower().StartsWith(labelField.Label.ToLower()));
            }

            if (allMatchingFields.Count > 0)
            {
                Field field = GetNearestField(labelField.Bounds, allMatchingFields);
                return GetUpdatedSelfField(field, labelField.Label);
            }

            return null;
        }

        private Field GetBestMatchingSelfFieldWithoutLabel(SIRFields sirFields, FieldLayout labelField)
        {
            Field selfField = sirFields.Fields.FirstOrDefault(field => _rectOperations.GetCenterDistanceBetweenRects(field.Bounds, labelField.Bounds) < MaxDistanceOffset);

            if (labelField.IsMultiline)
            {
                if (selfField != null)
                {
                    Field parentField = sirFields.Regions.FirstOrDefault(fi => fi.Id == selfField.ParentId);
                    return GetBestMatchingSelfRegion(sirFields, parentField);
                }
                else
                {
                    Field parentField = sirFields.Regions.FirstOrDefault(field => _rectOperations.GetCenterDistanceBetweenRects(field.Bounds, labelField.Bounds) < MaxDistanceOffset);
                    return GetBestMatchingSelfRegion(sirFields, parentField);
                }
            }

            return selfField;
        }

        public Field GetBestMatchingSelfRegion(SIRFields sirFields, Field selfField)
        {
            if (selfField != null)
            {
                return GetFieldRegionValue(selfField, sirFields.Fields);
            }
            else
            {
                return null;
            }
        }

        private Field GetUpdatedSelfField(Field field, string removeText)
        {
            if (field == null)
            {
                return null;
            }

            Field updateField = field.Clone();
            if (updateField.Text.StartsWith(removeText))
            {
                updateField.Text = updateField.Text.Length >= removeText.Length
                    ? updateField.Text.Remove(0, removeText.Length)
                    : updateField.Text;
            }
            return updateField;
        }

        public Field GetBestMatchingRegion(SIRFields sirFields, FieldLayout fieldLayout)
        {
            if (string.IsNullOrWhiteSpace(fieldLayout.Label))
                return null;

            List<Field> allMatchingFields;

            allMatchingFields = sirFields.Fields.FindAll(fi => fi.Text.ToLower().StartsWith(fieldLayout.Label.ToLower()));

            if (allMatchingFields.Count > 0)
            {
                return GetFieldRegionValue(GetNearestField(fieldLayout.Bounds, allMatchingFields), sirFields.Fields);
            }
            else
            {
                return null;
            }
        }

        private Field GetFieldRegionValue(Field field, List<Field> fieldList)
        {
            List<Field> fields = fieldList.FindAll(fi => fi.ParentId == field.Id);
            field.Text = GetTextFromFields(fields);
            return field;
        }

        public Field GetNearestField(Rectangle sourcePoint, List<Field> fields)
        {
            if (fields.Count == 0)
            {
                return null;
            }

            Field foundField = fields[0];
            int distance = 9999999;

            foreach (var field in fields)
            {
                int dist = _rectOperations.GetCenterDistanceBetweenRects(sourcePoint, field.Bounds);

                if (distance > dist)
                {
                    distance = dist;
                    foundField = field;
                }
            }

            return foundField;
        }

        public List<Field> GetFieldsWithinRect(List<Field> fields, Rectangle cellRect)
        {
            return fields.FindAll(field => field.Type == FieldType.SystemField && field.Bounds.X >= cellRect.X &&
                                  field.Bounds.Y >= cellRect.Y && (field.Bounds.X + field.Bounds.Width) <= cellRect.Right &&
                                  (field.Bounds.Y + field.Bounds.Height) <= cellRect.Bottom);
        }
        public List<Field> GetAllSystemFieldsWithinRect(List<Field> fields, Rectangle cellRect)
        {
            return fields.FindAll(field => (field.Type == FieldType.SystemField || field.Type == FieldType.SystemRegion) && field.Bounds.X >= cellRect.X &&
                                  field.Bounds.Y >= cellRect.Y && (field.Bounds.X + field.Bounds.Width) <= cellRect.Right &&
                                  (field.Bounds.Y + field.Bounds.Height) <= cellRect.Bottom);
        }

        public List<Field> GetFieldsWithinBoundField(List<Field> fields, Rectangle cellRect)
        {
            return fields.FindAll(field => cellRect.Contains(field.Bounds.X + field.Bounds.Width / 2, field.Bounds.Y + field.Bounds.Height / 2));
        }
        public List<Field> GetFieldsWithinBoundFieldByOverlapping(List<Field> fields, Rectangle cellRect, float overlapRatio)
        {
            return fields.FindAll(field => OverlapArea(cellRect, field.Bounds) >= (field.Bounds.Width * field.Bounds.Height) * overlapRatio);
        }
        private int OverlapArea(Rectangle rect1, Rectangle rect2)
        {
            int x_overlap = Math.Max(0, Math.Min(rect1.Right, rect2.Right) - Math.Max(rect1.Left, rect2.Left));
            int y_overlap = Math.Max(0, Math.Min(rect1.Bottom, rect2.Bottom) - Math.Max(rect1.Top, rect2.Top));
            int overlapArea = x_overlap * y_overlap;
            return overlapArea;
        }

        public List<Field> SortFields(List<Field> fieldLists)
        {
            if (fieldLists == null)
            {
                return fieldLists;
            }
            //SortFieldsX(fieldLists);
            //SortFieldsY(fieldLists);
            fieldLists.Sort(new FieldPositionComparer());

            return fieldLists;
        }

        public void SortFieldsX(List<Field> fieldLists)
        {
            if (fieldLists != null)
            {
                bool changes;
                int count = fieldLists.Count;
                do
                {
                    changes = false;
                    count--;
                    for (int i = 0; i < count; i++)
                    {
                        if (fieldLists[i].Bounds.X > fieldLists[i + 1].Bounds.X)
                        {
                            Field temp = fieldLists[i + 1];
                            fieldLists[i + 1] = fieldLists[i];
                            fieldLists[i] = temp;
                            changes = true;
                        }
                    }
                }
                while (changes);
            }
        }

        public void SortFieldsY(List<Field> fieldLists)
        {
            if (fieldLists != null)
            {
                bool changes;
                int count = fieldLists.Count;
                do
                {
                    changes = false;
                    count--;
                    for (int i = 0; i < count; i++)
                    {
                        if (fieldLists[i].Bounds.Y > (fieldLists[i + 1].Bounds.Y + fieldLists[i + 1].Bounds.Height / 2) + 1
                            && fieldLists[i].Bounds.Y + fieldLists[i].Bounds.Width / 2 > fieldLists[i + 1].Bounds.Y + fieldLists[i + 1].Bounds.Height)
                        {
                            Field temp = fieldLists[i + 1];
                            fieldLists[i + 1] = fieldLists[i];
                            fieldLists[i] = temp;
                            changes = true;
                        }
                    }
                }
                while (changes);
            }
        }

        public string GetTextFromFields(List<Field> fields)
        {
            fields = SortFields(fields);
            string spaceString = " ";
            string valueText = string.Empty;

            if (fields != null && fields.Count > 0)
            {
                valueText = fields[0].Text;

                for (int i = 1; i < fields.Count; i++)
                {
                    //spaceString = GetSpaceDistance(fields[i-1] , fields[i]);
                    valueText += spaceString + fields[i].Text;
                }
            }

            return valueText.Replace("\r", " ");
        }

        private string GetSpaceDistance(Field preField, Field curField)
        {
            string distSpace = string.Empty;
            int charWidth = getCharWidth(preField);

            int curCenterPoint = curField.Bounds.Top + curField.Bounds.Height / 2;
            int distBetweenFields = curField.Bounds.Left - (preField.Bounds.Left + preField.Bounds.Width);

            if ((distBetweenFields >= charWidth / 10 && distBetweenFields > 8) && curCenterPoint > preField.Bounds.Top && curCenterPoint <= preField.Bounds.Top + preField.Bounds.Height)
            {
                //if (!(preField.Text.EndsWith(",") || preField.Text.EndsWith("-") || curField.Text.StartsWith(".") || curField.Text.StartsWith("-")))
                distSpace = " ";
            }
            else if (preField.Bounds.Left > curField.Bounds.Left)
            {
                distSpace = " ";
            }
            else if (!(preField.Text.EndsWith(",") || preField.Text.EndsWith("-") || preField.Text.EndsWith(".") || preField.Text.EndsWith(":")
                    || curField.Text.StartsWith(",") || curField.Text.StartsWith("-") || curField.Text.StartsWith(".") || curField.Text.StartsWith(":")))
                distSpace = " ";

            return distSpace;
        }

        private int getCharWidth(Field field)
        {
            if (!string.IsNullOrEmpty(field.Text))
            {
                return field.Bounds.Width / field.Text.Length;
            }
            return 0;
        }

        public bool IsFieldLable(Field field)
        {
            if (field.Text.Length < 3)
                return false;

            return field.Text.Trim().EndsWith(":") || field.Text.Trim().EndsWith("#") || field.Text.Trim().EndsWith(",");
        }

        public int GetMinAllowedDistanceToMergeFields(Field currentField, Field prevField)
        {
            //int distance = _rectOperations.GetXDistanceBetweenRects(currentField.Bounds, prevField.Bounds);
            return (currentField.Bounds.Width + prevField.Bounds.Width) / (currentField.Text.Length + prevField.Text.Length);
        }

        public Field GetValueField(FieldLayout labelField, SIRFields sirFields)
        {
            Field fieldValue = GetSingleLineValueField(labelField, sirFields);

            if (IsFieldNotFound(fieldValue))
                return fieldValue;

            if (labelField.IsMultiline && labelField.FieldDirection != FieldDirection.None)
                return GetMultilineValueField(fieldValue, sirFields.Fields, sirFields.Lines);
            else
                return GetFilteredFieldValue(fieldValue);
        }

        public Field GetFilteredFieldValue(Field fieldValue)
        {
            if (string.IsNullOrWhiteSpace(fieldValue.Text))
                return fieldValue;

            fieldValue.Text = fieldValue.Text.Trim();

            if (fieldValue.Text.StartsWith(":#"))
                fieldValue.Text = fieldValue.Text.Remove(0, 2);

            if (fieldValue.Text.StartsWith("#:"))
                fieldValue.Text = fieldValue.Text.Remove(0, 2);

            if (fieldValue.Text.StartsWith("#"))
                fieldValue.Text = fieldValue.Text.Remove(0, 1);

            if (fieldValue.Text.StartsWith(":"))
                fieldValue.Text = fieldValue.Text.Remove(0, 1);

            return fieldValue;
        }

        private static bool IsFieldNotFound(Field fieldValue)
        {
            return fieldValue == null || string.IsNullOrWhiteSpace(fieldValue.Text);
        }

        private Field GetSingleLineValueField(FieldLayout labelField, SIRFields sirFields)
        {
            switch (labelField.FieldDirection)
            {
                case FieldDirection.Right:
                    return GetRightValueField(labelField, sirFields.Fields, sirFields.Lines);

                case FieldDirection.Bottom:
                    return GetBottomValueField(labelField, sirFields.Fields, sirFields.Lines);

                case FieldDirection.None:
                    {
                        Field field = GetSelfValueField(labelField, sirFields);

                        if (field == null || field.Confidence == 0)
                        {
                            field = GetBestSelfSimilarField(sirFields, labelField);
                        }
                        return field;
                    }
                default:
                    return GetUserDefinedValueField(labelField, sirFields.Fields);
            }
        }

        public Field GetBestSelfSimilarField(SIRFields sirFields, FieldLayout fieldLayout)
        {
            IWordDictionary iWordDictionary = new EnglishWordDictionary(VBotSettings.ApplicationPath, Common.Constants.VISIONBOT_FOLDER);
            List<string> words = iWordDictionary.GetSimilarWords(fieldLayout.Label);

            FieldLayout searchfieldLayout = fieldLayout.Clone();

            foreach (string word in words)
            {
                searchfieldLayout.Label = word;
                Field layoutField = GetSelfValueField(searchfieldLayout, sirFields);
                if (layoutField != null) return layoutField;
            }
            return new Field();
        }

        public Field GetBestSimilarField(SIRFields sirFields, FieldLayout fieldLayout)
        {
            IWordDictionary wordDictionary = new EnglishWordDictionary(VBotSettings.ApplicationPath, Common.Constants.VISIONBOT_FOLDER);
            List<string> words = wordDictionary.GetSimilarWords(fieldLayout.Label);

            FieldLayout searchfieldLayout = fieldLayout.Clone();

            foreach (string word in words)
            {
                searchfieldLayout.Label = word;
                searchfieldLayout.SimilarityFactor = 1.0f;

                Field layoutField = GetBestMatchingField(sirFields, searchfieldLayout);

                if (layoutField != null)
                {
                    return layoutField;
                }
            }

            return new Field();
        }

        public Field GetUserDefinedValueField(FieldLayout labelField, List<Field> fields)
        {
            var valueRect = FindRelativeBound(labelField);
            valueRect.Inflate(5, 5);

            List<Field> valueFields = GetFieldsWithinBoundField(fields, valueRect);
            Field multilineValueField = GetSingleFieldFromFields(valueFields);

            removeSelfLabelFromItsValue(labelField, valueFields, multilineValueField);

            return multilineValueField;
        }

        public Field GetSystemDefinedValueRegion(FieldLayout labelField, SIRFields sirFields)
        {
            var valueRect = FindRelativeBound(labelField);
            valueRect.Inflate(5, 5);

            var valueFields = getRegionFields(sirFields, valueRect);

            Field multilineValueField = GetSingleFieldFromFields(valueFields);

            removeSelfLabelFromItsValue(labelField, valueFields, multilineValueField);

            return multilineValueField;
        }

        private void removeSelfLabelFromItsValue(FieldLayout labelField, List<Field> valueFields, Field multilineValueField)
        {
            if (valueFields != null && valueFields.Count > 0)
            {
                string[] labels = labelField.Label.Split(new char[] { '|' });
                if (labels != null && labels.Length > 0)
                {
                    foreach (var label in labels)
                    {
                        if (labelField.DisplayValue.StartsWith(label) ||
                            labelField.DisplayValue.Substring(0, labelField.DisplayValue.Length >= label.Length ? label.Length : labelField.DisplayValue.Length).GetSimilarity(label) >= labelField.SimilarityFactor)
                        {
                            if (!string.IsNullOrEmpty(labelField.DisplayValue))
                            {
                                string candidateForRemove = labelField.DisplayValue.Substring(0, labelField.DisplayValue.Length >= label.Length ? label.Length : labelField.DisplayValue.Length);
                                if (!string.IsNullOrEmpty(candidateForRemove) && multilineValueField.Text.StartsWith(candidateForRemove, StringComparison.OrdinalIgnoreCase))
                                    multilineValueField.Text = multilineValueField.Text.Remove(0, candidateForRemove.Length);
                                break;
                            }
                        }
                    }
                }
            }
        }

        private List<Field> getRegionFields(SIRFields sirFields, Rectangle valueRect)
        {
            List<Field> valueFields = this.GetFieldsWithinBoundField(sirFields.Fields, valueRect);
            valueFields = valueFields.FindAll(f => f.Bounds.X <= (valueRect.X + valueRect.Width / 2) && f.Bounds.Y <= (valueRect.Y + valueRect.Height / 2));

            if (valueFields.Count > 0)
            {
                List<string> parentIDs = (from q in valueFields select q.ParentId).ToList();
                parentIDs = parentIDs.Distinct().ToList();
                if (parentIDs.Count > 0)
                    valueFields = sirFields.Fields.FindAll(fi => parentIDs.Contains(fi.ParentId));
            }

            return valueFields;
        }

        public Field GetUserDefinedField(FieldLayout labelField, List<Field> fields)
        {
            var valueRect = labelField.Bounds;
            valueRect.Inflate(10, 10);
            List<Field> valueFields = GetFieldsWithinRect(fields, valueRect);
            Field multilineValueField = GetSingleFieldFromFields(valueFields);
            if (valueFields != null && valueFields.Count > 0)
            {
                //if (multilineValueField.Text.StartsWith(labelField.Label, StringComparison.InvariantCulture))
                //    multilineValueField.Text = multilineValueField.Text.Remove(0, labelField.Label.Length);
            }
            return multilineValueField;
        }

        public Rectangle FindRelativeBound(FieldLayout labelField)
        {
            Rectangle valueRect = labelField.ValueBounds;
            if (labelField.XDistant > 0)
            {
                valueRect.X += labelField.Bounds.X + labelField.Bounds.Width + labelField.XDistant;
            }
            else
            {
                valueRect.X += labelField.Bounds.X;
            }
            valueRect.Y += labelField.Bounds.Y;

            return valueRect;
        }

        public Rectangle FindAspectRect(Rectangle valueRect, double aspectRatioX, double aspectRatioY)
        {
            int top = (int)((double)valueRect.Top * aspectRatioY);
            int left = (int)((double)valueRect.Left * aspectRatioX);
            int height = (int)((double)valueRect.Height * aspectRatioY);
            int width = (int)((double)valueRect.Width * aspectRatioX);
            return new Rectangle(left, top, width, height);
        }

        public Field GetMultilineValueField(Field valueField, List<Field> fields, List<Line> lines)
        {
            List<Field> belowFields = fields.FindAll(field => (field.ParentId != null) && field.ParentId.Equals(valueField.ParentId) && field.Bounds.Top >= valueField.Bounds.Top);

            Field multilineValueField = GetSingleFieldFromFields(belowFields);

            return multilineValueField;
        }

        public Field GetSingleFieldFromFields(List<Field> fields)
        {
            Field multilineValueField = new Field();
            multilineValueField.Id = Guid.NewGuid().ToString();

            multilineValueField.Text = GetTextFromFields(fields);
            multilineValueField.Bounds = GetBoundsFromFields(fields);
            multilineValueField.Confidence = new ConfidenceHelper().GetMergedFieldConfidence(fields);

            return multilineValueField;
        }

        public Rectangle GetBoundsFromFields(List<Field> fields)
        {
            if (fields == null || fields.Count == 0)
            {
                return new Rectangle();
            }

            int left = fields[0].Bounds.Left;
            int top = fields[0].Bounds.Top;
            int right = fields[0].Bounds.Right;
            int bottom = fields[0].Bounds.Bottom;

            for (int i = 1; i < fields.Count; i++)
            {
                if (fields[i].Bounds.X < left)
                {
                    left = fields[i].Bounds.X;
                }

                if (fields[i].Bounds.Y < top)
                {
                    top = fields[i].Bounds.Y;
                }

                int newRight = fields[i].Bounds.X + fields[i].Bounds.Width;

                if (newRight > right)
                {
                    right = newRight;
                }

                int newBottom = fields[i].Bounds.Y + fields[i].Bounds.Height;

                if (newBottom > bottom)
                {
                    bottom = newBottom;
                }
            }

            return new Rectangle(left, top, right - left, bottom - top);
        }

        public Field GetRightValueField(FieldLayout labelField, List<Field> fields, List<Line> lines)
        {
            Field rightField = GetRightValueFieldList(new Field() { Bounds = labelField.Bounds }, fields, lines,
                labelField.MergeRatio);

            return GetValidatedField(rightField);
        }

        public Field GetRightValueFieldList(Field field, List<Field> fields, List<Line> lines, double mergeRatio)
        {
            List<Field> allRightFields = fields.FindAll(fi => Math.Abs(fi.Bounds.Top - field.Bounds.Top) < field.Bounds.Height && fi.Bounds.Left > (field.Bounds.Left + field.Bounds.Width - 5));

            allRightFields = allRightFields.OrderBy(fi => _rectOperations.GetCenterDistanceBetweenRects(fi.Bounds, field.Bounds)).ToList();
            SortFieldsX(allRightFields);

            if (allRightFields.Count > 0)
            {
                Field rightField = allRightFields[0].Clone();

                for (int i = 1; i < allRightFields.Count; i++)
                {
                    if (IsRightMeargingField(rightField, allRightFields[i], mergeRatio))
                    {
                        MeargeFieldText(rightField, allRightFields[i]);
                    }
                    else
                    {
                        break;
                    }
                }

                return rightField;
            }

            return null;
        }

        public Field GetNearestBottomField(Field topField, List<Field> fields, int heightFactor = 1)
        {
            List<Field> belowFields = GetFieldsWithinRect(fields, new Rectangle(topField.Bounds.Left, topField.Bounds.Bottom + 1, topField.Bounds.Width, topField.Bounds.Height * heightFactor));

            if (belowFields != null && belowFields.Count > 0)
            {
                belowFields = belowFields.OrderBy(field => field.Bounds.Top).ToList();
                return belowFields[0];
            }

            int offset = 20;
            int step = 0;

            do
            {
                belowFields = GetFieldsIntersectingWithRect(fields, new Rectangle(topField.Bounds.Left - step, topField.Bounds.Bottom + 1, topField.Bounds.Width + (step * 2), topField.Bounds.Height * heightFactor));

                if (belowFields != null && belowFields.Count > 0)
                {
                    belowFields = belowFields.OrderBy(field => field.Bounds.Top).ToList();
                    return belowFields[0];
                }

                step++;
            } while (step <= offset);

            return null;
        }

        public Field GetBottomValueFieldList(Field field, List<Field> fields, List<Line> lines, double mergeRatio)
        {
            Rectangle targetRect = field.Bounds;

            targetRect.Y = field.Bounds.Bottom + 1;
            targetRect.Height = BottomValueHeightOffsetFactor * field.Bounds.Height;

            Field bottomFields = fields.FirstOrDefault(fi => targetRect.Contains(fi.Bounds) || targetRect.IntersectsWith(fi.Bounds));

            if (bottomFields != null)
            {
                Field bottomNearestField = GetRightValueFieldList(bottomFields.Clone(), fields, lines, mergeRatio);

                if (bottomNearestField != null && IsRightMeargingField(bottomFields, bottomNearestField, mergeRatio))
                    MeargeFieldText(bottomFields, bottomNearestField);
            }

            return bottomFields;
        }

        public bool IsRightMeargingField(Field sourceField, Field destField, double mergeRatio)
        {
            int charWidth = sourceField.Text.Length > 0 ? sourceField.Bounds.Width / sourceField.Text.Length : (int)mergeRatio;
            return (sourceField.Bounds.X + sourceField.Bounds.Width + mergeRatio * charWidth) >
                   destField.Bounds.X;
        }

        private bool IsBottomMeargingField(Field sourceField, Field destField, double mergeRatio)
        {
            int charHeight = sourceField.Bounds.Height;
            return (sourceField.Bounds.Y + sourceField.Bounds.Height + mergeRatio * charHeight) >
                   destField.Bounds.Y;
        }

        private void MeargeFieldText(Field field1, Field field2)
        {
            if (field1 != null && field2 != null)
                field1.Text += " " + field2.Text;
        }

        public Field GetBottomValueField(FieldLayout labelField, List<Field> fields, List<Line> lines)
        {
            Field bottomField = GetBottomValueFieldList(new Field() { Bounds = labelField.Bounds }, fields, lines,
                labelField.MergeRatio);

            return GetValidatedField(bottomField);
        }

        public Field GetSelfValueField(FieldLayout labelField, SIRFields sirFields)
        {
            Field selfField = new Field();

            if (!string.IsNullOrEmpty(labelField.Label))
            {
                selfField = GetBestMatchingSelfField(sirFields, labelField);
                return GetValidatedField(selfField);
            }
            else
                selfField = GetBestMatchingSelfFieldWithoutLabel(sirFields, labelField);
            //selfField = sirFields.Fields.FirstOrDefault(field => _rectOperations.GetCenterDistanceBetweenRects(field.Bounds, labelField.Bounds) < MaxDistanceOffset);

            return GetValidatedField(selfField);
        }

        public Field GetValidatedField(Field valueField)
        {
            if (valueField != null)
                return valueField;
            else
                return new Field();
        }

        public List<Field> GetFieldsIntersectingWithRect(List<Field> fields, Rectangle colRect)
        {
            return fields.FindAll(field => field.Type == FieldType.SystemField && colRect.IntersectsWith(field.Bounds));
        }

        public List<Field> GetAllSystemFieldsIntersectingWithRect(List<Field> fields, Rectangle colRect)
        {
            return fields.FindAll(field => (field.Type == FieldType.SystemField || field.Type == FieldType.SystemRegion) && colRect.IntersectsWith(field.Bounds));
        }

        public bool IsDateField(Field field)
        {
            DateTime dateTime;
            return DateTime.TryParse(field.Text, out dateTime);
        }

        internal bool IsFieldValidForMerge(Field field)
        {
            if (field.Text.Trim().Length > 2 && (field.Text.Trim().StartsWith(",") ||
                field.Text.Trim().StartsWith(":") || field.Text.Trim().StartsWith("#")))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns CheckBox State.
        /// -2 : OCR failed. Key not identified.
        /// -1 : OCR passed. Checkbox recognition failed.
        /// 1 : Checkbox is checked.
        /// 0 : Checkbox is un-checked.
        /// </summary>
        /// <param name="checkBoxField">CheckBoxField</param>
        /// <returns></returns>
        public Field GetUserDefinedCheckBoxValue(string imagePath, FieldLayout checkBoxField, FieldDef checkBoxFieldDef, bool isFeatureEnabled, IVisionBotPageImageProvider visionBotPageImageProvider)
        {
            Bitmap image = null;
            try
            {
                var checkBoxRect = FindRelativeBound(checkBoxField);
                //Adjusting the rectangle as checkbox is UDR and for UDR field, we have magic numbers
                var adjustedValue = 5;
                var adjustedCheckBoxRect = new Rectangle(checkBoxRect.X - adjustedValue, checkBoxRect.Y - adjustedValue, checkBoxRect.Width + (adjustedValue * 2), checkBoxRect.Height + (adjustedValue * 2));

                //TODO: Need to expose mothod for not using borderPadding
                CroppedImage croppedImage = visionBotPageImageProvider.GetCroppedImageFromProcessDocument(adjustedCheckBoxRect.X, adjustedCheckBoxRect.Y, adjustedCheckBoxRect.Width, adjustedCheckBoxRect.Height);

                image = croppedImage.GetCroppedImage();
                string checkBoxImagePath = Path.Combine(Path.GetTempPath(), Automation.Cognitive.VisionBotEngine.Properties.Resources.VisionBotTempDirectory, checkBoxFieldDef.Name + ".png");
                image.Save(checkBoxImagePath);

                var checkBoxValueField = new Field();
                checkBoxValueField.Id = checkBoxField.FieldId;
                checkBoxValueField.Bounds = checkBoxRect;
                checkBoxValueField.Type = FieldType.UserDefinedField;
                checkBoxValueField.SegmentationType = SegmentationType.Word;

                var borderPadding = 30;
                int checkBoxValue = isFeatureEnabled
                    ? PatternRecognizer.MatchAndRecognizeCheckBox(checkBoxImagePath, borderPadding, borderPadding, adjustedCheckBoxRect.Width, adjustedCheckBoxRect.Height) : -3;
                checkBoxValueField.Text = checkBoxValue.ToString();

                if (checkBoxValue == -3)
                    checkBoxValueField.Text = "CHECKBOX RECOGNITION DISABLED";
                if (checkBoxValue == -1)
                    checkBoxValueField.Text = "CHECKBOX NOT FOUND IN GIVEN BOUND";

                CheckBoxDebugInfo(checkBoxImagePath, checkBoxFieldDef, adjustedCheckBoxRect, checkBoxValueField.Text);
                image.Dispose();
                return checkBoxValueField;
            }
            finally
            {
                if (image != null)
                    image.Dispose();
            }
        }

        /// <summary>
        /// Dump debug info - checkbox recognition.
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="checkBoxFieldDef"></param>
        /// <param name="checkBoxRect"></param>
        /// <param name="checkBoxValue"></param>
        private void CheckBoxDebugInfo(string checkBoxImagePath, FieldDef checkBoxFieldDef, Rectangle checkBoxRect, string checkBoxValue)
        {
            StringBuilder debugInfo = new StringBuilder();
            debugInfo.AppendLine(string.Format("X: {0}", checkBoxRect.X));
            debugInfo.AppendLine(string.Format("Y: {0}", checkBoxRect.Y));
            debugInfo.AppendLine(string.Format("W: {0}", checkBoxRect.Width));
            debugInfo.AppendLine(string.Format("H: {0}", checkBoxRect.Height));
            /* Bitmap bitMap = new Bitmap(imagePath);
             Bitmap fieldImage = new Crop(checkBoxRect).Apply(bitMap);
             string checkBoxImagePath = Path.Combine(Path.GetTempPath(), Properties.Resources.VisionBotTempDirectory, checkBoxFieldDef.Name + ".png");*/
            debugInfo.AppendLine(string.Format("ImageName: {0}", checkBoxImagePath));
            //fieldImage.Save(checkBoxImagePath);
            debugInfo.AppendLine(string.Format("CheckBoxValue: {0}", checkBoxValue));
            File.WriteAllText(checkBoxImagePath + ".txt", debugInfo.ToString());
        }
    }

    internal class FieldPositionComparer : IComparer<Field>
    {
        public int Compare(Field field1, Field field2)
        {
            const double overlappingThreshold_1 = .3;
            if (areRectsOverlappingGivenThreshold(field1.Bounds, field2.Bounds, overlappingThreshold_1))
            {
                const double overlappingThreshold_2 = .7;

                bool areWords = isWord(field1)
                    && isWord(field2);

                if (areWords
                    && !areRectsOverlappingGivenThreshold(field1.Bounds, field2.Bounds, overlappingThreshold_2))
                {
                    return commonComparer(field1.Bounds.Y, field2.Bounds.Y);
                }
                else
                {
                    double overlappingThreshold_3 = .6;

                    if (!areWords)
                        overlappingThreshold_3 = .3;

                    if (areRectsOverlappingGivenThreshold(field1.Bounds, field2.Bounds, overlappingThreshold_3))
                    {
                        return commonComparer(field1.Bounds.X, field2.Bounds.X);
                    }
                    else
                    {
                        return commonComparer(field1.Bounds.Y, field2.Bounds.Y);
                    }
                }
            }
            else
            {
                return commonComparer(field1.Bounds.Y, field2.Bounds.Y);
            }
        }

        private bool areRectsOverlappingGivenThreshold(Rectangle bounds1
            , Rectangle bounds2, double overlappingThreshold)
        {
            double heightOfFirstRectangle = bounds1.Height;
            double heightOfSecondRecctangle = bounds2.Height;
            double OverlappingHeightThreshold = Math.Min(bounds1.Height, bounds2.Height) * overlappingThreshold;

            if (bounds1.Top < bounds2.Top)
            {
                return (bounds1.Bottom - bounds2.Top) > OverlappingHeightThreshold;
            }
            else
            {
                return (bounds2.Bottom - bounds1.Top) > OverlappingHeightThreshold;
            }
        }

        private bool isWord(Field field)
        {
            return field.Text.Length > 1;
        }

        private bool isWord(Rectangle rect)
        {
            const int factor = 2;
            return rect.Width > (factor * rect.Height);
        }

        private int commonComparer(int value1, int value2)
        {
            return value1 < value2 ? -1 : (value1 > value2 ? 1 : 0);
        }
    }
}