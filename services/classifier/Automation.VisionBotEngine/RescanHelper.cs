﻿using AForge.Imaging.Filters;
using Automation.VisionBotEngine.Common;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.OcrEngine;
using Automation.VisionBotEngine.OcrEngine.Tesseract;
using Automation.VisionBotEngine.Validation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine
{
    ////TODO : Refactoring is pending
    internal static class RescanHelper
    {
        public static IVisionBotPageImageProvider _pageImageProvider;
        public static EngineType _engineType;
        public static ProcessingMode? _processingMode;
        public static string _dataPath;
        public static VBotEngineSettings _engineSettings;

        public static void Init(IVisionBotPageImageProvider pageImageProvider, EngineType engineType, ProcessingMode processingMode, string dataPath, VBotEngineSettings engineSettings)
        {
            _pageImageProvider = pageImageProvider;
            _engineType = engineType;
            _processingMode = processingMode;
            _dataPath = dataPath;
            _engineSettings = engineSettings;
        }

        public static string GetResizeTextValue(Rectangle boundsForValue, FieldDef fieldDef, Field valueField, DocumentProperties docprops)
        {
            try
            {
                string docpath = docprops != null ? docprops.Path : "";
                if (_engineSettings != null && _engineSettings.IsAccuracyLevelEnabled && _pageImageProvider != null && _processingMode != null && _processingMode == ProcessingMode.Image)
                {
                    DateTypeParser dp = new DateTypeParser();
                    if (valueField.Confidence < 90 && fieldDef.ValueType.ToString().Equals("Date"))
                    {
                        double resizeFactorValue = Math.Ceiling(CalculateResizeFactor(boundsForValue.Width, boundsForValue.Height));
                        if (resizeFactorValue < 1)
                        {
                            resizeFactorValue = 1;
                        }
                        List<Tuple<double, Field>> ocrData = new List<Tuple<double, Field>>();
                        while (resizeFactorValue >= 0.9)
                        {
                            Field valueFieldSecondPassTemp = GetValueFieldFromRectNew(boundsForValue, valueField, fieldDef.ValueType, TesseractOcrEngine.SegmentationMode.PSM_AUTO, TesseractOcrEngine.OcrEngineMode.OEM_TESSERACT_ONLY, null, true, resizeFactorValue);
                            VBotLogger.Debug(() => "[GetResizeTextValue] Second Pass Value with resizing : PSM_AUTO: OEM_TESSERACT_ONLY: Resize Factor: " + resizeFactorValue + " : " + valueFieldSecondPassTemp.Text + " with confidence value " + valueFieldSecondPassTemp.Confidence + " for file: " + docpath);
                            if (dp.Parse(valueFieldSecondPassTemp.Text) != null)
                            {
                                Tuple<double, Field> newData = new Tuple<double, Field>(resizeFactorValue, valueFieldSecondPassTemp);
                                ocrData.Add(newData);
                                VBotLogger.Debug(() => "[GetResizeTextValue]  ADDED Second Pass Value with resizing : PSM_AUTO: OEM_TESSERACT_ONLY: Resize Factor: " + resizeFactorValue + " : " + valueFieldSecondPassTemp.Text + " with confidence value " + valueFieldSecondPassTemp.Confidence + " for file: " + docpath);
                            }
                            resizeFactorValue = resizeFactorValue - 0.5;
                        }

                        Tuple<double, Field> valueFieldSecondPassTuple = ocrData.OrderByDescending(item => item.Item2.Confidence).FirstOrDefault();
                        Field valueFieldSecondPass = valueFieldSecondPassTuple != null && valueFieldSecondPassTuple.Item2 != null ? valueFieldSecondPassTuple.Item2 : null;
                        if (valueFieldSecondPass != null && !string.IsNullOrWhiteSpace(valueFieldSecondPass.Text))
                        {
                            if (dp.Parse(valueField.Text) == null && dp.Parse(valueFieldSecondPass.Text) != null)
                            {
                                VBotLogger.Debug(() => "[GetResizeTextValue] Final Value considering second pass value with resizing : PSM_AUTO: OEM_TESSERACT_ONLY: " + valueFieldSecondPass.Text + " with confidence value " + valueFieldSecondPass.Confidence + " for file: " + docpath);
                                return valueFieldSecondPass.Text;
                            }
                            else if (dp.Parse(valueField.Text) != null && dp.Parse(valueFieldSecondPass.Text) == null)
                            {
                                VBotLogger.Debug(() => "[GetResizeTextValue]  Final Value considering first pass value with resizing : PSM_AUTO: OEM_TESSERACT_ONLY: " + valueField.Text + " with confidence value " + valueField.Confidence + " for file: " + docpath);
                                return valueField.Text;
                            }

                            if ((valueField.Confidence - valueFieldSecondPass.Confidence) < 25)
                            {
                                VBotLogger.Debug(() => "[GetResizeTextValue] Final Value considering Second pass value with resizing as confidence < 25: PSM_AUTO: OEM_TESSERACT_ONLY: " + valueFieldSecondPass.Text + " with confidence value " + valueFieldSecondPass.Confidence + " for file: " + docpath);
                                valueField.Text = valueFieldSecondPass.Text;
                            }
                            else
                            {
                                VBotLogger.Debug(() => "[GetResizeTextValue] Final Value considering First pass value with resizing as confidence > 25: PSM_AUTO: OEM_TESSERACT_ONLY: " + valueField.Text + " with confidence value " + valueField.Confidence + " for file: " + docpath);

                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => "[GetResizeTextValue] Error while rescaning. Exception: " + ex.ToString());
            }

            return valueField != null ? valueField.Text : "";
        }

        private static Field GetValueFieldFromRectNew(Rectangle fieldValueRect, Field field, FieldValueType fieldType, TesseractOcrEngine.SegmentationMode segMode, TesseractOcrEngine.OcrEngineMode engineMode, string whiteList, bool resizeFactor, double resizeFactorValue)
        {
            VBotLogger.Trace(() => "[GetValueFieldFromRect] IN Extract field value from rect.");

            Field valueField = new Field();
            FieldOperations fieldOp = new FieldOperations();

            try
            {
                if (_pageImageProvider != null && _processingMode != null && _processingMode == ProcessingMode.Image)
                {
                    string imgPath = GetFieldImageFilePathNew(fieldValueRect, resizeFactor, resizeFactorValue);

                    List<Field> scannedFields = GetScannedFieldsForDataType(imgPath, _dataPath, fieldType, segMode, engineMode);

                    if (scannedFields.Count > 0)
                    {
                        valueField = fieldOp.GetSingleFieldFromFields(scannedFields);
                    }

                    if (File.Exists(imgPath))
                    {
                        File.Delete(imgPath);
                    }
                }
                else
                {
                    VBotLogger.Trace(() => "[GetValueFieldFromRect] Field re-scan is not allowed.");
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[GetValueFieldFromRect] Exception : {0}", ex.Message));
            }

            VBotLogger.Trace(() => "[GetValueFieldFromRect] OUT Extract field value from rect.");

            return valueField;
        }

        private static string GetFieldImageFilePathNew(Rectangle fieldValueRect, bool resizeFactor, double resizeFactorValue)
        {

            string imgPath = null;

            Rectangle fieldRect = fieldValueRect;
            Page ImagePage = _pageImageProvider.GetImageFromProcessDocument(0);
            if (ImagePage != null && ImagePage.ProcessedPageImage != null)
            {
                imgPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

                CroppedImage cropImage = _pageImageProvider.GetCroppedImageFromProcessDocument(fieldRect.X, fieldRect.Y, fieldRect.Width, fieldRect.Height);

                Bitmap image = new Bitmap(cropImage.GetCroppedImage());
                Bitmap reSizedImage = image;
                //new CanvasCrop(new Rectangle(borderOffset, borderOffset, fieldRect.Width - 2 * borderOffset, fieldRect.Height - 2 * borderOffset)).ApplyInPlace(reSizedImage);
                if (resizeFactorValue > 1)
                {
                    ResizeBilinear filter = new ResizeBilinear((int)(image.Width * resizeFactorValue), (int)(image.Height * resizeFactorValue));
                    //   apply the filter
                    reSizedImage = filter.Apply(image);
                }
                reSizedImage.Save(imgPath);
                reSizedImage.Dispose();
                ImagePage.Dispose();
            }

            return imgPath;
        }

        private static List<Field> GetScannedFieldsForDataType(string imagePath, string dataPath, FieldValueType valueType, TesseractOcrEngine.SegmentationMode segMode, TesseractOcrEngine.OcrEngineMode engineMode)
        {
            List<Field> scannedFields = new List<Field>();

            OcrEngineStratagy ocrEngineStratagy = new OcrEngineStratagy();
            IOcrEngine ocrEngine = ocrEngineStratagy.GetOcrEngine(_engineType, segMode, engineMode);
            SegmentedDocument segDoc = null;

            segDoc = ocrEngine.ExtractRegions(imagePath, Constants.SearchString, dataPath);


            for (int i = 0; i < segDoc.Regions.Count; i++)
            {
                scannedFields.Add(VBotDocumentGenerator.CreateFieldFromRegion(segDoc.Regions[i]));
            }

            return scannedFields;
        }

        private static float CalculateResizeFactor(int originalWidth, int originalHeight)
        {
            float maxWidth = 543;
            float maxHeight = 237;
            float ratio = 1;
            if (originalWidth > 0 && originalHeight > 0)
            {
                float ratioX = maxWidth / (float)originalWidth;
                float ratioY = maxHeight / (float)originalHeight;
                ratio = Math.Min(ratioX, ratioY);
            }
            return ratio;
        }

    }
}
