﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.VisionBotEngine.CustomExceptions
{
    public class MaxPageLimitException : System.Exception
    {
        public MaxPageLimitException() : base()
        {
        }

        public MaxPageLimitException(string message) : base(message)
        {
            
        }

        public MaxPageLimitException(string message, System.Exception innerException)
            :base(message, innerException)
        {
        }

    }
}
