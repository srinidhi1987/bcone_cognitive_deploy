﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LightInject;
using System.Diagnostics.CodeAnalysis;

namespace Automation.VisionBotEngine
{
    class AssemblyRootComposition : ICompositionRoot
    {
        [ExcludeFromCodeCoverage]
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<IVBotDocClassifier, VBotDocClassifier>();
            serviceRegistry.Register<IVBotDocumentGenerator, VBotDocumentGenerator>();
        }
    }
}
