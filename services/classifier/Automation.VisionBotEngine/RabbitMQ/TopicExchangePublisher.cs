﻿using Automation.Services.Client;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.RabbitMQ
{
    class TopicExchangePublisher
    {
        internal const string TOPIC_FILE_PROCESSING = "FileProcessing";
        internal const string TOPIC_FILE_PROCESSING_UNPROCESSED = TOPIC_FILE_PROCESSING + ".Unprocessed";
        internal const string TOPIC_FILE_PROCESSING_UNPROCESSED_VISIONBOT_NOT_FOUND = TOPIC_FILE_PROCESSING_UNPROCESSED + ".VisionBotNotFound";

        public string HostName { get; private set; }
        public string VirtualHostName { get; private set; }

        public string ExchangeName { get; private set; }

        public string UserName { get; private set; }
        public string Password { get; private set; }

        public TopicExchangePublisher(string exchangeName, string hostName = "localhost")
        {
            this.HostName = hostName;
            this.ExchangeName = exchangeName;
        }

        public TopicExchangePublisher(string exchangeName, RabbitMQSetting setting)
        {
            this.HostName = setting.Uri;
            this.VirtualHostName = setting.VirtualHost;
            this.UserName = setting.UserName;
            this.Password = setting.Password;
            this.ExchangeName = exchangeName;
        }

        public void PublishMessage(string routeKey,
         string message)
        {
            var factory = new ConnectionFactory()
            {
                HostName = this.HostName,
                VirtualHost = this.VirtualHostName,
                UserName = this.UserName,
                Password = this.Password,
                AutomaticRecoveryEnabled = true
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: this.ExchangeName,
                                  type: "topic",
                                  durable: true,
                                  autoDelete: false,
                                  arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: this.ExchangeName,
                                     routingKey: routeKey,
                                     basicProperties: properties,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }
        }
    }
}
