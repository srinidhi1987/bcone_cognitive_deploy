﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Interfaces;
    using Automation.VisionBotEngine.Model;
    using Services.Client;

    internal class AliasServiceFieldKeyStorage : IFieldKeyStorage
    {
        private static IList<FieldKey> _fieldKeys = new List<FieldKey>();

        public IList<FieldKey> GetFieldKeys()
        {
            return _fieldKeys;
        }

        void IFieldKeyStorage.InsertFieldKey(FieldKey fieldKey)
        {
            throw new NotImplementedException();
        }

        public void SetFieldKeys(string projectTypeId, string languageId)
        {
            _fieldKeys = ReadFieldKeysFromAliasService(projectTypeId, languageId);
        }

        private IList<FieldKey> ReadFieldKeysFromAliasService(string projectTypeId, string languageId)
        {
            VBotLogger.Trace(() => $"[AliasServiceFieldKeyStorage::ReadFieldKeysFromAliasService]: Getting Aliases Key from alias service");
            try
            {
                IList<FieldKey> fieldKeys = new List<FieldKey>();

                AliasDetailBasedOnProjectTypeAndLanguage aliasDetail = GetAliases(projectTypeId, languageId);

                int iCount = 0;
                if (aliasDetail == null)
                    return fieldKeys;
                foreach (AliasDetail curraliasDetail in aliasDetail.fields)
                {
                    foreach (string alias in curraliasDetail.aliases)
                    {
                        var fieldName = alias.Trim();
                        var isMultiLineValue = false;
                        var direction = Direction.Right;

                        FieldKey fieldKey = new FieldKey()
                        {
                            Id = iCount.ToString(),
                            Name = fieldName,
                            IsMultilineValue = isMultiLineValue,
                            Direction = direction
                        };
                        fieldKeys.Add(fieldKey);
                        iCount++;
                    }
                }
                VBotLogger.Trace(() => $"[AliasServiceFieldKeyStorage::ReadFieldKeysFromAliasService]: Getting Aliases Key Success");
                return fieldKeys;
            }
            catch (Exception ex)
            {
                VBotLogger.Trace(() => $"[AliasServiceFieldKeyStorage::ReadFieldKeysFromAliasService]: Error in getting aliases from service ");
                return new List<FieldKey>();
            }
        }

        private static AliasDetailBasedOnProjectTypeAndLanguage GetAliases(string projectTypeId, string languageId)
        {
            AliasServiceEndPointConsumer aliasService = new AliasServiceEndPointConsumer();
            AliasDetailBasedOnProjectTypeAndLanguage aliasDetail = aliasService.GetAliasesBasedOnProjectTypeAndLanguageId(projectTypeId, languageId);
            return aliasDetail;
        }
    }
}