﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Interfaces;
    using Automation.VisionBotEngine.Model;
    using System.IO;

    internal class CsvFieldKeyStorage : IFieldKeyStorage
    {
        public string FilePath { get; private set; }

        public CsvFieldKeyStorage(string filePath)
        {
            FilePath = filePath;

            CheckTrainedDataFile();
        }

        private static void CheckTrainedDataFile()
        {
            string FileExtractTo = Path.Combine(Configuration.VBotSettings.ApplicationPath, Automation.Cognitive.VisionBotEngine.Properties.Resources.TrainedDataFileName);

            if (!File.Exists(FileExtractTo))
            {
                copyFileFromResources(FileExtractTo);
            }
        }

        void IFieldKeyStorage.InsertFieldKey(FieldKey fieldKey)
        {
            throw new NotImplementedException();
        }

        IList<FieldKey> IFieldKeyStorage.GetFieldKeys()
        {
            string errorString = validateFile(FilePath);

            if (string.IsNullOrEmpty(errorString))
            {
                var fieldKeys = this.ReadFieldKeysFromCsv();

                return fieldKeys;
            }

            return new List<FieldKey>();
        }

        #region Private Methods

        private static void copyFileFromResources(string FileExtractTo)
        {
            string input = Automation.Cognitive.VisionBotEngine.Properties.Resources.TrainedFields;

            File.WriteAllText(FileExtractTo, input);
        }

        private IList<FieldKey> ReadFieldKeysFromCsv()
        {
            IList<FieldKey> fieldKeys = new List<FieldKey>();

            ICsvReaderWriter csvHelper = new CsvHelperReaderWriter(this.FilePath, ",");
            List<string[]> csvList = csvHelper.ReadContents();

            for (int i = 0; i < csvList.Count; i++)
            {
                string[] line = csvList[i];

                var fieldName = getFieldName(line);
                var isMultiLineValue = isMultiLineFieldValue(line);
                var direction = getFieldDirection(line);

                FieldKey fieldKey = new FieldKey()
                {
                    Id = i.ToString(),
                    Name = fieldName,
                    IsMultilineValue = isMultiLineValue,
                    Direction = direction
                };

                fieldKeys.Add(fieldKey);
            }
            return fieldKeys;
        }

        private static string getFieldName(string[] line)
        {
            string fieldName = string.Empty;

            if (line.Length >= 1)
                fieldName = line[0].Trim();

            return fieldName;
        }

        private static Direction getFieldDirection(string[] line)
        {
            Direction direction = Direction.Right;

            if (line.Length >= 3)
                Enum.TryParse(line[2], true, out direction);

            return direction;
        }

        private static bool isMultiLineFieldValue(string[] line)
        {
            bool isMultiLine = false;

            if (line.Length >= 2)
                isMultiLine = Convert.ToBoolean(line[1]);

            return isMultiLine;
        }

        private string validateFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return string.Format(Automation.Cognitive.VisionBotEngine.Properties.Resources.FileNotExists, filePath);
            }

            if (!Path.GetExtension(filePath).Equals(".csv"))
            {
                return Automation.Cognitive.VisionBotEngine.Properties.Resources.InvalidFileFormat;
            }

            return string.Empty;
        }

        #endregion Private Methods
    }
}