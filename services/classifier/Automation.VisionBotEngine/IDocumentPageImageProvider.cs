﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;

/**
* Copyright (c) 2015 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

namespace Automation.VisionBotEngine
{
    internal interface IDocumentPageImageProvider
    {
        System.Drawing.Bitmap GetPage(int pageIndex);

        Page GetProcessedPageBitmap(int pageIndex, int imageCacheCount);

        CroppedImage GetCroppedImageFromAbsBounds(int x, int y, int w, int h);

        CroppedImage GetCroppedImageFromRelativeBounds(int x, int y, int w, int h, int pageIndex);
    }
}