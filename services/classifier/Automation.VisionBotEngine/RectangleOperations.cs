﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    internal interface IRectangleOperations
    {
        bool IsRectangleEmpty(Rectangle valueBounds);

        int GetMostTopOfTwoRectangle(Rectangle bounds1, Rectangle bounds2);

        int GetMostBottomOfTwoRectangle(Rectangle bounds1, Rectangle bounds2);

        int GetCenterDistanceBetweenRects(Rectangle bounds1, Rectangle bounds2);

        int GetXDistanceBetweenRects(Rectangle bounds1, Rectangle bounds2);

        int GetYDistanceBetweenRects(Rectangle bounds1, Rectangle bounds2);

        Point GetCenterPoint(Rectangle rectangle);

        List<Line> FindAllHorizontalLinesInBetweenRects(Rectangle rect1
            , Rectangle rect2
            , List<Line> lines);

        List<Line> FindAllVerticalLinesInBetweenRects(Rectangle rect1
         , Rectangle rect2
         , List<Line> lines);
    }

    internal class AspectRatio
    {
        public double XRatio { get; set; }
        public double YRatio { get; set; }
    }

    internal class RectangleOperations : IRectangleOperations
    {
        private const int MaxAlignementOffset = 30;
        private const int AngleTolerance = 10;
        internal static readonly int DistanceTolerance = 300;
        private const int VerticalImagePadding = 200;
        private const int HorizontalImagePadding = 400;

        public bool AreRectsSeperatedByAnyLine(Rectangle rect1, Rectangle rect2, List<Line> lines)
        {
            if (AreRectsSeperatedByAnyVerticalLine(rect1, rect2, lines))
                return true;

            if (AreRectsSeperatedByAnyHorizontalLine(rect1, rect2, lines))
                return true;

            return false;
        }

        public bool AreRectsSeperatedByAnyVerticalLine(Rectangle rect1, Rectangle rect2, List<Line> lines)
        {
            if (lines == null || lines.Count == 0)
                return false;

            List<Line> allMatchedlines = FindAllVerticalLinesInBetweenRects(rect1
                , rect2
                , lines);

            if (allMatchedlines != null
               && allMatchedlines.Count > 0)
            {
                return true;
            }

            return false;
        }

        public List<Line> FindAllVerticalLinesInBetweenRects(Rectangle rect1
         , Rectangle rect2
         , List<Line> lines)
        {
            if (lines == null || lines.Count == 0)
                return new List<Line>();
            int leftX, rightX;

            #region rect1 is left most non-horizontally-overlapping rectangle
            if (rect1.Right < rect2.Left)
            {
                leftX = rect1.Right;
                rightX = rect2.Left;
            }
            #endregion
            #region rect2 is left most non-horizontally-overlapping rectangle
            else if (rect2.Right < rect1.Left)
            {
                leftX = rect2.Right;
                rightX = rect1.Left;
            }
            #endregion
            #region overlapped rectangle scenario
            else
            {
                return new List<Line>();
            }
            #endregion
            //int leftX = rect1.Left < rect2.Left ? rect1.Left : rect2.Left;
            //int rightX = rect1.Left < rect2.Left ? rect2.Left + rect2.Width / 2 : rect1.Left + rect1.Width / 2;

            #region finding most top and bottom of rectangles
            int topY = rect1.Top < rect2.Top ? rect1.Top : rect2.Top;
            int bottomY = rect1.Bottom > rect2.Bottom ? rect1.Bottom : rect2.Bottom;
            #endregion

            #region finding lines
            List<Line> allMatchedlines = lines.FindAll(line => line.X1 > leftX && line.X2 < rightX);
            List<Line> matchedline = allMatchedlines.FindAll(line => line.Y1 < topY && line.Y2 > bottomY);
            #endregion

            return matchedline;
        }

        public bool AreRectsSeperatedByAnyHorizontalLine(Rectangle rect1, Rectangle rect2, List<Line> lines)
        {
            if (lines == null || lines.Count == 0)
                return false;

            List<Line> allMatchedlines = FindAllHorizontalLinesInBetweenRects(rect1
                , rect2
                , lines);

            if (allMatchedlines != null
                && allMatchedlines.Count > 0)
            {
                return true;
            }

            return false;
        }

        public List<Line> FindAllHorizontalLinesInBetweenRects(Rectangle rect1
            , Rectangle rect2
            , List<Line> lines)
        {
            if (lines == null || lines.Count == 0)
                return new List<Line>();

            int topY = rect1.Y < rect2.Y ? (rect1.Y + rect1.Height) : (rect2.Y + rect2.Height);
            int bottomY = rect1.Y > rect2.Y ? rect1.Y : rect2.Y;

            int centerX1 = (rect1.X + rect1.Width / 2);

            int left = rect1.X <= rect2.X ? rect1.X : rect2.X;
            int right = rect1.X > rect2.X ? rect1.X : rect2.X;

            List<Line> aaMatchlines = lines.FindAll(line => line.Y1 > topY && line.Y1 < bottomY);
            List<Line> matchline = aaMatchlines.FindAll(line =>
            {
                return ((line.X1 < centerX1 && line.X2 > centerX1)
                        || (line.X1 <= right && line.X2 >= left));
            });

            return matchline;
        }

        public int GetCenterDistanceBetweenRects(Rectangle rect1, Rectangle rect2)
        {
            int centerx1 = rect1.Left + (rect1.Width / 2);
            int centery1 = rect1.Top + (rect1.Height / 2);

            int centerx2 = rect2.Left + (rect2.Width / 2);
            int centery2 = rect2.Top + (rect2.Height / 2);

            return (int)Math.Sqrt(Math.Pow(centerx1 - centerx2, 2) + Math.Pow(centery1 - centery2, 2));
        }

        public int GetXDistanceBetweenRects(Rectangle rect1, Rectangle rect2)
        {
            if (rect2.Left >= rect1.Right)
            {
                return Math.Abs(rect2.Left - rect1.Right);
            }
            else if (rect1.Left >= rect2.Right)
            {
                return Math.Abs(rect1.Left - rect2.Right);
            }
            else
            {
                return Math.Abs(rect1.Left - rect2.Left);
            }
        }

        public int GetYDistanceBetweenRects(Rectangle rect1, Rectangle rect2)
        {
            if (rect2.Top >= rect1.Bottom)
            {
                return Math.Abs(rect2.Top - rect1.Bottom);
            }
            else if (rect1.Top >= rect2.Bottom)
            {
                return Math.Abs(rect1.Top - rect2.Bottom);
            }
            else
            {
                return Math.Abs(rect1.Top - rect2.Top);
            }
        }

        public int GetYCenterDistanceBetweenRects(Rectangle rect1, Rectangle rect2)
        {
            return Math.Abs((rect1.Y + rect1.Height / 2) - (rect2.Y + rect2.Height / 2));
        }

        public int GetCenterXOffsetBetweenRects(Rectangle rect1, Rectangle rect2)
        {
            return Math.Abs((rect1.Left + rect1.Width / 2) - (rect2.Left + rect2.Width / 2));
        }

        public bool IsHeightToleranceMatch(int sourceFieldHeight, int mappingFieldHeight, double heightInflate)
        {
            return sourceFieldHeight * (1 + heightInflate) > mappingFieldHeight &&
                   sourceFieldHeight * (1 - heightInflate) < mappingFieldHeight;
        }

        public bool IsPatternMatch(List<Point> markerPoint, List<Point> identifierPoint)
        {
            for (int i = 0; i < markerPoint.Count - 1; i++)
            {
                float angleBetweenMarkerPoints = CalculateAngle(markerPoint[i], markerPoint[i + 1]);
                float angleBetweenIdentifierPoints = CalculateAngle(identifierPoint[i], identifierPoint[i + 1]);

                int distanceOfMarkerPoint = DistanceBetweenTwoPoints(markerPoint[i], markerPoint[i + 1]);
                int distanceOfIdentifierPoint = DistanceBetweenTwoPoints(identifierPoint[i], identifierPoint[i + 1]);

                if (!IsAngleToleranceVarify(angleBetweenMarkerPoints, angleBetweenIdentifierPoints)
                    || !IsDistanceToleranceVarify(distanceOfMarkerPoint, distanceOfIdentifierPoint))
                {
                    VBotLogger.Trace(() => string.Format("[Angle/DistanceCalculation] Layout Marker Point {0} , Identifier Point {1}", markerPoint[i], identifierPoint[i]));
                    VBotLogger.Trace(() => string.Format("[Angle/DistanceCalculation] Layout verification failed at angle/distance calculation."));
                    return false;
                }
            }
            VBotLogger.Trace(() => string.Format("[Angle/DistanceCalculation] Layout verification success at angle/distance calculation."));
            return true;
        }

        public bool IsAngleToleranceVarify(float angleBetweenMarkerPoints, float angleBetweenIdentifierPoints)
        {
            return Math.Abs(angleBetweenMarkerPoints - angleBetweenIdentifierPoints) < AngleTolerance;
        }

        public bool IsDistanceToleranceVarify(int distanceOfMarkerPoint, int distanceOfIdentifierPoint)
        {
            return Math.Abs(distanceOfMarkerPoint - distanceOfIdentifierPoint) < DistanceTolerance;
        }

        public int DistanceBetweenTwoPoints(Point point1, Point point2)
        {
            return (int)Math.Sqrt(Math.Pow(point1.X - point2.X, 2) + Math.Pow(point1.Y - point2.Y, 2));
        }

        public float CalculateAngle(Point point1, Point point2)
        {
            return (float)((Math.Atan2(point2.Y - point1.Y, point2.X - point1.X)) * 180 / Math.PI);
        }

        public int CalculateXLocation(Rectangle identifierField, int layoutWidth, int docWidth)
        {
            double xRation = CalculateXRatioForResizeImage(layoutWidth, docWidth);
            return (int)(((double)(identifierField.X + (identifierField.Width / 2))) * xRation);
        }

        public int CalculateYLocation(Rectangle identifierField, int layoutHeight, int docHeight)
        {
            double yRation = CalculateYRatioForResizeImage(layoutHeight, docHeight);
            return (int)(((double)(identifierField.Y + (identifierField.Height / 2))) * yRation);
        }

        public double CalculateXRatioForResizeImage(int layoutWidth, int docWidth)
        {
            return (double)layoutWidth / (double)docWidth;
        }

        public double CalculateYRatioForResizeImage(int layoutHeight, int docHeight)
        {
            return (double)layoutHeight / (double)docHeight;
        }
        public Rectangle GetBoundsFromRects(List<Rectangle> rects)
        {
            if (rects == null || rects.Count == 0)
            {
                return new Rectangle();
            }

            int left = rects[0].Left;
            int top = rects[0].Top;
            int right = rects[0].Right;
            int bottom = rects[0].Bottom;

            for (int i = 1; i < rects.Count; i++)
            {
                if (rects[i].X < left)
                {
                    left = rects[i].X;
                }

                if (rects[i].Y < top)
                {
                    top = rects[i].Y;
                }

                int newRight = rects[i].X + rects[i].Width;

                if (newRight > right)
                {
                    right = newRight;
                }

                int newBottom = rects[i].Y + rects[i].Height;

                if (newBottom > bottom)
                {
                    bottom = newBottom;
                }
            }

            return new Rectangle(left, top, right - left, bottom - top);
        }
        public Rectangle GetBoundingRect(Rectangle marker, int imageWidth, int imageHeight, AspectRatio aspectRatio)
        {
            Rectangle boundingRect = new Rectangle();
            boundingRect.X = (int)(((double)marker.X) * aspectRatio.XRatio) - HorizontalImagePadding;
            boundingRect.Width = (int)(((double)marker.Width) * aspectRatio.XRatio) + (2 * HorizontalImagePadding);
            boundingRect.Y = (int)(((double)marker.Y) * aspectRatio.YRatio) - VerticalImagePadding;
            boundingRect.Height = (int)(((double)marker.Height) * aspectRatio.YRatio) + (2 * VerticalImagePadding);

            if (boundingRect.X < 0)
            {
                boundingRect.X = 0;
            }
            if (boundingRect.Y < 0)
            {
                boundingRect.Y = 0;
            }
            if (boundingRect.Width > imageWidth)
            {
                boundingRect.Width = imageWidth - boundingRect.X;
            }
            if (boundingRect.Height > imageHeight)
            {
                boundingRect.Height = imageHeight - boundingRect.Y;
            }

            return boundingRect;
        }

        public AspectRatio GetAspectRatio(int widthDoc1, int heightDoc1, int widthDoc2, int heightDoc2)
        {
            return new AspectRatio
            {
                XRatio = CalculateXRatioForResizeImage(widthDoc1, widthDoc2),
                YRatio = CalculateYRatioForResizeImage(heightDoc1, heightDoc2)
            };
        }

        public Rectangle GetMappedBoundingRectangle(Rectangle matchings, Rectangle boundingRect, AspectRatio aspectRatio)
        {
            Rectangle resultRect = new Rectangle();
            resultRect.X = boundingRect.X + (int)(((double)matchings.X) * aspectRatio.XRatio);
            resultRect.Y = boundingRect.Y + (int)(((double)matchings.Y) * aspectRatio.YRatio);
            resultRect.Width = (int)(((double)matchings.Width) * aspectRatio.XRatio);
            resultRect.Height = (int)(((double)matchings.Height) * aspectRatio.YRatio);
            return resultRect;
        }

        public int GetMostLeftOfTwoRectangle(Rectangle rect1, Rectangle rect2)
        {
            return rect1.Left < rect2.Left ? rect1.Left : rect2.Left;
        }

        public int GetMostRightOfTwoRectangle(Rectangle rect1, Rectangle rect2)
        {
            return rect1.Right > rect2.Right ? rect1.Right : rect2.Right;
        }

        public int GetMostTopOfTwoRectangle(Rectangle rect1, Rectangle rect2)
        {
            return rect1.Top < rect2.Top ? rect1.Top : rect2.Top;
        }

        public int GetMostBottomOfTwoRectangle(Rectangle rect1, Rectangle rect2)
        {
            return rect1.Bottom > rect2.Bottom ? rect1.Bottom : rect2.Bottom;
        }

        public Rectangle ReturnEmptyRect()
        {
            return (new Rectangle());
        }

        public Rectangle AddOffset(Rectangle rect, int horizontalOffset, int verticalOffset)
        {
            return new Rectangle(rect.X + horizontalOffset, rect.Y + verticalOffset, rect.Width, rect.Height);
        }

        public Rectangle AddOffsetWithScale(Rectangle rect, int horizontalOffset, int verticalOffset, double scaleX = 1.0, double scaleY = 1.0)
        {
            return new Rectangle(horizontalOffset + (int)(rect.X * scaleX), verticalOffset + (int)(rect.Y * scaleY), (int)(rect.Width * scaleX), (int)(rect.Height * scaleY));
        }

        public bool IsRectangleEmpty(Rectangle rect)
        {
            return Rectangle.Empty == rect;
        }

        public Point GetCenterPoint(Rectangle rect)
        {
            return new Point(rect.Location.X + rect.Width / 2,
                        rect.Location.Y + rect.Height / 2);
        }


    }

    internal static class RectangleExtentions
    {
        public static bool Contains(this Rectangle rect, Line line)
        {
            return (line.X1 >= rect.Left
                && line.X2 >= rect.Left
                && line.X1 <= rect.Right
                && line.X2 <= rect.Right
                && line.Y1 >= rect.Top
                && line.Y2 >= rect.Top
                && line.Y1 <= rect.Bottom
                && line.Y2 <= rect.Bottom);
        }

        public static Rectangle Clone(this Rectangle rect)
        {
            return new Rectangle(rect.Left
                , rect.Top
                , rect.Width
                , rect.Height);
        }

        public static bool IsHorizonatallyAlignedWithReferenceRect(this Rectangle rect, Rectangle referanceRect)
        {
            var rectangleOperation = new RectangleOperations();

            bool isRefRectOnSameHorizontalPlaneOfRect = rectangleOperation
                .GetYDistanceBetweenRects(referanceRect, rect) < rect.Height;
            if (isRefRectOnSameHorizontalPlaneOfRect)
            {
                return true;
            }

            bool isRefRectCenterOnSameHorizontalPlaneOfRect = rectangleOperation
                .GetCenterDistanceBetweenRects(referanceRect, rect) < rect.Height;
            if (isRefRectCenterOnSameHorizontalPlaneOfRect)
            {
                return true;
            }
            bool areRectsTopAlignedWithinTolerance = Math.Abs(referanceRect.Top - rect.Top) < rect.Height;
            if (areRectsTopAlignedWithinTolerance)
            {
                return true;
            }
            bool areRectsBottomAlignedWithinTolerance = Math.Abs(referanceRect.Bottom - rect.Bottom) < rect.Height;
            if (areRectsBottomAlignedWithinTolerance)
            {
                return true;
            }

            return false;
        }
    }
}