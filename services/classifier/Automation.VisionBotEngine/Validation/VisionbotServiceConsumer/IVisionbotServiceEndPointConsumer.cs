﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Validation.VisionbotServiceConsumer
{
    interface IVisionbotServiceEndPointConsumer
    {
        VisionBotEngine.Model.VisionBot LoadVisionBot(VisionBotManifest visionBotManifest);

        bool SaveVisionBot(VisionBotManifest visionBotManifest
            , VisionBotEngine.Model.VisionBot visionBot);

        VbotValidatorData GetValidationData(string visionbotID);

        bool SetValidationData(string visionbotID, VbotValidatorData validationData);

        Bitmap GetImage(string documentID, int pageIndex);

        VBotDocument ExtractVBotData(string visionbotID, string LayoutID);

        VBotDocument GenerateAndExtractVBotDocument(string visionbotID, string documentID);
    }
}
