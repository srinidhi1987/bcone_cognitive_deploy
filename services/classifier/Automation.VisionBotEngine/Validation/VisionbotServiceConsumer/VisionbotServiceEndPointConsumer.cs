﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.Services.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;
using System.Drawing;

namespace Automation.VisionBotEngine.Validation.VisionbotServiceConsumer
{
    internal class VisionbotServiceEndPointConsumer : IVisionbotServiceEndPointConsumer
    {
        private VisionbotServiceAdapter _client = null;

        private const string VISIONBOT_WITH_SLASH = "visionbot/";

        private string baseUri = "http://localhost:9998/";

        public VisionbotServiceEndPointConsumer()
        {
            _client = new VisionbotServiceAdapter(baseUri);
        }

        #region Interface Implementor

        public VbotValidatorData GetValidationData(string visionbotID)
        {
            string endPoint = getValidationDataAccessURI(visionbotID);

            var response = _client.Get<VbotValidatorData>(endPoint);
            return response;
        }

        public bool SetValidationData(string visionbotID, VbotValidatorData validationData)
        {
            string endPoint = getValidationDataAccessURI(visionbotID);
            var response = _client.Post<object, VbotValidatorData>(endPoint, validationData);
            return true;
        }

        public VisionBot LoadVisionBot(VisionBotManifest visionBotManifest)
        {
            string endPoint = getVisionBotDataAccessURI(visionBotManifest.Id);
            var response = _client.Get<Dictionary<string, object>>(endPoint);
            var visionBot = Newtonsoft.Json.JsonConvert.DeserializeObject<VisionBotEngine.Model.VisionBot>(response["datablob"].ToString());
            visionBot.Id = visionBotManifest.Id;
            return visionBot;
        }

        public bool SaveVisionBot(VisionBotManifest visionBotManifest, VisionBot visionBot)
        {
            string endPoint = getVisionBotDataAccessURI(visionBotManifest.Id);
            var response = _client.Post<object, VisionBotEngine.Model.VisionBot>(endPoint, visionBot);
            return true;
        }

        public Bitmap GetImage(string documentID, int pageIndex)
        {
            throw new NotImplementedException();
        }

        public VBotDocument ExtractVBotData(string visionbotID, string LayoutID)
        {
            throw new NotImplementedException();
        }

        public VBotDocument GenerateAndExtractVBotDocument(string visionbotID, string documentID)
        {
            throw new NotImplementedException();
        }

        #endregion Interface Implementor

        #region Private Methods

        private static string getVisionBotDataAccessURI(string visionbotID)
        {
            return $"/organizations/1/projects/1/visionbot/{visionbotID}";
        }

        private static string getValidationDataAccessURI(string visionbotID)
        {
            return $"visionbot/{visionbotID}/validationdata";
        }

        #endregion Private Methods
    }
}