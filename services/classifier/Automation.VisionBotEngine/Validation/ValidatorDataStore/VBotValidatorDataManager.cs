/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;



    [Obfuscation(Exclude = true, ApplyToMembers = false)]
    public class VBotValidatorDataManager : IVBotValidatorDataManager
    {
        #region Variables & Properties

        private VbotValidatorData VbotValidatorData { get; }

        //private string VBotFilePath => this.Storage.PackageSourceFile;

        //private readonly VbotValidatorFileStorage Storage;
        //private readonly IValidatorDataStorage<VbotValidatorData> Storage;
        VBotValidatorServiceStorage Storage;

        #endregion Variables & Properties

        #region Constructor
        //public VBotValidatorDataManager(string vbotFilePath)
        //{
        //    this.Storage = new VbotValidatorFileStorage(vbotFilePath);

        //    this.VbotValidatorData = this.Storage.Load();
        //}

        //public VBotValidatorDataManager(Stream vbotStream)
        //{
        //    this.Storage = new VbotValidatorFileStorage(vbotStream);

        //    this.VbotValidatorData = this.Storage.Load();
        //}
        public VBotValidatorDataManager(string visionBotId)
        {
            this.Storage = new VBotValidatorServiceStorage(visionBotId);

             this.VbotValidatorData = this.Storage.Load();
            if (this.VbotValidatorData == null)
            {
                this.VbotValidatorData = new VbotValidatorData();
                this.SaveState();
            }
           
        }

        #endregion Constructor

        #region IVBotValidatorDataManager implementations

        public IDictionary<Type, ValidatorData> GetAll(string id)
        {
            var validatorDatas = this.VbotValidatorData.ValidatorData.Where(item => item.ID == id);

            return validatorDatas.ToDictionary(validatorData => validatorData.TypeOfData, validatorData => validatorData.ValidatorData);
        }

        public T Get<T>(string id) where T : ValidatorData
        {
            if (id == null) { id = string.Empty; }

            var fieldValidatorData = this.GetFieldValidatorData<T>(id);

            return fieldValidatorData?.ValidatorData as T;
        }

        public void Set<T>(string id, T validatorData) where T : ValidatorData
        {
            if (validatorData == null)
            {
                throw new ArgumentNullException(nameof(validatorData));
            }

            if (typeof(ValidatorData) == validatorData.GetType())
            {
                throw new ArgumentException("object needs to be of a subtype of ValidationData", nameof(validatorData));
            }

            try
            {
                if (id == null) { id = string.Empty; }

                var fieldValidatorData = this.GetFieldValidatorData<T>(id);
                if (fieldValidatorData == null)
                {
                    var vData = new FieldValidatorData { ID = id };
                    vData.SetValidatorData(validatorData);
                    this.VbotValidatorData.ValidatorData.Add(vData);
                }
                else
                {
                    fieldValidatorData.SetValidatorData(validatorData);
                }

                this.SaveState();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VBotValidatorDataManager), "Set<T>");
            }
        }

        public void Remove<T>(string id) where T : ValidatorData
        {
            var fieldValidatorData = this.GetFieldValidatorData<T>(id);
            if (fieldValidatorData != null)
            {
                this.VbotValidatorData.ValidatorData.Remove(fieldValidatorData);
                this.SaveState();
            }
        }

        public void SaveState()
        {
            this.Storage.Save(this.VbotValidatorData);
        }

        #endregion IVBotValidatorDataManager implementations

        #region Internal Functionality

        private FieldValidatorData GetFieldValidatorData<T>(string id) where T : class
        {
            return this.VbotValidatorData.ValidatorData.FirstOrDefault(item => item.ID == id && item.ValidatorData is T);
        }

        #endregion Internal Functionality
    }
}