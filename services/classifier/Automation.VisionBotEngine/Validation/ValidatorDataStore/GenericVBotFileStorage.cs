/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.Generic.Compression;
    using Automation.Generic.Serialization;
    using Generic;
    using System;
    using System.IO;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public abstract class GenericVBotFileStorage<T> where T : class, new()
    {
        protected GenericVBotFileStorage(string packageSourceFile)
        {
            this.PackageSourceFile = packageSourceFile;
        }

        protected GenericVBotFileStorage(Stream packageStream)
        {
            this.PackageStream = packageStream;
        }

        protected string PackageSourceFile { get; }
        protected Stream PackageStream { get; }

        public void Save(T vBotDocSet)
        {
            using (var resourcePackage = new VResourcePackage())
            {
                this.CreateOrOpenResourcePackage(resourcePackage);

                this.UpdateContentsFile(vBotDocSet, resourcePackage, string.Empty);

                var resourceData = this.CreateResourceData(vBotDocSet, string.Empty);

                resourcePackage.AddResource(resourceData);
                resourcePackage.Close();
            }
        }

        public void Save(T testDocSet, string parentFolder)
        {
            using (var resourcePackage = new VResourcePackage())
            {
                this.CreateOrOpenResourcePackage(resourcePackage);

                this.UpdateContentsFile(testDocSet, resourcePackage, parentFolder);
                var resourceData = this.CreateResourceData(testDocSet, parentFolder);

                resourcePackage.AddResource(resourceData);
                resourcePackage.Close();
            }
        }

        public T Load(string parentFolder)
        {
            return this.GetVBotDocSet(parentFolder);
        }

        public T Load()
        {
            return this.GetVBotDocSet(string.Empty);
        }

        public void Delete(string parentFolder)
        {
            if (!string.IsNullOrWhiteSpace(parentFolder))
            {
                var docSetFilePath = Path.Combine(parentFolder, this.GetFileName());
                using (var resourcePackage = new VResourcePackage())
                {
                    this.CreateOrOpenResourcePackage(resourcePackage);

                    var resourceData = resourcePackage.GetResource(docSetFilePath);
                    var vBotDocSet = DeserializeData(resourceData);

                    this.PreDeleteCleanUp(vBotDocSet, resourcePackage, parentFolder);

                    this.DeleteFile(resourcePackage, docSetFilePath);
                    resourcePackage.Close();
                }
            }
        }

        protected void DeleteFile(VResourcePackage resourcePackage, string fileToDelete)
        {
            try
            {
                resourcePackage.DeleteFile(fileToDelete);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(GenericVBotFileStorage<T>), nameof(DeleteFile), $"Deleting file \"{fileToDelete}\"");
            }
        }

        private T GetVBotDocSet(string parentFolder)
        {
            T vBotDocSet = null;
            var docSetFilePath = Path.Combine(parentFolder, this.GetFileName());
            using (var resourcePackage = new VResourcePackage())
            {
                this.CreateOrOpenResourcePackage(resourcePackage);

                var resourceData = resourcePackage.GetResource(docSetFilePath);
                vBotDocSet = DeserializeData(resourceData);

                this.PostProcessDeserializedObject(vBotDocSet, resourceData, resourcePackage, parentFolder);

                resourcePackage.Close();
            }

            return vBotDocSet;
        }

        private void CreateOrOpenResourcePackage(VResourcePackage resourcePackage)
        {
            if (this.PackageStream != null)
            {
                resourcePackage.Open(this.PackageStream);
                return;
            }

            if (File.Exists(this.PackageSourceFile))
            {
                resourcePackage.Open(this.PackageSourceFile);
            }
            else
            {
                resourcePackage.Create(this.PackageSourceFile);
            }
        }

        private ResourceData CreateResourceData(T storageObject, string parentFolder)
        {
            return new ResourceData
            {
                UniqueId = Path.Combine(parentFolder, this.GetFileName()),
                Content = GetSerializedContent(storageObject),
                ContentType = this.GetMimeContentType(),
                FilePath = string.Empty
            };
        }

        private static Stream GetSerializedContent(T vBotDocSet)
        {
            ISerialization serialization = new JSONSerialization();
            return serialization.SerializeToStream(vBotDocSet);
        }

        private static T DeserializeData(ResourceData resourceData)
        {
            ISerialization serialization = new JSONSerialization();
            var result = resourceData.Content == null ? new T() : serialization.DeserializeFromStream<T>(resourceData.Content);

            return result;
        }

        private void UpdateContentsFile(T vBotDocSet, IResourcePackage resourcePackage, string parentFolder)
        {
            var docSetFileName = Path.Combine(parentFolder, this.GetFileName());
            var resourceData = resourcePackage.GetResource(docSetFileName);

            if (resourceData.Content != null)
            {
                var currentVbotDocSet = DeserializeData(resourceData);

                this.UpdateContentsForExistingFile(vBotDocSet, currentVbotDocSet, resourcePackage, parentFolder);

                resourcePackage.DeleteFile(docSetFileName);
            }
            else
            {
                this.AddContentsForNewFile(vBotDocSet, resourcePackage, parentFolder);
            }
        }

        protected abstract string GetMimeContentType();

        protected abstract string GetFileName();

        protected virtual void PostProcessDeserializedObject(
            T vBotDocSet,
            ResourceData resourceData,
            VResourcePackage resourcePackage,
            string parentFolder)
        { }

        protected virtual void PreDeleteCleanUp(T vBotDocSet, VResourcePackage resourcePackage, string parentFolder)
        {
        }

        protected virtual void UpdateContentsForExistingFile(
            T vBotDocSet,
            T currentVbotDocSet,
            IResourcePackage resourcePackage,
            string parentFolder)
        { }

        protected virtual void AddContentsForNewFile(
            T vBotDocSet,
            IResourcePackage resourcePackage,
            string parentFolder)
        { }

        internal static class StandardMimeTypes
        {
            public static readonly string Json = "text/json";
        }
    }
}