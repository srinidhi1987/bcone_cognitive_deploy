﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Validation
{
    internal interface IValidatorDataStorage<T>
    {
        T Load();

        void Save(T validationData);
    }
}
