﻿using Automation.Services.Client;
using Automation.VisionBotEngine.Validation;
using Automation.VisionBotEngine.Validation.VisionbotServiceConsumer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Validation
{
    internal class VBotValidatorServiceStorage : IValidatorDataStorage<VbotValidatorData>
    {

        private string _visionbotID = string.Empty;
        private IVisionbotServiceEndPointConsumer _visionbotServiceEndPointConsumer = null;
        public VBotValidatorServiceStorage(string visionbotID)
        {
            _visionbotID = visionbotID;
            _visionbotServiceEndPointConsumer = new VisionbotServiceEndPointConsumer();
        }
        public VbotValidatorData Load()
        {
           return _visionbotServiceEndPointConsumer.GetValidationData(_visionbotID);
        }
        public void Save(VbotValidatorData valdidationData)
        {
            _visionbotServiceEndPointConsumer.SetValidationData(_visionbotID,valdidationData);
        }
    }
}
