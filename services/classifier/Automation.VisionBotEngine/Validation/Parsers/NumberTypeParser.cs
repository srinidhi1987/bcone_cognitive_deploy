/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine.AutoCorrect.Number;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.RegularExpressions;

    internal class NumberTypeParser : RegExPatternParser<decimal>
    {
        public override decimal? Parse(string value)
        {
            value = GetSanitizeNumberForNegativesContainingBrackets(value);
            value = ConvertToProperNegativeValue(value);
            var culturesAlreadyProcessed = new List<CultureInfo>();

            foreach (var cultureInfo in CultureHelper.SupportedNumberCultures())
            {
                if (NumericHelper.IsSimilarNumberCulturePresent(cultureInfo, culturesAlreadyProcessed.ToArray()))
                {
                    continue;
                }

                var parsedValue = NumericHelper.ParseDecimal(value, cultureInfo);
                if (parsedValue.HasValue)
                {
                    return parsedValue;
                }

                culturesAlreadyProcessed.Add(cultureInfo);
            }

            return null;
        }

        private string GetSanitizeNumberForNegativesContainingBrackets(string value)
        {
            if (value.StartsWith("(") && value.EndsWith(")"))
            {
                if (IsNumericOrDecimalWithBrackets(value))
                {
                    char[] chars = { '(', ')' };
                    value = value.Trim(chars); //remove brackets
                    value = value.Trim();      //remove trailing spaces
                    value = "-" + value;
                }
            }
            return value;
        }

        public override decimal? Parse(string value, string pattern)
        {
            value = GetSanitizeNumberForNegativesContainingBrackets(value);
            value = ConvertToProperNegativeValue(value);
            var considerRegEx = string.IsNullOrWhiteSpace(pattern) ||
                NumberPattern.ShouldPatternBeConsideredAsRegEx(pattern);

            NumberPattern objPattern = null;
            if (!considerRegEx)
            {
                objPattern = new NumberPattern(pattern);
                if (!objPattern.IsNumberPatternValid())
                {
                    considerRegEx = true;
                }
            }

            if (considerRegEx)
            {
                return base.Parse(value, pattern);
            }

            #region Number Pattern route

            {
                if (IsValidConvertedNumber(value, objPattern))
                {
                    return ConvertToDecimal(value);
                }

                var autoCorrector = new PatternBasedNumber(objPattern, value);
                if (autoCorrector.IsNumberValid()) return ConvertToDecimal(autoCorrector.GetOutputValue());

                autoCorrector.ApplyAutoCorrection();
                var newNumericValue = autoCorrector.GetOutputValue();
                if (autoCorrector.IsNumberValid())
                {
                    VBotLogger.Trace(
                        () =>
                        $"[Number Type Validator] [Number re-assignment] pattern=\"{pattern}\", detected=\"{autoCorrector.OriginalNumber}\", auto-corrected=\"{autoCorrector.ProcessedNumber}\", output=\"{newNumericValue}\"");
                    return ConvertToDecimal(newNumericValue);
                }
                else
                {
                    VBotLogger.Trace(
                        () =>
                        $"[Number Type Validator] [Number not re-assigned] pattern=\"{pattern}\", detected=\"{autoCorrector.OriginalNumber}\", auto-corrected=\"{autoCorrector.ProcessedNumber}\"");
                    return null;
                }
            }

            #endregion Number Pattern route
        }

        private string ConvertToProperNegativeValue(string value)
        {
            if (value.EndsWith("-") && !value.StartsWith("-") && value.Length > 1 && Char.IsNumber(value[value.Length - 2]))
            {
                string numericString = value.Substring(0, value.Length - 1);
                if (IsNumericOrDecimalWithoutBrackets(numericString))
                {
                    value = numericString;
                    value = "-" + value;
                }
            }
            return value;
        }

        private bool IsNumericOrDecimalWithoutBrackets(string value)
        {
            return Regex.IsMatch(value, @"^.*?[^\d]*\s*(\d+)\s*.*\s*[^\d]*\s*.*$");
        }

        private static bool IsNumericOrDecimalWithBrackets(string value)
        {
            return Regex.IsMatch(value, @"^.*?\([^\d]*\s*(\d+)\s*.*\s*[^\d]*\s*\).*$");
        }

        private static bool IsValidConvertedNumber(string value, NumberPattern pattern)
        {
            var convertedPattern = pattern.GetOnlyNumberPattern();
            var objConvertedPattern = new NumberPattern(convertedPattern)
            {
                ParsedPattern =
                                                  {
                                                      PrefixComponent = new NoNumberPatternPrefixComponent(),
                                                      PostfixComponent = new NoNumberPatternPostfixComponent()
                                                  }
            };
            var engine = new PatternBasedNumber(objConvertedPattern, value);
            return engine.IsNumberValid();
        }

        private static decimal? ConvertToDecimal(string value)
        {
            decimal parsedValue;
            if (decimal.TryParse(value, out parsedValue))
            {
                return parsedValue;
            }

            return null;
        }
    }
}