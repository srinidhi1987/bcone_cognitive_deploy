/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Globalization;

    internal class DateTypeParser : IParsableType<DateTime>
    {
        public DateTime? Parse(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            foreach (var supportedFormat in CultureHelper.SupportedDateTimeCultures())
            {
                DateTime parsedValue;
                if (DateTime.TryParse(value, supportedFormat, DateTimeStyles.None, out parsedValue))
                {
                    return parsedValue;
                }
            }

            return null;
        }

        public DateTime? Parse(string value, string pattern)
        {
            if (string.IsNullOrWhiteSpace(pattern))
            {
                return this.Parse(value);
            }

            DateTime parsedValue;
            return DateTime.TryParseExact(value, this.SanitizePattern(pattern), CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedValue)
                ? parsedValue
                : (DateTime?)null;
        }

        private string SanitizePattern(string rawPattern)
        {
            return rawPattern.ToLower().Replace('m', 'M');
        }
    }
}