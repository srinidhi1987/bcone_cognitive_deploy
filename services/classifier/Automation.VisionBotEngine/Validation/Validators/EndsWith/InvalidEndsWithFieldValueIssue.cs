/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Reflection;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class InvalidEndsWithFieldValueIssue : ValidationIssue
    {
        public InvalidEndsWithFieldValueIssue(string applicableEndWith)
            : base(FieldDataValidationIssueCodes.InvalidEndsWithFieldValue)
        {
            if (string.IsNullOrWhiteSpace(applicableEndWith))
            {
                throw new ArgumentException(
                    $"parameter {nameof(applicableEndWith)} cannot be null or empty or whitespace");
            }

            this.ApplicableEndWith = applicableEndWith;
        }

        public string ApplicableEndWith { get; private set; }
    }
}