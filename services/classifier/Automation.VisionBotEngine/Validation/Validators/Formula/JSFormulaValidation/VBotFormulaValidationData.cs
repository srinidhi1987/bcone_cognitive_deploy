﻿/**
* Copyright (c) 2015 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System.Collections.Generic;
using System.Reflection;

namespace Automation.VisionBotEngine.Validation.JSFormulaValidation
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    class VBotFormulaValidationData
    {
        public IList<FieldsData> Fields { get; set; } = new List<FieldsData>();

        public IList<Table> Tables { get; set; } = new List<Table>();

        public IList<Rules> Rules { get; set; } = new List<Rules>();
    }
}
