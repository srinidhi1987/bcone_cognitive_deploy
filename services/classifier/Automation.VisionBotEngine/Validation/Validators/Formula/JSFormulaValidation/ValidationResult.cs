﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation.JSFormulaValidation
{
    using System.Reflection;

    using Automation.VisionBotEngine;

    internal class ValidationResult
    {
        public ValidationResult(string result)
        {
            bool isSuccess;
            if (!bool.TryParse(result, out isSuccess))
            {
                this.Error = result;
            }
            this.IsSuccess = isSuccess;
        }

        public bool IsSuccess { get; }
        public string Error { get; }

        public FieldDataValidationIssueCodes? ValidationIssueCode
        {
            get
            {
                if (this.IsSuccess) return null;

                if (string.IsNullOrWhiteSpace(this.Error))
                {
                    return FieldDataValidationIssueCodes.FormulaValueMismatch;
                }

                VBotLogger.Trace(() => $"[Formula Validation] [{nameof(FieldDataValidationIssueCodes.InvalidFormula)}] Reason: {this.Error}");
                return FieldDataValidationIssueCodes.InvalidFormula;
            }
        }

        public override string ToString()
        {
            return string.Format(
                "IsSuccess: \"{0}\", ErrorMessage: \"{1}\", IssueCode: \"{2}\"",
                this.IsSuccess,
                this.Error,
                this.ValidationIssueCode.HasValue ? this.ValidationIssueCode.Value.ToString() : "null");
        }
    }
}
