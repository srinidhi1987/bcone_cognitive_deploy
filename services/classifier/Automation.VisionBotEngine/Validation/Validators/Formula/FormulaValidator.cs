﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation.Helpers;

    internal class FormulaValidator : IFieldValidator
    {
        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vbotValidatorDataReader)
        {
            if (fieldData.ValidationIssue != null)
            {
                return fieldData.ValidationIssue;
            }

            if (!fieldData.Field.IsFormulaValidationAvailable() ||
                string.IsNullOrWhiteSpace(fieldData.Value.Field.Text) ||
                string.IsNullOrWhiteSpace(fieldData.Field.Formula))
            {
                return null;
            }

            return ValidationDataFiller.FormulaValidationManager.ValidateField(fieldData, vbotData);
        }
    }
}
