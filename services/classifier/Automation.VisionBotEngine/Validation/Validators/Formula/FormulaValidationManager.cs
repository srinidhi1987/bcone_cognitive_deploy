﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation.JSFormulaValidation;
    using System;
    using System.Collections.Generic;

    internal class FormulaValidationManager
    {
        private IDictionary<FieldData, ValidationResult> ValidationResult { get; set; }

        private VBotData VbotData { get; set; }

        public ValidationIssue ValidateField(FieldData fieldData, VBotData vbotData)
        {
            try
            {
                if (this.ValidationResult == null || vbotData != this.VbotData)
                {
                    this.VbotData = vbotData;
                    this.ValidationResult = (new FormulaValidationManagerHelper()).GetValidationResult(vbotData);
                }

                if (this.ValidationResult.ContainsKey(fieldData))
                {
                    var result = this.ValidationResult[fieldData];

                    VBotLogger.Trace(() => $"[Formula Validation] field: \"{fieldData.Field.Name}\", result: \"{result}\"");

                    if (!result.ValidationIssueCode.HasValue)
                    {
                        return result.ValidationIssueCode.ToValidationIssue();
                    }

                    switch (result.ValidationIssueCode.Value)
                    {
                        case FieldDataValidationIssueCodes.InvalidFormula:
                            return new InvalidFormulaIssue(fieldData.Field.Formula, result.Error);

                        case FieldDataValidationIssueCodes.FormulaValueMismatch:
                            return new FormulaValueMismatchIssue(fieldData.Field.Formula);
                    }

                    return result.ValidationIssueCode.ToValidationIssue();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(FormulaValidationManager), nameof(ValidateField));
            }

            return FieldDataValidationIssueCodes.ValidationEngineIssue.ToValidationIssue();
        }
    }
}