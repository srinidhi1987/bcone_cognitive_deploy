/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.AutoCorrect.Number;
    using Automation.VisionBotEngine.Model;
    using System.Globalization;

    internal class NumberTypeValidator : IFieldValidator
    {
        private IParsableType<decimal> Parser => new NumberTypeParser();

        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vBotValidatorDataReader)
        {
            if (fieldData.Field.ValueType != FieldValueType.Number
                || string.IsNullOrWhiteSpace(fieldData.Value.Field.Text))
            {
                return null;
            }

            var pattern = fieldData.Field.GetApplicablePattern(layout);

            var considerRegEx = string.IsNullOrWhiteSpace(pattern) ||
                NumberPattern.ShouldPatternBeConsideredAsRegEx(pattern);

            var value = this.Parser.Parse(fieldData.Value.Field.Text, pattern);
            if (value.HasValue)
            {
                var newNumericValue = this.GetOutputNumberString(value.Value);
                ChangeAndLogIfNewValueIsDifferent(fieldData, newNumericValue);
                return null;
            }

            if (!string.IsNullOrWhiteSpace(pattern))
            {
                return new PatternMismatchIssue(pattern);
            }

            return FieldDataValidationIssueCodes.InvalidNumberTypeValue.ToValidationIssue();
        }

        private static void ChangeAndLogIfNewValueIsDifferent(FieldData fieldData, string newNumericValue)
        {
            if (fieldData.Value.Field.Text == newNumericValue) return;

            VBotLogger.Trace(() =>
                $"[Number Type Validator] [FieldId=\"{fieldData.Field.Id}\"] [Number re-assignment] detected=\"{fieldData.Value.Field.Text}\", changed to=\"{newNumericValue}\" ");

            fieldData.Value.Field.Text = newNumericValue;
        }

        protected string GetOutputNumberString(decimal value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}