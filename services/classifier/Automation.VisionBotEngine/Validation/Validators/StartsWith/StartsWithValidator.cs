/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    internal class StartsWithValidator : IFieldValidator
    {
        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vBotValidatorDataReader)
        {
            if (string.IsNullOrWhiteSpace(fieldData.Value.Field.Text))
            {
                return null;
            }

            var specifiedStartsWith = fieldData.Field.GetApplicableStartWith(layout);

            var issue = string.IsNullOrWhiteSpace(specifiedStartsWith)
                        || fieldData.Value.Field.Text.TrimStart().StartsWith(specifiedStartsWith.Trim(), StringComparison.OrdinalIgnoreCase)
                ? (FieldDataValidationIssueCodes?)null
                : FieldDataValidationIssueCodes.InvalidStartsWithFieldValue;

            return issue.HasValue ? new InvalidStartsWithFieldValueIssue(specifiedStartsWith) : null;
        }
    }
}