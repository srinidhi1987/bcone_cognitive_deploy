/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    internal class RequiredFieldValidator : IFieldValidator
    {
        public ValidationIssue Validate(FieldData fieldData, Layout layout, VBotData vbotData, IVBotValidatorDataReader vBotValidatorDataReader)
        {
            return (fieldData.Field.IsRequired && string.IsNullOrWhiteSpace(fieldData.Value.Field.Text)
                ? FieldDataValidationIssueCodes.MissingRequiredFieldValue
                : (FieldDataValidationIssueCodes?)null)
                .ToValidationIssue();
        }
    }
}