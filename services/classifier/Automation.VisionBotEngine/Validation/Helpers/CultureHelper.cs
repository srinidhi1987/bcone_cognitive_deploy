/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 
namespace Automation.VisionBotEngine.Validation
{
    using System.Collections.Generic;
    using System.Globalization;

    internal static class CultureHelper
    {
        /* For a list of culture codes refer https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx */
        /* For a list of date formats across different countries refer https://en.wikipedia.org/wiki/Date_format_by_country */
        /* Different number formats refer http://userguide.icu-project.org/formatparse */

        public static IEnumerable<CultureInfo> SupportedDateTimeCultures()
        {
            return new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("en-AU"),
                CultureInfo.CurrentCulture
            };
        }

        public static IEnumerable<CultureInfo> SupportedNumberCultures()
        {
            return new[]
            {
                new CultureInfo("en-US"),
                //new CultureInfo("de-DE"),
                //new CultureInfo("fr-FR"),
                CultureInfo.CurrentCulture
            };
        }
    }
}