/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Automation.VisionBotEngine.Model;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class ValidationResultHelper
    {
        public static VBotDataValidationResultTypes GetValidationResult(this VBotData vBotData)
        {
            var fieldDataHavingIssue = vBotData.GetAllInvalidFieldData();

            if (fieldDataHavingIssue == null || fieldDataHavingIssue.Count <= 0)
            {
                return VBotDataValidationResultTypes.Success;
            }

            return fieldDataHavingIssue
                .Select(fieldData => fieldData.ValidationIssue)
                .Any(validationIssue => validationIssue.GetIssueType() == ValidationIssueCodeTypes.Error) 
                ? VBotDataValidationResultTypes.Failure 
                : VBotDataValidationResultTypes.Warning;
        }

        public static List<FieldData> GetAllInvalidFieldData(this VBotData vBotData)
        {
            var result = new List<FieldData>();

            result.AddRange(vBotData.FieldDataRecord.GetAllInvalidFieldData());

            foreach (var subResult in vBotData
                .TableDataRecord
                .Select(tableValue => tableValue.GetAllInvalidFieldData())
                .Where(subResult => subResult != null && subResult.Count > 0))
            {
                result.AddRange(subResult);
            }

            return result;
        }

        public static List<FieldData> GetAllInvalidFieldData(this TableValue tableValue)
        {
            var result = new List<FieldData>();

            if (tableValue == null)
            {
                return result;
            }

            foreach (var subResult in tableValue.Rows
                .Select(GetAllInvalidFieldData)
                .Where(subResult => subResult != null && subResult.Count > 0))
            {
                result.AddRange(subResult);
            }

            return result;
        }

        public static List<FieldData> GetAllInvalidFieldData(this DataRecord dataRecord)
        {
            return dataRecord?.Fields
                .Where(fieldData => fieldData.ValidationIssue.HasValue())
                .ToList() 
                ?? new List<FieldData>();
        }
    }
}