/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation.Helpers
{
    using Automation.VisionBotEngine.Model;

    internal static class ValidationAvailabilityDecider
    {
        public static bool IsFormulaValidationAvailable(this FieldDef fieldDef)
        {
            return fieldDef.ValueType == FieldValueType.Number;
        }

        public static bool IsListValidationAvailable(this FieldDef fieldDef)
        {
            return fieldDef.ValueType == FieldValueType.Text;
        }
    }
}