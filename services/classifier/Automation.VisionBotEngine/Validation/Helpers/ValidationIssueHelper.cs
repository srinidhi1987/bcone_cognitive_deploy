﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System.Reflection;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class ValidationIssueHelper
    {
        public static bool HasValue(this ValidationIssue validationIssue)
        {
            return validationIssue != null;
        }

        public static FieldDataValidationIssueCodes? Value(this ValidationIssue validationIssue)
        {
            return validationIssue?.IssueCode;
        }

        public static ValidationIssue ToValidationIssue(this FieldDataValidationIssueCodes? issueCode)
        {
            return issueCode?.ToValidationIssue();
        }

        public static ValidationIssue ToValidationIssue(this FieldDataValidationIssueCodes issueCode)
        {
            return new ValidationIssue(issueCode);
        }

        public static ValidationIssueCodeTypes? GetIssueType(this ValidationIssue validationIssue)
        {
            return validationIssue?.IssueCode.GetIssueType();
        }
    }
}