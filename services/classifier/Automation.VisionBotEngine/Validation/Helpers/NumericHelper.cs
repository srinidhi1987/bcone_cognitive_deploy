/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Validation
{
    using System;
    using System.Globalization;
    using System.Linq;

    internal static class NumericHelper
    {
        public static bool IsNumberOfIndianStyleThousandSeperated(string value, string thousandSeparator, string decimalSeparator)
        {
            return IsNumberProperlyThousandSeperated(value, thousandSeparator, decimalSeparator, 3, 2);
        }

        private static bool IsNumberProperlyThousandSeperated(string value, string thousandSeparator, string decimalSeparator, int startGroupLength, int nextGroupLength)
        {
            var indexOfDecimalSeparator = value.IndexOf(decimalSeparator, StringComparison.Ordinal);
            if (indexOfDecimalSeparator >= 0)
            {
                value = value.Substring(0, indexOfDecimalSeparator);
            }

            var split = value.Split(new[] { thousandSeparator }, StringSplitOptions.None);

            for (var splitGroupIndex = split.Length - 1; splitGroupIndex >= 0; splitGroupIndex--)
            {
                #region LAST Item
                if (splitGroupIndex == split.Length - 1) 
                {
                    if (split[splitGroupIndex].Trim().Length != startGroupLength) return false;
                }
                #endregion

                #region FIRST Item
                else if (splitGroupIndex == 0) 
                {
                    if (split[splitGroupIndex].Trim().Length > nextGroupLength) return false;
                }
                #endregion

                #region OTHER Items
                else
                {
                    if (split[splitGroupIndex].Trim().Length != nextGroupLength) return false;
                }
                #endregion
            }

            return true;
        }

        public static bool IsNumberOfUSStyleThousandSeparated(string value, string thousandSeparator, string decimalSeparator)
        {
            return IsNumberProperlyThousandSeperated(value, thousandSeparator, decimalSeparator, 3, 3);
        }

        public static decimal? ParseDecimal(string value)
        {
            return ParseDecimal(value, CultureInfo.CurrentCulture);
        }
        public static decimal? ParseDecimal(string value, CultureInfo cultureInfo)
        {
            return ParseDecimal(
                value,
                cultureInfo.NumberFormat.NumberGroupSeparator,
                cultureInfo.NumberFormat.NumberDecimalSeparator);
        }

        private static bool DidValueGetChanged(ref string originalValue, Func<string, string> operation)
        {
            var valueBeforeOperation = originalValue;
            var valueAfterOperation = operation(originalValue);

            originalValue = valueAfterOperation;

            return valueBeforeOperation != valueAfterOperation;
        }

        public static decimal? ParseDecimal(string value, string thousandSeparator, string decimalSeparator)
        {
            if (string.IsNullOrWhiteSpace(value) ||
                value.Contains(string.Format("{0}{0}", thousandSeparator)))
            {
                return null;
            }

            var isPercentageSymbolRemoved = DidValueGetChanged(ref value, NumericHelper.RemovePercentageSymbol);

            var isCurrencySymbolRemoved = DidValueGetChanged(ref value, CurrencyHelper.RemoveCurrencySymbol);

            if (isPercentageSymbolRemoved && isCurrencySymbolRemoved)
            {
                return null;
            }

            if (value.Contains(decimalSeparator))
            {
                var decimalSplitComponents = value.Split(new[] { decimalSeparator }, StringSplitOptions.None);

                if (decimalSplitComponents.Length > 2 ||
                    decimalSplitComponents[1].Contains(thousandSeparator))
                {
                    return null;
                }
            }

            value = value.Trim();

            var isNegativeNumber = value.StartsWith("-");
            if (isNegativeNumber)
            {
                value = value.Substring(1);
            }

            value = value.Trim();

            if (value.Contains(thousandSeparator))
            {
                if (value.StartsWith(thousandSeparator) ||
                    value.EndsWith(thousandSeparator) ||
                    value.EndsWith(decimalSeparator) ||
                    value.Contains($"{thousandSeparator}{decimalSeparator}"))
                {
                    return null;
                }

                if (IsNumberOfUSStyleThousandSeparated(value, thousandSeparator, decimalSeparator) ||
                    IsNumberOfIndianStyleThousandSeperated(value, thousandSeparator, decimalSeparator))
                {
                    value = value.Replace(thousandSeparator, string.Empty);
                }
                else
                {
                    return null;
                }
            }

            if (isNegativeNumber)
            {
                value = "-" + value;
            }

            decimal parsedValue;
            if (decimal.TryParse(value, out parsedValue))
            {
                return parsedValue;
            }

            return null;
        }

        public static bool IsSimilarNumberCulturePresent(
            CultureInfo cultureToCheckFor,
            params CultureInfo[] culturesToCheckAgainst)
        {
            var groupSeparator = cultureToCheckFor.NumberFormat.NumberGroupSeparator;
            var decimalSeparator = cultureToCheckFor.NumberFormat.NumberDecimalSeparator;

            return culturesToCheckAgainst.Any(
                cultureInfo => groupSeparator == cultureInfo.NumberFormat.NumberGroupSeparator && 
                decimalSeparator == cultureInfo.NumberFormat.NumberDecimalSeparator);
        }

        public static string RemovePercentageSymbol(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            value = value.Trim();

            if (value.EndsWith("%"))
            {
                return value.Substring(0, value.Length - 1).Trim();
            }

            return value;
        }
    }
}