﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.PdfEngine
{
    internal class PDFRegion
    {
        public string Text { get; set; }
        public SegmentationType Type { get; set; }
        public int Confidence { get; set; }
        public PointRectangle Bounds { get; set; }
        public decimal Angle { get; set; }
        public float textAngle { get; set; }
        //public Rectangle Rect
        //{
        //    get
        //    {
        //        return new Rectangle((int)Bounds.LeftTop.X, (int)Bounds.LeftTop.Y, (int)Math.Abs(Bounds.LeftTop.X - Bounds.RightTop.X), (int)Math.Abs(Bounds.LeftTop.Y - Bounds.LeftBottom.Y));
        //    }
        //}

        public Rectangle Rect
        {
            get; set;
        }

        public PDFRegion()
        {
            Text = string.Empty;
            Type = SegmentationType.Word;
            Confidence = 0;
            Bounds = new PointRectangle();
        }
    }

    internal class PointRectangle
    {
        public PointF LeftTop { get; set; }
        public PointF RightTop { get; set; }
        public PointF RightBottom { get; set; }
        public PointF LeftBottom { get; set; }

        public void Rotate(PointF relativePointForRotation, PointF relativePointForTransform, double angle)
        {
            LeftTop = rotatePoint(LeftTop, relativePointForRotation, relativePointForTransform, angle);
            RightTop = rotatePoint(RightTop, relativePointForRotation, relativePointForTransform, angle);
            RightBottom = rotatePoint(RightBottom, relativePointForRotation, relativePointForTransform, angle);
            LeftBottom = rotatePoint(LeftBottom, relativePointForRotation, relativePointForTransform, angle);
        }

        private PointF rotatePoint(PointF pointToRotate, PointF relativePointForRotation, PointF relativePointForTransform, double angleInDegrees)
        {
            double angleInRadians = angleInDegrees * (Math.PI / 180); // Math.Atan(angleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new PointF
            {
                X =
                    (float)
                    (cosTheta * (pointToRotate.X - relativePointForRotation.X) -
                    sinTheta * (pointToRotate.Y - relativePointForRotation.Y) + relativePointForTransform.X),
                Y =
                    (float)
                    (sinTheta * (pointToRotate.X - relativePointForRotation.X) +
                    cosTheta * (pointToRotate.Y - relativePointForRotation.Y) + relativePointForTransform.Y)
            };
        }
    }
}