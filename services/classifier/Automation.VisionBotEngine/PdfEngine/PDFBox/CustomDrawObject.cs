﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using org.apache.pdfbox.contentstream.@operator;
using org.apache.pdfbox.contentstream.@operator.graphics;
using org.apache.pdfbox.cos;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.pdmodel.graphics;
using org.apache.pdfbox.pdmodel.graphics.form;
using org.apache.pdfbox.pdmodel.graphics.image;
using System;

namespace Automation.VisionBotEngine.PdfEngine
{
    public sealed class CustomDrawObject : GraphicsOperatorProcessor
    {
        private const string OPERATION_NAME = "Do";
        public override string getName()
        {
            return OPERATION_NAME;
        }

        public override void process(Operator o, java.util.List operands)
        {
            throwExceptionIfOperationIsNotValid(o, operands);

            COSBase base0 = operands.get(0) as COSBase;
            if (!(base0 is COSName))
            {
                return;
            }

            COSName objectName = (COSName)base0;
            PDXObject xobject = null;
            try
            {
                xobject = context.getResources().getXObject(objectName);
            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => $"[CustomDrawObject][process] handled case for image creation");
                return;
            }

            if (xobject == null)
            {
                throw new MissingResourceException("Missing XObject: " + objectName.getName());
            }
            else if (xobject is PDImageXObject)
            {
                PDImageXObject image = (PDImageXObject)xobject;

                context.drawImage(image);
            }
            else if (xobject is PDTransparencyGroup)
            {
                getContext().showTransparencyGroup((PDTransparencyGroup)xobject);
            }
            else if (xobject is PDFormXObject)
            {
                getContext().showForm((PDFormXObject)xobject);
            }
        }

        private static void throwExceptionIfOperationIsNotValid(Operator o, java.util.List operands)
        {
            if (operands.size() < 1)
            {
                throw new MissingOperandException(o, operands);
            }
        }
    }
}
