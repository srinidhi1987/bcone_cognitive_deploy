﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using Automation.VisionBotEngine;
using Automation.VisionBotEngine.PdfEngine;
using java.awt.geom;
using java.io;
using java.lang;
using java.util;
using org.apache.pdfbox.contentstream;
using org.apache.pdfbox.contentstream.@operator;
using org.apache.pdfbox.contentstream.@operator.state;
using org.apache.pdfbox.cos;
using org.apache.pdfbox.filter;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.pdmodel.common;
using org.apache.pdfbox.pdmodel.font;
using org.apache.pdfbox.pdmodel.graphics.form;
using org.apache.pdfbox.pdmodel.graphics.image;
using org.apache.pdfbox.pdmodel.interactive.annotation;
using org.apache.pdfbox.util;
using System.Collections.Generic;
using System.Drawing;

namespace Automation.VisionBotEngine.PdfEngine
{
    public sealed class CustomPDFGraphicsStreamEngine : PDFGraphicsStreamEngine
    {
        // clipping winding rule used for the clipping path
        private int clipWindingRule;
        private GeneralPath linePath;

        private PDRectangle pageSize;
        float xStart;
        float yStart;
        internal List<PDFRegion> pdfRegions = null;

        private readonly float lineThickNess;

        #region Constructor

        public CustomPDFGraphicsStreamEngine(PDPage page) : base(page)
        {
            clipWindingRule = -1;
            linePath = new GeneralPath();
            pdfRegions = new List<PDFRegion>();
            pageSize = this.getPage().getMediaBox();
            xStart = 0;
            yStart = 0;
            lineThickNess = (float)(PDFBoxEngine.LineThickNess / PDFBoxEngine.DpiScale);

            overridingOperatorsForHandlingCustomScenario();
        }

        #endregion

        internal List<PDFRegion> GetLines()
        {
            VBotLogger.Trace(() => $"[CustomPDFGraphicsStreamEngin][GetLines] Entered");
            pdfRegions.Clear();

            try
            {
                processPage(this.getPage());
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[CustomPDFGraphicsStreamEngin][GetLines] Exception Occured {ex.Message}");
            }
            VBotLogger.Trace(() => $"[CustomPDFGraphicsStreamEngin][GetLines] Exited");

            return pdfRegions;
        }

        #region Public Overrided Methods (Used)

        protected override void operatorException(Operator @operator, List operands, IOException e)
        {
            VBotLogger.Warning(() => $"[CustomPDFGraphicsStreamEngine][operatorException] operator :{@operator.getName()}. Message : {e.getMessage()}");
        }

        public override void moveTo(float x, float y)
        {
            linePath.moveTo(x, y);
            xStart = x;
            yStart = y;
        }

        public override void lineTo(float x, float y)
        {
            linePath.lineTo(x, y);
            //  g.DrawRectangle(new Pen(Brushes.Red), x, y, 10, 10);
            //  g.DrawLine(new Pen(Brushes.Black), xStart, yStart, x, y);
        }

        public override void appendRectangle(Point2D p0
          , Point2D p1
          , Point2D p2
          , Point2D p3)
        {
            // to ensure that the path is created in the right direction, we have to create
            // it by combining single lines instead of creating a simple rectangle
            linePath.moveTo((float)p0.getX(), (float)p0.getY());
            linePath.lineTo((float)p1.getX(), (float)p1.getY());
            linePath.lineTo((float)p2.getX(), (float)p2.getY());
            linePath.lineTo((float)p3.getX(), (float)p3.getY());
            linePath.lineTo((float)p0.getX(), (float)p0.getY());

            // close the subpath instead of adding the last line so that a possible set line
            // cap style isn't taken into account at the "beginning" of the rectangle
            //linePath.closePath();
        }

        bool isCurvedText = false;
        public override void curveTo(float x1, float y1
            , float x2, float y2
            , float x3, float y3)
        {
            isCurvedText = true;
            // linePath.curveTo(x1, y1, x2, y2, x3, y3);
        }


        public override void fillPath(int windingRule)
        {
            generateLinesFromPath();
            getLinePath().reset();
        }

        public override void fillAndStrokePath(int windingRule)
        {
            // TODO can we avoid cloning the path?
            GeneralPath path = (GeneralPath)linePath.clone();
            fillPath(windingRule);
            linePath = path;
            strokePath();
        }

        public override void strokePath()
        {
            generateLinesFromPath();
            getLinePath().reset();
        }

        #endregion

        #region Overrided Public Protected Methods (Not Used)

        public override void processOperator(string operation
           , List arguments)
        { }

        public override void drawImage(PDImage pdImage)
        { }

        public override void showAnnotation(PDAnnotation annotation)
        { }

        public override void showTransparencyGroup(PDTransparencyGroup form)
        { }

        protected GeneralPath getLinePath()
        {
            return linePath;
        }

        public override void endPath()
        {
            if (clipWindingRule != -1)
            {
                linePath.setWindingRule(clipWindingRule);
                getGraphicsState().intersectClippingPath(linePath);
                clipWindingRule = -1;
            }
            linePath.reset();
        }

        public override void closePath() { }

        public override void beginText() { }

        public override Point2D getCurrentPoint()
        {
            return linePath.getCurrentPoint();
        }

        public override void clip(int windingRule)
        {
            // the clipping path will not be updated until the succeeding painting operator is called
            clipWindingRule = windingRule;
        }

        public override void shadingFill(COSName shadingName)
        { }

        public override void showTextString(byte[] @string) { }
        public override void showTextStrings(COSArray array) { }

        protected override void showType3Glyph(Matrix textRenderingMatrix
            , PDType3Font font
            , int code
            , string unicode
            , org.apache.pdfbox.util.Vector displacement)
        { }

        protected override void showGlyph(Matrix textRenderingMatrix
            , PDFont font
            , int code
            , string unicode
            , org.apache.pdfbox.util.Vector displacement)
        { }

        protected override void showFontGlyph(Matrix textRenderingMatrix
            , PDFont font
            , int code
            , string unicode
            , org.apache.pdfbox.util.Vector displacement)
        { }

        protected override void processType3Stream(PDType3CharProc charProc
            , Matrix textRenderingMatrix)
        { }

        #endregion

        #region Private Methods

        private void overridingOperatorsForHandlingCustomScenario()
        {
            overridingDrawObjectMethodToHandleImageException();
        }

        private void overridingDrawObjectMethodToHandleImageException()
        {
            addOperator(new CustomDrawObject());
        }

        private void generateLinesFromPath()
        {
            VBotLogger.Trace(() => $"[CustomPDFGraphicsStreamEngin][generateLinesFromPath] Entered");

            if (isCurvedText)
            {
                isCurvedText = false;
                return;
            }
            bool isMoveTo = false;
            GeneralPath path = getLinePath();
            PathIterator pathIterator = path.getPathIterator(null);

            float x = 0, y = 0;
            float[] coords = new float[6];
            while (!pathIterator.isDone())
            {
                switch (pathIterator.currentSegment(coords))
                {
                    case PathIterator.SEG_MOVETO:

                        x = coords[0];
                        y = coords[1];
                        isMoveTo = true;
                        break;
                    case PathIterator.SEG_LINETO:
                        x = coords[0];
                        y = coords[1];
                        break;
                    case PathIterator.SEG_QUADTO:
                        x = coords[2];
                        y = coords[3];
                        break;
                    case PathIterator.SEG_CUBICTO:
                        x = coords[4];
                        y = coords[5];
                        break;
                    case PathIterator.SEG_CLOSE:
                        VBotLogger.Warning(() => $"[CustomPDFGraphicsStreamEngin][generateLinesFromPath] Seg Close detected. Please verify case.");
                        break;
                }
                if (!isMoveTo && (!(Math.abs(xStart - x) < .5 && Math.abs(yStart - y) < .5)))
                {
                    VBotLogger.Trace(() => $"[CustomPDFGraphicsStreamEngin][generateLinesFromPath] Lined Detected");
                    try
                    {
                        float x0 = xStart, y0 = yStart, x1 = x, y1 = y;

                        y0 = mirrorVerticallyWithRepectToPageHeight(y0);
                        y1 = mirrorVerticallyWithRepectToPageHeight(y1);
                        if (x0 == x1 || y0 == y1)
                        {
                            RectangleF rect = getLineBound(x0, y0, x1, y1);

                            VBotLogger.Trace(() => $"[CustomPDFGraphicsStreamEngin][generateLinesFromPath] Lined Detected with given rect 'x' :{rect.Left}, 'y' :{rect.Top}, 'height' :{rect.Height}, 'width' :{rect.Width} ");

                            PDFRegion pdfRegion = getLineRegion(rect);
                            pdfRegions.Add(pdfRegion);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        VBotLogger.Error(() => $"[CustomPDFGraphicsStreamEngin][generateLinesFromPath] Exception Occured {ex.Message}");
                    }

                }
                isMoveTo = false;
                xStart = x;
                yStart = y;
                pathIterator.next();
            }
            VBotLogger.Trace(() => $"[CustomPDFGraphicsStreamEngin][generateLinesFromPath] Exited");
        }

        private static PDFRegion getLineRegion(RectangleF rect)
        {
            return new PDFRegion()
            {
                Bounds = new PointRectangle()
                {
                    LeftTop = new System.Drawing.PointF(rect.Left, rect.Top),
                    RightTop = new System.Drawing.PointF(rect.Right, rect.Top),
                    RightBottom = new System.Drawing.PointF(rect.Right, rect.Bottom),
                    LeftBottom = new System.Drawing.PointF(rect.Left, rect.Bottom)
                },
                Rect = new Rectangle((int)rect.Left, (int)rect.Top, (int)rect.Width, (int)rect.Height),
                Text = string.Empty,
                Type = SegmentationType.Line,
                Confidence = 100,
                Angle = 0
            };
        }

        private RectangleF getLineBound(float x0, float y0, float x1, float y1)
        {
            bool isHoriZontalLine = false;
            if (x0 != x1)
            {
                isHoriZontalLine = true;
            }
            RectangleF rect = RectangleF.Empty;
            if (isHoriZontalLine)
            {
                if (x0 < x1)
                    rect = new RectangleF(x0, y0, Math.abs(x1 - x0), lineThickNess);
                else
                    rect = new RectangleF(x1, y0, Math.abs(x0 - x1), lineThickNess);
            }
            else
            {
                if (y0 < y1)
                    rect = new RectangleF(x0, y0, lineThickNess, Math.abs(y1 - y0));
                else
                    rect = new RectangleF(x1, y1, lineThickNess, Math.abs(y0 - y1));
            }

            return rect;
        }

        private float mirrorVerticallyWithRepectToPageHeight(float y0)
        {
            return pageSize.getHeight() - y0;
        }

        #endregion
    }
}
