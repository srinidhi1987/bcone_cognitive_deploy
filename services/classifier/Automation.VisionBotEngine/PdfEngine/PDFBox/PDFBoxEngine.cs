﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.PdfEngine
{
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.PdfEngine.PDFBox;
    using org.apache.pdfbox.pdmodel;
    using org.apache.pdfbox.pdmodel.common;
    using org.apache.pdfbox.pdmodel.encryption;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    internal class PDFBoxEngine : IPdfEngine
    {
        internal const float LineThickNess = 1;
        private const string PDFBOX_DLL_PATH = @"plugins\commands\pdf\pdfbox";

        public PDFBoxEngine()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(LoadFromSameFolder);
        }

        private static Assembly LoadFromSameFolder(object sender, ResolveEventArgs args)
        {
            string folderPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), PDFBOX_DLL_PATH);
            string assemblyPath = Path.Combine(folderPath, new AssemblyName(args.Name).Name + ".dll");
            if (!File.Exists(assemblyPath)) return null;
            Assembly assembly = Assembly.LoadFrom(assemblyPath);
            return assembly;
        }

        // private double DpiScale = 2.778;
        internal const double DpiScale = 4.1667;

        private int ImageDpi = 200;

        public int DocHeight(string pdfFilePath)
        {
            throw new NotImplementedException();
        }

        public int DocWidth(string pdfFilePath)
        {
            throw new NotImplementedException();
        }

        public void ExportPdfToImage(string pdfFilePath, string imagePath, int renderDpi, int pageIndex, int pageCount)
        {
            using (var document = PdfiumViewer.PdfDocument.Load(pdfFilePath))
            {
                var image = document.Render(pageIndex,
                            (int)(document.PageSizes[pageIndex].Width * DpiScale), (int)(document.PageSizes[pageIndex].Height * DpiScale),
                            (float)renderDpi, (float)renderDpi, false);
                image.Save(imagePath);
            }
        }
        private void removeRegionsHavingOddAngle(List<PDFRegion> regions)
        {
            if (regions.Count <= 0)
                return;

            Dictionary<float, int> angleCount = new Dictionary<float, int>();
            foreach (PDFRegion currRegion in regions)
            {
                if (angleCount.ContainsKey(currRegion.textAngle))
                    angleCount[currRegion.textAngle]++;
                else
                    angleCount.Add(currRegion.textAngle, 1);
            }

            float key = angleCount.OrderByDescending(x => x.Value)
                .FirstOrDefault()
                .Key;

            regions.RemoveAll(x => x.textAngle != key);
        }
        public SegmentedDocument ExtractRegions(string pdfFilePath
            , DocumentProperties documentProperties)
        {
            int pageHeight = 0;
            SegmentedDocument segDoc = new SegmentedDocument();

            PDDocument document = null;
            try
            {
                document = PDDocument.load(new java.io.File(pdfFilePath));
                if (document.isEncrypted())
                {
                    try
                    {
                        //  document.decrypt("");
                    }
                    catch (InvalidPasswordException ex)
                    {
                        ex.Log(nameof(PDFBoxEngine), nameof(ExtractRegions));

                        return segDoc;
                    }
                }

                var allPages = document.getDocumentCatalog().getPages();

                for (int pageIndex = 0; pageIndex < allPages.getCount(); pageIndex++)
                {
                    PDPage page = (PDPage)allPages.get(pageIndex);

                    PDFBoxPdfRegionsExtractor regionExtractor = new PDFBoxPdfRegionsExtractor();
                    List<PDFRegion> pageRegions = regionExtractor.ExtractRegions(document, pageIndex);
                    removeRegionsHavingOddAngle(pageRegions);
                    addRegionsToSegDoc(pageHeight, segDoc, pageRegions);

                    PDRectangle pageSize = page.getMediaBox();

                    if (pageRegions != null && pageRegions.Count > 0)
                    {
                        List<PDFRegion> lineRegions = getVectorLines(page);

                        addRegionsToSegDoc(pageHeight, segDoc, lineRegions);
                    }

                    double tempPageHeight = pageSize.getHeight();
                    double tempPageWidth = pageSize.getWidth();

                    bool needToAlterHeightWidth = false;
                    if (pageRegions.Count > 0)
                    {
                        int mostx = pageRegions.Max(x => x.Rect.Right);
                        int mosty = pageRegions.Max(x => x.Rect.Bottom);

                        needToAlterHeightWidth = doesNeedToAlterHeightAndWidth(tempPageHeight, tempPageWidth, mostx, mosty);
                    }

                    PageProperties pageProperty = createPageProperty(page, documentProperties.PageProperties.Count, needToAlterHeightWidth);

                    documentProperties.PageProperties.Add(pageProperty);
                    pageHeight += pageProperty.Height;
                    documentProperties.Width = pageProperty.Width;
                }
            }
            finally
            {
                if (document != null)
                {
                    document.close();
                }
            }

            documentProperties.Height = pageHeight;

            return segDoc;
        }

        private void addRegionsToSegDoc(int pageHeight, SegmentedDocument segDoc, List<PDFRegion> pageRegions)
        {
            foreach (PDFRegion pdfRegion in pageRegions)
            {
                SegmentedRegion segReg = new SegmentedRegion()
                {
                    Text = pdfRegion.Text,
                    Bounds = new Rectangle((int)Math.Round((pdfRegion.Bounds.LeftTop.X * DpiScale)),
                    (int)(Math.Round((pdfRegion.Bounds.LeftTop.Y * DpiScale)) + pageHeight),
                    (int)(Math.Round(Math.Abs((pdfRegion.Bounds.RightTop.X - pdfRegion.Bounds.LeftTop.X)) * DpiScale, MidpointRounding.AwayFromZero)),
                    (int)(Math.Round(Math.Abs((pdfRegion.Bounds.LeftTop.Y - pdfRegion.Bounds.LeftBottom.Y)) * DpiScale, MidpointRounding.AwayFromZero))),
                    Angle = 0,
                    Confidence = pdfRegion.Confidence,
                    Type = pdfRegion.Type
                };

                segDoc.Regions.Add(segReg);
            }
        }

        private static List<PDFRegion> getVectorLines(PDPage page)
        {
           
            CustomPDFGraphicsStreamEngine pdfGraphicsEngine = new CustomPDFGraphicsStreamEngine(page);
            try
            {
                return pdfGraphicsEngine.GetLines();
            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => $"[PDFBoxEngine][getVectorLines] handled case for any error occured during line finding. Warning Details {ex.Message}.");
            }

            return pdfGraphicsEngine.pdfRegions;
        }

        private static bool doesNeedToAlterHeightAndWidth(double pageHeight, double pageWidth, int contentsMostX, int contentsMostY)
        {
            return (isContentsMostXGreaterThanPageWidthAndLessThanPageHeight(pageHeight
                , pageWidth, contentsMostX, contentsMostY)
                || isContentsMostYGreaterThanPageHeightAndLessThanPageWidth(pageHeight
                , pageWidth, contentsMostY, contentsMostX));
        }

        private static bool isContentsMostXGreaterThanPageWidthAndLessThanPageHeight(double pageHeight, double pageWidth, int contentsMostX, int contentsMostY)
        {
            return contentsMostX > pageWidth && contentsMostX <= pageHeight && contentsMostY <= pageWidth;
        }

        private static bool isContentsMostYGreaterThanPageHeightAndLessThanPageWidth(double pageHeight, double pageWidth, int contentsMostY, int contentsMostX)
        {
            return contentsMostY > pageHeight && contentsMostY <= pageWidth && contentsMostX <= pageHeight;
        }


        public IList<Bitmap> GetPagewiseBitmaps(string pdfFilePath, int renderDpi, int pageIndex, int pageCount)
        {
            renderDpi = 300;
            IList<Bitmap> images = new List<Bitmap>();

            int pageEndIndex = pageIndex + pageCount;
            using (var document = PdfiumViewer.PdfDocument.Load(pdfFilePath))
            {
                for (int index = pageIndex; index < (pageEndIndex <= document.PageCount ? pageEndIndex : document.PageCount); index++)
                {
                    try
                    {
                        using (var image = document.Render(index,
                                   (int)(document.PageSizes[index].Width * DpiScale), (int)(document.PageSizes[index].Height * DpiScale),
                                   (float)renderDpi, (float)renderDpi, false))
                        {
                            Bitmap bitmap = null;

                            bitmap = AForge.Imaging.Image.Clone((Bitmap)image, System.Drawing.Imaging.PixelFormat.Format24bppRgb); //cropImage((Bitmap)image, new Rectangle(0, 0, image.Width, image.Height));// AForge.Imaging.Image.Clone((Bitmap)image, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                            images.Add(bitmap);
                        }
                    }
                    catch (Exception)
                    {
                        Bitmap bitmap = document.Render(index,
                             (int)(document.PageSizes[index].Width), (int)(document.PageSizes[index].Height),
                             (float)renderDpi, (float)renderDpi, false) as Bitmap;
                        images.Add(bitmap);
                        VBotLogger.Warning(() => $"[{nameof(GetPagewiseBitmaps)}] Unable to Convert Format32AbppRgb to Format24bppRgb");
                    }
                }
            }
            return images;
        }

        public int PageCount(string pdfFilePath)
        {
            int pageCount = 0;

            try
            {
                using (PDDocument pdfDocument = PDDocument.load(new java.io.File(pdfFilePath)))
                {
                    pageCount = pdfDocument.getDocumentCatalog().getPages().getCount();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(PDFBoxEngine), nameof(PageCount));
            }

            return pageCount;
        }

        private PageProperties createPageProperty(PDPage page, int pageIndex, bool invertHeightWidth)
        {
            PDRectangle pageSize = page.getMediaBox();

            if (pageSize == null)
            {
                //TODO: If pageSize Null then decide what to do
            }

            int height = (int)(pageSize.getHeight() * DpiScale);
            int width = (int)(pageSize.getWidth() * DpiScale);

            PageProperties pageProperty = new PageProperties();
            pageProperty.PageIndex = pageIndex;

            if (!invertHeightWidth)
            {
                pageProperty.Height = height;
                pageProperty.Width = width;
            }
            else
            {
                pageProperty.Height = width;
                pageProperty.Width = height;
            }

            return pageProperty;
        }
    }
}