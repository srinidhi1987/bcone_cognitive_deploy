﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using java.io;
using java.lang;
using java.text;
using java.util;
using org.apache.pdfbox.contentstream.@operator;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.text;
using System.Text.RegularExpressions;

namespace Automation.VisionBotEngine.PdfEngine.PDFBox
{
    internal class PDFBoxPdfRegionsExtractor
    {
        public System.Collections.Generic.List<PDFRegion> ExtractRegions(PDDocument document, int pageIndex)
        {
            PrintTextLocations printer = new PrintTextLocations();

            printer.pdfregions = new System.Collections.Generic.List<PDFRegion>();
            if (pageIndex > -1 && pageIndex < document.getDocumentCatalog().getPages().getCount())
            {
                printer.setStartPage(pageIndex + 1);
                printer.setEndPage(pageIndex + 1);
                printer.getText(document);
                printer.endWord();
                return printer.pdfregions;
            }

            return new System.Collections.Generic.List<PDFRegion>();
        }
    }

    internal class PrintTextLocations : PDFTextStripper
    {
     
        private bool is1stChar = true;
        private bool lineMatch;
        private int pageNo = 1;
        private double lastYVal;

        public PrintTextLocations()
        {
            base.setSortByPosition(true);
        }

        internal System.Collections.Generic.List<PDFRegion> pdfregions = new System.Collections.Generic.List<PDFRegion>();

        private TextPosition currentTextPosition;

        protected override void operatorException(Operator @operator, List operands, IOException e)
        {
            VBotLogger.Warning(() => $"[PrintTextLocations][operatorException] operator :{@operator.getName()}. Message :{e.getMessage()}");
        }

        protected override void processTextPosition(TextPosition text)
        {
            string tChar = text.getUnicode();
        
            string REGEX = @"[\S]+";
            char c = tChar[0];
            lineMatch = matchCharLine(text);
            currentTextPosition = text;
          
            if ((!is1stChar) && (
               (Math.abs(currentTextPosition.getXDirAdj() - (left + width)) > (currentWordNormalCharacterWidth / 2)) ||
                 (Math.abs(currentTextPosition.getXDirAdj() - (left + width)) > Math.abs(text.getWidthOfSpace())) ||
               (Math.abs((currentTextPosition.getYDirAdj() - Math.abs(currentTextPosition.getHeightDir())) - (top)) > Math.abs(bottom - top))
               ))
            {
                endWord();
            }

            if ((Regex.IsMatch(tChar, REGEX)) && (!Character.isWhitespace(c)))
            {
                if ((!is1stChar) && (lineMatch == true))
                {
                    appendChar(tChar);
                }
                else if (is1stChar == true)
                {
                    setWordCoord(text, tChar);
                }

                if (top > (currentTextPosition.getYDirAdj() - Math.abs(currentTextPosition.getHeightDir())))
                {
                    top = currentTextPosition.getYDirAdj() - Math.abs(currentTextPosition.getHeightDir());
                }
                if (bottom < (currentTextPosition.getYDirAdj()))
                {
                    bottom = (currentTextPosition.getYDirAdj());
                }
                width = Math.abs(left - (currentTextPosition.getXDirAdj() + Math.abs(currentTextPosition.getWidthDirAdj())));
                //if (currentTextPosition.getHeightDir() > height)
                //{
                //    height = currentTextPosition.getHeightDir();
                //}
                if (currentWordNormalCharacterWidth < Math.abs(currentTextPosition.getWidthDirAdj()))
                {
                    currentWordNormalCharacterWidth = Math.abs(currentTextPosition.getWidthDirAdj());
                }
            }
            else
            {
                endWord();
            }
        }

        private float left = 0;
        private float top = 0;
        private float bottom = 0;
        private float width = 0;
        private float currentWordNormalCharacterWidth = 0;

        private string currentText = string.Empty;

        protected void appendChar(string tChar)
        {
            currentText = currentText + tChar;
            is1stChar = false;
        }

        protected void setWordCoord(TextPosition text, string tChar)
        {
            currentText = currentText + tChar;
            left = text.getXDirAdj();
            top = text.getYDirAdj() - Math.abs(text.getHeightDir());
            bottom = text.getYDirAdj();
            currentWordNormalCharacterWidth = Math.abs(text.getWidthDirAdj());
            is1stChar = false;
        }

        public void endWord()
        {
            if (!string.Empty.Equals(currentText))
            {
                float tempHeight = bottom - top;
                top = top - (tempHeight * 0.35f);
                bottom = bottom + (tempHeight * 0.13f);

                float height = bottom - top;

                PDFRegion pdfRegion = new PDFRegion()
                {
                    Bounds = new PointRectangle()
                    {
                        LeftTop = new System.Drawing.PointF(left, top),
                        RightTop = new System.Drawing.PointF(left + width, top),
                        RightBottom = new System.Drawing.PointF(left + width, top + height),
                        LeftBottom = new System.Drawing.PointF(left, top + height)
                    },
                    Rect = new System.Drawing.Rectangle((int)left,
                    (int)top,
                    (int)System.Math.Round(width, System.MidpointRounding.AwayFromZero),
                    (int)System.Math.Round(height, System.MidpointRounding.AwayFromZero)),
                    Text = currentText,
                    Type = SegmentationType.Word,
                    Confidence = 100,
                    Angle = 0,
                    textAngle = currentTextPosition.getDir()
                };

                pdfregions.Add(pdfRegion);
            }
           

            is1stChar = true;
            currentText = string.Empty;
            left = 0;
            top = 0;
            width = 0;
            currentWordNormalCharacterWidth = 0;
        }

        protected bool matchCharLine(TextPosition text)
        {
            double yVal = roundVal(Float.valueOf(text.getYDirAdj()));
            if (yVal == lastYVal)
            {
                return true;
            }
            lastYVal = yVal;
            endWord();
            return false;
        }

        protected double roundVal(Float yVal)
        {
            float test = yVal.floatValue();

            double yValDub = System.Convert.ToDouble(test, System.Globalization.CultureInfo.InvariantCulture);
            yValDub = System.Math.Round(yValDub, 2);
            return yValDub;
        }
    }
}