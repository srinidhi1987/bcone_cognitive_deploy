﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

namespace Automation.VisionBotEngine.PdfEngine
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = false)]
    public static class PdfEngineStrategy
    {
        private static IPdfEngine _engine;

        [Obfuscation(Exclude = true, ApplyToMembers = false)]
        public static IPdfEngine GetPdfEngine()
        {
            return _engine ?? (_engine = new PDFBoxEngine());
        }
    }
}