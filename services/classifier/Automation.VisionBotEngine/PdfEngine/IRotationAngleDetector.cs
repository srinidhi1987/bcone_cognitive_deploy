﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.PdfEngine
{
    internal interface IRotationAngleDetector
    {
        double DetectAngle(List<PDFRegion> pdfRegions);
    }

    internal class QuickPdfRotationAngleDetector : IRotationAngleDetector
    {
        public double DetectAngle(List<PDFRegion> pdfRegions)
        {
            double detectedAngle = 0;

            if (pdfRegions.Count > 0)
            {
                PointRectangle pointRectagle = pdfRegions[0].Bounds;

                if (pointRectagle.LeftTop.Y == pointRectagle.RightTop.Y && pointRectagle.LeftTop.X < pointRectagle.RightTop.X)
                {
                    detectedAngle = 0;
                }
                else if (pointRectagle.LeftTop.Y == pointRectagle.LeftBottom.Y && pointRectagle.LeftTop.X > pointRectagle.LeftBottom.X)
                {
                    detectedAngle = 90;
                }
                else if (pointRectagle.LeftTop.Y == pointRectagle.RightTop.Y && pointRectagle.LeftTop.X > pointRectagle.RightTop.X)
                {
                    detectedAngle = 180;
                }
                else if (pointRectagle.LeftTop.Y == pointRectagle.LeftBottom.Y && pointRectagle.LeftTop.X < pointRectagle.LeftBottom.X)
                {
                    detectedAngle = 270;
                }
            }

            return detectedAngle;
        }
    }
}