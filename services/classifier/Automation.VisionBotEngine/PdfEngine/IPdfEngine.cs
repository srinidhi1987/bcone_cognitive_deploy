﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.PdfEngine
{
    using Automation.VisionBotEngine.Model;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Reflection;

    internal enum PDFEngineTypes
    {
        PDFLibNet,
        QuickPDF,
        AsposePdf,
        Spire,
        PDFBox
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public interface IPdfEngine
    {
        SegmentedDocument ExtractRegions(string pdfFilePath
            , DocumentProperties documentProperties);

        int DocWidth(string pdfFilePath);

        int DocHeight(string pdfFilePath);

        void ExportPdfToImage(string pdfFilePath, string imagePath, int renderDpi, int pageIndex, int pageCount);

        // PdfSegmentedDocument GetSegmentedDocument(string pdfFilePath);

        int PageCount(string pdfFilePath);

        IList<Bitmap> GetPagewiseBitmaps(string pdfFilePath, int renderDpi, int pageIndex, int pageCount);
    }
}