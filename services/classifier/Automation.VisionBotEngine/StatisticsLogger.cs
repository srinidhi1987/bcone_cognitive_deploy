﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Automation.Cognitive.VisionBotEngine
{
    static class StatisticsLogger
    {
        private static string logFile = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
            "Automation Anywhere IQBot Platform",
            "Logs",
            "DocumentStatistics.log");

        public static void Log(string data)
        {
            //TODO : REMOVE RETURN STATEMENT ONCE CONFIGURATION BASED STATISTIC LOGGER AVAILABLE
            return;

            try
            {
                string logData = $"{Environment.NewLine}{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}\t{data}";
                File.AppendAllText(logFile, logData);
            }
            catch (Exception ex) { };
        }
    }
}
