﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using Automation.VisionBotEngine.Model;

namespace Automation.VisionBotEngine
{
    internal interface IUdrOperations
    {
        Field FindLayoutField(FieldLayout fieldLayout, SIRFields sirFields, LayoutConfidence layoutConfidence);
        List<Field> GetFieldsAspectToFieldlayout(FieldLayout fieldLayout, SIRFields sirFields);

    }

    internal class UdrOperations : IUdrOperations
    {
        private double _xAspectRatio = 1.0;
        private double _yAspectRatio = 1.0;

        public double XAspectRation
        {
            set { _xAspectRatio = value; }
        }
        public double YAspectRation
        {
            set { _yAspectRatio = value; }
        }

        readonly FieldOperations _fieldOperations = new FieldOperations();

        public List<Field> GetFieldsAspectToFieldlayout(FieldLayout fieldLayout, SIRFields sirFields)
        {
            throw new System.NotImplementedException();
        }

        public Field FindLayoutField(FieldLayout fieldLayout, SIRFields sirFields, LayoutConfidence layoutConfidence)
        {
            fieldLayout = fieldLayout.Clone();
            // fieldLayout.SimilarityFactor = 1.0;
            Field field = null;

            List<Field> findFieldLayout = new List<Field>() ;

            if (fieldLayout.IsValueBoundAuto)
            {
                findFieldLayout=  _fieldOperations.GetAllBestMatchingFields(sirFields.Fields, fieldLayout.Label, fieldLayout.SimilarityFactor);

                if (findFieldLayout != null && findFieldLayout.Count > 0)
                {
                    return findFieldLayout[0];
                }
            }
            else
            {
                findFieldLayout = _fieldOperations.GetAllBestMatchingFields(sirFields.Fields, fieldLayout.Label, fieldLayout.SimilarityFactor);
            }
            if (findFieldLayout != null && findFieldLayout.Count > 0)
            {
                field = _fieldOperations.GetNearestField(fieldLayout.Bounds, findFieldLayout);
            }

            if (field == null && !string.IsNullOrEmpty(fieldLayout.Label))
            {
                field = GetCustomeLayoutField(fieldLayout, sirFields);
            }
            return field;
        }

        private Field GetCustomeLayoutField(FieldLayout fieldLayout, SIRFields sirFields)
        {
            string[] labelsStrings = GetFieldsLabels(fieldLayout.Label);
            FieldLayout splitLayout = fieldLayout.Clone();
            foreach (string strLabel in labelsStrings)
            {
                splitLayout.Label = strLabel;
                List<Field> findFieldLayouts = _fieldOperations.GetAllBestMatchingFields(sirFields.Fields, splitLayout.Label, 1.0);
                foreach (var VARIABLE in findFieldLayouts)
                {
                    if (VARIABLE != null)
                    {
                        int height = splitLayout.Bounds.Height > VARIABLE.Bounds.Height
                            ? splitLayout.Bounds.Height
                            : VARIABLE.Bounds.Height;


                        splitLayout.Bounds = new Rectangle(VARIABLE.Bounds.X, VARIABLE.Bounds.Y,
                            splitLayout.Bounds.Width, height);
                        Field field = GetCustomeRegionValue(splitLayout, sirFields);
                        if (field.Text.GetSimilarity(fieldLayout.Label) > fieldLayout.SimilarityFactor)
                        {
                            return field;
                        }
                    }
                }
            }
            return null;
        }

        private Field GetCustomeRegionValue(FieldLayout fieldLayout, SIRFields sirFields)
        {
            fieldLayout.Bounds = _fieldOperations.FindAspectRect(fieldLayout.Bounds, _xAspectRatio, _yAspectRatio);
            return _fieldOperations.GetUserDefinedField(fieldLayout, sirFields.Fields);
        }

        private string[] GetFieldsLabels(string fieldLabel)
        {
            return fieldLabel.Split(Convert.ToChar(" "));
        }
    }
}
