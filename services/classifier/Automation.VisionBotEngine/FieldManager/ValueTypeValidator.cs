﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.Validation;

namespace Automation.VisionBotEngine
{
    internal class ValueTypeValidator : IValueTypeValidator
    {
        public bool IsValidType(string valueText, FieldLayout fieldLayout, FieldDef fieldDef)
        {
            string pattern = fieldDef.GetApplicablePattern(fieldLayout);

            if (fieldDef.ValueType == FieldValueType.Number || fieldDef.ValueType == FieldValueType.Decimal)
            {
                return isValidNumberType(valueText, pattern);
            }
            if (fieldDef.ValueType == FieldValueType.Date)
            {
                return isValidDateType(valueText, pattern);
            }
            if (fieldDef.ValueType == FieldValueType.Text)
            {
                return isValidTextType(valueText, pattern);
            }

            return true;
        }


        private bool isValidNumberType(string valueText, string pattern)
        {
            var numberType = new NumberTypeParser();

            if (numberType.Parse(valueText, pattern).HasValue)
                return true;
            else
                return false;
        }


        private bool isValidDateType(string valueText, string pattern)
        {
            var numberType = new DateTypeParser();

            if (numberType.Parse(valueText, pattern).HasValue)
                return true;
            else
                return false;
        }
        private bool isValidTextType(string valueText, string pattern)
        {
            var issue = (string.IsNullOrWhiteSpace(pattern) || PatternHelper.IsMatch(valueText, pattern)
               ? (FieldDataValidationIssueCodes?)null
               : (string.IsNullOrEmpty(pattern) ? FieldDataValidationIssueCodes.InvalidTextTypeValue : FieldDataValidationIssueCodes.PatternMismatch));

            if (issue == FieldDataValidationIssueCodes.PatternMismatch)
            {
                return false;
            }
            return true;
        }
    }
}

