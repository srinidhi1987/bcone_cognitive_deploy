﻿using Automation.VisionBotEngine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.VisionBotEngine.Model;

namespace Automation.VisionBotEngine
{
    internal class RasterPdfToSegmentedDocumentCreator : ISegmentedDocumentCreator
    {
        private ImageToSegmentedDocumentCreator _imageToSegmentedDocumentCreator = null;

        public RasterPdfToSegmentedDocumentCreator(ImageToSegmentedDocumentCreator imageToSegmentedDocumentCreator)
        {
            _imageToSegmentedDocumentCreator = imageToSegmentedDocumentCreator;
        }
        public SegmentedDocument Create()
        {
            return _imageToSegmentedDocumentCreator.Create();
        }
    }
}
