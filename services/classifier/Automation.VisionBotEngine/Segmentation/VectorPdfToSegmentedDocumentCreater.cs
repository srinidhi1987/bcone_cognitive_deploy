﻿using System;
using Automation.VisionBotEngine.Interfaces;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.PdfEngine;

namespace Automation.VisionBotEngine
{
    internal class VectorPdfToSegmentedDocumentCreater : ISegmentedDocumentCreator
    {
        private IPdfEngine _pdfEngine = null;

        private string _filePath;
        private DocumentProperties _documentProperties;
        public VectorPdfToSegmentedDocumentCreater(IPdfEngine pdfEngine,string filePath,DocumentProperties docProperties)
        {
            _pdfEngine = pdfEngine;
            _filePath = filePath;
            _documentProperties = docProperties;
        }
        public SegmentedDocument Create()
        {
           return _pdfEngine.ExtractRegions(_filePath,_documentProperties);
        }
    }
}
