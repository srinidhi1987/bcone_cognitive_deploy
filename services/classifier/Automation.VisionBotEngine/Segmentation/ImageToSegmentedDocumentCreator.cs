﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Common;
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Interfaces;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.OcrEngine;
    using Automation.VisionBotEngine.PdfEngine;
    using System;
    using System.IO;

    internal abstract class ImageToSegmentedDocumentCreator : ISegmentedDocumentCreator
    {
        protected DocumentProperties _documentProperties;
        protected IVisionBotPageImageProvider _visionBotPageImageProvider;
        private IOcrEngine _ocrEngine;

        internal ImageToSegmentedDocumentCreator(IOcrEngine ocrEngine
            , DocumentProperties documentProperties
            , IVisionBotPageImageProvider visionBotPageImageProvider
            )
        {
            _ocrEngine = ocrEngine;
            _documentProperties = documentProperties;
            _visionBotPageImageProvider = visionBotPageImageProvider;
        }

        protected abstract SegmentedDocument CreateSegmentedDocument();

        public SegmentedDocument Create()
        {
            return CreateSegmentedDocument();
        }

        protected SegmentedDocument getSegmentedDocumentForPage(ImageDetails imageDetails)
        {
            SegmentedDocument pageSegDoc = _ocrEngine.ExtractRegions(imageDetails, Constants.SearchString);
            return pageSegDoc;
        }

        protected int getPageCount()
        {
            int pageCount;
            if (CommonMethods.IsPdfFile(_documentProperties.Path))
            {
                IPdfEngine pdfEngine = getPdfEngine();
                pageCount = pdfEngine.PageCount(_documentProperties.Path);
                return pageCount;
            }
            else
            {
                ImageOperations imageOperations = new ImageOperations();
                pageCount = imageOperations.GetPageCount(_documentProperties.Path);
            }

            return pageCount;
        }

        private IPdfEngine getPdfEngine()
        {
            IPdfEngine pdfEngine = PdfEngineStrategy.GetPdfEngine();
            return pdfEngine;
        }

        protected void deleteFile(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageToSegmentedDocumentCreator), nameof(deleteFile));
            }
        }
    }
}