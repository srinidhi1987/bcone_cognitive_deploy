﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Common;
using Automation.VisionBotEngine.Interfaces;
using Automation.VisionBotEngine.Model;
using Automation.VisionBotEngine.OcrEngine;
using Automation.VisionBotEngine.PdfEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.VisionBotEngine
{
    internal class SegmentedDocumentProvider
    {
        private IOcrEngine _ocrEngine = null;
        private IPdfEngine _pdfEngine = null;
        private IDocumentVectorDetailsCacheManager _documentVectorDetailsCacheManager;

        #region Constructors

        public SegmentedDocumentProvider(IOcrEngine ocrEngine
            , IPdfEngine pdfEngine
            , IDocumentVectorDetailsCacheManager documentVectorDetailsCacheManager)
        {
            _ocrEngine = ocrEngine;
            _pdfEngine = pdfEngine;
            _documentVectorDetailsCacheManager = documentVectorDetailsCacheManager;
        }

        #endregion Constructors

        #region Public Methods

        public SegmentedDocument GetSegmentedDocument(DocumentProperties docProperties
            , ProcessingMode processingMode
            , IVisionBotPageImageProvider visionBotPageImageProvider
            , VBotEngineSettings engineSettings, string documentId
            , SegmentedDocumentCacheConfiguration segmentedDocumentCacheConfiguration = null)
        {
            if (segmentedDocumentCacheConfiguration == null)
                segmentedDocumentCacheConfiguration = new SegmentedDocumentCacheConfiguration();
            SegmentedDocument segDoc = null;
            if (segmentedDocumentCacheConfiguration.IsReadFromCache && tryToFillDataFromCache(documentId, ref docProperties, out segDoc))
            {
                new SegmentedDocumentOperations().MergeLineRegions(segDoc.Regions,docProperties);

                return segDoc;
            }

            segDoc = generateSegmentedDocument(docProperties
                , processingMode
                , visionBotPageImageProvider
                , engineSettings);

            if (segDoc.Regions.Count > 0) // This is to solve COG-2133
            {
                new SegmentedDocumentOperations().MergeLineRegions(segDoc.Regions,docProperties);

                tryToSaveDataInCache(documentId, docProperties, segDoc);
            }

            return segDoc;
        }

        public SegmentedDocument GetSegmentedDocument(DocumentProperties docProperties
          , IVisionBotPageImageProvider visionBotPageImageProvider
          , VBotEngineSettings engineSettings, string documentId)
        {
            bool isPdfDocument = CommonMethods.IsPdfFile(docProperties.Path);
            ProcessingMode processingMode = isPdfDocument ? ProcessingMode.Pdf : ProcessingMode.Image;
            docProperties.Type = isPdfDocument ? DocType.Pdf : DocType.Image;

            return GetSegmentedDocument(docProperties
                , processingMode
                , visionBotPageImageProvider
                , engineSettings
                , documentId);
        }

        #endregion Public Methods

        #region Private Methods

        private SegmentedDocument generateSegmentedDocument(DocumentProperties docProperties
            , ProcessingMode processingMode
            , IVisionBotPageImageProvider visionBotPageImageProvider
            , VBotEngineSettings engineSettings)
        {
            SegmentedDocument segDoc = new SegmentedDocument();
            if (processingMode == ProcessingMode.Pdf)
            {
                ISegmentedDocumentCreator segmentedDocumnetCreator =
                    getVectorPdfToSegmentedDocumentCreator(docProperties);

                segDoc = segmentedDocumnetCreator.Create();
                segDoc.processingMode = ProcessingMode.Pdf;

                if (unableToGenerateRegions(segDoc))
                {
                    resetDocumentProperties(docProperties);

                    segmentedDocumnetCreator = getRasterPdfToSegmentedDocumentCreator(docProperties
                        , visionBotPageImageProvider
                        , engineSettings);

                    segDoc = segmentedDocumnetCreator.Create();
                    segDoc.processingMode = ProcessingMode.Image;
                }
            }
            else
            {
                ISegmentedDocumentCreator segmentedDocumentCreator = getImageToSegmentedDocumentCreator(
                    visionBotPageImageProvider
                    , docProperties
                    , engineSettings);

                segDoc = segmentedDocumentCreator.Create();
                segDoc.processingMode = ProcessingMode.Image;
            }

            return segDoc;
        }

        private static void resetDocumentProperties(DocumentProperties docProperties)
        {
            docProperties.PageProperties.Clear();
            docProperties.Width = 0;
            docProperties.Height = 0;
            docProperties.BorderRect = new System.Drawing.Rectangle();
        }

        private RasterPdfToSegmentedDocumentCreator getRasterPdfToSegmentedDocumentCreator(DocumentProperties docProperties
            , IVisionBotPageImageProvider visionBotPageImageProvider
            , VBotEngineSettings engineSettings)
        {
            return new RasterPdfToSegmentedDocumentCreator(
                                    getImageToSegmentedDocumentCreator(visionBotPageImageProvider
                                    , docProperties
                                    , engineSettings));
        }

        private ImageToSegmentedDocumentCreator getImageToSegmentedDocumentCreator(IVisionBotPageImageProvider visionBotPageImageProvider
            , DocumentProperties docProperties
            , VBotEngineSettings engineSettings)
        {
            ImageToSegmentedDocumentCreator segmentedDocumentCreator = null;

            if (doesParallelSegmentationEnabled(engineSettings))
            {
                segmentedDocumentCreator = getParallelSegmentedDocumentCreator(visionBotPageImageProvider
                    , docProperties
                    , engineSettings.MaxDegreeOfParallelism);
            }
            else
            {
                segmentedDocumentCreator = getSerialSegmentedDocumentCreator(visionBotPageImageProvider
                    , docProperties);
            }

            return segmentedDocumentCreator;
        }

        private static bool doesParallelSegmentationEnabled(VBotEngineSettings engineSettings)
        {
            return engineSettings != null && engineSettings.MaxDegreeOfParallelism > 1;
        }

        private ImageToSegmentedDocumentCreator getSerialSegmentedDocumentCreator(IVisionBotPageImageProvider visionBotPageImageProvider
            , DocumentProperties docProperties)
        {
            ImageToSegmentedDocumentCreator segmentedDocumentCreator;
            VBotLogger.Trace(() => "[VBotEngine -> GetSegmentedDocument] Creating instance of SerialSegmentedDocumentCreator.");
            segmentedDocumentCreator = new SerialSegmentedDocumentCreator(_ocrEngine
                , docProperties
                , visionBotPageImageProvider);
            return segmentedDocumentCreator;
        }

        private ImageToSegmentedDocumentCreator getParallelSegmentedDocumentCreator(IVisionBotPageImageProvider visionBotPageImageProvider
            , DocumentProperties docProperties
            , int maxDegreeOfParallelism)
        {
            ImageToSegmentedDocumentCreator segmentedDocumentCreator;
            VBotLogger.Trace(() => "[VBotEngine -> GetSegmentedDocument] Creating instance of ParallelSegmentedDocumentCreator.");
            segmentedDocumentCreator = new ParallelSegmentedDocumentCreator(_ocrEngine
                , maxDegreeOfParallelism
                , docProperties
                , visionBotPageImageProvider);
            return segmentedDocumentCreator;
        }

        private ISegmentedDocumentCreator getVectorPdfToSegmentedDocumentCreator(DocumentProperties docProperties)
        {
            return new VectorPdfToSegmentedDocumentCreater(_pdfEngine
                , docProperties.Path
                , docProperties);
        }

        private static bool unableToGenerateRegions(SegmentedDocument segDoc)
        {
            return segDoc.Regions.Count < 10; //In case of vector/hybrid pdf when regions found less than 10, it should be considerred as a Raster pdf.
        }

        private bool tryToSaveDataInCache(string documentId
            , DocumentProperties docProperties
            , SegmentedDocument segDoc)
        {
            if (_documentVectorDetailsCacheManager == null)
            { return false; }

            try
            {
                var vectorDatails = new DocumentVectorDetails(docProperties, segDoc);
                _documentVectorDetailsCacheManager.Set(documentId, vectorDatails);
                return true;
            }
            catch (Exception ex)
            {
                VBotLogger.Debug(() => $"{nameof(SegmentedDocumentProvider)}=>{nameof(tryToSaveDataInCache)} : unable to savedocumentVectorDatails");
                ex.Log(nameof(SegmentedDocumentProvider), nameof(tryToSaveDataInCache));
            }

            return false;
        }

        private bool tryToFillDataFromCache(string documentId
            , ref DocumentProperties docProperties
            , out SegmentedDocument segDoc)
        {
            segDoc = null;

            if (_documentVectorDetailsCacheManager == null)
            {
                return false;
            }

            try
            {
                var documentVectorDetails = _documentVectorDetailsCacheManager.Get(documentId);

                if (documentVectorDetails?.DocumentProperties != null
                    && documentVectorDetails.SegmentedDocument != null)
                {
                    var tempDocProperties = documentVectorDetails.DocumentProperties;

                    docProperties.Id = tempDocProperties.Id;
                    docProperties.Name = tempDocProperties.Name;
                    docProperties.BorderRect = tempDocProperties.BorderRect;
                    docProperties.Height = tempDocProperties.Height;
                    docProperties.PageProperties = tempDocProperties.PageProperties;
                    docProperties.RenderDPI = tempDocProperties.RenderDPI;
                    docProperties.Type = tempDocProperties.Type;
                    docProperties.Version = tempDocProperties.Version;
                    docProperties.Width = tempDocProperties.Width;

                    segDoc = documentVectorDetails.SegmentedDocument;
                }

                return true;
            }
            catch (Exception ex)
            {
                VBotLogger.Debug(() => $"{nameof(SegmentedDocumentProvider)}=>{nameof(tryToFillDataFromCache)} : unable to fetch documentVectorDetails from service.");
            }

            return false;
        }

        #endregion Private Methods
    }
}