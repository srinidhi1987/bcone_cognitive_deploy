﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using Cognitive.VisionBotEngine.CustomExceptions;
    using OcrEngine;
    using OcrEngine.Tesseract;
    using System;
    using System.IO;

    internal class SerialSegmentedDocumentCreator : ImageToSegmentedDocumentCreator
    {
        private const int MAX_PAGES_SUPPORTED = 60;

        public SerialSegmentedDocumentCreator(IOcrEngine ocrEngine
            , DocumentProperties documentProperties
            , IVisionBotPageImageProvider visionBotPageImageProvider)
            : base(ocrEngine, documentProperties, visionBotPageImageProvider)
        { }

        protected override SegmentedDocument CreateSegmentedDocument()
        {
            VBotLogger.Trace(() => "[SerialSegmentedDocumentCreator -> CreateSegmentedDocument] Start.");
            int pageCount = 0;
            int documentHeight = 0;
            SegmentedDocument segDoc = new SegmentedDocument();

            pageCount = getPageCount();

            if(pageCount > MAX_PAGES_SUPPORTED)
            {
                throw new MaxPageLimitException($"Maximum {MAX_PAGES_SUPPORTED} pages are supported.");
            }

            VBotLogger.Trace(() => string.Format("[SerialSegmentedDocumentCreator -> CreateSegmentedDocument] pageCount: {0}", pageCount));
            for (int i = 0; i < pageCount; i++)
            {
                Page processedPage = null;
                string tempFilePath = null;
                try
                {
                    processedPage = _visionBotPageImageProvider.GetImageFromProcessDocument(i);
                    VBotLogger.Trace(() => string.Format("[SerialSegmentedDocumentCreator -> CreateSegmentedDocument] page index: {0} processedPage is not null: {1}", i, processedPage != null));
                    VBotLogger.Trace(() => string.Format("[SerialSegmentedDocumentCreator -> CreateSegmentedDocument] page index: {0} PageProperties Height Width: {1}", i, processedPage != null && processedPage.PageProperties != null ? processedPage.PageProperties.Height.ToString() + " " + processedPage.PageProperties.Width.ToString() : string.Empty));
                    PageProperties pageProperties = processedPage.PageProperties;
                    _documentProperties.PageProperties.Add(pageProperties);
                    tempFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()+".tif");
                    processedPage.ProcessedPageImage.Save(tempFilePath);

                    string skewcorrectedFilePath = _documentProperties.Path + "_Page" + i.ToString() + ".vimg";

                    ImageDetails imageDetails = new ImageDetails(tempFilePath, _documentProperties.LanguageCode);
                    imageDetails.AdditionalParameters.Add(
                        Tesseract4_OCREngine.SKEW_AND_ORIENTATION_CORRECTED_IMAGE_PATH
                        , skewcorrectedFilePath);
                    SegmentedDocument pageSegDoc = getSegmentedDocumentForPage(imageDetails);

                    if (File.Exists(skewcorrectedFilePath))
                        File.Delete(skewcorrectedFilePath);
                    VBotLogger.Trace(() => string.Format("[SerialSegmentedDocumentCreator -> CreateSegmentedDocument] page index: {0} SegmentedData Region Count: {1}", i, pageSegDoc != null && pageSegDoc.Regions != null ? pageSegDoc.Regions.Count : 0));
                    new SegmentedDocumentOperations().Merge(segDoc, pageSegDoc, 0, documentHeight);
                    documentHeight += pageProperties.Height;
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(SerialSegmentedDocumentCreator), nameof(CreateSegmentedDocument));
                    throw;
                }
                finally
                {
                    processedPage?.Dispose();
                    deleteFile(tempFilePath);
                }
            }

            _documentProperties.Height = documentHeight;
            _documentProperties.Width = _documentProperties.PageProperties[0].Width;

            VBotLogger.Trace(() => "[SerialSegmentedDocumentCreator -> CreateSegmentedDocument] End.");
            return segDoc;
        }
    }
}