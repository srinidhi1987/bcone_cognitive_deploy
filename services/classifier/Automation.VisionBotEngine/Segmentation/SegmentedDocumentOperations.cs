﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine
{
    internal class SegmentedDocumentOperations
    {
        public void Merge(SegmentedDocument document, SegmentedDocument documentToMerge, int horizonalOffset, int verticalOffset, double scaleX = 1.0, double scaleY = 1.0)
        {
            RectangleOperations rectangleOperations = new RectangleOperations();
            if (documentToMerge != null && documentToMerge.Regions != null)
            {
                for (int i = 0; i < documentToMerge.Regions.Count; i++)
                {
                    SegmentedRegion regionToMerge = documentToMerge.Regions[i];
                    regionToMerge.Bounds = rectangleOperations.AddOffsetWithScale(regionToMerge.Bounds, horizonalOffset, verticalOffset, scaleX, scaleY);
                    document.Regions.Add(regionToMerge);
                }
            }
        }

        internal void MergeLineRegions(List<SegmentedRegion> regions, DocumentProperties docProperties)
        {
            if (regions == null || regions.Count == 0)
                return;

            List<SegmentedRegion> allLines = regions.FindAll(x => x.Type == SegmentationType.Line);

            if (allLines.Count == 0)
                return;

            int currentPageHeight = 0;
            const int MaxLineDistanceTolerance = 5;

            foreach (PageProperties page in docProperties.PageProperties)
            {
                var pageRect = new Rectangle(0
                    , currentPageHeight
                    , page.Width
                    , page.Height);

                List<SegmentedRegion> linesInPage = allLines.FindAll(line
                      => pageRect.Contains(line.Bounds));

                if (linesInPage != null && linesInPage.Count > 0)
                {
                    for (int iterationCount = 1; iterationCount < linesInPage.Count; iterationCount++)
                    {
                        bool isAnyMatchFound;

                        do
                        {
                            isAnyMatchFound = false;
                            int primaryLineIndex = linesInPage.Count - iterationCount;
                            SegmentedRegion primaryLine = linesInPage[primaryLineIndex];

                            for (int comparisonLineIndex = primaryLineIndex - 1; comparisonLineIndex >= 0; comparisonLineIndex--)
                            {
                                SegmentedRegion comparisonLine = linesInPage[comparisonLineIndex];
                                if (primaryLine != comparisonLine)
                                {
                                    #region Vertical Line
                                    if (primaryLine.Bounds.Left == comparisonLine.Bounds.Left
                                        && primaryLine.Bounds.Right == comparisonLine.Bounds.Right)
                                    {
                                        if (Math.Abs(primaryLine.Bounds.Top - comparisonLine.Bounds.Bottom) < MaxLineDistanceTolerance
                                            || Math.Abs(primaryLine.Bounds.Bottom - comparisonLine.Bounds.Top) < MaxLineDistanceTolerance)
                                        {
                                            int newTop = Math.Min(primaryLine.Bounds.Top, comparisonLine.Bounds.Top);
                                            int newBottom = Math.Max(primaryLine.Bounds.Bottom, comparisonLine.Bounds.Bottom);
                                            primaryLine.Bounds = new System.Drawing.Rectangle(primaryLine.Bounds.Left, newTop, primaryLine.Bounds.Width, newBottom - newTop);

                                            linesInPage.Remove(comparisonLine);
                                            regions.Remove(comparisonLine);

                                            isAnyMatchFound = true;
                                        }
                                    }
                                    #endregion
                                    #region Horizontal Line
                                    else if (primaryLine.Bounds.Top == comparisonLine.Bounds.Top
                                        && primaryLine.Bounds.Bottom == comparisonLine.Bounds.Bottom)
                                    {
                                        if (Math.Abs(primaryLine.Bounds.Right - comparisonLine.Bounds.Left) < MaxLineDistanceTolerance
                                            || Math.Abs(primaryLine.Bounds.Left - comparisonLine.Bounds.Right) < MaxLineDistanceTolerance)
                                        {
                                            int newLeft = Math.Min(primaryLine.Bounds.Left, comparisonLine.Bounds.Left);
                                            int newRight = Math.Max(primaryLine.Bounds.Right, comparisonLine.Bounds.Right);
                                            primaryLine.Bounds = new System.Drawing.Rectangle(newLeft, primaryLine.Bounds.Top, newRight - newLeft, primaryLine.Bounds.Height);

                                            linesInPage.Remove(comparisonLine);
                                            regions.Remove(comparisonLine);

                                            isAnyMatchFound = true;
                                        }
                                    }
                                    #endregion
                                }
                            }
                        } while (isAnyMatchFound);
                    }

                    currentPageHeight += page.Height;
                }
            }
        }
    }
}