﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Model;
    using OcrEngine;
    using OcrEngine.Tesseract;
    using System;
    using System.IO;
    using System.Threading;

    internal class ParallelSegmentedDocumentCreator : ImageToSegmentedDocumentCreator
    {
        private int _maxDegreeOfParallelism = 2;
        private int _currentPage;
        private object _lockObject;
        private int _pageCount;
        private SegmentedDocumentInfo[] _documents;

        public ParallelSegmentedDocumentCreator(IOcrEngine ocrEngine
            , int maxDegreeOfParallelism
            , DocumentProperties documentProperties
            , IVisionBotPageImageProvider visionBotPageImageProvider)
            : base(ocrEngine, documentProperties, visionBotPageImageProvider)
        {
            if (maxDegreeOfParallelism < 1)
            {
                VBotLogger.Error(() => "maxDegreeOfParallelism should be greater than 0");
                throw new ArgumentException("maxDegreeOfParallelism should be greater than 0.", "maxDegreeOfParallelism");
            }

            _maxDegreeOfParallelism = maxDegreeOfParallelism;
            _lockObject = new object();
        }

        protected override SegmentedDocument CreateSegmentedDocument()
        {
            SegmentedDocument segDoc = new SegmentedDocument();

            try
            {
                _currentPage = 0;
                _pageCount = getPageCount();
                if (_pageCount > 0)
                {
                    _documents = new SegmentedDocumentInfo[_pageCount];
                    int parallelWorkers = getParallelWorkersRequired(_pageCount);
                    VBotLogger.Trace(() => string.Format("[ParallelSegmentedDocumentCreator -> CreateSegmentedDocument] Page Count: {0} Parallel Workers: {1}", _pageCount, parallelWorkers));
                    var workers = new Thread[parallelWorkers];
                    var waitHandles = new WaitHandle[parallelWorkers];
                    for (int index = 0; index < parallelWorkers; index++)
                    {
                        var handle = new EventWaitHandle(false, EventResetMode.ManualReset);
                        workers[index] = new Thread(obj =>
                        {
                            try
                            {
                                processPage(obj);
                            }
                            catch (Exception ex)
                            {
                                ex.Log(nameof(ParallelSegmentedDocumentCreator), nameof(CreateSegmentedDocument), "Exception on Worker");
                            }
                            finally
                            {
                                handle.Set();
                            }
                        });
                        workers[index].Name = string.Format("Worker{0}", index);
                        workers[index].SetApartmentState(ApartmentState.MTA);
                        waitHandles[index] = handle;
                        workers[index].Start(index);
                    }

                    ManualResetEvent resetEvent = new ManualResetEvent(false);
                    Thread masterThread = new Thread(() =>
                    {
                        try
                        {
                            WaitHandle.WaitAll(waitHandles);
                        }
                        finally
                        {
                            resetEvent.Set();
                        }
                    });
                    masterThread.Name = "Master Thread";
                    masterThread.Start();
                    resetEvent.WaitOne(new TimeSpan(0, _pageCount * 5, 0));

                    for (int threadIndex = 0; threadIndex < parallelWorkers; threadIndex++)
                    {
                        try
                        {
                            if (workers[threadIndex].IsAlive)
                            {
                                VBotLogger.Error(() => $"Thread[{threadIndex}] is Aborted for Document[{Path.GetFileName(_documentProperties.Path)}]");
                                workers[threadIndex].Abort();
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.Log(nameof(ParallelSegmentedDocumentCreator), nameof(CreateSegmentedDocument));
                        }
                    }

                    VBotLogger.Trace(() => "[ParallelSegmentedDocumentCreator -> CreateSegmentedDocument] resetEvent signaled from Master Thread.");
                    int documentHeight = 0;
                    foreach (var document in _documents)
                    {
                        _documentProperties.PageProperties.Add(document.PageProperties);
                        VBotLogger.Trace(() => string.Format("[ParallelSegmentedDocumentCreator -> CreateSegmentedDocument] PageProperties Height Width:{0} Segmented Data Region Count:{1}", document.PageProperties != null ? document.PageProperties.Height.ToString() + " " + document.PageProperties.Width.ToString() : string.Empty, document.SegmentedDocument != null && document.SegmentedDocument.Regions != null ? document.SegmentedDocument.Regions.Count : 0));
                        new SegmentedDocumentOperations().Merge(segDoc, document.SegmentedDocument, 0, documentHeight);
                        documentHeight += document.PageProperties.Height;
                    }

                    _documentProperties.Height = documentHeight;
                    _documentProperties.Width = _documentProperties.PageProperties[0].Width;
                }
                else
                {
                    VBotLogger.Error(() => string.Format("[ParallelSegmentedDocumentCreator -> CreateSegmentedDocument] getPageCount returned pagecount less than 1. PageCount: {0}", _pageCount));
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ParallelSegmentedDocumentCreator), nameof(CreateSegmentedDocument));
                throw;
            }

            return segDoc;
        }

        private int getParallelWorkersRequired(int pageCount)
        {
            int noOfWorkers = _maxDegreeOfParallelism < pageCount ? _maxDegreeOfParallelism : pageCount;
            if (noOfWorkers <= 0)
            {
                noOfWorkers = 1;
            }

            return noOfWorkers;
        }

        private int getNextPage()
        {
            int newPageIndex = -1;
            lock (_lockObject)
            {
                if (_currentPage < _pageCount)
                {
                    newPageIndex = _currentPage;
                    _currentPage++;
                }
            }
            return newPageIndex;
        }

        private void processPage(object param)
        {
            int pageIndex;
            //string dataPath = _dataPath;//string.Format("{0}{1}", _dataPath, Convert.ToInt32(param));
            while ((pageIndex = getNextPage()) > -1)
            {
                //var processedPage = processPage(pageIndex, dataPath);
                var processedPage = processPage(pageIndex);
                if (processedPage.PageIndex == -1)
                {
                    break;
                }
                _documents[processedPage.PageIndex] = processedPage; //new Tuple<SegmentedDocument, int, PageProperties>(processedPage.Item2, processedPage.Item3, processedPage.Item4);
            }
        }

        private SegmentedDocumentInfo processPage(int index) //, string dataPath)
        {
            Page processedPage = null;
            var processedSegementedPage = new SegmentedDocumentInfo(-1, null, null); //new Tuple<int, SegmentedDocument, int, PageProperties>(-1, null, 0, null);
            string tempFilePath = null;
            try
            {
                processedPage = _visionBotPageImageProvider.GetImageFromProcessDocument(index);
                VBotLogger.Trace(() => string.Format("[ParallelSegmentedDocumentCreator -> processPage] page index: {0} processedPage is not null: {1}", index, processedPage != null));
                VBotLogger.Trace(() => string.Format("[ParallelSegmentedDocumentCreator -> processPage] page index: {0} PageProperties Height Width: {1}", index, processedPage != null && processedPage.PageProperties != null ? processedPage.PageProperties.Height.ToString() + " " + processedPage.PageProperties.Width.ToString() : string.Empty));
                PageProperties pageProperties = processedPage.PageProperties;
                //_documentProperties.PageProperties.Add(pageProperties);
                tempFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()+".tif");
                processedPage.ProcessedPageImage.Save(tempFilePath);

                string skewcorrectedFilePath = _documentProperties.Path + "_Page" + index.ToString() + ".vimg";

                ImageDetails imageDetails = new ImageDetails(tempFilePath, _documentProperties.LanguageCode);
                imageDetails.AdditionalParameters.Add(
                    Tesseract4_OCREngine.SKEW_AND_ORIENTATION_CORRECTED_IMAGE_PATH
                    , skewcorrectedFilePath);

                SegmentedDocument segmentedDocument = getSegmentedDocumentForPage(imageDetails);

                processedSegementedPage = new SegmentedDocumentInfo(index
                    , segmentedDocument
                    , pageProperties);

                if (File.Exists(skewcorrectedFilePath))
                    File.Delete(skewcorrectedFilePath);
                VBotLogger.Trace(() => string.Format("[ParallelSegmentedDocumentCreator -> processPage] page index: {0} SegmentedData Region Count: {1}", index, processedSegementedPage.SegmentedDocument != null && processedSegementedPage.SegmentedDocument.Regions != null ? processedSegementedPage.SegmentedDocument.Regions.Count : 0));
            }
            finally
            {
                processedPage?.Dispose();
                deleteFile(tempFilePath);
            }
            return processedSegementedPage;
        }

        private class SegmentedDocumentInfo
        {
            private SegmentedDocument _segmentedDocument;
            private PageProperties _pageProperties;
            private int _pageIndex;

            public SegmentedDocument SegmentedDocument
            {
                get
                {
                    return _segmentedDocument;
                }
            }

            public PageProperties PageProperties
            {
                get
                {
                    return _pageProperties;
                }
            }

            public int PageIndex
            {
                get
                {
                    return _pageIndex;
                }
            }

            public SegmentedDocumentInfo(int pageIndex, SegmentedDocument segmentedDocument, PageProperties pageProperties)
            {
                _pageIndex = pageIndex;
                _segmentedDocument = segmentedDocument;
                _pageProperties = pageProperties;
            }
        }
    }
}