﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Drawing;

    internal class Page : IDisposable
    {
        private PageProperties _pageProperties;

        public PageProperties PageProperties
        {
            get
            {
                return _pageProperties;
            }
        }

        private Image _processedPageImage;

        public Image ProcessedPageImage
        {
            get
            {
                return _processedPageImage;
            }
        }

        private bool _isProcessed;

        public bool IsProcessed
        {
            get
            {
                return _isProcessed;
            }
        }

        public Page(PageProperties pageProperties, Image processedPageImage, bool isProcessed)
        {
            _pageProperties = pageProperties;
            _processedPageImage = processedPageImage;
            _isProcessed = isProcessed;
        }

        public Page Clone()
        {
            PageProperties pageProperties = _pageProperties.Clone();
            Image clonedImage = null;
            int count = 0;
            do
            {
                count++;
                try
                {
                    clonedImage = new ImageOperations().CloneImage(_processedPageImage);
                    if (clonedImage != null)
                    {
                        (clonedImage as Bitmap)?.SetResolution(_processedPageImage.HorizontalResolution, _processedPageImage.VerticalResolution);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(Page), nameof(Clone));
                }
            } while (count < 2);

            Page page = new Page(pageProperties, clonedImage, true);
            return page;
        }

        public void Dispose()
        {
            try
            {
                _pageProperties = null;
                if (_processedPageImage != null)
                {
                    _processedPageImage.Dispose();
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(Page), nameof(Dispose));
            }
        }
    }
}