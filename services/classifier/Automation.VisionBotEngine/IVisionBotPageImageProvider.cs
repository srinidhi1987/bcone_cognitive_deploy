﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;

namespace Automation.VisionBotEngine
{
    internal interface IVisionBotPageImageProvider
    {
        Page GetImageFromLayout(Layout layout, int pageIndex);
        Page GetImageFromProcessDocument(int pageIndex);

        CroppedImage GetCroppedImageFromProcessDocument(int x, int y, int w, int h);

        CroppedImage GetCroppedImageFromFromProcessDocument(int x, int y, int w, int h, int pageIndex);

        CroppedImage GetCroppedImageFromLayout(Layout layout, int x, int y, int w, int h);

        CroppedImage GetCroppedImageFromFromLayout(Layout layout, int x, int y, int w, int h, int pageIndex);

        ImageProcessingConfig GetImageProcessingConfig();


    }
}