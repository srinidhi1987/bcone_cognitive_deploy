﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.Generic.Compression;
    using Automation.Generic.Serialization;
    using Automation.VisionBotEngine.Model;
    using Generic;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public interface IVBotDocSetStorage
    {
        void Save(VBotDocSet vBotDocSet);

        void Save(VBotDocSet vBotDocSet, string parentFolder);

        VBotDocSet Load(string parentFolder);

        VBotDocSet Load();

        void Delete(string parentFolder);
    }

    [Obfuscation(Exclude = true, ApplyToMembers = false)]
    public class VBotDocSetStorage : IVBotDocSetStorage
    {
        private string PackageSourceFile;

        internal const string JsonMimeContentType = "text/json";

        internal const string DocSetFileName = "vbot.docset";

        public VBotDocSetStorage(string packageSourceFile)
        {
            this.PackageSourceFile = packageSourceFile;
        }

        public void Save(VBotDocSet vBotDocSet)
        {
            VBotLogger.Trace(() => "[VBotDocSetStorage::Save] Saving DocSet....");
            using (var resourcePackage = new VResourcePackage())
            {
                this.createOrOpenResourcePackage(resourcePackage);

                UpdateCotentsFile(vBotDocSet, resourcePackage, string.Empty);

                var resourceData = this.createResourceData(vBotDocSet, string.Empty);

                resourcePackage.AddResource(resourceData);
                resourcePackage.Close();
            }
        }

        public void Save(VBotDocSet testDocSet, string parentFolder)
        {
            using (var resourcePackage = new VResourcePackage())
            {
                this.createOrOpenResourcePackage(resourcePackage);

                UpdateCotentsFile(testDocSet, resourcePackage, parentFolder);
                var resourceData = this.createResourceData(testDocSet, parentFolder);

                resourcePackage.AddResource(resourceData);
                resourcePackage.Close();
            }
        }

        public VBotDocSet Load(string parentFolder)
        {
            return getVBotDocSet(parentFolder);
        }

        public VBotDocSet Load()
        {
            return this.getVBotDocSet(string.Empty);
        }

        public void Delete(string parentFolder)
        {
            if (!string.IsNullOrWhiteSpace(parentFolder))
            {
                string docSetFilePath = Path.Combine(parentFolder, DocSetFileName);
                using (var resourcePackage = new VResourcePackage())
                {
                    this.createOrOpenResourcePackage(resourcePackage);

                    var resourceData = resourcePackage.GetResource(docSetFilePath);
                    var vBotDocSet = DeserializeData(resourceData);

                    foreach (var documentSetItem in vBotDocSet.DocItems)
                    {
                        DeleteFile(resourcePackage, Path.Combine(parentFolder, documentSetItem.Id));
                    }

                    DeleteFile(resourcePackage, docSetFilePath);
                    resourcePackage.Close();
                }
            }
        }

        private static void DeleteFile(VResourcePackage resourcePackage, string fileToDelete)
        {
            try
            {
                resourcePackage.DeleteFile(fileToDelete);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VBotDocSetStorage), nameof(DeleteFile), $"File \"{fileToDelete}\"");
            }
        }

        private VBotDocSet getVBotDocSet(string parentFolder)
        {
            VBotDocSet vBotDocSet;
            string docSetFilePath = Path.Combine(parentFolder, DocSetFileName);
            using (var resourcePackage = new VResourcePackage())
            {
                this.createOrOpenResourcePackage(resourcePackage);

                var resourceData = resourcePackage.GetResource(docSetFilePath);
                vBotDocSet = DeserializeData(resourceData);

                foreach (var documentSetItem in vBotDocSet.DocItems)
                {
                    var vBotTempPath = createTempVisionBotDirectory(documentSetItem);

                    documentSetItem.DocumentProperties.Path = Path.Combine(vBotTempPath, documentSetItem.Id);
                    var docPath = documentSetItem.DocumentProperties.Path;
                    updateDocSetPageProperties(documentSetItem.DocumentProperties);
                    resourceData = resourcePackage.GetResource(Path.Combine(parentFolder, documentSetItem.Id));

                    if (resourceData.Content == null)
                    {
                        continue;
                    }

                    if (File.Exists(docPath))
                    {
                        File.Delete(docPath);
                    }

                    using (var fileStream = File.Create(docPath))
                    {
                        using (var compressedStream = resourceData.Content)
                        {
                            compressedStream.CopyTo(fileStream);
                            compressedStream.Close();
                        }

                        fileStream.Close();
                    }
                }

                resourcePackage.Close();
            }

            return vBotDocSet;
        }

        private void updateDocSetPageProperties(DocumentProperties documentProperties)
        {
            if (documentProperties.PageProperties == null || documentProperties.PageProperties.Count == 0)
            {
                documentProperties.PageProperties = new List<PageProperties>();
                documentProperties.PageProperties.Add(new PageProperties() { PageIndex = 0, Width = documentProperties.Width, Height = documentProperties.Height });
            }
        }

        private void createOrOpenResourcePackage(VResourcePackage resourcePackage)
        {
            if (File.Exists(this.PackageSourceFile))
            {
                resourcePackage.Open(this.PackageSourceFile);
            }
            else
            {
                resourcePackage.Create(this.PackageSourceFile);
            }
        }

        private ResourceData createResourceData(VBotDocSet vBotDocSet, string parentFolder)
        {
            return new ResourceData
            {
                UniqueId = Path.Combine(parentFolder, DocSetFileName),
                Content = this.getSerializedContent(vBotDocSet),
                ContentType = JsonMimeContentType,
                FilePath = string.Empty
            };
        }

        private Stream getSerializedContent(VBotDocSet vBotDocSet)
        {
            ISerialization serialization = new JSONSerialization();
            return serialization.SerializeToStream(vBotDocSet);
        }

        private static VBotDocSet DeserializeData(ResourceData resourceData)
        {
            ISerialization serialization = new JSONSerialization();
            var vBotDocSet = resourceData.Content == null ? new VBotDocSet() : serialization.DeserializeFromStream<VBotDocSet>(resourceData.Content);

            return vBotDocSet;
        }

        private static void UpdateCotentsFile(VBotDocSet vBotDocSet, IResourcePackage resourcePackage, string parentFolder)
        {
            string docSetFileName = Path.Combine(parentFolder, DocSetFileName);
            var resourceData = resourcePackage.GetResource(docSetFileName);

            if (resourceData.Content != null)
            {
                var currentVbotDocSet = DeserializeData(resourceData);
                foreach (var vBotDocSetItem in currentVbotDocSet.DocItems)
                {
                    var isFound = vBotDocSet.DocItems.Any(docItem => docItem.Id.Equals(vBotDocSetItem.Id));

                    if (!isFound)
                    {
                        resourcePackage.DeleteFile(Path.Combine(parentFolder, vBotDocSetItem.Id));
                    }
                }

                foreach (var vBotDocItem in vBotDocSet.DocItems)
                {
                    var isFound = currentVbotDocSet.DocItems.Any(docItem => docItem.Id.Equals(vBotDocItem.Id));

                    if (!isFound)
                    {
                        createTempVisionBotDirectory(vBotDocItem);

                        var imagePath = vBotDocItem.DocumentProperties.Path;

                        if (File.Exists(imagePath))
                        {
                            resourcePackage.AddFile(Path.Combine(parentFolder, vBotDocItem.Id), imagePath);
                        }

                        vBotDocItem.ImageView = null;
                    }
                }

                resourcePackage.DeleteFile(docSetFileName);
            }
            else
            {
                foreach (var vBotDocItem in vBotDocSet.DocItems)
                {
                    createTempVisionBotDirectory(vBotDocItem);

                    var imagePath = vBotDocItem.DocumentProperties.Path;

                    if (File.Exists(imagePath))
                    {
                        resourcePackage.AddFile(Path.Combine(parentFolder, vBotDocItem.Id), imagePath);
                    }

                    vBotDocItem.ImageView = null;
                }
            }
        }

        private static string createTempVisionBotDirectory(VBotDocItem vBotDocItem)
        {
            var vBotTempPath = Path.Combine(
                Path.GetTempPath(),
                Automation.Cognitive.VisionBotEngine.Properties.Resources.VisionBotTempDirectory,
                vBotDocItem.DocumentName);
            Directory.CreateDirectory(vBotTempPath);

            return vBotTempPath;
        }
    }
}