﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Automation.Generic.Compression;
using Automation.Generic.Serialization;

namespace Automation.VisionBotEngine
{
    interface IVBotDocSet
    {
        void AddDoc(string docId, DataTable data);
        void RemoveDoc(string docId);
        void UpdateDoc(string docId, DataTable data);
    }

    public class VBDocSetInvalidOperationException : Exception
    {
        public VBDocSetInvalidOperationException(string message)
            : base(message)
        {
            
        }
    }

    //TODO : Single Doc Set Item with Image
    public class DocSetItem
    {
        public string DocId { get; set; }
        public string DataId { get; set; }

    }

    //TODO : Doc Set with items

    //TODO : Doc set reader writer
    public class VBotDocSet : IVBotDocSet
    {
        private const string MapperId = "Mapper";
        internal const string JsonMimeContentType = "text/json";

        private string _packageSourceFile;

        public VBotDocSet(string packageSourceFile)
        {
            _packageSourceFile = packageSourceFile;
        }

        public void AddDoc(string docId, DataTable data)
        {
            using (var resourcePackage = new ResourcePackage())
            {
                createOrOpenResourcePackage(resourcePackage);
                var resourceData = resourcePackage.GetResource(MapperId);
                List<DocSetItem> mapper = getMapper(resourceData);

                var docSetItems = findDocSetItemByDocId(docId, mapper);
                if (docSetItems.Any())
                {
                    throw new VBDocSetInvalidOperationException("Cannot add same item to document set");
                }

                mapper.Add(createDocSetItemFromDocId(docId));

                updateMapper(resourceData, resourcePackage, mapper);

                resourceData = createResourceData(docId, data);
                resourcePackage.AddResource(resourceData);

                resourcePackage.Close();
            }
        }

        private void updateMapper(ResourceData resourceData, ResourcePackage resourcePackage, List<DocSetItem> mapper)
        {
            if (!string.IsNullOrEmpty(resourceData.UniqueId))
            {
                resourcePackage.DeleteFile(MapperId);
            }
            resourceData = createResourceDataFromList(MapperId, mapper);
            resourcePackage.AddResource(resourceData);
        }

        private static DocSetItem createDocSetItemFromDocId(string docId)
        {
            return new DocSetItem
            {
                DocId = docId,
                DataId = docId
            };
        }

        private void createOrOpenResourcePackage(ResourcePackage resourcePackage)
        {
            if (File.Exists(_packageSourceFile))
            {
                resourcePackage.Open(_packageSourceFile);
            }
            else
            {
                resourcePackage.Create(_packageSourceFile);
            }
        }

        private ResourceData createResourceDataFromList(string docId, List<DocSetItem> docSetMapper)
        {
            ISerialization serialization = new JSONSerialization();
            return new ResourceData
            {
                UniqueId = docId,
                Content = serialization.SerializeToStream(docSetMapper),
                ContentType = JsonMimeContentType,
                FilePath = string.Empty
            };
        }

        private List<DocSetItem> getMapper(ResourceData resourceData)
        {
            if (!string.IsNullOrEmpty(resourceData.UniqueId))
            {
                ISerialization serialization = new JSONSerialization();
                return serialization.DeserializeFromStream<List<DocSetItem>>(resourceData.Content);
            }
            
            return new List<DocSetItem>();
        }

        public void RemoveDoc(string docId)
        {
            using (var resourcePackage = new ResourcePackage())
            {
                createOrOpenResourcePackage(resourcePackage);

                resourcePackage.DeleteFile(docId);

                resourcePackage.Close();
            }
        }

        public void UpdateDoc(string docId, DataTable data)
        {
            using (var resourcePackage = new ResourcePackage())
            {
                createOrOpenResourcePackage(resourcePackage);
                var resourceData = resourcePackage.GetResource(MapperId);
                List<DocSetItem> mapper = getMapper(resourceData);

                var docSetItems = findDocSetItemByDocId(docId, mapper);
                if (!docSetItems.Any())
                {
                    throw new VBDocSetInvalidOperationException("Unable to find specified item from document set");
                }

                mapper.Add(createDocSetItemFromDocId(docId));

                updateMapper(resourceData, resourcePackage, mapper);

                if (!string.IsNullOrEmpty(resourceData.UniqueId))
                {
                    resourcePackage.DeleteFile(docId);
                }
                resourceData = createResourceData(docId, data);
                resourcePackage.AddResource(resourceData);

                resourcePackage.Close();
            }
        }

        private static List<DocSetItem> findDocSetItemByDocId(string docId, List<DocSetItem> mapper)
        {
            return new List<DocSetItem>(mapper.Where(item => item.DocId.Equals(docId)));
        }

        private ResourceData createResourceData(string docId, DataTable data)
        {
            return new ResourceData
            {
                UniqueId = docId,
                Content = getDataTableStream(data),
                ContentType = JsonMimeContentType,
                FilePath = string.Empty
            };
        }

        private Stream getDataTableStream(DataTable dataTable)
        {
            ISerialization serialization = new JSONSerialization();
            return serialization.SerializeToStream(dataTable);
        }
    }
}
