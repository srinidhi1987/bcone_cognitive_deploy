﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;

namespace Automation.VisionBotEngine
{
    using System.Reflection;

    using Automation.VisionBotEngine.Validation;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]

    public interface IVBotEngine
    {
        VBotDocument GetVBotDocument(VisionBot vbot
            , DocumentProperties docProperties
            , IVBotValidatorDataManager validatorDataManager
            , VBotEngineSettings engineSettings
            , ImageProcessingConfig settings
            , string documentId
            , int confidenceThreshold);
       
        //TODO : Refactoring is pending
        void ExtractVBotData(VisionBot vbot, VBotDocument vbotDoc, string dataPath, VBotEngineSettings engineSettings, ImageProcessingConfig imageProcessingSettings);
        Field GetValueField(FieldLayout fieldLayout, VBotDocument vbotDoc);

        Field GetValueField(FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc);

        //TODO : Refactoring is pending
        Field GetValueField(FieldLayout fieldLayout, FieldDef fieldDef, VBotDocument vbotDoc, DocumentProperties docProps, VBotEngineSettings engineSettings, ImageProcessingConfig imageConfig);

        int GetPageOrientation(string docPath, string dataPath);
    }
}
