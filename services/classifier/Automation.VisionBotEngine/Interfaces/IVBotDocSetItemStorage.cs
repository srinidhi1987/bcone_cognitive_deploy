﻿using System;

namespace Automation.VisionBotEngine
{
    using System.Collections.Generic;
    using System.Data;
    using System.IO;

    using Automation.VisionBotEngine.Model;
    using System.Drawing;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public interface IVBotDocumentStorage
    {
        VBotDocItem AddDocument(string documentPath);

        VBotDocItem AddDocument(string documentPath, DataTable data, Image image);

        void RemoveDocItem(string Id);

        void RemoveDocItem(VBotDocItem vBotDocItem);

        VBotDocItem GetDocItemById(string Id);

        void UpdateData(string id, BenchmarkData benchmarkData, bool isBenchmarkDataValidated, bool isBenchmarkStructureMismatchFound);

        List<VBotDocItem> GeDocItems();

        Image GetImage(VBotDocItem vBotDocItem);

        void UpdateTestSetBenchmarkMismatchStatus(string id, bool isBenchmarkStructureMismatchFound);
    }

    [Obfuscation(Exclude = true, ApplyToMembers = false)]
    public class VBotDocumentStorage : IVBotDocumentStorage
    {
        public List<VBotDocItem> VBotDocSetItems { get; set; }

        public VBotDocumentStorage(List<VBotDocItem> vBotDocSetItems)
        {
            VBotDocSetItems = vBotDocSetItems;
        }

        public VBotDocumentStorage()
        {
            VBotDocSetItems = new List<VBotDocItem>();
        }

        public VBotDocItem AddDocument(string documentPath)
        {
            VBotDocItem vBotDocItem = new VBotDocItem()
            {
                DocumentName = Path.GetFileName(documentPath),
            };

            VBotDocSetItems.Add(vBotDocItem);
            return vBotDocItem;
        }

        public VBotDocItem AddDocument(string documentPath, DataTable data, Image image)
        {
            VBotDocItem vBotDocItem = new VBotDocItem()
            {
                DocumentName = Path.GetFileName(documentPath),
                ImageView = image
            };

            VBotDocSetItems.Add(vBotDocItem);
            return vBotDocItem;
        }

        public Image GetImage(VBotDocItem vBotDocItem)
        {
            Image image = null;

            string imagePath = Path.Combine(Path.GetTempPath(), Automation.Cognitive.VisionBotEngine.Properties.Resources.VisionBotTempDirectory, vBotDocItem.DocumentName, vBotDocItem.Id);
            if (File.Exists(imagePath))
            {
                using (FileStream stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
                {
                    image = Image.FromStream(stream);

                    stream.Close();
                }
            }

            return image;
        }

        public void RemoveDocItem(string Id)
        {
            for (int index = 0; index < VBotDocSetItems.Count; index++)
            {
                if (VBotDocSetItems[index].Id.Equals(Id))
                {
                    VBotDocSetItems.RemoveAt(index);
                    break;
                }
            }
        }

        public void RemoveDocItem(VBotDocItem vBotDocItem)
        {
            RemoveDocItem(vBotDocItem.Id);
        }

        public VBotDocItem GetDocItemById(string Id)
        {
            return VBotDocSetItems.Count > 0
                ? VBotDocSetItems.Find(x => x.Id.Equals(Id))
                : new VBotDocItem();
        }

        public void UpdateData(string id, BenchmarkData benchmarkData, bool isBenchmarkDataValidated, bool isBenchmarkStructureMismatchFound)
        {
            for (int index = 0; index < VBotDocSetItems.Count; index++)
            {
                if (VBotDocSetItems[index].Id.Equals(id))
                {
                    VBotDocSetItems[index].BenchmarkData = benchmarkData;
                    VBotDocSetItems[index].IsBenchMarkDataValidated = isBenchmarkDataValidated;
                    VBotDocSetItems[index].IsBenchmarkStructureMismatchFound = isBenchmarkStructureMismatchFound;
                    break;
                }
            }
        }

        public void UpdateTestSetBenchmarkMismatchStatus(string id, bool isBenchmarkStructureMismatchFound)
        {
            foreach (var docSetItem in VBotDocSetItems)
            {
                if (docSetItem.Id.Equals(id))
                {
                    docSetItem.IsBenchmarkStructureMismatchFound = isBenchmarkStructureMismatchFound;
                    break;
                }
            }
        }

        public List<VBotDocItem> GeDocItems()
        {
            return VBotDocSetItems;
        }

        public byte[] GetByteImage(VBotDocItem vBotDocItem)
        {
            throw new NotImplementedException();
        }
    }
}