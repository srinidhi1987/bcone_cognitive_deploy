﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Automation.VisionBotEngine.Model;

namespace Automation.VisionBotEngine
{
    interface IVBotDocClassifier
    {
        ClasificationResult Clasify(VisionBot vbot, List<Field> fields, int LayoutConfidenceThreshold);
        void UpdatetAspectLayoutConfidence(LayoutConfidence layoutConfidence, DocumentProperties layoutDoc);
    }
}
