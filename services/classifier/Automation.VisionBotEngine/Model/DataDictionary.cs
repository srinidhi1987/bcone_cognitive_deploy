﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automation.VisionBotEngine.Model
{
    class DataDictionary
    {
        public List<WordDictionary> Words;

        public DataDictionary()
        {
            Words = new List<WordDictionary>();

            Words.Add(new WordDictionary("ordered", "[ordered", "frdered"));
        }
    }

    class WordDictionary
    {
        public string Name { get; set; }
        public List<string> Values { get; set; }

        public WordDictionary()
        {
            Name = string.Empty;
            Values = new List<string>();
        }

        public WordDictionary(string name, params string[] values)
        {
            Name = string.Empty;
            Values = new List<string>();
        }
    }
}
