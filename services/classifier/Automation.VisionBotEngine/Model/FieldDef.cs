﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Collections.Specialized;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class FieldDef
    {
        public string Id { get; set; }

        //public string FieldId { get; set; }
        public string Name { get; set; }

        public FieldValueType ValueType { get; set; }
        public string Formula { get; set; }
        public string ValidationID { get; set; }
        public bool IsRequired { get; set; }
        public string DefaultValue { get; set; }

        public string StartsWith { get; set; }
        public string EndsWith { get; set; }

        public string FormatExpression { get; set; }

        public bool IsDeleted { get; set; }

        public FieldDef()
        {
            Id = string.Empty;
            //FieldId = string.Empty;
            Name = string.Empty;
            ValueType = FieldValueType.Text;
            Formula = string.Empty;
            ValidationID = string.Empty;
            DefaultValue = string.Empty;

            StartsWith = string.Empty;
            EndsWith = string.Empty;
            FormatExpression = string.Empty;
            IsDeleted = false;
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TableData : FieldData
    {
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class DataRecord
    {
        public List<FieldData> Fields { get; set; }

        public DataRecord()
        {
            Fields = new List<FieldData>();
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class FieldData
    {
        public string Id { get; }
        public FieldDef Field { get; set; }
        public FieldValue Value { get; set; }
        public ValidationIssue ValidationIssue { get; set; }

        public FieldData()
        {
            Id = Guid.NewGuid().ToString();
            Field = new FieldDef();
            Value = new TextValue();
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class ValidationIssue
    {
        public virtual FieldDataValidationIssueCodes IssueCode { get; set; }

        public ValidationIssue(FieldDataValidationIssueCodes issueCode)
        {
            this.IssueCode = issueCode;
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public abstract class FieldValue
    {
        public Field Field { get; set; }

        public FieldValue()
        {
            Field = new Field();
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TableValue : FieldValue
    {
        public TableDef TableDef { get; set; }
        public List<FieldDef> Headers { get; set; }
        public List<DataRecord> Rows { get; set; }

        public TableValue()
        {
            Rows = new List<DataRecord>();
            Headers = new List<FieldDef>();
        }
    }

    internal class DateValue : FieldValue
    {
        public DateTime Value { get; set; }
    }

    internal class CurrencyValue : FieldValue
    {
        public decimal Value { get; set; }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TextValue : FieldValue
    {
        public string Value { get; set; }
    }

    internal class NumberValue : FieldValue
    {
        public long Value { get; set; }
    }
}