﻿
namespace Automation.VisionBotEngine.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class VBotDocItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DocumentProperties DocumentProperties { get; set; }
        public BenchmarkData BenchmarkData { get; set; }

        public bool IsBenchMarkDataValidated { get; set; }

        public bool IsBenchmarkStructureMismatchFound { get; set; }

        public Image ImageView
        {
            get
            {
                return null;
            }
            set
            {
               if (value != null)
                {
                    string imagePath = Path.Combine(Path.GetTempPath(), Properties.Resources.VisionBotTempDirectory, this.DocumentName);
                    Directory.CreateDirectory(imagePath);
                    imagePath = Path.Combine(imagePath, this.Id);

                    if (File.Exists(imagePath))
                        File.Delete(imagePath);

                    value.Save(imagePath);
                }
            }
        }

        public string DocumentName { get; set; }
        public SIRFields SirFields { get; set; }
        public VBotDocItem()
        {
            this.Id = Guid.NewGuid().ToString();
            DocumentName = string.Empty;
            Name = string.Empty;
            this.BenchmarkData = new BenchmarkData();
            DocumentProperties = new DocumentProperties();
        }
    }
    

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class BenchmarkData
    {
        public DataTable Fields { get; set; }
        public DataSet TableSet { get; set; }

        public BenchmarkData()
        {
            Fields = new DataTable();
            TableSet = new DataSet();
        }
    }

    internal class TestData
    {
        public List<TestFieldData> Fields { get; set; }
        public List<TestTableData> Tables { get; set; }

        public TestData()
        {
            Fields = new List<TestFieldData>();
            Tables = new List<TestTableData>();
        }
    }

    internal class TestFieldData
    {
        public string ID { get; set; }
        public string Value { get; set; }

        public TestFieldData()
        {
            ID = string.Empty;
            Value = string.Empty;
        }
    }

    internal class TestTableData
    {
        public List<TestFieldData> Headers { get; set; }
        public List<TableRow> Rows { get; set; }

        public TestTableData()
        {
            Headers = new List<TestFieldData>();
            Rows = new List<TableRow>();
        }
    }

    internal class TableRow
    {
        public List<string> Row { get; set; }

        public TableRow()
        {
            Row = new List<string>();
        }
    }

}
