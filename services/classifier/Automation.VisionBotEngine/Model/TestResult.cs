﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;

namespace Automation.VisionBotEngine.Model
{
    public class TestResult
    {
        public List<ComparisonError> ComparisonErrors { get; private set; }
        public double ConfidenceScore { get; private set; }

        public TestResult()
        {
            this.ComparisonErrors = new List<ComparisonError>();
            this.ConfidenceScore = 0.0;
        }

        public TestResult(List<ComparisonError> comparisonErrors, double confidence)
        {
            this.ComparisonErrors = comparisonErrors;
            this.ConfidenceScore = confidence;
        }
    }
}
