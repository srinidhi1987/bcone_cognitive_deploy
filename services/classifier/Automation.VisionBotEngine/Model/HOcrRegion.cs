﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Model
{
    public class HOcrRegion
    {
        public int X1 { get; set; }

        public int X2 { get; set; }

        public int Y1 { get; set; }

        public int Y2 { get; set; } 

        public RegionType Type { get; set; }

        public int Confidence { get; set; }

        public string Text { get; set; }
    }
}
