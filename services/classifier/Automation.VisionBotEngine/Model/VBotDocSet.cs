﻿
namespace Automation.VisionBotEngine.Model
{
    using System;
    using System.Collections.Generic;


    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class VBotDocSet
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<VBotDocItem> DocItems { get; set; }

        public VBotDocSet()
        {
            Id = Guid.NewGuid().ToString();
            DocItems = new List<VBotDocItem>();
        }
    }

}
