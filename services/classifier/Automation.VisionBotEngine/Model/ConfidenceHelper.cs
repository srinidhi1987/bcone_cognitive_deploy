﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using Automation.Cognitive.VisionBotEngine.Model.Generic;
using Automation.VisionBotEngine.Model;


namespace Automation.Cognitive.VisionBotEngine.Common
{
    public class ConfidenceHelper
    {
        public int GetMergedFieldConfidence(List<Field> fields)
        {
            IList<Tuple<double, int>> fieldConfidenceAndLength = new List<Tuple<double, int>>();
            foreach (Field f in fields)
            {
                fieldConfidenceAndLength.Add(new Tuple<double, int>((double)f.Confidence/100, f.Text.Length));
            }
            return (int)new ConfidenceProvider().GetMergerdFieldConfidence(fieldConfidenceAndLength);
        }
       
    }
}
