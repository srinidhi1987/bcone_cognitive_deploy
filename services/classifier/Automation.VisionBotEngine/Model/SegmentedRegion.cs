﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class SegmentedRegion
    {
        public string Text { get; set; }
        public SegmentationType Type { get; set; }
        public int Confidence { get; set; }
        public Rectangle Bounds { get; set; }
        public decimal Angle { get; set; }

        public SegmentedRegion()
        {
            Text = string.Empty;
            Type = SegmentationType.Word;
            Confidence = 0;
            Bounds = new Rectangle();
        }

        public SegmentedRegion Clone()
        {
            SegmentedRegion segmentedRegion = new SegmentedRegion();
            segmentedRegion.Bounds = Bounds;
            segmentedRegion.Text = Text;
            segmentedRegion.Type = Type;
            segmentedRegion.Confidence = Confidence;
            segmentedRegion.Angle = Angle;

            return segmentedRegion;
        }
    }
}