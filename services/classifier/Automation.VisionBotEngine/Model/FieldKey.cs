﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    internal enum Direction
    {
        Left,
        Right,
        Top,
        Bottom
    }

    internal class FieldKey
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Direction Direction { get; set; }

        public bool IsMultilineValue { get; set; }

        public FieldKey()
        {
        }

        public FieldKey(string name, Direction direction, bool isMultiline)
        {
            Id = Guid.NewGuid().ToString();

            Name = name;

            Direction = direction;

            IsMultilineValue = isMultiline;
    }
    }
}
