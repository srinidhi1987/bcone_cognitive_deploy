﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;

namespace Automation.VisionBotEngine.Model
{
    using Newtonsoft.Json;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class VisionBot
    {
        public string Id;

        [JsonIgnore]
        public string Name;

        public float Version;

        public List<Layout> Layouts;

        public DataModel DataModel;

        public BlueprintIdSnap DataModelIdStructure;

        public ImageProcessingConfig Settings;

        public VisionBot()
        {
            Layouts = new List<Layout>();
            DataModel = new DataModel();
            DataModelIdStructure = new BlueprintIdSnap();
            Settings = new ImageProcessingConfig();
            Version = 0;
        }
    }
}