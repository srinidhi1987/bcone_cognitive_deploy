﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class ImageProcessingConfig
    {
        #region Properties

        public bool EnableAutoRotation { get; set; } = true;

        public bool EnablePageOrientation { get; set; } = true;

        public bool EnableBorderFilter { get; set; } = true;

        public bool EnableBinarization { get; set; } = true;

        public bool EnableGrayscaleConversion { get; set; } = true;

        public bool EnableNoiseFilter { get; set; } = false;

        public bool EnableRemoveLines { get; set; } = false;

        public bool EnableContrastStretch { get; set; } = false;

        public BinarizationTypes BinarizationType { get; set; } = BinarizationTypes.Auto;

        public int NoiseThreshold { get; set; } = 145;

        public int BinarizationThreshold { get; set; } = 145;

        public bool EnbleBackgroundRemoval { get; set; } = false;

        public bool PreserveOriginalDpi { get; set; } = true;

        public bool UseVectorInfoInPdf { get; set; } = true;

        #endregion Properties

        public virtual ImageProcessingConfig Clone()
        {
            ImageProcessingConfig config = new ImageProcessingConfig();
            return Clone(config);
        }

        protected virtual ImageProcessingConfig Clone(ImageProcessingConfig config)
        {
            Copy(config, this);
            return config;
        }

        public static void Copy(ImageProcessingConfig to, ImageProcessingConfig from)
        {
            to.EnableAutoRotation = from.EnableAutoRotation;
            to.EnablePageOrientation = from.EnablePageOrientation;
            to.EnableBorderFilter = from.EnableBorderFilter;
            to.EnableBinarization = from.EnableBinarization;
            to.EnableGrayscaleConversion = from.EnableGrayscaleConversion;
            to.EnableNoiseFilter = from.EnableNoiseFilter;
            to.EnableRemoveLines = from.EnableRemoveLines;
            to.EnableContrastStretch = from.EnableContrastStretch;
            to.BinarizationType = from.BinarizationType;
            to.NoiseThreshold = from.NoiseThreshold;
            to.BinarizationThreshold = from.BinarizationThreshold;
            to.EnbleBackgroundRemoval = from.EnbleBackgroundRemoval;
            to.PreserveOriginalDpi = from.PreserveOriginalDpi;
            to.UseVectorInfoInPdf = from.UseVectorInfoInPdf;
        }
    }
}