﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class SegmentedDocument
    {
        public List<SegmentedRegion> Regions { get; set; }

        public SegmentedDocument()
        {
            Regions = new List<SegmentedRegion>();
        }

        public void Merge(SegmentedDocument documentToMerge,int horizonalOffset, int verticalOffset, double scaleX=1.0,double scaleY=1.0)
        {
            RectangleOperations rectangleOperations = new RectangleOperations();
            if (documentToMerge != null && documentToMerge.Regions != null)
            {
                for (int i = 0; i < documentToMerge.Regions.Count; i++)
                {
                    SegmentedRegion regionToMerge = documentToMerge.Regions[i];
                    regionToMerge.Bounds = rectangleOperations.AddOffsetWithScale(regionToMerge.Bounds, horizonalOffset, verticalOffset,scaleX,scaleY);
                    Regions.Add(regionToMerge);
                }
            }
        }
    }
}
