﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Model
{
    using System.IO;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    //TODO : Not required in model
    public class VisionBotManifest
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string ParentPath { get; set; }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class VisionBotManifestExtensions
    {
        public static string GetFilePath(this VisionBotManifest manifest)
        {
            return Path.Combine(manifest.ParentPath
                                , manifest.Name + Util.IQBots.Constants.VisionBot.FILE_EXTENSION_WITH_DOT);
        }
    }
}
