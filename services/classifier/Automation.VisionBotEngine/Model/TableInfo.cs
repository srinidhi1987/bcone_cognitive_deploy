﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    class TableInfo
    {
        public List<Field> Fields;
        public List<Line> Lines;
        public List<Field> Regions;
        public List<Field> Headers;

        public TableDef ColumnDefs;
        public TableLayout Layout;
     
        public TableInfo(List<Field> fields, List<Line> lines)
        {
            InitData();

            Fields = fields;
            Lines = lines;
        }

        public TableInfo Clone()
        {
            TableInfo cloneTableInfo = new TableInfo(CloneList(Fields), CloneList(Lines))
            {
                Regions=CloneList(Regions),
                Headers = CloneList(Headers),
                ColumnDefs = ColumnDefs.Clone(),
                Layout = Layout.Clone()
            };
            return cloneTableInfo;
        }

        public TableInfo(TableDef tableDef, TableLayout tableLayout, SIRFields sirFields)
        {
            InitData();

            Fields = sirFields.Fields;
            Lines = sirFields.Lines;
            Regions = sirFields.Regions;
            ColumnDefs = tableDef;
            Layout = tableLayout;
        }

        public TableInfo(List<Field> fields, List<Field> headers, List<Line> lines)
        {
            InitData();

            Fields = fields;
            Lines = lines;
            Headers = headers;
        }

        void InitData()
        {
            Fields = new List<Field>();
            Lines = new List<Line>();
            Headers = new List<Field>();

            ColumnDefs = new TableDef();
            Layout = new TableLayout();
        }

        private List<Field> CloneList(List<Field> listToClone)
        {
            return listToClone.Select(item => item.Clone()).ToList();
        }
        private  List<Line> CloneList(List<Line> listToClone)
        {
            return listToClone.Select(item => item.Clone()).ToList();
        }
    
    }
}
