﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    internal class CroppedImage : IDisposable
    {
        private Rectangle _bounds;
        private Bitmap fieldImage;
        private Rectangle _destinationImageBound;

        public Rectangle DestinationImageBound
        {
            get
            {
                return _destinationImageBound;
            }

            set
            {
                _destinationImageBound = value;
            }
        }

        public Rectangle Bounds
        {
            get
            {
                return _bounds;
            }
        }

        public CroppedImage(Bitmap fieldImage, Rectangle bounds)
        {
            this.fieldImage = fieldImage;
            _bounds = bounds;
        }

        public Bitmap GetCroppedImage()
        {
            return fieldImage;
        }

        public void Dispose()
        {
            if (fieldImage != null)
            {
                fieldImage.Dispose();
                fieldImage = null;
            }
        }
    }
}