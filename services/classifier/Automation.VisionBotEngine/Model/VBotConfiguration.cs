﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automation.VisionBotEngine.Model
{
    internal class VBotConfiguration
    {
        int MinSupportedDPI { get; set; }
        
        public VBotConfiguration()
        {
            MinSupportedDPI = 150;
        }
    }
}
