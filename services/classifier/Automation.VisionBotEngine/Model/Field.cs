﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class Field
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string Text { get; set; }

        public int Confidence { get; set; }

        //TODO : remove field type
        public SegmentationType SegmentationType { get; set; }

        public FieldType Type { get; set; }
        public Rectangle Bounds { get; set; }

        public decimal Angle { get; set; }

        public Field()
        {
            Id = string.Empty;
            Text = string.Empty;
            Confidence = 0;
            Bounds = new Rectangle();
            SegmentationType = SegmentationType.Word;
        }

        public Field Clone()
        {
            Field cloneField = new Field();
            cloneField.Id = Id;
            cloneField.ParentId = ParentId;
            cloneField.Text = Text;
            cloneField.Confidence = Confidence;
            cloneField.SegmentationType = SegmentationType;
            cloneField.Bounds = Bounds;
            cloneField.Type = Type;
            cloneField.Angle = Angle;
            return cloneField;
        }
    }
}