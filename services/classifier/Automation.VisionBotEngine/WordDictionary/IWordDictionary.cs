﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    public interface IWordDictionary
    {
        List<string> GetSimilarWords(string correctWord);
    }

    internal class EnglishWordDictionary : IWordDictionary
    {
        private XmlDocument xmlDocument;

        public EnglishWordDictionary(string applicationPath, string folderName)
            : this(Path.Combine(applicationPath, folderName, "WordDictionary.xml"))
        { }

        public EnglishWordDictionary(string xmlFilePath)
        {
            LoadDocument(xmlFilePath);
        }

        public EnglishWordDictionary(Stream dataStream)
        {
            LoadDocument(dataStream);
        }

        private void LoadDocument(string xmlFilePath)
        {
            if (!File.Exists(xmlFilePath))
            {
                VBotLogger.Warning(() => $"[EnglishWordDictionary] file '{xmlFilePath}' not found");
                return;
            }
            FileStream fileStream = new FileStream(xmlFilePath, FileMode.Open, FileAccess.Read);

            LoadDocument(fileStream);
        }

        private void LoadDocument(Stream dataStream)
        {
            xmlDocument = new XmlDocument();
            xmlDocument.XmlResolver = null;
            try
            {
                xmlDocument.Load(dataStream);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(EnglishWordDictionary), nameof(LoadDocument));
            }
        }

        public List<string> GetSimilarWords(string correctWord)
        {
            var result = new List<string>();

            if (xmlDocument != null)
                try
                {
                    var itemNodes = xmlDocument.DocumentElement.SelectNodes($"//WordDictionary/English/Words/Key[@Name='{correctWord}']/Values/item");

                    if (itemNodes != null)
                    {
                        foreach (XmlNode itemNode in itemNodes)
                        {
                            result.Add(itemNode.InnerText);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(EnglishWordDictionary), nameof(GetSimilarWords));
                }

            return result;
        }
    }
}