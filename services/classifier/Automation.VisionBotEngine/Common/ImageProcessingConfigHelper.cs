﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Common
{
    using Automation.Cognitive.VisionBotEngine.Properties;
    using Automation.Generic;
    using Automation.VisionBotEngine.Model;
    using Services.Client;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    internal static class ImageProcessingConfigHelper
    {
        public static ImageProcessingConfig GetImageProcessingSettings(ProjectDetails projectDetails)
        {
            if (projectDetails.OcrEngineDetails == null ||
                projectDetails.OcrEngineDetails.Length <= 0)
            {
                return null;
            }

            var engineType = projectDetails.OcrEngineDetails[0].EngineType;

            var configuredSetting = ImageProcessingConfigProvider
                 .Instance.GetSettings(engineType);

            if (configuredSetting != null)
            {
                    return configuredSetting;
            }

            engineType = engineType.ToLower();

            if (engineType.Equals(Resources.Tesseract4OcrEngineName.ToLower()))
            {
                ImageProcessingConfig config = GetDefaultTesseract4Settings();
                if (engineType.Equals(Resources.Tesseract4OcrEngineName.ToLower()))
                {
                    Tesseract4SettingProvider T4SettingsProvider = new Tesseract4SettingProvider();
                    Tesseract4Settings T4Settings = T4SettingsProvider.GetTesseract4Settings();
                    if (T4Settings.UseSegmentation == false)
                        config.EnableBorderFilter = true;
                }

                return config;
            }

            if (engineType.Equals(Resources.Tesseract3OcrEngineName.ToLower()))
                return GetDefaultTesseract3Settings();

            return GetDefaultImageProcessingConfig();
        }

        public static ImageProcessingConfig GetDefaultTesseract4Settings()
        {
            return new ImageProcessingConfig()
            {
                EnableAutoRotation = true,
                EnableBinarization = false,
                EnableBorderFilter = false,
                EnableContrastStretch = false,
                EnableNoiseFilter = true,
                EnablePageOrientation = true,
                EnableGrayscaleConversion = true,
                EnableRemoveLines = false,
                EnbleBackgroundRemoval = false,
                IsTesseract4Engine=true
            };
        }

        public static ImageProcessingConfig GetDefaultTesseract3Settings()
        {
            return new ImageProcessingConfig()
            {
                EnableAutoRotation = true,
                EnableBinarization = true,
                EnableBorderFilter = true,
                EnableContrastStretch = false,
                EnableNoiseFilter = true,
                EnablePageOrientation = true,
                EnableGrayscaleConversion = true,
                EnableRemoveLines = false,
                EnbleBackgroundRemoval = false
            };
        }

        public static ImageProcessingConfig GetDefaultImageProcessingConfig()
        {
            VBotLogger.Trace(() => $"GetDefaultImageProcessingConfig");

            return new ImageProcessingConfig()
            {
                EnableAutoRotation = true,
                EnableBinarization = false,
                EnableBorderFilter = false,
                EnableContrastStretch = false,
                EnableNoiseFilter = false,
                EnablePageOrientation = true,
                EnableGrayscaleConversion = false,
                EnableRemoveLines = false,
                EnbleBackgroundRemoval = false
            };
        }
    }

    internal sealed class ImageProcessingConfigProvider
    {
        #region Constants

        private const string ConfigFileName = "ImageProcessingConfig.json";

        private static readonly string FullInstallationConfigFileRelativePath = $@"../../Configurations/{ConfigFileName}";

        private readonly string CurrentAssemblyPath;

        private string ConfigFileAbsolutePath_FullInstallation
        {
            get
            {
                return Path.Combine(CurrentAssemblyPath, FullInstallationConfigFileRelativePath);
            }
        }

        private string ConfigFileAbsolutePath_AssemblyLocation
        {
            get
            {
                return Path.Combine(CurrentAssemblyPath, ConfigFileName);
            }
        }

        #endregion Constants

        #region Statics

        private static ImageProcessingConfigProvider _imageProcessingConfigProvider = null;

        #endregion Statics

        #region Singleton ImageProcessingConfigProvider Instance

        public static ImageProcessingConfigProvider Instance
        {
            get
            {
                return _imageProcessingConfigProvider ??
                    (_imageProcessingConfigProvider = new ImageProcessingConfigProvider());
            }
        }

        #endregion Singleton ImageProcessingConfigProvider Instance

        #region Private Member

        private IDictionary<string, ImageProcessingConfig> _imageProcessingConfigs = null;

        #endregion Private Member

        #region Constructor

        private ImageProcessingConfigProvider()
        {
            CurrentAssemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        #endregion Constructor

        #region Public Methods

        public ImageProcessingConfig GetSettings(string engineType)
        {
            if (_imageProcessingConfigs == null)
            {
                initializeSettings(ConfigFileAbsolutePath_FullInstallation);

                if (_imageProcessingConfigs == null)
                {
                    initializeSettings(ConfigFileAbsolutePath_AssemblyLocation);
                }
            }

            engineType = engineType.ToLower();

            return _imageProcessingConfigs.ContainsKey(engineType)
                ? _imageProcessingConfigs[engineType]
                : null;
        }

        #endregion Public Methods

        #region Private Methods

        private void initializeSettings(string settingFilePath)
        {
            _imageProcessingConfigs = new Dictionary<string, ImageProcessingConfig>();

            if (!File.Exists(settingFilePath))
            {
                VBotLogger.Warning(() => $"config file '{settingFilePath}' not found");
                return;
            }

            VBotLogger.Trace(() => $"Image Processing Config File Path: {settingFilePath}");

            string configFileContents = null;
            try
            {
                configFileContents = File.ReadAllText(settingFilePath);
            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => $"unable to read from file '{settingFilePath}'{Environment.NewLine}exception:{ex.Message}");
                return;
            }

            if (string.IsNullOrWhiteSpace(configFileContents))
            {
                VBotLogger.Warning(() => $"[{nameof(ImageProcessingConfigProvider)}] Empty file '{settingFilePath}'");
                return;
            }

            try
            {
                _imageProcessingConfigs = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, ImageProcessingConfig>>(configFileContents);
                _imageProcessingConfigs = _imageProcessingConfigs.GetLowerCaseKeyDictionary();
            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => $"unable to deserialize contents of file '{settingFilePath}'{Environment.NewLine}exception:{ex.Message}{Environment.NewLine}contents:{configFileContents}");
            }
        }

        #endregion Private Methods
    }
}