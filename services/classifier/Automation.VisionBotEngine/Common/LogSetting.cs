﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.Generic.Config;
    using Automation.Generic.Config.XmlConfig;
    using Automation.Generic.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum LogTypes
    {
        Debug = 1,
        Error = 2,
        Fatal = 3,
        Information = 4,
        Warning = 5
    }

    internal interface ILogSettings
    {
        bool IsEnabled(string filePath, string module, LogTypes logType);
    }

    internal class LogSettings : ILogSettings
    {
        private const string LOG_SECTION = "log";

        public bool IsEnabled(string filePath, string module, LogTypes logType)
        {
            IConfiguration productConfiguration = ConfigFactory.GetXmlConfiguation();
            IConfigSource xmlConfigSource = new XmlConfigSource(filePath, module, LOG_SECTION);

            string isModuleEnabled = productConfiguration.GetValue(xmlConfigSource, logType.ToString().ToLower(), "false");

            return isModuleEnabled.Equals("true", StringComparison.OrdinalIgnoreCase) || isModuleEnabled.Equals("1");
        }
    }
}