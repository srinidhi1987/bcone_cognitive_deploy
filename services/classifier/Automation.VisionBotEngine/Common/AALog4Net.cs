﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.Generic.Logging;
    using System;
    using System.IO;

    internal class AALog4Net
    {
        private object _alog;

        public AALog4Net(string configFilePath, string applicationpath)
        {
            LogConfiguration lconfig = new LogConfiguration();
            lconfig.LogFilePath = Path.Combine(applicationpath, "VisionBotLog.txt");
            lconfig.LoggerName = Properties.Resources.ConstLoggerName;
            _alog = LoggerFactory.GetLogger(lconfig);
        }

        public void Write(LogTypes logType, /*Modules module,*/ Func<string> function)
        {
            //TODO: get function name from actual caller.
            string functioname = "";

            if (_alog == null)
                return;

            switch (logType)
            {
                case LogTypes.Debug:
                    ((IAALogger)_alog).LogDebug(functioname, function);
                    break;

                case LogTypes.Error:
                    ((IAALogger)_alog).LogError(functioname, function);
                    break;

                case LogTypes.Fatal:
                    //TODO: change the temperory fatal exception logging solution.
                    ((IAALogger)_alog).LogException(functioname, () => new Exception(function()));
                    break;

                case LogTypes.Information:
                    ((IAALogger)_alog).LogInfo(functioname, function);
                    break;

                case LogTypes.Warning:
                    ((IAALogger)_alog).LogWarning(functioname, function);
                    break;
            }
        }
    }
}