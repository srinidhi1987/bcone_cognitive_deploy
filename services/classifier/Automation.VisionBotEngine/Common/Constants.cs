﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Common
{
    static class Constants
    {
        public const string SearchString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/.,#$@()*<>:-=%&_?~`!^[]{}\\+';";
        public const string PdfExtension = ".pdf";
		public const string VISIONBOT_FOLDER = "VisionBot";
        public const string MyIqBotFolderPath = "Automation Anywhere\\My IQBots";
    }
}
