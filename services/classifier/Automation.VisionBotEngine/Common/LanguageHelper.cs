﻿using Automation.Services.Client;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.VisionBotEngine.Common
{
   internal static class LanguageHelper
    {

        public static string GetLanguage(string organizationId, string projectId)
        {
            try
            {
                ProjectServiceEndPointConsumer projectConsumer = new ProjectServiceEndPointConsumer();
                ProjectDetails projectDetail = projectConsumer.GetProjectMetaData(organizationId, projectId);

                if (string.IsNullOrEmpty(projectDetail?.PrimaryLanguage))
                    return "English";

                return projectDetail?.PrimaryLanguage;
            }
            catch (Exception ex)
            {
            }
            return "English";
        }

        public static string GetLanguageCode(string language)
        {
            try
            {
                if (string.IsNullOrEmpty(language))
                    return "eng";
                //TODO: Keep caching to get language detail instead of calling service each time
                AliasServiceEndPointConsumer aliasService = new AliasServiceEndPointConsumer();
                AliasLanguageDetail languageDetail = aliasService.GetLanguageDetail(language);
                if (string.IsNullOrEmpty(languageDetail?.Name))
                    return "eng";
                return languageDetail.Code;
            }
            catch (Exception ex)
            {
            }
            return "eng";
        }

        public static string GetLanguageCode(string organizationId, string projectId)
        {

            string language = GetLanguage(organizationId, projectId);

            return GetLanguageCode(language);

        }
    }
    internal interface ILanguageCode
    {
        string GetLanguageCode(string languageName);
    }
    internal class LanguageCodeFactory
    {
        private static ILanguageCode _languageCode = null;
        public static ILanguageCode GetLanguageCodeProvider()
        {
            if(_languageCode == null)
                _languageCode = new DictionaryLanguageCode();
            return _languageCode;
        }
    }
    internal class DictionaryLanguageCode : ILanguageCode
    {
        public string GetLanguageCode(string languageName)
        {
            string languageCode = "eng";
            if (!string.IsNullOrEmpty(languageName))
            {
                switch (languageName.ToLower())
                {
                    case "english":
                        languageCode = "eng"; break;
                    case "german":
                        languageCode = "deu"; break;
                    case "french":
                        languageCode = "fra"; break;
                    case "spanish":
                        languageCode = "spa"; break;
                    case "italian":
                        languageCode = "ita"; break;
                    case "afrikaans":
                        languageCode = "afr"; break;
                    case "arabic":
                        languageCode = "ara"; break;
                    case "bulgarian":
                        languageCode = "bul"; break;
                    case "catalan":
                    case "valencian":
                        languageCode = "cat"; break;
                    case "chinese - simplified":
                        languageCode = "chi_sim"; break;
                    case "chinese - traditional":
                        languageCode = "chi_tra"; break;
                    case "czech":
                        languageCode = "ces"; break;
                    case "danish":
                        languageCode = "dan"; break;
                    case "dutch":
                    case "flemish":
                        languageCode = "nld"; break;
                    case "greek":
                        languageCode = "ell"; break;
                    case "hungarian":
                        languageCode = "hun"; break;
                    case "indonesian":
                        languageCode = "ind"; break;
                    case "japanese":
                        languageCode = "jpn"; break;
                    case "korean":
                        languageCode = "kor"; break;
                    case "malay":
                        languageCode = "msa"; break;
                    case "norwegian":
                        languageCode = "nor"; break;
                    case "polish":
                        languageCode = "pol"; break;
                    case "portuguese":
                        languageCode = "por"; break;
                    case "romanian":
                    case "moldavian":
                    case "moldovan":
                        languageCode = "ron"; break;
                    case "russian":
                        languageCode = "rus"; break;
                    case "serbian(latin)":
                        languageCode = "srp_latn"; break;
                     case "swedish":
                        languageCode = "swe"; break;
                    case "turkish":
                        languageCode = "tur"; break;
                    case "slovak":
                        languageCode = "slk"; break;
                    default:
                        languageCode = "eng"; break;
                }
            }
            return languageCode;
        }
    }
 
}
