﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine.Common
{
    using Configuration;
    using Model;
    using System.IO;
    using System.Reflection;

    public static class CommonMethods
    {
        public static bool IsPdfFile(string filepath)
        {
            return Path.GetExtension(filepath).ToLower().Equals(Constants.PdfExtension);
        }

        internal static string GetVBotPath(string vBotName)
        {
            VisionBotManifest vBotMeniFest = new VisionBotManifest() { Name = vBotName, ParentPath = Path.Combine(VBotSettings.ApplicationPath, Constants.MyIqBotFolderPath) };
            return vBotMeniFest.GetFilePath();
        }
    }
}