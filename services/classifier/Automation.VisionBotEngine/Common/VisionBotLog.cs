﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using System;
    using System.IO;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class VBotLogger
    {
        private static ILogSettings logsetting = null;

        //private static ILogService logservice = null;
        private static string configFilePath = string.Empty;

        //private static AALog4Net _log = null;

        public static void Initlization(string applicationPath)
        {
            string logFileName = Path.Combine(applicationPath, "VisionBotLog.txt");
            configFilePath = Path.Combine(applicationPath, "AA.Settings.xml");
            logsetting = new LogSettings();
            try
            {
                //  _log = new AALog4Net(configFilePath,applicationPath);
            }
            catch { }
            LogSample.FilePath = Path.Combine(applicationPath, "VisionBotLog.txt");
        }

        public static void Trace(Func<string> function)
        {
            try
            {
                LogThis(LogTypes.Information, function);
            }
            catch (Exception ex)
            {
                LogSample.Write(LogTypes.Debug, ex.Message);
            }
        }

        public static void Error(Func<string> function)
        {
            LogThis(LogTypes.Error, function);
        }

        public static void Debug(Func<string> function)
        {
            LogThis(LogTypes.Debug, function);
        }

        public static void Warning(Func<string> function)
        {
            LogThis(LogTypes.Warning, function);
        }

        private static void LogThis(LogTypes logType, Func<string> function)
        {
            try
            {
                //if (_log != null)
                //{
                //    //_log.Write(logType, Modules.Main, function);
                //}
                //else
                //{
                VisionBotOldLog(logType, function);
                //}
            }
            catch (Exception)
            {
                // throw;
            }
        }

        public static bool IsLoggingEnabled(LogTypes logType)
        {
            return logsetting != null && logsetting.IsEnabled(configFilePath, "Main", logType);
        }

        private static void VisionBotOldLog(LogTypes logType, Func<string> function)
        {
            if (IsLoggingEnabled(logType))
                LogSample.Write(logType, function() ?? string.Empty);
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class ExceptionLoggingExtensions
    {
        public static void Log(this Exception ex, Func<string> function)
        {
            VBotLogger.Error(() => $"[EXCEPTION ENCOUNTERED] {function()}{Environment.NewLine}[ERROR MESSAGE]{Environment.NewLine}{ex.Message}{Environment.NewLine}[CALL STACK]{Environment.NewLine}{ex.StackTrace}");
        }

        public static void Log(this Exception ex, string className, string functionName, string blockInfo = "")
        {
            ex.Log(() => $"[{className} -> {functionName}] {blockInfo}");
        }
    }

    internal class LogInfo
    {
        public string ClassName { get; set; }
        public string FunctionName { get; set; }
        public string BlockInfo { get; set; } = string.Empty;

        public override string ToString()
        {
            return $"[{this.ClassName} -> {this.FunctionName}] {this.BlockInfo}";
        }
    }
}