﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using Automation.Generic.Logging;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Automation.Peers;

    internal static class LogSample
    {
        private static string LogText = string.Empty;

        private static object lockObject = new object();

        public static string FilePath = "d:\\VisionLog_______FOR_____DEBUG.txt";

        public static void Write(LogTypes type, string strLogText)
        {
            string text = string.Format("{0} \t{1} \tThread ID:{2} :\t{3}", type, DateTime.Now, System.Threading.Thread.CurrentThread.ManagedThreadId, strLogText);

            lock (lockObject)
            {
                if (string.IsNullOrEmpty(LogText))
                    LogText = text;
                else
                    LogText = text + Environment.NewLine;
                VerifyFileSizeAndMove();
                writeToFile();
            }
        }

        private static void VerifyFileSizeAndMove()
        {
            if (!File.Exists(FilePath))
                return;

            FileInfo f = new FileInfo(FilePath);
            long fileSize = f.Length / 1024; //Convert to KB
            if (fileSize / 1024 > 10)
            {
                File.Move(FilePath, getMoveFileName());
            }
        }

        private static string getMoveFileName()
        {
            string file = Path.GetFileNameWithoutExtension(FilePath);
            string tempFile = DateTime.Now.ToString(CultureInfo.InvariantCulture);

            tempFile = tempFile.Replace("/", "_");
            tempFile = file + tempFile.Replace(":", "_");

            string moveFile = file;
            do
            {
                if (file != null) moveFile = FilePath.Replace(file, tempFile);
                if (!File.Exists(moveFile))
                    break;
            } while (true);
            return moveFile;
        }

        private static void writeToFile()
        {
            try
            {
                StreamWriter sr = new StreamWriter(FilePath, true);
                sr.WriteLine(LogText);
                sr.Close();
                LogText = string.Empty;
            }
            catch
            {
                //This
                //throw;
            }
        }
    }
}