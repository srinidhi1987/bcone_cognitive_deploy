﻿using Automation.Services.Client;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.VisionBotEngine
{
    public static class DocumentVectorDetailsCacheManagerStrategy
    {
        public static IDocumentVectorDetailsCacheManager GetDocumentVectorDetailsCacheManager()
        {
            return new DocumentVectorDetailsCacheManager();
        }
    }

    public interface IDocumentVectorDetailsCacheManager
    {
        DocumentVectorDetails Get(string documentId);
        void Set(string documentId, DocumentVectorDetails documentVectorDatails);
    }

    internal class DocumentVectorDetailsCacheManager : IDocumentVectorDetailsCacheManager
    {
        private IFileServiceEndPointConsumer _fileService = null;
        public DocumentVectorDetailsCacheManager()
        {
            _fileService = new FileServiceEndPointConsumer("1", "");
        }

        public void Set(string documentId, DocumentVectorDetails documentVectorDatails)
        {
            if (string.IsNullOrWhiteSpace(documentId))
                throw new ArgumentException("documentId is null or empty");

            _fileService.SaveSegmentedDocument(documentId, documentVectorDatails);
        }

        public DocumentVectorDetails Get(string documentId)
        {
            if (string.IsNullOrWhiteSpace(documentId))
                throw new ArgumentException("documentId is null or empty");

            DocumentVectorDetails documentVectorDetails = null;

            FileServiceEndPointConsumer fileService = new FileServiceEndPointConsumer("1", "");
            documentVectorDetails = fileService.GetSegmentedDocument(documentId);


            return documentVectorDetails;
        }
    }
}
