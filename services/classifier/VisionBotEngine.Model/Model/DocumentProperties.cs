﻿using System.Collections.Generic;

/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class DocumentProperties
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public string Path { get; set; }
        public DocType Type { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int RenderDPI { get; set; }

        public string LanguageCode { get; set; }

        public Rectangle BorderRect { get; set; }

        public List<PageProperties> PageProperties { get; set; }

        public double Version { get; set; }

        public DocumentProperties()
        {
            PageProperties = new List<PageProperties>();
        }

        public DocumentProperties Clone()
        {
            DocumentProperties documentProperties = new DocumentProperties();
            documentProperties.BorderRect = this.BorderRect;
            documentProperties.Height = this.Height;
            documentProperties.PageProperties = new List<PageProperties>();
            if (this.PageProperties != null)
            {
                //TODO: Make this identical to other model list clone methods
                for (int i = 0; i < this.PageProperties.Count; i++)
                {
                    documentProperties.PageProperties.Add(this.PageProperties[i].Clone());
                }
            }
            documentProperties.Id = this.Id;
            documentProperties.Name = this.Name;
            documentProperties.Path = this.Path;
            documentProperties.RenderDPI = this.RenderDPI;
            documentProperties.Type = this.Type;
            documentProperties.Width = this.Width;
            documentProperties.Version = this.Version;
            documentProperties.LanguageCode = this.LanguageCode;

            return documentProperties;
        }
    }
}