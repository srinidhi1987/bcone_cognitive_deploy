﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    public class SegmentedDocumentCacheConfiguration
    {
        public bool IsReadFromCache { get; set; } = true;
    }
}