﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Integrator.CognitiveDataAutomation
{
    /// <summary>
    /// The cognitive data.
    /// </summary>
    public class CognitiveData
    {
        /// <summary>
        /// Gets or sets the name of visionbot to be used for data extraction.
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// Gets or sets the input file from which the data needs to be extracted.
        /// </summary>
        public string InputFile { get; set; }

        /// <summary>
        /// Gets or sets the output file path.
        /// </summary>
        public string OutputFile { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether the output data is to be appended if output file already exists.
        /// </summary>
        public bool IsAppend { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether output should be validator friendly or not.
        /// </summary>
        public bool EnableValidator { get; set; }

        /// <summary>
        /// Gets or sets name of task invoking the cognitive command.
        /// </summary>
        public string TaskName { get; set; }
    }
}