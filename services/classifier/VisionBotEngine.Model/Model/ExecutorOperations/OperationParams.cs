﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Automation.Cognitive.VisionBotEngine.Model.ExecutorOperations
{
    public static class ConsoleArgsCreationHelper
    {
        public static string CreateArgsString(IDictionary<string, string> args)
        {
            var result = new StringBuilder();

            if (args != null)
            {
                foreach (KeyValuePair<string, string> keyValue in args)
                {
                    result.AppendFormat(" {0}{1} \"{2}\"",
                        "--",
                        keyValue.Key,
                        SanitizeArgValue(keyValue.Value));
                }
            }

            return result.ToString();
        }

        public static string CreateArgsString(IDictionary<char, string> args)
        {
            var result = new StringBuilder();

            if (args != null)
            {
                foreach (KeyValuePair<char, string> keyValue in args)
                {
                    result.AppendFormat(" {0}{1} \"{2}\"",
                        "-",
                        keyValue.Key,
                        SanitizeArgValue(keyValue.Value));
                }
            }

            return result.ToString();
        }

        private static string SanitizeArgValue(string rawValue)
        {
             return rawValue?.Replace("\\","\\\\")
                .Replace("\"", "\\\"")
                .Replace(Environment.NewLine, "");
        }
    }

    public interface IConsoleCallableOperation
    {
        string ToConsoleArgs();
    }

    public class OperationParam : IConsoleCallableOperation
    {
        [Option('z', "operation", Required = true, HelpText = "Operation")]
        public string Name { get; set; }

        public string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("operation", Name);

            return ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class CreateVisionBotOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('v', "visionbotdetailid", Required = true, HelpText = "VisionBotDetailId")]
        public string VisionBotDetailId { get; set; }

        public static readonly string OperationName = "createvisionbot";

        public CreateVisionBotOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.VisionBotDetailId).ToLower(), this.VisionBotDetailId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class AddNewFieldsOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        public static readonly string OperationName = "addnewfields";

        public AddNewFieldsOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class GenerateAndExtractVbotDataOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('v', "visionbotid", Required = true, HelpText = "VisionBotId")]
        public string VisionBotId { get; set; }

        [Option('d', "documentid", Required = true, HelpText = "DocumentId")]
        public string DocumentId { get; set; }

        public static readonly string OperationName = "generateandextractvbotdata";

        public GenerateAndExtractVbotDataOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.VisionBotId).ToLower(), this.VisionBotId);
            parameters.Add(nameof(this.DocumentId).ToLower(), this.DocumentId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class ExtractVBotDataOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('v', "visionbotid", Required = true, HelpText = "VisionBotId")]
        public string VisionBotId { get; set; }

        [Option('d', "layoutid", Required = true, HelpText = "LayoutId")]
        public string LayoutId { get; set; }

        public static readonly string OperationName = "extractvbotdata";

        public ExtractVBotDataOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.VisionBotId).ToLower(), this.VisionBotId);
            parameters.Add(nameof(this.LayoutId).ToLower(), this.LayoutId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class GetValueFieldOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('v', "visionbotid", Required = true, HelpText = "VisionBotId")]
        public string VisionBotId { get; set; }

        [Option('l', "layoutid", Required = true, HelpText = "LayoutId")]
        public string LayoutId { get; set; }

        [Option('f', "fieldlayout", Required = true, HelpText = "FieldLayout")]
        public string FieldLayout { get; set; }

        [Option('e', "fielddefid", Required = true, HelpText = "FieldDefId")]
        public string FieldDefId { get; set; }

        [Option('d', "documentid", Required = true, HelpText = "DocumentId")]
        public string DocumentId { get; set; }

        public static readonly string OperationName = "getvaluefield";

        public GetValueFieldOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.VisionBotId).ToLower(), this.VisionBotId);
            parameters.Add(nameof(this.LayoutId).ToLower(), this.LayoutId);
            parameters.Add(nameof(this.FieldLayout).ToLower(), this.FieldLayout);
            parameters.Add(nameof(this.FieldDefId).ToLower(), this.FieldDefId);
            parameters.Add(nameof(this.DocumentId).ToLower(), this.DocumentId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class GetImageOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('d', "documentid", Required = true, HelpText = "DocumentId")]
        public string DocumentId { get; set; }

        [Option('i', "pageindex", Required = true, HelpText = "PageIndex")]
        public string PageIndex { get; set; }

        public static readonly string OperationName = "getimage";

        public GetImageOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.DocumentId).ToLower(), this.DocumentId);
            parameters.Add(nameof(this.PageIndex).ToLower(), this.PageIndex);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class ExportDataOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('v', "vbotdata", Required = false, HelpText = "VbotData")]
        public string VbotData { get; set; }

        [Option('d', "vbotdocument", Required = false, HelpText = "VbotDocument")]
        public string VbotDocument { get; set; }

        public static readonly string OperationName = "exportdata";

        public ExportDataOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.VbotData).ToLower(), this.VbotData);
            parameters.Add(nameof(this.VbotDocument).ToLower(), this.VbotDocument);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class ProcessDocumentOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('d', "documentid", Required = true, HelpText = "DocumentId")]
        public string DocumentId { get; set; }

        public static readonly string OperationName = "processdocument";

        public ProcessDocumentOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.DocumentId).ToLower(), this.DocumentId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class ProcessStagingDocumentOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('c', "categoryid", Required = true, HelpText = "CategoryId")]
        public string CategoryId { get; set; }

        [Option('v', "visionbotid", Required = true, HelpText = "VisionBotId")]
        public string VisionBotId { get; set; }

        [Option('d', "documentid", Required = true, HelpText = "DocumentId")]
        public string DocumentId { get; set; }

        [Option('r', "visionbotrundetailsid", Required = true, HelpText = "VisionBotRunDetailsId")]
        public string VisionBotRunDetailsId { get; set; }

        [Option('e', "documentprocesseddetailsid", Required = true, HelpText = "DocumentProcessedDetailsId")]
        public string DocumentProcessedDetailsId { get; set; }

        public static readonly string OperationName = "processstagingdocument";

        public ProcessStagingDocumentOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.CategoryId).ToLower(), this.CategoryId);
            parameters.Add(nameof(this.VisionBotId).ToLower(), this.VisionBotId);
            parameters.Add(nameof(this.DocumentId).ToLower(), this.DocumentId);
            parameters.Add(nameof(this.VisionBotRunDetailsId).ToLower(), this.VisionBotRunDetailsId);
            parameters.Add(nameof(this.DocumentProcessedDetailsId).ToLower(), this.DocumentProcessedDetailsId);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class GetVBotReportDataOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('v', "vbotdoument", Required = true, HelpText = "VBotDocument")]
        public string VBotDocument { get; set; }

        public static readonly string OperationName = "getvbotreportdata";

        public GetVBotReportDataOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.VBotDocument).ToLower(), this.VBotDocument);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }

    public class ClassifyDocumentOperationParams : OperationParam, IConsoleCallableOperation
    {
        [Option('o', "organizationid", Required = true, HelpText = "OrganizationId")]
        public string OrganizationId { get; set; }

        [Option('p', "projectid", Required = true, HelpText = "ProjectId")]
        public string ProjectId { get; set; }

        [Option('f', "fileid", Required = true, HelpText = "FileId")]
        public string FileId { get; set; }

        [Option('n', "filename", Required = true, HelpText = "FileName")]
        public string FileName { get; set; }

        public static readonly string OperationName = "classifydocument";

        public ClassifyDocumentOperationParams()
        {
            this.Name = OperationName;
        }

        public new string ToConsoleArgs()
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(nameof(this.OrganizationId).ToLower(), this.OrganizationId);
            parameters.Add(nameof(this.ProjectId).ToLower(), this.ProjectId);
            parameters.Add(nameof(this.FileId).ToLower(), this.FileId);
            parameters.Add(nameof(this.FileName).ToLower(), this.FileName);

            return base.ToConsoleArgs() + ConsoleArgsCreationHelper.CreateArgsString(parameters);
        }
    }
}