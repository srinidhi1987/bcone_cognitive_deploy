﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Reflection;

namespace Automation.VisionBotEngine.Model
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class VBotEngineSettings
    {
        private int _maxDegreeOfParallelism;

        public int MaxDegreeOfParallelism
        {
            get
            {
                return _maxDegreeOfParallelism;
            }

            set
            {
                _maxDegreeOfParallelism = value;
            }
        }

      
        //TODO: Field Definition (Checkbox) should hold if that particular field definition is supported within the engine.
        private bool _isCheckBoxFeatureEnabled;

        public bool IsCheckBoxFeatureEnabled
        {
            get { return _isCheckBoxFeatureEnabled; }
            set { _isCheckBoxFeatureEnabled = value; }
        }

        private bool _isAccuracyLevelEnabled = false;

        public bool IsAccuracyLevelEnabled
        {
            get
            {
                return _isAccuracyLevelEnabled;
            }

            set
            {
                _isAccuracyLevelEnabled = value;
            }
        }
        
        public override string ToString()
        {
            //TODO: Use nameof in following string to obtain name of properties in string.
            return string.Format("VBotEngineSettings: MaxDegreeOfParallelism: {0} PreserveGrayscaledPage: {1} DataPath: {2} Isprocessed: {3} IsCheckBoxRecognitionFeatureEnabled: {4}",
                MaxDegreeOfParallelism,
                IsCheckBoxFeatureEnabled);
        }

        public VBotEngineSettings Clone()
        {
            VBotEngineSettings engineSettings = new VBotEngineSettings();
            engineSettings.IsCheckBoxFeatureEnabled = IsCheckBoxFeatureEnabled;
            engineSettings.IsAccuracyLevelEnabled = IsAccuracyLevelEnabled;
            return engineSettings;
        }
    }
}