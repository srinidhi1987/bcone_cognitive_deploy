﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    public class DocumentVectorDetails
    {
        public SegmentedDocument SegmentedDocument { get; private set; }
        public DocumentProperties DocumentProperties { get; private set; }

        public DocumentVectorDetails(DocumentProperties documentProperties
            ,SegmentedDocument segmentedDocument)
        {
            SegmentedDocument = segmentedDocument;
            DocumentProperties = documentProperties;
        }
       
    }
}
