﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class VBotDocument
    {
        //TODO: Need link to vbot
        public Layout Layout { get; set; }

        public SegmentedDocument SegmentedDoc { get; set; }
        public VBotData Data { get; set; }

        public SIRFields SirFields { get; set; }

        public DocumentProperties DocumentProperties { get; set; }

        public VBotDocument()
        {
            SegmentedDoc = new SegmentedDocument();
            Layout = new Layout();
            Data = new VBotData();
            SirFields = new SIRFields();
            DocumentProperties = new DocumentProperties();
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    //TODO : changes for data
    public class VBotData
    {
        public DataRecord FieldDataRecord { get; set; }
        public List<TableValue> TableDataRecord { get; set; }

        public VBotData()
        {
            FieldDataRecord = new DataRecord();
            TableDataRecord = new List<TableValue>();
        }
    }

    public enum VBotDocumentValidationStatus
    {
        Fail,
        Pass
    }
}