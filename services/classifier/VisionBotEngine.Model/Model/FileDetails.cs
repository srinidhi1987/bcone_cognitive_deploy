﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    public class FileDetails
    {
        [JsonProperty(PropertyName = "fileId")]
        public string FileId { get; set; }

        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "format")]
        public string Format { get; set; }

        [JsonProperty(PropertyName = "isProduction")]
        public bool IsProduction { get; set; }

        [JsonProperty(PropertyName = "classificationId")]
        public string ClassificationId { get; set; }

        [JsonProperty(PropertyName = "layoutId")]
        public string LayoutId { get; set; }
    }
}