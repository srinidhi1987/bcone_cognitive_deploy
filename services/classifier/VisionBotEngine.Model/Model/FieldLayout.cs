﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class FieldLayout
    {
        public string Id { get; set; }

        public string FieldId { get; set; }

        public string Label { get; set; }

        public string StartsWith { get; set; }
        public string EndsWith { get; set; }
        public string FormatExpression { get; set; }
        public string DisplayValue { get; set; }

        public bool IsMultiline { get; set; }

        public Rectangle Bounds { get; set; }
        public Rectangle ValueBounds { get; set; }

        public FieldDirection FieldDirection { get; set; }

        public double MergeRatio { get; set; }

        public double SimilarityFactor { get; set; }

        public FieldType Type { get; set; }
        public int XDistant { get; set; }

        public bool IsValueBoundAuto { get; set; }

        public FieldType ValueType { get; set; }
        public string IsDollarCurrency { get; set; }

        public FieldLayout()        
        {
            Id = System.Guid.NewGuid().ToString();
            FieldDirection = VisionBotEngine.FieldDirection.Right;
            MergeRatio = 1.0f;
            SimilarityFactor = 0.7f;

            StartsWith = string.Empty;
            EndsWith = string.Empty;
            FormatExpression = string.Empty;

            DisplayValue = string.Empty;
            Type = FieldType.SystemField;
            IsValueBoundAuto = false;
            ValueType = FieldType.SystemField;
            IsDollarCurrency = string.Empty;
        }

        public FieldLayout Clone()
        {
            FieldLayout cloneFieldLayout = new FieldLayout();

            cloneFieldLayout.Id = Id;
            cloneFieldLayout.FieldId = FieldId;
            cloneFieldLayout.Label = Label;
		    cloneFieldLayout.IsValueBoundAuto = IsValueBoundAuto;
            cloneFieldLayout.StartsWith = StartsWith;
            cloneFieldLayout.EndsWith = EndsWith;
            cloneFieldLayout.FormatExpression = FormatExpression;
            cloneFieldLayout.IsMultiline = IsMultiline;

            cloneFieldLayout.Bounds = Bounds;
            cloneFieldLayout.ValueBounds = ValueBounds;
            cloneFieldLayout.FieldDirection = FieldDirection;
            cloneFieldLayout.MergeRatio = MergeRatio;
            cloneFieldLayout.XDistant = XDistant;
            cloneFieldLayout.SimilarityFactor = SimilarityFactor;
            cloneFieldLayout.DisplayValue = DisplayValue;
            cloneFieldLayout.ValueType = ValueType;
            cloneFieldLayout.IsDollarCurrency = IsDollarCurrency;
            return cloneFieldLayout;
        }
	}
}
