﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Drawing;

using Automation.VisionBotEngine;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class MarkerLayout
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Label { get; set; }

        public Rectangle Bounds { get; set; }

        public double MergeRatio { get; set; }

        public double SimilarityFactor { get; set; }

        public MarkerType MarkerType { get; set; }

        public FieldType Type { get; set; }

        public MarkerLayout()
        {
            Id = System.Guid.NewGuid().ToString();
            MergeRatio = 1.0f;
            SimilarityFactor = 0.7f;
            Type = FieldType.SystemField;
        }
    }
}