﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    public class ProjectDetails
    {
        public string id { get; set; }

        public string name { get; set; }

        [JsonProperty(PropertyName = "environment")]
        public string Environment { get; set; }

        public List<ClassifiedFolderManifest> categories { get; set; }

        public ProjectFields fields { get; set; }

        [JsonProperty(PropertyName = "primaryLanguage")]
        public string PrimaryLanguage { get; set; }

        [JsonProperty(PropertyName = "projectTypeId")]
        public string ProjectTypeId { get; set; }

        [DefaultValue(0)]
        [JsonProperty(PropertyName = "confidenceThreshold", DefaultValueHandling = DefaultValueHandling.Populate)]
        public int ConfidenceThreshold { get; set; }

        [JsonProperty(PropertyName = "ocrEngineDetails")]
        public OCREngineDetails [] OcrEngineDetails { get; set; }

        //public DateTime updatedAt { get; set; }

        //ToDo : For Table Field
        // public List<VisionBotManifest> visionBotManifests { get; set; }

        public ProjectDetails()
        {
            //visionBotManifests = new List<VisionBotManifest>();
            //  categories = new List<ClassifiedFolder>();
        }
    }

    public class ProjectFields
    {
        public List<string> standard { get; set; }
        public List<ProjectCustomField> custom { get; set; }

        public ProjectFields()
        {
            standard = new List<string>();
            custom = new List<ProjectCustomField>();
        }
    }

    public class ProjectCustomField
    {
        public string id { get; set; }
        public string name { get; set; }

        public ProjectFieldType type { get; set; }

        public FieldValueType FieldValueType { get; set; }

        public bool IsAddedLater { get; set; } = false;
    }

    public class ProjectStandardField
    {
        [JsonProperty(PropertyName = "id")]
        public string FieldId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string FieldName { get; set; }

        [JsonProperty(PropertyName = "fieldType")]
        public ProjectFieldType FieldType { get; set; }

        [JsonProperty(PropertyName = "dataType")]
        public FieldValueType FieldValueType { get; set; }

        public bool IsAddedLater { get; set; } = false;
    }

    public enum ProjectFieldType
    {
        FormField,
        TableField
    }

    public class ProjectManager
    {
        private List<ProjectManifest> ProjectsManifests { get; set; }
    }

    public class AliasLanguageDetail
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name;

        [JsonProperty(PropertyName = "code")]
        public string Code;
    }

    public class AliasDetailBasedOnProjectTypeAndLanguage
    {
        public List<AliasDetail> fields
        {
            get; set;
        }
    }

    public class AliasDetail
    {
        public string id { get; set; }
        public string name { get; set; }
        public string fieldType { get; set; }

        public List<string> aliases
        {
            get; set;
        }
    }

    public class OCREngineDetails
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "engineType")]
        public string EngineType { get; set; }

    }
}