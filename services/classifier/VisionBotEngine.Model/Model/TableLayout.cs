﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TableLayout
    {
        public string Id { get; set; }

        public string TableId { get; set; }

        public List<FieldLayout> Columns { get; set; }

        public Rectangle Bounds { get; set; }

        public FieldLayout PrimaryColumn;

        public FieldLayout Footer { get; set; }
        public bool IsValueMappingEnabled { get; set; }

        public TableLayout Clone()
        {
            TableLayout cloneTableLayout = new TableLayout();
           
            cloneTableLayout.Id = Id;
            cloneTableLayout.TableId = TableId;
            cloneTableLayout.Columns = CloneList(Columns);
            cloneTableLayout.Bounds = Bounds;
            if(PrimaryColumn != null)
                cloneTableLayout.PrimaryColumn = PrimaryColumn.Clone();
            if(Footer != null)
                cloneTableLayout.Footer = Footer.Clone();
            cloneTableLayout.IsValueMappingEnabled = IsValueMappingEnabled;

            return cloneTableLayout;
        }
        private List<FieldLayout> CloneList(List<FieldLayout> listToClone)
        {
            return listToClone.Select(item => item.Clone()).ToList();
        }
        public TableLayout()
        {
            Id = string.Empty;
            TableId = string.Empty;

            Columns = new List<FieldLayout>();
            Bounds = new Rectangle();

            PrimaryColumn = new FieldLayout();
            Footer = new FieldLayout();
            IsValueMappingEnabled = false;
        }
    }
    public class ValueBasedTableLayout
    {
        public string Id { get; set; }
        public List<ValueBasedTableRows> Rows;
        public ValueBasedTableLayout()
        {
            Rows = new List<ValueBasedTableRows>();
        }
    }
    public class ValueBasedTableRows
    {

        public List<ValueBasedTableCols> Columns;
        public ValueBasedTableRows()
        {
            Columns = new List<ValueBasedTableCols>();
        }
    }
    public class ValueBasedTableCols
    {
        public string id { get; set; }
        public string columnid { get; set; }
        public string defid { get; set; }
        public string text { get; set; }
        public Bound Bound { get; set; }
    }
    public class Bound
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }


    }

    public static class BoundExtension
    {
        public static Rectangle ToRectangle(this Bound bound)
        {
            return new Rectangle(bound.X, bound.Y, bound.Width, bound.Height);
        }
    }
}
