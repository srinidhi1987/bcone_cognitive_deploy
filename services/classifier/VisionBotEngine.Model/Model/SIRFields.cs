using System.Linq;

namespace Automation.VisionBotEngine.Model
{
    using System.Collections.Generic;

    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class SIRFields
    {
        public List<Line> Lines { get; set; }

        public List<Field> Fields { get; set; }

        public List<Field> Regions { get; set; }
        
        public SIRFields()
        {
            Lines = new List<Line>();
            Fields = new List<Field>();
            Regions = new List<Field>();
        }

        public SIRFields Clone()
        {
            SIRFields sirFieldsClone = new SIRFields();
            sirFieldsClone.Lines = Lines.ToList();
            sirFieldsClone.Fields = Fields.ToList();
            sirFieldsClone.Regions = Regions.ToList();
            return sirFieldsClone;
        }

    }
}