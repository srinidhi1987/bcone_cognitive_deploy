﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Linq;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class Layout
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DocumentProperties DocProperties { get; set; }
        public string VBotDocSetId { get; set; }
        public string VBotTestDocSetId { get; set; }

        public ImageProcessingConfig Settings;

        public SIRFields SirFields { get; set; }

        public List<FieldLayout> Fields { get; set; }
        public List<TableLayout> Tables { get; set; }

        public List<MarkerLayout> Identifiers { get; set; }

        public Layout()
        {
            SirFields = new SIRFields();
            Fields = new List<FieldLayout>();
            Identifiers = new List<MarkerLayout>();
            Tables = new List<TableLayout>();
            DocProperties = new DocumentProperties();
            Settings = new ImageProcessingConfig();
        }

        public Layout Clone()
        {
            var layoutClone = new Layout
            {
                Id = Id,
                Name = Name,
                DocProperties = DocProperties,
                VBotDocSetId = VBotDocSetId,
                VBotTestDocSetId = VBotTestDocSetId,
                Settings = Settings,
                SirFields = SirFields.Clone(),
                Fields = CloneList(Fields),
                Tables = Tables.ToList(),
                Identifiers = Identifiers.ToList()
            };


            return layoutClone;
        }
        public static List<FieldLayout> CloneList(List<FieldLayout> listToClone)
        {
            return listToClone.Select(item => item.Clone()).ToList();
        }


    }
}
