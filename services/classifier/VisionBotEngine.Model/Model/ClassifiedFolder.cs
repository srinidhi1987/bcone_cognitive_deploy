﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    public class ClassifiedFolderManifest
    {
        public string organizationId { get; set; }

        public string projectId { get; set; }

        public string id { get; set; }

        public string name { get; set; }

        //public string fileCount { get; set; }

        // public string projectId { get; set; }

        //  public VisionBotManifest Folder { get; set; }

        //  public string LastUpdated { get; set; }
    }
}