﻿/**
* Copyright (c) 2015 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]

    public class PageProperties
    {
        public int PageIndex { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public Rectangle BorderRect { get; set; }

        public PageProperties Clone()
        {
            PageProperties pageProperties = new PageProperties();
            pageProperties.PageIndex = this.PageIndex;
            pageProperties.Width = this.Width;
            pageProperties.Height = this.Height;
            pageProperties.BorderRect = this.BorderRect;
            return pageProperties;
        }
    }
}
