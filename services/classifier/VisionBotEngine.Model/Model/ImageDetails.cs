﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.OcrEngine
{
    public class ImageDetails
    {
        public string ImagePath { get; private set; }

        public string Langugae { get; private set; }

        public IDictionary<string,object> AdditionalParameters { get; set; }

        public ImageDetails(string imapePath, string language)
        {
            ImagePath = imapePath;
            Langugae = language;
            AdditionalParameters = new Dictionary<string, object>();
        }
    }
}