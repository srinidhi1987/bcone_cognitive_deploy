﻿/**
 * Copyright (c) 2018 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.VisionBotEngine.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;

namespace Automation.Cognitive.VisionBotEngine.Model.V2.Project
{
    public class ProjectDetailsV2
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "environment")]
        public string Environment { get; set; }

        [JsonProperty(PropertyName = "categories")]
        public List<ClassifiedFolderManifest> Categories { get; set; }

        [JsonProperty(PropertyName = "fields")]
        public ProjectFieldsV2 Fields { get; set; }

        [JsonProperty(PropertyName = "primaryLanguage")]
        public string PrimaryLanguage { get; set; }

        [JsonProperty(PropertyName = "projectTypeId")]
        public string ProjectTypeId { get; set; }

        [DefaultValue(0)]
        [JsonProperty(PropertyName = "confidenceThreshold", DefaultValueHandling = DefaultValueHandling.Populate)]
        public int ConfidenceThreshold { get; set; }

        [JsonProperty(PropertyName = "ocrEngineDetails")]
        public OCREngineDetails[] OcrEngineDetails { get; set; }

    }
}
