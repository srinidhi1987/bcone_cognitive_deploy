﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Drawing;

namespace Automation.VisionBotEngine.Model
{
    using System.Collections.Specialized;
    using System.Reflection;
    using System.Runtime.Serialization;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class FieldDef
    {
        public string Id { get; set; }

        //public string FieldId { get; set; }
        public string Name { get; set; }

        public FieldValueType ValueType { get; set; }
        public string Formula { get; set; }
        public string ValidationID { get; set; }
        public bool IsRequired { get; set; }
        public string DefaultValue { get; set; }

        public string StartsWith { get; set; }
        public string EndsWith { get; set; }

        public string FormatExpression { get; set; }

        public bool IsDeleted { get; set; }

        public FieldDef()
        {
            Id = string.Empty;
            //FieldId = string.Empty;
            Name = string.Empty;
            ValueType = FieldValueType.Text;
            Formula = string.Empty;
            ValidationID = string.Empty;
            DefaultValue = string.Empty;

            StartsWith = string.Empty;
            EndsWith = string.Empty;
            FormatExpression = string.Empty;
            IsDeleted = false;
        }

        public FieldDef Clone()
        {
            return new FieldDef
            {
                Id = this.Id,
                Name = this.Name,
                IsRequired = this.IsRequired,
                ValueType = this.ValueType,
                Formula = this.Formula,
                ValidationID = this.ValidationID,
                DefaultValue = this.DefaultValue,
                StartsWith = this.StartsWith,
                EndsWith = this.EndsWith,
                FormatExpression = this.FormatExpression,
                IsDeleted = this.IsDeleted
            };
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TableData : FieldData
    {
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class DataRecord
    {
        public List<FieldData> Fields { get; set; }

        public DataRecord()
        {
            Fields = new List<FieldData>();
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class FieldData
    {
        public string Id { get; }
        public FieldDef Field { get; set; }

        [DataMember]
        public FieldValue Value { get; set; }

        public ValidationIssue ValidationIssue { get; set; }

        public FieldData()
        {
            Id = Guid.NewGuid().ToString();
            Field = new FieldDef();
            Value = new TextValue();
        }

        public FieldData Clone()
        {
            return new FieldData()
            {
                Field = this.Field.Clone(),
                ValidationIssue = this.ValidationIssue.Clone(),
                Value = this.Value.Clone()
            };
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class ValidationIssue
    {
        public virtual FieldDataValidationIssueCodes IssueCode { get; set; }

        public ValidationIssue()
        {
            IssueCode = FieldDataValidationIssueCodes.FormulaValueMismatch;
        }

        public ValidationIssue(FieldDataValidationIssueCodes issueCode)
        {
            this.IssueCode = issueCode;
        }

        public ValidationIssue Clone()
        {
            return new ValidationIssue
            {
                IssueCode = this.IssueCode
            };
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [DataContract]
    [KnownType(typeof(TextValue))]
    [KnownType(typeof(TableValue))]
    public abstract class FieldValue
    {
        [DataMember]
        public Field Field { get; set; }

        public FieldValue()
        {
            Field = new Field();
        }

        public abstract FieldValue Clone();
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TableValue// : FieldValue
    {
        public TableDef TableDef { get; set; }
        public List<FieldDef> Headers { get; set; }
        public List<DataRecord> Rows { get; set; }

        public Field Field { get; set; }

        public TableValue()
        {
            Rows = new List<DataRecord>();
            Headers = new List<FieldDef>();
            TableDef = new TableDef();
            Field = new Field();
        }
    }

    internal class DateValue : FieldValue
    {
        public DateTime Value { get; set; }

        public override FieldValue Clone()
        {
            return new DateValue
            {
                Field = this.Field.Clone(),
                Value = this.Value
            };
        }
    }

    internal class CurrencyValue : FieldValue
    {
        public decimal Value { get; set; }

        public override FieldValue Clone()
        {
            return new CurrencyValue
            {
                Field = this.Field.Clone(),
                Value = this.Value
            };
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [DataContract]
    public class TextValue : FieldValue
    {
        [DataMember]
        public string Value { get; set; }

        public override FieldValue Clone()
        {
            return new TextValue
            {
                Field = this.Field.Clone(),
                Value = this.Value
            };
        }
    }

    internal class NumberValue : FieldValue
    {
        public long Value { get; set; }

        public override FieldValue Clone()
        {
            return new NumberValue
            {
                Field = this.Field.Clone(),
                Value = this.Value
            };
        }
    }
}