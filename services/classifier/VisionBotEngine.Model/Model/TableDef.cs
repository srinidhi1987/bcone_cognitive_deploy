﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Automation.VisionBotEngine.Model
{
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class TableDef
    {
        public string Id { get; set; }

        public string Name { get; set; }        

        public List<FieldDef> Columns { get; set; }

        public bool IsDeleted { get; set; }

        public TableDef Clone()
        {
            TableDef cloneTableDef = new TableDef();
            cloneTableDef.Id = Id;
            cloneTableDef.Name = Name;
            cloneTableDef.Columns = Columns.ToList();
            cloneTableDef.IsDeleted = IsDeleted;
            return cloneTableDef;
        }

        public TableDef()
        {
            Id = Guid.NewGuid().ToString();
            Columns = new List<FieldDef>();
            IsDeleted = false;
        }
    }
}
