﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBotEngine.Model.Model
{
    public class ExportConfiguration
    {
        public string ExportFilePath { get; set; }

        public string ProcessedDocumnetPath { get; set; }

        public string TaskName { get; set; }

        public bool IsValidatorEnabled { get; set; }

        public string VisionbotPath { get; set; }
    }
}
