﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.VisionBotEngine.Model.Generic
{
    public class IntegrationFactory
    {
        public static T CreateObject<T>(string assemblyName, string namespaceName, string className) where T : class
        {
            return CreateObject<T>(assemblyName, string.Format("{0}.{1}", namespaceName, className));
        }

        public static T CreateObject<T>(string assemblyName, string fullyQualifiedClassName) where T : class
        {
            return CreateObject<T>(string.Format("{0}, {1}", fullyQualifiedClassName, assemblyName));
        }

        public static T CreateObject<T>(string dllName, params object[] args) where T : class
        {
            return createObject(dllName, args) as T;
        }

        public static T CreateObject<T>(string dllName) where T : class
        {
            return createObject(dllName, null) as T;
        }

        private static object createObject(string assemblyName, params object[] args)
        {
            try
            {
                throwIfAssemblyFullNameIsNullOrEmpty(assemblyName);

                throwIfAssemblyFullNameDoesNotContainsComma(assemblyName);

                string[] temp = assemblyName.Split(',');

                System.Reflection.Assembly integrationAssembly = System.Reflection.Assembly.LoadFrom(getFilePath(temp[1]));


                Type vcType = Type.GetType(assemblyName);

                object obj = null;

                if (args == null || args.Length == 0)
                    obj = Activator.CreateInstance(vcType);
                else
                    obj = Activator.CreateInstance(vcType, args);

                if (obj == null)
                    throw new Exception("Assembly is not loaded.");

                return obj;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static void throwIfAssemblyFullNameIsNullOrEmpty(string assemblyFullName)
        {
            if (string.IsNullOrWhiteSpace(assemblyFullName))
                throw new ArgumentNullException(nameof(assemblyFullName));
        }

        private static void throwIfAssemblyFullNameDoesNotContainsComma(string assemblyFullName)
        {
            if (assemblyFullName.IndexOf(',') < 0)
                throw new ArgumentException($"{nameof(assemblyFullName)} does not contains {','}");
        }

        private static string getFilePath(string filename)
        {
            string assemblyLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            const string DLL_FILE_EXTENSION = ".dll";

            return System.IO.Path.Combine(assemblyLocation, filename.Trim() + DLL_FILE_EXTENSION);
        }
    }
}
