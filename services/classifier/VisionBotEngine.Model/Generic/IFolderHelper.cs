﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.IO;
namespace Automation.Generic
{
    public interface IFolderHelper
    {
        void Create(string folderPath);

        bool DoesExists(string folderPath);

        void DeepCopy(string sourceFolderPath, string destinationFolderPath);

        void Delete(string folderPath);

        void Merge(string sourceFolderPath, string destinationFolderPath);

        void Move(string sourceFolderPath, string destinationFolderPath);

        string[] GetFiles(string directoryPath, string searchPattern, SearchOption searchOption);
    }
}
