﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.VisionBotEngine.Model.Generic
{
   public static class CultureHelper
    {
        
        public static CultureInfo GetCultureInfo(string language)
        {
            Dictionary<string, string> mapBetweenLanguageCodeAndCultureCode = getMappingBetweenLanguageCodeAndCultureCode();
           if(language!=null&& mapBetweenLanguageCodeAndCultureCode.ContainsKey(language))
            {
                return new CultureInfo(mapBetweenLanguageCodeAndCultureCode[language]);
            }

            return new CultureInfo("en");
        }

        private static Dictionary<string,string> getMappingBetweenLanguageCodeAndCultureCode()
        {
            Dictionary<string, string> mapBetweenLanguageAndCultureCode = new Dictionary<string, string>()
            {
                { "1","en" },
                { "2","de" },
                { "3","fr" },
                { "4","es" },
                { "5","it" }
            };

            return mapBetweenLanguageAndCultureCode;
        }
    }
}
