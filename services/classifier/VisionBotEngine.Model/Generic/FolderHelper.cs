﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.Generic
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;

    public class FolderHelper : IFolderHelper
    {
        [ExcludeFromCodeCoverage]
        public bool DoesExists(string folderPath)
        {
            return Directory.Exists(folderPath);
        }

        public void DeepCopy(string sourceFolderPath, string destinationFolderPath)
        {
            foreach (string dirPath in Directory.GetDirectories(sourceFolderPath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourceFolderPath, destinationFolderPath));
            }

            foreach (string newPath in Directory.GetFiles(sourceFolderPath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(sourceFolderPath, destinationFolderPath), true);
            }
        }

        [ExcludeFromCodeCoverage]
        public void Delete(string folderPath)
        {
            Directory.Delete(folderPath, true);
        }

        public void Merge(string sourceFolderPath, string destinationFolderPath)
        {
            if (sourceFolderPath.Equals(destinationFolderPath, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(sourceFolderPath))
            {
                throw new ArgumentException(
                    "sourceFolderPath cannot be blank",
                    "sourceFolderPath");
            }
            if (!Directory.Exists(sourceFolderPath))
            {
                throw new ArgumentException(
                    string.Format("sourceFolderPath \"{0}\" does not exist", sourceFolderPath),
                    "sourceFolderPath");
            }

            if (string.IsNullOrWhiteSpace(destinationFolderPath))
            {
                throw new ArgumentException(
                    "destinationFolderPath cannot be blank",
                    "destinationFolderPath");
            }
            if (!Directory.Exists(destinationFolderPath))
            {
                throw new ArgumentException(
                    string.Format("destinationFolderPath \"{0}\" does not exist", destinationFolderPath),
                    "destinationFolderPath");
            }

            foreach (var dirPath in Directory.GetDirectories(sourceFolderPath, "*", SearchOption.AllDirectories))
            {
                if (this.DoesExists(dirPath))
                {
                    continue;
                }
                Directory.CreateDirectory(dirPath.Replace(sourceFolderPath, destinationFolderPath));
            }

           // var pathProvider = new Automation.Generic.Security.IO.AAPathProvider();

            foreach (var newPath in Directory.GetFiles(sourceFolderPath, "*.*", SearchOption.AllDirectories))
            {
                //TODO :Add Depedancy of generic to this project
                //File.Copy(
                //    pathProvider.GetFilePathProvider(newPath).Get(),
                //    pathProvider.GetFilePathProvider(newPath.Replace(sourceFolderPath, destinationFolderPath)).Get(),
                //    true);
            }
        }

        [ExcludeFromCodeCoverage]
        public void Create(string folderPath)
        {
            Directory.CreateDirectory(folderPath);
        }

        [ExcludeFromCodeCoverage]
        public void Move(string sourceFolderPath, string destinationFolderPath)
        {
            Directory.Move(sourceFolderPath, destinationFolderPath);
        }

        [ExcludeFromCodeCoverage]
        public string[] GetFiles(string directoryPath, string searchPattern, SearchOption searchOption)
        {
            return Directory.GetFiles(directoryPath, searchPattern, searchOption);
        }
    }
}