﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Model
{
    public class FieldAccuracyModel
    {
        [JsonProperty(PropertyName = "fieldId")]
        public String FieldId;

        [JsonProperty(PropertyName = "oldValue")]
        public String OldValue;

        [JsonProperty(PropertyName = "newValue")]
        public String NewValue;

        [JsonProperty(PropertyName = "passAccuracyValue")]
        public float PassAccuracyValue;
        
    }
}
