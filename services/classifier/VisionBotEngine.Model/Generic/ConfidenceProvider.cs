﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Automation.Cognitive.VisionBotEngine.Model.Generic
{
    public class ConfidenceProvider
    {
        #region public

        /// <summary>
        /// Field level confidence from character level confidence = Character_Confidence ^ number_of_characters_in_field
        /// </summary>
        /// <param name="characterLevelConfidence"></param>
        /// <param name="characterCount"></param>
        /// <returns>Field level confidence</returns>
        public double GetFieldLevelConfidence(double characterLevelConfidence, int characterCount)
        {
            return Math.Pow(characterLevelConfidence, characterCount) * 100;
        }

        /// <summary>
        /// Character level confidence from SIR confidence = Field_Confidence ^ (1 / number_of_characters_in_field)
        /// </summary>
        /// <param name="fieldConfidence"></param>
        /// <param name="numberOfCharacters"></param>
        /// <returns>Character level confidence</returns>
        public double GetCharacterLevelConfidence(double fieldConfidence, int numberOfCharacters)
        {
            return Math.Pow(fieldConfidence, (double)1 / numberOfCharacters);

        }


        /// <summary>
        /// AverageCharacterLevelConfidence = ((First_Field_CharConfidence * NoOfCharcters) + (Second_Field_CharConfidence * NoOfCharacters2)) / totalcharacters
        /// AverageSirLevelConfidence = AverageCharacterLevelConfidence ^ (totalcharacters + Space_Count_Between_Fields);
        /// </summary>
        /// <param name="fieldConfidenceAndCharactersCount"></param>
        /// <returns>Average SIR level confidence by applying upper formula on multiple fields confidence</returns>
        public double GetMergerdFieldConfidence(IList<Tuple<double, int>> fieldConfidenceAndCharactersCount)
        { 
            int totalCharacters = fieldConfidenceAndCharactersCount.Sum(k => k.Item2);
            int totalCharactersWithSpace = totalCharacters + (fieldConfidenceAndCharactersCount.Count - 1);
            double confidenceValue = 0.0;
            foreach (Tuple<double, int> record in fieldConfidenceAndCharactersCount)
            {
                double characterConfidence = GetCharacterLevelConfidence(record.Item1, record.Item2);
                confidenceValue = confidenceValue + (characterConfidence * record.Item2);
            }

            double averageCharacterLevelConfidence = confidenceValue / totalCharacters ;

            return GetFieldLevelConfidence(averageCharacterLevelConfidence, totalCharactersWithSpace);
        }
        
        #endregion

        


    }

}
