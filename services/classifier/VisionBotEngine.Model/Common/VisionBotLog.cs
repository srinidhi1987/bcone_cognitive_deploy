﻿/**
 * Copyright (c) 2017 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security.Permissions;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class VBotLogger
    {
        private static string serviceConfigurationPath = "CognitiveServiceConfiguration.json";
        private static LoggerSettings loggerSetting = null;
        private static string cognitiveServiceJsonFilePath = string.Empty;

        private static int defaultLogLevelValue = (int)LogTypes.ERROR;
        private static string defaultLogLevel = "ERROR";

        private static bool isDebugLogEnbled = false;

        private static bool isTraceLogEnbled = false;
        private static bool isErrorLogEnbled = false;
        private static bool isWarningLogEnbled = false;

        private static int _logLevel = (int)LogTypes.ALL;

        public static void Initlization(string applicationPath, LoggerSettings loggerSettings = null)
        {
            string logFileName = Path.Combine(applicationPath);
            int logLevelValue = 0;
            InitializeFileWatcher();
            if (loggerSettings != null)
            {
                logLevelValue = ConvertLoggerSettingToValue(loggerSettings);
            }

            loggerSetting = loggerSettings;
            UpdateLogLevels(loggerSetting, logLevelValue);

            LogSample.FilePath = applicationPath;
        }

        public static string GetLogFilePath()
        {
            return LogSample.FilePath;
        }

        public static int GetLogLevel()
        {
            return _logLevel;
        }

        public static void SetLogLevel(int logLevel)
        {
            _logLevel = logLevel;
        }

        public static bool IsLoggingEnabled(LogTypes logType)
        {
            return loggerSetting != null && ((logType == LogTypes.Information && isTraceLogEnbled)
                || (logType == LogTypes.DEBUG && isDebugLogEnbled)
                || (logType == LogTypes.ERROR && isErrorLogEnbled)
                || (logType == LogTypes.WARN && isWarningLogEnbled));
        }

        public static Action<Func<string>> GetLogMethod(LogTypes logType)
        {
            switch (logType)
            {
                case LogTypes.TRACE:
                    return Trace;

                case LogTypes.DEBUG:
                    return Debug;

                case LogTypes.Information:
                    return Info;

                case LogTypes.WARN:
                    return Warning;

                case LogTypes.ERROR:
                    return Error;

                case LogTypes.ALL:
                case LogTypes.OFF:
                default:
                    throw new ArgumentException($"cannot provide log method for logType {logType}");
            }
        }

        #region Logging Functions

        public static void Trace(Func<string> function)
        {
            try
            {
                if (isTraceLogEnbled)
                    LogThis(LogTypes.TRACE, function);
            }
            catch (Exception ex)
            {
                LogSample.Write(LogTypes.DEBUG, ex.Message);
            }
        }

        public static void Error(Func<string> function)
        {
            if (isErrorLogEnbled)
                LogThis(LogTypes.ERROR, function);
        }

        public static void Debug(Func<string> function)
        {
            if (isDebugLogEnbled)
                LogThis(LogTypes.DEBUG, function);
        }

        public static void Info(Func<string> function)
        {
            if (isDebugLogEnbled)
                LogThis(LogTypes.Information, function);
        }

        public static void Warning(Func<string> function)
        {
            if (isWarningLogEnbled)
                LogThis(LogTypes.WARN, function);
        }

        #endregion Logging Functions

        #region Private Functionality

        private static void LogThis(LogTypes logType, Func<string> function)
        {
            try
            {               
                VisionBotOldLog(logType, function);
            }
            catch (Exception)
            {
            }
        }

        private static void VisionBotOldLog(LogTypes logType, Func<string> function)
        {
            LogSample.Write(logType, function() ?? string.Empty);
        }

        private static int ConvertLoggerSettingToValue(LoggerSettings loggerSettings)
        {
            int logLevelValue = 0;
            switch (loggerSettings.level.ToString().ToUpper())
            {
                case "ALL":
                    logLevelValue = (int)LogTypes.ALL;
                    break;

                case "TRACE":
                    logLevelValue = (int)LogTypes.TRACE;
                    break;

                case "DEBUG":
                    logLevelValue = (int)LogTypes.DEBUG;
                    break;

                case "INFO":
                    logLevelValue = (int)LogTypes.Information;
                    break;

                case "WARN":
                    logLevelValue = (int)LogTypes.WARN;
                    break;

                case "OFF":
                    logLevelValue = (int)LogTypes.OFF;
                    break;

                case "ERROR":
                    logLevelValue = (int)LogTypes.ERROR;
                    break;

                default:
                    logLevelValue = defaultLogLevelValue;
                    break;
            }
            return logLevelValue;
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static void InitializeFileWatcher()
        {
            string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            VBotLogger.Trace(() => "File Path: " + filePath);
            cognitiveServiceJsonFilePath = filePath;

            // Create a new FileSystemWatcher and set its properties.
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = filePath;
            /* Watch for changes in LastAccess and LastWrite times, and
               the renaming of files or directories. */
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            // Only watch text files.
            watcher.Filter = "CognitiveServiceConfiguration.json";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }

        // Define the event handlers.
        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                string filePath = Path.Combine(cognitiveServiceJsonFilePath, serviceConfigurationPath);
                if (!File.Exists(filePath))
                {
                    UpdateLogLevelsToDefault();
                    return;
                }
                string json = File.ReadAllText(filePath);
                var jsonSerializationSettings = new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All };
                CognitiveServicesLoggerConfiguration _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesLoggerConfiguration>(json, jsonSerializationSettings);
                loggerSetting = _cognitiveServicesConfiguration.LoggerSettings;
                int logLevelValue = 0;
                if (loggerSetting != null)
                {
                    logLevelValue = ConvertLoggerSettingToValue(loggerSetting);
                }
                UpdateLogLevels(loggerSetting, logLevelValue);
            }
            catch (Exception ex)
            {
                UpdateLogLevelsToDefault();
                VBotLogger.Error(() => "Exception caught in OnChanged() event of FileWatcher: " + ex);
            }
        }

        private static void UpdateLogLevelsToDefault()
        {
            LoggerSettings logSetting = new LoggerSettings();
            logSetting.level = defaultLogLevel;
            int currentLogLevelValue = ConvertLoggerSettingToValue(logSetting);
            UpdateLogLevels(logSetting, currentLogLevelValue);
        }

        private static void UpdateLogLevels(LoggerSettings loggerSetting, int logLevelValue)
        {
            isTraceLogEnbled = loggerSetting != null && ((int)LogTypes.TRACE >= logLevelValue);
            isDebugLogEnbled = loggerSetting != null && ((int)LogTypes.DEBUG >= logLevelValue);
            isErrorLogEnbled = loggerSetting != null && ((int)LogTypes.ERROR >= logLevelValue);
            isWarningLogEnbled = loggerSetting != null && ((int)LogTypes.WARN >= logLevelValue);
            SetLogLevel(logLevelValue);
        }

        #endregion Private Functionality
    }

    public class CognitiveServicesLoggerConfiguration
    {
        public LoggerSettings LoggerSettings = null;

        public CognitiveServicesLoggerConfiguration()
        {
            LoggerSettings = new LoggerSettings();
        }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public static class ExceptionLoggingExtensions
    {
        public static void Log(this Exception ex, string className, string functionName, string message = "", LogTypes logAs = LogTypes.ERROR)
        {
            ex.Log(() => $"[{className} -> {functionName}] {message}",
                logAs);
        }

        public static void Log(this Exception ex, Func<string> message, LogTypes logAs = LogTypes.ERROR)
        {
            coreLog(ex,
                () => $"{message()}",
                logAs);
        }

        private static void coreLog(Exception ex, Func<string> function, LogTypes logAs)
        {
            VBotLogger.GetLogMethod(logAs)(() => $"[EXCEPTION ENCOUNTERED] {function()}{Environment.NewLine}[ERROR MESSAGE]{Environment.NewLine}{ex.ToString()}");
        }
    }

    public class LoggerSettings
    {
        public string level = String.Empty;
    }

    internal class LogInfo
    {
        public string ClassName { get; set; }
        public string FunctionName { get; set; }
        public string BlockInfo { get; set; } = string.Empty;

        public override string ToString()
        {
            return $"[{this.ClassName} -> {this.FunctionName}] {this.BlockInfo}";
        }
    }
}