/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.VisionBotEngine
{
    public enum FieldDataValidationIssueCodes
    {
        MissingRequiredFieldValue = 0,

        InvalidTextTypeValue = 1,
        InvalidDateTypeValue = 2,
        InvalidNumberTypeValue = 3,
        //InvalidDecimalTypeValue = 4,
        //InvalidCurrencyTypeValue = 5,

        InvalidStartsWithFieldValue = 6,
        InvalidEndsWithFieldValue = 7,

        InvalidFormula = 8,
        FormulaValueMismatch = 9,
        ValueNotInList = 10,

        PatternMismatch = 11,
        ValidationEngineIssue=12,

        ConfidenceIssue=100

        // Start Warning type codes from FieldDataValidationIssueCodesHelper.WarningIssueCodeStartingValue
    }
}