﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBotEngine.Model.Enum
{
    public enum ErrorExportTypes
    {
        None = 0,
        SeparateErrorCsv = 1,
        CombinedDataAndErrorCsv = 2,
        EyeballForError = 3
    }
}
