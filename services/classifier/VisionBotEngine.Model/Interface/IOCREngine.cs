﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.OcrEngine
{
    public interface IOcrEngine
    {
        SegmentedDocument ExtractRegions(ImageDetails imageDetails, string whiteListingPattern);
    }
}
