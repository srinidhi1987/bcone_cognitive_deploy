﻿using System;
using System.IO;

/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

namespace Automation.VisionBotEngine.Configuration
{
    using Automation.VisionBotEngine.Configuration;

    //using Automation.Common;
    using System;
    using System.IO;
    using System.Linq;

    public class IQBotDataPathProvider : IDataPathProvider
    {
        private static string __iqBotDefaultDataPath;
        private static string _iqBotSpecifiedDataPath;

        private static string IQBotDataPath
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_iqBotSpecifiedDataPath)) return _iqBotSpecifiedDataPath;

                if (string.IsNullOrEmpty(__iqBotDefaultDataPath))
                {
                    __iqBotDefaultDataPath = Path.Combine(
                        RoutineProvider.Routine.GetApplicationPath(),
                        RoutineProvider.Routine.GetProductName(),
                        "My IQBots");
                }

                return __iqBotDefaultDataPath;
            }
        }

        public static void SetSpecificDataPath(string specificDataPath)
        {
            _iqBotSpecifiedDataPath = specificDataPath;
        }

        public static void ResetSpecificDataPath()
        {
            SetSpecificDataPath(null);
        }

        public string GetPath()
        {
            return IQBotDataPath;
        }

        public static string GetTemplatePath(string templateName)
        {
            if (IsTemplatePath(templateName))
            {
                return templateName;
            }

            return templateName.Contains(Path.DirectorySeparatorChar)
                ? templateName
                : Path.Combine(IQBotDataPath, templateName + ".vbot");
        }

        private static bool IsTemplatePath(string value)
        {
            return value.Contains(Path.DirectorySeparatorChar)
                   && value.EndsWith(".vbot");
        }
    }

    public static class RoutineProvider
    {
        public static IRoutines Routine { get; set; } = new AARoutines();
    }

    public interface IRoutines
    {
        string GetApplicationPath();

        string GetProductName();
    }

    public class AARoutines : IRoutines
    {
        public string GetApplicationPath()
        {
            var path = new Uri(
    System.IO.Path.GetDirectoryName(
        System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
    ).LocalPath;
            // return Routines.GetApplicationPath();
            // string tempApplicationPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Automation Anywhere Files");
            string tempApplicationPath = path; //@"D:\pooja.wani\My Documents\Automation Anywhere Files";
            return tempApplicationPath;
        }

        public string GetProductName()
        {
            //return Routines.GetProductName();
            string tempProductName = "Automation Anywhere";
            return tempProductName;
        }
    }
}