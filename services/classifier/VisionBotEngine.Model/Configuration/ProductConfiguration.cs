﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.VisionBotEngine.Configuration
{
    public sealed class ProductConfiguration
    {
        private static ProductConfiguration _instance;

        public static ProductConfiguration Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProductConfiguration();
                }
                return _instance;
            }
        }

        private ProductConfiguration()
        {
        }

        public static string Get(string root, string section, string key, string defaultValue)
        {
            //ToDo : Need to return proper value by reading setting.
            //Currently returning the default value
            return defaultValue;
        }

        public static void set(string root, string section, string key, string defaultValue)
        {
            //TODO : Decide set or not???
        }
    }
}