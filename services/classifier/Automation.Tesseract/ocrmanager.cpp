#include <baseapi.h>
#include <allheaders.h>
#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include "ocrmanager.h"
#include "PageOrientation.h"
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "documentprocess.h"
#include <exception>
#define _countof(X) (sizeof(X) / sizeof(X[0]))

using namespace std;
using namespace tesseract;

const wchar_t* globalHOCRText;
namespace OCRManager
{
	char* Scan(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath, int pageSegmentationMode, int ocrEngineMode, char* applicationPath, char* hOCROutputFile)
	{
		try
		{
			DocumentProcess *documentProcess = new DocumentProcess(pageSegmentationMode, ocrEngineMode, applicationPath);
			char* HOCRText = documentProcess->DocumentProcessing(imagePath, searchString, languageCode, trainingDataPath, hOCROutputFile);
			//int i = documentProcess->DocumentProcessing(imagePath, searchString, languageCode, trainingDataPath, hOCROutputFile);
			documentProcess->~DocumentProcess();
			delete documentProcess;
			return HOCRText;
		}
		catch (...)
		{
		}
		return NULL;
	}
	int GetPageOrientation(char* imagePath, char* trainingDataPath, char* applicationPath)
	{
		try
		{
			PageOrientation *pageOrientation = new PageOrientation();
			pageOrientation->CalculateOrientationAngle(imagePath, applicationPath, trainingDataPath);
			int angle = pageOrientation->GetPageOrientationAngle();
			pageOrientation->~PageOrientation();
			delete pageOrientation;
			return angle;
		}
		catch (...)
		{
		}
		return 0;
	}
};