#include "documentprocess.h"
#include <baseapi.h>
#include <allheaders.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <ctime>
#include "ocrlogs.h"

#ifdef HAVE_CONFIG_H
#include "config_auto.h"
#endif

#include <iostream>
#include "baseapi.h"
#include "basedir.h"
#include "renderer.h"
#include "strngs.h"
#include "tprintf.h"
#include <locale>
#include <codecvt>

using namespace std;
using namespace tesseract;

DocumentProcess::DocumentProcess(int pageSegmentationMode, int ocrEngineMode, char* applicationPath)
{
	OCRLogs::ApplicationPath = applicationPath;
	SegMode = (PageSegMode)pageSegmentationMode;
	EngineMode = (OcrEngineMode)ocrEngineMode;
}

DocumentProcess::~DocumentProcess()
{
	api.~TessBaseAPI();
}

char* DocumentProcess::DocumentProcessing(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath, char* hOCROutputFile)
{
	PIX *image = NULL;
	try
	{
		const char* inputfile = imagePath;
		image = pixRead(inputfile);

		OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing parameters with training");
		if (api.Init(trainingDataPath, languageCode, EngineMode) != NULL)
		{
			if (image)
				pixDestroy(&image);
			api.~TessBaseAPI();
			OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing failed");
			if (trainingDataPath)
				OCRLogs::WriteLogToFile(trainingDataPath);

			if (languageCode)
				OCRLogs::WriteLogToFile(languageCode);
			return NULL;
		}
		OCRLogs::WriteLogToFile("[Scan/OCR] End initializing parameters with training ");

		if (searchString != NULL && searchString != "\0" && searchString != "")
			api.SetVariable("tessedit_char_whitelist", searchString);

		OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing segmentation mode ");
		api.SetPageSegMode(SegMode);
		OCRLogs::WriteLogToFile("[Scan/OCR] End initializing segmentation mode ");
		OCRLogs::WriteLogToFile("[Scan/OCR] Start setting image to ocr ");
		api.SetImage(image);
		OCRLogs::WriteLogToFile("[Scan/OCR] End setting image to ocr ");

		OCRLogs::WriteLogToFile("[Scan/OCR] Start extraction of data ");
		api.Recognize(0);
		OCRLogs::WriteLogToFile("[Scan/OCR] End extractiong of data ");

		OCRLogs::WriteLogToFile("[Scan/OCR] start destroying image ");
		pixDestroy(&image);
		OCRLogs::WriteLogToFile("[Scan/OCR] End destroying image ");

		OCRLogs::WriteLogToFile("[Scan/OCR] returning data ");

		std::ofstream out(hOCROutputFile);
		out << api.GetHOCRText(0);

		out.flush();
		out.close();

		return api.GetHOCRText(0);
	}
	catch (exception exc)
	{
		if (image)
			pixDestroy(&image);
		cout << exc.what() << endl;
		OCRLogs::WriteLogToFile("[Scan/OCR] Exception in OCREngine ");
		OCRLogs::WriteLogToFile(exc.what());
	}

	return NULL;
}