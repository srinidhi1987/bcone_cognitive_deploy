#include "ScannedRegion.h"

namespace OCRManager
{
	extern "C" { __declspec(dllexport) char* Scan(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath, int pageSegmentationMode, int ocrEngineMode, char* applicationPath, char* hOCROutputFile); }
	extern "C" { __declspec(dllexport) int GetPageOrientation(char* imagePath, char* trainingdataPath, char* applicationPath); }
}