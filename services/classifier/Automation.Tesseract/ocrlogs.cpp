#include "ocrlogs.h"

string OCRLogs::ApplicationPath = "";

void OCRLogs::WriteLogToFile(string currentLog)
{
	try {
		if (!ApplicationPath.empty())
		{
			string LogFileName = ApplicationPath;// "\\OCRExtractionLog.txt";
			ofstream myfile;
			time_t rawtime;
			struct tm * timeinfo;

			time(&rawtime);
			timeinfo = localtime(&rawtime);
			string currtime = asctime(timeinfo);

			myfile.open(ApplicationPath + LogFileName, std::ios_base::app);
			myfile << "  Time :" << currtime << currentLog << endl;
			myfile.close();
		}
	}
	catch (...)
	{
	}
}