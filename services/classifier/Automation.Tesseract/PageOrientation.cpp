#include "pageorientation.h"
#include <osdetect.h>
#include <ctime>
#include "ocrlogs.h"

PageOrientation::PageOrientation()
{

}

void PageOrientation::CalculateOrientationAngle(char* imagePath, char* applicationPath, char* trainingDataPath)
{
	try
	{
		OCRLogs::ApplicationPath = string(applicationPath);
		api = new TessBaseAPI();
		if (api->Init(trainingDataPath, "eng")) {
			return;
		}

		OCRLogs::WriteLogToFile("[Orientation/OCR] Start loading image " + string(imagePath));
		Pix *image = pixRead(imagePath);
		api->SetImage(image);
		OCRLogs::WriteLogToFile("[Orientation/OCR] End loading image " + string(imagePath));

		OSResults os_results;
		OCRLogs::WriteLogToFile("[Orientation/OCR] Start calculating orientation");
		api->DetectOS(&os_results);
		OCRLogs::WriteLogToFile("[Orientation/OCR] End calculating orientation");
		SetPageOrientationAngle(os_results.best_result.orientation_id);

		pixDestroy(&image);
	}
	catch (...)
	{

	}
}

PageOrientation::~PageOrientation()
{
	api->~TessBaseAPI();
}
int PageOrientation::GetPageOrientationAngle()
{
	if (Angle == 1)
	{
		OCRLogs::WriteLogToFile("[Orientation/OCR] Orientation angle found 90 degree");
		return 90;
	}
	else if (Angle == 2)
	{
		OCRLogs::WriteLogToFile("[Orientation/OCR] Orientation angle found 180 degree");
		return 180;
	}
	else if (Angle == 3)
	{
		OCRLogs::WriteLogToFile("[Orientation/OCR] Orientation angle found 270 degree");
		return 270;
	}
	else
	{
		OCRLogs::WriteLogToFile("[Orientation/OCR] Orientation angle found 0 degree");
		return 0;
	}
}
void PageOrientation::SetPageOrientationAngle(int orientation)
{
	Angle = orientation;
}