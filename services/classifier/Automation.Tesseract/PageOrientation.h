#pragma once

#include <baseapi.h>
#include <allheaders.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace tesseract;
using namespace std;

class PageOrientation
{
public:
	PageOrientation();
	~PageOrientation();
	int GetPageOrientationAngle();
	void CalculateOrientationAngle(char* imagePath, char* applicationPath, char* trainingDataPath);
	void SetPageOrientationAngle(int);
	TessBaseAPI *api;
	PIX *Image;
	void WriteOrientationLog(string currentStatusLog);

private:
	int Angle;
	char* ApplicationPath;
};

