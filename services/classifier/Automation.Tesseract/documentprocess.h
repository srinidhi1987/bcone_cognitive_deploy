#include <baseapi.h>
#include <allheaders.h>
#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

using namespace tesseract;
class DocumentProcess
{
public:
	DocumentProcess(int pageSegmentationMode, int ocrEngineMode, char* applicationPath);
	char * DocumentProcessing(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath, char* hOCROutputFile);
	~DocumentProcess();

private:
	TessBaseAPI api;
	PageSegMode SegMode;
	OcrEngineMode EngineMode;
};