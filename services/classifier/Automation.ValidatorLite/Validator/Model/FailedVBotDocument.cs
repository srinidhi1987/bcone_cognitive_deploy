﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Model
{
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FailedVBotDocument
    {
        public VBotDocument FailedDocument { get; set; }
        public FailedVBotDocumentMetaData MetaData { get; set; }
        public VBotDocument CorrectedDocument { get; set; }
        public int State { get; set; }

        public string StateDescription { get; set; }

        public QSetting Queue { get; set; }

        public FileStream PackageFileStream { get; set; }
    }

    public class FailedVBotDocumentMetaData
    {
        public string TaskName { get; set; }
        public string VBotName { get; set; }
        public string TaskVersion { get; set; }
        public string VBotVersion { get; set; }
        public long CreatedDateTimeAsTicks { get; set; }

        public string OutputFilePath { get; set; }

        /// <summary>
        /// It is derived property.
        /// Path.Combine(Queue.QPath.ServerPath,Path.GetFileName(VBotDocument.DocumentProperties.Path + ".failed"))
        /// </summary>
        public string FailedVBotFullPath { get; set; }

        /// <summary>
        /// It is derived property.
        /// Path.Combine(Queue.QPath.LocalMachinePath,Path.GetFileName(VBotDocument.DocumentProperties.Path + ".failed"))
        /// </summary>
        public string FailedVBotFullPathOnLocalMachine { get; set; }

        public FailedVBotDocumentStatus Status { get; set; }
    }

    public enum FailedVBotDocumentStatus
    {
        Created,
        Corrected
    }
}