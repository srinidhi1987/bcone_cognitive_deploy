﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Security.AccessControl;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using VisionBotEngine;
    using ValidatorServices;

    // common contracts; in a separate dll

    //public class ConnectionEndPoint
    //{

    //}

    //public class FileSystemBasedConnectionEndPoint : ConnectionEndPoint
    //{
    //    public string FilePath { get; set; }
    //}

    //public enum ConnectionStatus
    //{
    //    Online,
    //    Offline
    //}

    //public delegate void ConnectivityStatusChangedHandler(ConnectionEndPoint connectionEndPoint, ConnectionStatus oldConnectionStatus, ConnectionStatus newConnectionStatus);

    //public interface IConnectivityService
    //{
    //    ConnectionStatus GetConnectionStatus(ConnectionEndPoint connectionEndPoint);
    //}

    //public interface IConnectivityNotifierService
    //{ 
    //    event ConnectivityStatusChangedHandler ConnectivityStatusChanged;

    //    void StartNotification();
    //}

    //// implementation; in a separate dll

    //public class FileSystemBasedConnectivityService : IConnectivityService
    //{
    //    public ConnectionStatus GetConnectionStatus(ConnectionEndPoint connectionEndPoint)
    //    {
    //        var endPoint = connectionEndPoint as FileSystemBasedConnectionEndPoint;
    //        if (endPoint == null) throw new ArgumentException($"{nameof(connectionEndPoint)} must be of type {nameof(FileSystemBasedConnectionEndPoint)}");

    //        return new NetworkStatus().CheckStatus(endPoint.FilePath) 
    //            ? ConnectionStatus.Online : ConnectionStatus.Offline;
    //    }
    //}

    //public class IterativelyCheckingConnectivityNotifierService : IConnectivityNotifierService,IDisposable
    //{
    //    public ConnectionEndPoint ConnectionEndPoint { get; private set; }
    //    private BackgroundWorker _backgroundWorker_folderAvailability;
    //    public IterativelyCheckingConnectivityNotifierService(
    //        ConnectionEndPoint connectionEndPoint,
    //        IConnectivityService connectivityService)
    //    {
    //        this.ConnectionEndPoint = connectionEndPoint;
    //        this.ConnectivityService = connectivityService;
    //    }

    //    private bool IsNotifierStarted = false;
    //    ConnectionStatus oldConnectivityStatus;
    //    public void StartNotification()
    //    {
    //        if (IsNotifierStarted) return;

    //        IsNotifierStarted = true;
    //        oldConnectivityStatus = ConnectionStatus.Offline;
    //        //TODO: call below in thread
    //        _backgroundWorker_folderAvailability = new BackgroundWorker();
    //        _backgroundWorker_folderAvailability.WorkerSupportsCancellation = true;
    //        _backgroundWorker_folderAvailability.DoWork += new DoWorkEventHandler(backgroundWorker_folderAvailability_DoWork);
    //        _backgroundWorker_folderAvailability.RunWorkerAsync();
    //        _backgroundWorker_folderAvailability.RunWorkerCompleted += _backgroundWorker_folderAvailability_RunWorkerCompleted;
    //    }
    //    private void backgroundWorker_folderAvailability_DoWork(object sender, DoWorkEventArgs e)
    //    {
    //        BackgroundWorker bw = (BackgroundWorker)sender;
    //        while (!bw.CancellationPending)
    //        {
    //            Worker();
    //        }
    //        e.Cancel = true;
    //        //_resetEvent.Set();
    //    }
    //    private void _backgroundWorker_folderAvailability_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    //    {
    //        _backgroundWorker_folderAvailability.Dispose();
    //    }
    //    public event ConnectivityStatusChangedHandler ConnectivityStatusChanged;

    //    private IConnectivityService ConnectivityService { get; set; }

    //    private void Worker()
    //    {
    //        while(true)
    //        {
                
    //            try
    //            {
    //                var newConnectivityStatus = ConnectivityService.GetConnectionStatus(this.ConnectionEndPoint);

    //                if (oldConnectivityStatus != newConnectivityStatus)
    //                {
    //                    if (this.ConnectivityStatusChanged != null)
    //                    {
    //                        ConnectivityStatusChanged(this.ConnectionEndPoint, oldConnectivityStatus, newConnectivityStatus);
    //                    }
    //                }
    //                oldConnectivityStatus = newConnectivityStatus;
    //            }
    //            finally
    //            {
    //                System.Threading.Thread.Sleep(500);
    //            }
    //        }
    //    }

    //    public void Dispose()
    //    {
    //        if (_backgroundWorker_folderAvailability != null)
    //        {
    //            _backgroundWorker_folderAvailability.CancelAsync();
    //        }
    //    }
    //    ~IterativelyCheckingConnectivityNotifierService()
    //    {
    //        this.Dispose();
    //    }
    //}



    //public class DirectoryWatcher : IDisposable
    //{
    //    #region Events and Delegates

    //    public event Action<bool> StatusChanged;
    //    public event Action NetworkReconnected;
    //    #endregion

    //    #region Properties and Variables
    //    INetworkStatus ins;
    //    private string _path;
    //    private BackgroundWorker _backgroundWorker_folderAvailability;
    //    private bool? _isOnline;
    //    public bool IsOnline
    //    {
    //        get
    //        {
    //            return _isOnline.HasValue ? _isOnline.Value : false;
    //        }
    //        set
    //        {
    //            if (_isOnline != value)
    //            {
    //                if(_isOnline == false && value == true)
    //                {
    //                    if (NetworkReconnected != null)
    //                    {
    //                        VBotLogger.Trace(() => $"[{nameof(DirectoryWatcher)}->{nameof(NetworkReconnected)}] NetworkReconnected is fired. Qpath={_path}");
    //                        NetworkReconnected.Invoke();
    //                    }
    //                }
    //                _isOnline = value;
    //                if (StatusChanged != null)
    //                {
    //                    VBotLogger.Trace(() => $"[{nameof(DirectoryWatcher)}->{nameof(StatusChanged)}] StatusChanged is fired. Qpath={_path}");
    //                    StatusChanged.Invoke(value);
    //                }
    //            }
    //        }
    //    }

    //    #endregion

    //    #region Constructor

    //    public DirectoryWatcher(string directoryPath)
    //    {
    //        _path = directoryPath;
    //        IsOnline = true;
    //        ins = new NetworkStatus();
    //        _backgroundWorker_folderAvailability = new BackgroundWorker();
    //        _backgroundWorker_folderAvailability.WorkerSupportsCancellation = true;
    //        _backgroundWorker_folderAvailability.DoWork += new DoWorkEventHandler(backgroundWorker_folderAvailability_DoWork);
    //        _backgroundWorker_folderAvailability.RunWorkerAsync();
    //        _backgroundWorker_folderAvailability.RunWorkerCompleted += _backgroundWorker_folderAvailability_RunWorkerCompleted;
    //        VBotLogger.Trace(() => $"[{nameof(DirectoryWatcher)}->{nameof(DirectoryWatcher)}] DirectoryWather is initialized. Qpath={_path}");
    //    }

    //    private void _backgroundWorker_folderAvailability_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    //    {
    //        _backgroundWorker_folderAvailability.Dispose();
    //    }
    //    ~DirectoryWatcher()
    //    {
    //        this.Dispose();
    //    }

    //    #endregion

    //    #region Implemented Members

    //    public void Dispose()
    //    {
    //        if (_backgroundWorker_folderAvailability != null)
    //        {
    //            _backgroundWorker_folderAvailability.CancelAsync();
    //        }
    //        VBotLogger.Trace(() => $"[{nameof(DirectoryWatcher)}->{nameof(DirectoryWatcher)}] DirectoryWather is disposed. Qpath={_path}");
    //    }
        

    //    #endregion

    //    #region Internal Functionality


    //    private void backgroundWorker_folderAvailability_DoWork(object sender, DoWorkEventArgs e)
    //    {
    //        BackgroundWorker bw = (BackgroundWorker)sender;
    //        while (!bw.CancellationPending)
    //        {
    //            CheckFolderAvailability();
    //        }
    //        e.Cancel = true;
    //        //_resetEvent.Set();
    //    }

    //    private void CheckFolderAvailability()
    //    {
    //        try
    //        {
                
    //            IsOnline = ins.CheckStatus(_path);
    //        }
    //        catch
    //        {
    //            IsOnline = false;
    //        }
    //        finally
    //        {
    //            System.Threading.Thread.Sleep(500);
    //        }
    //    }
    //    #endregion
    //}
}
