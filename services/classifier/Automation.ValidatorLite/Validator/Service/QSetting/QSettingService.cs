﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service
{
    using Analytics;
    //using Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    //using Properties;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.AccessControl;
    using System.Threading.Tasks;
    using UI.ViewModel;
    using VisionBot;
    using VisionBotEngine;

    public class QSettingService : IObservable<QSetting>, IQSettingService
    {
        public static List<IObserver<QSetting>> observers;
        protected string initPathOfQSettings;
        protected string pathOfJSON;

        //TODO: Remove hard-coding.
        private string qSettingsFileName = Automation.CognitiveData.Properties.Resources.qSettingsJSONFileName;

        private static QSettingWatcher.QSettingWatcher qSettingWatcher;

        public static event Action<QSetting> OpenDocumentInValidatorRequest;

        //TODO: Have a service registry to resolve depedency of QSettingService.
        //TODO: Remove hard coupling of the code with System.IO.File interface.
        public QSettingService(IEnumerable<IObserver<QSetting>> listOfObservers = null)
        {
            subscribe(listOfObservers);
            Setup();
        }
        ~QSettingService()
        {
        }
        public static void ValidatorViewModel_QueueReconnected(string queuepath)
        {
            qSettingWatcher.RemoveQueueFromWatcher(queuepath);
            QSetting q = new QSetting();
            q.QueuePath = new QPath();
            q.QueuePath.ServerPath = queuepath;
            qSettingWatcher.AddQueueInWatcher(q);
            if (OpenDocumentInValidatorRequest != null)
                Task.Run(() => { OpenDocumentInValidatorRequest(q); });
        }

        private void subscribe(IEnumerable<IObserver<QSetting>> listOfObservers)
        {
            if (listOfObservers != null)
            {
                observers = listOfObservers.ToList();

                foreach (var observer in observers)
                    this.Subscribe(observer);
            }
        }

        protected virtual void Setup()
        {
            //TODO: Why if logic inside constructor?
            //It should be strategy driven.
            if (string.IsNullOrEmpty(initPathOfQSettings))
            {
                IQBotDataPathProvider provider = new IQBotDataPathProvider();
                pathOfJSON = provider.GetPath();
                initPathOfQSettings = Path.GetTempPath();
            }
        }

        /// <summary>
        /// Add new Error Queue to Error Queue List.
        /// </summary>
        /// <param name="addRequest"></param>
        /// <returns></returns>
        //public async Task<IServiceResponse<IEnumerable<QSetting>>> Add(IServiceRequest<QSetting> addRequest)
        //{
        //    //QSetting exists for the given user?
        //    //If yes, read all available queue settings.
        //    //Is qsetting already exists? if Yes, update stats and send response back.
        //    //If No, add qsetting to the list of queue settings.
        //    //Make network calls and check the validity of the qsetting object.
        //    try
        //    {
        //        VBotLogger.Trace(() => string.Format("[QSettingService -> Add] Adding queue  -> {0}", addRequest.Model.QueuePath.DisplayPath));
        //        QSettingAddResponse response = new QSettingAddResponse();

        //        if (addRequest == null)
        //        {
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 401;
        //            response.ErrorCode = 100; //Request not found.
        //            return response;
        //        }
        //        if (addRequest.Model == null)
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 101; //Model not found.
        //            return response;
        //        }
        //        if (addRequest.Model.QueuePath == null)
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 201; //QueuePath not found.
        //            return response;
        //        }
        //        if (string.IsNullOrEmpty(addRequest.Model.QueuePath.LocalMachinePath))
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 301; //LocalMachinePath for queue not found.
        //            return response;
        //        }
        //        if (string.IsNullOrEmpty(addRequest.Model.QueuePath.ServerPath))
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 401; //ServerPath for queue not found.
        //            return response;
        //        }
        //        List<Task> tasks = new List<Task>();
        //        QSettingList qSettingList = null;

        //        if (addRequest.Model.QueuePath != null &&
        //            !string.IsNullOrEmpty(addRequest.Model.QueuePath.ServerPath)
        //            && !string.IsNullOrEmpty(addRequest.Model.QueuePath.LocalMachinePath))
        //        {
        //            //Check if this record exists in local machine's qSettings.json file.
        //            FileInfo fileInfo =
        //                    new FileInfo(Path.Combine(this.pathOfJSON, qSettingsFileName));
        //            if (!fileInfo.Exists)
        //            {
        //                //Not valid request.
        //                response.Model = null;
        //                response.Request = null;
        //                response.ResponseCode = 301;
        //                response.ErrorCode = 601; //QSettings.json file for queue not found.
        //                return response;
        //            }
        //            if (fileInfo.Exists)
        //            {
        //                addRequest.Model.QueuePath.SetQueueName();
        //                addRequest.Model.QueuePath.LocalMachinePath = Path.Combine(this.initPathOfQSettings, addRequest.Model.QueuePath.LocalMachinePath);
        //                addRequest.Model.State = Convert.ToInt32(QSettingsState.Init);
        //                addRequest.Model.StateDescription = "Adding entry to Queue settings…";
        //                //NotifyMessageToObservers(addRequest.Model);

        //                QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
        //                {
        //                    using (var fileStream = fileInfo.OpenText())

        //                    {
        //                        string qSettingAsJson = fileStream.ReadToEnd();
        //                        qSettingList = JsonConvert.DeserializeObject<QSettingList>(qSettingAsJson);
        //                    }
        //                });
        //                if (qSettingList.qSettings.Count > 0)
        //                {
        //                    var queueEntry = (from q in qSettingList.qSettings
        //                                      where q.QueuePath.ServerPath.Equals(addRequest.Model.QueuePath.ServerPath)
        //                                      select q).FirstOrDefault();

        //                    if (queueEntry != null)
        //                    {
        //                        //Not valid request.
        //                        response.Model = null;
        //                        response.Request = null;
        //                        response.ResponseCode = 301;
        //                        response.ErrorCode = 701; //Queue already exists found.
        //                        NotifyCompleteMessageToObservers();

        //                        return response;
        //                    }
        //                }
        //            }
        //        }
        //        //NotifyMessageToObservers(addRequest.Model);

        //        addRequest.Model.QueuePermission = new QPermission();
        //        addRequest.Model.QueueStats = new QStats();
        //        addRequest.Model.OperationType = Convert.ToInt32(QSettingOperationType.Add);
        //        tasks.Add(Task.Run(
        //                   () =>
        //                   {
        //                       QSetting tempQSetting = addRequest.Model;
        //                       tempQSetting.OperationType = Convert.ToInt32(QSettingOperationType.Add);
        //                       LongRunningTask(tempQSetting);
        //                       AddQueueInWatcher(addRequest.Model);
        //                   }));

        //        await Task.WhenAll(tasks.ToArray()).
        //            ContinueWith((ascendent) =>
        //            {
        //                if (ascendent.IsCompleted) { }
        //            });
        //        if (addRequest.Model.QueuePermission.CanReadQueue
        //            && addRequest.Model.QueuePermission.CanWriteQueue
        //            && addRequest.Model.QueuePermission.CanDeleteQueue
        //            && addRequest.Model.QueuePermission.CanConnectQueue)
        //        {
        //            if (qSettingList == null)
        //            {
        //                qSettingList = new QSettingList();
        //            }
        //            if (qSettingList.qSettings == null)
        //            {
        //                qSettingList.qSettings = new List<QSetting>();
        //            }
        //            qSettingList.qSettings.Add(addRequest.Model);

        //            addRequest.Model.StateDescription = string.Format("Queue added successfully : {0}", addRequest.Model.QueuePath.DisplayPath);
        //            //NotifyMessageToObservers(addRequest.Model);
        //            NotifyCompleteMessageToObservers();

        //            JsonSerializer serializer = new JsonSerializer();
        //            serializer.Converters.Add(new JavaScriptDateTimeConverter());
        //            serializer.NullValueHandling = NullValueHandling.Ignore;
        //            QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
        //            {
        //                using (StreamWriter sw =
        //                new StreamWriter(JSONFileFullPath))
        //                using (JsonWriter jw = new JsonTextWriter(sw))
        //                {
        //                    serializer.Serialize(jw, qSettingList);
        //                }
        //            });
        //            response = new QSettingAddResponse();
        //            response.ResponseCode = 200;
        //            response.Model = qSettingList.qSettings;
        //        }
        //        else
        //        {
        //            VBotLogger.Trace(() => string.Format("[QSettingService -> Add] Queue not added. Queue does not have permission  -> {0}", addRequest.Model.QueuePath.DisplayPath));
        //            response = new QSettingAddResponse();
        //            response.ErrorCode = 801;
        //            response.Model = qSettingList.qSettings;
        //        }

        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.Log(nameof(QSettingService), nameof(Add));
        //        QSettingAddResponse response = new QSettingAddResponse();
        //        //TODO: Need to fix the type definition.
        //        //response.Request = initRequest;
        //        response.ResponseCode = 100;
        //        return response;
        //    }
        //}

        /// <summary>
        /// Get Error Queue Details.
        /// </summary>
        /// <param name="getRequest"></param>
        /// <returns></returns>
        //public async Task<IServiceResponse<IEnumerable<QSetting>>> Get(IServiceRequest<QSetting> getRequest)
        //{
        //    //QSettings for given user?
        //    //If not found, return not found.
        //    //TODO: input request validation should be part of each function call.
        //    //TODO: Refactor the code. Input validation should be done in separate class.
        //    QSettingGetResponse response = new QSettingGetResponse();
        //    if (getRequest == null)
        //    {
        //        response.Model = null;
        //        response.Request = null;
        //        response.ResponseCode = 401;
        //        response.ErrorCode = 100; //Request is null;
        //        return response;
        //    }
        //    if (getRequest.Model == null)
        //    {
        //        //Not valid request.
        //        response.Model = null;
        //        response.Request = null;
        //        response.ResponseCode = 301;
        //        response.ErrorCode = 101; //Model not found.
        //        return response;
        //    }
        //    if (getRequest.Model.QueuePath == null)
        //    {
        //        //Not valid request.
        //        response.Model = null;
        //        response.Request = null;
        //        response.ResponseCode = 301;
        //        response.ErrorCode = 201; //QueuePath not found.
        //        return response;
        //    }
        //    if (string.IsNullOrEmpty(getRequest.Model.QueuePath.LocalMachinePath))
        //    {
        //        //Not valid request.
        //        response.Model = null;
        //        response.Request = null;
        //        response.ResponseCode = 301;
        //        response.ErrorCode = 301; //LocalMachinePath for queue not found.
        //        return response;
        //    }
        //    if (string.IsNullOrEmpty(getRequest.Model.QueuePath.ServerPath))
        //    {
        //        //Not valid request.
        //        response.Model = null;
        //        response.Request = null;
        //        response.ResponseCode = 301;
        //        response.ErrorCode = 401; //ServerPath for queue not found.
        //        return response;
        //    }
        //    List<Task> tasks = new List<Task>();
        //    QSettingList qSettingList = null;

        //    if (getRequest.Model.QueuePath != null &&
        //        !string.IsNullOrEmpty(getRequest.Model.QueuePath.ServerPath)
        //        && !string.IsNullOrEmpty(getRequest.Model.QueuePath.LocalMachinePath))
        //    {
        //        //Check if this record exists in local machine's qSettings.json file.
        //        FileInfo fileInfo =
        //                new FileInfo(Path.Combine(pathOfJSON, qSettingsFileName));
        //        if (!fileInfo.Exists)
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 601; //QSettings.json file for queue not found.
        //            return response;
        //        }

        //        if (fileInfo.Exists)
        //        {
        //            getRequest.Model.State = Convert.ToInt32(QSettingsState.Init);

        //            getRequest.Model.StateDescription = "Finding Queue Entry in QSettings...";
        //            //NotifyMessageToObservers(getRequest.Model);
        //            QSettingList qSettings = null;
        //            using (var fileStream = fileInfo.OpenText())
        //            {
        //                string qSettingAsJson = fileStream.ReadToEnd();
        //                qSettings = JsonConvert.DeserializeObject<QSettingList>(qSettingAsJson);
        //            }

        //            var queueEntry = (from q in qSettings.qSettings
        //                              where q.QueuePath.ServerPath.Equals(getRequest.Model.QueuePath.ServerPath)
        //                              select q).FirstOrDefault();

        //            if (queueEntry == null)
        //            {
        //                //Not valid request.
        //                response.Model = null;
        //                response.Request = null;
        //                response.ResponseCode = 301;
        //                response.ErrorCode = 501; //Queue not found.
        //                return response;
        //            }
        //            //TODO: Override list of queues to a single queue entry. And then fire async tasks.
        //            qSettingList = new QSettingList()
        //            {
        //                qSettings = new List<QSetting>() { getRequest.Model }
        //            };
        //        }
        //    }

        //    if (qSettingList != null &&
        //        qSettingList.qSettings.Count > 0)
        //    {
        //        await Task.Run(async () =>
        //        {
        //            getRequest.Model.StateDescription = Convert.ToString(qSettingList.qSettings.Count);
        //            //getRequest.Model.StateDescription = string.Format("Count:{0}", qSettingList.qSettings.Count);
        //            //NotifyMessageToObservers(getRequest.Model);
        //            foreach (var q in qSettingList.qSettings)
        //            {
        //                q.StateDescription = string.Format("Found:{0}", q.QueuePath.ServerPath);
        //                //NotifyMessageToObservers(q);
        //            }
        //            foreach (var qsetting in qSettingList.qSettings)
        //            {
        //                tasks.Add(Task.Run(
        //                       () =>
        //                       {
        //                           QSetting tempQSetting = qsetting;
        //                           LongRunningTask(tempQSetting);
        //                       }));
        //            }
        //            await Task.WhenAll(tasks);
        //        });
        //    }
        //    response = new QSettingGetResponse();
        //    response.ResponseCode = 200;
        //    response.Model = qSettingList.qSettings;
        //    return response;
        //}

        /// <summary>
        /// Initialize Validator Settings Grid.
        /// </summary>
        /// <param name="initRequest">Request</param>
        /// <returns>Response</returns>
        //public async Task<IServiceResponse<IEnumerable<QSetting>>> Init(IServiceRequest<QSetting> initRequest)
        //{
        //    try
        //    {
        //        //TODO: change the state of the model and call observers.
        //        VBotLogger.Trace(() => string.Format("[QSettingService -> Init] Queues initialization started"));
        //        QSettingList qSettings = null;
        //        ConcurrentBag<Task> tasks = new ConcurrentBag<Task>();
        //        QSettingInitRequest tempinitRequest = initRequest as QSettingInitRequest;
        //        if (tempinitRequest == null)
        //        {
        //            tempinitRequest = new QSettingInitRequest();
        //            tempinitRequest.Model = new QSetting();
        //            tempinitRequest.Model.QueuePath = new QPath();
        //            tempinitRequest.Model.QueuePath.LocalMachinePath = initPathOfQSettings;
        //            tempinitRequest.Model.State = Convert.ToInt32(QSettingsState.Init);
        //            tempinitRequest.Model.StateDescription = "Initializing Queue settings...";
        //            //NotifyMessageToObservers(tempinitRequest.Model);
        //        }
        //        //Initialize QSetting for user.
        //        if (tempinitRequest.Model.QueuePath != null
        //            && !string.IsNullOrEmpty(tempinitRequest.Model.QueuePath.LocalMachinePath))
        //        {
        //            FileInfo fileInfo =
        //                new FileInfo(Path.Combine(pathOfJSON, qSettingsFileName));
        //            //For first time, qsettings will be empty.
        //            //Create an empty qsettins and save into application path.
        //            if (!fileInfo.Exists)
        //            {
        //                QSettingList list = new QSettingList();
        //                list.qSettings = new List<QSetting>();
        //                FakeQSettingPath(list.qSettings);
        //                JsonSerializer serializer = new JsonSerializer();
        //                serializer.Converters.Add(new JavaScriptDateTimeConverter());
        //                serializer.NullValueHandling = NullValueHandling.Ignore;
        //                QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
        //                {
        //                    using (StreamWriter sw =
        //                        new StreamWriter(JSONFileFullPath))
        //                    using (JsonWriter jw = new JsonTextWriter(sw))
        //                    {
        //                        serializer.Serialize(jw, list);
        //                    }
        //                });
        //            }

        //            fileInfo.Refresh();
        //            //Next time, file exists.
        //            //Read json file and deserialize the qsetting objects.
        //            //Return Model
        //            if (fileInfo.Exists)
        //            {
        //                tempinitRequest.Model.State = Convert.ToInt32(QSettingsState.Found);
        //                tempinitRequest.Model.StateDescription = "Reading Queue settings...";
        //                //NotifyMessageToObservers(tempinitRequest.Model);

        //                using (var fileStream = fileInfo.OpenText())
        //                {
        //                    string qSettingAsJson = fileStream.ReadToEnd();
        //                    qSettings = JsonConvert.DeserializeObject<QSettingList>(qSettingAsJson);
        //                }
        //                if (qSettings.qSettings.Count > 0)
        //                {
        //                    tempinitRequest.Model.State = Convert.ToInt32(QSettingsState.Count);
        //                    tempinitRequest.Model.StateDescription = qSettings.qSettings.Count.ToString();
        //                    //NotifyMessageToObservers(tempinitRequest.Model);
        //                    foreach (var qsetting in qSettings.qSettings)
        //                    {
        //                        tasks.Add(Task.Run(
        //                        () =>
        //                        {
        //                            QSetting tempQSetting = qsetting;
        //                            tempQSetting.OperationType = Convert.ToInt32(QSettingOperationType.Init);
        //                            LongRunningTask(tempQSetting);
        //                            AddQueueInWatcher(tempQSetting);
        //                        }));
        //                    }

        //                    await Task.WhenAll(tasks.ToArray()).
        //                    ContinueWith((ascendent) =>
        //                    {
        //                        if (ascendent.IsCompleted)
        //                        {
        //                            JsonSerializer jsSerializer = new JsonSerializer();
        //                            jsSerializer.Converters.Add(new JavaScriptDateTimeConverter());
        //                            jsSerializer.NullValueHandling = NullValueHandling.Ignore;
        //                            QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
        //                            {
        //                                using (StreamWriter sw =
        //                                new StreamWriter(JSONFileFullPath))
        //                                using (JsonWriter jw = new JsonTextWriter(sw))
        //                                {
        //                                    jsSerializer.Serialize(jw, qSettings);
        //                                }
        //                            });

        //                            NotifyCompleteMessageToObservers();
        //                        }
        //                    });
        //                }
        //                else
        //                {
        //                    NotifyCompleteMessageToObservers();
        //                }
        //            }
        //        }

        //        //First time user, needs to have Initialize QSetting.
        //        QSettingInitResponse response = new QSettingInitResponse();
        //        response.ResponseCode = 200;
        //        response.Model = qSettings.qSettings;
        //        VBotLogger.Trace(() => string.Format("[QSettingService -> Init] Queues initialization completed"));
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.Log(nameof(QSettingService), nameof(Init));
        //        QSettingInitResponse response = new QSettingInitResponse();
        //        //TODO: Need to fix the type definition.
        //        //response.Request = initRequest;
        //        response.ResponseCode = 100;
        //        return response;
        //    }
        //}

        protected virtual void FakeQSettingPath(IList<QSetting> qSettings)
        {
        }

        internal void LongRunningTask(QSetting qSetting)
        {
            try
            {
                VBotLogger.Trace(() => string.Format("[QSettingService -> LongRunningTask] Checking permission for queue -> {0}", qSetting.QueuePath.DisplayPath));

                if (qSetting.QueueStats == null)
                    qSetting.QueueStats = new QStats();

                //FailQ folder exist
                if (Directory.Exists(qSetting.QueuePath.ServerPath))
                {
                    if (doesQueueHavePermission(qSetting, qSetting.QueuePath.ServerPath))
                    {
                        VBotLogger.Trace(() => string.Format("[QSettingService -> LongRunningTask] Queue have all permission : {0}", qSetting.QueuePath.DisplayPath));
                        DirectoryInfo directoryInfo = new DirectoryInfo(qSetting.QueuePath.ServerPath);
                        var fileInfos = directoryInfo.GetFiles("*" + Eyeball.FailedFileExtensionWithDot);
                        var fileInfosCorrected = directoryInfo.GetFiles("*" + Eyeball.CorrectedFileExtension);
                        Dictionary<string, FileInfo> dictFileInfoCorrected = fileInfosCorrected.ToDictionary(x => Path.GetFileNameWithoutExtension(x.Name), x => x);
                        int count = 0;
                        foreach (var fileinfo in fileInfos)
                        {
                            if (!dictFileInfoCorrected.ContainsKey(fileinfo.Name))
                                count++;
                            else
                            {
                                Task.Run(() => {
                                    try {
                                        File.Delete(fileinfo.FullName);
                                    }
                                    catch(Exception e)
                                    {
                                        e.Log(nameof(QSettingService), nameof(LongRunningTask), $"Unable to cleanup file: {fileinfo.FullName}");
                                    }
                                });
                            }
                        }
                        qSetting.QueueStats.TotalNoOfDocumentsInQ = count;

                        if (qSetting.QSettingsFileSystemEventType == QSettingsFileSystemEventType.Created &&
                            count > 0 && OpenDocumentInValidatorRequest != null)
                        {
                            VBotLogger.Trace(() => $"[{nameof(QSettingService)}->{nameof(LongRunningTask)}] Autoload Event Trigger.");
                            Task.Run(() => { OpenDocumentInValidatorRequest(qSetting); });
                        }

                        qSetting.QueuePath.TaskName = getTaskName(qSetting, fileInfos);
                    }
                    else
                    {
                        qSetting.QueueStats.TotalNoOfDocumentsInQ = null;
                        qSetting.QueuePath.TaskName = "Queue is inaccessible";
                    }
                }
                else
                {
                    //FailQ folder does not exist
                    if (doesQueueHavePermission(qSetting, Path.GetDirectoryName(qSetting.QueuePath.ServerPath)))
                    {
                        VBotLogger.Trace(() => string.Format("[QSettingService -> LongRunningTask] Queue have all permission : {0}", qSetting.QueuePath.DisplayPath));
                        qSetting.QueueStats.TotalNoOfDocumentsInQ = 0;
                        qSetting.QueuePath.TaskName = string.Empty;
                    }
                    else
                    {
                        qSetting.QueueStats.TotalNoOfDocumentsInQ = null;
                        qSetting.QueuePath.TaskName = "Queue is inaccessible";
                    }
                }

                qSetting.State = Convert.ToInt32(QSettingsState.Complete);
                qSetting.StateDescription = string.Format("Queue read/ write permission check complete : {0}", qSetting.QueuePath.DisplayPath);
                //NotifyMessageToObservers(qSetting);

                VBotLogger.Trace(() => string.Format("[QSettingService -> LongRunningTask] Successful processing of queue {0}", qSetting.QueuePath.DisplayPath));
            }
            catch (Exception ex)
            {
                ex.Log(nameof(QSettingService), nameof(LongRunningTask));
            }
        }

        private static string getTaskName(QSetting qSetting, FileInfo[] fileInfos)
        {
            if (fileInfos.Count() <= 0)
                return string.Empty;

            ValidatorEngine validatorEngine = new ValidatorEngine();
            foreach (FileInfo file in fileInfos)
            {
                try
                {
                    var metaData = validatorEngine.TryGetMetadata(file.FullName);
                    if (metaData == null) continue;
                    return metaData.TaskName;
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(QSettingService), nameof(getTaskName));
                }
            }
            return string.Empty;
        }

        private bool doesQueueHavePermission(QSetting qSetting, string path)
        {
            return IsNetworkAvailable(qSetting, path) && DoesQueueHaveAccessPermission(qSetting, path);
        }

        private bool DoesQueueHaveAccessPermission(QSetting qSetting, string path)
        {
            bool permission = true;
            qSetting.State = Convert.ToInt32(QSettingsState.ReadPermissionCheck);
            qSetting.StateDescription = string.Format("Checking Read/Write Permission: {0}", qSetting.QueuePath.DisplayPath);
            //NotifyMessageToObservers(qSetting);
            try
            {
                AuthorizationRuleCollection collection = Directory.
                                                    GetAccessControl(path)
                                                    .GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                foreach (FileSystemAccessRule rule in collection)
                {
                    if (rule.AccessControlType == AccessControlType.Deny)
                    {
                        permission = false;
                        VBotLogger.Trace(() => string.Format("[QSettingService -> DoesQueueHaveAccessPermission] Read/Write permission fail for Queue : {0}", qSetting.QueuePath.DisplayPath));
                    }
                }

                qSetting.QueuePermission.CanReadQueue = permission;
                qSetting.QueuePermission.CanWriteQueue = permission;
                qSetting.QueuePermission.CanDeleteQueue = permission;

                return permission;
            }
            catch (Exception ex)
            {
                permission = false;
                qSetting.QueuePermission.CanReadQueue = permission;
                qSetting.QueuePermission.CanWriteQueue = permission;
                qSetting.QueuePermission.CanDeleteQueue = permission;
                VBotLogger.Trace(() => string.Format("[QSettingService -> DoesQueueHaveAccessPermission] Read/Write permission fail for Queue : {0}", qSetting.QueuePath.DisplayPath));
                return permission;
            }
        }

        private bool IsNetworkAvailable(QSetting qSetting, string path)
        {
            try
            {
                qSetting.State = Convert.ToInt32(QSettingsState.NetworkCheck);
                qSetting.StateDescription = string.Format("Checking Network Availability: {0}", qSetting.QueuePath.DisplayPath);
                //NotifyMessageToObservers(qSetting);

                qSetting.QueuePermission.CanConnectQueue = Directory.Exists(path);

                if (!qSetting.QueuePermission.CanConnectQueue)
                    VBotLogger.Trace(() => string.Format("[QSettingService -> IsNetworkAvailable] Network is not available for Queue :{0}", qSetting.QueuePath.DisplayPath));
                return qSetting.QueuePermission.CanConnectQueue;
            }
            catch (Exception ex)
            {
                VBotLogger.Trace(() => string.Format("[QSettingService -> IsNetworkAvailable] Network is not available for Queue :{0}", qSetting.QueuePath.DisplayPath));
                return qSetting.QueuePermission.CanConnectQueue = false;
            }
        }

        private void AddQueueInWatcher(QSetting qSetting)
        {
            if (qSettingWatcher == null)
                qSettingWatcher = new QSettingWatcher.QSettingWatcher(observers);

            if (qSettingWatcher != null
                && qSetting.QueuePermission.CanConnectQueue
                && qSetting.QueuePermission.CanReadQueue
                && qSetting.QueuePermission.CanWriteQueue
                && qSetting.QueuePermission.CanDeleteQueue)
            {
                qSettingWatcher.AddQueueInWatcher(qSetting);
            }
        }

        public IDisposable Subscribe(IObserver<QSetting> observer)
        {
            //Return IDisposable unscriber back to the Service.
            //Service allows
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }

        /// <summary>
        /// Updates QSettings.
        /// Pass null to refresh all error queues.
        /// </summary>
        /// <param name="updateRequest"></param>
        /// <returns></returns>
        public async Task<IServiceResponse<IEnumerable<QSetting>>> Refresh(IServiceRequest<QSetting> updateRequest)
        {
            //Send updated model.
            //if updateRequest is null.
            try
            {
                VBotLogger.Trace(() => string.Format("[QSettingService -> Refresh] Refreshing queue -> {0}", updateRequest.Model.QueuePath.DisplayPath));
                QSettingList qSettingList = null;
                var tasks = new List<Task>();

                QSettingUpdateResponse response = new QSettingUpdateResponse();
                if (updateRequest == null)
                {
                    updateRequest = new QSettingUpdateRequest
                    {
                        Model = new QSetting
                        {
                            QueuePath = new QPath
                            {
                                ServerPath = initPathOfQSettings
                            },
                            State = Convert.ToInt32(QSettingsState.Init),
                            StateDescription = "Refreshing QSettings..."
                        }
                    };
                    //NotifyMessageToObservers(updateRequest.Model);
                }
                //TODO: Check Parameters here. See Add API.
                //Initialize QSetting for user.
                //Assuming Parameter checks are done.
                if (!string.IsNullOrEmpty(updateRequest.Model.QueuePath?.ServerPath))
                {
                    var fileInfo =
                        new FileInfo(Path.Combine(pathOfJSON, qSettingsFileName));
                    //For first time, qsettings will be empty.
                    //Create an empty qsettins and save into application path.
                    if (!fileInfo.Exists)
                    {
                        var list = new QSettingList { qSettings = new List<QSetting>() };
                        FakeQSettingPath(list.qSettings);
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Converters.Add(new JavaScriptDateTimeConverter());
                        serializer.NullValueHandling = NullValueHandling.Ignore;
                        QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
                        {
                            using (var sw =
                            new StreamWriter(JSONFileFullPath))
                            using (JsonWriter jw = new JsonTextWriter(sw))
                            {
                                serializer.Serialize(jw, list);
                            }
                        });
                    }
                    fileInfo.Refresh();
                    //Next time, file exists.
                    //Read json file and deserialize the qsetting objects.
                    //Return Model
                    if (fileInfo.Exists)
                    {
                        updateRequest.Model.State = Convert.ToInt32(QSettingsState.Found);
                        updateRequest.Model.StateDescription = "Refreshing QSettings...";
                        //NotifyMessageToObservers(updateRequest.Model);
                        QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
                        {
                            using (var fileStream = fileInfo.OpenText())
                            {
                                string qSettingAsJson = fileStream.ReadToEnd();
                                qSettingList = JsonConvert.DeserializeObject<QSettingList>(qSettingAsJson);
                            }
                        });

                        if (qSettingList.qSettings.Count > 0)
                        {
                            var queueEntry = (from q in qSettingList.qSettings
                                              where q.QueuePath.ServerPath.Equals(updateRequest.Model.QueuePath.ServerPath)
                                              select q).FirstOrDefault();
                            queueEntry.OperationType = Convert.ToInt32(QSettingOperationType.Refresh);
                            queueEntry.QSettingsFileSystemEventType = updateRequest.Model.QSettingsFileSystemEventType;
                            QSetting tempQSetting = null;
                            await Task.Run(async () =>
                            {
                                if (queueEntry != null)
                                {
                                    Task task = Task.Run(
                                        () =>
                                        {
                                            tempQSetting = queueEntry;
                                            LongRunningTask(tempQSetting);
                                            AddQueueInWatcher(tempQSetting);
                                        }
                                        );

                                    await Task.WhenAll(task).
                                    ContinueWith((ascendent) =>
                                    {
                                        if (ascendent.IsCompleted)
                                        {
                                            (from q in qSettingList.qSettings
                                             where q.QueuePath.ServerPath.Equals(tempQSetting.QueuePath.ServerPath)
                                             select q).ToList().ForEach(q => q = tempQSetting);

                                            JsonSerializer jsSerializer = new JsonSerializer();
                                            jsSerializer.Converters.Add(new JavaScriptDateTimeConverter());
                                            jsSerializer.NullValueHandling = NullValueHandling.Ignore;

                                            QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
                                            {
                                                using (StreamWriter sw =
                                                new StreamWriter(JSONFileFullPath))
                                                using (JsonWriter jw = new JsonTextWriter(sw))
                                                {
                                                    jsSerializer.Serialize(jw, qSettingList);
                                                }
                                            });

                                            NotifyCompleteMessageToObservers();
                                        }
                                    });
                                }
                            });
                        }
                    }
                }

                response = new QSettingUpdateResponse();
                response.ResponseCode = 200;
                response.Model = qSettingList.qSettings;
                return response;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(QSettingService), nameof(Refresh));
                QSettingUpdateResponse response = new QSettingUpdateResponse();
                //TODO: Need to fix the type definition.
                //response.Request = initRequest;
                response.ResponseCode = 100;
                return response;
            }
        }

        //public async Task<IServiceResponse<IEnumerable<QSetting>>> Delete(IServiceRequest<QSetting> deleteRequest)
        //{
        //    //Delete
        //    //Clean up local directory
        //    try
        //    {
        //        VBotLogger.Trace(() => string.Format("[QSettingService -> Delete] Deleting queue  -> {0}", deleteRequest.Model.QueuePath.DisplayPath));
        //        var response = new QSettingDeleteResponse();
        //        if (deleteRequest == null)
        //        {
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 401;
        //            response.ErrorCode = 100; //Request not found.
        //            return response;
        //        }
        //        if (deleteRequest.Model == null)
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 101; //Model not found.
        //            return response;
        //        }
        //        if (deleteRequest.Model.QueuePath == null)
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 201; //QueuePath not found.
        //            return response;
        //        }
        //        //if (string.IsNullOrEmpty(deleteRequest.Model.QueuePath.LocalMachinePath))
        //        //{
        //        //    //Not valid request.
        //        //    response.Model = null;
        //        //    response.Request = null;
        //        //    response.ResponseCode = 301;
        //        //    response.ErrorCode = 301; //LocalMachinePath for queue not found.
        //        //    return response;
        //        //}
        //        if (string.IsNullOrEmpty(deleteRequest.Model.QueuePath.ServerPath))
        //        {
        //            //Not valid request.
        //            response.Model = null;
        //            response.Request = null;
        //            response.ResponseCode = 301;
        //            response.ErrorCode = 401; //ServerPath for queue not found.
        //            return response;
        //        }
        //        var tasks = new List<Task>();
        //        QSettingList qSettingList = null;
        //        if (!string.IsNullOrEmpty(deleteRequest.Model.QueuePath?.ServerPath))
        //        // && !string.IsNullOrEmpty(deleteRequest.Model.QueuePath.LocalMachinePath))
        //        {
        //            //Check if this record exists in local machine's qSettings.json file.
        //            var fileInfo =
        //                new FileInfo(Path.Combine(this.pathOfJSON, qSettingsFileName));
        //            if (!fileInfo.Exists)
        //            {
        //                //Not valid request.
        //                response.Model = null;
        //                response.Request = null;
        //                response.ResponseCode = 301;
        //                response.ErrorCode = 601; //QSettings.json file for queue not found.
        //                return response;
        //            }
        //            if (fileInfo.Exists)
        //            {
        //                deleteRequest.Model.State = Convert.ToInt32(QSettingsState.Init);
        //                deleteRequest.Model.StateDescription = "Deleting entry from Queue settings...";
        //                //NotifyMessageToObservers(deleteRequest.Model);

        //                QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
        //                {
        //                    using (var fileStream = fileInfo.OpenText())
        //                    {
        //                        var qSettingAsJson = fileStream.ReadToEnd();
        //                        qSettingList = JsonConvert.DeserializeObject<QSettingList>(qSettingAsJson);
        //                    }
        //                });
        //                if (qSettingList.qSettings.Count > 0)
        //                {
        //                    var queueEntry = (from q in qSettingList.qSettings
        //                                      where q.QueuePath.ServerPath.Equals(deleteRequest.Model.QueuePath.ServerPath)
        //                                      select q).FirstOrDefault();

        //                    if (queueEntry != null)
        //                    {
        //                        qSettingList.qSettings.Remove(queueEntry);

        //                        qSettingWatcher.RemoveQueueFromWatcher(queueEntry.QueuePath.ServerPath);
        //                        VBotLogger.Trace(() => string.Format("[QSettingService -> Delete] Queue deleted successfully : {0} ", queueEntry.QueuePath.DisplayPath));

        //                        deleteRequest.Model.StateDescription = string.Format("Queue deleted successfully:{0}", queueEntry.QueuePath.DisplayPath);
        //                        //NotifyMessageToObservers(deleteRequest.Model);

        //                        NotifyCompleteMessageToObservers();

        //                        //TODO: Delete local directory.
        //                        //  DirectoryInfo deleteLocalDirectory = new DirectoryInfo(Path.Combine(queueEntry.QueuePath.LocalMachinePath));
        //                        // deleteLocalDirectory.Delete(true);
        //                    }
        //                    if (queueEntry == null)
        //                    {
        //                        //Not valid request.
        //                        response.Model = null;
        //                        response.Request = null;
        //                        response.ResponseCode = 301;
        //                        response.ErrorCode = 501; //Queue not found.
        //                        return response;
        //                    }
        //                }
        //            }
        //        }

        //        JsonSerializer serializer = new JsonSerializer();
        //        serializer.Converters.Add(new JavaScriptDateTimeConverter());
        //        serializer.NullValueHandling = NullValueHandling.Ignore;
        //        QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
        //        {
        //            using (StreamWriter sw =
        //            new StreamWriter(JSONFileFullPath))
        //            using (JsonWriter jw = new JsonTextWriter(sw))
        //            {
        //                serializer.Serialize(jw, qSettingList);
        //            }
        //        });
        //        response = new QSettingDeleteResponse();
        //        response.ResponseCode = 200;
        //        response.Model = qSettingList.qSettings;
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.Log(nameof(QSettingService), nameof(Delete));
        //        QSettingDeleteResponse response = new QSettingDeleteResponse();
        //        //TODO: Need to fix the type definition.
        //        //response.Request = initRequest;
        //        response.ResponseCode = 100;
        //        return response;
        //    }
        //}

        private void NotifyMessageToObservers(QSetting qSetting)
        {
            //initRequest.Model.State = "Idenitfying Network Baud Rate...";
            //foreach (var observer in observers)
            //    observer.OnNext(qSetting);
        }

        private void NotifyErrorMessageToObservers(Exception exception)
        {
            //foreach (var observer in observers)
            //    observer.OnError(exception);
        }

        internal void NotifyCompleteMessageToObservers()
        {
            //foreach (var observer in observers)
            //    observer.OnCompleted();
        }
    }

    internal class Unsubscriber : IDisposable
    {
        private List<IObserver<QSetting>> _observers;
        private IObserver<QSetting> _observer;

        public Unsubscriber(List<IObserver<QSetting>> observers, IObserver<QSetting> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observer != null && _observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }

    internal static class QSettingsHelper
    {
        public static QSetting GetQSetting(string queuePath)
        {
            var result = new QSetting
            {
                QueuePath = new QPath
                {
                    ServerPath = queuePath
                }
            };
            result.Refresh();
            return result;
        }

        public static void Refresh(this QSetting qSetting)
        {
            if (qSetting == null) throw new ArgumentNullException(nameof(qSetting));
            var service = new QSettingService();
            QSettingUpdateRequest updateRequest = null;
            IList<QSetting> updatedQSettings = null;
            VBotLogger.Trace(() => $"[{nameof(QSettingsHelper)}->{nameof(Refresh)}] This refresh is manual triggered.");
            var task = Task.Run(() =>
            {
                updateRequest = new QSettingUpdateRequest();
                updateRequest.Model = new QSetting();
                updateRequest.Model.QueuePath = new QPath();
                updateRequest.Model.QueuePath.ServerPath = qSetting.QueuePath.ServerPath;

                //Call QsettingService Refresh method to update QSettting
                var response = service.Refresh(updateRequest);
                while (!response.IsCompleted)
                    System.Threading.Thread.Yield();
                updatedQSettings = response.Result.Model.ToList();
            });
            Task.WaitAll(task);

            var queueEntry = (from q in updatedQSettings
                              where q.QueuePath.ServerPath.Equals(updateRequest.Model.QueuePath.ServerPath)
                              select q).FirstOrDefault();
            qSetting.CopyFrom(queueEntry);
        }
    }

    internal static class QPathExtensions
    {
        public static void SetQueueName(this QPath qPath)
        {
            string directoryPath = Path.GetDirectoryName(qPath.ServerPath);
            qPath.QueueName = Path.GetFileNameWithoutExtension(directoryPath);
        }
    }

    internal static class QSettingFileAccessor
    {
        private static object qSettingJsonFileAccessLock = new object();
        private static string qSettingsFileName = Automation.CognitiveData.Properties.Resources.qSettingsJSONFileName;
        private static string filePath;

        public static void PerformOperation(Action<string> method)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                filePath = getJSONFileFullPath();
            }

            lock (qSettingJsonFileAccessLock)
            {
                method.Invoke(filePath);
            }
        }

        private static string getJSONFileFullPath()
        {
            IQBotDataPathProvider provider = new IQBotDataPathProvider();
            return Path.Combine(provider.GetPath(), qSettingsFileName);
        }
    }
}