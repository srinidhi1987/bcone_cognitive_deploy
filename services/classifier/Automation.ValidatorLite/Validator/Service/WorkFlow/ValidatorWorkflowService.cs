/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Service.WorkFlow
{
    using Automation.CognitiveData.Analytics;
    using Automation.CognitiveData.Validator.Model;
    using Automation.CognitiveData.VisionBot;
    using Automation.Generic.Compression;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.VisionBotEngine.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using UI.Service;
    using VisionBotEngine;
    using VisionBotEngine.Generic;

    public class ValidatorWorkflowService : IWorkflowService
    {
        public static List<IObserver<FailedVBotDocument>> observers;

        public ValidatorWorkflowService()
        {
            observers = new List<IObserver<FailedVBotDocument>>(); // Bring it from registry of UI
            Setup();
        }

        public IDisposable Subscribe(IObserver<FailedVBotDocument> observer)
        {
            //Return IDisposable unscriber back to the Service.
            //Service allows
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new ValidatorUnsubscriber(observers, observer);
        }

        public IServiceResponse<FailedVBotDocument> Init(IServiceRequest<FailedVBotDocument> initRequest)
        {
            throw new NotImplementedException();
        }

        public IServiceResponse<FailedVBotDocument> Close(IServiceRequest<FailedVBotDocument> closeRequest)
        {
            if (closeRequest == null)
                return null;

            //Is user just toying with next button?
            string failedFile = closeRequest.Model.FailedDocument != null
                ? closeRequest.Model.FailedDocument.DocumentProperties.Path
                : string.Empty;

            if (string.IsNullOrEmpty(failedFile))
            {
                var errorresponse = new CloseDocumentResponse()
                {
                    ErrorCode = 102 //File not found.
                };
                return errorresponse;
            }

            //UnLock existing failed document.
            //TODO: Move code to resilient state.

            var failedVBotDocumentLocalMachinePath = failedFile;
            var fileStream = closeRequest.Model.PackageFileStream;
            if (fileStream != null)
            {
                try
                {
                    fileStream.Unlock(0, 64);
                }
                catch (System.ObjectDisposedException ex)
                {
                    //This means fileStream is already unlocked.
                }
                catch (IOException e)
                {
                    var errorresponse = new CloseDocumentResponse()
                    {
                        ErrorCode = 103 //Exception in unlocking
                    };
                    return errorresponse;
                }
                finally
                {
                    fileStream.Close();
                    fileStream.Dispose();
                }
            }
            //Delete local file.
            if (File.Exists(failedVBotDocumentLocalMachinePath))
            {
                FileInfo file = new FileInfo(failedVBotDocumentLocalMachinePath);
                while (IsFileLocked(file))
                    Thread.Sleep(1000);
                File.Delete(failedVBotDocumentLocalMachinePath);
            }
            VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Close)}] filestream has been closed and deleted successfully.");
            var response = new CloseDocumentResponse()
            {
                ResponseCode = 200
            };
            return response;
        }

        private bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)) { };
            }
            catch (IOException)
            {
                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(IsFileLocked)}] file is locked by another process. vbotpfilename={file.Name}");
                return true;
            }

            return false;
        }

        public IServiceResponse<FailedVBotDocument> Prev(IServiceRequest<FailedVBotDocument> prevRequest)
        {
            //Logic is similar to Next.
            //Change is in the linQ.
            var qsetting = prevRequest.Model.Queue;
            //QSettingList qSettings = new QSettingList()
            //{
            //    qSettings = { prevRequest.Model.Queue }
            //};
            var sanityCheckResponse = DoSanityCheck(ref qsetting);
            if (sanityCheckResponse != null)
                return sanityCheckResponse;

            //Is user just toying with prev button?
            string failedFile = prevRequest.Model.FailedDocument != null
                ? Path.GetFileName(prevRequest.Model.FailedDocument.DocumentProperties.Path)
                : string.Empty;

            //UnLock existing failed document.
            //TODO: Move code to resilient state.
            if (!string.IsNullOrEmpty(failedFile))
            {
                var failedVBotDocumentLocalMachinePath = Path.Combine(prevRequest.Model.Queue.QueuePath.LocalMachinePath,
                    failedFile, Eyeball.FailedFileExtensionWithDot);
                var fileStream = prevRequest.Model.PackageFileStream;

                try
                {
                    fileStream.Unlock(0, 64);
                    if (File.Exists(failedVBotDocumentLocalMachinePath))
                        File.Delete(failedVBotDocumentLocalMachinePath);
                }
                catch (IOException e)
                {
                }
                finally
                {
                    fileStream.Close();
                }
            }
            //Get DirectoryInfo.
            //Get Prev Available File.
            var dirInfo = new DirectoryInfo(prevRequest.Model.Queue.QueuePath.ServerPath);
            IEnumerable<FileInfo> prevavailablefiles = new List<FileInfo>();
            if (prevRequest.Model.MetaData != null)
                prevavailablefiles = from file in dirInfo.EnumerateFiles($"*.{Eyeball.FailedFileExtension}")
                                     where prevRequest.Model.MetaData != null && file.CreationTime.Ticks < prevRequest.Model.MetaData.CreatedDateTimeAsTicks
                                     select file;
            if (prevRequest.Model.MetaData == null)
                prevavailablefiles = from file in dirInfo.EnumerateFiles($"*.{Eyeball.FailedFileExtension}")
                                     select file;
            //Lock file for user.
            FileInfo prevAvailableFileLockedByUser = null;
            foreach (var availablefile in prevavailablefiles)
            {
                FileStream fileStream = null;
                try
                {
                    fileStream = new FileStream(availablefile.FullName,
                    FileMode.Open,
                    FileAccess.ReadWrite,
                    FileShare.None);
                    fileStream.Lock(0, 64);
                    prevAvailableFileLockedByUser = availablefile;
                    prevRequest.Model.PackageFileStream = fileStream;
                    break;
                }
                catch (IOException e)
                {
                    if (fileStream != null)
                        fileStream.Dispose();
                    continue;
                }
            }

            //Get different components of the package file into object model.
            //Write them into package file as they arrive to local machine path.
            //TODO: Return the object to UI in observers.
            if (prevAvailableFileLockedByUser != null)
            {
                var validatorUtility = new WorkFlowServiceUtility();
                var validatorutilityEngine = new ValidatorEngine();

                VBotDocument failedVBotDocument = null;
                FailedVBotDocumentMetaData failedVBotDocumentMetaData = null;

                var localpkgpath = Path.Combine(qsetting.QueuePath.LocalMachinePath, prevAvailableFileLockedByUser.Name,
                    Eyeball.FailedFileExtensionWithDot);
                //Write a package file on localmachinepath
                using (var pkg = new Automation.Generic.Compression.ResourcePackage())
                {
                    //assuming .vbotp doesn't exists at all.
                    if (!File.Exists(localpkgpath))
                        pkg.Create(localpkgpath);
                    try
                    {
                        // serialize vbotdocument >> add to package as ".vbotdocument"
                        var data = new Automation.Generic.Compression.ResourceData
                        {
                            ContentType = @"text/json",
                            FilePath = "",
                            UniqueId = Eyeball.Parts.OriginalVBotDocument
                        };
                        string serializedData =
                        PackageHelper.GetFromPackage<string>(prevAvailableFileLockedByUser.FullName,
                            data.UniqueId,
                            (Stream contentStream, ResourcePackage resourcePackage) => contentStream.GetUTF8String());

                        data.Content = serializedData.GetMemoryStream();
                        pkg.AddResource(data);

                        failedVBotDocument = Newtonsoft.Json.JsonConvert.DeserializeObject<VBotDocument>(serializedData);

                        var metadataResource = new Automation.Generic.Compression.ResourceData
                        {
                            ContentType = @"text/json",
                            FilePath = "",
                            UniqueId = Eyeball.Parts.Metadata
                        };
                        //TODO: Fix Metadata information.
                        var metadata = validatorUtility.GetMetaData(prevAvailableFileLockedByUser.FullName);
                        var serializedmetadata =
                            Newtonsoft.Json.JsonConvert.SerializeObject(
                                metadata);

                        metadataResource.Content = serializedmetadata.GetMemoryStream();
                        pkg.AddResource(metadataResource);

                        failedVBotDocumentMetaData =
                            Newtonsoft.Json.JsonConvert.DeserializeObject<FailedVBotDocumentMetaData>(serializedmetadata);

                        var faileddocument = new Automation.Generic.Compression.ResourceData
                        {
                            FilePath =
                                validatorUtility.GetDocumentLocalMachinePath(prevAvailableFileLockedByUser.FullName),
                            UniqueId = prevAvailableFileLockedByUser.Name
                        };

                        pkg.AddResource(faileddocument);

                        failedVBotDocument.DocumentProperties.Path = faileddocument.FilePath;
                    }
                    finally
                    {
                        pkg.Close();
                    }
                }
                //TODO: Need to unit-test it before working on the object.
                //var Document = validatorUtility.GetDocumentLocalMachinePath(localpkgpath);
                var failedDocument = new FailedVBotDocument()
                {
                    FailedDocument = failedVBotDocument,
                    MetaData = failedVBotDocumentMetaData,
                    Queue = qsetting,
                    PackageFileStream = prevRequest.Model.PackageFileStream,
                    State = 101,
                    StateDescription = "Package Document downloaded."
                };
                var response = new PrevDocumentResponse();
                response.Model = failedDocument;
                response.Request = prevRequest;
                response.ResponseCode = 200;
                return response;
            }
            var noNextDocumentResponse = new PrevDocumentResponse()
            {
                ResponseCode = 102,
                ErrorCode = 103 //No files available.
            };
            return noNextDocumentResponse;
        }

        public IServiceResponse<FailedVBotDocument> Next(IServiceRequest<FailedVBotDocument> nextRequest)
        {
            //Get QSetting object from request.
            //Do sanity check of QSetting object.
            //Find next failed document available for eyeball.
            //Copy the available document to local machine path.
            //Create object and return back as response.
            var qsetting = nextRequest.Model.Queue;
            //QSettingList qSettings = new QSettingList()
            //{
            //    qSettings = { nextRequest.Model.Queue }
            //};
            var sanityCheckResponse = DoSanityCheck(ref qsetting);
            if (sanityCheckResponse != null)
                return sanityCheckResponse;

            this.Close(nextRequest);
            qsetting.Refresh();
            //Get DirectoryInfo.
            //Get Next Available File.
            if (!Directory.Exists(nextRequest.Model.Queue.QueuePath.ServerPath))
            {
                var noDocumentResponse = new NextDocumentResponse()
                {
                    ResponseCode = 102,
                    ErrorCode = 103 //No files available.
                };
                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] Directory does not exist. Qpath={nextRequest.Model.Queue.QueuePath.ServerPath}");
                return noDocumentResponse;
            }
            var dirInfo = new DirectoryInfo(nextRequest.Model.Queue.QueuePath.ServerPath);
            IEnumerable<FileInfo> nextavailablefiles = new List<FileInfo>();
            if (nextRequest.Model.MetaData != null)
                nextavailablefiles = from file in dirInfo.EnumerateFiles("*" + Eyeball.FailedFileExtensionWithDot)
                                     where nextRequest.Model.MetaData != null && file.CreationTime.Ticks > nextRequest.Model.MetaData.CreatedDateTimeAsTicks
                                     orderby file.CreationTime
                                     select file;
            if (nextRequest.Model.MetaData == null)
                nextavailablefiles = from file in dirInfo.EnumerateFiles("*" + Eyeball.FailedFileExtensionWithDot)
                                     orderby file.CreationTime
                                     select file;

            if (nextavailablefiles.Count() == 0)
                nextavailablefiles = from file in dirInfo.EnumerateFiles("*" + Eyeball.FailedFileExtensionWithDot)
                                     orderby file.CreationTime
                                     select file;
            //Lock file for user.
            FileInfo nextAvailableFileLockedByUser = null;
            long nextfileCreateDateAsTicks = 0;

            var availablefiles = nextavailablefiles.ToList();
            //foreach (var availablefile in nextavailablefiles)

            int fileIndex = 0;
            for (; fileIndex < nextavailablefiles.Count(); fileIndex++)
            {
                var availablefile = availablefiles[fileIndex];

                FileStream fileStream = null;

                try
                {
                    var doCorrectedFileExistsForGivenAvailableFile = File.Exists(Path.Combine(nextRequest.Model.Queue.QueuePath.ServerPath,
                    $"{Path.GetFileNameWithoutExtension(availablefile.FullName)}.{Eyeball.CorrectedFileExtension}"));
                    if (doCorrectedFileExistsForGivenAvailableFile)
                    {
                        VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] corrected file exist so skiped the file. vbotpfilename={availablefile.FullName}");
                        continue;
                    }
                    fileStream = new FileStream(availablefile.FullName,
                    FileMode.Open,
                    FileAccess.ReadWrite,
                    FileShare.None);
                    fileStream.Lock(0, 64);
                    VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] next file loaded is: {availablefile.FullName}");
                    nextAvailableFileLockedByUser = availablefile;
                    nextfileCreateDateAsTicks = availablefile.CreationTime.Ticks;
                    nextRequest.Model.PackageFileStream = fileStream;

                    //Get different components of the package file into object model.
                    //Write them into package file as they arrive to local machine path.
                    //TODO: Return the object to UI in observers.
                    if (nextAvailableFileLockedByUser != null)
                    {
                        var validatorUtility = new WorkFlowServiceUtility();
                        var validatorutilityEngine = new ValidatorEngine();

                        VBotDocument failedVBotDocument = null;
                        FailedVBotDocumentMetaData failedVBotDocumentMetaData = null;

                        //var localpkgpath = Path.Combine(Path.GetTempPath(), qsetting.QueuePath.LocalMachinePath, nextAvailableFileLockedByUser.Name);
                        //Write a package file on localmachinepath
                        using (var pkg = new Automation.Generic.Compression.ResourcePackage())
                        {
                            //assuming .vbotp doesn't exists at all.
                            //if (!File.Exists(localpkgpath))
                            //    pkg.Create(localpkgpath);
                            try
                            {
                                // serialize vbotdocument >> add to package as ".vbotdocument"
                                var data = new Automation.Generic.Compression.ResourceData
                                {
                                    ContentType = @"text/json",
                                    FilePath = "",
                                    UniqueId = Eyeball.Parts.OriginalVBotDocument
                                };
                                var serializedData =
                                    PackageHelper.GetFromPackage<string>(nextRequest.Model.PackageFileStream,
                                        data.UniqueId,
                                        (Stream contentStream, VResourcePackage resourcePackage) =>
                                        {
                                            if (contentStream == null)
                                            {
                                                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] Package content {Eyeball.Parts.OriginalVBotDocument} is empty. vbotpfilename={availablefile.FullName}");
                                                throw new ContentStreamNullException(Eyeball.Parts.OriginalVBotDocument);
                                            }
                                            return contentStream.GetUTF8String();
                                        });

                                data.Content = serializedData.GetMemoryStream();
                                //pkg.AddResource(data);

                                failedVBotDocument = Newtonsoft.Json.JsonConvert.DeserializeObject<VBotDocument>(serializedData);

                                var metadata = new Automation.Generic.Compression.ResourceData
                                {
                                    ContentType = @"text/json",
                                    FilePath = "",
                                    UniqueId = Eyeball.Parts.Metadata
                                };
                                var serializedmetadata =
                                    PackageHelper.GetFromPackage<string>(nextRequest.Model.PackageFileStream,
                                        metadata.UniqueId,
                                        (Stream contentStream, VResourcePackage resourcePackage) =>
                                        {
                                            if (contentStream == null)
                                            {
                                                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] Package content {Eyeball.Parts.Metadata} is empty. vbotpfilename={availablefile.FullName}");
                                                throw new ContentStreamNullException(Eyeball.Parts.Metadata);
                                            }
                                            return contentStream.GetUTF8String();
                                        });

                                metadata.Content = serializedmetadata.GetMemoryStream();
                                //pkg.AddResource(metadata);

                                failedVBotDocumentMetaData =
                                    Newtonsoft.Json.JsonConvert.DeserializeObject<FailedVBotDocumentMetaData>(serializedmetadata);
                                failedVBotDocumentMetaData.CreatedDateTimeAsTicks = nextfileCreateDateAsTicks;

                                var processedDocName = Path.GetFileName(failedVBotDocument.DocumentProperties.Path);

                                //var faileddocument = new Automation.Generic.Compression.ResourceData
                                //{
                                //    FilePath =
                                //        validatorUtility.GetDocumentLocalMachinePath(nextRequest.Model.PackageFileStream, processedDocName),
                                //    UniqueId = processedDocName
                                //};
                                //pkg.AddResource(faileddocument);

                                failedVBotDocument.DocumentProperties.Path = validatorUtility.GetDocumentLocalMachinePath(nextRequest.Model.PackageFileStream, processedDocName);
                            }
                            finally
                            {
                                pkg.Close();
                            }
                        }
                        //TODO: Need to unit-test it before working on the object.
                        //var Document = validatorUtility.GetDocumentLocalMachinePath(localpkgpath);
                        var failedDocument = new FailedVBotDocument()
                        {
                            FailedDocument = failedVBotDocument,
                            MetaData = failedVBotDocumentMetaData,
                            Queue = qsetting,
                            PackageFileStream = nextRequest.Model.PackageFileStream,
                            State = 101,
                            StateDescription = "Package Document downloaded."
                        };
                        VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Next)}] Package Document Loaded. vbotpfilename={availablefile.FullName}");
                        var response = new NextDocumentResponse()
                        {
                            Model = failedDocument,
                            Request = nextRequest,
                            ResponseCode = 200
                        };
                        return response;
                    }
                    break;
                }
                catch (ContentStreamNullException ex)
                {
                    if (fileStream != null)
                        fileStream.Dispose();

                    availablefiles.Remove(availablefile);
                    fileIndex--;

                    ex.Log(nameof(ValidatorWorkflowService), nameof(Next), "[ValidatorWorkflowService->Next] [InvalidFileFound] <File> " + nextAvailableFileLockedByUser.FullName + " <missing resource> " + ex.StreamName);
                    try
                    {
                        //rename incorrect file
                        var renameTo = $"{nextAvailableFileLockedByUser.FullName}.{ex.StreamName}.incorrect";
                        VBotLogger.Warning(() => $"[ValidatorWorkflowService->Next] [InvalidFileRename] <File> \"{nextAvailableFileLockedByUser.FullName}\" <rename to> \"{renameTo}\"");

                        nextAvailableFileLockedByUser.MoveTo(renameTo);
                    }
                    catch (Exception exception)
                    {
                        exception.Log(nameof(ValidatorWorkflowService), nameof(Next), "Caught within ContentStreamNullException");
                    }

                    continue;
                }
                catch (FileFormatException)
                {
                    if (fileStream != null)
                        fileStream.Dispose();

                    availablefiles.Remove(availablefile);
                    fileIndex--;

                    VBotLogger.Warning(() => $"[ValidatorWorkflowService->Next] [InvalidFileFound] <File> \"{nextAvailableFileLockedByUser.FullName}\" <not a package file>");
                    try
                    {
                        //rename incorrect file
                        var renameTo = $"{nextAvailableFileLockedByUser.FullName}.notpkg.incorrect";
                        VBotLogger.Warning(() => $"[ValidatorWorkflowService->Next] [InvalidFileRename] <File> \"{nextAvailableFileLockedByUser.FullName}\" <rename to> \"{renameTo}\"");

                        nextAvailableFileLockedByUser.MoveTo(renameTo);
                    }
                    catch (Exception exception)
                    {
                        exception.Log(nameof(ValidatorWorkflowService), nameof(Next), "Caught within FileFormatException");
                    }

                    continue;
                }
                catch (IOException e)
                {
                    if (fileStream != null)
                        fileStream.Dispose();
                    continue;
                }
            }
            var noNextDocumentResponse = new NextDocumentResponse()
            {
                ResponseCode = 102,
                ErrorCode = 103 //No files available.
            };
            return noNextDocumentResponse;
        }

        public IServiceResponse<FailedVBotDocument> Save(IServiceRequest<FailedVBotDocument> saveRequest)
        {
            //return new Task<IServiceResponse<FailedVBotDocument>>(() =>
            //{
            //Get QSetting object from request.
            var qSetting = saveRequest.Model.Queue;

            //Do sanity check of QSetting object.
            //DoSanityCheck(qSetting);

            //Create package file and save on local machine path.

            //Add correct json in existing package file on network.
            var failedPackageFilePath = Path.Combine(qSetting.QueuePath.ServerPath,
                $"{Path.GetFileName(saveRequest.Model.PackageFileStream.Name)}");
            if (!File.Exists(failedPackageFilePath))
            {
                VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}] file does not exist. vbotpfilename={failedPackageFilePath}");
                throw new FileNotFoundException("file not found", failedPackageFilePath);
            }

            VBotDocument originalVbotDocument = null;

            var pkg = new VResourcePackage();
            pkg.Open(saveRequest.Model.PackageFileStream);
            try
            {
                var data = new JsonResourceData<VBotDocument>(Eyeball.Parts.HumanCorrectedVBotDocument);

                /* if .hcorrected already exists, delete it and then add as per latest data */
                var existingDataIfExists = pkg.GetResource(Eyeball.Parts.HumanCorrectedVBotDocument);
                if (existingDataIfExists != null && existingDataIfExists.Content != null)
                {
                    pkg.DeleteFile(Eyeball.Parts.HumanCorrectedVBotDocument);
                }

                data.SetContent(saveRequest.Model.CorrectedDocument);
                pkg.AddResource(data);

                var originalContent = new JsonResourceData<VBotDocument>(pkg.GetResource(Eyeball.Parts.OriginalVBotDocument));
                originalVbotDocument = originalContent.GetContent();

                //Update status information in metadata.
                saveRequest.Model.MetaData.Status = FailedVBotDocumentStatus.Corrected;
                pkg.DeleteFile(Eyeball.Parts.Metadata);
                var metadataResource = new JsonResourceData<FailedVBotDocumentMetaData>(Eyeball.Parts.Metadata);
                metadataResource.SetContent(saveRequest.Model.MetaData);
                pkg.AddResource(metadataResource);

                FailedVbotAuditLogHelper.AddAuditLog(
                    pkg,
                    FailedVbotAuditLogHelper.CreateAuditLogMessage(
                        Automation.Common.Routines.GetClientConfiguration().Username,
                        "corrected"));

                //Save csv to SuccessQ
                var successQFolderPath = Path.GetDirectoryName(qSetting.QueuePath.ServerPath);
                var csvFilePath = Path.Combine(successQFolderPath,
                   $"{Path.GetFileNameWithoutExtension(saveRequest.Model.MetaData.OutputFilePath)}.csv");

                var processedDocumentPath = originalVbotDocument != null
                    ? originalVbotDocument.DocumentProperties.Path
                    : saveRequest.Model.CorrectedDocument.DocumentProperties.Path;

                var exportConfiguration = new ExportConfiguration
                {
                    ExportFilePath = csvFilePath,
                    IsValidatorEnabled = false,
                    ProcessedDocumnetPath = processedDocumentPath,
                    TaskName = saveRequest.Model.MetaData.TaskName
                };

                var exportUtility = new ExportDataUtility();
                exportUtility.ExportData(saveRequest.Model.CorrectedDocument, exportConfiguration);

                //Send Qsetting object, failed vbot document object in response to UI.
                //Rename package file on network.

                var correctedPackageFilePath = Path.Combine(qSetting.QueuePath.ServerPath,
                    $"{Path.GetFileNameWithoutExtension(saveRequest.Model.PackageFileStream.Name)}.{Eyeball.CorrectedFileExtension}");

                //Create Corrected Package File.
                using (var correctedPkg = new VResourcePackage())
                {
                    correctedPkg.CreateNew(correctedPackageFilePath);
                    //Unlock vbotp file stream.
                    foreach (var resources in pkg.GetAllResources())
                        correctedPkg.AddResource(resources);
                }
                saveRequest.Model.PackageFileStream.Unlock(0, 64);
            }
            finally
            {
                pkg.Close();
                saveRequest.Model.PackageFileStream.Dispose();
                saveRequest.Model.PackageFileStream = null;
                File.Delete(failedPackageFilePath);
            }
            VBotLogger.Trace(() => $"[{nameof(ValidatorWorkflowService)}->{nameof(Save)}] file has been saved successfully. vbotpfilename={failedPackageFilePath}");
            return new SaveDocumentResponse
            {
                Model = saveRequest.Model,
                Request = saveRequest,
                ResponseCode = 1, //TODO: decide ResponseCode
                ErrorCode = -1 //TODO: decide ErrorCode
            };
            //});
        }

        protected virtual void Setup()
        {
            //TODO: Why if logic inside constructor?
            //It should be strategy driven.
            //Update: logic moved to lazy-loading property
            //if (string.IsNullOrEmpty(initPathOfQSettings))
            //{
            //    var provider = new IQBotDataPathProvider();
            //    initPathOfQSettings = provider.GetPath();
            //}
        }

        private void LongRunningTask(FailedVBotDocument failedVBotDocument)
        {
            Thread.Sleep(500);
            //TODO: Check Permissions
            failedVBotDocument.State = Convert.ToInt32(QSettingsState.ReadPermissionCheck);
            failedVBotDocument.StateDescription = string.Format("Read Permission Check:{0}", failedVBotDocument.Queue.QueuePath.ServerPath);
            NotifyMessageToObservers(failedVBotDocument);

            Thread.Sleep(500);
            failedVBotDocument.State = Convert.ToInt32(QSettingsState.WritePermissionCheck);
            failedVBotDocument.StateDescription = string.Format("Write Permission Check: {0}", failedVBotDocument.Queue.QueuePath.ServerPath);
            NotifyMessageToObservers(failedVBotDocument);

            Thread.Sleep(500);
            failedVBotDocument.State = Convert.ToInt32(QSettingsState.DeletePermissionCheck);
            failedVBotDocument.StateDescription = string.Format("Delete Permission Check: {0}", failedVBotDocument.Queue.QueuePath.ServerPath);
            NotifyMessageToObservers(failedVBotDocument);

            //TODO: Network & IO Baud Rate.
            Thread.Sleep(500);
            failedVBotDocument.State = Convert.ToInt32(QSettingsState.NetworkCheck);
            failedVBotDocument.StateDescription = string.Format("Network Baud Rate: {0}", failedVBotDocument.Queue.QueuePath.ServerPath);
            NotifyMessageToObservers(failedVBotDocument);

            failedVBotDocument.State = Convert.ToInt32(QSettingsState.Complete);
            failedVBotDocument.StateDescription = string.Format("Found:{0}", failedVBotDocument.Queue.QueuePath.ServerPath);
            NotifyMessageToObservers(failedVBotDocument);

            NotifyCompleteMessageToObservers();
        }

        private void NotifyMessageToObservers(FailedVBotDocument qSetting)
        {
            //initRequest.Model.State = "Idenitfying Network Baud Rate...";
            foreach (var observer in observers)
                observer.OnNext(qSetting);
        }

        private void NotifyErrorMessageToObservers(Exception exception)
        {
            foreach (var observer in observers)
                observer.OnError(exception);
        }

        private void NotifyCompleteMessageToObservers()
        {
            foreach (var observer in observers)
                observer.OnCompleted();
        }

        private IServiceResponse<FailedVBotDocument> DoSanityCheck(ref QSetting qsetting)
        {
            QSettingList qSettingList = null;
            //Call long running task to get QSettingPermission Object filled up.
            QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
            {
                FileInfo fileInfo = new FileInfo(JSONFileFullPath);

                using (var fileStream = fileInfo.OpenText())
                {
                    string qSettingAsJson = fileStream.ReadToEnd();
                    qSettingList = JsonConvert.DeserializeObject<QSettingList>(qSettingAsJson);
                }
            });

            string qpath = qsetting.QueuePath.ServerPath; //CHANGED
            var queuedoesnotexists = (from q in qSettingList.qSettings
                                      where q.QueuePath.ServerPath.Equals(qpath) //CHANGED //where q.QueuePath.ServerPath.Equals(qsetting.QueuePath.ServerPath)
                                      select q).FirstOrDefault() == null;

            //TODO: Return
            if (queuedoesnotexists)
            {
                var response = new NextDocumentResponse
                {
                    Model = new FailedVBotDocument { Queue = qsetting },
                    ResponseCode = 101,
                    ErrorCode = 100
                };
                //Server Path does not exists in QueueSetting.json file.
                return response;
            }

            var queueObj = (from q in qSettingList.qSettings
                            where q.QueuePath.ServerPath.Equals(qpath) //CHANGED //where q.QueuePath.ServerPath.Equals(qsetting.QueuePath.ServerPath)
                            select q).FirstOrDefault();

            qsetting = queueObj;

            //qsetting.QueuePath.LocalMachinePath = Guid.NewGuid().ToString();

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            QSettingFileAccessor.PerformOperation((JSONFileFullPath) =>
            {
                using (StreamWriter sw = new StreamWriter(JSONFileFullPath))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    serializer.Serialize(jw, qSettingList);
                }
            });

            //var dirInfo = new DirectoryInfo(Path.Combine(Path.GetTempPath(), qsetting.QueuePath.LocalMachinePath));
            //if (!dirInfo.Exists)
            //    dirInfo.Create();

            return null;
        }
    }

    internal class FailedVbotAuditLogHelper
    {
        public static void AddAuditLog(VResourcePackage pkg, string msg)
        {
            if (pkg == null) return;

            var rsAuditLog = pkg.GetResource(Eyeball.Parts.AuditLog);
            if (rsAuditLog != null && rsAuditLog.Content != null)
            {
                var existingMsgs = rsAuditLog.Content.GetUTF8String();
                msg = existingMsgs + Environment.NewLine + msg;

                pkg.DeleteFile(Eyeball.Parts.AuditLog);

                //var msgBytes = Encoding.UTF8.GetBytes(msg);
                //rsAuditLog.Content.Write(msgBytes, 0, msgBytes.Length);
                //rsAuditLog.Content.Flush();
            }

            pkg.AddResource(new ResourceData
            {
                UniqueId = Eyeball.Parts.AuditLog,
                ContentType = "text/plain",
                Content = msg.GetMemoryStream()
            });
        }

        public static string CreateAuditLogMessage(string user, string whatWasDone)
        {
            var now = DateTime.UtcNow;
            return $"[{now.ToShortDateString()} {now.ToLongTimeString()}] [User: {user}] {whatWasDone}";
        }
    }

    internal class ValidatorUnsubscriber : IDisposable
    {
        private readonly IObserver<FailedVBotDocument> _observer;
        private readonly List<IObserver<FailedVBotDocument>> _observers;

        public ValidatorUnsubscriber(List<IObserver<FailedVBotDocument>> observers, IObserver<FailedVBotDocument> observer)
        {
            _observers = observers;
            _observer = observer;
        }

        public void Dispose()
        {
            if (_observer != null && _observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }

    internal class WorkFlowServiceUtility
    {
        private readonly ValidatorEngine validatorEngine;

        public WorkFlowServiceUtility()
        {
            validatorEngine = new ValidatorEngine();
        }

        public VBotDocument GetVBotDocument(string filePath)
        {
            return validatorEngine.GetVbotDocument(filePath);
        }

        public FailedVBotDocumentMetaData GetMetaData(string filePath)
        {
            return validatorEngine.GetMetadata(filePath);
        }

        public string GetDocumentLocalMachinePath(string filePath)
        {
            return validatorEngine.GetDocument(filePath);
        }

        public string GetDocumentLocalMachinePath(Stream fileStream, string filePath)
        {
            return validatorEngine.GetDocument(fileStream, filePath);
        }
    }
}