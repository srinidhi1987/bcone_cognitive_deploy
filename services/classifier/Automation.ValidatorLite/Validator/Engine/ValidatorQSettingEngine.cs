﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Engine
{
    using Automation.CognitiveData.Validator.Service;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VisionBotEngine;

    public class ValidatorQSettingEngine
    {
        public static List<IObserver<QSetting>> observers;
        protected Service.IQSettingService qSettingService;

        public ValidatorQSettingEngine(IEnumerable<IObserver<QSetting>> listOfObservers)
        {
            //TODO: call service registry to get the instance of service.
            //TODO: call UI registry to get instance of UI Observer on the basis of the API & request type.

            SetupComponents(null, listOfObservers);
        }

        public IQSettingService GetQSettingService
        {
            get
            {
                return qSettingService;
            }
            private set { }
        }

        public virtual void SetupComponents(Service.IQSettingService qService = null, IEnumerable<IObserver<QSetting>> listOfObservers = null)
        {
            VBotLogger.Trace(() => string.Format("[ValidatorQSettingEngine -> SetupComponents]"));
            this.qSettingService = new Service.QSettingService(listOfObservers);
        }

        /// <summary>
        /// Initialize QSetting Engine.
        /// </summary>
        /// <param name="initRequest"></param>
        /// <returns></returns>
        //public async Task<QSettingInitResponse> Init(QSettingInitRequest initRequest)
        //{
        //    return await qSettingService.Init(initRequest) as QSettingInitResponse;
        //}

        //public async Task<QSettingAddResponse> Add(QSettingAddRequest addRequest)
        //{
        //    return await qSettingService.Add(addRequest) as QSettingAddResponse;
        //}

        //public async Task<QSettingDeleteResponse> Delete(QSettingDeleteRequest deleteRequest)
        //{
        //    return await qSettingService.Delete(deleteRequest) as QSettingDeleteResponse;
        //}
    }
}