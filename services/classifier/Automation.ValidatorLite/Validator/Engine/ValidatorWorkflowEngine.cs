﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.Engine
{
    using Automation.CognitiveData.Validator.Model;
    //using Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Service.WorkFlow;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ValidatorWorkflowEngine
    {
        //public static List<IObserver<FailedVBotDocument>> observers;
        protected Service.IWorkflowService workflowService;
        
        public ValidatorWorkflowEngine(QSetting qSetting)
        {
            SetupComponents(qSetting);
        }

        public virtual void SetupComponents(QSetting qSetting, Service.IQSettingService qService = null, IEnumerable<IObserver<QSetting>> listOfObservers = null)
        {
            this.workflowService = new ValidatorWorkflowService(qSetting);
            //if (listOfObservers != null)
            //{
            //    ValidatorQSettingEngine.observers = listOfObservers.ToList();

            //    foreach (var observer in observers)
            //        workflowService.Subscribe(observer);
            //}
        }

        public NextDocumentResponse Next(NextDocumentRequest nextDocumentRequest)
        {
            return workflowService.Next(nextDocumentRequest) as NextDocumentResponse;
        }

        public InvalidDocumentResponse Invalid(InvalidDocumentRequest invalidDocumentRequest)
        {
            InvalidDocumentResponse response = null;
            try
            {
                response = workflowService.Invalid(invalidDocumentRequest) as InvalidDocumentResponse;
                
            }
            catch (Exception ex)
            {
                AddAuditEntryForFailSave();
                throw ex;
            }
            AddAuditEntryForSuccessfulSave();
            return response;
        }

        public SaveDocumentResponse Save(SaveDocumentRequest saveDocumentRequest)
        {
            SaveDocumentResponse response = null;
            try
            {
                response = workflowService.Save(saveDocumentRequest) as SaveDocumentResponse;
            }
            catch (Exception ex)
            {
                AddAuditEntryForFailSave();
                throw ex;
            }
            AddAuditEntryForSuccessfulSave();
            return response;
        }

        public ServiceResponse<FailedVBotDocument> Close(ServiceRequest<FailedVBotDocument> closeRequest)
        {
            workflowService.Unlock(closeRequest);
            return workflowService.Close(closeRequest);
        }

        private void AddAuditEntryForSuccessfulSave()
        {
            
        }

        private void AddAuditEntryForFailSave()
        {
           
        }
    }
}