﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.Validator.UI.Contract
{
    internal interface IStatusBarMessage
    {
        string GetStringMessage();
    }
}
