﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.Validator.UI.Contract
{
    internal class ZoomSettings
    {
        public double ZoomPercentage { get; set; }
        public ViewModel.ZoomViewModel.ZoomModeEnum ZoomMode { get; set; }

        public ZoomSettings()
        {
            ZoomPercentage = 1;
            ZoomMode = ViewModel.ZoomViewModel.ZoomModeEnum.FitToWidth;
        }
        public ZoomSettings(double zoomPercentage, ViewModel.ZoomViewModel.ZoomModeEnum zoomMode)
        {
            ZoomPercentage = zoomPercentage;
            ZoomMode = zoomMode;
        }
    }
}
