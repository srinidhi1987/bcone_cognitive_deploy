﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.Control
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for Star.xaml
    /// </summary>
    public partial class Star : UserControl
    {
        #region Properties

        public double Radius
        {
            get { return (double)GetValue(RadiusProperty); }
            set { SetValue(RadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Radius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(double), typeof(Star), new PropertyMetadata(10.0, new PropertyChangedCallback(RadiusPropertyChangedCallback)));

        #endregion Properties

        #region ctor

        public Star()
        {
            InitializeComponent();
            CreateStar();
        }

        #endregion ctor

        #region Public Method(s)

        public void CreateStar()
        {
            StarPath.Data = CreatePathForStar();
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private static void RadiusPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Star starShape = d as Star;
            if (starShape != null)
            {
                starShape.CreateStar();
            }
        }

        private Geometry CreatePathForStar()
        {
            Path orangePath = new Path();

            string sData = string.Empty;
            Point centerPoint = new Point(Radius, Radius);

            for (float angle = 270; angle < 180 + 270; angle += 60)
            {
                Point startPoint = PointOnCircle(Radius, angle, centerPoint);
                Point endPoint = PointOnCircle(Radius, angle + 180, centerPoint);

                sData += $"M{startPoint.X.ToString(CultureInfo.InvariantCulture)},{startPoint.Y.ToString(CultureInfo.InvariantCulture)} L{endPoint.X.ToString(CultureInfo.InvariantCulture)},{endPoint.Y.ToString(CultureInfo.InvariantCulture)} ";
            }

            var converter = TypeDescriptor.GetConverter(typeof(Geometry));
            return (Geometry)converter.ConvertFrom(sData);
        }

        private Point PointOnCircle(double radius, double angleInDegrees, Point origin)
        {
            // Convert from degrees to radians via multiplication by PI/180
            double x = (double)(radius * Math.Cos(angleInDegrees * Math.PI / 180F)) + origin.X;
            double y = (double)(radius * Math.Sin(angleInDegrees * Math.PI / 180F)) + origin.Y;

            return new Point(x, y);
        }

        #endregion Private Method(s)
    }
}