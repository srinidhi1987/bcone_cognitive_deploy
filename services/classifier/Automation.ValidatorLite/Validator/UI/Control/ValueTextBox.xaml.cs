﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.Control
{
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using Automation.CognitiveData.VisionBot.UI;
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for ValueTextBox.xaml
    /// </summary>
    public partial class ValueTextBox : UserControl
    {
        private bool _isLoaded = false;

        #region ctor

        public ValueTextBox()
        {
            InitializeComponent();
            this.Loaded += ValueTextBox_Loaded;
        }

        #endregion ctor

        #region Private Method(s)/Event handlers

        private void ValueTextBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_isLoaded)
            {
                _isLoaded = true;
                ScrollViewer scrollViewer = ((DependencyObject)sender).FindAncestor<ScrollViewer>("DataScrollViewer");
                if (scrollViewer != null)
                {
                    scrollViewer.ScrollChanged += ScrollViewer_ScrollChanged;
                }
            }
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (ValueToolTip.IsOpen)
            {
                ValueToolTip.IsOpen = false;
            }
            var offset = RecommendationPopup.HorizontalOffset;
            RecommendationPopup.HorizontalOffset = offset + 1;
            RecommendationPopup.HorizontalOffset = offset;
        }

        private void FieldValue_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.DataContext is SingleFieldEditorViewModel)
            {
                (this.DataContext as SingleFieldEditorViewModel).IsSelected = true;
            }
            else if (this.DataContext is TableCellEditorViewModel)
            {
                (this.DataContext as TableCellEditorViewModel).IsSelected = true;
            }
        }

        private void FieldValue_GotFocus(object sender, RoutedEventArgs e)
        {
            if (this.DataContext is SingleFieldEditorViewModel)
            {
                (this.DataContext as SingleFieldEditorViewModel).IsSelected = true;
                if ((this.DataContext as SingleFieldEditorViewModel).isRecommendedOnce == false &&
                    (this.DataContext as SingleFieldEditorViewModel).Value != "" &&
                    (this.DataContext as SingleFieldEditorViewModel).Suggestion.Count > 0
                    )
                {
                    FieldName.Text = (this.DataContext as SingleFieldEditorViewModel).Name;
                    RecommendationPopup.IsOpen = true;
                }
            }
            else if (this.DataContext is TableCellEditorViewModel)
            {
                (this.DataContext as TableCellEditorViewModel).IsSelected = true;
                if ((this.DataContext as TableCellEditorViewModel).isRecommendedOnce == false &&
                    (this.DataContext as TableCellEditorViewModel).Value != "" &&
                    (this.DataContext as TableCellEditorViewModel).Suggestion.Count > 0
                    )
                {
                    FieldName.Text = (this.DataContext as TableCellEditorViewModel).Name;
                    RecommendationPopup.IsOpen = true;
                }
            }

            if (FieldValueTextBox.Text != null)
            {
                FieldValueTextBox.Select(FieldValueTextBox.Text.Length, 0);
                FieldValueTextBox.ScrollToEnd();
            }

            if (!ValueToolTip.IsOpen)
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => showTooltip()));
            }
        }

        private void FieldValue_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.DataContext is SingleFieldEditorViewModel)
            {
                RecommendationPopup.IsOpen = false;
                //(this.DataContext as SingleFieldEditorViewModel).isRecommendedOnce = true;
                (this.DataContext as SingleFieldEditorViewModel).IsSelected = false;
            }
            else if (this.DataContext is TableCellEditorViewModel)
            {
                RecommendationPopup.IsOpen = false;
                //(this.DataContext as TableCellEditorViewModel).isRecommendedOnce = true;
                (this.DataContext as TableCellEditorViewModel).IsSelected = false;
            }
            closeTooltip();
        }

        private void FieldValue_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            closeTooltip();
        }

        private void FieldValue_MouseLeave(object sender, MouseEventArgs e)
        {
            closeTooltip();
        }

        private void showTooltip()
        {
            if (!ValueToolTip.IsOpen)
            {
                ValueToolTip.PlacementTarget = FieldValueTextBox;
                ValueToolTip.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                ValueToolTip.IsOpen = true;
            }
        }

        private void closeTooltip()
        {
            ValueToolTip.IsOpen = false;
        }

        #endregion Private Method(s)/Event handlers

        private void recommendationValue_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.DataContext is SingleFieldEditorViewModel)
                (this.DataContext as SingleFieldEditorViewModel).Value = (sender as TextBlock).Text;
            if (this.DataContext is TableCellEditorViewModel)
                (this.DataContext as TableCellEditorViewModel).Value = (sender as TextBlock).Text;
            RecommendationPopup.IsOpen = false;
            TraversalRequest traversalRequest = new TraversalRequest(FocusNavigationDirection.Next);
            bool isNext = this.MoveFocus(traversalRequest);
        }
    }
}