﻿using Automation.Validator.Validator.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automation.Validator.Validator.UI.View
{
    /// <summary>
    /// Interaction logic for InvalidReasonWindow.xaml
    /// </summary>
    public partial class InvalidReasonWindow : Window
    {
        public InvalidReasonWindow()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as InvalidReasonWindowViewModel).IsCancelled = true;
            Close();
        }

        private void MarkInvalidButton_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as InvalidReasonWindowViewModel).IsCancelled = false;
            Close();
        }
    }
}
