﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using Automation.CognitiveData.Validator.UI.ViewModel;

    //using Automation.Generic.Process;
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;

    //using Util.Configuration;

    /// <summary>
    /// Interaction logic for ValidatorView.xaml
    /// </summary>
    public partial class ValidatorView : UserControl
    {
        #region ctor

        public ValidatorView()
        {
            this.DataContextChanged += ValidatorView_DataContextChanged;
            InitializeComponent();
        }

        #endregion ctor

        #region Public Method(s)

        public void ResetDocumentEditorGrid()
        {
            DocumentEditorView.ResetDocumentEditorGridColumns();
        }

        #endregion Public Method(s)

        #region Private Method(s) / Event Handler(s)

        private void ValidatorView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ValidatorViewModel oldVM = e.OldValue as ValidatorViewModel;
            ValidatorViewModel newVM = e.NewValue as ValidatorViewModel;
            if (oldVM != null)
            {
                oldVM.FocusRequested -= FocusRequestedHandler;
                oldVM.NetworkStatusChanged -= NetworkStatusChangedHandler;
            }

            if (newVM != null)
            {
                newVM.FocusRequested += FocusRequestedHandler;
                newVM.NetworkStatusChanged += NetworkStatusChangedHandler;
            }
        }

        private void NetworkStatusChangedHandler(object sender, EventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()), DispatcherPriority.Background);
        }

        private void FocusRequestedHandler(object sender, EventArgs e)
        {
            DocumentEditorView.RequestFocusForDataEditor();
        }

        #endregion Private Method(s) / Event Handler(s)

        //private void Hyperlink_Click(object sender, RoutedEventArgs e)
        //{
        //    IProcessInvoker processInvoker = new ProcessInvoker();
        //    ProcessStartInfo processStartInfo = new ProcessStartInfo();
        //    processStartInfo.FileName = WebLinksConfiguration.GetLinkURL(
        //        WebLinksConfiguration.ComponentName.Client,
        //        WebLinksConfiguration.LinkName.SMART_Technology);
        //    processInvoker.Invoke(processStartInfo);
        //}
    }
}