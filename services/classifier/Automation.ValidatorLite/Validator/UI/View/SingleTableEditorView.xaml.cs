﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using Automation.CognitiveData.VisionBot.UI;
    using Automation.CognitiveData.VisionBot.UI.Behaviors;
    using Automation.CognitiveData.VisionBot.UI.Extensions;
    using System;
    using System.Data;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;
    using VisionBot.UI.EventArguments;
    using VisionBotEngine;

    /// <summary>
    /// Interaction logic for SingleTableEditorView.xaml
    /// </summary>
    public partial class SingleTableEditorView : UserControl
    {
        private DataGridDragDropBehavior<DataRowView> _fieldGridDragDropBehavior = null;
        private Point? _startPoint;
        private DataGridRow _lastrow = null;

        #region ctor

        public SingleTableEditorView()
        {
            InitializeComponent();
            attachDragDropBehaviors();
        }

        #endregion ctor

        #region Private Method(s) / Event Handler(s)

        private void attachDragDropBehaviors()
        {
            _fieldGridDragDropBehavior = new DataGridDragDropBehavior<DataRowView>();
            _fieldGridDragDropBehavior.Attach(TableEditorDataGrid);
            _fieldGridDragDropBehavior.Drop += FieldGridDragDropBehavior_Drop;
        }

        private void FieldGridDragDropBehavior_Drop(object sender, DropArgs e)
        {
            try
            {
                int fromIndex = int.Parse((e.DragEventArgs.Data.GetData(typeof(DragData)) as DragData).Id);
                int toIndex = e.TargetIndex;
                (this.DataContext as SingleTableEditorViewModel).MoveItem(fromIndex, toIndex);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(SingleTableEditorView), nameof(FieldGridDragDropBehavior_Drop));
            }
        }

        private void TableEditorDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid != null && !string.IsNullOrWhiteSpace(e.PropertyName))
            {
                if (e.PropertyName == "draghandle")
                {
                    e.Column = new DataGridTemplateColumn
                    {
                        Header = new TableHeaderVM() { IsRequired = false },
                        CellTemplate = this.FindResource("DragHandleDataTemplate") as DataTemplate
                    };
                }
                else
                {
                    e.Column = CreateDataGridTemplateColumn(e.PropertyName);
                }
            }
        }

        private DataTemplate CreateSimpleCellTemplate(string propertyName)
        {
            DataTemplate template = null;
            try
            {
                ParserContext context = new ParserContext();
                context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
                context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
                context.XmlnsDictionary.Add("local", "clr-namespace:Automation.CognitiveData.Validator.UI.View;assembly=Automation.Cognitive.Validator");

                //Here we have to create binding according to the column name of datatable, which is not fix(it is dynamic and data dependent) and can be known only at runtime.
                //So we have to create binding expression at runtime. That is why following DataTemplate is not written in XAML file and it is
                //created programatically at runtime.
                string _dataTemplateString = @"<DataTemplate>
                                                <local:TableCellEditorView DataContext=""{Binding " + propertyName + @"}""/>
                                           </DataTemplate>";

                using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(_dataTemplateString)))
                {
                    template = (DataTemplate)XamlReader.Load(memoryStream, context);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(SingleTableEditorView), nameof(CreateSimpleCellTemplate));
            }

            return template;
        }

        private DataGridTemplateColumn CreateDataGridTemplateColumn(string fieldDefID)
        {
            DataGridTemplateColumn newColumn = null;
            try
            {
                SingleTableEditorViewModel tableModel = this.DataContext as SingleTableEditorViewModel;
                VisionBotEngine.Model.FieldDef headerDef = tableModel?.GetHeader(fieldDefID);
                newColumn = new DataGridTemplateColumn
                {
                    Header = new TableHeaderVM() { Label = headerDef != null ? headerDef.Name : fieldDefID, IsRequired = headerDef != null ? headerDef.IsRequired : false }, // getCaption(previewTableModel, e.PropertyName),
                    CellTemplate = CreateSimpleCellTemplate(fieldDefID)
                };
            }
            catch (Exception ex)
            {
                ex.Log(nameof(SingleTableEditorView), nameof(CreateDataGridTemplateColumn));
            }

            return newColumn;
        }

        private void TableEditorDataGrid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = ((DependencyObject)sender).FindAncestor<ScrollViewer>("DataScrollViewer");

            if (scrollViewer != null)
            {
                scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta);
            }
        }

        private void Image_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(null);
            setCursorForImage(sender as Image, Cursors.SizeNS);
        }

        private void Image_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (_startPoint.HasValue && e.LeftButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(null);

                if (Math.Abs(position.X - _startPoint.Value.X) > 2 ||
                    Math.Abs(position.Y - _startPoint.Value.Y) > 2)
                {
                    _startPoint = null;
                    DataGrid parentDataGrid = (sender as DependencyObject) != null ? (sender as DependencyObject).GetParentOfType<DataGrid>() : null;
                    DataRowView fieldToDrag = TableEditorDataGrid.SelectedItem as DataRowView;
                    //if (fieldToDrag != null)
                    //{
                    string startindex = fieldToDrag.Row.Table.Rows.IndexOf(fieldToDrag.Row).ToString();
                    DragData dragData = new DragData { Id = startindex, Name = "Test" };
                    _fieldGridDragDropBehavior.StartDrag(dragData);
                    // }
                }
            }
        }

        private void Image_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            setCursorForImage(sender as Image, Cursors.Hand);
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {
            setCursorForImage(sender as Image, Cursors.Hand);
        }

        private void setCursorForImage(Image image, Cursor cursor)
        {
            if (image != null)
            {
                image.Cursor = cursor;
            }
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            var row = (sender as ContextMenu);
            if (row?.DataContext != null)
            {
                DataGridRow item = TableEditorDataGrid.ItemContainerGenerator.ContainerFromItem(row.DataContext) as DataGridRow;
                if (item != null)
                {
                    item.Background = Brushes.Transparent;
                }
            }
        }

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            if (_lastrow != null)
            {
                _lastrow.Background = Brushes.Transparent;
            }
            var row = (sender as ContextMenu);
            if (row?.DataContext != null)
            {
                DataGridRow item = TableEditorDataGrid.ItemContainerGenerator.ContainerFromItem(row.DataContext) as DataGridRow;
                if (item != null)
                {
                    item.Background = new SolidColorBrush(Color.FromArgb(255, 178, 230, 245));
                    _lastrow = item;
                }
            }
        }

        private void InsertRowAboveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SingleTableEditorViewModel tableViewModle = this.DataContext as SingleTableEditorViewModel;
            if (tableViewModle != null)
            {
                DataRowView rowView = (sender as MenuItem)?.DataContext as DataRowView;

                if (rowView?.Row != null)
                {
                    tableViewModle.InsertRowAbove.Execute(rowView.Row);
                }
            }
        }

        private void InsertRowBelowMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SingleTableEditorViewModel tableViewModle = this.DataContext as SingleTableEditorViewModel;
            if (tableViewModle != null)
            {
                DataRowView rowView = (sender as MenuItem)?.DataContext as DataRowView;
                if (rowView?.Row != null)
                {
                    tableViewModle.InsertRowBelow.Execute(rowView.Row);
                }
            }
        }

        private void DeleteRowMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SingleTableEditorViewModel tableViewModle = this.DataContext as SingleTableEditorViewModel;
            if (tableViewModle != null)
            {
                DataRowView rowView = (sender as MenuItem)?.DataContext as DataRowView;
                if (rowView?.Row != null)
                {
                    tableViewModle.DeleteRow.Execute(rowView.Row);
                }
            }
        }

        private void AddRowMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SingleTableEditorViewModel tableViewModle = this.DataContext as SingleTableEditorViewModel;
            if (tableViewModle != null)
            {
                tableViewModle.AddRow.Execute(null);
            }
        }

        #endregion Private Method(s) / Event Handler(s)
    }

    public class TableHeaderVM
    {
        public string Label { get; set; } = string.Empty;
        public bool IsRequired { get; set; } = false;
    }
}