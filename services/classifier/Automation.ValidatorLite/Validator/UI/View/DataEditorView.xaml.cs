﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for DataEditorView.xaml
    /// </summary>
    public partial class DataEditorView : UserControl
    {
        public DataEditorView()
        {
            InitializeComponent();
        }

        //This will set focus to first child control
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);

            if (e.OriginalSource == this)
            {
                TraversalRequest tRequest = new TraversalRequest(FocusNavigationDirection.Next);
                this.MoveFocus(tRequest);
            }
        }
    }
}