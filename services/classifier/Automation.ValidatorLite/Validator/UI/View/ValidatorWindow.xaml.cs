﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using Automation.CognitiveData.Validator.UI.Service;
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using Automation.CognitiveData.VisionBot.UI;
    using System;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Threading;
    using Validator.Service;
    using VisionBotEngine;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.Validator.Validator.UI.View;
    using Automation.Validator.Validator.UI.ViewModel;
    using System.IO;

    /// <summary>
    /// Interaction logic for VisionBotMainWindow.xaml
    /// </summary>
    public partial class ValidatorWindow : Window
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button

        private IntPtr _windowHandle;

        private ValidatorWindowViewModel _validatorWindowVM = null;
        private bool _loadedCalled = false;
        private bool _firstDocumentRequested = false;

        //  private bool _firstDocumentOpenRequest = false;
        private DispatcherTimer _timer;

        #region ctor

        internal ValidatorWindow(ValidatorWindowViewModel validatorWindowVM)
        {
            IInteractionService<ErrorModel> popupService = new PopupService();
            popupService.PopupRequested += PopupService_PopupRequested;

            IInteractionService<BusyIndicatorViewModel> busyIndicatorService = new BusyIndicatorService();
            busyIndicatorService.PopupRequested += BusyIndicatorService_PopupRequested;

            IInteractionService<InvalidReasonWindowViewModel> invaliidReasonService = new InvalidReasonService();
            invaliidReasonService.PopupRequested += InvalidReasonService_PopupRequested;

            validatorWindowVM.ValidatorVM.PopupService = popupService;
            validatorWindowVM.ValidatorVM.BusyIndicatorService = busyIndicatorService;
            validatorWindowVM.ValidatorVM.InvalidReasonsService = invaliidReasonService;

            _validatorWindowVM = validatorWindowVM;
            this.DataContext = _validatorWindowVM;
            InitializeComponent();

            SourceInitialized += MainWindow_SourceInitialized;
            QSettingService.OpenDocumentInValidatorRequest += QSettingService_OpenDocumentInValidatorRequest;

            _validatorWindowVM.CloseRequested += CloseWindow;
        }

        #endregion ctor

        #region Protected Method(s)

        protected void DisableMinimizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");

            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MINIMIZEBOX);
        }

        #endregion Protected Method(s)

        #region Private Method(s) / Event Handler(s)

        private void CloseWindow(Object sender, EventArgs e)
        {
            this.Close();
        }

        private void BusyIndicatorService_PopupRequested(object sender, InteractionArgs<BusyIndicatorViewModel> e)
        {
            BusyIndicatorView busyIndicatorView = new BusyIndicatorView(e.Context, e.Callback);
            busyIndicatorView.Owner = this;
            busyIndicatorView.ShowDialog();
        }

        private void PopupService_PopupRequested(object sender, InteractionArgs<VisionBot.UI.ErrorModel> e)
        {
            ErrorMessageDialog errorMsgDialog = new ErrorMessageDialog();
            errorMsgDialog.DataContext = e.Context;
            errorMsgDialog.Owner = this;
            errorMsgDialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            errorMsgDialog.ShowDialog();
        }

        private void InvalidReasonService_PopupRequested(object sender, InteractionArgs<InvalidReasonWindowViewModel> e)
        {
            InvalidReasonWindow invalidReasonDialog = new InvalidReasonWindow();
            invalidReasonDialog.DataContext = e.Context;
            invalidReasonDialog.Owner = this;
            invalidReasonDialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            invalidReasonDialog.ShowDialog();
        }

        private void ValidatorMainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MoveFocus(); //When closing window if cursor is inside some textbox then move focus so that textbox's binding can be updated if updatesourcetrigger is set as lostfocus.
            if (_validatorWindowVM.CloseWithoutMessage || _validatorWindowVM.ValidatorVM.CanCloseValidator())
            {
                QSettingService.OpenDocumentInValidatorRequest -= QSettingService_OpenDocumentInValidatorRequest;

                var directoryPath = Path.Combine(
                          Path.GetTempPath(), Cognitive.Validator.Properties.Resources.ValidatorTempDirectory);
                try
                {
                    if (Directory.Exists(directoryPath))
                    {
                        Directory.Delete(directoryPath, true);
                        VBotLogger.Trace(() => string.Format("[ValidatorMainWindow_Closing] [Delete temporary directory] [Success] [{0}]", directoryPath));
                    }
                }
                catch (Exception ex)
                {
                    VBotLogger.Warning(() => string.Format("[ValidatorMainWindow_Closing] [Delete temporary directory] [Fail] [{0}] [Exception -> {1}]", directoryPath, ex.Message));
                }
            }
            else
            {
                e.Cancel = true;
            }
            
        }

        private static void MoveFocus()
        {
            // Gets the element with keyboard focus.
            UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;
            // Change keyboard focus.
            if (elementWithFocus != null)
            {
                elementWithFocus.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }

        private void ValidatorMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_loadedCalled)
            {
                _loadedCalled = true;
                this.Loaded -= ValidatorMainWindow_Loaded;
                _timer = new DispatcherTimer(DispatcherPriority.Background, this.Dispatcher);
                _timer.Interval = new TimeSpan(0, 0, 0, 0, 5000);
                _timer.Tick += Timer_Tick;
                _timer.Start();
            }
        }

        private void QSettingService_OpenDocumentInValidatorRequest(QSetting obj)
        {
            if (_validatorWindowVM.ValidatorVM.GetQName() == obj.QueuePath.ServerPath &&
                !_firstDocumentRequested &&
                _validatorWindowVM.ValidatorVM.IsValidatorHasFileAccess()
                )
            {
                _firstDocumentRequested = true;
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                    new Action(() =>
                    {
                        VBotLogger.Trace(() => $"[{nameof(ValidatorWindow)}->{nameof(QSettingService_OpenDocumentInValidatorRequest)}] Autoload Document triggered. Qpath={obj.QueuePath.ServerPath}");
                        _validatorWindowVM.ValidatorVM.NextDocument();
                        _firstDocumentRequested = false;
                    }));
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();
            if (!_firstDocumentRequested)
            {
                _firstDocumentRequested = true;
                VBotLogger.Trace(() => $"[{nameof(ValidatorWindow)} -> {nameof(Timer_Tick)}]");
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    _validatorWindowVM.ValidatorVM.NextDocument();
                    _firstDocumentRequested = false;
                }));
            }
        }

        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;

            //disable minimize button
            DisableMinimizeButton();
        }

        private void ValidatorMainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ValidatorView.ResetDocumentEditorGrid();
        }

        #endregion Private Method(s) / Event Handler(s)
    }
}