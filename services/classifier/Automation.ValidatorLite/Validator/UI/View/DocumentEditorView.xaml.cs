﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for DocumentEditorView.xaml
    /// </summary>
    public partial class DocumentEditorView : UserControl
    {
        #region ctor

        public DocumentEditorView()
        {
            InitializeComponent();
        }

        #endregion ctor

        #region Public Method(s)

        public void RequestFocusForDataEditor()
        {
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => setFocus()));
        }

        public void ResetDocumentEditorGridColumns()
        {
            double width = this.ActualWidth;
            double dataEditorColumnWidth = width / 2.0;
            DocumentEditorGrid.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star); //new GridLength(dataEditorColumnWidth, GridUnitType.Pixel);
            DocumentEditorGrid.ColumnDefinitions[0].MinWidth = 420;
            DocumentEditorGrid.ColumnDefinitions[1].Width = new GridLength(7, GridUnitType.Pixel);
            DocumentEditorGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Star);
            DocumentEditorGrid.ColumnDefinitions[2].MinWidth = 420;
        }

        #endregion Public Method(s)

        #region Private Method(s) / Event Handler(s)

        private void setFocus()
        {
            DataEditor.Focus();
        }

        private void CurrentPageNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            textBox.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
        }

        private void CurrentPageNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int currentPage = -1;
                if (Int32.TryParse(CurrentPageNumber.Text, out currentPage))
                {
                    currentPage = currentPage - 1; //Adjust 1 based index to 0 based index
                    (this.DataContext as DocumentEditorViewModel).DocumentViewModel.GoToPage(currentPage);
                }

                TraversalRequest tRequest = new TraversalRequest(FocusNavigationDirection.Next);
                CurrentPageNumber.MoveFocus(tRequest);
            }
        }

        #endregion Private Method(s) / Event Handler(s)
    }
}