﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using VisionBotEngine;

    /// <summary>
    /// Interaction logic for DocumentView.xaml
    /// </summary>
    public partial class DocumentView : UserControl,IDisposable
    {
        #region ctor

        public DocumentView()
        {
            this.DataContextChanged += DocumentView_DataContextChanged;
            InitializeComponent();
        }
        ~ DocumentView()
        {
            this.Dispose();
        }
        #endregion ctor

        #region Private Method(s) / Event Handler(s)

        private void DocumentView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            DocumentViewModel oldVM = e.OldValue as DocumentViewModel;
            DocumentViewModel newVM = e.NewValue as DocumentViewModel;

            if (oldVM != null)
            {
                oldVM.PageChangeRequested -= PageChangeRequested;
                oldVM.ZoomPercentageChanged -= ZoomPercentageChangedHandler;
            }
            if (newVM != null)
            {
                newVM.PageChangeRequested += PageChangeRequested;
                newVM.ZoomPercentageChanged += ZoomPercentageChangedHandler;
                ResetDocumentScrollViewer();
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    newVM.ZoomViewModel.ActualWidth = DocumentScrollViewer.ActualWidth;
                    SetCurrentPage();
                }));
            }
        }

        private void ZoomPercentageChangedHandler(object sender, EventArgs e)
        {
            SetCurrentPage();
        }

        private void DocumentScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange != 0)
            {
                //e.VerticalOffset
                SetCurrentPage();
                CommandManager.InvalidateRequerySuggested();
            }
        }

        private void DocumentPages_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DocumentViewModel documentVM = this.DataContext as DocumentViewModel;
            if (documentVM != null)
            {
                documentVM.ZoomViewModel.ActualWidth = DocumentScrollViewer.ActualWidth;
                SetCurrentPage();
            }
        }

        private void SetCurrentPage()
        {
            DocumentViewModel documentVM = (this.DataContext as DocumentViewModel);
            if (documentVM != null)
            {
                double zoom = documentVM.ZoomViewModel.ZoomPercentage;
                documentVM.ViewRect = new Rect(DocumentScrollViewer.HorizontalOffset / zoom
                    , DocumentScrollViewer.VerticalOffset / zoom
                    , (DocumentScrollViewer.ActualWidth / zoom)
                    , (DocumentScrollViewer.ActualHeight / zoom));

                documentVM.SetCurrentPage(DocumentScrollViewer.VerticalOffset + (DocumentScrollViewer.ActualHeight / 2));
            }
        }

        private void PageChangeRequested(object sender, EventArgs e)
        {
            try
            {
                var ip = (ItemsControl)DocumentScrollViewer.Content;
                UIElement item = DocumentPages.ItemContainerGenerator.ContainerFromItem((this.DataContext as DocumentViewModel).CurrentPage) as UIElement;
                Point point = item.TranslatePoint(new Point(0, 0), ip);
                DocumentScrollViewer.ScrollToVerticalOffset(point.Y);
                DocumentScrollViewer.ScrollToHorizontalOffset(0);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DocumentView), nameof(PageChangeRequested));
            }
        }

        private void ResetDocumentScrollViewer()
        {
            ResetScrollViewer(DocumentScrollViewer);
        }

        private void ResetScrollViewer(ScrollViewer scrollViewer)
        {
            scrollViewer.ScrollToLeftEnd();
            scrollViewer.ScrollToTop();
        }

        public void Dispose()
        {
            this.DataContextChanged -= DocumentView_DataContextChanged;
            this.DocumentScrollViewer.ScrollChanged -= DocumentScrollViewer_ScrollChanged;
            this.DocumentScrollViewer.SizeChanged -= DocumentPages_SizeChanged;

        }

        #endregion Private Method(s) / Event Handler(s)
    }
}