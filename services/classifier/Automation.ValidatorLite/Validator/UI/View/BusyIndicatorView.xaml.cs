﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.View
{
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using System;
    using System.Windows;

    /// <summary>
    /// Interaction logic for ErrorMessageDialog.xaml
    /// </summary>
    public partial class BusyIndicatorView : Window
    {
        private BusyIndicatorViewModel _busyIndicatorVM = null;
        private Action<BusyIndicatorViewModel> _callback = null;

        #region ctro

        internal BusyIndicatorView(BusyIndicatorViewModel busyIndicatorVM, Action<BusyIndicatorViewModel> callback)
        {
            InitializeComponent();
            busyIndicatorVM.CloseRequested += BusyIndicatorVM_CloseRequested;
            _busyIndicatorVM = busyIndicatorVM;
            _callback = callback;
            this.DataContext = _busyIndicatorVM;
        }

        #endregion ctro

        #region Private Method(s) / Event Handler(s)

        private void BusyIndicatorVM_CloseRequested(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Left = Left - 10;
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => invokeCallback()));
        }

        private void invokeCallback()
        {
            if (_callback != null)
            {
                _callback(_busyIndicatorVM);
            }
        }

        #endregion Private Method(s) / Event Handler(s)
    }
}