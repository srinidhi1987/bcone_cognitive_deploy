﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.Service
{
    using Automation.CognitiveData.Validator.Service;
    using Contract;
    using Engine;
    using System;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using System.IO;
    using System.Runtime.ExceptionServices;

    //using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Interop;
    using View;
    using ViewModel;
    using VisionBotEngine;

    public class ValidatorUIService : IValidatorUIService
    {
        public void OpenValidator(QSetting qSetting)
        {
            try
            {
                if (qSetting == null)
                {
                    throw new ArgumentNullException("qSetting");
                }
                qSetting.OperationType = Convert.ToInt32(QSettingOperationType.Refresh);
                qSetting.Refresh();

                ValidatorWindowViewModel validatorWindowVM = ValidatorWindowViewModelFactory.CreateValidatorWindowViewModel(qSetting);
                ValidatorWindow validatorWindow = new ValidatorWindow(validatorWindowVM);
                ShowModalDialog(validatorWindow);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidatorUIService), nameof(OpenValidator));
            }
        }

        public void OpenValidator(string queuePath)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(queuePath))
                {
                    throw new ArgumentException("Path is required.", "queuePath");
                }
                QSetting qSetting = new QSetting();
                qSetting.QueuePath = new QPath();
                qSetting.QueuePermission = new QPermission();
                qSetting.QueuePath.ServerPath = queuePath;
                OpenValidator(qSetting);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidatorUIService), nameof(OpenValidator), " QueuePath: " + queuePath);
                //throw;
            }
        }

        [HandleProcessCorruptedStateExceptions]
        public void ShowModalDialog(ValidatorWindow wpfwindow)
        {
            try
            {
                const string AAEMainFormType = "AA.Main.frmMain";

                Form mainForm = null;
                foreach (Form form in System.Windows.Forms.Application.OpenForms)
                {
                    if (form.GetType().ToString().Equals(AAEMainFormType))
                    {
                        mainForm = form;
                        break;
                    }
                }

                if (mainForm == null)
                {
                    VBotLogger.Error(() =>
                    $"[ShowModalDialog] Main Form Type \"{AAEMainFormType}\" is not found. Now VisionBot Designer will not be assigned owner.");
                }
                else
                {
                    wpfwindow.Left = mainForm.Left + (mainForm.Width - ((int)wpfwindow.Width)) / 2;
                    wpfwindow.Top = mainForm.Top + (mainForm.Height - ((int)wpfwindow.Height)) / 2;

                    WindowInteropHelper helper = new WindowInteropHelper(wpfwindow);
                    helper.Owner = mainForm.Handle;
                }
                wpfwindow.ShowDialog();
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidatorUIService), nameof(ShowModalDialog));
            }
        }
    }

    internal static class ValidatorWindowViewModelFactory
    {
        public static ValidatorWindowViewModel CreateValidatorWindowViewModel(QSetting qSetting)
        {

            ValidatorWorkflowEngine engine = new ValidatorWorkflowEngine(qSetting);
            ValidatorSettings settings = new ValidatorSettings
            {
                ShowAllMappedFields = Configurations.Visionbot.ShowAllMappedFieldsInValidator,
                ZoomSettings = new ZoomSettings()
            };
            ValidatorWindowViewModel validatorWindowVM = new ValidatorWindowViewModel(qSetting, engine, settings);
            return validatorWindowVM;
        }
    }
}