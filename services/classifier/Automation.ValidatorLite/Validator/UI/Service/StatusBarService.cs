﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Automation.CognitiveData.Validator.UI.Contract;
using Automation.CognitiveData.Validator.UI.ViewModel;

namespace Automation.CognitiveData.Validator.UI.Service
{
    internal class StatusBarService : IStatusBarService
    {
        private StatusBarViewModel _statusbarVM = null;

        public void Init(StatusBarViewModel statusbarViewModel)
        {
            _statusbarVM = statusbarViewModel;
            
        }

        public void Clear()
        {
            _statusbarVM?.Clear();
        }

        public void ShowMessage<T>(T message) where T : IStatusBarMessage
        {
            _statusbarVM?.ShowMessage<T>(message);
        }
        public void ShowErrorCount(int errorCount)
        {
            _statusbarVM?.ShowErrorCount(errorCount);
        }

        public void ShowCorrectedCount(int correctedCount)
        {
            _statusbarVM?.ShowCorrectedCount(correctedCount);
        }
        public void SetUserName(string userName)
        {
            _statusbarVM?.SetUserName(userName);
        }
        public void SetIsOnline(bool userName)
        {
            _statusbarVM?.SetIsOnline(userName);
        }
    }
}
