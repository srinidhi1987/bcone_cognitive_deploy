﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.Service
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using Automation.CognitiveData.Validator.UI.ViewModel;
    using Automation.CognitiveData.VisionBot.UI;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;

    //TODO: When return page or doc properties clone it before returning

    internal class DocumentPageImageProvider : IDocumentPageImageProvider, IDisposable
    {
        private DocumentProperties _documentProperties;
        private IList<PagePropertiesViewModel> _pageProperties;
        private string _storagePath;
        private string _documentName;
        private ImageProcessingConfig _imageProcessingConfig;
        private object _lockObject = new object();
        private bool _isDisposeRequested;

        //TODO: Think about how to get imageProcessingConfig
        public DocumentPageImageProvider(DocumentProperties documentProperties, ImageProcessingConfig imageProcessingConfig)
        {
            _documentProperties = documentProperties;
            _storagePath = Path.Combine(Path.GetTempPath(), "ValidatorUI", Guid.NewGuid().ToString().Replace("-", String.Empty));
            _pageProperties = new List<PagePropertiesViewModel>();
            _documentName = Path.GetFileName(_documentProperties.Path);
            _imageProcessingConfig = imageProcessingConfig;
            createPageProperties();
        }

        //public BitmapImage GetPage(int index)
        //{
        //    BitmapImage image = null;

        //    try
        //    {
        //        if (!_isDisposeRequested)
        //        {
        //            createFolderIfNotPresent();
        //            PagePropertiesViewModel pagePropertiesVM = GetPageProperties(index);
        //            if (pagePropertiesVM != null)
        //            {
        //                VisionBotDesignerResourcesHelper resourceHelper = new VisionBotDesignerResourcesHelper();
        //                lock (_lockObject)
        //                {
        //                    if (!_isDisposeRequested)
        //                    {
        //                        image = resourceHelper.GetBitmapImage(_documentProperties, index, pagePropertiesVM.ImagePath, _imageProcessingConfig, false);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.Log(nameof(DocumentPageImageProvider), nameof(GetPage));
        //    }

        //    return image;
        //}

        public PagePropertiesViewModel GetPageProperties(int index)
        {
            PagePropertiesViewModel pagePropertiesVM = null;
            if (_pageProperties.Count > index)
            {
                pagePropertiesVM = _pageProperties[index];
            }
            return pagePropertiesVM;
        }

        public void Dispose()
        {
            _isDisposeRequested = true;
            try
            {
                lock (_lockObject)
                {
                    if (Directory.Exists(_storagePath))
                    {
                        Directory.Delete(_storagePath, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(DocumentPageImageProvider), nameof(Dispose));
            }
        }

        private void createPageProperties()
        {
            if (_documentProperties.PageProperties != null && _documentProperties.PageProperties.Count > 0)
            {
                IList<PageProperties> pagePropertiesSorted = _documentProperties.PageProperties.OrderBy(q => q.PageIndex).ToList();
                int previousBottom = 0;
                for (int i = 0; i < pagePropertiesSorted.Count; i++)
                {
                    PageProperties currentPageProperties = pagePropertiesSorted[i];
                    PagePropertiesViewModel pagePropertyVM = new PagePropertiesViewModel(currentPageProperties, Path.Combine(_storagePath, _documentName + "_Page" + currentPageProperties.PageIndex), previousBottom);
                    _pageProperties.Add(pagePropertyVM);
                    previousBottom += currentPageProperties.Height;
                }
            }
            else
            {
                PageProperties pageProperties = new PageProperties();
                pageProperties.PageIndex = 0;
                pageProperties.Height = _documentProperties.Height;
                pageProperties.Width = _documentProperties.Width;
                PagePropertiesViewModel pagePropertyVM = new PagePropertiesViewModel(pageProperties, Path.Combine(_storagePath, _documentName + "_Page" + pageProperties.PageIndex), 0);
                _pageProperties.Add(pagePropertyVM);
            }
        }

        private void createFolderIfNotPresent()
        {
            if (!Directory.Exists(_storagePath))
            {
                Directory.CreateDirectory(_storagePath);
            }
        }
    }
}