﻿using Automation.CognitiveData.Validator.UI.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.Validator.UI.Service
{
    internal class StringStatusBarMessage : IStatusBarMessage
    {
        private string _message;

        public string Message
        {
            get
            {
                return _message;
            }
        }

        public string GetStringMessage()
        {
            return Message;
        }

        public StringStatusBarMessage(string message)
        {
            _message = message;
        }
    }
}
