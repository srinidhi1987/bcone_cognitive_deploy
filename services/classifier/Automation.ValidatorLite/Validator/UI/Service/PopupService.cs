﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.Validator.UI.Contract;
using Automation.CognitiveData.Validator.UI.ViewModel;
using Automation.CognitiveData.VisionBot.UI;
using Automation.Validator.Validator.UI.ViewModel;

/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Automation.CognitiveData.Validator.UI.Service
{
    //internal class PopupService : IPopupService
    //{
    //    public event EventHandler<ErrorModel> PopupRequested;

    //    public ButtonModel ShowPopup(ErrorModel content)
    //    {
    //        if (PopupRequested != null)
    //        {
    //            PopupRequested(this, content);
    //        }

    //        return content.SelectedAction;
    //    }
    //}

    internal class PopupService : IInteractionService<ErrorModel>
    {
        public event EventHandler<InteractionArgs<ErrorModel>> PopupRequested;

        public void Raise(ErrorModel context)
        {
            if (PopupRequested != null)
            {
                PopupRequested(this, new InteractionArgs<ErrorModel>(context, null));
            }
        }

        public void Raise(ErrorModel context, Action<ErrorModel> callback)
        {
            throw new NotImplementedException();
        }
    }

    internal class BusyIndicatorService : IInteractionService<BusyIndicatorViewModel>
    {
        public event EventHandler<InteractionArgs<BusyIndicatorViewModel>> PopupRequested;

        public void Raise(BusyIndicatorViewModel context)
        {
            throw new NotImplementedException();
        }

        public void Raise(BusyIndicatorViewModel context, Action<BusyIndicatorViewModel> callback)
        {
            if (PopupRequested != null)
            {
                PopupRequested(this, new InteractionArgs<BusyIndicatorViewModel>(context, callback));
            }
        }
    }

    internal class InvalidReasonService : IInteractionService<InvalidReasonWindowViewModel>
    {
        public event EventHandler<InteractionArgs<InvalidReasonWindowViewModel>> PopupRequested;
              
        public void Raise(InvalidReasonWindowViewModel context)
        {
            throw new NotImplementedException();
        }
        

        public void Raise(InvalidReasonWindowViewModel context, Action<InvalidReasonWindowViewModel> callback)
        {
            if (PopupRequested != null)
            {
                PopupRequested(this, new InteractionArgs<InvalidReasonWindowViewModel>(context, callback));
                callback?.Invoke(context);
            }
        }

    }
}