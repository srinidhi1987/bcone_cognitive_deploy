﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI
{
    using Automation.Generic.Serialization;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Threading.Tasks;

    internal class Utility
    {
        public static T Clone<T>(T obj) where T : class
        {
            T clonedObj = null;
            //Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            ISerialization serialization = SerializationFactory.GetJSONSerializer();
            string serializedObj = serialization.SerializeToString<T>(obj);
            clonedObj = serialization.DeserializeFromString<T>(serializedObj);

            return clonedObj;
        }
    }
}