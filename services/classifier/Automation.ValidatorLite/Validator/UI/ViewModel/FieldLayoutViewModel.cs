﻿using Automation.CognitiveData.VisionBot.UI.ViewModels;
using Automation.VisionBotEngine.Model;

/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    internal class FieldLayoutViewModel : ViewModelBase
    {
        private FieldLayout _fieldLayout;

        private bool _isSelected = false;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected = !value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public Rectangle Bounds
        {
            get { return _fieldLayout.Bounds; }
            set
            {
                if (_fieldLayout.Bounds != value)
                {
                    _fieldLayout.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public FieldLayoutViewModel(FieldLayout fieldLayout)
        {
            _fieldLayout = fieldLayout;
        }
    }
}