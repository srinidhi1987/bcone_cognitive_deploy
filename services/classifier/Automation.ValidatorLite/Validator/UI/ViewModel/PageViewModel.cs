﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine.Model;
    using System;

    internal class PageViewModel : ViewModelBase,IDisposable
    {
        private IDocumentPageImageProvider _documentPageImageProvider;

        #region Properties

        private PageImageSource _page;

        public PageImageSource Page
        {
            get
            {
                return _page;
            }

            set
            {
                if (_page != value)
                {
                    _page = null;
                    _page = value;
                    OnPropertyChanged(nameof(Page));
                }
            }
        }

        private RegionsViewModel _regions;

        public RegionsViewModel RegionsViewModel
        {
            get
            {
                return _regions;
            }

            set
            {
                if (_regions != value)
                {
                    _regions = value;
                    OnPropertyChanged(nameof(RegionsViewModel));
                    //_regions.SelectionChanged += Regions_SelectionChanged;
                }
            }
        }

        private ZoomViewModel _zoomViewModel;

        public ZoomViewModel ZoomViewModel
        {
            get
            {
                return _zoomViewModel;
            }
        }

        #endregion Properties

        #region Events

        //public event EventHandler SelectionChanged;

        #endregion Events

        #region ctor

        public PageViewModel(PagePropertiesViewModel pagePropertiesViewModel, RegionsViewModel regionViewModel, ZoomViewModel zoomViewModel, IDocumentPageImageProvider documentPageImageProvider)
        {
            _page = new PageImageSource(pagePropertiesViewModel, documentPageImageProvider);
            _regions = regionViewModel;
            _zoomViewModel = zoomViewModel;
            _documentPageImageProvider = documentPageImageProvider;
        }
        ~PageViewModel()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            _page = null;
            _regions = null;
            _zoomViewModel = null;
            _documentPageImageProvider = null;
        }

        #endregion ctor

        #region Public Method(s)

        //public void LoadPageIfNotPresent()
        //{
        //    _page?.LoadPageIfNotPresent();
        //}

        #endregion Public Method(s)
    }

    public class PagePropertiesViewModel : ViewModelBase
    {
        #region Properties

        private PageProperties _pageProperties;

        public PageProperties PageProperties
        {
            get
            {
                return _pageProperties;
            }
        }

        private string _imagePath;

        public string ImagePath
        {
            get
            {
                return _imagePath;
            }
        }

        private int _top;

        public int Top
        {
            get
            {
                return _top;
            }
        }

        #endregion Properties

        #region ctor

        public PagePropertiesViewModel(PageProperties pageProperties, string path, int pageTop)
        {
            _pageProperties = pageProperties;
            _imagePath = path;
            _top = pageTop;
        }

        #endregion ctor
    }
}