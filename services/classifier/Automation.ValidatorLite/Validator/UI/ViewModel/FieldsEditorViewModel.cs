﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VisionBotEngine;

    internal class FieldsEditorViewModel : ValidatorViewModelBase,IDisposable
    {
        private Layout _layout;
        private IList<FieldData> _fieldDataList;

        #region Properties

        private ObservableCollection<SingleFieldEditorViewModel> _fields;

        public ObservableCollection<SingleFieldEditorViewModel> Fields
        {
            get
            {
                return _fields;
            }

            set
            {
                _fields = value;
            }
        }

        private int _optionalErrorCount;
        public int OptionalErrorCount
        {
            get { return _optionalErrorCount; }
            set
            {
                _optionalErrorCount = value;
            }
        }

        private int _errorCount;

        public int ErrorCount
        {
            get
            {
                return _errorCount;
            }

            set
            {
                if (_errorCount != value)
                {
                    _errorCount = value;
                    OnPropertyChanged(nameof(ErrorCount));
                }
            }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler SelectionChanged;

        public event EventHandler FieldValueChanged;

        #endregion Event(s)

        #region ctor

        public FieldsEditorViewModel(Layout layout, IList<FieldData> fieldDataList)
        {
            _layout = layout;
            _fieldDataList = fieldDataList;
            _fields = new ObservableCollection<SingleFieldEditorViewModel>();
            if (fieldDataList != null)
            {
                foreach (FieldData fieldData in fieldDataList)
                {
                    FieldLayout fieldLayout = (from q in layout.Fields where q.FieldId == fieldData.Field.Id select q).FirstOrDefault();
                    SingleFieldEditorViewModel singleFieldEditorViewModel = new SingleFieldEditorViewModel(fieldLayout, fieldData);
                    singleFieldEditorViewModel.SelectionChanged += FieldsEditorViewModel_SelectionChanged;
                    singleFieldEditorViewModel.ValueChanged += SingleFieldEditorViewModel_ValueChanged;
                    Fields.Add(singleFieldEditorViewModel);
                }
            }
        }
        public void Dispose()
        {
            _layout = null;
            _fieldDataList = null;
            _fields = null;
        }
        #endregion ctor

        #region Public Method(s)

        public void RefreshValidationIssue()
        {
            foreach (SingleFieldEditorViewModel singleField in Fields)
            {
                singleField.RefreshValidationIssue();
            }
        }

        public void UpdateValidationIssueCount()
        {
            int errorCount = 0;
            int optionalErrorCount = 0;
            foreach (SingleFieldEditorViewModel fieldVM in _fields)
            {
                if (fieldVM.ValidationIssue != null)
                {
                    errorCount++;

                    if (fieldVM.ValidationIssue.IssueCode.Equals(FieldDataValidationIssueCodes.ConfidenceIssue))
                    {
                        optionalErrorCount++;
                    }
                }
            }

            ErrorCount = errorCount;
            OptionalErrorCount = optionalErrorCount;
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void SingleFieldEditorViewModel_ValueChanged(object sender, EventArgs e)
        {
            OnValueChanged(sender, e);
        }

        private void FieldsEditorViewModel_SelectionChanged(object sender, EventArgs e)
        {
            OnSelectionChanged(sender, e);
        }

        private void OnSelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(sender, e);
            }
        }

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (FieldValueChanged != null)
            {
                FieldValueChanged(sender, e);
            }
        }

        

        #endregion Private Method(s)
    }
}