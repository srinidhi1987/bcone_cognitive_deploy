﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using System;

    //TODO: This viewmodel can be enhanced more accrding to the features we want on Busy Dialog.
    internal class BusyIndicatorViewModel : ValidatorViewModelBase
    {
        #region Propertie(s)

        private string _message;

        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                if (_message != value)
                {
                    _message = value;
                    OnPropertyChanged(nameof(Message));
                }
            }
        }

        #endregion Propertie(s)

        #region Event(s)

        public event EventHandler CloseRequested;

        #endregion Event(s)

        #region Public Method(s)

        public void OnCloseRequested()
        {
            if (CloseRequested != null)
            {
                CloseRequested(this, EventArgs.Empty);
            }
        }

        #endregion Public Method(s)
    }
}