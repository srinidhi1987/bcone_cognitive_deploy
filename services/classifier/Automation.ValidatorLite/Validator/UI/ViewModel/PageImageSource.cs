﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    //TODO: IDisposible
    internal class PageImageSource : ValidatorViewModelBase
    {
        private PagePropertiesViewModel _pagePropertiesViewModel;
        private IDocumentPageImageProvider _documentPageImageProvider;
        private bool _isLoadingPage = false;

        #region Properties

        public int Top
        {
            get
            {
                return _pagePropertiesViewModel.Top;
            }
        }

        public double Height
        {
            get
            {
                return _pagePropertiesViewModel.PageProperties.Height;
            }
        }

        public double Width
        {
            get
            {
                return _pagePropertiesViewModel.PageProperties.Width;
            }
        }

        private BitmapImage _pageImage;

        public BitmapImage PageImage
        {
            get
            {
                return _pageImage;
            }

            set
            {
                if (_pageImage != value)
                {
                    _pageImage = value;
                    OnPropertyChanged("PageImage");
                }
            }
        }

        private double _dpiX;

        public double DpiX
        {
            get
            {
                return _dpiX;
            }

            set
            {
                _dpiX = value;
                AdjustStretch();
            }
        }

        private double _dpiY;

        public double DpiY
        {
            get
            {
                return _dpiY;
            }

            set
            {
                _dpiY = value;
                AdjustStretch();
            }
        }

        private double _zoomHeight;

        public double ZoomHeight
        {
            get
            {
                return _zoomHeight;
            }

            set
            {
                if (_zoomHeight != value)
                {
                    _zoomHeight = value;
                    OnPropertyChanged("ZoomHeight");
                }
            }
        }

        private double _zoomWidth;

        public double ZoomWidth
        {
            get
            {
                return _zoomWidth;
            }

            set
            {
                if (_zoomWidth != value)
                {
                    _zoomWidth = value;
                    OnPropertyChanged("ZoomWidth");
                }
            }
        }

        private System.Windows.Media.Stretch _stretch = System.Windows.Media.Stretch.Fill;

        public Stretch Stretch
        {
            get
            {
                return _stretch;
            }

            set
            {
                if (_stretch != value)
                {
                    _stretch = value;
                    OnPropertyChanged("Stretch");
                }
            }
        }

        public string PageImagePath
        {
            get
            {
                return _pagePropertiesViewModel.ImagePath;
            }
        }

        public int PageIndex
        {
            get
            {
                return _pagePropertiesViewModel.PageProperties.PageIndex;
            }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler PageLoaded;

        #endregion Event(s)

        #region ctor

        public PageImageSource(PagePropertiesViewModel pageProperties, IDocumentPageImageProvider documentPageImageProvider)
        {
            _pagePropertiesViewModel = pageProperties;
            _documentPageImageProvider = documentPageImageProvider;
        }

        #endregion ctor

        #region Public Method(s)

        //public async void LoadPageIfNotPresent()
        //{
        //    if (_pageImage == null && !_isLoadingPage)
        //    {
        //        _isLoadingPage = true;
        //        try
        //        {
        //            BitmapImage pageImageToAssign = await GetPageImageAsync();
        //            PageImage = pageImageToAssign;
        //            OnPageLoaded();
        //        }
        //        finally
        //        {
        //            _isLoadingPage = false;
        //        }
        //    }
        //}

        //public void OnPageLoaded()
        //{
        //    if (PageLoaded != null)
        //    {
        //        PageLoaded(this, EventArgs.Empty);
        //    }
        //}

        #endregion Public Method(s)

        #region Internal Method(s)

        internal void SetZoomHeightWidth(double zoomPercentage)
        {
            //ZoomHeight = (Height * (DpiY / 96)) * zoomPercentage;
            //ZoomWidth = (Width * (DpiX / 96)) * zoomPercentage;
            ZoomHeight = (Height) * zoomPercentage;
            ZoomWidth = (Width) * zoomPercentage;
        }

        #endregion Internal Method(s)

        #region Private Method(s)

        private void AdjustStretch()
        {
            //When image is having different DPIX and DPIY it was not aligned with SIRs on UI.
            //So in that case Stretch should be Fill otherwise Stretch should be uniform.

            //TODO: Check whether Stretch.Fill can work for all scenarios. If so we can remove this method.
            Stretch stretchToApply = Stretch.Fill;
            if (Convert.ToInt32(_dpiX) == Convert.ToInt32(_dpiY))
            {
                stretchToApply = Stretch.Uniform;
            }

            Stretch = stretchToApply;
        }

        //private Task<BitmapImage> GetPageImageAsync()
        //{
        //    return Task.Run(() => _documentPageImageProvider.GetPage(PageIndex));
        //}

        #endregion Private Method(s)
    }
}