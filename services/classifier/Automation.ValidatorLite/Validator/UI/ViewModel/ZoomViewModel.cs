﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.VisionBot.UI;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Contract;
    using System;
    using System.Windows.Input;

    internal class ZoomViewModel : ViewModelBase
    {
        private double _maxZoomAllowed = 2;
        private double _step = 0.1;
        private double _documentWidth;
        private ZoomSettings _zoomSettings;
        #region Properties
        

        public double ZoomPercentage
        {
            get
            {
                return _zoomSettings.ZoomPercentage;
            }

            set
            {
                if (_zoomSettings.ZoomPercentage != value)
                {
                    _zoomSettings.ZoomPercentage = value;
                    OnPropertyChanged(nameof(ZoomPercentage));
                    OnZoomPercentageChanged(value);
                }
            }
        }

        private ICommand _zoomIn;

        public ICommand ZoomIn
        {
            get
            {
                return _zoomIn;
            }

            set
            {
                _zoomIn = value;
            }
        }

        private ICommand _zoomOut;

        public ICommand ZoomOut
        {
            get
            {
                return _zoomOut;
            }

            set
            {
                _zoomOut = value;
            }
        }

        private ICommand _fitToWidth;

        public ICommand FitToWidth
        {
            get
            {
                return _fitToWidth;
            }

            set
            {
                _fitToWidth = value;
            }
        }

        private double _actualWidth;

        public double ActualWidth
        {
            get
            {
                return _actualWidth;
            }

            set
            {
                _actualWidth = value;
                if (_zoomSettings.ZoomMode == ZoomModeEnum.FitToWidth)
                {
                    fitToWidthHandler(null);
                }
            }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler<ZoomPercentageChangedArgs> ZoomPercentageChanged;

        #endregion Event(s)

        #region ctor

        public ZoomViewModel(double step, double documentWidth, ZoomSettings zoomSettings)
        {
            _documentWidth = documentWidth;
            _step = step;
            if (zoomSettings != null)
            {
                _zoomSettings = zoomSettings;
            }
            _zoomIn = new RelayCommand(zoomInHandler);
            _zoomOut = new RelayCommand(zoomOutHandler);
            _fitToWidth = new RelayCommand(fitToWidthHandler);
        }

        #endregion ctor

        #region Private Method(s)

        private void zoomInHandler(object obj)
        {
            _zoomSettings.ZoomMode = ZoomModeEnum.Explicit;
            if (Math.Round(_zoomSettings.ZoomPercentage, 1) >= _maxZoomAllowed)
            {
                return;
            }

            double nextZoom = ZoomPercentage + _step;
            if (ZoomPercentage < 1 && nextZoom > 1)
            {
                nextZoom = 1;
            }

            ZoomPercentage = nextZoom;
        }

        private void zoomOutHandler(object obj)
        {
            _zoomSettings.ZoomMode = ZoomModeEnum.Explicit;
            if (Math.Round(_zoomSettings.ZoomPercentage, 1) <= _step)
            {
                return;
            }

            double nextZoom = ZoomPercentage - _step;
            if (ZoomPercentage > 1 && nextZoom < 1)
            {
                nextZoom = 1;
            }

            ZoomPercentage = nextZoom;
        }

        private void fitToWidthHandler(object obj)
        {
                _zoomSettings.ZoomMode = ZoomModeEnum.FitToWidth;
                double zoomFactor = 1;
                if (_documentWidth > 0 && (_actualWidth - 15) > 0)
                {
                    zoomFactor = (_actualWidth - 15) / _documentWidth;
                }
                else
                {
                    zoomFactor = 1;
                }

                ZoomPercentage = zoomFactor;
        }

        private void OnZoomPercentageChanged(double percentage)
        {
            if (ZoomPercentageChanged != null)
            {
                ZoomPercentageChangedArgs args = new ZoomPercentageChangedArgs(percentage);
                ZoomPercentageChanged(this, args);
            }
        }

        #endregion Private Method(s)

        public enum ZoomModeEnum
        {
            FitToWidth,
            Explicit
        }
    }

    internal class ZoomPercentageChangedArgs : EventArgs
    {
        public double Percentage { get; private set; }

        public ZoomPercentageChangedArgs(double percentage)
        {
            Percentage = percentage;
        }
    }
}