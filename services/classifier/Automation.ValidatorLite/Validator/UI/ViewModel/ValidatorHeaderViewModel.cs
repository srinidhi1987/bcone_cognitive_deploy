﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.Cognitive.Validator.Properties;
    internal class ValidatorHeaderViewModel : ValidatorViewModelBase
    {
        private QSetting _qSetting;

        #region Properties

        private string _taskName;
        public string TaskName { get { return _taskName; } }

        private string _queueName;

        public string QueueName { get { return _queueName; } }

        private string _projectName;

        public string ProjectName { get { return _projectName; } }
        public string RemainingDocumnetMessage
        {
            get
            {
                int? docCount = getRemainingDocumentsCount();
                if (docCount.HasValue)
                {
                    return $"{docCount.Value} {Automation.Cognitive.Validator.Properties.Resources.DocumentRemainingMessage}";
                }
                return string.Empty;
            }
        }

        #endregion Properties

        #region ctor

        public ValidatorHeaderViewModel(QSetting qSetting)
        {
            Refresh(qSetting);
        }

        #endregion ctor

        #region Public Method(s)

        public void Refresh(QSetting qSetting)
        {
            refreshQueue(qSetting);
            refreshBindings();
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private int? getRemainingDocumentsCount()
        {
            return (_qSetting?.QueueStats != null ? _qSetting.QueueStats.TotalNoOfDocumentsInQ : (int?)null);
        }

        private void refreshQueue(QSetting qSetting)
        {
            _qSetting = qSetting;
            assignQueueNameAndTaskName();
        }

        private void refreshBindings()
        {
            OnPropertyChanged(nameof(TaskName));
            OnPropertyChanged(nameof(QueueName));
            OnPropertyChanged(nameof(RemainingDocumnetMessage));
        }

        private void assignQueueNameAndTaskName()
        {
            if (_qSetting?.QueuePath != null)
            {
                if (!string.IsNullOrWhiteSpace(_qSetting.QueuePath.ServerPath))
                {
                    _queueName = _qSetting.QueuePath.DisplayPath;
                    _projectName = _qSetting.ProjectName;
                }

                if (!string.IsNullOrWhiteSpace(_qSetting.QueuePath.TaskName))
                {
                    _taskName = _qSetting.QueuePath.TaskName;
                }
            }
        }

        #endregion Private Method(s)
    }
}