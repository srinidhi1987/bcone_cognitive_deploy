﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    internal interface IRegion
    {
        string Id { get; }
        Rectangle Bounds { get; set; }

        bool IsSelected { get; set; }

        event EventHandler SelectionChanged;
    }

    internal abstract class RegionBase : ViewModelBase, IRegion
    {
        #region Properties

        public abstract Rectangle Bounds { get; set; }

        public abstract string Id { get; }

        private bool _isSelected = false;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));

                    if (SelectionChanged != null)
                    {
                        SelectionChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler SelectionChanged;

        #endregion Event(s)
    }

    internal class SIFRegion : RegionBase
    {
        private Field _field;

        #region Properties

        public override Rectangle Bounds
        {
            get
            {
                return _field.Bounds;
            }

            set
            {
                if (_field.Bounds != value)
                {
                    _field.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public override string Id
        {
            get
            {
                return _field.Id;
            }
        }

        #endregion Properties

        #region ctor

        public SIFRegion(Field field)
        {
            _field = field;
        }

        #endregion ctor
    }

    internal class FieldRegion : RegionBase
    {
        private FieldLayout _keyField;

        #region Properties

        public override Rectangle Bounds
        {
            get
            {
                return _keyField.Bounds;
            }

            set
            {
                if (_keyField.Bounds != value)
                {
                    _keyField.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public override string Id
        {
            get
            {
                return _keyField.Id;
            }
        }

        #endregion Properties

        #region ctor

        public FieldRegion(FieldLayout fieldLayout)
        {
            _keyField = fieldLayout;
        }

        #endregion ctor
    }

    internal class FieldDataRegion : RegionBase
    {
        private FieldData _fieldData;

        #region Properties

        public override Rectangle Bounds
        {
            get
            {
                return _fieldData.Value.Field.Bounds;
            }

            set
            {
                if (_fieldData.Value.Field.Bounds != value)
                {
                    _fieldData.Value.Field.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public override string Id
        {
            get
            {
                return _fieldData.Id;
            }
        }

        #endregion Properties

        #region ctor

        public FieldDataRegion(FieldData fieldData)
        {
            _fieldData = fieldData;
        }

        #endregion ctor
    }

    internal class ColumnHeaderRegion : RegionBase
    {
        private FieldLayout _keyField;

        #region Properties

        public override Rectangle Bounds
        {
            get
            {
                return _keyField.Bounds;
            }

            set
            {
                if (_keyField.Bounds != value)
                {
                    _keyField.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public override string Id
        {
            get
            {
                return _keyField.Id;
            }
        }

        #endregion Properties

        #region ctor

        public ColumnHeaderRegion(FieldLayout fieldLayout)
        {
            _keyField = fieldLayout;
        }

        #endregion ctor
    }

    internal class TableCellRegion : RegionBase
    {
        private FieldData _fieldData;

        #region Properties

        public override Rectangle Bounds
        {
            get
            {
                return _fieldData.Value.Field.Bounds;
            }

            set
            {
                if (_fieldData.Value.Field.Bounds != value)
                {
                    _fieldData.Value.Field.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public override string Id
        {
            get
            {
                return _fieldData.Id;
            }
        }

        #endregion Properties

        #region ctor

        public TableCellRegion(FieldData fieldData)
        {
            _fieldData = fieldData;
        }

        #endregion ctor
    }

    internal class RegionsViewModel : ValidatorViewModelBase
    {
        //private VBotDocument _document;

        #region Properties

        private ObservableCollection<IRegion> _regions = new ObservableCollection<IRegion>();

        public ObservableCollection<IRegion> Regions
        {
            get
            {
                return _regions;
            }

            set
            {
                _regions = value;
                //_regions.CollectionChanged += _regions_CollectionChanged;
            }
        }

        private IList<IRegion> _allRegions;

        internal IList<IRegion> AllRegions
        {
            get
            {
                return _allRegions;
            }
        }

        #endregion Properties

        #region Event(s)

        //public event EventHandler SelectionChanged;

        #endregion Event(s)

        #region ctor

        public RegionsViewModel(IList<IRegion> regions)
        {
            _allRegions = regions != null ? regions : new List<IRegion>();
        }

        #endregion ctor

        #region Public Method(s)

        public void AttachRegionsToUI()
        {
            if (_regions.Count == 0)
            {
                foreach (IRegion region in _allRegions)
                {
                    if (!(region is SIFRegion))
                    {
                        _regions.Add(region);
                    }
                }
            }
        }

        public void DetachRegionsFromUI()
        {
            _regions.Clear();
        }

        #endregion Public Method(s)

        #region Private Method(s)

        //private void _regions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.NewItems != null && e.OldStartingIndex == -1)
        //    {
        //        foreach (IRegion region in e.NewItems)
        //        {
        //            region.SelectionChanged += Region_SelectionChanged;
        //        }
        //    }
        //}

        //private void Region_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (SelectionChanged != null)
        //    {
        //        SelectionChanged(sender, e);
        //    }
        //}

        #endregion Private Method(s)
    }
}